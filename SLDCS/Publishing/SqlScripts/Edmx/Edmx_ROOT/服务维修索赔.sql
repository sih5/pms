/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2021/9/2 15:40:32                            */
/*==============================================================*/


drop procedure "Prc_InvalidRepairOrder"
/

alter table "DTMRepairContractItem"
   drop constraint FK_DTMREPAI_FK_REPCON_DTMREPA2
/

alter table "DTMRepairContractMaterial"
   drop constraint FK_DTMREPAI_FKT_REPCI_DTMREPAI
/

alter table "DTMRepairContractOrderlist"
   drop constraint FK_DTMREPAI_FK_REPCON_DTMREPAI
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ClaimApproverStrategy"');
  if num>0 then
    execute immediate 'drop table "ClaimApproverStrategy" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"CustomerServiceEvaluate"');
  if num>0 then
    execute immediate 'drop table "CustomerServiceEvaluate" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DTMRepairContract"');
  if num>0 then
    execute immediate 'drop table "DTMRepairContract" cascade constraints';
  end if;
end;
/

drop index "FK_RepCon_RepConItem_FK"
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DTMRepairContractItem"');
  if num>0 then
    execute immediate 'drop table "DTMRepairContractItem" cascade constraints';
  end if;
end;
/

drop index "FKt_RepCItem_RepCMaterial_FK"
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DTMRepairContractMaterial"');
  if num>0 then
    execute immediate 'drop table "DTMRepairContractMaterial" cascade constraints';
  end if;
end;
/

drop index "FK_RepCon_RepConOrderlist_FK"
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DTMRepairContractOrderlist"');
  if num>0 then
    execute immediate 'drop table "DTMRepairContractOrderlist" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ESBLogNCC"');
  if num>0 then
    execute immediate 'drop table "ESBLogNCC" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ExpenseAdjustmentBill"');
  if num>0 then
    execute immediate 'drop table "ExpenseAdjustmentBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ExpressVehicleList"');
  if num>0 then
    execute immediate 'drop table "ExpressVehicleList" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"FDWarrantyMessage"');
  if num>0 then
    execute immediate 'drop table "FDWarrantyMessage" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"FaultyPartsSupplierAssembly"');
  if num>0 then
    execute immediate 'drop table "FaultyPartsSupplierAssembly" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"InteSettleBillInvoiceLink"');
  if num>0 then
    execute immediate 'drop table "InteSettleBillInvoiceLink" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"IntegralClaimBill"');
  if num>0 then
    execute immediate 'drop table "IntegralClaimBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"IntegralClaimBillDetail"');
  if num>0 then
    execute immediate 'drop table "IntegralClaimBillDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"IntegralClaimBillSettle"');
  if num>0 then
    execute immediate 'drop table "IntegralClaimBillSettle" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"IntegralClaimBillSettleDtl"');
  if num>0 then
    execute immediate 'drop table "IntegralClaimBillSettleDtl" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"IntegralClaimInvoiceInfo"');
  if num>0 then
    execute immediate 'drop table "IntegralClaimInvoiceInfo" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"IntegralSettleDictate"');
  if num>0 then
    execute immediate 'drop table "IntegralSettleDictate" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"MaintainRedProductMap"');
  if num>0 then
    execute immediate 'drop table "MaintainRedProductMap" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"MarketABQualityInformation"');
  if num>0 then
    execute immediate 'drop table "MarketABQualityInformation" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"OutofWarrantyPayment"');
  if num>0 then
    execute immediate 'drop table "OutofWarrantyPayment" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PQIReport"');
  if num>0 then
    execute immediate 'drop table "PQIReport" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartRequisitionReturnBill"');
  if num>0 then
    execute immediate 'drop table "PartRequisitionReturnBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsClaimApplication"');
  if num>0 then
    execute immediate 'drop table "PartsClaimApplication" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsClaimApplicationDetail"');
  if num>0 then
    execute immediate 'drop table "PartsClaimApplicationDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsClaimMaterialDetail"');
  if num>0 then
    execute immediate 'drop table "PartsClaimMaterialDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsClaimOrder"');
  if num>0 then
    execute immediate 'drop table "PartsClaimOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsClaimOrderNew"');
  if num>0 then
    execute immediate 'drop table "PartsClaimOrderNew" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsClaimRepairItemDetail"');
  if num>0 then
    execute immediate 'drop table "PartsClaimRepairItemDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PostSaleClaims_Tmp"');
  if num>0 then
    execute immediate 'drop table "PostSaleClaims_Tmp" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PreSaleCheckOrder"');
  if num>0 then
    execute immediate 'drop table "PreSaleCheckOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PreSaleItem"');
  if num>0 then
    execute immediate 'drop table "PreSaleItem" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PreSalesCheckDetail"');
  if num>0 then
    execute immediate 'drop table "PreSalesCheckDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RedPacketsMsg"');
  if num>0 then
    execute immediate 'drop table "RedPacketsMsg" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairClaimAppDetail"');
  if num>0 then
    execute immediate 'drop table "RepairClaimAppDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairClaimApplication"');
  if num>0 then
    execute immediate 'drop table "RepairClaimApplication" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairClaimBill"');
  if num>0 then
    execute immediate 'drop table "RepairClaimBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairClaimItemDetail"');
  if num>0 then
    execute immediate 'drop table "RepairClaimItemDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairClaimMaterialDetail"');
  if num>0 then
    execute immediate 'drop table "RepairClaimMaterialDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairClaimSupplierDetail"');
  if num>0 then
    execute immediate 'drop table "RepairClaimSupplierDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairIntegral"');
  if num>0 then
    execute immediate 'drop table "RepairIntegral" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairIntegralRightsDetail"');
  if num>0 then
    execute immediate 'drop table "RepairIntegralRightsDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairMemberRightsDetail"');
  if num>0 then
    execute immediate 'drop table "RepairMemberRightsDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairOrder"');
  if num>0 then
    execute immediate 'drop table "RepairOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairOrderFaultReason"');
  if num>0 then
    execute immediate 'drop table "RepairOrderFaultReason" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairOrderItemDetail"');
  if num>0 then
    execute immediate 'drop table "RepairOrderItemDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairOrderMaterialDetail"');
  if num>0 then
    execute immediate 'drop table "RepairOrderMaterialDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairOrderSupplierDetail"');
  if num>0 then
    execute immediate 'drop table "RepairOrderSupplierDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairTemplet"');
  if num>0 then
    execute immediate 'drop table "RepairTemplet" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairTempletItemDetail"');
  if num>0 then
    execute immediate 'drop table "RepairTempletItemDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairTempletMaterialDetail"');
  if num>0 then
    execute immediate 'drop table "RepairTempletMaterialDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairWorkOrder"');
  if num>0 then
    execute immediate 'drop table "RepairWorkOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairWorkOrderMd"');
  if num>0 then
    execute immediate 'drop table "RepairWorkOrderMd" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RepairWorkOrderMdFaultReason"');
  if num>0 then
    execute immediate 'drop table "RepairWorkOrderMdFaultReason" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ReturnVisitQuest"');
  if num>0 then
    execute immediate 'drop table "ReturnVisitQuest" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ReturnVisitQuestDetail"');
  if num>0 then
    execute immediate 'drop table "ReturnVisitQuestDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ServiceTripClaimApplication"');
  if num>0 then
    execute immediate 'drop table "ServiceTripClaimApplication" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ServiceTripClaimBill"');
  if num>0 then
    execute immediate 'drop table "ServiceTripClaimBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SupplierExpenseAdjustBill"');
  if num>0 then
    execute immediate 'drop table "SupplierExpenseAdjustBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"TransactionInterfaceList"');
  if num>0 then
    execute immediate 'drop table "TransactionInterfaceList" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"TransactionInterfaceLog"');
  if num>0 then
    execute immediate 'drop table "TransactionInterfaceLog" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"VehicleBrandPMSRealation"');
  if num>0 then
    execute immediate 'drop table "VehicleBrandPMSRealation" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"VehicleMileageChangeRecord"');
  if num>0 then
    execute immediate 'drop table "VehicleMileageChangeRecord" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ClaimApproverStrategy"');
  if num>0 then
    execute immediate 'drop sequence "S_ClaimApproverStrategy"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_CustomerServiceEvaluate"');
  if num>0 then
    execute immediate 'drop sequence "S_CustomerServiceEvaluate"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_DTMRepairContractOrderlist"');
  if num>0 then
    execute immediate 'drop sequence "S_DTMRepairContractOrderlist"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ESBLogNCC"');
  if num>0 then
    execute immediate 'drop sequence "S_ESBLogNCC"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ExpenseAdjustmentBill"');
  if num>0 then
    execute immediate 'drop sequence "S_ExpenseAdjustmentBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ExpressVehicleList"');
  if num>0 then
    execute immediate 'drop sequence "S_ExpressVehicleList"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_FDWarrantyMessage"');
  if num>0 then
    execute immediate 'drop sequence "S_FDWarrantyMessage"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_FaultyPartsSupplierAssembly"');
  if num>0 then
    execute immediate 'drop sequence "S_FaultyPartsSupplierAssembly"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_InteSettleBillInvoiceLink"');
  if num>0 then
    execute immediate 'drop sequence "S_InteSettleBillInvoiceLink"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_IntegralClaimBill"');
  if num>0 then
    execute immediate 'drop sequence "S_IntegralClaimBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_IntegralClaimBillDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_IntegralClaimBillDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_IntegralClaimBillSettle"');
  if num>0 then
    execute immediate 'drop sequence "S_IntegralClaimBillSettle"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_IntegralClaimBillSettleDtl"');
  if num>0 then
    execute immediate 'drop sequence "S_IntegralClaimBillSettleDtl"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_IntegralClaimInvoiceInfo"');
  if num>0 then
    execute immediate 'drop sequence "S_IntegralClaimInvoiceInfo"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_IntegralSettleDictate"');
  if num>0 then
    execute immediate 'drop sequence "S_IntegralSettleDictate"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_MaintainRedProductMap"');
  if num>0 then
    execute immediate 'drop sequence "S_MaintainRedProductMap"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_MarketABQualityInformation"');
  if num>0 then
    execute immediate 'drop sequence "S_MarketABQualityInformation"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_OutofWarrantyPayment"');
  if num>0 then
    execute immediate 'drop sequence "S_OutofWarrantyPayment"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PQIReport"');
  if num>0 then
    execute immediate 'drop sequence "S_PQIReport"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartRequisitionReturnBill"');
  if num>0 then
    execute immediate 'drop sequence "S_PartRequisitionReturnBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsClaimApplication"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsClaimApplication"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsClaimApplicationDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsClaimApplicationDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsClaimMaterialDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsClaimMaterialDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsClaimOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsClaimOrder"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsClaimOrderNew"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsClaimOrderNew"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsClaimRepairItemDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsClaimRepairItemDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PostSaleClaims_Tmp"');
  if num>0 then
    execute immediate 'drop sequence "S_PostSaleClaims_Tmp"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PreSaleCheckOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_PreSaleCheckOrder"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PreSaleItem"');
  if num>0 then
    execute immediate 'drop sequence "S_PreSaleItem"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PreSalesCheckDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_PreSalesCheckDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RedPacketsMsg"');
  if num>0 then
    execute immediate 'drop sequence "S_RedPacketsMsg"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairClaimAppDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairClaimAppDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairClaimApplication"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairClaimApplication"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairClaimBill"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairClaimBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairClaimItemDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairClaimItemDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairClaimMaterialDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairClaimMaterialDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairClaimSupplierDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairClaimSupplierDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairIntegral"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairIntegral"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairIntegralRightsDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairIntegralRightsDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairMemberRightsDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairMemberRightsDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairOrder"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairOrderFaultReason"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairOrderFaultReason"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairOrderItemDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairOrderItemDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairOrderMaterialDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairOrderMaterialDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairOrderSupplierDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairOrderSupplierDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairTemplet"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairTemplet"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairTempletItemDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairTempletItemDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairTempletMaterialDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairTempletMaterialDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairWorkOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairWorkOrder"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairWorkOrderMd"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairWorkOrderMd"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RepairWorkOrderMdFaultReason"');
  if num>0 then
    execute immediate 'drop sequence "S_RepairWorkOrderMdFaultReason"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ReturnVisitQuest"');
  if num>0 then
    execute immediate 'drop sequence "S_ReturnVisitQuest"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ReturnVisitQuestDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_ReturnVisitQuestDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ServiceTripClaimApplication"');
  if num>0 then
    execute immediate 'drop sequence "S_ServiceTripClaimApplication"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ServiceTripClaimBill"');
  if num>0 then
    execute immediate 'drop sequence "S_ServiceTripClaimBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SupplierExpenseAdjustBill"');
  if num>0 then
    execute immediate 'drop sequence "S_SupplierExpenseAdjustBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_TransactionInterfaceList"');
  if num>0 then
    execute immediate 'drop sequence "S_TransactionInterfaceList"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_TransactionInterfaceLog"');
  if num>0 then
    execute immediate 'drop sequence "S_TransactionInterfaceLog"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_VehicleBrandPMSRealation"');
  if num>0 then
    execute immediate 'drop sequence "S_VehicleBrandPMSRealation"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_VehicleMileageChangeRecord"');
  if num>0 then
    execute immediate 'drop sequence "S_VehicleMileageChangeRecord"';
  end if;
end;
/

create sequence "S_ClaimApproverStrategy"
/

create sequence "S_CustomerServiceEvaluate"
/

create sequence "S_DTMRepairContractOrderlist"
/

create sequence "S_ESBLogNCC"
/

create sequence "S_ExpenseAdjustmentBill"
/

create sequence "S_ExpressVehicleList"
/

create sequence "S_FDWarrantyMessage"
/

create sequence "S_FaultyPartsSupplierAssembly"
/

create sequence "S_InteSettleBillInvoiceLink"
/

create sequence "S_IntegralClaimBill"
/

create sequence "S_IntegralClaimBillDetail"
/

create sequence "S_IntegralClaimBillSettle"
/

create sequence "S_IntegralClaimBillSettleDtl"
/

create sequence "S_IntegralClaimInvoiceInfo"
/

create sequence "S_IntegralSettleDictate"
/

create sequence "S_MaintainRedProductMap"
/

create sequence "S_MarketABQualityInformation"
/

create sequence "S_OutofWarrantyPayment"
/

create sequence "S_PQIReport"
/

create sequence "S_PartRequisitionReturnBill"
/

create sequence "S_PartsClaimApplication"
/

create sequence "S_PartsClaimApplicationDetail"
/

create sequence "S_PartsClaimMaterialDetail"
/

create sequence "S_PartsClaimOrder"
/

create sequence "S_PartsClaimOrderNew"
/

create sequence "S_PartsClaimRepairItemDetail"
/

create sequence "S_PostSaleClaims_Tmp"
/

create sequence "S_PreSaleCheckOrder"
/

create sequence "S_PreSaleItem"
/

create sequence "S_PreSalesCheckDetail"
/

create sequence "S_RedPacketsMsg"
/

create sequence "S_RepairClaimAppDetail"
/

create sequence "S_RepairClaimApplication"
/

create sequence "S_RepairClaimBill"
/

create sequence "S_RepairClaimItemDetail"
/

create sequence "S_RepairClaimMaterialDetail"
/

create sequence "S_RepairClaimSupplierDetail"
/

create sequence "S_RepairIntegral"
/

create sequence "S_RepairIntegralRightsDetail"
/

create sequence "S_RepairMemberRightsDetail"
/

create sequence "S_RepairOrder"
/

create sequence "S_RepairOrderFaultReason"
/

create sequence "S_RepairOrderItemDetail"
/

create sequence "S_RepairOrderMaterialDetail"
/

create sequence "S_RepairOrderSupplierDetail"
/

create sequence "S_RepairTemplet"
/

create sequence "S_RepairTempletItemDetail"
/

create sequence "S_RepairTempletMaterialDetail"
/

create sequence "S_RepairWorkOrder"
/

create sequence "S_RepairWorkOrderMd"
/

create sequence "S_RepairWorkOrderMdFaultReason"
/

create sequence "S_ReturnVisitQuest"
/

create sequence "S_ReturnVisitQuestDetail"
/

create sequence "S_ServiceTripClaimApplication"
/

create sequence "S_ServiceTripClaimBill"
/

create sequence "S_SupplierExpenseAdjustBill"
/

create sequence "S_TransactionInterfaceList"
/

create sequence "S_TransactionInterfaceLog"
/

create sequence "S_VehicleBrandPMSRealation"
/

create sequence "S_VehicleMileageChangeRecord"
/

/*==============================================================*/
/* Table: "ClaimApproverStrategy"                               */
/*==============================================================*/
create table "ClaimApproverStrategy"  (
   "Id"                 NUMBER(9)                       not null,
   "Branchid"           NUMBER(9)                       not null,
   "BranchCode"         VARCHAR2(50),
   "BranchName"         VARCHAR2(100),
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryName" VARCHAR2(100),
   "FeeFrom"            NUMBER(19,4),
   "FeeTo"              NUMBER(19,4),
   "AfterApproverStatus" NUMBER(9),
   "InitialApproverStatus" NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_CLAIMAPPROVERSTRATEGY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "CustomerServiceEvaluate"                             */
/*==============================================================*/
create table "CustomerServiceEvaluate"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairClaimBillId"  NUMBER(9)                       not null,
   "RepairClaimBillCode" VARCHAR2(100)                   not null,
   "EvaluateSource"     VARCHAR2(500)                   not null,
   "EvaluateGrade"      NUMBER(9)                       not null,
   "DisContentFactor"   NUMBER(9),
   "EvaluateTime"       DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   constraint PK_CUSTOMERSERVICEEVALUATE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "DTMRepairContract"                                   */
/*==============================================================*/
create table "DTMRepairContract"  (
   "DTMObjid"           VARCHAR2(40)                    not null,
   "DCSRepairOrderId"   NUMBER(9),
   "Code"               VARCHAR2(50),
   "SerialCode"         VARCHAR2(40),
   "CustomerName"       VARCHAR2(100),
   "CellNumber"         VARCHAR2(50),
   "Address"            VARCHAR2(200),
   "ZipCode"            VARCHAR2(6),
   "Vin"                VARCHAR2(50)                    not null,
   "EngineCode"         VARCHAR2(50),
   "PlateNumber"        VARCHAR2(20),
   "VehicleModelCode"   VARCHAR2(50),
   "VehicleModelName"   VARCHAR2(100),
   "Mileage"            NUMBER(9),
   "RepairType"         NUMBER(9),
   "RepairTime"         DATE,
   "ExpectedFinishTime" DATE,
   "CompletionTime"     DATE,
   "VehicleObjects"     VARCHAR2(200),
   "FuelAmount"         NUMBER(15,6),
   "Description"        VARCHAR2(200),
   "CustomerComplaint"  VARCHAR2(200),
   "WorkingFee"         NUMBER(19,4),
   "MaterialFee"        NUMBER(19,4),
   "OtherFee"           NUMBER(19,4),
   "OtherFeeDescription" VARCHAR2(200),
   "IsOutService"       NUMBER(1),
   "IsOutClaim"         NUMBER(1),
   "TechnicalServiceActivityCode" VARCHAR2(50),
   "ClaimStatus"        NUMBER(9),
   "MaintenanceStatus"  NUMBER(9),
   "Status"             NUMBER(9),
   "Remark"             VARCHAR2(200),
   "OutletName"         VARCHAR2(100),
   "VehicleRepairName"  VARCHAR2(100),
   "VehicleRepairAddress" VARCHAR2(100),
   "VehicleRepairCellPhone" VARCHAR2(20),
   "BelongBrand"        VARCHAR2(50),
   "BrandCode"          VARCHAR2(50),
   "BrandName"          VARCHAR2(100),
   "DCSCorporationId"   NUMBER(9),
   "CorporationId"      NUMBER(9),
   "CorporationName"    VARCHAR2(100),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "SPType"             NUMBER(9),
   "RepairObjectName"   VARCHAR2(100),
   "CauseAnalyse"       VARCHAR2(200),
   "RegionCode"         VARCHAR2(50),
   "TravelDestination"  VARCHAR2(200),
   "Range"              NUMBER(9),
   "TravelDistance"     NUMBER(9),
   "StartTime"          DATE,
   "EndTime"            DATE,
   "OutPersonCode"      VARCHAR2(100),
   "OutVehicleKind"     NUMBER(1),
   "OutDays"            NUMBER(9),
   "OutNumber"          NUMBER(9),
   "HighwayFreight"     NUMBER(19,4),
   "OutFarePrice"       NUMBER(19,4),
   "SubsidyPrice"       NUMBER(19,4),
   "TotalExpense"       NUMBER(19,4),
   constraint PK_DTMREPAIRCONTRACT primary key ("DTMObjid")
)
/

/*==============================================================*/
/* Table: "DTMRepairContractItem"                               */
/*==============================================================*/
create table "DTMRepairContractItem"  (
   "DTMObjid"           VARCHAR2(40)                    not null,
   "DTMRepairContractItemObjid" VARCHAR2(40)                    not null,
   "RepairItemCode"     VARCHAR2(50),
   "RepairItemName"     VARCHAR2(200),
   "AccountingProperty" NUMBER(9),
   "Hours"              NUMBER(15,6),
   "BWorkHour"          NUMBER(9,2),
   "Price"              NUMBER(19,4),
   "WorkingFee"         NUMBER(19,4),
   "WorkingFeeDiscount" NUMBER(19,4),
   "Repairman"          VARCHAR2(100),
   "RepairReason"       VARCHAR2(200),
   "ClaimStatus"        NUMBER(9),
   "Remark"             VARCHAR2(200),
   constraint PK_DTMREPAIRCONTRACTITEM primary key ("DTMRepairContractItemObjid")
)
/

/*==============================================================*/
/* Index: "FK_RepCon_RepConItem_FK"                             */
/*==============================================================*/
create index "FK_RepCon_RepConItem_FK" on "DTMRepairContractItem" (
   "DTMObjid" ASC
)
/

/*==============================================================*/
/* Table: "DTMRepairContractMaterial"                           */
/*==============================================================*/
create table "DTMRepairContractMaterial"  (
   "DTMRepairContractMaterialObjid" VARCHAR2(40)                    not null,
   "DTMRepairContractItemObjid" VARCHAR2(40)                    not null,
   "DTMRepairContractObjid" VARCHAR2(40)                    not null,
   "SparePartCode"      VARCHAR2(50),
   "SparePartName"      VARCHAR2(100),
   "AccountingProperty" NUMBER(9),
   "MeasureUnitName"    VARCHAR2(100),
   "Amount"             NUMBER(9),
   "Price"              NUMBER(19,4),
   "GuidePrice"         NUMBER(19,4),
   "MaterialFee"        NUMBER(19,4),
   "MaterialFeeDiscount" NUMBER(19,4),
   "ClaimStatus"        NUMBER(9),
   "Remark"             VARCHAR2(200),
   "OldSparePartCode"   VARCHAR2(50),
   "OldSparePartName"   VARCHAR2(100),
   "OldSparePartSupplierCode" VARCHAR2(50),
   "OldSparePartSupplierName" VARCHAR2(100),
   "UsedPartsBarCode"   VARCHAR2(50),
   constraint PK_DTMREPAIRCONTRACTMATERIAL primary key ("DTMRepairContractMaterialObjid")
)
/

/*==============================================================*/
/* Index: "FKt_RepCItem_RepCMaterial_FK"                        */
/*==============================================================*/
create index "FKt_RepCItem_RepCMaterial_FK" on "DTMRepairContractMaterial" (
   "DTMRepairContractItemObjid" ASC
)
/

/*==============================================================*/
/* Table: "DTMRepairContractOrderlist"                          */
/*==============================================================*/
create table "DTMRepairContractOrderlist"  (
   "Id"                 NUMBER(9)                       not null,
   "DTMRepairContractObjid" VARCHAR2(40)                    not null,
   "DCSRepairOrderId"   NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "WarrantyStatus"     NUMBER(9),
   constraint PK_DTMREPAIRCONTRACTORDERLIST primary key ("Id")
)
/

/*==============================================================*/
/* Index: "FK_RepCon_RepConOrderlist_FK"                        */
/*==============================================================*/
create index "FK_RepCon_RepConOrderlist_FK" on "DTMRepairContractOrderlist" (
   "DTMRepairContractObjid" ASC
)
/

/*==============================================================*/
/* Table: "ESBLogNCC"                                           */
/*==============================================================*/
create table "ESBLogNCC"  (
   "Id"                 NUMBER(9)                       not null,
   "InterfaceName"      VARCHAR2(50),
   "SyncNumberBegin"    NUMBER(9),
   "SyncNumberEnd"      NUMBER(9),
   "Message"            CLOB,
   "TheDate"            DATE,
   RNO                  NUMBER(9),
   constraint PK_ESBLOGNCC primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ExpenseAdjustmentBill"                               */
/*==============================================================*/
create table "ExpenseAdjustmentBill"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Status"             NUMBER(9)                       not null,
   "SettlementStatus"   NUMBER(9)                       not null,
   "DebitOrReplenish"   NUMBER(9)                       not null,
   "TransactionCategory" NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "ProductLineType"    NUMBER(9),
   "ServiceProductLineId" NUMBER(9),
   "DealerId"           NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(20)                    not null,
   "DealerName"         VARCHAR2(100)                   not null,
   "SourceType"         NUMBER(9),
   "SourceId"           NUMBER(9),
   "SourceCode"         VARCHAR2(50),
   "RepairClaimBillCode" VARCHAR2(50),
   "DealerContactPerson" VARCHAR2(50),
   "DealerPhoneNumber"  VARCHAR2(50),
   "TransactionAmount"  NUMBER(19,4),
   "TransactionReason"  VARCHAR2(200),
   "IfClaimToResponsible" NUMBER(1)                       not null,
   "GradeCoefficientId" NUMBER(9),
   "ResponsibleUnitId"  NUMBER(9),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_EXPENSEADJUSTMENTBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ExpressVehicleList"                                  */
/*==============================================================*/
create table "ExpressVehicleList"  (
   "Id"                 NUMBER(9)                       not null,
   "MarketABQualityInfoId" NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   "VehicleId"          NUMBER(9)                       not null,
   "VehicleCode"        VARCHAR2(50)                    not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryName" VARCHAR2(50)                    not null,
   "OutOfFactoryDate"   DATE,
   "SalesDate"          DATE,
   "FaultyMileage"      NUMBER(15,6),
   "FaultTime"          DATE,
   "BatchNumber"        VARCHAR2(50),
   constraint PK_EXPRESSVEHICLELIST primary key ("Id")
)
/

/*==============================================================*/
/* Table: "FDWarrantyMessage"                                   */
/*==============================================================*/
create table "FDWarrantyMessage"  (
   "Id"                 NUMBER(9)                       not null,
   "gengxinshijian"     DATE,
   "fengongsibianhao"   VARCHAR2(50),
   "fengongsimingcheng" VARCHAR2(200),
   "suopeidanbianhao"   VARCHAR2(50)                    not null,
   "danjuzhuangtai"     VARCHAR2(50),
   "weixiuleixing"      VARCHAR2(50),
   "Vin"                VARCHAR2(50)                    not null,
   "chuchangbianhao"    VARCHAR2(50),
   "pinpai"             VARCHAR2(50),
   "zhengchechanpinxian" VARCHAR2(50),
   "fuwuchanpinxian"    VARCHAR2(50),
   "chanpinxianleixing" VARCHAR2(50),
   "chuchangriqi"       DATE,
   "goumairiqi"         DATE,
   "xingshilicheng"     NUMBER(19,2),
   "shouciguzhanglicheng" NUMBER(19,2),
   "gongkuangleibie"    VARCHAR2(50),
   "qudongxingshi"      VARCHAR2(50),
   "chexing"            VARCHAR2(50),
   "gonggaohao"         VARCHAR2(50),
   "fadongjixinghao"    VARCHAR2(50),
   "fadongjibianhao"    VARCHAR2(50),
   "shichangbu"         VARCHAR2(50),
   "fuwuzhanbianhao"    VARCHAR2(50),
   "fuwuzhanmingcheng"  VARCHAR2(200),
   "yewubianhao"        VARCHAR2(50),
   "chelianglianxiren"  VARCHAR2(50),
   "chanpinbianhao"     VARCHAR2(50),
   "baoxiushijian"      DATE,
   "chuangjianriqi"     DATE,
   "guzhangdaima"       VARCHAR2(50),
   "guzhangxianxiangmiaoshu" VARCHAR2(500),
   "guzhangmoshi"       VARCHAR2(50),
   "huoshoujiantuhao"   VARCHAR2(50),
   "huoshoujianmingcheng" VARCHAR2(100),
   "huoshoujianshengchanchangjia" VARCHAR2(50),
   "zerendanweibianma"  VARCHAR2(50),
   "zerendanweimingcheng" VARCHAR2(200),
   "huoshoujiansuoshuzongcheng" VARCHAR2(50),
   "peijiansuoshuzongcheng" VARCHAR2(50),
   "gongyingshangquerenzhuangtai" VARCHAR2(50),
   "fengongsishenheshijian" DATE,
   "zhongshenyijian"    VARCHAR2(500),
   "zhongshenshijian"   DATE,
   "zhongshenren"       VARCHAR2(50),
   "gongyingshangquerenyijian" VARCHAR2(500),
   "iszerengongyingshangsuopei" VARCHAR2(50),
   "weixiusuopeishenqingdanhao" VARCHAR2(50),
   "weixiusuopeishenqingleixing" VARCHAR2(50),
   "waichusuopeidanbianhao" VARCHAR2(50),
   "waichusuopeidanzhuangtai" VARCHAR2(50),
   "waichusuopeishenqingdanhao" VARCHAR2(50),
   "waichurenshu"       NUMBER(19,2),
   "waichutianshu"      NUMBER(19,2),
   "waichulicheng"      NUMBER(19,2),
   "waichuchefeidanjia" NUMBER(19,2),
   "waichulutubuzhudanjia" NUMBER(19,2),
   "waichuleixing"      VARCHAR2(50),
   "waichuyuanyin"      VARCHAR2(500),
   "waichushenheyijian" VARCHAR2(500),
   "suopeidanqitafeiyongshuoming" VARCHAR2(500),
   "waichuqitafeiyongshuoming" VARCHAR2(500),
   "guzhangbeizhu"      VARCHAR2(500),
   "jiesuanzhuangtai"   VARCHAR2(50),
   "jiesuanshijian"     DATE,
   "cailiaofei"         NUMBER(19,2),
   "gongshifei"         NUMBER(19,2),
   "waichufuwufei"      NUMBER(19,2),
   "peijianguanlifei"   NUMBER(19,2),
   "guzhangjianqingtuiyunfei" NUMBER(19,2),
   "qitafeiyong"        NUMBER(19,2),
   "feiyongheji"        NUMBER(19,2),
   "gongshidinge"       NUMBER(19,2),
   "gongshidanjia"      NUMBER(19,2),
   "fuwuhuodongbianhao" VARCHAR2(50),
   "fuwuhuodongmingcheng" VARCHAR2(200),
   "fuwuhuodongleixing" VARCHAR2(50),
   "fuwuhuodongneirong" VARCHAR2(1000),
   "kehuleixing"        VARCHAR2(50),
   "feiyongfenlei"      VARCHAR2(50),
   "wenjianbianhao"     VARCHAR2(50),
   "baogaojianshu"      VARCHAR2(500),
   "shichangbuyijian"   VARCHAR2(500),
   "pizhunren"          VARCHAR2(50),
   "shejijine"          NUMBER(19,2),
   "zhiliangbuyijian"   VARCHAR2(500),
   "kuaibaodaihao"      VARCHAR2(50),
   "shejicheliang"      VARCHAR2(50),
   "wentimiaoshu"       VARCHAR2(1000),
   "zhenggaiweixiufangan" VARCHAR2(1000),
   "suopeidanwei"       VARCHAR2(50),
   "isshiyebusuopei"    VARCHAR2(50),
   "zongchengjianmingcheng" VARCHAR2(100),
   "shichangABkuaibaobianhao" VARCHAR2(50),
   "weixiusuopeidanbianhao" VARCHAR2(50),
   "zhuangtai"          VARCHAR2(50),
   "lianxidianhua"      VARCHAR2(50),
   "koukuanjine"        NUMBER(19,2),
   "bukuanjine"         NUMBER(19,2),
   "suopeidanweishiyebubianma" VARCHAR2(50),
   "suopeidanweishiyebumingcheng" VARCHAR2(200),
   "waichugonglishu"    NUMBER(19,2),
   "gaosugonglutuochefei" NUMBER(19,2),
   "waidijiuyuandi"     VARCHAR2(200),
   "chepaihao"          VARCHAR2(50),
   "shendanleixing"     VARCHAR2(50),
   "suopeigongyingshangbianhao" VARCHAR2(50),
   "suopeigongyingshangmingcheng" VARCHAR2(200),
   "chuangjianshijian"  DATE,
   "kehudizhi"          VARCHAR2(200),
   "weixiushuxing"      VARCHAR2(50),
   "waichusuopeibeizhuxinxi" VARCHAR2(500),
   "fuwuzhanzhucezhenghao" VARCHAR2(50),
   "fuwuzhanzhucemingcheng" VARCHAR2(200),
   "fuwuzhanyouzhengbianma" VARCHAR2(50),
   "fuwuzhangudingdianhua" VARCHAR2(50),
   "fuwuzhanchuanzhen"  VARCHAR2(50),
   "fuwuzhanqiyeleixing" VARCHAR2(50),
   "fuwuzhanshengbianma" VARCHAR2(50),
   "fuwuzhanshengmingcheng" VARCHAR2(200),
   "fuwuzhanshibianma"  VARCHAR2(50),
   "fuwuzhanshimingcheng" VARCHAR2(200),
   "fuwuzhanqubianma"   VARCHAR2(50),
   "fuwuzhanqumingcheng" VARCHAR2(200),
   "fuwuzhandizhi"      VARCHAR2(200),
   "fuwuzhanlianxiren"  VARCHAR2(50),
   "fuwuzhandianziyouxiang" VARCHAR2(50),
   "fuwuzhanjingli"     VARCHAR2(50),
   "fuwuzhanchengliriqi" DATE,
   "fuwuzhanzhuangtai"  VARCHAR2(50),
   "erjifuwuzhanbianhao" VARCHAR2(50),
   "erjifuwuzhanmingcheng" VARCHAR2(200),
   "erjifuwuzhangudingdianhua" VARCHAR2(50),
   "erjifuwuzhanqiyeleixing" VARCHAR2(50),
   "erjifuwuzhandizhi"  VARCHAR2(200),
   "erjifuwuzhandianziyouxiang" VARCHAR2(50),
   "erjifuwuzhanjingli" VARCHAR2(50),
   "erjifuwuzhanzhuangtai" VARCHAR2(50),
   "Status"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifyTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "RowVersion"         TIMESTAMP,
   constraint PK_FDWARRANTYMESSAGE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "FaultyPartsSupplierAssembly"                         */
/*==============================================================*/
create table "FaultyPartsSupplierAssembly"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Name"               VARCHAR2(50),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_FAULTYPARTSSUPPLIERASSEMBLY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "InteSettleBillInvoiceLink"                           */
/*==============================================================*/
create table "InteSettleBillInvoiceLink"  (
   "Id"                 NUMBER(9)                       not null,
   "IntegralClaimInvoiceId" NUMBER(9)                       not null,
   "IntegralClaimSettlementBillId" NUMBER(9)                       not null,
   constraint PK_INTESETTLEBILLINVOICELINK primary key ("Id")
)
/

/*==============================================================*/
/* Table: "IntegralClaimBill"                                   */
/*==============================================================*/
create table "IntegralClaimBill"  (
   "Id"                 NUMBER(9)                       not null,
   "IntegralClaimBillCode" VARCHAR2(50)                    not null,
   "RepairOrderId"      NUMBER(9)                       not null,
   "RepairCode"         VARCHAR2(50)                    not null,
   "DealerId"           NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(50)                    not null,
   "DealerName"         VARCHAR2(100),
   "DealerBusinessCode" VARCHAR2(50),
   "IntegralClaimBillType" NUMBER(9),
   "IntegralStatus"     NUMBER(9),
   "SettlementStatus"   NUMBER(9),
   "BranchId"           NUMBER(9),
   "BranchCode"         VARCHAR2(50),
   "BranchName"         VARCHAR2(100),
   "BrandId"            NUMBER(9),
   "BrandName"          VARCHAR2(50),
   "MemberCode"         VARCHAR2(15),
   "MemberName"         VARCHAR2(100),
   "MemberPhone"        VARCHAR2(30),
   "IDType"             VARCHAR2(100),
   "IDNumer"            VARCHAR2(50),
   "BusinessType"       VARCHAR2(200),
   "MemberRank"         VARCHAR2(100),
   "MemberUsedIntegral" NUMBER(19,4),
   "MemberUsedPrice"    NUMBER(19,4),
   "MemberRightsCost"   NUMBER(19,4),
   "RedPacketsCode"     VARCHAR2(50),
   "RedPacketsPhone"    VARCHAR2(30),
   "RedPacketsUsedPrice" NUMBER(19,4),
   "DeductionTotalAmount" NUMBER(19,4),
   "DeductionLaborCost" NUMBER(19,4),
   "CostDiscount"       NUMBER(19,4),
   "Discount"           NUMBER(19,4),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "CheckerId"          NUMBER(9),
   "CheckerName"        VARCHAR2(100),
   "CheckTime"          DATE,
   "Memo"               VARCHAR2(100),
   "RowVersion"         TIMESTAMP,
   "ExistingIntegral"   NUMBER(19,4),
   "ApprovalComment"    VARCHAR2(1000),
   "DebitAmount"        NUMBER(19,4),
   "ComplementAmount"   NUMBER(19,4),
   "RedPacketsType"     NUMBER(9),
   "RedPacketsRange"    NUMBER(9),
   "RedPostAmount"      NUMBER(19,4),
   "RejectComment"      VARCHAR2(200),
   "ApproveCommentHistory" VARCHAR2(4000),
   "LssuingUnit"        NUMBER(9),
   constraint PK_INTEGRALCLAIMBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "IntegralClaimBillDetail"                             */
/*==============================================================*/
create table "IntegralClaimBillDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "IntegralClaimItemBillId" NUMBER(9)                       not null,
   "RightsCode"         VARCHAR2(100),
   "RightsItem"         VARCHAR2(100),
   "IsRights"           NUMBER(1),
   "BenefiType"         VARCHAR2(30),
   "BenefiUom"          NUMBER(9),
   "DiscountNum"        NUMBER(19,4),
   "Time"               NUMBER(9),
   constraint PK_INTEGRALCLAIMBILLDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "IntegralClaimBillSettle"                             */
/*==============================================================*/
create table "IntegralClaimBillSettle"  (
   "Id"                 NUMBER(9)                       not null,
   "BonusPointsSettleCode" VARCHAR2(50)                    not null,
   "DealerId"           NUMBER(9),
   "DealerCode"         VARCHAR2(50),
   "DealerName"         VARCHAR2(100),
   "DealerBusinessCode" VARCHAR2(50),
   "BrandId"            NUMBER(9),
   "BranchCode"         VARCHAR2(50),
   "BrandName"          VARCHAR2(50),
   "PartsSalesCategoryId" NUMBER(9),
   "ServiceProductLineId" NUMBER(9),
   "ServiceProductLineName" VARCHAR2(50),
   "SettleType"         NUMBER(9),
   "DebitAmount"        NUMBER(19,4),
   "ComplementAmount"   NUMBER(19,4),
   "BonusPointsSum"     NUMBER(19,4),
   "IfInvoiced"         NUMBER(1),
   "TaxRate"            NUMBER(15,6),
   "InvoiceAmountDifference" NUMBER(19,4),
   "Status"             NUMBER(9),
   "SettleValidFrom"    DATE,
   "SettleValidTo"      DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "Memo"               VARCHAR2(200),
   "RowVersion"         TIMESTAMP,
   constraint PK_INTEGRALCLAIMBILLSETTLE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "IntegralClaimBillSettleDtl"                          */
/*==============================================================*/
create table "IntegralClaimBillSettleDtl"  (
   "Id"                 NUMBER(9)                       not null,
   "IntegralClaimBillSettleId" NUMBER(9)                       not null,
   "IntegralClaimBillId" NUMBER(9),
   "IntegralClaimBillCode" VARCHAR2(50),
   "UsedPrice"          NUMBER(19,4),
   constraint PK_INTEGRALCLAIMBILLSETTLEDTL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "IntegralClaimInvoiceInfo"                            */
/*==============================================================*/
create table "IntegralClaimInvoiceInfo"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "InvoiceCode"        VARCHAR2(50)                    not null,
   "InvoiceNumber"      VARCHAR2(50)                    not null,
   "ClaimSettlementBillCode" VARCHAR2(500)                   not null,
   "BranchId"           NUMBER(9)                       not null,
   "BranchCode"         VARCHAR2(50)                    not null,
   "BranchName"         VARCHAR2(100)                   not null,
   "DealerId"           NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(50)                    not null,
   "DealerName"         VARCHAR2(100)                   not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "InvoiceFee"         NUMBER(19,4)                    not null,
   "TaxRate"            NUMBER(15,6)                    not null,
   "InvoiceTaxFee"      NUMBER(19,4)                    not null,
   "Status"             NUMBER(9)                       not null,
   "InvoiceDate"        DATE                            not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "InitialApproverId"  NUMBER(9),
   "InitialApproverName" VARCHAR2(100),
   "InitialApproverTime" DATE,
   "InitialApproverComment" VARCHAR2(200),
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "ApproveComment"     VARCHAR2(200),
   "RejectId"           NUMBER(9),
   "RejectName"         VARCHAR2(100),
   "RejectTime"         DATE,
   "RejectComment"      VARCHAR2(200),
   "ApproveCommentHistory" VARCHAR2(2000),
   "RowVersion"         TIMESTAMP,
   "SettleType"         NUMBER(9),
   constraint PK_INTEGRALCLAIMINVOICEINFO primary key ("Id")
)
/

/*==============================================================*/
/* Table: "IntegralSettleDictate"                               */
/*==============================================================*/
create table "IntegralSettleDictate"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9),
   "BranchCode"         VARCHAR2(50),
   "BranchName"         VARCHAR2(200),
   "BrandId"            NUMBER(9),
   "Brand"              VARCHAR2(50),
   "ServiceProductLineId" NUMBER(9),
   "ServiceProductLineName" VARCHAR2(100),
   "Type"               NUMBER(9),
   "Status"             NUMBER(9),
   "SettlementStartTime" DATE,
   "SettlementEndTime"  DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ExecutionTime"      DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonerTime"      DATE,
   "ExecutionResult"    VARCHAR2(200),
   constraint PK_INTEGRALSETTLEDICTATE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "MaintainRedProductMap"                               */
/*==============================================================*/
create table "MaintainRedProductMap"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9),
   "PartsSalesCategoryName" VARCHAR2(100),
   "PartsSalesCategoryCode" VARCHAR2(50),
   "ServiceProductLineId" NUMBER(9),
   "ServiceProductLineCode" VARCHAR2(50),
   "ServiceProductLineName" VARCHAR2(100),
   "FixedAmount"        NUMBER(19,4),
   "Status"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   constraint PK_MAINTAINREDPRODUCTMAP primary key ("Id")
)
/

/*==============================================================*/
/* Table: "MarketABQualityInformation"                          */
/*==============================================================*/
create table "MarketABQualityInformation"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "ServiceProductLineId" NUMBER(9)                       not null,
   "RepairObjectId"     NUMBER(9),
   "Status"             NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "DealerId"           NUMBER(9)                       not null,
   "FaultDescription"   VARCHAR2(500)                   not null,
   "VehicleId"          NUMBER(9)                       not null,
   "SerialNumber"       VARCHAR2(50),
   VIN                  VARCHAR2(50),
   "FailureTimes"       NUMBER(9),
   "FailureType"        NUMBER(9)                       not null,
   "FailureSerialNumberDetail" VARCHAR2(50),
   "FailureMode"        VARCHAR2(40)                    not null,
   "Emission"           VARCHAR2(50),
   "Revision"           VARCHAR2(50),
   "Series"             VARCHAR2(50),
   "Drive"              VARCHAR2(50),
   "ProductCategoryCode" VARCHAR2(50),
   "SalesDate"          DATE,
   "OutOfFactoryDate"   DATE,
   "FaultDate"          DATE,
   "Mileage"            NUMBER(9)                       not null,
   "RoadConditions"     VARCHAR2(100),
   "CargoTypes"         VARCHAR2(100),
   "FaultyPartsId"      NUMBER(9),
   "FaultyPartsCode"    VARCHAR2(50),
   "FaultyPartsName"    VARCHAR2(100),
   "FaultyPartsSupplierId" NUMBER(9)                       not null,
   "FaultyPartsSupplierCode" VARCHAR2(50),
   "FaultyPartsSupplierName" VARCHAR2(100),
   "FaultReason"        VARCHAR2(500)                   not null,
   "HaveMeasures"       VARCHAR2(500),
   "FaultyDescription"  VARCHAR2(1000),
   "FaultyLocation"     VARCHAR2(200),
   "FaultyQuantity"     NUMBER(9),
   "FaultyNature"       VARCHAR2(50),
   "MessageLevel"       VARCHAR2(50),
   "StationMasterPhone" VARCHAR2(50),
   "ManagePhone"        VARCHAR2(50),
   "CarryWeight"        NUMBER(15,6),
   "FaultyMileage"      NUMBER(15,6),
   "BatchNumber"        VARCHAR2(50),
   "InformationPhone"   VARCHAR2(100)                   not null,
   "InitialApprovertComment" VARCHAR2(500),
   "ApprovertComment"   VARCHAR2(500),
   "ApproveCommentHistory" VARCHAR2(2000),
   "Remark"             VARCHAR2(200),
   "Path"               VARCHAR2(2000),
   "EngModel"           VARCHAR2(100),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "InitialApproverId"  NUMBER(9),
   "InitialApproverName" VARCHAR2(100),
   "InitialApproverTime" DATE,
   "RejectId"           NUMBER(9),
   "RejectName"         VARCHAR2(100),
   "RejectTime"         DATE,
   "RejectReason"       VARCHAR2(200),
   constraint PK_MARKETABQUALITYINFORMATION primary key ("Id")
)
/

/*==============================================================*/
/* Table: "OutofWarrantyPayment"                                */
/*==============================================================*/
create table "OutofWarrantyPayment"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Status"             NUMBER(9)                       not null,
   "SettlementStatus"   NUMBER(9)                       not null,
   "DebitOrReplenish"   NUMBER(9)                       not null,
   "TransactionCategory" NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "ProductLineType"    NUMBER(9),
   "ServiceProductLineId" NUMBER(9),
   "DealerId"           NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(20)                    not null,
   "DealerName"         VARCHAR2(100)                   not null,
   "SourceType"         NUMBER(9),
   "SourceId"           NUMBER(9),
   "SourceCode"         VARCHAR2(50),
   "RepairClaimBillCode" VARCHAR2(50),
   "DealerContactPerson" VARCHAR2(50),
   "DealerPhoneNumber"  VARCHAR2(50),
   "TransactionAmount"  NUMBER(19,4),
   "TransactionReason"  VARCHAR2(200),
   "IfClaimToResponsible" NUMBER(1)                       not null,
   "GradeCoefficientId" NUMBER(9),
   "ResponsibleUnitId"  NUMBER(9),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_OUTOFWARRANTYPAYMENT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PQIReport"                                           */
/*==============================================================*/
create table "PQIReport"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "DealerId"           NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(50)                    not null,
   "DealerName"         VARCHAR2(100)                   not null,
   "Applicant"          VARCHAR2(100)                   not null,
   "ContactPhone"       VARCHAR2(50)                    not null,
   "VehicleId"          NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   "Mileage"            NUMBER(9)                       not null,
   "EngineSerialNumber" VARCHAR2(50)                    not null,
   "GearSerialNumber"   VARCHAR2(50)                    not null,
   "Theme"              VARCHAR2(200)                   not null,
   "SameFaultVIN"       VARCHAR2(50),
   "FaultyPartsId"      NUMBER(9)                       not null,
   "FaultyPartsCode"    VARCHAR2(50)                    not null,
   "FaultyPartsName"    VARCHAR2(100)                   not null,
   "RepairResult"       NUMBER(9)                       not null,
   "CustomerSatisfactionDetail" NUMBER(9)                       not null,
   "RepairDate"         DATE                            not null,
   "CustomerComplain"   VARCHAR2(500),
   "MalfunctionDescription" VARCHAR2(500)                   not null,
   "FaultAnalysis"      VARCHAR2(500)                   not null,
   "ProcessMethod"      VARCHAR2(500),
   "DealerComment"      VARCHAR2(500),
   "IfRecoverParts"     NUMBER(1),
   "RecoverParts"       VARCHAR2(500),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_PQIREPORT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartRequisitionReturnBill"                           */
/*==============================================================*/
create table "PartRequisitionReturnBill"  (
   "Id"                 NUMBER(9)                       not null,
   "PartRequisitionReturnCode" VARCHAR2(50)                    not null,
   "RequisitionReturnStatus" NUMBER(9)                       not null,
   "RepairCode"         VARCHAR2(50)                    not null,
   "RepairMaterialId"   NUMBER(9),
   "DealerId"           NUMBER(9)                       not null,
   "SubDealerId"        NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "UsedPartsId"        NUMBER(9),
   "UsedPartsCode"      VARCHAR2(50),
   "UsedPartsName"      VARCHAR2(100),
   "PartsWarrantyCategoryId" NUMBER(9),
   "UsedPartsSerialNumber" VARCHAR2(50),
   "UsedPartsBatchNumber" VARCHAR2(50),
   "UsedPartsBarCode"   VARCHAR2(50),
   "UsedPartsSupplierId" NUMBER(9),
   "UsedPartsSupplierCode" VARCHAR2(50),
   "UsedPartsSupplierName" VARCHAR2(100),
   "NewPartsId"         NUMBER(9)                       not null,
   "NewPartsCode"       VARCHAR2(50)                    not null,
   "NewPartsName"       VARCHAR2(100)                   not null,
   "NewPartsSerialNumber" VARCHAR2(50),
   "NewPartsSupplierId" NUMBER(9)                       not null,
   "NewPartsSupplierCode" VARCHAR2(50)                    not null,
   "NewPartsSupplierName" VARCHAR2(100)                   not null,
   "NewPartsBatchNumber" VARCHAR2(50),
   "NewPartsSecurityNumber" VARCHAR2(50),
   "Quantity"           NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   constraint PK_PARTREQUISITIONRETURNBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsClaimApplication"                               */
/*==============================================================*/
create table "PartsClaimApplication"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Status"             NUMBER(9)                       not null,
   "SalesStatus"        NUMBER(9)                       not null,
   "Motive"             VARCHAR2(200)                   not null,
   "SalesRegionId"      NUMBER(9),
   "MarketDepartmentId" NUMBER(9)                       not null,
   "RepairRequestOrderCode" VARCHAR2(50),
   "WarrantyStartTime"  DATE                            not null,
   "RelatedBusinessBillCode" VARCHAR2(50),
   "PartsSalesOrderId"  NUMBER(9),
   "PartsRetailOrderId" NUMBER(9),
   "PartsSalesOrderCode" VARCHAR2(50),
   "PartsRetailOrderCode" VARCHAR2(50),
   "DealerId"           NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(50)                    not null,
   "DealerName"         VARCHAR2(100)                   not null,
   "FirstClassStationId" NUMBER(9),
   "FirstClassStationCode" VARCHAR2(50),
   "FirstClassStationName" VARCHAR2(100),
   "VehicleId"          NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "RepairObjectId"     NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   "VehicleLicensePlate" VARCHAR2(50),
   "Mileage"            NUMBER(9),
   "SalesDate"          DATE,
   "OutOfFactoryDate"   DATE,
   "BranchId"           NUMBER(9)                       not null,
   "ServiceProductLineId" NUMBER(9)                       not null,
   "ProductLineType"    NUMBER(9),
   "VehicleContactPerson" VARCHAR2(100),
   "ContactPhone"       VARCHAR2(50),
   "ContactAddress"     VARCHAR2(200),
   "RepairRequestTime"  DATE,
   "MalfunctionLocation" VARCHAR2(50),
   "MalfunctionId"      NUMBER(9),
   "MalfunctionCode"    VARCHAR2(20),
   "MalfunctionDescription" VARCHAR2(200),
   "MalfunctionReason"  VARCHAR2(200),
   "FaultyPartsId"      NUMBER(9),
   "FaultyPartsCode"    VARCHAR2(50),
   "FaultyPartsName"    VARCHAR2(500),
   "FaultyPartsSupplierId" NUMBER(9),
   "FaultyPartsSupplierCode" VARCHAR2(50),
   "FaultyPartsSupplierName" VARCHAR2(100),
   "EstimatedFinishingTime" DATE,
   "EstimatedLaborCost" NUMBER(19,4),
   "EstimatedMaterialCost" NUMBER(19,4),
   "OtherCost"          NUMBER(19,4),
   "OtherCostReason"    VARCHAR2(200),
   "TotalAmount"        NUMBER(19,4),
   "InitialApproveComment" VARCHAR2(200),
   "FinalApproveComment" VARCHAR2(200),
   "ApproveCommentHistory" VARCHAR2(1000),
   "ClaimStatus"        NUMBER(1)                       not null,
   "Remark"             VARCHAR2(200),
   "WorkingHours"       NUMBER(9),
   "Capacity"           NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "InitialApproverId"  NUMBER(9),
   "InitialApproverName" VARCHAR2(100),
   "InitialApproveTime" DATE,
   "FinalApproverId"    NUMBER(9),
   "FinalApproverName"  VARCHAR2(100),
   "FinalApproveTime"   DATE,
   "CheckerId"          NUMBER(9),
   "CheckerName"        VARCHAR2(100),
   "CheckTime"          DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "SupApprover"        VARCHAR2(100),
   "SupApproverId"      NUMBER(9),
   "SupApproverTime"    DATE,
   "RejectId"           NUMBER(9),
   "RejectName"         VARCHAR2(100),
   "RejectTime"         DATE,
   "RejectReason"       VARCHAR2(200),
   "Path"               VARCHAR2(2000),
   "RowVersion"         TIMESTAMP,
   constraint PK_PARTSCLAIMAPPLICATION primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsClaimApplicationDetail"                         */
/*==============================================================*/
create table "PartsClaimApplicationDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsClaimApplicationId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "Quantity"           NUMBER(9)                       not null,
   "Price"              NUMBER(19,4)                    not null,
   "PartsSupplierId"    NUMBER(9),
   "Remark"             VARCHAR2(200),
   constraint PK_PARTSCLAIMAPPLICATIONDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsClaimMaterialDetail"                            */
/*==============================================================*/
create table "PartsClaimMaterialDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsClaimRepairItemDetailId" NUMBER(9)                       not null,
   "UsedPartsId"        NUMBER(9)                       not null,
   "UsedPartsCode"      VARCHAR2(50)                    not null,
   "UsedPartsName"      VARCHAR2(100)                   not null,
   "PartsWarrantyCategoryId" NUMBER(9),
   "PartsSalesOrderId"  NUMBER(9),
   "PartsSalesOrderCode" VARCHAR2(50),
   "UsedPartsSerialNumber" VARCHAR2(50),
   "UsedPartsBatchNumber" VARCHAR2(50),
   "UsedPartsBarCode"   VARCHAR2(50)                    not null,
   "UsedPartsSupplierId" NUMBER(9)                       not null,
   "UsedPartsSupplierCode" VARCHAR2(50)                    not null,
   "UsedPartsSupplierName" VARCHAR2(100)                   not null,
   "Quantity"           NUMBER(9)                       not null,
   "UnitPrice"          NUMBER(19,4)                    not null,
   "attachment1"        VARCHAR2(200),
   "attachment2"        VARCHAR2(200),
   "MaterialCost"       NUMBER(19,4)                    not null,
   "PartsManagementCost" NUMBER(19,4)                    not null,
   "UsedPartsDisposalStatus" NUMBER(9)                       not null,
   "UsedPartsReturnPolicy" NUMBER(9)                       not null,
   "NewPartsId"         NUMBER(9)                       not null,
   "NewPartsCode"       VARCHAR2(50)                    not null,
   "NewPartsName"       VARCHAR2(100)                   not null,
   "NewPartsBatchNumber" VARCHAR2(50),
   "NewPartsSerialNumber" VARCHAR2(50),
   "NewPartsSupplierId" NUMBER(9)                       not null,
   "NewPartsSupplierCode" VARCHAR2(50)                    not null,
   "NewPartsSupplierName" VARCHAR2(100)                   not null,
   "NewPartsSecurityNumber" VARCHAR2(50),
   "Remark"             VARCHAR2(200),
   constraint PK_PARTSCLAIMMATERIALDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsClaimOrder"                                     */
/*==============================================================*/
create table "PartsClaimOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "RejectQty"          NUMBER(9),
   "RejectReason"       VARCHAR2(200),
   "SalesRegionId"      NUMBER(9),
   "MalfunctionId"      NUMBER(9),
   "MalfunctionCode"    VARCHAR2(50),
   "MalfunctionReason"  VARCHAR2(200),
   "MalfunctionDescription" VARCHAR2(200),
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "SalesUnitId"        NUMBER(9),
   "PartsRetailOrderCode" VARCHAR2(50),
   "PartsRetailOrderId" NUMBER(9),
   "MarketingDepartmentId" NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "SettlementStatus"   NUMBER(9)                       not null,
   "DealerId"           NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(50)                    not null,
   "DealerName"         VARCHAR2(100)                   not null,
   "FirstClassStationId" NUMBER(9),
   "FirstClassStationCode" VARCHAR2(50),
   "FirstClassStationName" VARCHAR2(100),
   "BranchId"           NUMBER(9)                       not null,
   "ServiceProductLineId" NUMBER(9)                       not null,
   "ProductLineType"    NUMBER(9),
   "ClaimReqPerson"     VARCHAR2(50),
   "ContactPhone"       VARCHAR2(50),
   "ContactAddress"     VARCHAR2(200),
   "WarrantyStartTime"  DATE                            not null,
   "ClaimReqTime"       DATE                            not null,
   "LaborCost"          NUMBER(19,4)                    not null,
   "MaterialCost"       NUMBER(19,4)                    not null,
   "PartsManagementCost" NUMBER(19,4),
   "OtherCost"          NUMBER(19,4),
   "OtherCostReason"    VARCHAR2(200),
   "TotalAmount"        NUMBER(19,4),
   "UsedPartsDisposalStatus" NUMBER(9)                       not null,
   "InitialApprovertComment" VARCHAR2(100),
   "FinalApproverComment" VARCHAR2(100),
   "ApproveCommentHistory" VARCHAR2(2000),
   "ServiceDepartmentComment" VARCHAR2(200),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "InitialApproverId"  NUMBER(9),
   "InitialApproverName" VARCHAR2(100),
   "InitialApproverTime" DATE,
   "FinalApproverId"    NUMBER(9),
   "FinalApproverName"  VARCHAR2(100),
   "FinalApproverTime"  DATE,
   "RejectId"           NUMBER(9),
   "RejectName"         VARCHAR2(100),
   "RejectTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_PARTSCLAIMORDER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsClaimOrderNew"                                  */
/*==============================================================*/
create table "PartsClaimOrderNew"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Branchid"           NUMBER(9)                       not null,
   "ReturnCompanyId"    NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsRetailOrderId" NUMBER(9),
   "PartsRetailOrderCode" VARCHAR2(50),
   "PartsSalesOrderId"  NUMBER(9),
   "PartsSalesOrderCode" VARCHAR2(50),
   "RepairOrderId"      NUMBER(9),
   "RepairOrderCode"    VARCHAR2(50),
   "ReturnWarehouseId"  NUMBER(9)                       not null,
   "ReturnWarehouseCode" VARCHAR2(50),
   "ReturnWarehouseName" VARCHAR2(100),
   "RWarehouseCompanyCode" VARCHAR2(50),
   "RWarehouseCompanyType" NUMBER(9),
   "RWarehouseCompanyId" NUMBER(9),
   "RDCReceiptSituation" VARCHAR2(500),
   "CDCReturnWarehouseId" NUMBER(9),
   "CDCReturnWarehouseCode" VARCHAR2(50),
   "CDCReturnWarehouseName" VARCHAR2(100),
   "CDCRWCompanyCode"   VARCHAR2(50),
   "CDCRWCompanyType"   NUMBER(9),
   "CDCRWCompanyId"     NUMBER(9),
   "SalesWarehouseCode" VARCHAR2(50),
   "SalesWarehouseId"   NUMBER(9),
   "SalesWarehouseName" VARCHAR2(100),
   "AgencyOutWarehouseId" NUMBER(9),
   "CustomerContactPerson" VARCHAR2(100),
   "CustomerContactPhone" VARCHAR2(200),
   "FaultyPartsId"      NUMBER(9)                       not null,
   "FaultyPartsCode"    VARCHAR2(50),
   "FaultyPartsName"    VARCHAR2(100),
   "FaultyPartsSupplierId" NUMBER(9),
   "FaultyPartsSupplierCode" VARCHAR2(50),
   "FaultyPartsSupplierName" VARCHAR2(100),
   "Quantity"           NUMBER(9),
   "PartsWarrantyLong"  NUMBER(9),
   "RepairRequestTime"  DATE,
   "PartsSaleTime"      DATE,
   "PartsSaleLong"      NUMBER(9),
   "IsOutWarranty"      NUMBER(1),
   "MaterialCost"       NUMBER(19,4),
   "MaterialManagementCost" NUMBER(19,4),
   "LaborCost"          NUMBER(19,4),
   "OtherCost"          NUMBER(19,4),
   "RejectStatus"       NUMBER(9),
   "RejectQty"          NUMBER(9),
   "RejectReason"       VARCHAR2(200),
   "TotalAmount"        NUMBER(19,4),
   "MalfunctionDescription" VARCHAR2(600),
   "Remark"             VARCHAR2(600),
   "Path"               VARCHAR2(2000),
   "ApproveCommentHistory" VARCHAR2(2000),
   "ApproveComment"     VARCHAR2(200),
   "OverWarrantyDate"   NUMBER(9),
   "Status"             NUMBER(9)                       not null,
   "SettlementStatus"   NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "SubmitterId"        NUMBER(9),
   "SubmitterName"      NUMBER(9),
   "SubmitTime"         NUMBER(9),
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "CancelId"           NUMBER(9),
   "CancelName"         VARCHAR2(100),
   "CancelTime"         DATE,
   "RejectId"           NUMBER(9),
   "RejectName"         VARCHAR2(100),
   "RejectTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "ReturnReason"       VARCHAR2(200),
   "AbandonerReason"    VARCHAR2(200),
   "StopReason"         VARCHAR2(200),
   "ShippingCode"       VARCHAR2(50),
   "CenterPartsSalesOrderId" NUMBER(9),
   "CenterPartsSalesOrderCode" VARCHAR2(50),
   "InitialApproveId"   NUMBER(9),
   "InitialApproveDt"   DATE,
   "InitialAppName"     VARCHAR2(100),
   "AuditId"            NUMBER(9),
   "AuditName"          VARCHAR2(100),
   "AuditTime"          DATE,
   "ExamineId"          NUMBER(9),
   "ExamineName"        VARCHAR2(100),
   "ExamineTime"        DATE,
   constraint PK_PARTSCLAIMORDERNEW primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsClaimRepairItemDetail"                          */
/*==============================================================*/
create table "PartsClaimRepairItemDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsClaimBillId"   NUMBER(9)                       not null,
   "RepairItemId"       NUMBER(9)                       not null,
   "RepairItemCode"     VARCHAR2(50)                    not null,
   "RepairItemName"     VARCHAR2(100)                   not null,
   "DefaultLaborHour"   NUMBER(15,6)                    not null,
   "LaborUnitPrice"     NUMBER(19,4)                    not null,
   "LaborCost"          NUMBER(19,4)                    not null,
   "ApproveStatus"      NUMBER(9)                       not null,
   "IfSelfDefine"       NUMBER(1),
   "IfObliged"          NUMBER(1),
   "Remark"             VARCHAR2(200),
   constraint PK_PARTSCLAIMREPAIRITEMDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PostSaleClaims_Tmp"                                  */
/*==============================================================*/
create table "PostSaleClaims_Tmp"  (
   "Id"                 NUMBER(9)                       not null,
   "ClaimBill_Code"     VARCHAR2(30),
   "Dealer_Code"        VARCHAR2(30),
   "CreateTime"         DATE,
   "IsClaim"            NUMBER(9),
   "SparePart_Code"     VARCHAR2(18),
   "SparePart_Name"     VARCHAR2(100),
   "Amount"             NUMBER,
   "Status"             NUMBER(9),
   "Type"               NUMBER(9),
   "TransferTime"       DATE,
   "SalesPrice"         NUMBER(19,4),
   "RetailGuidePrice"   NUMBER(19,4),
   "GroupCode"          VARCHAR2(50),
   constraint PK_POSTSALECLAIMS_TMP primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PreSaleCheckOrder"                                   */
/*==============================================================*/
create table "PreSaleCheckOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairWorkOrderId"  NUMBER(9),
   "RepairWorkOrderCode" VARCHAR2(50),
   "BranchId"           NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "ServiceProductLineId" NUMBER(9)                       not null,
   "ProductLineType"    NUMBER(9),
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "SettleStatus"       NUMBER(9),
   "InternalCode"       VARCHAR2(50),
   "Revision"           VARCHAR2(100),
   "Series"             VARCHAR2(100),
   "ProductCategory"    VARCHAR2(50),
   "Drive"              VARCHAR2(100),
   "DriverTel"          VARCHAR2(50),
   "DriverName"         VARCHAR2(50),
   "ResponsibleUnitId"  NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   "SerialNumber"       VARCHAR2(50)                    not null,
   "VehicleId"          NUMBER(9)                       not null,
   "VehicleLicensePlate" VARCHAR2(50),
   "DealerId"           NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(50)                    not null,
   "DealerName"         VARCHAR2(100)                   not null,
   "Cost"               NUMBER(9),
   "Remark"             VARCHAR2(200),
   "IsSaleService"      NUMBER(1),
   "SaleDealerName"     VARCHAR2(100),
   "ApprovertComment"   VARCHAR2(200),
   "RejectComment"      VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "submittedId"        NUMBER(9),
   "submittedName"      VARCHAR2(100),
   "SubmitTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "RejectId"           NUMBER(9),
   "RejectName"         VARCHAR2(100),
   "RejectTime"         DATE,
   "Status"             NUMBER(9),
   "Path"               VARCHAR2(2000),
   constraint PK_PRESALECHECKORDER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PreSaleItem"                                         */
/*==============================================================*/
create table "PreSaleItem"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "ServiceProductLineId" NUMBER(9)                       not null,
   "CheckItem"          VARCHAR2(200)                   not null,
   "Category"           VARCHAR2(200)                   not null,
   "FaultPattern"       VARCHAR2(200)                   not null,
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   constraint PK_PRESALEITEM primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PreSalesCheckDetail"                                 */
/*==============================================================*/
create table "PreSalesCheckDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "PreSalesCheckId"    NUMBER(9)                       not null,
   "ItemId"             NUMBER(9)                       not null,
   "Category"           VARCHAR2(200),
   "CheckItem"          VARCHAR2(200),
   "FaultPattern"       VARCHAR2(200),
   "FaultDescription"   VARCHAR2(200),
   "RepairMethod"       NUMBER(9),
   "ClaimUnit"          NUMBER(9),
   constraint PK_PRESALESCHECKDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RedPacketsMsg"                                       */
/*==============================================================*/
create table "RedPacketsMsg"  (
   "Id"                 NUMBER(9)                       not null,
   "Tel"                VARCHAR2(20),
   "BrandId"            NUMBER(9),
   "BrandName"          VARCHAR2(50),
   "RepairCode"         VARCHAR2(50),
   "RepairOrderId"      NUMBER(9),
   "CouponNo"           VARCHAR2(100),
   "Title"              VARCHAR2(255),
   "State"              NUMBER(9),
   "StartTime"          DATE,
   "EndTime"            DATE,
   "UsedPrice"          NUMBER(19,4),
   "LimitPrice"         VARCHAR2(100),
   "UsedTime"           DATE,
   "DateTime"           DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   "RedPacketsType"     NUMBER(9),
   "RedPacketsRange"    NUMBER(9),
   "LssuingUnit"        NUMBER(9),
   VIN                  VARCHAR2(50),
   constraint PK_REDPACKETSMSG primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairClaimAppDetail"                                */
/*==============================================================*/
create table "RepairClaimAppDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairClaimApplicationId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "Quantity"           NUMBER(9)                       not null,
   "Price"              NUMBER(19,4)                    not null,
   "PartsSupplierId"    NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   constraint PK_REPAIRCLAIMAPPDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairClaimApplication"                              */
/*==============================================================*/
create table "RepairClaimApplication"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Motive"             VARCHAR2(200),
   "Applicant"          VARCHAR2(50),
   "ApplicationTel"     VARCHAR2(50),
   "Status"             NUMBER(9)                       not null,
   "SalesStatus"        NUMBER(9)                       not null,
   "SalesRegionId"      NUMBER(9),
   "MarketDepartmentId" NUMBER(9)                       not null,
   "ClaimType"          NUMBER(9)                       not null,
   "ApplicationType"    NUMBER(9),
   "RepairType"         NUMBER(9),
   "DealerId"           NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(50)                    not null,
   "DealerName"         VARCHAR2(100)                   not null,
   "FirstClassStationId" NUMBER(9),
   "FirstClassStationCode" VARCHAR2(50),
   "FirstClassStationName" VARCHAR2(100),
   "RepairRequestOrderCode" VARCHAR2(50),
   "VehicleId"          NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   "VehicleLicensePlate" VARCHAR2(50),
   "Mileage"            NUMBER(9),
   "WorkingHours"       NUMBER(9),
   "Capacity"           NUMBER(9),
   "ProductType"        NUMBER(9),
   "BridgeType"         NUMBER(9),
   "SalesDate"          DATE,
   "OutOfFactoryDate"   DATE,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "RepairObjectId"     NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "ServiceProductLineId" NUMBER(9)                       not null,
   "ProductLineType"    NUMBER(9),
   "VehicleContactPerson" VARCHAR2(100),
   "ContactPhone"       VARCHAR2(50),
   "ContactAddress"     VARCHAR2(200),
   "RepairRequestTime"  DATE,
   "CustomOpinion"      VARCHAR2(200),
   "VehicleUse"         VARCHAR2(200),
   "CarActuality"       VARCHAR2(200),
   "MalfunctionLocation" VARCHAR2(50),
   "MalfunctionId"      NUMBER(9),
   "MalfunctionCode"    VARCHAR2(50),
   "MalfunctionDescription" VARCHAR2(200),
   "MalfunctionReason"  VARCHAR2(200),
   "FaultyPartsId"      NUMBER(9),
   "FaultyPartsCode"    VARCHAR2(50),
   "FaultyPartsName"    VARCHAR2(100),
   "TempSupplierName"   VARCHAR2(100),
   "FaultyPartsSupplierId" NUMBER(9),
   "FaultyPartsSupplierCode" VARCHAR2(50),
   "FaultyPartsSupplierName" VARCHAR2(100),
   "EstimatedFinishingTime" DATE,
   "EstimatedLaborCost" NUMBER(19,4),
   "EstimatedMaterialCost" NUMBER(19,4),
   "OtherCost"          NUMBER(19,4),
   "OtherCostReason"    VARCHAR2(200),
   "TotalAmount"        NUMBER(19,4),
   "DealComment"        VARCHAR2(200),
   "BonusExtensionDescription" VARCHAR2(200),
   "ChangeAssemblyReason" NUMBER(9),
   "VehicleWarrantyCardId" NUMBER(9),
   "VehicleWarrantyCardCode" VARCHAR2(50),
   "VehicleMaintenancePoilcyId" NUMBER(9),
   "VehicleMaintenancePoilcyName" VARCHAR2(200),
   "ServiceActivityId"  NUMBER(9),
   "ServiceActivityCode" VARCHAR2(50),
   "ServiceActivityContent" VARCHAR2(200),
   "ServiceActivityType" NUMBER(9),
   "InitialApproveComment" VARCHAR2(200),
   "FinalApproveComment" VARCHAR2(200),
   "CheckComment"       VARCHAR2(200),
   "ApproveComment"     VARCHAR2(200),
   "SupplyInitialApproveComment" VARCHAR2(200),
   "ApproveCommentHistory" VARCHAR2(2200),
   "ClaimStatus"        NUMBER(1)                       not null,
   "Remark"             VARCHAR2(500),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "InitialApproverId"  NUMBER(9),
   "InitialApproverName" VARCHAR2(100),
   "InitialApproveTime" DATE,
   "FinalApproverId"    NUMBER(9),
   "FinalApproverName"  VARCHAR2(100),
   "FinalApproveTime"   DATE,
   "CheckerId"          NUMBER(9),
   "CheckerName"        VARCHAR2(100),
   "CheckTime"          DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "SupApprover"        VARCHAR2(100),
   "SupApproverId"      NUMBER(9),
   "SupApproverTime"    DATE,
   "RowVersion"         TIMESTAMP,
   "RejectId"           NUMBER(9),
   "RejectName"         VARCHAR2(100),
   "RejectTime"         DATE,
   "RejectReason"       VARCHAR2(200),
   "Path"               VARCHAR2(2000),
   "SubmitterId"        NUMBER(9),
   "SubmitterName"      VARCHAR2(100),
   "SubmitTime"         DATE,
   "IsAppPicture"       NUMBER(9)                      default 0,
   constraint PK_REPAIRCLAIMAPPLICATION primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairClaimBill"                                     */
/*==============================================================*/
create table "RepairClaimBill"  (
   "Id"                 NUMBER(9)                       not null,
   "ClaimBillCode"      VARCHAR2(50)                    not null,
   "RepairContractCode" VARCHAR2(50),
   "RepairOrderId"      NUMBER(9)                       not null,
   "VideoMobileNumber"  VARCHAR2(50),
   "MVSOutRange"        NUMBER(15,6),
   "MVSOutTime"         NUMBER(15,6),
   "MVSOutDistance"     NUMBER(15,6),
   "MVSOutCoordinate"   VARCHAR2(50),
   "RepairWorkOrderId"  NUMBER(9),
   "RepairWorkOrder"    VARCHAR2(50),
   "RepairOrderFaultReasonId" NUMBER(9),
   "QualityInformationId" NUMBER(9),
   "QualityInformationCode" VARCHAR2(50),
   "SalesStatus"        NUMBER(9)                       not null,
   "ClaimType"          NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "RepairType"         NUMBER(9),
   "SettlementStatus"   NUMBER(9)                       not null,
   "SupplierSettleStatus" NUMBER(9),
   "Amendments"         VARCHAR2(200),
   "RejectStatus"       NUMBER(9),
   "RejectQty"          NUMBER(9),
   "RejectReason"       VARCHAR2(200),
   "RepairObjectId"     NUMBER(9)                       not null,
   "SalesRegionId"      NUMBER(9),
   "MarketingDepartmentId" NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "ServiceTripClaimAppId" NUMBER(9),
   "ServiceTripClaimAppCode" VARCHAR2(50),
   "RepairClaimApplicationId" NUMBER(9),
   "RepairClaimApplicationCode" VARCHAR2(50),
   "ApplicationType"    NUMBER(9),
   "ServiceActivityType" NUMBER(9),
   "ServiceActivityId"  NUMBER(9),
   "ServiceActivityCode" VARCHAR2(50),
   "ServiceActivityContent" VARCHAR2(200),
   "ServiceActivityTime" DATE,
   "ServiceActivityAppId" NUMBER(9),
   "ServiceActivityAppCode" VARCHAR2(50),
   "VehicleMaintenancePoilcyId" NUMBER(9),
   "VehicleMaintenancePoilcyName" VARCHAR2(200),
   "DealerId"           NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(50)                    not null,
   "DealerName"         VARCHAR2(100)                   not null,
   "FirstClassStationId" NUMBER(9),
   "FirstClassStationCode" VARCHAR2(50),
   "FirstClassStationName" VARCHAR2(100),
   "BranchId"           NUMBER(9)                       not null,
   "VehicleContactPerson" VARCHAR2(100),
   "ContactPhone"       VARCHAR2(50),
   "ContactAddress"     VARCHAR2(200),
   "VehicleId"          NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   "SalesDate"          DATE,
   "OutOfFactoryDate"   DATE,
   "ServiceProductLineId" NUMBER(9)                       not null,
   "ProductLineType"    NUMBER(9),
   "VehicleLicensePlate" VARCHAR2(50),
   "Mileage"            NUMBER(9),
   "WorkingHours"       NUMBER(9),
   "Capacity"           NUMBER(9),
   "RepairRequestTime"  DATE,
   "FinishingTime"      DATE,
   "LaborCost"          NUMBER(19,4)                    not null,
   "MaterialCost"       NUMBER(19,4)                    not null,
   "PartsManagementCost" NUMBER(19,4),
   "OtherCost"          NUMBER(19,4),
   "OtherCostReason"    VARCHAR2(200),
   "TotalAmount"        NUMBER(19,4),
   "MalfunctionReason"  VARCHAR2(600),
   "EngineModel"        VARCHAR2(50),
   "EngineModelId"      NUMBER(9),
   "EngineSerialNumber" VARCHAR2(50),
   "GearModel"          VARCHAR2(50),
   "GearSerialNumber"   VARCHAR2(50),
   "MalfunctionId"      NUMBER(9),
   "MalfunctionCode"    VARCHAR2(50),
   "MalfunctionDescription" VARCHAR2(600),
   "RepairTempletId"    NUMBER(9),
   "RepairTempletCode"  VARCHAR2(50),
   "FaultyPartsId"      NUMBER(9),
   "FaultyPartsCode"    VARCHAR2(50),
   "FaultyPartsName"    VARCHAR2(100),
   "FaultyPartsSupplierId" NUMBER(9),
   "FaultyPartsWCId"    NUMBER(9),
   "FaultyPartsSupplierCode" VARCHAR2(50),
   "FaultyPartsSupplierName" VARCHAR2(100),
   "ClaimSupplierId"    NUMBER(9),
   "ClaimSupplierCode"  VARCHAR2(50),
   "ClaimSupplierName"  VARCHAR2(100),
   "IfClaimToSupplier"  NUMBER(1)                       not null,
   "ResponsibleUnitId"  NUMBER(9)                       not null,
   "SupplierConfirmorId" NUMBER(9),
   "SupplierConfirmorName" VARCHAR2(100),
   "SupplierConfirmTime" DATE,
   "SupplierConfirmStatus" NUMBER(9),
   "SupplierConfirmResult" NUMBER(9),
   "SupplierConfirmNotP" NUMBER(9),
   "SupplierConfirmComment" VARCHAR2(200),
   "SupplierCheckerId"  NUMBER(9),
   "SupplierCheckerName" VARCHAR2(100),
   "SupplierCheckTime"  DATE,
   "SupplierCheckStatus" NUMBER(9),
   "SupplierCheckResult" NUMBER(9),
   "SupplierCheckNotP"  NUMBER(9),
   "SupplierIdeaNotPassReson" VARCHAR2(500),
   "SupplierCheckComment" VARCHAR2(200),
   "ServiceDepartmentComment" VARCHAR2(200),
   "InitialApprovertComment" VARCHAR2(200),
   "FinalApproverComment" VARCHAR2(200),
   "AutoApproveComment" VARCHAR2(4000),
   "AutoApproveStatus"  NUMBER(9),
   "ApproveCommentHistory" VARCHAR2(4000),
   "UsedPartsDisposalStatus" NUMBER(9)                       not null,
   "Remark"             VARCHAR2(600),
   "TempSupplierId"     NUMBER(9),
   "TempSupplierCode"   VARCHAR2(50),
   "TempSupplierName"   VARCHAR2(100),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "CancelReason"       VARCHAR2(200),
   "InitialApproverCompanyId" NUMBER(9),
   "InitialApproverId"  NUMBER(9),
   "InitialApproverName" VARCHAR2(100),
   "InitialApproverTime" DATE,
   "FinalApproverId"    NUMBER(9),
   "FinalApproverName"  VARCHAR2(100),
   "FinalApproverTime"  DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "RejectId"           NUMBER(9),
   "RejectName"         VARCHAR2(100),
   "RejectTime"         DATE,
   "RowVersion"         TIMESTAMP,
   "CauseOtherMalfunction" NUMBER(1),
   "TrimCost"           NUMBER(19,4),
   "FaultyPartsAssemblyId" NUMBER(9),
   "FaultyPartsAssemblyCode" VARCHAR2(50),
   "FaultyPartsAssemblyName" VARCHAR2(100),
   "Path"               VARCHAR2(2000),
   "RetainedCustomer"   VARCHAR2(100),
   "RetainedCustomerPhone" VARCHAR2(100),
   "RetainedCustomerId" NUMBER(9),
   "DebitAmount"        NUMBER(19,4),
   "ComplementAmount"   NUMBER(19,4),
   "IsSameToLoadingDetail" NUMBER(9),
   "ProductSeries"      VARCHAR2(100),
   "Grade"              VARCHAR2(50),
   "VehicleLocationCheck" NUMBER(9),
   constraint PK_REPAIRCLAIMBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairClaimItemDetail"                               */
/*==============================================================*/
create table "RepairClaimItemDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairClaimBillId"  NUMBER(9)                       not null,
   "RepairOrderMaterialDetailId" NUMBER(9),
   "RepairItemId"       NUMBER(9)                       not null,
   "RepairItemCode"     VARCHAR2(50)                    not null,
   "RepairItemName"     VARCHAR2(600)                   not null,
   "DefaultLaborHour"   NUMBER(15,6)                    not null,
   "LaborUnitPrice"     NUMBER(19,4)                    not null,
   "LaborCost"          NUMBER(19,4)                    not null,
   "ApproveStatus"      NUMBER(9)                       not null,
   "IfSelfDefine"       NUMBER(1),
   "IfObliged"          NUMBER(1),
   "Remark"             VARCHAR2(600),
   "RepairWorkerId"     NUMBER(9),
   "RepairWorkerName"   VARCHAR2(100),
   "ProductCategoryID"  NUMBER(9),
   constraint PK_REPAIRCLAIMITEMDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairClaimMaterialDetail"                           */
/*==============================================================*/
create table "RepairClaimMaterialDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairClaimItemDetailId" NUMBER(9)                       not null,
   "RepairOrderMaterialDetailId" NUMBER(9),
   "UsedPartsId"        NUMBER(9)                       not null,
   "UsedPartsCode"      VARCHAR2(50)                    not null,
   "UsedPartsName"      VARCHAR2(100)                   not null,
   "PartsWarrantyCategoryId" NUMBER(9),
   "UsedPartsSerialNumber" VARCHAR2(50),
   "UsedPartsBatchNumber" VARCHAR2(50),
   "UsedPartsBarCode"   VARCHAR2(50)                    not null,
   "UsedPartsSupplierId" NUMBER(9)                       not null,
   "UsedPartsSupplierCode" VARCHAR2(50)                    not null,
   "UsedPartsSupplierName" VARCHAR2(100)                   not null,
   "Quantity"           NUMBER(9)                       not null,
   "OldUnitPrice"       NUMBER(19,4)                    not null,
   "UnitPrice"          NUMBER(19,4)                    not null,
   "MaterialCost"       NUMBER(19,4)                    not null,
   "PartsManagementCost" NUMBER(19,4)                    not null,
   "UsedPartsDisposalStatus" NUMBER(9)                       not null,
   "UsedPartsReturnPolicy" NUMBER(9)                       not null,
   "NewPartsId"         NUMBER(9)                       not null,
   "NewPartsCode"       VARCHAR2(50)                    not null,
   "NewPartsName"       VARCHAR2(100)                   not null,
   "NewPartsSerialNumber" VARCHAR2(50),
   "NewPartsSupplierId" NUMBER(9)                       not null,
   "NewPartsSupplierCode" VARCHAR2(50)                    not null,
   "NewPartsSupplierName" VARCHAR2(100)                   not null,
   "NewPartsBatchNumber" VARCHAR2(50),
   "NewPartsSecurityNumber" VARCHAR2(50),
   "Remark"             VARCHAR2(600),
   "PartSource"         NUMBER(9),
   "MaterialType"       NUMBER(9)                       not null,
   constraint PK_REPAIRCLAIMMATERIALDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairClaimSupplierDetail"                           */
/*==============================================================*/
create table "RepairClaimSupplierDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairClaimId"      NUMBER(9)                       not null,
   "SupplierId"         NUMBER(9)                       not null,
   "TotalAmount"        NUMBER(19,4),
   "LaborCost"          NUMBER(19,4),
   "MaterialCost"       NUMBER(19,4),
   "PartsManagementCost" NUMBER(19,4),
   "OtherCost"          NUMBER(19,4),
   "RepairOrderSupplierClaimId" NUMBER(9),
   "SettleStatus"       NUMBER(9),
   constraint PK_REPAIRCLAIMSUPPLIERDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairIntegral"                                      */
/*==============================================================*/
create table "RepairIntegral"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairOrderId"      NUMBER(9),
   "MemberCode"         VARCHAR2(15),
   "MemberName"         VARCHAR2(100),
   "IDType"             VARCHAR2(100),
   "IDNumer"            VARCHAR2(50),
   "MemberRank"         VARCHAR2(100),
   "MemberPhone"        VARCHAR2(30),
   "MemberValue"        NUMBER(19,4),
   "MaxMemberValue"     NUMBER(9),
   "MemberUsedIntegral" NUMBER(19,4),
   "LaborCost"          NUMBER(19,4),
   "MaterialCost"       NUMBER(19,4),
   "TrimCost"           NUMBER(19,4),
   "OtherCost"          NUMBER(19,4),
   "OutRange"           NUMBER(9),
   "FieldTotalAmount"   NUMBER(19,4),
   "TotalAmount"        NUMBER(19,4),
   "Discount"           NUMBER(19,4),
   "CostDiscount"       NUMBER(19,4),
   "DeductionCost"      NUMBER(19,4),
   "MemberRightsCost"   NUMBER(19,4),
   "DeductionTotalAmount" NUMBER(19,4),
   "TransactionTime"    DATE,
   constraint PK_REPAIRINTEGRAL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairIntegralRightsDetail"                          */
/*==============================================================*/
create table "RepairIntegralRightsDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairIntegralId"   NUMBER(9)                       not null,
   "RightsItem"         VARCHAR2(100),
   "RightsCode"         VARCHAR2(100),
   "IsRights"           NUMBER(1),
   "BenefiType"         VARCHAR2(30),
   "BenefiUom"          NUMBER(9),
   "DiscountNum"        NUMBER(19,4),
   "Time"               NUMBER(9),
   constraint PK_REPAIRINTEGRALRIGHTSDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairMemberRightsDetail"                            */
/*==============================================================*/
create table "RepairMemberRightsDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairOrderId"      NUMBER(9)                       not null,
   "RightsCode"         VARCHAR2(100),
   "RightsItem"         VARCHAR2(100),
   "IsRights"           NUMBER(1),
   "BenefiType"         VARCHAR2(30),
   "BenefiUom"          NUMBER(9),
   "DiscountNum"        NUMBER(19,4),
   "Time"               NUMBER(9),
   constraint PK_REPAIRMEMBERRIGHTSDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairOrder"                                         */
/*==============================================================*/
create table "RepairOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "RepairWorkOrderId"  NUMBER(9),
   "RepairWorkOrder"    VARCHAR2(50),
   "ServiceProductLineId" NUMBER(9)                       not null,
   "ProductLineType"    NUMBER(9),
   "RepairObjectId"     NUMBER(9)                       not null,
   "SalesRegionId"      NUMBER(9),
   "MarketDepartmentId" NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "WarrantyStatus"     NUMBER(9),
   "RepairClassificationId" NUMBER(9),
   "RejectStatus"       NUMBER(9),
   "ProductType"        NUMBER(9),
   "MVSOutRange"        NUMBER(15,6),
   "MVSOutTime"         NUMBER(15,6),
   "MVSOutDistance"     NUMBER(15,6),
   VIN                  VARCHAR2(50)                    not null,
   "RepairType"         NUMBER(9)                       not null,
   "ServiceActivityId"  NUMBER(9),
   "ServiceActivityCode" VARCHAR2(50),
   "VehicleMaintenancePoilcyId" NUMBER(9),
   "VehicleMaintenancePoilcyName" VARCHAR2(200),
   "ServiceTripClaimAppId" NUMBER(9),
   "ServiceTripClaimAppCode" VARCHAR2(50),
   "QBTime"             NUMBER(9),
   "WorkFeeStand"       NUMBER(19,4),
   "CarActuality"       VARCHAR2(600),
   "CauseAnalyse"       VARCHAR2(200),
   "DealMethod"         VARCHAR2(600),
   "CarThing"           VARCHAR2(200),
   "DealResult"         VARCHAR2(200),
   "BridgeType"         NUMBER(9),
   "RepairAddition"     NUMBER(9),
   "IsRoadVehicl"       NUMBER(1),
   "IsOutPurOrSale"     NUMBER(1),
   "CustomOpinion"      VARCHAR2(600),
   "EngineClaim"        NUMBER(9),
   "EngineWarrantyProperties" NUMBER(9),
   "EngineApplicationArea" NUMBER(9),
   "EngineDamagedCondition" NUMBER(9),
   "EngineCode"         VARCHAR2(50),
   "EngineWarrantyStartDate" DATE,
   "RelatedExplain"     VARCHAR2(200),
   "SPSettleStatus"     NUMBER(9),
   "RejectReason"       VARCHAR2(200),
   "OutClaimSupplierId" NUMBER(9),
   "OutClaimSupplierName" VARCHAR2(100),
   "ResponsibleUnitId"  NUMBER(9)                       not null,
   "OutClaimSupplierCode" VARCHAR2(50),
   "OutFaultReasonId"   NUMBER(9),
   "OutVehicleLicensePlate" VARCHAR2(50),
   "OtherTrafficFee"    NUMBER(19,4)                    not null,
   "TrafficWay"         VARCHAR2(200),
   "VehicleUse"         VARCHAR2(600),
   "OutMemo"            VARCHAR2(200),
   "ServiceTripType"    NUMBER(9),
   "OutRange"           NUMBER(9),
   "ServiceTripAreaId"  NUMBER(9),
   "WorkingHours"       NUMBER(9),
   "Capacity"           NUMBER(9),
   "RegionID"           VARCHAR2(32),
   "ProvinceName"       VARCHAR2(50),
   "CityName"           VARCHAR2(50),
   "CountyName"         VARCHAR2(50),
   "DetailedAddress"    VARCHAR2(200),
   "Range"              NUMBER(9),
   "GoTime"             DATE,
   "ReturnTime"         DATE,
   "ServiceTripPerson"  NUMBER(9),
   "ServiceTripDuration" NUMBER(9),
   "OutPersonCode"      VARCHAR2(40),
   "OutServiceCarUnitPrice" NUMBER(19,4),
   "OutSubsidyPrice"    NUMBER(19,4),
   "IfUseOwnVehicle"    NUMBER(1),
   "IfUseTowTruck"      NUMBER(1),
   "IsCalled"           NUMBER(1),
   "ContentDegree"      NUMBER(9),
   "CallOpinion"        VARCHAR2(200),
   "IsNeedSP"           NUMBER(1),
   "DutyUnitID"         NUMBER(9),
   "IsOutSP"            NUMBER(1),
   "RecentRepTime"      DATE,
   "RepInterval"        NUMBER(9),
   ISEXISTOUT           NUMBER(1),
   USETRANSPORTPATH     VARCHAR2(20),
   OUTLINKID            NUMBER(9),
   WORKFEESTANDKMS      NUMBER(19,4),
   "SalesStatus"        NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "FirstClassStationId" NUMBER(9),
   "FirstClassStationCode" VARCHAR2(50),
   "FirstClassStationName" VARCHAR2(100),
   "DealerId"           NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(50)                    not null,
   "DealerName"         VARCHAR2(100)                   not null,
   "AppointDealerId"    NUMBER(9),
   "BranchId"           NUMBER(9)                       not null,
   "VehicleContactPerson" VARCHAR2(100),
   "ContactPhone"       VARCHAR2(50),
   "ContactAddress"     VARCHAR2(200),
   "VehicleId"          NUMBER(9)                       not null,
   "RetainedCustomer"   VARCHAR2(100),
   "RetainedCustomerPhone" VARCHAR2(100),
   "RetainedCustomerId" NUMBER(9),
   "SalesDate"          DATE,
   "OutOfFactoryDate"   DATE                            not null,
   "VehicleLicensePlate" VARCHAR2(50),
   "Mileage"            NUMBER(9),
   "RepairRequestTime"  DATE                            not null,
   "FinishingTime"      DATE,
   "LaborCost"          NUMBER(19,4)                    not null,
   "MaterialCost"       NUMBER(19,4)                    not null,
   "TrimCost"           NUMBER(19,4)                    not null,
   "PartsManagementCost" NUMBER(19,4)                    not null,
   "TowCharge"          NUMBER(19,4)                    not null,
   "TripServiceCharge"  NUMBER(19,4)                    not null,
   "FieldServiceCharge" NUMBER(19,4)                    not null,
   "OtherCost"          NUMBER(19,4)                    not null,
   "OtherCostReason"    VARCHAR2(200),
   "TotalAmount"        NUMBER(19,4)                    not null,
   "Remark"             VARCHAR2(600),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "RepairFinishTime"   DATE,
   "RepairFinishName"   VARCHAR2(100),
   "ScamFinishTime"     DATE,
   "ScamFinishName"     VARCHAR2(100),
   "CallCenterFinishTime" DATE,
   "CallCenterName"     VARCHAR2(100),
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "Path"               VARCHAR2(2000),
   "SysCode"            VARCHAR2(50),
   "RowVersion"         TIMESTAMP,
   "GradeCoefficientId" NUMBER(9),
   "FaultyPartsAssemblyId" NUMBER(9),
   "FaultyPartsAssemblyCode" VARCHAR2(50),
   "FaultyPartsAssemblyName" VARCHAR2(100),
   "PartsPriceUpperLimit" NUMBER(19,4),
   "PartsPriceLowerLimit" NUMBER(19,4),
   "UpperLimitAmount"   NUMBER(19,4),
   "LowerLimitAmount"   NUMBER(19,4),
   "PartsManagementCostRate" NUMBER(15,6),
   "ProductSeries"      VARCHAR2(100),
   "VehicleLinkmanId"   NUMBER(9),
   "BonusPointsStatus"  NUMBER(9),
   "MemberCode"         VARCHAR2(15),
   "MemberName"         VARCHAR2(100),
   "MemberPhone"        VARCHAR2(30),
   "IDType"             VARCHAR2(100),
   "IDNumer"            VARCHAR2(50),
   "MemberRank"         VARCHAR2(100),
   "MemberHandStatus"   NUMBER(9),
   "FrozenStatus"       NUMBER(9),
   "GenerateClaimTime"  DATE,
   "IsContainPhoto"     NUMBER(9),
   "VehicleLocationCheck" NUMBER(9),
   "TrailerMileage"     NUMBER(9),
   "IsAgreement"        NUMBER(9),
   "ArrivalTime"        DATE,
   "DepartureTime"      DATE,
   "AtStationTime"      NUMBER(15,6),
   "ExtendedWarrantyProductId" NUMBER(9),
   "ExtendedWarrantyProductCode" VARCHAR2(50),
   "ExtendedWarrantyProductName" VARCHAR2(100),
   "ExtendedWarrantyMileage" NUMBER(9),
   "ExtendedWarrantyTerm" NUMBER(15,6),
   "ExtensionCoverage"  VARCHAR2(500),
   "TestResults"        VARCHAR2(4000),
   "GradeCoefficient"   NUMBER(15,6),
   constraint PK_REPAIRORDER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairOrderFaultReason"                              */
/*==============================================================*/
create table "RepairOrderFaultReason"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairOrderId"      NUMBER(9)                       not null,
   "RepairTempletId"    NUMBER(9),
   "RepairTempletCode"  VARCHAR2(50),
   "MalfunctionId"      NUMBER(9),
   "QualityInformationId" NUMBER(9),
   "QualityInformationCode" VARCHAR2(50),
   "MalfunctionCode"    VARCHAR2(50),
   "MalfunctionDescription" VARCHAR2(600),
   "RepairClaimApplicationId" NUMBER(9),
   "RepairClaimApplicationCode" VARCHAR2(50),
   "OutFaultReason"     NUMBER(9)                       not null,
   "Amendments"         VARCHAR2(600),
   "RejectStatus"       NUMBER(9),
   "FaultyPartsId"      NUMBER(9)                       not null,
   "FaultyPartsCode"    VARCHAR2(50),
   "FaultyPartsName"    VARCHAR2(100),
   "FaultyPartsSupplierId" NUMBER(9)                       not null,
   "FaultyPartsWCId"    NUMBER(9),
   "FaultyPartsSupplierCode" VARCHAR2(50),
   "FaultyPartsSupplierName" VARCHAR2(100),
   "FaultyPartsSerialNumber" VARCHAR2(50),
   "FaultyPartsAssemblySerial" VARCHAR2(50),
   "ClaimSupplierId"    NUMBER(9),
   "ClaimSupplierCode"  VARCHAR2(50),
   "ClaimSupplierName"  VARCHAR2(100),
   "TempSupplierId"     NUMBER(9),
   "TempSupplierCode"   VARCHAR2(50),
   "TempSupplierName"   VARCHAR2(100),
   "ServiceActivityId"  NUMBER(9),
   "ServiceActivityCode" VARCHAR2(50),
   "ServiceActivityType" NUMBER(9),
   "BillingMethod"      NUMBER(9)                       not null,
   "ServiceActivityTime" DATE,
   "CauseOtherMalfunction" NUMBER(1),
   "ClaimStatus"        NUMBER(9),
   "SettleAttribute"    NUMBER(9),
   "LaborCost"          NUMBER(19,4)                    not null,
   "MaterialCost"       NUMBER(19,4)                    not null,
   "PartsManagementCost" NUMBER(19,4)                    not null,
   "TotalAmount"        NUMBER(19,4)                    not null,
   "MalfunctionReason"  VARCHAR2(600),
   "Path"               VARCHAR2(2000),
   "Remark"             VARCHAR2(600),
   "OtherCost"          NUMBER(19,4)                    not null,
   "FaultyPartsAssemblyId" NUMBER(9),
   "FaultyPartsAssemblyCode" VARCHAR2(50),
   "FaultyPartsAssemblyName" VARCHAR2(100),
   "OtherCostReason"    VARCHAR2(200),
   constraint PK_REPAIRORDERFAULTREASON primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairOrderItemDetail"                               */
/*==============================================================*/
create table "RepairOrderItemDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairOrderFaultReasonId" NUMBER(9)                       not null,
   "RepairItemId"       NUMBER(9)                       not null,
   "RepairItemCode"     VARCHAR2(50)                    not null,
   "RepairItemName"     VARCHAR2(600)                   not null,
   "DefaultLaborHour"   NUMBER(15,6)                    not null,
   "WarrantyLaborHour"  NUMBER(15,6),
   "LaborUnitPrice"     NUMBER(19,4)                    not null,
   "ActualLaborCost"    NUMBER(19,4)                    not null,
   "LaborCost"          NUMBER(19,4)                    not null,
   "ActuralMaterialCost" NUMBER(19,4)                    not null,
   "MaterialCost"       NUMBER(19,4)                    not null,
   "IfSelfDefine"       NUMBER(1),
   "IfObliged"          NUMBER(1),
   "SettleAttribute"    NUMBER(9),
   "Remark"             VARCHAR2(600),
   "RepairWorkerId"     NUMBER(9),
   "RepairWorkerName"   VARCHAR2(100),
   "ProductCategoryID"  NUMBER(9),
   constraint PK_REPAIRORDERITEMDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairOrderMaterialDetail"                           */
/*==============================================================*/
create table "RepairOrderMaterialDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairClaimItemDetailId" NUMBER(9)                       not null,
   "UsedPartsId"        NUMBER(9)                       not null,
   "UsedPartsCode"      VARCHAR2(50)                    not null,
   "UsedPartsName"      VARCHAR2(100)                   not null,
   "PartsWarrantyCategoryId" NUMBER(9),
   "UsedPartsSerialNumber" VARCHAR2(50),
   "UsedPartsBatchNumber" VARCHAR2(50),
   "UsedPartsBarCode"   VARCHAR2(50),
   "UsedPartsSupplierId" NUMBER(9)                       not null,
   "UsedPartsSupplierCode" VARCHAR2(50),
   "UsedPartsSupplierName" VARCHAR2(100),
   "Quantity"           NUMBER(9)                       not null,
   "OldUnitPrice"       NUMBER(19,4)                    not null,
   "UnitPrice"          NUMBER(19,4)                    not null,
   "WarrantyPrice"      NUMBER(19,4),
   "MaterialCost"       NUMBER(19,4)                    not null,
   "ActuralMaterialCost" NUMBER(19,4)                    not null,
   "PartsManagementCost" NUMBER(19,4)                    not null,
   "UsedPartsDisposalStatus" NUMBER(9)                       not null,
   "UsedPartsReturnPolicy" NUMBER(9)                       not null,
   "NewPartsId"         NUMBER(9)                       not null,
   "NewPartsCode"       VARCHAR2(50)                    not null,
   "NewPartsName"       VARCHAR2(100)                   not null,
   "NewPartsSerialNumber" VARCHAR2(50),
   "NewPartsSupplierId" NUMBER(9)                       not null,
   "NewPartsSupplierCode" VARCHAR2(50)                    not null,
   "NewPartsSupplierName" VARCHAR2(100)                   not null,
   "NewPartsBatchNumber" VARCHAR2(50),
   "NewPartsSecurityNumber" VARCHAR2(50),
   "Remark"             VARCHAR2(600),
   "PartSource"         NUMBER(9),
   "MaterialType"       NUMBER(9)                       not null,
   constraint PK_REPAIRORDERMATERIALDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairOrderSupplierDetail"                           */
/*==============================================================*/
create table "RepairOrderSupplierDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairOrderFaultReasonId" NUMBER(9)                       not null,
   "SupplierId"         NUMBER(9)                       not null,
   "TotalAmount"        NUMBER(19,4),
   "LaborCost"          NUMBER(19,4),
   "MaterialCost"       NUMBER(19,4),
   "PartsManagementCost" NUMBER(19,4),
   "OtherCost"          NUMBER(19,4),
   constraint PK_REPAIRORDERSUPPLIERDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairTemplet"                                       */
/*==============================================================*/
create table "RepairTemplet"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Name"               VARCHAR2(100),
   "TempletType"        NUMBER(9),
   "BranchId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "ServiceProductLineId" NUMBER(9)                       not null,
   "ProductLineType"    NUMBER(9),
   "Status"             NUMBER(9),
   "MalfunctionId"      NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_REPAIRTEMPLET primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairTempletItemDetail"                             */
/*==============================================================*/
create table "RepairTempletItemDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairTempletId"    NUMBER(9)                       not null,
   "RepairItemId"       NUMBER(9)                       not null,
   "RepairItemCode"     VARCHAR2(50)                    not null,
   "RepairItemName"     VARCHAR2(600)                   not null,
   "DefaultLaborHour"   NUMBER(15,6)                    not null,
   "Remark"             VARCHAR2(200),
   constraint PK_REPAIRTEMPLETITEMDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairTempletMaterialDetail"                         */
/*==============================================================*/
create table "RepairTempletMaterialDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairTempletItemId" NUMBER(9)                       not null,
   "Quantity"           NUMBER(9)                       not null,
   "UnitPrice"          NUMBER(19,4)                    not null,
   "MaterialCost"       NUMBER(19,4)                    not null,
   "PartsManagementCost" NUMBER(19,4)                    not null,
   "NewPartsId"         NUMBER(9)                       not null,
   "NewPartsCode"       VARCHAR2(50)                    not null,
   "NewPartsName"       VARCHAR2(100)                   not null,
   "NewPartsSerialNumber" VARCHAR2(50),
   "NewPartsSupplierId" NUMBER(9)                       not null,
   "NewPartsSupplierCode" VARCHAR2(50)                    not null,
   "NewPartsSupplierName" VARCHAR2(100)                   not null,
   "NewPartsBatchNumber" VARCHAR2(50),
   "NewPartsSecurityNumber" VARCHAR2(50),
   "Remark"             VARCHAR2(200),
   constraint PK_REPAIRTEMPLETMATERIALDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairWorkOrder"                                     */
/*==============================================================*/
create table "RepairWorkOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9),
   "Code"               VARCHAR2(50)                    not null,
   "Vin"                VARCHAR2(50)                    not null,
   "VehicleId"          NUMBER(9)                       not null,
   "DealerId"           NUMBER(9)                       not null,
   "ServiceProductLineId" NUMBER(9)                       not null,
   "ProductLineType"    NUMBER(9),
   "DispatchingCode"    VARCHAR2(50),
   "VPCDispatchingCode" VARCHAR2(50),
   "Dispatching"        NUMBER(9),
   "CallCenterDispatching" VARCHAR2(50),
   "DispatchingSource"  NUMBER(9),
   "RepairClassificationId" NUMBER(9),
   "VehicleLicensePlate" VARCHAR2(50),
   "ContactPhone"       VARCHAR2(50),
   "VehicleContactPerson" VARCHAR2(50),
   "ContactAddress"     VARCHAR2(200),
   "DealerName"         VARCHAR2(100),
   "DealerCode"         VARCHAR2(50),
   "ConfirmDate"        DATE,
   "ConfirmUserId"      VARCHAR2(50),
   "ConfirmUserName"    VARCHAR2(100),
   "Distance"           NUMBER(18,4),
   "ServicerXPoint"     VARCHAR2(50),
   "ServicerYPoint"     VARCHAR2(50),
   "XPoint"             VARCHAR2(50),
   "YPoint"             VARCHAR2(50),
   SEQ                  NUMBER(9),
   "Memo"               VARCHAR2(200),
   "WorkOrderContent"   VARCHAR2(500),
   "PositionofFault"    VARCHAR2(50),
   "Customer"           VARCHAR2(51),
   "EngineerProcessingContent" VARCHAR2(500),
   "ArrivalDate"        DATE,
   "FinishingTime"      DATE,
   "ClosedLoopTime"     DATE,
   "EstimatedFinishingTime" DATE,
   "ComplaintClassification" NUMBER(9),
   "IsCallCenterSupport" NUMBER(1),
   "FeedBackNumber"     NUMBER(9),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9)                       not null,
   "CreatorName"        VARCHAR2(100)                   not null,
   "CreateTime"         DATE                            not null,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "NextTime"           DATE,
   "TaskProject"        VARCHAR2(500),
   "IsLastVisitOut"     NUMBER(1),
   "IsSatisfy"          NUMBER(9),
   "NoSatisfyReason"    VARCHAR2(200),
   constraint PK_REPAIRWORKORDER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairWorkOrderMd"                                   */
/*==============================================================*/
create table "RepairWorkOrderMd"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "BranchId"           NUMBER(9)                       not null,
   "RowNo"              VARCHAR2(50),
   "ChewId"             VARCHAR2(50),
   "Srnum"              VARCHAR2(50),
   "Type"               NUMBER(9),
   "SubType"            NUMBER(9),
   "DetailType"         VARCHAR2(50),
   "Description"        VARCHAR2(2000),
   "UtcdateTime"        DATE,
   "SubCompany"         VARCHAR2(100),
   "FaultStmptom"       VARCHAR2(200),
   "FalutGround"        VARCHAR2(200),
   "UrgencyLevel"       NUMBER(9),
   "Expect"             VARCHAR2(50),
   "ExpectTime"         DATE,
   "SourceChannel"      VARCHAR2(50),
   "VinCode"            VARCHAR2(50),
   "VehicleId"          NUMBER(9),
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryCode" VARCHAR2(50),
   "ServiceProductLineId" NUMBER(9)                       not null,
   "SubBrand"           VARCHAR2(50),
   "DriveMile"          VARCHAR2(50),
   "AccountName"        VARCHAR2(200),
   "ContactName"        VARCHAR2(50),
   "ContactCellPhone"   VARCHAR2(50),
   "Spec"               VARCHAR2(200),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9)                       not null,
   "CreatorName"        VARCHAR2(100)                   not null,
   "CreateTime"         DATE                            not null,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   "ActiviteId"         VARCHAR2(50),
   "ActiviteNum"        VARCHAR2(50),
   "SendTime"           DATE,
   "HandlePerson"       VARCHAR2(100),
   "HandleDepartment"   VARCHAR2(100),
   "DealerName"         VARCHAR2(100),
   "DealerCode"         VARCHAR2(30),
   "DealerId"           NUMBER(9)                       not null,
   "MarketDepartmentId" NUMBER(9)                       not null,
   "Status"             NUMBER(9),
   "ReceiveTime"        DATE,
   "ReceiverId"         NUMBER(9),
   "ReceiverName"       VARCHAR2(100),
   "StartRepairTime"    DATE,
   "StartRepairId"      NUMBER(9),
   "StartRepairName"    VARCHAR2(100),
   "ArriveStationTime"  DATE,
   "ArriveStationerId"  NUMBER(9),
   "ArriveStationerName" VARCHAR2(100),
   "RevokeTime"         DATE,
   "IsCanFinish"        NUMBER(9),
   "CannotHandleReason" NUMBER(9),
   "RejectTime"         DATE,
   "RejectorId"         NUMBER(9),
   "RejectorName"       VARCHAR2(100),
   "LeavingTime"        DATE,
   "LeavingId"          NUMBER(9),
   "LeavingName"        VARCHAR2(100),
   "ArriveTime"         DATE,
   "ArriverId"          NUMBER(9),
   "ArriverName"        VARCHAR2(100),
   "FimishTime"         DATE,
   "FimishId"           NUMBER(9),
   "FimishName"         VARCHAR2(100),
   "FeedBackTime"       DATE,
   "RefuseReason"       NUMBER(9),
   "RepairCategory"     NUMBER(9),
   "ProcessMode"        NUMBER(9),
   "ProcessContent"     VARCHAR2(500),
   "RepairOrderId"      NUMBER(9),
   "RepairOrderCode"    VARCHAR2(50),
   "PartsCodeName"      VARCHAR2(200),
   "PartsSourceChannel" NUMBER(9),
   "PartsSalesOrderId"  NUMBER(9),
   "PartsSalesOrderCode" VARCHAR2(50),
   "SalesCategoryName"  VARCHAR2(100),
   "PartArriveTime"     DATE,
   "IsUsed"             NUMBER(1)                       not null,
   "RevokeId"           NUMBER(9),
   "RevokeName"         VARCHAR2(100),
   "IsClickTime"        DATE,
   "MemberType"         VARCHAR2(100),
   constraint PK_REPAIRWORKORDERMD primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RepairWorkOrderMdFaultReason"                        */
/*==============================================================*/
create table "RepairWorkOrderMdFaultReason"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairWorkOrderMdId" NUMBER(9)                       not null,
   "MalfunctionCode"    VARCHAR2(100),
   "MalfunctionDescription" VARCHAR2(1000),
   "RepairComment"      VARCHAR2(1000),
   constraint PK_REPAIRWORKORDERMDFAULTREASO primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ReturnVisitQuest"                                    */
/*==============================================================*/
create table "ReturnVisitQuest"  (
   "id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "QuestsessionId"     VARCHAR2(50),
   "IsConnection"       NUMBER(1),
   "FailReason"         VARCHAR2(500),
   "IsTrue"             NUMBER(1),
   "Description"        VARCHAR2(500),
   "Type"               NUMBER(9),
   "OrderId"            VARCHAR2(50),
   "Score"              VARCHAR2(50),
   "Channel"            NUMBER(9),
   "SurverName"         VARCHAR2(50),
   "DealerId"           NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(100),
   "DealerName"         VARCHAR2(100),
   "MarketDepartmentId" NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryCode" VARCHAR2(50),
   "RevisitDays"        DATE,
   "CreatorName"        VARCHAR2(100),
   "CreatorId"          NUMBER(9),
   "CreateTime"         DATE,
   "Status"             NUMBER(9),
   "Remark"             VARCHAR2(200),
   constraint PK_RETURNVISITQUEST primary key ("id")
)
/

/*==============================================================*/
/* Table: "ReturnVisitQuestDetail"                              */
/*==============================================================*/
create table "ReturnVisitQuestDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "QuestsessionDetailID" VARCHAR2(30),
   "ReturnVisitQuestId" NUMBER(9)                       not null,
   QUEST                VARCHAR2(1000),
   "Choice"             VARCHAR2(1000),
   "ChoiceScore"        VARCHAR2(50),
   "TextResponse"       VARCHAR2(1000),
   constraint PK_RETURNVISITQUESTDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ServiceTripClaimApplication"                         */
/*==============================================================*/
create table "ServiceTripClaimApplication"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Status"             NUMBER(9)                       not null,
   "SalesRegionId"      NUMBER(9),
   "SalesStatus"        NUMBER(9)                       not null,
   "MarketDepartmentId" NUMBER(9)                       not null,
   "ServiceTripType"    NUMBER(9),
   "RepairRequestOrderCode" VARCHAR2(50),
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "RepairObjectId"     NUMBER(9)                       not null,
   "ProductType"        NUMBER(9),
   "Motive"             VARCHAR2(200),
   "Applicant"          VARCHAR2(50),
   "ApplicationTel"     VARCHAR2(50),
   "DealerId"           NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(50)                    not null,
   "DealerName"         VARCHAR2(100)                   not null,
   "FirstClassStationId" NUMBER(9),
   "FirstClassStationCode" VARCHAR2(50),
   "FirstClassStationName" VARCHAR2(100),
   "TrafficWay"         VARCHAR2(50),
   "VehicleId"          NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   "VehicleLicensePlate" VARCHAR2(50),
   "SalesDate"          DATE,
   "OutOfFactoryDate"   DATE,
   "FaultyPartsId"      NUMBER(9),
   "FaultyPartsCode"    VARCHAR2(50),
   "FaultyPartsName"    VARCHAR2(100),
   "FaultyPartsSupplierId" NUMBER(9),
   "FaultyPartsSupplierCode" VARCHAR2(50),
   "FaultyPartsSupplierName" VARCHAR2(100),
   "RepairRequestTime"  DATE,
   "BranchId"           NUMBER(9)                       not null,
   "ServiceProductLineId" NUMBER(9)                       not null,
   "ProductLineType"    NUMBER(9),
   "VehicleContactPerson" VARCHAR2(100),
   "ContactPhone"       VARCHAR2(50),
   "ContactAddress"     VARCHAR2(200),
   "ProvinceName"       VARCHAR2(50),
   "CityName"           VARCHAR2(50),
   "CountyName"         VARCHAR2(50),
   "DetailedAddress"    VARCHAR2(200),
   "ServiceTripReason"  VARCHAR2(200),
   "ServiceTripDistance" NUMBER(9),
   "ServiceTripPerson"  NUMBER(9),
   "ServiceTripDuration" NUMBER(9),
   "IfOwnVehicle"       NUMBER(1),
   "EstimatedFinishingTime" DATE,
   "IfUseTowTruck"      NUMBER(1),
   "EstimatedTowCharge" NUMBER(19,4),
   "EstimatedFieldServiceCharge" NUMBER(19,4),
   "OtherCost"          NUMBER(19,4),
   "OtherCostReason"    VARCHAR2(200),
   "TotalAmount"        NUMBER(19,4),
   "RejectReason"       VARCHAR2(200),
   "ApproveCommentHistory" VARCHAR2(2000),
   "InitialApproveComment" VARCHAR2(200),
   "SupplyApproveComment" VARCHAR2(200),
   "FinalApproveComment" VARCHAR2(200),
   "CheckComment"       VARCHAR2(200),
   "ApproveComment"     VARCHAR2(200),
   "ClaimStatus"        NUMBER(1)                       not null,
   "Mileage"            NUMBER(9),
   "WorkingHours"       NUMBER(9),
   "Capacity"           NUMBER(9),
   "Remark"             VARCHAR2(500),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "InitialApproverId"  NUMBER(9),
   "InitialApproverName" VARCHAR2(100),
   "InitialApproveTime" DATE,
   "SupplyApproverId"   NUMBER(9),
   "SupplyApproverName" VARCHAR2(100),
   "SupplyApproveTime"  DATE,
   "FinalApproverId"    NUMBER(9),
   "FinalApproverName"  VARCHAR2(100),
   "FinalApproveTime"   DATE,
   "CheckerId"          NUMBER(9),
   "CheckerName"        VARCHAR2(100),
   "CheckTime"          DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "RejectId"           NUMBER(9),
   "RejectName"         VARCHAR2(100),
   "RejectTime"         DATE,
   "Path"               VARCHAR2(2000),
   "RowVersion"         TIMESTAMP,
   "OutVehicleLicensePlate" VARCHAR2(50),
   "SubmitterId"        NUMBER(9),
   "SubmitterName"      VARCHAR2(100),
   "SubmitTime"         DATE,
   constraint PK_SERVICETRIPCLAIMAPPLICATION primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ServiceTripClaimBill"                                */
/*==============================================================*/
create table "ServiceTripClaimBill"  (
   "Id"                 NUMBER(9)                       not null,
   "ServiceTripClaimAppId" NUMBER(9),
   "ServiceTripClaimAppCode" VARCHAR2(50),
   "Code"               VARCHAR2(50)                    not null,
   "Status"             NUMBER(9)                       not null,
   "SettlementStatus"   NUMBER(9)                       not null,
   "SupplierSettleStatus" NUMBER(9),
   "SalesStatus"        NUMBER(9)                       not null,
   "ServiceTripType"    NUMBER(9)                       not null,
   "DealerId"           NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(50)                    not null,
   "DealerName"         VARCHAR2(100)                   not null,
   "MarketingDepartmentId" NUMBER(9)                       not null,
   "VehicleId"          NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   "VehicleLicensePlate" VARCHAR2(50),
   "RepairRequestTime"  DATE                            not null,
   "SalesDate"          DATE,
   "Mileage"            NUMBER(9),
   "OutOfFactoryDate"   DATE,
   "Capacity"           NUMBER(9),
   "WorkingHours"       NUMBER(9),
   "BranchId"           NUMBER(9)                       not null,
   "ServiceProductLineId" NUMBER(9)                       not null,
   "ProductLineType"    NUMBER(9),
   "VehicleContactPerson" VARCHAR2(100),
   "ContactPhone"       VARCHAR2(50),
   "ContactAddress"     VARCHAR2(200),
   "ServiceTripTime"    DATE,
   "FinishingTime"      DATE,
   "ServiceTripReason"  VARCHAR2(200),
   "OutServiceCarUnitPrice" NUMBER(19,4),
   "OutSubsidyPrice"    NUMBER(19,4),
   "RejectQty"          NUMBER(9),
   "RejectReason"       VARCHAR2(200),
   "ProvinceName"       VARCHAR2(50),
   "CityName"           VARCHAR2(50),
   "CountyName"         VARCHAR2(50),
   "DetailedAddress"    VARCHAR2(200),
   "TrafficWay"         VARCHAR2(200),
   "AreaId"             NUMBER(9),
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "RepairObjectId"     NUMBER(9),
   "OutVehicleLicensePlate" VARCHAR2(50),
   "RepairClaimBillId"  NUMBER(9)                       not null,
   "OneLevelDealerId"   NUMBER(9),
   "OneLevelDealerCode" VARCHAR2(50),
   "OneLevelDealerName" VARCHAR2(100),
   "RepairOrderId"      NUMBER(9),
   "ServiceTripRegion"  VARCHAR2(50),
   "ServiceTripDistance" NUMBER(9)                       not null,
   "SettleDistance"     NUMBER(9)                       not null,
   "DealerTripPerson"   NUMBER(9),
   "DealerTripDuration" NUMBER(9),
   "ServiceTripPerson"  NUMBER(9),
   "ServiceTripDuration" NUMBER(9),
   "IfUseOwnVehicle"    NUMBER(1),
   "IfUseTowTruck"      NUMBER(1),
   "TowCharge"          NUMBER(19,4),
   "FieldServiceCharge" NUMBER(19,4),
   "OtherCost"          NUMBER(19,4),
   "OtherCostReason"    VARCHAR2(200),
   "TotalAmount"        NUMBER(19,4),
   "IfClaimToSupplier"  NUMBER(1)                       not null,
   "ClaimSupplierId"    NUMBER(9),
   "ClaimSupplierCode"  VARCHAR2(50),
   "ClaimSupplierName"  VARCHAR2(100),
   "ResponsibleUnitId"  NUMBER(9),
   "ApprovalComment"    VARCHAR2(200),
   "FinalApproveComment" VARCHAR2(200),
   "ApproveCommentHistory" VARCHAR2(2000),
   "Remark"             VARCHAR2(600),
   "SupplierCheckerId"  NUMBER(9),
   "SupplierCheckerName" VARCHAR2(50),
   "SupplierCheckTime"  DATE,
   "SupplierCheckComment" VARCHAR2(200),
   "SupplierCheckStatus" NUMBER(9),
   "SupplierApproverId" NUMBER(9),
   "SupplierApproverName" VARCHAR2(50),
   "SupplierApproveTime" DATE,
   "SupplierApproveComment" VARCHAR2(200),
   "SupplierApproveStatus" NUMBER(9),
   "InitialApproverId"  NUMBER(9),
   "InitialApproverName" VARCHAR2(100),
   "InitialApproveTime" DATE,
   "InitialApproveComment" VARCHAR2(200),
   "FinalApproverId"    NUMBER(9),
   "FinalApproverName"  VARCHAR2(100),
   "FinalApproverTime"  DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "CancelReason"       VARCHAR2(200),
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "RowVersion"         TIMESTAMP,
   "RejectId"           NUMBER(9),
   "RejectName"         VARCHAR2(100),
   "RejectTime"         DATE,
   "OfferOtherCost"     NUMBER(19,4),
   "OffertrailerCost"   NUMBER(19,4),
   "TrailerMileage"     NUMBER(9),
   constraint PK_SERVICETRIPCLAIMBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SupplierExpenseAdjustBill"                           */
/*==============================================================*/
create table "SupplierExpenseAdjustBill"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Status"             NUMBER(9)                       not null,
   "SettlementStatus"   NUMBER(9)                       not null,
   "TransactionCategory" NUMBER(9)                       not null,
   "SupplierId"         NUMBER(9)                       not null,
   "SupplierCode"       VARCHAR2(20)                    not null,
   "SupplierName"       VARCHAR2(100)                   not null,
   "BranchId"           NUMBER(9)                       not null,
   "BranchCode"         VARCHAR2(20)                    not null,
   "BranchName"         VARCHAR2(100)                   not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "ProductLineType"    NUMBER(9),
   "ServiceProductLineId" NUMBER(9),
   "SourceType"         NUMBER(9),
   "SourceId"           NUMBER(9),
   "SourceCode"         VARCHAR2(50),
   "DebitOrReplenish"   NUMBER(9)                       not null,
   "TransactionAmount"  NUMBER(19,4)                    not null,
   "TransactionReason"  VARCHAR2(200)                   not null,
   "IfClaimToResponsible" NUMBER(1)                       not null,
   "ResponsibleUnitId"  NUMBER(9)                       not null,
   "SupplierContactPerson" VARCHAR2(50),
   "SupplierPhoneNumber" VARCHAR2(50),
   "SupplierAddress"    VARCHAR2(100),
   "Memo"               VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_SUPPLIEREXPENSEADJUSTBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "TransactionInterfaceList"                            */
/*==============================================================*/
create table "TransactionInterfaceList"  (
   "Id"                 NUMBER(9)                       not null,
   "TransactionInterfaceLogId" NUMBER(9),
   "RightsItem"         VARCHAR2(100),
   "RightsCode"         VARCHAR2(100),
   "IsRights"           NUMBER(1),
   "BenefiType"         VARCHAR2(30),
   "BenefiUom"          NUMBER(9),
   "DiscountNum"        NUMBER(19,4),
   "Time"               NUMBER(9),
   constraint PK_TRANSACTIONINTERFACELIST primary key ("Id")
)
/

/*==============================================================*/
/* Table: "TransactionInterfaceLog"                             */
/*==============================================================*/
create table "TransactionInterfaceLog"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairOrderID"      NUMBER(9),
   "MemberNumber"       VARCHAR2(15),
   "MemberName"         VARCHAR2(100),
   "IDType"             VARCHAR2(100),
   "IDNumer"            VARCHAR2(50),
   "MemberRank"         VARCHAR2(100),
   "MemberPhone"        VARCHAR2(30),
   "MemberValue"        NUMBER(19,4),
   "MaxMemberValue"     NUMBER(9),
   "MemberUsedIntegral" NUMBER(19,4),
   "LaborCost"          NUMBER(19,4),
   "MaterialCost"       NUMBER(19,4),
   "TrimCost"           NUMBER(19,4),
   "OtherCost"          NUMBER(19,4),
   "OutRange"           NUMBER(9),
   "FieldTotalAmount"   NUMBER(19,4),
   "TotalAmount"        NUMBER(19,4),
   "Discount"           NUMBER(19,4),
   "CostDiscount"       NUMBER(19,4),
   "DeductionCost"      NUMBER(19,4),
   "MemberRightsCost"   NUMBER(19,4),
   "DeductionTotalAmount" NUMBER(19,4),
   "TransactionTime"    DATE,
   "SyncStatus"         NUMBER(9),
   "SyncMessage"        VARCHAR2(200),
   "SyncTime"           DATE,
   VIN                  VARCHAR2(100),
   "MinServiceCode"     VARCHAR2(100),
   "FrozenStatus"       NUMBER(9),
   constraint PK_TRANSACTIONINTERFACELOG primary key ("Id")
)
/

/*==============================================================*/
/* Table: "VehicleBrandPMSRealation"                            */
/*==============================================================*/
create table "VehicleBrandPMSRealation"  (
   "Id"                 NUMBER(9)                       not null,
   "MainBrandCode"      VARCHAR2(100),
   "MainBrandName"      VARCHAR2(100),
   "PartsSalesCategoryCode" VARCHAR2(100),
   "PartsSalesCategoryName" VARCHAR2(100),
   "PartsSalesCategoryId" NUMBER(9),
   "BranchCode"         VARCHAR2(100),
   "BranchId"           NUMBER(9),
   constraint PK_VEHICLEBRANDPMSREALATION primary key ("Id")
)
/

/*==============================================================*/
/* Table: "VehicleMileageChangeRecord"                          */
/*==============================================================*/
create table "VehicleMileageChangeRecord"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "RelatedBillId"      NUMBER(9)                       not null,
   "RelatedBillType"    NUMBER(9)                       not null,
   "VehicleId"          NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   "OriginalMileage"    NUMBER(9)                       not null,
   "NewMileage"         NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_VEHICLEMILEAGECHANGERECORD primary key ("Id")
)
/

alter table "DTMRepairContractItem"
   add constraint FK_DTMREPAI_FK_REPCON_DTMREPA2 foreign key ("DTMObjid")
      references "DTMRepairContract" ("DTMObjid")
/

alter table "DTMRepairContractMaterial"
   add constraint FK_DTMREPAI_FKT_REPCI_DTMREPAI foreign key ("DTMRepairContractItemObjid")
      references "DTMRepairContractItem" ("DTMRepairContractItemObjid")
/

alter table "DTMRepairContractOrderlist"
   add constraint FK_DTMREPAI_FK_REPCON_DTMREPAI foreign key ("DTMRepairContractObjid")
      references "DTMRepairContract" ("DTMObjid")
/


create or replace procedure "Prc_InvalidRepairOrder"(<arg> in out <type>) as
declare
begin
  update RepairClaimApplication a
  set a.Status=99,a.Remark ='超期15天未使用，自动作废'
  where ceil((select sysdate from dual) - a.submittime)>15;

  update ServiceTripClaimApplication b
  set b.Status=99,b.Remark ='超期15天未使用，自动作废'
  where ceil((select sysdate from dual) - b.submittime)>15;

end;
/

