/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2022/9/20 11:02:29                           */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"OverstockPartsAdjustBill"');
  if num>0 then
    execute immediate 'drop table "OverstockPartsAdjustBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"OverstockPartsAdjustDetail"');
  if num>0 then
    execute immediate 'drop table "OverstockPartsAdjustDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"OverstockPartsApp"');
  if num>0 then
    execute immediate 'drop table "OverstockPartsApp" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"OverstockPartsAppDetail"');
  if num>0 then
    execute immediate 'drop table "OverstockPartsAppDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"OverstockPartsInformation"');
  if num>0 then
    execute immediate 'drop table "OverstockPartsInformation" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"OverstockPartsPlatFormBill"');
  if num>0 then
    execute immediate 'drop table "OverstockPartsPlatFormBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"OverstockPartsRecommend"');
  if num>0 then
    execute immediate 'drop table "OverstockPartsRecommend" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"OverstockPartsRecommendBill"');
  if num>0 then
    execute immediate 'drop table "OverstockPartsRecommendBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"OverstockPartsRecommendDetail"');
  if num>0 then
    execute immediate 'drop table "OverstockPartsRecommendDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"OverstockTransferOrder"');
  if num>0 then
    execute immediate 'drop table "OverstockTransferOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"OverstockTransferOrderDetail"');
  if num>0 then
    execute immediate 'drop table "OverstockTransferOrderDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_OverstockPartsAdjustBill"');
  if num>0 then
    execute immediate 'drop sequence "S_OverstockPartsAdjustBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_OverstockPartsAdjustDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_OverstockPartsAdjustDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_OverstockPartsApp"');
  if num>0 then
    execute immediate 'drop sequence "S_OverstockPartsApp"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_OverstockPartsAppDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_OverstockPartsAppDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_OverstockPartsInformation"');
  if num>0 then
    execute immediate 'drop sequence "S_OverstockPartsInformation"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_OverstockPartsPlatFormBill"');
  if num>0 then
    execute immediate 'drop sequence "S_OverstockPartsPlatFormBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_OverstockPartsRecommend"');
  if num>0 then
    execute immediate 'drop sequence "S_OverstockPartsRecommend"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_OverstockPartsRecommendBill"');
  if num>0 then
    execute immediate 'drop sequence "S_OverstockPartsRecommendBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_OverstockPartsRecommendDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_OverstockPartsRecommendDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_OverstockTransferOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_OverstockTransferOrder"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_OverstockTransferOrderDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_OverstockTransferOrderDetail"';
  end if;
end;
/

create sequence "S_OverstockPartsAdjustBill"
/

create sequence "S_OverstockPartsAdjustDetail"
/

create sequence "S_OverstockPartsApp"
/

create sequence "S_OverstockPartsAppDetail"
/

create sequence "S_OverstockPartsInformation"
/

create sequence "S_OverstockPartsPlatFormBill"
/

create sequence "S_OverstockPartsRecommend"
/

create sequence "S_OverstockPartsRecommendBill"
/

create sequence "S_OverstockPartsRecommendDetail"
/

create sequence "S_OverstockTransferOrder"
/

create sequence "S_OverstockTransferOrderDetail"
/

/*==============================================================*/
/* Table: "OverstockPartsAdjustBill"                            */
/*==============================================================*/
create table "OverstockPartsAdjustBill"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "SourceId"           NUMBER(9)                       not null,
   "SourceCode"         VARCHAR2(50)                    not null,
   "OriginalStorageCompanyId" NUMBER(9)                       not null,
   "OriginalStorageCompanyCode" VARCHAR2(50)                    not null,
   "OriginalStorageCompanyName" VARCHAR2(100)                   not null,
   "OriginalStorageCoCustAccountId" NUMBER(9)                       not null,
   "StorageCompanyType" NUMBER(9)                       not null,
   "DestStorageCompanyId" NUMBER(9)                       not null,
   "DestStorageCompanyCode" VARCHAR2(50)                    not null,
   "DestStorageCompanyName" VARCHAR2(100)                   not null,
   "DestStorageCoCustomerAccountId" NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "BranchCode"         VARCHAR2(50)                    not null,
   "BranchName"         VARCHAR2(100)                   not null,
   "SalesUnitId"        NUMBER(9)                       not null,
   "SalesUnitName"      VARCHAR2(100)                   not null,
   "SalesUnitOwnerCompanyId" NUMBER(9)                       not null,
   "SalesUnitOwnerCompanyCode" VARCHAR2(50)                    not null,
   "SalesUnitOwnerCompanyName" VARCHAR2(100)                   not null,
   "TransferReason"     VARCHAR2(200),
   "OriginalRequirementBillId" NUMBER(9)                       not null,
   "OriginalRequirementBillType" NUMBER(9)                       not null,
   "OriginalRequirementBillCode" VARCHAR2(50)                    not null,
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "ModifyTime"         DATE,
   "ConfirmorId"        NUMBER(9),
   "ConfirmorName"      VARCHAR2(100),
   "ConfirmationTime"   DATE,
   "AccomplisherId"     NUMBER(9),
   "AccomplisherName"   VARCHAR2(100),
   "AccomplishTime"     DATE,
   "RowVersion"         TIMESTAMP,
   "ReceiptWarehouseId" NUMBER(9),
   "ReceiptWarehouseCode" VARCHAR2(50),
   "ReceiptWarehouseName" VARCHAR2(100),
   "ShippingMethod"     NUMBER(9),
   "LinkName"           VARCHAR2(100),
   "LinkPhone"          VARCHAR2(100),
   "ReceivingAddress"   VARCHAR2(200),
   constraint PK_OVERSTOCKPARTSADJUSTBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "OverstockPartsAdjustDetail"                          */
/*==============================================================*/
create table "OverstockPartsAdjustDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "OverstockPartsAdjustBillId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "Quantity"           NUMBER(9)                       not null,
   "dealprice"          NUMBER(19,4)                    not null,
   "TransferOutPrice"   NUMBER(19,4)                    not null,
   "TransferInPrice"    NUMBER(19,4)                    not null,
   "Remark"             VARCHAR2(200),
   "finishQuantity"     NUMBER(9),
   constraint PK_OVERSTOCKPARTSADJUSTDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "OverstockPartsApp"                                   */
/*==============================================================*/
create table "OverstockPartsApp"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "StorageCompanyId"   NUMBER(9)                       not null,
   "StorageCompanyCode" VARCHAR2(50)                    not null,
   "StorageCompanyName" VARCHAR2(100)                   not null,
   "StorageCompanyType" NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "BranchCode"         VARCHAR2(50)                    not null,
   "BranchName"         VARCHAR2(100)                   not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryName" VARCHAR2(50),
   "ContactPerson"      VARCHAR2(50),
   "ContactPhone"       VARCHAR2(50),
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   "RejecterName"       VARCHAR2(50),
   "RejecterTime"       DATE,
   "RejectComment"      VARCHAR2(200),
   constraint PK_OVERSTOCKPARTSAPP primary key ("Id")
)
/

/*==============================================================*/
/* Table: "OverstockPartsAppDetail"                             */
/*==============================================================*/
create table "OverstockPartsAppDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "OverstockPartsAppId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "DealPrice"          NUMBER(19,4)                    not null,
   "SalePrice"          NUMBER(19,4),
   "IfConfirmed"        NUMBER(1),
   "Remark"             VARCHAR2(200),
   constraint PK_OVERSTOCKPARTSAPPDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "OverstockPartsInformation"                           */
/*==============================================================*/
create table "OverstockPartsInformation"  (
   "Id"                 NUMBER(9)                       not null,
   "StorageCompanyId"   NUMBER(9)                       not null,
   "StorageCompanyType" NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9),
   "PartsSalesCategoryName" VARCHAR2(50),
   "SparePartId"        NUMBER(9)                       not null,
   "dealprice"          NUMBER(19,4)                    not null,
   "ContactPerson"      VARCHAR2(50),
   "ContactPhone"       VARCHAR2(50),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   "SalePrice"          NUMBER(19,4),
   constraint PK_OVERSTOCKPARTSINFORMATION primary key ("Id")
)
/

/*==============================================================*/
/* Table: "OverstockPartsPlatFormBill"                          */
/*==============================================================*/
create table "OverstockPartsPlatFormBill"  (
   "Id"                 NUMBER(9)                       not null,
   "SihCompanyId"       NUMBER(9),
   "SihCompanyCode"     VARCHAR2(50),
   "SihCompanyName"     VARCHAR2(100),
   "CenterId"           NUMBER(9),
   "CenterName"         VARCHAR2(100),
   "DealerId"           NUMBER(9),
   "DealerCode"         VARCHAR2(50),
   "DealerName"         VARCHAR2(100),
   "WarehouseId"        NUMBER(9),
   "WarehouseCode"      VARCHAR2(50),
   "WarehouseName"      VARCHAR2(100),
   "SparePartId"        NUMBER(9),
   "SparePartCode"      VARCHAR2(50),
   "SparePartName"      VARCHAR2(100),
   "DiscountRate"       NUMBER(15,6),
   "DiscountedPrice"    NUMBER(19,4),
   "EntityStatus"       VARCHAR2(100),
   "Attachment"         VARCHAR2(2000),
   "Status"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   constraint PK_OVERSTOCKPARTSPLATFORMBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "OverstockPartsRecommend"                             */
/*==============================================================*/
create table "OverstockPartsRecommend"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Status"             NUMBER(9)                       not null,
   "WarehouseId"        NUMBER(9)                       not null,
   "WarehouseCode"      VARCHAR2(50)                    not null,
   "WarehouseName"      VARCHAR2(100)                   not null,
   "RejecteId"          NUMBER(9),
   "RejecterName"       VARCHAR2(50),
   "RejecterTime"       DATE,
   "RejectComment"      VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "SubmitterId"        NUMBER(9),
   "SubmitterName"      VARCHAR2(100),
   "SubmitTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "InitialApproverId"  NUMBER(9),
   "InitialApproverName" VARCHAR2(100),
   "InitialApproveTime" DATE,
   "CheckerId"          NUMBER(9),
   "CheckerName"        VARCHAR2(100),
   "CheckTime"          DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_OVERSTOCKPARTSRECOMMEND primary key ("Id")
)
/

/*==============================================================*/
/* Table: "OverstockPartsRecommendBill"                         */
/*==============================================================*/
create table "OverstockPartsRecommendBill"  (
   "Id"                 NUMBER(9)                       not null,
   "CenterId"           NUMBER(9),
   "CenterName"         VARCHAR2(100),
   "DealerId"           NUMBER(9),
   "DealerCode"         VARCHAR2(50),
   "DealerName"         VARCHAR2(100),
   "WarehouseId"        NUMBER(9),
   "WarehouseCode"      VARCHAR2(50),
   "WarehouseName"      VARCHAR2(100),
   "SparePartId"        NUMBER(9),
   "SparePartCode"      VARCHAR2(50),
   "SparePartName"      VARCHAR2(100),
   "CreateTime"         DATE,
   constraint PK_OVERSTOCKPARTSRECOMMENDBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "OverstockPartsRecommendDetail"                       */
/*==============================================================*/
create table "OverstockPartsRecommendDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "OverstockPartsRecommendId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "DiscountRate"       NUMBER(15,6),
   "DiscountedPrice"    NUMBER(19,4),
   "RetailPrice"        NUMBER(19,4),
   "ActualUseableQty"   NUMBER(9),
   "Quantity"           NUMBER(9),
   constraint PK_OVERSTOCKPARTSRECOMMENDDETA primary key ("Id")
)
/

/*==============================================================*/
/* Table: "OverstockTransferOrder"                              */
/*==============================================================*/
create table "OverstockTransferOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "TransferInWarehouseId" NUMBER(9),
   "TransferInWarehouseCode" VARCHAR2(50),
   "TransferInWarehouseName" VARCHAR2(50),
   "TransferInCorpId"   NUMBER(9)                       not null,
   "TransferInCorpCode" VARCHAR2(50)                    not null,
   "TransferInCorpName" VARCHAR2(100)                   not null,
   "TransferOutWarehouseId" NUMBER(9),
   "TransferOutWarehouseCode" VARCHAR2(50),
   "TransferOutWarehouseName" VARCHAR2(50),
   "TransferOutCorpId"  NUMBER(9)                       not null,
   "TransferOutCorpCode" VARCHAR2(50)                    not null,
   "TransferOutCorpName" VARCHAR2(100)                   not null,
   "CustomerType"       NUMBER(9),
   "TotalAmount"        NUMBER(19,4),
   "ReceivingAddress"   VARCHAR2(200),
   "ShippingMethod"     NUMBER(9),
   "PromisedDeliveryTime" DATE,
   "StopComment"        VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   constraint PK_OVERSTOCKTRANSFERORDER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "OverstockTransferOrderDetail"                        */
/*==============================================================*/
create table "OverstockTransferOrderDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "OverstockPartsTransferOrderId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "TransferQty"        NUMBER(9),
   "EnoughQty"          NUMBER(9),
   "RetailPrice"        NUMBER(19,4),
   "DiscountRate"       NUMBER(15,6),
   "DiscountedPrice"    NUMBER(19,4),
   constraint PK_OVERSTOCKTRANSFERORDERDETAI primary key ("Id")
)
/

