/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2018/1/15 16:42:20                           */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"AuthenticationType"');
  if num>0 then
    execute immediate 'drop table "AuthenticationType" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"BrandGradeMessage"');
  if num>0 then
    execute immediate 'drop table "BrandGradeMessage" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ChannelCapability"');
  if num>0 then
    execute immediate 'drop table "ChannelCapability" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"Dealer"');
  if num>0 then
    execute immediate 'drop table "Dealer" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DealerBusinessPermit"');
  if num>0 then
    execute immediate 'drop table "DealerBusinessPermit" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DealerBusinessPermitHistory"');
  if num>0 then
    execute immediate 'drop table "DealerBusinessPermitHistory" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DealerGradeInfo"');
  if num>0 then
    execute immediate 'drop table "DealerGradeInfo" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DealerHistory"');
  if num>0 then
    execute immediate 'drop table "DealerHistory" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DealerKeyEmployee"');
  if num>0 then
    execute immediate 'drop table "DealerKeyEmployee" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DealerKeyEmployeeHistory"');
  if num>0 then
    execute immediate 'drop table "DealerKeyEmployeeHistory" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DealerKeyPosition"');
  if num>0 then
    execute immediate 'drop table "DealerKeyPosition" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DealerMobileNumberHistory"');
  if num>0 then
    execute immediate 'drop table "DealerMobileNumberHistory" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DealerMobileNumberList"');
  if num>0 then
    execute immediate 'drop table "DealerMobileNumberList" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DealerPerTrainAut"');
  if num>0 then
    execute immediate 'drop table "DealerPerTrainAut" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DealerServiceExt"');
  if num>0 then
    execute immediate 'drop table "DealerServiceExt" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DealerServiceExtHistory"');
  if num>0 then
    execute immediate 'drop table "DealerServiceExtHistory" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DealerServiceInfo"');
  if num>0 then
    execute immediate 'drop table "DealerServiceInfo" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DealerServiceInfoHistory"');
  if num>0 then
    execute immediate 'drop table "DealerServiceInfoHistory" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PersonSubDealer"');
  if num>0 then
    execute immediate 'drop table "PersonSubDealer" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SubChannelRelation"');
  if num>0 then
    execute immediate 'drop table "SubChannelRelation" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SubDealer"');
  if num>0 then
    execute immediate 'drop table "SubDealer" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"TrainingType"');
  if num>0 then
    execute immediate 'drop table "TrainingType" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_AuthenticationType"');
  if num>0 then
    execute immediate 'drop sequence "S_AuthenticationType"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_BrandGradeMessage"');
  if num>0 then
    execute immediate 'drop sequence "S_BrandGradeMessage"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ChannelCapability"');
  if num>0 then
    execute immediate 'drop sequence "S_ChannelCapability"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_DealerBusinessPermit"');
  if num>0 then
    execute immediate 'drop sequence "S_DealerBusinessPermit"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_DealerBusinessPermitHistory"');
  if num>0 then
    execute immediate 'drop sequence "S_DealerBusinessPermitHistory"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_DealerGradeInfo"');
  if num>0 then
    execute immediate 'drop sequence "S_DealerGradeInfo"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_DealerHistory"');
  if num>0 then
    execute immediate 'drop sequence "S_DealerHistory"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_DealerKeyEmployee"');
  if num>0 then
    execute immediate 'drop sequence "S_DealerKeyEmployee"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_DealerKeyEmployeeHistory"');
  if num>0 then
    execute immediate 'drop sequence "S_DealerKeyEmployeeHistory"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_DealerKeyPosition"');
  if num>0 then
    execute immediate 'drop sequence "S_DealerKeyPosition"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_DealerMobileNumberHistory"');
  if num>0 then
    execute immediate 'drop sequence "S_DealerMobileNumberHistory"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_DealerMobileNumberList"');
  if num>0 then
    execute immediate 'drop sequence "S_DealerMobileNumberList"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_DealerPerTrainAut"');
  if num>0 then
    execute immediate 'drop sequence "S_DealerPerTrainAut"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_DealerServiceExtHistory"');
  if num>0 then
    execute immediate 'drop sequence "S_DealerServiceExtHistory"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_DealerServiceInfo"');
  if num>0 then
    execute immediate 'drop sequence "S_DealerServiceInfo"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_DealerServiceInfoHistory"');
  if num>0 then
    execute immediate 'drop sequence "S_DealerServiceInfoHistory"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PersonSubDealer"');
  if num>0 then
    execute immediate 'drop sequence "S_PersonSubDealer"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SubChannelRelation"');
  if num>0 then
    execute immediate 'drop sequence "S_SubChannelRelation"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SubDealer"');
  if num>0 then
    execute immediate 'drop sequence "S_SubDealer"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_TrainingType"');
  if num>0 then
    execute immediate 'drop sequence "S_TrainingType"';
  end if;
end;
/

create sequence "S_AuthenticationType"
/

create sequence "S_BrandGradeMessage"
/

create sequence "S_ChannelCapability"
/

create sequence "S_DealerBusinessPermit"
/

create sequence "S_DealerBusinessPermitHistory"
/

create sequence "S_DealerGradeInfo"
/

create sequence "S_DealerHistory"
/

create sequence "S_DealerKeyEmployee"
/

create sequence "S_DealerKeyEmployeeHistory"
/

create sequence "S_DealerKeyPosition"
/

create sequence "S_DealerMobileNumberHistory"
/

create sequence "S_DealerMobileNumberList"
/

create sequence "S_DealerPerTrainAut"
/

create sequence "S_DealerServiceExtHistory"
/

create sequence "S_DealerServiceInfo"
/

create sequence "S_DealerServiceInfoHistory"
/

create sequence "S_PersonSubDealer"
/

create sequence "S_SubChannelRelation"
/

create sequence "S_SubDealer"
/

create sequence "S_TrainingType"
/

/*==============================================================*/
/* Table: "AuthenticationType"                                  */
/*==============================================================*/
create table "AuthenticationType"  (
   "Id"                 NUMBER(9)                       not null,
   "AuthenticationTypeName" VARCHAR2(50)                    not null,
   "BranchId"           NUMBER(9)                       not null,
   "CreatorName"        VARCHAR2(100),
   "CreatorId"          NUMBER(9),
   "CreateTime"         DATE,
   "AuthenticationTypeCode" VARCHAR2(50),
   "ValDate"            NUMBER(9),
   "ModifyName"         VARCHAR2(100),
   "ModifyId"           NUMBER(9),
   "ModifyTime"         DATE,
   "CancelName"         VARCHAR2(100),
   "CancelId"           NUMBER(9),
   "CancelTime"         DATE,
   "Status"             NUMBER(9),
   constraint PK_AUTHENTICATIONTYPE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "BrandGradeMessage"                                   */
/*==============================================================*/
create table "BrandGradeMessage"  (
   "Id"                 NUMBER(9)                       not null,
   "BrandId"            NUMBER(9)                       not null,
   "BrandName"          VARCHAR2(50),
   "Grade"              VARCHAR2(50)                    not null,
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_BRANDGRADEMESSAGE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ChannelCapability"                                   */
/*==============================================================*/
create table "ChannelCapability"  (
   "Id"                 NUMBER(9)                       not null,
   "Name"               VARCHAR2(50)                    not null,
   "ChannelGrade"       NUMBER(9)                       not null,
   "Description"        VARCHAR2(500)                   not null,
   "IsSale"             NUMBER(1)                       not null,
   "IsPart"             NUMBER(1)                       not null,
   "IsService"          NUMBER(1)                       not null,
   "IsSurvey"           NUMBER(1)                       not null,
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_CHANNELCAPABILITY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "Dealer"                                              */
/*==============================================================*/
create table "Dealer"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Name"               VARCHAR2(100)                   not null,
   "IsVirtualDealer"    NUMBER(1)                       not null,
   "ShortName"          VARCHAR2(50),
   "Manager"            VARCHAR2(50),
   "CreateTime"         DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "ModifyTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "Status"             NUMBER(9)                       not null,
   "RowVersion"         TIMESTAMP,
   "IsSynchronization"  NUMBER(1),
   constraint PK_DEALER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "DealerBusinessPermit"                                */
/*==============================================================*/
create table "DealerBusinessPermit"  (
   "Id"                 NUMBER(9)                       not null,
   "DealerId"           NUMBER(9)                       not null,
   "ServiceProductLineId" NUMBER(9)                       not null,
   "ProductLineType"    NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "ServiceFeeGradeId"  NUMBER(9)                       not null,
   "EngineRepairPower"  NUMBER(9),
   "CreateTime"         DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   constraint PK_DEALERBUSINESSPERMIT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "DealerBusinessPermitHistory"                         */
/*==============================================================*/
create table "DealerBusinessPermitHistory"  (
   "Id"                 NUMBER(9)                       not null,
   "DealerBusinessPermitId" NUMBER(9),
   "DealerId"           NUMBER(9),
   "ServiceProductLineId" NUMBER(9),
   "BranchId"           NUMBER(9),
   "ServiceFeeGradeId"  NUMBER(9),
   "Status"             NUMBER(9)                       not null,
   "EngineRepairPower"  NUMBER(9),
   "CreateTime"         DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "FileTime"           DATE,
   "FilerId"            NUMBER(9),
   "FilerName"          VARCHAR2(50),
   "ProductLineType"    NUMBER(9),
   constraint PK_DEALERBUSINESSPERMITHISTORY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "DealerGradeInfo"                                     */
/*==============================================================*/
create table "DealerGradeInfo"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryName" VARCHAR2(100)                   not null,
   "GradeName"          VARCHAR2(100)                   not null,
   "Remark"             VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "CreateTime"         DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "ModifierId"         NUMBER(9),
   "ModifyTime"         DATE,
   "ModifierName"       VARCHAR2(100),
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_DEALERGRADEINFO primary key ("Id")
)
/

/*==============================================================*/
/* Table: "DealerHistory"                                       */
/*==============================================================*/
create table "DealerHistory"  (
   "Id"                 NUMBER(9)                       not null,
   "RecordId"           NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Name"               VARCHAR2(100),
   "CustomerCode"       VARCHAR2(50),
   "SupplierCode"       VARCHAR2(50),
   "FoundDate"          DATE,
   "RegionId"           NUMBER(9),
   "ProvinceName"       VARCHAR2(50),
   "CityName"           VARCHAR2(50),
   "CountyName"         VARCHAR2(50),
   "CityLevel"          NUMBER(9),
   "ShortName"          VARCHAR2(50),
   "ContactMobile"      VARCHAR2(50),
   "ContactPhone"       VARCHAR2(50),
   "ContactMail"        VARCHAR2(50),
   "CompanyFax"         VARCHAR2(50),
   "ContactPostCode"    VARCHAR2(100),
   "BusinessAddress"    VARCHAR2(200),
   "RegisterCode"       VARCHAR2(50),
   "RegisterName"       VARCHAR2(100),
   "RegisterCapital"    NUMBER(19,4),
   "FixedAsset"         NUMBER(19,4),
   "RegisterDate"       DATE,
   "CorporateNature"    NUMBER(9),
   "LegalRepresentative" VARCHAR2(100),
   "LegalRepresentTel"  VARCHAR2(100),
   "IdDocumentType"     NUMBER(9),
   "IdDocumentNumber"   VARCHAR2(50),
   "RegisteredAddress"  VARCHAR2(200),
   "BusinessScope"      VARCHAR2(200),
   "Remark"             VARCHAR2(200),
   "Manager"            VARCHAR2(50),
   "RepairQualification" NUMBER(9),
   "HasBranch"          NUMBER(9),
   "DangerousRepairQualification" NUMBER(9),
   "BrandScope"         VARCHAR2(50),
   "CompetitiveBrandScope" VARCHAR2(50),
   "ParkingArea"        NUMBER(15,6),
   "RepairingArea"      NUMBER(15,6),
   "EmployeeNumber"     NUMBER(9),
   "PartWarehouseArea"  NUMBER(15,6),
   "GeographicPosition" NUMBER(9),
   "OwnerCompany"       VARCHAR2(100),
   "MainBusinessAreas"  VARCHAR2(200),
   "AndBusinessAreas"   VARCHAR2(200),
   "BuildTime"          DATE,
   "TrafficRestrictionsdescribe" VARCHAR2(100),
   "ManagerPhoneNumber" VARCHAR2(50),
   "ManagerMobile"      VARCHAR2(50),
   "ManagerMail"        VARCHAR2(50),
   "ReceptionRoomArea"  NUMBER(15,6),
   "IsVehicleSalesInvoice" NUMBER(1),
   "IsPartsSalesInvoice" NUMBER(1),
   "TaxRegisteredNumber" VARCHAR2(50),
   "InvoiceTitle"       VARCHAR2(100),
   "InvoiceType"        NUMBER(9),
   "TaxpayerQualification" NUMBER(9),
   "InvoiceAmountQuota" NUMBER(19,4),
   "TaxRegisteredAddress" VARCHAR2(200),
   "TaxRegisteredPhone" VARCHAR2(50),
   "BankName"           VARCHAR2(200),
   "BankAccount"        VARCHAR2(200),
   "InvoiceTax"         NUMBER(15,6),
   "Linkman"            VARCHAR2(50),
   "ContactNumber"      VARCHAR2(50),
   "Fax"                VARCHAR2(50),
   "Distance"           NUMBER(9),
   "MinServiceCode"     VARCHAR2(50),
   "MinServiceName"     VARCHAR2(100),
   "FilerId"            NUMBER(9),
   "FilerName"          VARCHAR2(100),
   "FileTime"           DATE,
   "CreateTime"         DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "ModifyTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "Status"             NUMBER(9),
   "AName"              VARCHAR2(100),
   "AShortName"         VARCHAR2(50),
   "AManager"           VARCHAR2(50),
   "ARepairQualification" NUMBER(9),
   "AHasBranch"         NUMBER(9),
   "ADangerousRepairQualification" NUMBER(9),
   "ABrandScope"        VARCHAR2(50),
   "ACompetitiveBrandScope" VARCHAR2(50),
   "AParkingArea"       NUMBER(15,6),
   "ARepairingArea"     NUMBER(15,6),
   "AEmployeeNumber"    NUMBER(9),
   "APartWarehouseArea" NUMBER(15,6),
   "AGeographicPosition" NUMBER(9),
   "AOwnerCompany"      VARCHAR2(100),
   "AMainBusinessAreas" VARCHAR2(200),
   "AAndBusinessAreas"  VARCHAR2(200),
   "ABuildTime"         DATE,
   "ATrafficRestrictionsdescribe" VARCHAR2(100),
   "AManagerPhoneNumber" VARCHAR2(50),
   "AManagerMobile"     VARCHAR2(50),
   "AManagerMail"       VARCHAR2(50),
   "AReceptionRoomArea" NUMBER(15,6),
   "AIsVehicleSalesInvoice" NUMBER(1),
   "AIsPartsSalesInvoice" NUMBER(1),
   "ATaxRegisteredNumber" VARCHAR2(50),
   "AInvoiceTitle"      VARCHAR2(100),
   "AInvoiceType"       NUMBER(9),
   "ATaxpayerQualification" NUMBER(9),
   "AInvoiceAmountQuota" NUMBER(19,4),
   "ATaxRegisteredAddress" VARCHAR2(200),
   "ATaxRegisteredPhone" VARCHAR2(50),
   "ABankName"          VARCHAR2(200),
   "ABankAccount"       VARCHAR2(200),
   "AInvoiceTax"        NUMBER(15,6),
   "ALinkman"           VARCHAR2(50),
   "AContactNumber"     VARCHAR2(50),
   "AFax"               VARCHAR2(50),
   "AStatus"            NUMBER(9),
   "ContactPerson"      VARCHAR2(100),
   "ACustomerCode"      VARCHAR2(50),
   "ASupplierCode"      VARCHAR2(50),
   "AFoundDate"         DATE,
   "ARegionId"          NUMBER(9),
   "AProvinceName"      VARCHAR2(50),
   "ACityName"          VARCHAR2(50),
   "ACountyName"        VARCHAR2(50),
   "ACityLevel"         NUMBER(9),
   "AContactPerson"     VARCHAR2(100),
   "AContactMobile"     VARCHAR2(50),
   "AContactPhone"      VARCHAR2(50),
   "AContactMail"       VARCHAR2(50),
   "ACompanyFax"        VARCHAR2(50),
   "AContactPostCode"   VARCHAR2(100),
   "ABusinessAddress"   VARCHAR2(200),
   "ARegisterCode"      VARCHAR2(50),
   "ARegisterName"      VARCHAR2(100),
   "ARegisterCapital"   NUMBER(19,4),
   "AFixedAsset"        NUMBER(19,4),
   "ARegisterDate"      DATE,
   "ACorporateNature"   NUMBER(9),
   "ALegalRepresentative" VARCHAR2(100),
   "ALegalRepresentTel" VARCHAR2(100),
   "AIdDocumentType"    NUMBER(9),
   "AIdDocumentNumber"  VARCHAR2(50),
   "ARegisteredAddress" VARCHAR2(200),
   "ABusinessScope"     VARCHAR2(200),
   "Aremark"            VARCHAR2(200),
   "BusinessAddressLongitude" NUMBER(14,6),
   "BusinessAddressLatitude" NUMBER(14,6),
   "ABusinessAddressLongitude" NUMBER(14,6),
   "ABusinessAddressLatitude" NUMBER(14,6),
   "ADistance"          NUMBER(9),
   "AMinServiceCode"    VARCHAR2(50),
   "AMinServiceName"    VARCHAR2(100),
   constraint PK_DEALERHISTORY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "DealerKeyEmployee"                                   */
/*==============================================================*/
create table "DealerKeyEmployee"  (
   "Id"                 NUMBER(9)                       not null,
   "DealerId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "KeyPositionId"      NUMBER(9)                       not null,
   "Name"               VARCHAR2(100),
   "Sex"                NUMBER(9),
   "PhoneNumber"        VARCHAR2(100),
   "MobileNumber"       VARCHAR2(100),
   "Address"            VARCHAR2(100),
   "PostCode"           VARCHAR2(100),
   "Mail"               VARCHAR2(50),
   "Remark"             VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "CreatorName"        VARCHAR2(100),
   "CreatorId"          NUMBER(9),
   "CreateTime"         DATE,
   "ModifyTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "AbandonTime"        DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "MarketingDepartmentId" NUMBER(9),
   "IdCardNumber"       var50,
   "Birthday"           DATE,
   "School"             VARCHAR2(50),
   "Education"          NUMBER(9),
   "Professional"       VARCHAR2(100),
   "ProfessionalRank"   NUMBER(9),
   "EntryTime"          DATE,
   "IsOnJob"            NUMBER(9),
   "LeaveTime"          DATE,
   "WorkType"           NUMBER(9),
   "SkillLevel"         NUMBER(9),
   "BTPositionId"       NUMBER(9),
   "TransferTime"       DATE,
   "IsKeyPositions"     NUMBER(1)                       not null,
   constraint PK_DEALERKEYEMPLOYEE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "DealerKeyEmployeeHistory"                            */
/*==============================================================*/
create table "DealerKeyEmployeeHistory"  (
   "Id"                 NUMBER(9)                       not null,
   "RecordId"           NUMBER(9),
   "DealerId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "KeyPositionId"      NUMBER(9)                       not null,
   "Name"               VARCHAR2(100),
   "Sex"                NUMBER(9),
   "PhoneNumber"        VARCHAR2(100),
   "Address"            VARCHAR2(100),
   "PostCode"           VARCHAR2(100),
   "Mail"               VARCHAR2(50),
   "Remark"             VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "FileTime"           DATE,
   "FilerId"            NUMBER(9),
   "FilerName"          VARCHAR2(50),
   "CreateTime"         DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "ModifyTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "AbandonTime"        DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "MobileNumber"       VARCHAR2(100),
   "MarketingDepartmentId" NUMBER(9),
   "IdCardNumber"       var50,
   "Birthday"           DATE,
   "School"             VARCHAR2(50),
   "Education"          NUMBER(9),
   "Professional"       VARCHAR2(100),
   "ProfessionalRank"   NUMBER(9),
   "EntryTime"          DATE,
   "IsOnJob"            NUMBER(9),
   "LeaveTime"          DATE,
   "WorkType"           NUMBER(9),
   "SkillLevel"         NUMBER(9),
   "BTPositionId"       NUMBER(9),
   "TransferTime"       DATE,
   constraint PK_DEALERKEYEMPLOYEEHISTORY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "DealerKeyPosition"                                   */
/*==============================================================*/
create table "DealerKeyPosition"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "PositionName"       VARCHAR2(100),
   "Responsibility"     VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "ModifierName"       VARCHAR2(100),
   "Remark"             VARCHAR2(200),
   "IsKeyPositions"     NUMBER(1)                       not null,
   "RowVersion"         TIMESTAMP,
   constraint PK_DEALERKEYPOSITION primary key ("Id")
)
/

/*==============================================================*/
/* Table: "DealerMobileNumberHistory"                           */
/*==============================================================*/
create table "DealerMobileNumberHistory"  (
   "Id"                 NUMBER(9)                       not null,
   "DealerId"           NUMBER(9)                       not null,
   "VideoMobileNumber"  VARCHAR2(50),
   "CreateTime"         DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   constraint PK_DEALERMOBILENUMBERHISTORY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "DealerMobileNumberList"                              */
/*==============================================================*/
create table "DealerMobileNumberList"  (
   "Id"                 NUMBER(9)                       not null,
   "DealerId"           NUMBER(9)                       not null,
   "VideoMobileNumber"  VARCHAR2(50),
   constraint PK_DEALERMOBILENUMBERLIST primary key ("Id")
)
/

/*==============================================================*/
/* Table: "DealerPerTrainAut"                                   */
/*==============================================================*/
create table "DealerPerTrainAut"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "MarketingDepartmentId" NUMBER(9)                       not null,
   "DealerId"           NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(50),
   "DealerName"         VARCHAR2(100),
   "Name"               VARCHAR2(100)                   not null,
   "PositionId"         NUMBER(9)                       not null,
   "TrainingType"       NUMBER(9),
   "TrainingTime"       DATE,
   "AuthenticationType" NUMBER(9),
   "AuthenticationTime" DATE,
   "Status"             NUMBER(9)                       not null,
   "CreateTime"         DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "ModifierId"         NUMBER(9),
   "ModifyTime"         DATE,
   "ModifierName"       VARCHAR2(100),
   "IdCard"             VARCHAR2(50),
   "CertificateId"      VARCHAR2(100),
   "TrainingName"       VARCHAR2(100),
   "CancelId"           NUMBER(9),
   "CancelTime"         DATE,
   "CancelName"         VARCHAR2(100),
   constraint PK_DEALERPERTRAINAUT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "DealerServiceExt"                                    */
/*==============================================================*/
create table "DealerServiceExt"  (
   "Id"                 NUMBER(9)                       not null,
   "RepairQualification" NUMBER(9),
   "HasBranch"          NUMBER(9),
   "DangerousRepairQualification" NUMBER(9),
   "VehicleTravelRoute" NUMBER(9),
   "VehicleUseSpeciality" NUMBER(9),
   "VehicleDockingStation" NUMBER(9),
   "BrandScope"         VARCHAR2(1000),
   "CompetitiveBrandScope" VARCHAR2(50),
   "ParkingArea"        NUMBER(15,6),
   "RepairingArea"      NUMBER(15,6),
   "EmployeeNumber"     NUMBER(9),
   "PartWarehouseArea"  NUMBER(15,6),
   "GeographicPosition" NUMBER(9),
   "OwnerCompany"       VARCHAR2(100),
   "MainBusinessAreas"  VARCHAR2(2000),
   "AndBusinessAreas"   VARCHAR2(200),
   "BuildTime"          DATE,
   "TrafficRestrictionsdescribe" VARCHAR2(100),
   "ManagerPhoneNumber" VARCHAR2(50),
   "ManagerMobile"      VARCHAR2(50),
   "ManagerMail"        VARCHAR2(50),
   "ReceptionRoomArea"  NUMBER(15,6),
   "Remark"             VARCHAR2(500),
   "Status"             NUMBER(9)                       not null,
   "CreateTime"         DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "ModifierId"         NUMBER(9),
   "ModifyTime"         DATE,
   "ModifierName"       VARCHAR2(100),
   "RowVersion"         TIMESTAMP,
   "IsSynchronization"  NUMBER(1),
   constraint PK_DEALERSERVICEEXT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "DealerServiceExtHistory"                             */
/*==============================================================*/
create table "DealerServiceExtHistory"  (
   "Id"                 NUMBER(9)                       not null,
   "RecordId"           NUMBER(9)                       not null,
   "RepairQualification" NUMBER(9),
   "HasBranch"          NUMBER(9),
   "DangerousRepairQualification" NUMBER(9),
   "BrandScope"         VARCHAR2(50),
   "CompetitiveBrandScope" VARCHAR2(50),
   "ParkingArea"        NUMBER(15,6),
   "RepairingArea"      NUMBER(15,6),
   "EmployeeNumber"     NUMBER(9),
   "PartWarehouseArea"  NUMBER(15,6),
   "GeographicPosition" NUMBER(9),
   "OwnerCompany"       VARCHAR2(100),
   "MainBusinessAreas"  VARCHAR2(2000),
   "AndBusinessAreas"   VARCHAR2(200),
   "BuildTime"          DATE,
   "TrafficRestrictionsdescribe" VARCHAR2(100),
   "ManagerPhoneNumber" VARCHAR2(50),
   "ManagerMobile"      VARCHAR2(50),
   "ManagerMail"        VARCHAR2(50),
   "ReceptionRoomArea"  NUMBER(15,6),
   "Status"             NUMBER(9)                       not null,
   "FilerId"            NUMBER(9),
   "FilerName"          VARCHAR2(100),
   "FileTime"           DATE,
   "CreateTime"         DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "ModifierId"         NUMBER(9),
   "ModifyTime"         DATE,
   "ModifierName"       VARCHAR2(100),
   constraint PK_DEALERSERVICEEXTHISTORY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "DealerServiceInfo"                                   */
/*==============================================================*/
create table "DealerServiceInfo"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "BusinessCode"       VARCHAR2(50)                    not null,
   "BusinessName"       VARCHAR2(100)                   not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "DealerId"           NUMBER(9)                       not null,
   "MarketingDepartmentId" NUMBER(9)                       not null,
   "UsedPartsWarehouseId" NUMBER(9),
   "WarehouseId"        NUMBER(9),
   "ChannelCapabilityId" NUMBER(9)                       not null,
   "PartsManagingFeeGradeId" NUMBER(9),
   "GradeCoefficientId" NUMBER(9)                       not null,
   "OutServiceradii"    NUMBER(9)                       not null,
   "BusinessDivision"   VARCHAR2(100),
   "AccreditTime"       DATE,
   "ServicePermission"  VARCHAR2(100),
   "ServiceStationType" NUMBER(9)                       not null,
   "Area"               VARCHAR2(100),
   "RegionType"         VARCHAR2(100),
   "MaterialCostInvoiceType" NUMBER(9),
   "LaborCostCostInvoiceType" NUMBER(9),
   "LaborCostInvoiceRatio" NUMBER(15,6),
   "InvoiceTypeInvoiceRatio" NUMBER(15,6),
   "CancellingDate"     DATE,
   "IsOnDuty"           NUMBER(1)                       not null,
   "LaborHourFloatCoefficient" NUMBER(15,6),
   "FloatValidFrom"     DATE,
   "FloatValidTo"       DATE,
   "OutFeeGradeId"      NUMBER(9),
   "RepairAuthorityGrade" NUMBER(9)                       not null,
   "HotLine"            VARCHAR2(50),
   "Fix"                VARCHAR2(50),
   "Fax"                VARCHAR2(50),
   "Remark"             VARCHAR2(500),
   "Distance"           NUMBER(9),
   "MinServiceCode"     VARCHAR2(50),
   "MinServiceName"     VARCHAR2(100),
   "Status"             NUMBER(9)                       not null,
   "CreateTime"         DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "ModifierId"         NUMBER(9),
   "ModifyTime"         DATE,
   "ModifierName"       VARCHAR2(100),
   "PartReserveAmount"  VARCHAR2(100),
   "RowVersion"         TIMESTAMP,
   "SaleandServicesiteLayout" NUMBER(9),
   "HourPhone24"        VARCHAR2(11),
   "TrunkNetworkType"   NUMBER(9),
   "CenterStack"        NUMBER(1),
   "Grade"              VARCHAR2(50)                    not null,
   "ExternalState"      NUMBER(9),
   "DealerGradeId"      NUMBER(9),
   "DealerGradeName"    VARCHAR2(100),
   constraint PK_DEALERSERVICEINFO primary key ("Id")
)
/

/*==============================================================*/
/* Table: "DealerServiceInfoHistory"                            */
/*==============================================================*/
create table "DealerServiceInfoHistory"  (
   "Id"                 NUMBER(9)                       not null,
   "RecordId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "DealerId"           NUMBER(9)                       not null,
   "BusinessCode"       VARCHAR2(50),
   "BusinessName"       VARCHAR2(100),
   "MarketingDepartmentId" NUMBER(9),
   "UsedPartsWarehouseId" NUMBER(9),
   "WarehouseId"        NUMBER(9),
   "ChannelCapabilityId" NUMBER(9),
   "PartsManagingFeeGradeId" NUMBER(9),
   "GradeCoefficientId" NUMBER(9),
   "OutFeeGradeId"      NUMBER(9),
   "OutServiceradii"    NUMBER(9),
   "BusinessDivision"   VARCHAR2(100),
   "AccreditTime"       DATE,
   "ServicePermission"  VARCHAR2(100),
   "ServiceStationType" NUMBER(9),
   "Area"               VARCHAR2(100),
   "RegionType"         VARCHAR2(100),
   "MaterialCostInvoiceType" NUMBER(9),
   "LaborCostCostInvoiceType" NUMBER(9),
   "ATrunkNetworkType"  NUMBER(9),
   "LaborCostInvoiceRatio" NUMBER(15,6),
   "InvoiceTypeInvoiceRatio" NUMBER(15,6),
   "CancellingDate"     DATE,
   "IsOnDuty"           NUMBER(1),
   "RepairAuthorityGrade" NUMBER(9),
   "HotLine"            VARCHAR2(50),
   "Fix"                VARCHAR2(50),
   "Fax"                VARCHAR2(50),
   "Remark"             VARCHAR2(500),
   "Distance"           NUMBER(9),
   "MinServiceCode"     VARCHAR2(50),
   "MinServiceName"     VARCHAR2(100),
   "Grade"              VARCHAR2(50),
   "FileTime"           DATE,
   "FilerId"            NUMBER(9),
   "FilerName"          VARCHAR2(50),
   "Status"             NUMBER(9),
   "CreateTime"         DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "ModifyTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "PartReserveAmount"  VARCHAR2(100),
   "SaleandServicesiteLayout" NUMBER(9),
   "ABusinessCode"      VARCHAR2(50),
   "ABusinessName"      VARCHAR2(100),
   "AMarketingDepartmentId" NUMBER(9),
   "AUsedPartsWarehouseId" NUMBER(9),
   "AWarehouseId"       NUMBER(9),
   "AChannelCapabilityId" NUMBER(9),
   "APartsManagingFeeGradeId" NUMBER(9),
   "AGradeCoefficientId" NUMBER(9),
   "AOutFeeGradeId"     NUMBER(9),
   "AOutServiceradii"   NUMBER(9),
   "ABusinessDivision"  VARCHAR2(100),
   "AAccreditTime"      DATE,
   "AServicePermission" VARCHAR2(100),
   "AServiceStationType" NUMBER(9),
   "AArea"              VARCHAR2(100),
   "ARegionType"        VARCHAR2(100),
   "AMaterialCostInvoiceType" NUMBER(9),
   "ALaborCostCostInvoiceType" NUMBER(9),
   "ALaborCostInvoiceRatio" NUMBER(15,6),
   "AInvoiceTypeInvoiceRatio" NUMBER(15,6),
   "ACancellingDate"    DATE,
   "AIsOnDuty"          NUMBER(1),
   "ARepairAuthorityGrade" NUMBER(9),
   "AHotLine"           VARCHAR2(50),
   "AFix"               VARCHAR2(50),
   "AFax"               VARCHAR2(50),
   "ARemark"            VARCHAR2(500),
   "ADistance"          NUMBER(9),
   "AMinServiceCode"    VARCHAR2(50),
   "AMinServiceName"    VARCHAR2(100),
   "APartReserveAmount" VARCHAR2(100),
   "ASaleandServicesiteLayout" NUMBER(9),
   "AStatus"            NUMBER(9),
   "TrunkNetworkType"   NUMBER(9),
   "CenterStack"        NUMBER(1),
   "HourPhone24"        VARCHAR2(11),
   "ACenterStack"       NUMBER(1),
   "A24HourPhone"       VARCHAR2(11),
   "AGrade"             VARCHAR2(50),
   constraint PK_DEALERSERVICEINFOHISTORY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PersonSubDealer"                                     */
/*==============================================================*/
create table "PersonSubDealer"  (
   "Id"                 NUMBER(9)                       not null,
   "CompanyId"          NUMBER(9)                       not null,
   "PersonId"           NUMBER(9)                       not null,
   "SubDealerId"        NUMBER(9)                       not null,
   constraint PK_PERSONSUBDEALER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SubChannelRelation"                                  */
/*==============================================================*/
create table "SubChannelRelation"  (
   "Id"                 NUMBER(9)                       not null,
   "ParentChanelId"     NUMBER(9)                       not null,
   "SubChanelId"        NUMBER(9)                       not null,
   "BusinessScope"      VARCHAR2(50),
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   constraint PK_SUBCHANNELRELATION primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SubDealer"                                           */
/*==============================================================*/
create table "SubDealer"  (
   "Id"                 NUMBER(9)                       not null,
   "Name"               VARCHAR2(100),
   "Code"               VARCHAR2(50),
   "SubDealerType"      NUMBER(9),
   "DealerId"           NUMBER(9)                       not null,
   "Address"            VARCHAR2(200),
   "CreateTime"         DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "ModifyTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "AbandonTime"        DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "Status"             NUMBER(9)                       not null,
   "DealerManager"      VARCHAR2(200),
   "Manager"            VARCHAR2(50),
   "ManagerPhoneNumber" VARCHAR2(50),
   "ManagerMobile"      VARCHAR2(50),
   "Remark"             VARCHAR2(500),
   "ManagerMail"        VARCHAR2(50),
   "RowVersion"         TIMESTAMP,
   "BusinessAddressLongitude" NUMBER(14,6),
   "BusinessAddressLatitude" NUMBER(14,6),
   constraint PK_SUBDEALER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "TrainingType"                                        */
/*==============================================================*/
create table "TrainingType"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "CreatorName"        VARCHAR2(100),
   "CreatorId"          NUMBER(9),
   "CreateTime"         DATE,
   "TrainingTypeCode"   NUMBER(9),
   "TrainingCode"       VARCHAR2(50),
   "TrainingName"       VARCHAR2(100),
   "ModifyName"         VARCHAR2(100),
   "ModifyId"           NUMBER(9),
   "ModifyTime"         DATE,
   "TrainingTypeName"   NUMBER(9)                       not null,
   "CancelName"         VARCHAR2(100),
   "CancelId"           NUMBER(9),
   "CancelTime"         DATE,
   "Status"             NUMBER(9),
   constraint PK_TRAININGTYPE primary key ("Id")
)
/

