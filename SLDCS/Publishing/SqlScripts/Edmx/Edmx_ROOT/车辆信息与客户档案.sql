/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2018/2/23 16:48:19                           */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"Customer"');
  if num>0 then
    execute immediate 'drop table "Customer" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"LoadingDetail"');
  if num>0 then
    execute immediate 'drop table "LoadingDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RetainedCustomer"');
  if num>0 then
    execute immediate 'drop table "RetainedCustomer" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RetainedCustomerExtended"');
  if num>0 then
    execute immediate 'drop table "RetainedCustomerExtended" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"RetainedCustomerVehicleList"');
  if num>0 then
    execute immediate 'drop table "RetainedCustomerVehicleList" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"VehicleLinkman"');
  if num>0 then
    execute immediate 'drop table "VehicleLinkman" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"VehicleMaintePoilcyHistroy"');
  if num>0 then
    execute immediate 'drop table "VehicleMaintePoilcyHistroy" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"VehicleMaintenancePoilcy"');
  if num>0 then
    execute immediate 'drop table "VehicleMaintenancePoilcy" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"VehicleWarrantyCard"');
  if num>0 then
    execute immediate 'drop table "VehicleWarrantyCard" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_Customer"');
  if num>0 then
    execute immediate 'drop sequence "S_Customer"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_LoadingDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_LoadingDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RetainedCustomer"');
  if num>0 then
    execute immediate 'drop sequence "S_RetainedCustomer"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RetainedCustomerExtended"');
  if num>0 then
    execute immediate 'drop sequence "S_RetainedCustomerExtended"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_RetainedCustomerVehicleList"');
  if num>0 then
    execute immediate 'drop sequence "S_RetainedCustomerVehicleList"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_VehicleLinkman"');
  if num>0 then
    execute immediate 'drop sequence "S_VehicleLinkman"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_VehicleMaintePoilcyHistroy"');
  if num>0 then
    execute immediate 'drop sequence "S_VehicleMaintePoilcyHistroy"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_VehicleMaintenancePoilcy"');
  if num>0 then
    execute immediate 'drop sequence "S_VehicleMaintenancePoilcy"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_VehicleWarrantyCard"');
  if num>0 then
    execute immediate 'drop sequence "S_VehicleWarrantyCard"';
  end if;
end;
/

create sequence "S_Customer"
/

create sequence "S_LoadingDetail"
/

create sequence "S_RetainedCustomer"
/

create sequence "S_RetainedCustomerExtended"
/

create sequence "S_RetainedCustomerVehicleList"
/

create sequence "S_VehicleLinkman"
/

create sequence "S_VehicleMaintePoilcyHistroy"
/

create sequence "S_VehicleMaintenancePoilcy"
/

create sequence "S_VehicleWarrantyCard"
/

/*==============================================================*/
/* Table: "Customer"                                            */
/*==============================================================*/
create table "Customer"  (
   "Id"                 NUMBER(9)                       not null,
   "CustomerCode"       VARCHAR2(50)                    not null,
   "Name"               VARCHAR2(100)                   not null,
   "Gender"             NUMBER(9)                       not null,
   "CustomerType"       NUMBER(9)                       not null,
   "Age"                NUMBER(9),
   "RegionId"           NUMBER(9),
   "RegionName"         VARCHAR2(100),
   "ProvinceName"       VARCHAR2(100),
   "CityName"           VARCHAR2(100),
   "Fax"                VARCHAR2(50),
   "IfComplain"         NUMBER(1)                       not null,
   "CountyName"         VARCHAR2(100),
   "Address"            VARCHAR2(200),
   "PostCode"           VARCHAR2(6),
   "CellPhoneNumber"    VARCHAR2(50),
   "HomePhoneNumber"    VARCHAR2(50),
   "OfficePhoneNumber"  VARCHAR2(50),
   "IdDocumentType"     NUMBER(9),
   "IdDocumentNumber"   VARCHAR2(50),
   "Birthdate"          DATE,
   "Email"              VARCHAR2(50),
   "CompanyName"        VARCHAR2(100),
   "BusinessType"       NUMBER(9),
   "OccupationType"     NUMBER(9),
   "JobPosition"        NUMBER(9),
   "EducationLevel"     NUMBER(9),
   "DrivingSeniority"   NUMBER(9),
   "Status"             NUMBER(9)                       not null,
   "Income"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "Remark"             VARCHAR2(200),
   "RowVersion"         TIMESTAMP,
   "AddressType"        NUMBER(9),
   "PhoneNumber"        VARCHAR2(50),
   "CustomerGroup"      NUMBER(9),
   "CarCustomerType"    NUMBER(9),
   "CustomerAreaType"   NUMBER(9),
   "LegalRepresent"     VARCHAR2(30),
   "RegisterCapital"    NUMBER(9),
   "RegisterDate"       DATE,
   "RegisteredAddress"  VARCHAR2(200),
   "EnterpriseWebsite"  VARCHAR2(200),
   "Nation"             NUMBER(9),
   "FirstLincenseDate"  DATE,
   "Wechat"             VARCHAR2(50),
   "AnnualIncome"       NUMBER(9),
   "CustomerAddresType" NUMBER(9),
   "Profit"             NUMBER(9),
   "CustomerIdentity"   NUMBER(9),
   "CustomerNature"     NUMBER(9),
   "ProductQuality"     NUMBER(9),
   "CompetitiveQuality" NUMBER(9),
   "RegionalIndustryRankings" VARCHAR2(30),
   "EngageIndustry"     VARCHAR2(30),
   "IsAnchored"         NUMBER(1),
   "AnchoredBranch"     VARCHAR2(100),
   "CustomerStatus"     VARCHAR2(30),
   "IsDecisionMaker"    NUMBER(1),
   constraint PK_CUSTOMER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "LoadingDetail"                                       */
/*==============================================================*/
create table "LoadingDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "VehicleId"          NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   "SerialNumber"       VARCHAR2(50)                    not null,
   "FaultyPartsAssemblyName" VARCHAR2(100)                   not null,
   "ResponsibleUnitCode" VARCHAR2(50)                    not null,
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_LOADINGDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RetainedCustomer"                                    */
/*==============================================================*/
create table "RetainedCustomer"  (
   "Id"                 NUMBER(9)                       not null,
   "CustomerId"         NUMBER(9)                       not null,
   "IfVisitBackNeeded"  NUMBER(1),
   "IfTextAccepted"     NUMBER(1),
   "IfAdAccepted"       NUMBER(1),
   "CardNumber"         VARCHAR2(50),
   "IfVIP"              NUMBER(1),
   "VIPType"            NUMBER(9),
   "MemberType"         NUMBER(9),
   "BonusPoints"        NUMBER(9),
   "PlateOwner"         VARCHAR2(100),
   "IfMarried"          NUMBER(9),
   "WeddingDay"         DATE,
   "FamilyIncome"       NUMBER(9),
   "Location"           NUMBER(9),
   "FamilyComposition"  NUMBER(9),
   "Referral"           VARCHAR2(100),
   "Hobby"              VARCHAR2(200),
   "SpecialDay"         DATE,
   "SpecialDayDetails"  VARCHAR2(200),
   "UserGender"         NUMBER(9),
   "CompanyType"        NUMBER(9),
   "AcquisitorName"     VARCHAR2(100),
   "AcquisitorPhone"    VARCHAR2(50),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(600),
   "RowVersion"         TIMESTAMP,
   "Path"               VARCHAR2(2000),
   constraint PK_RETAINEDCUSTOMER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RetainedCustomerExtended"                            */
/*==============================================================*/
create table "RetainedCustomerExtended"  (
   "Id"                 NUMBER(9)                       not null,
   "RetainedCustomerId" NUMBER(9)                       not null,
   "TemplateId"         NUMBER(9)                       not null,
   "Content"            VARCHAR2(200),
   constraint PK_RETAINEDCUSTOMEREXTENDED primary key ("Id")
)
/

/*==============================================================*/
/* Table: "RetainedCustomerVehicleList"                         */
/*==============================================================*/
create table "RetainedCustomerVehicleList"  (
   "Id"                 NUMBER(9)                       not null,
   "RetainedCustomerId" NUMBER(9)                       not null,
   "VehicleId"          NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "Mileage"            NUMBER(9),
   "ChangeTime"         DATE,
   "ChangeOperation"    VARCHAR2(50),
   "ChangeOperationId"  NUMBER(9),
   constraint PK_RETAINEDCUSTOMERVEHICLELIST primary key ("Id")
)
/

/*==============================================================*/
/* Table: "VehicleLinkman"                                      */
/*==============================================================*/
create table "VehicleLinkman"  (
   "Id"                 NUMBER(9)                       not null,
   "RetainedCustomerId" NUMBER(9)                       not null,
   "VehicleInformationId" NUMBER(9)                       not null,
   "CustomerCode"       VARCHAR2(50)                    not null,
   "LinkmanType"        NUMBER(9)                       not null,
   "Name"               VARCHAR2(100)                   not null,
   "Gender"             NUMBER(9)                       not null,
   "CellPhoneNumber"    VARCHAR2(50),
   "RegionId"           NUMBER(9),
   "RegionName"         VARCHAR2(100),
   "ProvinceName"       VARCHAR2(100),
   "CityName"           VARCHAR2(100),
   "CountyName"         VARCHAR2(100),
   "Email"              VARCHAR2(50),
   "Address"            VARCHAR2(600),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   "IDType"             NUMBER(9),
   "IDNumer"            VARCHAR2(50),
   "Telephone"          VARCHAR2(50),
   "OtherPhone"         VARCHAR2(50),
   "JobPosition"        NUMBER(9),
   "OccupationType"     NUMBER(9),
   "AnnualInCome"       NUMBER(9),
   "Hobby"              VARCHAR2(200),
   "EducationLevel"     NUMBER(9),
   "BirthDate"          DATE,
   "Fax"                VARCHAR2(50),
   "CustomerAddressType" NUMBER(9),
   "PostCode"           VARCHAR2(6),
   constraint PK_VEHICLELINKMAN primary key ("Id")
)
/

/*==============================================================*/
/* Table: "VehicleMaintePoilcyHistroy"                          */
/*==============================================================*/
create table "VehicleMaintePoilcyHistroy"  (
   "Id"                 NUMBER(9)                       not null,
   "VehicleMaintenancePoilcyId" NUMBER(9),
   "VehicleWarrantyCardId" NUMBER(9)                       not null,
   "VehicleMainteTermId" NUMBER(9),
   "VehicleMainteTermCode" VARCHAR2(50),
   "VehicleMainteTermName" VARCHAR2(200),
   "MainteType"         NUMBER(9),
   "WorkingHours"       NUMBER(9),
   "Capacity"           NUMBER(9),
   "MaxMileage"         NUMBER(15,6),
   "MinMileage"         NUMBER(15,6),
   "DaysUpperLimit"     NUMBER(9),
   "DaysLowerLimit"     NUMBER(9),
   "IfEmployed"         NUMBER(1)                       not null,
   "DisplayOrder"       NUMBER(9),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   constraint PK_VEHICLEMAINTEPOILCYHISTROY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "VehicleMaintenancePoilcy"                            */
/*==============================================================*/
create table "VehicleMaintenancePoilcy"  (
   "Id"                 NUMBER(9)                       not null,
   "VehicleWarrantyCardId" NUMBER(9)                       not null,
   "VehicleMainteTermId" NUMBER(9),
   "VehicleMainteTermCode" VARCHAR2(50),
   "VehicleMainteTermName" VARCHAR2(200),
   "MainteType"         NUMBER(9),
   "WorkingHours"       NUMBER(9)                       not null,
   "Capacity"           NUMBER(9)                       not null,
   "MaxMileage"         NUMBER(15,6),
   "MinMileage"         NUMBER(15,6),
   "DaysUpperLimit"     NUMBER(9),
   "DaysLowerLimit"     NUMBER(9),
   "IfEmployed"         NUMBER(1)                       not null,
   "DisplayOrder"       NUMBER(9),
   constraint PK_VEHICLEMAINTENANCEPOILCY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "VehicleWarrantyCard"                                 */
/*==============================================================*/
create table "VehicleWarrantyCard"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "VehicleId"          NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   "VehicleCategoryId"  NUMBER(9)                       not null,
   "VehicleCategoryName" VARCHAR2(100)                   not null,
   "ServiceProductLineId" NUMBER(9)                       not null,
   "ServiceProductLineName" VARCHAR2(100)                   not null,
   "ProductId"          NUMBER(9)                       not null,
   "ProductCode"        VARCHAR2(50)                    not null,
   "SalesDate"          DATE,
   "WarrantyStartDate"  DATE                            not null,
   "RetainedCustomerId" NUMBER(9)                       not null,
   "CustomerName"       VARCHAR2(100)                   not null,
   "CustomerGender"     NUMBER(9)                       not null,
   "IdDocumentType"     NUMBER(9),
   "IdDocumentNumber"   VARCHAR2(50),
   "WarrantyPolicyCategory" NUMBER(9)                       not null,
   "WarrantyPolicyId"   NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_VEHICLEWARRANTYCARD primary key ("Id")
)
/

