/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2021/4/25 16:34:39                           */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"AccurateTrace"');
  if num>0 then
    execute immediate 'drop table "AccurateTrace" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"BottomStock"');
  if num>0 then
    execute immediate 'drop table "BottomStock" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"BottomStockForceReserveSub"');
  if num>0 then
    execute immediate 'drop table "BottomStockForceReserveSub" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"BottomStockForceReserveType"');
  if num>0 then
    execute immediate 'drop table "BottomStockForceReserveType" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"BottomStockSettleTable"');
  if num>0 then
    execute immediate 'drop table "BottomStockSettleTable" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"DealerPartsStock"');
  if num>0 then
    execute immediate 'drop table "DealerPartsStock" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"FullStockTask"');
  if num>0 then
    execute immediate 'drop table "FullStockTask" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"InOutSettleSummary"');
  if num>0 then
    execute immediate 'drop table "InOutSettleSummary" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"InOutWorkPerHour"');
  if num>0 then
    execute immediate 'drop table "InOutWorkPerHour" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsBatchRecord"');
  if num>0 then
    execute immediate 'drop table "PartsBatchRecord" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsLockedStock"');
  if num>0 then
    execute immediate 'drop table "PartsLockedStock" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsStandardStockBill"');
  if num>0 then
    execute immediate 'drop table "PartsStandardStockBill" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsStock"');
  if num>0 then
    execute immediate 'drop table "PartsStock" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsStockBatchDetail"');
  if num>0 then
    execute immediate 'drop table "PartsStockBatchDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsStockCoefficient"');
  if num>0 then
    execute immediate 'drop table "PartsStockCoefficient" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsStockCoefficientPrice"');
  if num>0 then
    execute immediate 'drop table "PartsStockCoefficientPrice" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsStockHistory"');
  if num>0 then
    execute immediate 'drop table "PartsStockHistory" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsSupplierStock"');
  if num>0 then
    execute immediate 'drop table "PartsSupplierStock" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsSupplierStockHistory"');
  if num>0 then
    execute immediate 'drop table "PartsSupplierStockHistory" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"Retailer_PartsStockYX_SYNC"');
  if num>0 then
    execute immediate 'drop table "Retailer_PartsStockYX_SYNC" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SYNCWMSINOUTLOGINFO');
  if num>0 then
    execute immediate 'drop table SYNCWMSINOUTLOGINFO cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SupplierLabelReport"');
  if num>0 then
    execute immediate 'drop table "SupplierLabelReport" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SupplierSpareOutBoud"');
  if num>0 then
    execute immediate 'drop table "SupplierSpareOutBoud" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"TraceTempType"');
  if num>0 then
    execute immediate 'drop table "TraceTempType" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"VehiclePartsStockLevel"');
  if num>0 then
    execute immediate 'drop table "VehiclePartsStockLevel" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"WMSLabelPrintingSerial"');
  if num>0 then
    execute immediate 'drop table "WMSLabelPrintingSerial" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"Warehouse"');
  if num>0 then
    execute immediate 'drop table "Warehouse" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"WarehouseArea"');
  if num>0 then
    execute immediate 'drop table "WarehouseArea" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"WarehouseAreaCategory"');
  if num>0 then
    execute immediate 'drop table "WarehouseAreaCategory" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"WarehouseAreaHistory"');
  if num>0 then
    execute immediate 'drop table "WarehouseAreaHistory" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"WarehouseAreaManager"');
  if num>0 then
    execute immediate 'drop table "WarehouseAreaManager" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"WarehouseOperator"');
  if num>0 then
    execute immediate 'drop table "WarehouseOperator" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"WmsCongelationStock"');
  if num>0 then
    execute immediate 'drop table "WmsCongelationStock" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"WmsCongelationStockSync"');
  if num>0 then
    execute immediate 'drop table "WmsCongelationStockSync" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"WorkGruopPersonNumber"');
  if num>0 then
    execute immediate 'drop table "WorkGruopPersonNumber" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_AccurateTrace"');
  if num>0 then
    execute immediate 'drop sequence "S_AccurateTrace"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_BottomStock"');
  if num>0 then
    execute immediate 'drop sequence "S_BottomStock"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_BottomStockForceReserveSub"');
  if num>0 then
    execute immediate 'drop sequence "S_BottomStockForceReserveSub"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_BottomStockForceReserveType"');
  if num>0 then
    execute immediate 'drop sequence "S_BottomStockForceReserveType"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_BottomStockSettleTable"');
  if num>0 then
    execute immediate 'drop sequence "S_BottomStockSettleTable"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_DealerPartsStock"');
  if num>0 then
    execute immediate 'drop sequence "S_DealerPartsStock"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_FullStockTask"');
  if num>0 then
    execute immediate 'drop sequence "S_FullStockTask"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_InOutSettleSummary"');
  if num>0 then
    execute immediate 'drop sequence "S_InOutSettleSummary"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_InOutWorkPerHour"');
  if num>0 then
    execute immediate 'drop sequence "S_InOutWorkPerHour"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsBatchRecord"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsBatchRecord"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsLockedStock"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsLockedStock"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsStandardStockBill"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsStandardStockBill"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsStock"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsStock"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsStockBatchDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsStockBatchDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsStockCoefficient"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsStockCoefficient"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsStockCoefficientPrice"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsStockCoefficientPrice"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsStockHistory"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsStockHistory"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsSupplierStock"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsSupplierStock"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsSupplierStockHistory"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsSupplierStockHistory"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SYNCWMSINOUTLOGINFO');
  if num>0 then
    execute immediate 'drop sequence S_SYNCWMSINOUTLOGINFO';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SupplierLabelReport"');
  if num>0 then
    execute immediate 'drop sequence "S_SupplierLabelReport"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SupplierSpareOutBoud"');
  if num>0 then
    execute immediate 'drop sequence "S_SupplierSpareOutBoud"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_TraceTempType"');
  if num>0 then
    execute immediate 'drop sequence "S_TraceTempType"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_VehiclePartsStockLevel"');
  if num>0 then
    execute immediate 'drop sequence "S_VehiclePartsStockLevel"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_WMSLabelPrintingSerial"');
  if num>0 then
    execute immediate 'drop sequence "S_WMSLabelPrintingSerial"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_Warehouse"');
  if num>0 then
    execute immediate 'drop sequence "S_Warehouse"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_WarehouseArea"');
  if num>0 then
    execute immediate 'drop sequence "S_WarehouseArea"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_WarehouseAreaCategory"');
  if num>0 then
    execute immediate 'drop sequence "S_WarehouseAreaCategory"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_WarehouseAreaHistory"');
  if num>0 then
    execute immediate 'drop sequence "S_WarehouseAreaHistory"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_WarehouseAreaManager"');
  if num>0 then
    execute immediate 'drop sequence "S_WarehouseAreaManager"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_WarehouseOperator"');
  if num>0 then
    execute immediate 'drop sequence "S_WarehouseOperator"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_WmsCongelationStock"');
  if num>0 then
    execute immediate 'drop sequence "S_WmsCongelationStock"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_WmsCongelationStockSync"');
  if num>0 then
    execute immediate 'drop sequence "S_WmsCongelationStockSync"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_WorkGruopPersonNumber"');
  if num>0 then
    execute immediate 'drop sequence "S_WorkGruopPersonNumber"';
  end if;
end;
/

create sequence "S_AccurateTrace"
/

create sequence "S_BottomStock"
/

create sequence "S_BottomStockForceReserveSub"
/

create sequence "S_BottomStockForceReserveType"
/

create sequence "S_BottomStockSettleTable"
/

create sequence "S_DealerPartsStock"
/

create sequence "S_FullStockTask"
/

create sequence "S_InOutSettleSummary"
/

create sequence "S_InOutWorkPerHour"
/

create sequence "S_PartsBatchRecord"
/

create sequence "S_PartsLockedStock"
/

create sequence "S_PartsStandardStockBill"
/

create sequence "S_PartsStock"
/

create sequence "S_PartsStockBatchDetail"
/

create sequence "S_PartsStockCoefficient"
/

create sequence "S_PartsStockCoefficientPrice"
/

create sequence "S_PartsStockHistory"
/

create sequence "S_PartsSupplierStock"
/

create sequence "S_PartsSupplierStockHistory"
/

create sequence S_SYNCWMSINOUTLOGINFO
/

create sequence "S_SupplierLabelReport"
/

create sequence "S_SupplierSpareOutBoud"
/

create sequence "S_TraceTempType"
/

create sequence "S_VehiclePartsStockLevel"
/

create sequence "S_WMSLabelPrintingSerial"
/

create sequence "S_Warehouse"
/

create sequence "S_WarehouseArea"
/

create sequence "S_WarehouseAreaCategory"
/

create sequence "S_WarehouseAreaHistory"
/

create sequence "S_WarehouseAreaManager"
/

create sequence "S_WarehouseOperator"
/

create sequence "S_WmsCongelationStock"
/

create sequence "S_WmsCongelationStockSync"
/

create sequence "S_WorkGruopPersonNumber"
/

/*==============================================================*/
/* Table: "AccurateTrace"                                       */
/*==============================================================*/
create table "AccurateTrace"  (
   "Id"                 NUMBER(9)                       not null,
   "Type"               NUMBER(9),
   "CompanyType"        NUMBER(9),
   "CompanyId"          NUMBER(9),
   "SourceBillId"       NUMBER(9),
   "PartId"             NUMBER(9),
   "TraceCode"          VARCHAR2(50),
   "SIHLabelCode"       VARCHAR2(50),
   "Status"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   "TraceProperty"      NUMBER(9),
   "BoxCode"            VARCHAR2(50),
   "InQty"              NUMBER(9),
   "OutQty"             NUMBER(9),
   "PackingTaskId"      NUMBER(9),
   "PartsStockId"       NUMBER(9),
   "WarehouseAreaId"    NUMBER(9),
   "WarehouseAreaCode"  VARCHAR2(50),
   constraint PK_ACCURATETRACE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "BottomStock"                                         */
/*==============================================================*/
create table "BottomStock"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryName" VARCHAR2(100),
   "CompanyID"          NUMBER(9)                       not null,
   "CompanyCode"        VARCHAR2(50),
   "CompanyName"        VARCHAR2(100),
   "CompanyType"        NUMBER(9),
   "WarehouseID"        NUMBER(9),
   "WarehouseCode"      VARCHAR2(50),
   "WarehouseName"      VARCHAR2(100),
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50),
   "SparePartName"      VARCHAR2(100),
   "StockQty"           NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "Remark"             VARCHAR2(200),
   "Status"             NUMBER(9),
   constraint PK_BOTTOMSTOCK primary key ("Id")
)
/

/*==============================================================*/
/* Table: "BottomStockForceReserveSub"                          */
/*==============================================================*/
create table "BottomStockForceReserveSub"  (
   "Id"                 NUMBER(9)                       not null,
   "ReserveType"        VARCHAR2(100),
   "SubCode"            VARCHAR2(50),
   "SubName"            VARCHAR2(100),
   "Serial"             NUMBER(9),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_BOTTOMSTOCKFORCERESERVESUB primary key ("Id")
)
/

/*==============================================================*/
/* Table: "BottomStockForceReserveType"                         */
/*==============================================================*/
create table "BottomStockForceReserveType"  (
   "Id"                 NUMBER(9)                       not null,
   "ReserveType"        VARCHAR2(100),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "ReserveName"        VARCHAR2(100),
   constraint PK_BOTTOMSTOCKFORCERESERVETYPE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "BottomStockSettleTable"                              */
/*==============================================================*/
create table "BottomStockSettleTable"  (
   "Id"                 NUMBER(9)                       not null,
   "CreateTime"         DATE,
   "DistributionCenterCode" VARCHAR2(50),
   "DistributionCenterName" VARCHAR2(100),
   "CenterCode"         VARCHAR2(50),
   "CenterName"         VARCHAR2(100),
   "DealerCode"         VARCHAR2(50),
   "DealerName"         VARCHAR2(100),
   "AccountSpeciesNum"  NUMBER(9),
   "AccountFee"         NUMBER(19,4),
   "ActualSpeciesNum"   NUMBER(9),
   "ActualFee"          NUMBER(19,4),
   "DifferSpeciesNum"   NUMBER(9),
   constraint PK_BOTTOMSTOCKSETTLETABLE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "DealerPartsStock"                                    */
/*==============================================================*/
create table "DealerPartsStock"  (
   "Id"                 NUMBER(9)                       not null,
   "DealerId"           NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(50)                    not null,
   "DealerName"         VARCHAR2(100)                   not null,
   "SubDealerId"        NUMBER(9),
   "BranchId"           NUMBER(9)                       not null,
   "SalesCategoryId"    NUMBER(9)                       not null,
   "SalesCategoryName"  VARCHAR2(100)                   not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "Quantity"           NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_DEALERPARTSSTOCK primary key ("Id")
)
/

/*==============================================================*/
/* Table: "FullStockTask"                                       */
/*==============================================================*/
create table "FullStockTask"  (
   "Id"                 NUMBER(9)                       not null,
   "SyncTime"           VARCHAR2(100),
   "CreatorId"          NUMBER(9),
   "CreateTime"         DATE,
   "CreatorName"        VARCHAR2(100),
   constraint PK_FULLSTOCKTASK primary key ("Id")
)
/

/*==============================================================*/
/* Table: "InOutSettleSummary"                                  */
/*==============================================================*/
create table "InOutSettleSummary"  (
   "Id"                 NUMBER(9)                       not null,
   "WarehouseId"        NUMBER(9),
   "RecordTime"         DATE,
   "InPurchaseList"     VARCHAR2(100),
   "InReturnList"       VARCHAR2(100),
   "InPurchaseQty"      NUMBER(9),
   "InReturnQty"        NUMBER(9),
   "InPurchaseFee"      NUMBER(19,4),
   "InReturnFee"        NUMBER(19,4),
   "WaitInPurchaseList" VARCHAR2(100),
   "WaitInLineList"     VARCHAR2(100),
   "WaitInArriveFee"    NUMBER(19,4),
   "WaitInLineFee"      NUMBER(19,4),
   "PackingPurchaseList" VARCHAR2(100),
   "PackingReturnList"  VARCHAR2(100),
   "PackingPurchaseQty" NUMBER(9),
   "PackingReturnQty"   NUMBER(9),
   "PackingPurchaseFee" NUMBER(19,4),
   "PackingReturnFee"   NUMBER(19,4),
   "WaitPackPurchaseList" VARCHAR2(100),
   "WaitPackReturnList" VARCHAR2(100),
   "WaitPackPurchaseFee" NUMBER(19,4),
   "WaitPackReturnFee"  NUMBER(19,4),
   "ShelvesPurchaseList" VARCHAR2(100),
   "ShelvesReturnList"  VARCHAR2(100),
   "ShelvesPurchaseQty" NUMBER(9),
   "ShelvesReturnQty"   NUMBER(9),
   "ShelvesPurchaseFee" NUMBER(19,4),
   "ShelvesReturnFee"   NUMBER(19,4),
   "WaitShelvesPurchaseLis" VARCHAR2(100),
   "WaitShelvesReturnList" VARCHAR2(100),
   "WaitShelvesPurchaseFee" NUMBER(19,4),
   "WaitShelvesReturnFee" NUMBER(19,4),
   "SaleApproveList"    VARCHAR2(100),
   "SaleApproveQty"     NUMBER(9),
   "SaleApproveFee"     NUMBER(19,4),
   "PickingList"        VARCHAR2(100),
   "PickingQty"         NUMBER(9),
   "PickingFee"         NUMBER(19,4),
   "OutList"            VARCHAR2(100),
   "OutQty"             NUMBER(9),
   "OutFee"             NUMBER(19,4),
   "WaitOutList"        VARCHAR2(100),
   "WaitOutQty"         NUMBER(9),
   "WaitOutFee"         NUMBER(19,4),
   "ShippingFee"        NUMBER(19,4),
   "ShippingWeight"     NUMBER(15,6),
   constraint PK_INOUTSETTLESUMMARY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "InOutWorkPerHour"                                    */
/*==============================================================*/
create table "InOutWorkPerHour"  (
   "Id"                 NUMBER(9)                       not null,
   "Dates"              DATE,
   "TimeFrame"          VARCHAR2(50),
   "InPersonNum"        NUMBER(9),
   "InOrderLineNum"     NUMBER(9),
   "InOrderItemNum"     NUMBER(9),
   "InQty"              NUMBER(9),
   "InFee"              NUMBER(19,4),
   "PackingPersonNum"   NUMBER(9),
   "PackingLineNum"     NUMBER(9),
   "PackingItemNum"     NUMBER(9),
   "PackingQty"         NUMBER(9),
   "PackingFee"         NUMBER(19,4),
   "ShelvePersonNum"    NUMBER(9),
   "ShelveLineNum"      NUMBER(9),
   "ShelveItemNum"      NUMBER(9),
   "ShelveQty"          NUMBER(9),
   "ShelveFee"          NUMBER(19,4),
   "UnShelvePersonNum"  NUMBER(9),
   "UnShelveLineNum"    NUMBER(9),
   "UnShelveItemNum"    NUMBER(9),
   "UnShelveQty"        NUMBER(9),
   "UnShelveFee"        NUMBER(19,4),
   "CreateTime"         DATE,
   "OutPersonNum"       NUMBER(9),
   "OutOrderLineNum"    NUMBER(9),
   "OutOrderItemNum"    NUMBER(9),
   "OutQty"             NUMBER(9),
   "OutFee"             NUMBER(19,4),
   constraint PK_INOUTWORKPERHOUR primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsBatchRecord"                                    */
/*==============================================================*/
create table "PartsBatchRecord"  (
   "Id"                 NUMBER(9)                       not null,
   "PartId"             NUMBER(9)                       not null,
   "Quantity"           NUMBER(9)                       not null,
   "BatchNumber"        VARCHAR2(100)                   not null,
   "Status"             NUMBER(9)                       not null,
   "OriginalBatchNumber" VARCHAR2(100),
   "CreatorId"          NUMBER(9)                       not null,
   "CreatorName"        VARCHAR2(100)                   not null,
   "CreatorOwnerCompanyId" NUMBER(9)                       not null,
   "CreatorOwnerCompanyCode" VARCHAR2(50)                    not null,
   "CreateTime"         DATE                            not null,
   constraint PK_PARTSBATCHRECORD primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsLockedStock"                                    */
/*==============================================================*/
create table "PartsLockedStock"  (
   "Id"                 NUMBER(9)                       not null,
   "WarehouseId"        NUMBER(9)                       not null,
   "StorageCompanyId"   NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "PartId"             NUMBER(9)                       not null,
   "LockedQuantity"     NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_PARTSLOCKEDSTOCK primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsStandardStockBill"                              */
/*==============================================================*/
create table "PartsStandardStockBill"  (
   "Id"                 NUMBER(9)                       not null,
   "PartId"             NUMBER(9),
   "PartCode"           VARCHAR2(50),
   "PartName"           VARCHAR2(100),
   "StandardQty"        NUMBER(9),
   "DailySaleNum"       NUMBER(9,2),
   "QtyLowerLimit"      NUMBER(9),
   "CreateTime"         DATE,
   "ModifyTime"         DATE,
   constraint PK_PARTSSTANDARDSTOCKBILL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsStock"                                          */
/*==============================================================*/
create table "PartsStock"  (
   "Id"                 NUMBER(9)                       not null,
   "WarehouseId"        NUMBER(9)                       not null,
   "StorageCompanyId"   NUMBER(9)                       not null,
   "StorageCompanyType" NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "WarehouseAreaId"    NUMBER(9)                       not null,
   "WarehouseAreaCategoryId" NUMBER(9),
   "PartId"             NUMBER(9)                       not null,
   "Quantity"           NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   "LockedQty"          NUMBER(9),
   "OutingQty"          NUMBER(9),
   constraint PK_PARTSSTOCK primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsStockBatchDetail"                               */
/*==============================================================*/
create table "PartsStockBatchDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsStockId"       NUMBER(9)                       not null,
   "WarehouseId"        NUMBER(9)                       not null,
   "StorageCompanyId"   NUMBER(9)                       not null,
   "StorageCompanyType" NUMBER(9)                       not null,
   "PartId"             NUMBER(9)                       not null,
   "BatchNumber"        VARCHAR2(100)                   not null,
   "Quantity"           NUMBER(9)                       not null,
   "InboundTime"        DATE                            not null,
   constraint PK_PARTSSTOCKBATCHDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsStockCoefficient"                               */
/*==============================================================*/
create table "PartsStockCoefficient"  (
   "Id"                 NUMBER(9)                       not null,
   "Type"               NUMBER(9)                       not null,
   "ProductType"        NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_PARTSSTOCKCOEFFICIENT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsStockCoefficientPrice"                          */
/*==============================================================*/
create table "PartsStockCoefficientPrice"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsStockCoefficientId" NUMBER(9)                       not null,
   "MinPrice"           NUMBER(19,4),
   "MaxPrice"           NUMBER(19,4),
   "Coefficient"        NUMBER(15,6),
   constraint PK_PARTSSTOCKCOEFFICIENTPRICE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsStockHistory"                                   */
/*==============================================================*/
create table "PartsStockHistory"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsStockId"       NUMBER(9)                       not null,
   "WarehouseId"        NUMBER(9)                       not null,
   "StorageCompanyId"   NUMBER(9)                       not null,
   "StorageCompanyType" NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "WarehouseAreaId"    NUMBER(9)                       not null,
   "WarehouseAreaCategoryId" NUMBER(9),
   "PartId"             NUMBER(9)                       not null,
   "Quantity"           NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "LockedQty"          NUMBER(9),
   "OutingQty"          NUMBER(9),
   "OldQuantity"        NUMBER(9),
   constraint PK_PARTSSTOCKHISTORY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsSupplierStock"                                  */
/*==============================================================*/
create table "PartsSupplierStock"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSupplierId"    NUMBER(9)                       not null,
   "PartsSupplierCode"  VARCHAR2(100)                   not null,
   "PartId"             NUMBER(9)                       not null,
   "Quantity"           NUMBER(9)                       not null,
   "LockedQty"          NUMBER(9),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_PARTSSUPPLIERSTOCK primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsSupplierStockHistory"                           */
/*==============================================================*/
create table "PartsSupplierStockHistory"  (
   "Id"                 NUMBER(9)                       not null,
   "PartId"             NUMBER(9)                       not null,
   "LockedQty"          NUMBER(9),
   "ChangedQuantity"    NUMBER(9),
   "OldQuantity"        NUMBER(9),
   "Quantity"           NUMBER(9),
   "PartsStockId"       NUMBER(9)                       not null,
   "StorageCompanyId"   NUMBER(9)                       not null,
   "ChageCode"          VARCHAR2(200),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   constraint PK_PARTSSUPPLIERSTOCKHISTORY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "Retailer_PartsStockYX_SYNC"                          */
/*==============================================================*/
create table "Retailer_PartsStockYX_SYNC"  (
   SYNCNUMBER           NUMBER(9)                       not null,
   BUSSINESSID          NUMBER(9),
   BUSSINESSTABLENAME   VARCHAR2(100),
   SENDTIME             DATE,
   constraint PK_RETAILER_PARTSSTOCKYX_SYNC primary key (SYNCNUMBER)
)
/

/*==============================================================*/
/* Table: SYNCWMSINOUTLOGINFO                                   */
/*==============================================================*/
create table SYNCWMSINOUTLOGINFO  (
   "Objid"              NUMBER(9)                       not null,
   "SYNCDate"           DATE                            not null,
   "Code"               VARCHAR2(1000)                  not null,
   "SYNCType"           NUMBER(9)                       not null,
   "SYNCCode"           VARCHAR2(25)                    not null,
   "InOutCode"          VARCHAR2(25),
   "Status"             NUMBER(9)                       not null,
   constraint PK_SYNCWMSINOUTLOGINFO primary key ("Objid")
)
/

/*==============================================================*/
/* Table: "SupplierLabelReport"                                 */
/*==============================================================*/
create table "SupplierLabelReport"  (
   "Id"                 NUMBER(9)                       not null,
   "DataMark"           VARCHAR2(100),
   "Year"               VARCHAR2(100),
   "Month"              VARCHAR2(100),
   "PartsSupplierId"    NUMBER(9)                       not null,
   "PartsSupplierCode"  VARCHAR2(100)                   not null,
   "PartsSupplierName"  VARCHAR2(100),
   "Label"              NUMBER(9),
   "OutQty"             NUMBER(9),
   "NeedQty"            NUMBER(9),
   "UpQty"              NUMBER(9),
   "SuggQty"            NUMBER(9),
   "PartId"             NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50),
   "SparePartName"      VARCHAR2(100),
   "SupplierPartCode"   VARCHAR2(50),
   "CreateTime"         DATE,
   constraint PK_SUPPLIERLABELREPORT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SupplierSpareOutBoud"                                */
/*==============================================================*/
create table "SupplierSpareOutBoud"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSupplierId"    NUMBER(9)                       not null,
   "PartId"             NUMBER(9)                       not null,
   "Quantity"           NUMBER(9)                       not null,
   "ShippingOrderCode"  VARCHAR2(200),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   constraint PK_SUPPLIERSPAREOUTBOUD primary key ("Id")
)
/

/*==============================================================*/
/* Table: "TraceTempType"                                       */
/*==============================================================*/
create table "TraceTempType"  (
   "Id"                 NUMBER(9)                       not null,
   "Type"               NUMBER(9),
   "SourceBillId"       NUMBER(9),
   "SourceBillDetailId" NUMBER(9),
   "PartId"             NUMBER(9),
   "TraceCode"          VARCHAR2(50),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   "Status"             NUMBER(9),
   "TraceProperty"      NUMBER(9),
   "SIHLabelCode"       VARCHAR2(100),
   "BoxCode"            VARCHAR2(100),
   "Quantity"           NUMBER(9),
   "WarehouseAreaId"    NUMBER(9),
   "WarehouseAreaCode"  VARCHAR2(50),
   "InboundQty"         NUMBER(9),
   constraint PK_TRACETEMPTYPE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "VehiclePartsStockLevel"                              */
/*==============================================================*/
create table "VehiclePartsStockLevel"  (
   "Id"                 NUMBER(9)                       not null,
   "BrandId"            NUMBER(9)                       not null,
   "BrandCode"          VARCHAR2(50)                    not null,
   "BrandName"          VARCHAR2(100)                   not null,
   "WarehouseId"        NUMBER(9)                       not null,
   "WarehouseCode"      VARCHAR2(50)                    not null,
   "WarehouseName"      VARCHAR2(100)                   not null,
   "PartId"             NUMBER(9)                       not null,
   "PartCode"           VARCHAR2(50)                    not null,
   "PartName"           VARCHAR2(100)                   not null,
   "StockMaximum"       NUMBER(9),
   "StockMinimum"       NUMBER(9),
   "SafeStock"          NUMBER(9),
   "Status"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   constraint PK_VEHICLEPARTSSTOCKLEVEL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "WMSLabelPrintingSerial"                              */
/*==============================================================*/
create table "WMSLabelPrintingSerial"  (
   "Id"                 NUMBER(9)                       not null,
   "PartId"             NUMBER(9),
   "PartCode"           VARCHAR2(50),
   "PartName"           VARCHAR2(100),
   "Serial"             VARCHAR2(50),
   "CreateCompanyId"    NUMBER(9),
   "CreateCompanyCode"  VARCHAR2(50),
   "CreateCompanyName"  VARCHAR2(100),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   constraint PK_WMSLABELPRINTINGSERIAL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "Warehouse"                                           */
/*==============================================================*/
create table "Warehouse"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Name"               VARCHAR2(100)                   not null,
   "Type"               NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "Address"            VARCHAR2(200),
   "RegionId"           NUMBER(9),
   "PhoneNumber"        VARCHAR2(50),
   "Contact"            VARCHAR2(50),
   "Fax"                VARCHAR2(50),
   "Email"              VARCHAR2(50),
   "StorageStrategy"    NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "Picture"            VARCHAR2(200),
   "BranchId"           NUMBER(9)                       not null,
   "StorageCompanyId"   NUMBER(9)                       not null,
   "StorageCompanyType" NUMBER(9)                       not null,
   "StorageCenter"      NUMBER(9),
   "WmsInterface"       NUMBER(1)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "IsCentralizedPurchase" NUMBER(1),
   "RowVersion"         TIMESTAMP,
   "IsQualityWarehouse" NUMBER(1),
   "IfSync"             NUMBER(1)                      default 0,
   "IsEcommerceWarehouse" NUMBER(9),
   "PwmsInterface"      NUMBER(1),
   "IsSales"            NUMBER(1),
   "IsPurchase"         NUMBER(1),
   "IsAutoApp"          NUMBER(1),
   "PackReceivingWarehouseId" NUMBER(9),
   "PackReceivingWarehouseName" VARCHAR2(100),
   constraint PK_WAREHOUSE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "WarehouseArea"                                       */
/*==============================================================*/
create table "WarehouseArea"  (
   "Id"                 NUMBER(9)                       not null,
   "WarehouseId"        NUMBER(9)                       not null,
   "ParentId"           NUMBER(9),
   "Code"               VARCHAR2(50)                    not null,
   "TopLevelWarehouseAreaId" NUMBER(9),
   "AreaCategoryId"     NUMBER(9),
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "AreaKind"           NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_WAREHOUSEAREA primary key ("Id")
)
/

/*==============================================================*/
/* Table: "WarehouseAreaCategory"                               */
/*==============================================================*/
create table "WarehouseAreaCategory"  (
   "Id"                 NUMBER(9)                       not null,
   "Category"           NUMBER(9)                       not null,
   constraint PK_WAREHOUSEAREACATEGORY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "WarehouseAreaHistory"                                */
/*==============================================================*/
create table "WarehouseAreaHistory"  (
   "Id"                 NUMBER(9)                       not null,
   "WarehouseId"        NUMBER(9)                       not null,
   "StorageCompanyId"   NUMBER(9)                       not null,
   "StorageCompanyType" NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "DestWarehouseAreaId" NUMBER(9),
   "DestWarehouseAreaCategoryId" NUMBER(9),
   "WarehouseAreaId"    NUMBER(9)                       not null,
   "WarehouseAreaCategoryId" NUMBER(9),
   "PartId"             NUMBER(9)                       not null,
   "Quantity"           NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_WAREHOUSEAREAHISTORY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "WarehouseAreaManager"                                */
/*==============================================================*/
create table "WarehouseAreaManager"  (
   "Id"                 NUMBER(9)                       not null,
   "WarehouseAreaId"    NUMBER(9)                       not null,
   "ManagerId"          NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_WAREHOUSEAREAMANAGER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "WarehouseOperator"                                   */
/*==============================================================*/
create table "WarehouseOperator"  (
   "Id"                 NUMBER(9)                       not null,
   "WarehouseId"        NUMBER(9)                       not null,
   "OperatorId"         NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_WAREHOUSEOPERATOR primary key ("Id")
)
/

/*==============================================================*/
/* Table: "WmsCongelationStock"                                 */
/*==============================================================*/
create table "WmsCongelationStock"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "BranchCode"         VARCHAR2(50)                    not null,
   "BranchName"         VARCHAR2(100)                   not null,
   "StorageCenter"      NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryCode" VARCHAR2(50)                    not null,
   "PartsSalesCategoryName" VARCHAR2(100)                   not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "CongelationStockQty" NUMBER(9)                       not null,
   "DisabledStock"      NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_WMSCONGELATIONSTOCK primary key ("Id")
)
/

/*==============================================================*/
/* Table: "WmsCongelationStockSync"                             */
/*==============================================================*/
create table "WmsCongelationStockSync"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchName"         VARCHAR2(100)                   not null,
   "StorageCenter"      VARCHAR2(50)                    not null,
   "PartsSalesCategoryName" VARCHAR2(100)                   not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "Direction"          VARCHAR2(50)                    not null,
   "CongelationStockQty" NUMBER(9)                       not null,
   "DisabledStock"      NUMBER(9)                       not null,
   constraint PK_WMSCONGELATIONSTOCKSYNC primary key ("Id")
)
/

/*==============================================================*/
/* Table: "WorkGruopPersonNumber"                               */
/*==============================================================*/
create table "WorkGruopPersonNumber"  (
   "Id"                 NUMBER(9)                       not null,
   "WorkGruop"          NUMBER(9),
   "EmployeeNumber"     NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_WORKGRUOPPERSONNUMBER primary key ("Id")
)
/

