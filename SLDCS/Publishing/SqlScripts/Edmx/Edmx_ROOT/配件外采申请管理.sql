/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2020-05-26 16:36:41                          */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsOuterPurchaseChange"');
  if num>0 then
    execute immediate 'drop table "PartsOuterPurchaseChange" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsOuterPurchaselist"');
  if num>0 then
    execute immediate 'drop table "PartsOuterPurchaselist" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsOuterPurchaseChange"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsOuterPurchaseChange"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsOuterPurchaselist"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsOuterPurchaselist"';
  end if;
end;
/

create sequence "S_PartsOuterPurchaseChange"
/

create sequence "S_PartsOuterPurchaselist"
/

/*==============================================================*/
/* Table: "PartsOuterPurchaseChange"                            */
/*==============================================================*/
create table "PartsOuterPurchaseChange"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "BranchId"           NUMBER(9)                       not null,
   "BranchName"         VARCHAR2(100)                   not null,
   "SourceId"           NUMBER(9),
   "SourceCode"         VARCHAR2(50),
   "SourceCategoryr"    NUMBER(9),
   "PartsSalesCategoryrId" NUMBER(9)                       not null,
   "PartsSalesCategoryName" VARCHAR2(100)                   not null,
   "CustomerCompanyId"  NUMBER(9)                       not null,
   "CustomerCompanyCode" VARCHAR2(50)                    not null,
   "CustomerCompanyNace" VARCHAR2(100)                   not null,
   "Amount"             NUMBER(19,4)                    not null,
   "Remark"             VARCHAR2(200),
   "OuterPurchaseComment" NUMBER(9)                       not null,
   "ApproveComment"     VARCHAR2(200),
   "AbandonComment"     VARCHAR2(200),
   "RejectComment"      VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "SubmitterId"        NUMBER(9),
   "SubmitterName"      VARCHAR2(100),
   "SubmitTime"         DATE,
   "InitialApproverId"  NUMBER(9),
   "InitialApproverName" VARCHAR2(100),
   "InitialApproveTime" DATE,
   "CheckerId"          NUMBER(9),
   "CheckerName"        VARCHAR2(100),
   "CheckTime"          DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "RowVersion"         TIMESTAMP,
   "RejecterId"         NUMBER(9),
   "RejectTime"         DATE,
   "RejecterName"       VARCHAR2(100),
   "ReceivingWarehouseId" NUMBER(9),
   "ReceivingWarehouseName" VARCHAR2(50),
   "Path"               VARCHAR2(2000),
   "ConfirmID"          NUMBER(9),
   "ConfirmName"        VARCHAR2(100),
   "ConfirmTime"        DATE,
   "AdvancedAuditID"    NUMBER(9),
   "AdvancedAuditName"  VARCHAR2(100),
   "AdvancedAuditTime"  DATE,
   "SeniorApproveID"    NUMBER(9),
   "SeniorApproveName"  VARCHAR2(100),
   "SeniorApproveTime"  DATE,
   constraint PK_PARTSOUTERPURCHASECHANGE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsOuterPurchaselist"                              */
/*==============================================================*/
create table "PartsOuterPurchaselist"  (
   "id"                 NUMBER(9)                       not null,
   "SerialNumber"       NUMBER(9)                       not null,
   "PartsOuterPurchaseChangeId" NUMBER(9)                       not null,
   "PartsId"            NUMBER(9)                       not null,
   "PartsCode"          VARCHAR2(50)                    not null,
   "PartsName"          VARCHAR2(100)                   not null,
   "OuterPurchasePrice" NUMBER(19,4)                    not null,
   "TradePrice"         NUMBER(19,4)                    not null,
   "Quantity"           NUMBER(9)                       not null,
   "Supplier"           VARCHAR2(100),
   constraint PK_PARTSOUTERPURCHASELIST primary key ("id")
)
/

