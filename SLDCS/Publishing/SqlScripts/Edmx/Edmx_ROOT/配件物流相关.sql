/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2021/7/8 9:43:10                             */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"APartsShippingOrderDetail"');
  if num>0 then
    execute immediate 'drop table "APartsShippingOrderDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"AgencyLogisticCompany"');
  if num>0 then
    execute immediate 'drop table "AgencyLogisticCompany" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"AgencyPartsShippingOrder"');
  if num>0 then
    execute immediate 'drop table "AgencyPartsShippingOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"AgencyPartsShippingOrderRef"');
  if num>0 then
    execute immediate 'drop table "AgencyPartsShippingOrderRef" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"EcommerceFreightDisposal"');
  if num>0 then
    execute immediate 'drop table "EcommerceFreightDisposal" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"Express"');
  if num>0 then
    execute immediate 'drop table "Express" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ExpressToLogistics"');
  if num>0 then
    execute immediate 'drop table "ExpressToLogistics" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"HauDistanceInfor"');
  if num>0 then
    execute immediate 'drop table "HauDistanceInfor" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"Logistics"');
  if num>0 then
    execute immediate 'drop table "Logistics" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"LogisticsDetail"');
  if num>0 then
    execute immediate 'drop table "LogisticsDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"LogisticsTime"');
  if num>0 then
    execute immediate 'drop table "LogisticsTime" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"LogisticsTracking"');
  if num>0 then
    execute immediate 'drop table "LogisticsTracking" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"LogisticsTrackingDetail"');
  if num>0 then
    execute immediate 'drop table "LogisticsTrackingDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsShippingOrder"');
  if num>0 then
    execute immediate 'drop table "PartsShippingOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsShippingOrderDetail"');
  if num>0 then
    execute immediate 'drop table "PartsShippingOrderDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsShippingOrderRef"');
  if num>0 then
    execute immediate 'drop table "PartsShippingOrderRef" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ReturnFreightDisposal"');
  if num>0 then
    execute immediate 'drop table "ReturnFreightDisposal" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_APartsShippingOrderDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_APartsShippingOrderDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_AgencyLogisticCompany"');
  if num>0 then
    execute immediate 'drop sequence "S_AgencyLogisticCompany"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_AgencyPartsShippingOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_AgencyPartsShippingOrder"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_AgencyPartsShippingOrderRef"');
  if num>0 then
    execute immediate 'drop sequence "S_AgencyPartsShippingOrderRef"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_EcommerceFreightDisposal"');
  if num>0 then
    execute immediate 'drop sequence "S_EcommerceFreightDisposal"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_Express"');
  if num>0 then
    execute immediate 'drop sequence "S_Express"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ExpressToLogistics"');
  if num>0 then
    execute immediate 'drop sequence "S_ExpressToLogistics"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_HauDistanceInfor"');
  if num>0 then
    execute immediate 'drop sequence "S_HauDistanceInfor"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_Logistics"');
  if num>0 then
    execute immediate 'drop sequence "S_Logistics"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_LogisticsDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_LogisticsDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_LogisticsTime"');
  if num>0 then
    execute immediate 'drop sequence "S_LogisticsTime"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_LogisticsTracking"');
  if num>0 then
    execute immediate 'drop sequence "S_LogisticsTracking"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_LogisticsTrackingDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_LogisticsTrackingDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsShippingOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsShippingOrder"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsShippingOrderDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsShippingOrderDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsShippingOrderRef"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsShippingOrderRef"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ReturnFreightDisposal"');
  if num>0 then
    execute immediate 'drop sequence "S_ReturnFreightDisposal"';
  end if;
end;
/

create sequence "S_APartsShippingOrderDetail"
/

create sequence "S_AgencyLogisticCompany"
/

create sequence "S_AgencyPartsShippingOrder"
/

create sequence "S_AgencyPartsShippingOrderRef"
/

create sequence "S_EcommerceFreightDisposal"
/

create sequence "S_Express"
/

create sequence "S_ExpressToLogistics"
/

create sequence "S_HauDistanceInfor"
/

create sequence "S_Logistics"
/

create sequence "S_LogisticsDetail"
/

create sequence "S_LogisticsTime"
/

create sequence "S_LogisticsTracking"
/

create sequence "S_LogisticsTrackingDetail"
/

create sequence "S_PartsShippingOrder"
/

create sequence "S_PartsShippingOrderDetail"
/

create sequence "S_PartsShippingOrderRef"
/

create sequence "S_ReturnFreightDisposal"
/

/*==============================================================*/
/* Table: "APartsShippingOrderDetail"                           */
/*==============================================================*/
create table "APartsShippingOrderDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsShippingOrderId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "ShippingAmount"     NUMBER(9)                       not null,
   "ConfirmedAmount"    NUMBER(9),
   "DifferenceClassification" NUMBER(9),
   "TransportLossesDisposeMethod" NUMBER(9),
   "InTransitDamageLossAmount" NUMBER(9),
   "SettlementPrice"    NUMBER(19,4)                    not null,
   "Remark"             VARCHAR2(200),
   constraint PK_APARTSSHIPPINGORDERDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "AgencyLogisticCompany"                               */
/*==============================================================*/
create table "AgencyLogisticCompany"  (
   "Id"                 NUMBER(9)                       not null,
   "AgencyId"           NUMBER(9)                       not null,
   "Code"               VARCHAR2(50),
   "Name"               VARCHAR2(100),
   "Status"             NUMBER(9),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonerTime"      DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_AGENCYLOGISTICCOMPANY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "AgencyPartsShippingOrder"                            */
/*==============================================================*/
create table "AgencyPartsShippingOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Type"               NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9),
   "ShippingCompanyId"  NUMBER(9)                       not null,
   "ShippingCompanyCode" VARCHAR2(50)                    not null,
   "ShippingCompanyName" VARCHAR2(100)                   not null,
   "SettlementCompanyId" NUMBER(9)                       not null,
   "SettlementCompanyCode" VARCHAR2(50)                    not null,
   "SettlementCompanyName" VARCHAR2(100)                   not null,
   "ReceivingCompanyId" NUMBER(9)                       not null,
   "ReceivingCompanyCode" VARCHAR2(50)                    not null,
   "InvoiceReceiveSaleCateId" NUMBER(9),
   "InvoiceReceiveSaleCateName" VARCHAR2(50),
   "ReceivingCompanyName" VARCHAR2(100)                   not null,
   "WarehouseId"        NUMBER(9),
   "WarehouseCode"      VARCHAR2(50),
   "WarehouseName"      VARCHAR2(100),
   "ReceivingWarehouseId" NUMBER(9),
   "ReceivingWarehouseCode" VARCHAR2(50),
   "ReceivingWarehouseName" VARCHAR2(100),
   "ReceivingAddress"   VARCHAR2(200),
   "LogisticCompanyId"  NUMBER(9),
   "LogisticCompanyCode" VARCHAR2(50),
   "LogisticCompanyName" VARCHAR2(100),
   "OriginalRequirementBillId" NUMBER(9),
   "OriginalRequirementBillCode" VARCHAR2(50),
   "OriginalRequirementBillType" NUMBER(9),
   "IsTransportLosses"  NUMBER(1),
   "TransportLossesDisposeStatus" NUMBER(9),
   "ShippingMethod"     NUMBER(9),
   "LogisticArrivalDate" DATE,
   "RequestedArrivalDate" DATE,
   "ShippingDate"       DATE,
   "Weight"             NUMBER(15,6),
   "Volume"             NUMBER(15,6),
   "BillingMethod"      NUMBER(9),
   "TransportCostRate"  NUMBER(9),
   "TransportCost"      NUMBER(19,4),
   "TransportCostSettleStatus" NUMBER(1),
   "InsuranceRate"      NUMBER(9),
   "InsuranceFee"       NUMBER(19,4),
   "TransportMileage"   NUMBER(9),
   "TransportVehiclePlate" VARCHAR2(50),
   "TransportDriver"    VARCHAR2(100),
   "TransportDriverPhone" VARCHAR2(100),
   "DeliveryBillNumber" VARCHAR2(50),
   "InterfaceRecordId"  VARCHAR2(25),
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "OrderApproveComment" VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "ConsigneeId"        NUMBER(9),
   "ConsigneeName"      VARCHAR2(100),
   "ConfirmedReceptionTime" DATE,
   "ReceiptConfirmorId" NUMBER(9),
   "ReceiptConfirmorName" VARCHAR2(100),
   "ReceiptConfirmTime" DATE,
   "RowVersion"         TIMESTAMP,
   "GPMSPurOrderCode"   VARCHAR2(50),
   "ArrivalMode"        NUMBER(9),
   "ShippingTime"       DATE,
   "ShippingMsg"        VARCHAR2(100),
   "ArrivalTime"        DATE,
   "CargoTerminalMsg"   VARCHAR2(100),
   "FlowFeedback"       VARCHAR2(4000),
   "ExpressCompanyId"   NUMBER(9),
   "ExpressCompanyCode" VARCHAR2(100),
   "ExpressCompany"     VARCHAR2(100),
   "QueryURL"           VARCHAR2(100),
   "GPSCode"            VARCHAR2(100),
   "ExpectedPlaceDate"  DATE,
   "Logistic"           VARCHAR2(100),
   "SAPPurchasePlanCode" VARCHAR2(50),
   "ERPSourceOrderCode" VARCHAR2(500),
   constraint PK_AGENCYPARTSSHIPPINGORDER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "AgencyPartsShippingOrderRef"                         */
/*==============================================================*/
create table "AgencyPartsShippingOrderRef"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsShippingOrderId" NUMBER(9)                       not null,
   "PartsOutboundBillId" NUMBER(9)                       not null,
   constraint PK_AGENCYPARTSSHIPPINGORDERREF primary key ("Id")
)
/

/*==============================================================*/
/* Table: "EcommerceFreightDisposal"                            */
/*==============================================================*/
create table "EcommerceFreightDisposal"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "VehiclePartsHandleOrderId" NUMBER(9)                       not null,
   "PartsOutboundBillId" NUMBER(9),
   "PartsOutboundBillCode" VARCHAR2(50),
   "PartsInboundCheckBillId" NUMBER(9),
   "PartsInboundCheckBillCode" VARCHAR2(50),
   "PartsPurchaseSettleBillId" NUMBER(9),
   "PartsPurchaseSettleBillCode" VARCHAR2(50),
   "PurchaseSettlementStatus" NUMBER(9),
   "PartsSalesSettlementId" NUMBER(9),
   "PartsSalesSettlementCode" VARCHAR2(50),
   "SalesSettlementStatus" NUMBER(9),
   "FreightCharges"     NUMBER(19,4),
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   "ERPSourceOrderCode" VARCHAR2(500),
   "VehiclePartsHandleOrderCode" VARCHAR2(50),
   constraint PK_ECOMMERCEFREIGHTDISPOSAL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "Express"                                             */
/*==============================================================*/
create table "Express"  (
   "Id"                 NUMBER(9)                       not null,
   "ExpressCode"        VARCHAR2(50)                    not null,
   "ExpressName"        VARCHAR2(100)                   not null,
   "Contacts"           VARCHAR2(100),
   "ContactsNumber"     VARCHAR2(50),
   "FixedTelephone"     VARCHAR2(50),
   "E_Mail"             VARCHAR2(100),
   "Status"             NUMBER(9),
   "Address"            VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "CancelId"           NUMBER(9),
   "CancelName"         VARCHAR2(100),
   "CancelTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_EXPRESS primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ExpressToLogistics"                                  */
/*==============================================================*/
create table "ExpressToLogistics"  (
   "Id"                 NUMBER(9)                       not null,
   "FocufingCoreId"     NUMBER(9)                       not null,
   "FocufingCoreCode"   VARCHAR2(50)                    not null,
   "FocufingCoreName"   VARCHAR2(100)                   not null,
   "LogisticsCompanyId" NUMBER(9)                       not null,
   "LogisticsCompanyCode" VARCHAR2(50)                    not null,
   "LogisticsCompanyName" VARCHAR2(100)                   not null,
   "ExpressId"          NUMBER(9)                       not null,
   "ExpressCode"        VARCHAR2(50)                    not null,
   "ExpressName"        VARCHAR2(100)                   not null,
   "Status"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "CancelId"           NUMBER(9),
   "CancelName"         VARCHAR2(100),
   "CancelTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_EXPRESSTOLOGISTICS primary key ("Id")
)
/

/*==============================================================*/
/* Table: "HauDistanceInfor"                                    */
/*==============================================================*/
create table "HauDistanceInfor"  (
   "Id"                 NUMBER(9)                       not null,
   "StorageCompanyId"   NUMBER(9)                       not null,
   "CompanyId"          NUMBER(9)                       not null,
   "CompanyAddressId"   NUMBER(9),
   "WarehouseId"        NUMBER(9)                       not null,
   "HauDistance"        NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_HAUDISTANCEINFOR primary key ("Id")
)
/

/*==============================================================*/
/* Table: "Logistics"                                           */
/*==============================================================*/
create table "Logistics"  (
   "Id"                 NUMBER(9)                       not null,
   "ShippingCode"       VARCHAR2(50)                    not null,
   "OrderCode"          VARCHAR2(50)                    not null,
   "ShippingCompanyName" VARCHAR2(100),
   "ShippingCompanyNumber" VARCHAR2(50),
   "ApproveTime"        DATE,
   "WarehouseName"      VARCHAR2(100),
   "ShippingDate"       DATE,
   "RequestedArrivalDate" DATE,
   "ReceivingAddress"   VARCHAR2(200),
   "LogisticName"       VARCHAR2(100),
   "TransportDriverPhone" VARCHAR2(100),
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "CancelId"           NUMBER(9),
   "CancelName"         VARCHAR2(100),
   "CancelTime"         DATE,
   "RowVersion"         TIMESTAMP,
   "Status"             NUMBER(9)                       not null,
   "ShippingMethod"     NUMBER(9),
   "SignStatus"         NUMBER(9),
   "PartsSalesOrderStatus" NUMBER(9),
   constraint PK_LOGISTICS primary key ("Id")
)
/

/*==============================================================*/
/* Table: "LogisticsDetail"                                     */
/*==============================================================*/
create table "LogisticsDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "Logisticsid"        NUMBER(9)                       not null,
   "PointName"          VARCHAR2(100),
   "PointTime"          DATE,
   "ScanTime"           DATE,
   "ScanName"           VARCHAR2(100),
   "Remark"             VARCHAR2(200),
   "Signatory"          VARCHAR2(100),
   "ShippingMethod"     NUMBER(9),
   constraint PK_LOGISTICSDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "LogisticsTime"                                       */
/*==============================================================*/
create table "LogisticsTime"  (
   "Id"                 NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "CompanyId"          NUMBER(9)                       not null,
   "CompanyCode"        VARCHAR2(50),
   "CompanyName"        VARCHAR2(100),
   "CompanyType"        NUMBER(9),
   "LogisticCompanyId"  NUMBER(9)                       not null,
   "LogisticCompanyCode" VARCHAR2(50),
   "LogisticCompanyName" VARCHAR2(100),
   "ShippingWarehouseId" NUMBER(9)                       not null,
   "ShippingWarehouseName" VARCHAR2(100),
   "ShippingWarehouseCode" VARCHAR2(50),
   "ReceivingWarehouseId" NUMBER(9),
   "ReceivingWarehouseCode" VARCHAR2(50),
   "ReceivingWarehouseName" VARCHAR2(100),
   "ReceivingAddress"   VARCHAR2(200),
   "Day"                NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_LOGISTICSTIME primary key ("Id")
)
/

/*==============================================================*/
/* Table: "LogisticsTracking"                                   */
/*==============================================================*/
create table "LogisticsTracking"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsShippingOrderCode" VARCHAR2(50)                    not null,
   "CarrierNumber"      VARCHAR2(50)                    not null,
   "LogisticsLabel"     VARCHAR2(100)                   not null,
   "MergerCaseNumber"   VARCHAR2(100)                   not null,
   "BoxUpTaskCode"      VARCHAR2(50)                    not null,
   "CounterpartCompanyCode" VARCHAR2(50)                    not null,
   "CounterpartCompanyName" VARCHAR2(100),
   "DispatchOrderCode"  VARCHAR2(500),
   "ArrivalTime"        DATE,
   "LatitudeAndLongitude" VARCHAR2(100),
   "Position"           VARCHAR2(200),
   "CarrierDate"        DATE,
   "TheoryArrivalTime"  DATE,
   "RouteDescription"   VARCHAR2(200),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "OperationTime"      DATE,
   "ExpectArrivalTime"  DATE,
   constraint PK_LOGISTICSTRACKING primary key ("Id")
)
/

/*==============================================================*/
/* Table: "LogisticsTrackingDetail"                             */
/*==============================================================*/
create table "LogisticsTrackingDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "LogisticsTrackingId" NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100),
   "Amount"             NUMBER(9)                       not null,
   constraint PK_LOGISTICSTRACKINGDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsShippingOrder"                                  */
/*==============================================================*/
create table "PartsShippingOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Type"               NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9),
   "ShippingCompanyId"  NUMBER(9)                       not null,
   "ShippingCompanyCode" VARCHAR2(50)                    not null,
   "ShippingCompanyName" VARCHAR2(100)                   not null,
   "SettlementCompanyId" NUMBER(9)                       not null,
   "SettlementCompanyCode" VARCHAR2(50)                    not null,
   "SettlementCompanyName" VARCHAR2(100)                   not null,
   "ReceivingCompanyId" NUMBER(9)                       not null,
   "ReceivingCompanyCode" VARCHAR2(50)                    not null,
   "InvoiceReceiveSaleCateId" NUMBER(9),
   "InvoiceReceiveSaleCateName" VARCHAR2(50),
   "ReceivingCompanyName" VARCHAR2(100)                   not null,
   "WarehouseId"        NUMBER(9),
   "WarehouseCode"      VARCHAR2(50),
   "WarehouseName"      VARCHAR2(100),
   "ReceivingWarehouseId" NUMBER(9),
   "ReceivingWarehouseCode" VARCHAR2(50),
   "ReceivingWarehouseName" VARCHAR2(100),
   "ReceivingAddress"   VARCHAR2(200),
   "LogisticCompanyId"  NUMBER(9),
   "LogisticCompanyCode" VARCHAR2(50),
   "LogisticCompanyName" VARCHAR2(100),
   "OriginalRequirementBillId" NUMBER(9),
   "OriginalRequirementBillCode" VARCHAR2(50),
   "OriginalRequirementBillType" NUMBER(9),
   "IsTransportLosses"  NUMBER(1),
   "TransportLossesDisposeStatus" NUMBER(9),
   "ShippingMethod"     NUMBER(9),
   "LogisticArrivalDate" DATE,
   "RequestedArrivalDate" DATE,
   "ShippingDate"       DATE,
   "Weight"             NUMBER(15,6),
   "Volume"             NUMBER(15,6),
   "BillingMethod"      NUMBER(9),
   "TransportCostRate"  NUMBER(9),
   "TransportCost"      NUMBER(19,4),
   "TransportCostSettleStatus" NUMBER(1),
   "InsuranceRate"      NUMBER(9),
   "InsuranceFee"       NUMBER(19,4),
   "TransportMileage"   NUMBER(9),
   "TransportVehiclePlate" VARCHAR2(50),
   "TransportDriver"    VARCHAR2(100),
   "TransportDriverPhone" VARCHAR2(100),
   "DeliveryBillNumber" VARCHAR2(50),
   "InterfaceRecordId"  VARCHAR2(25),
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "OrderApproveComment" VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "ConsigneeId"        NUMBER(9),
   "ConsigneeName"      VARCHAR2(100),
   "ConfirmedReceptionTime" DATE,
   "ReceiptConfirmorId" NUMBER(9),
   "ReceiptConfirmorName" VARCHAR2(100),
   "ReceiptConfirmTime" DATE,
   "RowVersion"         TIMESTAMP,
   "GPMSPurOrderCode"   VARCHAR2(50),
   "ArrivalMode"        NUMBER(9),
   "ShippingTime"       DATE,
   "ShippingMsg"        VARCHAR2(100),
   "ArrivalTime"        DATE,
   "CargoTerminalMsg"   VARCHAR2(100),
   "FlowFeedback"       VARCHAR2(4000),
   "ExpressCompanyId"   NUMBER(9),
   "ExpressCompanyCode" VARCHAR2(100),
   "ExpressCompany"     VARCHAR2(100),
   "QueryURL"           VARCHAR2(100),
   "GPSCode"            VARCHAR2(100),
   "ExpectedPlaceDate"  DATE,
   "Logistic"           VARCHAR2(100),
   "SAPPurchasePlanCode" VARCHAR2(50),
   "AppraiserName"      VARCHAR2(100),
   "AppraiserPhone"     VARCHAR2(50),
   "PackageEvaluation"  NUMBER(9),
   "AuditEvaluation"    NUMBER(9),
   "ServiceEvaluateStar" NUMBER(9),
   "EvaluationContent"  VARCHAR2(500),
   "images"             VARCHAR2(4000),
   "ERPSourceOrderCode" VARCHAR2(500),
   "EcommerceMoney"     NUMBER(19,4),
   "PartsOutboundPlanCode" VARCHAR2(50),
   "PartsSalesOrderTypeId" NUMBER(9),
   "PartsSalesOrderTypeName" VARCHAR2(100),
   "LinkName"           VARCHAR2(100),
   "LinkPhone"          VARCHAR2(100),
   "IsSync"             NUMBER(1),
   constraint PK_PARTSSHIPPINGORDER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsShippingOrderDetail"                            */
/*==============================================================*/
create table "PartsShippingOrderDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsShippingOrderId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "ShippingAmount"     NUMBER(9)                       not null,
   "ConfirmedAmount"    NUMBER(9),
   "DifferenceClassification" NUMBER(9),
   "TransportLossesDisposeMethod" NUMBER(9),
   "InTransitDamageLossAmount" NUMBER(9),
   "SettlementPrice"    NUMBER(19,4)                    not null,
   "Remark"             VARCHAR2(200),
   "ContainerNumber"    VARCHAR2(50),
   "PartsOutboundPlanId" NUMBER(9),
   "PartsOutboundPlanCode" VARCHAR2(50),
   "TaskId"             NUMBER(9),
   "TaskType"           NUMBER(9),
   "OriginalPrice"      NUMBER(19,4),
   constraint PK_PARTSSHIPPINGORDERDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsShippingOrderRef"                               */
/*==============================================================*/
create table "PartsShippingOrderRef"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsShippingOrderId" NUMBER(9)                       not null,
   "PartsOutboundBillId" NUMBER(9)                       not null,
   constraint PK_PARTSSHIPPINGORDERREF primary key ("Id")
)
/

/*==============================================================*/
/* Table: "ReturnFreightDisposal"                               */
/*==============================================================*/
create table "ReturnFreightDisposal"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "ServiceApplyCode"   VARCHAR2(50)                    not null,
   "PartsOutboundBillId" NUMBER(9),
   "PartsOutboundBillCode" VARCHAR2(50),
   "PartsInboundCheckBillId" NUMBER(9),
   "PartsInboundCheckBillCode" VARCHAR2(50),
   "PartsPurchaseSettleBillId" NUMBER(9),
   "PartsPurchaseSettleBillCode" VARCHAR2(50),
   "PurchaseSettlementStatus" NUMBER(9),
   "PartsPurchaseRtnSettleId" NUMBER(9),
   "PartsPurchaseRtnSettleCode" VARCHAR2(50),
   "PartsSalesSettlementId" NUMBER(9),
   "PartsSalesSettlementCode" VARCHAR2(50),
   "SalesSettlementStatus" NUMBER(9),
   "PartsSalesRtnSettlementId" NUMBER(9),
   "PartsSalesRtnSettlementCode" VARCHAR2(50),
   "FreightCharges"     NUMBER(19,4),
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   "ERPSourceOrderCode" VARCHAR2(500),
   constraint PK_RETURNFREIGHTDISPOSAL primary key ("Id")
)
/

