/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2023/10/16 16:09:24                          */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"IMSPurchaseLoginInfo"');
  if num>0 then
    execute immediate 'drop table "IMSPurchaseLoginInfo" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"IMSShippingLoginInfo"');
  if num>0 then
    execute immediate 'drop table "IMSShippingLoginInfo" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"ManualScheduling"');
  if num>0 then
    execute immediate 'drop table "ManualScheduling" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartConPurchasePlan"');
  if num>0 then
    execute immediate 'drop table "PartConPurchasePlan" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsPurReturnOrder"');
  if num>0 then
    execute immediate 'drop table "PartsPurReturnOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsPurReturnOrderDetail"');
  if num>0 then
    execute immediate 'drop table "PartsPurReturnOrderDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsPurchaseInfoSummary"');
  if num>0 then
    execute immediate 'drop table "PartsPurchaseInfoSummary" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsPurchaseOrder"');
  if num>0 then
    execute immediate 'drop table "PartsPurchaseOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsPurchaseOrderDetail"');
  if num>0 then
    execute immediate 'drop table "PartsPurchaseOrderDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsPurchaseOrder_Sync"');
  if num>0 then
    execute immediate 'drop table "PartsPurchaseOrder_Sync" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsPurchasePlan"');
  if num>0 then
    execute immediate 'drop table "PartsPurchasePlan" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsPurchasePlanDetail"');
  if num>0 then
    execute immediate 'drop table "PartsPurchasePlanDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsPurchasePlanDetail_HW"');
  if num>0 then
    execute immediate 'drop table "PartsPurchasePlanDetail_HW" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsPurchasePlanTemp"');
  if num>0 then
    execute immediate 'drop table "PartsPurchasePlanTemp" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsPurchasePlan_HW"');
  if num>0 then
    execute immediate 'drop table "PartsPurchasePlan_HW" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PurchaseOrderFinishedDetail"');
  if num>0 then
    execute immediate 'drop table "PurchaseOrderFinishedDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PurchaseOrderTrackDetail"');
  if num>0 then
    execute immediate 'drop table "PurchaseOrderTrackDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PurchaseOrderTracking"');
  if num>0 then
    execute immediate 'drop table "PurchaseOrderTracking" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PurchaseOrderUnFinishDetail"');
  if num>0 then
    execute immediate 'drop table "PurchaseOrderUnFinishDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PurchasePersonSupplierLink"');
  if num>0 then
    execute immediate 'drop table "PurchasePersonSupplierLink" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SupplierPlanArrear"');
  if num>0 then
    execute immediate 'drop table "SupplierPlanArrear" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SupplierPreAppLoanDetail"');
  if num>0 then
    execute immediate 'drop table "SupplierPreAppLoanDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SupplierPreApprovedLoan"');
  if num>0 then
    execute immediate 'drop table "SupplierPreApprovedLoan" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SupplierShippingDetail"');
  if num>0 then
    execute immediate 'drop table "SupplierShippingDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SupplierShippingOrder"');
  if num>0 then
    execute immediate 'drop table "SupplierShippingOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SupplierTraceCode"');
  if num>0 then
    execute immediate 'drop table "SupplierTraceCode" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SupplierTraceCode2"');
  if num>0 then
    execute immediate 'drop table "SupplierTraceCode2" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SupplierTraceCodeDetail"');
  if num>0 then
    execute immediate 'drop table "SupplierTraceCodeDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SupplierTraceCodeDetail2"');
  if num>0 then
    execute immediate 'drop table "SupplierTraceCodeDetail2" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SupplierTraceDetail"');
  if num>0 then
    execute immediate 'drop table "SupplierTraceDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"TemPurchaseOrder"');
  if num>0 then
    execute immediate 'drop table "TemPurchaseOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"TemPurchaseOrderDetail"');
  if num>0 then
    execute immediate 'drop table "TemPurchaseOrderDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"TemPurchasePlanOrder"');
  if num>0 then
    execute immediate 'drop table "TemPurchasePlanOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"TemPurchasePlanOrderDetail"');
  if num>0 then
    execute immediate 'drop table "TemPurchasePlanOrderDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"TemShippingOrderDetail"');
  if num>0 then
    execute immediate 'drop table "TemShippingOrderDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"TemSupplierShippingOrder"');
  if num>0 then
    execute immediate 'drop table "TemSupplierShippingOrder" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_IMSPurchaseLoginInfo"');
  if num>0 then
    execute immediate 'drop sequence "S_IMSPurchaseLoginInfo"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_IMSShippingLoginInfo"');
  if num>0 then
    execute immediate 'drop sequence "S_IMSShippingLoginInfo"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_ManualScheduling"');
  if num>0 then
    execute immediate 'drop sequence "S_ManualScheduling"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartConPurchasePlan"');
  if num>0 then
    execute immediate 'drop sequence "S_PartConPurchasePlan"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsPurReturnOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsPurReturnOrder"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsPurReturnOrderDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsPurReturnOrderDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsPurchaseInfoSummary"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsPurchaseInfoSummary"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsPurchaseOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsPurchaseOrder"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsPurchaseOrderDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsPurchaseOrderDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsPurchaseOrder_Sync"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsPurchaseOrder_Sync"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsPurchasePlan"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsPurchasePlan"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsPurchasePlanDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsPurchasePlanDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsPurchasePlanDetail_HW"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsPurchasePlanDetail_HW"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsPurchasePlanTemp"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsPurchasePlanTemp"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsPurchasePlan_HW"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsPurchasePlan_HW"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PurchaseOrderFinishedDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_PurchaseOrderFinishedDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PurchaseOrderTrackDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_PurchaseOrderTrackDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PurchaseOrderTracking"');
  if num>0 then
    execute immediate 'drop sequence "S_PurchaseOrderTracking"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PurchaseOrderUnFinishDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_PurchaseOrderUnFinishDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PurchasePersonSupplierLink"');
  if num>0 then
    execute immediate 'drop sequence "S_PurchasePersonSupplierLink"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SupplierPlanArrear"');
  if num>0 then
    execute immediate 'drop sequence "S_SupplierPlanArrear"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SupplierPreAppLoanDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_SupplierPreAppLoanDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SupplierPreApprovedLoan"');
  if num>0 then
    execute immediate 'drop sequence "S_SupplierPreApprovedLoan"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SupplierShippingDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_SupplierShippingDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SupplierShippingOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_SupplierShippingOrder"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SupplierTraceCode"');
  if num>0 then
    execute immediate 'drop sequence "S_SupplierTraceCode"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SupplierTraceCode2"');
  if num>0 then
    execute immediate 'drop sequence "S_SupplierTraceCode2"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SupplierTraceCodeDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_SupplierTraceCodeDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SupplierTraceCodeDetail2"');
  if num>0 then
    execute immediate 'drop sequence "S_SupplierTraceCodeDetail2"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SupplierTraceDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_SupplierTraceDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_TemPurchaseOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_TemPurchaseOrder"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_TemPurchaseOrderDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_TemPurchaseOrderDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_TemPurchasePlanOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_TemPurchasePlanOrder"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_TemPurchasePlanOrderDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_TemPurchasePlanOrderDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_TemShippingOrderDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_TemShippingOrderDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_TemSupplierShippingOrder"');
  if num>0 then
    execute immediate 'drop sequence "S_TemSupplierShippingOrder"';
  end if;
end;
/

create sequence "S_IMSPurchaseLoginInfo"
/

create sequence "S_IMSShippingLoginInfo"
/

create sequence "S_ManualScheduling"
/

create sequence "S_PartConPurchasePlan"
/

create sequence "S_PartsPurReturnOrder"
/

create sequence "S_PartsPurReturnOrderDetail"
/

create sequence "S_PartsPurchaseInfoSummary"
/

create sequence "S_PartsPurchaseOrder"
/

create sequence "S_PartsPurchaseOrderDetail"
/

create sequence "S_PartsPurchaseOrder_Sync"
/

create sequence "S_PartsPurchasePlan"
/

create sequence "S_PartsPurchasePlanDetail"
/

create sequence "S_PartsPurchasePlanDetail_HW"
/

create sequence "S_PartsPurchasePlanTemp"
/

create sequence "S_PartsPurchasePlan_HW"
/

create sequence "S_PurchaseOrderFinishedDetail"
/

create sequence "S_PurchaseOrderTrackDetail"
/

create sequence "S_PurchaseOrderTracking"
/

create sequence "S_PurchaseOrderUnFinishDetail"
/

create sequence "S_PurchasePersonSupplierLink"
/

create sequence "S_SupplierPlanArrear"
/

create sequence "S_SupplierPreAppLoanDetail"
/

create sequence "S_SupplierPreApprovedLoan"
/

create sequence "S_SupplierShippingDetail"
/

create sequence "S_SupplierShippingOrder"
/

create sequence "S_SupplierTraceCode"
/

create sequence "S_SupplierTraceCode2"
/

create sequence "S_SupplierTraceCodeDetail"
/

create sequence "S_SupplierTraceCodeDetail2"
/

create sequence "S_SupplierTraceDetail"
/

create sequence "S_TemPurchaseOrder"
/

create sequence "S_TemPurchaseOrderDetail"
/

create sequence "S_TemPurchasePlanOrder"
/

create sequence "S_TemPurchasePlanOrderDetail"
/

create sequence "S_TemShippingOrderDetail"
/

create sequence "S_TemSupplierShippingOrder"
/

/*==============================================================*/
/* Table: "IMSPurchaseLoginInfo"                                */
/*==============================================================*/
create table "IMSPurchaseLoginInfo"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50),
   "FileName"           VARCHAR2(200),
   "Status"             NUMBER(9),
   "SyncTime"           DATE,
   "ErrorMsg"           VARCHAR2(2000),
   constraint PK_IMSPURCHASELOGININFO primary key ("Id")
)
/

/*==============================================================*/
/* Table: "IMSShippingLoginInfo"                                */
/*==============================================================*/
create table "IMSShippingLoginInfo"  (
   "id"                 NUMBER(9)                       not null,
   "DealerCode"         VARCHAR2(2000),
   "OrderNo"            NUMBER(9),
   "OrderLineNo"        VARCHAR2(160),
   "DelNote"            VARCHAR2(160),
   "DelLineNo"          VARCHAR2(160),
   "ReqPart"            VARCHAR2(160),
   "vendor"             VARCHAR2(160),
   "ReqQty"             VARCHAR2(160),
   "DelPart"            VARCHAR2(160),
   "DelQty"             VARCHAR2(160),
   "DelDate"            DATE,
   "ReplCode"           VARCHAR2(160),
   "InfoCode"           VARCHAR2(160),
   "Reference"          VARCHAR2(160),
   "Backorder"          VARCHAR2(160),
   "Notes"              VARCHAR2(160),
   "SpecialPrice"       VARCHAR2(160),
   "FreightCharges"     VARCHAR2(160),
   FILENAME             VARCHAR2(160),
   SYNCSTATUS           NUMBER(9),
   SYNCTIME             DATE,
   ERRORMESSAGE         VARCHAR2(160),
   constraint PK_IMSSHIPPINGLOGININFO primary key ("id")
)
/

/*==============================================================*/
/* Table: "ManualScheduling"                                    */
/*==============================================================*/
create table "ManualScheduling"  (
   "Id"                 NUMBER(9)                       not null,
   "InterfaceName"      VARCHAR2(100),
   "OldSyncStatus"      NUMBER(9),
   "NewSyncStatus"      NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_MANUALSCHEDULING primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartConPurchasePlan"                                 */
/*==============================================================*/
create table "PartConPurchasePlan"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "BranchName"         VARCHAR2(100)                   not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryName" VARCHAR2(100)                   not null,
   "PartId"             NUMBER(9)                       not null,
   "PartCode"           VARCHAR2(50)                    not null,
   "PartName"           VARCHAR2(100)                   not null,
   "PrimarySupplierId"  NUMBER(9),
   "PrimarySupplierCode" VARCHAR2(50),
   "PrimarySupplierName" VARCHAR2(100),
   "DFOrderWarehouse"   VARCHAR2(100),
   "PartABC"            NUMBER(9),
   "PruductLifeCycle"   NUMBER(9),
   "PlannedPriceType"   NUMBER(9),
   "Near12MOutFrequency" NUMBER(9),
   "PlannedPrice"       NUMBER(19,4),
   "OrderQuantity1"     NUMBER(9),
   "OrderQuantity2"     NUMBER(9),
   "OrderQuantity3"     NUMBER(9),
   "OrderQuantityFor3M" NUMBER(9),
   "OrderQuantityFor6M" NUMBER(9),
   "OrderQuantityFor12M" NUMBER(9),
   "DayAvgFor1M"        NUMBER(15,6),
   "DayAvgFor3M"        NUMBER(15,6),
   "DayAvgFor6M"        NUMBER(15,6),
   "DayAvgFor12M"       NUMBER(15,6),
   "Average"            NUMBER(15,6),
   "ValidStock"         NUMBER(9),
   "PurchaseInLine"     NUMBER(9),
   "TransferInLine"     NUMBER(9),
   "ReStock"            NUMBER(9),
   "RePurchaseInLine"   NUMBER(9),
   "ExStock"            NUMBER(9),
   "ExPurchaseInLine"   NUMBER(9),
   "SalesPending"       NUMBER(9),
   "PurchaseToBeConfirm" NUMBER(9),
   "ConArrivalCycle"    NUMBER(9),
   "OrderCycle"         NUMBER(9),
   "EconomicalBatch"    NUMBER(9),
   "MinPurchaseQuantity" NUMBER(9),
   "TheoryMaxStock"     NUMBER(9),
   "PurchaseMethod1"    NUMBER(9),
   "PurchaseMethod2"    NUMBER(9),
   "PurchaseMethod3"    NUMBER(9),
   "PurchaseMethod4"    NUMBER(9),
   "AdvicePurchase"     NUMBER(9),
   "ActualSupplierId"   NUMBER(9),
   "ActualSupplierCode" VARCHAR2(50),
   "ActualOrderWarehouseId" NUMBER(9),
   "ActualOrderWarehouse" VARCHAR2(100),
   "ActualPurchase"     NUMBER(9),
   "DateT"              DATE,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_PARTCONPURCHASEPLAN primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsPurReturnOrder"                                 */
/*==============================================================*/
create table "PartsPurReturnOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "PartsPurchaseOrderId" NUMBER(9),
   "PartsPurchaseOrderCode" VARCHAR2(50),
   "BranchId"           NUMBER(9)                       not null,
   "BranchCode"         VARCHAR2(50)                    not null,
   "BranchName"         VARCHAR2(100)                   not null,
   "PartsSupplierId"    NUMBER(9)                       not null,
   "PartsSupplierCode"  VARCHAR2(50)                    not null,
   "PartsSupplierName"  VARCHAR2(100)                   not null,
   "TotalAmount"        NUMBER(19,4)                    not null,
   "WarehouseId"        NUMBER(9)                       not null,
   "WarehouseName"      VARCHAR2(100)                   not null,
   "WarehouseAddress"   VARCHAR2(200),
   "InvoiceRequirement" VARCHAR2(100),
   "Status"             NUMBER(9)                       not null,
   "ReturnReason"       NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "RowVersion"         TIMESTAMP,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryName" VARCHAR2(100)                   not null,
   "OutStatus"          NUMBER(9),
   "SAPPurReturnOrderCode" VARCHAR2(50),
   "ServiceApplyCode"   VARCHAR2(50),
   "ERPSourceOrderCode" VARCHAR2(500),
   "InitialApproverId"  NUMBER(9),
   "InitialApproverName" VARCHAR2(100),
   "InitialApproveTime" DATE,
   "InitialApproverComment" VARCHAR2(200),
   "ApprovertComment"   VARCHAR2(200),
   constraint PK_PARTSPURRETURNORDER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsPurReturnOrderDetail"                           */
/*==============================================================*/
create table "PartsPurReturnOrderDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsPurReturnOrderId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SerialNumber"       NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "SupplierPartCode"   VARCHAR2(50),
   "Quantity"           NUMBER(9)                       not null,
   "InspectedQuantity"  NUMBER(9),
   "MeasureUnit"        VARCHAR2(50),
   "BatchNumber"        VARCHAR2(50),
   "UnitPrice"          NUMBER(19,4)                    not null,
   "Remark"             VARCHAR2(200),
   constraint PK_PARTSPURRETURNORDERDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsPurchaseInfoSummary"                            */
/*==============================================================*/
create table "PartsPurchaseInfoSummary"  (
   "Id"                 NUMBER(9)                       not null,
   "PartId"             NUMBER(9),
   "PartCode"           VARCHAR2(50),
   "PartName"           VARCHAR2(100),
   "SupplyOnLineQty"    NUMBER(9),
   "OverdueOnLineQty"   NUMBER(9),
   "WaitInQty"          NUMBER(9),
   "WaitPackQty"        NUMBER(9),
   "WaitShelvesQty"     NUMBER(9),
   "OweOrderNum"        NUMBER(9),
   "OwePartNum"         NUMBER(9),
   "CreateTime"         DATE,
   "ModifyTime"         DATE,
   constraint PK_PARTSPURCHASEINFOSUMMARY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsPurchaseOrder"                                  */
/*==============================================================*/
create table "PartsPurchaseOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "BranchId"           NUMBER(9)                       not null,
   "BranchCode"         VARCHAR2(50)                    not null,
   "BranchName"         VARCHAR2(100)                   not null,
   "WarehouseId"        NUMBER(9)                       not null,
   "WarehouseName"      VARCHAR2(100)                   not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryName" VARCHAR2(100)                   not null,
   "DirectRecWarehouseId" NUMBER(9),
   "DirectRecWarehouseName" VARCHAR2(100),
   "ReceivingCompanyId" NUMBER(9),
   "ReceivingCompanyName" VARCHAR2(100),
   "ReceivingAddress"   VARCHAR2(200),
   "PartsSupplierId"    NUMBER(9)                       not null,
   "PartsSupplierCode"  VARCHAR2(50)                    not null,
   "PartsSupplierName"  VARCHAR2(100)                   not null,
   "IfDirectProvision"  NUMBER(1)                       not null,
   "OriginalRequirementBillId" NUMBER(9),
   "OriginalRequirementBillType" NUMBER(9),
   "OriginalRequirementBillCode" VARCHAR2(50),
   "SAPPurchasePlanCode" VARCHAR2(50),
   "TotalAmount"        NUMBER(19,4)                    not null,
   "PartsPurchaseOrderTypeId" NUMBER(9)                       not null,
   "RequestedDeliveryTime" DATE                            not null,
   "ShippingMethod"     NUMBER(9),
   "PlanSource"         VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "ConfirmationRemark" VARCHAR2(200),
   "Remark"             VARCHAR2(200),
   "InStatus"           NUMBER(9),
   "AbandonOrStopReason" VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "SubmitterId"        NUMBER(9),
   "SubmitterName"      VARCHAR2(100),
   "SubmitTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "FirstApproveTime"   DATE,
   "ApproveTime"        DATE,
   "CloserId"           NUMBER(9),
   "CloserName"         VARCHAR2(100),
   "CloseTime"          DATE,
   "RowVersion"         TIMESTAMP,
   "GPMSPurOrderCode"   VARCHAR2(50),
   "PurOrderId"         NUMBER(9),
   "HWPurOrderCode"     VARCHAR2(50),
   "PurOrderCode"       VARCHAR2(50),
   "IfInnerDirectProvision" NUMBER(1),
   "BPMAudited"         NUMBER(9),
   "ERPSourceOrderCode" VARCHAR2(500),
   "OMVehiclePartsHandleCode" VARCHAR2(50),
   "CPPartsPurchaseOrderCode" VARCHAR2(50),
   "CPPartsInboundCheckCode" VARCHAR2(50),
   "Path"               VARCHAR2(2000),
   "IsTransSap"         NUMBER(1),
   "IsPack"             NUMBER(1),
   constraint PK_PARTSPURCHASEORDER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsPurchaseOrderDetail"                            */
/*==============================================================*/
create table "PartsPurchaseOrderDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsPurchaseOrderId" NUMBER(9)                       not null,
   "SerialNumber"       NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "SupplierPartCode"   VARCHAR2(50),
   "PromisedDeliveryTime" DATE,
   "UnitPrice"          NUMBER(19,4)                    not null,
   "OrderAmount"        NUMBER(9)                       not null,
   "ConfirmedAmount"    NUMBER(9)                       not null,
   "ShippingAmount"     NUMBER(9),
   "DirectOrderAmount"  NUMBER(9),
   "MeasureUnit"        VARCHAR2(50),
   "Specification"      VARCHAR2(100),
   "PackingAmount"      NUMBER(9),
   "PackingSpecification" VARCHAR2(200),
   "Remark"             VARCHAR2(200),
   "ConfirmationRemark" VARCHAR2(200),
   "PurchasePriceBefore" NUMBER(19,4),
   "OverseasPartsFigure" VARCHAR2(25),
   "OverseasProjectNumber" NUMBER(9),
   "OriginalPlanNumber" NUMBER(9),
   "POCode"             VARCHAR2(50),
   "ShortSupReason"     NUMBER(9),
   "ShippingDate"       DATE,
   "PriceType"          NUMBER(9),
   "PriceTypeName"      VARCHAR2(20),
   "ABCStrategy"        VARCHAR2(10),
   "TraceProperty"      NUMBER(9),
   "IsDirect"           NUMBER(1),
   "SIHCodes"           VARCHAR2(2000),
   constraint PK_PARTSPURCHASEORDERDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsPurchaseOrder_Sync"                             */
/*==============================================================*/
create table "PartsPurchaseOrder_Sync"  (
   "Id"                 NUMBER(9)                       not null,
   "Objid"              VARCHAR2(40),
   "IOStatus"           NUMBER(9),
   "CreateTime"         DATE,
   "FirstSendTime"      DATE,
   "ModifySendTime"     DATE,
   "Token"              VARCHAR2(50),
   "Sender"             VARCHAR2(20),
   "Message"            VARCHAR2(500),
   constraint PK_PARTSPURCHASEORDER_SYNC primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsPurchasePlan"                                   */
/*==============================================================*/
create table "PartsPurchasePlan"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "BranchId"           NUMBER(9)                       not null,
   "BranchCode"         VARCHAR2(50)                    not null,
   "BranchName"         VARCHAR2(100)                   not null,
   "WarehouseId"        NUMBER(9)                       not null,
   "WarehouseName"      VARCHAR2(100)                   not null,
   "Status"             NUMBER(9)                       not null,
   "Memo"               VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "SubmitterId"        NUMBER(9),
   "SubmitterName"      VARCHAR2(100),
   "SubmitTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "ApproveMemo"        VARCHAR2(200),
   "CheckerId"          NUMBER(9),
   "CheckerName"        VARCHAR2(100),
   "CheckTime"          DATE,
   "CheckMemo"          VARCHAR2(200),
   "CloserId"           NUMBER(9),
   "CloserName"         VARCHAR2(100),
   "CloseTime"          DATE,
   "CloseMemo"          VARCHAR2(200),
   "PartsSalesCategoryId" NUMBER(9),
   "PartsSalesCategoryName" VARCHAR2(100),
   "RowVersion"         TIMESTAMP,
   "WarehouseCode"      VARCHAR2(50),
   "PartsPlanTypeId"    NUMBER(9),
   "AbandonComment"     VARCHAR2(200),
   "Path"               VARCHAR2(2000),
   "IsTransSap"         NUMBER(1),
   "IsPack"             NUMBER(1),
   constraint PK_PARTSPURCHASEPLAN primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsPurchasePlanDetail"                             */
/*==============================================================*/
create table "PartsPurchasePlanDetail"  (
   "id"                 NUMBER(9)                       not null,
   "PurchasePlanId"     NUMBER(9)                       not null,
   "SerialNumber"       NUMBER(9)                       not null,
   "SuplierId"          NUMBER(9),
   "SuplierCode"        VARCHAR2(50),
   "SuplierName"        VARCHAR2(100),
   "ShippingMethod"     NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "SupplierPartCode"   VARCHAR2(50),
   "Price"              NUMBER(19,4)                    not null,
   "PlanAmount"         NUMBER(9)                       not null,
   "Issued"             NUMBER(9)                       not null,
   "PromisedDeliveryTime" DATE                            not null,
   "MeasureUnit"        VARCHAR2(50),
   "PackingAmount"      NUMBER(9),
   "PackingSpecification" VARCHAR2(200),
   "Memo"               VARCHAR2(200),
   "PriceTypeName"      VARCHAR2(20),
   "ABCStrategy"        VARCHAR2(10),
   "IsDirect"           NUMBER(1),
   constraint PK_PARTSPURCHASEPLANDETAIL primary key ("id")
)
/

/*==============================================================*/
/* Table: "PartsPurchasePlanDetail_HW"                          */
/*==============================================================*/
create table "PartsPurchasePlanDetail_HW"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsPurchasePlanId" NUMBER(9),
   "PartId"             NUMBER(9),
   "PartCode"           VARCHAR2(50),
   "PartName"           VARCHAR2(100),
   "GPMSPartCode"       VARCHAR2(25),
   "PartsPurchasePlanQty" NUMBER(9),
   "PartsSupplierId"    NUMBER(9),
   "PartsSupplierCode"  VARCHAR2(50),
   "PartsSupplierName"  VARCHAR2(100),
   "UntFulfilledQty"    NUMBER(9),
   "TransferQty"        NUMBER(9),
   "PurchaseQty"        NUMBER(9),
   "PurchaseConfimQty"  NUMBER(9),
   "TransferConfimQty"  NUMBER(9),
   "UnitPrice"          NUMBER(19,4),
   "MeasureUnit"        VARCHAR2(50),
   "GPMSRowNum"         VARCHAR2(50),
   "RequestedDeliveryTime" DATE,
   "Remark"             VARCHAR2(200),
   "FaultReason"        VARCHAR2(500),
   "OverseasSuplierCode" VARCHAR2(50),
   "POCode"             VARCHAR2(50),
   "GDCDCStock"         NUMBER(9),
   "BJCDCStock"         NUMBER(9),
   "SDCDCStock"         NUMBER(9),
   "SuplierCode"        VARCHAR2(50),
   "WarehouseId"        NUMBER(9),
   "WarehouseCode"      VARCHAR2(50),
   "WarehouseName"      VARCHAR2(100),
   "RowVersion"         TIMESTAMP,
   "EndAmount"          NUMBER(9),
   constraint PK_PARTSPURCHASEPLANDETAIL_HW primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsPurchasePlanTemp"                               */
/*==============================================================*/
create table "PartsPurchasePlanTemp"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsPurchaseOrderId" NUMBER(9)                       not null,
   "PartsPurchaseOrderCode" VARCHAR2(50)                    not null,
   "WarehouseId"        NUMBER(9)                       not null,
   "WarehouseName"      VARCHAR2(100)                   not null,
   "PackSparePartId"    NUMBER(9),
   "PackSparePartCode"  VARCHAR2(50),
   "PackSparePartName"  VARCHAR2(100),
   "PackPlanQty"        NUMBER(9),
   "PackNum"            NUMBER(9),
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "OrderAmount"        NUMBER(9)                       not null,
   "Status"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "PackPurcheseTime"   DATE,
   "PackPartsPurchaseOrderCode" VARCHAR2(50)
)
/

/*==============================================================*/
/* Table: "PartsPurchasePlan_HW"                                */
/*==============================================================*/
create table "PartsPurchasePlan_HW"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50),
   "WarehouseId"        NUMBER(9),
   "WarehouseCode"      VARCHAR2(50),
   "PartsSalesCategoryId" NUMBER(9),
   "PartsSalesCategoryName" VARCHAR2(50),
   "PartsSupplierId"    NUMBER(9),
   "PurchasePlanType"   NUMBER(9),
   "TotalAmount"        NUMBER(19,4),
   "RequestedDeliveryTime" DATE,
   "Status"             NUMBER(9),
   "SAPPurchasePlanCode" VARCHAR2(50),
   "StopReason"         VARCHAR2(200),
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "StoporId"           NUMBER(9),
   "StoporName"         VARCHAR2(100),
   "StopTime"           DATE,
   "Aboardcode"         VARCHAR2(50),
   INSTID               VARCHAR2(100),
   "restatus"           NUMBER(9),
   "RowVersion"         TIMESTAMP,
   constraint PK_PARTSPURCHASEPLAN_HW primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PurchaseOrderFinishedDetail"                         */
/*==============================================================*/
create table "PurchaseOrderFinishedDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "PurchaseCode"       VARCHAR2(50),
   "SpareCode"          VARCHAR2(50),
   "PurchasePlanCode"   VARCHAR2(50),
   "SupplierName"       VARCHAR2(100),
   "SupplierPartCode"   VARCHAR2(50),
   "MapName"            VARCHAR2(100),
   "SihCode"            VARCHAR2(50),
   "PartABC"            NUMBER(9),
   "PurchasePlanQty"    NUMBER(9),
   "SupplyConfirmQty"   NUMBER(9),
   "SupplyConfirmReason" VARCHAR2(200),
   "SupplyShippingQty"  NUMBER(9),
   "InboundQty"         NUMBER(9),
   "InboundForceQty"    NUMBER(9),
   "OnLineQty"          NUMBER(9),
   "PurchaseCreateTime" DATE,
   "SupplyConfirmTime"  DATE,
   "SupplyShippingTime" DATE,
   "ExpectDeliveryTime" DATE,
   "InboundTime"        DATE,
   "PlanType"           VARCHAR2(100),
   "WarehouseName"      VARCHAR2(100),
   "SupplyShippingCycle" NUMBER(9),
   "SupplyArrivalCycle" NUMBER(9),
   "TheoryDeliveryTime" DATE,
   "PurchasePlanRemark" VARCHAR2(200),
   "PurchasePlanTime"   DATE,
   constraint PK_PURCHASEORDERFINISHEDDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PurchaseOrderTrackDetail"                            */
/*==============================================================*/
create table "PurchaseOrderTrackDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "TrackingId"         NUMBER(9)                       not null,
   "TrackStatus"        VARCHAR2(500),
   "TrackDate"          DATE,
   constraint PK_PURCHASEORDERTRACKDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PurchaseOrderTracking"                               */
/*==============================================================*/
create table "PurchaseOrderTracking"  (
   "Id"                 NUMBER(9)                       not null,
   "PurchaseCode"       VARCHAR2(50)                    not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "SupplierPartCode"   VARCHAR2(50),
   "OrderAmount"        NUMBER(9),
   "PartsSupplierId"    NUMBER(9),
   "PartsSupplierCode"  VARCHAR2(50),
   "PartsSupplierName"  VARCHAR2(100),
   "WarehouseId"        NUMBER(9),
   "WarehouseName"      VARCHAR2(100),
   "WarehouseCode"      VARCHAR2(50),
   "OrderCreateTime"    DATE,
   "PersonName"         VARCHAR2(100),
   "Levels"             NUMBER(9),
   "PartsPurchaseOrderTypeId" NUMBER(9),
   constraint PK_PURCHASEORDERTRACKING primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PurchaseOrderUnFinishDetail"                         */
/*==============================================================*/
create table "PurchaseOrderUnFinishDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "PurchaseCode"       VARCHAR2(50),
   "SpareCode"          VARCHAR2(50),
   "PurchasePlanCode"   VARCHAR2(50),
   "SupplierName"       VARCHAR2(100),
   "SupplierPartCode"   VARCHAR2(50),
   "MapName"            VARCHAR2(100),
   "SihCode"            VARCHAR2(50),
   "PartABC"            NUMBER(9),
   "PurchasePlanQty"    NUMBER(9),
   "SupplyConfirmQty"   NUMBER(9),
   "SupplyConfirmReason" VARCHAR2(200),
   "SupplyShippingQty"  NUMBER(9),
   "InboundQty"         NUMBER(9),
   "InboundForceQty"    NUMBER(9),
   "OnLineQty"          NUMBER(9),
   "PurchaseCreateTime" DATE,
   "SupplyConfirmTime"  DATE,
   "SupplyShippingTime" DATE,
   "ExpectDeliveryTime" DATE,
   "InboundTime"        DATE,
   "PlanType"           VARCHAR2(100),
   "WarehouseName"      VARCHAR2(100),
   "SupplyShippingCycle" NUMBER(9),
   "SupplyArrivalCycle" NUMBER(9),
   "TheoryDeliveryTime" DATE,
   "PurchasePlanRemark" VARCHAR2(200),
   "PurchasePlanTime"   DATE,
   constraint PK_PURCHASEORDERUNFINISHDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PurchasePersonSupplierLink"                          */
/*==============================================================*/
create table "PurchasePersonSupplierLink"  (
   "Id"                 NUMBER(9)                       not null,
   "PersonId"           NUMBER(9)                       not null,
   "PersonCode"         VARCHAR2(50),
   "PersonName"         VARCHAR2(50),
   "SupplierId"         NUMBER(9)                       not null,
   "SupplierCode"       VARCHAR2(50),
   "SupplierName"       VARCHAR2(100),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_PURCHASEPERSONSUPPLIERLINK primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SupplierPlanArrear"                                  */
/*==============================================================*/
create table "SupplierPlanArrear"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "SupplierCode"       VARCHAR2(50)                    not null,
   "SupplierId"         NUMBER(9)                       not null,
   "SupplierCompanyCode" VARCHAR2(50)                    not null,
   "SupplierCompanyName" VARCHAR2(100)                   not null,
   "PlannedAmountOwed"  NUMBER(19,4)                    not null,
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_SUPPLIERPLANARREAR primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SupplierPreAppLoanDetail"                            */
/*==============================================================*/
create table "SupplierPreAppLoanDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "SupplierPreApprovedLoanId" NUMBER(9)                       not null,
   "SupplierCode"       VARCHAR2(50)                    not null,
   "SupplierId"         NUMBER(9)                       not null,
   "SupplierCompanyCode" VARCHAR2(50)                    not null,
   "SupplierCompanyName" VARCHAR2(100)                   not null,
   "BankName"           VARCHAR2(50),
   "BankCode"           VARCHAR2(20),
   "PlannedAmountOwed"  NUMBER(19,4)                    not null,
   "ActualDebt"         NUMBER(19,4)                    not null,
   "ExceedAmount"       NUMBER(19,4)                    not null,
   "ApprovalAmount"     NUMBER(19,4),
   "ActualPaidAmount"   NUMBER(19,4),
   "IsOverstock"        NUMBER(1)                       not null,
   "Remark"             VARCHAR2(500),
   constraint PK_SUPPLIERPREAPPLOANDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SupplierPreApprovedLoan"                             */
/*==============================================================*/
create table "SupplierPreApprovedLoan"  (
   "Id"                 NUMBER(9)                       not null,
   "PreApprovedCode"    VARCHAR2(50)                    not null,
   "BranchId"           NUMBER(9),
   "BranchCode"         VARCHAR2(50),
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "ActualDebt"         NUMBER(19,4)                    not null,
   "PlanArrear"         NUMBER(19,4)                    not null,
   "ExceedAmount"       NUMBER(19,4)                    not null,
   "ApprovalAmount"     NUMBER(19,4)                    not null,
   "ActualPaidAmount"   NUMBER(19,4),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_SUPPLIERPREAPPROVEDLOAN primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SupplierShippingDetail"                              */
/*==============================================================*/
create table "SupplierShippingDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "SupplierShippingOrderId" NUMBER(9)                       not null,
   "SerialNumber"       NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "SupplierPartCode"   VARCHAR2(50),
   "MeasureUnit"        VARCHAR2(50),
   "Quantity"           NUMBER(9)                       not null,
   "ConfirmedAmount"    NUMBER(9),
   "UnitPrice"          NUMBER(19,4)                    not null,
   "SpareOrderRemark"   VARCHAR2(200),
   "Remark"             VARCHAR2(200),
   "POCode"             VARCHAR2(50),
   "Weight"             NUMBER(15,6),
   "Volume"             NUMBER(15,6),
   constraint PK_SUPPLIERSHIPPINGDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SupplierShippingOrder"                               */
/*==============================================================*/
create table "SupplierShippingOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "PartsSupplierId"    NUMBER(9)                       not null,
   "PartsSupplierCode"  VARCHAR2(50)                    not null,
   "PartsSupplierName"  VARCHAR2(100)                   not null,
   "DirectRecWarehouseId" NUMBER(9),
   "DirectRecWarehouseName" VARCHAR2(100),
   "IfDirectProvision"  NUMBER(1)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "BranchCode"         VARCHAR2(50)                    not null,
   "BranchName"         VARCHAR2(100)                   not null,
   "PartsPurchaseOrderId" NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryName" VARCHAR2(100)                   not null,
   "PartsPurchaseOrderCode" VARCHAR2(50)                    not null,
   "OriginalRequirementBillId" NUMBER(9)                       not null,
   "OriginalRequirementBillType" NUMBER(9)                       not null,
   "OriginalRequirementBillCode" VARCHAR2(50)                    not null,
   "ReceivingWarehouseId" NUMBER(9)                       not null,
   "ReceivingWarehouseName" VARCHAR2(100)                   not null,
   "ReceivingCompanyId" NUMBER(9),
   "ReceivingCompanyName" VARCHAR2(100),
   "ReceivingCompanyCode" VARCHAR2(100),
   "ReceivingAddress"   VARCHAR2(200),
   "LogisticCompany"    VARCHAR2(100),
   "Phone"              VARCHAR2(50),
   "VehicleLicensePlate" VARCHAR2(50),
   "Driver"             VARCHAR2(50),
   "PlanSource"         VARCHAR2(200),
   "ShippingMethod"     NUMBER(9)                       not null,
   "DeliveryBillNumber" VARCHAR2(50),
   "Status"             NUMBER(9)                       not null,
   "DirectProvisionFinished" NUMBER(1),
   "ShippingDate"       DATE,
   "ArrivalDate"        DATE,
   "LogisticArrivalDate" DATE,
   "RequestedDeliveryTime" DATE                            not null,
   "PlanDeliveryTime"   DATE                            not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "ConfirmorId"        NUMBER(9),
   "ConfirmorName"      VARCHAR2(100),
   "ConfirmationTime"   DATE,
   "RowVersion"         TIMESTAMP,
   "FirstConfirmationTime" DATE,
   "GPMSPurOrderCode"   VARCHAR2(50),
   "SAPPurchasePlanCode" VARCHAR2(50),
   "ERPSourceOrderCode" VARCHAR2(500),
   "TotalWeight"        NUMBER(15,6),
   "TotalVolume"        NUMBER(15,6),
   "ArriveMethod"       NUMBER(9),
   "ModifyStatus"       NUMBER(9),
   "InboundTime"        DATE,
   "ComfirmStatus"      NUMBER(9),
   "Path"               VARCHAR2(2000),
   "ModifyArrive"       DATE,
   "SupplierComfirmId"  NUMBER(9),
   "SupplierComfirm"    VARCHAR2(100),
   "SupplierComfirmDate" DATE,
   "SupplierModifyId"   NUMBER(9),
   "SupplierModify"     VARCHAR2(100),
   "SupplierModifyDate" DATE,
   "ApproveId"          NUMBER(9),
   "Approver"           VARCHAR2(100),
   "ApproveDate"        DATE,
   "RejectReason"       VARCHAR2(500),
   constraint PK_SUPPLIERSHIPPINGORDER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SupplierTraceCode"                                   */
/*==============================================================*/
create table "SupplierTraceCode"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "SubmitterId"        NUMBER(9),
   "SubmitterName"      VARCHAR2(100),
   "SubmitTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproverTime"       DATE,
   "CheckerId"          NUMBER(9),
   "CheckerName"        VARCHAR2(100),
   "CheckTime"          DATE,
   "Path"               VARCHAR2(2000),
   "ApproveComment"     VARCHAR2(200),
   constraint PK_SUPPLIERTRACECODE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SupplierTraceCode2"                                  */
/*==============================================================*/
create table "SupplierTraceCode2"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "SubmitterId"        NUMBER(9),
   "SubmitterName"      VARCHAR2(100),
   "SubmitTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproverTime"       DATE,
   "CheckerId"          NUMBER(9),
   "CheckerName"        VARCHAR2(100),
   "CheckTime"          DATE,
   "Path"               VARCHAR2(2000),
   "ApproveComment"     VARCHAR2(200),
   constraint PK_SUPPLIERTRACECODE2 primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SupplierTraceCodeDetail"                             */
/*==============================================================*/
create table "SupplierTraceCodeDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "SupplierTraceCodeId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9),
   "SparePartCode"      VARCHAR2(50),
   "SparePartName"      VARCHAR2(100),
   "PartsSupplierId"    NUMBER(9),
   "PartsSupplierCode"  VARCHAR2(50),
   "PartsSupplierName"  VARCHAR2(100),
   "PartsPurchaseOrderId" NUMBER(9),
   "PartsPurchaseOrderCode" VARCHAR2(50),
   "ShippingId"         NUMBER(9),
   "ShippingCode"       VARCHAR2(50),
   "TraceProperty"      NUMBER(9),
   "OldTraceCode"       VARCHAR2(100),
   "NewTraceCode"       VARCHAR2(100),
   constraint PK_SUPPLIERTRACECODEDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SupplierTraceCodeDetail2"                            */
/*==============================================================*/
create table "SupplierTraceCodeDetail2"  (
   "Id"                 NUMBER(9)                       not null,
   "SupplierTraceCodeId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9),
   "SparePartCode"      VARCHAR2(50),
   "SparePartName"      VARCHAR2(100),
   "PartsSupplierId"    NUMBER(9),
   "PartsSupplierCode"  VARCHAR2(50),
   "PartsSupplierName"  VARCHAR2(100),
   "PartsPurchaseOrderId" NUMBER(9),
   "PartsPurchaseOrderCode" VARCHAR2(50),
   "ShippingId"         NUMBER(9),
   "ShippingCode"       VARCHAR2(50),
   "TraceProperty"      NUMBER(9),
   "OldTraceCode"       VARCHAR2(100),
   "NewTraceCode"       VARCHAR2(100),
   constraint PK_SUPPLIERTRACECODEDETAIL2 primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SupplierTraceDetail"                                 */
/*==============================================================*/
create table "SupplierTraceDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "ShippingDetailId"   NUMBER(9)                       not null,
   "SIHCode"            VARCHAR2(200),
   "TracesCode"         VARCHAR2(200),
   "BoxCode"            VARCHAR2(200),
   "Quantity"           NUMBER(9),
   constraint PK_SUPPLIERTRACEDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "TemPurchaseOrder"                                    */
/*==============================================================*/
create table "TemPurchaseOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "TemPurchasePlanOrderId" NUMBER(9)                       not null,
   "TemPurchasePlanOrderCode" VARCHAR2(50)                    not null,
   "WarehouseId"        NUMBER(9),
   "WarehouseName"      VARCHAR2(100),
   "Status"             NUMBER(9)                       not null,
   "ApproveStatus"      NUMBER(9),
   "SuplierId"          NUMBER(9),
   "SuplierCode"        VARCHAR2(50),
   "SuplierName"        VARCHAR2(100),
   "OrderType"          NUMBER(9),
   "IsTurnSale"         NUMBER(1),
   "ReceCompanyId"      NUMBER(9),
   "ReceCompanyCode"    VARCHAR2(50),
   "ReceCompanyName"    VARCHAR2(100),
   "OrderCompanyId"     NUMBER(9),
   "OrderCompanyCode"   VARCHAR2(50),
   "OrderCompanyName"   VARCHAR2(100),
   "ShippingMethod"     NUMBER(9),
   "ReceiveStatus"      NUMBER(9),
   "PartsSalesOrderCode" VARCHAR2(50),
   "PartsPurchaseOrderCode" VARCHAR2(50),
   "BuyerId"            NUMBER(9),
   "Buyer"              VARCHAR2(50),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "Memo"               VARCHAR2(200),
   "SubmitterId"        NUMBER(9),
   "SubmitterName"      VARCHAR2(100),
   "SubmitTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "CheckerId"          NUMBER(9),
   "CheckerName"        VARCHAR2(100),
   "CheckTime"          DATE,
   "ConfirmerId"        NUMBER(9),
   "Confirmer"          VARCHAR2(100),
   "ConfirmeTime"       DATE,
   "ShippingId"         NUMBER(9),
   "Shipper"            VARCHAR2(50),
   "ShippTime"          DATE,
   "StopperId"          NUMBER(9),
   "Stopper"            VARCHAR2(50),
   "StopTime"           DATE,
   "StopReason"         VARCHAR2(500),
   "ForcederId"         NUMBER(9),
   "Forcedder"          VARCHAR2(50),
   "ForcedTime"         DATE,
   "ForcedReason"       VARCHAR2(500),
   "ReceiveAddress"     VARCHAR2(200),
   "Linker"             VARCHAR2(50),
   "LinkPhone"          VARCHAR2(50),
   "CustomerType"       NUMBER(9),
   "FreightType"        NUMBER(9),
   "RejectReason"       VARCHAR2(200),
   constraint PK_TEMPURCHASEORDER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "TemPurchaseOrderDetail"                              */
/*==============================================================*/
create table "TemPurchaseOrderDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "TemPurchaseOrderId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9),
   "SparePartCode"      VARCHAR2(50),
   "SparePartName"      VARCHAR2(100),
   "SupplierPartCode"   VARCHAR2(50),
   "PlanAmount"         NUMBER(9)                       not null,
   "MeasureUnit"        VARCHAR2(50),
   "ConfirmedAmount"    NUMBER(9)                       not null,
   "ShippingAmount"     NUMBER(9),
   "Remark"             VARCHAR2(200),
   "ShortSupReason"     NUMBER(9),
   constraint PK_TEMPURCHASEORDERDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "TemPurchasePlanOrder"                                */
/*==============================================================*/
create table "TemPurchasePlanOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "WarehouseId"        NUMBER(9),
   "WarehouseName"      VARCHAR2(100),
   "Status"             NUMBER(9)                       not null,
   "IsTurnSale"         NUMBER(1),
   "PlanType"           NUMBER(9)                       not null,
   "ReceCompanyId"      NUMBER(9),
   "ReceCompanyCode"    VARCHAR2(50),
   "ReceCompanyName"    VARCHAR2(100),
   "CustomerType"       NUMBER(9),
   "OrderCompanyId"     NUMBER(9),
   "OrderCompanyCode"   VARCHAR2(50),
   "OrderCompanyName"   VARCHAR2(100),
   "ShippingMethod"     NUMBER(9),
   "IsReplace"          NUMBER(1),
   "Memo"               VARCHAR2(200),
   "FreightType"        NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "SubmitterId"        NUMBER(9),
   "SubmitterName"      VARCHAR2(100),
   "SubmitTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "CheckerId"          NUMBER(9),
   "ApproveTime"        DATE,
   "CheckerName"        VARCHAR2(100),
   "CheckTime"          DATE,
   "ConfirmerId"        NUMBER(9),
   "Confirmer"          VARCHAR2(100),
   "ConfirmeTime"       DATE,
   "RejectId"           NUMBER(9),
   "Rejector"           VARCHAR2(100),
   "RejectReason"       VARCHAR2(200),
   "RejectTime"         DATE,
   "Path"               VARCHAR2(2000),
   "ReceiveAddress"     VARCHAR2(200),
   "Linker"             VARCHAR2(50),
   "LinkPhone"          VARCHAR2(50),
   constraint PK_TEMPURCHASEPLANORDER primary key ("Id")
)
/

/*==============================================================*/
/* Table: "TemPurchasePlanOrderDetail"                          */
/*==============================================================*/
create table "TemPurchasePlanOrderDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "TemPurchasePlanOrderId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9),
   "SparePartCode"      VARCHAR2(50),
   "SparePartName"      VARCHAR2(100),
   "SupplierPartCode"   VARCHAR2(50),
   "ReferenceCode"      VARCHAR2(50),
   "PlanAmount"         NUMBER(9)                       not null,
   "MeasureUnit"        VARCHAR2(50),
   "SuplierId"          NUMBER(9),
   "SuplierCode"        VARCHAR2(50),
   "SuplierName"        VARCHAR2(100),
   "Memo"               VARCHAR2(200),
   "CodeSource"         NUMBER(9),
   constraint PK_TEMPURCHASEPLANORDERDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "TemShippingOrderDetail"                              */
/*==============================================================*/
create table "TemShippingOrderDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "ShippingOrderId"    NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "SupplierPartCode"   VARCHAR2(50),
   "MeasureUnit"        VARCHAR2(50),
   "Quantity"           NUMBER(9)                       not null,
   "ConfirmedAmount"    NUMBER(9),
   "Remark"             VARCHAR2(200),
   "Weight"             NUMBER(15,6),
   "Volume"             NUMBER(15,6),
   constraint PK_TEMSHIPPINGORDERDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "TemSupplierShippingOrder"                            */
/*==============================================================*/
create table "TemSupplierShippingOrder"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "PartsSupplierId"    NUMBER(9)                       not null,
   "PartsSupplierCode"  VARCHAR2(50)                    not null,
   "PartsSupplierName"  VARCHAR2(100)                   not null,
   "ReceivingWarehouseId" NUMBER(9),
   "ReceivingWarehouseName" VARCHAR2(100),
   "ReceivingCompanyId" NUMBER(9),
   "ReceivingCompanyName" VARCHAR2(100),
   "ReceivingCompanyCode" VARCHAR2(100),
   "ReceivingAddress"   VARCHAR2(200),
   "OriginalRequirementBillId" NUMBER(9)                       not null,
   "OriginalRequirementBillCode" VARCHAR2(50)                    not null,
   "LogisticCompany"    VARCHAR2(100),
   "Phone"              VARCHAR2(50),
   "VehicleLicensePlate" VARCHAR2(50),
   "Driver"             VARCHAR2(50),
   "ShippingMethod"     NUMBER(9)                       not null,
   "DeliveryBillNumber" VARCHAR2(50),
   "TotalWeight"        NUMBER(15,6),
   "TotalVolume"        NUMBER(15,6),
   "Status"             NUMBER(9)                       not null,
   "ShippingDate"       DATE,
   "PlanDeliveryTime"   DATE                            not null,
   "ArrivalDate"        DATE,
   "LogisticArrivalDate" DATE,
   "ConfirmorId"        NUMBER(9),
   "ConfirmorName"      VARCHAR2(100),
   "ConfirmationTime"   DATE,
   "FirstConfirmationTime" DATE,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "CloserId"           NUMBER(9),
   "CloserName"         VARCHAR2(100),
   "CloseTime"          DATE,
   "TemPurchaseOrderCode" VARCHAR2(50),
   "TemPurchaseOrderId" NUMBER(9),
   "ForceId"            NUMBER(9),
   "ForceName"          VARCHAR2(50),
   "ForceTime"          DATE,
   "Path"               VARCHAR2(2000),
   "OrderCompanyId"     NUMBER(9),
   "OrderCompanyCode"   VARCHAR2(50),
   "OrderCompanyName"   VARCHAR2(100),
   constraint PK_TEMSUPPLIERSHIPPINGORDER primary key ("Id")
)
/

