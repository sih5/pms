/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2018/7/3 16:00:57                            */
/*==============================================================*/


alter table "BonusPointsSummaryList"
   drop constraint FK_BONUSPOI_RELATIONS_BONUSPOI
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"AutoTaskExecutResult"');
  if num>0 then
    execute immediate 'drop table "AutoTaskExecutResult" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"BonusPointsSummary"');
  if num>0 then
    execute immediate 'drop table "BonusPointsSummary" cascade constraints';
  end if;
end;
/

drop index "Relationship_6_FK"
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"BonusPointsSummaryList"');
  if num>0 then
    execute immediate 'drop table "BonusPointsSummaryList" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsRebateAccount"');
  if num>0 then
    execute immediate 'drop table "PartsRebateAccount" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsRebateApplication"');
  if num>0 then
    execute immediate 'drop table "PartsRebateApplication" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsRebateChangeDetail"');
  if num>0 then
    execute immediate 'drop table "PartsRebateChangeDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsRebateType"');
  if num>0 then
    execute immediate 'drop table "PartsRebateType" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsSalesRtnSettlement"');
  if num>0 then
    execute immediate 'drop table "PartsSalesRtnSettlement" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsSalesRtnSettlementDetail"');
  if num>0 then
    execute immediate 'drop table "PartsSalesRtnSettlementDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsSalesRtnSettlementRef"');
  if num>0 then
    execute immediate 'drop table "PartsSalesRtnSettlementRef" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsSalesSettlement"');
  if num>0 then
    execute immediate 'drop table "PartsSalesSettlement" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsSalesSettlementDetail"');
  if num>0 then
    execute immediate 'drop table "PartsSalesSettlementDetail" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"PartsSalesSettlementRef"');
  if num>0 then
    execute immediate 'drop table "PartsSalesSettlementRef" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SaleNoSettleTable"');
  if num>0 then
    execute immediate 'drop table "SaleNoSettleTable" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SalesRtnSettleInvoiceRel"');
  if num>0 then
    execute immediate 'drop table "SalesRtnSettleInvoiceRel" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('"SettlementAutomaticTaskSet"');
  if num>0 then
    execute immediate 'drop table "SettlementAutomaticTaskSet" cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_AutoTaskExecutResult"');
  if num>0 then
    execute immediate 'drop sequence "S_AutoTaskExecutResult"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_BonusPointsSummary"');
  if num>0 then
    execute immediate 'drop sequence "S_BonusPointsSummary"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_BonusPointsSummaryList"');
  if num>0 then
    execute immediate 'drop sequence "S_BonusPointsSummaryList"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsRebateAccount"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsRebateAccount"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsRebateApplication"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsRebateApplication"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsRebateChangeDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsRebateChangeDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsRebateType"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsRebateType"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsSalesRtnSettlement"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsSalesRtnSettlement"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsSalesRtnSettlementDtl"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsSalesRtnSettlementDtl"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsSalesRtnSettlementRef"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsSalesRtnSettlementRef"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsSalesSettlement"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsSalesSettlement"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsSalesSettlementDetail"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsSalesSettlementDetail"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_PartsSalesSettlementRef"');
  if num>0 then
    execute immediate 'drop sequence "S_PartsSalesSettlementRef"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SaleNoSettleTable"');
  if num>0 then
    execute immediate 'drop sequence "S_SaleNoSettleTable"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SalesRtnSettleInvoiceRel"');
  if num>0 then
    execute immediate 'drop sequence "S_SalesRtnSettleInvoiceRel"';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('"S_SettlementAutomaticTaskSet"');
  if num>0 then
    execute immediate 'drop sequence "S_SettlementAutomaticTaskSet"';
  end if;
end;
/

create sequence "S_AutoTaskExecutResult"
/

create sequence "S_BonusPointsSummary"
/

create sequence "S_BonusPointsSummaryList"
/

create sequence "S_PartsRebateAccount"
/

create sequence "S_PartsRebateApplication"
/

create sequence "S_PartsRebateChangeDetail"
/

create sequence "S_PartsRebateType"
/

create sequence "S_PartsSalesRtnSettlement"
/

create sequence "S_PartsSalesRtnSettlementDtl"
/

create sequence "S_PartsSalesRtnSettlementRef"
/

create sequence "S_PartsSalesSettlement"
/

create sequence "S_PartsSalesSettlementDetail"
/

create sequence "S_PartsSalesSettlementRef"
/

create sequence "S_SaleNoSettleTable"
/

create sequence "S_SalesRtnSettleInvoiceRel"
/

create sequence "S_SettlementAutomaticTaskSet"
/

/*==============================================================*/
/* Table: "AutoTaskExecutResult"                                */
/*==============================================================*/
create table "AutoTaskExecutResult"  (
   "Id"                 NUMBER(9)                       not null,
   "SettlementAutoTaskSetId" NUMBER(9),
   "SourceCode"         VARCHAR2(50),
   "BrandId"            NUMBER(9),
   "BrandName"          VARCHAR2(100),
   "CompanyId"          NUMBER(9),
   "CompanyCode"        VARCHAR2(100),
   "CompanyName"        VARCHAR2(100),
   "ExecutionStatus"    NUMBER(9),
   "ExecutionTime"      DATE,
   "ExecutionLoseReason" VARCHAR2(200),
   constraint PK_AUTOTASKEXECUTRESULT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "BonusPointsSummary"                                  */
/*==============================================================*/
create table "BonusPointsSummary"  (
   "Id"                 NUMBER(9)                       not null,
   "BonusPointsSummaryCode" VARCHAR2(50)                    not null,
   "BrandId"            NUMBER(9),
   "BrandName"          VARCHAR2(50),
   "Status"             NUMBER(9),
   "SummaryBonusPoints" NUMBER(9),
   "SummaryAmount"      NUMBER(19,4),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(50),
   "CreateTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(50),
   "ApproveTime"        DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(50),
   "AbandonTime"        DATE,
   "CutTime"            DATE,
   "TaxRate"            NUMBER(15,6),
   "Tax"                NUMBER(19,4),
   constraint PK_BONUSPOINTSSUMMARY primary key ("Id")
)
/

/*==============================================================*/
/* Table: "BonusPointsSummaryList"                              */
/*==============================================================*/
create table "BonusPointsSummaryList"  (
   "Id"                 NUMBER(9)                       not null,
   "BonusPointsSummaryId" NUMBER(9)                       not null,
   "BonusPointsId"      NUMBER(9),
   "BonusPointsOrderCode" VARCHAR2(50),
   "PlatForm_Code"      VARCHAR2(50),
   "ServiceApplyCode"   VARCHAR2(50),
   "BonusPoints"        NUMBER(9),
   "BonusPointsAmount"  NUMBER(19,4),
   "SouceType"          NUMBER(9),
   "BonusPointsTime"    DATE,
   constraint PK_BONUSPOINTSSUMMARYLIST primary key ("Id")
)
/

/*==============================================================*/
/* Index: "Relationship_6_FK"                                   */
/*==============================================================*/
create index "Relationship_6_FK" on "BonusPointsSummaryList" (
   "BonusPointsSummaryId" ASC
)
/

/*==============================================================*/
/* Table: "PartsRebateAccount"                                  */
/*==============================================================*/
create table "PartsRebateAccount"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "AccountGroupId"     NUMBER(9)                       not null,
   "CustomerCompanyId"  NUMBER(9)                       not null,
   "CustomerCompanyCode" VARCHAR2(50)                    not null,
   "CustomerCompanyName" VARCHAR2(100)                   not null,
   "AccountBalanceAmount" NUMBER(19,4)                    not null,
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_PARTSREBATEACCOUNT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsRebateApplication"                              */
/*==============================================================*/
create table "PartsRebateApplication"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "CustomerCompanyId"  NUMBER(9)                       not null,
   "CustomerCompanyCode" VARCHAR2(50)                    not null,
   "CustomerCompanyName" VARCHAR2(100)                   not null,
   "BranchId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsRebateTypeId"  NUMBER(9)                       not null,
   "AccountGroupId"     NUMBER(9)                       not null,
   "RebateDirection"    NUMBER(9)                       not null,
   "Amount"             NUMBER(19,4)                    not null,
   "ApprovalComment"    VARCHAR2(200),
   "Motive"             VARCHAR2(200)                   not null,
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_PARTSREBATEAPPLICATION primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsRebateChangeDetail"                             */
/*==============================================================*/
create table "PartsRebateChangeDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "SourceId"           NUMBER(9)                       not null,
   "SourceCode"         VARCHAR2(50)                    not null,
   "SourceType"         NUMBER(9)                       not null,
   "PartsRebateAccountId" NUMBER(9)                       not null,
   "CustomerCompanyId"  NUMBER(9)                       not null,
   "AccountGroupId"     NUMBER(9)                       not null,
   "Amount"             NUMBER(19,4)                    not null,
   "Summary"            VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   constraint PK_PARTSREBATECHANGEDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsRebateType"                                     */
/*==============================================================*/
create table "PartsRebateType"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9)                       not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "Name"               VARCHAR2(100)                   not null,
   "Remark"             VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "RowVersion"         TIMESTAMP,
   constraint PK_PARTSREBATETYPE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsSalesRtnSettlement"                             */
/*==============================================================*/
create table "PartsSalesRtnSettlement"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "SettlementPath"     NUMBER(9)                       not null,
   "SalesCompanyId"     NUMBER(9)                       not null,
   "SalesCompanyCode"   VARCHAR2(50)                    not null,
   "SalesCompanyName"   VARCHAR2(100)                   not null,
   "CustomerCompanyId"  NUMBER(9)                       not null,
   "CustomerCompanyCode" VARCHAR2(50)                    not null,
   "CustomerCompanyName" VARCHAR2(100)                   not null,
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryName" VARCHAR2(100)                   not null,
   "AccountGroupId"     NUMBER(9)                       not null,
   "AccountGroupCode"   VARCHAR2(50)                    not null,
   "AccountGroupName"   VARCHAR2(100)                   not null,
   "CustomerAccountId"  NUMBER(9)                       not null,
   "TotalSettlementAmount" NUMBER(19,4)                    not null,
   "InvoiceAmountDifference" NUMBER(19,4)                    not null,
   "OffsettedSettlementBillId" NUMBER(9),
   "OffsettedSettlementBillCode" VARCHAR2(50),
   "TaxRate"            NUMBER(15,6)                    not null,
   "Tax"                NUMBER(19,4)                    not null,
   "DiscountRate"       NUMBER(15,6),
   "Discount"           NUMBER(19,4),
   "InvoicePath"        NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "InvoiceRegistrationOperatorId" NUMBER(9),
   "InvoiceRegistrationOperator" VARCHAR2(100),
   "InvoiceRegistrationTime" DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "RowVersion"         TIMESTAMP,
   "InvoiceDate"        DATE,
   "SettleType"         NUMBER(9),
   "BusinessType"       NUMBER(9),
   constraint PK_PARTSSALESRTNSETTLEMENT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsSalesRtnSettlementDetail"                       */
/*==============================================================*/
create table "PartsSalesRtnSettlementDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSalesRtnSettlementId" NUMBER(9)                       not null,
   "SerialNumber"       NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "OriginalPrice"      NUMBER(19,4)                    not null,
   "SettlementPrice"    NUMBER(19,4)                    not null,
   "QuantityToSettle"   NUMBER(9)                       not null,
   "SettlementAmount"   NUMBER(19,4)                    not null,
   "Remark"             VARCHAR2(200),
   constraint PK_PARTSSALESRTNSETTLEMENTDETA primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsSalesRtnSettlementRef"                          */
/*==============================================================*/
create table "PartsSalesRtnSettlementRef"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSalesRtnSettlementId" NUMBER(9)                       not null,
   "SourceId"           NUMBER(9)                       not null,
   "SourceCode"         VARCHAR2(50)                    not null,
   "SettlementAmount"   NUMBER(19,4)                    not null,
   "SourceBillCreateTime" DATE                            not null,
   "WarehouseId"        NUMBER(9)                       not null,
   "WarehouseName"      VARCHAR2(200)                   not null,
   constraint PK_PARTSSALESRTNSETTLEMENTREF primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsSalesSettlement"                                */
/*==============================================================*/
create table "PartsSalesSettlement"  (
   "Id"                 NUMBER(9)                       not null,
   "Code"               VARCHAR2(50)                    not null,
   "SettlementPath"     NUMBER(9)                       not null,
   "SalesCompanyId"     NUMBER(9)                       not null,
   "SalesCompanyCode"   VARCHAR2(50)                    not null,
   "SalesCompanyName"   VARCHAR2(100)                   not null,
   "WarehouseId"        NUMBER(9),
   "PartsSalesCategoryId" NUMBER(9)                       not null,
   "PartsSalesCategoryName" VARCHAR2(100)                   not null,
   "AccountGroupId"     NUMBER(9)                       not null,
   "AccountGroupCode"   VARCHAR2(50)                    not null,
   "AccountGroupName"   VARCHAR2(100)                   not null,
   "CustomerCompanyId"  NUMBER(9)                       not null,
   "CustomerCompanyCode" VARCHAR2(50)                    not null,
   "CustomerCompanyName" VARCHAR2(100)                   not null,
   "CustomerAccountId"  NUMBER(9)                       not null,
   "TotalSettlementAmount" NUMBER(19,4)                    not null,
   "RebateMethod"       NUMBER(9),
   "RebateAmount"       NUMBER(19,4),
   "InvoiceAmountDifference" NUMBER(19,4)                    not null,
   "OffsettedSettlementBillId" NUMBER(9),
   "OffsettedSettlementBillCode" VARCHAR2(50),
   "TaxRate"            NUMBER(15,6)                    not null,
   "Tax"                NUMBER(19,4)                    not null,
   "Status"             NUMBER(9)                       not null,
   "Remark"             VARCHAR2(200),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   "AbandonerId"        NUMBER(9),
   "AbandonerName"      VARCHAR2(100),
   "AbandonTime"        DATE,
   "InvoiceRegistrationOperatorId" NUMBER(9),
   "InvoiceRegistrationOperator" VARCHAR2(100),
   "InvoiceRegistrationTime" DATE,
   "ApproverId"         NUMBER(9),
   "ApproverName"       VARCHAR2(100),
   "ApproveTime"        DATE,
   "RowVersion"         TIMESTAMP,
   "IsUnifiedSettle"    NUMBER(1),
   "InvoiceDate"        DATE,
   "SettleType"         NUMBER(9),
   "BusinessType"       NUMBER(9),
   constraint PK_PARTSSALESSETTLEMENT primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsSalesSettlementDetail"                          */
/*==============================================================*/
create table "PartsSalesSettlementDetail"  (
   "Id"                 NUMBER(9)                       not null,
   "SerialNumber"       NUMBER(9)                       not null,
   "PartsSalesSettlementId" NUMBER(9)                       not null,
   "SparePartId"        NUMBER(9)                       not null,
   "SparePartCode"      VARCHAR2(50)                    not null,
   "SparePartName"      VARCHAR2(100)                   not null,
   "PriceBeforeRebate"  NUMBER(19,4)                    not null,
   "DiscountedPrice"    NUMBER(19,4)                    not null,
   "SettlementPrice"    NUMBER(19,4)                    not null,
   "QuantityToSettle"   NUMBER(9)                       not null,
   "SettlementAmount"   NUMBER(19,4)                    not null,
   "Remark"             VARCHAR2(200),
   "CatisClassifyId"    NUMBER(9),
   "CatisClassifyCode"  VARCHAR2(50),
   "CatisClassifyName"  VARCHAR2(50),
   constraint PK_PARTSSALESSETTLEMENTDETAIL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "PartsSalesSettlementRef"                             */
/*==============================================================*/
create table "PartsSalesSettlementRef"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSalesSettlementId" NUMBER(9)                       not null,
   "WarehouseId"        NUMBER(9)                       not null,
   "WarehouseName"      VARCHAR2(200)                   not null,
   "SourceId"           NUMBER(9)                       not null,
   "SourceCode"         VARCHAR2(50)                    not null,
   "SourceType"         NUMBER(9)                       not null,
   "SettlementAmount"   NUMBER(19,4)                    not null,
   "SourceBillCreateTime" DATE                            not null,
   "ReturnReason"       VARCHAR2(100),
   constraint PK_PARTSSALESSETTLEMENTREF primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SaleNoSettleTable"                                   */
/*==============================================================*/
create table "SaleNoSettleTable"  (
   "Id"                 NUMBER(9)                       not null,
   "StorageCompanyCode" VARCHAR2(50),
   "PartsSalesCategoryId" NUMBER(9),
   "Code"               VARCHAR2(50),
   "WarehouseId"        NUMBER(9),
   "WarehouseName"      VARCHAR2(100),
   "CounterPartCompanyName" VARCHAR2(100),
   "OutBoundType"       NUMBER(9),
   "OutBoundName"       VARCHAR2(8),
   "IsSettled"          VARCHAR2(6),
   "Total"              NUMBER(19,4),
   "TotalJH"            NUMBER(19,4),
   "TotalNoTax"         NUMBER(19,4),
   "SourceCode"         VARCHAR2(50),
   "CreateTime"         DATE,
   "SYNCDate"           VARCHAR2(100),
   constraint PK_SALENOSETTLETABLE primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SalesRtnSettleInvoiceRel"                            */
/*==============================================================*/
create table "SalesRtnSettleInvoiceRel"  (
   "Id"                 NUMBER(9)                       not null,
   "PartsSalesRtnSettlementId" NUMBER(9)                       not null,
   "InvoiceId"          NUMBER(9)                       not null,
   constraint PK_SALESRTNSETTLEINVOICEREL primary key ("Id")
)
/

/*==============================================================*/
/* Table: "SettlementAutomaticTaskSet"                          */
/*==============================================================*/
create table "SettlementAutomaticTaskSet"  (
   "Id"                 NUMBER(9)                       not null,
   "BranchId"           NUMBER(9),
   "BrandId"            NUMBER(9),
   "BrandName"          VARCHAR2(100),
   "AutomaticSettleTime" DATE,
   "SettleStartTime"    DATE,
   "SettleEndTime"      DATE,
   "Status"             NUMBER(9),
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   "ModifierId"         NUMBER(9),
   "ModifierName"       VARCHAR2(100),
   "ModifyTime"         DATE,
   constraint PK_SETTLEMENTAUTOMATICTASKSET primary key ("Id")
)
/

alter table "BonusPointsSummaryList"
   add constraint FK_BONUSPOI_RELATIONS_BONUSPOI foreign key ("BonusPointsSummaryId")
      references "BonusPointsSummary" ("Id")
/

