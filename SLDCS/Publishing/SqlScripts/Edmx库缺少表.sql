create table "ServiceOrderFixed"
(
  "Id"                 NUMBER(9) not null,
  "ServiceOrderId"     NUMBER(9) not null,
  "ServiceOrderCode"   VARCHAR2(32),
  "ServiceStationCode" VARCHAR2(12),
  "ServiceStationName" VARCHAR2(100),
  "CarType"            VARCHAR2(50),
  "Vin"                VARCHAR2(16),
  "MaterielType"       NUMBER(4),
  "MaterielCode"       VARCHAR2(20) not null,
  "MaterielName"       VARCHAR2(100),
  "OldMaterielCode"    VARCHAR2(20),
  "OldMaterielName"    VARCHAR2(50),
  "SupplyModel"       NUMBER(4),
  "MaterielNum"        NUMBER(8,2),
  "UnitPrice"          NUMBER(10,2),
  "PartCode"           VARCHAR2(20),
  "PartCodeDesc"       VARCHAR2(40),
  "ServiceTypeCode"    NUMBER(2),
  "ServiceTypeName"    VARCHAR2(40),
  "ServiceStatus"      NUMBER(4),
  "TransTime"          DATE,
  constraint PK_SERVICEORDERFIXED primary key ("Id")
);

create table "SupplierStockCycle"(
    "Id" number(9)  PRIMARY KEY, --改为小写Id
    "SupplierId" number(9) not null,--供应商ID
    "PartId"  number(9) not null,--配件ID
    "RelationId" number(9) not null,--配件-供应商关系ID
    "PartsSupplierCode"  varchar2(60) not null ,--供应商编号 60
    "PartsSupplierName" varchar2(100),--供应商名称  160
    "SupplierPartCode"  varchar2(100) ,--供应商图号
    "SparePartCode" varchar2(100) not null,--配件图号
    "SparePartName" varchar2(200)  ,--配件名称 200
    "StockDays"  number(4) default(15)  ,--备货天数（默认15天）
    "SafeDays"  number(4) default(7),--安全天数（默认7天）
    "UpperLimitDays" number  default(30) ,--上限天数（默认30天）
    "SaveTime" Date ,--推送日期（取执行时间）
    "ModifierId" number(5),
    "ModifierName"  varchar2(100) ,
    "ModifyTime" date ,
    "Status" number(2) --是否可用
);

create table "SupplierPartsMonthlySales"(
    "Id" number(9)  PRIMARY KEY, --改为小写Id
    "SupplierId" number(9) not null,--供应商ID
    "PartsSupplierCode"  varchar2(32) not null ,--供应商编号
    "PartsSupplierName" varchar2(100),--供应商名称
    "SupplierPartCode"  varchar2(100) ,--供应商图号
    "PartId"  number(9) not null,--配件ID
    "SparePartCode" varchar2(100) not null,--配件图号
    "SparePartName" varchar2(50)  ,--配件名称
    "SalesCount"  number(8) default(0) ,--销售数量
    "SalesMonth"  varchar2(14),--销售月份 如2022/05
    "SaveTime" Date not null  --结转时间
);

create  table "SupplierPartsDailySales"(
	"Id" number(9)  PRIMARY KEY, --改为小写Id
    "SupplierId" number(9) not null,--供应商ID
    "PartsSupplierCode"  varchar2(32) not null ,--供应商编号
    "PartsSupplierName" varchar2(100),--供应商名称
    "SupplierPartCode"  varchar2(100) ,--供应商图号
    "PartId"  number(9) not null,--配件ID
    "SparePartCode" varchar2(100) not null,--配件图号
    "SparePartName" varchar2(50)  ,--配件名称
    "SalesCount"  number(8) default(0) ,--销售数量
    "SalesTime"  Date,--销售日期
    "SaveTime" Date not null  --结转时间
);
