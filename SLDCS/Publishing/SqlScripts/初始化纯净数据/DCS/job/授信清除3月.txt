
  
BEGIN 
dbms_scheduler.create_job('"授信清除3月"',
job_type=>'STORED_PROCEDURE', job_action=>
'dcs.TimingEmptyCustomerCredence'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('25-MAR-2014 12.00.00.000000000 AM +08:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=> 
'Freq=Yearly;Interval=1'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.enable('"授信清除3月"');
COMMIT; 
END; 
