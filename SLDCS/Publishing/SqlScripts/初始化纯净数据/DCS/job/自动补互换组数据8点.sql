BEGIN
  dbms_scheduler.create_job('"自动补互换组数据8点"',
                            job_type            => 'STORED_PROCEDURE',
                            job_action          => 'Prc_InsertPartsExchangeGroup',
                            number_of_arguments => 0,
                            start_date          => systimestamp at time zone '+8:00',
                            repeat_interval     => 'FREQ=DAILY;BYHOUR=8;BYMINUTE=0',
                            end_date            => NULL,
                            job_class           => '"DEFAULT_JOB_CLASS"',
                            enabled             => FALSE,
                            auto_drop           => FALSE,
                            comments            => NULL);
  dbms_scheduler.enable('"自动补互换组数据8点"');
  COMMIT;
END;