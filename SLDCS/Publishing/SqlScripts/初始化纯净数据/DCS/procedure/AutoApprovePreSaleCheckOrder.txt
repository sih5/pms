create or replace procedure dcs.AutoApprovePreSaleCheckOrder as
begin

  declare
    cursor AutoApprove is(
      select p.Id PId, v.Id VId
        from PreSaleCheckOrder p
       inner join VehicleInformation v
          on p.VEHICLEID = v.Id

       where p.status = 2
         and p.SubmitTime >= sysdate - 1
         and ((v.PreSaleTime = 0) or  (v.PreSaleTime is null))
         and exists
       (select 1
                from Branchstrategy
               where Branchstrategy.Branchid = p.Branchid
               and Branchstrategy.Presalestrategy=1));

  begin
    for S_AutoApprove in AutoApprove loop
      begin
        update PreSaleCheckOrder
           set status                         = 3,
               PreSaleCheckOrder.Approverid   = 0,
               PreSaleCheckOrder.Approvername = '�Զ����',
               PreSaleCheckOrder.Approvetime  = sysdate
         where id = S_AutoApprove.PId;

        update VehicleInformation
           set PreSaleTime = nvl(PreSaleTime,0) + 1
         where id = S_AutoApprove.VId;
      end;
    end loop;
  end;

end AutoApprovePreSaleCheckOrder;