create or replace procedure dcs.InvalidDealerCreditLimitApp as
begin
  update VehicleCustomerAccount set CustomerCredenceAmount=0
   where exists (select * from VehicleDealerCreditLimitApp
          where VehicleCustomerAccount.CustomerCompanyId =  VehicleDealerCreditLimitApp.CustomerCompanyId
            and VehicleCustomerAccount.VehicleFundsTypeId =  VehicleDealerCreditLimitApp.VehicleFundsTypeId
            and VehicleDealerCreditLimitApp.status=3
            and to_date(VehicleDealerCreditLimitApp.ExpireDate, 'yyyy-mm-dd') = to_date(sysdate, 'yyyy-mm-dd'));

  update VehicleDealerCreditLimitApp set status=4
   where VehicleDealerCreditLimitApp.status=3
     and to_date(VehicleDealerCreditLimitApp.ExpireDate, 'yyyy-mm-dd') = to_date(sysdate, 'yyyy-mm-dd');
end;