create or replace procedure dcs.Prc_AutoGetPartSafeStock as
begin
  delete from   PartSafeStock;
  insert into PartSafeStock
    (id,
     BranchId,
     BranchName,
     PartsSalesCategoryId,
     PartsSalesCategoryName,
     PartId,
     PartCode,
     PartName,
     PrimarySupplierId,
     PrimarySupplierCode,
     PrimarySupplierName,
     OrderQuantityFor12M,
     CreatDuration,
     PartABC,
     PruductLifeCycle,
     PlannedPriceType,
     ServiceLevel,
     SafeCoefficient,
     NearFTWStandardDiff,
     NearFTWAverage,
     SafeStock)
    select S_PartSafeStock.Nextval,
           PartsBranch.BranchId,
           PartsBranch.Branchname,
           PartsBranch.PartsSalesCategoryId,
           PartsBranch.PartsSalesCategoryName,
           PartsBranch.Partid,
           PartsBranch.Partcode,
           PartsBranch.Partname,
           PartsSupplierRelation.SupplierId,
           PartsSupplierRelation.SupplierPartCode,
           PartsSupplierRelation.SupplierPartName,
           IntelligentOrderMonthly.FREQUENCYFOR12M as OrderQuantityFor12M ,--近12月出库频次
           ceil(months_between(sysdate, PartsBranch.Createtime)) as  CreatDuration , --向上取整  创建时长
           PartsBranch.PartABC,
           PartsBranch.ProductLifeCycle,
           PartsBranch.PlannedPriceCategory,
           PartsServiceLevel.ServiceLevel, --服务水平
           ServiceLevelBySafeCf.SafeCoefficient, --安全系数
           IntelligentOrderWeekly.DEVIATION, --近52周需求标准差
           IntelligentOrderWeekly.AVG, --近52周均
           power(exp(1),
                 ln(nvl(PartsSupplierRelation.OrderCycle, 0) +
                    nvl(PartsSupplierRelation.MonthlyArrivalPeriod, 0)) / 2) *
           (nvl(IntelligentOrderWeekly.DEVIATION, 0) / 7) *
           nvl(ServiceLevelBySafeCf.SafeCoefficient, 0) as safeStock --安全库存
      from PartsBranch
      left join PartsSupplierRelation
        on PartsSupplierRelation.BranchId = PartsBranch.BranchId
       and PartsSupplierRelation.PartsSalesCategoryId =
           PartsBranch.PartsSalesCategoryId
       and PartsBranch.Partid = PartsSupplierRelation.Partid
       and PartsSupplierRelation.IsPrimary=1
      left join IntelligentOrderMonthly --智能订货月度累计表
        on IntelligentOrderMonthly.BRANCHID = PartsBranch.BranchId
       and IntelligentOrderMonthly.SALESCATEGORYID =
           PartsBranch.PartsSalesCategoryId
       and IntelligentOrderMonthly.PARTID = PartsBranch.PartId
      left join IntelligentOrderWeekly --智能订货周度累计表
        on IntelligentOrderWeekly.BRANCHID = PartsBranch.BranchId
       and IntelligentOrderWeekly.SALESCATEGORYID =
           PartsBranch.PartsSalesCategoryId
       and IntelligentOrderWeekly.PARTID = PartsBranch.PartId
      left join PartsServiceLevel --配件服务水平
        on PartsServiceLevel.BranchId=PartsBranch.BranchId
       and PartsServiceLevel.PartsSalesCategoryId=PartsBranch.Partssalescategoryid
       and PartsServiceLevel.PartABC = PartsBranch.PartABC
       and PartsServiceLevel.StartDuration <=
           (months_between(sysdate, PartsBranch.Createtime) )
       and PartsServiceLevel.EndDuration >=
           (months_between(sysdate, PartsBranch.Createtime) )
       and PartsServiceLevel.PlannedPriceType =
           PartsBranch.PlannedPriceCategory
      left join ServiceLevelBySafeCf --配件服务水平对应安全系数   服务水平	ServiceLevel
        on ServiceLevelBySafeCf.ServiceLevel =
           PartsServiceLevel.ServiceLevel;

  end;