CREATE OR REPLACE PROCEDURE DCS.Prc_InsPartsInPlanToday IS
  FComplateId integer;
  FDateFormat date;
  FDateStr varchar2(8);
  FSerialLength integer;
  FNewCode varchar2(50);
  FcompanyCode varchar(50);
  v_Code varchar2(50);
  c_Code varchar2(50);
  cjh_Code varchar2(50);
  r_Code varchar2(50);
  rjh_Code varchar2(50);
  v_dbdid integer;
  v_ckjhid integer;
  v_ckdid integer;
  v_rkjhid integer;
  v_rkjyid integer;
  v_kcid integer;
  v_kccode varchar2(50);
  v_ckid integer;
  v_scid integer;
  v_sct varchar2(50);

CURSOR TPartsdispatch_CUR IS

select tmp1.warehouseid,tmp1.BCode,tmp1.code,tmp2.tcwarehouseid from (
select w1.id as warehouseid,b.code as BCode,c.code from Warehouse w1
left join Branch b on b.id=w1.branchid
left join Company c on c.id=w1.storagecompanyid
where w1.id in(1263,1261,1262)
)tmp1
inner join (
select w2.id as tcwarehouseid,(case w2.id when 7 then 1263 when 22 then 1261 when 38 then 1262 end) as warehouseid from Warehouse w2 where w2.id in(7,22,38)
) tmp2 on tmp1.warehouseid=tmp2.warehouseid where exists(select warehouseid from linshibiao l where tmp1.warehouseid=l.warehouseid);

TPartsdispatch_RECORD TPartsdispatch_CUR%ROWTYPE;

CURSOR TPPartsOutbound_CUR(fwarehouseid VARCHAR2) IS

select * from linshibiao where tcwarehouseid=fwarehouseid;

TPPartsOutbound_RECORD TPPartsOutbound_CUR%ROWTYPE;

BEGIN

  OPEN TPartsdispatch_CUR;
  LOOP
    FETCH TPartsdispatch_CUR
      INTO TPartsdispatch_RECORD;
    EXIT WHEN TPartsdispatch_CUR%NOTFOUND;
     --生成调拨单编号
        FcompanyCode:=TPartsdispatch_RECORD.BCode;
        FDateFormat:=trunc(sysdate,'DD');
        FDateStr:=to_char(sysdate,'yyyymmdd');
        FSerialLength:=6;
        FNewCode:='PT{CORPCODE}'||FDateStr||'{SERIAL}';
        /*获取编号生成模板Id*/
          begin
            select Id
              into FComplateId
              from codetemplate
             where name = 'PartsTransferOrder'
               and IsActived = 1;
          exception
            when NO_DATA_FOUND then
              raise_application_error(-20001, '未找到名称为"PartsTransferOrder"的有效编码规则。');
            when others then
              raise;
          end;

         select regexp_replace(
             regexp_replace(FNewCode, '{CORPCODE}', FcompanyCode),
              '{SERIAL}',
                lpad(to_char(GetSerial(FComplateId, FDateFormat, TPartsdispatch_RECORD.BCode)),FSerialLength,'0')
              ) Code into v_Code from dual;
--生成出库计划编号
        FcompanyCode:=TPartsdispatch_RECORD.code;
        FDateFormat:=trunc(sysdate,'DD');
        FDateStr:=to_char(sysdate,'yyyymmdd');
        FSerialLength:=6;
        FNewCode:='POP{CORPCODE}'||FDateStr||'{SERIAL}';
        /*获取编号生成模板Id*/
          begin
            select Id
              into FComplateId
              from codetemplate
             where name = 'PartsOutboundPlan'
               and IsActived = 1;
          exception
            when NO_DATA_FOUND then
              raise_application_error(-20001, '未找到名称为"PartsTransferOrder"的有效编码规则。');
            when others then
              raise;
          end;

         select regexp_replace(
             regexp_replace(FNewCode, '{CORPCODE}', FcompanyCode),
              '{SERIAL}',
                lpad(to_char(GetSerial(FComplateId, FDateFormat, TPartsdispatch_RECORD.code)),FSerialLength,'0')
              ) Code into cjh_Code from dual;
--生成出库单编号
        FcompanyCode:=TPartsdispatch_RECORD.code;
        FDateFormat:=trunc(sysdate,'DD');
        FDateStr:=to_char(sysdate,'yyyymmdd');
        FSerialLength:=6;
        FNewCode:='PO{CORPCODE}'||FDateStr||'{SERIAL}';
        /*获取编号生成模板Id*/
          begin
            select Id
              into FComplateId
              from codetemplate
             where name = 'PartsOutboundBill'
               and IsActived = 1;
          exception
            when NO_DATA_FOUND then
              raise_application_error(-20001, '未找到名称为"PartsTransferOrder"的有效编码规则。');
            when others then
              raise;
          end;

         select regexp_replace(
             regexp_replace(FNewCode, '{CORPCODE}', FcompanyCode),
              '{SERIAL}',
                lpad(to_char(GetSerial(FComplateId, FDateFormat, TPartsdispatch_RECORD.code)),FSerialLength,'0')
              ) Code into c_Code from dual;
 --生成入库检验单编号
         FcompanyCode:=TPartsdispatch_RECORD.code;
        FDateFormat:=trunc(sysdate,'DD');
        FDateStr:=to_char(sysdate,'yyyymmdd');
        FSerialLength:=6;
        FNewCode:='PIC{CORPCODE}'||FDateStr||'{SERIAL}';
        /*获取编号生成模板Id*/
          begin
            select Id
              into FComplateId
              from codetemplate
             where name = 'PartsInboundCheckBill'
               and IsActived = 1;
          exception
            when NO_DATA_FOUND then
              raise_application_error(-20001, '未找到名称为"PartsTransferOrder"的有效编码规则。');
            when others then
              raise;
          end;

         select regexp_replace(
             regexp_replace(FNewCode, '{CORPCODE}', FcompanyCode),
              '{SERIAL}',
                lpad(to_char(GetSerial(FComplateId, FDateFormat, TPartsdispatch_RECORD.code)),FSerialLength,'0')
              ) Code into r_Code from dual;
--生成入库计划编号
        FcompanyCode:=TPartsdispatch_RECORD.code;
        FDateFormat:=trunc(sysdate,'DD');
        FDateStr:=to_char(sysdate,'yyyymmdd');
        FSerialLength:=6;
        FNewCode:='PIP{CORPCODE}'||FDateStr||'{SERIAL}';
        /*获取编号生成模板Id*/
          begin
            select Id
              into FComplateId
              from codetemplate
             where name = 'PartsInboundPlan'
               and IsActived = 1;
          exception
            when NO_DATA_FOUND then
              raise_application_error(-20001, '未找到名称为"PartsTransferOrder"的有效编码规则。');
            when others then
              raise;
          end;

         select regexp_replace(
             regexp_replace(FNewCode, '{CORPCODE}', FcompanyCode),
              '{SERIAL}',
                lpad(to_char(GetSerial(FComplateId, FDateFormat, TPartsdispatch_RECORD.code)),FSerialLength,'0')
              ) Code into rjh_Code from dual;

--插入调拨单
select S_PartsTransferOrder.Nextval into v_dbdid from dual;
insert into PartsTransferOrder
  (Id,
   Code,
   Originalwarehouseid,
   Originalwarehousecode,
   Originalwarehousename,
   Storagecompanyid,
   Storagecompanytype,
   Destwarehouseid,
   Destwarehousecode,
   Destwarehousename,
   Totalamount,
   Status,
   Type,
   creatorid,
   Creatorname,
   Createtime)
  (select v_dbdid,
          v_Code,
          w.id,
          w.code,
          w.name,
          w.StorageCompanyId,
          w.storagecompanytype,
          whs.id,
          whs.code,
          whs.name,
          sum(ps.salesprice*l.transfornum),
          2,
          3,
          1,
          'Admin',
          sysdate
     from linshibiao l
    inner join Warehouse w
       on w.id = l.tcwarehouseid
    inner join Warehouse whs
       on whs.id = l.warehouseid
    inner join SalesUnitAffiWarehouse sw
       on sw.warehouseid = l.tcwarehouseid
    inner join SalesUnit su
       on su.id = sw.salesunitid
    inner join PartsSalesPrice ps
       on ps.sparepartid = l.partid
      and ps.partssalescategoryid = su.partssalescategoryid
    where l.tcwarehouseid = TPartsdispatch_RECORD.tcwarehouseid
    group by w.id,
             w.code,
             w.name,
             w.StorageCompanyId,
             w.storagecompanytype,
             whs.id,
             whs.code,
             whs.name);

--插入配件出库计划
select S_PartsOutboundPlan.Nextval into v_ckjhid from dual;
insert into PartsOutboundPlan
  (id,
   Code,
   Warehouseid,
   Warehousecode,
   Warehousename,
   Storagecompanyid,
   Storagecompanycode,
   Storagecompanyname,
   Storagecompanytype,
   Branchid,
   Branchcode,
   Branchname,
   PartsSalesCategoryId,
   Counterpartcompanyid,
   Counterpartcompanycode,
   Counterpartcompanyname,
   Receivingcompanyid,
   Receivingcompanycode,
   Receivingcompanyname,
   Sourceid,
   Sourcecode,
   Outboundtype,
   Originalrequirementbillid,
   Originalrequirementbilltype,
   Originalrequirementbillcode,
   Status,
   Ifwmsinterface,creatorid,Creatorname,Createtime)
  (select v_ckjhid,
          cjh_Code,
          w.id,
          w.code,
          w.name,
          w.storagecompanyid,
          c.code,
          c.name,
          w.storagecompanytype,
          w.branchid,
          b.code,
          b.name,
          su.PartsSalesCategoryId,
          cp.id,
          cp.code,
          cp.name,
          whs.storagecompanyid,
          cp.code,
          cp.name,
          v_dbdid,
          v_Code,
          4,
          v_dbdid,
          6,
          v_Code,
          3,
          w.wmsinterface,1,'Admin',sysdate
     from linshibiao l
    inner join warehouse w
       on l.tcwarehouseid = w.id
    inner join Company c
       on c.id = w.storagecompanyid
    inner join Branch b
       on b.id = w.branchid
    inner join SalesUnitAffiWarehouse suaw
       on suaw.warehouseid = w.id
    inner join SalesUnit su
       on su.id = suaw.salesunitid
    inner join warehouse whs
       on whs.id = l.warehouseid
    inner join Company cp
       on whs.storagecompanyid = cp.id
    where l.tcwarehouseid = TPartsdispatch_RECORD.tcwarehouseid
    group by w.id,
             w.code,
             w.name,
             w.storagecompanyid,
             c.code,
             c.name,
             w.storagecompanytype,
             w.branchid,
             b.code,
             b.name,
             su.PartsSalesCategoryId,
             cp.id,
             cp.code,
             cp.name,
             whs.storagecompanyid,
             cp.code,
             cp.name,
             w.wmsinterface);
--插入配件出库单
select S_PartsOutboundBill.Nextval into v_ckdid from dual;
insert into PartsOutboundBill
  (id,
   Code,
   Partsoutboundplanid,
   Warehouseid,
   Warehousecode,
   Warehousename,
   Storagecompanyid,
   Storagecompanycode,
   Storagecompanyname,
   Storagecompanytype,
   Branchid,
   Branchcode,
   Branchname,
   partssalescategoryid,
   Counterpartcompanyid,
   Counterpartcompanycode,
   Counterpartcompanyname,
   Receivingcompanyid,
   Receivingcompanycode,
   Receivingcompanyname,
   Outboundtype,
   Originalrequirementbillid,
   Originalrequirementbilltype,
   Originalrequirementbillcode,
   SettlementStatus,creatorid,Creatorname,Createtime)
  (select v_ckdid,
          c_Code,
          v_ckjhid,
          w.id,
          w.code,
          w.name,
          w.storagecompanyid,
          c.code,
          c.name,
          w.storagecompanytype,
          w.branchid,
          b.code,
          b.name,
          su.PartsSalesCategoryId,
          cp.id,
          cp.code,
          cp.name,
          whs.storagecompanyid,
          cp.code,
          cp.name,
          4,
          v_dbdid,
          6,
          v_Code,
          1,1,'Admin',sysdate
     from linshibiao l
    inner join warehouse w
       on l.tcwarehouseid = w.id
    inner join Company c
       on c.id = w.storagecompanyid
    inner join Branch b
       on b.id = w.branchid
    inner join SalesUnitAffiWarehouse suaw
       on suaw.warehouseid = w.id
    inner join SalesUnit su
       on su.id = suaw.salesunitid
    inner join warehouse whs
       on whs.id = l.warehouseid
    inner join Company cp
       on whs.storagecompanyid = cp.id
    where l.tcwarehouseid = TPartsdispatch_RECORD.tcwarehouseid
    group by w.id,
             w.code,
             w.name,
             w.storagecompanyid,
             c.code,
             c.name,
             w.storagecompanytype,
             w.branchid,
             b.code,
             b.name,
             su.PartsSalesCategoryId,
             cp.id,
             cp.code,
             cp.name,
             whs.storagecompanyid,
             cp.code,
             cp.name);

--插入配件入库计划
select S_PartsInboundPlan.Nextval into v_rkjhid from dual;
insert into PartsInboundPlan
  (id,
   Code,
   Warehouseid,
   Warehousecode,
   Warehousename,
   Storagecompanyid,
   Storagecompanycode,
   Storagecompanyname,
   Storagecompanytype,
   Branchid,
   Branchcode,
   Branchname,
   partssalescategoryid,
   Counterpartcompanyid,
   Counterpartcompanycode,
   Counterpartcompanyname,
   Sourceid,
   Sourcecode,
   InboundType,
   Originalrequirementbillid,
   Originalrequirementbilltype,
   Originalrequirementbillcode,
   Status,
   Ifwmsinterface,creatorid,Creatorname,Createtime)
  (select v_rkjhid,
          rjh_Code,
          w.id,
          w.code,
          w.name,
          w.storagecompanyid,
          c.code,
          c.name,
          w.storagecompanytype,
          w.branchid,
          b.code,
          b.name,
          su.PartsSalesCategoryId,
          cp.id,
          cp.code,
          cp.name,
          v_dbdid,
          v_Code,
          4,
          v_dbdid,
          6,
          v_Code,
          2,
          w.wmsinterface,1,'Admin',sysdate
     from linshibiao l
    inner join warehouse w
       on l.tcwarehouseid = w.id
    inner join Company c
       on c.id = w.storagecompanyid
    inner join Branch b
       on b.id = w.branchid
    inner join SalesUnitAffiWarehouse suaw
       on suaw.warehouseid = w.id
    inner join SalesUnit su
       on su.id = suaw.salesunitid
    inner join warehouse whs
       on whs.id = l.warehouseid
    inner join Company cp
       on whs.storagecompanyid = cp.id
    where l.warehouseid = TPartsdispatch_RECORD.warehouseid
    group by w.id,
             w.code,
             w.name,
             w.storagecompanyid,
             c.code,
             c.name,
             w.storagecompanytype,
             w.branchid,
             b.code,
             b.name,
             su.PartsSalesCategoryId,
             cp.id,
             cp.code,
             cp.name,
             w.wmsinterface);


--插入配件入库检验单
select S_PartsInboundCheckBill.Nextval into v_rkjyid from dual;
insert into PartsInboundCheckBill
  (id,
   Code,
   PartsInboundPlanId,
   Warehouseid,
   Warehousecode,
   Warehousename,
   Storagecompanyid,
   Storagecompanycode,
   Storagecompanyname,
   Storagecompanytype,
   Branchid,
   Branchcode,
   Branchname,
   partssalescategoryid,
   Counterpartcompanyid,
   Counterpartcompanycode,
   Counterpartcompanyname,
   InboundType,
   OriginalRequirementBillId,
   Originalrequirementbilltype,
   Originalrequirementbillcode,
   Status,
   SettlementStatus,creatorid,Creatorname,Createtime)
  (select v_rkjyid,
          r_Code,
          v_rkjhid,
          w.id,
          w.code,
          w.name,
          w.storagecompanyid,
          c.code,
          c.name,
          w.storagecompanytype,
          w.branchid,
          b.code,
          b.name,
          su.PartsSalesCategoryId,
          cp.id,
          cp.code,
          cp.name,
          4,
          v_dbdid,
          6,
          v_Code,
          1,
          1,1,'Admin',sysdate
     from linshibiao l
    inner join warehouse w
       on l.tcwarehouseid = w.id
    inner join Company c
       on c.id = w.storagecompanyid
    inner join Branch b
       on b.id = w.branchid
    inner join SalesUnitAffiWarehouse suaw
       on suaw.warehouseid = w.id
    inner join SalesUnit su
       on su.id = suaw.salesunitid
    inner join warehouse whs
       on whs.id = l.warehouseid
    inner join Company cp
       on whs.storagecompanyid = cp.id
    where l.warehouseid = TPartsdispatch_RECORD.warehouseid
    group by w.id,
             w.code,
             w.name,
             w.storagecompanyid,
             c.code,
             c.name,
             w.storagecompanytype,
             w.branchid,
             b.code,
             b.name,
             su.PartsSalesCategoryId,
             cp.id,
             cp.code,
             cp.name);



OPEN TPPartsOutbound_CUR(TPartsdispatch_RECORD.tcwarehouseid );
LOOP
  FETCH TPPartsOutbound_CUR
    INTO TPPartsOutbound_RECORD;
  EXIT WHEN TPPartsOutbound_CUR%NOTFOUND;

if TPPartsOutbound_RECORD.transfornum>TPPartsOutbound_RECORD.Quantity then
--插入配件出库清单
insert into PartsOutboundBillDetail
  (id,
   PartsOutboundBillId,
   Sparepartid,
   Sparepartcode,
   Sparepartname,
   OutboundAmount,
   WarehouseAreaId,
   SettlementPrice,
   CostPrice)values
  ( S_PartsOutboundBillDetail.Nextval,
          v_ckdid,
          TPPartsOutbound_RECORD.partid,
         ( select code from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
         ( select name from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
          TPPartsOutbound_RECORD.Quantity,
          TPPartsOutbound_RECORD.Warehouseareaid,
         (select SalesPrice from PartsSalesPrice ps
inner join SalesUnitAffiWarehouse sw on sw.warehouseid=TPPartsOutbound_RECORD.tcwarehouseid
inner join SalesUnit su on su.id=sw.salesunitid and ps.PartsSalesCategoryId=su.partssalescategoryid
where ps.sparepartid=TPPartsOutbound_RECORD.partid),(select PlannedPrice from PartsPlannedPrice ps
inner join SalesUnitAffiWarehouse sw on sw.warehouseid=TPPartsOutbound_RECORD.tcwarehouseid
inner join SalesUnit su on su.id=sw.salesunitid and ps.PartsSalesCategoryId=su.partssalescategoryid
where ps.sparepartid=TPPartsOutbound_RECORD.partid)
    );

--插入配件入库检验清单
select id,code into v_kcid,v_kccode from WarehouseArea wha where wha.warehouseid=TPartsdispatch_RECORD.warehouseid and rownum=1;

insert into PartsInboundCheckBillDetail
  (id,
   PartsInboundCheckBillId,
   Sparepartid,
   Sparepartcode,
   Sparepartname,
   WarehouseAreaId,
   Warehouseareacode,
   InspectedQuantity,
   SettlementPrice,
   CostPrice)values
  ( S_PartsInboundCheckBillDetail.Nextval,
          v_rkjyid,
          TPPartsOutbound_RECORD.partid,
          ( select code from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
         ( select name from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
          v_kcid,
          v_kccode,
         TPPartsOutbound_RECORD.Quantity,
         (select SalesPrice from PartsSalesPrice ps
inner join SalesUnitAffiWarehouse sw on sw.warehouseid=TPPartsOutbound_RECORD.warehouseid
inner join SalesUnit su on su.id=sw.salesunitid and ps.PartsSalesCategoryId=su.partssalescategoryid
where ps.sparepartid=TPPartsOutbound_RECORD.partid),(select PlannedPrice from PartsPlannedPrice ps
inner join SalesUnitAffiWarehouse sw on sw.warehouseid=TPPartsOutbound_RECORD.warehouseid
inner join SalesUnit su on su.id=sw.salesunitid and ps.PartsSalesCategoryId=su.partssalescategoryid
where ps.sparepartid=TPPartsOutbound_RECORD.partid))
    ;

--插入配件入库计划清单
insert into PartsInboundPlanDetail
  (id,
   PartsInboundPlanId,
   Sparepartid,
   Sparepartcode,
   Sparepartname,
   PlannedAmount,INSPECTEDQUANTITY,
   Price)values
  ( S_PartsInboundPlanDetail.Nextval,
          v_rkjhid,
          TPPartsOutbound_RECORD.PartId,
         ( select code from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
         ( select name from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
          TPPartsOutbound_RECORD.Quantity, TPPartsOutbound_RECORD.Quantity,
        (select SalesPrice from PartsSalesPrice ps
inner join SalesUnitAffiWarehouse sw on sw.warehouseid=TPPartsOutbound_RECORD.warehouseid
inner join SalesUnit su on su.id=sw.salesunitid and ps.PartsSalesCategoryId=su.partssalescategoryid
where ps.sparepartid=TPPartsOutbound_RECORD.partid));


--插入配件出库计划清单
insert into PartsOutboundPlanDetail
  (id,
   Partsoutboundplanid,
   Sparepartid,
   Sparepartcode,
   Sparepartname,
   PlannedAmount,OUTBOUNDFULFILLMENT,
   Price)values
  ( S_PartsOutboundPlanDetail.Nextval,
          v_ckjhid,
          TPPartsOutbound_RECORD.partid,
          ( select code from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
         ( select name from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
          TPPartsOutbound_RECORD.Quantity,   TPPartsOutbound_RECORD.Quantity,
  (select SalesPrice from PartsSalesPrice ps
inner join SalesUnitAffiWarehouse sw on sw.warehouseid=TPPartsOutbound_RECORD.tcwarehouseid
inner join SalesUnit su on su.id=sw.salesunitid and ps.PartsSalesCategoryId=su.partssalescategoryid
where ps.sparepartid=TPPartsOutbound_RECORD.partid));


--插入调拨单清单
insert into PartsTransferOrderDetail
  (id,
   Partstransferorderid,
   Sparepartid,
   Sparepartcode,
   Sparepartname,
   PlannedAmount,CONFIRMEDAMOUNT,
   Price)values
  ( S_PartsTransferOrderDetail.Nextval,
          v_dbdid,
          TPPartsOutbound_RECORD.partid,
         ( select code from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
         ( select name from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
          TPPartsOutbound_RECORD.Quantity,TPPartsOutbound_RECORD.Quantity,
        (select SalesPrice from PartsSalesPrice ps
inner join SalesUnitAffiWarehouse sw on sw.warehouseid=TPPartsOutbound_RECORD.tcwarehouseid
inner join SalesUnit su on su.id=sw.salesunitid and ps.PartsSalesCategoryId=su.partssalescategoryid
where ps.sparepartid=TPPartsOutbound_RECORD.partid));

--更新库存
update PartsStock ps set Quantity=nvl(ps.quantity,0)-(TPPartsOutbound_RECORD.Quantity)
where
ps.WarehouseId = TPPartsOutbound_RECORD.tcwarehouseid and
ps.partid = TPPartsOutbound_RECORD.partid and
ps.warehouseareaid=TPPartsOutbound_RECORD.Warehouseareaid;


select StorageCompanyId,StorageCompanyType,BranchId into v_scid,v_sct,v_ckid from PartsStock where PartsStock.Warehouseid=TPPartsOutbound_RECORD.Warehouseid and rownum=1;
   MERGE INTO PartsStock p
       USING (select ps.warehouseid,
                 ps.warehouseareaid                 as warehouseareaid,
                 ps.partid,
                 TPPartsOutbound_RECORD.Quantity as quantity
            from PartsStock ps inner join warehouseareacategory on ps.warehouseareacategoryid=warehouseareacategory.id
           where ps.WarehouseId = TPPartsOutbound_RECORD.warehouseid
             and ps.partid = TPPartsOutbound_RECORD.partid  and warehouseareacategory.category=1 and rownum=1) np
   ON (p.warehouseid = np.warehouseid and p.warehouseareaid = np.warehouseareaid and p.PartId = np.partid)
   WHEN MATCHED THEN
     UPDATE
        SET p.quantity = p.Quantity + np.quantity
      WHERE p.warehouseid = np.warehouseid
        and p.warehouseareaid = np.warehouseareaid
        and p.partid = np.partid
   WHEN NOT MATCHED THEN
     INSERT
       (id,
        WAREHOUSEID,
        STORAGECOMPANYID,
        STORAGECOMPANYTYPE,
        BRANCHID,
        WAREHOUSEAREAID,
        PARTID,
        QUANTITY)
     values
       (s_PartsStock.Nextval,
        np.warehouseid,
        v_scid,
        v_sct,
        v_ckid,
        v_kcid,
        TPPartsOutbound_RECORD.Partid,
        np.quantity);


--更新临时表数据
update linshibiao set transfornum=transfornum-Quantity where linshibiao.tcwarehouseid=TPPartsOutbound_RECORD.tcwarehouseid and linshibiao.partid=TPPartsOutbound_RECORD.Partid;

else
--插入配件出库清单
insert into PartsOutboundBillDetail
  (id,
   PartsOutboundBillId,
   Sparepartid,
   Sparepartcode,
   Sparepartname,
   OutboundAmount,
   WarehouseAreaId,
   SettlementPrice,
   CostPrice)values
  ( S_PartsOutboundBillDetail.Nextval,
          v_ckdid,
          TPPartsOutbound_RECORD.partid,
         ( select code from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
         ( select name from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
          TPPartsOutbound_RECORD.Transfornum,
          TPPartsOutbound_RECORD.Warehouseareaid,
         (select SalesPrice from PartsSalesPrice ps
inner join SalesUnitAffiWarehouse sw on sw.warehouseid=TPPartsOutbound_RECORD.tcwarehouseid
inner join SalesUnit su on su.id=sw.salesunitid and ps.PartsSalesCategoryId=su.partssalescategoryid
where ps.sparepartid=TPPartsOutbound_RECORD.partid),(select PlannedPrice from PartsPlannedPrice ps
inner join SalesUnitAffiWarehouse sw on sw.warehouseid=TPPartsOutbound_RECORD.tcwarehouseid
inner join SalesUnit su on su.id=sw.salesunitid and ps.PartsSalesCategoryId=su.partssalescategoryid
where ps.sparepartid=TPPartsOutbound_RECORD.partid)
    );

--插入配件入库检验清单
select id,code into v_kcid,v_kccode from WarehouseArea wha where wha.warehouseid=TPartsdispatch_RECORD.warehouseid and rownum=1;

insert into PartsInboundCheckBillDetail
  (id,
   PartsInboundCheckBillId,
   Sparepartid,
   Sparepartcode,
   Sparepartname,
   WarehouseAreaId,
   Warehouseareacode,
   InspectedQuantity,
   SettlementPrice,
   CostPrice)values
  ( S_PartsInboundCheckBillDetail.Nextval,
          v_rkjyid,
          TPPartsOutbound_RECORD.partid,
          ( select code from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
         ( select name from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
          v_kcid,
          v_kccode,
         TPPartsOutbound_RECORD.Transfornum,
         (select SalesPrice from PartsSalesPrice ps
inner join SalesUnitAffiWarehouse sw on sw.warehouseid=TPPartsOutbound_RECORD.warehouseid
inner join SalesUnit su on su.id=sw.salesunitid and ps.PartsSalesCategoryId=su.partssalescategoryid
where ps.sparepartid=TPPartsOutbound_RECORD.partid),(select PlannedPrice from PartsPlannedPrice ps
inner join SalesUnitAffiWarehouse sw on sw.warehouseid=TPPartsOutbound_RECORD.warehouseid
inner join SalesUnit su on su.id=sw.salesunitid and ps.PartsSalesCategoryId=su.partssalescategoryid
where ps.sparepartid=TPPartsOutbound_RECORD.partid))
    ;

--插入配件入库计划清单
insert into PartsInboundPlanDetail
  (id,
   PartsInboundPlanId,
   Sparepartid,
   Sparepartcode,
   Sparepartname,
   PlannedAmount,INSPECTEDQUANTITY,
   Price)values
  ( S_PartsInboundPlanDetail.Nextval,
          v_rkjhid,
          TPPartsOutbound_RECORD.PartId,
         ( select code from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
         ( select name from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
          TPPartsOutbound_RECORD.Transfornum, TPPartsOutbound_RECORD.Transfornum,
       (select SalesPrice from PartsSalesPrice ps
inner join SalesUnitAffiWarehouse sw on sw.warehouseid=TPPartsOutbound_RECORD.warehouseid
inner join SalesUnit su on su.id=sw.salesunitid and ps.PartsSalesCategoryId=su.partssalescategoryid
where ps.sparepartid=TPPartsOutbound_RECORD.partid));


--插入配件出库计划清单
insert into PartsOutboundPlanDetail
  (id,
   Partsoutboundplanid,
   Sparepartid,
   Sparepartcode,
   Sparepartname,
   PlannedAmount,OUTBOUNDFULFILLMENT,
   Price)values
  ( S_PartsOutboundPlanDetail.Nextval,
          v_ckjhid,
          TPPartsOutbound_RECORD.partid,
          ( select code from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
         ( select name from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
          TPPartsOutbound_RECORD.Transfornum,   TPPartsOutbound_RECORD.Transfornum,
  (select SalesPrice from PartsSalesPrice ps
inner join SalesUnitAffiWarehouse sw on sw.warehouseid=TPPartsOutbound_RECORD.tcwarehouseid
inner join SalesUnit su on su.id=sw.salesunitid and ps.PartsSalesCategoryId=su.partssalescategoryid
where ps.sparepartid=TPPartsOutbound_RECORD.partid));


--插入调拨单清单
insert into PartsTransferOrderDetail
  (id,
   Partstransferorderid,
   Sparepartid,
   Sparepartcode,
   Sparepartname,
   PlannedAmount,CONFIRMEDAMOUNT,
   Price)values
  ( S_PartsTransferOrderDetail.Nextval,
          v_dbdid,
          TPPartsOutbound_RECORD.partid,
         ( select code from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
         ( select name from SparePart sp where  sp.id = TPPartsOutbound_RECORD.partid),
          TPPartsOutbound_RECORD.Transfornum,TPPartsOutbound_RECORD.Transfornum,
        (select SalesPrice from PartsSalesPrice ps
inner join SalesUnitAffiWarehouse sw on sw.warehouseid=TPPartsOutbound_RECORD.tcwarehouseid
inner join SalesUnit su on su.id=sw.salesunitid and ps.PartsSalesCategoryId=su.partssalescategoryid
where ps.sparepartid=TPPartsOutbound_RECORD.partid));
--更新库存
update PartsStock ps set Quantity=nvl(ps.quantity,0)-(TPPartsOutbound_RECORD.Transfornum)
where ps.WarehouseId = TPPartsOutbound_RECORD.tcwarehouseid and
ps.partid = TPPartsOutbound_RECORD.partid and
ps.warehouseareaid=TPPartsOutbound_RECORD.Warehouseareaid;

select StorageCompanyId,StorageCompanyType,BranchId into v_scid,v_sct,v_ckid from warehouse where warehouse.id=TPPartsOutbound_RECORD.Warehouseid;
   MERGE INTO PartsStock p
   USING (select ps.warehouseid,
                 ps.warehouseareaid                 as warehouseareaid,
                 ps.partid,
                 TPPartsOutbound_RECORD.Transfornum as quantity
            from PartsStock ps inner join warehouseareacategory on ps.warehouseareacategoryid=warehouseareacategory.id
           where ps.WarehouseId = TPPartsOutbound_RECORD.warehouseid
             and ps.partid = TPPartsOutbound_RECORD.partid  and warehouseareacategory.category=1 and rownum=1) np
   ON (p.warehouseid = np.warehouseid and p.warehouseareaid = np.warehouseareaid and p.PartId = np.partid)
   WHEN MATCHED THEN
     UPDATE
        SET p.quantity = p.Quantity + np.quantity
      WHERE p.warehouseid = np.warehouseid
        and p.warehouseareaid = np.warehouseareaid
        and p.partid = np.partid
   WHEN NOT MATCHED THEN
     INSERT
       (id,
        WAREHOUSEID,
        STORAGECOMPANYID,
        STORAGECOMPANYTYPE,
        BRANCHID,
        WAREHOUSEAREAID,
        PARTID,
        QUANTITY)
     values
       (s_PartsStock.Nextval,
        np.warehouseid,
        v_scid,
        v_sct,
        v_ckid,
        v_kcid,
        TPPartsOutbound_RECORD.Partid,
        np.quantity);


end if;

END LOOP;
CLOSE TPPartsOutbound_CUR;

END LOOP;
CLOSE TPartsdispatch_CUR;

delete linshibiao;

END Prc_InsPartsInPlanToday;