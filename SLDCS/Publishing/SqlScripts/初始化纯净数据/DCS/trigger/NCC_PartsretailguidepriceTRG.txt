CREATE OR REPLACE TRIGGER DCS.NCC_PartsretailguidepriceTRG
AFTER INSERT OR UPDATE ON Partsretailguideprice
FOR EACH ROW
BEGIN
  INSERT INTO NCC_SparePart_SYNC VALUES(S_NCC_SparePart_SYNC.NEXTVAL,:NEW.ID,'Partsretailguideprice',sysdate);
END NCC_PartsretailguidepriceTRG;