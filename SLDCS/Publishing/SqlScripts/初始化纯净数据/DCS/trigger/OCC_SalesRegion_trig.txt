create or replace trigger DCS.OCC_SalesRegion_trig
  after INSERT or update ON  salesregion
  FOR EACH ROW
DECLARE
BEGIN
  merge into OCC_SalesRegion_SYNC bys
  using (select '1' from dual) N
  on (bys.billid = :new.id)
  WHEN MATCHED THEN
    UPDATE set syncnum = S_OCC_SALESREGION_INCREASE.nextval
  WHEN NOT MATCHED THEN
    INSERT
      (billid, syncnum)
    values
      (:new.id, S_OCC_SALESREGION_INCREASE.nextval);

END;