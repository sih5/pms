create or replace trigger DCS.PARTSLOCKEDSTOCK_trg
   after insert or update on PARTSLOCKEDSTOCK
   for each row
begin
  if :old.id is not null then
    insert into PARTSLOCKEDSTOCK_log
      (id,
       Warehouseid,
       Storagecompanyid,
       Branchid,
       Partid,
       Lockedquantity,
       Creatorid,
       Creatorname,
       Createtime,
       Lockedquantityold,syncdate)
    values
      (:new.id,
       :new.Warehouseid,
       :new.Storagecompanyid,
       :new.Branchid,
       :new.Partid,
       :new.Lockedquantity,
       :new.Creatorid,
       :new.Creatorname,
       :new.Createtime,
       :old.Lockedquantity,sysdate);
  else
    insert into PARTSLOCKEDSTOCK_log
      (id,
       Warehouseid,
       Storagecompanyid,
       Branchid,
       Partid,
       Lockedquantity,
       Creatorid,
       Creatorname,
       Createtime,
       Lockedquantityold,syncdate)
    values
      (:new.id,
       :new.Warehouseid,
       :new.Storagecompanyid,
       :new.Branchid,
       :new.Partid,
       :new.Lockedquantity,
       :new.Creatorid,
       :new.Creatorname,
       :new.Createtime,
       null,sysdate);
  end if;
end PARTSLOCKEDSTOCK_trg;