CREATE OR REPLACE TRIGGER DCS.QIS_RepairClaimBill_TRG
AFTER INSERT OR UPDATE ON RepairClaimBill
FOR EACH ROW
BEGIN
  INSERT INTO QIS_RepairClaimBill_SYNC VALUES(S_QIS_RepairClaimBill_SYNC.NEXTVAL,:NEW.ID,'RepairClaimBill',sysdate);
END QIS_RepairClaimBill_TRG;
/

prompt
prompt Creating trigger QTSINFO_TRG
prompt ============================
prompt
create or replace trigger DCS.QTSINFO_TRG
after insert on dcs.qtp_QTSINFO
for each row
begin
  if (:new.syscode = 'TEST' or :new.syscode = 'TYDMS') then
    Insert into  yxdcs.sep_qtp_QTSINFO
    values
      (:new.DETAILID,
       :new.CSCODE,
       :new.ZCBMCODE,
       :new.VINCODE,
       :new.ASSEMBLY_DATE,
       :new.BIND_DATE,
       :new.FACTORY,
       :new.MAPID,
       :new.MAPNAME,
       :new.PATCH,
       :new.PRODUCELINE,
       :new.SUBMITTIME,
       :new.CHUCHANGID,
       :new.SYSCODE,
       :new.THEDATE,
       :new.RECEIVETIME,
       :new.HANDLESTATUS,
       :new.HANDLETIME,
       :new.HANDLEMESSAGE,
       :new.ID);
  end if;
  if (:new.syscode = 'LS' or :new.syscode = 'FD') then
    Insert into dcs.sep_qtp_QTSINFO
    values
      (:new.DETAILID,
       :new.CSCODE,
       :new.ZCBMCODE,
       :new.VINCODE,
       :new.ASSEMBLY_DATE,
       :new.BIND_DATE,
       :new.FACTORY,
       :new.MAPID,
       :new.MAPNAME,
       :new.PATCH,
       :new.PRODUCELINE,
       :new.SUBMITTIME,
       :new.CHUCHANGID,
       :new.SYSCODE,
       :new.THEDATE,
       :new.RECEIVETIME,
       :new.HANDLESTATUS,
       :new.HANDLETIME,
       :new.HANDLEMESSAGE,
       :new.ID);
  end if;
end;