CREATE OR REPLACE TRIGGER DCS.SAP_PARTSPURCHASERTNSETTLE_TRG
AFTER INSERT OR UPDATE ON PARTSPURCHASERTNSETTLEBILL
FOR EACH ROW
declare
 lscompanyId number(9);
 yxcompanyId number(9);
 gccompanyId number(9);
 fdcompanyId number(9);
BEGIN
select companyId  into lscompanyid from (
select case (select count(*)
           from company
          where code = '2470'
            and status = 1
            and rownum = 1)
         when 1 then
          (select id
             from company
            where code = '2470'
              and status = 1
              and rownum = 1)
         else
          -1
       end as companyId
  from dual);
select companyId
  into yxcompanyid
  from (select case (select count(*)
                   from company
                  where code = '1101'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '1101'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
select companyId
  into gccompanyId
  from (select case (select count(*)
                   from company
                  where code = '2230'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2230'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
 select companyId
  into fdcompanyId
  from (select case (select count(*)
                   from company
                  where code = '2450'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2450'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);

  --营销
  IF(:NEW.STATUS=2  AND :NEW.SETTLEMENTPATH=1 AND :NEW.TOTALSETTLEMENTAMOUNT<>0 AND (:NEW.BranchId=yxcompanyId or :new.BranchId=lscompanyId or :new.BranchId=gccompanyId)AND :new.INVOICEPATH=2) THEN--SAP_3(采购退货结算)
    INSERT INTO SAP_3_SYNC VALUES(S_SAP_3_SYNC.NEXTVAL,:NEW.ID,'PARTSPURCHASERTNSETTLEBILL');
  END IF;
  IF(:NEW.STATUS=2  AND :NEW.SETTLEMENTPATH=1 AND :NEW.TOTALSETTLEMENTAMOUNT<>0 AND (:NEW.BranchId=yxcompanyId or :new.branchId=lscompanyId or :new.BranchId=gccompanyId) AND :new.INVOICEPATH=1) THEN--SAP_16(采购退货结算开红票)
    INSERT INTO SAP_16_SYNC VALUES(S_SAP_16_SYNC.NEXTVAL,:NEW.ID,'PARTSPURCHASERTNSETTLEBILL');
  END IF;
  --福戴
  IF(:NEW.STATUS=2  AND :NEW.SETTLEMENTPATH=1 AND :NEW.TOTALSETTLEMENTAMOUNT<>0 AND (:NEW.BranchId=fdcompanyId)AND :new.INVOICEPATH=2) THEN--SAP_3(采购退货结算)
    INSERT INTO SAP_3_SYNC_FD VALUES(S_SAP_3_SYNC_FD.NEXTVAL,:NEW.ID,'PARTSPURCHASERTNSETTLEBILL');
  END IF;
  IF(:NEW.STATUS=2  AND :NEW.SETTLEMENTPATH=1 AND :NEW.TOTALSETTLEMENTAMOUNT<>0 AND (:NEW.BranchId=fdcompanyId) AND :new.INVOICEPATH=1) THEN--SAP_16(采购退货结算开红票)
    INSERT INTO SAP_16_SYNC_FD VALUES(S_SAP_16_SYNC_FD.NEXTVAL,:NEW.ID,'PARTSPURCHASERTNSETTLEBILL');
  END IF;
END SAP_PARTSPURCHASERTNSETTLE_TRG;