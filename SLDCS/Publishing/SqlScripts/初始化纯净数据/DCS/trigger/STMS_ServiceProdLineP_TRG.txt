CREATE OR REPLACE TRIGGER DCS.STMS_ServiceProdLineP_TRG
 AFTER INSERT OR UPDATE ON ServiceProdLineProduct
 FOR EACH ROW

BEGIN
   INSERT INTO STMS_10_SYNC_LS VALUES(S_STMS_10_SYNC_LS.NEXTVAL,:NEW.ID,'ServiceProdLineProduct');
 END STMS_ServiceProdLineProduct_TR ;