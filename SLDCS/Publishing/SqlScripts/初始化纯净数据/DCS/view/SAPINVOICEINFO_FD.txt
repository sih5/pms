CREATE OR REPLACE VIEW DCS.SAPINVOICEINFO_FD AS
SELECT id,
       BUKRS         as CompanyCode,
       2             as BusinessType,
       code,
       InvoiceNumber,
       InvoiceAmount,
       invoicetax,
       CreateTime,
       Status,
       Message
  from Sap_FD_SalesBillInfo
union
SELECT id,
       BUKRS         as CompanyCode,
       3             as BusinessType,
       jsdnum,
       InvoiceNumber,
       InvoiceAmount,
       invoicetax,
       CreateTime,
       Status,
       Message
  from Sap_FD_PurchaseRtnInvoice
union
SELECT id,
       BUKRS as CompanyCode,
       1 as BusinessType,
       jsdnum,
       to_char('') as InvoiceNumber,
       to_number('') as InvoiceAmount,
       to_number('') as invoicetax,
       CreateTime,
       Status,
       Message
  from Sap_FD_InvoiceVerAccountInfo;