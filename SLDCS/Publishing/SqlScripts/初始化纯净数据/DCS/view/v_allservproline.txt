create or replace view dcs.v_allservproline as
select 'dcs' as syscode,code as serviceproductlinecode from dcs.serviceproductline union all
select 'yxdcs' as syscode,code as serviceproductlinecode from yxdcs.serviceproductline union all
select 'gcdcs' as syscode,code as serviceproductlinecode from gcdcs.serviceproductline  where PartsSalesCategoryId not in (21,22)  union all
select 'sddcs' as syscode,code as serviceproductlinecode from sddcs.serviceproductline;