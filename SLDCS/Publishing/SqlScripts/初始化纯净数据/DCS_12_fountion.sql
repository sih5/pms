-- 生成编号
create or replace function GetSerial(FTemplateId integer, FCreateDate date, FCorpCode varchar2) return integer
as
  PRAGMA AUTONOMOUS_TRANSACTION;
  FId integer;
  FSerial integer;
begin
  begin
    if FCorpCode is null then
      select id,serial into FId,FSerial from CodeTemplateSerial
       where TemplateId = FTemplateId and CreateDate = FCreateDate and CorpCode is null and rownum<2
         for update;
    else
      select id,serial into FId,FSerial from CodeTemplateSerial
       where TemplateId = FTemplateId and CreateDate = FCreateDate and CorpCode = FCorpCode and rownum<2
         for update;
    end if;
    FSerial:=FSerial+1;
    UPDATE CodeTemplateSerial SET Serial = FSerial WHERE Id = FId;
    commit;
  exception
    when NO_DATA_FOUND then
      FSerial:=1;
      INSERT INTO CodeTemplateSerial(Id ,TemplateId, CreateDate, CorpCode, Serial)
      VALUES(S_CodeTemplateSerial.Nextval, FTemplateId, FCreateDate, FCorpCode, FSerial);
      commit;
    when others then
      rollback;
      raise;
  end;
  return FSerial;
end;
/


-- 根据Id查询库区库位分类及其全部下级库区库位
create or replace function GetChildWarehouseAreas(Id integer) return SYS_REFCURSOR
as
  Result SYS_REFCURSOR;
  my_sql varchar2(4000);
begin
  my_sql := 'with tmp(id, parentid) as( '||
            '  select id, parentid from warehousearea '||
            '  where id = '||to_char(Id)||
            '  union all '||
            '  select warehousearea.id, warehousearea.parentid '||
            '  from warehousearea '||
            '  inner join tmp on tmp.id = warehousearea.parentid '||
            ')  cycle id set dup_id TO ''Y'' DEFAULT ''N'' '||
            'select * from warehousearea '||
            'where exists (select * from tmp where warehousearea.id = tmp.id) ';
  open Result for my_sql;
  return(Result);
end;
/

-- 根据Id查询旧件库区库位分类及其全部下级库区库位
create or replace function GetChildUsedPartsWhseAreas(Id integer) return SYS_REFCURSOR
as
  Result SYS_REFCURSOR;
  my_sql varchar2(4000);
begin
  my_sql := 'with tmp(id, parentid) as( '||
            '  select id, parentid from usedpartswarehousearea '||
            '  where id = '||to_char(Id)||
            '  union all '||
            '  select usedpartswarehousearea.id, usedpartswarehousearea.parentid '||
            '  from usedpartswarehousearea '||
            '  inner join tmp on tmp.id = usedpartswarehousearea.parentid '||
            ')  cycle id set dup_id TO ''Y'' DEFAULT ''N'' '||
            'select * from usedpartswarehousearea '||
            'where exists (select * from tmp where usedpartswarehousearea.id = tmp.id) ';
  open Result for my_sql;
  return(Result);
end;
/

-- 根据Id查询故障现象分类及其全部下级分类
create or replace function GetChildMalfunctionCategories(Id integer) return SYS_REFCURSOR
as
  Result SYS_REFCURSOR;
  my_sql varchar2(4000);
begin
  my_sql := 'with tmp(id, parentid) as( '||
            '  select id, parentid from MalfunctionCategory '||
            '  where id = '||to_char(Id)||
            '  union all '||
            '  select MalfunctionCategory.id, MalfunctionCategory.parentid '||
            '  from MalfunctionCategory '||
            '  inner join tmp on tmp.id = MalfunctionCategory.parentid '||
            ')  cycle id set dup_id TO ''Y'' DEFAULT ''N'' '||
            'select * from MalfunctionCategory '||
            'where exists (select * from tmp where MalfunctionCategory.id = tmp.id) ';
  open Result for my_sql;
  return(Result);
end;
/


create or replace function GenerateSsClaimSettlementBill(FBranchId in integer, FDealerId in integer, FStartTime in date, FEndTime in date,
  FCreatorId in integer, FCreatorName in varchar2,FPartsSalesCategoryId in integer,FServiceProductLineId in integer,FProductLineType in integer, FSettleMethods in integer) return integer
as
  GeneratedBillQuantity integer;
  /*营销分公司编号*/
  FBranchCode VARCHAR2(50);
  /*营销分公司名称*/
  FBranchName VARCHAR2(100);
  /*编号模板Id*/
  FComplateId integer;
  /*编码规则相关设置*/
  FDateFormat date;
  FDateStr varchar2(8);
  FSerialLength integer;
  FNewCode varchar2(50);
  FSsClaimSettlementBillId integer;
  FBILLFORCLAIMSETTLEMENTiD integer;
begin
  /*编码规则变更，需同步调整*/
  --FDateFormat:=trunc(sysdate,'YEAR');
  --FDateFormat:=trunc(sysdate,'MONTH');
  FDateFormat:=trunc(sysdate,'DD');
  FDateStr:=to_char(sysdate,'yyyymmdd');
  FSerialLength:=6;
  FNewCode:='SCS{CORPCODE}'||FDateStr||'{SERIAL}';

  /*获取编号生成模板Id*/
  begin
    select Id
      into FComplateId
      from codetemplate
     where name = 'SsClaimSettlementBill'
       and IsActived = 1;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20001, '未找到名称为"SsClaimSettlementBill"的有效编码规则。');
    when others then
      raise;
  end;

  select Code,Name into FBranchCode, FBranchName from Branch where id = FBranchId;
  GeneratedBillQuantity := 0;

  /*维修保养索赔单*/
  insert into BillForClaimSettlement
    (SourceId,
     SourceCode,
     SourceType,
     DealerId,
     BranchId,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     Partssalescategoryid,
     ServiceProductLineId,
     ProductLineType)
    select Id,
           ClaimBillCode,
           decode(ClaimType, 1, 1, 2, 2, 3, 3,10,1, 0) SourceType, --维修保养索赔单索赔类型、服务站索赔结算源单据类型，可能存在差异
           DealerId,
           BranchId,
           LaborCost,
           MaterialCost,
           NVL(PartsManagementCost, 0) PartsManagementCost,
           0 FieldServiceExpense,
           0 DebitAmount,
           0 ComplementAmount,
           NVL(OtherCost, 0) OtherCost,
           0 PreSaleAmount,
           Partssalescategoryid,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  ServiceProductLineId) ServiceProductLineId,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From dcs.serviceproductlineview v
                    Where v.ProductLineId =
                          RepairClaimBill.ServiceProductLineId)) ProductLineType
      from RepairClaimBill
     where CreateTime <= FEndTime
       and (FStartTime is null or CreateTime >= FStartTime)
       and SettlementStatus = 2
       and BranchId = FBranchId
       and (FDealerId is null or DealerId = FDealerId)
       and status = 4
       and (FPartsSalesCategoryId = 0 or
           PartsSalesCategoryId = FPartsSalesCategoryId)
            /* and (FProductLineType is null or ProductLineType=FProductLineType)*/
       and (FServiceProductLineId is null or FServiceProductLineId = 0 or
           ServiceProductLineId = FServiceProductLineId);
/*
   update RepairClaimBill
      set SettlementStatus = 3
    where RepairClaimBill.Id in
          (select SourceId
             from BillForClaimSettlement
            where SourceType in (1, 2, 3));*/

   /*配件索赔单*/
  insert into BillForClaimSettlement
    (SourceId,
     SourceCode,
     SourceType,
     DealerId,
     BranchId,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     Partssalescategoryid,
     ServiceProductLineId,
     ProductLineType)
    select Id SourceId,
           Code SourceCode,
           4 SourceType,
           DealerId,
           BranchId,
           LaborCost,
           MaterialCost,
           NVL(PartsManagementCost, 0),
           0 FieldServiceExpense,
           0 DebitAmount,
           0 ComplementAmount,
           NVL(OtherCost, 0) OtherCost,
           0 PreSaleAmount,
           Partssalescategoryid,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  ServiceProductLineId) ServiceProductLineId,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From dcs.serviceproductlineview v
                    Where v.ProductLineId =
                          PartsClaimOrder.ServiceProductLineId)) ProductLineType
      from PartsClaimOrder
     where CreateTime <= FEndTime
       and (FStartTime is null or CreateTime >= FStartTime)
       and SettlementStatus = 2
       and BranchId = FBranchId
       and (FDealerId is null or DealerId = FDealerId)
       and status = 3
       and (FPartsSalesCategoryId = 0 or
           PartsSalesCategoryId = FPartsSalesCategoryId)
           /*and (FProductLineType is null or ProductLineType=FProductLineType)*/
       and (FServiceProductLineId is null or FServiceProductLineId = 0 or
           ServiceProductLineId = FServiceProductLineId) ;
/*
   update PartsClaimOrder
      set SettlementStatus = 3
    where PartsClaimOrder.Id in
          (select SourceId from BillForClaimSettlement where SourceType = 4);*/

  /*外出服务索赔单*/
  insert into BillForClaimSettlement
    (SourceId,
     SourceCode,
     SourceType,
     DealerId,
     BranchId,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     Partssalescategoryid,
     ServiceProductLineId,
     ProductLineType)
    select Id SourceId,
           Code SourceCode,
           5 SourceType,
           DealerId,
           BranchId,
           0 LaborCost,
           0 MaterialCost,
           0 PartsManagementCost,
           NVL(TowCharge, 0) + NVL(FieldServiceCharge, 0) FieldServiceExpense,
           0 DebitAmount,
           0 ComplementAmount,
           NVL(OtherCost, 0) OtherCost,
           0 PreSaleAmount,
           Partssalescategoryid,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  ServiceProductLineId) ServiceProductLineId,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From dcs.serviceproductlineview v
                    Where v.ProductLineId =
                          ServiceTripClaimBill.ServiceProductLineId)) ProductLineType
      from ServiceTripClaimBill
     where CreateTime <= FEndTime
       and (FStartTime is null or CreateTime >= FStartTime)
       and SettlementStatus = 2
       and BranchId = FBranchId
       and (FDealerId is null or DealerId = FDealerId)
       and status = 6
       and (FPartsSalesCategoryId = 0 or
           PartsSalesCategoryId = FPartsSalesCategoryId)
          /*and (FProductLineType is null or ProductLineType=FProductLineType)*/
       and (FServiceProductLineId is null or FServiceProductLineId = 0 or
           ServiceProductLineId = FServiceProductLineId);
/*
   update ServiceTripClaimBill
      set SettlementStatus = 3
    where ServiceTripClaimBill.Id in
          (select SourceId from BillForClaimSettlement where SourceType = 5);*/

  /*扣补款单*/
  insert into BillForClaimSettlement
    (SourceId,
     SourceCode,
     SourceType,
     DealerId,
     BranchId,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     Partssalescategoryid,
     ServiceProductLineId,
     ProductLineType,DEBITTYPE )
    select Id SourceId,
           Code SourceCode,
           6 SourceType,
           DealerId,
           BranchId,
           0 LaborCost,
           0 MaterialCost,
           0 PartsManagementCost,
           0 FieldServiceExpense,
           decode(DebitOrReplenish, 1, -TransactionAmount, 0) DebitAmount,
           decode(DebitOrReplenish, 2, TransactionAmount, 0) ComplementAmount,
           0 OtherCost,
           0 PreSaleAmount,
           Partssalescategoryid,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  ServiceProductLineId) ServiceProductLineId,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From dcs.serviceproductlineview v
                    Where v.ProductLineId =
                          ExpenseAdjustmentBill.ServiceProductLineId)) ProductLineType,ExpenseAdjustmentBill.TransactionCategory
      from ExpenseAdjustmentBill
     where CreateTime <= FEndTime
       and (FStartTime is null or CreateTime >= FStartTime)
       and SettlementStatus = 2
       and BranchId = FBranchId
       and (FDealerId is null or DealerId = FDealerId)
       and status = 2
       and (FPartsSalesCategoryId = 0 or
           PartsSalesCategoryId = FPartsSalesCategoryId)
           /*and (FProductLineType is null or ProductLineType=FProductLineType)*/
       and (FServiceProductLineId is null or FServiceProductLineId = 0 or
           ServiceProductLineId = FServiceProductLineId);
/*
   update ExpenseAdjustmentBill
      set SettlementStatus = 3
    where ExpenseAdjustmentBill.Id in
          (select SourceId from BillForClaimSettlement where SourceType = 6);*/

 /*售前检查单*/
 insert into BillForClaimSettlement
   (SourceId,
    SourceCode,
    SourceType,
    DealerId,
    BranchId,
    LaborCost,
    MaterialCost,
    PartsManagementCost,
    FieldServiceExpense,
    DebitAmount,
    ComplementAmount,
    OtherCost,
    PreSaleAmount,
    Partssalescategoryid,
    ServiceProductLineId,
    ProductLineType)
   select Id SourceId,
          Code SourceCode,
          7 SourceType,
          DealerId,
          BranchId,
          0 LaborCost, --工时费
          0 MaterialCost, --材料费
          0 PartsManagementCost, --配件管理费
          0 FieldServiceExpense, --外出费用
          0 DebitAmount, --扣款金额
          0 ComplementAmount, --补款金额
          0 OtherCost, --其他费用
          nvl(Cost, 0) PreSaleAmount, --售前检查费用
          Partssalescategoryid, --品牌Id
          decode(FServiceProductLineId,
                 null,
                 null,
                 0,
                 null,
                 ServiceProductLineId),
         decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From dcs.serviceproductlineview v
                    Where v.ProductLineId =
                          PreSaleCheckOrder.ServiceProductLineId)) ProductLineType
     from PreSaleCheckOrder
    where CreateTime <= FEndTime
      and (FStartTime is null or CreateTime >= FStartTime)
      and SettleStatus = 2
      and BranchId = FBranchId
      and (FDealerId is null or DealerId = FDealerId)
      and status = 3
      and (FPartsSalesCategoryId = 0 or
          PartsSalesCategoryId = FPartsSalesCategoryId)
      and (FServiceProductLineId is null or FServiceProductLineId = 0 or
          ServiceProductLineId = FServiceProductLineId);
/*
 update PreSaleCheckOrder
    set SettleStatus = 3
  where PreSaleCheckOrder.Id in
        (select SourceId from BillForClaimSettlement where SourceType = 7);
*/
FBillForClaimSettlementId:=s_BillForClaimSettlement.Nextval; 
 if FSettleMethods=1 then begin
declare
  cursor Cursor_role is(

     select
          0 IfInvoiced,
          1 Status,
          regexp_replace(
            regexp_replace(FNewCode, '{CORPCODE}', Dealer.Code),
            '{SERIAL}',
            lpad(to_char(GetSerial(FComplateId, FDateFormat, Dealer.Code)),FSerialLength,'0')
          ) Code,
          DealerId,
          Dealer.Code DealerCode,
          Dealer.Name DealerName,
          FBranchId BranchId,
          FBranchCode BranchCode,
          FBranchName BranchName,
          FStartTime SettlementStartTime,
          FEndTime SettlementEndTime,
          LaborCost,
          MaterialCost,
          PartsManagementCost,
          FieldServiceExpense,
          DebitAmount,
          ComplementAmount,
          OtherCost,
          PreSaleAmount,
          LaborCost + MaterialCost + PartsManagementCost + FieldServiceExpense + DebitAmount + ComplementAmount +OtherCost+PreSaleAmount TotalAmount,
          FCreatorId CreatorId,
          FCreatorName CreatorName,
          sysdate CreateTime,
          PARTSSALESCATEGORYID,
          ServiceProductLineId ,
          ProductLineType
     from (select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  sum(MaterialCost) MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(PreSaleAmount) PreSaleAmount
             from BillForClaimSettlement
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join Dealer on Dealer.Id = t_date.DealerId
       );
begin
  for S_role in Cursor_role loop
   
    FSsClaimSettlementBillId:=S_SsClaimSettlementBill.Nextval;
    /*主单生成*/
  if s_role.TotalAmount>=0 then
    begin
    update RepairClaimBill
       set SettlementStatus = 3
     where RepairClaimBill.Id in
           (select SourceId
              from BillForClaimSettlement
             where SourceType in (1, 2, 3)
               and BillForClaimSettlement.Dealerid = s_role.DealerId
               and BranchId = s_role.BranchId
               and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                   PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
               and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                   ServiceProductLineId = s_role.ServiceProductLineId));
   
   update ServiceTripClaimBill
      set SettlementStatus = 3
    where ServiceTripClaimBill.Id in
          (select SourceId
             from BillForClaimSettlement
            where SourceType = 5
              and BillForClaimSettlement.Dealerid = s_role.DealerId
              and BranchId = s_role.BranchId
              and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                  PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
              and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                  ServiceProductLineId = s_role.ServiceProductLineId));


 update PreSaleCheckOrder
    set SettleStatus = 3
  where PreSaleCheckOrder.Id in
        (select SourceId
           from BillForClaimSettlement
          where SourceType = 7
            and BillForClaimSettlement.Dealerid = s_role.DealerId
            and BranchId = s_role.BranchId
            and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
            and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                ServiceProductLineId = s_role.ServiceProductLineId));
                
   update ExpenseAdjustmentBill
      set SettlementStatus = 3
    where ExpenseAdjustmentBill.Id in
          (select SourceId
             from BillForClaimSettlement
            where SourceType = 6
              and BillForClaimSettlement.Dealerid = s_role.DealerId
              and BranchId = s_role.BranchId
              and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                  PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
              and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                  ServiceProductLineId = s_role.ServiceProductLineId));

  insert into SsClaimSettlementBill
    (Id,
     IfInvoiced,
     Status,
     Code,
     DealerId,
     DealerCode,
     DealerName,
     BranchId,
     BranchCode,
     BranchName,
     SettlementStartTime,
     SettlementEndTime,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     CreatorId,
     CreatorName,
     CreateTime,
     PartsSalesCategoryId,
     ServiceProductLineId,
     ProductLineType,SettleType,RELATEDID,SettleMethods)
     values(FSsClaimSettlementBillId,
     s_role.IfInvoiced,
     s_role.Status,
     s_role.Code,
     s_role.DealerId,
     s_role.DealerCode,
     s_role.DealerName,
     s_role.BranchId,
     s_role.BranchCode,
     s_role.BranchName,
     s_role.SettlementStartTime,
     s_role.SettlementEndTime,
     s_role.LaborCost,
     s_role.MaterialCost,
     s_role.PartsManagementCost,
     s_role.FieldServiceExpense,
     s_role.DebitAmount,
     s_role.ComplementAmount,
     s_role.OtherCost,
     s_role.PreSaleAmount,
     s_role.TotalAmount,
     s_role.CreatorId,
     s_role.CreatorName,
     s_role.CreateTime,
     s_role.PartsSalesCategoryId,
     s_role.ServiceProductLineId,
     s_role.ProductLineType,3,FBillForClaimSettlementId,1);

   /*清单生成*/
      insert into SsClaimSettlementDetail
    (Id,
     SsClaimSettlementBillId,
     SourceId,
     SourceCode,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     SourceType)
  select S_SsClaimSettlementDetail.NEXTVAL,
         FSsClaimSettlementBillId,
         BillForClaimSettlement.SourceId,
         BillForClaimSettlement.SourceCode,
         BillForClaimSettlement.LaborCost,
         BillForClaimSettlement.MaterialCost,
         BillForClaimSettlement.PartsManagementCost,
         BillForClaimSettlement.FieldServiceExpense,
         BillForClaimSettlement.DebitAmount,
         BillForClaimSettlement.ComplementAmount,
         BillForClaimSettlement.OtherCost,
         BillForClaimSettlement.PreSaleAmount,
         BillForClaimSettlement.LaborCost + BillForClaimSettlement.MaterialCost + BillForClaimSettlement.PartsManagementCost
         + BillForClaimSettlement.FieldServiceExpense + BillForClaimSettlement.DebitAmount + BillForClaimSettlement.ComplementAmount
         + BillForClaimSettlement.OtherCost+BillForClaimSettlement.PreSaleAmount,
         SourceType
    from BillForClaimSettlement
   inner join SsClaimSettlementBill on BillForClaimSettlement.DealerId = SsClaimSettlementBill.DealerId
                                   and BillForClaimSettlement.BranchId = SsClaimSettlementBill.BranchId
                                   And BillForClaimSettlement.PARTSSALESCATEGORYID = SsClaimSettlementBill.PARTSSALESCATEGORYID
                                   And (BillForClaimSettlement.ServiceProductLineId = SsClaimSettlementBill.ServiceProductLineId or BillForClaimSettlement.ServiceProductLineId  =0 or
                                   BillForClaimSettlement.ServiceProductLineId is null)
                                   And (BillForClaimSettlement.ProductLineType = SsClaimSettlementBill.ProductLineType or BillForClaimSettlement.ProductLineType is null
                                   or BillForClaimSettlement.ProductLineType=0)
   where not exists (select * from SsClaimSettlementDetail where SsClaimSettlementDetail.Ssclaimsettlementbillid = SsClaimSettlementBill.Id) AND SsClaimSettlementBill.ID=FSsClaimSettlementBillId;



      end;
  /* raise_application_error(-20001, '存在结算汇总单总金额为负数的单据'); */
   end if;

  end loop;
end;

end;
 else begin

declare
  cursor Cursor_role is(
     --材料费
     select
          0 IfInvoiced,
          1 Status,
          regexp_replace(
            regexp_replace(FNewCode, '{CORPCODE}', Dealer.Code),
            '{SERIAL}',
            lpad(to_char(GetSerial(FComplateId, FDateFormat, Dealer.Code)),FSerialLength,'0')
          ) Code,
          DealerId,
          Dealer.Code DealerCode,
          Dealer.Name DealerName,
          FBranchId BranchId,
          FBranchCode BranchCode,
          FBranchName BranchName,
          FStartTime SettlementStartTime,
          FEndTime SettlementEndTime,
          LaborCost,
          MaterialCost,
          PartsManagementCost,
          FieldServiceExpense,
          DebitAmount,
          ComplementAmount,
          OtherCost,
          PreSaleAmount,
          LaborCost + MaterialCost + PartsManagementCost + FieldServiceExpense + DebitAmount + ComplementAmount +OtherCost+PreSaleAmount TotalAmount,
          FCreatorId CreatorId,
          FCreatorName CreatorName,
          sysdate CreateTime,
          PARTSSALESCATEGORYID,
          ServiceProductLineId ,
          ProductLineType
     from (select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  0 LaborCost,
                  sum(MaterialCost) MaterialCost,
                  0 PartsManagementCost,
                  0 FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  0 OtherCost,
                  0 PreSaleAmount
             from BillForClaimSettlement where( BillForClaimSettlement.Sourcetype in (1,2,3)) or (BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype in(2,16))
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join Dealer on Dealer.Id = t_date.DealerId
    where  not exists(select 1 from (
               select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  0 MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(PreSaleAmount) PreSaleAmount
             from BillForClaimSettlement where (BillForClaimSettlement.Sourcetype in (1,2,3,4,5,7)) or(BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype not in(2,16))
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t
            where  t_date.DealerId = t.DealerId
       and t_date.BranchId = t.BranchId
       And t_date.PARTSSALESCATEGORYID =
           t.PARTSSALESCATEGORYID
       And (t_date.ServiceProductLineId =
           t.ServiceProductLineId or
           t_date.ServiceProductLineId = 0 or
           t_date.ServiceProductLineId is null and
           t.LaborCost  + t.PartsManagementCost + t.FieldServiceExpense +t. DebitAmount +t. ComplementAmount +t.OtherCost+PreSaleAmount<0)
    )
       );
begin
  for S_role in Cursor_role loop
   
    FSsClaimSettlementBillId:=S_SsClaimSettlementBill.Nextval;
    /*主单生成*/
  if s_role.TotalAmount>=0 then
    begin
/*    update RepairClaimBill
       set SettlementStatus = 3
     where RepairClaimBill.Id in
           (select SourceId
              from BillForClaimSettlement
             where SourceType in (1, 2, 3)
               and BillForClaimSettlement.Dealerid = s_role.DealerId
               and BranchId = s_role.BranchId
               and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                   PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
               and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                   ServiceProductLineId = s_role.ServiceProductLineId));*/
   
   update ExpenseAdjustmentBill
      set SettlementStatus = 3
    where ExpenseAdjustmentBill.Id in
          (select SourceId
             from BillForClaimSettlement
            where SourceType = 6 and Debittype  in(2,16)
              and BillForClaimSettlement.Dealerid = s_role.DealerId
              and BranchId = s_role.BranchId
              and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                  PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
              and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                  ServiceProductLineId = s_role.ServiceProductLineId));

  insert into SsClaimSettlementBill
    (Id,
     IfInvoiced,
     Status,
     Code,
     DealerId,
     DealerCode,
     DealerName,
     BranchId,
     BranchCode,
     BranchName,
     SettlementStartTime,
     SettlementEndTime,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     CreatorId,
     CreatorName,
     CreateTime,
     PartsSalesCategoryId,
     ServiceProductLineId,
     ProductLineType,SettleType,RelatedId,SettleMethods)
     values(FSsClaimSettlementBillId,
     s_role.IfInvoiced,
     s_role.Status,
     s_role.Code,
     s_role.DealerId,
     s_role.DealerCode,
     s_role.DealerName,
     s_role.BranchId,
     s_role.BranchCode,
     s_role.BranchName,
     s_role.SettlementStartTime,
     s_role.SettlementEndTime,
     s_role.LaborCost,
     s_role.MaterialCost,
     s_role.PartsManagementCost,
     s_role.FieldServiceExpense,
     s_role.DebitAmount,
     s_role.ComplementAmount,
     s_role.OtherCost,
     s_role.PreSaleAmount,
     s_role.TotalAmount,
     s_role.CreatorId,
     s_role.CreatorName,
     s_role.CreateTime,
     s_role.PartsSalesCategoryId,
     s_role.ServiceProductLineId,
     s_role.ProductLineType,1,FBillForClaimSettlementId,2);

   /*清单生成*/
  insert into SsClaimSettlementDetail
    (Id,
     SsClaimSettlementBillId,
     SourceId,
     SourceCode,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     SourceType)
    select S_SsClaimSettlementDetail.NEXTVAL,
           FSsClaimSettlementBillId,
           BillForClaimSettlement.SourceId,
           BillForClaimSettlement.SourceCode,
           0,
           BillForClaimSettlement.MaterialCost,
           0,
           0,
           BillForClaimSettlement.DebitAmount,
           BillForClaimSettlement.Complementamount,
           0,
           0,
           0 +
           BillForClaimSettlement.MaterialCost +
           0 +
           0 +
           BillForClaimSettlement.DebitAmount +
           BillForClaimSettlement.Complementamount +
           0 +
           0,
           SourceType
      from BillForClaimSettlement
     inner join SsClaimSettlementBill
        on BillForClaimSettlement.DealerId = SsClaimSettlementBill.DealerId
       and BillForClaimSettlement.BranchId = SsClaimSettlementBill.BranchId
       And BillForClaimSettlement.PARTSSALESCATEGORYID =
           SsClaimSettlementBill.PARTSSALESCATEGORYID
       And (BillForClaimSettlement.ServiceProductLineId =
           SsClaimSettlementBill.ServiceProductLineId or
           BillForClaimSettlement.ServiceProductLineId = 0 or
           BillForClaimSettlement.ServiceProductLineId is null)
       And (BillForClaimSettlement.ProductLineType =
           SsClaimSettlementBill.ProductLineType or
           BillForClaimSettlement.ProductLineType is null or
           BillForClaimSettlement.ProductLineType = 0)
     where not exists
     (select *
              from SsClaimSettlementDetail
             where SsClaimSettlementDetail.Ssclaimsettlementbillid =
                   SsClaimSettlementBill.Id)
       AND SsClaimSettlementBill.ID = FSsClaimSettlementBillId
      and ( ( BillForClaimSettlement.Sourcetype in (1,2,3)) or (BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype in(2,16)));


      end;
  /* raise_application_error(-20001, '存在结算汇总单总金额为负数的单据'); */
   end if;

  end loop;
end;   
 
declare
  cursor Cursor_role is(

     select
          0 IfInvoiced,
          1 Status,
          regexp_replace(
            regexp_replace(FNewCode, '{CORPCODE}', Dealer.Code),
            '{SERIAL}',
            lpad(to_char(GetSerial(FComplateId, FDateFormat, Dealer.Code)),FSerialLength,'0')
          ) Code,
          DealerId,
          Dealer.Code DealerCode,
          Dealer.Name DealerName,
          FBranchId BranchId,
          FBranchCode BranchCode,
          FBranchName BranchName,
          FStartTime SettlementStartTime,
          FEndTime SettlementEndTime,
          LaborCost,
          MaterialCost,
          PartsManagementCost,
          FieldServiceExpense,
          DebitAmount,
          ComplementAmount,
          OtherCost,
          PreSaleAmount,
          LaborCost + MaterialCost + PartsManagementCost + FieldServiceExpense + DebitAmount + ComplementAmount +OtherCost+PreSaleAmount TotalAmount,
          FCreatorId CreatorId,
          FCreatorName CreatorName,
          sysdate CreateTime,
          PARTSSALESCATEGORYID,
          ServiceProductLineId ,
          ProductLineType
    from (select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  0 MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(PreSaleAmount) PreSaleAmount
             from BillForClaimSettlement where (BillForClaimSettlement.Sourcetype in (1,2,3,4,5,7)) or(BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype not in(2,16))
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join Dealer on Dealer.Id = t_date.DealerId
        where  not exists(select 1 from (
              select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  0 LaborCost,
                  sum(MaterialCost) MaterialCost,
                  0 PartsManagementCost,
                  0 FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  0 OtherCost,
                  0 PreSaleAmount
             from BillForClaimSettlement where( BillForClaimSettlement.Sourcetype in (1,2,3)) or (BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype in(2,16))
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t
            where  t_date.DealerId = t.DealerId
       and t_date.BranchId = t.BranchId
       And t_date.PARTSSALESCATEGORYID =
           t.PARTSSALESCATEGORYID
       And (t_date.ServiceProductLineId =
           t.ServiceProductLineId or
           t_date.ServiceProductLineId = 0 or
           t_date.ServiceProductLineId is null and
           t.MaterialCost  + t.DebitAmount+t.ComplementAmount <0)
    )
       );
begin
  for S_role in Cursor_role loop
   
    FSsClaimSettlementBillId:=S_SsClaimSettlementBill.Nextval;
    /*主单生成*/
  if s_role.TotalAmount>=0 then
    begin
    update RepairClaimBill
       set SettlementStatus = 3
     where RepairClaimBill.Id in
           (select SourceId
              from BillForClaimSettlement
             where SourceType in (1, 2, 3)
               and BillForClaimSettlement.Dealerid = s_role.DealerId
               and BranchId = s_role.BranchId
               and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                   PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
               and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                   ServiceProductLineId = s_role.ServiceProductLineId));
   
   update ServiceTripClaimBill
      set SettlementStatus = 3
    where ServiceTripClaimBill.Id in
          (select SourceId
             from BillForClaimSettlement
            where SourceType = 5
              and BillForClaimSettlement.Dealerid = s_role.DealerId
              and BranchId = s_role.BranchId
              and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                  PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
              and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                  ServiceProductLineId = s_role.ServiceProductLineId));


 update PreSaleCheckOrder
    set SettleStatus = 3
  where PreSaleCheckOrder.Id in
        (select SourceId
           from BillForClaimSettlement
          where SourceType = 7
            and BillForClaimSettlement.Dealerid = s_role.DealerId
            and BranchId = s_role.BranchId
            and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
            and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                ServiceProductLineId = s_role.ServiceProductLineId));
                
   update ExpenseAdjustmentBill
      set SettlementStatus = 3
    where ExpenseAdjustmentBill.Id in
          (select SourceId
             from BillForClaimSettlement
            where SourceType = 6 and Debittype not in(2,16)
              and BillForClaimSettlement.Dealerid = s_role.DealerId
              and BranchId = s_role.BranchId
              and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                  PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
              and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                  ServiceProductLineId = s_role.ServiceProductLineId));

  insert into SsClaimSettlementBill
    (Id,
     IfInvoiced,
     Status,
     Code,
     DealerId,
     DealerCode,
     DealerName,
     BranchId,
     BranchCode,
     BranchName,
     SettlementStartTime,
     SettlementEndTime,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     CreatorId,
     CreatorName,
     CreateTime,
     PartsSalesCategoryId,
     ServiceProductLineId,
     ProductLineType,SettleType,RELATEDID,SettleMethods)
     values(FSsClaimSettlementBillId,
     s_role.IfInvoiced,
     s_role.Status,
     s_role.Code,
     s_role.DealerId,
     s_role.DealerCode,
     s_role.DealerName,
     s_role.BranchId,
     s_role.BranchCode,
     s_role.BranchName,
     s_role.SettlementStartTime,
     s_role.SettlementEndTime,
     s_role.LaborCost,
     s_role.MaterialCost,
     s_role.PartsManagementCost,
     s_role.FieldServiceExpense,
     s_role.DebitAmount,
     s_role.ComplementAmount,
     s_role.OtherCost,
     s_role.PreSaleAmount,
     s_role.TotalAmount,
     s_role.CreatorId,
     s_role.CreatorName,
     s_role.CreateTime,
     s_role.PartsSalesCategoryId,
     s_role.ServiceProductLineId,
     s_role.ProductLineType,2,FBillForClaimSettlementId,2);

   /*清单生成*/
  insert into SsClaimSettlementDetail
    (Id,
     SsClaimSettlementBillId,
     SourceId,
     SourceCode,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     SourceType)
    select S_SsClaimSettlementDetail.NEXTVAL,
           FSsClaimSettlementBillId,
           BillForClaimSettlement.SourceId,
           BillForClaimSettlement.SourceCode,
           BillForClaimSettlement.LaborCost,
           0,
           BillForClaimSettlement.PartsManagementCost,
           BillForClaimSettlement.FieldServiceExpense,
           BillForClaimSettlement.DebitAmount,
           BillForClaimSettlement.ComplementAmount,
           BillForClaimSettlement.OtherCost,
           BillForClaimSettlement.PreSaleAmount,
           BillForClaimSettlement.LaborCost +
           0 +
           BillForClaimSettlement.PartsManagementCost +
           BillForClaimSettlement.FieldServiceExpense +
           BillForClaimSettlement.DebitAmount +
           BillForClaimSettlement.ComplementAmount +
           BillForClaimSettlement.OtherCost +
           BillForClaimSettlement.PreSaleAmount,
           SourceType
      from BillForClaimSettlement
     inner join SsClaimSettlementBill
        on BillForClaimSettlement.DealerId = SsClaimSettlementBill.DealerId
       and BillForClaimSettlement.BranchId = SsClaimSettlementBill.BranchId
       And BillForClaimSettlement.PARTSSALESCATEGORYID =
           SsClaimSettlementBill.PARTSSALESCATEGORYID
       And (BillForClaimSettlement.ServiceProductLineId =
           SsClaimSettlementBill.ServiceProductLineId or
           BillForClaimSettlement.ServiceProductLineId = 0 or
           BillForClaimSettlement.ServiceProductLineId is null)
       And (BillForClaimSettlement.ProductLineType =
           SsClaimSettlementBill.ProductLineType or
           BillForClaimSettlement.ProductLineType is null or
           BillForClaimSettlement.ProductLineType = 0)
     where not exists
     (select *
              from SsClaimSettlementDetail
             where SsClaimSettlementDetail.Ssclaimsettlementbillid =
                   SsClaimSettlementBill.Id)
       AND SsClaimSettlementBill.ID = FSsClaimSettlementBillId
      and ( ( BillForClaimSettlement.Sourcetype in (1,2,3,4,5,7)) or (BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype not in(2,16)));


      end;
  /* raise_application_error(-20001, '存在结算汇总单总金额为负数的单据'); */
   end if;

  end loop;
end; 
                 
  end;
  end if;
/*
  \*主单生成*\
   FSsClaimSettlementBillId:=S_SsClaimSettlementBill.Nextval;
  insert into SsClaimSettlementBill
    (Id,
     IfInvoiced,
     Status,
     Code,
     DealerId,
     DealerCode,
     DealerName,
     BranchId,
     BranchCode,
     BranchName,
     SettlementStartTime,
     SettlementEndTime,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     CreatorId,
     CreatorName,
     CreateTime,
     PartsSalesCategoryId,
     ServiceProductLineId,
     ProductLineType)
   select FSsClaimSettlementBillId,
          0,
          1,
          regexp_replace(
            regexp_replace(FNewCode, '{CORPCODE}', Dealer.Code),
            '{SERIAL}',
            lpad(to_char(GetSerial(FComplateId, FDateFormat, Dealer.Code)),FSerialLength,'0')
          ) Code,
          DealerId,
          Dealer.Code,
          Dealer.Name,
          FBranchId,
          FBranchCode,
          FBranchName,
          FStartTime,
          FEndTime,
          LaborCost,
          MaterialCost,
          PartsManagementCost,
          FieldServiceExpense,
          DebitAmount,
          ComplementAmount,
          OtherCost,
          PreSaleAmount,
          LaborCost + MaterialCost + PartsManagementCost + FieldServiceExpense + DebitAmount + ComplementAmount +OtherCost+PreSaleAmount,
          FCreatorId,
          FCreatorName,
          sysdate,
          PARTSSALESCATEGORYID,
          ServiceProductLineId,
          ProductLineType
     from (select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  sum(MaterialCost) MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(PreSaleAmount) PreSaleAmount
             from BillForClaimSettlement
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join Dealer on Dealer.Id = t_date.DealerId;*/
IF FSettleMethods=1 THEN 
 select count(*) into GeneratedBillQuantity from (select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  sum(MaterialCost) MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(PreSaleAmount) PreSaleAmount
             from BillForClaimSettlement
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join Dealer on Dealer.Id = t_date.DealerId where LaborCost + MaterialCost + PartsManagementCost + FieldServiceExpense + DebitAmount + ComplementAmount +OtherCost+PreSaleAmount>=0
    ;
    ELSE
    select count(*) into GeneratedBillQuantity from 
   ( select * from(select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  0 LaborCost,
                  sum(MaterialCost) MaterialCost,
                  0 PartsManagementCost,
                  0 FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  0 ComplementAmount,
                  0 OtherCost,
                  0 PreSaleAmount
             from BillForClaimSettlement where( BillForClaimSettlement.Sourcetype in (1,2,3)) or (BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype in(2,16))
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join Dealer on Dealer.Id = t_date.DealerId where  MaterialCost + DebitAmount>=0
    and  not exists(select 1 from (
               select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  0 MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(PreSaleAmount) PreSaleAmount
             from BillForClaimSettlement where (BillForClaimSettlement.Sourcetype in (1,2,3,4,5,7)) or(BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype not in(2,16))
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t
            where  t_date.DealerId = t.DealerId
       and t_date.BranchId = t.BranchId
       And t_date.PARTSSALESCATEGORYID =
           t.PARTSSALESCATEGORYID
       And (t_date.ServiceProductLineId =
           t.ServiceProductLineId or
           t_date.ServiceProductLineId = 0 or
           t_date.ServiceProductLineId is null and
           t.LaborCost  + t.PartsManagementCost + t.FieldServiceExpense +t. DebitAmount +t. ComplementAmount +t.OtherCost+PreSaleAmount<0)
    )
    union 
    select * from(select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  0,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(PreSaleAmount) PreSaleAmount
            from BillForClaimSettlement where (BillForClaimSettlement.Sourcetype in (1,2,3,4,5,7)) or(BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype not in(2,16))            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join Dealer on Dealer.Id = t_date.DealerId where LaborCost+ PartsManagementCost+FieldServiceExpense+DebitAmount + ComplementAmount+OtherCost+PreSaleAmount>=0
     and  not exists(select 1 from (
              select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  0 LaborCost,
                  sum(MaterialCost) MaterialCost,
                  0 PartsManagementCost,
                  0 FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  0 OtherCost,
                  0 PreSaleAmount
             from BillForClaimSettlement where( BillForClaimSettlement.Sourcetype in (1,2,3)) or (BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype in(2,16))
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t
            where  t_date.DealerId = t.DealerId
       and t_date.BranchId = t.BranchId
       And t_date.PARTSSALESCATEGORYID =
           t.PARTSSALESCATEGORYID
       And (t_date.ServiceProductLineId =
           t.ServiceProductLineId or
           t_date.ServiceProductLineId = 0 or
           t_date.ServiceProductLineId is null and
           t.MaterialCost + t.ComplementAmount+ t.DebitAmount <0)
    )
    );

      END IF;
  --GeneratedBillQuantity:=SQL%ROWCOUNT;

  /*清单生成*/
 /* insert into SsClaimSettlementDetail
    (Id,
     SsClaimSettlementBillId,
     SourceId,
     SourceCode,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     SourceType)
  select S_SsClaimSettlementDetail.NEXTVAL,
         FSsClaimSettlementBillId,
         BillForClaimSettlement.SourceId,
         BillForClaimSettlement.SourceCode,
         BillForClaimSettlement.LaborCost,
         BillForClaimSettlement.MaterialCost,
         BillForClaimSettlement.PartsManagementCost,
         BillForClaimSettlement.FieldServiceExpense,
         BillForClaimSettlement.DebitAmount,
         BillForClaimSettlement.ComplementAmount,
         BillForClaimSettlement.OtherCost,
         BillForClaimSettlement.PreSaleAmount,
         BillForClaimSettlement.LaborCost + BillForClaimSettlement.MaterialCost + BillForClaimSettlement.PartsManagementCost
         + BillForClaimSettlement.FieldServiceExpense + BillForClaimSettlement.DebitAmount + BillForClaimSettlement.ComplementAmount
         + BillForClaimSettlement.OtherCost+BillForClaimSettlement.PreSaleAmount,
         SourceType
    from BillForClaimSettlement
   inner join SsClaimSettlementBill on BillForClaimSettlement.DealerId = SsClaimSettlementBill.DealerId
                                   and BillForClaimSettlement.BranchId = SsClaimSettlementBill.BranchId
                                   And BillForClaimSettlement.PARTSSALESCATEGORYID = SsClaimSettlementBill.PARTSSALESCATEGORYID
                                   And (BillForClaimSettlement.ServiceProductLineId = SsClaimSettlementBill.ServiceProductLineId or BillForClaimSettlement.ServiceProductLineId  =0 or
                                   BillForClaimSettlement.ServiceProductLineId is null)
                                   And (BillForClaimSettlement.ProductLineType = SsClaimSettlementBill.ProductLineType or BillForClaimSettlement.ProductLineType is null
                                   or BillForClaimSettlement.ProductLineType=0)
   where not exists (select * from SsClaimSettlementDetail where SsClaimSettlementDetail.Ssclaimsettlementbillid = SsClaimSettlementBill.Id) AND SsClaimSettlementBill.ID=FSsClaimSettlementBillId;
*/
 -- delete BillForClaimSettlement;
  return(GeneratedBillQuantity);
end;
/



create or replace function GenerateSupClaimSettlementBill(FBranchId in integer, FSupplierId in integer, FStartTime in date, FEndTime in date,
  FCreatorId in integer, FCreatorName in varchar2,FPartsSalesCategoryId in integer,FServiceProductLineId in integer,FProductLineType in integer) return integer
as
  GeneratedBillQuantity integer;
  /*营销分公司编号*/
  FBranchCode VARCHAR2(50);
  /*营销分公司名称*/
  FBranchName VARCHAR2(100);
  /*编号模板Id*/
  FComplateId integer;
  /*编码规则相关设置*/
  FDateFormat date;
  FDateStr varchar2(8);
  FSerialLength integer;
  FNewCode varchar2(50);
  FSupplierClaimSettlementBillId integer;
    ifsuppilerbranchid  integer;
begin
  /*编码规则变更，需同步调整*/
  --FDateFormat:=trunc(sysdate,'YEAR');
  --FDateFormat:=trunc(sysdate,'MONTH');
  FDateFormat:=trunc(sysdate,'DD');
  FDateStr:=to_char(sysdate,'yyyymmdd');
  FSerialLength:=6;
  FNewCode:='SCSB{CORPCODE}'||FDateStr||'{SERIAL}';

  /*获取编号生成模板Id*/
  begin
    select Id
      into FComplateId
      from codetemplate
     where name = 'SupplierClaimSettlementBill'
       and IsActived = 1;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20001, '未找到名称为"SupplierClaimSettlementBill"的有效编码规则。');
    when others then
      raise;
  end;
  begin
    select Id
    into ifsuppilerbranchid
    from dcs.branchsupplierrelation t
    where t.branchid=FBranchId and t.supplierid=FSupplierId and t.partssalescategoryid=FPartsSalesCategoryId;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20002,'未维护供应商的索赔结算相关系数，请维护分公司与供应商关系管理！');
      when others then
        raise;
  end;
  select Code,Name into FBranchCode, FBranchName from Branch where id = FBranchId;
  GeneratedBillQuantity := 0;

  /*维修保养索赔单*/
  insert into BILLFORSUPPLIERCLAIMSETTLEMENT
    (SourceId,
     SourceCode,
     SourceType,
     supplierId,
     BranchId,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OldPartTranseCost,
     OtherCost,
     Partssalescategoryid,
     ServiceProductLineId,
     ProductLineType)
    select T.Id,
           RepairClaimBill.ClaimBillCode,
           decode(ClaimType, 1, 5, 2, 10, 3, 15,10,5, 0) SourceType, --维修保养索赔单索赔类型、服务站索赔结算源单据类型，可能存在差异
           t.supplierid,
           RepairClaimBill.BranchId,
           nvl(t.laborcost,0)*nvl(a.LaborCoefficient,0) LaborCost,
           --t.laborcost,
           t.MaterialCost,
           NVL(t.partsmanagementcost, 0) PartsManagementCost,
           0 FieldServiceExpense,
           0 DebitAmount,
           0 ComplementAmount,
           --nvl(t.MaterialCost,0)*nvl(a.OldPartTransCoefficient,0)  as OldPartTranseCost,
           nvl(dt.MaterialCost,0)*nvl(a.OldPartTransCoefficient,0)  as OldPartTranseCost,
           NVL(t.OtherCost, 0) OtherCost,
           RepairClaimBill.Partssalescategoryid,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  RepairClaimBill.ServiceProductLineId) ServiceProductLineId,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From dcs.serviceproductlineview v
                    Where v.ProductLineId =
                          RepairClaimBill.ServiceProductLineId)) ProductLineType
      from RepairClaimBill
      --2014-06-13 add
     left join (select rcid.repairclaimbillid, sum(case when rcmd.UsedPartsReturnPolicy = 1 then rcmd.MaterialCost else 0 end) as MaterialCost
          from RepairClaimMaterialDetail rcmd inner join RepairClaimItemDetail rcid on rcmd.repairclaimitemdetailid = rcid.id
          inner join RepairClaimBill r on r.id = rcid.repairclaimbillid
          where r.CreateTime <= FEndTime and (FStartTime is null or r.CreateTime >= FStartTime)
          and r.BranchId = FBranchId and r.status = 4 and r.repairtype=15
          group by  rcid.repairclaimbillid
          ) dt on dt.repairclaimbillid = RepairClaimBill.Id
     inner join dcs.repairclaimsupplierdetail t
        on t.repairclaimid = RepairClaimBill.id
     inner join dcs.branchsupplierrelation a
        on a.branchid = RepairClaimBill.Branchid
       and a.partssalescategoryid = RepairClaimBill.Partssalescategoryid
       and a.supplierid = t.supplierid
     where RepairClaimBill.CreateTime <= FEndTime
       and (FStartTime is null or RepairClaimBill.CreateTime >= FStartTime)
       and t.settlestatus = 2
       and RepairClaimBill.BranchId = FBranchId
       and (FSupplierId is null or T.SUPPLIERID = FSupplierId)
       and RepairClaimBill.status = 4
       and RepairClaimBill.repairtype=15
       and (FPartsSalesCategoryId = 0 or RepairClaimBill.
            PartsSalesCategoryId = FPartsSalesCategoryId)
         --  and (FProductLineType is null or ProductLineType=FProductLineType)
       and (FServiceProductLineId is null or FServiceProductLineId = 0 or
           ServiceProductLineId = FServiceProductLineId);

  insert into BILLFORSUPPLIERCLAIMSETTLEMENT
    (SourceId,
     SourceCode,
     SourceType,
     supplierId,
     BranchId,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OldPartTranseCost,
     OtherCost,
     Partssalescategoryid,
     ServiceProductLineId,
     ProductLineType)
    select T.Id,
           RepairClaimBill.ClaimBillCode,
           decode(ClaimType, 1, 5, 2, 10, 3, 15,10,5, 0) SourceType, --维修保养索赔单索赔类型、服务站索赔结算源单据类型，可能存在差异
           t.supplierid,
           RepairClaimBill.BranchId,
           decode(a.IfSPByLabor,0,nvl(t.laborcost,0)*nvl(a.LaborCoefficient,0),1, (select sum(nvl(b.defaultlaborhour,0)) from RepairClaimItemDetail b where b.repairclaimbillid=RepairClaimBill.id )*nvl(a.LaborUnitPrice,0),nvl(t.laborcost,0)*nvl(a.LaborCoefficient,0) ) LaborCost,
           --t.laborcost,
           t.MaterialCost,
           NVL(t.partsmanagementcost, 0) PartsManagementCost,
           0 FieldServiceExpense,
           0 DebitAmount,
           0 ComplementAmount,
          -- nvl(t.MaterialCost,0)*nvl(a.OldPartTransCoefficient,0)  as OldPartTranseCost,
           nvl(dt.MaterialCost,0)*nvl(a.OldPartTransCoefficient,0)  as OldPartTranseCost,
           NVL(t.OtherCost, 0) OtherCost,
           RepairClaimBill.Partssalescategoryid,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  RepairClaimBill.ServiceProductLineId) ServiceProductLineId,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From dcs.serviceproductlineview v
                    Where v.ProductLineId =
                          RepairClaimBill.ServiceProductLineId)) ProductLineType
      from RepairClaimBill
     --2014-06-13 add
     left join (select rcid.repairclaimbillid, sum(case when rcmd.UsedPartsReturnPolicy = 1 then rcmd.MaterialCost else 0 end) as MaterialCost
          from RepairClaimMaterialDetail rcmd inner join RepairClaimItemDetail rcid on rcmd.repairclaimitemdetailid = rcid.id
          inner join RepairClaimBill r on r.id = rcid.repairclaimbillid
          where r.CreateTime <= FEndTime and (FStartTime is null or r.CreateTime >= FStartTime)
          and r.BranchId = FBranchId and r.status = 4 and r.repairtype<>15
          group by  rcid.repairclaimbillid
          ) dt on dt.repairclaimbillid = RepairClaimBill.Id
     inner join dcs.repairclaimsupplierdetail t
        on t.repairclaimid = RepairClaimBill.id
     inner join dcs.branchsupplierrelation a
        on a.branchid = RepairClaimBill.Branchid
       and a.partssalescategoryid = RepairClaimBill.Partssalescategoryid
       and a.supplierid = t.supplierid
     where RepairClaimBill.CreateTime <= FEndTime
       and (FStartTime is null or RepairClaimBill.CreateTime >= FStartTime)
       and t.settlestatus = 2
       and RepairClaimBill.BranchId = FBranchId
       and (FSupplierId is null or T.SUPPLIERID = FSupplierId)
       and RepairClaimBill.status = 4
       and RepairClaimBill.repairtype<>15
       and (FPartsSalesCategoryId = 0 or RepairClaimBill.
            PartsSalesCategoryId = FPartsSalesCategoryId)
           --and (FProductLineType is null or ProductLineType=FProductLineType)
       and (FServiceProductLineId is null or FServiceProductLineId = 0 or
           ServiceProductLineId = FServiceProductLineId);



  /*外出服务索赔单*/
  insert into BILLFORSUPPLIERCLAIMSETTLEMENT
    (SourceId,
     SourceCode,
     SourceType,
     SUPPLIERID,
     BranchId,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     OldPartTranseCost,
     Partssalescategoryid,
     ServiceProductLineId,
     ProductLineType)
    select ServiceTripClaimBill.Id SourceId,
           Code SourceCode,
           20 SourceType,
           ClaimSupplierId,
           ServiceTripClaimBill.BranchId,
           0 LaborCost,
           0 MaterialCost,
           0 PartsManagementCost,
           decode (a.IfHaveOutFee,1,( NVL(TowCharge, 0)+NVL(OtherCost, 0) + NVL(FieldServiceCharge, 0)),0,( NVL(TowCharge, 0)+NVL(OtherCost, 0)+NVL(ServiceTripClaimBill.SettleDistance,0)*NVL(A.OutServiceCarUnitPrice,0)*NVL(IfUseOwnVehicle,0)+NVL(A.OutSubsidyPrice,0)*nvl(ServiceTripClaimBill.ServiceTripPerson,0)*nvl(ServiceTripClaimBill.ServiceTripDuration,0))) FieldServiceExpense,
         -- NVL(TowCharge, 0)+NVL(OtherCost, 0) + NVL(FieldServiceCharge, 0) FieldServiceExpense,
           0 DebitAmount,
           0 ComplementAmount,
           NVL(OtherCost, 0) OtherCost,
           0 OldPartTranseCost,
           ServiceTripClaimBill.Partssalescategoryid,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  ServiceProductLineId) ServiceProductLineId,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From dcs.serviceproductlineview v
                    Where v.ProductLineId =
                          ServiceTripClaimBill.ServiceProductLineId)) ProductLineType
      from ServiceTripClaimBill
      inner join dcs.branchsupplierrelation a
        on a.branchid = ServiceTripClaimBill.Branchid
       and a.partssalescategoryid = ServiceTripClaimBill.Partssalescategoryid
       and a.supplierid =  ServiceTripClaimBill.ClaimSupplierId
     where ServiceTripClaimBill.CreateTime <= FEndTime
       and (FStartTime is null or ServiceTripClaimBill.CreateTime >= FStartTime)
       and SupplierSettleStatus = 2
       and ServiceTripClaimBill.BranchId = FBranchId
       and (FSupplierId is null or ServiceTripClaimBill.ClaimSupplierId = FSupplierId)
       and ServiceTripClaimBill.status = 6
       and (FPartsSalesCategoryId = 0 or
           ServiceTripClaimBill.PartsSalesCategoryId = FPartsSalesCategoryId)
         -- and (FProductLineType is null or ProductLineType=FProductLineType)
       and (FServiceProductLineId is null or FServiceProductLineId = 0 or
           ServiceProductLineId = FServiceProductLineId);



  /*扣补款单*/
  insert into BILLFORSUPPLIERCLAIMSETTLEMENT
    (SourceId,
     SourceCode,
     SourceType,
     SUPPLIERID,
     BranchId,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     OldPartTranseCost,
     Partssalescategoryid,
     ServiceProductLineId,
     ProductLineType)
    select Id SourceId,
           Code SourceCode,
           25 SourceType,
           SUPPLIERID,
           BranchId,
           0 LaborCost,
           0 MaterialCost,
           0 PartsManagementCost,
           0 FieldServiceExpense,
           decode(DebitOrReplenish, 1, -TransactionAmount, 0) DebitAmount,
           decode(DebitOrReplenish, 2, TransactionAmount, 0) ComplementAmount,
           0 OtherCost,
           0 OldPartTranseCost,
           Partssalescategoryid,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  ServiceProductLineId) ServiceProductLineId,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From dcs.serviceproductlineview v
                    Where v.ProductLineId =
                          supplierexpenseadjustbill.ServiceProductLineId)) ProductLineType
      from dcs.supplierexpenseadjustbill
     where CreateTime <= FEndTime
       and (FStartTime is null or CreateTime >= FStartTime)
       and SettlementStatus = 2
       and BranchId = FBranchId
       and (FSupplierId is null or SUPPLIERID = FSupplierId)
       and status = 2
       and (FPartsSalesCategoryId = 0 or
           PartsSalesCategoryId = FPartsSalesCategoryId)
          -- and (FProductLineType is null or ProductLineType=FProductLineType)
       and (FServiceProductLineId is null or FServiceProductLineId = 0 or
           ServiceProductLineId = FServiceProductLineId);




declare
  cursor Cursor_role is(

     select
          0 IfInvoiced,
          5 Status,
          regexp_replace(
            regexp_replace(FNewCode, '{CORPCODE}', partssupplier.Code),
            '{SERIAL}',
            lpad(to_char(GetSerial(FComplateId, FDateFormat, partssupplier.Code)),FSerialLength,'0')
          ) Code,
          supplierid,
          partssupplier.Code SupplierCode,
          partssupplier.Name SupplierName,
          FBranchId BranchId,
          FBranchCode BranchCode,
          FBranchName BranchName,
          FStartTime SettlementStartTime,
          FEndTime SettlementEndTime,
          LaborCost,
          MaterialCost,
          PartsManagementCost,
          FieldServiceExpense,
          DebitAmount,
          ComplementAmount,
          OtherCost,
          OldPartTranseCost,
          LaborCost + MaterialCost + PartsManagementCost + FieldServiceExpense + DebitAmount + ComplementAmount +OtherCost+OLDPARTTRANSECOST TotalAmount,
          FCreatorId CreatorId,
          FCreatorName CreatorName,
          sysdate CreateTime,
          PARTSSALESCATEGORYID,
          ServiceProductLineId ,
          ProductLineType
     from (select supplierid,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  sum(MaterialCost) MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OLDPARTTRANSECOST) OLDPARTTRANSECOST,
                  sum(OtherCost) OtherCost
             from BILLFORSUPPLIERCLAIMSETTLEMENT
            group by supplierid, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join dcs.partssupplier on partssupplier.Id = t_date.supplierid
       );
begin
  for S_role in Cursor_role loop
    FSupplierClaimSettlementBillId:=S_SupplierClaimSettleBill.Nextval;
    /*主单生成*/
  if s_role.TotalAmount>=0 then
    begin

   update repairclaimsupplierdetail T
      set T.SETTLESTATUS = 3
    where T.Id in
          (select SourceId
             from BILLFORSUPPLIERCLAIMSETTLEMENT
            where SourceType in (5, 10, 15)
              and BILLFORSUPPLIERCLAIMSETTLEMENT.supplierid =
                  s_role.supplierid
              and BranchId = s_role.BranchId
              and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                  PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
              and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                  ServiceProductLineId = s_role.ServiceProductLineId));

   update ServiceTripClaimBill
      set SupplierSettleStatus = 3
    where ServiceTripClaimBill.Id in
          (select SourceId
             from BILLFORSUPPLIERCLAIMSETTLEMENT
            where SourceType = 20
              and BILLFORSUPPLIERCLAIMSETTLEMENT.supplierid =
                  s_role.supplierid
              and BranchId = s_role.BranchId
              and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                  PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
              and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                  ServiceProductLineId = s_role.ServiceProductLineId));

   update supplierexpenseadjustbill
      set SettlementStatus = 3
    where supplierexpenseadjustbill.Id in
          (select SourceId
             from BILLFORSUPPLIERCLAIMSETTLEMENT
            where SourceType = 25
              and BILLFORSUPPLIERCLAIMSETTLEMENT.supplierid =
                  s_role.supplierid
              and BranchId = s_role.BranchId
              and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                  PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
              and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                  ServiceProductLineId = s_role.ServiceProductLineId));

  insert into dcs.supplierclaimsettlebill
    (Id,
     IfInvoiced,
     Status,
     Code,
     SupplierId,
     SupplierCode,
     SupplierName,
     BranchId,
     BranchCode,
     BranchName,
     SettlementStartTime,
     SettlementEndTime,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     OldPartTranseCost,
     TotalAmount,
     CreatorId,
     CreatorName,
     CreateTime,
     PartsSalesCategoryId,
     ServiceProductLineId,
     ProductLineType)
     values(FSupplierClaimSettlementBillId,
     s_role.IfInvoiced,
     s_role.Status,
     s_role.Code,
     s_role.SupplierID,
     s_role.SupplierCode,
     s_role.SupplierName,
     s_role.BranchId,
     s_role.BranchCode,
     s_role.BranchName,
     s_role.SettlementStartTime,
     s_role.SettlementEndTime,
     s_role.LaborCost,
     s_role.MaterialCost,
     s_role.PartsManagementCost,
     s_role.FieldServiceExpense,
     s_role.DebitAmount,
     s_role.ComplementAmount,
     s_role.OtherCost,
     s_role. OldPartTranseCost,
     s_role.TotalAmount,
     s_role.CreatorId,
     s_role.CreatorName,
     s_role.CreateTime,
     s_role.PartsSalesCategoryId,
     s_role.ServiceProductLineId,
     s_role.ProductLineType);

   /*清单生成*/
      insert into dcs.supplierclaimsettledetail    (id,supplierclaimsettlementbillid,sourceid,sourcecode,sourcetype,laborcost,materialcost,partsmanagementcost,fieldserviceexpense,debitamount,complementamount,othercost,OldPartTranseCost,totalamount)
  select S_supplierclaimsettledetail.NEXTVAL,
         FSupplierClaimSettlementBillId,
         BILLFORSUPPLIERCLAIMSETTLEMENT.SourceId,
         BILLFORSUPPLIERCLAIMSETTLEMENT.SourceCode,
         SourceType,
         BILLFORSUPPLIERCLAIMSETTLEMENT.LaborCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.MaterialCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.PartsManagementCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.FieldServiceExpense,
         BILLFORSUPPLIERCLAIMSETTLEMENT.DebitAmount,
         BILLFORSUPPLIERCLAIMSETTLEMENT.ComplementAmount,
         BILLFORSUPPLIERCLAIMSETTLEMENT.OtherCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.OldPartTranseCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.LaborCost + BILLFORSUPPLIERCLAIMSETTLEMENT.MaterialCost + BILLFORSUPPLIERCLAIMSETTLEMENT.PartsManagementCost
         + BILLFORSUPPLIERCLAIMSETTLEMENT.FieldServiceExpense + BILLFORSUPPLIERCLAIMSETTLEMENT.DebitAmount + BILLFORSUPPLIERCLAIMSETTLEMENT.ComplementAmount
         + BILLFORSUPPLIERCLAIMSETTLEMENT.OtherCost+ BILLFORSUPPLIERCLAIMSETTLEMENT.OldPartTranseCost
    from BILLFORSUPPLIERCLAIMSETTLEMENT
   inner join SupplierClaimSettleBill on BILLFORSUPPLIERCLAIMSETTLEMENT.Supplierid = SupplierClaimSettleBill.supplierid
                                   and BILLFORSUPPLIERCLAIMSETTLEMENT.BranchId = SupplierClaimSettleBill.BranchId
                                   And BILLFORSUPPLIERCLAIMSETTLEMENT.PARTSSALESCATEGORYID = SupplierClaimSettleBill.PARTSSALESCATEGORYID
                                   And (BILLFORSUPPLIERCLAIMSETTLEMENT.ServiceProductLineId = SupplierClaimSettleBill.ServiceProductLineId or BILLFORSUPPLIERCLAIMSETTLEMENT.ServiceProductLineId  =0 or
                                   BILLFORSUPPLIERCLAIMSETTLEMENT.ServiceProductLineId is null)
                                   And (BILLFORSUPPLIERCLAIMSETTLEMENT.ProductLineType = SupplierClaimSettleBill.ProductLineType or BILLFORSUPPLIERCLAIMSETTLEMENT.ProductLineType is null
                                   or BILLFORSUPPLIERCLAIMSETTLEMENT.ProductLineType=0)
   where not exists (select * from supplierclaimsettledetail where supplierclaimsettledetail.supplierclaimsettlementbillid = SupplierClaimSettleBill.Id) AND SupplierClaimSettleBill.ID=FSupplierClaimSettlementBillId;



      end;
  /* raise_application_error(-20001, '存在结算汇总单总金额为负数的单据'); */
   end if;

  end loop;
end;

/*
  \*主单生成*\
   FSupplierClaimSettlementBillId:=S_SupplierClaimSettlementBill.Nextval;
  insert into SupplierClaimSettlementBill
    (Id,
     IfInvoiced,
     Status,
     Code,
     DealerId,
     DealerCode,
     DealerName,
     BranchId,
     BranchCode,
     BranchName,
     SettlementStartTime,
     SettlementEndTime,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     CreatorId,
     CreatorName,
     CreateTime,
     PartsSalesCategoryId,
     ServiceProductLineId,
     ProductLineType)
   select FSupplierClaimSettlementBillId,
          0,
          1,
          regexp_replace(
            regexp_replace(FNewCode, '{CORPCODE}', Dealer.Code),
            '{SERIAL}',
            lpad(to_char(GetSerial(FComplateId, FDateFormat, Dealer.Code)),FSerialLength,'0')
          ) Code,
          DealerId,
          Dealer.Code,
          Dealer.Name,
          FBranchId,
          FBranchCode,
          FBranchName,
          FStartTime,
          FEndTime,
          LaborCost,
          MaterialCost,
          PartsManagementCost,
          FieldServiceExpense,
          DebitAmount,
          ComplementAmount,
          OtherCost,
          PreSaleAmount,
          LaborCost + MaterialCost + PartsManagementCost + FieldServiceExpense + DebitAmount + ComplementAmount +OtherCost+PreSaleAmount,
          FCreatorId,
          FCreatorName,
          sysdate,
          PARTSSALESCATEGORYID,
          ServiceProductLineId,
          ProductLineType
     from (select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  sum(MaterialCost) MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(PreSaleAmount) PreSaleAmount
             from BILLFORSUPPLIERCLAIMSETTLEMENT
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join Dealer on Dealer.Id = t_date.DealerId;*/

 select count(*) into GeneratedBillQuantity from (select supplierid,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  sum(MaterialCost) MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(OldPartTranseCost) OldPartTranseCost
             from BILLFORSUPPLIERCLAIMSETTLEMENT
            group by supplierid, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join partssupplier on partssupplier.Id = t_date.supplierid where LaborCost + MaterialCost + PartsManagementCost + FieldServiceExpense + DebitAmount + ComplementAmount +OtherCost>=0;
  --GeneratedBillQuantity:=SQL%ROWCOUNT;

  /*清单生成*/
 /* insert into SsClaimSettlementDetail
    (Id,
     SupplierClaimSettlementBillId,
     SourceId,
     SourceCode,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     SourceType)
  select S_SsClaimSettlementDetail.NEXTVAL,
         FSupplierClaimSettlementBillId,
         BILLFORSUPPLIERCLAIMSETTLEMENT.SourceId,
         BILLFORSUPPLIERCLAIMSETTLEMENT.SourceCode,
         BILLFORSUPPLIERCLAIMSETTLEMENT.LaborCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.MaterialCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.PartsManagementCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.FieldServiceExpense,
         BILLFORSUPPLIERCLAIMSETTLEMENT.DebitAmount,
         BILLFORSUPPLIERCLAIMSETTLEMENT.ComplementAmount,
         BILLFORSUPPLIERCLAIMSETTLEMENT.OtherCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.PreSaleAmount,
         BILLFORSUPPLIERCLAIMSETTLEMENT.LaborCost + BILLFORSUPPLIERCLAIMSETTLEMENT.MaterialCost + BILLFORSUPPLIERCLAIMSETTLEMENT.PartsManagementCost
         + BILLFORSUPPLIERCLAIMSETTLEMENT.FieldServiceExpense + BILLFORSUPPLIERCLAIMSETTLEMENT.DebitAmount + BILLFORSUPPLIERCLAIMSETTLEMENT.ComplementAmount
         + BILLFORSUPPLIERCLAIMSETTLEMENT.OtherCost+BILLFORSUPPLIERCLAIMSETTLEMENT.PreSaleAmount,
         SourceType
    from BILLFORSUPPLIERCLAIMSETTLEMENT
   inner join SupplierClaimSettlementBill on BILLFORSUPPLIERCLAIMSETTLEMENT.DealerId = SupplierClaimSettlementBill.DealerId
                                   and BILLFORSUPPLIERCLAIMSETTLEMENT.BranchId = SupplierClaimSettlementBill.BranchId
                                   And BILLFORSUPPLIERCLAIMSETTLEMENT.PARTSSALESCATEGORYID = SupplierClaimSettlementBill.PARTSSALESCATEGORYID
                                   And (BILLFORSUPPLIERCLAIMSETTLEMENT.ServiceProductLineId = SupplierClaimSettlementBill.ServiceProductLineId or BILLFORSUPPLIERCLAIMSETTLEMENT.ServiceProductLineId  =0 or
                                   BILLFORSUPPLIERCLAIMSETTLEMENT.ServiceProductLineId is null)
                                   And (BILLFORSUPPLIERCLAIMSETTLEMENT.ProductLineType = SupplierClaimSettlementBill.ProductLineType or BILLFORSUPPLIERCLAIMSETTLEMENT.ProductLineType is null
                                   or BILLFORSUPPLIERCLAIMSETTLEMENT.ProductLineType=0)
   where not exists (select * from SsClaimSettlementDetail where SsClaimSettlementDetail.SupplierClaimSettlementBillid = SupplierClaimSettlementBill.Id) AND SupplierClaimSettlementBill.ID=FSupplierClaimSettlementBillId;
*/
  delete BILLFORSUPPLIERCLAIMSETTLEMENT;
  return(GeneratedBillQuantity);
 --return(1);
end;
/



create or replace function Getrepairclaimbills
(
  PVIN  char
) return sys_refcursor as
  result    sys_refcursor;
begin
    open result for
      select v1."索赔单编号",v1."单据状态",v1."市场部",v1."申请单编号",v1."索赔单类型",v1."故障原因编码",v1."客户意见",v1."是否已回访",v1."回访意见",v1."服务科审核意见",v1."材料费",v1."配件管理费",v1."工时费",v1."索赔类型",v1."维修索赔备注信息",v1."创建日期",v1."创建人编号",v1."分公司审核时间",v1."故障原因名称",v1."星级系数",v1."是否需要向事业部索赔",v1."客户姓名",v1."客户电话",v1."客户手机",v1."祸首件图号",v1."祸首件名称",v1."供应商确认状态",v1."供应商审核意见",v1."外出车费单价",v1."外出路途补助单价",v1."外出公里数",v1."高速公路拖车费",v1."外出人数",v1."外出天数",v1."外出差旅费用合计",v1."外出救援地",v1."外出索赔备注信息",v1."维修属性",v1."出厂编号",v1."品牌名称",v1."产品线名称",v1."报修时间",v1."运行里程",v1."故障现象及原因描述",v1."维修附加属性",v1."福康工时单价",v1."祸首件生产厂家",v1."祸首件标记",v1."祸首件总成生产厂家",v1."祸首件总成标记",v1."车型",v1."发动机型号",v1."发动机编号",v1."VIN码",v1."祸首件总成名称",v1."祸首件总成图号",v1."祸首件流水号",v1."购买日期",v1."首次故障里程",v1."生产日期",v1."服务站编号",v1."服务站名称",v1."索赔单位事业部编号",v1."工时单价",v1."索赔单位事业部名称",v1."客户地址",v1."责任单位供应商编号",v1."责任单位供应商名称",
       nvl(工时费,0) + nvl(材料费,0) + nvl(外出差旅费用合计,0) as 维修索赔费用合计
  from (select --tbyservicecards.objid,
               tbyservicecards.code 索赔单编号,
               tbyservicecards.ISVALID 单据状态,
               tmarkets.name as 市场部,
               (select TAPPLYREPORTS.Code
                  from TAPPLYREPORTS@lservice_130
                 inner join TAPPLYREPORTLINKS@lservice_130
                    on TAPPLYREPORTLINKS.ApplyReportID = TAPPLYREPORTS.ObjID
                 where tRepContracts.ObjID = TAPPLYREPORTLINKS.RepContractID) as 申请单编号,
               decode(tbyservicecards.SERVICECARDTYPE,1,'维修类',2,'强保类') as  索赔单类型,
               tbyservicecards.FAULTREASONCODE 故障原因编码,
               tbyservicecards.FAULTDES 客户意见,
               decode(tbyservicecards.ISFEEDBACK,1,'否',2,'是')as 是否已回访,
               tbyservicecards.FEEDBACKIDEA 回访意见,
               tbyservicecards.EXAMIDEA 服务科审核意见,
               tbyservicecards.MATFEE 材料费,
               case tbyservicecards.SERVICECARDTYPE
                         when 1 then
                          ServiceMagFee
                         When 2 then
                          0
                       end as  配件管理费,
               case tbyservicecards.SERVICECARDTYPE
                         when 1 then
                          tbyservicecards.spworkfee
                         When 2 then
                          tbyservicecards.workfee
                       end as  工时费,
               decode(tbyservicecards.SPTYPE,0,'配件索赔',1,'装车件索赔',2,'非索赔') as 索赔类型,
               tbyservicecards.MEMO 维修索赔备注信息,
               tbyservicecards.CREATETIME 创建日期,
               tbyservicecards.CREATORCODE 创建人编号,
               tbyservicecards.ConfirmTime as 分公司审核时间,
              -- tbyservicecards.CHECKMANCODE,
               tbyservicecards.FAULTREASONNAME 故障原因名称,
               tbyservicecards.LEVELS_QUOTIETY 星级系数,
               decode(tbyservicecards.ISDUTYSP ,0,'否',1,'是' )as 是否需要向事业部索赔,
              -- tbyservicecards.TOTALFEE as SPTOTALFEE,
               tbyservicecards.CUSTNAME 客户姓名,
               tbyservicecards.CUSTPHONE 客户电话,
               tbyservicecards.CUSTMOBILE 客户手机,
               tbyservicecards.CAUSEMCODE 祸首件图号,
               tbyservicecards.CAUSEMNAME 祸首件名称,
            --   tbyservicecards.ISREPEATREP,
               decode(tbyservicecards.SUPPLIERSTATE,0,'仍未确认',1,'确认已通过',2,'确认未通过') as 供应商确认状态,
               tbyservicecards.CONFIRMIDEA 供应商审核意见,
              -- FT.TOUTSETTLES.ISPRIVATECAR,
               tOutSettles.OUTFARE 外出车费单价,
               tOutSettles.OUTSUBSIDY 外出路途补助单价,
               tOutSettles.OUTKMS 外出公里数,
               tOutSettles.TICKETFEE 高速公路拖车费,
               tOutSettles.SERVICEMENS 外出人数,
               tOutSettles.SERVICEDAYS 外出天数,
               nvl(tOutSettles.TOTALFEE, 0) as 外出差旅费用合计,
               tOutSettles.OUTPLACE 外出救援地,
               tOutSettles.Memo as  外出索赔备注信息,
               tRepContracts.REPAIRPROP 维修属性,
               tRepContracts.SERIALCODE 出厂编号,
               tBrands.NAME AS 品牌名称,
               tProDuctLines.NAME AS 产品线名称,
             --  TREPCONTRACTS.OBJID AS REPID,
               tRepContracts.RepBeginTime 报修时间,
               tRepContracts.runrange 运行里程,
               tRepContracts.FaultInfo 故障现象及原因描述,
               decode(tRepContracts.ADDITIONAL,1,'驾送维修',2,'经销商库存车维修',3,'经销商接车维修') AS 维修附加属性,
               decode(tByServiceCards.iskms, 1, tRepContracts.WORKFEESTANDKMS, 0) as 福康工时单价,
               tbyservicecards.C_P_Manufacturers as  祸首件生产厂家,
               tbyservicecards.C_Markings as  祸首件标记,
               tbyservicecards.C_P_A_Manufacturers as  祸首件总成生产厂家,
               tbyservicecards.C_A_Markings as  祸首件总成标记,
               TPURCHASEINFOS.PRODUCT_TYPE_NAME 车型,
               TPURCHASEINFOS.ENGINE_MODEL 发动机型号,
               TPURCHASEINFOS.ENGINE_CODE 发动机编号,
               TPURCHASEINFOS.VINCODE vin码,
               tbyservicecards.C_P_A_NAME AS 祸首件总成名称 ,
               tbyservicecards.C_P_A_CODE AS 祸首件总成图号 ,
               tbyservicecards.C_P_NUMBER AS 祸首件流水号,
               TPuRCHASEINFOS.Sales_Date 购买日期,
               TPuRCHASEINFOS.mile 首次故障里程,
               TPURCHASEINFOS.Leave_Factory_Date 生产日期,
               tstations.code as  服务站编号,
               tstations.name as  服务站名称,
               D1.Code as "索赔单位事业部编号",
               tRepContracts.WORKFEESTAND AS 工时单价,
               D1.Name as "索赔单位事业部名称",
               (select TPURCHASELINKERS.address
                  from ft.TPURCHASELINKERS@lservice_130
                 where TPURCHASEINFOS.ObjID = TPURCHASELINKERS.parentid) 客户地址,
               (select D2.Code
                  from ft.TDUTYUNITS@lservice_130 D2
                 where D2.ObjID = tbyservicecards.dutyunitid) as "责任单位供应商编号",
               (select D2.Name
                  from ft.TDUTYUNITS@lservice_130 D2
                 where D2.ObjID = tbyservicecards.dutyunitid) as "责任单位供应商名称"
          from ft.tByServiceCards@lservice_130
         inner join ft.tstations@lservice_130
            on tbyservicecards.entercode = tstations.objid
         inner join ft.tstationlists@lservice_130
            on tstations.objid = tstationlists.stationid
           and tbyservicecards.brandid = tstationlists.brandid
         Inner join ft.TMARKETS@lservice_130
            ON tstationlists.MARKETID = TMARKETS.OBJID
         inner join ft.tBYSerCardRepConLinks@lservice_130
            on tByServiceCards.objid = tBYSerCardRepConLinks.BYServiceCardID
         inner join ft.tRepContracts@lservice_130
            on tBYSerCardRepConLinks.RepContractID = tRepContracts.ObjID
          left join ft.tOutSettles@lservice_130
            on tbyservicecards.objid = tOutSettles.byserviceid
           and tOutSettles.isvalid > 0
         inner join ft.tBrands@lservice_130
            on tBYServiceCards.BrandID = tBrands.ObjID
         inner join ft.tProductLines@lservice_130
            on tBYServiceCards.ProductLineID = tProductLines.ObjID
         inner join ft.TPURCHASEINFOS@lservice_130
            on tRepContracts.SerialCode = TPURCHASEINFOS.LEAVE_FACTORY_CODE
         inner join ft.tDutyUnits@lservice_130 d1
            on tbyservicecards.SPunitID = d1.objid
         where 1 = 1 and TPURCHASEINFOS.VINCODE=pvin and tByServiceCards.createtime>=to_date('2013-1-1','yyyy-mm-dd')) v1
    ;
  return(result);
end Getrepairclaimbills;


create or replace function GetRepairOrders
(
  PVIN  char
) return sys_refcursor as
  result    sys_refcursor;
begin
    open result for
    select tRepmaterials.MATERIALCODE 配件图号,
       tRepmaterials.MATERIALNAME 配件名称,
       TREPMATERIALS.AMOUNT 数量,
       decode(TREPMATERIALS.ISTRANSPORT,0,'否',1,'是') 是否发运,
       TREPMATERIALS.SPAREPRICE 配件维修价,
       TREPMATERIALS.BSPAREPRICE 调整后配件维修价,
      -- TREPMATERIALS.SETTLEPROPAS 结算属性,
       TREPMATERIALS.OLDPARTCODE 旧件图号,
       '<' || TREPMATERIALS.SECURITYCODE || '>' as  防伪编码,
       TREPMATERIALS.BARCODE 旧件条码,
       TREPMATERIALS.ISORIGINAL 是否装车件,
       TREPMATERIALS.BATCHCODE 新件批次号,
       TREPMATERIALS.SPEC "规格型号",
       TREPMATERIALS.SUPPLYCODE 旧件供应商编号,
       TREPMATERIALS.SUPPLYNAME 旧件供应商名称,
       TREPITEMS.WORKITEMCODE 维修项目编号,
       TREPITEMS.WORKITEMNAME 维修项目名称,
       decode(TREPITEMS.SETTLEPROP,1,'索赔',2,'免费',3,'自费',4 ,'返修') as 结算属性,
       TREPITEMS.BWORKHOUR 调整后工时,
       TREPITEMS.FINISHDATE 完成时间,
       TREPITEMS.WORKHOUR 工时,
       TREPFAULTREASONS.FAULTREASONCODE 故障原因编码,
       TREPFAULTREASONS.FAULTREASONNAME 故障原因名称,
       decode (TREPFAULTREASONS.FREASONSPTYPE,0,'配件索赔',1,'装车件索赔',2,'非索赔') 故障索赔类型,
       TREPFAULTREASONS.CAUSEMCODE 祸首件图号,
       TREPFAULTREASONS.CAUSEMNAME 祸首件名称,
       TREPFAULTREASONS.MEMO as 故障原因备注信息,
       TREPCONTRACTS.CODE 维修单编号,
     --  TREPCONTRACTS.ENTERCODE,
       TREPCONTRACTS.ISVALID 单据状态,
       TREPCONTRACTS.SERIALCODE 出厂编号,
       TREPCONTRACTS.RANGE 国家规定标准里程,
       TREPCONTRACTS.REPAIRPROP 维修属性,
       decode(TREPCONTRACTS.OLDDEALTYPE,1,'客户带走',2,'服务站处理',3,'其他') 旧件处理方式,
     --  TREPCONTRACTS.WORKFEE,
     --  TREPCONTRACTS.MATFEE,
     --  TREPCONTRACTS.FMATFEE,
     --  TREPCONTRACTS.OTHERFEE,
     --  TREPCONTRACTS.ALLFEE,
       TREPCONTRACTS.CARTHING 随车物件,
       TREPCONTRACTS.CARACTUALITY 车况描述,
       TREPCONTRACTS.FAULTINFO 故障现象及原因描述,
       TREPCONTRACTS.CAUSEANALYSE 外出原因,
       TREPCONTRACTS.DEALMETHOD 处理方法,
       TREPCONTRACTS.DEALRESULT 处理结果,
       TREPCONTRACTS.RELATEDEXPLAIN 连带责任说明,
      -- TREPCONTRACTS.MONITORID,
       decode(TREPCONTRACTS.SPSETTLESTATUS,1,'无索赔',2,'产生索赔' ,4,'全部生成索赔单')保用服务状态,
       TREPCONTRACTS.FAULTANALYSIS 客户意见,
       TREPCONTRACTS.REPBEGINTIME 报修时间,
       TREPCONTRACTS.FAITHFINTIME 预定交车时间,
       TREPCONTRACTS.OUTPLACE 外出救援地,
       TREPCONTRACTS.OUTKM 结算里程,
       TREPCONTRACTS.GOTIME 出发时间,
       TREPCONTRACTS.RETURNTIME 返回时间,
       TREPCONTRACTS.OUTPERSONCODE 外出人员编号,
       decode(TREPCONTRACTS.ISSELFCAR,1,'否',2,'是') 是否使用自备车,
       TREPCONTRACTS.MEMO 备注信息,
       TREPCONTRACTS.CREATETIME 单据创建时间,
       TREPCONTRACTS.CREATORCODE 单据创建人编号,
    --   TREPCONTRACTS.MODIFYTIME,
    --   TREPCONTRACTS.MODIFIERCODE,
       TREPCONTRACTS.ENROLLNUM 车牌号,
       TREPCONTRACTS.WORKFEESTAND 工时费单价,
       TREPCONTRACTS.OTHERFEEREASON 其他费用产生原因,
    --   TREPCONTRACTS.ISCALLED,
    --   TREPCONTRACTS.CONTENTDEGREE,
    --   TREPCONTRACTS.CALLOPINION,
     --  TREPCONTRACTS.BRANDID,
       decode(TREPCONTRACTS.ISSPAPPLY,0,'否',1,'是') 是否包括重大或优惠索赔,
       TREPCONTRACTS.CUSTNAME 客户姓名,
       TREPCONTRACTS.CUSTPHONE 固定电话,
       TREPCONTRACTS.CUSTMOBILE 移动电话,
       TREPCONTRACTS.RUNRANGE 运行里程,
       TREPCONTRACTS.QBTIME 强保次数,
      -- TREPCONTRACTS.ISROADVEHICLE,
      -- TREPCONTRACTS.BRIDGETYPE,
      -- TREPCONTRACTS.ISNEEDSP,
      -- TREPCONTRACTS.ISOUTSP,
      decode(tRepContracts.ADDITIONAL,1,'驾送维修',2,'经销商库存车维修',3,'经销商接车维修') AS 维修附加属性,
     --  TREPCONTRACTS.RECENTREPTIME,
     --  TREPCONTRACTS.REPINTERVAL,
       TREPCONTRACTS.REPBEGINTIME as  故障发生时间,
       TPURCHASEINFOS.PRODUCT_TYPE_NAME 车型,
       TPURCHASEINFOS.ENGINE_MODEL 发动机型号,
       TPURCHASEINFOS.ENGINE_CODE 发动机编号,
       TPURCHASEINFOS.VINCODE vin码,
       TPURCHASEINFOS.LEAVE_FACTORY_DATE 生产日期,
       TPURCHASEINFOS.SALES_DATE 购买日期,
       TPURCHASEINFOS.MILE 首次故障里程,
       TREPCONTRACTS.CUSTNAME as 联系人姓名,
       TPURCHASELINKERS.ADDRESS 联系人地址,
       D1.Name as  事业部,
       D2.code as  外出责任单位编号,
       D2.Name as  外出责任单位名称,
       D3.code as  祸首件供应商编号,
  --     D3.Name as DutyUnitName,
       TAPPLYREPORTS.Code As 索赔单编号,
       TCUSTREGISTERS.Code As 来客登记单编号,
       TSERVERPLOYSETS.Code As  服务活动编号,
       tstations.code as  服务站编号,
       tstations.name as  服务站名称
  from FT.TREPFAULTREASONS@lservice_130
  left JOIN FT.TREPITEMS@lservice_130 ON TREPITEMS.PARENTID = FT.TREPFAULTREASONS.OBJID
  left JOIN FT.TREPMATERIALS@lservice_130 ON TREPMATERIALS.PARENTID = FT.TREPITEMS.OBJID
 inner JOIN FT.TREPCONTRACTS@lservice_130 ON TREPFAULTREASONS.PARENTID =
                                FT.TREPCONTRACTS.OBJID
 inner JOIN FT.TPURCHASEINFOS@lservice_130 ON TREPCONTRACTS.SERIALCODE =
                                 FT.TPURCHASEINFOS.LEAVE_FACTORY_CODE
  left JOIN FT.TPURCHASELINKERS@lservice_130 ON TPURCHASEINFOS.OBJID =
                                   TPURCHASELINKERS.PARENTID
                               AND TREPCONTRACTS.CUSTNAME =
                                   TPURCHASELINKERS.NAME
 inner join ft.tstations@lservice_130 on trepcontracts.entercode = tstations.objid
  left join ft.tstationlists@lservice_130 on tstations.objid = tstationlists.stationid
                            and trepcontracts.brandid =
                                tstationlists.brandid
 inner join ft.tdutyunits@lservice_130 d1 on trepcontracts.ProductComID = d1.objid
  left join ft.tdutyunits@lservice_130 d2 on trepcontracts.dutyunitid = d2.objid
  left join ft.tdutyunits@lservice_130 d3 on trepfaultreasons.dutyunitid = d3.objid
  Left Join ft.TAPPLYREPORTLINKS@lservice_130 On tRepcontracts.ObjID =
                                    TAPPLYREPORTLINKS.RepContractID
  Left Join ft.TAPPLYREPORTS@lservice_130 on TAPPLYREPORTLINKS.ApplyReportID =
                                TAPPLYREPORTS.ObjID
 inner Join ft.TCUSREGREPCONLINKS@lservice_130 On trepcontracts.ObjID =
                                     TCUSREGREPCONLINKS.RepContractID
 inner Join ft.TCUSTREGISTERS@lservice_130 On TCUSREGREPCONLINKS.CustRegID =
                                 TCUSTREGISTERS.ObjID
  Left Outer Join ft.TSEVPLOYSETREPCONLINKS@lservice_130 On trepcontracts.ObjID =
                                               TSEVPLOYSETREPCONLINKS.RepContractID
  Left Outer Join ft.TSERVERPLOYSETS@lservice_130 On TSEVPLOYSETREPCONLINKS.ServerPloySetID =
                                        TSERVERPLOYSETS.ObjID
 where 1 = 1
 /*  And "TREPCONTRACTS".BrandID = '{DBE00437-FE9F-426C-B090-AD1F4EC317D3}'
   And "TREPCONTRACTS".ProductLineID =
       '{5645FF9E-8EB7-4C5F-8B06-EAA4D060BB81}'*/
   And "TREPCONTRACTS".createtime >= TO_DATE('2013-1-1', 'yyyy:MM:DD') and TPURCHASEINFOS.vincode=pvin;
  -- And "TREPCONTRACTS".createtime < TO_DATE('2014-4-11', 'yyyy:MM:DD');
  return(result);
end GetRepairOrders;


create or replace function GetRepairOrdersForPMS
(
  PVIN  char
) return sys_refcursor as
  result    sys_refcursor;
begin
    open result for
    select tRepmaterials.MATERIALCODE 配件图号,
       tRepmaterials.MATERIALNAME 配件名称,
       TREPMATERIALS.AMOUNT 数量,
       decode(TREPMATERIALS.ISTRANSPORT,0,'否',1,'是') 是否发运,
       TREPMATERIALS.SPAREPRICE 配件维修价,
       TREPMATERIALS.BSPAREPRICE 调整后配件维修价,
      -- TREPMATERIALS.SETTLEPROPAS 结算属性,
       TREPMATERIALS.OLDPARTCODE 旧件图号,
       '<' || TREPMATERIALS.SECURITYCODE || '>' as  防伪编码,
       TREPMATERIALS.BARCODE 旧件条码,
       TREPMATERIALS.ISORIGINAL 是否装车件,
       TREPMATERIALS.BATCHCODE 新件批次号,
       TREPMATERIALS.SPEC 规格型号,
       TREPMATERIALS.SUPPLYCODE 旧件供应商编号,
       TREPMATERIALS.SUPPLYNAME 旧件供应商名称,
       TREPITEMS.WORKITEMCODE 维修项目编号,
       TREPITEMS.WORKITEMNAME 维修项目名称,
       decode(TREPITEMS.SETTLEPROP,1,'索赔',2,'免费',3,'自费',4 ,'返修') as 结算属性,
       TREPITEMS.BWORKHOUR 调整后工时,
       TREPITEMS.FINISHDATE 完成时间,
       TREPITEMS.WORKHOUR 工时,
       TREPFAULTREASONS.FAULTREASONCODE 故障原因编码,
       TREPFAULTREASONS.FAULTREASONNAME 故障原因名称,
       decode (TREPFAULTREASONS.FREASONSPTYPE,0,'配件索赔',1,'装车件索赔',2,'非索赔') 故障索赔类型,
       TREPFAULTREASONS.CAUSEMCODE 祸首件图号,
       TREPFAULTREASONS.CAUSEMNAME 祸首件名称,
       TREPFAULTREASONS.MEMO as 故障原因备注信息,
       TREPCONTRACTS.CODE 维修单编号,
     --  TREPCONTRACTS.ENTERCODE,
       TREPCONTRACTS.ISVALID 单据状态,
       TREPCONTRACTS.SERIALCODE 出厂编号,
       TREPCONTRACTS.RANGE 国家规定标准里程,
       TREPCONTRACTS.REPAIRPROP 维修属性,
       decode(TREPCONTRACTS.OLDDEALTYPE,1,'客户带走',2,'服务站处理',3,'其他') 旧件处理方式,
     --  TREPCONTRACTS.WORKFEE,
     --  TREPCONTRACTS.MATFEE,
     --  TREPCONTRACTS.FMATFEE,
     --  TREPCONTRACTS.OTHERFEE,
     --  TREPCONTRACTS.ALLFEE,
       TREPCONTRACTS.CARTHING 随车物件,
       TREPCONTRACTS.CARACTUALITY 车况描述,
       TREPCONTRACTS.FAULTINFO 故障现象及原因描述,
       TREPCONTRACTS.CAUSEANALYSE 外出原因,
       TREPCONTRACTS.DEALMETHOD 处理方法,
       TREPCONTRACTS.DEALRESULT 处理结果,
       TREPCONTRACTS.RELATEDEXPLAIN 连带责任说明,
      -- TREPCONTRACTS.MONITORID,
       decode(TREPCONTRACTS.SPSETTLESTATUS,1,'无索赔',2,'产生索赔' ,4,'全部生成索赔单')保用服务状态,
       TREPCONTRACTS.FAULTANALYSIS 客户意见,
       TREPCONTRACTS.REPBEGINTIME 报修时间,
       TREPCONTRACTS.FAITHFINTIME 预定交车时间,
       TREPCONTRACTS.OUTPLACE 外出救援地,
       TREPCONTRACTS.OUTKM 结算里程,
       TREPCONTRACTS.GOTIME 出发时间,
       TREPCONTRACTS.RETURNTIME 返回时间,
       TREPCONTRACTS.OUTPERSONCODE 外出人员编号,
       decode(TREPCONTRACTS.ISSELFCAR,1,'否',2,'是') 是否使用自备车,
       TREPCONTRACTS.MEMO 备注信息,
       TREPCONTRACTS.CREATETIME 单据创建时间,
       TREPCONTRACTS.CREATORCODE 单据创建人编号,
    --   TREPCONTRACTS.MODIFYTIME,
    --   TREPCONTRACTS.MODIFIERCODE,
       TREPCONTRACTS.ENROLLNUM 车牌号,
       TREPCONTRACTS.WORKFEESTAND 工时费单价,
       TREPCONTRACTS.OTHERFEEREASON 其他费用产生原因,
    --   TREPCONTRACTS.ISCALLED,
    --   TREPCONTRACTS.CONTENTDEGREE,
    --   TREPCONTRACTS.CALLOPINION,
     --  TREPCONTRACTS.BRANDID,
       decode(TREPCONTRACTS.ISSPAPPLY,0,'否',1,'是') 是否包括重大或优惠索赔,
       TREPCONTRACTS.CUSTNAME 客户姓名,
       TREPCONTRACTS.CUSTPHONE 固定电话,
       TREPCONTRACTS.CUSTMOBILE 移动电话,
       TREPCONTRACTS.RUNRANGE 运行里程,
       TREPCONTRACTS.QBTIME 强保次数,
      -- TREPCONTRACTS.ISROADVEHICLE,
      -- TREPCONTRACTS.BRIDGETYPE,
      -- TREPCONTRACTS.ISNEEDSP,
      -- TREPCONTRACTS.ISOUTSP,
      decode(tRepContracts.ADDITIONAL,1,'驾送维修',2,'经销商库存车维修',3,'经销商接车维修') AS 维修附加属性,
     --  TREPCONTRACTS.RECENTREPTIME,
     --  TREPCONTRACTS.REPINTERVAL,
       TREPCONTRACTS.REPBEGINTIME as  故障发生时间,
       TPURCHASEINFOS.PRODUCT_TYPE_NAME 车型,
       TPURCHASEINFOS.ENGINE_MODEL 发动机型号,
       TPURCHASEINFOS.ENGINE_CODE 发动机编号,
       TPURCHASEINFOS.VINCODE vin码,
       TPURCHASEINFOS.LEAVE_FACTORY_DATE 生产日期,
       TPURCHASEINFOS.SALES_DATE 购买日期,
       TPURCHASEINFOS.MILE 首次故障里程,
       TREPCONTRACTS.CUSTNAME as 联系人姓名,
       TPURCHASELINKERS.ADDRESS 联系人地址,
       D1.Name as  事业部,
       D2.code as  外出责任单位编号,
       D2.Name as  外出责任单位名称,
       D3.code as  祸首件供应商编号,
  --     D3.Name as DutyUnitName,
       TAPPLYREPORTS.Code As 索赔单编号,
       TCUSTREGISTERS.Code As 来客登记单编号,
       TSERVERPLOYSETS.Code As  服务活动编号,
       tstations.code as  服务站编号,
       tstations.name as  服务站名称
  from FT.TREPFAULTREASONS@lservice_130
  left JOIN FT.TREPITEMS@lservice_130 ON TREPITEMS.PARENTID = FT.TREPFAULTREASONS.OBJID
  left JOIN FT.TREPMATERIALS@lservice_130 ON TREPMATERIALS.PARENTID = FT.TREPITEMS.OBJID
 inner JOIN FT.TREPCONTRACTS@lservice_130 ON TREPFAULTREASONS.PARENTID =
                                FT.TREPCONTRACTS.OBJID
 inner JOIN FT.TPURCHASEINFOS@lservice_130 ON TREPCONTRACTS.SERIALCODE =
                                 FT.TPURCHASEINFOS.LEAVE_FACTORY_CODE
  left JOIN FT.TPURCHASELINKERS@lservice_130 ON TPURCHASEINFOS.OBJID =
                                   TPURCHASELINKERS.PARENTID
                               AND TREPCONTRACTS.CUSTNAME =
                                   TPURCHASELINKERS.NAME
 inner join ft.tstations@lservice_130 on trepcontracts.entercode = tstations.objid
  left join ft.tstationlists@lservice_130 on tstations.objid = tstationlists.stationid
                            and trepcontracts.brandid =
                                tstationlists.brandid
 inner join ft.tdutyunits@lservice_130 d1 on trepcontracts.ProductComID = d1.objid
  left join ft.tdutyunits@lservice_130 d2 on trepcontracts.dutyunitid = d2.objid
  left join ft.tdutyunits@lservice_130 d3 on trepfaultreasons.dutyunitid = d3.objid
  Left Join ft.TAPPLYREPORTLINKS@lservice_130 On tRepcontracts.ObjID =
                                    TAPPLYREPORTLINKS.RepContractID
  Left Join ft.TAPPLYREPORTS@lservice_130 on TAPPLYREPORTLINKS.ApplyReportID =
                                TAPPLYREPORTS.ObjID
 inner Join ft.TCUSREGREPCONLINKS@lservice_130 On trepcontracts.ObjID =
                                     TCUSREGREPCONLINKS.RepContractID
 inner Join ft.TCUSTREGISTERS@lservice_130 On TCUSREGREPCONLINKS.CustRegID =
                                 TCUSTREGISTERS.ObjID
  Left Outer Join ft.TSEVPLOYSETREPCONLINKS@lservice_130 On trepcontracts.ObjID =
                                               TSEVPLOYSETREPCONLINKS.RepContractID
  Left Outer Join ft.TSERVERPLOYSETS@lservice_130 On TSEVPLOYSETREPCONLINKS.ServerPloySetID =
                                        TSERVERPLOYSETS.ObjID
 where 1 = 1
 /*  And "TREPCONTRACTS".BrandID = '{DBE00437-FE9F-426C-B090-AD1F4EC317D3}'
   And "TREPCONTRACTS".ProductLineID =
       '{5645FF9E-8EB7-4C5F-8B06-EAA4D060BB81}'*/
   And TREPCONTRACTS.createtime >= TO_DATE('2013-1-1', 'yyyy:MM:DD') ;
  -- And "TREPCONTRACTS".createtime < TO_DATE('2014-4-11', 'yyyy:MM:DD');
  return(result);
end GetRepairOrdersForPMS;
/