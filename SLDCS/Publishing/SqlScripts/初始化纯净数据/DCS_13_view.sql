
-- 配件索赔价
create or replace view partsclaimprice as
select PartsBranch.BranchId                  as BranchId,
       SparePart.Id                          as UsedPartsId,
       SparePart.Code                        as UsedPartsCode,
       SparePart.Name                        as UsedPartsName,
       PartsBranch.PartsWarrantyCategoryId,
       PartsBranch.PartsWarrantyCategoryName,
       PartsBranch.PartsSalesCategoryId,
       PartsSupplierRelation.SupplierId      as PartsSupplierId,
       PartsSupplier.Code                    as PartsSupplierCode,
       PartsSupplier.Name                    as PartsSupplierName,
       PartsSalesPrice.SalesPrice,
       SparePart.MeasureUnit,
       PARTSBRANCH.PARTSRETURNPOLICY         as UsedPartsReturnPolicy,
       SparePart.Specification,
       SparePart.Feature,
       --PartsWarrantyTerm.PartsWarrantyType,
       SparePart.PartType,
       --WarrantyPolicy.Category as WarrantyPolicyCategory ,
       PartDeleaveInformation.Deleaveamount   as Deleaveamount,
       PartDeleaveInformation.DeleavePartCode as DeleavePartCode,
       0 dealerId,
       0 subdealerid,
       0 as quantity
  from SparePart
 inner join PartsSalesPrice
    on PartsSalesPrice.SparePartId = SparePart.Id
 inner join PartsBranch
    on SparePart.Id = PartsBranch.PartId
   and PartsBranch.
 Partssalescategoryid = PartsSalesPrice.Partssalescategoryid
 inner join PartsSupplierRelation
    on PartsSupplierRelation.PartId = SparePart.Id
   and PartsSupplierRelation.Partssalescategoryid =
       PartsBranch.Partssalescategoryid
 inner join PartsSupplier
    on PartsSupplier.Id = PartsSupplierRelation.SupplierId
--left join PartsWarrantyTerm on PartsWarrantyTerm.PartsWarrantyCategoryId = PartsBranch.PartsWarrantyCategoryId
--left join WarrantyPolicy on PartsWarrantyTerm.WarrantyPolicyId= WarrantyPolicy.Id and WarrantyPolicy.Status=1 and WarrantyPolicy.Applicationscope=2 and WarrantyPolicy.Partswarrantyterminvolved=1
  left join PartDeleaveInformation
    on PartDeleaveInformation.Oldpartid = SparePart.Id
   and

       PartDeleaveInformation.Branchid = PartsBranch.Branchid
   and

       PartDeleaveInformation.Partssalescategoryid =
       PartsBranch.Partssalescategoryid
  --  left join dealerpartsstock on dealerpartsstock.sparepartid=SparePart.id
   -- left join dealer on dealerpartsstock.dealerid=dealer.id
 where SparePart.Status = 1
   and PartsBranch.Status = 1
   and PartsSupplierRelation.Status = 1
   and PartsSalesPrice.Status = 1
   and PartsSupplier.Status = 1
      --and PartsSalesPrice.Ifclaim = 1
   and exists
 (select *
          from PartsSalesCategory
         where PartsSalesCategory.Id = PartsSalesPrice.PartsSalesCategoryId
           and Status = 1);
/



-- 服务产品线产品关系
create or replace view ServProdLineAffiProduct as
select ProductId,ServiceProductLineId,ProductLinetype from
(
select Product.id as ProductId,
       case
         when ServProdLineProductDetail.Serviceproductlineid is null then
           ServiceProdLineProduct.Serviceproductlineid
         else
           ServProdLineProductDetail.Serviceproductlineid
       end as ServiceProductLineId,
       1 as ProductLinetype
  from Product
 left join ServiceProdLineProduct
    on Product.Brandid = ServiceProdLineProduct.Brandid
  left join ServProdLineProductDetail
    on Product.Id = ServProdLineProductDetail.Productid
union
select Product.id                                 as ProductId,
       EngineModelProductLine.Engineproductlineid as ServiceProductLineId,
       2                                          as ProductLinetype
  from EngineModelProductLine
 inner join EngineModel
    on EngineModelProductLine.Enginemodeid = EngineModel.Id
 inner join Product
    on Product.EngineTypeCode = EngineModel.Enginemodel)
    where ProductId is not null and  ServiceProductLineId is not null;
/

-- 车辆种类产品关系
create or replace view VehicleCategoryAffiProduct as
select VehicleCategoryId,
       ProductId
  from VehicleCategoryProductDetail
/

-- 责任单位产品关系
create or replace view ResponsibleUnitAffiProduct as
select ResponsibleUnitId,
       ProductId
  from ResponsibleUnitProductDetail
/

-- 库区库位及配件库存
create or replace view WarehouseAreaWithPartsStock as
select WarehouseArea.Id as  WarehouseAreaId,
       WarehouseArea.WarehouseId,
       WarehouseArea.ParentId as WarehouseAreaParentId,
       WarehouseArea.Code as WarehouseAreaCode,
       WarehouseArea.TopLevelWarehouseAreaId,
       WarehouseArea.AreaCategoryId,
       WarehouseArea.Status,
       WarehouseArea.Remark,
       WarehouseArea.AreaKind,
       WarehouseAreaCategory.Category as AreaCategory,
       Warehouse.Code as WarehouseCode,
       Warehouse.Name as WarehouseName,
       Warehouse.Type as WarehouseType,
       Warehouse.BranchId,
       Warehouse.StorageCompanyId,
       Warehouse.StorageCompanyType,
       nvl(PartsStock.PartId,0) as SparePartId,
       PartsStock.Quantity,
       SparePart.Code as SparePartCode,
       SparePart.Name as SparePartName
  from WarehouseArea
 inner join Warehouse on WarehouseArea.WarehouseId = Warehouse.Id
  left join WarehouseAreaCategory on WarehouseArea.AreaCategoryId = WarehouseAreaCategory.Id
  left join PartsStock on WarehouseArea.Id = PartsStock.WarehouseAreaId
  left join SparePart on PartsStock.PartId = SparePart.Id
/

-- 销售车型以及零售指导价
create or replace view VehModelRetailSuggestedPrice as
select distinct 
       ProductCategory.Id as ProductCategoryId,
       ProductCategory.Code as ProductCategoryCode,
       ProductCategory.Name as ProductCategoryName,  
       VehicleRetailSuggestedPrice.Price
  from VehicleRetailSuggestedPrice
 inner join Product on Product.Id = VehicleRetailSuggestedPrice.ProductId
 inner join ProductAffiProductCategory on ProductAffiProductCategory.ProductId = Product.Id
 inner join ProductCategory on ProductCategory.Id = ProductAffiProductCategory.ProductCategoryId
 where Product.Status = 1 
   and ProductCategory.Status = 1 
   and VehicleRetailSuggestedPrice.Status = 2
/


--服务产品线（视图）
create or replace view serviceproductlineview as
select ServiceProductLine.Id                   as ProductLineId,
       1                                       as ProductLineType,
       ServiceProductLine.Code as ProductLineCode,
       ServiceProductLine.Name as ProductLineName,
       ServiceProductLine.Partssalescategoryid,
       ServiceProductLine.Branchid
  from ServiceProductLine
 where ServiceProductLine.Status = 1
union all
select EngineProductLine.Id                   as ProductLineId,
       2 as ProductLineType,
       EngineProductLine.Enginecode as ProductLineCode,
       EngineProductLine.Enginename as ProductLineName,
       EngineProductLine.Partssalescategoryid,
       EngineProductLine.Branchid
  from EngineProductLine
 where EngineProductLine.Status=1;
/
--维修项目（视图）
create or replace view repairitemview as
select repairitem.id               as RepairItemId,
       RepairItem.Code             as Code,
       RepairItem.Name             as Name,
       RepairItemCategory.id       as RepairItemCategoryId,
       RepairItemCategory.code     as RepairItemCategoryCode,
       RepairItemCategory.Name     as RepairItemCategoryName,
       RepairItemCategory.parentid as ParentId,
       Parent_RepairItemCategory.Code as ParentCode,
        GParent_RepairItemCategory.Code as GrandParentCode,
       RepairItemBrandRelation.Id as MalfunctionBrandRelationId,
       Parent_RepairItemCategory.Name as ParentName,
       GParent_RepairItemCategory.Name as GrandParentName,       
       RepairItemBrandRelation.Partssalescategoryid,
       Partssalescategory.Name     as PartssalescategoryName
  from RepairItemCategory
 inner join RepairItemCategory Parent_RepairItemCategory
  on RepairItemCategory.Parentid =Parent_RepairItemCategory.Id
   inner join RepairItemCategory GParent_RepairItemCategory
  on Parent_RepairItemCategory.Parentid=GParent_RepairItemCategory.id
 inner join repairitem
    on repairitem.repairitemcategoryid = RepairItemCategory.Id
   and repairitem.status = 1
 inner join RepairItemBrandRelation
    on RepairItemBrandRelation.Repairitemid = repairitem.id
   and RepairItemBrandRelation.Status = 1
 inner join Partssalescategory
 on Partssalescategory.Id=RepairItemBrandRelation.Partssalescategoryid
 where RepairItemCategory.status = 1
   and not exists
 (select *
          from LayerNodeType
         where RepairItemCategory.Layernodetypeid = LayerNodeType.Id
           and LayerNodeType.Toplevelnode = 1);

/
--故障原因（视图）
create or replace view malfunctionview as
select Malfunction.id               as MalfunctionId,
       Malfunction.Code             as Code,
       Malfunction.Description      as Description,
       MalfunctionCategory.id       as MalfunctionCategoryId,
       MalfunctionCategory.code     as MalfunctionCategoryCode,
       MalfunctionCategory.Name     as MalfunctionCategoryName,
       MalfunctionCategory.parentid as ParentId,
       Parent_MalfunctionCategory.Code as ParentCode,
       Parent_MalfunctionCategory.Name as ParentName,
       GParent_MalfunctionCategory.code as GrandParentCode,
       GParent_MalfunctionCategory.Name as GrandParentName,
       MalfunctionBrandRelation.Id as MalfunctionBrandRelationId,
       MalfunctionBrandRelation.Partssalescategoryid,
       Partssalescategory.Name     as PartssalescategoryName
  from MalfunctionCategory
 inner join MalfunctionCategory Parent_MalfunctionCategory
  on MalfunctionCategory.Parentid =Parent_MalfunctionCategory.Id
 inner join MalfunctionCategory GParent_MalfunctionCategory
  on Parent_MalfunctionCategory.Parentid=GParent_MalfunctionCategory.id
 inner join Malfunction
    on Malfunction.MalfunctionCategoryId = MalfunctionCategory.Id
   and Malfunction.status = 1
 inner join MalfunctionBrandRelation
    on MalfunctionBrandRelation.Malfunctionid = Malfunction.id
   and MalfunctionBrandRelation.Status = 1
 inner join Partssalescategory
 on Partssalescategory.Id=MalfunctionBrandRelation.Partssalescategoryid
 where MalfunctionCategory.status = 1
   and not exists
 (select *
          from LayerNodeType
         where MalfunctionCategory.Layernodetypeid = LayerNodeType.Id
           and LayerNodeType.Toplevelnode = 1);
/
--维修项目与服务产品线关系（视图）
create or replace view repairitemlaborhourview as
select repairitem.id               as RepairItemId,
       RepairItem.Code             as Code,
       RepairItem.Name             as Name,
       RepairItemCategory.id       as RepairItemCategoryId,
       RepairItemCategory.code     as RepairItemCategoryCode,
       RepairItemCategory.Name     as RepairItemCategoryName,
       RepairItemCategory.parentid as ParentId,
       Parent_RepairItemCategory.Code as ParentCode,
       RepairItemBrandRelation.Id as MalfunctionBrandRelationId,
       Parent_RepairItemCategory.Name as ParentName,
       RepairItemBrandRelation.Partssalescategoryid,
       Partssalescategory.Name     as PartssalescategoryName,
       RepairItemAffiServProdLine.Serviceproductlineid,
       RepairItemAffiServProdLine.Defaultlaborhour,
       RepairItemAffiServProdLine.ProductLineType as ProductLineType
  from RepairItemCategory
 inner join RepairItemCategory Parent_RepairItemCategory
  on RepairItemCategory.Parentid =Parent_RepairItemCategory.Id
 inner join repairitem
    on repairitem.repairitemcategoryid = RepairItemCategory.Id
   and repairitem.status = 1
 inner join RepairItemBrandRelation
    on RepairItemBrandRelation.Repairitemid = repairitem.id
   and RepairItemBrandRelation.Status = 1
 inner join Partssalescategory
 on Partssalescategory.Id=RepairItemBrandRelation.Partssalescategoryid
 inner join RepairItemAffiServProdLine on RepairItemAffiServProdLine.RepairItemId =RepairItem.Id
 where RepairItemCategory.status = 1 and RepairItemAffiServProdLine.Status=1
   and not exists
 (select *
          from LayerNodeType
         where RepairItemCategory.Layernodetypeid = LayerNodeType.Id
           and LayerNodeType.Toplevelnode = 1);
/
--WMS仓库配件冻结库存（视图）
create or replace view WmsCongelationStockView
as 
select d.Id                     as WmsStockId, --  WMS冻结库存Id
       d.BranchId               as BranchId, --  营销分公司Id
       d.BranchCode             as BranchCode, --  营销分公司编号
       d.BranchName             as BranchName, --  营销分公司名称
       d.StorageCenter          as StorageCenter, --  储运中心
       d.PartsSalesCategoryId   as PartsSalesCategoryId, --  配件销售类型Id
       d.PartsSalesCategoryCode as PartsSalesCategoryCode, --  配件销售类型编号
       d.PartsSalesCategoryName as PartsSalesCategoryName, --  配件销售类型名称
       d.SparePartId            as SparePartId, --  配件Id
       d.SparePartCode          as SparePartCode, --  配件编号
       d.SparePartName          as SparePartName, --  配件名称
       d.CongelationStockQty    as CongelationStockQty, --  冻结库存
       d.DisabledStock          as DisabledStock, --  不可用库存
       a.Id                     as WarehouseId, --  仓库Id
       a.StorageCompanyId       as StorageCompanyId --  仓储企业Id
  from Warehouse a
 inner join SalesUnitAffiWarehouse b
    on a.Id = b.WarehouseId
 inner join SalesUnit c
    on b.SalesUnitId = c.Id
 inner join WmsCongelationStock d
    on c.PartsSalesCategoryId = d.PartsSalesCategoryId
 where d.StorageCenter = a.storagecenter
   and d.Branchid = a.branchid
   and a.WmsInterface = 1;
/



--保外配件索赔价
create or replace view partsclaimpriceforbw as
select nvl(PartsBranch.BranchId,0)                  as BranchId,
       nvl(SparePart.Id,0)                          as UsedPartsId,
       nvl(SparePart.Code,' ')                         as UsedPartsCode,
       nvl(SparePart.Name,' ')                       as UsedPartsName,
       nvl(PartsBranch.PartsWarrantyCategoryId,0) PartsWarrantyCategoryId,
       nvl(PartsBranch.PartsWarrantyCategoryName,' ') PartsWarrantyCategoryName,
       nvl(PartsBranch.PartsSalesCategoryId,0) PartsSalesCategoryId,
       nvl(PartsSupplierRelation.SupplierId,0)       as PartsSupplierId,
       nvl(PartsSupplier.Code,' ')                    as PartsSupplierCode,
       nvl(PartsSupplier.Name,' ')                    as PartsSupplierName,
       nvl(PartsSalesPrice.SalesPrice,0) SalesPrice,
       nvl(SparePart.MeasureUnit,' ') MeasureUnit,
       nvl(PARTSBRANCH.PARTSRETURNPOLICY ,0)        as UsedPartsReturnPolicy,
       nvl(SparePart.Specification,'') Specification,
       nvl(SparePart.Feature,' ') Feature,
       --PartsWarrantyTerm.PartsWarrantyType,
       nvl(SparePart.PartType,0) PartType,
       --WarrantyPolicy.Category as WarrantyPolicyCategory ,
       nvl(PartDeleaveInformation.Deleaveamount,0)   as Deleaveamount,
       nvl(PartDeleaveInformation.DeleavePartCode,' ') as DeleavePartCode
  from SparePart
  left join PartsSalesPrice
    on PartsSalesPrice.SparePartId = SparePart.Id
 left join PartsBranch
    on SparePart.Id = PartsBranch.PartId
   and PartsBranch.
 Partssalescategoryid = PartsSalesPrice.Partssalescategoryid
  left join PartsSupplierRelation
    on PartsSupplierRelation.PartId = SparePart.Id
   and PartsSupplierRelation.Partssalescategoryid =
       PartsBranch.Partssalescategoryid
  left join PartsSupplier
    on PartsSupplier.Id = PartsSupplierRelation.SupplierId
--left join PartsWarrantyTerm on PartsWarrantyTerm.PartsWarrantyCategoryId = PartsBranch.PartsWarrantyCategoryId
--left join WarrantyPolicy on PartsWarrantyTerm.WarrantyPolicyId= WarrantyPolicy.Id and WarrantyPolicy.Status=1 and WarrantyPolicy.Applicationscope=2 and WarrantyPolicy.Partswarrantyterminvolved=1
  left join PartDeleaveInformation
    on PartDeleaveInformation.Oldpartid = SparePart.Id
   and

       PartDeleaveInformation.Branchid = PartsBranch.Branchid
   and PartDeleaveInformation.Partssalescategoryid =
       PartsBranch.Partssalescategoryid
 where SparePart.Status = 1
  -- and PartsBranch.Status = 1
  -- and PartsSupplierRelation.Status = 1
  -- and PartsSalesPrice.Status = 1
  -- and PartsSupplier.Status = 1
      --and PartsSalesPrice.Ifclaim = 1
  /* and exists
 (select *
          from PartsSalesCategory
         where PartsSalesCategory.Id = PartsSalesPrice.PartsSalesCategoryId
           and Status = 1);*/

--产品（视图）
create or replace view ProductView as
  select t.ID,
         t.CODE,
         t.PRODUCTCATEGORYCODE,
         t.PRODUCTCATEGORYNAME,
         t.BRANDID,
         t.BRANDCODE,
         t.BRANDNAME,
         t.SUBBRANDCODE,
         t.SUBBRANDNAME,
         t.TERRACECODE,
         t.TERRACENAME,
         t.PRODUCTLINE,
         t.PRODUCTNAME,
         t.OLDINTERNALCODE,
         t.ANNOUCEMENTNUMBER,
         t.TONNAGECODE,
         t.ENGINETYPECODE,
         t.ENGINEMANUFACTURERCODE,
         t.GEARSERIALTYPECODE,
         t.GEARSERIALMANUFACTURERCODE,
         t.REARAXLETYPECODE,
         t.REARAXLEMANUFACTURERCODE,
         t.TIRETYPECODE,
         t.TIREFORMCODE,
         t.BRAKEMODECODE,
         t.DRIVEMODECODE,
         t.FRAMECONNETCODE,
         t.VEHICLESIZE,
         t.AXLEDISTANCECODE,
         t.EMISSIONSTANDARDCODE,
         t.PRODUCTFUNCTIONCODE,
         t.PRODUCTLEVELCODE,
         t.VEHICLETYPECODE,
         t.COLORDESCRIBE,
         t.SEATINGCAPACITY,
         t.WEIGHT,
         t.REMARK,
         t.STATUS,
         t.ORDERCYCLE,
         t.PURCHASECYCLE,
         t.PARAMETERDESCRIPTION,
         t.VERSION,
         t.CONFIGURATION,
         t.ENGINECYLINDER,
         t.EMISSIONSTANDARD,
         t.MANUALAUTOMATIC,
         t.COLORCODE,
         t.PICTURE,
         t.CREATORID,
         t.CREATORNAME,
         t.CREATETIME,
         t.MODIFIERID,
         t.MODIFIERNAME,
         t.MODIFYTIME,
         t.ABANDONERID,
         t.ABANDONERNAME,
         t.ABANDONTIME,
         decode (v.serviceproductlineid,null,0, 1)                            as IsSetRelation,
         v.serviceproductlineid       as serviceproductlineid
    from Product t
  left join ServProdLineProductDetail v on t.id=v.productid

create or replace view repairorderquery as
select tRepmaterials.MATERIALCODE 配件图号,
       tRepmaterials.MATERIALNAME 配件名称,
       TREPMATERIALS.AMOUNT 数量,
       decode(TREPMATERIALS.ISTRANSPORT,0,'否',1,'是') 是否发运,
       TREPMATERIALS.SPAREPRICE 配件维修价,
       TREPMATERIALS.BSPAREPRICE 调整后配件维修价,
      -- TREPMATERIALS.SETTLEPROPAS 结算属性,
       TREPMATERIALS.OLDPARTCODE 旧件图号,
       '<' || TREPMATERIALS.SECURITYCODE || '>' as  防伪编码,
       TREPMATERIALS.BARCODE 旧件条码,
       TREPMATERIALS.ISORIGINAL 是否装车件,
       TREPMATERIALS.BATCHCODE 新件批次号,
       TREPMATERIALS.SPEC "规格型号",
       TREPMATERIALS.SUPPLYCODE 旧件供应商编号,
       TREPMATERIALS.SUPPLYNAME 旧件供应商名称,
       TREPITEMS.WORKITEMCODE 维修项目编号,
       TREPITEMS.WORKITEMNAME 维修项目名称,
       decode(TREPITEMS.SETTLEPROP,1,'索赔',2,'免费',3,'自费',4 ,'返修') as 结算属性,
       TREPITEMS.BWORKHOUR 调整后工时,
       TREPITEMS.FINISHDATE 完成时间,
       TREPITEMS.WORKHOUR 工时,
       TREPFAULTREASONS.FAULTREASONCODE 故障原因编码,
       TREPFAULTREASONS.FAULTREASONNAME 故障原因名称,
       decode (TREPFAULTREASONS.FREASONSPTYPE,0,'配件索赔',1,'装车件索赔',2,'非索赔') 故障索赔类型,
       TREPFAULTREASONS.CAUSEMCODE 祸首件图号,
       TREPFAULTREASONS.CAUSEMNAME 祸首件名称,
       TREPFAULTREASONS.MEMO as 故障原因备注信息,
       TREPCONTRACTS.CODE 维修单编号,
     --  TREPCONTRACTS.ENTERCODE,
       TREPCONTRACTS.ISVALID 单据状态,
       TREPCONTRACTS.SERIALCODE 出厂编号,
       TREPCONTRACTS.RANGE 国家规定标准里程,
       TREPCONTRACTS.REPAIRPROP 维修属性,
       decode(TREPCONTRACTS.OLDDEALTYPE,1,'客户带走',2,'服务站处理',3,'其他') 旧件处理方式,
     --  TREPCONTRACTS.WORKFEE,
     --  TREPCONTRACTS.MATFEE,
     --  TREPCONTRACTS.FMATFEE,
     --  TREPCONTRACTS.OTHERFEE,
     --  TREPCONTRACTS.ALLFEE,
       TREPCONTRACTS.CARTHING 随车物件,
       TREPCONTRACTS.CARACTUALITY 车况描述,
       TREPCONTRACTS.FAULTINFO 故障现象及原因描述,
       TREPCONTRACTS.CAUSEANALYSE 外出原因,
       TREPCONTRACTS.DEALMETHOD 处理方法,
       TREPCONTRACTS.DEALRESULT 处理结果,
       TREPCONTRACTS.RELATEDEXPLAIN 连带责任说明,
      -- TREPCONTRACTS.MONITORID,
       decode(TREPCONTRACTS.SPSETTLESTATUS,1,'无索赔',2,'产生索赔' ,4,'全部生成索赔单')保用服务状态,
       TREPCONTRACTS.FAULTANALYSIS 客户意见,
       TREPCONTRACTS.REPBEGINTIME 报修时间,
       TREPCONTRACTS.FAITHFINTIME 预定交车时间,
       TREPCONTRACTS.OUTPLACE 外出救援地,
       TREPCONTRACTS.OUTKM 结算里程,
       TREPCONTRACTS.GOTIME 出发时间,
       TREPCONTRACTS.RETURNTIME 返回时间,
       TREPCONTRACTS.OUTPERSONCODE 外出人员编号,
       decode(TREPCONTRACTS.ISSELFCAR,1,'否',2,'是') 是否使用自备车,
       TREPCONTRACTS.MEMO 备注信息,
       TREPCONTRACTS.CREATETIME 单据创建时间,
       TREPCONTRACTS.CREATORCODE 单据创建人编号,
    --   TREPCONTRACTS.MODIFYTIME,
    --   TREPCONTRACTS.MODIFIERCODE,
       TREPCONTRACTS.ENROLLNUM 车牌号,
       TREPCONTRACTS.WORKFEESTAND 工时费单价,
       TREPCONTRACTS.OTHERFEEREASON 其他费用产生原因,
    --   TREPCONTRACTS.ISCALLED,
    --   TREPCONTRACTS.CONTENTDEGREE,
    --   TREPCONTRACTS.CALLOPINION,
     --  TREPCONTRACTS.BRANDID,
       decode(TREPCONTRACTS.ISSPAPPLY,0,'否',1,'是') 是否包括重大或优惠索赔,
       TREPCONTRACTS.CUSTNAME 客户姓名,
       TREPCONTRACTS.CUSTPHONE 固定电话,
       TREPCONTRACTS.CUSTMOBILE 移动电话,
       TREPCONTRACTS.RUNRANGE 运行里程,
       TREPCONTRACTS.QBTIME 强保次数,
      -- TREPCONTRACTS.ISROADVEHICLE,
      -- TREPCONTRACTS.BRIDGETYPE,
      -- TREPCONTRACTS.ISNEEDSP,
      -- TREPCONTRACTS.ISOUTSP,
      decode(tRepContracts.ADDITIONAL,1,'驾送维修',2,'经销商库存车维修',3,'经销商接车维修') AS 维修附加属性,
     --  TREPCONTRACTS.RECENTREPTIME,
     --  TREPCONTRACTS.REPINTERVAL,
       TREPCONTRACTS.REPBEGINTIME as  故障发生时间,
       TPURCHASEINFOS.PRODUCT_TYPE_NAME 车型,
       TPURCHASEINFOS.ENGINE_MODEL 发动机型号,
       TPURCHASEINFOS.ENGINE_CODE 发动机编号,
       TPURCHASEINFOS.VINCODE vin码,
       TPURCHASEINFOS.LEAVE_FACTORY_DATE 生产日期,
       TPURCHASEINFOS.SALES_DATE 购买日期,
       TPURCHASEINFOS.MILE 首次故障里程,
       TREPCONTRACTS.CUSTNAME as 联系人姓名,
       TPURCHASELINKERS.ADDRESS 联系人地址,
       D1.Name as  事业部,
       D2.code as  外出责任单位编号,
       D2.Name as  外出责任单位名称,
       D3.code as  祸首件供应商编号,
  --     D3.Name as DutyUnitName,
       TAPPLYREPORTS.Code As 索赔单编号,
       TCUSTREGISTERS.Code As 来客登记单编号,
       TSERVERPLOYSETS.Code As  服务活动编号,
       tstations.code as  服务站编号,
       tstations.name as  服务站名称
  from FT.TREPFAULTREASONS@oservice_65
  left JOIN FT.TREPITEMS@oservice_65 ON TREPITEMS.PARENTID = FT.TREPFAULTREASONS.OBJID
  left JOIN FT.TREPMATERIALS@oservice_65 ON TREPMATERIALS.PARENTID = FT.TREPITEMS.OBJID
 inner JOIN FT.TREPCONTRACTS@oservice_65 ON TREPFAULTREASONS.PARENTID =
                                FT.TREPCONTRACTS.OBJID
 inner JOIN FT.TPURCHASEINFOS@oservice_65 ON TREPCONTRACTS.SERIALCODE =
                                 FT.TPURCHASEINFOS.LEAVE_FACTORY_CODE
  left JOIN FT.TPURCHASELINKERS@oservice_65 ON TPURCHASEINFOS.OBJID =
                                   TPURCHASELINKERS.PARENTID
                               AND TREPCONTRACTS.CUSTNAME =
                                   TPURCHASELINKERS.NAME
 inner join ft.tstations@oservice_65 on trepcontracts.entercode = tstations.objid
  left join ft.tstationlists@oservice_65 on tstations.objid = tstationlists.stationid
                            and trepcontracts.brandid =
                                tstationlists.brandid
 inner join ft.tdutyunits@oservice_65 d1 on trepcontracts.ProductComID = d1.objid
  left join ft.tdutyunits@oservice_65 d2 on trepcontracts.dutyunitid = d2.objid
  left join ft.tdutyunits@oservice_65 d3 on trepfaultreasons.dutyunitid = d3.objid
  Left Join ft.TAPPLYREPORTLINKS@oservice_65 On tRepcontracts.ObjID =
                                    TAPPLYREPORTLINKS.RepContractID
  Left Join ft.TAPPLYREPORTS@oservice_65 on TAPPLYREPORTLINKS.ApplyReportID =
                                TAPPLYREPORTS.ObjID
 inner Join ft.TCUSREGREPCONLINKS@oservice_65 On trepcontracts.ObjID =
                                     TCUSREGREPCONLINKS.RepContractID
 inner Join ft.TCUSTREGISTERS@oservice_65 On TCUSREGREPCONLINKS.CustRegID =
                                 TCUSTREGISTERS.ObjID
  Left Outer Join ft.TSEVPLOYSETREPCONLINKS@oservice_65 On trepcontracts.ObjID =
                                               TSEVPLOYSETREPCONLINKS.RepContractID
  Left Outer Join ft.TSERVERPLOYSETS@oservice_65 On TSEVPLOYSETREPCONLINKS.ServerPloySetID =
                                        TSERVERPLOYSETS.ObjID
 where 1 = 1
 /*  And "TREPCONTRACTS".BrandID = '{DBE00437-FE9F-426C-B090-AD1F4EC317D3}'
   And "TREPCONTRACTS".ProductLineID =
       '{5645FF9E-8EB7-4C5F-8B06-EAA4D060BB81}'*/
   And "TREPCONTRACTS".createtime >= TO_DATE('2013-1-1', 'yyyy:MM:DD')
  -- And "TREPCONTRACTS".createtime < TO_DATE('2014-4-11', 'yyyy:MM:DD');


create or replace view repairclaimbillquery as
select v1."索赔单编号",v1."单据状态",v1."市场部",v1."申请单编号",v1."索赔单类型",v1."故障原因编码",v1."客户意见",v1."是否已回访",v1."回访意见",v1."服务科审核意见",v1."材料费",v1."配件管理费",v1."工时费",v1."索赔类型",v1."维修索赔备注信息",v1."创建日期",v1."创建人编号",v1."分公司审核时间",v1."故障原因名称",v1."星级系数",v1."是否需要向事业部索赔",v1."客户姓名",v1."客户电话",v1."客户手机",v1."祸首件图号",v1."祸首件名称",v1."供应商确认状态",v1."供应商审核意见",v1."外出车费单价",v1."外出路途补助单价",v1."外出公里数",v1."高速公路拖车费",v1."外出人数",v1."外出天数",v1."外出差旅费用合计",v1."外出救援地",v1."外出索赔备注信息",v1."维修属性",v1."出厂编号",v1."品牌名称",v1."产品线名称",v1."报修时间",v1."运行里程",v1."故障现象及原因描述",v1."维修附加属性",v1."福康工时单价",v1."祸首件生产厂家",v1."祸首件标记",v1."祸首件总成生产厂家",v1."祸首件总成标记",v1."车型",v1."发动机型号",v1."发动机编号",v1."VIN码",v1."祸首件总成名称",v1."祸首件总成图号",v1."祸首件流水号",v1."购买日期",v1."首次故障里程",v1."生产日期",v1."服务站编号",v1."服务站名称",v1."索赔单位事业部编号",v1."工时单价",v1."索赔单位事业部名称",v1."客户地址",v1."责任单位供应商编号",v1."责任单位供应商名称",
       nvl(工时费,0) + nvl(材料费,0) + nvl(外出差旅费用合计,0) as 维修索赔费用合计
  from (select --tbyservicecards.objid,
               tbyservicecards.code 索赔单编号,
               tbyservicecards.ISVALID 单据状态,
               tmarkets.name as 市场部,
               (select TAPPLYREPORTS.Code
                  from TAPPLYREPORTSoservice_65
                 inner join TAPPLYREPORTLINKSoservice_65
                    on TAPPLYREPORTLINKS.ApplyReportID = TAPPLYREPORTS.ObjID
                 where tRepContracts.ObjID = TAPPLYREPORTLINKS.RepContractID) as 申请单编号,
               decode(tbyservicecards.SERVICECARDTYPE,1,'维修类',2,'强保类') as  索赔单类型,
               tbyservicecards.FAULTREASONCODE 故障原因编码,
               tbyservicecards.FAULTDES 客户意见,
               decode(tbyservicecards.ISFEEDBACK,1,'否',2,'是')as 是否已回访,
               tbyservicecards.FEEDBACKIDEA 回访意见,
               tbyservicecards.EXAMIDEA 服务科审核意见,
               tbyservicecards.MATFEE 材料费,
               case tbyservicecards.SERVICECARDTYPE
                         when 1 then
                          ServiceMagFee
                         When 2 then
                          0
                       end as  配件管理费,
               case tbyservicecards.SERVICECARDTYPE
                         when 1 then
                          tbyservicecards.spworkfee
                         When 2 then
                          tbyservicecards.workfee
                       end as  工时费,
               decode(tbyservicecards.SPTYPE,0,'配件索赔',1,'装车件索赔',2,'非索赔') as 索赔类型,
               tbyservicecards.MEMO 维修索赔备注信息,
               tbyservicecards.CREATETIME 创建日期,
               tbyservicecards.CREATORCODE 创建人编号,
               tbyservicecards.ConfirmTime as 分公司审核时间,
              -- tbyservicecards.CHECKMANCODE,
               tbyservicecards.FAULTREASONNAME 故障原因名称,
               tbyservicecards.LEVELS_QUOTIETY 星级系数,
               decode(tbyservicecards.ISDUTYSP ,0,'否',1,'是' )as 是否需要向事业部索赔,
              -- tbyservicecards.TOTALFEE as SPTOTALFEE,
               tbyservicecards.CUSTNAME 客户姓名,
               tbyservicecards.CUSTPHONE 客户电话,
               tbyservicecards.CUSTMOBILE 客户手机,
               tbyservicecards.CAUSEMCODE 祸首件图号,
               tbyservicecards.CAUSEMNAME 祸首件名称,
            --   tbyservicecards.ISREPEATREP,
               decode(tbyservicecards.SUPPLIERSTATE,0,'仍未确认',1,'确认已通过',2,'确认未通过') as 供应商确认状态,
               tbyservicecards.CONFIRMIDEA 供应商审核意见,
              -- FT.TOUTSETTLES.ISPRIVATECAR,
               tOutSettles.OUTFARE 外出车费单价,
               tOutSettles.OUTSUBSIDY 外出路途补助单价,
               tOutSettles.OUTKMS 外出公里数,
               tOutSettles.TICKETFEE 高速公路拖车费,
               tOutSettles.SERVICEMENS 外出人数,
               tOutSettles.SERVICEDAYS 外出天数,
               nvl(tOutSettles.TOTALFEE, 0) as 外出差旅费用合计,
               tOutSettles.OUTPLACE 外出救援地,
               tOutSettles.Memo as  外出索赔备注信息,
               tRepContracts.REPAIRPROP 维修属性,
               tRepContracts.SERIALCODE 出厂编号,
               tBrands.NAME AS 品牌名称,
               tProDuctLines.NAME AS 产品线名称,
             --  TREPCONTRACTS.OBJID AS REPID,
               tRepContracts.RepBeginTime 报修时间,
               tRepContracts.runrange 运行里程,
               tRepContracts.FaultInfo 故障现象及原因描述,
               decode(tRepContracts.ADDITIONAL,1,'驾送维修',2,'经销商库存车维修',3,'经销商接车维修') AS 维修附加属性,
               decode(tByServiceCards.iskms, 1, tRepContracts.WORKFEESTANDKMS, 0) as 福康工时单价,
               tbyservicecards.C_P_Manufacturers as  祸首件生产厂家,
               tbyservicecards.C_Markings as  祸首件标记,
               tbyservicecards.C_P_A_Manufacturers as  祸首件总成生产厂家,
               tbyservicecards.C_A_Markings as  祸首件总成标记,
               TPURCHASEINFOS.PRODUCT_TYPE_NAME 车型,
               TPURCHASEINFOS.ENGINE_MODEL 发动机型号,
               TPURCHASEINFOS.ENGINE_CODE 发动机编号,
               TPURCHASEINFOS.VINCODE vin码,
               tbyservicecards.C_P_A_NAME AS 祸首件总成名称 ,
               tbyservicecards.C_P_A_CODE AS 祸首件总成图号 ,
               tbyservicecards.C_P_NUMBER AS 祸首件流水号,
               TPuRCHASEINFOS.Sales_Date 购买日期,
               TPuRCHASEINFOS.mile 首次故障里程,
               TPURCHASEINFOS.Leave_Factory_Date 生产日期,
               tstations.code as  服务站编号,
               tstations.name as  服务站名称,
               D1.Code as "索赔单位事业部编号",
               tRepContracts.WORKFEESTAND AS 工时单价,
               D1.Name as "索赔单位事业部名称",
               (select TPURCHASELINKERS.address
                  from ft.TPURCHASELINKERSoservice_65
                 where TPURCHASEINFOS.ObjID = TPURCHASELINKERS.parentid) 客户地址,
               (select D2.Code
                  from ft.TDUTYUNITSoservice_65 D2
                 where D2.ObjID = tbyservicecards.dutyunitid) as "责任单位供应商编号",
               (select D2.Name
                  from ft.TDUTYUNITSoservice_65 D2
                 where D2.ObjID = tbyservicecards.dutyunitid) as "责任单位供应商名称"
          from ft.tByServiceCardsoservice_65
         inner join ft.tstationsoservice_65
            on tbyservicecards.entercode = tstations.objid
         inner join ft.tstationlistsoservice_65
            on tstations.objid = tstationlists.stationid
           and tbyservicecards.brandid = tstationlists.brandid
         Inner join ft.TMARKETSoservice_65
            ON tstationlists.MARKETID = TMARKETS.OBJID
         inner join ft.tBYSerCardRepConLinksoservice_65
            on tByServiceCards.objid = tBYSerCardRepConLinks.BYServiceCardID
         inner join ft.tRepContractsoservice_65
            on tBYSerCardRepConLinks.RepContractID = tRepContracts.ObjID
          left join ft.tOutSettlesoservice_65
            on tbyservicecards.objid = tOutSettles.byserviceid
           and tOutSettles.isvalid > 0
         inner join ft.tBrandsoservice_65
            on tBYServiceCards.BrandID = tBrands.ObjID
         inner join ft.tProductLinesoservice_65
            on tBYServiceCards.ProductLineID = tProductLines.ObjID
         inner join ft.TPURCHASEINFOSoservice_65
            on tRepContracts.SerialCode = TPURCHASEINFOS.LEAVE_FACTORY_CODE
         inner join ft.tDutyUnitsoservice_65 d1
            on tbyservicecards.SPunitID = d1.objid
         where 1 = 1) v1
 where  v1.创建日期>=to_date('2013-1-1','yyyy-mm-dd');



create or replace view qtsdataquery as
select Cscode,Zcbmcode,Vincode,Assembly_date,Bind_date,Factory,Mapid,Patch,Produceline,Mapname
     from qts.v_qtsinfo @orcl_qtsinfo;


create or replace view qtsdataqueryfordgn as
select a.Cscode,
         a.Zcbmcode,
         a.Vincode,
         b.Assembly_date,
         a.Bind_date,
         b.Factory,
         b.Mapid,
         b.Patch,
         b.Produceline,
         b.Mapname
    from qts.o_vinbind @orcl_16 a
    left join qts.o_tmepassembly_cs @orcl_16 b on a.cscode = b.cscode;

create or replace view qtsdataqueryforsp as
select a.Cscode,
         a.Zcbmcode,
         a.Vincode,
         b.Assembly_date,
         a.Bind_date,
         b.Factory,
         b.Mapid,
         b.Patch,
         b.Produceline,
         b.Mapname,
         b.submittime
    from qts.o_vinbind @qtsorcl_10 a
    left join qts.o_tmepassembly_cs @qtsorcl_10 b on a.cscode = b.cscode;

create or replace view wmscongelationstockview as
select d.Id                     as WmsStockId, --  WMS冻结库存Id
       d.BranchId               as BranchId, --  营销分公司Id
       d.BranchCode             as BranchCode, --  营销分公司编号
       d.BranchName             as BranchName, --  营销分公司名称
       d.StorageCenter          as StorageCenter, --  储运中心
       d.PartsSalesCategoryId   as PartsSalesCategoryId, --  配件销售类型Id
       d.PartsSalesCategoryCode as PartsSalesCategoryCode, --  配件销售类型编号
       d.PartsSalesCategoryName as PartsSalesCategoryName, --  配件销售类型名称
       d.SparePartId            as SparePartId, --  配件Id
       d.SparePartCode          as SparePartCode, --  配件编号
       d.SparePartName          as SparePartName, --  配件名称
       d.CongelationStockQty    as CongelationStockQty, --  冻结库存
       d.DisabledStock          as DisabledStock, --  不可用库存
       a.Id                     as WarehouseId, --  仓库Id
       a.StorageCompanyId       as StorageCompanyId --  仓储企业Id
  from Warehouse a
 inner join SalesUnitAffiWarehouse b
    on a.Id = b.WarehouseId
 inner join SalesUnit c
    on b.SalesUnitId = c.Id
 inner join WmsCongelationStock d
    on c.PartsSalesCategoryId = d.PartsSalesCategoryId
 where d.StorageCenter = a.storagecenter
   and d.Branchid = a.branchid
   and a.WmsInterface = 1;



create or replace view qtsdataqueryforsp as
select a.Cscode,
         a.Zcbmcode,
         a.Vincode,
         b.Assembly_date,
         a.Bind_date,
         b.Factory,
         b.Mapid,
         b.Patch,
         b.Produceline,
         b.Mapname,
         b.submittime
    from qts.o_vinbind @qtsorcl_10 a
    left join qts.o_tmepassembly_cs @qtsorcl_10 b on a.cscode = b.cscode;





create or replace view qtsdataquery as
select Cscode,Zcbmcode,Vincode,Assembly_date,Bind_date,Factory,Mapid,Patch,Produceline,Mapname
     from qts.v_qtsinfo @orcl_qtsinfo;


create or replace view qtsdataqueryfordgn as
select a.Cscode,
         a.Zcbmcode,
         a.Vincode,
         b.Assembly_date,
         a.Bind_date,
         b.Factory,
         b.Mapid,
         b.Patch,
         b.Produceline,
         b.Mapname
    from qts.o_vinbind @orcl_16 a
    left join qts.o_tmepassembly_cs @orcl_16 b on a.cscode = b.cscode;

create or replace view qtsdataqueryforsp as
select a.Cscode,
         a.Zcbmcode,
         a.Vincode,
         b.Assembly_date,
         a.Bind_date,
         b.Factory,
         b.Mapid,
         b.Patch,
         b.Produceline,
         b.Mapname,
         b.submittime
    from qts.o_vinbind @qtsorcl_10 a
    left join qts.o_tmepassembly_cs @qtsorcl_10 b on a.cscode = b.cscode;

create or replace view wmscongelationstockview as
select d.Id                     as WmsStockId, --  WMS冻结库存Id
       d.BranchId               as BranchId, --  营销分公司Id
       d.BranchCode             as BranchCode, --  营销分公司编号
       d.BranchName             as BranchName, --  营销分公司名称
       d.StorageCenter          as StorageCenter, --  储运中心
       d.PartsSalesCategoryId   as PartsSalesCategoryId, --  配件销售类型Id
       d.PartsSalesCategoryCode as PartsSalesCategoryCode, --  配件销售类型编号
       d.PartsSalesCategoryName as PartsSalesCategoryName, --  配件销售类型名称
       d.SparePartId            as SparePartId, --  配件Id
       d.SparePartCode          as SparePartCode, --  配件编号
       d.SparePartName          as SparePartName, --  配件名称
       d.CongelationStockQty    as CongelationStockQty, --  冻结库存
       d.DisabledStock          as DisabledStock, --  不可用库存
       a.Id                     as WarehouseId, --  仓库Id
       a.StorageCompanyId       as StorageCompanyId --  仓储企业Id
  from Warehouse a
 inner join SalesUnitAffiWarehouse b
    on a.Id = b.WarehouseId
 inner join SalesUnit c
    on b.SalesUnitId = c.Id
 inner join WmsCongelationStock d
    on c.PartsSalesCategoryId = d.PartsSalesCategoryId
 where d.StorageCenter = a.storagecenter
   and d.Branchid = a.branchid
   and a.WmsInterface = 1;



create or replace view qtsdataqueryforsp as
select a.Cscode,
         a.Zcbmcode,
         a.Vincode,
         b.Assembly_date,
         a.Bind_date,
         b.Factory,
         b.Mapid,
         b.Patch,
         b.Produceline,
         b.Mapname,
         b.submittime
    from qts.o_vinbind @qtsorcl_10 a
    left join qts.o_tmepassembly_cs @qtsorcl_10 b on a.cscode = b.cscode;




--SAP发票信息视图
create or replace view SAPINVOICEINFO 
AS
SELECT  a.id,
        BUKRS         as CompanyCode,
        2             as BusinessType,
        code,
        InvoiceNumber,
        InvoiceAmount,
        a.invoicetax,
        CreateTime,
        Status,
        Message
   from SAP_YX_SALESBILLINFO a
 union
 SELECT 
        b.id,
        BUKRS         as CompanyCode,
        1             as BusinessType,
        code,
        InvoiceNumber,
        InvoiceAmount,
        b.invoicetax,
        CreateTime,
        Status,
        Message
   from SAP_YX_INVOICEVERACCOUNTINFO b

create or replace view productcategoryview as
select distinct t.productcategorycode,nvl(t.productcategoryname,' ') productcategoryname from product t;