create or replace procedure ExecuteClaimSettleInstruction as
  /*定义接收参数*/
  GeneratedBillQuantity integer;
  ErrorMsg varchar2(200);
  /*定义游标*/
  cursor Instructions Is
    select Id,
           BranchId,
           SettlementStartTime,
           SettlementEndTime,
           CreatorId,
           CreatorName,
           nvl(PartsSalesCategoryId,0) PartsSalesCategoryId,
           nvl(ServiceProductLineId,0) ServiceProductLineId,
           ProductLineType,SettleMethods
      from SsClaimSettleInstruction
     where SettlementEndTime < trunc(sysdate,'DD')
       and Status = 1
     order by SettlementEndTime;
   Instruction Instructions%rowtype;
begin
  for Instruction in Instructions loop
    ErrorMsg:='';
    GeneratedBillQuantity:=0;
    begin
      GeneratedBillQuantity:=GenerateSsClaimSettlementBill(Instruction.branchid,null,Instruction.settlementstarttime,Instruction.settlementendtime,Instruction.creatorid,Instruction.creatorname,Instruction.PartsSalesCategoryId,Instruction.ServiceProductLineId,Instruction.ProductLineType,Instruction.SettleMethods);
      Update SsClaimSettleInstruction set SsClaimSettleInstruction.Status = 2, SsClaimSettleInstruction.ExecutionTime = sysdate
       where Id = Instruction.Id;
    exception
      when others then
        rollback;
        ErrorMsg:=Sqlerrm;
        Update SsClaimSettleInstruction set SsClaimSettleInstruction.Status = 3, SsClaimSettleInstruction.ExecutionResult = ErrorMsg, SsClaimSettleInstruction.ExecutionTime = sysdate
         where Id = Instruction.Id;
        commit;
        exit;
    end;
    commit;
  end loop;
end;

create or replace procedure BatchPlannedPriceApp
as
  cursor PlannedPriceApps is
    select Id from plannedpriceapp where Status = 2 and PlannedExecutionTime <= sysdate order by PlannedExecutionTime asc;
  PlannedpPriceAppRow PlannedPriceApps%rowtype;

begin
  for PlannedpPriceAppRow in PlannedPriceApps loop
    ExecutePlannedPriceApp(PlannedpPriceAppRow.id);
  end loop;
  commit;
end;

CREATE OR REPLACE PROCEDURE ExecutePlannedPriceApp(
    priceAppId IN NUMBER)
AS
  /*变更前价格合计*/
  sumMoney_Before NUMBER(19, 4);
  /*变更后价格合计*/
  sumMoney_After NUMBER(19, 4);
  /*变更后价格差异合计*/
  sumMoney_Diff NUMBER(19, 4);
  /*仓库成本变更单自增长Id*/
  n INTEGER;
  /*计划价申请单*/
  FOwnerCompanyType INTEGER;
  FPlannedPriceApp PlannedPriceApp%rowtype;
  /*自动获取单据编码相关*/
  FDateFormat   DATE;
  FDateStr      VARCHAR2(8);
  FSerialLength INTEGER;
  FNewCode      VARCHAR2(50);
  FComplateId   INTEGER;
  CURSOR warehouses
  IS
    SELECT w.Id,
      w.Code,
      w.Name,
      w.Type
    FROM Warehouse w
    INNER JOIN salesunitaffiwarehouse sw
    ON w.Id = sw.warehouseid
    INNER JOIN salesunit s
    ON sw.salesunitid         = s.id
    WHERE StorageCompanyId    = FPlannedPriceApp.OwnerCompanyId
    AND s.partssalescategoryid=FPlannedPriceApp.Partssalescategoryid
    AND w.Status              = 1
    AND w.Type               IN(1,2);
  warehouse warehouses%rowtype;
BEGIN
  /*编码规则变更，需同步调整*/
  FDateFormat:=TRUNC(sysdate,'YEAR');
  --FDateFormat:=trunc(sysdate,'MONTH');
  --FDateFormat   := trunc(sysdate, 'DD');
  FDateStr      := TO_CHAR(sysdate, 'yyyy');
  FSerialLength := 4;
  FNewCode      := 'WCC{CORPCODE}' || FDateStr || '{SERIAL}';
  BEGIN
    SELECT *
    INTO FPlannedPriceApp
    FROM PlannedPriceApp
    WHERE Id   = priceAppId
    AND Status = 2;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    raise_application_error(-20001, '只允许执行处于“已审核”状态的计划价申请单。');
  WHEN OTHERS THEN
    raise;
  END;
  -- 获取隶属企业类型
  BEGIN
    SELECT Type
    INTO FOwnerCompanyType
    FROM Company
    WHERE Id = FPlannedPriceApp.OwnerCompanyId;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    raise_application_error(-20002, '计划价申请单隶属企业不存在。');
  WHEN OTHERS THEN
    raise;
  END;
  /*获取编号生成模板Id*/
  BEGIN
    SELECT Id
    INTO FComplateId
    FROM CodeTemplate
    WHERE name    = 'WarehouseCostChangeBill'
    AND IsActived = 1;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    raise_application_error(-20003, '未找到名称为"WarehouseCostChangeBill"的有效编码规则。');
  WHEN OTHERS THEN
    raise;
  END;
  /*更新计划价申请单，清单赋值变更前计划价*/
  UPDATE PlannedPriceAppDetail detail
  SET
    (
      PriceBeforeChange,
      Quantity,
      AmountDifference
    )
    =
    (SELECT NVL(PlannedPrice,0),
      0,
      0
    FROM PartsPlannedPrice price
    WHERE OwnerCompanyId          = FPlannedPriceApp.OwnerCompanyId
    AND price.partssalescategoryid=FPlannedPriceApp.partssalescategoryid
    AND price.SparePartId         = detail.SparePartId
    )
  WHERE detail.PlannedPriceAppId = FPlannedPriceApp.Id
  AND EXISTS
    (SELECT 1
    FROM PartsPlannedPrice price
    WHERE price.OwnerCompanyId    = FPlannedPriceApp.OwnerCompanyId
    AND price.partssalescategoryid=FPlannedPriceApp.partssalescategoryid
    AND price.SparePartId         = detail.SparePartId
    );
  UPDATE PlannedPriceAppDetail detail
  SET PriceBeforeChange          =0,
    Quantity                     =0,
    AmountDifference             =0
  WHERE detail.PlannedPriceAppId = FPlannedPriceApp.Id
  AND NOT EXISTS
    (SELECT 1
    FROM PartsPlannedPrice price
    WHERE price.OwnerCompanyId    = FPlannedPriceApp.OwnerCompanyId
    AND price.partssalescategoryid=FPlannedPriceApp.partssalescategoryid
    AND price.SparePartId         = detail.SparePartId
    );
  /*更新计划价申请单，清单赋值数量、差异金额(数量包括在库中的数量和在途的数量，在途数量从企业调拨在途配件成本中获取，在库中的数量从配件库存中获取）*/
  UPDATE PlannedPriceAppDetail detail
  SET
    (
      quantity,
      Amountdifference
    )
    =
    (SELECT NVL(cost.Quantity, 0),
      (detail.RequestedPrice - NVL(detail.PriceBeforeChange, 0)) * cost.Quantity
    FROM
      (SELECT tmp.sparepartid,
        SUM(tmp.tempquantity) AS quantity
      FROM
        (SELECT a.partid     AS sparepartId,
          NVL(a.quantity, 0) AS tempquantity
        FROM partsstock a
        INNER JOIN SalesUnitAffiWarehouse
        ON A.warehouseid = SalesUnitAffiWarehouse.Warehouseid
        INNER JOIN Salesunit
        ON SalesUnitAffiWarehouse.Salesunitid = Salesunit.ID
        WHERE A.StorageCompanyType            = 1
        AND Salesunit.Partssalescategoryid    = FPlannedPriceApp.Partssalescategoryid
        UNION ALL
        SELECT pobd.SparePartId,
          pobd.OutboundAmount - NVL(picbd.InspectedQuantity, 0)
        FROM PartsTransferOrder pto
        INNER JOIN PartsOutboundBill pob
        ON pob.OriginalRequirementBillCode = pto.Code
        AND pob.OutboundType               = 4
        INNER JOIN PartsOutboundBillDetail pobd
        ON pob.Id = pobd.PartsOutboundBillId
        LEFT JOIN PartsInboundCheckBill picb
        ON picb.OriginalRequirementBillCode = pto.code
        AND picb.InboundType                = 3
        LEFT JOIN PartsInboundCheckBillDetail picbd
        ON picb.Id                    = picbd.PartsInboundCheckBillId
        AND pobd.sparepartid          = picbd.sparepartid
        WHERE pob.partssalescategoryid=FPlannedPriceApp.Partssalescategoryid
        ) tmp
      GROUP BY tmp.sparepartid
      ) cost
    WHERE cost.sparepartid = detail.sparepartid
    )
  WHERE detail.PlannedPriceAppId = priceAppId;
  /*更新配件计划价，如果不存在则新增*/
  merge INTO PartsPlannedPrice price USING
  (SELECT *
  FROM PlannedPriceAppDetail
  WHERE PlannedPriceAppId           = FPlannedPriceApp.Id
  ) detail ON (price.OwnerCompanyId = FPlannedPriceApp.OwnerCompanyId AND price.SparePartId = detail.SparePartId AND price.partssalescategoryid=FPlannedPriceApp.partssalescategoryid)
WHEN matched THEN
  UPDATE
  SET price.plannedprice = detail.RequestedPrice,
    price.modifierid     =FPlannedPriceApp.Modifierid,
    price.modifiername   =FPlannedPriceApp.Modifiername,
    price.modifytime     =FPlannedPriceApp.Modifytime WHEN NOT matched THEN
  INSERT
    (
      Id,
      OwnerCompanyId,
      OwnerCompanyType,
      SparePartId,
      PlannedPrice,
      CreatorId,
      CreatorName,
      CreateTime,
      PartsSalesCategoryId,
      PartsSalesCategoryName
    )
    VALUES
    (
      S_PartsPlannedPrice.Nextval,
      FPlannedPriceApp.OwnerCompanyId,
      FOwnerCompanyType,
      detail.SparePartId,
      detail.RequestedPrice,
      FPlannedPriceApp.CreatorId,
      FPlannedPriceApp.CreatorName,
      sysdate,
      FPlannedPriceApp.PartsSalesCategoryId,
      FPlannedPriceApp.PartsSalesCategoryName
    );
  /*更新计划价申请单*/
  UPDATE PlannedPriceApp
  SET ActualExecutionTime                                     = SYSDATE,
    Status                                                    = 3,
    (AmountBeforeChange, AmountAfterChange, AmountDifference) =
    (SELECT SUM(PriceBeforeChange * Quantity),
      SUM(RequestedPrice          * Quantity),
      SUM(AmountDifference)
    FROM plannedPriceAppDetail
    WHERE plannedPriceAppDetail.PlannedPriceAppId = FPlannedPriceApp.Id
    )
  WHERE ID = FPlannedPriceApp.ID;
  /*针对每一个仓库，生成仓库成本变更单和清单*/
  FOR warehouse IN warehouses
  LOOP
    /*获取自增长Id，插入库存成本变更单*/
    SELECT S_WAREHOUSECOSTCHANGEBILL.NEXTVAL
    INTO n
    FROM dual;
    /*插入库存成本变更清单*/
    INSERT
    INTO WarehouseCostChangeDetail
      (
        Id,
        WarehouseCostChangeBillId,
        WarehouseId,
        SparePartId,
        SparePartCode,
        SparePartName,
        Pricebeforechange,
        Quantity,
        PriceAfterChange,
        AmountDifference
      )
    SELECT S_WAREHOUSECOSTCHANGEDETAIL.NEXTVAL,
      n,
      warehouse.Id,
      SparePartId,
      SparePartCode,
      SparePartName,
      PriceBeforeChange,
      stock.TotalQuantity,
      RequestedPrice,
      (RequestedPrice - NVL(PriceBeforeChange, 0)) * stock.TotalQuantity
    FROM
      (SELECT t.PartId,
        SUM(Quantity) AS TotalQuantity
      FROM
        (SELECT PartId,
          Quantity
        FROM PartsStock
        WHERE StorageCompanyType = 1
        AND WarehouseId          = warehouse.Id
        AND EXISTS
          (SELECT 1
          FROM SalesUnitAffiWarehouse
          WHERE SalesUnitAffiWarehouse.Warehouseid = warehouse.Id
          AND SalesUnitAffiWarehouse.Salesunitid  IN
            (SELECT id
            FROM Salesunit
            WHERE Salesunit.Partssalescategoryid = FPlannedPriceApp.Partssalescategoryid
            )
          )
        AND EXISTS
          (SELECT 1
          FROM PlannedPriceAppDetail
          WHERE PlannedPriceAppDetail.PlannedPriceAppId = FPlannedPriceApp.Id
          AND PlannedPriceAppDetail.SparePartId         = PartsStock.PartId
          )
        UNION ALL
        SELECT pobd.SparePartId,
          (pobd.OutboundAmount - NVL(picbd.InspectedQuantity, 0))
        FROM PartsTransferOrder pto
        INNER JOIN PartsOutboundBill pob
        ON pob.OriginalRequirementBillCode = pto.Code
        AND pob.OutboundType               = 4
        INNER JOIN PartsOutboundBillDetail pobd
        ON pob.Id = pobd.PartsOutboundBillId
        LEFT JOIN PartsInboundCheckBill picb
        ON picb.OriginalRequirementBillCode = pto.code
        AND picb.InboundType                = 3
        LEFT JOIN PartsInboundCheckBillDetail picbd
        ON picb.Id                  = picbd.PartsInboundCheckBillId
        AND pobd.sparepartid        = picbd.sparepartid
        WHERE pto.DestWarehouseId   = warehouse.Id
        AND pob.partssalescategoryid=FPlannedPriceApp.Partssalescategoryid
        AND EXISTS
          (SELECT 1
          FROM PlannedPriceAppDetail
          WHERE PlannedPriceAppDetail.PlannedPriceAppId = FPlannedPriceApp.Id
          AND PlannedPriceAppDetail.SparePartId         = pobd.SparePartId
          )
        ) t
      GROUP BY t.PartId
      ) stock
    INNER JOIN PlannedPriceAppDetail
    ON PlannedPriceAppDetail.SparePartId          = stock.PartId
    WHERE PlannedPriceAppDetail.PlannedPriceAppId = FPlannedPriceApp.Id;
    /*不存在变更清单，仍然需要生成主单*/
    /*计算该仓库下的合计*/
    SELECT NVL(SUM(PriceBeforechange * Quantity),0),
      NVL(SUM(PriceAfterChange       * Quantity),0),
      NVL(SUM(Amountdifference),0)
    INTO sumMoney_Before,
      sumMoney_After,
      sumMoney_Diff
    FROM WarehouseCostChangeDetail
    WHERE WarehouseCostChangeBillId = n;
    /*插入主单*/
    INSERT
    INTO WarehouseCostChangeBill
      (
        Id,
        Code,
        PlannedPriceAppId,
        OwnerCompanyId,
        OwnerCompanyCode,
        OwnerCompanyName,
        WarehouseId,
        WarehouseCode,
        WarehouseName,
        AmountBeforeChange,
        AmountAfterChange,
        AmountDifference,
        RecordStatus,
        CreatorId,
        CreatorName,
        CreateTime
      )
      VALUES
      (
        n,
        regexp_replace( regexp_replace(FNewCode, '{CORPCODE}', FPlannedPriceApp.OwnerCompanyCode), '{SERIAL}', lpad(TO_CHAR(GetSerial(FComplateId, FDateFormat, FPlannedPriceApp.OwnerCompanyCode)),FSerialLength,'0')),
        FPlannedPriceApp.Id,
        FPlannedPriceApp.OwnerCompanyId,
        FPlannedPriceApp.OwnerCompanyCode,
        FPlannedPriceApp.OwnerCompanyName,
        warehouse.Id,
        warehouse.Code,
        warehouse.Name,
        sumMoney_Before,
        sumMoney_After,
        sumMoney_Diff,
        2,
        FPlannedPriceApp.CreatorId,
        FPlannedPriceApp.CreatorName,
        sysdate
      );
  END LOOP;
END;



-- 整车经销商信用额度JOB
create or replace procedure InvalidDealerCreditLimitApp as
begin
  update VehicleCustomerAccount set CustomerCredenceAmount=0
   where exists (select * from VehicleDealerCreditLimitApp 
          where VehicleCustomerAccount.CustomerCompanyId =  VehicleDealerCreditLimitApp.CustomerCompanyId
            and VehicleCustomerAccount.VehicleFundsTypeId =  VehicleDealerCreditLimitApp.VehicleFundsTypeId
            and VehicleDealerCreditLimitApp.status=3
            and to_date(VehicleDealerCreditLimitApp.ExpireDate, 'yyyy-mm-dd') = to_date(sysdate, 'yyyy-mm-dd'));

  update VehicleDealerCreditLimitApp set status=4
   where VehicleDealerCreditLimitApp.status=3 
     and to_date(VehicleDealerCreditLimitApp.ExpireDate, 'yyyy-mm-dd') = to_date(sysdate, 'yyyy-mm-dd');
end;
/

-- 订货计划转成普通订单
create or replace procedure PrcOrderPlanToOrder 
as
  /*自动获取单据编码相关*/
  FDateFormat   date;
  FDateStr      varchar2(8);
  FSerialLength integer;
  FNewCode      varchar2(50);
  FComplateId   integer;
  FSONSerialLength integer;
  FExpectedShippingDate date;
  /*定义信息字段*/
  FMonthOfOrder  integer;
  FYearOfOrder integer;
  FVehicleSalesTypeId integer;
begin

  /*编码规则变更，需同步调整*/
  --FDateFormat:=trunc(sysdate,'YEAR');
  --FDateFormat:=trunc(sysdate,'MONTH');
  FDateFormat   := trunc(sysdate, 'DD');
  FDateStr      := to_char(sysdate, 'yyyymmdd');
  FSerialLength := 4;
  FNewCode      := '{CORPCODE}OR' || FDateStr || '{SERIAL}';
  
  FSONSerialLength:=4;
  
  begin
    select MonthOfOrder, YearOfOrder into FMonthOfOrder, FYearOfOrder from VehicleOrderSchedule where trunc(OrderPlanToPODate, 'DD')=trunc(sysdate, 'DD');
  exception
    when NO_DATA_FOUND then
      return;
    when others then
      raise;
  end;
  
  /*获取编号生成模板Id*/
  begin
    select Id into FComplateId from codetemplate where name = 'VehicleOrder' and IsActived = 1;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20001,'未找到名称为"VehicleOrder"的有效编码规则。');
    when others then
      raise;
  end;
  
  /*获取整车销售类型*/
  begin
    select VehicleSalesTypeId into FVehicleSalesTypeId from VehicleSalesOrganization where Id=1; --默认销售组织
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20002,'未设置默认销售组织');
    when others then
      raise;
  end;
  
  FExpectedShippingDate:=add_months(trunc(sysdate,'MONTH'),1);
  
  declare 
    VehicleOrderId integer;
    VehicleOrderCode varchar2(50);
    NoPriceProductCount integer;
    VehicleCustomerAccountId integer;
    FIndex integer;
    FTotalAmount number(19,4);
    cursor vehicleOrderPlans is
      select Id,Code,VehicleSalesOrgId,VehicleSalesOrgCode,VehicleSalesOrgName,DealerId,DealerCode,DealerName from VehicleOrderPlan
       where YearOfPlan =  FYearOfOrder and MonthOfPlan = FMonthOfOrder and Status = 1;
    vehicleOrderPlan vehicleOrderPlans%rowtype;
  begin
    for vehicleOrderPlan in vehicleOrderPlans loop
      begin  
        NoPriceProductCount:=0;
        select count(*) into NoPriceProductCount from VehicleOrderPlanDetail 
         where VehicleOrderPlanId=vehicleOrderPlan.Id
           and not exists (select * from VehicleWholesalePrice
                            where VehicleWholesalePrice.Status=1
                              and VehicleWholesalePrice.VehicleSalesTypeId=FVehicleSalesTypeId
                              and VehicleWholesalePrice.ProductId=VehicleOrderPlanDetail.ProductId);
        if NoPriceProductCount>0 then
          raise_application_error(-20003,'编号为“' || vehicleOrderPlan.Code || '”的订单计划中存在产品没有对应的整车批发价格');
        end if;
        
        --获取新增单据Id
        select S_VehicleOrder.NEXTVAL into VehicleOrderId from dual;
        --获取新单据号
        VehicleOrderCode:=regexp_replace(regexp_replace(FNewCode, '{CORPCODE}', vehicleOrderPlan.DealerCode),
          '{SERIAL}',lpad(to_char(GetSerial(FComplateId, FDateFormat, vehicleOrderPlan.DealerCode)),FSerialLength,'0'));
        --获取整车客户账户ID
        begin
          select Id into VehicleCustomerAccountId from VehicleCustomerAccount 
           where VehicleFundsTypeId=1 
             and CustomerCompanyId=vehicleOrderPlan.VehicleSalesOrgId 
             and VehicleAccountGroupId = 1 --默认CAM销售组织
             and Status=1;
        exception
          when NO_DATA_FOUND then
            VehicleCustomerAccountId:=null;
          when others then
            raise;
        end;
        --插入新订单单据
        Insert into VehicleOrder(Id,VehicleSalesOrgId,VehicleSalesOrgCode,VehicleSalesOrgName,
          DealerId,DealerCode,DealerName,Code,VehicleOrderPlanId,VehicleOrderPlanCode,OrderType,
          YearOfPlan,MonthOfPlan,TotalAmount,VehicleFundsTypeId,VehicleCustomerAccountId,Status,CreateTime)
        values(VehicleOrderId,vehicleOrderPlan.VehicleSalesOrgId,vehicleOrderPlan.VehicleSalesOrgCode,vehicleOrderPlan.VehicleSalesOrgName,
          vehicleOrderPlan.DealerId,vehicleOrderPlan.DealerCode,vehicleOrderPlan.DealerName,VehicleOrderCode,vehicleOrderPlan.Id,vehicleOrderPlan.Code,1,
          FYearOfOrder,FMonthOfOrder,0,1,VehicleCustomerAccountId,1,sysdate);
        
        FIndex:=1;
        FTotalAmount:=0;
        
        declare
          PlanQty integer;
          Price number(19,4);
          cursor OPD_Cursor is
            select ProductId,ProductCode,ProductName,ProductCategoryCode,ProductCategoryName,PlannedQuantity
              from VehicleOrderPlanDetail
             where VehicleOrderPlanId = vehicleOrderPlan.Id;
          detail OPD_Cursor%rowtype;
        begin
          --根据订货清单中的数据循环插入到订单清单中
          for detail in OPD_Cursor loop
            PlanQty:=detail.PlannedQuantity;
            --获取批发价格
            select VehicleWholesalePrice.Price into Price from VehicleWholesalePrice
             where VehicleWholesalePrice.Status=1 
               and VehicleWholesalePrice.VehicleSalesTypeId=VehicleSalesTypeId
               and VehicleWholesalePrice.ProductId=detail.ProductId
               and rownum=1;               
            while PlanQty>0 loop
              insert into VehicleOrderDetail (Id,VehicleOrderId, ProductId, ProductCode, ProductName, ProductCategoryCode, ProductCategoryName,
                SON, Status, HasChanged, Price, ExpectedShippingDate)
              values(S_VehicleOrderDetail.Nextval,VehicleOrderId,detail.ProductId,detail.ProductCode,detail.ProductName,detail.ProductCategoryCode,detail.ProductCategoryName,
                VehicleOrderCode||lpad(to_char(FIndex),FSerialLength,'0'),
                1, 0, Price, FExpectedShippingDate);
              PlanQty:=PlanQty-1;
              FIndex:=FIndex+1;
              FTotalAmount:=FTotalAmount+Price;
            end loop;  
            
            --更新订单总金额
            update VehicleOrder set TotalAmount=FTotalAmount where id=VehicleOrderId;
    
            --更新计划单状态为生效
            update VehicleOrderPlan set Status=3 where id=vehicleOrderPlan.Id;          
          end loop;
        end;
      exception
        when others then
          rollback;
          raise;
      end;
      commit;
    end loop;
  end;
end;
/


-- 执行整车零售价格变更申请单
create or replace procedure ExecuteVehRetailPriceChangeApp as
  RetailSuggested_id integer;
  cursor RetailPriceDetails is
    select ret.Branchid,
           ret.Branchcode,
           ret.Branchname,
           detail.Productid,
           detail.Productcode,
           detail.Productname,
           detail.priceafterchange as Price,
           ret.ApproverId,
           ret.ApproverName
      from VehRetailPriceChangeApp ret,VehRetailPriceChangeAppdetail detail
     where ret.id = detail.vehretailpricechangeappid
       and ret.status = 2
       and to_char(ret.ExecutionDate, 'yyyy-mm-dd') = to_char(sysdate, 'yyyy-mm-dd')
     order by ret.ExecutionDate;
  retailPriceDetailsRow retailPriceDetails%rowtype;
begin
  for retailPriceDetailsRow in RetailPriceDetails loop
    begin
      select id into RetailSuggested_id
        from vehicleRetailSuggestedPrice
       where ProductId = retailPriceDetailsRow.Productid;
           
      update vehicleRetailSuggestedPrice
         set Price = retailPriceDetailsRow.price, ExecutionTime = sysdate,
             ModifierId = retailPriceDetailsRow.ApproverId, ModifierName = retailPriceDetailsRow.ApproverName, ModifyTime = sysdate
       where id = RetailSuggested_id;
    exception
      when NO_DATA_FOUND then
        insert into VehicleRetailSuggestedPrice
          (Id, Branchid, Branchcode, Branchname, 
           Productid, Productcode, Productname,
           Price, Status, Executiontime,
           CreatorId, CreatorName, CreateTime)
        values
          (S_VehicleRetailSuggestedPrice.Nextval, retailPriceDetailsRow.Branchid, retailPriceDetailsRow.Branchcode, retailPriceDetailsRow.Branchname,
           retailPriceDetailsRow.Productid, retailPriceDetailsRow.Productcode, retailPriceDetailsRow.Productname,
           retailPriceDetailsRow.Price, 2, sysdate,
           retailPriceDetailsRow.ApproverId, retailPriceDetailsRow.ApproverName, sysdate);
      when others then
        raise;
    end;
  end loop;
  update VehRetailPriceChangeApp set status = 3
   where status = 2
     and to_char(ExecutionDate, 'yyyy-mm-dd') = to_char(sysdate, 'yyyy-mm-dd');
  commit;
end;
/


--执行整车批发价格变更申请单
create or replace procedure ExecuteVehPifaPriceChangeApp as
  WholesalePrice_ID integer;
  cursor PifaPriceDetails is
    select pifa.Branchid,
           pifa.Branchcode,
           pifa.Branchname,
           pifa.VehicleSalesTypeId,
           pifa.VehicleSalesTypeCode,
           pifa.VehicleSalesTypeName,
           detail.Productid,
           detail.Productcode,
           detail.Productname,
           detail.priceafterchange as Price,
           pifa.ApproverId,
           pifa.ApproverName
      from VehPifaPriceChangeApp pifa, Vehpifapricechangeappdetail detail
     where pifa.id = detail.vehretailpricechangeappid
       and pifa.status = 2
       and to_char(pifa.ExecutionDate, 'yyyy-mm-dd') = to_char(sysdate, 'yyyy-mm-dd')
     order by pifa.ExecutionDate;
  PifaPriceDetailsRow PifaPriceDetails%rowtype;
begin
  for PifaPriceDetailsRow in PifaPriceDetails loop
    begin
      select id into WholesalePrice_ID from VehicleWholesalePrice
       where ProductId = PifaPriceDetailsRow.Productid
         and VehicleSalesTypeId = PifaPriceDetailsRow.VehicleSalesTypeId;
         
      update VehicleWholesalePrice
         set Price = PifaPriceDetailsRow.price, ExecutionDate = sysdate,
             ModifierId = PifaPriceDetailsRow.ApproverId, ModifierName = PifaPriceDetailsRow.ApproverName, ModifyTime = sysdate
       where id = WholesalePrice_ID;  
    exception
      when NO_DATA_FOUND then
        insert into VehicleWholesalePrice
          (Id, Branchid, Branchcode, Branchname,
           VehicleSalesTypeId, VehicleSalesTypeCode, VehicleSalesTypeName,
           Productid, Productcode, Productname,
           Price, Status, ExecutionDate,
           CreatorId, CreatorName, CreateTime)
        values
          (S_VehicleRetailSuggestedPrice.Nextval, PifaPriceDetailsRow.Branchid, PifaPriceDetailsRow.Branchcode, PifaPriceDetailsRow.Branchname,
           PifaPriceDetailsRow.VehicleSalesTypeId, PifaPriceDetailsRow.VehicleSalesTypeCode, PifaPriceDetailsRow.VehicleSalesTypeName,
           PifaPriceDetailsRow.Productid, PifaPriceDetailsRow.Productcode, PifaPriceDetailsRow.Productname,
           PifaPriceDetailsRow.Price, 2, sysdate,
           PifaPriceDetailsRow.ApproverId, PifaPriceDetailsRow.ApproverName, sysdate);
      when others then
        raise;
    end;
  end loop;
  
  update VehPifaPriceChangeApp set status = 3
   where status = 2
     and to_char(ExecutionDate, 'yyyy-mm-dd') = to_char(sysdate, 'yyyy-mm-dd');
  commit;
end;
/

create or replace procedure EffectPartsHistoryStock as
 cursor tmpCursor is
  select id from FinancialSnapshotSet a
   where a.status = '1'
       and to_date(to_char(a.snapshottime, 'yyyy-mm-dd'),'yyyy-mm-dd') =
           to_date(to_char(sysdate-1, 'yyyy-mm-dd'),'yyyy-mm-dd');
  tmpCursorRow  tmpCursor%rowtype;
begin
  open tmpCursor;
  loop
    fetch tmpCursor into tmpCursorRow;
    exit when tmpCursor%notfound;

    insert into PARTSHISTORYSTOCK
     select S_PARTSHISTORYSTOCK.Nextval,
            d.warehouseid,
            d.storagecompanyid,
            d.storagecompanytype,
            d.branchid,
            d.warehouseareaid,
            d.warehouseareacategoryid,
            d.partid,
            d.quantity,
            e.plannedprice,
            (d.quantity * e.plannedprice) as PLANNEDPRICEAMOUNT,
            d.remark,
            a.snapshottime,
            a.creatorid,
            a.creatorname,
            sysdate,
            null,
            null,
            null,f.id
       from FinancialSnapshotSet a --财务快照
       inner  join partssalescategory f  on a.branchid=f.branchid
       inner join salesunit b --销售组织
         on a.partssalescategoryid = f.id
       inner join SalesUnitAffiWarehouse c --销售组织仓库关系
         on b.id = c.salesunitid
       inner join PartsStock d --配件库存
         on c.warehouseid = d.warehouseid
       inner join PartsPlannedPrice e --配件销售价
         on d.partid = e.Sparepartid
        and e.partssalescategoryid = f.id
      where a.Id = tmpCursorRow.Id;
    update FinancialSnapshotSet set status = 2 where id = tmpCursorRow.Id;
 end loop;
 close tmpCursor;
end;
/



create or replace procedure AutoApproveClaimBill as

  IsAssociatedApplication number(9);
  IsNoMaterialFee number(9);
  ServiceFee float;
  RepeatedRepairTime number(9);
  ClaimTimeOutLong number(9);
  Capacity number(9);
  WorkingHours number(9);
  Mileage number(9);
  hasAutoApproveStrategy number(9);
  IsRepairOverloadProtection number(9);
  TermCapacity number(9);
  TermWorkingHours number(9);
  TermupMileage number(9);
  TermlowMileage number(9);
  TermupDay  number(9);
  TermlowDay  number(9);
  partTermCapacity number(9);
  partTermWorkingHours number(9);
  partTermMileage number(9);
  partTermDay number(9);
  flagIsture boolean;
  flagPartIsTrue boolean;
  IsContainRepairTerm  number(9);
  IsContainPartTerm  number(9);
  IsContaimRemainTerm Number(9);
  hasPartTerm Number(9);
  hasRepairTerm Number(9);
  hasRemainTerm Number(9);
  hasPartTermevery Number(9);
  hasRepairTermevery Number(9);
  hasRemainTermevery Number(9);
  hasVehicleWarrantyCard number(9);
  hasVideoMobileNumber  number(9);
  RepeatedRepairTimeRC number(9);
  hasRepairFaultReason number(9);
  StrategyId number(9);
  hasDealer number(9);
  hasServiceProductLine number(9);
  hasRepairType number(9);
  hasSupplierId number(9);
  finishtime date;
  QBTIME number(9);
  begin


declare
  cursor AutoApprove is(
  select id,
         t.autoapprovestatus,
         partssalescategoryid,
         t.vehicleid,
         Capacity,
         WorkingHours,
         Mileage,
         floor(RepairRequestTime - SalesDate) timerange,
         floor(RepairRequestTime - OutOfFactoryDate - 90) timeoutrange,
         FaultyPartsWCId,
         TotalAmount,
         MaterialCost,
         ServiceTripClaimAppId,
         RepairClaimApplicationId,
         vin,
         MalfunctionId,
         dealerid,
         ServiceProductLineId,
         repairtype,
         FaultyPartsSupplierId,
         createtime,
         repairorderid,
         SupplierConfirmStatus,claimtype,VehicleMaintenancePoilcyId,sysdate as time1
    from repairclaimbill t
   where status = 2
     and autoapprovestatus = 0
  );
/*查询车辆信息（车辆信息.车辆Id=维修单.车辆Id）如果车辆信息.行驶里程>维修单.行驶里程且维修单.行驶里程不为空，
则自动审核意见追加“维修单提报行驶里程小于上次维修行驶里程，无法提报维修单”。如果车辆信息.方量>维修单.方量且维修单.方量不为空，
则自动审核意见追加“维修单提报方量小于上次维修方量，无法提报维修单”。如果车辆信息.工作小时>维修单.工作小时且维修单.工作小时不为空，
则自动审核意见追加“维修单提报工作小时小于上次维修工作小时，无法提报维修单”
自动审核状态=不通过*/
  begin
    for S_AutoApprove in AutoApprove loop
      begin
      select Capacity,WorkingHours,Mileage into Capacity,WorkingHours,Mileage from vehicleinformation where id =S_AutoApprove.vehicleid;
      exception
      when NO_DATA_FOUND then
      raise_application_error(-20001,'车辆信息不存在');
      end;
      --里程倒跑
      update repairclaimbill set autoapprovestatus=1 where id=S_AutoApprove.id;

      if (Mileage>S_AutoApprove.Mileage )and (S_AutoApprove.Mileage is not null) then
        begin
          update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'维修单提报行驶里程小于上次维修行驶里程 '||chr(10),t.autoapprovestatus=2
          ,t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'维修单提报行驶里程小于上次维修行驶里程 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10) where id=S_AutoApprove.id;

       end;
      end if;
      --方量倒跑
       if (Capacity>S_AutoApprove.Capacity )and (S_AutoApprove.Capacity is not null) then
        begin
          update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'维修单提报方量小于上次维修方量 '||chr(10),t.autoapprovestatus=2
          ,t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'维修单提报方量小于上次维修方量 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
       end;
      end if;
      --工作小时倒跑
       if (WorkingHours>S_AutoApprove.WorkingHours )and (S_AutoApprove.WorkingHours is not null) then
        begin
          update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'维修单提报方量小于上次工作小时 '||chr(10),t.autoapprovestatus=2
          ,t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'维修单提报方量小于上次工作小时 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10) where id=S_AutoApprove.id;
       end;
      end if;
     /* 2.假设返回N条保修卡信息，循环判断每张保修卡，只要有一张通过下述校验，则返回 空：
  1.）查询保修政策（单车保修卡.保修政策Id）如果保修条款为空，返回“没有对应的整车保修条款”
  2.）如果保修政策.含维修条款=true，
  查询整车保修条款（保修政策Id）  ，判断里程在闭区间[里程下限，里程上限]，报修时间-销售时间（天数，不含时分秒）在闭区间[天数下限，天数上限]、方量大于等于方量、工作小时大于等于工作小时则整车没有超保； 则自动审核意见追加"整车保修超保"
  或者
        如果保修政策.含维修条款=true，查询整车保修条款（保修政策Id）  ，判断里程在闭区间[里程下限，里程上限]，报修时间-出厂时间-90（天数，不含时分秒）在闭区间[天数下限，天数上限]、方量大于等于参数.方量、工作小时大于等于参数.工作小时则整车没有超保；则自动审核意见追加"整车保修超保"
自动审核状态=不通过*/
      flagPartIsTrue:=false;
      flagIsture:=false;
      hasPartTerm:=0;
      hasRepairTerm:=0;
      hasRemainTerm:=0;
      begin
             select count(*)
               into hasVehicleWarrantyCard
               from VehicleWarrantyCard v
              where v.vehicleid = S_AutoApprove.vehicleid
                and status=1;
          exception
              when no_data_found then
                update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'单车保修卡不存在 '||chr(10),t.autoapprovestatus=2,
                 t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'单车保修卡不存在 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
              end;

                         declare
              cursor VehicleWarrantyCard is(
              select * from VehicleWarrantyCard v where v.vehicleid=S_AutoApprove.vehicleid and status=1
              );
              begin
           for S_VehicleWarrantyCard in VehicleWarrantyCard loop
             begin
               select warrantypolicy.repairterminvolved,warrantypolicy.partswarrantyterminvolved,warrantypolicy.mainteterminvolved into IsContainRepairTerm,IsContainPartTerm,IsContaimRemainTerm  from warrantypolicy where id=S_VehicleWarrantyCard.Warrantypolicyid;

             select MaxMileage,
                    MinMileage,
                    DaysUpperLimit,
                    DaysLowerLimit,
                    Capacity,
                    WorkingHours,1,1
               into TermupMileage,
                    TermlowMileage,
                    TermupDay,
                    TermlowDay,
                    TermCapacity,
                    TermWorkingHours,hasRepairTerm,hasRepairTermevery
               from VehicleWarrantyTerm t
              where t.warrantypolicyid =
                    S_VehicleWarrantyCard.Warrantypolicyid;
                    exception
                      when no_data_found then begin
                        IsContainRepairTerm:=0;
                        hasRepairTermevery:=0;
                        end;
                        end;

                  if (IsContainRepairTerm=1)and (S_AutoApprove.claimtype=1 ) and(hasRepairTermevery=1) then begin

                    if (S_AutoApprove.timerange>=TermlowDay )and (S_AutoApprove.timerange<=TermupDay) then
                      flagIsture:=true;
                      end if;
                    if (S_AutoApprove.timeoutrange>=TermlowDay )and (S_AutoApprove.timeoutrange<=TermupDay) then
                      flagIsture:=true;
                      end if;
                    if (S_AutoApprove.Mileage>=TermlowMileage)and (S_AutoApprove.Mileage<=TermupMileage) then
                      flagIsture:=true;
                      end if;
                    if (TermCapacity>=S_AutoApprove.Capacity)  then
                      flagIsture:=true;
                      end if;
                    if (TermWorkingHours>=S_AutoApprove.WorkingHours)  then
                      flagIsture:=true;
                      end if;
                  end;
                   else
                   flagIsture:=true;
                  end if;


                  begin
                  select WarrantyPeriod,
                         WarrantyMileage,
                         Capacity,
                         WorkingHours,1,1
                    into partTermDay,
                         partTermMileage,
                         partTermCapacity,
                         partTermWorkingHours,hasPartTerm,hasPartTermevery
                    from PartsWarrantyTerm t
                   where t.warrantypolicyid =
                         S_VehicleWarrantyCard.Warrantypolicyid
                     and t.partswarrantycategoryid =
                         S_AutoApprove.FaultyPartsWCId and t.status=1;
                    exception
                      when no_data_found then
                        --update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'配件保修条款不存在 '||chr(10),t.autoapprovestatus=2 where id=S_AutoApprove.id;
                       hasPartTermevery:=0;
                       end;
                  if (IsContainPartTerm =1) and (hasPartTermevery=1)  then  begin
                    if ((partTermMileage>=S_AutoApprove.Mileage)or(S_AutoApprove.Mileage is null))and  ((partTermDay>=S_AutoApprove.timerange )or (S_AutoApprove.timerange is null) )and((partTermCapacity>=S_AutoApprove.Capacity)or(S_AutoApprove.Capacity is null)) and((partTermWorkingHours>=S_AutoApprove.WorkingHours)or(S_AutoApprove.WorkingHours is null)) then
                    flagPartIsTrue:=true;
                    end if ;
                  end;
                  else
                    flagPartIsTrue:=true;
                    end if;

                    if S_AutoApprove.claimtype=2 then begin
              begin
              select  MaxMileage,
                    MinMileage,
                    DaysUpperLimit,
                    DaysLowerLimit,
                    Capacity,
                    WorkingHours,1,1
               into TermupMileage,
                    TermlowMileage,
                    TermupDay,
                    TermlowDay,
                    TermCapacity,
                    TermWorkingHours ,hasRemainTerm,hasRemainTermevery from VehicleMaintenancePoilcy where id= S_AutoApprove.VehicleMaintenancePoilcyId;
                    exception
                      when no_data_found then
                        hasRemainTermevery:=0;

                        end;               
                        if hasRemainTermevery=1 then begin              
                    if (S_AutoApprove.timerange<TermlowDay )or (S_AutoApprove.timerange>TermupDay) then
                     update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'整车保养时间超保 '||chr(10),t.autoapprovestatus=2,
                      t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'整车保养时间超保 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
                     end if;
                    if (S_AutoApprove.Mileage<TermlowMileage)or (S_AutoApprove.Mileage>TermupMileage) then
                     update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'整车保养里程超保 '||chr(10),t.autoapprovestatus=2,
                      t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'整车保养里程超保 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
                     end if;
                    if (TermCapacity<S_AutoApprove.Capacity)and  (TermCapacity is not null) then
                     update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'整车方量超保 '||chr(10),t.autoapprovestatus=2,
                      t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'整车方量超保 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
                     end if;
                    if (TermWorkingHours<S_AutoApprove.WorkingHours)and  (TermWorkingHours is not null) then
                     update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'整车工作小时超保 '||chr(10),t.autoapprovestatus=2,
                      t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'整车工作小时超保 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
                     end if;
                     end;
                     end if;
              end;
              end if;

             end loop;
             end;
             if hasRemainTerm=0 and S_AutoApprove.repairtype in (3,4) then
             update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'单车保养条款不存在 '||chr(10),t.autoapprovestatus=2,
              t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'单车保养条款不存在 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
               end if;
            /* if hasRepairTerm=0 then
               update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'整车保修条款不存在 '||chr(10),t.autoapprovestatus=2,
                t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'整车保修条款不存在 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
               else*/ if hasRepairTerm=1 then begin
             IF flagIsture=FALSE THEN
               update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'车辆整车超保 '||chr(10),t.autoapprovestatus=2,
                t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'车辆整车超保 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10) where id=S_AutoApprove.id;
               end if;
               end ;
               end if;
               /*if hasPartTerm=0 then
               update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'配件保修条款不存在 '||chr(10),t.autoapprovestatus=2,
                t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'配件保修条款不存在 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;

               else*/
               if (hasPartTerm=1) then
                 begin
                    if flagPartIsTrue=false then
                  update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'祸首件超保 '||chr(10),t.autoapprovestatus=2,
                   t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'祸首件超保 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
                   end if;
                   end;
               end if;


        /*
查询保修政策服务产品线关系（保修政策服务产品线关系.服务产品线Id=参数 服务产品线Id ）
查询保修政策（保修政策.Id=步骤1返回的保修政策服务产品线关系.保修政策Id）
判断配件保修条款是否存在，查询配件保修条款（配件保修条款.保修政策Id=步骤2返回的保修政策.Id,配件保修条款.配件保修分类Id in 参数 配件保修分类Id数组）
2.逐条判断保修条款.如果保修时长<参数保修天数  or  方量<参数.方量  or  工作小时<参数.工作小时，则自动审核意见追加：配件保修分类xxx已经过保
如果参数 行驶里程，方量、工作小时为空再步骤2中不纳入判断
如果步骤2中查询不到数据则不作判断
自动审核状态=不通过*/


      begin
      select ServiceFee,--服务费金额,
             IsAssociatedApplication,--是否关联申请,
             IsNoMaterialFee,--是否材料费为0,
             RepeatedRepairTime,--重复维修次数,
             ClaimTimeOutLong, --索赔超时时长
             1,id
        into ServiceFee,
             IsAssociatedApplication,
             IsNoMaterialFee,
             RepeatedRepairTime,ClaimTimeOutLong,hasAutoApproveStrategy,StrategyId
        from AutoApproveStrategy t
       where t.partssalescategoryid = S_AutoApprove.partssalescategoryid;
       exception
         when no_data_found then
           hasAutoApproveStrategy:=0;
       end;
       if hasAutoApproveStrategy=1 then begin

          if S_AutoApprove.TotalAmount>ServiceFee then
          update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'索赔单服务费金额超过标准 '||chr(10),t.autoapprovestatus=2,
           t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'索赔单服务费金额超过标准 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
          end if;

          if (S_AutoApprove.MaterialCost=0)and (IsNoMaterialFee=1)then
          update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'索赔单材料费为0 '||chr(10),t.autoapprovestatus=2,
           t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'索赔单材料费为0 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
          end if;

          begin
          select 1
            into hasVideoMobileNumber
            from repairclaimbill
           where id = S_AutoApprove.id
             and VideoMobileNumber is  null and exists
                  (select 1
                      from DealerMobileNumberList
                     where DealerMobileNumberList.Dealerid =
                           repairclaimbill.dealerid);
                         exception
                           when no_data_found then
                             hasVideoMobileNumber:=0;
                             end;
           if hasVideoMobileNumber=1 then
             update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'索赔单缺失视频手机号 '||chr(10),t.autoapprovestatus=2,
              t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'索赔单缺失视频手机号 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
             end if;

           if (IsAssociatedApplication=1)and (S_AutoApprove.RepairClaimApplicationId is not null)then
             update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'关联申请单自动审核不通过 '||chr(10),t.autoapprovestatus=2,
              t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'关联申请单自动审核不通过 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
             end if;

              begin
            select  count(*)into RepeatedRepairTimeRC from repairclaimbill where id<>S_AutoApprove.id and vin=S_AutoApprove.vin and createtime>sysdate-30 and status<>99 and repairtype not in(3,4,7);
            exception
              when no_data_found then
                RepeatedRepairTimeRC:=0;
            end;
           if RepeatedRepairTimeRC> RepeatedRepairTime then
               update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'车辆重复维修 '||chr(10),t.autoapprovestatus=2,
                t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'车辆重复维修 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
               end if;

           begin
           select count(*) into hasRepairFaultReason from AppointFaultReasonDtl where AutoApproveStrategyId=StrategyId and  FaultReasonId=S_AutoApprove.MalfunctionId;
           exception
             when no_data_found then
               hasRepairFaultReason:=0;
               end;
            if  hasRepairFaultReason>0 then
              update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'指定故障原因自动审核不通过 '||chr(10),t.autoapprovestatus=2,
               t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'指定故障原因自动审核不通过  '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
              end if;

           begin
           select count(*) into hasDealer from AppointDealerDtl where AutoApproveStrategyId=StrategyId and  dealerid=S_AutoApprove.dealerid;
           exception
             when no_data_found then
               hasDealer:=0;
               end;
            if  hasDealer>0 then
              update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'指定服务站自动审核不通过 '||chr(10),t.autoapprovestatus=2,
               t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'指定服务站自动审核不通过  '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
              end if;

           begin
           select count(*) into hasServiceProductLine from AppointServiceProductLineDtl where AutoApproveStrategyId=StrategyId and  ServiceProductlineId=S_AutoApprove.ServiceProductlineId;
           exception
             when no_data_found then
               hasServiceProductLine:=0;
               end;
            if  hasServiceProductLine>0 then
              update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'指定服务产品线自动审核不通过 '||chr(10),t.autoapprovestatus=2,
               t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'指定服务产品线自动审核不通过  '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
              end if;

            begin
           select count(*) into hasRepairType from AppointRepairTypeDtl where AutoApproveStrategyId=StrategyId  and  repairtype=S_AutoApprove.repairtype;
           exception
             when no_data_found then
               hasRepairType:=0;
               end;
            if  hasRepairType>0 then
              update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'指定维修类型自动审核不通过 '||chr(10),t.autoapprovestatus=2,
               t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'指定维修类型自动审核不通过  '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
              end if;

            begin
           select count(*) into hasSupplierId from AppointSupplierDtl where AutoApproveStrategyId=StrategyId  and  SupplierId=S_AutoApprove.FaultyPartsSupplierId;
           exception
             when no_data_found then
               hasSupplierId:=0;
               end;
            if  hasSupplierId>0 then
              update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'指定供应商自动审核不通过 '||chr(10),t.autoapprovestatus=2,
               t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'指定供应商自动审核不通过  '||chr(10)||'自动审核时间：'||S_AutoApprove.time1 ||chr(10) where id=S_AutoApprove.id;
              end if;
         end;
         end if;

         select FinishingTime into finishtime from repairorder where repairorder.id=S_AutoApprove.repairorderid;
         if (floor(to_number(S_AutoApprove.createtime-finishtime)*24)>ClaimTimeOutLong)then
           update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'索赔提报超时 '||chr(10),t.autoapprovestatus=2,
            t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'索赔提报超时 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
           end if;

         begin
         select count(*) into QBTIME  from   repairorder t where t.vin=S_AutoApprove.vin and status<>99 AND repairtype=3 ;
         exception
           when no_data_found then
             QBTIME:=0;
             end;
         if qbtime>=2 then
            update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'进行了多次强保，强保次数为'||QBTIME||'次'||chr(10),t.autoapprovestatus=2,
            t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'进行了多次强保，强保次数为 '||QBTIME||'次'||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
            end if;

         if S_AutoApprove.SupplierConfirmStatus =3 then
            update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'供应商确认不通过'||chr(10),t.autoapprovestatus=2,
            t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'供应商确认不通过  '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
            end if;

 /*




查询维修单（vin=维修保养索赔单.vin 维修类型=强制保养，状态<>作废）
如果查询到的维修单数量>2 自动审核意见追加:进行了多次强保，强保次数为<查询到维修单数量>
自动审核状态=不通过


如果维修保养索赔单.供应商确认状态<>已确认，自动审核状态追加：供应商确认不通过或未确认
自动审核状态=不通过


*/


      end loop;
    end;
  end;

/



create or replace procedure AutoApprovePreSaleCheckOrder as
begin

  declare
    cursor AutoApprove is(
      select p.Id PId,
             v.Id VId
        from dcs.PreSaleCheckOrder p
       inner join dcs.VehicleInformation v
          on p.VEHICLEID = v.Id
       where p.status = 2
             and p.SubmitTime >= sysdate - 1
             and v.PreSaleTime = 0);
  
  begin
    for S_AutoApprove in AutoApprove
    loop
      begin
        update PreSaleCheckOrder
           set status = 3
         where id = S_AutoApprove.PId;
      
        update VehicleInformation
           set PreSaleTime = PreSaleTime + 1
         where id = S_AutoApprove.VId;
      end;
    end loop;
  end;

end AutoApprovePreSaleCheckOrder;
/



create or replace procedure TimingEmptyCustomerCredence as

  cursor tmpCursor is
    select branchid from Branchstrategy bs
    where IsResetCreditStrategy=1;

  tmpCursorRow  tmpCursor%rowtype;

begin
  open tmpCursor;
    loop

      fetch tmpCursor into tmpCursorRow;
      exit when tmpCursor%notfound;

 -- 查询 分公司策略（是否清除授信策略=是）
-- 关联账户组（分公司策略.营销分公司Id=账户组.销售企业Id）
-- 关联客户账户（客户账户.账户组Id=账户组.Id）
      declare
        cursor Cursor_ca is(
          select ca.Id CustomerAccountId,ca.AccountGroupId,ca.CustomerCompanyId,sc.Code CompanyCode,sc.Name CompanyName from CustomerAccount ca
          inner join AccountGroup ag on ca.accountgroupid=ag.id
          inner join Company sc on ag.salescompanyid=sc.id
          where ag.salescompanyid=tmpCursorRow.branchid
        );

-- 更新客户账户 赋值：客户信用总额=0
       begin
         for S_role in Cursor_ca loop
           update CustomerAccount set CustomerCredenceAmount = 0 where id = S_role.CustomerAccountId;

           insert into CredenceApplication
           (  id,
              Code,
              SalesCompanyId,
              GuarantorCompanyId,
              GuarantorCompanyName,
              AccountGroupId,
              CustomerCompanyId,
              ValidationDate,
              ExpireDate,
              ApplyCondition,
              CredenceLimit,
              Status,
              ConsumedAmount,
              CREATORID             ,
              CREATORNAME          ,
              CREATETIME
           )
            -- 编码 {CA}{销售企业.编号}{YYYYMMDD}{4位=9000+rownum}
            -- 生成信用申请单 赋值：
            -- 信用申请单.销售企业Id=分公司策略.营销分公司Id
            -- 信用申请单.担保企业Id=分公司策略.营销分公司Id
            -- 信用申请单.担保企业名称=企业.企业名称
            -- 信用申请单.账户组Id=客户账户.账户组id
            -- 信用申请单.客户企业Id=客户账户.客户企业id
            -- 信用申请单.生效日期=当前时间
            -- 信用申请单.失效日期=当前时间
            -- 信用申请单.使用条件=自由支配使用
            -- 信用申请单.信用额度=0
            -- 信用申请单.状态=已审批
            -- 信用申请单.已使用金额=0
           values
           (s_CredenceApplication.Nextval,
             regexp_replace(
                regexp_replace('CA{CORPCODE}'||to_char(sysdate,'yyyymmdd')||'{SERIAL}','{CORPCODE}',S_role.CompanyCode),
                '{SERIAL}',
                9000+rownum
              ) ,
            /* 'CA'|| S_role.CompanyCode||CO_CHAR(SYSDATE,'YYYYMMDD')||9000+rownum CODE,*/
             tmpCursorRow.Branchid,
             tmpCursorRow.Branchid,
             S_role.CompanyName,
             S_role.AccountGroupId,
             S_role.CustomerCompanyId,
             sysdate,
             sysdate,
             2,
             0,
             2,
             0,
             1,
             'Admin',
             sysdate
           );
         end loop;
         close Cursor_ca;
       end;

    end loop;
 close tmpCursor;
end;
/


create or replace procedure AutoExpireCredit is
 cursor tmpCursor is
    select bs.* from CredenceApplication bs
    inner join Branchstrategy  on  bs.salescompanyid=Branchstrategy.Branchid
    where to_char(bs.expiredate,'yyyy-mm-dd') =
to_char(sysdate,'yyyy-mm-dd') and bs.status=3 and Branchstrategy.IsResetCreditStrategy=2;

  tmpCursorRow  tmpCursor%rowtype;

begin
  open tmpCursor;
    loop

      fetch tmpCursor into tmpCursorRow;
      exit when tmpCursor%notfound;
      update CredenceApplication
         set status = 4
       where CredenceApplication.id = tmpCursorRow.Id;
       update CustomerAccount t
          set CustomerCredenceAmount = 0
        where t.customercompanyid = tmpCursorRow.Customercompanyid
          and t.accountgroupid = tmpCursorRow.Accountgroupid;
   end loop;
 close tmpCursor;
end AutoExpireCredit;

--自动设置采购订单入库状态
Create Or Replace Procedure Autosetpurchaseorderinstatus As
  Isallnew       Number(9);
  Isallfinish    Number(9);
  Hasforcefinish Number(9);
  Isforcefinish  Number(9);
  Iscanncel      Number(9);
Begin

  Declare
    Cursor Autoset Is(
      Select Id
        From Partspurchaseorder
       Where Status In (3, 4, 5, 6, 7, 99)
         And Instatus In (5, 10));
  
  Begin
    For s_Autoset In Autoset Loop
      Begin
        Select Count(*)
          Into Isallnew
          From Suppliershippingorder a
         Inner Join Partsinboundplan b
            On a.Code = b.Sourcecode
         Inner Join Partsinboundplandetail c
            On b.Id = c.Partsinboundplanid
         Where a.Partspurchaseorderid = s_Autoset.Id
           And Nvl(c.Inspectedquantity, 0) > 0;
        If Isallnew = 0 Then
          Update Partspurchaseorder
             Set Instatus = 5
           Where Id = s_Autoset.Id;
        End If;
        Select Count(*)
          Into Isallfinish
          From Partspurchaseorderdetail d
          Left Join (Select a.Partspurchaseorderid,
                            c.Sparepartid,
                            Sum(c.Inspectedquantity) Inspectedquantity,
                            Sum(c.Plannedamount) Plannedamount
                       From Suppliershippingorder a
                      Inner Join Partsinboundplan b
                         On a.Code = b.Sourcecode
                      Inner Join Warehouse w
                         On b.Warehouseid = w.Id
                      Inner Join Partsinboundplandetail c
                         On b.Id = c.Partsinboundplanid
                      Inner Join Partspurchaseorderdetail d
                         On d.Partspurchaseorderid = a.Partspurchaseorderid
                        And d.Sparepartid = c.Sparepartid
                      Where a.Partspurchaseorderid = s_Autoset.Id
                        And w.Type != 99
                      Group By c.Sparepartid, a.Partspurchaseorderid) t
            On d.Partspurchaseorderid = t.Partspurchaseorderid
           And d.Sparepartid = t.Sparepartid
         Where ((t.Inspectedquantity <> t.Plannedamount) Or
               (Nvl(d.Confirmedamount, 0) <> Nvl(t.Inspectedquantity, 0)) Or
               (d.Confirmedamount Is Null Or d.Confirmedamount = 0))
           And d.Partspurchaseorderid = s_Autoset.Id;
      
        Select Count(*)
          Into Iscanncel
          From Partspurchaseorder
         Where Id = s_Autoset.Id
           And Status = 99;
        Select Count(*)
          Into Hasforcefinish
          From Partspurchaseorder a
         Inner Join Partspurchaseorderdetail b
            On a.Id = b.Partspurchaseorderid
          Left Join Suppliershippingorder c
            On c.Partspurchaseordercode = a.Code
          Left Join Suppliershippingdetail d
            On d.Suppliershippingorderid = c.Id
           And b.Sparepartid = d.Sparepartid
          Left Join Partsinboundplan e
            On e.Sourcecode = c.Code
          Left Join Partsinboundplandetail f
            On f.Partsinboundplanid = e.Id
           And b.Sparepartid = f.Sparepartid
         Where (a.Status = 7 Or c.Status = 4 Or e.Status = 3)
           And a.Id = s_Autoset.Id;
        Select Count(*)
          Into Isforcefinish
          From (Select *
                  From (Select a.Id          Aid,
                               c.Id          Cid,
                               e.Id          Eid,
                               b.Sparepartid,
                               -- d.sparepartid,
                               Sum(Nvl(b.Orderamount, 0) -
                                   Nvl(b.Confirmedamount, 0)) Partspurchaseorderamount,
                               Sum(Nvl(b.Confirmedamount, 0) -
                                   Nvl(b.Shippingamount, 0)) Shippingamount,
                               Sum(Nvl(d.Quantity, 0) -
                                   Nvl(d.Confirmedamount, 0)) Suppliershippingorderamount,
                               Sum(Nvl(f.Plannedamount, 0) -
                                   Nvl(f.Inspectedquantity, 0)) Partsinboundplanamount,
                               Sum(Nvl(f.Inspectedquantity, 0)) Inspectedquantity,
                               Sum(Nvl(b.Orderamount, 0)) Orderamount
                          From Partspurchaseorder a
                         Inner Join Partspurchaseorderdetail b
                            On a.Id = b.Partspurchaseorderid
                          Left Join Suppliershippingorder c
                            On c.Partspurchaseordercode = a.Code
                          Left Join Suppliershippingdetail d
                            On d.Suppliershippingorderid = c.Id
                           And b.Sparepartid = d.Sparepartid
                          Left Join Partsinboundplan e
                            On e.Sourcecode = c.Code
                          Left Join Partsinboundplandetail f
                            On f.Partsinboundplanid = e.Id
                           And b.Sparepartid = f.Sparepartid
                         Where (a.Status = 7 Or c.Status = 4 Or
                               (e.Status = 3 And f.Id Is Not Null))
                           And a.Id = s_Autoset.Id
                         Group By a.Id, c.Id, e.Id, b.Sparepartid --, d.sparepartid
                        )
                 Where Inspectedquantity + Partsinboundplanamount +
                       Suppliershippingorderamount +
                       Partspurchaseorderamount + Shippingamount <>
                       Orderamount
                Union
                Select *
                
                  From (Select a.Id          Aid,
                               c.Id          Cid,
                               e.Id          Eid,
                               b.Sparepartid,
                               -- d.sparepartid,
                               Sum(Nvl(b.Orderamount, 0) -
                                   Nvl(b.Confirmedamount, 0)) Partspurchaseorderamount,
                               Sum(Nvl(b.Confirmedamount, 0) -
                                   Nvl(b.Shippingamount, 0)) Shippingamount,
                               Sum(Nvl(d.Quantity, 0) -
                                   Nvl(d.Confirmedamount, 0)) Suppliershippingorderamount,
                               Sum(Nvl(f.Plannedamount, 0) -
                                   Nvl(f.Inspectedquantity, 0)) Partsinboundplanamount,
                               Sum(Nvl(f.Inspectedquantity, 0)) Inspectedquantity,
                               Sum(Nvl(b.Orderamount, 0)) Orderamount
                          From Partspurchaseorder a
                         Inner Join Partspurchaseorderdetail b
                            On a.Id = b.Partspurchaseorderid
                          Left Join Suppliershippingorder c
                            On c.Partspurchaseordercode = a.Code
                          Left Join Suppliershippingdetail d
                            On d.Suppliershippingorderid = c.Id
                           And b.Sparepartid = d.Sparepartid
                          Left Join Partsinboundplan e
                            On e.Sourcecode = c.Code
                          Left Join Partsinboundplandetail f
                            On f.Partsinboundplanid = e.Id
                           And b.Sparepartid = f.Sparepartid
                         Where (a.Status = 7 Or c.Status = 4 Or e.Status = 3)
                           And (e.Status = 4 Or c.Status = 1 Or c.Status = 3 Or
                               e.Status = 1)
                           And a.Id = s_Autoset.Id
                         Group By a.Id, c.Id, e.Id, b.Sparepartid --, d.sparepartid
                        ));
        If (Isallfinish = 0) And (Isallnew <> 0) Then
          Update Partspurchaseorder
             Set Instatus = 15
           Where Id = s_Autoset.Id;
        End If;
        If (Isallnew > 0) And (Isallfinish > 0) Then
          Update Partspurchaseorder
             Set Instatus = 10
           Where Id = s_Autoset.Id;
        End If;
        If (Hasforcefinish > 0) And (Isforcefinish = 0) /* and ( IsAllFinish<>0)*/
         Then
          Update Partspurchaseorder
             Set Instatus = 11
           Where Id = s_Autoset.Id;
        End If;
        If Iscanncel > 0 Then
          Update Partspurchaseorder
             Set Instatus = 99
           Where Id = s_Autoset.Id;
        End If;
      End;
    End Loop;
  End;
End Autosetpurchaseorderinstatus;


create or replace procedure addPartsExchange as
  TCTABLE VARCHAR2(200) := 'truncate table ExchangeAdd_Tmp';
begin
  declare
    cursor Autoadd is(
      select id, code, referencecode
        from sparepart
       where not exists (select 1
                from PartsExchange
               where PartsExchange.Partid = sparepart.id)
         and sparepart.status = 1
         and exists (select 1
                from sparepart a
               where a.id <> sparepart.id
                 and a.referencecode = sparepart.referencecode
                 and a.status = 1)
         and rownum <= 1000);

  begin
    EXECUTE IMMEDIATE TCTABLE;


    for S_Autoadd in Autoadd loop
      begin
        insert into ExchangeAdd_Tmp
          (id,
           Exchangecode,
           Exchangename,
           Partid,
           Status,
           Creatorid,
           Creatorname,
           Createtime,
           remark)
          select s_PartsExchange.Nextval,
                 a.referencecode,
                 a.referencename,
                 a.id,
                 1,
                 1,
                 'Admin',
                 sysdate,
                 '自动批量处理互换号'
            from (select distinct case
                                    when tmp.referencecode is not null then
                                     tmp.referencecode
                                    else
                                     a.referencecode
                                  end as referencecode,
                                  case
                                    when tmp.referencecode is not null then
                                     tmp.referencecode
                                    else
                                     a.referencecode
                                  end as referencename,
                                  a.id
                    from sparepart a
                    left join (select a.exchangecode,
                                     a.exchangename,
                                     b.referencecode
                                from partsexchange a
                               inner join sparepart b
                                  on a.partid = b.id
                               where a.status = 1) tmp
                      on a.referencecode = tmp.referencecode
                   where not exists (select 1
                            from PartsExchange
                           where PartsExchange.Partid = a.id
                             and PartsExchange. status = 1)
                     and a.status = 1
                     and replace(a.referencecode, ' ', '') is not null
                     and a.id = S_Autoadd.id
                     and exists
                   (select 1
                            from sparepart p
                           where p.id <> a.id
                             and p.referencecode = a.referencecode
                             and p.status = 1)) a;



        insert into PartsExchange
          (id,
           Exchangecode,
           Exchangename,
           Partid,
           Status,
           Creatorid,
           Creatorname,
           Createtime,
           remark)
          select id,
                 Exchangecode,
                 Exchangename,
                 Partid,
                 Status,
                 Creatorid,
                 Creatorname,
                 Createtime,
                 remark
            from ExchangeAdd_Tmp;

        insert into PartsExchangeHistory
          (id,
           Partsexchangeid,
           Exchangecode,
           Exchangename,
           Partid,
           Status,
           Remark,
           Creatorid,
           Creatorname,
           Createtime)
          select s_PartsExchangeHistory.Nextval,
                 id,
                 Exchangecode,
                 Exchangename,
                 Partid,
                 Status,
                 remark,
                 Creatorid,
                 Creatorname,
                 Createtime
            from ExchangeAdd_Tmp;
                EXECUTE IMMEDIATE TCTABLE;
      end;
    end loop;
  end;
end addpartsexchange;
