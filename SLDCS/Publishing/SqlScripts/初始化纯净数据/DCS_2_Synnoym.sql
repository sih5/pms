-- 为 xxSecurity 数据库的 Enterprise、Personnel、Organization、Role、OrganizationPersonnel、RolePersonnel 创建同义词
-- 请调整xxSecurity对应方案名
create or replace synonym Organization for xxSecurity.Organization;
create or replace synonym OrganizationPersonnel for xxSecurity.OrganizationPersonnel;
create or replace synonym AddEnterprise for xxSecurity.AddEnterprise;
create or replace synonym EditEnterprise for xxSecurity.EditEnterprise;
create or replace synonym AbandonEnterprise for xxSecurity.AbandonEnterprise;
