--创建层次结构
truncate table LayerStructure
/
insert into LayerStructure(id,code,purpose,maxlayernumber,remark)
values(1,'WXXM','维修项目',3,'');
insert into LayerStructure(id,code,purpose,maxlayernumber,remark)
values(2,'GZXX','故障现象',3,'');

--层次节点类型
truncate table LayerNodeType
/
drop sequence S_LayerNodeType;
create sequence S_LayerNodeType start with 7;
insert into LayerNodeType(id,layerstructureid,code,name,endnode,toplevelnode)
values(1,1,'根','根',0,1);
insert into LayerNodeType(id,layerstructureid,code,name,endnode,toplevelnode)
values(2,1,'总成','总成',0,0);
insert into LayerNodeType(id,layerstructureid,code,name,endnode,toplevelnode)
values(3,1,'零部件','零部件',1,0);
insert into LayerNodeType(id,layerstructureid,code,name,endnode,toplevelnode)
values(4,2,'根','根',0,1);
insert into LayerNodeType(id,layerstructureid,code,name,endnode,toplevelnode)
values(5,2,'总成','总成',0,0);
insert into LayerNodeType(id,layerstructureid,code,name,endnode,toplevelnode)
values(6,2,'零部件','零部件',1,0);

--层次节点类型隶属关系
truncate table LayerNodeTypeAffiliation
/
insert into LayerNodeTypeAffiliation(id,layerstructureid,parentlayernodetypeid,sublayernodetypeid)
values(1,1,1,2);
insert into LayerNodeTypeAffiliation(id,layerstructureid,parentlayernodetypeid,sublayernodetypeid)
values(2,1,2,2);
insert into LayerNodeTypeAffiliation(id,layerstructureid,parentlayernodetypeid,sublayernodetypeid)
values(3,1,2,3);
insert into LayerNodeTypeAffiliation(id,layerstructureid,parentlayernodetypeid,sublayernodetypeid)
values(4,2,4,5);
insert into LayerNodeTypeAffiliation(id,layerstructureid,parentlayernodetypeid,sublayernodetypeid)
values(5,2,5,5);
insert into LayerNodeTypeAffiliation(id,layerstructureid,parentlayernodetypeid,sublayernodetypeid)
values(6,2,5,6);

--指定层次结构用途
truncate table LayerStructurePurpose
/
insert into LayerStructurePurpose(id,purposedescription,layerstructureid,rootnodeid,boname,purposetype)
values(1,'保留',1,0,'保留',1);
insert into LayerStructurePurpose(id,purposedescription,layerstructureid,rootnodeid,boname,purposetype)
values(2,'保留',2,0,'保留',2);

--车辆属性
truncate table vehicleattribute
/
insert into vehicleattribute(id,attributename,datatype)
values(1,'整车编号'，4);
insert into vehicleattribute(id,attributename,datatype)
values(2,'销售日期'，1);
insert into vehicleattribute(id,attributename,datatype)
values(3,'下线日期'，1);
insert into vehicleattribute(id,attributename,datatype)
values(4,'出厂日期'，1);

--添加树的根节点
insert into MalfunctionCategory(
ID,  CODE,  NAME , LAYERNODETYPEID , PARENTID,  ROOTGROUPID  ,STATUS  ,REMARK  ,CREATORID,  CREATORNAME,  CREATETIME) values 
(S_MalfunctionCategory.Nextval,'WXXM根','WXXM根',4,'',1,1,'',1,'系统管理员',sysdate);
/
--添加集团企业
insert into Company (id,Type,Code,Name,status) values (0,8,'集团企业','集团企业',1);
--添加集团企业登陆图片
insert into CompanyLoginPicture (id,Companyid) values (0,0);

--服务活动 已使用台次 默认0
alter table SERVICEACTIVITY modify USEDACTIVITYCOUNT default 0;
--服务活动配额 已使用配额 默认0
alter table SERVICEACTIVITYQUOTA modify QUOTAUSED default 0;
--外出服务索赔单 供应商结算状态 默认2
alter table ServiceTripClaimBill modify SupplierSettleStatus default 2;

--给售前检查添加无故障数据
insert into PreSaleItem (ID, BRANCHID, PARTSSALESCATEGORYID, CHECKITEM, CATEGORY, FAULTPATTERN, STATUS, CREATORID, CREATORNAME, CREATETIME, MODIFIERID, MODIFIERNAME, MODIFYTIME, ABANDONERID, ABANDONERNAME, ABANDONTIME, SERVICEPRODUCTLINEID)
values (999999, 0, 0, '无故障', '无故障', '无故障', 1, null, '', null, null, '', null, null, '', null, 0);

--维修保养索赔单 旧件处理状态 默认1
alter table RepairClaimBill modify Usedpartsdisposalstatus default 1;

--配件营销信息 配件仓储管理粒度 默认3
alter table PartsBranch modify PartsWarhouseManageGranularity default 3;

--分公司与供应商关系工时系数，旧件运费系数赋默认值1，是否按工时单价索赔，是否存在外出费用赋默认值0
alter table BranchSupplierRelation modify IfSPByLabor       default 0;
alter table BranchSupplierRelation modify IfHaveOutFee       default 0;
alter table BranchSupplierRelation modify OldPartTransCoefficient default 1;
alter table BranchSupplierRelation modify LaborCoefficient     default 1;

--分公司策略
insert into BRANCHSTRATEGY (ID, BRANCHID, PARTSPURCHASEPRICINGSTRATEGY, EMERGENCYSALESSTRATEGY, USEDPARTSRETURNSTRATEGY, PARTSSHIPPING, ISGPS, DIRECTSUPPLYSTRATEGY, PURCHASEPRICINGSTRATEGY, CLAIMAUTOSTOPSTRATEGY, CLAIMREJECTTIMESTRATEGY, STOPCLAIMSTARTSTRATEGY, STOPCLAIMTIMESTRATEGY, SCANCODESTARTTIME, ISALLSERVICETRIPAPP, ISFIRSTMAINTEMANAGE, ISOUTERPURCHASEMANAGE, ISDEBTMANAGE, OUTFORCE, OUTNUMBERPEOPLE, OUTNUMBERDAYS, STATUS, CREATORID, CREATORNAME, CREATETIME, MODIFIERID, MODIFIERNAME, MODIFYTIME, ABANDONERID, ABANDONERNAME, ABANDONTIME, ROWVERSION)
values (S_Branchstrategy.Nextval, 2, 2, null, null, null, null, null, null, null, null, null, 24, null, null, null, null, null, null, null, null, 1, null, null, null, null, null, null, null, null, null, null);

--品牌策略
insert into SALESCENTERSTRATEGY (ID, PARTSSALESCATEGORYID, PARTSBRANCHDEFAULT, ALLOWCUSTOMERCREDIT, STATUS, CREATORID, CREATORNAME, CREATETIME, MODIFIERID, MODIFIERNAME, MODIFYTIME, ABANDONERID, ABANDONERNAME, ABANDONTIME, ROWVERSION)
values (S_SalesCenterstrategy.Nextval, 1, null, 0, 2, null, null, null, null, null, null, null, null, null, null);


--故障原因默认值
alter table REPAIRORDERFAULTREASON modify OUTFAULTREASON default 0;

--售前检查结算状态默认值
alter table PRESALECHECKORDER modify SETTLESTATUS default 2;

insert into repairitem
  (ID,
   CODE,
   NAME,
   REPAIRITEMCATEGORYID,
   QUICKCODE,
   JOBTYPE,
   REPAIRQUALIFICATIONGRADE,
   STATUS,
   REMARK,
   CREATORID,
   CREATORNAME,
   CREATETIME,
   MODIFIERID,
   MODIFIERNAME,
   MODIFYTIME)
values
  (99999999,
   '9999999999',
   '虚拟维修项目',
   0,
   '',
   4,
   0,
   1,
   '',
   1,
   'Admin',
   sysdate,
   null,
   '',
   null);

insert into malfunction
  (ID,
   CODE,
   DESCRIPTION,
   MALFUNCTIONCATEGORYID,
   QUICKCODE,
   STATUS,
   REMARK,
   CREATORID,
   CREATORNAME,
   CREATETIME)
values
  (99999999,
   '9999999999',
   '虚拟故障原因',
   0,
   '',
   1,
   '',
   1,
   'Admin',
   Sysdate);


insert into malfunctionbrandrelation
  (ID,
   BRANCHID,
   MALFUNCTIONID,
   MALFUNCTIONCODE,
   MALFUNCTIONNAME,
   PARTSSALESCATEGORYID,
   ISAPPLICATION,
   CREATORID,
   CREATORNAME,
   CREATETIME,
   STATUS)
select
  s_malfunctionbrandrelation.nextval,
   t.id,
   99999999,
   '9999999999',
   '虚拟故障原因',
   v.id,
   0,
   1,
   'Admin',
   sysdate,
   1
   from branch t inner join partssalescategory v on t.id=v.branchid;

insert into repairitembrandrelation
  (ID,
   BRANCHID,
   REPAIRITEMID,
   REPAIRITEMCODE,
   REPAIRITEMNAME,
   PARTSSALESCATEGORYID,
   CREATORID,
   CREATORNAME,
   CREATETIME,
   STATUS,repairqualificationgrade)
select 
 s_repairitembrandrelation.nextval,
   t.id,
   99999999,
   '9999999999',
   '虚拟维修项目',
   v.id,
   1,
   'Admin',
   sysdate,
   '1',1
   from branch t inner join partssalescategory v on t.id=v.branchid;


insert into VEHICLECATEGORY (BRANCHID, ID, CODE, NAME, EXTENDEDINFOTEMPLATECATEGORY, STATUS, REMARK, CREATORID, CREATORNAME, CREATETIME, MODIFIERID, MODIFIERNAME, MODIFYTIME, ROWVERSION)
values (2, 1, '2', '2', null, 1, '', null, '', null, null, '', null, '');

insert into RepairItemCatAffiVehiCat（id,VehicleCategoryId,RepairItemCategoryId）values(s_RepairItemCatAffiVehiCat.nextval,1,1);
insert into MalfCatAffiVehiCat(id,VehicleCategoryId,MalfunctionCategoryId)values(s_MalfCatAffiVehiCat.nextval,1,1);

--仓库wms接口默认0
alter table WAREHOUSE modify WMSINTERFACE default 0;
update WAREHOUSE set WMSINTERFACE=0 where WMSINTERFACE is null;
insert into repairobject
  (id,
   partssalescategoryid,
   partssalescategoryname,
   repairtarget,
   status,
   creatorid,
   creatorname,
   createtime)
values
  (s_repairobject.nextval, 0, ' ', '整车', 1, 1, 'Admin', sysdate);
insert into repairobject
  (id,
   partssalescategoryid,
   partssalescategoryname,
   repairtarget,
   status,
   creatorid,
   creatorname,
   createtime)values
  (s_repairobject.nextval, 0, ' ', '底盘', 1, 1, 'Admin', sysdate);
insert into repairobject
  (id,
   partssalescategoryid,
   partssalescategoryname,
   repairtarget,
   status,
   creatorid,
   creatorname,
   createtime)values
  (s_repairobject.nextval, 0, ' ', '专项发动机', 1, 1, 'Admin', sysdate);

