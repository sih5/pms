--公共业务
create Index Idx_Personnel_LoginId on Personnel (LoginId);
create Index Idx_Personnel_CorporationId on Personnel (CorporationId);
create Index Idx_Countersignature_SourceId on Countersignature (SourceId);
create Index Idx_CountersignDtl_CgnatureId on CountersignatureDetail (CountersignatureId);
create Index Idx_CountersignDtl_ApproverId on CountersignatureDetail (ApproverId);
create Index Idx_Notification_CompanyId on Notification (CompanyId);
create Index Idx_NotifiLimit_NoticeId on NotificationLimit (NoticeId);
create Index Idx_Region_ParentId on Region (ParentId);
create Index Idx_Region_EnterpriseId on Region (EnterpriseId);
create Index Idx_SExportState_EnterpriseId on ScheduleExportState (EnterpriseId);
create Index Idx_LSPurpose_LayerStructureId on LayerStructurePurpose (LayerStructureId);
create Index Idx_LSPurpose_RootNodeId on LayerStructurePurpose (RootNodeId);
create Index Idx_LNType_LayerStructureId on LayerNodeType (LayerStructureId);
create Index Idx_LNTypeAff_LayerStructureId on LayerNodeTypeAffiliation (LayerStructureId);
create Index Idx_LNTypeAff_PLayerNodeTypeId on LayerNodeTypeAffiliation (ParentLayerNodeTypeId);
create Index Idx_LNTypeAff_SLayerNodeTypeId on LayerNodeTypeAffiliation (SubLayerNodeTypeId);
--create Index Idx_CodeTemplate_CorporationId on CodeTemplate (CorporationId);
create Index Idx_CodeTemplateS_TemplateId on CodeTemplateSerial (TemplateId);
--配件信息相关
create Index Idx_ABCStrategy_BranchId on ABCStrategy (BranchId);
create Index Idx_PartsExchange_PartId on PartsExchange (PartId);
create Index Idx_PartsExchangeHis_PartId on PartsExchangeHistory (PartId);
create Index Idx_PartsExchangeHis_PartsExId on PartsExchangeHistory (PartsExchangeId);
create Index Idx_SparePartHis_SparePartId on SparePartHistory (SparePartId);
create Index Idx_PartDeleaveInfo_OldPartId on PartDeleaveInformation (OldPartId);
create Index Idx_PartDeleaveInfo_PSCId on PartDeleaveInformation (PartsSalesCategoryId);
create Index Idx_PartDeleaveInfo_BranchId on PartDeleaveInformation (BranchId);
create Index Idx_PartDeleaveHis_PDInfoId on PartDeleaveHistory (PartDeleaveInformationId);
create Index Idx_PartDeleaveHis_OldPartId on PartDeleaveHistory (OldPartId);
create Index Idx_PartDeleaveHis_PSCId on PartDeleaveHistory (PartsSalesCategoryId);
create Index Idx_PartsReplacement_OldPartId on PartsReplacement (OldPartId);
create Index Idx_PartsReplacement_NewPartId on PartsReplacement (NewPartId);
create Index Idx_PartsReplaceHis_NewPartId on PartsReplacementHistory (NewPartId);
create Index Idx_PartsReplaceHis_OldPartId on PartsReplacementHistory (OldPartId);
create Index Idx_PartsReplaceHis_PReplaceId on PartsReplacementHistory (PartsReplacementId);
create Index Idx_CombinedPart_ParentId on CombinedPart (ParentId);
create Index Idx_CombinedPart_PartId on CombinedPart (PartId);
create Index Idx_PartsBranch_PartId on PartsBranch (PartId);
create Index Idx_PartsBranch_BranchId on PartsBranch (BranchId);
create Index Idx_PartsBranch_PSCategoryId on PartsBranch (PartsSalesCategoryId);
create Index Idx_PartsBranch_PWCategoryId on PartsBranch (PartsWarrantyCategoryId);
create Index Idx_PartsBranch_ABCStrategyId on PartsBranch (ABCStrategyId);
create Index Idx_PartsBranchHis_PBranchId on PartsBranchHistory (PartsBranchId);
create Index Idx_PartsBranchHis_PartId on PartsBranchHistory (PartId);
create Index Idx_PartsBranchHis_BranchId on PartsBranchHistory (BranchId);
create Index Idx_PartsBranchHis_ABCSId on PartsBranchHistory (ABCStrategyId);
create Index Idx_PartsBranchHistory_PSCId on PartsBranchHistory (PartsSalesCategoryId);
create Index Idx_PartsBranchHistory_PWCId on PartsBranchHistory (PartsWarrantyCategoryId);
--产品结构
create Index Idx_Product_BrandId on Product (BrandId);
create Index Idx_ProAffiCategory_ProCId on ProductAffiProductCategory (ProductCategoryId);
create Index Idx_ProAffiCategory_ProductId on ProductAffiProductCategory (ProductId);
create Index Idx_ProAffiCategory_RootCId on ProductAffiProductCategory (RootCategoryId);
create Index Idx_ProductCategory_LNTypeId on ProductCategory (LayerNodeTypeId);
create Index Idx_ProductCategory_RootCId on ProductCategory (RootCategoryId);
create Index Idx_ProCExtendInfo_ProCId on ProductCategoryExtendInfo (ProductCategoryId);
create Index Idx_ProCExtendInfo_ExInfoTemId on ProductCategoryExtendInfo (ExtendedInfoTemplateId);
create Index Idx_ProExtendInfo_ProductId on ProductExtendInfo (ProductId);
create Index Idx_ProExtendInfo_ExInfoTempId on ProductExtendInfo (ExtendedInfoTemplateId);
/*create Index Idx_OptionalList_ProductId on OptionalList (ProductId);
create Index Idx_OptionalList_CategoryId on OptionalList (CategoryId);
create Index Idx_OptionalList_SparePartId on OptionalList (SparePartId);
create Index Idx_OptionalList_MeasureUnitId on OptionalList (MeasureUnitId);
create Index Idx_OptionalList_SupplierId on OptionalList (SupplierId);*/
create Index Idx_BranchVehBrand_BranchId on BranchVehicleBrand (BranchId);
create Index Idx_EngineProductLine_BranchId on EngineProductLine (BranchId);
create Index Idx_EngineProductLine_PSCId on EngineProductLine (PartsSalesCategoryId);
create Index Idx_EngineModel_BranchId on EngineModel (BranchId);
create Index Idx_EMProLine_BranchId on EngineModelProductLine (BranchId);
create Index Idx_EMProLine_PSCId on EngineModelProductLine (PartsSalesCategoryId);
create Index Idx_EMProLine_EProLineId on EngineModelProductLine (EngineProductLineId);
create Index Idx_EMProLine_EngineModeId on EngineModelProductLine (EngineModeId);
--create Index Idx_ExInfoTemp_BranchId on ExtendedInfoTemplate (BRANDID);
create Index Idx_ExInfoTemp_CorporationId on ExtendedInfoTemplate (CorporationId);
create Index Idx_SerProductLine_BranchId on ServiceProductLine (BranchId);
create Index Idx_SerProductLine_PSCId on ServiceProductLine (PartsSalesCategoryId);
create Index Idx_SerProdLinePro_BranchId on ServiceProdLineProduct (BRANDID);
create Index Idx_SerProdLinePro_PSCId on ServiceProdLineProduct (PartsSalesCategoryId);
create Index Idx_SerProdLinePro_SPLineId on ServiceProdLineProduct (ServiceProductLineId);

create Index Idx_SProdLineProDtl_SerPLineId on ServProdLineProductDetail (ServiceProductLineId);
create Index Idx_SProdLineProDtl_ProductId on ServProdLineProductDetail (ProductId);

create Index Idx_VehicleCategory_BranchId on VehicleCategory (BranchId);

create Index Idx_VehCProdDtl_VehCId on VehicleCategoryProductDetail (VehicleCategoryId);
create Index Idx_VehCProdDtl_ProductId on VehicleCategoryProductDetail (ProductId);
--企业及内部组织
create Index Idx_AgencyAffiBranch_BranchId on AgencyAffiBranch (BranchId);
create Index Idx_AgencyAffiBranch_AgencyId on AgencyAffiBranch (AgencyId);
create Index Idx_Company_RegionId on Company (RegionId);
create Index Idx_CompanyInvInfo_ICompanyId on CompanyInvoiceInfo (InvoiceCompanyId);
create Index Idx_CompanyInvInfo_CompanyId on CompanyInvoiceInfo (CompanyId);
create Index Idx_CompanyAddress_RegionId on CompanyAddress (RegionId);
create Index Idx_CompanyAddress_CompanyId on CompanyAddress (CompanyId);
create Index Idx_CompanyLoginPicture_CpyId on CompanyLoginPicture (CompanyId);
create Index Idx_Branchstrategy_BranchId on Branchstrategy (BranchId);
create Index Idx_RegPersonRelation_BranchId on RegionPersonnelRelation (BranchId);
create Index Idx_RegPersonRelation_SaleReId on RegionPersonnelRelation (SalesRegionId);
create Index Idx_RegPersonRelation_PersonId on RegionPersonnelRelation (PersonnelId);
create Index Idx_RegMarketDptRel_BranchId on RegionMarketDptRelation (BranchId);
create Index Idx_RegMarketDptRel_SalesRegId on RegionMarketDptRelation (SalesRegionId);
create Index Idx_RegMarketDptRel_MDepartId on RegionMarketDptRelation (MarketDepartmentId);
create Index Idx_MarketDpPersonRel_BranchId on MarketDptPersonnelRelation (BranchId);
create Index Idx_MarketDPersonRel_MDepartId on MarketDptPersonnelRelation (MarketDepartmentId);
create Index Idx_MarketDpPersonRel_PersonId on MarketDptPersonnelRelation (PersonnelId);
create Index Idx_DealerDpPersonRel_MarketId on DealerMarketDptRelation (MarketId);
create Index Idx_DealerDpPersonRel_DealerId on DealerMarketDptRelation (DealerId);
create Index Idx_DealerDpPersonRel_BranchId on DealerMarketDptRelation (BranchId);
create Index Idx_LCompanySerRange_BranchId on LogisticCompanyServiceRange (BranchId);
create Index Idx_LCompanyRange_LCompanyId on LogisticCompanyServiceRange (LogisticCompanyId);
create Index Idx_MarketingDepart_BranchId on MarketingDepartment (BranchId);
create Index Idx_MarketingDepart_PSCId on MarketingDepartment (PartsSalesCategoryId);
create Index Idx_MarketingDepart_RegionId on MarketingDepartment (RegionId);
create Index Idx_RespUnitBranch_RespUnitId on ResponsibleUnitBranch (ResponsibleUnitId);
create Index Idx_RespUnitBranch_BranchId on ResponsibleUnitBranch (BranchId);
create Index Idx_RespUnitProDtl_RespUnitId on ResponsibleUnitProductDetail (ResponsibleUnitId);
create Index Idx_RespUnitProDtl_ProductId on ResponsibleUnitProductDetail (ProductId);
create Index Idx_SalesCenter_PSCategoryId on SalesCenterstrategy (PartsSalesCategoryId);
create Index Idx_SalesRegion_BranchId on SalesRegion (BranchId);
create Index Idx_SalesRegion_PSCategoryId on SalesRegion (PartsSalesCategoryId);
--多级审核相关
create Index Idx_RegionDealerList_DealerId on RegionDealerList (DealerId);
create Index Idx_RegionDealerList_RegConId on RegionDealerList (RegionConfugurationId);
create Index Idx_MultiLAuditCfg_BranchId on MultiLevelAuditConfig (BranchId);
create Index Idx_MultiLAuditCfg_BusinObjId on MultiLevelAuditConfig (BusinessObjectId);
create Index Idx_MultiLAuditCfgDtl_MLAdtCId on MultiLevelAuditConfigDetail (MultiLevelAuditConfigId);
create Index Idx_MultiLAuditCfgDtl_AuHSetId on MultiLevelAuditConfigDetail (AuditHierarchySettingId);
create Index Idx_MultiLAuditCfgDtl_IPAStaId on MultiLevelAuditConfigDetail (InitialPostAuditStatusId);
create Index Idx_MultiLAuditCfgDtl_APAStaId on MultiLevelAuditConfigDetail (ApprovedPostAuditStatusId);
create Index Idx_MultiLAuditCfgDtl_DPAStaId on MultiLevelAuditConfigDetail (DisapprovedPostAuditStatusId);
create Index Idx_AudRoleAffiPercon_PersonId on AuditRoleAffiPerconnel (PersonnelId);
create Index Idx_AudRoleAffiPercon_AHiSetId on AuditRoleAffiPerconnel (AuditHierarchySettingId);
create Index Idx_RegRoleCfg_AHSettingId on RegionalRoleConfig (AuditHierarchySettingId);
create Index Idx_RegRoleCfg_RegConfugueId on RegionalRoleConfig (RegionConfugurationId);
create Index Idx_PostAuditStatus_AHSetId on PostAuditStatus (AuditHierarchySettingId);
create Index Idx_ClaimTemplate_BranchId on ClaimTemplate (BranchId);
create Index Idx_ClaimTemplate_FaultyPartId on ClaimTemplate (FaultyPartsId);
create Index Idx_ClaimTempMaDtl_CTempIDtlId on ClaimTemplateMaterialDetail (ClaimTemplateItemDetailId);
create Index Idx_ClaimTempMaDtl_SparePartId on ClaimTemplateMaterialDetail (SparePartId);
create Index Idx_ClaimTemItemDtl_ClaimTemId on ClaimTemplateItemDetail (ClaimTemplateId);
create Index Idx_ClaimTemItemDtl_RepItemId on ClaimTemplateItemDetail (RepairItemId);
----------------------------------------------------------------------------------------------------------------------------------------

--车辆信息
create Index Idx_ChassisInfo_ResUnitId on ChassisInformation (ResponsibleUnitId);
create Index Idx_ChassisInfo_VCategoryId on ChassisInformation (VehicleCategoryId);
create Index Idx_ChassisInfo_ProductId on ChassisInformation (ProductId);

create Index Idx_VehInfo_ResUnitId on VehicleInformation (ResponsibleUnitId);
create Index Idx_VehInfo_VCategoryId on VehicleInformation (VehicleCategoryId);
create Index Idx_VehInfo_ProductId on VehicleInformation (ProductId);
create Index Idx_VehInfo_EngineModelId on VehicleInformation (EngineModelId);

create Index Idx_VehiExtenInfo_VehicleId on VehicleExtensionInformation (VehicleId);
create Index Idx_VehiExtenInfo_ProblemId on VehicleExtensionInformation (ProblemId);
--经销商相关
create Index Idx_SubDealer_DealerId on SubDealer (DealerId);

create Index Idx_SubCRela_ParChanelId on SubChannelRelation (ParentChanelId);
create Index Idx_SubCRela_SubChanelId on SubChannelRelation (SubChanelId);

create Index Idx_PersSubDea_CompanyId on PersonSubDealer (CompanyId);
create Index Idx_PersSubDea_PersonId on PersonSubDealer (PersonId);
create Index Idx_PersSubDea_SubDealerId on PersonSubDealer (SubDealerId);

create Index Idx_DeaKeyPosi_BranchId on DealerKeyPosition (BranchId);

create Index Idx_DeaKeyEmploy_DealerId on DealerKeyEmployee (DealerId);
create Index Idx_DeaKeyEmploy_PSCateId on DealerKeyEmployee (PartsSalesCategoryId);
create Index Idx_DeaKeyEmploy_KeyPosiId on DealerKeyEmployee (KeyPositionId);

create Index Idx_DeaKeyEHis_RecordId on DealerKeyEmployeeHistory (RecordId);
create Index Idx_DeaKeyEHis_PSCateId on DealerKeyEmployeeHistory (PartsSalesCategoryId);
create Index Idx_DeaKeyEHis_DealerId on DealerKeyEmployeeHistory (DealerId);
create Index Idx_DeaKeyEHis_KeyPositionId on DealerKeyEmployeeHistory (KeyPositionId);

create Index Idx_DeaSInfo_BranchId on DealerServiceInfo (BranchId);
create Index Idx_DeaSInfo_PSCateId on DealerServiceInfo (PartsSalesCategoryId);
create Index Idx_DeaSInfo_MDeptId on DealerServiceInfo (MarketingDepartmentId);
create Index Idx_DeaSInfo_DealerId on DealerServiceInfo (DealerId);
create Index Idx_DeaSInfo_GradeCId on DealerServiceInfo (GradeCoefficientId);
create Index Idx_DeaSInfo_UPWarehouseId on DealerServiceInfo (UsedPartsWarehouseId);
create Index Idx_DeaSInfo_PMFeeGradeId on DealerServiceInfo (PartsManagingFeeGradeId);
create index Idx_DeaSInfo_ChannelCapaId on DealerServiceInfo (ChannelCapabilityId);
create index Idx_DeaSInfo_WarehouseId on DealerServiceInfo (WarehouseId);
create index Idx_DeaSInfo_OutFeeGradeId on DealerServiceInfo (OutFeeGradeId);

create index Idx_DeaSInfoHis_RecordId on DealerServiceInfoHistory (RecordId);
create index Idx_DeaSInfoHis_PSCateId on DealerServiceInfoHistory (PartsSalesCategoryId);
create index Idx_DeaSInfoHis_DealerId on DealerServiceInfoHistory (DealerId);
create index Idx_DeaSInfoHis_BranchId on DealerServiceInfoHistory (BranchId);
create index Idx_DeaSInfoHis_MarketDeptId on DealerServiceInfoHistory (MarketingDepartmentId);
create index Idx_DeaSInfoHis_GradeCoeffId on DealerServiceInfoHistory (GradeCoefficientId);
create index Idx_DeaSInfoHis_UPWarehouseId on DealerServiceInfoHistory (UsedPartsWarehouseId);
create index Idx_DeaSInfoHis_OutFeeGradeId on DealerServiceInfoHistory (OutFeeGradeId);
create index Idx_DeaSInfoHis_ChaCapaId on DealerServiceInfoHistory (ChannelCapabilityId);
create index Idx_DeaSInfoHis_WarehouseId on DealerServiceInfoHistory (WarehouseId);
create index Idx_DeaSInfoHis_PMFeeGradeId on DealerServiceInfoHistory (PartsManagingFeeGradeId);

create index Idx_DeaHistory_RecordId on DealerHistory (RecordId);

create index Idx_DSExtHistory_RecordId on DealerServiceExtHistory (RecordId);

create index Idx_DBusiPermit_DealerId on DealerBusinessPermit (DealerId);
create index Idx_DBusiPermit_SPLineId on DealerBusinessPermit (ServiceProductLineId);
create index Idx_DBusiPermit_SFGradeId on DealerBusinessPermit (ServiceFeeGradeId);
create index Idx_DBusiPermit_BranchId on DealerBusinessPermit (BranchId);

create index Idx_DBusiPermitHis_DealerId on DealerBusinessPermitHistory (DealerId);
create index Idx_DBusiPermitHis_DBPermitId on DealerBusinessPermitHistory (DealerBusinessPermitId);
create index Idx_DBusiPermitHis_SPLineId on DealerBusinessPermitHistory (ServiceProductLineId);
create index Idx_DBusiPermitHis_BranchId on DealerBusinessPermitHistory (BranchId);
create index Idx_DBusiPermitHis_SFGradeId on DealerBusinessPermitHistory (ServiceFeeGradeId);
--网络规划相关
create index Idx_CPDetail_ParentId on ChannelPlanDetail (ParentId);
create index Idx_CPDetail_CCbilityId on ChannelPlanDetail (ChannelCapabilityId);
create index Idx_CPDetail_RegionId on ChannelPlanDetail (RegionId);

create index Idx_CPlan_BrandId on ChannelPlan (BrandId);
create index Idx_CPlanHis_ChannelPlanId on ChannelPlanHistory (ChannelPlanId);
create index Idx_CPlanHis_BrandId on ChannelPlanHistory (BrandId);
create index Idx_CPHisDetail_ParentId on ChannelPlanHistoryDetail (ParentId);
create index Idx_CPHisDetail_CCapaId on ChannelPlanHistoryDetail (ChannelCapabilityId);
create index Idx_CPHisDetail_RegionId on ChannelPlanHistoryDetail (RegionId);

--内部领用管理
create index Idx_InAcquBill_WarehouseId on InternalAcquisitionBill (WarehouseId);
create index Idx_InAcquBill_BranchId on InternalAcquisitionBill (BranchId);
create index Idx_InAcquBill_DeptId on InternalAcquisitionBill (DepartmentId);

create index Idx_InAcquDetail_InAcquBillId on InternalAcquisitionDetail (InternalAcquisitionBillId);
create index Idx_InAcquDetail_SparePartId on InternalAcquisitionDetail (SparePartId);

create index Idx_InAlloBill_BranchId on InternalAllocationBill (BranchId);
create index Idx_InAlloBill_WarehouseId on InternalAllocationBill (WarehouseId);
create index Idx_InAlloBill_DepartmentId on InternalAllocationBill (DepartmentId);
create index Idx_InAlloDetail_InAllBillId on InternalAllocationDetail (InternalAllocationBillId);
create index Idx_InAlloDetail_SparePartId on InternalAllocationDetail (SparePartId);
create index Idx_DeptInfo_BranchId on DepartmentInformation (BranchId);
--配件采购管理
create index Idx_SupShipOrder_PSupplierId on SupplierShippingOrder (PartsSupplierId);
create index Idx_SupShipOrder_PSCateId on SupplierShippingOrder (PartsSalesCategoryId);
create index Idx_SupShipOrder_PPurOrderId on SupplierShippingOrder (PartsPurchaseOrderId);
create index Idx_SupShipOrder_BranchId on SupplierShippingOrder (BranchId);
create index Idx_SupShipOrder_ReWarehouseId on SupplierShippingOrder (ReceivingWarehouseId);
create index Idx_SupShipOrder_ReCompanyId on SupplierShippingOrder (ReceivingCompanyId);
create index Idx_SupShipOrder_OriRequBillId on SupplierShippingOrder (OriginalRequirementBillId);

create index Idx_SupShipDetail_SparePartId on SupplierShippingDetail (SparePartId);
create index Idx_SShipDetail_SupShipOrderId on SupplierShippingDetail (SupplierShippingOrderId);

create index Idx_PPOrderDetail_SparePartId on PartsPurchaseOrderDetail (SparePartId);
create index Idx_PPOrderDetail_PPOrderId on PartsPurchaseOrderDetail (PartsPurchaseOrderId);

create index Idx_PPPlan_BranchId on PartsPurchasePlan (BranchId);
create index Idx_PPPlan_WarehouseId on PartsPurchasePlan (WarehouseId);

create index Idx_PPPDetail_PPlanId on PartsPurchasePlanDetail (PurchasePlanId);
create index Idx_PPPDetail_SupId on PartsPurchasePlanDetail (SuplierId);
create index Idx_PPPDetail_SparePartId on PartsPurchasePlanDetail (SparePartId);

create index Idx_PPurOrder_BranchId on PartsPurchaseOrder (BranchId);
create index Idx_PPurOrder_PSCateId on PartsPurchaseOrder (PartsSalesCategoryId);
create index Idx_PPurOrder_WarehouseId on PartsPurchaseOrder (WarehouseId);
create index Idx_PPurOrder_PartsSupId on PartsPurchaseOrder (PartsSupplierId);
create index Idx_PPurOrder_ReceCompanyId on PartsPurchaseOrder (ReceivingCompanyId);
create index Idx_PPurOrder_OriRequiBillId on PartsPurchaseOrder (OriginalRequirementBillId);
create index Idx_PPurOrder_PPurOrderTypeId on PartsPurchaseOrder (PartsPurchaseOrderTypeId);

create index Idx_PPurRetnOrder_BranchId on PartsPurReturnOrder (BranchId);
create index Idx_PPurRetnOrder_PPurOrderId on PartsPurReturnOrder (PartsPurchaseOrderId);
create index Idx_PPurRetnOrder_WarehouseId on PartsPurReturnOrder (WarehouseId);
create index Idx_PPurRetnOrder_PartsSuprId on PartsPurReturnOrder (PartsSupplierId);
create index Idx_PPurRetnOrder_PSCategoryId on PartsPurReturnOrder (PartsSalesCategoryId);

create index Idx_PPRODetail_SparePartId on PartsPurReturnOrderDetail (SparePartId);
create index Idx_PPRODetail_PPurReOrderId on PartsPurReturnOrderDetail (PartsPurReturnOrderId);
--配件采购基础数据

create index Idx_TempSup_BranchId on TemporarySupplier (BranchId);

create index Idx_BSRelation_BranchId on BranchSupplierRelation (BranchId);
create index Idx_BSRelation_PMCostGraId on BranchSupplierRelation (PartsManagementCostGradeId);
create index Idx_BSRelation_PSCateId on BranchSupplierRelation (PartsSalesCategoryId);
create index Idx_BSRelation_SupId on BranchSupplierRelation (SupplierId);

create index Idx_PSRelation_BranchId on PartsSupplierRelation (BranchId);
create index Idx_PSRelation_PSCateId on PartsSupplierRelation (PartsSalesCategoryId);
create index Idx_PSRelation_SupId on PartsSupplierRelation (SupplierId);
create index Idx_PSRelation_PartId on PartsSupplierRelation (PartId);

create index Idx_PPPricing_BranchId on PartsPurchasePricing (BranchId);
create index Idx_PPPricing_PSCateId on PartsPurchasePricing (PartsSalesCategoryId);
create index Idx_PPPricing_PartId on PartsPurchasePricing (PartId);
create index Idx_PPPricing_PartsSupId on PartsPurchasePricing (PartsSupplierId);

create index Idx_PPPriChange_BranchId on PartsPurchasePricingChange (BranchId);
create index Idx_PPPriChange_PSCateId on PartsPurchasePricingChange (PartsSalesCategoryId);

create index Idx_PPPriDetail_ParentId on PartsPurchasePricingDetail (ParentId);
create index Idx_PPPriDetail_PartId on PartsPurchasePricingDetail (PartId);
create index Idx_PPPriDetail_SupId on PartsPurchasePricingDetail (SupplierId);

create index Idx_PPOrderType_BranchId on PartsPurchaseOrderType (BranchId);
create index Idx_PPOrderType_PSCateId on PartsPurchaseOrderType (PartsSalesCategoryId);
--内部领用结算
create index Idx_PReqSBill_BranchId on PartsRequisitionSettleBill (BranchId);
create index Idx_PReqSBill_PSCateId on PartsRequisitionSettleBill (PartsSalesCategoryId);
create index Idx_PReqSBill_DetId on PartsRequisitionSettleBill (DepartmentId);

create index Idx_PReqSRef_SourceId on PartsRequisitionSettleRef (SourceId);
create index Idx_PReqSRef_PReqSBillId on PartsRequisitionSettleRef (PartsRequisitionSettleBillId);

create index Idx_PReqSDetail_SparePartId on PartsRequisitionSettleDetail (SparePartId);
create index Idx_PReqSDetail_PReqSBillId on PartsRequisitionSettleDetail (PartsRequisitionSettleBillId);
--配件采购结算管理
create index Idx_PPSBill_WarehouseId on PartsPurchaseSettleBill (WarehouseId);
create index Idx_PPSBill_PSCateId on PartsPurchaseSettleBill (PartsSalesCategoryId);
create index Idx_PPSBill_PartsSupId on PartsPurchaseSettleBill (PartsSupplierId);
create index Idx_PPSBill_BranchId on PartsPurchaseSettleBill (BranchId);
create index Idx_PPSBill_OffSBillId on PartsPurchaseSettleBill (OffsettedSettlementBillId);

create index Idx_PPSRef_SourceId on PartsPurchaseSettleRef (SourceId);
create index Idx_PPSRef_PPSBillId on PartsPurchaseSettleRef (PartsPurchaseSettleBillId);

create index Idx_PPSDetail_SparePartId on PartsPurchaseSettleDetail (SparePartId);
create index Idx_PPSDetail_PPSBillId on PartsPurchaseSettleDetail (PartsPurchaseSettleBillId);

create index Idx_PPRtnSBill_WarehouseId on PartsPurchaseRtnSettleBill (WarehouseId);
create index Idx_PPRtnSBill_OffSBillId on PartsPurchaseRtnSettleBill (OffsettedSettlementBillId);
create index Idx_PPRtnSBill_PSCateId on PartsPurchaseRtnSettleBill (PartsSalesCategoryId);
create index Idx_PPRtnSBill_BranchId on PartsPurchaseRtnSettleBill (BranchId);
create index Idx_PPRtnSBill_PartsSupId on PartsPurchaseRtnSettleBill (PartsSupplierId);

create index Idx_PPRtnSRef_SourceId on PartsPurchaseRtnSettleRef (SourceId);
create index Idx_PPRtnSRef_PPRtnSBillId on PartsPurchaseRtnSettleRef (PartsPurchaseRtnSettleBillId);

create index Idx_PPRtnSDetail_SparePartId on PartsPurchaseRtnSettleDetail (SparePartId);
create index Idx_PPRtnSDetail_PPRtnSBillId on PartsPurchaseRtnSettleDetail (PartsPurchaseRtnSettleBillId);
--配件外采申请管理
create index Idx_POPChange_BranchId on PartsOuterPurchaseChange (BranchId);
create index Idx_POPChange_PSCategoryrId on PartsOuterPurchaseChange (PartsSalesCategoryrId);
create index Idx_POPChange_CCompanyId on PartsOuterPurchaseChange (CustomerCompanyId);
create index Idx_POPChange_SourceId on PartsOuterPurchaseChange (SourceId);

create index Idx_POPlist_PartsId on PartsOuterPurchaselist (PartsId);
create index Idx_POPlist_POPChangeId on PartsOuterPurchaselist (PartsOuterPurchaseChangeId);
--配件销售价格信息
create index Idx_COPGrade_PSOrderTypeId on CustomerOrderPriceGrade (PartsSalesOrderTypeId);
create index Idx_COPGrade_PSCategoryId on CustomerOrderPriceGrade (PartsSalesCategoryId);
create index Idx_COPGrade_CCompanyId on CustomerOrderPriceGrade (CustomerCompanyId);
create index Idx_COPGrade_BranchId on CustomerOrderPriceGrade (BranchId);

create index Idx_COPGradeHis_PSOrderTypeId on CustomerOrderPriceGradeHistory (PartsSalesOrderTypeId);
create index Idx_COPGradeHis_PSCateId on CustomerOrderPriceGradeHistory (PartsSalesCategoryId);
create index Idx_COPGradeHis_CCompanyId on CustomerOrderPriceGradeHistory (CustomerCompanyId);
create index Idx_COPGradeHis_BranchId on CustomerOrderPriceGradeHistory (BranchId);

create index Idx_PSTPrice_BranchId on PartsSpecialTreatyPrice (BranchId);
create index Idx_PSTPrice_PSCategoryId on PartsSpecialTreatyPrice (PartsSalesCategoryId);
create index Idx_PSTPrice_SparePartId on PartsSpecialTreatyPrice (SparePartId);
create index Idx_PSTPrice_CompanyId on PartsSpecialTreatyPrice (CompanyId);

create index Idx_PSPHistory_BranchId on PartsSpecialPriceHistory (BranchId);
create index Idx_PSPHistory_PSTPriceId on PartsSpecialPriceHistory (PartsSpecialTreatyPriceId);
create index Idx_PSPHistory_PSCategoryId on PartsSpecialPriceHistory (PartsSalesCategoryId);
create index Idx_PSPHistory_SparePartId on PartsSpecialPriceHistory (SparePartId);
create index Idx_PSPHistory_CompanyId on PartsSpecialPriceHistory (CompanyId);

create index Idx_PSalesPrice_BranchId on PartsSalesPrice (BranchId);
create index Idx_PSalesPrice_PSCategoryId on PartsSalesPrice (PartsSalesCategoryId);
create index Idx_PSalesPrice_SparePartId on PartsSalesPrice (SparePartId);

create index Idx_PSPHistory_BranchId on PartsSalesPriceHistory (BranchId);
create index Idx_PSPHistory_PSCategoryId on PartsSalesPriceHistory (PartsSalesCategoryId);
create index Idx_PSPHistory_SparePartId on PartsSalesPriceHistory (SparePartId);

create index Idx_PSPChange_BranchId on PartsSalesPriceChange (BranchId);
create index Idx_PSPChange_PSCategoryId on PartsSalesPriceChange (PartsSalesCategoryId);

create index Idx_PSPCDetail_ParentId on PartsSalesPriceChangeDetail (ParentId);
create index Idx_PSPCDetail_SparePartId on PartsSalesPriceChangeDetail (SparePartId);

create index Idx_PSCategory_BranchId on PartsSalesCategory (BranchId);

create index Idx_PSOrderType_BranchId on PartsSalesOrderType (BranchId);
create index Idx_PSOrderType_PSCategoryId on PartsSalesOrderType (PartsSalesCategoryId);

create index Idx_PRGPrice_BranchId on PartsRetailGuidePrice (BranchId);
create index Idx_PRGPrice_PSCategoryId on PartsRetailGuidePrice (PartsSalesCategoryId);
create index Idx_PRGPrice_SparePartId on PartsRetailGuidePrice (SparePartId);

create index Idx_PRGPriceHis_BranchId on PartsRetailGuidePriceHistory (BranchId);
create index Idx_PRGPriceHis_PSCategoryId on PartsRetailGuidePriceHistory (PartsSalesCategoryId);
create index Idx_PRGPriceHis_SparePartId on PartsRetailGuidePriceHistory (SparePartId);
--配件销售业务管理
create index Idx_SCSPlan_SClassStationId on SecondClassStationPlan (SecondClassStationId);
create index Idx_SCSPlan_FClassStationId on SecondClassStationPlan (FirstClassStationId);
create index Idx_SCSPlan_PSCategoryId on SecondClassStationPlan (PartsSalesCategoryId);

create index Idx_SCSPlanDetail_SCSPlanId on SecondClassStationPlanDetail (SecondClassStationPlanId);
create index Idx_SCSPlanDetail_SparePartId on SecondClassStationPlanDetail (SparePartId);

create index Idx_PSCenterLink_CompanyId on PersonSalesCenterLink (CompanyId);
create index Idx_PSCenterLink_PSCategoryId on PersonSalesCenterLink (PartsSalesCategoryId);

create index Idx_ADRelation_BranchId on AgencyDealerRelation (BranchId);
create index Idx_ADRelation_PSOrderTypeId on AgencyDealerRelation (PartsSalesOrderTypeId);
create index Idx_ADRelation_AgencyId on AgencyDealerRelation (AgencyId);
create index Idx_ADRelation_DealerId on AgencyDealerRelation (DealerId);

create index Idx_CustInfo_SCompanyId on CustomerInformation (SalesCompanyId);
create index Idx_CustInfo_CCompanyId on CustomerInformation (CustomerCompanyId);

create index Idx_DPRetOrder_BranchId on DealerPartsRetailOrder (BranchId);
create index Idx_DPRetOrder_PSCateId on DealerPartsRetailOrder (PartsSalesCategoryId);
create index Idx_DPRetOrder_ParentChanelId on DealerPartsRetailOrder (ParentChanelId);
create index Idx_DPRetOrder_DealerId on DealerPartsRetailOrder (DealerId);
create index Idx_DPRetOrder_SubDealerId on DealerPartsRetailOrder (SubDealerId);

create index Idx_DPSReturnBill_BranchId on DealerPartsSalesReturnBill (BranchId);
create index Idx_DPSReturnBill_SCategoryId on DealerPartsSalesReturnBill (SalesCategoryId);
create index Idx_DPSReturnBill_DealerId on DealerPartsSalesReturnBill (DealerId);

create index Idx_DRRetBillDetail_PartsId on DealerRetailReturnBillDetail (PartsId);
create index Idx_DRRBillDetail_DPSRetBillId on DealerRetailReturnBillDetail (DealerPartsSalesReturnBillId);

create index Idx_DRODetail_PartsId on DealerRetailOrderDetail (PartsId);
create index Idx_DRODetail_DPROrderId on DealerRetailOrderDetail (DealerPartsRetailOrderId);

create index Idx_PSOrder_BranchId on PartsSalesOrder (BranchId);
create index Idx_PSOrder_SUOCompanyId on PartsSalesOrder (SalesUnitOwnerCompanyId);
create index Idx_PSOrder_OriRequiBillId on PartsSalesOrder (OriginalRequirementBillId);
create index Idx_PSOrder_SCategoryId on PartsSalesOrder (SalesCategoryId);
create index Idx_PSOrder_SUnitId on PartsSalesOrder (SalesUnitId);

create index Idx_PSOrder_CAccountId on PartsSalesOrder (CustomerAccountId);
create index Idx_PSOrder_IRCompanyId on PartsSalesOrder (InvoiceReceiveCompanyId);
create index Idx_PSOrder_IRSaleCateId on PartsSalesOrder (InvoiceReceiveSaleCateId);
create index Idx_PSOrder_SubmitCompanyId on PartsSalesOrder (SubmitCompanyId);
create index Idx_PSOrder_FClassStationId on PartsSalesOrder (FirstClassStationId);
create index Idx_PSOrder_PSOrderTypeId on PartsSalesOrder (PartsSalesOrderTypeId);
create index Idx_PSOrder_RCompanyId on PartsSalesOrder (ReceivingCompanyId);
create index Idx_PSOrder_RWarehouseId on PartsSalesOrder (ReceivingWarehouseId);
create index Idx_PSOrder_SourceBillId on PartsSalesOrder (SourceBillId);

create index Idx_PSOProcess_OriSalesOrderId on PartsSalesOrderProcess (OriginalSalesOrderId);

create index Idx_PSOPDetail_WarehouseId on PartsSalesOrderProcessDetail (WarehouseId);
create index Idx_PSOPDetail_PSOPId on PartsSalesOrderProcessDetail (PartsSalesOrderProcessId);
create index Idx_PSOPDetail_SupCompanyId on PartsSalesOrderProcessDetail (SupplierCompanyId);
create index Idx_PSOPDetail_SparePartId on PartsSalesOrderProcessDetail (SparePartId);
create index Idx_PSOPDetail_NewPartId on PartsSalesOrderProcessDetail (NewPartId);
create index Idx_PSOPDetail_SupWarehouseId on PartsSalesOrderProcessDetail (SupplierWarehouseId);

create index Idx_PSODetail_PSOrderId on PartsSalesOrderDetail (PartsSalesOrderId);
create index Idx_PSODetail_SparePartId on PartsSalesOrderDetail (SparePartId);

create index Idx_PSRBill_ReturnCompanyId on PartsSalesReturnBill (ReturnCompanyId);
create index Idx_PSRBill_IRCompanyId on PartsSalesReturnBill (InvoiceReceiveCompanyId);
create index Idx_PSRBill_FClassStationId on PartsSalesReturnBill (FirstClassStationId);
create index Idx_PSRBill_SubmitCompanyId on PartsSalesReturnBill (SubmitCompanyId);
create index Idx_PSRBill_SalesUnitId on PartsSalesReturnBill (SalesUnitId);
create index Idx_PSRBill_WarehouseId on PartsSalesReturnBill (WarehouseId);
create index Idx_PSRBill_SUOCompanyId on PartsSalesReturnBill (SalesUnitOwnerCompanyId);
create index Idx_PSRBill_CAccountId on PartsSalesReturnBill (CustomerAccountId);
create index Idx_PSRBill_PSOrderId on PartsSalesReturnBill (PartsSalesOrderId);
create index Idx_PSRBill_BranchId on PartsSalesReturnBill (BranchId);
create index Idx_PSRBill_OriReqBillId on PartsSalesReturnBill (OriginalRequirementBillId);

create index Idx_PSRBillDetail_PSRBillId on PartsSalesReturnBillDetail (PartsSalesReturnBillId);
create index Idx_PSRBillDetail_SparePartId on PartsSalesReturnBillDetail (SparePartId);

create index Idx_PROrder_CAccountId on PartsRetailOrder (CustomerAccountId);
create index Idx_PROrder_RCCompanyId on PartsRetailOrder (RetailCustomerCompanyId);
create index Idx_PROrder_SUOwnerCompanyId on PartsRetailOrder (SalesUnitOwnerCompanyId);
create index Idx_PROrder_BranchId on PartsRetailOrder (BranchId);
create index Idx_PROrder_WarehouseId on PartsRetailOrder (WarehouseId);
create index Idx_PROrder_SalesUnitId on PartsRetailOrder (SalesUnitId);

create index Idx_PROrderDetail_PROrderId on PartsRetailOrderDetail (PartsRetailOrderId);
create index Idx_PROrderDetail_SparePartId on PartsRetailOrderDetail (SparePartId);

create index Idx_PRRBill_SourceId on PartsRetailReturnBill (SourceId);
create index Idx_PRRBill_RCCompanyId on PartsRetailReturnBill (RetailCustomerCompanyId);
create index Idx_PRRBill_CAccountId on PartsRetailReturnBill (CustomerAccountId);
create index Idx_PRRBill_BranchId on PartsRetailReturnBill (BranchId);
create index Idx_PRRBill_SUOwnerCompanyId on PartsRetailReturnBill (SalesUnitOwnerCompanyId);
create index Idx_PRRBill_SalesUnitId on PartsRetailReturnBill (SalesUnitId);
create index Idx_PRRBill_WarehouseId on PartsRetailReturnBill (WarehouseId);

create index Idx_PRRBillDetail_PRRBillId on PartsRetailReturnBillDetail (PartsRetailReturnBillId);
create index Idx_PRRBillDetail_SparePartId on PartsRetailReturnBillDetail (SparePartId);

create index Idx_SalesUnit_BranchId on SalesUnit (BranchId);
create index Idx_SalesUnit_OwnerCompanyId on SalesUnit (OwnerCompanyId);
create index Idx_SalesUnit_PSCategoryId on SalesUnit (PartsSalesCategoryId);
create index Idx_SalesUnit_AccountGroupId on SalesUnit (AccountGroupId);

create index Idx_SUAPersonnel_PersonnelId on SalesUnitAffiPersonnel (PersonnelId);
create index Idx_SUAPersonnel_SalesUnitId on SalesUnitAffiPersonnel (SalesUnitId);

create index Idx_SUAWarehouse_WarehouseId on SalesUnitAffiWarehouse (WarehouseId);
create index Idx_SUAWarehouse_SalesUnitId on SalesUnitAffiWarehouse (SalesUnitId);

create index Idx_SUAWhouseHis_WarehouseId on SalesUnitAffiWhouseHistory (WarehouseId);
create index Idx_SUAWhouseHis_SalesUnitId on SalesUnitAffiWhouseHistory (SalesUnitId);
--配件销售结算管理
create index Idx_PRChDetail_BranchId on PartsRebateChangeDetail (BranchId);
create index Idx_PRChDetail_PRAccountId on PartsRebateChangeDetail (PartsRebateAccountId);
create index Idx_PRChDetail_CCompanyId on PartsRebateChangeDetail (CustomerCompanyId);
create index Idx_PRChDetail_SourceId on PartsRebateChangeDetail (SourceId);
create index Idx_PRChDetail_AccountGroupId on PartsRebateChangeDetail (AccountGroupId);

create index Idx_PRApp_BranchId on PartsRebateApplication (BranchId);
create index Idx_PRApp_CCompanyId on PartsRebateApplication (CustomerCompanyId);
create index Idx_PRApp_PSCategoryId on PartsRebateApplication (PartsSalesCategoryId);
create index Idx_PRApp_AccountGroupId on PartsRebateApplication (AccountGroupId);
create index Idx_PRApp_PRebateTypeId on PartsRebateApplication (PartsRebateTypeId);

create index Idx_PRebateType_PSCategoryId on PartsRebateType (PartsSalesCategoryId);

create index Idx_PRebateAccount_BranchId on PartsRebateAccount (BranchId);
create index Idx_PRebateAccount_AGroupId on PartsRebateAccount (AccountGroupId);
create index Idx_PRebateAccount_CCompanyId on PartsRebateAccount (CustomerCompanyId);

create index Idx_PSSettle_SalesCompanyId on PartsSalesSettlement (SalesCompanyId);
create index Idx_PSSettle_PSCategoryId on PartsSalesSettlement (PartsSalesCategoryId);
create index Idx_PSSettle_OffSettleBillId on PartsSalesSettlement (OffsettedSettlementBillId);
create index Idx_PSSettle_AccountGroupId on PartsSalesSettlement (AccountGroupId);
create index Idx_PSSettle_CAccountId on PartsSalesSettlement (CustomerAccountId);
create index Idx_PSSettle_CCompanyId on PartsSalesSettlement (CustomerCompanyId);

create index Idx_PSSRef_SourceId on PartsSalesSettlementRef (SourceId);
create index Idx_PSSRef_PSSettlementId on PartsSalesSettlementRef (PartsSalesSettlementId);

create index Idx_PSSDetail_SparePartId on PartsSalesSettlementDetail (SparePartId);
create index Idx_PSSDetail_PSSettlementId on PartsSalesSettlementDetail (PartsSalesSettlementId);

create index Idx_PSRtnSRef_SourceId on PartsSalesRtnSettlementRef (SourceId);
create index Idx_PSRtnSRef_PSRtnSId on PartsSalesRtnSettlementRef (PartsSalesRtnSettlementId);

create index Idx_PSRtnSettle_SalesCompanyId on PartsSalesRtnSettlement (SalesCompanyId);
create index Idx_PSRtnSettle_OffSBillId on PartsSalesRtnSettlement (OffsettedSettlementBillId);
create index Idx_PSRtnSettle_PSCategoryId on PartsSalesRtnSettlement (PartsSalesCategoryId);
create index Idx_PSRtnSettle_AccountGroupId on PartsSalesRtnSettlement (AccountGroupId);
create index Idx_PSRtnSettle_CAccountId on PartsSalesRtnSettlement (CustomerAccountId);
create index Idx_PSRtnSettle_CCompanyId on PartsSalesRtnSettlement (CustomerCompanyId);

create index Idx_PSRtnSDetail_SparePartId on PartsSalesRtnSettlementDetail (SparePartId);
create index Idx_PSRtnSDetail_PSRtnSId on PartsSalesRtnSettlementDetail (PartsSalesRtnSettlementId);


------------------------------------------------------------------------------------------------------------------------------------------------

--市场旧件处理
create index Idx_UPShipOrder_BranchId on UsedPartsShippingOrder (BranchId);
create index Idx_UPShipOrder_DWarehouseId on UsedPartsShippingOrder (DestinationWarehouseId);
create index Idx_UPShipOrder_PSCategoryId on UsedPartsShippingOrder (PartsSalesCategoryId);
create index Idx_UPShipOrder_DealerId on UsedPartsShippingOrder (DealerId);
create index Idx_UPShipOrder_LogisCompanyId on UsedPartsShippingOrder (LogisticCompanyId);

create index Idx_UPSDetail_BranchId on UsedPartsShippingDetail (BranchId);
create index Idx_UPSDetail_UPShipOrdId on UsedPartsShippingDetail (UsedPartsShippingOrderId);
create index Idx_UPSDetail_PSCategoryId on UsedPartsShippingDetail (PartsSalesCategoryId);
create index Idx_UPSDetail_UsedPartsId on UsedPartsShippingDetail (UsedPartsId);
create index Idx_UPSDetail_UPSupplierId on UsedPartsShippingDetail (UsedPartsSupplierId);
create index Idx_UPSDetail_UPWAreaId on UsedPartsShippingDetail (UsedPartsWarehouseAreaId);
create index Idx_UPSDetail_RsibleUnitId on UsedPartsShippingDetail (ResponsibleUnitId);
create index Idx_UPSDetail_FPSupplierId on UsedPartsShippingDetail (FaultyPartsSupplierId);
create index Idx_UPSDetail_FaultyPartsId on UsedPartsShippingDetail (FaultyPartsId);
create index Idx_UPSDetail_NewPartsId on UsedPartsShippingDetail (NewPartsId);
create index Idx_UPSDetail_NewPSupplierId on UsedPartsShippingDetail (NewPartsSupplierId);
create index Idx_UPSDetail_ClaimBillId on UsedPartsShippingDetail (ClaimBillId);
create index Idx_UPSDetail_SubDealerId on UsedPartsShippingDetail (SubDealerId);

create index Idx_UPLogLBill_DealerId on UsedPartsLogisticLossBill (DealerId);
create index Idx_UPLogLBill_UPShipOrderId on UsedPartsLogisticLossBill (UsedPartsShippingOrderId);
create index Idx_UPLogLBill_LogCompanyId on UsedPartsLogisticLossBill (LogisticCompanyId);

create index Idx_UPLogLsDil_BranchId on UsedPartsLogisticLossDetail (BranchId);
create index Idx_UPLogLsDil_UPLogLBillId on UsedPartsLogisticLossDetail (UsedPartsLogisticLossBillId);
create index Idx_UPLogLsDil_PSCategoryId on UsedPartsLogisticLossDetail (PartsSalesCategoryId);
create index Idx_UPLogLsDil_ClaimBillId on UsedPartsLogisticLossDetail (ClaimBillId);
create index Idx_UPLogLsDil_UsedPartsId on UsedPartsLogisticLossDetail (UsedPartsId);
create index Idx_UPLogLsDil_NewPSuppId on UsedPartsLogisticLossDetail (NewPartsSupplierId);
create index Idx_UPLogLsDil_UPSuppliId on UsedPartsLogisticLossDetail (UsedPartsSupplierId);
create index Idx_UPLogLsDil_FaultyPartsId on UsedPartsLogisticLossDetail (FaultyPartsId);
create index Idx_UPLogLsDil_FaultyPSId on UsedPartsLogisticLossDetail (FaultyPartsSupplierId);
create index Idx_UPLogLsDil_NewPartsId on UsedPartsLogisticLossDetail (NewPartsId);
create index Idx_UPLogLsDil_RespUnitId on UsedPartsLogisticLossDetail (ResponsibleUnitId);

create index Idx_UPDisInfor_UPCompanyId on UsedPartsDistanceInfor (UsedPartsCompanyId);
create index Idx_UPDisInfor_OUPWarehouseId on UsedPartsDistanceInfor (OriginUsedPartsWarehouseId);
create index Idx_UPDisInfor_UPWarehouseId on UsedPartsDistanceInfor (UsedPartsWarehouseId);
create index Idx_UPDisInfor_OriginCompanyId on UsedPartsDistanceInfor (OriginCompanyId);

create index Idx_UPRetPHistory_DealerId on UsedPartsReturnPolicyHistory (DealerId);
create index Idx_UPRetPHistory_UPSupplId on UsedPartsReturnPolicyHistory (UsedPartsSupplierId);
create index Idx_UPRetPHistory_PSCategoryId on UsedPartsReturnPolicyHistory (PartsSalesCategoryId);
create index Idx_UPRetPHistory_ClaimBillId on UsedPartsReturnPolicyHistory (ClaimBillId);
create index Idx_UPRetPHistory_ResponUnitId on UsedPartsReturnPolicyHistory (ResponsibleUnitId);
create index Idx_UPRetPHistory_UsedPartsId on UsedPartsReturnPolicyHistory (UsedPartsId);
create index Idx_UPRetPHistory_FtyPSuppliId on UsedPartsReturnPolicyHistory (FaultyPartsSupplierId);
create index Idx_UPRetPHistory_FtyPartsId on UsedPartsReturnPolicyHistory (FaultyPartsId);
create index Idx_UPRetPHistory_BranchId on UsedPartsReturnPolicyHistory (BranchId);
create index Idx_UPRetPHistory_NewPartsId on UsedPartsReturnPolicyHistory (NewPartsId);
create index Idx_UPRetPHistory_NewPSuppliId on UsedPartsReturnPolicyHistory (NewPartsSupplierId);

create index Idx_SsUPDisBill_DealerId on SsUsedPartsDisposalBill (DealerId);
create index Idx_SsUPDisBill_PSCategoryId on SsUsedPartsDisposalBill (PartsSalesCategoryId);
create index Idx_SsUPDisBill_BranchId on SsUsedPartsDisposalBill (BranchId);

create index Idx_SsUPDisDetail_BranchId on SsUsedPartsDisposalDetail (BranchId);
create index Idx_SsUPDisDetail_PSCategId on SsUsedPartsDisposalDetail (PartsSalesCategoryId);
create index Idx_SsUPDisDetail_SsUPDisBlId on SsUsedPartsDisposalDetail (SsUsedPartsDisposalBillId);
create index Idx_SsUPDisDetail_ClaimBillId on SsUsedPartsDisposalDetail (ClaimBillId);
create index Idx_SsUPDisDetail_UsedPartsId on SsUsedPartsDisposalDetail (UsedPartsId);
create index Idx_SsUPDisDetail_NewPSuppliId on SsUsedPartsDisposalDetail (NewPartsSupplierId);
create index Idx_SsUPDisDetail_UPSupplierId on SsUsedPartsDisposalDetail (UsedPartsSupplierId);
create index Idx_SsUPDisDetail_FaultyPartId on SsUsedPartsDisposalDetail (FaultyPartsId);
create index Idx_SsUPDisDetail_FtyPartsSId on SsUsedPartsDisposalDetail (FaultyPartsSupplierId);
create index Idx_SsUPDisDetail_NewPartsId on SsUsedPartsDisposalDetail (NewPartsId);
create index Idx_SsUPDisDetail_ResponUnitId on SsUsedPartsDisposalDetail (ResponsibleUnitId);

create index Idx_SsUPStorage_DealerId on SsUsedPartsStorage (DealerId);
create index Idx_SsUPStorage_UPSupplierId on SsUsedPartsStorage (UsedPartsSupplierId);
create index Idx_SsUPStorage_SubDealerId on SsUsedPartsStorage (SubDealerId);
create index Idx_SsUPStorage_ClaimBillId on SsUsedPartsStorage (ClaimBillId);
create index Idx_SsUPStorage_UsedPartsId on SsUsedPartsStorage (UsedPartsId);
create index Idx_SsUPStorage_PSCategoryId on SsUsedPartsStorage (PartsSalesCategoryId);
create index Idx_SsUPStorage_FtyPartsId on SsUsedPartsStorage (FaultyPartsId);
create index Idx_SsUPStorage_FtyPSuppliId on SsUsedPartsStorage (FaultyPartsSupplierId);
create index Idx_SsUPStorage_BranchId on SsUsedPartsStorage (BranchId);
create index Idx_SsUPStorage_ResponUnitId on SsUsedPartsStorage (ResponsibleUnitId);
create index Idx_SsUPStorage_NewPartsId on SsUsedPartsStorage (NewPartsId);
create index Idx_SsUPStorage_NewPSuppliId on SsUsedPartsStorage (NewPartsSupplierId);

--服务维修索赔
create index Idx_PQIReport_BranchId on PQIReport (BranchId);
create index Idx_PQIReport_DealerId on PQIReport (DealerId);
create index Idx_PQIReport_FaultyPartsId on PQIReport (FaultyPartsId);
create index Idx_PQIReport_VehicleId on PQIReport (VehicleId);

create index Idx_SupExAdjustBill_SupId on SupplierExpenseAdjustBill (SupplierId);
create index Idx_SupExAdjustBill_BranId on SupplierExpenseAdjustBill (BranchId);
create index Idx_SupExAdjustBill_RUnitId on SupplierExpenseAdjustBill (ResponsibleUnitId);
create index Idx_SupExAdjustBill_SourId on SupplierExpenseAdjustBill (SourceId);

create index Idx_PSaleCkOrder_BranchId on PreSaleCheckOrder (BranchId);
create index Idx_PSaleCkOrder_SPdtLineId on PreSaleCheckOrder (ServiceProductLineId);
create index Idx_PSaleCkOrder_PSCategoryId on PreSaleCheckOrder (PartsSalesCategoryId);
create index Idx_PSaleCkOrder_VehicleId on PreSaleCheckOrder (VehicleId);
create index Idx_PSaleCkOrder_ResponUnitId on PreSaleCheckOrder (ResponsibleUnitId);
create index Idx_PSaleCkOrder_DealerId on PreSaleCheckOrder (DealerId);
 
create index Idx_PreSCheckDetail_PSCheckId on PreSalesCheckDetail (PreSalesCheckId);
create index Idx_PreSCheckDetail_ItemId on PreSalesCheckDetail (ItemId);

create index Idx_PreSaleItem_BranchId on PreSaleItem (BranchId);
create index Idx_PreSaleItem_PSCategoryId on PreSaleItem (PartsSalesCategoryId);

create index Idx_SeTripCBill_DealerId on ServiceTripClaimBill (DealerId);
create index Idx_SeTripCBill_SeTripCAppId on ServiceTripClaimBill (ServiceTripClaimAppId);
create index Idx_SeTripCBill_MarkDepmtId on ServiceTripClaimBill (MarketingDepartmentId);
create index Idx_SeTripCBill_VehicleId on ServiceTripClaimBill (VehicleId);
create index Idx_SeTripCBill_BranchId on ServiceTripClaimBill (BranchId);
create index Idx_SeTripCBill_SePdtLineId on ServiceTripClaimBill (ServiceProductLineId);
create index Idx_SeTripCBill_RepairObjId on ServiceTripClaimBill (RepairObjectId);
create index Idx_SeTripCBill_RepCBillId on ServiceTripClaimBill (RepairClaimBillId);
create index Idx_SeTripCBill_AreaId on ServiceTripClaimBill (AreaId);
create index Idx_SeTripCBill_OneLelDealId on ServiceTripClaimBill (OneLevelDealerId);
create index Idx_SeTripCBill_RepairOrdId on ServiceTripClaimBill (RepairOrderId);
create index Idx_SeTripCBill_ClaimSuppId on ServiceTripClaimBill (ClaimSupplierId);
create index Idx_SeTripCBill_ResponUnitId on ServiceTripClaimBill (ResponsibleUnitId);

create index Idx_SeTCApplication_SRegionId on ServiceTripClaimApplication (SalesRegionId);
create index Idx_SeTCApplication_MtDeptId on ServiceTripClaimApplication (MarketDepartmentId);
create index Idx_SeTCApplication_PSCateId on ServiceTripClaimApplication (PartsSalesCategoryId);
create index Idx_SeTCApplication_DealerId on ServiceTripClaimApplication (DealerId);
create index Idx_SeTCApplication_RepObjId on ServiceTripClaimApplication (RepairObjectId);
create index Idx_SeTCApplication_FClStaId on ServiceTripClaimApplication (FirstClassStationId);
create index Idx_SeTCApplication_SePdtLId on ServiceTripClaimApplication (ServiceProductLineId);
create index Idx_SeTCApplication_BranchId on ServiceTripClaimApplication (BranchId);
create index Idx_SeTCApplication_VehicleId on ServiceTripClaimApplication (VehicleId);

create index Idx_ExAdjustmentBill_BranchId on ExpenseAdjustmentBill (BranchId);
create index Idx_ExAdjustmentBill_PSCateId on ExpenseAdjustmentBill (PartsSalesCategoryId);
create index Idx_ExAdjustmentBill_SourceId on ExpenseAdjustmentBill (SourceId);
create index Idx_ExAdjustmentBill_DealerId on ExpenseAdjustmentBill (DealerId);
create index Idx_ExAdjustmentBill_ResUnitId on ExpenseAdjustmentBill (ResponsibleUnitId);

create index Idx_RpClaimBill_RepairOrderId on RepairClaimBill (RepairOrderId);
create index Idx_RpClaimBill_RepairObjectId on RepairClaimBill (RepairObjectId);
create index Idx_RpClaimBill_ROrdFtReasonId on RepairClaimBill (RepairOrderFaultReasonId);
create index Idx_RpClaimBill_SalesRegionId on RepairClaimBill (SalesRegionId);
create index Idx_RpClaimBill_MarDeptmentId on RepairClaimBill (MarketingDepartmentId);
create index Idx_RpClaimBill_PSCategoryId on RepairClaimBill (PartsSalesCategoryId);
create index Idx_RpClaimBill_SeTripCAppId on RepairClaimBill (ServiceTripClaimAppId);
create index Idx_RpClaimBill_RCAplicationId on RepairClaimBill (RepairClaimApplicationId);
create index Idx_RpClaimBill_DealerId on RepairClaimBill (DealerId);
create index Idx_RpClaimBill_SeActtyId on RepairClaimBill (ServiceActivityId);
create index Idx_RpClaimBill_SeActtyAppId on RepairClaimBill (ServiceActivityAppId);
create index Idx_RpClaimBill_VMaintPId on RepairClaimBill (VehicleMaintenancePoilcyId);
create index Idx_RpClaimBill_FCStationId on RepairClaimBill (FirstClassStationId);
create index Idx_RpClaimBill_BranchId on RepairClaimBill (BranchId);
create index Idx_RpClaimBill_VehicleId on RepairClaimBill (VehicleId);
create index Idx_RpClaimBill_SePdtLineId on RepairClaimBill (ServiceProductLineId);
create index Idx_RpClaimBill_MalfunId on RepairClaimBill (MalfunctionId);
create index Idx_RpClaimBill_EngineMdlId on RepairClaimBill (EngineModelId);
create index Idx_RpClaimBill_RepTempletId on RepairClaimBill (RepairTempletId);
create index Idx_RpClaimBill_FtyPartsId on RepairClaimBill (FaultyPartsId);
create index Idx_RpClaimBill_FtyPSuppliId on RepairClaimBill (FaultyPartsSupplierId);
create index Idx_RpClaimBill_ResponUnitId on RepairClaimBill (ResponsibleUnitId);
create index Idx_RpClaimBill_FtyPartsWCId on RepairClaimBill (FaultyPartsWCId);
create index Idx_RpClaimBill_CSupplierId on RepairClaimBill (ClaimSupplierId);
create index Idx_RpClaimBill_TempSuppId on RepairClaimBill (TempSupplierId);

create index Idx_RepCMDetail_RepCItemDeId on RepairClaimMaterialDetail (RepairClaimItemDetailId);
create index Idx_RepCMDetail_RepOMateDeId on RepairClaimMaterialDetail (RepairOrderMaterialDetailId);
create index Idx_RepCMDetail_PWCategoryId on RepairClaimMaterialDetail (PartsWarrantyCategoryId);
create index Idx_RepCMDetail_UsedPartsId on RepairClaimMaterialDetail (UsedPartsId);
create index Idx_RepCMDetail_UPSuppliId on RepairClaimMaterialDetail (UsedPartsSupplierId);
create index Idx_RepCMDetail_NewPartsId on RepairClaimMaterialDetail (NewPartsId);
create index Idx_RepCMDetail_NewPSupplId on RepairClaimMaterialDetail (NewPartsSupplierId);

create index Idx_RepCItemDtl_RCBillId on RepairClaimItemDetail (RepairClaimBillId);
create index Idx_RepCItemDtl_ROMDetailId on RepairClaimItemDetail (RepairOrderMaterialDetailId);
create index Idx_RepCItemDtl_RepWorkerId on RepairClaimItemDetail (RepairWorkerId);
create index Idx_RepCItemDtl_RepItemId on RepairClaimItemDetail (RepairItemId);

create index Idx_RepClApplica_SRegionId on RepairClaimApplication (SalesRegionId);
create index Idx_RepClApplica_MkDepartId on RepairClaimApplication (MarketDepartmentId);
create index Idx_RepClApplica_FCStaId on RepairClaimApplication (FirstClassStationId);
create index Idx_RepClApplica_DealerId on RepairClaimApplication (DealerId);
create index Idx_RepClApplica_PSCategId on RepairClaimApplication (PartsSalesCategoryId);
create index Idx_RepClApplica_RepObjId on RepairClaimApplication (RepairObjectId);
create index Idx_RepClApplica_VMPoilcyId on RepairClaimApplication (VehicleMaintenancePoilcyId);
create index Idx_RepClApplica_MalfunId on RepairClaimApplication (MalfunctionId);
create index Idx_RepClApplica_VehicleId on RepairClaimApplication (VehicleId);
create index Idx_RepClApplica_SePductId on RepairClaimApplication (ServiceProductLineId);
create index Idx_RepClApplica_FtyPartsId on RepairClaimApplication (FaultyPartsId);
create index Idx_RepClApplica_FtyPSuppId on RepairClaimApplication (FaultyPartsSupplierId);
create index Idx_RepClApplica_VehWCardId on RepairClaimApplication (VehicleWarrantyCardId);
create index Idx_RepClApplica_BranchId on RepairClaimApplication (BranchId);
create index Idx_RepClApplica_SeActivityId on RepairClaimApplication (ServiceActivityId);

create index Idx_RepCAppDetail_SparePartId on RepairClaimAppDetail (SparePartId);
create index Idx_RepCAppDetail_RepCAppId on RepairClaimAppDetail (RepairClaimApplicationId);
create index Idx_RepCAppDetail_PSupplierId on RepairClaimAppDetail (PartsSupplierId);

create index Idx_ReOrder_RepairWorkOrderId on RepairOrder (RepairWorkOrderId);
create index Idx_ReOrder_SerProductLineId on RepairOrder (ServiceProductLineId);
create index Idx_ReOrder_RepairObjectId on RepairOrder (RepairObjectId);
create index Idx_ReOrder_SalesRegionId on RepairOrder (SalesRegionId);
create index Idx_ReOrder_MarkDepartmentId on RepairOrder (MarketDepartmentId);
create index Idx_ReOrder_PSalesCategoryId on RepairOrder (PartsSalesCategoryId);
create index Idx_ReOrder_RepCficationId on RepairOrder (RepairClassificationId);
create index Idx_ReOrder_SeActivityId on RepairOrder (ServiceActivityId);
create index Idx_ReOrder_RegionID on RepairOrder (RegionID);
create index Idx_ReOrder_SeTClaimAppId on RepairOrder (ServiceTripClaimAppId);
create index Idx_ReOrder_OutCSupplierId on RepairOrder (OutClaimSupplierId);
create index Idx_ReOrder_VehMainPoilcyId on RepairOrder (VehicleMaintenancePoilcyId);
create index Idx_ReOrder_ResponsUnitId on RepairOrder (ResponsibleUnitId);
create index Idx_ReOrder_OUTLINKID on RepairOrder (OUTLINKID);
create index Idx_ReOrder_OutFaultReasonId on RepairOrder (OutFaultReasonId);
create index Idx_ReOrder_SeTripAreaId on RepairOrder (ServiceTripAreaId);
create index Idx_ReOrder_FirstCStationId on RepairOrder (FirstClassStationId);
create index Idx_ReOrder_AppointDealerId on RepairOrder (AppointDealerId);
create index Idx_ReOrder_DealerId on RepairOrder (DealerId);
create index Idx_ReOrder_BranchId on RepairOrder (BranchId);
create index Idx_ReOrder_VehicleId on RepairOrder (VehicleId);
create index Idx_ReOrder_RCustomerId on RepairOrder (RetainedCustomerId);

create index Idx_ReOFtReason_RepOrderId on RepairOrderFaultReason (RepairOrderId);
create index Idx_ReOFtReason_ReTempletId on RepairOrderFaultReason (RepairTempletId);
create index Idx_ReOFtReason_ReCAppId on RepairOrderFaultReason (RepairClaimApplicationId);
create index Idx_ReOFtReason_MalfunId on RepairOrderFaultReason (MalfunctionId);
create index Idx_ReOFtReason_FtyPartsId on RepairOrderFaultReason (FaultyPartsId);
create index Idx_ReOFtReason_FtyPSuppId on RepairOrderFaultReason (FaultyPartsSupplierId);
create index Idx_ReOFtReason_CSupplierId on RepairOrderFaultReason (ClaimSupplierId);
create index Idx_ReOFtReason_SActivityId on RepairOrderFaultReason (ServiceActivityId);
create index Idx_ReOFtReason_TempSuppId on RepairOrderFaultReason (TempSupplierId);

create index Idx_ReOMDetail_UsedPartsId on RepairOrderMaterialDetail (UsedPartsId);
create index Idx_ReOMDetail_RCItemDetId on RepairOrderMaterialDetail (RepairClaimItemDetailId);
create index Idx_ReOMDetail_PWCategoryId on RepairOrderMaterialDetail (PartsWarrantyCategoryId);
create index Idx_ReOMDetail_NewPartsId on RepairOrderMaterialDetail (NewPartsId);
create index Idx_ReOMDetail_UPSupplierId on RepairOrderMaterialDetail (UsedPartsSupplierId);
create index Idx_ReOMDetail_NewPSuppId on RepairOrderMaterialDetail (NewPartsSupplierId);

create index Idx_ReOIDetail_RepairItemId on RepairOrderItemDetail (RepairItemId);
create index Idx_ReOIDetail_ROFtReasonId on RepairOrderItemDetail (RepairOrderFaultReasonId);
create index Idx_ReOIDetail_RepWorkerId on RepairOrderItemDetail (RepairWorkerId);

create index Idx_ReWorkOrder_BranchId on RepairWorkOrder (BranchId);
create index Idx_ReWorkOrder_SeProductId on RepairWorkOrder (ServiceProductLineId);
create index Idx_ReWorkOrder_ReCificaId on RepairWorkOrder (RepairClassificationId);
create index Idx_ReWorkOrder_VehicleId on RepairWorkOrder (VehicleId);
create index Idx_ReWorkOrder_DealerId on RepairWorkOrder (DealerId);

create index Idx_ReTemplet_BranchId on RepairTemplet (BranchId);
create index Idx_ReTemplet_PSCategoryId on RepairTemplet (PartsSalesCategoryId);
create index Idx_ReTemplet_SerProducteId on RepairTemplet (ServiceProductLineId);
create index Idx_ReTemplet_MalfunctionId on RepairTemplet (MalfunctionId);

create index Idx_ReTMatDetail_NewPartsId on RepairTempletMaterialDetail (NewPartsId);
create index Idx_ReTMatDetail_ReTemId on RepairTempletMaterialDetail (RepairTempletItemId);
create index Idx_ReTMatDetail_NewPSuppId on RepairTempletMaterialDetail (NewPartsSupplierId);

create index Idx_ReTItemDetail_ReTempId on RepairTempletItemDetail (RepairTempletId);
create index Idx_ReTItemDetail_ReItemId on RepairTempletItemDetail (RepairItemId);

create index Idx_VMChangeRecord_BranchId on VehicleMileageChangeRecord (BranchId);
create index Idx_VMChangeRecord_RelBillId on VehicleMileageChangeRecord (RelatedBillId);
create index Idx_VMChangeRecord_VehicleId on VehicleMileageChangeRecord (VehicleId);

create index Idx_PCOrder_PCApplicationId on PartsClaimOrder (PartsClaimApplicationId);
create index Idx_PCOrder_PWCategoryId on PartsClaimOrder (PartsWarrantyCategoryId);
create index Idx_PCOrder_RepairObjectId on PartsClaimOrder (RepairObjectId);
create index Idx_PCOrder_SalesRegionId on PartsClaimOrder (SalesRegionId);
create index Idx_PCOrder_PSCategoryId on PartsClaimOrder (PartsSalesCategoryId);
create index Idx_PCOrder_MalfunctionId on PartsClaimOrder (MalfunctionId);
create index Idx_PCOrder_MarkDepartId on PartsClaimOrder (MarketingDepartmentId);
create index Idx_PCOrder_SalesUnitId on PartsClaimOrder (SalesUnitId);
create index Idx_PCOrder_DealerId on PartsClaimOrder (DealerId);
create index Idx_PCOrder_PRetailOrderId on PartsClaimOrder (PartsRetailOrderId);
create index Idx_PCOrder_FirstCStationId on PartsClaimOrder (FirstClassStationId);
create index Idx_PCOrder_SerProductLineId on PartsClaimOrder (ServiceProductLineId);
create index Idx_PCOrder_VehicleId on PartsClaimOrder (VehicleId);
create index Idx_PCOrder_FaultyPartsId on PartsClaimOrder (FaultyPartsId);
create index Idx_PCOrder_FltyPSupplierId on PartsClaimOrder (FaultyPartsSupplierId);
create index Idx_PCOrder_ClaimSupplierId on PartsClaimOrder (ClaimSupplierId);
create index Idx_PCOrder_ResponsibleUnitId on PartsClaimOrder (ResponsibleUnitId);
create index Idx_PCOrder_PartsSalesOrderId on PartsClaimOrder (PartsSalesOrderId);
create index Idx_PCOrder_BranchId on PartsClaimOrder (BranchId);

create index Idx_PCMatDetail_PWCategoryId on PartsClaimMaterialDetail (PartsWarrantyCategoryId);
create index Idx_PCMatDetail_PCRItemDetId on PartsClaimMaterialDetail (PartsClaimRepairItemDetailId);
create index Idx_PCMatDetail_UPSupplierId on PartsClaimMaterialDetail (UsedPartsSupplierId);
create index Idx_PCMatDetail_UsedPartsId on PartsClaimMaterialDetail (UsedPartsId);
create index Idx_PCMatDetail_NewPartsId on PartsClaimMaterialDetail (NewPartsId);
create index Idx_PCMatDetail_NewPSupplId on PartsClaimMaterialDetail (NewPartsSupplierId);

create index Idx_PCRItemDet_PClaimBillId on PartsClaimRepairItemDetail (PartsClaimBillId);
create index Idx_PCRItemDet_RepairItemId on PartsClaimRepairItemDetail (RepairItemId);

create index Idx_PCApplica_SalesRegionId on PartsClaimApplication (SalesRegionId);
create index Idx_PCApplica_MDepartmentId on PartsClaimApplication (MarketDepartmentId);
create index Idx_PCApplica_PSalesOrderId on PartsClaimApplication (PartsSalesOrderId);
create index Idx_PCApplica_DealerId on PartsClaimApplication (DealerId);
create index Idx_PCApplica_PRetailOrderId on PartsClaimApplication (PartsRetailOrderId);
create index Idx_PCApplica_FirstCStationId on PartsClaimApplication (FirstClassStationId);
create index Idx_PCApplica_SeProductLineId on PartsClaimApplication (ServiceProductLineId);
create index Idx_PCApplica_RepairObjectId on PartsClaimApplication (RepairObjectId);
create index Idx_PCApplica_VehicleId on PartsClaimApplication (VehicleId);
create index Idx_PCApplica_MalfunctionId on PartsClaimApplication (MalfunctionId);
create index Idx_PCApplica_FaultyPartsId on PartsClaimApplication (FaultyPartsId);
create index Idx_PCApplica_FtyPSupplierId on PartsClaimApplication (FaultyPartsSupplierId);
create index Idx_PCApplica_BranchId on PartsClaimApplication (BranchId);

create index Idx_PCAppDetail_PCApplicaId on PartsClaimApplicationDetail (PartsClaimApplicationId);
create index Idx_PCAppDetail_SparePartId on PartsClaimApplicationDetail (SparePartId);
create index Idx_PCAppDetail_PSupplierId on PartsClaimApplicationDetail (PartsSupplierId);

create index Idx_PRReturnBill_ReMaterialId on PartRequisitionReturnBill (RepairMaterialId);
create index Idx_PRReturnBill_PSCategoryId on PartRequisitionReturnBill (PartsSalesCategoryId);
create index Idx_PRReturnBill_UsedPartsId on PartRequisitionReturnBill (UsedPartsId);
create index Idx_PRReturnBill_DealerId on PartRequisitionReturnBill (DealerId);
create index Idx_PRReturnBill_UPSupplierId on PartRequisitionReturnBill (UsedPartsSupplierId);
create index Idx_PRReturnBill_SubDealerId on PartRequisitionReturnBill (SubDealerId);
create index Idx_PRReturnBill_PWCategoryId on PartRequisitionReturnBill (PartsWarrantyCategoryId);
create index Idx_PRReturnBill_NewPSuppliId on PartRequisitionReturnBill (NewPartsSupplierId);
create index Idx_PRReturnBill_NewPartsId on PartRequisitionReturnBill (NewPartsId);
--旧件仓库结构
create index Idx_UPWarehouse_BranchId on UsedPartsWarehouse (BranchId);
create index Idx_UPWarehouse_PSCategoryId on UsedPartsWarehouse (PartsSalesCategoryId);
create index Idx_UPWarehouse_RegionId on UsedPartsWarehouse (RegionId);

create index Idx_UPWarehStaff_PersonnelId on UsedPartsWarehouseStaff (PersonnelId);
create index Idx_UPWarehStaff_UPWarehId on UsedPartsWarehouseStaff (UsedPartsWarehouseId);

create index Idx_UPWarehArea_UPWarehouseId on UsedPartsWarehouseArea (UsedPartsWarehouseId);
create index Idx_UPWarehArea_TopUPWhAreaId on UsedPartsWarehouseArea (TopLevelUsedPartsWhseAreaId);
create index Idx_UPWarehArea_ParentId on UsedPartsWarehouseArea (ParentId);

create index Idx_UPWarehManager_PersonId on UsedPartsWarehouseManager (PersonnelId);
create index Idx_UPWarehManager_UPWarAreaId on UsedPartsWarehouseManager (UsedPartsWarehouseAreaId);

create index Idx_UPStock_UPartsWarehouseId on UsedPartsStock (UsedPartsWarehouseId);
create index Idx_UPStock_UPWarehouseAreaId on UsedPartsStock (UsedPartsWarehouseAreaId);
create index Idx_UPStock_ClaimBillId on UsedPartsStock (ClaimBillId);
create index Idx_UPStock_UsedPartsId on UsedPartsStock (UsedPartsId);
create index Idx_UPStock_UPartsSupplierId on UsedPartsStock (UsedPartsSupplierId);
create index Idx_UPStock_FtyPSupplierId on UsedPartsStock (FaultyPartsSupplierId);
create index Idx_UPStock_ResponUnitId on UsedPartsStock (ResponsibleUnitId);
create index Idx_UPStock_BranchId on UsedPartsStock (BranchId);
--旧件仓储执行
create index Idx_UPLoanBill_UPWarehouseId on UsedPartsLoanBill (UsedPartsWarehouseId);

create index Idx_UPLoanDetail_UPLBillId on UsedPartsLoanDetail (UsedPartsLoanBillId);
create index Idx_UPLoanDetail_UsedPartsId on UsedPartsLoanDetail (UsedPartsId);
create index Idx_UPLoanDetail_BranchId on UsedPartsLoanDetail (BranchId);

create index Idx_UPInboundOrd_UPWarehId on UsedPartsInboundOrder (UsedPartsWarehouseId);
create index Idx_UPInboundOrd_SourceId on UsedPartsInboundOrder (SourceId);
create index Idx_UPInboundOrd_RelCompanyId on UsedPartsInboundOrder (RelatedCompanyId);

create index Idx_UPInboundDet_UPInbOrderId on UsedPartsInboundDetail (UsedPartsInboundOrderId);
create index Idx_UPInboundDet_UsedPartsId on UsedPartsInboundDetail (UsedPartsId);
create index Idx_UPInboundDet_UPWarehAreaId on UsedPartsInboundDetail (UsedPartsWarehouseAreaId);
create index Idx_UPInboundDet_BranchId on UsedPartsInboundDetail (BranchId);

create index Idx_UPOutboundOrd_UPWarehId on UsedPartsOutboundOrder (UsedPartsWarehouseId);
create index Idx_UPOutboundOrd_SourceId on UsedPartsOutboundOrder (SourceId);
create index Idx_UPOutboundOrd_RelCompanyId on UsedPartsOutboundOrder (RelatedCompanyId);

create index Idx_UPOutboundDet_UsedPartsId on UsedPartsOutboundDetail (UsedPartsId);
create index Idx_UPOutboundDet_UPOutOrderId on UsedPartsOutboundDetail (UsedPartsOutboundOrderId);
create index Idx_UPOutboundDet_UPWareAreaId on UsedPartsOutboundDetail (UsedPartsWarehouseAreaId);
create index Idx_UPOutboundDet_BranchId on UsedPartsOutboundDetail (BranchId);

create index Idx_UPRefitBill_UPWarehouseId on UsedPartsRefitBill (UsedPartsWarehouseId);

create index Idx_UPRRetDetail_UPRefitBillId on UsedPartsRefitReturnDetail (UsedPartsRefitBillId);
create index Idx_UPRRetDetail_UsedPartsId on UsedPartsRefitReturnDetail (UsedPartsId);
create index Idx_UPRRetDetail_BranchId on UsedPartsRefitReturnDetail (BranchId);

create index Idx_UPRReqDetail_UPRefitBillId on UsedPartsRefitReqDetail (UsedPartsRefitBillId);
create index Idx_UPRReqDetail_UsedPartsId on UsedPartsRefitReqDetail (UsedPartsId);
create index Idx_UPRReqDetail_BranchId on UsedPartsRefitReqDetail (BranchId);

create index Idx_UPDisposalBill_UPWarehId on UsedPartsDisposalBill (UsedPartsWarehouseId);

create index Idx_UPDisDetail_UPDisBillId on UsedPartsDisposalDetail (UsedPartsDisposalBillId);
create index Idx_UPDisDetail_UsedPartsId on UsedPartsDisposalDetail (UsedPartsId);
create index Idx_UPDisDetail_BranchId on UsedPartsDisposalDetail (BranchId);

create index Idx_UPRetOrder_RetOfficeId on UsedPartsReturnOrder (ReturnOfficeId);
create index Idx_UPRetOrder_OutboundWaId on UsedPartsReturnOrder (OutboundWarehouseId);

create index Idx_UPRetDetail_UPRetOrderId on UsedPartsReturnDetail (UsedPartsReturnOrderId);
create index Idx_UPRetDetail_BranchId on UsedPartsReturnDetail (BranchId);
create index Idx_UPRetDetail_UPSupplierId on UsedPartsReturnDetail (UsedPartsSupplierId);
create index Idx_UPRetDetail_UsedPartsId on UsedPartsReturnDetail (UsedPartsId);
create index Idx_UPRetDetail_ClaimBillId on UsedPartsReturnDetail (ClaimBillId);

create index Idx_UPShiftOrder_UPWarehId on UsedPartsShiftOrder (UsedPartsWarehouseId);

create index Idx_UPShiftDetail_UPSOrderId on UsedPartsShiftDetail (UsedPartsShiftOrderId);
create index Idx_UPShiftDetail_OSPositionId on UsedPartsShiftDetail (OriginStoragePositionId);
create index Idx_UPShiftDetail_UsedPartsId on UsedPartsShiftDetail (UsedPartsId);
create index Idx_UPShiftDetail_BranchId on UsedPartsShiftDetail (BranchId);
create index Idx_UPShiftDetail_DSPositionId on UsedPartsShiftDetail (DestiStoragePositionId);

create index Idx_UPTransferOrder_OriWarehId on UsedPartsTransferOrder (OriginWarehouseId);
create index Idx_UPTransferOrder_DesWarehId on UsedPartsTransferOrder (DestinationWarehouseId);

create index Idx_UPTransDetail_UsedPartsId on UsedPartsTransferDetail (UsedPartsId);
create index Idx_UPTransDetail_UPTransOrdId on UsedPartsTransferDetail (UsedPartsTransferOrderId);
create index Idx_UPTransDetail_ClaimBillId on UsedPartsTransferDetail (ClaimBillId);
create index Idx_UPTransDetail_BranchId on UsedPartsTransferDetail (BranchId);
create index Idx_UPTransDetail_ResponUnitId on UsedPartsTransferDetail (ResponsibleUnitId);
create index Idx_UPTransDetail_UPSupplierId on UsedPartsTransferDetail (UsedPartsSupplierId);
create index Idx_UPTransDetail_FtyPSupplrId on UsedPartsTransferDetail (FaultyPartsSupplierId);
--服务站结算相关
create index Idx_DInInfo_BranchId on DealerInvoiceInformation (BranchId);
create index Idx_DInInfo_PSalesCategoryId on DealerInvoiceInformation (PartsSalesCategoryId);
create index Idx_DInInfo_DealerId on DealerInvoiceInformation (DealerId);

create index Idx_SetBillInvo_DealerInvoId on SettleBillInvoiceLink (DealerInvoiceId);
create index Idx_SetBillInvo_CSettleBillId on SettleBillInvoiceLink (ClaimSettlementBillId);

create index Idx_SsCSetBill_DealerId on SsClaimSettlementBill (DealerId);
create index Idx_SsCSetBill_PSCategoryId on SsClaimSettlementBill (PartsSalesCategoryId);
create index Idx_SsCSetBill_BranchId on SsClaimSettlementBill (BranchId);

create index Idx_SsCSetDetail_SourceId on SsClaimSettlementDetail (SourceId);
create index Idx_SsCSetDetail_SsCSetBillId on SsClaimSettlementDetail (SsClaimSettlementBillId);

create index Idx_SsCSetInstruction_BranchId on SsClaimSettleInstruction (BranchId);

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--配件仓储相关
create index Idx_WmsCStock_BranchId on WmsCongelationStock (BranchId);
create index Idx_WmsCStock_PartsSCId on WmsCongelationStock (PartsSalesCategoryId);
create index Idx_WmsCStock_SparePartId on WmsCongelationStock (SparePartId);

create index Idx_Warehouse_RegionId on Warehouse (RegionId);
create index Idx_Warehouse_SCompanyId on Warehouse (StorageCompanyId);
create index Idx_Warehouse_BranchId on Warehouse (BranchId);

create index Idx_WOperator_WarehouseId on WarehouseOperator (WarehouseId);
create index Idx_WOperator_OperatorId on WarehouseOperator (OperatorId);

create index Idx_WArea_WhouseId on WarehouseArea (WarehouseId);
create index Idx_WArea_TLvlWAreaId on WarehouseArea (TopLevelWarehouseAreaId);
create index Idx_WArea_ACategId on WarehouseArea (AreaCategoryId);
create index Idx_WArea_ParentId on WarehouseArea (ParentId);

create index Idx_WAreaManager_WAreaId on WarehouseAreaManager (WarehouseAreaId);
create index Idx_WAreaManager_ManagerId on WarehouseAreaManager (ManagerId);

create index Idx_DPartsStock_DealerId on DealerPartsStock (DealerId);
create index Idx_DPartsStock_SubDealerId on DealerPartsStock (SubDealerId);
create index Idx_DPartsStock_SCategoryId on DealerPartsStock (SalesCategoryId);
create index Idx_DPartsStock_BranchId on DealerPartsStock (BranchId);
create index Idx_DPartsStock_SparePartId on DealerPartsStock (SparePartId);

create index Idx_WAHistory_WarehouseId on WarehouseAreaHistory (WarehouseId);
create index Idx_WAHistory_StorageCompanyId on WarehouseAreaHistory (StorageCompanyId);
create index Idx_WAHistory_WACategoryId on WarehouseAreaHistory (WarehouseAreaCategoryId);
create index Idx_WAHistory_BranchId on WarehouseAreaHistory (BranchId);
create index Idx_WAHistory_WarehouseAreaId on WarehouseAreaHistory (WarehouseAreaId);
create index Idx_WAHistory_PartId on WarehouseAreaHistory (PartId);

create index Idx_PartsStock_WarehouseId on PartsStock (WarehouseId);
create index Idx_PartsStock_SCompanyId on PartsStock (StorageCompanyId);
create index Idx_PartsStock_WACategoryId on PartsStock (WarehouseAreaCategoryId);
create index Idx_PartsStock_BranchId on PartsStock (BranchId);
create index Idx_PartsStock_WarehouseAreaId on PartsStock (WarehouseAreaId);
create index Idx_PartsStock_PartId on PartsStock (PartId);

create index Idx_PSBatchDetail_PartsStockId on PartsStockBatchDetail (PartsStockId);
create index Idx_PSBatchDetail_WarehouseId on PartsStockBatchDetail (WarehouseId);
create index Idx_PSBatchDetail_SCompanyId on PartsStockBatchDetail (StorageCompanyId);
create index Idx_PSBatchDetail_PartId on PartsStockBatchDetail (PartId);

create index Idx_PartsBatchRecord_PartId on PartsBatchRecord (PartId);
create index Idx_PBRecord_COCompanyId on PartsBatchRecord (CreatorOwnerCompanyId);

create index Idx_PLStock_WarehouseId on PartsLockedStock (WarehouseId);
create index Idx_PLStock_StorageCompanyId on PartsLockedStock (StorageCompanyId);
create index Idx_PLStock_PartId on PartsLockedStock (PartId);
create index Idx_PLStock_BranchId on PartsLockedStock (BranchId);
--积压件平台
create index Idx_OsPInfo_StorageCompanyId on OverstockPartsInformation (StorageCompanyId);
create index Idx_OsPInfo_PSCategoryId on OverstockPartsInformation (PartsSalesCategoryId);
create index Idx_OstockPartsInfo_BranchId on OverstockPartsInformation (BranchId);
create index Idx_OsPartsInfo_SparePartId on OverstockPartsInformation (SparePartId);

create index Idx_OstockPartsApp_SCompanyId on OverstockPartsApp (StorageCompanyId);
create index Idx_OstockPartsApp_PSCategId on OverstockPartsApp (PartsSalesCategoryId);
create index Idx_OstockPartsApp_BranchId on OverstockPartsApp (BranchId);

create index Idx_OsPartsAppDet_OsPartsAppId on OverstockPartsAppDetail (OverstockPartsAppId);
create index Idx_OsPartsAppDet_SparePartId on OverstockPartsAppDetail (SparePartId);

create index Idx_OsPAdjustBi_OSCompanyId on OverstockPartsAdjustBill (OriginalStorageCompanyId);
create index Idx_OsPAdjustBi_OSCCAccountId on OverstockPartsAdjustBill (OriginalStorageCoCustAccountId);
create index Idx_OsPartsAdjustBi_SUnitId on OverstockPartsAdjustBill (SalesUnitId);
create index Idx_OsPartsAdjustBi_SourceId on OverstockPartsAdjustBill (SourceId);
create index Idx_OsPartsAdjustBi_DSCompId on OverstockPartsAdjustBill (DestStorageCompanyId);
create index Idx_OsPartsAdjustBi_SUOCompId on OverstockPartsAdjustBill (SalesUnitOwnerCompanyId);
create index Idx_OsPartsAdjustBi_DSCCAccId on OverstockPartsAdjustBill (DestStorageCoCustomerAccountId);
create index Idx_OsPartsAdjustBi_OReqBiId on OverstockPartsAdjustBill (OriginalRequirementBillId);
create index Idx_OsPartsAdjustBi_BranchId on OverstockPartsAdjustBill (BranchId);

create index Idx_OsPartsAdjDet_SparePartId on OverstockPartsAdjustDetail (SparePartId);
create index Idx_OsPartsAdjDet_OsPAdjBiId on OverstockPartsAdjustDetail (OverstockPartsAdjustBillId);
--配件仓储执行
create index Idx_CompTraferO_PSCategoryId on CompanyTransferOrder (PartsSalesCategoryId);
create index Idx_CompTraferO_OCompanyId on CompanyTransferOrder (OriginalCompanyId);
create index Idx_CompTraferO_DCompanyId on CompanyTransferOrder (DestCompanyId);
create index Idx_CompTraferO_DWId on CompanyTransferOrder (DestWarehouseId);
create index Idx_CompTraferO_OWarehouseId on CompanyTransferOrder (OriginalWarehouseId);

create index Idx_CompTraferODet_PartsId on CompanyTransferOrderDetail (PartsId);
create index Idx_CompTraferODet_PTraferOId on CompanyTransferOrderDetail (PartsTransferOrderId);

create index Idx_DPInventoryBi_BranchId on DealerPartsInventoryBill (BranchId);
create index Idx_DPInventoryBi_SCompanyId on DealerPartsInventoryBill (StorageCompanyId);
create index Idx_DPInventoryBi_WarehouseId on DealerPartsInventoryBill (WarehouseId);
create index Idx_DPInventoryBi_SCategoryId on DealerPartsInventoryBill (SalesCategoryId);

create index Idx_DPInventoryDet_SparePartId on DealerPartsInventoryDetail (SparePartId);
create index Idx_DPInventoryDet_DPInventId on DealerPartsInventoryDetail (DealerPartsInventoryId);

create index Idx_PInbPackingDet_SparePartId on PartsInboundPackingDetail (SparePartId);
create index Idx_PInbPackingDet_PInbChkBiId on PartsInboundPackingDetail (PartsInboundCheckBillId);
create index Idx_PInbPackingDet_WareAId on PartsInboundPackingDetail (WarehouseAreaId);

create index Idx_PInboundChkBi_WarehId on PartsInboundCheckBill (WarehouseId);
create index Idx_PInboundChkBi_PInbPlId on PartsInboundCheckBill (PartsInboundPlanId);
create index Idx_PInboundChkBi_SCompId on PartsInboundCheckBill (StorageCompanyId);
create index Idx_PInboundChkBi_BranchId on PartsInboundCheckBill (BranchId);
create index Idx_PInboundChkBi_PSCategId on PartsInboundCheckBill (PartsSalesCategoryId);
create index Idx_PInboundChkBi_CpCompId on PartsInboundCheckBill (CounterpartCompanyId);
create index Idx_PInboundChkBi_OReqBiId on PartsInboundCheckBill (OriginalRequirementBillId);
create index Idx_PInboundChkBi_CustAccId on PartsInboundCheckBill (CustomerAccountId);

create index Idx_PInbChkBiDet_SPartId on PartsInboundCheckBillDetail (SparePartId);
create index Idx_PInbChkBiDet_PInbChkBiId on PartsInboundCheckBillDetail (PartsInboundCheckBillId);
create index Idx_PInbChkBiDet_WarehAId on PartsInboundCheckBillDetail (WarehouseAreaId);

create index Idx_PInboundPl_WarehId on PartsInboundPlan (WarehouseId);
create index Idx_PInboundPl_SCompanyId on PartsInboundPlan (StorageCompanyId);
create index Idx_PInboundPl_PSCategId on PartsInboundPlan (PartsSalesCategoryId);
create index Idx_PInboundPl_BranchId on PartsInboundPlan (BranchId);
create index Idx_PInboundPl_CpCompanyId on PartsInboundPlan (CounterpartCompanyId);
create index Idx_PInboundPl_CustAccoId on PartsInboundPlan (CustomerAccountId);
create index Idx_PInboundPl_OReqBiId on PartsInboundPlan (OriginalRequirementBillId);
create index Idx_PInboundPl_SourceId on PartsInboundPlan (SourceId);

create index Idx_PInbPlanDet_SPartId on PartsInboundPlanDetail (SparePartId);
create index Idx_PInbPlanDet_PInbPlanId on PartsInboundPlanDetail (PartsInboundPlanId);

create index Idx_POutbBi_POutbPlanId on PartsOutboundBill (PartsOutboundPlanId);
create index Idx_POutbBi_PSalesCategId on PartsOutboundBill (PartsSalesCategoryId);
create index Idx_POutbBi_SCompId on PartsOutboundBill (StorageCompanyId);
create index Idx_POutbBi_WarehId on PartsOutboundBill (WarehouseId);
create index Idx_POutbBi_CopCompId on PartsOutboundBill (CounterpartCompanyId);
create index Idx_POutbBi_ReceivingCompId on PartsOutboundBill (ReceivingCompanyId);
create index Idx_POutbBi_OrReqBiId on PartsOutboundBill (OriginalRequirementBillId);
create index Idx_POutbBi_ReceivingWarehId on PartsOutboundBill (ReceivingWarehouseId);
create index Idx_POutbBi_BranchId on PartsOutboundBill (BranchId);
create index Idx_POutbBi_CustAccId on PartsOutboundBill (CustomerAccountId);

create index Idx_POutbBiDet_SPartId on PartsOutboundBillDetail (SparePartId);
create index Idx_POutbBiDet_POutbBiId on PartsOutboundBillDetail (PartsOutboundBillId);
create index Idx_POutbBiDet_WarehArId on PartsOutboundBillDetail (WarehouseAreaId);

create index Idx_POutbPlan_WarehId on PartsOutboundPlan (WarehouseId);
create index Idx_POutbPlan_PSCategId on PartsOutboundPlan (PartsSalesCategoryId);
create index Idx_POutbPlan_SCompId on PartsOutboundPlan (StorageCompanyId);
create index Idx_POutbPlan_BranchId on PartsOutboundPlan (BranchId);
create index Idx_POutbPlan_CopCompId on PartsOutboundPlan (CounterpartCompanyId);
create index Idx_POutbPlan_RevingCompId on PartsOutboundPlan (ReceivingCompanyId);
create index Idx_POutbPlan_RevingWarehId on PartsOutboundPlan (ReceivingWarehouseId);
create index Idx_POutbPlan_CustAccId on PartsOutboundPlan (CustomerAccountId);
create index Idx_POutbPlan_SourceId on PartsOutboundPlan (SourceId);
create index Idx_POutbPlan_OrReqBiId on PartsOutboundPlan (OriginalRequirementBillId);

create index Idx_POutbPlDet_SparePartId on PartsOutboundPlanDetail (SparePartId);
create index Idx_POutbPlDet_POutbPlanId on PartsOutboundPlanDetail (PartsOutboundPlanId);

create index Idx_PPacking_PartsStockId on PartsPacking (PartsStockId);
create index Idx_PPacking_StorageCompanyId on PartsPacking (StorageCompanyId);
create index Idx_PPacking_SparePartId on PartsPacking (SparePartId);
create index Idx_PPacking_WarehouseId on PartsPacking (WarehouseId);
create index Idx_PPacking_WarehouseAreaId on PartsPacking (WarehouseAreaId);

create index Idx_PLogisticBatch_CompanyId on PartsLogisticBatch (CompanyId);
create index Idx_PLogisticBatch_SourceId on PartsLogisticBatch (SourceId);

create index Idx_PLogBatchBiDet_BillId on PartsLogisticBatchBillDetail (BillId);
create index Idx_PLogBatchBiDet_PLogBatchId on PartsLogisticBatchBillDetail (PartsLogisticBatchId);

create index Idx_PLogBItDet_PLogBatchId on PartsLogisticBatchItemDetail (PartsLogisticBatchId);
create index Idx_PLogBItDet_CoCompId on PartsLogisticBatchItemDetail (CounterpartCompanyId);
create index Idx_PLogBItDet_RevingCompId on PartsLogisticBatchItemDetail (ReceivingCompanyId);
create index Idx_PLogBItDet_SPartId on PartsLogisticBatchItemDetail (SparePartId);
create index Idx_PLogBItDet_ShippCompId on PartsLogisticBatchItemDetail (ShippingCompanyId);

create index Idx_PInventoryBi_WarehouseId on PartsInventoryBill (WarehouseId);
create index Idx_PInventoryBi_StorageCompId on PartsInventoryBill (StorageCompanyId);
create index Idx_PInventoryBi_BranchId on PartsInventoryBill (BranchId);

create index Idx_PInvDet_PInventoryBiId on PartsInventoryDetail (PartsInventoryBillId);
create index Idx_PInventoryDet_WarehAId on PartsInventoryDetail (WarehouseAreaId);
create index Idx_PInventoryDet_SPartId on PartsInventoryDetail (SparePartId);

create index Idx_PShiftO_BranchId on PartsShiftOrder (BranchId);
create index Idx_PShiftO_WarehouseId on PartsShiftOrder (WarehouseId);
create index Idx_PShiftO_StorageCompId on PartsShiftOrder (StorageCompanyId);
--create index Idx_PShiftO_SPartId on PartsShiftOrder (SparePartId);

create index Idx_PShiftODet_DestWarehAId on PartsShiftOrderDetail (DestWarehouseAreaId);
create index Idx_PShiftODet_PartsShiftOId on PartsShiftOrderDetail (PartsShiftOrderId);
create index Idx_PShiftODet_OrWarehAId on PartsShiftOrderDetail (OriginalWarehouseAreaId);

create index Idx_PTransferO_OrWarehouseId on PartsTransferOrder (OriginalWarehouseId);
create index Idx_PTransferO_StorageCompId on PartsTransferOrder (StorageCompanyId);
create index Idx_PTransferO_DestWarehId on PartsTransferOrder (DestWarehouseId);

create index Idx_PTransferODet_SPartId on PartsTransferOrderDetail (SparePartId);
create index Idx_PTransferODet_PartsTraOId on PartsTransferOrderDetail (PartsTransferOrderId);
--配件物流相关
create index Idx_HauDistanceInfo_StorCompId on HauDistanceInfor (StorageCompanyId);
create index Idx_HauDistanceInfo_CompAddId on HauDistanceInfor (CompanyAddressId);
create index Idx_HauDistanceInfo_CompId on HauDistanceInfor (CompanyId);
create index Idx_HauDistanceInfo_WarehId on HauDistanceInfor (WarehouseId);

create index Idx_PShippingO_ShippCompId on PartsShippingOrder (ShippingCompanyId);
create index Idx_PShippingO_SettCompId on PartsShippingOrder (SettlementCompanyId);
create index Idx_PShippingO_RevingCompId on PartsShippingOrder (ReceivingCompanyId);
create index Idx_PShippingO_BranchId on PartsShippingOrder (BranchId);
create index Idx_PShippingO_RevingWarehId on PartsShippingOrder (ReceivingWarehouseId);
create index Idx_PShippingO_LogcCompId on PartsShippingOrder (LogisticCompanyId);
create index Idx_PShippingO_OrReqBiId on PartsShippingOrder (OriginalRequirementBillId);

create index Idx_PShippingORef_POutbBiId on PartsShippingOrderRef (PartsOutboundBillId);
create index Idx_PShippingORef_PShippingOId on PartsShippingOrderRef (PartsShippingOrderId);

--应收应付管理
create index Idx_PayOutBi_PSalesCategId on PayOutBill (PartsSalesCategoryId);
create index Idx_PayOutBi_BuyerCompId on PayOutBill (BuyerCompanyId);
create index Idx_PayOutBi_SuppCompId on PayOutBill (SupplierCompanyId);
create index Idx_PayOutBi_PayOutPlanId on PayOutBill (PayOutPlanId);
create index Idx_PayOutBi_BankAccId on PayOutBill (BankAccountId);

create index Idx_SuppInfo_BuyerCompId on SupplierInformation (BuyerCompanyId);
create index Idx_SuppInfo_SuppCompId on SupplierInformation (SupplierCompanyId);

create index Idx_SuppOpenAccApp_BuyerCompId on SupplierOpenAccountApp (BuyerCompanyId);
create index Idx_SuppOpenAccApp_PSCategId on SupplierOpenAccountApp (PartsSalesCategoryId);
create index Idx_SuppOpenAccApp_SuppCompId on SupplierOpenAccountApp (SupplierCompanyId);

create index Idx_SuppAcc_BuyerCompId on SupplierAccount (BuyerCompanyId);
create index Idx_SuppAcc_PSalesCategId on SupplierAccount (PartsSalesCategoryId);
create index Idx_SuppAcc_SuppCompId on SupplierAccount (SupplierCompanyId);

create index Idx_SuppTraBi_BuyerCompId on SupplierTransferBill (BuyerCompanyId);
create index Idx_SuppTraBi_OutbCompId on SupplierTransferBill (OutboundCompanyId);
create index Idx_SuppTraBi_InbCompId on SupplierTransferBill (InboundCompanyId);

create index Idx_CredenceApp_SalesCompId on CredenceApplication (SalesCompanyId);
create index Idx_CredenceApp_GuarCompId on CredenceApplication (GuarantorCompanyId);
create index Idx_CredenceApp_FinaContractId on CredenceApplication (FinancingContractId);
create index Idx_CredenceApp_AccGroupId on CredenceApplication (AccountGroupId);
create index Idx_CredenceApp_CustCompId on CredenceApplication (CustomerCompanyId);

create index Idx_InvoiceInfo_OwnerCompId on InvoiceInformation (OwnerCompanyId);
create index Idx_InvoiceInfo_InvoiceCompId on InvoiceInformation (InvoiceCompanyId);
create index Idx_InvoiceInfo_InvoiceRCompId on InvoiceInformation (InvoiceReceiveCompanyId);
create index Idx_InvoiceInfo_SourceId on InvoiceInformation (SourceId);

create index Idx_CustOpenAccApp_AccGroupId on CustomerOpenAccountApp (AccountGroupId);
create index Idx_CustOpenAccApp_CustCompId on CustomerOpenAccountApp (CustomerCompanyId);

create index Idx_CustAccount_AccGroupId on CustomerAccount (AccountGroupId);
create index Idx_CustAccount_CustCompId on CustomerAccount (CustomerCompanyId);

create index Idx_CustAccHisDet_CustAccId on CustomerAccountHisDetail (CustomerAccountId);
create index Idx_CustAccHisDet_SourceId on CustomerAccountHisDetail (SourceId);

create index Idx_CustTraBi_InbCustCompId on CustomerTransferBill (InboundCustomerCompanyId);
create index Idx_CustTraBi_OutbAccGroupId on CustomerTransferBill (OutboundAccountGroupId);
create index Idx_CustTraBi_OutbCustCompId on CustomerTransferBill (OutboundCustomerCompanyId);
create index Idx_CustTraBi_SalesCompId on CustomerTransferBill (SalesCompanyId);
create index Idx_CustTraBi_InbAccGroupId on CustomerTransferBill (InboundAccountGroupId);

create index Idx_AccPayableHDet_SuppAccId on AccountPayableHistoryDetail (SupplierAccountId);
create index Idx_AccPayableHDet_SourceId on AccountPayableHistoryDetail (SourceId);

--无AccountReceivableBill表
--create index Idx_AccRevableBi_SalesCompId on AccountReceivableBill (SalesCompanyId);
--create index Idx_AccRevableBi_CustCompId on AccountReceivableBill (CustomerCompanyId);
--create index Idx_AccRevableBi_ArBankAccId on AccountReceivableBill (ArBankAccountId);

create index Idx_Summary_OwnerCompanyId on Summary (OwnerCompanyId);


create index Idx_PayBenefiL_SCompId on PaymentBeneficiaryList (SalesCompanyId);
create index Idx_PayBenefiL_AccGroupId on PaymentBeneficiaryList (AccountGroupId);
create index Idx_PayBenefiL_CustCompId on PaymentBeneficiaryList (CustomerCompanyId);
create index Idx_PayBenefiL_PayBiId on PaymentBeneficiaryList (PaymentBillId);

create index Idx_PaymentBi_SCompanyId on PaymentBill (SalesCompanyId);
create index Idx_PaymentBi_CustCompId on PaymentBill (CustomerCompanyId);
create index Idx_PaymentBi_BankAccId on PaymentBill (BankAccountId);

create index Idx_AccGroup_SCompId on AccountGroup (SalesCompanyId);

create index Idx_BankAccount_OCompId on BankAccount (OwnerCompanyId);
--存货核算
create index Idx_WCostChangeBi_PlPrAppId on WarehouseCostChangeBill (PlannedPriceAppId);
create index Idx_WCostChangeBi_OCompId on WarehouseCostChangeBill (OwnerCompanyId);
create index Idx_WCostChangeBi_WarehId on WarehouseCostChangeBill (WarehouseId);

create index Idx_WCostChangeDet_WCostCBiId on WarehouseCostChangeDetail (WarehouseCostChangeBillId);
create index Idx_WCostChangeDet_WarehId on WarehouseCostChangeDetail (WarehouseId);
create index Idx_WCostChangeDet_SPartId on WarehouseCostChangeDetail (SparePartId);

create index Idx_PCostTraInTransit_OCompId on PartsCostTransferInTransit (OwnerCompanyId);
create index Idx_PCostTraInTransit_PSCatId on PartsCostTransferInTransit (PartsSalesCategoryId);
create index Idx_PCostTraInTransit_SPartId on PartsCostTransferInTransit (SparePartId);

create index Idx_EntPartsCost_OCompId on EnterprisePartsCost (OwnerCompanyId);
create index Idx_EntPartsCost_PSCategId on EnterprisePartsCost (PartsSalesCategoryId);
create index Idx_EntPartsCost_SPartId on EnterprisePartsCost (SparePartId);

create index Idx_LoOverflowClaimApp_OCompId on LossOverflowClaimApp (OwnerCompanyId);
create index Idx_LoOvflowClaimApp_PSCategId on LossOverflowClaimApp (PartsSalesCategoryId);
create index Idx_LoOverflowClaimApp_WarehId on LossOverflowClaimApp (WarehouseId);

create index Idx_LoOvfCAppDet_LoOvfCAppId on LossOverflowClaimAppDetail (LossOverflowClaimAppId);
create index Idx_LoOvflowCAppDet_SPartId on LossOverflowClaimAppDetail (SparePartId);

create index Idx_PlPriceApp_OCompId on PlannedPriceApp (OwnerCompanyId);
create index Idx_PlPriceApp_PSCategId on PlannedPriceApp (PartsSalesCategoryId);

create index Idx_PlPAppDet_PlPriceAppId on PlannedPriceAppDetail (PlannedPriceAppId);
create index Idx_PlPAppDet_SPartId on PlannedPriceAppDetail (SparePartId);

create index Idx_FinSnapshotSet_BranchId on FinancialSnapshotSet (BranchId);
create index Idx_FinSnapshotSet_PSCategId on FinancialSnapshotSet (PartsSalesCategoryId);

create index Idx_PWPendingCost_OCompId on PartsWarehousePendingCost (OwnerCompanyId);
create index Idx_PWPendingCost_WarehId on PartsWarehousePendingCost (WarehouseId);
create index Idx_PWPendingCost_SPartId on PartsWarehousePendingCost (SparePartId);

create index Idx_PHStock_WarehouseId on PartsHistoryStock (WarehouseId);
create index Idx_PHtock_StoragCompId on PartsHistoryStock (StorageCompanyId);
create index Idx_PHStock_BranchId on PartsHistoryStock (BranchId);
create index Idx_PHStock_WarehAId on PartsHistoryStock (WarehouseAreaId);
create index Idx_PHStock_WarehACategId on PartsHistoryStock (WarehouseAreaCategoryId);
create index Idx_PHStock_PartId on PartsHistoryStock (PartId);

create index Idx_PPlannedPriceCompId on PartsPlannedPrice (OwnerCompanyId);
create index Idx_PPlannedPrice_PSCategId on PartsPlannedPrice (PartsSalesCategoryId);
create index Idx_PPlannedPrice_SPartId on PartsPlannedPrice (SparePartId);
--服务基础信息

create index Idx_WPolicy_BranchId on WarrantyPolicy (BranchId);
create index Idx_WPolicyHistory_MRecordId on WarrantyPolicyHistory (MasterRecordId);

create index Idx_WPolicyNServPL_WPolicyId on WarrantyPolicyNServProdLine (WarrantyPolicyId);
create index Idx_WPolicyNServPL_SerPLId on WarrantyPolicyNServProdLine (ServiceProductLineId);

create index Idx_WPolNSerProdLH_WPolicyId on WarrPolNServProdLineHistory (WarrantyPolicyId);
create index Idx_WPolNSerProdLH_WPNSerPLId on WarrPolNServProdLineHistory (WarrantyPolicyNServProdLineId);
create index Idx_WPolNSerProdLH_ServicePLId on WarrPolNServProdLineHistory (ServiceProductLineId);

create index Idx_ServiceTripArea_PSCategId on ServiceTripArea (PartsSalesCategoryId);
create index Idx_ServiceTripArea_BranchId on ServiceTripArea (BranchId);

create index Idx_ServTripPR_ServTPGradeId on ServiceTripPriceRate (ServiceTripPriceGradeId);
create index Idx_ServTripPriceR_ServPLId on ServiceTripPriceRate (ServiceProductLineId);

create index Idx_ServTripPGrade_BranchId on ServiceTripPriceGrade (BranchId);


create index Idx_LabHUPR_LabHUPGradeId on LaborHourUnitPriceRate (LaborHourUnitPriceGradeId);
create index Idx_LabHUPR_ServPLId on LaborHourUnitPriceRate (ServiceProductLineId);

create index Idx_LabHUPGrade_BranchId on LaborHourUnitPriceGrade (BranchId);
create index Idx_Malf_MalfCategId on Malfunction (MalfunctionCategoryId);

create index Idx_MalfBrandRelation_BranchId on MalfunctionBrandRelation (BranchId);
create index Idx_MalfBrandRelation_MalfId on MalfunctionBrandRelation (MalfunctionId);
create index Idx_MalfBrandRel_PSCategId on MalfunctionBrandRelation (PartsSalesCategoryId);

create index Idx_MalfCategory_ParentId on MalfunctionCategory (ParentId);
create index Idx_MalfCategory_RootGroupId on MalfunctionCategory (RootGroupId);
create index Idx_MalfCategory_LNodeTypeId on MalfunctionCategory (LayerNodeTypeId);

create index Idx_MalfCatAVehiCat_VCategId on MalfCatAffiVehiCat (VehicleCategoryId);
create index Idx_MalfCatAVehiCat_MCategId on MalfCatAffiVehiCat (MalfunctionCategoryId);

create index Idx_VWarrantyTerm_WPolicyId on VehicleWarrantyTerm (WarrantyPolicyId);


create index Idx_VWarrTermH_VWarrTermId on VehicleWarrantyTermHistory (VehicleWarrantyTermId);
create index Idx_VWarrTermH_WPolicyId on VehicleWarrantyTermHistory (WarrantyPolicyId);

create index Idx_VMainteTerm_WPolicyId on VehicleMainteTerm (WarrantyPolicyId);

create index Idx_VMainteTermH_VMainteTermId on VehicleMainteTermHistory (VehicleMainteTermId);
create index Idx_VMainteTermH_WPolicyId on VehicleMainteTermHistory (WarrantyPolicyId);

create index Idx_VMainteRepItem_VMTermId on VehicleMainteRepairItem (VehicleMainteTermId);
create index Idx_VMainteRepItem_RepItemId on VehicleMainteRepairItem (RepairItemId);

create index Idx_GradeCoefficient_BranchId on GradeCoefficient (BranchId);
create index Idx_GradeCoefficient_PSCategId on GradeCoefficient (PartsSalesCategoryId);

create index Idx_ServiceActivity_BranchId on ServiceActivity (BranchId);
create index Idx_ServiceActivity_PSCategId on ServiceActivity (PartsSalesCategoryId);
create index Idx_ServiceActivity_RespUId on ServiceActivity (ResponsibleUnitId);
create index Idx_ServiceActivity_FPartsId on ServiceActivity (FaultyPartsId);
create index Idx_ServActivity_FPartsSuppId on ServiceActivity (FaultyPartsSupplierId);
create index Idx_ServActivity_FPartsWCId on ServiceActivity (FaultyPartsWCId);

create index Idx_ServActCondition_ServActId on ServiceActivityCondition (ServiceActivityId);

create index Idx_ServActMaterialDet_SPartId on ServiceActivityMaterialDetail (SparePartId);
create index Idx_ServActMDet_ServActRItemId on ServiceActivityMaterialDetail (ServiceActivityRepairItemId);


create index Idx_ServActRItem_ServActId on ServiceActivityRepairItem (ServiceActivityId);
create index Idx_ServActRItem_RepairItemId on ServiceActivityRepairItem (RepairItemId);

create index Idx_ServActDealerV_ServActId on ServiceActivityDealerVehicle (ServiceActivityId);
create index Idx_ServActDealerV_VId on ServiceActivityDealerVehicle (VehicleId);
create index Idx_ServActDealerV_DealerId on ServiceActivityDealerVehicle (DealerId);

create index Idx_ServActVehicleL_ServActId on ServiceActivityVehicleList (ServiceActivityId);
create index Idx_ServActVehicleL_VId on ServiceActivityVehicleList (VehicleId);

create index Idx_ServActQuota_ServActId on ServiceActivityQuota (ServiceActivityId);
create index Idx_ServActQuota_DealerId on ServiceActivityQuota (DealerId);

create index Idx_RClassification_BranchId on RepairClassification (BranchId);
create index Idx_RClassification_PSCategId on RepairClassification (PartsSalesCategoryId);

create index Idx_RObject_PSCategoryId on RepairObject (PartsSalesCategoryId);
create index Idx_RItem_RepairItemCategoryId on RepairItem (RepairItemCategoryId);

create index Idx_RItemBrandRel_BranchId on RepairItemBrandRelation (BranchId);
create index Idx_RItemBrandRel_RItemId on RepairItemBrandRelation (RepairItemId);
create index Idx_RItemBrandRel_PSCategId on RepairItemBrandRelation (PartsSalesCategoryId);


create index Idx_ServActRItem_ServActId on ServiceActivityRepairItem (ServiceActivityId);
create index Idx_ServActRItem_RepairItemId on ServiceActivityRepairItem (RepairItemId);

create index Idx_ServActDealerV_ServActId on ServiceActivityDealerVehicle (ServiceActivityId);
create index Idx_ServActDealerV_VId on ServiceActivityDealerVehicle (VehicleId);
create index Idx_ServActDealerV_DealerId on ServiceActivityDealerVehicle (DealerId);

create index Idx_ServActVehicleL_ServActId on ServiceActivityVehicleList (ServiceActivityId);
create index Idx_ServActVehicleL_VId on ServiceActivityVehicleList (VehicleId);

create index Idx_ServActQuota_ServActId on ServiceActivityQuota (ServiceActivityId);
create index Idx_ServActQuota_DealerId on ServiceActivityQuota (DealerId);

create index Idx_RClassification_BranchId on RepairClassification (BranchId);
create index Idx_RClassification_PSCategId on RepairClassification (PartsSalesCategoryId);

create index Idx_RObject_PSCategoryId on RepairObject (PartsSalesCategoryId);
create index Idx_RItem_RepairItemCategId on RepairItem (RepairItemCategoryId);

create index Idx_RItemBrandR_BranchId on RepairItemBrandRelation (BranchId);
create index Idx_RItemBrandR_RepairItemId on RepairItemBrandRelation (RepairItemId);
create index Idx_RItemBrandR_PSCategId on RepairItemBrandRelation (PartsSalesCategoryId);

create index Idx_RItemAffiServPL_BranchId on RepairItemAffiServProdLine (BranchId);
create index Idx_RItemAffiServPL_RItemId on RepairItemAffiServProdLine (RepairItemId);
create index Idx_RItemAffiServPL_ServPLId on RepairItemAffiServProdLine (ServiceProductLineId);

create index Idx_RItemProdLineH_ServPLId on RepairItemProdLineHistory (ServiceProductLineId);
create index Idx_RItemProdLH_RIAffiServPLId on RepairItemProdLineHistory (RepairItemAffiServProdLineId);
create index Idx_RItemProdLineH_RItemId on RepairItemProdLineHistory (RepairItemId);
create index Idx_RItemProdLineH_BranchId on RepairItemProdLineHistory (BranchId);

create index Idx_RItemCateg_LNodeTypeId on RepairItemCategory (LayerNodeTypeId);
create index Idx_RItemCateg_ParentId on RepairItemCategory (ParentId);
create index Idx_RItemCateg_RootGroupId on RepairItemCategory (RootGroupId);


create index Idx_RItemCAffiV_VCategoryIdon on RepairItemCatAffiVehiCat (VehicleCategoryId);
create index Idx_RItemCAffiV_RItemCategId on RepairItemCatAffiVehiCat (RepairItemCategoryId);

create index Idx_PWarrantyCategory_BranchId on PartsWarrantyCategory (BranchId);

create index Idx_PWarrantyTerm_WPolicyId on PartsWarrantyTerm (WarrantyPolicyId);
create index Idx_PWarrantyTerm_PWCategId on PartsWarrantyTerm (PartsWarrantyCategoryId);

create index Idx_PWarrantyTermH_PWTermId on PartsWarrantyTermHistory (PartsWarrantyTermId);
create index Idx_PWarrantyTermH_PWCategId on PartsWarrantyTermHistory (PartsWarrantyCategoryId);
create index Idx_PWarrantyTermH_WPolicyId on PartsWarrantyTermHistory (WarrantyPolicyId);

create index Idx_PMCostR_PManagCostGradeId on PartsManagementCostRate (PartsManagementCostGradeId);

create index Idx_PMCostGrade_BranchId on PartsManagementCostGrade (BranchId);
create index Idx_PMCostGrade_PSCategId on PartsManagementCostGrade (PartsSalesCategoryId);

create index Idx_ApplicationType_BranchId on ApplicationType (BranchId);
--车辆信息与客户档案
create index Idx_RetainedCust_CustId on RetainedCustomer (CustomerId);


create index Idx_RetCustExtended_RetCustId on RetainedCustomerExtended (RetainedCustomerId);
create index Idx_RetCustExtended_TemplateId on RetainedCustomerExtended (TemplateId);

create index Idx_RetCustVL_RetainedCustIdon on RetainedCustomerVehicleList (RetainedCustomerId);
create index Idx_RetCustVehicleL_VId on RetainedCustomerVehicleList (VehicleId);

create index Idx_VWarrantyCard_VId on VehicleWarrantyCard (VehicleId);
create index Idx_VWarrantyCard_VCategId on VehicleWarrantyCard (VehicleCategoryId);
create index Idx_VWarrantyCard_ServPLId on VehicleWarrantyCard (ServiceProductLineId);
create index Idx_VWarrantyCard_ProductId on VehicleWarrantyCard (ProductId);
create index Idx_VWarrantyCard_RetCustId on VehicleWarrantyCard (RetainedCustomerId);
create index Idx_VWarrantyCard_WPolicyId on VehicleWarrantyCard (WarrantyPolicyId);

create index Idx_VMaintPoil_VWarrantyCardId on VehicleMaintenancePoilcy (VehicleWarrantyCardId);
create index Idx_VMaintPoilcy_VMaintTermId on VehicleMaintenancePoilcy (VehicleMainteTermId);

create index Idx_VMaintePoH_VMaintPoId on VehicleMaintePoilcyHistroy (VehicleMaintenancePoilcyId);
create index Idx_VMaintePoH_VWarrantyCardId on VehicleMaintePoilcyHistroy (VehicleWarrantyCardId);
create index Idx_VMaintePoH_VMainteTermId on VehicleMaintePoilcyHistroy (VehicleMainteTermId);

create index Idx_Cust_RegionId on Customer (RegionId);

create index Idx_VLinkman_RetainedCusId on VehicleLinkman (RetainedCustomerId);
create index Idx_VLinkman_VInformationId on VehicleLinkman (VehicleInformationId);
--视图表
create index Idx_Product_EngineTypeCode on Product (EngineTypeCode);





create index IDX_PSCENTERLINK_personnelid on PERSONSALESCENTERLINK (personid);

create index IDX_RPCLAIMBILL_CREATETIME on REPAIRCLAIMBILL (createtime);

create index INTEGRALCLAIMBILL_rid on INTEGRALCLAIMBILL (repairorderid);