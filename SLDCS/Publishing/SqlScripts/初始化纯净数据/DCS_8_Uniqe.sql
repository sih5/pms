delete CUSTOMERINFORMATION where id in  (select max(id) from CUSTOMERINFORMATION group by SALESCOMPANYID, CUSTOMERCOMPANYID,Status  having count(*)>1 );
alter table CUSTOMERINFORMATION add constraint U_CusInfo_GroupId unique (SALESCOMPANYID, CUSTOMERCOMPANYID,Status);
create unique index Unique_SalesUnit_PSIOCI on SALESUNIT (ownercompanyid, partssalescategoryid);
delete SERVPRODLINEPRODUCTDETAIL where id in (select max(id) from SERVPRODLINEPRODUCTDETAIL group by productid having count(*)>1);
alter table SERVPRODLINEPRODUCTDETAIL  add constraint U_ServProdLineProductDetail unique (PRODUCTID);

alter table SALESUNITAFFIWAREHOUSE
  add constraint SALESUNITIDWAREHOUSEIDUNIQUE unique (WAREHOUSEID);


create unique index uk_PARTSBRANCH_pidpscategoryID on PARTSBRANCH (partid, partssalescategoryid);

create unique index UK_VEHINFO_VIN on VEHICLEINFORMATION (VIN);


alter table REPAIRCLAIMBILL
  add constraint uk_REPAIRCLAIMBILL unique (CLAIMBILLCODE);
  
  alter table SERVICETRIPCLAIMBILL
  add constraint uk_SERVICETRIPCLAIMBILL unique (CODE);
  
  alter table PRESALECHECKORDER
  add constraint uk_PRESALECHECKORDER unique (CODE);
  
  alter table EXPENSEADJUSTMENTBILL
  add constraint uk_EXPENSEADJUSTMENTBILL unique (CODE);


  alter table repairorder
  add constraint uk_repairorder unique (CODE);
  
  

  alter table partsclaimorder
  add constraint uk_partsclaimorder unique (CODE);
  

alter table DEALERINVOICEINFORMATION
  add constraint UK_DEALERINVOICEINFORMATION unique (INVOICENUMBER, INVOICECODE);
  
  