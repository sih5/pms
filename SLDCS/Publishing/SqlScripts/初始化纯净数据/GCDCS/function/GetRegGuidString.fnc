create or replace function GetRegGuidString return varchar2

is
  tempStr varchar2(40);
begin
  tempStr:=sys_guid();
  return '{'||SUBSTR(tempStr,1,8)||'-'||SUBSTR(tempstr,9,4)||'-'||SUBSTR(tempstr,13,4)||'-'||SUBSTR(tempStr,17,4)||'-'||SUBSTR(tempStr,21,12)||'}';
end;
/
