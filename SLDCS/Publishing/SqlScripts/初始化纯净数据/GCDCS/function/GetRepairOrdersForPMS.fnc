create or replace function GetRepairOrdersForPMS
(
  PVIN  char
) return sys_refcursor as
  result    sys_refcursor;
begin
    open result for
    select tRepmaterials.MATERIALCODE 配件图号,
       tRepmaterials.MATERIALNAME 配件名称,
       TREPMATERIALS.AMOUNT 数量,
       decode(TREPMATERIALS.ISTRANSPORT,0,'否',1,'是') 是否发运,
       TREPMATERIALS.SPAREPRICE 配件维修价,
       TREPMATERIALS.BSPAREPRICE 调整后配件维修价,
      -- TREPMATERIALS.SETTLEPROPAS 结算属性,
       TREPMATERIALS.OLDPARTCODE 旧件图号,
       '<' || TREPMATERIALS.SECURITYCODE || '>' as  防伪编码,
       TREPMATERIALS.BARCODE 旧件条码,
       TREPMATERIALS.ISORIGINAL 是否装车件,
       TREPMATERIALS.BATCHCODE 新件批次号,
       TREPMATERIALS.SPEC 规格型号,
       TREPMATERIALS.SUPPLYCODE 旧件供应商编号,
       TREPMATERIALS.SUPPLYNAME 旧件供应商名称,
       TREPITEMS.WORKITEMCODE 维修项目编号,
       TREPITEMS.WORKITEMNAME 维修项目名称,
       decode(TREPITEMS.SETTLEPROP,1,'索赔',2,'免费',3,'自费',4 ,'返修') as 结算属性,
       TREPITEMS.BWORKHOUR 调整后工时,
       TREPITEMS.FINISHDATE 完成时间,
       TREPITEMS.WORKHOUR 工时,
       TREPFAULTREASONS.FAULTREASONCODE 故障原因编码,
       TREPFAULTREASONS.FAULTREASONNAME 故障原因名称,
       decode (TREPFAULTREASONS.FREASONSPTYPE,0,'配件索赔',1,'装车件索赔',2,'非索赔') 故障索赔类型,
       TREPFAULTREASONS.CAUSEMCODE 祸首件图号,
       TREPFAULTREASONS.CAUSEMNAME 祸首件名称,
       TREPFAULTREASONS.MEMO as 故障原因备注信息,
       TREPCONTRACTS.CODE 维修单编号,
     --  TREPCONTRACTS.ENTERCODE,
       TREPCONTRACTS.ISVALID 单据状态,
       TREPCONTRACTS.SERIALCODE 出厂编号,
       TREPCONTRACTS.RANGE 国家规定标准里程,
       TREPCONTRACTS.REPAIRPROP 维修属性,
       decode(TREPCONTRACTS.OLDDEALTYPE,1,'客户带走',2,'服务站处理',3,'其他') 旧件处理方式,
     --  TREPCONTRACTS.WORKFEE,
     --  TREPCONTRACTS.MATFEE,
     --  TREPCONTRACTS.FMATFEE,
     --  TREPCONTRACTS.OTHERFEE,
     --  TREPCONTRACTS.ALLFEE,
       TREPCONTRACTS.CARTHING 随车物件,
       TREPCONTRACTS.CARACTUALITY 车况描述,
       TREPCONTRACTS.FAULTINFO 故障现象及原因描述,
       TREPCONTRACTS.CAUSEANALYSE 外出原因,
       TREPCONTRACTS.DEALMETHOD 处理方法,
       TREPCONTRACTS.DEALRESULT 处理结果,
       TREPCONTRACTS.RELATEDEXPLAIN 连带责任说明,
      -- TREPCONTRACTS.MONITORID,
       decode(TREPCONTRACTS.SPSETTLESTATUS,1,'无索赔',2,'产生索赔' ,4,'全部生成索赔单')保用服务状态,
       TREPCONTRACTS.FAULTANALYSIS 客户意见,
       TREPCONTRACTS.REPBEGINTIME 报修时间,
       TREPCONTRACTS.FAITHFINTIME 预定交车时间,
       TREPCONTRACTS.OUTPLACE 外出救援地,
       TREPCONTRACTS.OUTKM 结算里程,
       TREPCONTRACTS.GOTIME 出发时间,
       TREPCONTRACTS.RETURNTIME 返回时间,
       TREPCONTRACTS.OUTPERSONCODE 外出人员编号,
       decode(TREPCONTRACTS.ISSELFCAR,1,'否',2,'是') 是否使用自备车,
       TREPCONTRACTS.MEMO 备注信息,
       TREPCONTRACTS.CREATETIME 单据创建时间,
       TREPCONTRACTS.CREATORCODE 单据创建人编号,
    --   TREPCONTRACTS.MODIFYTIME,
    --   TREPCONTRACTS.MODIFIERCODE,
       TREPCONTRACTS.ENROLLNUM 车牌号,
       TREPCONTRACTS.WORKFEESTAND 工时费单价,
       TREPCONTRACTS.OTHERFEEREASON 其他费用产生原因,
    --   TREPCONTRACTS.ISCALLED,
    --   TREPCONTRACTS.CONTENTDEGREE,
    --   TREPCONTRACTS.CALLOPINION,
     --  TREPCONTRACTS.BRANDID,
       decode(TREPCONTRACTS.ISSPAPPLY,0,'否',1,'是') 是否包括重大或优惠索赔,
       TREPCONTRACTS.CUSTNAME 客户姓名,
       TREPCONTRACTS.CUSTPHONE 固定电话,
       TREPCONTRACTS.CUSTMOBILE 移动电话,
       TREPCONTRACTS.RUNRANGE 运行里程,
       TREPCONTRACTS.QBTIME 强保次数,
      -- TREPCONTRACTS.ISROADVEHICLE,
      -- TREPCONTRACTS.BRIDGETYPE,
      -- TREPCONTRACTS.ISNEEDSP,
      -- TREPCONTRACTS.ISOUTSP,
      decode(tRepContracts.ADDITIONAL,1,'驾送维修',2,'经销商库存车维修',3,'经销商接车维修') AS 维修附加属性,
     --  TREPCONTRACTS.RECENTREPTIME,
     --  TREPCONTRACTS.REPINTERVAL,
       TREPCONTRACTS.REPBEGINTIME as  故障发生时间,
       TPURCHASEINFOS.PRODUCT_TYPE_NAME 车型,
       TPURCHASEINFOS.ENGINE_MODEL 发动机型号,
       TPURCHASEINFOS.ENGINE_CODE 发动机编号,
       TPURCHASEINFOS.VINCODE vin码,
       TPURCHASEINFOS.LEAVE_FACTORY_DATE 生产日期,
       TPURCHASEINFOS.SALES_DATE 购买日期,
       TPURCHASEINFOS.MILE 首次故障里程,
       TREPCONTRACTS.CUSTNAME as 联系人姓名,
       TPURCHASELINKERS.ADDRESS 联系人地址,
       D1.Name as  事业部,
       D2.code as  外出责任单位编号,
       D2.Name as  外出责任单位名称,
       D3.code as  祸首件供应商编号,
  --     D3.Name as DutyUnitName,
       TAPPLYREPORTS.Code As 索赔单编号,
       TCUSTREGISTERS.Code As 来客登记单编号,
       TSERVERPLOYSETS.Code As  服务活动编号,
       tstations.code as  服务站编号,
       tstations.name as  服务站名称
  from FT.TREPFAULTREASONS@oservice_65
  left JOIN FT.TREPITEMS@oservice_65 ON TREPITEMS.PARENTID = FT.TREPFAULTREASONS.OBJID
  left JOIN FT.TREPMATERIALS@oservice_65 ON TREPMATERIALS.PARENTID = FT.TREPITEMS.OBJID
 inner JOIN FT.TREPCONTRACTS@oservice_65 ON TREPFAULTREASONS.PARENTID =
                                FT.TREPCONTRACTS.OBJID
 inner JOIN FT.TPURCHASEINFOS@oservice_65 ON TREPCONTRACTS.SERIALCODE =
                                 FT.TPURCHASEINFOS.LEAVE_FACTORY_CODE
  left JOIN FT.TPURCHASELINKERS@oservice_65 ON TPURCHASEINFOS.OBJID =
                                   TPURCHASELINKERS.PARENTID
                               AND TREPCONTRACTS.CUSTNAME =
                                   TPURCHASELINKERS.NAME
 inner join ft.tstations@oservice_65 on trepcontracts.entercode = tstations.objid
  left join ft.tstationlists@oservice_65 on tstations.objid = tstationlists.stationid
                            and trepcontracts.brandid =
                                tstationlists.brandid
 inner join ft.tdutyunits@oservice_65 d1 on trepcontracts.ProductComID = d1.objid
  left join ft.tdutyunits@oservice_65 d2 on trepcontracts.dutyunitid = d2.objid
  left join ft.tdutyunits@oservice_65 d3 on trepfaultreasons.dutyunitid = d3.objid
  Left Join ft.TAPPLYREPORTLINKS@oservice_65 On tRepcontracts.ObjID =
                                    TAPPLYREPORTLINKS.RepContractID
  Left Join ft.TAPPLYREPORTS@oservice_65 on TAPPLYREPORTLINKS.ApplyReportID =
                                TAPPLYREPORTS.ObjID
 inner Join ft.TCUSREGREPCONLINKS@oservice_65 On trepcontracts.ObjID =
                                     TCUSREGREPCONLINKS.RepContractID
 inner Join ft.TCUSTREGISTERS@oservice_65 On TCUSREGREPCONLINKS.CustRegID =
                                 TCUSTREGISTERS.ObjID
  Left Outer Join ft.TSEVPLOYSETREPCONLINKS@oservice_65 On trepcontracts.ObjID =
                                               TSEVPLOYSETREPCONLINKS.RepContractID
  Left Outer Join ft.TSERVERPLOYSETS@oservice_65 On TSEVPLOYSETREPCONLINKS.ServerPloySetID =
                                        TSERVERPLOYSETS.ObjID
 where 1 = 1
 /*  And "TREPCONTRACTS".BrandID = '{DBE00437-FE9F-426C-B090-AD1F4EC317D3}'
   And "TREPCONTRACTS".ProductLineID =
       '{5645FF9E-8EB7-4C5F-8B06-EAA4D060BB81}'*/
   And TREPCONTRACTS.createtime >= TO_DATE('2013-1-1', 'yyyy:MM:DD') ;
  -- And "TREPCONTRACTS".createtime < TO_DATE('2014-4-11', 'yyyy:MM:DD');
  return(result);
end GetRepairOrdersForPMS;
/
