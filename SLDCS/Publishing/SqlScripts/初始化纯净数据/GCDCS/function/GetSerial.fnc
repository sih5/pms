create or replace function GetSerial(FTemplateId integer, FCreateDate date, FCorpCode varchar2) return integer
as
  PRAGMA AUTONOMOUS_TRANSACTION;
  FId integer;
  FSerial integer;
begin
  begin
    if FCorpCode is null then
      select id,serial into FId,FSerial from CodeTemplateSerial
       where TemplateId = FTemplateId and CreateDate = FCreateDate and CorpCode is null and rownum<2
         for update;
    else
      select id,serial into FId,FSerial from CodeTemplateSerial
       where TemplateId = FTemplateId and CreateDate = FCreateDate and CorpCode = FCorpCode and rownum<2
         for update;
    end if;
    FSerial:=FSerial+1;
    UPDATE CodeTemplateSerial SET Serial = FSerial WHERE Id = FId;
    commit;
  exception
    when NO_DATA_FOUND then
      FSerial:=1;
      INSERT INTO CodeTemplateSerial(Id ,TemplateId, CreateDate, CorpCode, Serial)
      VALUES(S_CodeTemplateSerial.Nextval, FTemplateId, FCreateDate, FCorpCode, FSerial);
      commit;
    when others then
      rollback;
      raise;
  end;
  return FSerial;
end;
/
