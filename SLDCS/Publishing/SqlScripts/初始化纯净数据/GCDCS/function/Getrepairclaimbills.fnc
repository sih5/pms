create or replace function Getrepairclaimbills
(
  PVIN  char
) return sys_refcursor as
  result    sys_refcursor;
begin
    open result for
      select v1."索赔单编号",v1."单据状态",v1."市场部",v1."申请单编号",v1."索赔单类型",v1."故障原因编码",v1."客户意见",v1."是否已回访",v1."回访意见",v1."服务科审核意见",v1."材料费",v1."配件管理费",v1."工时费",v1."索赔类型",v1."维修索赔备注信息",v1."创建日期",v1."创建人编号",v1."分公司审核时间",v1."故障原因名称",v1."星级系数",v1."是否需要向事业部索赔",v1."客户姓名",v1."客户电话",v1."客户手机",v1."祸首件图号",v1."祸首件名称",v1."供应商确认状态",v1."供应商审核意见",v1."外出车费单价",v1."外出路途补助单价",v1."外出公里数",v1."高速公路拖车费",v1."外出人数",v1."外出天数",v1."外出差旅费用合计",v1."外出救援地",v1."外出索赔备注信息",v1."维修属性",v1."出厂编号",v1."品牌名称",v1."产品线名称",v1."报修时间",v1."运行里程",v1."故障现象及原因描述",v1."维修附加属性",v1."福康工时单价",v1."祸首件生产厂家",v1."祸首件标记",v1."祸首件总成生产厂家",v1."祸首件总成标记",v1."车型",v1."发动机型号",v1."发动机编号",v1."VIN码",v1."祸首件总成名称",v1."祸首件总成图号",v1."祸首件流水号",v1."购买日期",v1."首次故障里程",v1."生产日期",v1."服务站编号",v1."服务站名称",v1."索赔单位事业部编号",v1."工时单价",v1."索赔单位事业部名称",v1."客户地址",v1."责任单位供应商编号",v1."责任单位供应商名称",
       nvl(工时费,0) + nvl(材料费,0) + nvl(外出差旅费用合计,0) as 维修索赔费用合计
  from (select --tbyservicecards.objid,
               tbyservicecards.code 索赔单编号,
               tbyservicecards.ISVALID 单据状态,
               tmarkets.name as 市场部,
               (select TAPPLYREPORTS.Code
                  from TAPPLYREPORTS@oservice_65
                 inner join TAPPLYREPORTLINKS@oservice_65
                    on TAPPLYREPORTLINKS.ApplyReportID = TAPPLYREPORTS.ObjID
                 where tRepContracts.ObjID = TAPPLYREPORTLINKS.RepContractID) as 申请单编号,
               decode(tbyservicecards.SERVICECARDTYPE,1,'维修类',2,'强保类') as  索赔单类型,
               tbyservicecards.FAULTREASONCODE 故障原因编码,
               tbyservicecards.FAULTDES 客户意见,
               decode(tbyservicecards.ISFEEDBACK,1,'否',2,'是')as 是否已回访,
               tbyservicecards.FEEDBACKIDEA 回访意见,
               tbyservicecards.EXAMIDEA 服务科审核意见,
               tbyservicecards.MATFEE 材料费,
               case tbyservicecards.SERVICECARDTYPE
                         when 1 then
                          ServiceMagFee
                         When 2 then
                          0
                       end as  配件管理费,
               case tbyservicecards.SERVICECARDTYPE
                         when 1 then
                          tbyservicecards.spworkfee
                         When 2 then
                          tbyservicecards.workfee
                       end as  工时费,
               decode(tbyservicecards.SPTYPE,0,'配件索赔',1,'装车件索赔',2,'非索赔') as 索赔类型,
               tbyservicecards.MEMO 维修索赔备注信息,
               tbyservicecards.CREATETIME 创建日期,
               tbyservicecards.CREATORCODE 创建人编号,
               tbyservicecards.ConfirmTime as 分公司审核时间,
              -- tbyservicecards.CHECKMANCODE,
               tbyservicecards.FAULTREASONNAME 故障原因名称,
               tbyservicecards.LEVELS_QUOTIETY 星级系数,
               decode(tbyservicecards.ISDUTYSP ,0,'否',1,'是' )as 是否需要向事业部索赔,
              -- tbyservicecards.TOTALFEE as SPTOTALFEE,
               tbyservicecards.CUSTNAME 客户姓名,
               tbyservicecards.CUSTPHONE 客户电话,
               tbyservicecards.CUSTMOBILE 客户手机,
               tbyservicecards.CAUSEMCODE 祸首件图号,
               tbyservicecards.CAUSEMNAME 祸首件名称,
            --   tbyservicecards.ISREPEATREP,
               decode(tbyservicecards.SUPPLIERSTATE,0,'仍未确认',1,'确认已通过',2,'确认未通过') as 供应商确认状态,
               tbyservicecards.CONFIRMIDEA 供应商审核意见,
              -- FT.TOUTSETTLES.ISPRIVATECAR,
               tOutSettles.OUTFARE 外出车费单价,
               tOutSettles.OUTSUBSIDY 外出路途补助单价,
               tOutSettles.OUTKMS 外出公里数,
               tOutSettles.TICKETFEE 高速公路拖车费,
               tOutSettles.SERVICEMENS 外出人数,
               tOutSettles.SERVICEDAYS 外出天数,
               nvl(tOutSettles.TOTALFEE, 0) as 外出差旅费用合计,
               tOutSettles.OUTPLACE 外出救援地,
               tOutSettles.Memo as  外出索赔备注信息,
               tRepContracts.REPAIRPROP 维修属性,
               tRepContracts.SERIALCODE 出厂编号,
               tBrands.NAME AS 品牌名称,
               tProDuctLines.NAME AS 产品线名称,
             --  TREPCONTRACTS.OBJID AS REPID,
               tRepContracts.RepBeginTime 报修时间,
               tRepContracts.runrange 运行里程,
               tRepContracts.FaultInfo 故障现象及原因描述,
               decode(tRepContracts.ADDITIONAL,1,'驾送维修',2,'经销商库存车维修',3,'经销商接车维修') AS 维修附加属性,
               decode(tByServiceCards.iskms, 1, tRepContracts.WORKFEESTANDKMS, 0) as 福康工时单价,
               tbyservicecards.C_P_Manufacturers as  祸首件生产厂家,
               tbyservicecards.C_Markings as  祸首件标记,
               tbyservicecards.C_P_A_Manufacturers as  祸首件总成生产厂家,
               tbyservicecards.C_A_Markings as  祸首件总成标记,
               TPURCHASEINFOS.PRODUCT_TYPE_NAME 车型,
               TPURCHASEINFOS.ENGINE_MODEL 发动机型号,
               TPURCHASEINFOS.ENGINE_CODE 发动机编号,
               TPURCHASEINFOS.VINCODE vin码,
               tbyservicecards.C_P_A_NAME AS 祸首件总成名称 ,
               tbyservicecards.C_P_A_CODE AS 祸首件总成图号 ,
               tbyservicecards.C_P_NUMBER AS 祸首件流水号,
               TPuRCHASEINFOS.Sales_Date 购买日期,
               TPuRCHASEINFOS.mile 首次故障里程,
               TPURCHASEINFOS.Leave_Factory_Date 生产日期,
               tstations.code as  服务站编号,
               tstations.name as  服务站名称,
               D1.Code as "索赔单位事业部编号",
               tRepContracts.WORKFEESTAND AS 工时单价,
               D1.Name as "索赔单位事业部名称",
               (select TPURCHASELINKERS.address
                  from ft.TPURCHASELINKERS@oservice_65
                 where TPURCHASEINFOS.ObjID = TPURCHASELINKERS.parentid) 客户地址,
               (select D2.Code
                  from ft.TDUTYUNITS@oservice_65 D2
                 where D2.ObjID = tbyservicecards.dutyunitid) as "责任单位供应商编号",
               (select D2.Name
                  from ft.TDUTYUNITS@oservice_65 D2
                 where D2.ObjID = tbyservicecards.dutyunitid) as "责任单位供应商名称"
          from ft.tByServiceCards@oservice_65
         inner join ft.tstations@oservice_65
            on tbyservicecards.entercode = tstations.objid
         inner join ft.tstationlists@oservice_65
            on tstations.objid = tstationlists.stationid
           and tbyservicecards.brandid = tstationlists.brandid
         Inner join ft.TMARKETS@oservice_65
            ON tstationlists.MARKETID = TMARKETS.OBJID
         inner join ft.tBYSerCardRepConLinks@oservice_65
            on tByServiceCards.objid = tBYSerCardRepConLinks.BYServiceCardID
         inner join ft.tRepContracts@oservice_65
            on tBYSerCardRepConLinks.RepContractID = tRepContracts.ObjID
          left join ft.tOutSettles@oservice_65
            on tbyservicecards.objid = tOutSettles.byserviceid
           and tOutSettles.isvalid > 0
         inner join ft.tBrands@oservice_65
            on tBYServiceCards.BrandID = tBrands.ObjID
         inner join ft.tProductLines@oservice_65
            on tBYServiceCards.ProductLineID = tProductLines.ObjID
         inner join ft.TPURCHASEINFOS@oservice_65
            on tRepContracts.SerialCode = TPURCHASEINFOS.LEAVE_FACTORY_CODE
         inner join ft.tDutyUnits@oservice_65 d1
            on tbyservicecards.SPunitID = d1.objid
         where 1 = 1 and TPURCHASEINFOS.VINCODE=pvin and tByServiceCards.createtime>=to_date('2013-1-1','yyyy-mm-dd')) v1
    ;
  return(result);
end Getrepairclaimbills;
/
