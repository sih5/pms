CREATE OR REPLACE FUNCTION SHA1(passwd IN VARCHAR2)
      RETURN VARCHAR2
      IS
        retval varchar2(50);
      BEGIN
        retval := dbms_crypto.hash(
                        src=>utl_i18n.string_to_raw(passwd,'AL32UTF8'),
                        typ=>dbms_crypto.HASH_SH1);
        RETURN retval;
      END;
/
