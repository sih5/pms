create or replace function str_to_date(pv_str in varchar2) return date is
ld_date date;
ex_01861 exception;
pragma exception_init(ex_01861,-1861);
begin
  return to_date(pv_str,'yyyy-mm-dd');
  exception
    when ex_01861 then
      return to_date('1990-01-01','yyyy-mm-dd');
      when others then
        return to_date('1990-01-01','yyyy-mm-dd');
  end;
/
