create or replace procedure AutoApproveClaimBill as

  IsAssociatedApplication number(9);
  IsNoMaterialFee number(9);
  ServiceFee float;
  RepeatedRepairTime number(9);
  ClaimTimeOutLong number(9);
  Capacity number(9);
  WorkingHours number(9);
  Mileage number(9);
  hasAutoApproveStrategy number(9);
  IsRepairOverloadProtection number(9);
  TermCapacity number(9);
  TermWorkingHours number(9);
  TermupMileage number(9);
  TermlowMileage number(9);
  TermupDay  number(9);
  TermlowDay  number(9);
  partTermCapacity number(9);
  partTermWorkingHours number(9);
  partTermMileage number(9);
  partTermDay number(9);
  flagIsture boolean;
  flagPartIsTrue boolean;
  IsContainRepairTerm  number(9);
  IsContainPartTerm  number(9);
  IsContaimRemainTerm Number(9);
  hasPartTerm Number(9);
  hasRepairTerm Number(9);
  hasRemainTerm Number(9);
  hasPartTermevery Number(9);
  hasRepairTermevery Number(9);
  hasRemainTermevery Number(9);
  hasVehicleWarrantyCard number(9);
  hasVideoMobileNumber  number(9);
  RepeatedRepairTimeRC number(9);
  hasRepairFaultReason number(9);
  StrategyId number(9);
  hasDealer number(9);
  hasServiceProductLine number(9);
  hasRepairType number(9);
  hasSupplierId number(9);
  finishtime date;
  QBTIME number(9);
  FForcedMainteTime number(9);
  FOutServiceradii NUMBER(9);
  Foutforce number(9);
  fServiceTripDistance number(9);
  FRepairObject VARCHAR2(10);
  IDealClaimBill NUMBER(9);
  ServiceActivityName varchar2(100);
  begin


declare
  cursor AutoApprove is(
  select id,
         t.autoapprovestatus,
         partssalescategoryid,
         t.vehicleid,
         Capacity,
         WorkingHours,
         Mileage,
         floor(RepairRequestTime - SalesDate) timerange,
         floor(RepairRequestTime - OutOfFactoryDate - 90) timeoutrange,
         FaultyPartsWCId,
         TotalAmount,
         MaterialCost,
         ServiceTripClaimAppId,
         RepairClaimApplicationId,
         vin,
         MalfunctionId,
         dealerid,
         ServiceProductLineId,
         repairtype,
         FaultyPartsSupplierId,
         createtime,
         repairorderid,
         SupplierConfirmStatus,claimtype,VehicleMaintenancePoilcyId,sysdate as time1,branchid,t.repairobjectid,QualityInformationId
    from repairclaimbill t
   where status = 2
     and autoapprovestatus = 0
  );
/*查询车辆信息（车辆信息.车辆Id=维修单.车辆Id）如果车辆信息.行驶里程>维修单.行驶里程且维修单.行驶里程不为空，
则自动审核意见追加“维修单提报行驶里程小于上次维修行驶里程，无法提报维修单”。如果车辆信息.方量>维修单.方量且维修单.方量不为空，
则自动审核意见追加“维修单提报方量小于上次维修方量，无法提报维修单”。如果车辆信息.工作小时>维修单.工作小时且维修单.工作小时不为空，
则自动审核意见追加“维修单提报工作小时小于上次维修工作小时，无法提报维修单”
自动审核状态=不通过*/
  begin
    for S_AutoApprove in AutoApprove loop
      begin
      select Capacity,WorkingHours,Mileage into Capacity,WorkingHours,Mileage from vehicleinformation where id =S_AutoApprove.vehicleid;
      exception
      when NO_DATA_FOUND then
      raise_application_error(-20001,'车辆信息不存在');
      end;
      --里程倒跑
      update repairclaimbill set autoapprovestatus=1 where id=S_AutoApprove.id;
      begin
        select dealerserviceinfo.outserviceradii
          into FOutServiceradii
          from dealerserviceinfo
         where dealerserviceinfo.dealerid = S_AutoApprove.dealerid
           and dealerserviceinfo.status = 1
           and dealerserviceinfo.partssalescategoryid =
               S_AutoApprove.partssalescategoryid;
      exception
        when NO_DATA_FOUND then
          FOutServiceradii := 0;
      end;
      begin
        select branchstrategy.ClaimAutoOutforceStrategy,branchstrategy.IsDealClaimBill
          into Foutforce,IDealClaimBill
          from branchstrategy
         where branchstrategy.branchid = S_AutoApprove.branchid;
      exception
        when NO_DATA_FOUND then
          Foutforce := null;
          IDealClaimBill := null;
      end;
      begin
        select ServiceTripDistance
          into fServiceTripDistance
          from servicetripclaimbill
         where servicetripclaimbill.repairclaimbillid = S_AutoApprove.id and status <>99;
      exception
        when NO_DATA_FOUND then
          fServiceTripDistance := null;
      end;
      if (fServiceTripDistance is not null)and (fServiceTripDistance>FOutServiceradii)    and (Foutforce=1) and(S_AutoApprove.repairtype=2) then
        begin
             update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'外出里程超出服务半径自动审核不通过 '||chr(10),t.autoapprovestatus=2
          ,t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'外出里程超出服务半径自动审核不通过 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10) where id=S_AutoApprove.id;

        end;
      end if;
      if (Mileage>S_AutoApprove.Mileage )and (S_AutoApprove.Mileage is not null) then
        begin
          update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'维修单提报行驶里程小于上次维修行驶里程 '||chr(10),t.autoapprovestatus=2
          ,t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'维修单提报行驶里程小于上次维修行驶里程 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10) where id=S_AutoApprove.id;

       end;
      end if;
      --方量倒跑
       if (Capacity>S_AutoApprove.Capacity )and (S_AutoApprove.Capacity is not null) then
        begin
          update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'维修单提报方量小于上次维修方量 '||chr(10),t.autoapprovestatus=2
          ,t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'维修单提报方量小于上次维修方量 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
       end;
      end if;
      --工作小时倒跑
       if (WorkingHours>S_AutoApprove.WorkingHours )and (S_AutoApprove.WorkingHours is not null) then
        begin
          update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'维修单提报方量小于上次工作小时 '||chr(10),t.autoapprovestatus=2
          ,t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'维修单提报方量小于上次工作小时 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10) where id=S_AutoApprove.id;
       end;
      end if;
      
      /*用车辆VIN码在服务活动管理节点--服务活动类型为“整改”的有效状态的服务活动进行核对，
      如果该车在“服务活动车辆清单”中存在，并且已经使用为否的，则自动审核意见提示“该车未完成XX活动 ”*/      
      if IDealClaimBill = 1 then
      begin
        begin
          select t.name
            into ServiceActivityName
            from ServiceActivity t
           inner join ServiceActivityVehicleList l
              on t.id = l.serviceactivityid
           where t.status = 1
             and t.type = 4 /*整改*/
             and t.startdate <= sysdate
             and t.enddate >= sysdate
             and l.Alreadyused = 0
             and l.vin = S_AutoApprove.VIN
             and rownum = 1;
        exception
          when NO_DATA_FOUND then
            ServiceActivityName := null;
        end;
        if ServiceActivityName is not null then
         begin
           update repairclaimbill t
              set t.autoapprovecomment    = t.autoapprovecomment || '该车VIN码['||S_AutoApprove.VIN||']未完成[' ||
                                            ServiceActivityName || ']活动 ' || chr(10),
                  t.autoapprovestatus     = 2,
                  t.approvecommenthistory = t.ApproveCommentHistory ||
                                            '自动审批意见：' || '该车VIN码['||S_AutoApprove.VIN||']未完成[' ||
                                            ServiceActivityName || ']活动 ' ||
                                            chr(10) || '自动审核时间：' ||
                                            S_AutoApprove.time1 || chr(10)
            where id = S_AutoApprove.id;
         end;
        end if;  
      end;
      end if;  
      
     /* 2.假设返回N条保修卡信息，循环判断每张保修卡，只要有一张通过下述校验，则返回 空：
  1.）查询保修政策（单车保修卡.保修政策Id）如果保修条款为空，返回“没有对应的整车保修条款”
  2.）如果保修政策.含维修条款=true，
  查询整车保修条款（保修政策Id）  ，判断里程在闭区间[里程下限，里程上限]，报修时间-销售时间（天数，不含时分秒）在闭区间[天数下限，天数上限]、方量大于等于方量、工作小时大于等于工作小时则整车没有超保； 则自动审核意见追加"整车保修超保"
  或者
        如果保修政策.含维修条款=true，查询整车保修条款（保修政策Id）  ，判断里程在闭区间[里程下限，里程上限]，报修时间-出厂时间-90（天数，不含时分秒）在闭区间[天数下限，天数上限]、方量大于等于参数.方量、工作小时大于等于参数.工作小时则整车没有超保；则自动审核意见追加"整车保修超保"
自动审核状态=不通过*/
      flagPartIsTrue:=false;
      flagIsture:=false;
      hasPartTerm:=0;
      hasRepairTerm:=0;
      hasRemainTerm:=0;
      begin
             select count(*)
               into hasVehicleWarrantyCard
               from VehicleWarrantyCard v
              where v.vehicleid = S_AutoApprove.vehicleid and v.serviceproductlineid=S_AutoApprove.ServiceProductLineId
                and status=1;
          exception
              when no_data_found then
                update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'单车保修卡不存在 '||chr(10),t.autoapprovestatus=2,
                 t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'单车保修卡不存在 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
              end;

                         declare
              cursor VehicleWarrantyCard is(
              select * from VehicleWarrantyCard v where v.vehicleid=S_AutoApprove.vehicleid and status=1
              );
              begin
           for S_VehicleWarrantyCard in VehicleWarrantyCard loop
             begin
               select warrantypolicy.repairterminvolved,warrantypolicy.partswarrantyterminvolved,warrantypolicy.mainteterminvolved into IsContainRepairTerm,IsContainPartTerm,IsContaimRemainTerm  from warrantypolicy where id=S_VehicleWarrantyCard.Warrantypolicyid;

             select MaxMileage,
                    MinMileage,
                    DaysUpperLimit,
                    DaysLowerLimit,
                    Capacity,
                    WorkingHours,1,1
               into TermupMileage,
                    TermlowMileage,
                    TermupDay,
                    TermlowDay,
                    TermCapacity,
                    TermWorkingHours,hasRepairTerm,hasRepairTermevery
               from VehicleWarrantyTerm t
              where t.warrantypolicyid =
                    S_VehicleWarrantyCard.Warrantypolicyid;
                    exception
                      when no_data_found then begin
                        IsContainRepairTerm:=0;
                        hasRepairTermevery:=0;
                        end;
                        end;

                  if (IsContainRepairTerm=1)and (S_AutoApprove.claimtype=1 ) and(hasRepairTermevery=1) then begin

                    if (S_AutoApprove.timerange>=TermlowDay )and (S_AutoApprove.timerange<=TermupDay) then
                      flagIsture:=true;
                      end if;
                    if (S_AutoApprove.timeoutrange>=TermlowDay )and (S_AutoApprove.timeoutrange<=TermupDay) then
                      flagIsture:=true;
                      end if;
                    if (S_AutoApprove.Mileage>=TermlowMileage)and (S_AutoApprove.Mileage<=TermupMileage) then
                      flagIsture:=true;
                      end if;
                    if (TermCapacity>=S_AutoApprove.Capacity)  then
                      flagIsture:=true;
                      end if;
                    if (TermWorkingHours>=S_AutoApprove.WorkingHours)  then
                      flagIsture:=true;
                      end if;
                  end;
                   else
                   flagIsture:=true;
                  end if;


                  begin
                  select WarrantyPeriod,
                         WarrantyMileage,
                         Capacity,
                         WorkingHours,1,1
                    into partTermDay,
                         partTermMileage,
                         partTermCapacity,
                         partTermWorkingHours,hasPartTerm,hasPartTermevery
                    from PartsWarrantyTerm t
                   where t.warrantypolicyid =
                         S_VehicleWarrantyCard.Warrantypolicyid
                     and t.partswarrantycategoryid =
                         S_AutoApprove.FaultyPartsWCId and t.status=1;
                    exception
                      when no_data_found then
                        --update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'配件保修条款不存在 '||chr(10),t.autoapprovestatus=2 where id=S_AutoApprove.id;
                       hasPartTermevery:=0;
                       end;
                  if (IsContainPartTerm =1) and (hasPartTermevery=1)  then  begin
                    if ((partTermMileage>=S_AutoApprove.Mileage)or(S_AutoApprove.Mileage is null))and ((partTermDay>=S_AutoApprove.timerange )or (S_AutoApprove.timerange is null) )and((partTermCapacity>=S_AutoApprove.Capacity)or(S_AutoApprove.Capacity is null)) and((partTermWorkingHours>=S_AutoApprove.WorkingHours)or(S_AutoApprove.WorkingHours is null)) then
                    flagPartIsTrue:=true;
                    end if ;
                  end;
                  else
                    flagPartIsTrue:=true;
                    end if;

                    if S_AutoApprove.claimtype=2 then begin
              begin
              select  MaxMileage,
                    MinMileage,
                    DaysUpperLimit,
                    DaysLowerLimit,
                    Capacity,
                    WorkingHours,1,1
               into TermupMileage,
                    TermlowMileage,
                    TermupDay,
                    TermlowDay,
                    TermCapacity,
                    TermWorkingHours ,hasRemainTerm,hasRemainTermevery from VehicleMaintenancePoilcy where id= S_AutoApprove.VehicleMaintenancePoilcyId;
                    exception
                      when no_data_found then
                        hasRemainTermevery:=0;

                        end;
                         begin
      select REPAIROBJECT.REPAIRTARGET INTO FRepairObject FROM REPAIROBJECT WHERE ID =S_AutoApprove.repairobjectid;
      exception
      when NO_DATA_FOUND then
      raise_application_error(-20001,'维修对象不存在');
      end;
                        if hasRemainTermevery=1 then begin
                          --
                          if FRepairObject='整车' then begin
                    if (S_AutoApprove.timerange<TermlowDay )or (S_AutoApprove.timerange>TermupDay) then
                     update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'整车保养时间超保 '||chr(10),t.autoapprovestatus=2,
                      t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'整车保养时间超保 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
                     end if;
                    if (S_AutoApprove.Mileage<TermlowMileage)or (S_AutoApprove.Mileage>TermupMileage) then
                     update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'整车保养里程超保 '||chr(10),t.autoapprovestatus=2,
                      t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'整车保养里程超保 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
                     end if;
                    if (TermCapacity<S_AutoApprove.Capacity)and  (TermCapacity is not null) then
                     update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'整车方量超保 '||chr(10),t.autoapprovestatus=2,
                      t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'整车方量超保 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
                     end if;
                    if (TermWorkingHours<S_AutoApprove.WorkingHours)and  (TermWorkingHours is not null) then
                     update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'整车工作小时超保 '||chr(10),t.autoapprovestatus=2,
                      t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'整车工作小时超保 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
                     end if;
                     end;
                     end if;
                     if FRepairObject='专项发动机' then begin
                         if (S_AutoApprove.timerange<TermlowDay )or (S_AutoApprove.timerange>TermupDay) then
                     update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'发动机保养时间超保 '||chr(10),t.autoapprovestatus=2,
                      t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'发动机保养时间超保 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
                     end if;
                    if (S_AutoApprove.Mileage<TermlowMileage)or (S_AutoApprove.Mileage>TermupMileage) then
                     update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'发动机保养里程超保 '||chr(10),t.autoapprovestatus=2,
                      t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'发动机保养里程超保 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
                     end if;
                    if (TermCapacity<S_AutoApprove.Capacity)and  (TermCapacity is not null) then
                     update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'发动机方量超保 '||chr(10),t.autoapprovestatus=2,
                      t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'发动机方量超保 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
                     end if;
                    if (TermWorkingHours<S_AutoApprove.WorkingHours)and  (TermWorkingHours is not null) then
                     update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'发动机工作小时超保 '||chr(10),t.autoapprovestatus=2,
                      t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'发动机工作小时超保 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
                     end if;
                     end;
                     end if;

                     --
                     end;
                     end if;
              end;
              end if;

             end loop;
             end;
             --20150610
             /*if hasRemainTerm=0 and S_AutoApprove.repairtype in (3,4) then
             update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'单车保养条款不存在 '||chr(10),t.autoapprovestatus=2,
              t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'单车保养条款不存在 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
               end if;*/

            /* if hasRepairTerm=0 then
               update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'整车保修条款不存在 '||chr(10),t.autoapprovestatus=2,
                t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'整车保修条款不存在 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
               else*/ if hasRepairTerm=1 then begin
             IF flagIsture=FALSE THEN
               update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'车辆整车超保 '||chr(10),t.autoapprovestatus=2,
                t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'车辆整车超保 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10) where id=S_AutoApprove.id;
               end if;
               end ;
               end if;
               /*if hasPartTerm=0 then
               update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'配件保修条款不存在 '||chr(10),t.autoapprovestatus=2,
                t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'配件保修条款不存在 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;

               else*/
               if (hasPartTerm=1) then
                 begin
                    if flagPartIsTrue=false then
                  update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'祸首件超保 '||chr(10),t.autoapprovestatus=2,
                   t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'祸首件超保 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
                   end if;
                   end;
               end if;


        /*
查询保修政策服务产品线关系（保修政策服务产品线关系.服务产品线Id=参数 服务产品线Id ）
查询保修政策（保修政策.Id=步骤1返回的保修政策服务产品线关系.保修政策Id）
判断配件保修条款是否存在，查询配件保修条款（配件保修条款.保修政策Id=步骤2返回的保修政策.Id,配件保修条款.配件保修分类Id in 参数 配件保修分类Id数组）
2.逐条判断保修条款.如果保修时长<参数保修天数  or  方量<参数.方量  or  工作小时<参数.工作小时，则自动审核意见追加：配件保修分类xxx已经过保
如果参数 行驶里程，方量、工作小时为空再步骤2中不纳入判断
如果步骤2中查询不到数据则不作判断
自动审核状态=不通过*/


      begin
      select ServiceFee,--服务费金额,
             IsAssociatedApplication,--是否关联申请,
             IsNoMaterialFee,--是否材料费为0,
             RepeatedRepairTime,--重复维修次数,
             ClaimTimeOutLong, --索赔超时时长
             1,id,ForcedMainteTime
        into ServiceFee,
             IsAssociatedApplication,
             IsNoMaterialFee,
             RepeatedRepairTime,ClaimTimeOutLong,hasAutoApproveStrategy,StrategyId,FForcedMainteTime
        from AutoApproveStrategy t
       where t.partssalescategoryid = S_AutoApprove.partssalescategoryid and status<>99;
       exception
         when no_data_found then
           hasAutoApproveStrategy:=0;
       end;
       if hasAutoApproveStrategy=1 then begin

          if S_AutoApprove.TotalAmount>ServiceFee then
          update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'索赔单服务费金额超过标准 '||chr(10),t.autoapprovestatus=2,
           t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'索赔单服务费金额超过标准 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
          end if;
          --20150610
          --20150804
          if (S_AutoApprove.MaterialCost=0)and (IsNoMaterialFee=1) and (S_AutoApprove.repairtype<>7)then
          update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'索赔单材料费为0 '||chr(10),t.autoapprovestatus=2,
           t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'索赔单材料费为0 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
          end if;

          begin
          select 1
            into hasVideoMobileNumber
            from repairclaimbill
           where id = S_AutoApprove.id
             and VideoMobileNumber is  null and exists
                  (select 1
                      from DealerMobileNumberList
                     where DealerMobileNumberList.Dealerid =
                           repairclaimbill.dealerid);
                         exception
                           when no_data_found then
                             hasVideoMobileNumber:=0;
                             end;
         /*  if (hasVideoMobileNumber=1) and (S_AutoApprove.repairtype<>3)then
             update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'索赔单缺失视频手机号 '||chr(10),t.autoapprovestatus=2,
              t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'索赔单缺失视频手机号 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
             end if; --2015616
*/
           if (IsAssociatedApplication=1)and( (S_AutoApprove.RepairClaimApplicationId is not null)or (S_AutoApprove.QualityInformationId is not null) )then
             update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'关联申请单或市场质量信息快报自动审核不通过 '||chr(10),t.autoapprovestatus=2,
              t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'关联申请单或市场质量信息快报自动审核不通过 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
             end if;

              begin
            select  count(*)into RepeatedRepairTimeRC from repairclaimbill where id<>S_AutoApprove.id and vin=S_AutoApprove.vin and createtime>sysdate-30 and status<>99 and repairtype not in(3,4,7);
            exception
              when no_data_found then
                RepeatedRepairTimeRC:=0;
            end;
           if RepeatedRepairTimeRC> RepeatedRepairTime then
               update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'车辆重复维修 '||chr(10),t.autoapprovestatus=2,
                t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'车辆重复维修 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
               end if;

           begin
           select count(*) into hasRepairFaultReason from AppointFaultReasonDtl where AutoApproveStrategyId=StrategyId and  FaultReasonId=S_AutoApprove.MalfunctionId;
           exception
             when no_data_found then
               hasRepairFaultReason:=0;
               end;
            if  hasRepairFaultReason>0 then
              update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'指定故障原因自动审核不通过 '||chr(10),t.autoapprovestatus=2,
               t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'指定故障原因自动审核不通过  '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
              end if;

           begin
           select count(*) into hasDealer from AppointDealerDtl where AutoApproveStrategyId=StrategyId and  dealerid=S_AutoApprove.dealerid;
           exception
             when no_data_found then
               hasDealer:=0;
               end;
            if  hasDealer>0 then
              update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'指定服务站自动审核不通过 '||chr(10),t.autoapprovestatus=2,
               t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'指定服务站自动审核不通过  '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
              end if;

           begin
           select count(*) into hasServiceProductLine from AppointServiceProductLineDtl where AutoApproveStrategyId=StrategyId and  ServiceProductlineId=S_AutoApprove.ServiceProductlineId;
           exception
             when no_data_found then
               hasServiceProductLine:=0;
               end;
            if  hasServiceProductLine>0 then
              update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'指定服务产品线自动审核不通过 '||chr(10),t.autoapprovestatus=2,
               t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'指定服务产品线自动审核不通过  '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
              end if;

            begin
           select count(*) into hasRepairType from AppointRepairTypeDtl where AutoApproveStrategyId=StrategyId  and  repairtype=S_AutoApprove.repairtype;
           exception
             when no_data_found then
               hasRepairType:=0;
               end;
            if  hasRepairType>0 then
              update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'指定维修类型自动审核不通过 '||chr(10),t.autoapprovestatus=2,
               t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'指定维修类型自动审核不通过  '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
              end if;

            begin
           select count(*) into hasSupplierId from AppointSupplierDtl where AutoApproveStrategyId=StrategyId  and  SupplierId=S_AutoApprove.FaultyPartsSupplierId;
           exception
             when no_data_found then
               hasSupplierId:=0;
               end;
            if  hasSupplierId>0 then
              update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'指定供应商自动审核不通过 '||chr(10),t.autoapprovestatus=2,
               t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'指定供应商自动审核不通过  '||chr(10)||'自动审核时间：'||S_AutoApprove.time1 ||chr(10) where id=S_AutoApprove.id;
              end if;
         end;
         end if;

         select FinishingTime into finishtime from repairorder where repairorder.id=S_AutoApprove.repairorderid;
         if (floor(to_number(S_AutoApprove.createtime-finishtime)*24)>ClaimTimeOutLong)then
           update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'索赔提报超时 '||chr(10),t.autoapprovestatus=2,
            t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'索赔提报超时 '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
           end if;

         begin
         select count(*) into QBTIME  from   repairorder t where t.vin=S_AutoApprove.vin and status<>99 AND repairtype=3 ;
         exception
           when no_data_found then
             QBTIME:=0;
             end;
             --20150804
         if (qbtime>FForcedMainteTime )and (S_AutoApprove.repairtype=3)then
            update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'车辆强保次数大于'||FForcedMainteTime||'次，自动审核不通过'||chr(10),t.autoapprovestatus=2,
            t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'车辆强保次数大于'||FForcedMainteTime||'次，自动审核不通过'||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
            end if;

         if S_AutoApprove.SupplierConfirmStatus =3 then
            update repairclaimbill t set t.autoapprovecomment=t.autoapprovecomment||'供应商确认不通过'||chr(10),t.autoapprovestatus=2,
            t.approvecommenthistory = t.ApproveCommentHistory || '自动审批意见：'||'供应商确认不通过  '||chr(10)||'自动审核时间：'||S_AutoApprove.time1||chr(10)  where id=S_AutoApprove.id;
            end if;

 /*




查询维修单（vin=维修保养索赔单.vin 维修类型=强制保养，状态<>作废）
如果查询到的维修单数量>2 自动审核意见追加:进行了多次强保，强保次数为<查询到维修单数量>
自动审核状态=不通过


如果维修保养索赔单.供应商确认状态<>已确认，自动审核状态追加：供应商确认不通过或未确认
自动审核状态=不通过


*/

commit;
      end loop;
    end;
  end;
