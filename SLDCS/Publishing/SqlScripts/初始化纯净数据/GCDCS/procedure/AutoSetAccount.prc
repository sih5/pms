create or replace procedure AutoSetAccount is
begin
  insert into tempTableForAccount
  select * from (select COUNTERPARTCOMPANYID,
       CUSTOMERACCOUNTID,
       sum(partofpendingamount) as pendingamount,(select customeraccount.pendingamount from customeraccount where id=CUSTOMERACCOUNTID) pendingamountold
  from (select a.submitcompanyid COUNTERPARTCOMPANYID,
       a.CUSTOMERACCOUNTID,
       (allamount - nvl(reduceamount, 0) - nvl(d.stopamount, 0)-nvl(e.stopSamount,0)) as partofpendingamount
  from  partssalesorder a
 inner join (select t1.originalsalesorderid,
                    sum(nvl(t2.currentfulfilledquantity, 0) * t2.orderprice) as allamount
               from  partssalesorderprocess t1
              inner join  partssalesorderprocessdetail t2
                 on t1.id = t2.partssalesorderprocessid
              group by t1.originalsalesorderid) b
    on a.id = b.originalsalesorderid
  left join (select -- t11.partssalescategoryid,
                    t11.originalrequirementbillcode,
                    sum(t22.outboundamount * t22.settlementprice) as reduceamount
               from  partsoutboundbill t11
              inner join  partsoutboundbilldetail t22
                 on t11.id = t22.partsoutboundbillid
              group by t11.originalrequirementbillcode) c
    on a.code = c.originalrequirementbillcode
  left join (select t111.originalrequirementbillcode,
                    sum((nvl(t222.orderamount, 0) -
                        nvl(t222.shippingamount, 0)) *
                        nvl(t333.orderprice, 0)) stopamount
               from  partspurchaseorder t111
              inner join  partspurchaseorderdetail t222
                 on t111.id = t222.partspurchaseorderid
              inner join  partssalesorderdetail t333
                 on t333.partssalesorderid = t111.originalrequirementbillid
                and t333.sparepartid = t222.sparepartid
                and t111.originalrequirementbilltype = 1
              where t111.status in( 7,99)
              group by t111.originalrequirementbillcode) d
    on a.code = d.originalrequirementbillcode
    left join (select t1111.originalrequirementbillcode,
                    sum((nvl(t2222.quantity, 0) -
                        nvl(t2222.confirmedamount, 0)) *
                        nvl(t3333.orderprice, 0)) stopSamount
    from  SupplierShippingOrder t1111
    inner join  SupplierShippingDetail t2222 on t2222.suppliershippingorderid=t1111.id
    inner join  partssalesorderdetail t3333 on t3333.partssalesorderid = t1111.originalrequirementbillid
                and t3333.sparepartid = t2222.sparepartid
                and t1111.originalrequirementbilltype = 1
                where t1111.status =4
              group by t1111.originalrequirementbillcode) e
    on a.code=e.originalrequirementbillcode
-- and a.partssalesordertypeid = c.partssalescategoryid
 where a.IFDIRECTPROVISION = 1
   --and a.submitcompanyid = 4179
        union all
        select t111.COUNTERPARTCOMPANYID,
               t111.CUSTOMERACCOUNTID,
               Sum(nvl(t222.Plannedamount - nvl(t222.outboundfulfillment, 0),
                       0) * nvl(t222.Price, 0)) as partofpendingamount
          from  partsoutboundplan t111
         inner join  partsoutboundplandetail t222
            on t111.id = t222.partsoutboundplanid
         where status < 4 and OutboundType=1
          -- and t111.counterpartcompanyid = 4179
         group by t111.COUNTERPARTCOMPANYID, t111.CUSTOMERACCOUNTID) ttttt
 group by COUNTERPARTCOMPANYID, CUSTOMERACCOUNTID) where pendingamount<>pendingamountold;



update CUSTOMERACCOUNT
   set pendingamount =
       (select t.pendingamoun
          from  tempTableForAccount t
         where CUSTOMERACCOUNT.id = t.CUSTOMERACCOUNTID
           and t.COUNTERPARTCOMPANYID = CUSTOMERACCOUNT.CUSTOMERCOMPANYID)
 where exists
 (select 1
          from tempTableForAccount t1
         where CUSTOMERACCOUNT.id = t1.CUSTOMERACCOUNTID
           and CUSTOMERACCOUNT.CUSTOMERCOMPANYID = t1.COUNTERPARTCOMPANYID)
   and status = 1;

Update Customeraccount m
   Set Shippedproductvalue = Nvl((Select Amount
                                   From (Select a.Customercompanyid,
                                                a.Accountgroupid,
                                                Sum(Nvl(d.Outboundamount, 0) *
                                                    Nvl(d.Settlementprice, 0)) As Amount
                                           From Customeraccount a
                                          Inner Join Salesunit b
                                             On a.Accountgroupid =
                                                b.Accountgroupid
                                          Inner Join Partsoutboundbill c
                                             On b.Partssalescategoryid =
                                                c.Partssalescategoryid
                                            And a.Id = c.Customeraccountid
                                            And c.Counterpartcompanyid =
                                                a.Customercompanyid
                                          Inner Join Partsoutboundbilldetail d
                                             On c.Id = d.Partsoutboundbillid
                                          Where a.Status = 1
                                            And b.Status = 1
                                            And Not Exists
                                          (Select 1
                                                   From Partssalessettlementref Pssr
                                                  Inner Join Partssalessettlement Pss
                                                     On Pssr.Partssalessettlementid =
                                                        Pss.Id
                                                  Where Pss.Status In (2, 3)
                                                    And Pssr.Sourcecode =
                                                        c.Code)
                                          Group By a.Customercompanyid,
                                                   a.Accountgroupid) n
                                  Where m.Customercompanyid =
                                        n.Customercompanyid
                                    And m.Accountgroupid = n.Accountgroupid),
                                 0)
 Where m.Status = 1;
end AutoSetAccount;
/
