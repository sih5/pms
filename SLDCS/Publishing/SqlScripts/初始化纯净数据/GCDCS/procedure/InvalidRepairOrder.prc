create or replace procedure InvalidRepairOrder as
begin
  update RepairClaimApplication a
     set a.Status        = 99,
         a.Remark        = substrb(a.Remark || ' 超期15天未使用，自动作废 作废人：admin 作废时间 ：' ||
                           sysdate,500),
         a.abandonerid   = 1,
         a.abandonername = 'Admin',
         a.abandontime   = sysdate,
         a.modifierid    = 1,
         a.modifiername  = 'Admin',
         a.modifytime    = sysdate
   where ceil((select sysdate from dual) - a.submittime) > 15
     and not exists (select 1
            from REPAIRORDERFAULTREASON v
            INNER JOIN REPAIRORDER T ON T.ID=V.REPAIRORDERID
           where v.repairclaimapplicationid = a.id
             and T.status <> 99) and status<>99;

  update ServiceTripClaimApplication b
     set b.Status        = 99,
         b.Remark        = substrb(b.Remark || ' 超期15天未使用，自动作废 作废人：admin 作废时间 ：' ||
                           sysdate,500),
         b.abandonerid   = 1,
         b.abandonername = 'Admin',
         b.abandontime   = sysdate,
         b.modifierid    = 1,
         b.modifiername  = 'Admin',
         b.modifytime    = sysdate
   where ceil((select sysdate from dual) - b.submittime) > 15
     and not exists (select 1
            from repairorder v
           where v.servicetripclaimappid = b.id
             and v.status <> 99)and status<>99;

end;
/
