CREATE OR REPLACE PROCEDURE PRO_MoveInvalidPartsExGroup AS
--将互换组下互换号和配件信息数量小于等于一条的互换组信息作废
begin
update PartsExchangeGroup
set status=99,PartsExchangeGroup.Modifierid=1,PartsExchangeGroup.Modifiername='Admin',PartsExchangeGroup.Modifytime=sysdate,
PartsExchangeGroup.Abandonerid=1,PartsExchangeGroup.Abandonername='Admin',PartsExchangeGroup.Abandontime=sysdate
where status<>99 and id in(
select id from(
select eg.id, eg.exgroupcode, count(e.exchangecode)
  from PartsExchangeGroup eg
 left outer join PartsExchange e
    on eg.exchangecode = e.exchangecode
    and e.status <> 99
 group by eg.id,eg.exgroupcode
 having count(e.exchangecode) <=1
 ));
 end PRO_MoveInvalidPartsExGroup;
/
