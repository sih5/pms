create or replace procedure PartsSalesOrderCennel
as
begin
  --根据配件销售订单的品牌获取该品牌对应的销售中心策略的值，如果：状态=新建，当前日期 -创建日期 >策略值，则作废配件销售订单；

   update PartsSalesOrder set Status = 99 ,PartsSalesOrder.AbandonerId=1,PartsSalesOrder.AbandonerName='系统管理员',PartsSalesOrder.AbandonTime=sysdate
   where  id in ( select a.id
   from PartsSalesOrder a
   inner join SalesCenterstrategy c on c.partssalescategoryid=a.salescategoryid
   where a.status=1 and (to_char(sysdate,'yyyymmdd')-to_char(a.createtime,'yyyymmdd'))>c.PartsSalesOrdernDays
   );

end PartsSalesOrderCennel;
/
