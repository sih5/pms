create or replace procedure PrcOrderPlanToOrder
as
  /*自动获取单据编码相关*/
  FDateFormat   date;
  FDateStr      varchar2(8);
  FSerialLength integer;
  FNewCode      varchar2(50);
  FComplateId   integer;
  FSONSerialLength integer;
  FExpectedShippingDate date;
  /*定义信息字段*/
  FMonthOfOrder  integer;
  FYearOfOrder integer;
  FVehicleSalesTypeId integer;
begin

  /*编码规则变更，需同步调整*/
  --FDateFormat:=trunc(sysdate,'YEAR');
  --FDateFormat:=trunc(sysdate,'MONTH');
  FDateFormat   := trunc(sysdate, 'DD');
  FDateStr      := to_char(sysdate, 'yyyymmdd');
  FSerialLength := 4;
  FNewCode      := '{CORPCODE}OR' || FDateStr || '{SERIAL}';

  FSONSerialLength:=4;

  begin
    select MonthOfOrder, YearOfOrder into FMonthOfOrder, FYearOfOrder from VehicleOrderSchedule where trunc(OrderPlanToPODate, 'DD')=trunc(sysdate, 'DD');
  exception
    when NO_DATA_FOUND then
      return;
    when others then
      raise;
  end;

  /*获取编号生成模板Id*/
  begin
    select Id into FComplateId from codetemplate where name = 'VehicleOrder' and IsActived = 1;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20001,'未找到名称为"VehicleOrder"的有效编码规则。');
    when others then
      raise;
  end;

  /*获取整车销售类型*/
  begin
    select VehicleSalesTypeId into FVehicleSalesTypeId from VehicleSalesOrganization where Id=1; --默认销售组织
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20002,'未设置默认销售组织');
    when others then
      raise;
  end;

  FExpectedShippingDate:=add_months(trunc(sysdate,'MONTH'),1);

  declare
    VehicleOrderId integer;
    VehicleOrderCode varchar2(50);
    NoPriceProductCount integer;
    VehicleCustomerAccountId integer;
    FIndex integer;
    FTotalAmount number(19,4);
    cursor vehicleOrderPlans is
      select Id,Code,VehicleSalesOrgId,VehicleSalesOrgCode,VehicleSalesOrgName,DealerId,DealerCode,DealerName from VehicleOrderPlan
       where YearOfPlan =  FYearOfOrder and MonthOfPlan = FMonthOfOrder and Status = 1;
    vehicleOrderPlan vehicleOrderPlans%rowtype;
  begin
    for vehicleOrderPlan in vehicleOrderPlans loop
      begin
        NoPriceProductCount:=0;
        select count(*) into NoPriceProductCount from VehicleOrderPlanDetail
         where VehicleOrderPlanId=vehicleOrderPlan.Id
           and not exists (select * from VehicleWholesalePrice
                            where VehicleWholesalePrice.Status=1
                              and VehicleWholesalePrice.VehicleSalesTypeId=FVehicleSalesTypeId
                              and VehicleWholesalePrice.ProductId=VehicleOrderPlanDetail.ProductId);
        if NoPriceProductCount>0 then
          raise_application_error(-20003,'编号为“' || vehicleOrderPlan.Code || '”的订单计划中存在产品没有对应的整车批发价格');
        end if;

        --获取新增单据Id
        select S_VehicleOrder.NEXTVAL into VehicleOrderId from dual;
        --获取新单据号
        VehicleOrderCode:=regexp_replace(regexp_replace(FNewCode, '{CORPCODE}', vehicleOrderPlan.DealerCode),
          '{SERIAL}',lpad(to_char(GetSerial(FComplateId, FDateFormat, vehicleOrderPlan.DealerCode)),FSerialLength,'0'));
        --获取整车客户账户ID
        begin
          select Id into VehicleCustomerAccountId from VehicleCustomerAccount
           where VehicleFundsTypeId=1
             and CustomerCompanyId=vehicleOrderPlan.VehicleSalesOrgId
             and VehicleAccountGroupId = 1 --默认CAM销售组织
             and Status=1;
        exception
          when NO_DATA_FOUND then
            VehicleCustomerAccountId:=null;
          when others then
            raise;
        end;
        --插入新订单单据
        Insert into VehicleOrder(Id,VehicleSalesOrgId,VehicleSalesOrgCode,VehicleSalesOrgName,
          DealerId,DealerCode,DealerName,Code,VehicleOrderPlanId,VehicleOrderPlanCode,OrderType,
          YearOfPlan,MonthOfPlan,TotalAmount,VehicleFundsTypeId,VehicleCustomerAccountId,Status,CreateTime)
        values(VehicleOrderId,vehicleOrderPlan.VehicleSalesOrgId,vehicleOrderPlan.VehicleSalesOrgCode,vehicleOrderPlan.VehicleSalesOrgName,
          vehicleOrderPlan.DealerId,vehicleOrderPlan.DealerCode,vehicleOrderPlan.DealerName,VehicleOrderCode,vehicleOrderPlan.Id,vehicleOrderPlan.Code,1,
          FYearOfOrder,FMonthOfOrder,0,1,VehicleCustomerAccountId,1,sysdate);

        FIndex:=1;
        FTotalAmount:=0;

        declare
          PlanQty integer;
          Price number(19,4);
          cursor OPD_Cursor is
            select ProductId,ProductCode,ProductName,ProductCategoryCode,ProductCategoryName,PlannedQuantity
              from VehicleOrderPlanDetail
             where VehicleOrderPlanId = vehicleOrderPlan.Id;
          detail OPD_Cursor%rowtype;
        begin
          --根据订货清单中的数据循环插入到订单清单中
          for detail in OPD_Cursor loop
            PlanQty:=detail.PlannedQuantity;
            --获取批发价格
            select VehicleWholesalePrice.Price into Price from VehicleWholesalePrice
             where VehicleWholesalePrice.Status=1
               and VehicleWholesalePrice.VehicleSalesTypeId=VehicleSalesTypeId
               and VehicleWholesalePrice.ProductId=detail.ProductId
               and rownum=1;
            while PlanQty>0 loop
              insert into VehicleOrderDetail (Id,VehicleOrderId, ProductId, ProductCode, ProductName, ProductCategoryCode, ProductCategoryName,
                SON, Status, HasChanged, Price, ExpectedShippingDate)
              values(S_VehicleOrderDetail.Nextval,VehicleOrderId,detail.ProductId,detail.ProductCode,detail.ProductName,detail.ProductCategoryCode,detail.ProductCategoryName,
                VehicleOrderCode||lpad(to_char(FIndex),FSerialLength,'0'),
                1, 0, Price, FExpectedShippingDate);
              PlanQty:=PlanQty-1;
              FIndex:=FIndex+1;
              FTotalAmount:=FTotalAmount+Price;
            end loop;

            --更新订单总金额
            update VehicleOrder set TotalAmount=FTotalAmount where id=VehicleOrderId;

            --更新计划单状态为生效
            update VehicleOrderPlan set Status=3 where id=vehicleOrderPlan.Id;
          end loop;
        end;
      exception
        when others then
          rollback;
          raise;
      end;
      commit;
    end loop;
  end;
end;
/
