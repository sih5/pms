create or replace procedure TimingEmptyCustomerCredence as

  cursor tmpCursor is
    select branchid from Branchstrategy bs
    where IsResetCreditStrategy=1;

  tmpCursorRow  tmpCursor%rowtype;

begin
  open tmpCursor;
    loop

      fetch tmpCursor into tmpCursorRow;
      exit when tmpCursor%notfound;

 -- 查询 分公司策略（是否清除授信策略=是）
-- 关联账户组（分公司策略.营销分公司Id=账户组.销售企业Id）
-- 关联客户账户（客户账户.账户组Id=账户组.Id）
      declare
        cursor Cursor_ca is(
          select ca.Id CustomerAccountId,ca.AccountGroupId,ca.CustomerCompanyId,sc.Code CompanyCode,sc.Name CompanyName from CustomerAccount ca
          inner join AccountGroup ag on ca.accountgroupid=ag.id
          inner join Company sc on ag.salescompanyid=sc.id
          where ag.salescompanyid=tmpCursorRow.branchid
        );

-- 更新客户账户 赋值：客户信用总额=0
       begin
         for S_role in Cursor_ca loop
           update CustomerAccount set CustomerCredenceAmount = 0 where id = S_role.CustomerAccountId;

           insert into CredenceApplication
           (  id,
              Code,
              SalesCompanyId,
              GuarantorCompanyId,
              GuarantorCompanyName,
              AccountGroupId,
              CustomerCompanyId,
              ValidationDate,
              ExpireDate,
              ApplyCondition,
              CredenceLimit,
              Status,
              ConsumedAmount,
              CREATORID             ,
              CREATORNAME          ,
              CREATETIME
           )
            -- 编码 {CA}{销售企业.编号}{YYYYMMDD}{4位=9000+rownum}
            -- 生成信用申请单 赋值：
            -- 信用申请单.销售企业Id=分公司策略.营销分公司Id
            -- 信用申请单.担保企业Id=分公司策略.营销分公司Id
            -- 信用申请单.担保企业名称=企业.企业名称
            -- 信用申请单.账户组Id=客户账户.账户组id
            -- 信用申请单.客户企业Id=客户账户.客户企业id
            -- 信用申请单.生效日期=当前时间
            -- 信用申请单.失效日期=当前时间
            -- 信用申请单.使用条件=自由支配使用
            -- 信用申请单.信用额度=0
            -- 信用申请单.状态=已审批
            -- 信用申请单.已使用金额=0
           values
           (s_CredenceApplication.Nextval,
             regexp_replace(
                regexp_replace('CA{CORPCODE}'||to_char(sysdate,'yyyymmdd')||'{SERIAL}','{CORPCODE}',S_role.CompanyCode),
                '{SERIAL}',
                9000+rownum
              ) ,
            /* 'CA'|| S_role.CompanyCode||CO_CHAR(SYSDATE,'YYYYMMDD')||9000+rownum CODE,*/
             tmpCursorRow.Branchid,
             tmpCursorRow.Branchid,
             S_role.CompanyName,
             S_role.AccountGroupId,
             S_role.CustomerCompanyId,
             sysdate,
             sysdate,
             2,
             0,
             2,
             0,
             1,
             'Admin',
             sysdate
           );
         end loop;
         close Cursor_ca;
       end;

    end loop;
 close tmpCursor;
end;
/
