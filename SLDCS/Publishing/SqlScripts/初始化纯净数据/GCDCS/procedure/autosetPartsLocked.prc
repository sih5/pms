create or replace procedure autosetPartsLocked as
begin 
insert into PartsLockedStocktmp
select a.warehouseid,b.sparepartid,sum(plannedamount) - sum(nvl(b.outboundfulfillment, 0)) lockqty from SDDCS.partsoutboundplan a
 inner join SDDCS.partsoutboundplandetail b on a.id = b.partsoutboundplanid where a.status in (1, 2) group by a.warehouseid, b.sparepartid;



update SDDCS.PartsLockedStock a
  set a.Lockedquantity =
      (select lockqty
         from PartsLockedStocktmp t
        where t.sparepartid = a.Partid
          and t.lockqty <> a.Lockedquantity
          and t.warehouseid = a.warehouseid)
where exists
(select 1 from PartsLockedStocktmp c where c.sparepartid = a.Partid and c.lockqty <> a.Lockedquantity and c.warehouseid = a.warehouseid);
end ;
/
