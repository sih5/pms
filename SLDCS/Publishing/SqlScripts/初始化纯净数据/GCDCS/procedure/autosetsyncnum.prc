create or replace procedure autosetsyncnum as
begin
 
 update GCDCS.PartsOutboundPlan_sync
    set PartsOutboundPlan_sync.Syncnum = PartsOutboundPlan_increase.Nextval
  where billid in
        (select pop.id
           from GCDCS.PartsOutboundPlan pop
          inner join GCDCS.PartsOutboundPlan_sync
             on PartsOutboundPlan_sync.billid = pop.id
          inner join GCDCS.warehouse
             on warehouse.id = pop.Warehouseid
          where ((warehouse.WmsInterface = 1) and
                (pop.OutboundType in (2, 3) or
                (pop.OutboundType = 1 and pop.creatorid > 0) or
                (pop.OutboundType = 4 and
                (exists (select 1
                              from GCDCS.PartsTransferOrder pto
                             inner join GCDCS.Warehouse owt
                                on pto.OriginalWarehouseId = owt.Id
                             inner join GCDCS.Warehouse dwt
                                on pto.DestWarehouseId = dwt.Id
                             where pto.Id = pop.OriginalRequirementBillId
                               and pop.OriginalRequirementBillType = 6
                               and (owt.StorageCenter != dwt.StorageCenter or
                                   dwt.WmsInterface = 0))))))
            and PartsOutboundPlan_sync.Syncnum <
                (select syncnum
                   from wmsinf.syncstatu@wmsinf_206
                  where objid = '{8D616656-0199-481F-A28F-6D373A7CEBD8}')
            and PartsOutboundPlan_sync.Syncnum >
                ((select syncnum
                    from wmsinf.syncstatu@wmsinf_206
                   where objid = '{8D616656-0199-481F-A28F-6D373A7CEBD8}') - 3000) ----- 一天3000条出库计划 guid 时代工程车营销各自不同
            and not exists
          (select 1
                   from t_shipmentheader@wmsinf_206 tmp
                  where tmp.interface_record_id = pop.code)and pop.status in(1,2));
 
 update GCDCS.partsinboundplan_sync
    set partsinboundplan_sync.Syncnum = PartsinboundPlan_increase.Nextval
  where billid in
        (select pip.id
           from GCDCS.PartsInboundPlan pip
          inner join GCDCS.PartsInboundPlan_sync
             on PartsInboundPlan_sync.billid = pip.id
          inner join GCDCS.warehouse
             on warehouse.Id = pip.Warehouseid
          where ((warehouse.WmsInterface = 1) and
                (((pip.InboundType = 1 and exists
                 (select 1
                       from GCDCS.PartsPurchaseOrder ppo
                      inner join GCDCS.PartsPurchaseOrderType ppot
                         on ppo.PartsPurchaseOrderTypeId = ppot.Id
                      where ppo.Id = pip.OriginalRequirementBillId)) or
                (pip.InboundType = 2 and exists
                 (select 1
                       from GCDCS.PartsSalesReturnBill psrb
                      where psrb.Id = pip.OriginalRequirementBillId)) or
                (pip.InboundType = 4)) or
                (pip.InboundType = 3 and exists
                 (select 1
                      from GCDCS.PartsTransferOrder ppo
                     where ppo.Id = pip.OriginalRequirementBillId) and
                 (exists (select 1
                              from GCDCS.PartsTransferOrder pto
                             inner join GCDCS.warehouse owt
                                on pto.OriginalWarehouseId = owt.Id
                             inner join GCDCS.warehouse dwt
                                on pto.DestWarehouseId = dwt.Id
                             where pto.Id = pip.OriginalRequirementBillId
                               and pip.Originalrequirementbilltype = 6
                               and (owt.StorageCenter != dwt.StorageCenter or
                                   owt.wmsinterface = 0))))))
            and PartsInboundPlan_sync.Syncnum <
                (select syncnum
                   from wmsinf.syncstatu@wmsinf_206
                  where objid = '{C122C1EC-9BA1-4667-A607-CF241C51C4A0}')
            and PartsInboundPlan_sync.Syncnum >
                ((select syncnum
                    from wmsinf.syncstatu@wmsinf_206
                   where objid = '{C122C1EC-9BA1-4667-A607-CF241C51C4A0}') - 3000) ----- 一天3000条出库计划 guid 时代工程车营销各自不同
            and not exists
          (select 1
                   from t_receiptheader@wmsinf_206 tmp
                  where tmp.interface_record_id = pip.code) and pip.status in(1,4));
  end;
/
