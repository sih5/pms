create or replace procedure insertwarehouseagedetails  as
 sqlstr varchar2(100);
begin
sqlstr:='truncate table  warehouseagedetailstatable';
   EXECUTE IMMEDIATE sqlstr;
  insert into warehouseagedetailstatable
  select (select code from warehouse where warehouse.id = t.warehouseid) 仓库编号,
       (select name from warehouse where warehouse.id = t.warehouseid) 仓库名称,
       (select code from sparepart where sparepart.id = t.partid) 配件编号,
       (select name from sparepart where sparepart.id = t.partid) 配件名称,
          (select code from branch where branch.id= d.branchid) branchcode,

        e.plannedprice,
       h.salesprice,
       t.qty 库存数量,
       t.qty * e.plannedprice as 三个月未出库计划金额,
       t.qty * h.salesprice 三个月未出库销售金额,
       v.qty * e.plannedprice as 六个月未出库计划金额,
       v.qty * h.salesprice 六个月未出库销售金额,
       g.qty * e.plannedprice as 十二个月未出库计划金额,
       g.qty * h.salesprice 十二个月未出库销售金额,
       s.qty * e.plannedprice as 十二个月以上未出库计划金额,
       s.qty * h.salesprice 十二个月以上未出库销售金额
  from (select warehouseid, partid, sum(partsstock.quantity) qty
          from partsstock
         where not exists (select 1
                  from partsoutboundbill a
                 inner join partsoutboundbilldetail b
                    on a.id = b.partsoutboundbillid
                 where a.warehouseid = partsstock.warehouseid
                   and b.sparepartid = partsstock.partid
                   and a.createtime >= sysdate - 90)
         group by warehouseid, partid) t
  left join salesunitaffiwarehouse c
    on c.warehouseid = t.warehouseid
  left join salesunit d
    on d.id = c.salesunitid
  left join partsplannedprice e
    on e.partssalescategoryid = d.partssalescategoryid
   and t.partid = e.sparepartid
  left join partssalesprice h
    on h.sparepartid = t.partid
   and h.partssalescategoryid = d.partssalescategoryid
   and h.status = 1
  left join (select warehouseid, partid, sum(partsstock.quantity) qty
               from partsstock
              where not exists (select 1
                       from partsoutboundbill a
                      inner join partsoutboundbilldetail b
                         on a.id = b.partsoutboundbillid
                      where a.warehouseid = partsstock.warehouseid
                        and b.sparepartid = partsstock.partid
                        and a.createtime >= sysdate - 180)
              group by warehouseid, partid) v
    on t.warehouseid = v.warehouseid
   and t.partid = v.partid
  left join (select warehouseid, partid, sum(partsstock.quantity) qty
               from partsstock
              where not exists (select 1
                       from partsoutboundbill a
                      inner join partsoutboundbilldetail b
                         on a.id = b.partsoutboundbillid
                      where a.warehouseid = partsstock.warehouseid
                        and b.sparepartid = partsstock.partid
                        and a.createtime >= sysdate - 365)
              group by warehouseid, partid) g
    on t.warehouseid = g.warehouseid
   and t.partid = g.partid
  left join (select warehouseid, partid, sum(partsstock.quantity) qty
               from partsstock
              where not exists
              (select 1
                       from partsoutboundbill a
                      inner join partsoutboundbilldetail b
                         on a.id = b.partsoutboundbillid
                      where a.warehouseid = partsstock.warehouseid
                        and b.sparepartid = partsstock.partid)
              group by warehouseid, partid) s
    on t.warehouseid = s.warehouseid
   and t.partid = s.partid
 where t.qty > 0;
 
end insertwarehouseagedetails;
/
