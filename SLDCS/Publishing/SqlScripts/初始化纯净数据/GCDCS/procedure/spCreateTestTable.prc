create or replace procedure spCreateTestTable
is
    v_CreateString varchar2(1000);
begin

insert into linshibiao
  (code,
   bcode,
   warehouseid,
   tcwarehouseid,
   avalidstock,
   kykc,
   safestock,
   tcsafestock,
   partid,
   transfornum,
   WarehouseAreaId,
   Quantity)
  (select tmp1.code,
          tmp1.bcode,
          tmp1.warehouseid,
          tmp2.tcwarehouseid,
          tmp1.avalidstock,
          tmp2.kykc,
          tmp1.safestock,
          tmp2.safestock as tcsafestock,
          tmp1.partid,
          (case
            when (nvl(tmp1.stockmaximum, 0) - nvl(avalidstock, 0)) <=
                 (nvl(tmp2.kykc, 0) - nvl(tmp2.safestock, 0)) then
             nvl(tmp1.StockMaximum, 0) - nvl(tmp1.avalidstock, 0)
            else
             nvl(tmp2.kykc, 0) - nvl(tmp2.SafeStock, 0)
          end) as transfornum,
          tmp2.WarehouseAreaId,
          tmp2.Quantity
     from ( select a.partid,
                 vps.stockmaximum,
                 vps.stockminimum,
                 vps.safestock,
                 c.code,
                 b.code as bcode,
                 a.warehouseid as warehouseid,
                 a.PlannedPrice,
                 (nvl(a.sumqty, 0) - nvl(pls.Lockedquantity, 0) -
                 nvl(CongelationStockQty, 0) - nvl(DisabledStock, 0)) as avalidstock
            from (select w.id warehouseid,
                         ps.partid,
                         w.branchid,
                         w.storagecompanyid,
                         sum(ps.quantity) sumqty,
                         psp.PlannedPrice
                    from Warehouse w
                   inner join PartsStock ps
                      on ps.warehouseid = w.id
                   inner join warehouseareacategory wc
                      on wc.id = ps.warehouseareacategoryid
                   inner join PartsSalesCategory psc
                      on psc.Name='随车行'
                   inner join PartsPlannedPrice psp
                      on psp.sparepartid=ps.partid and psp.partssalescategoryid=psc.id
                   where w.id in (1263, 1261, 1262)
                     and wc.category in (1, 3)
                   group by partid, w.id, w.branchid, w.storagecompanyid,PlannedPrice) a
           inner join VehiclePartsStockLevel vps
              on vps.warehouseid = a.warehouseid
             and vps.partid = a.partid
            left join Branch b
              on b.id = a.branchid
            left join Company c
              on c.id = a.storagecompanyid
            left join PartsLockedStock pls
              on pls.warehouseid = a.warehouseid
             and pls.partid = a.partid
            left join WmsCongelationStockView wcsv
              on wcsv.WarehouseId = a.warehouseid
             and wcsv.SparePartId = a.partid
           where nvl(a.sumqty, 0) - nvl(Lockedquantity, 0) -
                 nvl(CongelationStockQty, 0) - nvl(DisabledStock, 0) <=
                 vps.stockminimum
) tmp1
    inner join (

select a.warehouseid as tcwarehouseid,
                      a.partid,
                      ps1.WarehouseAreaId,
                      ps1.Quantity,
                      vps1.safestock,
                      a.PlannedPrice,
                      (case a.warehouseid
                        when 7 then
                         1263
                        when 22 then
                         1261
                        when 38 then
                         1262
                      end) as warehouseid,
                      (nvl(a.sumqty, 0) -
                      nvl(pls.Lockedquantity, 0) -
                      nvl(CongelationStockQty, 0) -
                      nvl(DisabledStock, 0)) as kykc
  from (select wh.id warehouseid,
               wh.branchid,
               wh.storagecompanyid,
               ps1.partid,
               sum(ps1.quantity) sumqty,
               PlannedPrice
          from Warehouse wh
         inner join PartsStock ps1
            on ps1.warehouseid = wh.id
         inner join warehouseareacategory wc
            on wc.id = ps1.warehouseareacategoryid
         inner join PartsSalesCategory psc
            on psc.Name='集团配件'
         inner join PartsPlannedPrice psp
            on psp.sparepartid=ps1.partid and psp.partssalescategoryid=psc.id
         where wh.id in (7, 22, 38)
           and wc.category in (1, 3)
         group by wh.id, ps1.partid, wh.branchid, wh.storagecompanyid,PlannedPrice) a
         inner join  PartsStock ps1 on ps1.warehouseid=a.warehouseid and ps1.partid=a.partid
 inner join VehiclePartsStockLevel vps1
    on vps1.warehouseid = a.warehouseid
   and vps1.partid = a.partid
  left join Branch b
    on b.id = a.branchid
  left join Company c
    on c.id = a.storagecompanyid
  left join PartsLockedStock pls
    on pls.warehouseid = a.warehouseid
   and pls.partid = a.partid
  left join WmsCongelationStockView wcsv
    on wcsv.WarehouseId = a.warehouseid
   and wcsv.SparePartId = a.partid
 where (nvl(a.sumqty, 0) - nvl(pls.Lockedquantity, 0) -
       nvl(CongelationStockQty, 0) - nvl(DisabledStock, 0)) >
       vps1.safestock
) tmp2
       on tmp1.warehouseid = tmp2.warehouseid and tmp1.partid=tmp2.partid and tmp1.PlannedPrice=tmp2.PlannedPrice where  Quantity>0);

end spCreateTestTable;
/
