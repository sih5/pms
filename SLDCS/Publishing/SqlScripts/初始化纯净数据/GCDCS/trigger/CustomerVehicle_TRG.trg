CREATE OR REPLACE TRIGGER CustomerVehicle_TRG
AFTER UPDATE ON VehicleInformation
FOR EACH ROW
declare number1 number;
--Pragma autonomous_transaction;
BEGIN
    select count(*) into number1 from retainedcustomervehiclelist where vehicleid = :NEW.ID;
  IF(number1>0 ) THEN
     INSERT INTO CustomerVehicle_SYNC VALUES(S_CustomerVehicle_SYNC.NEXTVAL,:NEW.ID,'CustomerVehicle');
  END IF;
END CustomerVehicle_TRG;
/
