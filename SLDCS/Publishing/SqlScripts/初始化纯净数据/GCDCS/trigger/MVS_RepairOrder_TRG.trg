CREATE OR REPLACE TRIGGER MVS_RepairOrder_TRG
  AFTER INSERT OR UPDATE ON RepairOrder
  FOR EACH ROW
BEGIN
  IF (:old.id is null or (:new.branchid=8481 and:new.status<>:old.status and (:NEW.STATUS = 1 or :NEW.STATUS = 6 or :NEW.STATUS = 99))) THEN
    INSERT INTO MVS_RepairOrder_SYNC  VALUES (S_MVS_RepairOrder_SYNC.NEXTVAL, :NEW.ID,'REPAIRORDER');
  END IF;
END RepairOrder_TRG;
/
