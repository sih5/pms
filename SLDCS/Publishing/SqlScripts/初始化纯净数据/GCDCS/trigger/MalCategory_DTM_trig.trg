create or replace trigger MalCategory_DTM_trig
  after INSERT or update ON MalfunctionCategory
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    insert into MalCategory_DTM_sync
      (billid, syncnum)
    values
      (:new.Id,MalCategory_DTM_increase.nextval);
  END;
END;
/
