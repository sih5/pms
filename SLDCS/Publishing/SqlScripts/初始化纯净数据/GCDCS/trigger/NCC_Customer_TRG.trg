CREATE OR REPLACE TRIGGER NCC_Customer_TRG
AFTER INSERT OR UPDATE  ON Customer
FOR EACH ROW
BEGIN
    if  (:New.IdDocumentType is not null and :New.IdDocumentNumber is not null and :New.CustomerType is not null and :New.cellphonenumber is not null)  then
  INSERT INTO NCC_Customer_SYNC VALUES(S_NCC_Customer_SYNC.NEXTVAL,:NEW.ID,'Customer',sysdate);
    end if;
END NCC_Customer_TRG;
/
