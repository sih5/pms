CREATE OR REPLACE TRIGGER NCC_DealerKeyEmployee_TRG
AFTER INSERT OR UPDATE OR DELETE ON DealerKeyEmployee
FOR EACH ROW
BEGIN
 
  if inserting then
          INSERT INTO NCC_DealerKeyEmployee_SYNC VALUES(S_NCC_DealerKeyEmployee_SYNC.NEXTVAL,:NEW.ID,'DealerKeyEmployee',sysdate,:NEW.DealerId);
  end if;
   if deleting then
        INSERT INTO NCC_DealerKeyEmployee_SYNC VALUES(S_NCC_DealerKeyEmployee_SYNC.NEXTVAL,:old.ID,'DealerKeyEmployee',sysdate,:old.DealerId);
  end if;
   if updating then
         INSERT INTO NCC_DealerKeyEmployee_SYNC VALUES(S_NCC_DealerKeyEmployee_SYNC.NEXTVAL,:NEW.ID,'DealerKeyEmployee',sysdate,:NEW.DealerId);
  end if;
END NCC_DealerKeyEmployee_TRG;
/
