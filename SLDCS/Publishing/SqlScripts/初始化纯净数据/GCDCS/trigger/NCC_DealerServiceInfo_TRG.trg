CREATE OR REPLACE TRIGGER NCC_DealerServiceInfo_TRG
AFTER INSERT OR UPDATE ON DealerServiceInfo
FOR EACH ROW
BEGIN
  INSERT INTO NCC_DealerServiceInfo_SYNC VALUES(S_NCC_DealerServiceInfo_SYNC.NEXTVAL,:NEW.ID,'DealerServiceInfo',sysdate);
END NCC_DealerServiceInfo_TRG;
/
