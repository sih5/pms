CREATE OR REPLACE TRIGGER NCC_EngineModelLine_TRG
AFTER INSERT  OR UPDATE ON Enginemodelproductline
FOR EACH ROW
BEGIN
  INSERT INTO NCC_EngineModelLine_SYNC VALUES(S_NCC_EngineModelLine_SYNC.NEXTVAL,:NEW.ID,'Enginemodelproductline',sysdate);
END NCC_EngineModelLine_TRG;
/
