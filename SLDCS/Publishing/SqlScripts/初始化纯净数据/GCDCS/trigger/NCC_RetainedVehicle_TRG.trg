CREATE OR REPLACE TRIGGER NCC_RetainedVehicle_TRG
AFTER INSERT OR UPDATE OR DELETE ON Retainedcustomervehiclelist
FOR EACH ROW
BEGIN
  if inserting then
  INSERT INTO NCC_RetainedVehicle_SYNC VALUES(S_NCC_RetainedVehicle_SYNC.NEXTVAL,
  :NEW.ID,
  'Retainedcustomervehiclelist',
  sysdate,
  :NEW.RetainedCustomerId,
  :NEW.VehicleId,
  :NEW.Status);
  end if;

  if deleting then
  INSERT INTO NCC_RetainedVehicle_SYNC VALUES(S_NCC_RetainedVehicle_SYNC.NEXTVAL,
  :old.ID,
  'Retainedcustomervehiclelist',
  sysdate,
  :old.RetainedCustomerId,
  :old.VehicleId,
  99);
  end if;

  if updating then
  INSERT INTO NCC_RetainedVehicle_SYNC VALUES(S_NCC_RetainedVehicle_SYNC.NEXTVAL,
  :NEW.ID,
  'Retainedcustomervehiclelist',
  sysdate,
  :old.RetainedCustomerId,
  :NEW.VehicleId,
  :NEW.Status);
  end if;

END NCC_RetainedVehicle_TRG;
/
