CREATE OR REPLACE TRIGGER NCC_ServiceProductLine_TRG
AFTER INSERT OR UPDATE ON ServiceProductLine
FOR EACH ROW
BEGIN
  INSERT INTO NCC_ServiceProductLine_SYNC VALUES(S_NCC_ServiceProductLine_SYNC.NEXTVAL,:NEW.ID,'ServiceProductLine',sysdate);
END NCC_ServiceProductLine_TRG;
/
