CREATE OR REPLACE TRIGGER NCC_Serviceprodlineproduct_TRG
AFTER INSERT  OR DELETE ON ServProdLineProductDetail
FOR EACH ROW
BEGIN
  
   if inserting then
         INSERT INTO NCC_ServProdLineAffi_SYNC VALUES(S_NCC_ServProdLineAffi_SYNC.NEXTVAL,:NEW.ID,'Serviceprodlineproduct',sysdate,:NEW.ProductId);
  end if;
   if deleting then
       INSERT INTO NCC_ServProdLineAffi_SYNC VALUES(S_NCC_ServProdLineAffi_SYNC.NEXTVAL,:old.ID,'Serviceprodlineproduct',sysdate,:old.ProductId);
       end if;
END NCC_Serviceprodlineproduct_TRG;
/
