CREATE OR REPLACE TRIGGER OCC_DEALERSERVICEINFO_TRG
  AFTER INSERT OR UPDATE ON DEALERSERVICEINFO
  FOR EACH ROW
BEGIN
    --保存的是清单的ID
    merge into OCC_DEALERSERVICEINFO_SYNC bys
    using (select '1' from dual) N
    on (bys.BILLID = :NEW.Id)
    WHEN MATCHED THEN
      UPDATE set syncnum = S_OCC_DEALERSERVICEINFO.nextval
    WHEN NOT MATCHED THEN
      INSERT
        (SYNCNUM, BILLID)
      VALUES
        (S_OCC_DEALERSERVICEINFO.NEXTVAL, :NEW.Id);
END;
/
