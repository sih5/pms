Create Or Replace Trigger OCC_DEALER_TRG
  After UPDATE ON DEALER
  FOR EACH ROW
DECLARE
BEGIN
  if :new.MANAGER <> :old.MANAGER then
      --清单需要触发服务经理
      update DEALERSERVICEINFO set status=status where dealerid = :NEW.ID;
  end if;
END;
/
