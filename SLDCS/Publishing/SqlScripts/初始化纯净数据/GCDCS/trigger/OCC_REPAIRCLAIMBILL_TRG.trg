CREATE OR REPLACE TRIGGER OCC_REPAIRCLAIMBILL_TRG
  AFTER INSERT OR UPDATE ON REPAIRCLAIMBILL
  FOR EACH ROW
declare
BEGIN
  IF inserting then
    /*新增*/
    merge into OCC_REPAIRCLAIMBILL_SYNC bys
    using (select '1' from dual) N
    on (bys.BILLID = :new.ID)
    WHEN MATCHED THEN
      UPDATE set syncnum = S_OCC_REPAIRCLAIMBILL.nextval
    WHEN NOT MATCHED THEN
      INSERT
        (SYNCNUM, BILLID)
      VALUES
        (S_OCC_REPAIRCLAIMBILL.NEXTVAL, :NEW.ID);

    /*解决统一平台历史车被覆盖为DMS商品车的问题，重新触发 保有客户，车辆联系人(车辆联系人的触发含车的触发)*/
    /*重新触发车主*/
    update retainedcustomer b
       set id = id
     where exists (select a.id from customer a where b.customerid = a.id)
       and exists (select c.id
              from retainedcustomervehiclelist c
             where c.retainedcustomerid = b.id
               and c.status = 1
               and c.vehicleid = :new.vehicleid);

    /*重新触发联系人*/
    update vehiclelinkman l
       set id = id
     where l.vehicleinformationid in
           (select s.id
              from vehicleinformation s
             inner join repairorder r
            /*on s.vin = r.vin*/
            on s.id = r.vehicleid -- gly modify 20140729
             where r.id = :new.repairorderid);
  else
    /*修改*/
    IF ((:NEW.STATUS = 1) OR (:NEW.STATUS = 99) OR
       (:NEW.STATUS = 3 and :NEW.STATUS <> :OLD.STATUS) OR
       (:NEW.STATUS = 4 and :NEW.STATUS <> :OLD.STATUS) OR
       (:NEW.STATUS = 6 and :NEW.STATUS <> :OLD.STATUS) OR
       ((:NEW.STATUS = 2) and
       (:NEW.AUTOAPPROVESTATUS = :OLD.AUTOAPPROVESTATUS))) THEN
      /*状态2的为了排除自动审核的*/
      merge into OCC_REPAIRCLAIMBILL_SYNC bys
      using (select '1' from dual) N
      on (bys.BILLID = :new.ID)
      WHEN MATCHED THEN
        UPDATE set syncnum = S_OCC_REPAIRCLAIMBILL.nextval
      WHEN NOT MATCHED THEN
        INSERT
          (SYNCNUM, BILLID)
        VALUES
          (S_OCC_REPAIRCLAIMBILL.NEXTVAL, :NEW.ID);

    END IF;
  end if;
END;
/
