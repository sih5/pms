CREATE OR REPLACE TRIGGER OCC_VEHICLELINKMAN_TRG
AFTER INSERT OR UPDATE ON VEHICLELINKMAN
FOR EACH ROW
declare
BEGIN
  merge into OCC_VEHICLELINKMAN_SYNC bys
  using (select '1' from dual) N
  on (bys.BILLID = :new.ID)
  WHEN MATCHED THEN
    UPDATE set syncnum = S_OCC_VEHICLELINKMAN.nextval
  WHEN NOT MATCHED THEN
    INSERT
      (SYNCNUM, BILLID)
    VALUES
      (S_OCC_VEHICLELINKMAN.NEXTVAL, :NEW.ID);

  /*���´��� ��*/
  update vehicleinformation
     set status = status
   where id = :new.vehicleinformationid;

END;
/
