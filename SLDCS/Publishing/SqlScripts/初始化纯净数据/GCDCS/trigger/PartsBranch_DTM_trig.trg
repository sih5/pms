create or replace trigger PartsBranch_DTM_trig
  after INSERT or update ON PartsBranch
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN

          insert into PartsBranch_DTM_sync (billid, syncnum) values(:new.Id,PartsBranch_DTM_increase.nextval);
  END;
END;
/
