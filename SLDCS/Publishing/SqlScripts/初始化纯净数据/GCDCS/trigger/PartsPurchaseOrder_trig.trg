create or replace trigger PartsPurchaseOrder_trig
  after insert or update ON PartsPurchaseOrder
  FOR EACH ROW
DECLARE
  FIsExists number(1);
BEGIN
  DECLARE
  BEGIN

    BEGIN
      select 1
        into FIsExists
        from PartsPurchaseOrder_sync
       where billid = :new.id;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        FIsExists := null;
      When others then
        FIsExists := null;
    END;

    if (:new.status = 2) and FIsExists is null then
      insert into PartsPurchaseOrder_sync
        (billid, syncnum)
      values
        (:new.Id, PartsPurchaseOrder_increase.nextval);
    end if;
  END;
END;
/
