create or replace trigger PartsReplacement_DTM_trig
  after INSERT or update ON PartsReplacement
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    insert into PartsReplacement_DTM_sync
      (billid, syncnum)
    values
      (:new.Id,PartsReplacement_DTM_increase.nextval);
  END;
END;
/
