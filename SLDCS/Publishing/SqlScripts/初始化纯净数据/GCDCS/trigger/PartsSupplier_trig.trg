create or replace trigger PartsSupplier_trig
  after insert or update ON PartsSupplier
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    insert into PartsSupplier_sync
      (billid, syncnum)
    values
      (:new.Id,PartsSupplier_increase.nextval);
  END;
END;
/
