create or replace trigger RepairItemBrand_DTM_trig
  after INSERT or update ON RepairItemBrandRelation
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    insert into RepairItemBrand_DTM_sync
      (billid, syncnum)
    values
      (:new.Id,RepairItemBrand_DTM_increase.nextval);
  END;
END;
/
