create or replace trigger Repairitem_DTM_trig
  after INSERT or update ON Repairitem
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    insert into Repairitem_DTM_sync
      (billid, syncnum)
    values
      (:new.Id,Repairitem_DTM_increase.nextval);
  END;
END;
/
