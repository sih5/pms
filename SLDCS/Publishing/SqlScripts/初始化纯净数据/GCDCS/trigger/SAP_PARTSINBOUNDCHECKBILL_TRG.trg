CREATE OR REPLACE TRIGGER SAP_PARTSINBOUNDCHECKBILL_TRG
AFTER INSERT ON PARTSINBOUNDCHECKBILL
FOR EACH ROW
declare
 lscompanyId number(9);
 yxcompanyId number(9);
 gccompanyId number(9);
 sdcompanyId number(9);
 ovcompanyId number(9);
BEGIN
select companyId  into lscompanyid from (
select case (select count(*)
           from company
          where code = '2470'
            and status = 1
            and rownum = 1)
         when 1 then
          (select id
             from company
            where code = '2470'
              and status = 1
              and rownum = 1)
         else
          -1
       end as companyId
  from dual);
select companyId
  into yxcompanyid
  from (select case (select count(*)
                   from company
                  where code = '1101'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '1101'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
select companyId
  into gccompanyId
  from (select case (select count(*)
                   from company
                  where code = '2230'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2230'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
select companyId
  into sdcompanyId
  from (select case (select count(*)
                   from company
                  where code = '2601'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2601'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual); 
select companyId
  into ovcompanyId
  from (select case (select count(*)
                   from company
                  where code = '2240'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2240'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual); 
  --营销
  IF (:NEW.INBOUNDTYPE=1 AND (:NEW.STORAGECOMPANYID =sdcompanyId or :NEW.STORAGECOMPANYID =yxcompanyId or :NEW.STORAGECOMPANYID =lscompanyId or :NEW.STORAGECOMPANYID =gccompanyId or :NEW.STORAGECOMPANYID =ovcompanyId)) THEN --SAP_9（采购入库)
    INSERT INTO SAP_9_SYNC VALUES(S_SAP_9_SYNC.NEXTVAL,:NEW.ID,'PARTSINBOUNDCHECKBILL');
  END IF;
  IF ((:NEW.INBOUNDTYPE=2 or :NEW.INBOUNDTYPE=8) AND (:NEW.STORAGECOMPANYID =sdcompanyId or :NEW.STORAGECOMPANYID =yxcompanyId or :NEW.STORAGECOMPANYID =lscompanyId or :NEW.STORAGECOMPANYID =gccompanyId or :NEW.STORAGECOMPANYID =ovcompanyId)) THEN --SAP_6(销售退货入库)
    INSERT INTO SAP_6_SYNC VALUES(S_SAP_6_SYNC.NEXTVAL,:NEW.ID,'PARTSINBOUNDCHECKBILL');
  END IF;
END SAP_PARTSINBOUNDCHECKBILL_TRG;
/
