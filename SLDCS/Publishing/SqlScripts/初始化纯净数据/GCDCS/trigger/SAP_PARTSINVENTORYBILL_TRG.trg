CREATE OR REPLACE TRIGGER SAP_PARTSINVENTORYBILL_TRG
AFTER INSERT OR UPDATE ON PARTSINVENTORYBILL
FOR EACH ROW
declare
 lscompanyId number(9);
 yxcompanyId number(9);
 gccompanyId number(9);
 sdcompanyId number(9);
  ovcompanyId number(9);
BEGIN
select companyId  into lscompanyid from (
select case (select count(*)
           from company
          where code = '2470'
            and status = 1
            and rownum = 1)
         when 1 then
          (select id
             from company
            where code = '2470'
              and status = 1
              and rownum = 1)
         else
          -1
       end as companyId
  from dual);
select companyId
  into yxcompanyid
  from (select case (select count(*)
                   from company
                  where code = '1101'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '1101'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
select companyId
  into gccompanyId
  from (select case (select count(*)
                   from company
                  where code = '2230'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2230'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
select companyId
  into sdcompanyId
  from (select case (select count(*)
                   from company
                  where code = '2601'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2601'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual); 
select companyId
  into ovcompanyId
  from (select case (select count(*)
                   from company
                  where code = '2240'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2240'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual); 
  --Ӫ��
  IF (:NEW.STATUS = 3 AND ((:NEW.BRANCHID=sdcompanyId AND :NEW.STORAGECOMPANYID=sdcompanyId) or (:NEW.BRANCHID=gccompanyId AND :NEW.STORAGECOMPANYID=gccompanyId)  or (:NEW.BRANCHID=ovcompanyId AND :NEW.STORAGECOMPANYID=ovcompanyId) or (:NEW.BRANCHID=yxcompanyId AND :NEW.STORAGECOMPANYID=yxcompanyId) or (:NEW.BRANCHID=lscompanyId AND :NEW.STORAGECOMPANYID=lscompanyId))) THEN --SAP_12(�̵�)
    INSERT INTO SAP_12_SYNC VALUES (S_SAP_12_SYNC.NEXTVAL, :NEW.ID, 'PARTSINVENTORYBILL');
  END IF;
END SAP_PARTSINVENTORYBILL_TRG;
/
