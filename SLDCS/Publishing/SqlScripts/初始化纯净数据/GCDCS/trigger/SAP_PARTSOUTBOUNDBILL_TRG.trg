CREATE OR REPLACE TRIGGER SAP_PARTSOUTBOUNDBILL_TRG
AFTER INSERT ON PARTSOUTBOUNDBILL
FOR EACH ROW
declare
 lscompanyId number(9);
 yxcompanyId number(9);
 gccompanyId number(9);
 sdcompanyId number(9);
 ovcompanyId number(9);
BEGIN
select companyId  into lscompanyid from (
select case (select count(*)
           from company
          where code = '2470'
            and status = 1
            and rownum = 1)
         when 1 then
          (select id
             from company
            where code = '2470'
              and status = 1
              and rownum = 1)
         else
          -1
       end as companyId
  from dual);
select companyId
  into yxcompanyid
  from (select case (select count(*)
                   from company
                  where code = '1101'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '1101'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
select companyId
  into gccompanyId
  from (select case (select count(*)
                   from company
                  where code = '2230'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2230'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
select companyId
  into sdcompanyId
  from (select case (select count(*)
                   from company
                  where code = '2601'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2601'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual); 
          
select companyId
  into ovcompanyId
  from (select case (select count(*)
                   from company
                  where code = '2240'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2240'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual); 
  --营销
  IF(:NEW.OUTBOUNDTYPE=1 AND (:new.STORAGECOMPANYID=sdcompanyId or :NEW.STORAGECOMPANYID=yxcompanyId or :NEW.STORAGECOMPANYID=lscompanyId or :NEW.STORAGECOMPANYID=gccompanyId or :NEW.STORAGECOMPANYID=ovcompanyId)) THEN--SAP_10(销售出库)
     INSERT INTO SAP_10_SYNC VALUES(S_SAP_10_SYNC.NEXTVAL,:NEW.ID,'PARTSOUTBOUNDBILL');
  END IF;
  IF(:NEW.OUTBOUNDTYPE=2 AND (:new.STORAGECOMPANYID=sdcompanyId or :NEW.STORAGECOMPANYID=yxcompanyId or :new.STORAGECOMPANYID=lscompanyId or :NEW.STORAGECOMPANYID=gccompanyId or :NEW.STORAGECOMPANYID=ovcompanyId)) THEN--SAP_5(采购退货出库)
     INSERT INTO SAP_5_SYNC VALUES(S_SAP_5_SYNC.NEXTVAL,:NEW.ID,'PARTSOUTBOUNDBILL');
  END IF;
END SAP_PARTSOUTBOUNDBILL_TRG;
/
