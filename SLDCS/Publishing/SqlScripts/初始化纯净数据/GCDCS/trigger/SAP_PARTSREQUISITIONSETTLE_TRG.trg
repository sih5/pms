CREATE OR REPLACE TRIGGER SAP_PARTSREQUISITIONSETTLE_TRG
AFTER INSERT OR UPDATE ON PARTSREQUISITIONSETTLEBILL
FOR EACH ROW
declare
 lscompanyId number(9);
 yxcompanyId number(9);
 gccompanyId number(9);
 sdcompanyId number(9);
 ovcompanyId number(9);
BEGIN
select companyId  into lscompanyid from (
select case (select count(*)
           from company
          where code = '2470'
            and status = 1
            and rownum = 1)
         when 1 then
          (select id
             from company
            where code = '2470'
              and status = 1
              and rownum = 1)
         else
          -1
       end as companyId
  from dual);
select companyId
  into yxcompanyid
  from (select case (select count(*)
                   from company
                  where code = '1101'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '1101'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
select companyId
  into gccompanyId
  from (select case (select count(*)
                   from company
                  where code = '2230'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2230'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
select companyId
  into sdcompanyId
  from (select case (select count(*)
                   from company
                  where code = '2601'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2601'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual); 
select companyId
  into ovcompanyId
  from (select case (select count(*)
                   from company
                  where code = '2240'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2240'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
 --营销
 if(:new.status=2 AND:NEW.TOTALSETTLEMENTAMOUNT<>0 AND (:new.branchid=sdcompanyId or :new.branchid=yxcompanyId or :NEW.branchid = lscompanyId or :NEW.branchid = gccompanyId or :NEW.branchid = ovcompanyId)) then --SAP_7(领用结算)
   Insert into SAP_7_SYNC VALUES(S_SAP_7_SYNC.NEXTVAL,:NEW.ID,'PARTSREQUISITIONSETTLEBILL');
 END IF;
END SAP_PARTSREQUISITIONSETTLE_TRG;
/
