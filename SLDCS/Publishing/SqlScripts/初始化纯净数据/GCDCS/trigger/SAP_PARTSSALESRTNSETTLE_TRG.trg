CREATE OR REPLACE TRIGGER SAP_PARTSSALESRTNSETTLE_TRG
AFTER INSERT OR UPDATE ON PARTSSALESRTNSETTLEMENT
FOR EACH ROW
declare
 lscompanyId number(9);
 yxcompanyId number(9);
 gccompanyId number(9);
 sdcompanyId number(9);
 ovcompanyId number(9);
BEGIN
select companyId  into lscompanyid from (
select case (select count(*)
           from company
          where code = '2470'
            and status = 1
            and rownum = 1)
         when 1 then
          (select id
             from company
            where code = '2470'
              and status = 1
              and rownum = 1)
         else
          -1
       end as companyId
  from dual);
select companyId
  into yxcompanyid
  from (select case (select count(*)
                   from company
                  where code = '1101'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '1101'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
select companyId
  into gccompanyId
  from (select case (select count(*)
                   from company
                  where code = '2230'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2230'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
select companyId
  into sdcompanyId
  from (select case (select count(*)
                   from company
                  where code = '2601'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2601'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual); 
select companyId
  into ovcompanyId
  from (select case (select count(*)
                   from company
                  where code = '2240'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2240'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
  IF(:NEW.STATUS=2 AND :NEW.SETTLEMENTPATH=1 AND :NEW.TOTALSETTLEMENTAMOUNT<>0 AND (:new.SALESCOMPANYID=sdcompanyId or :NEW.SALESCOMPANYID=lscompanyId or :NEW.SALESCOMPANYID = yxcompanyId or :NEW.SALESCOMPANYID = gccompanyId or :NEW.SALESCOMPANYID = ovcompanyId) AND :NEW.INVOICEPATH=2) THEN
    INSERT INTO SAP_4_SYNC VALUES(S_SAP_4_SYNC.NEXTVAL,:NEW.ID,'PARTSSALESRTNSETTLEMENT');--SAP_4(销售退货结算)
  END IF;
  IF(:NEW.STATUS=2 AND :NEW.SETTLEMENTPATH=1 AND :NEW.TOTALSETTLEMENTAMOUNT<>0 AND (:new.SALESCOMPANYID=sdcompanyId or :NEW.SALESCOMPANYID=lscompanyId or :NEW.SALESCOMPANYID = yxcompanyId or :NEW.SALESCOMPANYID = gccompanyId or :NEW.SALESCOMPANYID = ovcompanyId)AND :NEW.INVOICEPATH=1) THEN
    INSERT INTO SAP_15_SYNC VALUES(S_SAP_15_SYNC.NEXTVAL,:NEW.ID,'PARTSSALESRTNSETTLEMENT');--SAP_15(销售退货结算开红票)
  END IF;
  IF(:NEW.STATUS=3 AND :NEW.SETTLEMENTPATH=1 AND :NEW.TOTALSETTLEMENTAMOUNT<>0 AND (:new.SALESCOMPANYID=sdcompanyId or :NEW.SALESCOMPANYID=lscompanyId or :NEW.SALESCOMPANYID = yxcompanyId or :NEW.SALESCOMPANYID = gccompanyId or :NEW.SALESCOMPANYID = ovcompanyId)AND :NEW.INVOICEPATH=1) THEN
    INSERT INTO SAP_1_SYNC VALUES(S_SAP_1_SYNC.NEXTVAL,:NEW.ID,'PARTSSALESRTNSETTLEMENT');--SAP_1(销售退货结算单结转)
  END IF;



END SAP_PARTSSALESRTNSETTLE_TRG;
/
