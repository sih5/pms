CREATE OR REPLACE TRIGGER SAP_PARTSSALESSETTLEMENT_TRG
  AFTER INSERT OR UPDATE ON PARTSSALESSETTLEMENT
  FOR EACH ROW
declare
 lscompanyId number(9);
 yxcompanyId number(9);
 gccompanyId number(9);
 fdcompanyId number(9);
 ovcompanyId number(9);
BEGIN
select companyId  into lscompanyid from (
select case (select count(*)
           from company
          where code = '2470'
            and status = 1
            and rownum = 1)
         when 1 then
          (select id
             from company
            where code = '2470'
              and status = 1
              and rownum = 1)
         else
          -1
       end as companyId
  from dual);
select companyId
  into yxcompanyid
  from (select case (select count(*)
                   from company
                  where code = '1101'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '1101'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
select companyId
  into gccompanyId
  from (select case (select count(*)
                   from company
                  where code = '2230'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2230'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
select companyId
  into fdcompanyId
  from (select case (select count(*)
                   from company
                  where code = '2450'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2450'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
select companyId
  into ovcompanyId
  from (select case (select count(*)
                   from company
                  where code = '2240'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2240'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
  --营销
  IF(:NEW.STATUS=2 AND :NEW.SETTLEMENTPATH=1 AND :NEW.TOTALSETTLEMENTAMOUNT<>0 AND (:NEW.SALESCOMPANYID=lscompanyId or :NEW.SALESCOMPANYID = yxcompanyId or :NEW.Salescompanyid= gccompanyId or :NEW.Salescompanyid= ovcompanyId)) then
    INSERT INTO SAP_14_SYNC  VALUES (S_SAP_14_SYNC.NEXTVAL, :NEW.ID,'PARTSSALESSETTLEMENT');--SAP_14(销售结算)
  END IF;
  if(:NEW.STATUS=3 AND :NEW.SETTLEMENTPATH=1 AND (:NEW.SALESCOMPANYID=lscompanyId or :NEW.SALESCOMPANYID = yxcompanyId or :NEW.SALESCOMPANYID = gccompanyId  or :NEW.Salescompanyid= ovcompanyId)) then --改成发票登记才传 成本结转
   INSERT INTO SAP_2_SYNC  VALUES (S_SAP_2_SYNC.NEXTVAL, :NEW.ID, 'PARTSSALESSETTLEMENT');--SAP_2(销售成本结转) 由于存在结算金额为0但是成本不为0的，所以以成本金额为准，这个放在代码里面过滤
  end if;
  

END SAP_PARTSSALESSETTLEMENT_TRG;
/
