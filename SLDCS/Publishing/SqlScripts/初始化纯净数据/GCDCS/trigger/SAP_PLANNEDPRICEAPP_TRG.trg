CREATE OR REPLACE TRIGGER SAP_PLANNEDPRICEAPP_TRG
  AFTER UPDATE ON PLANNEDPRICEAPP
  FOR EACH ROW
declare
 lscompanyId number(9);
 yxcompanyId number(9);
 gccompanyId number(9);
 sdcompanyId number(9);
 ovcompanyId number(9);
BEGIN
select companyId  into lscompanyid from (
select case (select count(*)
           from company
          where code = '2470'
            and status = 1
            and rownum = 1)
         when 1 then
          (select id
             from company
            where code = '2470'
              and status = 1
              and rownum = 1)
         else
          -1
       end as companyId
  from dual);
select companyId
  into yxcompanyid
  from (select case (select count(*)
                   from company
                  where code = '1101'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '1101'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
select companyId
  into gccompanyId
  from (select case (select count(*)
                   from company
                  where code = '2230'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2230'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);
select companyId
  into sdcompanyId
  from (select case (select count(*)
                   from company
                  where code = '2601'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2601'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);  
select companyId
  into ovcompanyId
  from (select case (select count(*)
                   from company
                  where code = '2240'
                    and status = 1
                    and rownum = 1)
                 when 1 then
                  (select id
                     from company
                    where code = '2240'
                      and status = 1
                      and rownum = 1)
                 else
                  -1
               end as companyId
          from dual);        
  --营销
  IF(:new.status=3 and :old.status=2 and (:new.OWNERCOMPANYID=sdcompanyId or :new.OWNERCOMPANYID=lscompanyId or :new.OWNERCOMPANYID=yxcompanyId or :new.OWNERCOMPANYID=gccompanyId or :new.OWNERCOMPANYID=ovcompanyId)) THEN
  INSERT INTO SAP_13_SYNC VALUES (S_SAP_13_SYNC.NEXTVAL, :NEW.ID,  'PLANNEDPRICEAPP');--SAP_13(成本变更)
  END IF;
END SAP_PLANNEDPRICEAPP_TRG;
/
