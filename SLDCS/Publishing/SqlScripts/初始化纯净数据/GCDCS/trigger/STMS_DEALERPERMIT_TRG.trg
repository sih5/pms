CREATE OR REPLACE TRIGGER STMS_DEALERPERMIT_TRG
AFTER INSERT OR DELETE ON DEALERBUSINESSPERMIT
FOR EACH ROW
BEGIN
  if inserting then
         INSERT INTO STMS_7_SYNC_gc VALUES(S_STMS_7_SYNC_gc.NEXTVAL,:NEW.ID,'DEALERBUSINESSPERMIT',:NEW.dealerid,:NEW.branchid,:NEW.serviceproductlineid,:new.creatorid,:new.creatorname,:new.createtime);
  end if;
   if deleting then
         INSERT INTO STMS_7_SYNC_gc VALUES(S_STMS_7_SYNC_gc.NEXTVAL,:old.ID,'DEALERBUSINESSPERMIT',:old.dealerid,:old.branchid,:old.serviceproductlineid,:old.creatorid,:old.creatorname,:old.createtime);
  end if;
END  STMS_DEALERPERMIT_TRG;
/
