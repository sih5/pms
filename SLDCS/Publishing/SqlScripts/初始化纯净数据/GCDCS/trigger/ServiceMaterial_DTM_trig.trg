create or replace trigger ServiceMaterial_DTM_trig
  after INSERT or update ON ServiceActivityMaterialDetail
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    insert into ServiceMaterial_DTM_sync
      (billid, syncnum)
    values
      (:new.Id,ServiceMaterial_DTM_increase.nextval);
  END;
END;
/
