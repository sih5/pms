create or replace trigger ServiceVehicle_DTM_trig
  after INSERT or update ON ServiceActivityVehicleList
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    insert into ServiceVehicle_DTM_sync
      (billid, syncnum)
    values
      (:new.Id,ServiceVehicle_DTM_increase.nextval);
  END;
END;
/
