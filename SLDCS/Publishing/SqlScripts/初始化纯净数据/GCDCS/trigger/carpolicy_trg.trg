create or replace trigger carpolicy_trg
before delete on vehiclemaintenancepoilcy
for each row
declare
  checkcount number;
  checkException exception;
begin
     begin
     select count(*) into checkcount from repairorder where status<>99 and vehiclemaintenancepoilcyid=:old.Id;
     if(checkcount>0) then
       raise checkException;
     end if;
     exception
       when checkException then
         RAISE_APPLICATION_ERROR(-20001,'异常：试图删除已经被维修单使用的条款');
     end;
end;
/
