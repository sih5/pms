create or replace trigger dealerinvoiceinformation_trg
  before insert on dealerinvoiceinformation
  for each row
declare
  checkcount number;
  checkException exception;
begin
     begin
     select count(*)
       into checkcount
       from dealerinvoiceinformation
      where (status <> 99 and status <> 6)
        and dealerinvoiceinformation.invoicecode = :new.invoicecode
        and dealerinvoiceinformation.invoicenumber = :new.invoicenumber;
     if(checkcount>0) then
       raise checkException;
     end if;
     exception
       when checkException then
         RAISE_APPLICATION_ERROR(-20001,'异常：已存在非作废或审核不通过的发票');
     end;
end dealerinvoiceinformation_trg;
/
