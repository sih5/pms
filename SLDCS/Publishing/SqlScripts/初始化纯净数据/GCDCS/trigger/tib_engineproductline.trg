create or replace trigger tib_engineproductline before insert
on EngineProductLine for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column "Id" uses sequence S_ServiceProductLine
    select S_ServiceProductLine.NEXTVAL INTO :new.Id from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/
