create or replace trigger vehiclelinkman_trg
  before  insert  on vehiclelinkman
  for each row
declare
  checkcount number;
  checkException exception;
begin
  begin
    select count(*)
      into checkcount
      from vehiclelinkman
     where (status <> 99)
       and vehiclelinkman.id <> :new.id
       and vehiclelinkman.linkmantype = 65151001
       and vehiclelinkman.vehicleinformationid = :new.vehicleinformationid;
    if (checkcount > 0) and (:new.linkmantype = 65151001) and (:new.status = 1) then
      raise checkException;
    end if;
  exception
    when checkException then
      RAISE_APPLICATION_ERROR(-20001, '异常：已存在非作废的车主');
  end;
end vehiclelinkman_trg;
/
