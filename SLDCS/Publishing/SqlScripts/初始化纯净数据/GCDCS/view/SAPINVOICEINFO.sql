CREATE OR REPLACE VIEW SAPINVOICEINFO AS
SELECT id,
       BUKRS         as CompanyCode,
       2             as BusinessType,
       code,
       InvoiceNumber,
       InvoiceAmount,
       invoicetax,
       CreateTime,
       Status,
       Message
  from SAP_YX_SALESBILLINFO
union
SELECT id,
       BUKRS         as CompanyCode,
       3             as BusinessType,
       code,
       InvoiceNumber,
       InvoiceAmount,
       invoicetax,
       CreateTime,
       Status,
       Message
  from SAP_YX_INVOICEVERACCOUNTINFO
union
select id,
       BUKRS as CompanyCode,
       1 as BusinessType,
       JSDNUM as code,
       to_char('') as InvoiceNumber,
       to_number('') as InvoiceAmount,
       to_number('') as invoicetax,
       receivetime as createtime,
       handlestatus as status,
       handlemessage as message
  from sap_yx_purinvoicepassback;
