CREATE OR REPLACE VIEW SENDWAYBILL AS
select psc.Id as PartsSalesCategoryId,pso.WarehouseId,pso.Id as PartsShippingOrderId,psc.name,pso.code,pso.Status,pso.Type,pso.OriginalRequirementBillCode,pso.ShippingCompanyCode,pso.ShippingCompanyName,pso.ReceivingCompanyCode,pso.ReceivingCompanyName,pso.ReceivingWarehouseName,(SELECT SUM(psod.ShippingAmount*psod.settlementprice) FROM gcdcs.PartsShippingOrderDetail psod WHERE psod.PartsShippingOrderId=pso.id) as zongjine,
pso.ReceivingAddress,pso.IsTransportLosses,pso.TransportLossesDisposeStatus,pso.LogisticCompanyCode,pso.LogisticCompanyName,pso.ShippingMethod,pso.InterfaceRecordId,pso.RequestedArrivalDate,pso.ShippingDate,pso.TransportDriverPhone,
pso.DeliveryBillNumber,pso.WarehouseName,pso.TransportMileage,pso.TransportVehiclePlate,pso.TransportDriver,pso.Remark,pso.OrderApproveComment,pso.CreatorName,pso.CreateTime,pso.ModifierName,pso.ModifyTime,pso.ConsigneeName,pso.ConfirmedReceptionTime,
pso.ReceiptConfirmorName,pso.ReceiptConfirmTime
from gcdcs.PartsShippingOrder pso
inner join gcdcs.PartsSalesCategory psc on psc.id=pso.PartsSalesCategoryId;
