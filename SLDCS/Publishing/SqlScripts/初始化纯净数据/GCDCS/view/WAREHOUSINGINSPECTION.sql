CREATE OR REPLACE VIEW WAREHOUSINGINSPECTION AS
select pic.Id as PartsInboundCheckBillId,pic.code,pic.warehouseid,pic.warehousecode,pic.WarehouseName,pic.InboundType,(SELECT SUM(picbd.SettlementPrice*picbd.InspectedQuantity) FROM PartsInboundCheckBillDetail picbd WHERE picbd.PartsInboundCheckBillId=pic.id) AS zongjine,pic.OriginalRequirementBillCode,pip.code as PartsInboundPlanCode,
psc.name,pic.BranchName,pic.CounterpartCompanyCode,pic.Counterpartcompanyname,pic.Objid,pic.GPMSPurOrderCode,pic.SettlementStatus,pic.remark,pso.ERPSourceOrderCode,pso.code as PartsSalesOrderCode,
pso.InvoiceType,pic.Creatorname,pic.createtime,pic.ModifierName,pic.modifytime
from gcdcs.PartsInboundCheckBill pic
inner join gcdcs.PartsInboundPlan pip on pip.id=pic.PartsInboundPlanId
inner join gcdcs.PartsSalesCategory psc on psc.id=pic.PartsSalesCategoryId
inner join gcdcs.PartsSalesOrder pso on pso.id=pic.OriginalRequirementBillId and pic.Originalrequirementbilltype=1;
