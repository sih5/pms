create or replace view allvalidstock as
select StorageCompanyId as BranchId,PartsSalesCategoryId,PartId ,sum(nums) as nums from
          (select PartsStock.StorageCompanyId,
                 SalesUnit.Partssalescategoryid,
                 PartsStock.PartId,
                 sum(PartsStock.Quantity) as nums
            from PartsStock
           inner join SalesUnitAffiWarehouse --销售组织仓库关系
              on PartsStock.Warehouseid = SalesUnitAffiWarehouse.Warehouseid
           inner join SalesUnit
              on SalesUnit.Id = SalesUnitAffiWarehouse.Salesunitid and SalesUnit.Branchid=PartsStock.Branchid
           inner join WarehouseAreaCategory --库区用途
              on WarehouseAreaCategory.id = PartsStock.WarehouseAreaCategoryId
           where WarehouseAreaCategory.Category in (1, 3) --保管区、检验区
           group by PartsStock.StorageCompanyId,
                    PartsStock.PartId,
                    SalesUnit.Partssalescategoryid
          union all
          select PartsLockedStock.StorageCompanyId,
                 SalesUnit.PartsSalesCategoryid,
                 PartsLockedStock.PartId,
                 -nvl(PartsLockedStock.LockedQuantity, 0) as nums
            from PartsLockedStock --配件锁定库存
           inner join SalesUnitAffiWarehouse
              on PartsLockedStock.Warehouseid =
                 SalesUnitAffiWarehouse.Warehouseid
           inner join SalesUnit
              on SalesUnit.Id = SalesUnitAffiWarehouse.Salesunitid
              and  SalesUnit.Branchid=PartsLockedStock.Branchid
          union all
          select StorageCompanyId,
                 PartsSalesCategoryId,
                 SparePartId as PartId,
                 -nvl(CongelationStockQty, 0) as nums
            from WmsCongelationStockView --WMS仓库配件冻结库存（视图）
          union all
          select StorageCompanyId,
                 PartsSalesCategoryId,
                 SparePartId as ,
                 -nvl(DisabledStock, 0) as nums
            from WmsCongelationStockView
       ) group by StorageCompanyId,Partssalescategoryid,PartId;
