create or replace view dealercoincidencedetailsta as
select a."品牌",a."服务站网络总量",a."网络模式",a."重合",a."奥铃重合",a."欧马可重合",a."拓陆者萨普重合",a."蒙派克风景重合",a."起重机重合",a."欧曼重合",a."搅拌车重合",a."雷萨泵送重合",a."工程车北区重合",a."伽途重合",a."时代重合",a."迷迪重合",a."网络模式重合",a."奥铃网络模式重合",a."欧马可网络模式重合",a."拓陆者萨普网络模式重合",a."蒙派克风景网络模式重合",a."起重机网络模式重合",a."欧曼网络模式重合",a."搅拌车网络模式重合",a."雷萨泵送网络模式重合",a."工程车北区网络模式重合",a."伽途网络模式重合",a."时代网络模式重合",a."迷迪网络模式重合",
       round(重合 / 服务站网络总量,2) as 重合率,
       round(网络模式重合 / 服务站网络总量,2) as 网络模式重合率
  from (select (select name
                  from gcdcs.partssalescategory
                 where partssalescategory.id = partssalescategoryid) as 品牌,
              count(partssalescategoryid) /*(select count(*)
                  from yxdcs.dealerserviceinfo v
                  inner join yxdcs.channelcapability on channelcapability.id=v.channelcapabilityid
                 where v.status = 1 and v.partssalescategoryid= z.partssalescategoryid and channelcapability.name=网络模式)*/ as 服务站网络总量,
                 网络模式,
               sum(重合) as 重合,
               sum(奥铃重合) as 奥铃重合,
               sum(欧马可重合) as 欧马可重合,
               sum(拓陆者萨普重合) as 拓陆者萨普重合,
               sum(蒙派克风景重合) as 蒙派克风景重合,
               sum(起重机重合) as 起重机重合,
               sum(欧曼重合) as 欧曼重合,
               sum(搅拌车重合) as 搅拌车重合,
               sum(雷萨泵送重合) as 雷萨泵送重合,
               sum(工程车北区重合) as 工程车北区重合,
               sum(伽途重合) as 伽途重合,
               sum(时代重合) as 时代重合,
               sum(迷迪重合) as 迷迪重合,
               sum(网络模式重合) as 网络模式重合,
               sum(奥铃网络模式重合) as 奥铃网络模式重合,
               sum(欧马可网络模式重合) as  欧马可网络模式重合,
               sum(拓陆者萨普网络模式重合) as 拓陆者萨普网络模式重合,
               sum(蒙派克风景网络模式重合) as 蒙派克风景网络模式重合,
               sum(起重机网络模式重合) as 起重机网络模式重合,
               sum(欧曼网络模式重合) as 欧曼网络模式重合,
               sum(搅拌车网络模式重合) as 搅拌车网络模式重合,
               sum(雷萨泵送网络模式重合) as 雷萨泵送网络模式重合,
               sum(工程车北区网络模式重合) as 工程车北区网络模式重合,
               sum(伽途网络模式重合) as 伽途网络模式重合,
               sum(时代网络模式重合) as 时代网络模式重合,
               sum(迷迪网络模式重合) as 迷迪网络模式重合
          from (select --t.dealerid,
                 t.partssalescategoryid,
                 s.name as 网络模式,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                     union
                     select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                     union
                     select distinct 1
                       from gcdcs.dealerserviceinfo v
                      inner join gcdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join gcdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                     union
                     select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1),
                     0) as 重合,

                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '奥铃'),
                     0) as  奥铃重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '欧马可'),
                     0) as 欧马可重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '拓陆者萨普'),
                     0) as 拓陆者萨普重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '蒙派克风景'),
                     0) as 蒙派克风景重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '起重机'),
                     0) as 起重机重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '欧曼'),
                     0) as 欧曼重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '搅拌车'),
                     0) as 搅拌车重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '雷萨泵送'),
                     0) as 雷萨泵送重合,
                 nvl((select distinct 1
                       from gcdcs.dealerserviceinfo v
                      inner join gcdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join gcdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '工程车(北区)'),
                     0) as 工程车北区重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '伽途'),
                     0) as 伽途重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '时代'),
                     0) as 时代重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '迷迪'),
                     0) as 迷迪重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                     union
                     select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                     union
                     select distinct 1
                       from gcdcs.dealerserviceinfo v
                      inner join gcdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join gcdcs.company d
                         on d.id = v.dealerid
                      inner join gcdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                     union
                     select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      inner join sddcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1),
                     0) as 网络模式重合,

                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '奥铃'),
                     0) as 奥铃网络模式重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '欧马可'),
                     0) as  欧马可网络模式重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '拓陆者萨普'),
                     0) as 拓陆者萨普网络模式重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '蒙派克风景'),
                     0) as 蒙派克风景网络模式重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '起重机'),
                     0) as 起重机网络模式重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '欧曼'),
                     0) as 欧曼网络模式重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '搅拌车'),
                     0) as 搅拌车网络模式重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '雷萨泵送'),
                     0) as 雷萨泵送网络模式重合,
                 nvl((select distinct 1
                       from gcdcs.dealerserviceinfo v
                      inner join gcdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join gcdcs.company d
                         on d.id = v.dealerid
                      inner join gcdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '工程车(北区)'),
                     0) as 工程车北区网络模式重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join sddcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '伽途'),
                     0) as 伽途网络模式重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      inner join sddcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '时代'),
                     0) as 时代网络模式重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      inner join sddcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '迷迪'),
                     0) as 迷迪网络模式重合
                  from gcdcs.dealerserviceinfo t
                 inner join gcdcs.partssalescategory a
                    on t.partssalescategoryid = a.id
                 inner join gcdcs.company c
                    on c.id = t.dealerid
                 inner join gcdcs.channelcapability s
                    on s.id = t.channelcapabilityid  where a.name<>'工程车(南区)'  and t.status=1) z
         group by partssalescategoryid,网络模式) a
          union
          select a."品牌",a."服务站网络总量",a."网络模式",a."重合",a."奥铃重合",a."欧马可重合",a."拓陆者萨普重合",a."蒙派克风景重合",a."起重机重合",a."欧曼重合",a."搅拌车重合",a."雷萨泵送重合",a."工程车北区重合",a."伽途重合",a."时代重合",a."迷迪重合",a."网络模式重合",a."奥铃网络模式重合",a."欧马可网络模式重合",a."拓陆者萨普网络模式重合",a."蒙派克风景网络模式重合",a."起重机网络模式重合",a."欧曼网络模式重合",a."搅拌车网络模式重合",a."雷萨泵送网络模式重合",a."工程车北区网络模式重合",a."伽途网络模式重合",a."时代网络模式重合",a."迷迪网络模式重合",
       重合 / 服务站网络总量 as 重合率,
       网络模式重合 / 服务站网络总量 as 网络模式重合率
  from (select (select name
                  from sddcs.partssalescategory
                 where partssalescategory.id = partssalescategoryid) as 品牌,
              count(partssalescategoryid) /*(select count(*)
                  from yxdcs.dealerserviceinfo v
                  inner join yxdcs.channelcapability on channelcapability.id=v.channelcapabilityid
                 where v.status = 1 and v.partssalescategoryid= z.partssalescategoryid and channelcapability.name=网络模式)*/ as 服务站网络总量,
                 网络模式,
               sum(重合) as 重合,
               sum(奥铃重合) as 奥铃重合,
               sum(欧马可重合) as 欧马可重合,
               sum(拓陆者萨普重合) as 拓陆者萨普重合,
               sum(蒙派克风景重合) as 蒙派克风景重合,
               sum(起重机重合) as 起重机重合,
               sum(欧曼重合) as 欧曼重合,
               sum(搅拌车重合) as 搅拌车重合,
               sum(雷萨泵送重合) as 雷萨泵送重合,
               sum(工程车北区重合) as 工程车北区重合,
               sum(伽途重合) as 伽途重合,
               sum(时代重合) as 时代重合,
               sum(迷迪重合) as 迷迪重合,
               sum(网络模式重合) as 网络模式重合,
               sum(奥铃网络模式重合) as 奥铃网络模式重合,
               sum(欧马可网络模式重合) as  欧马可网络模式重合,
               sum(拓陆者萨普网络模式重合) as 拓陆者萨普网络模式重合,
               sum(蒙派克风景网络模式重合) as 蒙派克风景网络模式重合,
               sum(起重机网络模式重合) as 起重机网络模式重合,
               sum(欧曼网络模式重合) as 欧曼网络模式重合,
               sum(搅拌车网络模式重合) as 搅拌车网络模式重合,
               sum(雷萨泵送网络模式重合) as 雷萨泵送网络模式重合,
               sum(工程车北区网络模式重合) as 工程车北区网络模式重合,
               sum(伽途网络模式重合) as 伽途网络模式重合,
               sum(时代网络模式重合) as 时代网络模式重合,
               sum(迷迪网络模式重合) as 迷迪网络模式重合
          from (select --t.dealerid,
                 t.partssalescategoryid,
                 s.name as 网络模式,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                     union
                     select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                     union
                     select distinct 1
                       from gcdcs.dealerserviceinfo v
                      inner join gcdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join gcdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                     union
                     select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1),
                     0) as 重合,

                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '奥铃'),
                     0) as  奥铃重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '欧马可'),
                     0) as 欧马可重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '拓陆者萨普'),
                     0) as 拓陆者萨普重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '蒙派克风景'),
                     0) as 蒙派克风景重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '起重机'),
                     0) as 起重机重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '欧曼'),
                     0) as 欧曼重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '搅拌车'),
                     0) as 搅拌车重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '雷萨泵送'),
                     0) as 雷萨泵送重合,
                 nvl((select distinct 1
                       from gcdcs.dealerserviceinfo v
                      inner join gcdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join gcdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '工程车(北区)'),
                     0) as 工程车北区重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '伽途'),
                     0) as 伽途重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '时代'),
                     0) as 时代重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '迷迪'),
                     0) as 迷迪重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                     union
                     select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                     union
                     select distinct 1
                       from gcdcs.dealerserviceinfo v
                      inner join gcdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join gcdcs.company d
                         on d.id = v.dealerid
                      inner join gcdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                     union
                     select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      inner join sddcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1),
                     0) as 网络模式重合,

                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '奥铃'),
                     0) as 奥铃网络模式重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '欧马可'),
                     0) as  欧马可网络模式重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '拓陆者萨普'),
                     0) as 拓陆者萨普网络模式重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '蒙派克风景'),
                     0) as 蒙派克风景网络模式重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '起重机'),
                     0) as 起重机网络模式重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '欧曼'),
                     0) as 欧曼网络模式重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '搅拌车'),
                     0) as 搅拌车网络模式重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '雷萨泵送'),
                     0) as 雷萨泵送网络模式重合,
                 nvl((select distinct 1
                       from gcdcs.dealerserviceinfo v
                      inner join gcdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join gcdcs.company d
                         on d.id = v.dealerid
                      inner join gcdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '工程车(北区)'),
                     0) as 工程车北区网络模式重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join sddcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '伽途'),
                     0) as 伽途网络模式重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      inner join sddcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '时代'),
                     0) as 时代网络模式重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      inner join sddcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '迷迪'),
                     0) as 迷迪网络模式重合
                  from sddcs.dealerserviceinfo t
                 inner join sddcs.partssalescategory a
                    on t.partssalescategoryid = a.id
                 inner join sddcs.company c
                    on c.id = t.dealerid
                 inner join sddcs.channelcapability s
                    on s.id = t.channelcapabilityid  where t.status=1 ) z
         group by partssalescategoryid,网络模式) a
            union
          select a."品牌",a."服务站网络总量",a."网络模式",a."重合",a."奥铃重合",a."欧马可重合",a."拓陆者萨普重合",a."蒙派克风景重合",a."起重机重合",a."欧曼重合",a."搅拌车重合",a."雷萨泵送重合",a."工程车北区重合",a."伽途重合",a."时代重合",a."迷迪重合",a."网络模式重合",a."奥铃网络模式重合",a."欧马可网络模式重合",a."拓陆者萨普网络模式重合",a."蒙派克风景网络模式重合",a."起重机网络模式重合",a."欧曼网络模式重合",a."搅拌车网络模式重合",a."雷萨泵送网络模式重合",a."工程车北区网络模式重合",a."伽途网络模式重合",a."时代网络模式重合",a."迷迪网络模式重合",
       重合 / 服务站网络总量 as 重合率,
       网络模式重合 / 服务站网络总量 as 网络模式重合率
  from (select (select name
                  from yxdcs.partssalescategory
                 where partssalescategory.id = partssalescategoryid) as 品牌,
              count(partssalescategoryid) /*(select count(*)
                  from yxdcs.dealerserviceinfo v
                  inner join yxdcs.channelcapability on channelcapability.id=v.channelcapabilityid
                 where v.status = 1 and v.partssalescategoryid= z.partssalescategoryid and channelcapability.name=网络模式)*/ as 服务站网络总量,
                 网络模式,
               sum(重合) as 重合,
               sum(奥铃重合) as 奥铃重合,
               sum(欧马可重合) as 欧马可重合,
               sum(拓陆者萨普重合) as 拓陆者萨普重合,
               sum(蒙派克风景重合) as 蒙派克风景重合,
               sum(起重机重合) as 起重机重合,
               sum(欧曼重合) as 欧曼重合,
               sum(搅拌车重合) as 搅拌车重合,
               sum(雷萨泵送重合) as 雷萨泵送重合,
               sum(工程车北区重合) as 工程车北区重合,
               sum(伽途重合) as 伽途重合,
               sum(时代重合) as 时代重合,
               sum(迷迪重合) as 迷迪重合,
               sum(网络模式重合) as 网络模式重合,
               sum(奥铃网络模式重合) as 奥铃网络模式重合,
               sum(欧马可网络模式重合) as  欧马可网络模式重合,
               sum(拓陆者萨普网络模式重合) as 拓陆者萨普网络模式重合,
               sum(蒙派克风景网络模式重合) as 蒙派克风景网络模式重合,
               sum(起重机网络模式重合) as 起重机网络模式重合,
               sum(欧曼网络模式重合) as 欧曼网络模式重合,
               sum(搅拌车网络模式重合) as 搅拌车网络模式重合,
               sum(雷萨泵送网络模式重合) as 雷萨泵送网络模式重合,
               sum(工程车北区网络模式重合) as 工程车北区网络模式重合,
               sum(伽途网络模式重合) as 伽途网络模式重合,
               sum(时代网络模式重合) as 时代网络模式重合,
               sum(迷迪网络模式重合) as 迷迪网络模式重合
          from (select --t.dealerid,
                 t.partssalescategoryid,
                 s.name as 网络模式,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                     union
                     select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                     union
                     select distinct 1
                       from gcdcs.dealerserviceinfo v
                      inner join gcdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join gcdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                     union
                     select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1),
                     0) as 重合,

                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '奥铃'),
                     0) as  奥铃重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '欧马可'),
                     0) as 欧马可重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '拓陆者萨普'),
                     0) as 拓陆者萨普重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '蒙派克风景'),
                     0) as 蒙派克风景重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '起重机'),
                     0) as 起重机重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '欧曼'),
                     0) as 欧曼重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '搅拌车'),
                     0) as 搅拌车重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '雷萨泵送'),
                     0) as 雷萨泵送重合,
                 nvl((select distinct 1
                       from gcdcs.dealerserviceinfo v
                      inner join gcdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join gcdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '工程车(北区)'),
                     0) as 工程车北区重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '伽途'),
                     0) as 伽途重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '时代'),
                     0) as 时代重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '迷迪'),
                     0) as 迷迪重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                     union
                     select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                     union
                     select distinct 1
                       from gcdcs.dealerserviceinfo v
                      inner join gcdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join gcdcs.company d
                         on d.id = v.dealerid
                      inner join gcdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                     union
                     select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      inner join sddcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1),
                     0) as 网络模式重合,

                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '奥铃'),
                     0) as 奥铃网络模式重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '欧马可'),
                     0) as  欧马可网络模式重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '拓陆者萨普'),
                     0) as 拓陆者萨普网络模式重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '蒙派克风景'),
                     0) as 蒙派克风景网络模式重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '起重机'),
                     0) as 起重机网络模式重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '欧曼'),
                     0) as 欧曼网络模式重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '搅拌车'),
                     0) as 搅拌车网络模式重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '雷萨泵送'),
                     0) as 雷萨泵送网络模式重合,
                 nvl((select distinct 1
                       from gcdcs.dealerserviceinfo v
                      inner join gcdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join gcdcs.company d
                         on d.id = v.dealerid
                      inner join gcdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '工程车(北区)'),
                     0) as 工程车北区网络模式重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join sddcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '伽途'),
                     0) as 伽途网络模式重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      inner join sddcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '时代'),
                     0) as 时代网络模式重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      inner join sddcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '迷迪'),
                     0) as 迷迪网络模式重合
                  from yxdcs.dealerserviceinfo t
                 inner join yxdcs.partssalescategory a
                    on t.partssalescategoryid = a.id
                 inner join yxdcs.company c
                    on c.id = t.dealerid
                 inner join yxdcs.channelcapability s
                    on s.id = t.channelcapabilityid where   a.name not in ('海外中重卡','海外轻卡','海外乘用车','精品','统购','电子商务') and t.status=1) z
         group by partssalescategoryid,网络模式) a
           union
          select a."品牌",a."服务站网络总量",a."网络模式",a."重合",a."奥铃重合",a."欧马可重合",a."拓陆者萨普重合",a."蒙派克风景重合",a."起重机重合",a."欧曼重合",a."搅拌车重合",a."雷萨泵送重合",a."工程车北区重合",a."伽途重合",a."时代重合",a."迷迪重合",a."网络模式重合",a."奥铃网络模式重合",a."欧马可网络模式重合",a."拓陆者萨普网络模式重合",a."蒙派克风景网络模式重合",a."起重机网络模式重合",a."欧曼网络模式重合",a."搅拌车网络模式重合",a."雷萨泵送网络模式重合",a."工程车北区网络模式重合",a."伽途网络模式重合",a."时代网络模式重合",a."迷迪网络模式重合",
       重合 / 服务站网络总量 as 重合率,
       网络模式重合 / 服务站网络总量 as 网络模式重合率
  from (select (select name
                  from dcs.partssalescategory
                 where partssalescategory.id = partssalescategoryid) as 品牌,
              count(partssalescategoryid) /*(select count(*)
                  from yxdcs.dealerserviceinfo v
                  inner join yxdcs.channelcapability on channelcapability.id=v.channelcapabilityid
                 where v.status = 1 and v.partssalescategoryid= z.partssalescategoryid and channelcapability.name=网络模式)*/ as 服务站网络总量,
                 网络模式,
               sum(重合) as 重合,
               sum(奥铃重合) as 奥铃重合,
               sum(欧马可重合) as 欧马可重合,
               sum(拓陆者萨普重合) as 拓陆者萨普重合,
               sum(蒙派克风景重合) as 蒙派克风景重合,
               sum(起重机重合) as 起重机重合,
               sum(欧曼重合) as 欧曼重合,
               sum(搅拌车重合) as 搅拌车重合,
               sum(雷萨泵送重合) as 雷萨泵送重合,
               sum(工程车北区重合) as 工程车北区重合,
               sum(伽途重合) as 伽途重合,
               sum(时代重合) as 时代重合,
               sum(迷迪重合) as 迷迪重合,
               sum(网络模式重合) as 网络模式重合,
               sum(奥铃网络模式重合) as 奥铃网络模式重合,
               sum(欧马可网络模式重合) as  欧马可网络模式重合,
               sum(拓陆者萨普网络模式重合) as 拓陆者萨普网络模式重合,
               sum(蒙派克风景网络模式重合) as 蒙派克风景网络模式重合,
               sum(起重机网络模式重合) as 起重机网络模式重合,
               sum(欧曼网络模式重合) as 欧曼网络模式重合,
               sum(搅拌车网络模式重合) as 搅拌车网络模式重合,
               sum(雷萨泵送网络模式重合) as 雷萨泵送网络模式重合,
               sum(工程车北区网络模式重合) as 工程车北区网络模式重合,
               sum(伽途网络模式重合) as 伽途网络模式重合,
               sum(时代网络模式重合) as 时代网络模式重合,
               sum(迷迪网络模式重合) as 迷迪网络模式重合
          from (select --t.dealerid,
                 t.partssalescategoryid,
                 s.name as 网络模式,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                     union
                     select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                     union
                     select distinct 1
                       from gcdcs.dealerserviceinfo v
                      inner join gcdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join gcdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                     union
                     select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1),
                     0) as 重合,

                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '奥铃'),
                     0) as  奥铃重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '欧马可'),
                     0) as 欧马可重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '拓陆者萨普'),
                     0) as 拓陆者萨普重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '蒙派克风景'),
                     0) as 蒙派克风景重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '起重机'),
                     0) as 起重机重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '欧曼'),
                     0) as 欧曼重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '搅拌车'),
                     0) as 搅拌车重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '雷萨泵送'),
                     0) as 雷萨泵送重合,
                 nvl((select distinct 1
                       from gcdcs.dealerserviceinfo v
                      inner join gcdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join gcdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '工程车(北区)'),
                     0) as 工程车北区重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '伽途'),
                     0) as 伽途重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '时代'),
                     0) as 时代重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      where c.code = d.code
                        and a.name <> b.name
                        and v.status = 1
                        and b.name = '迷迪'),
                     0) as 迷迪重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                     union
                     select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                     union
                     select distinct 1
                       from gcdcs.dealerserviceinfo v
                      inner join gcdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join gcdcs.company d
                         on d.id = v.dealerid
                      inner join gcdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                     union
                     select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      inner join sddcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1),
                     0) as 网络模式重合,

                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '奥铃'),
                     0) as 奥铃网络模式重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '欧马可'),
                     0) as  欧马可网络模式重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '拓陆者萨普'),
                     0) as 拓陆者萨普网络模式重合,
                 nvl((select distinct 1
                       from yxdcs.dealerserviceinfo v
                      inner join yxdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join yxdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '蒙派克风景'),
                     0) as 蒙派克风景网络模式重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '起重机'),
                     0) as 起重机网络模式重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '欧曼'),
                     0) as 欧曼网络模式重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '搅拌车'),
                     0) as 搅拌车网络模式重合,
                 nvl((select distinct 1
                       from dcs.dealerserviceinfo v
                      inner join dcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join dcs.company d
                         on d.id = v.dealerid
                      inner join dcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '雷萨泵送'),
                     0) as 雷萨泵送网络模式重合,
                 nvl((select distinct 1
                       from gcdcs.dealerserviceinfo v
                      inner join gcdcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join gcdcs.company d
                         on d.id = v.dealerid
                      inner join gcdcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '工程车(北区)'),
                     0) as 工程车北区网络模式重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join yxdcs.company d
                         on d.id = v.dealerid
                      inner join sddcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '伽途'),
                     0) as 伽途网络模式重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      inner join sddcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '时代'),
                     0) as 时代网络模式重合,
                 nvl((select distinct 1
                       from sddcs.dealerserviceinfo v
                      inner join sddcs.partssalescategory b
                         on v.partssalescategoryid = b.id
                      inner join sddcs.company d
                         on d.id = v.dealerid
                      inner join sddcs.channelcapability t
                         on t.id = v.channelcapabilityid
                      where c.code = d.code
                        and a.name <> b.name
                        and s.name = t.name
                        and v.status = 1
                        and b.name = '迷迪'),
                     0) as 迷迪网络模式重合
                  from dcs.dealerserviceinfo t
                 inner join dcs.partssalescategory a
                    on t.partssalescategoryid = a.id
                 inner join dcs.company c
                    on c.id = t.dealerid
                 inner join dcs.channelcapability s
                    on s.id = t.channelcapabilityid where   a.name <>'欧曼保外' and t.status=1) z
         group by partssalescategoryid,网络模式) a;
