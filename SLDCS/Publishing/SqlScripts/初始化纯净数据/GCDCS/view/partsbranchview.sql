create or replace view partsbranchview as
select a.code as PartCode,
       b.partssalescategoryname as PartsSalesCategoryName,
       'DCS' || b.PartsSalesCategoryId as PartsSalesCategoryId
  from dcs.sparepart a
 inner join dcs.partsbranch b
    on a.id = b.partid
 where a.status = 1
   and b.status = 1
union
select a.code as PartCode,
       b.partssalescategoryname as PartsSalesCategoryName,
       'YXDCS' || b.PartsSalesCategoryId as PartsSalesCategoryId
  from yxdcs.sparepart a
 inner join yxdcs.partsbranch b
    on a.id = b.partid
 where a.status = 1
   and b.status = 1
union
select a.code as PartCode,
       b.partssalescategoryname as PartsSalesCategoryName,
       'SDDCS' || b.PartsSalesCategoryId as PartsSalesCategoryId
  from sddcs.sparepart a
 inner join sddcs.partsbranch b
    on a.id = b.partid
 where a.status = 1
   and b.status = 1
union
select a.code as PartCode,
       b.partssalescategoryname as PartsSalesCategoryName,
       'GCDCS' || b.PartsSalesCategoryId as PartsSalesCategoryId
  from gcdcs.sparepart a
 inner join gcdcs.partsbranch b
    on a.id = b.partid
 where a.status = 1
   and b.status = 1;
