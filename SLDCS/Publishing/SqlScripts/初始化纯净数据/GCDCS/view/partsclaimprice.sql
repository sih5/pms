create or replace view partsclaimprice as
select PartsBranch.BranchId                  as BranchId,
       SparePart.Id                          as UsedPartsId,
       SparePart.Code                        as UsedPartsCode,
       SparePart.Name                        as UsedPartsName,
       PartsBranch.PartsWarrantyCategoryId,
       PartsBranch.PartsWarrantyCategoryName,
       PartsBranch.PartsSalesCategoryId,
       PartsSupplierRelation.SupplierId      as PartsSupplierId,
       PartsSupplier.Code                    as PartsSupplierCode,
       PartsSupplier.Name                    as PartsSupplierName,
       PartsSalesPrice.SalesPrice,
       SparePart.MeasureUnit,
       PARTSBRANCH.PARTSRETURNPOLICY         as UsedPartsReturnPolicy,
       SparePart.Specification,
       SparePart.Feature,
       --PartsWarrantyTerm.PartsWarrantyType,
       SparePart.PartType,
       --WarrantyPolicy.Category as WarrantyPolicyCategory ,
       PartDeleaveInformation.Deleaveamount   as Deleaveamount,
       PartDeleaveInformation.DeleavePartCode as DeleavePartCode,
       0 dealerId,
       0 subdealerid,
       0 as quantity
  from SparePart
 inner join PartsSalesPrice
    on PartsSalesPrice.SparePartId = SparePart.Id
 inner join PartsBranch
    on SparePart.Id = PartsBranch.PartId
   and PartsBranch.
 Partssalescategoryid = PartsSalesPrice.Partssalescategoryid
 inner join PartsSupplierRelation
    on PartsSupplierRelation.PartId = SparePart.Id
   and PartsSupplierRelation.Partssalescategoryid =
       PartsBranch.Partssalescategoryid
 inner join PartsSupplier
    on PartsSupplier.Id = PartsSupplierRelation.SupplierId
--left join PartsWarrantyTerm on PartsWarrantyTerm.PartsWarrantyCategoryId = PartsBranch.PartsWarrantyCategoryId
--left join WarrantyPolicy on PartsWarrantyTerm.WarrantyPolicyId= WarrantyPolicy.Id and WarrantyPolicy.Status=1 and WarrantyPolicy.Applicationscope=2 and WarrantyPolicy.Partswarrantyterminvolved=1
  left join PartDeleaveInformation
    on PartDeleaveInformation.Oldpartid = SparePart.Id
   and

       PartDeleaveInformation.Branchid = PartsBranch.Branchid
   and

       PartDeleaveInformation.Partssalescategoryid =
       PartsBranch.Partssalescategoryid
  --  left join dealerpartsstock on dealerpartsstock.sparepartid=SparePart.id
   -- left join dealer on dealerpartsstock.dealerid=dealer.id
 where SparePart.Status = 1
   and PartsBranch.Status = 1
   and PartsSupplierRelation.Status = 1
   and PartsSalesPrice.Status = 1
   and PartsSupplier.Status = 1
      --and PartsSalesPrice.Ifclaim = 1
   and exists
 (select *
          from PartsSalesCategory
         where PartsSalesCategory.Id = PartsSalesPrice.PartsSalesCategoryId
           and Status = 1);
