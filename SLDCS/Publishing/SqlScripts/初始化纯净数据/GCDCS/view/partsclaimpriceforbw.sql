create or replace view partsclaimpriceforbw as
select nvl(PartsBranch.BranchId,0)                  as BranchId,
       nvl(SparePart.Id,0)                          as UsedPartsId,
       nvl(SparePart.Code,' ')                         as UsedPartsCode,
       nvl(SparePart.Name,' ')                       as UsedPartsName,
       nvl(PartsBranch.PartsWarrantyCategoryId,0) PartsWarrantyCategoryId,
       nvl(PartsBranch.PartsWarrantyCategoryName,' ') PartsWarrantyCategoryName,
       nvl(PartsBranch.PartsSalesCategoryId,0) PartsSalesCategoryId,
       nvl(PartsSupplierRelation.SupplierId,0)       as PartsSupplierId,
       nvl(PartsSupplier.Code,' ')                    as PartsSupplierCode,
       nvl(PartsSupplier.Name,' ')                    as PartsSupplierName,
       nvl(PartsSalesPrice.SalesPrice,0) SalesPrice,
       nvl(SparePart.MeasureUnit,' ') MeasureUnit,
       nvl(PARTSBRANCH.PARTSRETURNPOLICY ,0)        as UsedPartsReturnPolicy,
       nvl(SparePart.Specification,'') Specification,
       nvl(SparePart.Feature,' ') Feature,
       --PartsWarrantyTerm.PartsWarrantyType,
       nvl(SparePart.PartType,0) PartType,
       --WarrantyPolicy.Category as WarrantyPolicyCategory ,
       nvl(PartDeleaveInformation.Deleaveamount,0)   as Deleaveamount,
       nvl(PartDeleaveInformation.DeleavePartCode,' ') as DeleavePartCode
  from SparePart
  left join PartsSalesPrice
    on PartsSalesPrice.SparePartId = SparePart.Id
 left join PartsBranch
    on SparePart.Id = PartsBranch.PartId
   and PartsBranch.
 Partssalescategoryid = PartsSalesPrice.Partssalescategoryid
  left join PartsSupplierRelation
    on PartsSupplierRelation.PartId = SparePart.Id
   and PartsSupplierRelation.Partssalescategoryid =
       PartsBranch.Partssalescategoryid
  left join PartsSupplier
    on PartsSupplier.Id = PartsSupplierRelation.SupplierId
--left join PartsWarrantyTerm on PartsWarrantyTerm.PartsWarrantyCategoryId = PartsBranch.PartsWarrantyCategoryId
--left join WarrantyPolicy on PartsWarrantyTerm.WarrantyPolicyId= WarrantyPolicy.Id and WarrantyPolicy.Status=1 and WarrantyPolicy.Applicationscope=2 and WarrantyPolicy.Partswarrantyterminvolved=1
  left join PartDeleaveInformation
    on PartDeleaveInformation.Oldpartid = SparePart.Id
   and

       PartDeleaveInformation.Branchid = PartsBranch.Branchid
   and PartDeleaveInformation.Partssalescategoryid =
       PartsBranch.Partssalescategoryid
 where SparePart.Status = 1
  -- and PartsBranch.Status = 1
  -- and PartsSupplierRelation.Status = 1
  -- and PartsSalesPrice.Status = 1
  -- and PartsSupplier.Status = 1
      --and PartsSalesPrice.Ifclaim = 1
  /* and exists
 (select *
          from PartsSalesCategory
         where PartsSalesCategory.Id = PartsSalesPrice.PartsSalesCategoryId
           and Status = 1);*/;
