create or replace view partsinhistorystat as
select a.branchcode,ppot.name as 采购订单类型,a.code as InCode,c.storagecompanyid,c.Code WarehouseCode,c.Name WarehouseName,c.id as WarehouseId,nvl(e.code, g.code) as Code,
1 as kind,'采购入库' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName,a.CreateTime,f.code as PartCode,f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue, a.PartsSalesCategoryId, psc.name,e.PartsPurchaseOrderTypeId,b.CostPrice,
              jsd.code as 结算单号
  from dcs.PartsInboundCheckBill a
  left join (
       select ppsb.status,ppsr.sourcecode,ppsb.code from dcs.PartsPurchaseSettleRef ppsr
              left join dcs.PartsPurchaseSettleBill ppsb on ppsr.PartsPurchaseSettleBillId = ppsb.id
              where ppsb.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join dcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join dcs.warehouse c on c.id = a.warehouseid
 inner join dcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join dcs.sparepart f on f.id = b.sparepartid
  left join dcs.PartsPurchaseOrder e on e.code =
                                        d.OriginalRequirementBillCode
  left join dcs.PartsPurchaseOrderType ppot on e.PartsPurchaseOrderTypeId = ppot.id
  left join dcs.partssalesorder g on g.code = d.OriginalRequirementBillCode
  left join dcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 1
   and a.storagecompanyid in (2,2203)
 group by a.branchcode,a.code,c.storagecompanyid,c.code, c.name, c.id, nvl(e.code, g.code), a.counterpartcompanycode, a.counterpartcompanyname, a.createtime,
 f.code, f.name,b.settlementprice,ppot.name,a.settlementstatus,a.PartsSalesCategoryId,psc.name,e.PartsPurchaseOrderTypeId,b.CostPrice,jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型, a.code as InCode,c.storagecompanyid,c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
2 as kind,'销售退货入库' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName, a.CreateTime,f.code as PartCode,f.name as PartName,
sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId,b.CostPrice,jsd.code as 结算单号
  from dcs.PartsInboundCheckBill a
  left join (
       select psrs.status,psrsr.sourcecode,psrs.code from dcs.PartsSalesRtnSettlement psrs
              left join dcs.PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
              where psrs.status in (1,2,3,5,6)
       union all
       select pss.status,pssr.sourcecode,pss.code from dcs.PartsSalesSettlement pss
              left join dcs.PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
              where pss.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join dcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join dcs.warehouse c on c.id = a.warehouseid
 inner join dcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join dcs.sparepart f on f.id = b.sparepartid
 inner join dcs.PartsSalesReturnBill e on e.code =
                                          d.originalrequirementbillcode
 left join dcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype in(2,8)
   and a.storagecompanyid in (2,2203)
 group by a.branchcode,a.code,c.storagecompanyid, c.code, c.name, c.id,e.code,a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice,jsd.status,jsd.code

union all
select a.branchcode,null as 采购订单类型, a.code as InCode,c.storagecompanyid,c.Code WarehouseCode,c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
3 as kind,'配件调拨' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName,a.CreateTime, f.code as PartCode,f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
       case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2  end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算'  end as settlementstatusValue,  a.PartsSalesCategoryId, psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice
          ,null as 结算单号
  from dcs.PartsInboundCheckBill a
  inner join dcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join dcs.warehouse c on c.id = a.warehouseid
 inner join dcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join dcs.sparepart f on f.id = b.sparepartid
 inner join dcs.PartsTransferOrder e on e.id = d.originalrequirementbillid
 left join dcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 3
   and a.storagecompanyid in (2,2203)
 group by a.branchcode, a.code, c.storagecompanyid, c.code, c.name,c.id,e.code, a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
 b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice
union all
select a.branchcode,null as 采购订单类型,a.code as InCode, c.storagecompanyid, c.Code WarehouseCode,c.Name WarehouseName, c.id as WarehouseId,e.code as Code,
4 as kind,'内部领入' as KindName,a.CounterpartCompanyCode as CompanyCode, a.CounterpartCompanyName as CompanyName,a.CreateTime,f.code as PartCode,f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId,psc.name,null as PartsPurchaseOrderTypeId,b.CostPrice,jsd.code as 结算单号
  from dcs.PartsInboundCheckBill a
     left join (
       select prsb.status,prsr.sourcecode,prsb.code from dcs.PartsRequisitionSettleBill prsb
              left join dcs.PartsRequisitionSettleRef prsr on prsb.id = prsr.partsrequisitionsettlebillid
              where prsb.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join dcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join dcs.warehouse c on c.id = a.warehouseid
 inner join dcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join dcs.sparepart f on f.id = b.sparepartid
 inner join dcs.InternalAcquisitionBill e on e.id =
                                             d.OriginalRequirementBillId
 left join dcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 4
   and a.storagecompanyid in (2,2203)
 group by a.branchcode, a.code,c.storagecompanyid, c.code, c.name, c.id, e.code, a.counterpartcompanycode,a.counterpartcompanyname,a.createtime,f.code,f.name,
b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice,  jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型,a.code as InCode, c.storagecompanyid, c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
5 as kind, '配件零售退货入库' as KindName,a.CounterpartCompanyCode as CompanyCode, a.CounterpartCompanyName as CompanyName,a.CreateTime, f.code as PartCode, f.name as PartName,
sum(b.InspectedQuantity) as InQty,b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue, a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice,jsd.code as 结算单号
  from dcs.PartsInboundCheckBill a
      left join (
       select psrs.status,psrsr.sourcecode,psrs.code from dcs.PartsSalesRtnSettlement psrs
              left join dcs.PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
              where psrs.status in (1,2,3,5,6)
       union all
       select pss.status,pssr.sourcecode,pss.code from dcs.PartsSalesSettlement pss
              left join dcs.PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
              where pss.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join dcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join dcs.warehouse c on c.id = a.warehouseid
 inner join dcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join dcs.sparepart f on f.id = b.sparepartid
 inner join dcs.PartsRetailReturnBill e on e.id =
                                           d.OriginalRequirementBillId
 left join dcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 5
   and a.storagecompanyid in (2,2203)
 group by a.branchcode,a.code,c.storagecompanyid,c.code, c.name,c.id, e.code, a.counterpartcompanycode, a.counterpartcompanyname,a.createtime, f.code, f.name,
 b.settlementprice, a.settlementstatus, a.PartsSalesCategoryId, psc.name, b.CostPrice ,jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型,a.code as InCode, c.storagecompanyid, c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code, 6 as kind,
'积压件调剂入库' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName,a.CreateTime, f.code as PartCode, f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
      case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId, psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice,jsd.code as 结算单号
  from dcs.PartsInboundCheckBill a
      left join (
      select psrs.status,psrsr.sourcecode,psrs.code from dcs.PartsSalesRtnSettlement psrs
              left join dcs.PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
              where psrs.status in (1,2,3,5,6)
       union all
       select pss.status,pssr.sourcecode,pss.code from dcs.PartsSalesSettlement pss
              left join dcs.PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
              where pss.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join dcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join dcs.warehouse c on c.id = a.warehouseid
 inner join dcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join dcs.sparepart f on f.id = b.sparepartid
 inner join dcs.OverstockPartsAdjustBill e on e.id =
                                              d.OriginalRequirementBillId
 left join dcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 6
   and a.storagecompanyid in (2,2203)
 group by a.branchcode,a.code,c.storagecompanyid,c.code,c.name,c.id,e.code,a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice  ,jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型, a.code as InCode, c.storagecompanyid,c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId,e.code as Code,
 7 as kind,'配件外采入库' as KindName,a.CounterpartCompanyCode as CompanyCode, a.CounterpartCompanyName as CompanyName, a.CreateTime, f.code as PartCode, f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2  end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算'  end as settlementstatusValue, a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice
       ,null as 结算单号
        from dcs.PartsInboundCheckBill a
  inner join dcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join dcs.warehouse c on c.id = a.warehouseid
 inner join dcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join dcs.sparepart f on f.id = b.sparepartid
 inner join dcs.PartsOuterPurchaseChange e on e.id =
                                              d.OriginalRequirementBillId
 left join dcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 7
   and a.storagecompanyid in (2,2203)
 group by a.branchcode, a.code, c.storagecompanyid, c.code,c.name, c.id, e.code,a.counterpartcompanycode,a.counterpartcompanyname,a.createtime,f.code, f.name,
 b.settlementprice, a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice
 union all
 ------------------------------1101-----------------
 select a.branchcode,ppot.name as 采购订单类型,a.code as InCode,c.storagecompanyid,c.Code WarehouseCode,c.Name WarehouseName,c.id as WarehouseId,nvl(e.code, g.code) as Code,
1 as kind,'采购入库' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName,a.CreateTime,f.code as PartCode,f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue, a.PartsSalesCategoryId, psc.name,e.PartsPurchaseOrderTypeId,b.CostPrice,
              jsd.code as 结算单号
  from yxdcs.PartsInboundCheckBill a
  left join (
       select ppsb.status,ppsr.sourcecode,ppsb.code from yxdcs.PartsPurchaseSettleRef ppsr
              left join yxdcs.PartsPurchaseSettleBill ppsb on ppsr.PartsPurchaseSettleBillId = ppsb.id
              where ppsb.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join yxdcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join yxdcs.warehouse c on c.id = a.warehouseid
 inner join yxdcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join yxdcs.sparepart f on f.id = b.sparepartid
  left join yxdcs.PartsPurchaseOrder e on e.code =
                                        d.OriginalRequirementBillCode
  left join yxdcs.PartsPurchaseOrderType ppot on e.PartsPurchaseOrderTypeId = ppot.id
  left join yxdcs.partssalesorder g on g.code = d.OriginalRequirementBillCode
  left join yxdcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 1
   and a.storagecompanyid = 2
 group by a.branchcode,a.code,c.storagecompanyid,c.code, c.name, c.id, nvl(e.code, g.code), a.counterpartcompanycode, a.counterpartcompanyname, a.createtime,
 f.code, f.name,b.settlementprice,ppot.name,a.settlementstatus,a.PartsSalesCategoryId,psc.name,e.PartsPurchaseOrderTypeId,b.CostPrice,jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型, a.code as InCode,c.storagecompanyid,c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
2 as kind,'销售退货入库' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName, a.CreateTime,f.code as PartCode,f.name as PartName,
sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId,b.CostPrice,jsd.code as 结算单号
  from yxdcs.PartsInboundCheckBill a
  left join (
       select psrs.status,psrsr.sourcecode,psrs.code from yxdcs.PartsSalesRtnSettlement psrs
              left join yxdcs.PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
              where psrs.status in (1,2,3,5,6)
       union all
       select pss.status,pssr.sourcecode,pss.code from yxdcs.PartsSalesSettlement pss
              left join yxdcs.PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
              where pss.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join yxdcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join yxdcs.warehouse c on c.id = a.warehouseid
 inner join yxdcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join yxdcs.sparepart f on f.id = b.sparepartid
 inner join yxdcs.PartsSalesReturnBill e on e.code =
                                          d.originalrequirementbillcode
 left join yxdcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where  a.inboundtype =2
   and a.storagecompanyid = 2
 group by a.branchcode,a.code,c.storagecompanyid, c.code, c.name, c.id,e.code,a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice,jsd.status,jsd.code
union   all
select a.branchcode,null as 采购订单类型, a.code as InCode,c.storagecompanyid,c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
2 as kind,'销售退货入库' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName, a.CreateTime,f.code as PartCode,f.name as PartName,
sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId,b.CostPrice,jsd.code as 结算单号
  from yxdcs.PartsInboundCheckBill a
  left join (
       select psrs.status,psrsr.sourcecode,psrs.code from yxdcs.PartsSalesRtnSettlement psrs
              left join yxdcs.PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
              where psrs.status in (1,2,3,5,6)
       union all
       select pss.status,pssr.sourcecode,pss.code from yxdcs.PartsSalesSettlement pss
              left join yxdcs.PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
              where pss.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join yxdcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join yxdcs.warehouse c on c.id = a.warehouseid
 inner join yxdcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join yxdcs.sparepart f on f.id = b.sparepartid
 inner  join yxdcs.partsclaimordernew e on e.code =
                                          d.originalrequirementbillcode
 left join yxdcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype =8
   and a.storagecompanyid =2
 group by a.branchcode,a.code,c.storagecompanyid, c.code, c.name, c.id,e.code,a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice,jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型, a.code as InCode,c.storagecompanyid,c.Code WarehouseCode,c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
3 as kind,'配件调拨' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName,a.CreateTime, f.code as PartCode,f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
       case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2  end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' end as settlementstatusValue,  a.PartsSalesCategoryId, psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice
      ,null as 结算单号
        from yxdcs.PartsInboundCheckBill a
 inner join yxdcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join yxdcs.warehouse c on c.id = a.warehouseid
 inner join yxdcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join yxdcs.sparepart f on f.id = b.sparepartid
 inner join yxdcs.PartsTransferOrder e on e.id = d.originalrequirementbillid
 left join yxdcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 3
   and a.storagecompanyid = 2
 group by a.branchcode, a.code, c.storagecompanyid, c.code, c.name,c.id,e.code, a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
 b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice
union all
select a.branchcode,null as 采购订单类型,a.code as InCode, c.storagecompanyid, c.Code WarehouseCode,c.Name WarehouseName, c.id as WarehouseId,e.code as Code,
4 as kind,'内部领入' as KindName,a.CounterpartCompanyCode as CompanyCode, a.CounterpartCompanyName as CompanyName,a.CreateTime,f.code as PartCode,f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId,psc.name,null as PartsPurchaseOrderTypeId,b.CostPrice,jsd.code as 结算单号
  from yxdcs.PartsInboundCheckBill a
     left join (
       select prsb.status,prsr.sourcecode,prsb.code from yxdcs.PartsRequisitionSettleBill prsb
              left join yxdcs.PartsRequisitionSettleRef prsr on prsb.id = prsr.partsrequisitionsettlebillid
              where prsb.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join yxdcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join yxdcs.warehouse c on c.id = a.warehouseid
 inner join yxdcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join yxdcs.sparepart f on f.id = b.sparepartid
 inner join yxdcs.InternalAcquisitionBill e on e.id =
                                             d.OriginalRequirementBillId
 left join yxdcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 4
   and a.storagecompanyid = 2
 group by a.branchcode, a.code,c.storagecompanyid, c.code, c.name, c.id, e.code, a.counterpartcompanycode,a.counterpartcompanyname,a.createtime,f.code,f.name,
b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice,  jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型,a.code as InCode, c.storagecompanyid, c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
5 as kind, '配件零售退货入库' as KindName,a.CounterpartCompanyCode as CompanyCode, a.CounterpartCompanyName as CompanyName,a.CreateTime, f.code as PartCode, f.name as PartName,
sum(b.InspectedQuantity) as InQty,b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue, a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice,jsd.code as 结算单号
  from yxdcs.PartsInboundCheckBill a
      left join (
       select psrs.status,psrsr.sourcecode,psrs.code from yxdcs.PartsSalesRtnSettlement psrs
              left join yxdcs.PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
              where psrs.status in (1,2,3,5,6)
       union all
       select pss.status,pssr.sourcecode,pss.code from yxdcs.PartsSalesSettlement pss
              left join yxdcs.PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
              where pss.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join yxdcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join yxdcs.warehouse c on c.id = a.warehouseid
 inner join yxdcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join yxdcs.sparepart f on f.id = b.sparepartid
 inner join yxdcs.PartsRetailReturnBill e on e.id =
                                           d.OriginalRequirementBillId
 left join yxdcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 5
   and a.storagecompanyid = 2
 group by a.branchcode,a.code,c.storagecompanyid,c.code, c.name,c.id, e.code, a.counterpartcompanycode, a.counterpartcompanyname,a.createtime, f.code, f.name,
 b.settlementprice, a.settlementstatus, a.PartsSalesCategoryId, psc.name, b.CostPrice ,jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型,a.code as InCode, c.storagecompanyid, c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code, 6 as kind,
'积压件调剂入库' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName,a.CreateTime, f.code as PartCode, f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
      case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId, psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice,jsd.code as 结算单号
  from yxdcs.PartsInboundCheckBill a
      left join (
      select psrs.status,psrsr.sourcecode,psrs.code from yxdcs.PartsSalesRtnSettlement psrs
              left join yxdcs.PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
              where psrs.status in (1,2,3,5,6)
       union all
       select pss.status,pssr.sourcecode,pss.code from yxdcs.PartsSalesSettlement pss
              left join yxdcs.PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
              where pss.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join yxdcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join yxdcs.warehouse c on c.id = a.warehouseid
 inner join yxdcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join yxdcs.sparepart f on f.id = b.sparepartid
 inner join yxdcs.OverstockPartsAdjustBill e on e.id =
                                              d.OriginalRequirementBillId
 left join yxdcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 6
   and a.storagecompanyid = 2
 group by a.branchcode,a.code,c.storagecompanyid,c.code,c.name,c.id,e.code,a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice  ,jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型, a.code as InCode, c.storagecompanyid,c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId,e.code as Code,
 7 as kind,'配件外采入库' as KindName,a.CounterpartCompanyCode as CompanyCode, a.CounterpartCompanyName as CompanyName, a.CreateTime, f.code as PartCode, f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' end as settlementstatusValue, a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice
       ,null as 结算单号
        from yxdcs.PartsInboundCheckBill a
 inner join yxdcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join yxdcs.warehouse c on c.id = a.warehouseid
 inner join yxdcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join yxdcs.sparepart f on f.id = b.sparepartid
 inner join yxdcs.PartsOuterPurchaseChange e on e.id =
                                              d.OriginalRequirementBillId
 left join yxdcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 7
   and a.storagecompanyid = 2
 group by a.branchcode, a.code, c.storagecompanyid, c.code,c.name, c.id, e.code,a.counterpartcompanycode,a.counterpartcompanyname,a.createtime,f.code, f.name,
 b.settlementprice, a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice
 --------------------GCC-------------------------------------
  union all
 select a.branchcode,ppot.name as 采购订单类型,a.code as InCode,c.storagecompanyid,c.Code WarehouseCode,c.Name WarehouseName,c.id as WarehouseId,nvl(e.code, g.code) as Code,
1 as kind,'采购入库' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName,a.CreateTime,f.code as PartCode,f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue, a.PartsSalesCategoryId, psc.name,e.PartsPurchaseOrderTypeId,b.CostPrice,
              jsd.code as 结算单号
  from gcdcs.PartsInboundCheckBill a
  left join (
       select ppsb.status,ppsr.sourcecode,ppsb.code from gcdcs.PartsPurchaseSettleRef ppsr
              left join gcdcs.PartsPurchaseSettleBill ppsb on ppsr.PartsPurchaseSettleBillId = ppsb.id
              where ppsb.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join gcdcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join gcdcs.warehouse c on c.id = a.warehouseid
 inner join gcdcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join gcdcs.sparepart f on f.id = b.sparepartid
  left join gcdcs.PartsPurchaseOrder e on e.code =
                                        d.OriginalRequirementBillCode
  left join gcdcs.PartsPurchaseOrderType ppot on e.PartsPurchaseOrderTypeId = ppot.id
  left join gcdcs.partssalesorder g on g.code = d.OriginalRequirementBillCode
  left join gcdcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 1
   and a.storagecompanyid = 8481
 group by a.branchcode,a.code,c.storagecompanyid,c.code, c.name, c.id, nvl(e.code, g.code), a.counterpartcompanycode, a.counterpartcompanyname, a.createtime,
 f.code, f.name,b.settlementprice,ppot.name,a.settlementstatus,a.PartsSalesCategoryId,psc.name,e.PartsPurchaseOrderTypeId,b.CostPrice,jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型, a.code as InCode,c.storagecompanyid,c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
2 as kind,'销售退货入库' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName, a.CreateTime,f.code as PartCode,f.name as PartName,
sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId,b.CostPrice,jsd.code as 结算单号
  from gcdcs.PartsInboundCheckBill a
  left join (
       select psrs.status,psrsr.sourcecode,psrs.code from gcdcs.PartsSalesRtnSettlement psrs
              left join gcdcs.PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
              where psrs.status in (1,2,3,5,6)
       union all
       select pss.status,pssr.sourcecode,pss.code from gcdcs.PartsSalesSettlement pss
              left join gcdcs.PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
              where pss.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join gcdcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join gcdcs.warehouse c on c.id = a.warehouseid
 inner join gcdcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join gcdcs.sparepart f on f.id = b.sparepartid
 inner join gcdcs.PartsSalesReturnBill e on e.code =
                                          d.originalrequirementbillcode
 left join gcdcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where  a.inboundtype in(2,8)
   and a.storagecompanyid = 8481
 group by a.branchcode,a.code,c.storagecompanyid, c.code, c.name, c.id,e.code,a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice,jsd.status,jsd.code
union     all
select a.branchcode,null as 采购订单类型, a.code as InCode,c.storagecompanyid,c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
2 as kind,'销售退货入库' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName, a.CreateTime,f.code as PartCode,f.name as PartName,
sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId,b.CostPrice,jsd.code as 结算单号
  from gcdcs.PartsInboundCheckBill a
  left join (
       select psrs.status,psrsr.sourcecode,psrs.code from gcdcs.PartsSalesRtnSettlement psrs
              left join gcdcs.PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
              where psrs.status in (1,2,3,5,6)
       union all
       select pss.status,pssr.sourcecode,pss.code from gcdcs.PartsSalesSettlement pss
              left join gcdcs.PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
              where pss.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join gcdcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join gcdcs.warehouse c on c.id = a.warehouseid
 inner join gcdcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join gcdcs.sparepart f on f.id = b.sparepartid
 inner  join gcdcs.partsclaimordernew e on e.code =
                                          d.originalrequirementbillcode
 left join gcdcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype =8
   and a.storagecompanyid=8481
 group by a.branchcode,a.code,c.storagecompanyid, c.code, c.name, c.id,e.code,a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice,jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型, a.code as InCode,c.storagecompanyid,c.Code WarehouseCode,c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
3 as kind,'配件调拨' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName,a.CreateTime, f.code as PartCode,f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
       case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2  end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' end as settlementstatusValue,  a.PartsSalesCategoryId, psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice
      ,null as 结算单号
        from gcdcs.PartsInboundCheckBill a
 inner join gcdcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join gcdcs.warehouse c on c.id = a.warehouseid
 inner join gcdcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join gcdcs.sparepart f on f.id = b.sparepartid
 inner join gcdcs.PartsTransferOrder e on e.id = d.originalrequirementbillid
 left join gcdcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 3
   and a.storagecompanyid = 8481
 group by a.branchcode, a.code, c.storagecompanyid, c.code, c.name,c.id,e.code, a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
 b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice
union all
select a.branchcode,null as 采购订单类型,a.code as InCode, c.storagecompanyid, c.Code WarehouseCode,c.Name WarehouseName, c.id as WarehouseId,e.code as Code,
4 as kind,'内部领入' as KindName,a.CounterpartCompanyCode as CompanyCode, a.CounterpartCompanyName as CompanyName,a.CreateTime,f.code as PartCode,f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId,psc.name,null as PartsPurchaseOrderTypeId,b.CostPrice,jsd.code as 结算单号
  from gcdcs.PartsInboundCheckBill a
     left join (
       select prsb.status,prsr.sourcecode,prsb.code from gcdcs.PartsRequisitionSettleBill prsb
              left join gcdcs.PartsRequisitionSettleRef prsr on prsb.id = prsr.partsrequisitionsettlebillid
              where prsb.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join gcdcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join gcdcs.warehouse c on c.id = a.warehouseid
 inner join gcdcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join gcdcs.sparepart f on f.id = b.sparepartid
 inner join gcdcs.InternalAcquisitionBill e on e.id =
                                             d.OriginalRequirementBillId
 left join gcdcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 4
   and a.storagecompanyid = 8481
 group by a.branchcode, a.code,c.storagecompanyid, c.code, c.name, c.id, e.code, a.counterpartcompanycode,a.counterpartcompanyname,a.createtime,f.code,f.name,
b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice,  jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型,a.code as InCode, c.storagecompanyid, c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
5 as kind, '配件零售退货入库' as KindName,a.CounterpartCompanyCode as CompanyCode, a.CounterpartCompanyName as CompanyName,a.CreateTime, f.code as PartCode, f.name as PartName,
sum(b.InspectedQuantity) as InQty,b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue, a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice,jsd.code as 结算单号
  from gcdcs.PartsInboundCheckBill a
      left join (
       select psrs.status,psrsr.sourcecode,psrs.code from gcdcs.PartsSalesRtnSettlement psrs
              left join gcdcs.PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
              where psrs.status in (1,2,3,5,6)
       union all
       select pss.status,pssr.sourcecode,pss.code from gcdcs.PartsSalesSettlement pss
              left join gcdcs.PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
              where pss.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join gcdcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join gcdcs.warehouse c on c.id = a.warehouseid
 inner join gcdcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join gcdcs.sparepart f on f.id = b.sparepartid
 inner join gcdcs.PartsRetailReturnBill e on e.id =
                                           d.OriginalRequirementBillId
 left join gcdcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 5
   and a.storagecompanyid = 8481
 group by a.branchcode,a.code,c.storagecompanyid,c.code, c.name,c.id, e.code, a.counterpartcompanycode, a.counterpartcompanyname,a.createtime, f.code, f.name,
 b.settlementprice, a.settlementstatus, a.PartsSalesCategoryId, psc.name, b.CostPrice ,jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型,a.code as InCode, c.storagecompanyid, c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code, 6 as kind,
'积压件调剂入库' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName,a.CreateTime, f.code as PartCode, f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
      case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId, psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice,jsd.code as 结算单号
  from gcdcs.PartsInboundCheckBill a
      left join (
      select psrs.status,psrsr.sourcecode,psrs.code from gcdcs.PartsSalesRtnSettlement psrs
              left join gcdcs.PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
              where psrs.status in (1,2,3,5,6)
       union all
       select pss.status,pssr.sourcecode,pss.code from gcdcs.PartsSalesSettlement pss
              left join gcdcs.PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
              where pss.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join gcdcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join gcdcs.warehouse c on c.id = a.warehouseid
 inner join gcdcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join gcdcs.sparepart f on f.id = b.sparepartid
 inner join gcdcs.OverstockPartsAdjustBill e on e.id =
                                              d.OriginalRequirementBillId
 left join gcdcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 6
   and a.storagecompanyid = 8481
 group by a.branchcode,a.code,c.storagecompanyid,c.code,c.name,c.id,e.code,a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice  ,jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型, a.code as InCode, c.storagecompanyid,c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId,e.code as Code,
 7 as kind,'配件外采入库' as KindName,a.CounterpartCompanyCode as CompanyCode, a.CounterpartCompanyName as CompanyName, a.CreateTime, f.code as PartCode, f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2  end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' end as settlementstatusValue, a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice
       ,null as 结算单号
        from gcdcs.PartsInboundCheckBill a
   inner join gcdcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join gcdcs.warehouse c on c.id = a.warehouseid
 inner join gcdcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join gcdcs.sparepart f on f.id = b.sparepartid
 inner join gcdcs.PartsOuterPurchaseChange e on e.id =
                                              d.OriginalRequirementBillId
 left join gcdcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 7
   and a.storagecompanyid = 8481
 group by a.branchcode, a.code, c.storagecompanyid, c.code,c.name, c.id, e.code,a.counterpartcompanycode,a.counterpartcompanyname,a.createtime,f.code, f.name,
 b.settlementprice, a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice
  union all
---------------SDDCS-----------------------------------------
select a.branchcode,ppot.name as 采购订单类型,a.code as InCode,c.storagecompanyid,c.Code WarehouseCode,c.Name WarehouseName,c.id as WarehouseId,nvl(e.code, g.code) as Code,
1 as kind,'采购入库' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName,a.CreateTime,f.code as PartCode,f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue, a.PartsSalesCategoryId, psc.name,e.PartsPurchaseOrderTypeId,b.CostPrice,
              jsd.code as 结算单号
  from sddcs.PartsInboundCheckBill a
  left join (
       select ppsb.status,ppsr.sourcecode,ppsb.code from sddcs.PartsPurchaseSettleRef ppsr
              left join sddcs.PartsPurchaseSettleBill ppsb on ppsr.PartsPurchaseSettleBillId = ppsb.id
              where ppsb.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join sddcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join sddcs.warehouse c on c.id = a.warehouseid
 inner join sddcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join sddcs.sparepart f on f.id = b.sparepartid
  left join sddcs.PartsPurchaseOrder e on e.code =
                                        d.OriginalRequirementBillCode
  left join sddcs.PartsPurchaseOrderType ppot on e.PartsPurchaseOrderTypeId = ppot.id
  left join sddcs.partssalesorder g on g.code = d.OriginalRequirementBillCode
  left join sddcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 1
   and a.storagecompanyid in( 11181,12261)
 group by a.branchcode,a.code,c.storagecompanyid,c.code, c.name, c.id, nvl(e.code, g.code), a.counterpartcompanycode, a.counterpartcompanyname, a.createtime,
 f.code, f.name,b.settlementprice,ppot.name,a.settlementstatus,a.PartsSalesCategoryId,psc.name,e.PartsPurchaseOrderTypeId,b.CostPrice,jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型, a.code as InCode,c.storagecompanyid,c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
2 as kind,'销售退货入库' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName, a.CreateTime,f.code as PartCode,f.name as PartName,
sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId,b.CostPrice,jsd.code as 结算单号
  from sddcs.PartsInboundCheckBill a
  left join (
       select psrs.status,psrsr.sourcecode,psrs.code from sddcs.PartsSalesRtnSettlement psrs
              left join sddcs.PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
              where psrs.status in (1,2,3,5,6)
       union all
       select pss.status,pssr.sourcecode,pss.code from sddcs.PartsSalesSettlement pss
              left join sddcs.PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
              where pss.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join sddcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join sddcs.warehouse c on c.id = a.warehouseid
 inner join sddcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join sddcs.sparepart f on f.id = b.sparepartid
 inner join sddcs.PartsSalesReturnBill e on e.code =
                                          d.originalrequirementbillcode
 left join sddcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where  a.inboundtype in(2,8)
   and a.storagecompanyid in( 11181,12261)
 group by a.branchcode,a.code,c.storagecompanyid, c.code, c.name, c.id,e.code,a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice,jsd.status,jsd.code
union     all
select a.branchcode,null as 采购订单类型, a.code as InCode,c.storagecompanyid,c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
2 as kind,'销售退货入库' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName, a.CreateTime,f.code as PartCode,f.name as PartName,
sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId,b.CostPrice,jsd.code as 结算单号
  from sddcs.PartsInboundCheckBill a
  left join (
       select psrs.status,psrsr.sourcecode,psrs.code from sddcs.PartsSalesRtnSettlement psrs
              left join sddcs.PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
              where psrs.status in (1,2,3,5,6)
       union all
       select pss.status,pssr.sourcecode,pss.code from sddcs.PartsSalesSettlement pss
              left join sddcs.PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
              where pss.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join sddcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join sddcs.warehouse c on c.id = a.warehouseid
 inner join sddcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join sddcs.sparepart f on f.id = b.sparepartid
 inner  join sddcs.partsclaimordernew e on e.code =
                                          d.originalrequirementbillcode
 left join sddcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype =8
   and a.storagecompanyid in( 11181,12261)
 group by a.branchcode,a.code,c.storagecompanyid, c.code, c.name, c.id,e.code,a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice,jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型, a.code as InCode,c.storagecompanyid,c.Code WarehouseCode,c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
3 as kind,'配件调拨' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName,a.CreateTime, f.code as PartCode,f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
       case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2  end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算'  end as settlementstatusValue,  a.PartsSalesCategoryId, psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice
       ,null as 结算单号
        from sddcs.PartsInboundCheckBill a
  inner join sddcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join sddcs.warehouse c on c.id = a.warehouseid
 inner join sddcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join sddcs.sparepart f on f.id = b.sparepartid
 inner join sddcs.PartsTransferOrder e on e.id = d.originalrequirementbillid
 left join sddcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 3
   and a.storagecompanyid in( 11181,12261)
 group by a.branchcode, a.code, c.storagecompanyid, c.code, c.name,c.id,e.code, a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
 b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice
union all
select a.branchcode,null as 采购订单类型,a.code as InCode, c.storagecompanyid, c.Code WarehouseCode,c.Name WarehouseName, c.id as WarehouseId,e.code as Code,
4 as kind,'内部领入' as KindName,a.CounterpartCompanyCode as CompanyCode, a.CounterpartCompanyName as CompanyName,a.CreateTime,f.code as PartCode,f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId,psc.name,null as PartsPurchaseOrderTypeId,b.CostPrice,jsd.code as 结算单号
  from sddcs.PartsInboundCheckBill a
     left join (
       select prsb.status,prsr.sourcecode,prsb.code from sddcs.PartsRequisitionSettleBill prsb
              left join sddcs.PartsRequisitionSettleRef prsr on prsb.id = prsr.partsrequisitionsettlebillid
              where prsb.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join sddcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join sddcs.warehouse c on c.id = a.warehouseid
 inner join sddcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join sddcs.sparepart f on f.id = b.sparepartid
 inner join sddcs.InternalAcquisitionBill e on e.id =
                                             d.OriginalRequirementBillId
 left join sddcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 4
   and a.storagecompanyid in( 11181,12261)
 group by a.branchcode, a.code,c.storagecompanyid, c.code, c.name, c.id, e.code, a.counterpartcompanycode,a.counterpartcompanyname,a.createtime,f.code,f.name,
b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice,  jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型,a.code as InCode, c.storagecompanyid, c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code,
5 as kind, '配件零售退货入库' as KindName,a.CounterpartCompanyCode as CompanyCode, a.CounterpartCompanyName as CompanyName,a.CreateTime, f.code as PartCode, f.name as PartName,
sum(b.InspectedQuantity) as InQty,b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue, a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice,jsd.code as 结算单号
  from sddcs.PartsInboundCheckBill a
      left join (
       select psrs.status,psrsr.sourcecode,psrs.code from sddcs.PartsSalesRtnSettlement psrs
              left join sddcs.PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
              where psrs.status in (1,2,3,5,6)
       union all
       select pss.status,pssr.sourcecode,pss.code from sddcs.PartsSalesSettlement pss
              left join sddcs.PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
              where pss.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join sddcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join sddcs.warehouse c on c.id = a.warehouseid
 inner join sddcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join sddcs.sparepart f on f.id = b.sparepartid
 inner join sddcs.PartsRetailReturnBill e on e.id =
                                           d.OriginalRequirementBillId
 left join sddcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 5
   and a.storagecompanyid in( 11181,12261)
 group by a.branchcode,a.code,c.storagecompanyid,c.code, c.name,c.id, e.code, a.counterpartcompanycode, a.counterpartcompanyname,a.createtime, f.code, f.name,
 b.settlementprice, a.settlementstatus, a.PartsSalesCategoryId, psc.name, b.CostPrice ,jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型,a.code as InCode, c.storagecompanyid, c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId, e.code as Code, 6 as kind,
'积压件调剂入库' as KindName,a.CounterpartCompanyCode as CompanyCode,a.CounterpartCompanyName as CompanyName,a.CreateTime, f.code as PartCode, f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price,sum(b.InspectedQuantity * b.Settlementprice) as Total, sum(b.inspectedquantity * b.CostPrice) as TotalJH,
      case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4
          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回'
            when jsd.status = 6 then '发票已审核' end as settlementstatusValue,  a.PartsSalesCategoryId, psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice,jsd.code as 结算单号
  from sddcs.PartsInboundCheckBill a
      left join (
      select psrs.status,psrsr.sourcecode,psrs.code from sddcs.PartsSalesRtnSettlement psrs
              left join sddcs.PartsSalesRtnSettlementRef psrsr on psrs.id = psrsr.partssalesrtnsettlementid
              where psrs.status in (1,2,3,5,6)
       union all
       select pss.status,pssr.sourcecode,pss.code from sddcs.PartsSalesSettlement pss
              left join sddcs.PartsSalesSettlementRef pssr on pss.id = pssr.partssalessettlementid
              where pss.status in (1,2,3,5,6)
  ) jsd on jsd.sourcecode = a.code
 inner join sddcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join sddcs.warehouse c on c.id = a.warehouseid
 inner join sddcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join sddcs.sparepart f on f.id = b.sparepartid
 inner join sddcs.OverstockPartsAdjustBill e on e.id =
                                              d.OriginalRequirementBillId
 left join sddcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 6
   and a.storagecompanyid in( 11181,12261)
 group by a.branchcode,a.code,c.storagecompanyid,c.code,c.name,c.id,e.code,a.counterpartcompanycode,a.counterpartcompanyname, a.createtime, f.code,f.name,
b.settlementprice,a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice  ,jsd.status,jsd.code
union all
select a.branchcode,null as 采购订单类型, a.code as InCode, c.storagecompanyid,c.Code WarehouseCode, c.Name WarehouseName, c.id as WarehouseId,e.code as Code,
 7 as kind,'配件外采入库' as KindName,a.CounterpartCompanyCode as CompanyCode, a.CounterpartCompanyName as CompanyName, a.CreateTime, f.code as PartCode, f.name as PartName,
 sum(b.InspectedQuantity) as InQty, b.SettlementPrice as Price, sum(b.InspectedQuantity * b.Settlementprice) as Total,sum(b.inspectedquantity * b.CostPrice) as TotalJH,
        case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2  end as settlementstatus,
       case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' end as settlementstatusValue, a.PartsSalesCategoryId,psc.name, null as PartsPurchaseOrderTypeId, b.CostPrice
       ,null as 结算单号
        from sddcs.PartsInboundCheckBill a
 inner join sddcs.partsinboundcheckbilldetail b on a.id =
                                                 b.partsinboundcheckbillid
 inner join sddcs.warehouse c on c.id = a.warehouseid
 inner join sddcs.partsinboundplan d on d.id = a.partsinboundplanid
 inner join sddcs.sparepart f on f.id = b.sparepartid
 inner join sddcs.PartsOuterPurchaseChange e on e.id =
                                              d.OriginalRequirementBillId
 left join sddcs.PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
 where a.inboundtype = 7
   and a.storagecompanyid in( 11181,12261)
 group by a.branchcode, a.code, c.storagecompanyid, c.code,c.name, c.id, e.code,a.counterpartcompanycode,a.counterpartcompanyname,a.createtime,f.code, f.name,
 b.settlementprice, a.settlementstatus,a.PartsSalesCategoryId,psc.name,b.CostPrice;
