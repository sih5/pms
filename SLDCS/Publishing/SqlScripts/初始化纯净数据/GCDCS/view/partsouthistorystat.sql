create or replace view partsouthistorystat as
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       1 as kind,
       '销售出库' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
       case when a.settlementstatus=1 then 1
         when a.settlementstatus=2 then 2
           when jsd.status = 1 then 3
             when jsd.status = 2 then 4
               when jsd.status = 3 then 5
                when jsd.status = 4 then 6 end as  settlementstatus,
       case
         when a.settlementstatus=1 then
          '不结算'
         when a.settlementstatus=2 then
          '待结算'
         when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批'
              when jsd.status=3 then'发票已登记'
                when jsd.status= 4 then '已反冲'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       pso.PartsSalesOrderTypeName as 销售订单类型,
       pso.PartsSalesOrderTypeId,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from dcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from dcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from dcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from dcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( jsd.code as varchar2(50)) 结算单号
  from dcs.partsoutboundbill a
 inner join dcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join dcs.warehouse c on c.id = a.warehouseid
 inner join dcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join dcs.sparepart f on f.id = b.sparepartid
 inner join dcs.company h on h.id=a.counterpartcompanyid
 inner join dcs.partssalesorder e on e.code = d.originalrequirementbillcode
 inner join dcs.company h on h.id=a.counterpartcompanyid
 left join dcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
 left join dcs.PartsSalesOrder pso on a.OriginalRequirementBillId = pso.id
 left join(select pss.status,pssr.sourcecode,pss.code from dcs.partssalessettlementref pssr left join dcs.partssalessettlement pss on pss.id=pssr.partssalessettlementid
 where pss.status in(1,2,3)
 )jsd on jsd.sourcecode=a.code
 where a.outboundtype = 1
   and c.storagecompanyid in (2,2203)
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          pso.PartsSalesOrderTypeName,
          pso.partssalesordertypeid,
          psc.name, h.id,h.type,jsd.status,jsd.code
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       nvl(e.code, g.code) as Code,
       2 as kind,
       '采购退货出库' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
       case
         when a.settlementstatus = 1 then
          1
         when a.settlementstatus = 2 then
          2
         when jsd.status = 1 then
          3
         when jsd.status = 2 then
          4
         when jsd.status = 3 then
          5
         when jsd.status = 4 then
          6
         when jsd.status = 5 then
          7
         when jsd.status = 6 then
          8
           when jsds.status = 1 then
          3
         when jsds.status = 2 then
          4
         when jsds.status = 3 then
          5
         when jsds.status = 4 then
          6
         when jsds.status = 5 then
          7
         when jsds.status = 6 then
          8
       end as settlementstatus,
       case
         when a.settlementstatus = 1 then
          '不结算'
         when a.settlementstatus = 2 then
          '待结算'
         when jsd.status = 1 then
          '新建'
         when jsd.status = 2 then
          '已审批'
         when jsd.status = 3 then
          '发票登记'
         when jsd.status = 4 then
          '已反冲'
         when jsd.status = 5 then
          '发票驳回'
         when jsd.status = 6 then
          '发票已审核'
            when jsds.status = 1 then
          '新建'
         when jsds.status = 2 then
          '已审批'
         when jsds.status = 3 then
          '发票登记'
         when jsds.status = 4 then
          '已反冲'
         when jsds.status = 5 then
          '发票驳回'
         when jsds.status = 6 then
          '发票已审核'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       decode(h.type,
              2,
              (select distinct g.businesscode
                 from dcs.dealerserviceinfo g
                where g.partssalescategoryid = a.partssalescategoryid
                  and g.dealerid = h.id
                  and g.status <> 99),
              7,
              (select i.businesscode
                 from dcs.salesunit i
                where i.partssalescategoryid = a.partssalescategoryid
                  and i.ownercompanyid = h.id
                  and i.status <> 99),
              3,
              (select i.businesscode
                 from dcs.salesunit i
                where i.partssalescategoryid = a.partssalescategoryid
                  and i.ownercompanyid = h.id
                  and i.status <> 99),
              6,
              (select j.businesscode
                 from dcs.branchsupplierrelation j
                where j.partssalescategoryid = a.partssalescategoryid
                  and j.supplierid = h.id
                  and j.status <> 99),
              null) as 业务编码,cast( nvl(jsd.code ,jsds.code)as varchar2(50)) 结算单号
  from dcs.partsoutboundbill a
 inner join dcs.partsoutboundbilldetail b
    on a.id = b.partsoutboundbillid
 inner join dcs.warehouse c
    on c.id = a.warehouseid
 inner join dcs.partsoutboundplan d
    on d.id = a.partsoutboundplanid
 inner join dcs.sparepart f
    on f.id = b.sparepartid
 inner join dcs.company h
    on h.id = a.counterpartcompanyid
  left join dcs.partssalesreturnbill e
    on e.code = d.originalrequirementbillcode
  left join dcs.partspurreturnorder g
    on g.code = d.originalrequirementbillcode
   and g.status <> 99
  left join dcs.partssalescategory psc
    on a.PartsSalesCategoryId = psc.id
  left join (select pprsb.status, pprsr.sourcecode, pprsb.code
               from dcs.PartsPurchaseRtnSettleRef pprsr
               left join dcs.PartsPurchaseRtnSettleBill pprsb
                 on pprsb.id = pprsr.partspurchasertnsettlebillid
              where pprsb.status in (1, 2, 3, 5,6)) jsd
    on jsd.sourcecode = a.code
    left join (select pprsb.status, pprsr.sourcecode, pprsb.code
               from dcs.partspurchasesettleref pprsr
               left join dcs.partspurchasesettlebill pprsb
                 on pprsb.id = pprsr.PARTSPURCHASESETTLEBILLID
              where pprsb.status in (1, 2, 3,5,6)) jsds
    on jsds.sourcecode = a.code
 where a.outboundtype = 2
   and c.storagecompanyid in (2, 2203)
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          nvl(e.code, g.code),
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,
          b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name,
          h.id,
          h.type,
          jsd.status,
          jsd.code,jsds.code,jsds.status
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       3 as kind,
       '内部领出' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
       case when a.settlementstatus=1 then 1
        when  a.settlementstatus=2 then 2
          when jsd.status=1 then 3
            when jsd.status=2 then 4
end as settlementstatus ,
       case
         when a.settlementstatus=1 then
          '不结算'
         when a.settlementstatus=2 then
          '待结算'
         when jsd.status=1 then
          '新建'
          when jsd.status=2 then
            '已审核'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
      /* decode(h.type,2,(select distinct g.businesscode from dcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from dcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from dcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from dcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null)*/null as 业务编码,cast( jsd.code as varchar2(50)) as 结算单号
  from dcs.partsoutboundbill a
 inner join dcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join dcs.warehouse c on c.id = a.warehouseid
 inner join dcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join dcs.sparepart f on f.id = b.sparepartid
  /*inner join dcs.company h on h.id=a.counterpartcompanyid*/
 inner join dcs.InternalAllocationBill e on e.id =
                                            d.originalrequirementbillid
 left join dcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
 left join (select prsb.status, prsr.sourcecode, prsb.code
               from dcs.PartsRequisitionSettleRef prsr
               left join dcs.PartsRequisitionSettleBill prsb
                 on prsb.id = prsr.partsrequisitionsettlebillid
              where prsb.status in (1, 2)) jsd
    on jsd.sourcecode = a.code
 where a.outboundtype = 3
   and c.storagecompanyid in (2,2203)
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name/*, h.id,h.type*/,jsd.code,jsd.status
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       4 as kind,
       '配件调拨' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
       a.settlementstatus,
       case a.settlementstatus
         when 1 then
          '不结算'
         when 2 then
          '待结算'
         when 3 then
          '已结算'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from dcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from dcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from dcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from dcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( null as varchar2(50)) as 结算单号
  from dcs.partsoutboundbill a
 inner join dcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join dcs.warehouse c on c.id = a.warehouseid
 inner join dcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join dcs.sparepart f on f.id = b.sparepartid
  inner join dcs.company h on h.id=a.counterpartcompanyid
 inner join dcs.PartsTransferOrder e on e.id = d.sourceid/*d.originalrequirementbillid*/
 left join dcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
 where a.outboundtype = 4
   and c.storagecompanyid in (2,2203)
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name, h.id,h.type
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       5 as kind,
       '积压件调剂' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
       a.settlementstatus,
       case a.settlementstatus
         when 1 then
          '不结算'
         when 2 then
          '待结算'
         when 3 then
          '已结算'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from dcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from dcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from dcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from dcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,null as 结算单号
  from dcs.partsoutboundbill a
 inner join dcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join dcs.warehouse c on c.id = a.warehouseid
 inner join dcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join dcs.sparepart f on f.id = b.sparepartid
  inner join dcs.company h on h.id=a.counterpartcompanyid
 inner join dcs.OverstockPartsAdjustBill e on e.id =
                                              d.originalrequirementbillid
 left join dcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
 where a.outboundtype = 5
   and c.storagecompanyid in (2,2203)
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name, h.id,h.type
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       6 as kind,
       '配件零售' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
       a.settlementstatus,
       case a.settlementstatus
         when 1 then
          '不结算'
         when 2 then
          '待结算'
         when 3 then
          '已结算'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from dcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from dcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from dcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from dcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( null as varchar2(50)) as 结算单号
  from dcs.partsoutboundbill a
 inner join dcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join dcs.warehouse c on c.id = a.warehouseid
 inner join dcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join dcs.sparepart f on f.id = b.sparepartid
  inner join dcs.company h on h.id=a.counterpartcompanyid
 inner join dcs.PartsRetailOrder e on e.id = d.originalrequirementbillid
 left join dcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
 where a.outboundtype = 6
   and c.storagecompanyid in (2,2203)
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name, h.id,h.type
       ------------------------------
union all

select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       1 as kind,
       '销售出库' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
      case when a.settlementstatus=1 then 1
         when a.settlementstatus=2 then 2
           when jsd.status = 1 then 3
             when jsd.status = 2 then 4
               when jsd.status = 3 then 5
                when jsd.status = 4 then 6 end as  settlementstatus,
      case
         when a.settlementstatus=1 then
          '不结算'
         when a.settlementstatus=2 then
          '待结算'
         when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批'
              when jsd.status=3 then'发票已登记'
                when jsd.status= 4 then '已反冲'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       pso.PartsSalesOrderTypeName as 销售订单类型,
       pso.PartsSalesOrderTypeId,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from yxdcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from yxdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from yxdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from yxdcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( jsd.code as varchar2(50)) as 结算单号
  from yxdcs.partsoutboundbill a
 inner join yxdcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join yxdcs.warehouse c on c.id = a.warehouseid
 inner join yxdcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join yxdcs.sparepart f on f.id = b.sparepartid
  inner join yxdcs.company h on h.id=a.counterpartcompanyid
 inner join yxdcs.partssalesorder e on e.code = d.originalrequirementbillcode
 left join yxdcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
 left join yxdcs.PartsSalesOrder pso on a.OriginalRequirementBillId = pso.id
  left join(select pss.status,pssr.sourcecode,pss.code from yxdcs.partssalessettlementref pssr left join yxdcs.partssalessettlement pss on pss.id=pssr.partssalessettlementid
 where pss.status in(1,2,3)
 )jsd on jsd.sourcecode=a.code
 where a.outboundtype = 1
   and c.storagecompanyid = 2
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          pso.PartsSalesOrderTypeName,
          pso.partssalesordertypeid,
          psc.name, h.id,h.type,jsd.status,jsd.code
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       nvl(e.code, g.code) as Code,
       2 as kind,
       '采购退货出库' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
      case
         when a.settlementstatus = 1 then
          1
         when a.settlementstatus = 2 then
          2
         when jsd.status = 1 then
          3
         when jsd.status = 2 then
          4
         when jsd.status = 3 then
          5
         when jsd.status = 4 then
          6
         when jsd.status = 5 then
          7
         when jsd.status = 6 then
          8
          when jsds.status = 1 then
          3
         when jsds.status = 2 then
          4
         when jsds.status = 3 then
          5
         when jsds.status = 4 then
          6
         when jsds.status = 5 then
          7
         when jsds.status = 6 then
          8
       end as settlementstatus,
       case
         when a.settlementstatus = 1 then
          '不结算'
         when a.settlementstatus = 2 then
          '待结算'
         when jsd.status = 1 then
          '新建'
         when jsd.status = 2 then
          '已审批'
         when jsd.status = 3 then
          '发票登记'
         when jsd.status = 4 then
          '已反冲'
         when jsd.status = 5 then
          '发票驳回'
         when jsd.status = 6 then
          '发票已审核'
           when jsds.status = 1 then
          '新建'
         when jsds.status = 2 then
          '已审批'
         when jsds.status = 3 then
          '发票登记'
         when jsds.status = 4 then
          '已反冲'
         when jsds.status = 5 then
          '发票驳回'
         when jsds.status = 6 then
          '发票已审核'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from yxdcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from yxdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from yxdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from yxdcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( nvl(jsd.code ,jsds.code)as varchar2(50))  as 结算单号
  from yxdcs.partsoutboundbill a
 inner join yxdcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join yxdcs.warehouse c on c.id = a.warehouseid
 inner join yxdcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join yxdcs.sparepart f on f.id = b.sparepartid
  inner join yxdcs.company h on h.id=a.counterpartcompanyid
  left join yxdcs.partssalesreturnbill e on e.code =
                                          d.originalrequirementbillcode
  left join yxdcs.partspurreturnorder g on g.code =
                                         d.originalrequirementbillcode
                                           and g.status<>99
  left join yxdcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
   left join (select pprsb.status, pprsr.sourcecode, pprsb.code
               from yxdcs.PartsPurchaseRtnSettleRef pprsr
               left join yxdcs.PartsPurchaseRtnSettleBill pprsb
                 on pprsb.id = pprsr.partspurchasertnsettlebillid
              where pprsb.status in (1, 2, 3,5,6)) jsd
    on jsd.sourcecode = a.code
    left join (select pprsb.status, pprsr.sourcecode, pprsb.code
               from yxdcs.partspurchasesettleref pprsr
               left join yxdcs.partspurchasesettlebill pprsb
                 on pprsb.id = pprsr.PARTSPURCHASESETTLEBILLID
              where pprsb.status in (1, 2, 3,5,6)) jsds
    on jsds.sourcecode = a.code
 where a.outboundtype = 2
   and c.storagecompanyid = 2
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          nvl(e.code, g.code),
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name, h.id,h.type,jsd.status,jsd.code,jsds.code,jsds.status
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       3 as kind,
       '内部领出' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
              case when a.settlementstatus=1 then 1
        when  a.settlementstatus=2 then 2
          when jsd.status=1 then 3
            when jsd.status=2 then 4
end as settlementstatus ,
       case
         when a.settlementstatus=1 then
          '不结算'
         when a.settlementstatus=2 then
          '待结算'
         when jsd.status=1 then
          '新建'
          when jsd.status=2 then
            '已审核'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       /*decode(h.type,2,(select distinct g.businesscode from yxdcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from yxdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from yxdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from yxdcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null)*/null as 业务编码,cast( jsd.code as varchar2(50)) as 结算单号
  from yxdcs.partsoutboundbill a
 inner join yxdcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join yxdcs.warehouse c on c.id = a.warehouseid
 inner join yxdcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join yxdcs.sparepart f on f.id = b.sparepartid
 /* inner join yxdcs.company h on h.id=a.counterpartcompanyid*/
 inner join yxdcs.InternalAllocationBill e on e.id =
                                            d.originalrequirementbillid
 left join yxdcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
  left join (select prsb.status, prsr.sourcecode, prsb.code
               from yxdcs.PartsRequisitionSettleRef prsr
               left join yxdcs.PartsRequisitionSettleBill prsb
                 on prsb.id = prsr.partsrequisitionsettlebillid
              where prsb.status in (1, 2)) jsd
    on jsd.sourcecode = a.code
 where a.outboundtype = 3
   and c.storagecompanyid = 2
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name/*, h.id,h.type*/,jsd.code,jsd.status
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       4 as kind,
       '配件调拨' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
       a.settlementstatus,
       case a.settlementstatus
         when 1 then
          '不结算'
         when 2 then
          '待结算'
         when 3 then
          '已结算'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from yxdcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from yxdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from yxdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from yxdcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( null as varchar2(50))  as 结算单号
  from yxdcs.partsoutboundbill a
 inner join yxdcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join yxdcs.warehouse c on c.id = a.warehouseid
 inner join yxdcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join yxdcs.sparepart f on f.id = b.sparepartid
  inner join yxdcs.company h on h.id=a.counterpartcompanyid
 inner join yxdcs.PartsTransferOrder e on e.id =  d.sourceid/*d.originalrequirementbillid*/
 left join yxdcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
 where a.outboundtype = 4
   and c.storagecompanyid = 2
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name, h.id,h.type
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       5 as kind,
       '积压件调剂' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
       a.settlementstatus,
       case a.settlementstatus
         when 1 then
          '不结算'
         when 2 then
          '待结算'
         when 3 then
          '已结算'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from yxdcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from yxdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from yxdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from yxdcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( null as varchar2(50))  as 结算单号
  from yxdcs.partsoutboundbill a
 inner join yxdcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join yxdcs.warehouse c on c.id = a.warehouseid
 inner join yxdcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join yxdcs.sparepart f on f.id = b.sparepartid
  inner join yxdcs.company h on h.id=a.counterpartcompanyid
 inner join yxdcs.OverstockPartsAdjustBill e on e.id =
                                              d.originalrequirementbillid
 left join yxdcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
 where a.outboundtype = 5
   and c.storagecompanyid = 2
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name, h.id,h.type
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       6 as kind,
       '配件零售' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
       a.settlementstatus,
       case a.settlementstatus
         when 1 then
          '不结算'
         when 2 then
          '待结算'
         when 3 then
          '已结算'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from yxdcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from yxdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from yxdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from yxdcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( null as varchar2(50))  as 结算单号
  from yxdcs.partsoutboundbill a
 inner join yxdcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join yxdcs.warehouse c on c.id = a.warehouseid
 inner join yxdcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join yxdcs.sparepart f on f.id = b.sparepartid
  inner join yxdcs.company h on h.id=a.counterpartcompanyid
 inner join yxdcs.PartsRetailOrder e on e.id = d.originalrequirementbillid
 left join yxdcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
 where a.outboundtype = 6
   and c.storagecompanyid = 2
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name, h.id,h.type


--------------------------------------------------------------gc-------------------------------------------------------------------
union all

select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       1 as kind,
       '销售出库' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
      case when a.settlementstatus=1 then 1
         when a.settlementstatus=2 then 2
           when jsd.status = 1 then 3
             when jsd.status = 2 then 4
               when jsd.status = 3 then 5
                when jsd.status = 4 then 6 end as  settlementstatus,
       case
         when a.settlementstatus=1 then
          '不结算'
         when a.settlementstatus=2 then
          '待结算'
         when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批'
              when jsd.status=3 then'发票已登记'
                when jsd.status= 4 then '已反冲'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       pso.PartsSalesOrderTypeName as 销售订单类型,
       pso.PartsSalesOrderTypeId,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from gcdcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from gcdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from gcdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from gcdcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( jsd.code as varchar2(50)) as 结算单号
  from gcdcs.partsoutboundbill a
 inner join gcdcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join gcdcs.warehouse c on c.id = a.warehouseid
 inner join gcdcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join gcdcs.sparepart f on f.id = b.sparepartid
  inner join gcdcs.company h on h.id=a.counterpartcompanyid
 inner join gcdcs.partssalesorder e on e.code = d.originalrequirementbillcode
 left join gcdcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
 left join gcdcs.PartsSalesOrder pso on a.OriginalRequirementBillId = pso.id
  left join(select pss.status,pssr.sourcecode,pss.code from gcdcs.partssalessettlementref pssr left join gcdcs.partssalessettlement pss on pss.id=pssr.partssalessettlementid
 where pss.status in(1,2,3)
 )jsd on jsd.sourcecode=a.code
 where a.outboundtype = 1
   and c.storagecompanyid = 8481
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          pso.PartsSalesOrderTypeName,
          pso.partssalesordertypeid,
          psc.name, h.id,h.type,jsd.status,jsd.code
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       nvl(e.code, g.code) as Code,
       2 as kind,
       '采购退货出库' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
      case
         when a.settlementstatus = 1 then
          1
         when a.settlementstatus = 2 then
          2
         when jsd.status = 1 then
          3
         when jsd.status = 2 then
          4
         when jsd.status = 3 then
          5
         when jsd.status = 4 then
          6
         when jsd.status = 5 then
          7
         when jsd.status = 6 then
          8
          when jsds.status = 1 then
          3
         when jsds.status = 2 then
          4
         when jsds.status = 3 then
          5
         when jsds.status = 4 then
          6
         when jsds.status = 5 then
          7
         when jsds.status = 6 then
          8
       end as settlementstatus,
       case
         when a.settlementstatus = 1 then
          '不结算'
         when a.settlementstatus = 2 then
          '待结算'
         when jsd.status = 1 then
          '新建'
         when jsd.status = 2 then
          '已审批'
         when jsd.status = 3 then
          '发票登记'
         when jsd.status = 4 then
          '已反冲'
         when jsd.status = 5 then
          '发票驳回'
         when jsd.status = 6 then
          '发票已审核'
           when jsds.status = 1 then
          '新建'
         when jsds.status = 2 then
          '已审批'
         when jsds.status = 3 then
          '发票登记'
         when jsds.status = 4 then
          '已反冲'
         when jsds.status = 5 then
          '发票驳回'
         when jsds.status = 6 then
          '发票已审核'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from gcdcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from gcdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from gcdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from gcdcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( nvl(jsd.code ,jsds.code)as varchar2(50))  as 结算单号
  from gcdcs.partsoutboundbill a
 inner join gcdcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join gcdcs.warehouse c on c.id = a.warehouseid
 inner join gcdcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join gcdcs.sparepart f on f.id = b.sparepartid
   inner join gcdcs.company h on h.id=a.counterpartcompanyid
  left join gcdcs.partssalesreturnbill e on e.code =
                                          d.originalrequirementbillcode
  left join gcdcs.partspurreturnorder g on g.code =
                                         d.originalrequirementbillcode
                                           and g.status<>99
  left join gcdcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
   left join (select pprsb.status, pprsr.sourcecode, pprsb.code
               from gcdcs.PartsPurchaseRtnSettleRef pprsr
               left join gcdcs.PartsPurchaseRtnSettleBill pprsb
                 on pprsb.id = pprsr.partspurchasertnsettlebillid
              where pprsb.status in (1, 2, 3, 5,6)) jsd
    on jsd.sourcecode = a.code
    left join (select pprsb.status, pprsr.sourcecode, pprsb.code
               from gcdcs.partspurchasesettleref pprsr
               left join gcdcs.partspurchasesettlebill pprsb
                 on pprsb.id = pprsr.PARTSPURCHASESETTLEBILLID
              where pprsb.status in (1, 2, 3, 5,6)) jsds
    on jsds.sourcecode = a.code
 where a.outboundtype = 2
   and c.storagecompanyid = 8481
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          nvl(e.code, g.code),
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name, h.id,h.type,jsd.status,jsd.code,jsds.code,jsds.status
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       3 as kind,
       '内部领出' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
             case when a.settlementstatus=1 then 1
        when  a.settlementstatus=2 then 2
          when jsd.status=1 then 3
            when jsd.status=2 then 4
end as settlementstatus ,
       case
         when a.settlementstatus=1 then
          '不结算'
         when a.settlementstatus=2 then
          '待结算'
         when jsd.status=1 then
          '新建'
          when jsd.status=2 then
            '已审核'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       /*decode(h.type,2,(select distinct g.businesscode from gcdcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from gcdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from gcdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from gcdcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null)*/null as 业务编码,cast( jsd.code as varchar2(50)) as 结算单号
  from gcdcs.partsoutboundbill a
 inner join gcdcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join gcdcs.warehouse c on c.id = a.warehouseid
 inner join gcdcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join gcdcs.sparepart f on f.id = b.sparepartid
 --  inner join gcdcs.company h on h.id=a.counterpartcompanyid
 inner join gcdcs.InternalAllocationBill e on e.id =
                                            d.originalrequirementbillid
 left join gcdcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
  left join (select prsb.status, prsr.sourcecode, prsb.code
               from gcdcs.PartsRequisitionSettleRef prsr
               left join gcdcs.PartsRequisitionSettleBill prsb
                 on prsb.id = prsr.partsrequisitionsettlebillid
              where prsb.status in (1, 2)) jsd
    on jsd.sourcecode = a.code
 where a.outboundtype = 3
   and c.storagecompanyid = 8481
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name/*, h.id,h.type*/,jsd.code,jsd.status
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       4 as kind,
       '配件调拨' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
       a.settlementstatus,
       case a.settlementstatus
         when 1 then
          '不结算'
         when 2 then
          '待结算'
         when 3 then
          '已结算'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from gcdcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from gcdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from gcdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from gcdcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( null as varchar2(50))  as 结算单号
  from gcdcs.partsoutboundbill a
 inner join gcdcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join gcdcs.warehouse c on c.id = a.warehouseid
 inner join gcdcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join gcdcs.sparepart f on f.id = b.sparepartid
   inner join gcdcs.company h on h.id=a.counterpartcompanyid
 inner join gcdcs.PartsTransferOrder e on e.id =  d.sourceid/*d.originalrequirementbillid*/
 left join gcdcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
 where a.outboundtype = 4
   and c.storagecompanyid = 8481
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name, h.id,h.type
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       5 as kind,
       '积压件调剂' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
       a.settlementstatus,
       case a.settlementstatus
         when 1 then
          '不结算'
         when 2 then
          '待结算'
         when 3 then
          '已结算'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from gcdcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from gcdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from gcdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from gcdcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( null as varchar2(50))  as 结算单号
  from gcdcs.partsoutboundbill a
 inner join gcdcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join gcdcs.warehouse c on c.id = a.warehouseid
 inner join gcdcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join gcdcs.sparepart f on f.id = b.sparepartid
   inner join gcdcs.company h on h.id=a.counterpartcompanyid
 inner join gcdcs.OverstockPartsAdjustBill e on e.id =
                                              d.originalrequirementbillid
 left join gcdcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
 where a.outboundtype = 5
   and c.storagecompanyid = 8481
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name, h.id,h.type
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       6 as kind,
       '配件零售' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
       a.settlementstatus,
       case a.settlementstatus
         when 1 then
          '不结算'
         when 2 then
          '待结算'
         when 3 then
          '已结算'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from gcdcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from gcdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from gcdcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from gcdcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( null as varchar2(50))  as 结算单号
  from gcdcs.partsoutboundbill a
 inner join gcdcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join gcdcs.warehouse c on c.id = a.warehouseid
 inner join gcdcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join gcdcs.sparepart f on f.id = b.sparepartid
  inner join gcdcs.company h on h.id=a.counterpartcompanyid
 inner join gcdcs.PartsRetailOrder e on e.id = d.originalrequirementbillid
 left join gcdcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
 where a.outboundtype = 6
   and c.storagecompanyid = 8481
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name, h.id,h.type

             union all

select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       1 as kind,
       '销售出库' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
       case when a.settlementstatus=1 then 1
         when a.settlementstatus=2 then 2
           when jsd.status = 1 then 3
             when jsd.status = 2 then 4
               when jsd.status = 3 then 5
                when jsd.status = 4 then 6 end as  settlementstatus,
      case
         when a.settlementstatus=1 then
          '不结算'
         when a.settlementstatus=2 then
          '待结算'
         when jsd.status = 1 then '新建'
            when jsd.status = 2 then '已审批'
              when jsd.status=3 then'发票已登记'
                when jsd.status= 4 then '已反冲'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       pso.PartsSalesOrderTypeName as 销售订单类型,
       pso.PartsSalesOrderTypeId,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from sddcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from sddcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from sddcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from sddcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( jsd.code as varchar2(50)) 结算单号
  from sddcs.partsoutboundbill a
 inner join sddcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join sddcs.warehouse c on c.id = a.warehouseid
 inner join sddcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join sddcs.sparepart f on f.id = b.sparepartid
  inner join sddcs.company h on h.id=a.counterpartcompanyid
 inner join sddcs.partssalesorder e on e.code = d.originalrequirementbillcode
 left join sddcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
 left join sddcs.PartsSalesOrder pso on a.OriginalRequirementBillId = pso.id
  left join(select pss.status,pssr.sourcecode,pss.code from sddcs.partssalessettlementref pssr left join sddcs.partssalessettlement pss on pss.id=pssr.partssalessettlementid
 where pss.status in(1,2,3)
 )jsd on jsd.sourcecode=a.code
 where a.outboundtype = 1
   and c.storagecompanyid in( 11181,12261)
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          pso.PartsSalesOrderTypeName,
          pso.partssalesordertypeid,
          psc.name, h.id,h.type,jsd.code,jsd.status
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       nvl(e.code, g.code) as Code,
       2 as kind,
       '采购退货出库' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
      case
         when a.settlementstatus = 1 then
          1
         when a.settlementstatus = 2 then
          2
         when jsd.status = 1 then
          3
         when jsd.status = 2 then
          4
         when jsd.status = 3 then
          5
         when jsd.status = 4 then
          6
         when jsd.status = 5 then
          7
         when jsd.status = 6 then
          8
          when jsds.status = 1 then
          3
         when jsds.status = 2 then
          4
         when jsds.status = 3 then
          5
         when jsds.status = 4 then
          6
         when jsds.status = 5 then
          7
         when jsds.status = 6 then
          8
       end as settlementstatus,
       case
         when a.settlementstatus = 1 then
          '不结算'
         when a.settlementstatus = 2 then
          '待结算'
         when jsd.status = 1 then
          '新建'
         when jsd.status = 2 then
          '已审批'
         when jsd.status = 3 then
          '发票登记'
         when jsd.status = 4 then
          '已反冲'
         when jsd.status = 5 then
          '发票驳回'
         when jsd.status = 6 then
          '发票已审核'
           when jsds.status = 1 then
          '新建'
         when jsds.status = 2 then
          '已审批'
         when jsds.status = 3 then
          '发票登记'
         when jsds.status = 4 then
          '已反冲'
         when jsds.status = 5 then
          '发票驳回'
         when jsds.status = 6 then
          '发票已审核'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from sddcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from sddcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from sddcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from sddcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( nvl(jsd.code ,jsds.code)as varchar2(50))  as 结算单号
  from sddcs.partsoutboundbill a
 inner join sddcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join sddcs.warehouse c on c.id = a.warehouseid
 inner join sddcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join sddcs.sparepart f on f.id = b.sparepartid
  inner join sddcs.company h on h.id=a.counterpartcompanyid
  left join sddcs.partssalesreturnbill e on e.code =
                                          d.originalrequirementbillcode
  left join sddcs.partspurreturnorder g on g.code =
                                         d.originalrequirementbillcode
                                           and g.status<>99
  left join sddcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
   left join (select pprsb.status, pprsr.sourcecode, pprsb.code
               from sddcs.PartsPurchaseRtnSettleRef pprsr
               left join sddcs.PartsPurchaseRtnSettleBill pprsb
                 on pprsb.id = pprsr.partspurchasertnsettlebillid
              where pprsb.status in (1, 2, 3, 5,6)) jsd
    on jsd.sourcecode = a.code
left join (select pprsb.status, pprsr.sourcecode, pprsb.code
               from sddcs.partspurchasesettleref pprsr
               left join sddcs.partspurchasesettlebill pprsb
                 on pprsb.id = pprsr.PARTSPURCHASESETTLEBILLID
              where pprsb.status in (1, 2, 3, 5,6)) jsds
    on jsds.sourcecode = a.code
 where a.outboundtype = 2
   and c.storagecompanyid in( 11181,12261)
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          nvl(e.code, g.code),
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name, h.id,h.type,jsd.status,jsd.code,jsds.code ,jsds.status
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       3 as kind,
       '内部领出' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
             case when a.settlementstatus=1 then 1
        when  a.settlementstatus=2 then 2
          when jsd.status=1 then 3
            when jsd.status=2 then 4
end as settlementstatus ,
       case
         when a.settlementstatus=1 then
          '不结算'
         when a.settlementstatus=2 then
          '待结算'
         when jsd.status=1 then
          '新建'
          when jsd.status=2 then
            '已审核'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       /*decode(h.type,2,(select distinct g.businesscode from sddcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from sddcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from sddcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from sddcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null)*/null as 业务编码,cast( jsd.code as varchar2(50)) as 结算单号
  from sddcs.partsoutboundbill a
 inner join sddcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join sddcs.warehouse c on c.id = a.warehouseid
 inner join sddcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join sddcs.sparepart f on f.id = b.sparepartid
 -- inner join sddcs.company h on h.id=a.counterpartcompanyid
 inner join sddcs.InternalAllocationBill e on e.id =
                                            d.originalrequirementbillid
 left join sddcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
  left join (select prsb.status, prsr.sourcecode, prsb.code
               from sddcs.PartsRequisitionSettleRef prsr
               left join sddcs.PartsRequisitionSettleBill prsb
                 on prsb.id = prsr.partsrequisitionsettlebillid
              where prsb.status in (1, 2)) jsd
    on jsd.sourcecode = a.code
 where a.outboundtype = 3
   and c.storagecompanyid in( 11181,12261)
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name/*, h.id,h.type*/,jsd.code,jsd.status
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       4 as kind,
       '配件调拨' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
       a.settlementstatus,
       case a.settlementstatus
         when 1 then
          '不结算'
         when 2 then
          '待结算'
         when 3 then
          '已结算'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from sddcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from sddcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from sddcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from sddcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( null as varchar2(50))  as 结算单号
  from sddcs.partsoutboundbill a
 inner join sddcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join sddcs.warehouse c on c.id = a.warehouseid
 inner join sddcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join sddcs.sparepart f on f.id = b.sparepartid
   inner join sddcs.company h on h.id=a.counterpartcompanyid
 inner join sddcs.PartsTransferOrder e on e.id =  d.sourceid

 left join sddcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
 where a.outboundtype = 4
   and c.storagecompanyid in( 11181,12261)
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name, h.id,h.type
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       5 as kind,
       '积压件调剂' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
       a.settlementstatus,
       case a.settlementstatus
         when 1 then
          '不结算'
         when 2 then
          '待结算'
         when 3 then
          '已结算'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from sddcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from sddcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from sddcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from sddcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( null as varchar2(50))  as 结算单号
  from sddcs.partsoutboundbill a
 inner join sddcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join sddcs.warehouse c on c.id = a.warehouseid
 inner join sddcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join sddcs.sparepart f on f.id = b.sparepartid
   inner join sddcs.company h on h.id=a.counterpartcompanyid
 inner join sddcs.OverstockPartsAdjustBill e on e.id =
                                              d.originalrequirementbillid
 left join sddcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
 where a.outboundtype = 5
   and c.storagecompanyid in( 11181,12261)
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name, h.id,h.type
union all
select a.branchcode,
       a.code as outcode,
       c.storagecompanyid,
       c.Code WarehouseCode,
       c.Name WarehouseName,
       c.id as WarehouseId,
       e.code,
       6 as kind,
       '配件零售' as KindName,
       a.CounterpartCompanyCode as CompanyCode,
       a.CounterpartCompanyName as CompanyName,
       a.CreateTime,
       f.code as PartCode,
       f.name as PartName,
       sum(b.outboundamount) as OutQty,
       b.SettlementPrice as Price,
       sum(b.outboundamount * b.Settlementprice) as Total,
       sum(b.CostPrice * b.outboundamount) as TotalJH,
       b.CostPrice,
       a.settlementstatus,
       case a.settlementstatus
         when 1 then
          '不结算'
         when 2 then
          '待结算'
         when 3 then
          '已结算'
       end as settlementstatusValue,
       a.PartsSalesCategoryId,
       a.partssalesordertypename as 销售订单类型,
       a.partssalesordertypeid,
       psc.name as 品牌,
       decode(h.type,2,(select distinct g.businesscode from sddcs.dealerserviceinfo g where g.partssalescategoryid=a.partssalescategoryid and g.dealerid=h.id and g.status<>99),
       7,(select i.businesscode from sddcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       3,(select i.businesscode from sddcs.salesunit i where i.partssalescategoryid=a.partssalescategoryid and i.ownercompanyid=h.id and i.status<>99),
       6,(select j.businesscode from sddcs.branchsupplierrelation j where j.partssalescategoryid=a.partssalescategoryid and j.supplierid=h.id and j.status<>99),null) as 业务编码,cast( null as varchar2(50))  as 结算单号
  from sddcs.partsoutboundbill a
 inner join sddcs.partsoutboundbilldetail b on a.id = b.partsoutboundbillid
 inner join sddcs.warehouse c on c.id = a.warehouseid
 inner join sddcs.partsoutboundplan d on d.id = a.partsoutboundplanid
 inner join sddcs.sparepart f on f.id = b.sparepartid
   inner join sddcs.company h on h.id=a.counterpartcompanyid
 inner join sddcs.PartsRetailOrder e on e.id = d.originalrequirementbillid
 left join sddcs.partssalescategory psc on a.PartsSalesCategoryId = psc.id
 where a.outboundtype = 6
   and c.storagecompanyid in( 11181,12261)
 group by a.branchcode,
          a.code,
          c.storagecompanyid,
          c.code,
          c.name,
          c.id,
          e.code,
          a.counterpartcompanycode,
          a.counterpartcompanyname,
          a.createtime,
          f.code,
          f.name,
          b.settlementprice,b.CostPrice,
          a.settlementstatus,
          a.PartsSalesCategoryId,
          a.partssalesordertypename,
          a.partssalesordertypeid,
          psc.name
, h.id,h.type;
