create or replace view partspurchaseonline as
select BranchId,
       PartsSalesCategoryId,
       SparePartId as PartId,
       sum(nums) as nums
  from (select orders.BranchId,
               orders.PartsSalesCategoryId,
               detail.SparePartId,
               sum(detail.ConfirmedAmount) as nums  --确认量
          from PartsPurchaseOrderDetail detail
         inner join PartsPurchaseOrder orders  --配件采购订单
            on orders.id = detail.partspurchaseorderid
         where orders.status in (3, 4, 5, 6) --部分确认  确认完毕 部分发运 发运完毕
           and orders.InStatus = 5 --未入库
         group by orders.BranchId,
                  orders.PartsSalesCategoryId,
                  detail.SparePartId
        union all
        select plans.branchid,
               plans.partssalescategoryid,
               detail.sparepartid,
               sum(detail.PlannedAmount - detail.InspectedQuantity) as nums  -- 计划量 - 检验量
          from PartsInboundPlan plans  --配件入库计划
         inner join PartsInboundPlanDetail detail
            on plans.id = detail.PartsInboundPlanId
         where plans.InboundType = 1 --配件采购
           and plans.Status in (1, 4) --新建、部分检验
         group by plans.branchid,
                  plans.partssalescategoryid,
                  detail.sparepartid)
 group by BranchId, PartsSalesCategoryId, SparePartId;
