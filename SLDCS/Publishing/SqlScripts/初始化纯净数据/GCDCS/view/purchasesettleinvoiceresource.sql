create or replace view purchasesettleinvoiceresource as
select a.id partspurchasesettleid,
       a.code partspurchasesettlecode,
       '配件采购结算单' type,
       b.invoiceid invoiceid
  from partspurchasesettlebill a
 inner join PurchaseSettleInvoiceRel b
    on a.id = b.partspurchasesettlebillid
union
select a.id, a.code, '配件采购退货结算单' type, b.invoiceid
  from PartsPurchaseRtnSettleBill a
 inner join PurchaseRtnSettleInvoiceRel b
    on a.id = b.PARTSPURCHASERTNSETTLEBILLID;
