create or replace view qtsdataqueryfornfgc as
select a.Cscode,
         a.Zcbmcode,
         a.Vincode,
         b.Assembly_date,
         a.Bind_date,
         b.Factory,
         b.Mapid,
         b.Patch,
         b.Produceline,
         b.Mapname,
         b.submittime
    from qts.o_vinbind @NFGCCQTS a
    left join qts.o_tmepassembly_cs @NFGCCQTS b on a.cscode = b.cscode;
