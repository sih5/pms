create or replace view repairitemlaborhourview as
select  repairitem.id               as RepairItemId,
       RepairItem.Code             as Code,
       RepairItem.Name             as Name,
       RepairItemCategory.id       as RepairItemCategoryId,
       RepairItemCategory.code     as RepairItemCategoryCode,
       RepairItemCategory.Name     as RepairItemCategoryName,
       RepairItemCategory.parentid as ParentId,
       Parent_RepairItemCategory.Code as ParentCode,
       RepairItemBrandRelation.Id as MalfunctionBrandRelationId,
       Parent_RepairItemCategory.Name as ParentName,
       RepairItemBrandRelation.Partssalescategoryid,
       Partssalescategory.Name     as PartssalescategoryName,
       RepairItemAffiServProdLine.Serviceproductlineid,
       RepairItemAffiServProdLine.Defaultlaborhour,
       RepairItemAffiServProdLine.ProductLineType as ProductLineType
  from RepairItemCategory
 inner join RepairItemCategory Parent_RepairItemCategory
  on RepairItemCategory.Parentid =Parent_RepairItemCategory.Id
 inner join repairitem
    on repairitem.repairitemcategoryid = RepairItemCategory.Id
   and repairitem.status = 1
 inner join RepairItemBrandRelation
    on RepairItemBrandRelation.Repairitemid = repairitem.id
   and RepairItemBrandRelation.Status = 1
 inner join Partssalescategory
 on Partssalescategory.Id=RepairItemBrandRelation.Partssalescategoryid
 inner join RepairItemAffiServProdLine on RepairItemAffiServProdLine.RepairItemId =RepairItem.Id
 where RepairItemCategory.status = 1 and RepairItemAffiServProdLine.Status=1
   and not exists
 (select *
          from LayerNodeType
         where RepairItemCategory.Layernodetypeid = LayerNodeType.Id
           and LayerNodeType.Toplevelnode = 1);
