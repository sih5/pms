create or replace view serviceproductlineview2 as
select ServiceProductLine.Id                   as ProductLineId,
       1                                       as ProductLineType,
       ServiceProductLine.Code as ProductLineCode,
       ServiceProductLine.Name as ProductLineName,
       ServiceProductLine.Partssalescategoryid,
       ServiceProductLine.Branchid,Status,ServiceProductLine.Creatorname,ServiceProductLine.Createtime,ServiceProductLine.Modifiername,ServiceProductLine.Modifytime
  from ServiceProductLine
 where ServiceProductLine.Status = 1
union all
select EngineProductLine.Id                   as ProductLineId,
       2 as ProductLineType,
       EngineProductLine.Enginecode as ProductLineCode,
       EngineProductLine.Enginename as ProductLineName,
       EngineProductLine.Partssalescategoryid,
       EngineProductLine.Branchid,Status,EngineProductLine.Creatorname,EngineProductLine.Createtime,EngineProductLine.Modifiername,EngineProductLine.Modifytime
  from EngineProductLine
 where EngineProductLine.Status=1;
