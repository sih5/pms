create or replace view serviceproductlineviewalldb as
select a.ProductLineId   id,
       a.ProductLineName,
       b.name                                 as PartsSalesCategoryName
  from dcs.serviceproductlineview a
 inner join dcs.partssalescategory b
    on a.Partssalescategoryid = b.id
union
select a.ProductLineId   id,
       a.ProductLineName,
       b.name                                 as PartsSalesCategoryName
  from yxdcs.serviceproductlineview a
 inner join yxdcs.partssalescategory b
    on a.Partssalescategoryid = b.id
union
select a.ProductLineId   id,
       a.ProductLineName,
       b.name                                 as PartsSalesCategoryName
  from gcdcs.serviceproductlineview a
 inner join gcdcs.partssalescategory b
    on a.Partssalescategoryid = b.id
union
select a.ProductLineId   id,
       a.ProductLineName,
       b.name                                 as PartsSalesCategoryName
  from sddcs.serviceproductlineview a
 inner join sddcs.partssalescategory b
    on a.Partssalescategoryid = b.id;
