create or replace view ssclaimsettlementdetailview as
select scd.status as CacuStatus,case scd.status when 1 then '新增' when 2 then '已审批' when 3 then '发票登记' when 4 then '费用兑现'when null then '未结算'  end as CacuStatusValue
,b.branchcode,b.id as CategoryId,a.RepairType,a.status,spl.id as ProductLineId,a.Claimbillcode,st.value StatusValue,a.RepairClaimApplicationCode,a.ServiceTripClaimAppCode,b.name as CategoryName,
a.Vin,spl.name as productlinename,stb.ApprovalComment,c.ProductCategoryName,substr(a.vin,-8) as OutFactoryCode,rt.value as RepairTypeValue,b.BranchName,
d.Name as MarKetName,MalfunctionCode,a.MalfunctionDescription as MalfunctionReason,FaultyPartsCode,FaultyPartsName,nvl(kk1.TransactionAmount,0) as kk1
,kk1.TransactionReason as kkReason1,lx1.value as kklx1,nvl(kk2.TransactionAmount,0) as kk2,
kk2.TransactionReason as kkReason2,lx2.value as kklx2,nvl(wckk.TransactionAmount,0) as kk3,bk1.TransactionAmount as bk1,bk1.TransactionReason as bkReason1,lx3.value as bklx
,a.MaterialCost,a.materialcost - nvl(totalall.clkk,0) as ActualMaterialCost ---实际结算材料费
,round(a.LaborCost,2) as LaborCost,----工时费
a.LaborCost - nvl(totalall.gskk,0) as ActualLaborCost,a.OtherCost - nvl(totalall.Otherkk,0) as OtherCost,a.OtherCostReason,a.TotalAmount,
stb.DealerTripPerson as tbrs,stb.ServiceTripPerson as jsrs,stb.DealerTripDuration as tbts,
stb.ServiceTripDuration as jsts,stb.OutSubsidyPrice as rybz,stb.ServiceTripDistance as tblc,
stb.SettleDistance as jslc,stb.OutServiceCarUnitPrice as clbz,
case rpo.WarrantyStatus when 1 then nvl(stb.DealerTripPerson,0)*nvl(stb.DealerTripDuration,0)*nvl(stb.OutSubsidyPrice,0) + nvl(ServiceTripDistance,0)*stb.OutServiceCarUnitPrice+nvl(stb.TowCharge,0)+nvl(stb.OtherCost,0) else 0 end as WcTotal ,---保内
case rpo.WarrantyStatus when 1 then stb.Totalamount - nvl(wckk.TransactionAmount,0) else 0 end as ActualWcTotal,---保内实际
0 as WcTotalW ,---保外
0 as ActualWcTotalW,---保外实际
(a.materialcost - nvl(kk1.TransactionAmount,0)+a.LaborCost - nvl(kk2.TransactionAmount,0)+ nvl(bk1.TransactionAmount,0) +nvl(stb.Totalamount,0)- nvl(wckk.TransactionAmount,0)+a.OtherCost+a.PartsManagementCost) as ActualTotal
,a.Mileage,a.WorkingHours,a.Capacity,a.RepairRequestTime,a.MalfunctionReason as MalfunctionDescription,a.VehicleContactPerson,
a.ContactPhone,a.ContactPhone as CellNumber,a.ApproveTime,a.ContactAddress,stb.ServiceTripDuration,stb.Servicetripperson,
stb.DetailedAddress,stb.Outservicecarunitprice,stb.Settledistance,stb.OutSubsidyPrice,stb.Towcharge,a.InitialApprovertComment,
a.ServiceDepartmentComment as ServiceDepartmentComment,a.RejectReason,a.RejectQty,a.ClaimSupplierCode as TempSupplierCode,a.ClaimSupplierName as TempSupplierName,
case a.Supplierconfirmstatus when 1 then '未确认' when 2 then '已确认' when 3 then '确认未通过' end as SupplierConfirmStatus,
case a.IfClaimToSupplier when 1 then '是' else '否' end as IfClaimToSupplier,a.DealerCode,a.DealerName,a.CreatorName,a.CreateTime
,a.EngineModel,a.EngineSerialNumber,a.Salesdate,a.OutOfFactoryDate,c.FirstStoppageMileage,a.Suppliercheckcomment,round(gcc.coefficient,2) as Grade,
a.ClaimSupplierName as FaultyPartsSupplierName,a.PartsManagementCost,----配件管理费
nvl(a.PartsManagementCost,0) - nvl(totalall.glfkk,0) as ActualGlfCost,----实际结算管理费
scd.createtime as CacuTime,
null as 产品线分类,   ----------------产品线分类
null as ProductLineType,null as 外出初审意见,null as 外出终审意见
from dcs.RepairClaimBill a
inner join  dcs.partssalescategory b on a.partssalescategoryid = b.id
inner join  dcs.vehicleinformation c on c.id = a.vehicleid
left join  (
            select epl.id,epl.status,epl.enginename as name from dcs.engineproductline epl union all
            select spl1.id,spl1.status,spl1.name from dcs.serviceproductline spl1
       ) spl on spl.id = a.ServiceProductLineId and spl.status=1
left join  dcs.MarketingDepartment d on d.id = a.marketingdepartmentid
left join(select key,value from  dcs.keyvalueitem where category = 'DCS' and name ='RepairClaimBill_RepairClaimStatus') st on st.key = a.status
left join(select key,value from  dcs.keyvalueitem where category = 'DCS' and name ='Repair_Tpye')rt on rt.key = a.repairtype
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from
(select max(a.id) id ,a.sourceid,a.DebitOrReplenish
from  dcs.ExpenseAdjustmentBill a where a.status = 2 group by a.sourceid,a.DebitOrReplenish)a
inner join  dcs.ExpenseAdjustmentBill b on a.id = b.id where b.DebitOrReplenish=1 and b.SourceType in (1,2,3)) kk1 on kk1.sourceid = a.id
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from(select min(a.id) id ,a.sourceid,a.DebitOrReplenish
from  dcs.ExpenseAdjustmentBill a where a.status = 2 group by a.sourceid,a.DebitOrReplenish)a
inner join  dcs.ExpenseAdjustmentBill b on a.id = b.id where b.DebitOrReplenish=1 and b.SourceType in (1,2,3)
) kk2 on kk2.sourceid = a.id and kk1.id<>kk2.id
left join (select key,value from  dcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx1 on lx1.key =kk1.TransactionCategory
left join (select key,value from  dcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx2 on lx2.key =kk2.TransactionCategory
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from(select max(a.id) id ,a.sourceid,a.DebitOrReplenish
from  dcs.ExpenseAdjustmentBill a group by a.sourceid,a.DebitOrReplenish)a
inner join  dcs.ExpenseAdjustmentBill b on a.id = b.id where b.status=2 and b.DebitOrReplenish=2 and b.SourceType in (1,2,3)) bk1 on bk1.sourceid = a.id
left join (select key,value from  dcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx3 on lx3.key =bk1.TransactionCategory
----计算 材料费扣补款合计，工时费扣补款合计 + 配件管理费扣补款合计 + 其他 扣补款合计
left join (
select a.id,nvl(clkk.fee,0)-nvl(clbk.fee,0) as clkk,nvl(gskk.fee,0)-nvl(gsbk.fee,0) as gskk
,nvl(glfkk.fee,0)-nvl(glfbk.fee,0) as glfkk ,nvl(Otherkk.fee,0) - nvl(Otherbk.fee,0)as Otherkk
from
 dcs.RepairClaimBill a left join
(select sourceid,sum(TransactionAmount) as fee from  dcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(2,16)  and DebitOrReplenish=1 and sourcetype = 1 group by sourceid)clkk on clkk.sourceid = a.id
left join(select sourceid,sum(TransactionAmount) as fee from  dcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(2,16) and DebitOrReplenish=2  and sourcetype = 1 group by sourceid)clbk on clbk.sourceid = a.id
----工时扣补款
left join(select sourceid,sum(TransactionAmount) as fee from  dcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(1,15) and DebitOrReplenish=1  and sourcetype = 1 group by sourceid)gskk on gskk.sourceid = a.id
left join(select sourceid,sum(TransactionAmount) as fee from  dcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(1,15) and DebitOrReplenish=2  and sourcetype = 1 group by sourceid)gsbk on gsbk.sourceid = a.id
----管理费扣补款
left join(select sourceid,sum(TransactionAmount) as fee from  dcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(3,17) and DebitOrReplenish=1  and sourcetype = 1 group by sourceid)glfkk on glfkk.sourceid = a.id
left join(select sourceid,sum(TransactionAmount) as fee from  dcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(3,17) and DebitOrReplenish=2  and sourcetype = 1 group by sourceid)glfbk on glfbk.sourceid = a.id
----其他扣补款合计
left join(select sourceid,sum(TransactionAmount) as fee from  dcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory not in(1,2,3,15,16,17) and DebitOrReplenish=1  and sourcetype = 1 group by sourceid)Otherkk on Otherkk.sourceid = a.id
left join(select sourceid,sum(TransactionAmount) as fee from  dcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory not in(1,2,3,15,16,17) and DebitOrReplenish=2  and sourcetype = 1 group by sourceid)Otherbk on Otherbk.sourceid = a.id
)totalall on totalall.id = a.id
left join  dcs.ServiceTripClaimBill stb on stb.Repairclaimbillid= a.id and stb.status = 6
left join(select sourcecode,sum(TransactionAmount) as TransactionAmount from  dcs.ExpenseAdjustmentBill
where status <>99 and TransactionCategory = 12 group by sourcecode)wckk on wckk.sourcecode = stb.code
left join  dcs.RepairOrder rpo on rpo.id = stb.RepairOrderId
left join  dcs.DealerServiceInfo dsi on dsi.dealerid = a.dealerid and dsi.partssalescategoryid = a.partssalescategoryid
left join  dcs.GradeCoefficient gcc on gcc.id = dsi.gradecoefficientid
left join (select b.sourcecode,a.status,a.createtime from  dcs.SsClaimSettlementBill a inner join
dcs.SsClaimSettlementDetail b on a.id = b.SsClaimSettlementBillId where a.status<> 99)scd on scd.sourcecode = a.ClaimBillCode
where a.status<>99
union all
-----2、没有源单据的扣补款费用
select scd.status as CacuStatus,case scd.status when 1 then '新增' when 2 then '已审批' when 3 then '发票登记' when 4 then '费用兑现'when null then '未结算'  end as CacuStatusValue
,b.branchcode,a.PartsSalesCategoryId as CategoryId,null as RepairType,decode(a.status,1,1,2,4) as status,spl.id as ProductLineId,a.code as Claimbillcode,decode(a.status,1,'新增',2,'生效') StatusValue,null as RepairClaimApplicationCode,
null as ServiceTripClaimAppCode,b.name as CategoryName,
null as Vin,spl.name as productlinename,null as ApprovalComment,null as ProductCategoryName,null as  OutFactoryCode,null as RepairTypeValue,b.BranchName,
mdt.name as MarKetName,null as MalfunctionCode,null as MalfunctionReason,null as FaultyPartsCode,null as FaultyPartsName,
case a.DebitOrReplenish when 1 then a.TransactionAmount else 0 end as kk1
,case a.DebitOrReplenish when 1 then a.TransactionReason else null end as kkReason1,
case a.DebitOrReplenish when 1 then lx1.value else null end as kklx1,0 as kk2,
null as kkReason2,null as kklx2,0 as kk3,case a.DebitOrReplenish when 2 then a.TransactionAmount else 0 end as bk1,
case a.DebitOrReplenish when 2 then a.TransactionReason else null end as bkReason1,case a.DebitOrReplenish when 2 then lx1.value else null end as bklx
,null as MaterialCost,null as ActualMaterialCost,0 as LaborCost,
0 as ActualLaborCost,case a.DebitOrReplenish when 1 then -a.TransactionAmount else a.TransactionAmount end as OtherCost,null as OtherCostReason,0 as TotalAmount,
null as tbrs,null as jsrs,null as tbts,
null as jsts,null as rybz,null as tblc,
null as jslc,null as clbz,
null as WcTotal,
0 as ActualWcTotal,0 as WcTotalW,0 as ActualWcTotalW,case a.DebitOrReplenish when 1 then -a.TransactionAmount else a.TransactionAmount end as ActualTotal
,0 as Mileage,0 as WorkingHours,null as Capacity,null as RepairRequestTime,null as MalfunctionDescription,null as VehicleContactPerson,
a.DealerPhoneNumber as ContactPhone,null as CellNumber,null as ApproveTime,null as ContactAddress,null as ServiceTripDuration,null as Servicetripperson,
null as DetailedAddress,null as Outservicecarunitprice,null as Settledistance,null as OutSubsidyPrice,null as Towcharge,null as InitialApprovertComment,
null as ServiceDepartmentComment,null as RejectReason,0 as RejectQty,null as TempSupplierCode,null as TempSupplierName,
null as SupplierConfirmStatus,
null as IfClaimToSupplier,a.DealerCode,a.DealerName,a.CreatorName,a.CreateTime
,null as EngineModel,null as EngineSerialNumber,null as Salesdate,null as OutOfFactoryDate,null as FirstStoppageMileage,null as Suppliercheckcomment,round(gcc.coefficient,2) as Grade,
null as FaultyPartsSupplierName,0 as PartsManagementCost,0 as ActualGlfCost,scd.createtime as CacuTime,
null as 产品线分类,   ----------------产品线分类
null as ProductLineType,null as 外出初审意见,null as 外出终审意见
from  dcs.ExpenseAdjustmentBill a
inner join  dcs.partssalescategory b on a.partssalescategoryid = b.id
left join (select key,value from  dcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx1 on lx1.key =a.TransactionCategory
left join  dcs.DealerServiceInfo dsi on dsi.dealerid = a.dealerid and dsi.partssalescategoryid= a.partssalescategoryid
left join  dcs.GradeCoefficient gcc on gcc.id = dsi.gradecoefficientid
left join  (
            select epl.id,epl.status,epl.enginename as name from dcs.engineproductline epl union all
            select spl1.id,spl1.status,spl1.name from dcs.serviceproductline spl1
       ) spl on spl.id = a.serviceproductlineid
left join (select b.branchid,b.name,b.partssalescategoryid,a.dealerid from  dcs.DealerMarketDptRelation a
inner join  dcs.marketingdepartment b on a.marketid = b.id)mdt on mdt.dealerid = a.dealerid
and mdt.partssalescategoryid= a.partssalescategoryid and mdt.branchid = a.branchid
left join (select b.sourcecode,a.status,a.Createtime from  dcs.SsClaimSettlementBill a inner join
 dcs.SsClaimSettlementDetail b on a.id = b.SsClaimSettlementBillId where a.status<> 99)scd on scd.sourcecode = a.code
where a.status<>99 and a.sourceid is null
union all
---3、外出服务费用
select scd.status as CacuStatus,case scd.status when 1 then '新增' when 2 then '已审批' when 3 then '发票登记' when 4 then '费用兑现'when null then '未结算'  end as CacuStatusValue
,b.branchcode,b.id as CategoryId,rpo.RepairType,decode(a.status,1,1,2,2,4,3,6,4,10,6)as status,spl.id as ProductLineId,a.code as Claimbillcode,decode(a.status,1,'新增',2,'提交',4,'初审通过',6,'生效',10,'审核不通过') as StatusValue,null as RepairClaimApplicationCode,stca.code as ServiceTripClaimAppCode,b.name as CategoryName,
a.Vin,spl.name as productlinename,a.ApprovalComment,b.name as ProductCategoryName,substr(a.vin,-8) as OutFactoryCode,rt.value as RepairTypeValue,b.BranchName,
mdt.Name as MarKetName,faultreason.MalfunctionCode,faultreason.MalfunctionDescription as MalfunctionReason,null as FaultyPartsCode,null as FaultyPartsName,nvl(kk1.TransactionAmount,0) as kk1
,kk1.TransactionReason as kkReason1,lx1.value as kklx1,nvl(kk2.TransactionAmount,0) as kk2,
kk2.TransactionReason as kkReason2,lx2.value as kklx2,0 as kk3,nvl(bk1.TransactionAmount,0) as bk1,bk1.TransactionReason as bkReason1,lx3.value as bklx,
0 as MaterialCost,0 as ActualMaterialCost,0 as LaborCost,
0 as ActualLaborCost,a.OtherCost,a.OtherCostReason,0 as TotalAmount,
a.DealerTripPerson as tbrs,a.ServiceTripPerson as jsrs,a.DealerTripDuration as tbts,
a.ServiceTripDuration as jsts,a.OutSubsidyPrice as rybz,a.ServiceTripDistance as tblc,
a.SettleDistance as jslc,a.OutServiceCarUnitPrice as clbz,
case rpo.WarrantyStatus when 1 then nvl(a.DealerTripPerson,0)*nvl(a.DealerTripDuration,0)*nvl(a.OutSubsidyPrice,0) + nvl(a.ServiceTripDistance,0)*a.OutServiceCarUnitPrice+nvl(a.TowCharge,0)+nvl(a.OtherCost,0) else 0 end as WcTotal ,---保内
case rpo.WarrantyStatus when 1 then a.Totalamount - nvl(kk1.TransactionAmount,0) - nvl(kk2.TransactionAmount,0) + nvl(bk1.TransactionAmount,0) else 0 end as ActualWcTotal,---保内实际
case rpo.WarrantyStatus when 2 then nvl(a.DealerTripPerson,0)*nvl(a.DealerTripDuration,0)*nvl(a.OutSubsidyPrice,0) + nvl(a.ServiceTripDistance,0)*a.OutServiceCarUnitPrice+nvl(a.TowCharge,0)+nvl(a.OtherCost,0) else 0 end as WcTotalW ,---保外
case rpo.WarrantyStatus when 2 then a.Totalamount - nvl(kk1.TransactionAmount,0) - nvl(kk2.TransactionAmount,0) + nvl(bk1.TransactionAmount,0) else 0 end as ActualWcTotalW,---保外实际
a.Totalamount - nvl(kk1.TransactionAmount,0) - nvl(kk2.TransactionAmount,0) + nvl(bk1.TransactionAmount,0) as ActualTotal
,a.Mileage,0 as WorkingHours,a.Capacity,a.RepairRequestTime,null as MalfunctionDescription,a.VehicleContactPerson,
a.ContactPhone,a.ContactPhone as CellNumber,a.ApproveTime,a.ContactAddress,a.ServiceTripDuration,a.Servicetripperson,
a.DetailedAddress,a.Outservicecarunitprice,a.Settledistance,a.OutSubsidyPrice,a.Towcharge,null as InitialApprovertComment,
null as ServiceDepartmentComment,null as RejectReason,0 as RejectQty,null as TempSupplierCode,null as TempSupplierName,
case a.SupplierCheckStatus when 1 then '未确认' when 2 then '已确认' when 3 then '确认未通过' else '未确认' end as SupplierConfirmStatus,
case a.IfClaimToSupplier when 1 then '是' else '否' end as IfClaimToSupplier,a.DealerCode,a.DealerName,a.CreatorName,a.CreateTime
,vif.EngineModel,vif.EngineSerialNumber,vif.Salesdate,vif.OutOfFactoryDate,null as FirstStoppageMileage,
a.Suppliercheckcomment,round(gcc.coefficient,2) as Grade,
null as FaultyPartsSupplierName,0 as PartsManagementCost,0 as ActualGlfCost,scd.createtime as CacuTime,
null as 产品线分类,   ----------------产品线分类
null as ProductLineType,a.InitialApproveComment as 外出初审意见,a.ApprovalComment 外出终审意见
from dcs.ServiceTripClaimBill a
left join dcs.vehicleinformation vif on vif.id = a.vehicleid
left join (
            select epl.id,epl.status,epl.enginename as name from dcs.engineproductline epl union all
            select spl1.id,spl1.status,spl1.name from dcs.serviceproductline spl1
       ) spl on spl.id = a.ServiceProductLineId
left join (select b.sourcecode,a.status,a.createtime from dcs.SsClaimSettlementBill a inner join
dcs.SsClaimSettlementDetail b on a.id = b.SsClaimSettlementBillId where a.status<> 99)scd on scd.sourcecode = a.code
inner join dcs.RepairOrder rpo on rpo.id = a.RepairOrderId
inner join dcs.partssalescategory b on a.partssalescategoryid = b.id
inner join dcs.vehicleinformation c on c.id = a.vehicleid
left join (select b.branchid,b.name,b.partssalescategoryid,a.dealerid from dcs.DealerMarketDptRelation a
inner join dcs.marketingdepartment b on a.marketid = b.id)mdt on mdt.dealerid = a.dealerid
and mdt.partssalescategoryid= a.partssalescategoryid and mdt.branchid = a.branchid
left join (select b.MalfunctionDescription,b.MalfunctionCode,b.RepairOrderId
 from(select max(id) as id,RepairOrderId from dcs.RepairOrderFaultReason where OutFaultReason = 1 group by RepairOrderId) a
inner join dcs.RepairOrderFaultReason b on a.id = b.id)faultreason on faultreason.RepairOrderId = rpo.id
left join(select key,value from dcs.keyvalueitem where category = 'DCS' and name ='Repair_Tpye')rt on rt.key = rpo.repairtype
left join dcs.ServiceTripClaimApplication stca on   stca.id = a.Servicetripclaimappid
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from
(select max(a.id) id ,a.sourceid,a.DebitOrReplenish
from dcs.ExpenseAdjustmentBill a where a.status = 2 group by a.sourceid,a.DebitOrReplenish)a
inner join dcs.ExpenseAdjustmentBill b on a.id = b.id where b.DebitOrReplenish=1 and b.SourceType=5) kk1 on kk1.sourceid = a.id
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from(select min(a.id) id ,a.sourceid,a.DebitOrReplenish
from dcs.ExpenseAdjustmentBill a where a.status = 2 group by a.sourceid,a.DebitOrReplenish)a
inner join dcs.ExpenseAdjustmentBill b on a.id = b.id where b.DebitOrReplenish=1 and b.SourceType=5
) kk2 on kk2.sourceid = a.id and kk1.id<>kk2.id
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from(select max(a.id) id ,a.sourceid,a.DebitOrReplenish
from dcs.ExpenseAdjustmentBill a group by a.sourceid,a.DebitOrReplenish)a
inner join dcs.ExpenseAdjustmentBill b on a.id = b.id where b.status=2 and b.DebitOrReplenish=2 and b.SourceType=5) bk1 on bk1.sourceid = a.id
left join (select key,value from dcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx1 on lx1.key =kk1.TransactionCategory
left join (select key,value from dcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx2 on lx2.key =kk2.TransactionCategory
left join (select key,value from dcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx3 on lx3.key =bk1.TransactionCategory
left join dcs.DealerServiceInfo dsi on dsi.dealerid = a.dealerid and dsi.branchid = a.branchid and dsi.partssalescategoryid = a.partssalescategoryid
left join dcs.GradeCoefficient gcc on gcc.id = dsi.gradecoefficientid
left join dcs.RepairClaimBill rcb on rcb.id = a.RepairClaimBillId and rcb.status<>99
left join (select b.MalfunctionDescription,b.MalfunctionCode,b.RepairOrderId
 from(select max(id) as id,RepairOrderId from dcs.RepairOrderFaultReason where OutFaultReason = 1 group by RepairOrderId)a
inner join dcs.RepairOrderFaultReason b on a.id = b.id)faultreason on faultreason.RepairOrderId = rpo.id
where a.status <> 99 and rpo.WarrantyStatus =2
union all
-----4、售前检查单
select scd.status as CacuStatus,case scd.status when 1 then '新增' when 2 then '已审批' when 3 then '发票登记' when 4 then '费用兑现'when null then '未结算'  end as CacuStatusValue
,b.branchcode,b.id as CategoryId,null as RepairType,decode(a.status,1,1,2,2,3,4)as status,spl.id as ProductLineId,a.code as Claimbillcode,decode(a.status,1,'新增',2,'提交',3,'已审核',4,'审核不通过') as StatusValue,null as RepairClaimApplicationCode,null as ServiceTripClaimAppCode,b.name as CategoryName,
a.Vin,spl.name as productlinename,null as ApprovalComment,b.name as ProductCategoryName,substr(a.vin,-8) as OutFactoryCode,null as RepairTypeValue,b.BranchName,
mdt.Name as MarKetName,null as MalfunctionCode,null as MalfunctionReason,null as FaultyPartsCode,null as FaultyPartsName,0 as kk1
,null as kkReason1,null as kklx1,null as kk2,
null as kkReason2,null as kklx2,0 as kk3,null as bk1,null as bkReason1,null as bklx,
0 as MaterialCost,0 as ActualMaterialCost,0 as LaborCost,
0 as ActualLaborCost,a.cost as OtherCost,null as OtherCostReason,0 as TotalAmount,
null as tbrs,null as jsrs,null as tbts,
null as jsts,null as rybz,null as tblc,
null as jslc,null as clbz,----
null as WcTotal ,---保内
null as ActualWcTotal,---保内实际
null as WcTotalW ,---保外
null as ActualWcTotalW,---保外实际
a.cost as ActualTotal
,0 as Mileage,0 as WorkingHours,null as Capacity,null as RepairRequestTime,null as MalfunctionDescription,a.DriverName as VehicleContactPerson,
a.DriverTel as ContactPhone,a.DriverTel as CellNumber,a.ApproveTime,null as ContactAddress,null as ServiceTripDuration,null as Servicetripperson,
null as DetailedAddress,null as Outservicecarunitprice,null as Settledistance,null as OutSubsidyPrice,null as Towcharge,null as InitialApprovertComment,
null as ServiceDepartmentComment,null as RejectReason,0 as RejectQty,null as TempSupplierCode,null as TempSupplierName,
null as SupplierConfirmStatus,
null as IfClaimToSupplier,a.DealerCode,a.DealerName,a.CreatorName,a.CreateTime
,vif.EngineModel,vif.EngineSerialNumber,vif.Salesdate,vif.OutOfFactoryDate,null as FirstStoppageMileage,
null as Suppliercheckcomment,round(gcc.coefficient,2) as Grade,
null as FaultyPartsSupplierName,0 as PartsManagementCost,0 as ActualGlfCost,scd.createtime as CacuTime,
null as 产品线分类,   ----------------产品线分类
null as ProductLineType,null as 外出初审意见,null as 外出终审意见
from dcs.PreSaleCheckOrder a
left join dcs.vehicleinformation vif on vif.id = a.vehicleid
left join (
            select epl.id,epl.status,epl.enginename as name from dcs.engineproductline epl union all
            select spl1.id,spl1.status,spl1.name from dcs.serviceproductline spl1
       ) spl on spl.id = a.ServiceProductLineId
left join (select b.sourcecode,a.status,a.createtime from dcs.SsClaimSettlementBill a inner join
dcs.SsClaimSettlementDetail b on a.id = b.SsClaimSettlementBillId where a.status<> 99)scd on scd.sourcecode = a.code
inner join dcs.partssalescategory b on a.partssalescategoryid = b.id
inner join dcs.vehicleinformation c on c.id = a.vehicleid
left join (select b.branchid,b.name,b.partssalescategoryid,a.dealerid from dcs.DealerMarketDptRelation a
inner join dcs.marketingdepartment b on a.marketid = b.id)mdt on mdt.dealerid = a.dealerid
and mdt.partssalescategoryid= a.partssalescategoryid and mdt.branchid = a.branchid
left join dcs.DealerServiceInfo dsi on dsi.dealerid = a.dealerid and dsi.branchid = a.branchid and dsi.partssalescategoryid = a.partssalescategoryid
left join dcs.GradeCoefficient gcc on gcc.id = dsi.gradecoefficientid
where a.status <> 99
union all
------------------------------------------------------------------------------------1101-------------------------------------------------
-----1、维修索赔费用
select scd.status as CacuStatus,case scd.status when 1 then '新增' when 2 then '已审批' when 3 then '发票登记' when 4 then '费用兑现'when null then '未结算'  end as CacuStatusValue
,b.branchcode,b.id as CategoryId,a.RepairType,a.status,spl.id as ProductLineId,a.Claimbillcode,st.value StatusValue,a.RepairClaimApplicationCode,a.ServiceTripClaimAppCode,b.name as CategoryName,
a.Vin,spl.name as productlinename,stb.ApprovalComment,c.ProductCategoryName,substr(a.vin,-8) as OutFactoryCode,rt.value as RepairTypeValue,b.BranchName,
d.Name as MarKetName,MalfunctionCode,a.MalfunctionDescription as MalfunctionReason,FaultyPartsCode,FaultyPartsName,nvl(kk1.TransactionAmount,0) as kk1
,kk1.TransactionReason as kkReason1,lx1.value as kklx1,nvl(kk2.TransactionAmount,0) as kk2,
kk2.TransactionReason as kkReason2,lx2.value as kklx2,nvl(wckk.TransactionAmount,0) as kk3,bk1.TransactionAmount as bk1,bk1.TransactionReason as bkReason1,lx3.value as bklx
,a.MaterialCost,a.materialcost - nvl(totalall.clkk,0) as ActualMaterialCost, ---实际结算材料费
round(a.LaborCost,2) as LaborCost,----工时费
a.LaborCost - nvl(totalall.gskk,0) as ActualLaborCost,a.OtherCost - nvl(totalall.Otherkk,0) as OtherCost,a.OtherCostReason,a.TotalAmount,
stb.DealerTripPerson as tbrs,stb.ServiceTripPerson as jsrs,stb.DealerTripDuration as tbts,
stb.ServiceTripDuration as jsts,stb.OutSubsidyPrice as rybz,stb.ServiceTripDistance as tblc,
stb.SettleDistance as jslc,stb.OutServiceCarUnitPrice as clbz,
case rpo.WarrantyStatus when 1 then nvl(stb.DealerTripPerson,0)*nvl(stb.DealerTripDuration,0)*nvl(stb.OutSubsidyPrice,0) + nvl(ServiceTripDistance,0)*stb.OutServiceCarUnitPrice+nvl(stb.TowCharge,0)+nvl(stb.OtherCost,0) else 0 end as WcTotal ,---保内
case rpo.WarrantyStatus when 1 then stb.Totalamount - nvl(wckk.TransactionAmount,0) else 0 end as ActualWcTotal,---保内实际
0 as WcTotalW ,---保外
0 as ActualWcTotalW,---保外实际
(a.materialcost - nvl(kk1.TransactionAmount,0)+a.LaborCost - nvl(kk2.TransactionAmount,0)+ nvl(bk1.TransactionAmount,0) +nvl(stb.Totalamount,0)- nvl(wckk.TransactionAmount,0)+a.OtherCost+a.PartsManagementCost) as ActualTotal
,a.Mileage,a.WorkingHours,a.Capacity,a.RepairRequestTime,a.MalfunctionReason as MalfunctionDescription,a.VehicleContactPerson,
a.ContactPhone,a.ContactPhone as CellNumber,a.ApproveTime,a.ContactAddress,stb.ServiceTripDuration,stb.Servicetripperson,
stb.DetailedAddress,stb.Outservicecarunitprice,stb.Settledistance,stb.OutSubsidyPrice,stb.Towcharge,a.InitialApprovertComment,
a.ServiceDepartmentComment as ServiceDepartmentComment,a.RejectReason,a.RejectQty,a.ClaimSupplierCode as TempSupplierCode,a.ClaimSupplierName as TempSupplierName,
case a.Supplierconfirmstatus when 1 then '未确认' when 2 then '已确认' when 3 then '确认未通过' end as SupplierConfirmStatus,
case a.IfClaimToSupplier when 1 then '是' else '否' end as IfClaimToSupplier,a.DealerCode,a.DealerName,a.CreatorName,a.CreateTime
,a.EngineModel,a.EngineSerialNumber,a.Salesdate,a.OutOfFactoryDate,c.FirstStoppageMileage,a.Suppliercheckcomment,round(gcc.coefficient,2) as Grade,
a.ClaimSupplierName as FaultyPartsSupplierName,a.PartsManagementCost,----配件管理费
nvl(a.PartsManagementCost,0) - nvl(totalall.glfkk,0) as ActualGlfCost,----实际结算管理费
scd.createtime as CacuTime,
null as 产品线分类,   ----------------产品线分类
null as ProductLineType,stb.InitialApproveComment as 外出初审意见,stb.ApprovalComment 外出终审意见
from yxdcs.RepairClaimBill a
inner join  yxdcs.partssalescategory b on a.partssalescategoryid = b.id
inner join  yxdcs.vehicleinformation c on c.id = a.vehicleid
left join  (
            select epl.id,epl.status,epl.enginename as name from yxdcs.engineproductline epl union all
            select spl1.id,spl1.status,spl1.name from yxdcs.serviceproductline spl1
       ) spl on spl.id = a.ServiceProductLineId and spl.status=1
left join  yxdcs.MarketingDepartment d on d.id = a.marketingdepartmentid
left join(select key,value from  yxdcs.keyvalueitem where category = 'DCS' and name ='RepairClaimBill_RepairClaimStatus') st on st.key = a.status
left join(select key,value from  yxdcs.keyvalueitem where category = 'DCS' and name ='Repair_Tpye')rt on rt.key = a.repairtype
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from
(select max(a.id) id ,a.sourceid,a.DebitOrReplenish
from  yxdcs.ExpenseAdjustmentBill a where a.status = 2 group by a.sourceid,a.DebitOrReplenish)a
inner join  yxdcs.ExpenseAdjustmentBill b on a.id = b.id where b.DebitOrReplenish=1 and b.SourceType in (1,2,3)) kk1 on kk1.sourceid = a.id
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from(select min(a.id) id ,a.sourceid,a.DebitOrReplenish
from  yxdcs.ExpenseAdjustmentBill a where a.status = 2 group by a.sourceid,a.DebitOrReplenish)a
inner join  yxdcs.ExpenseAdjustmentBill b on a.id = b.id where b.DebitOrReplenish=1 and b.SourceType in (1,2,3)
) kk2 on kk2.sourceid = a.id and kk1.id<>kk2.id
left join (select key,value from  yxdcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx1 on lx1.key =kk1.TransactionCategory
left join (select key,value from  yxdcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx2 on lx2.key =kk2.TransactionCategory
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from(select max(a.id) id ,a.sourceid,a.DebitOrReplenish
from  yxdcs.ExpenseAdjustmentBill a group by a.sourceid,a.DebitOrReplenish)a
inner join  yxdcs.ExpenseAdjustmentBill b on a.id = b.id where b.status=2 and b.DebitOrReplenish=2 and b.SourceType in (1,2,3)) bk1 on bk1.sourceid = a.id
left join (select key,value from  yxdcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx3 on lx3.key =bk1.TransactionCategory
----计算 材料费扣补款合计，工时费扣补款合计 + 配件管理费扣补款合计 + 其他 扣补款合计
left join (
select a.id,nvl(clkk.fee,0)-nvl(clbk.fee,0) as clkk,nvl(gskk.fee,0)-nvl(gsbk.fee,0) as gskk
,nvl(glfkk.fee,0)-nvl(glfbk.fee,0) as glfkk ,nvl(Otherkk.fee,0) - nvl(Otherbk.fee,0)as Otherkk
from
 yxdcs.RepairClaimBill a left join
(select sourceid,sum(TransactionAmount) as fee from  yxdcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(2,16)  and DebitOrReplenish=1 and sourcetype = 1 group by sourceid)clkk on clkk.sourceid = a.id
left join(select sourceid,sum(TransactionAmount) as fee from  yxdcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(2,16) and DebitOrReplenish=2  and sourcetype = 1 group by sourceid)clbk on clbk.sourceid = a.id
----工时扣补款
left join(select sourceid,sum(TransactionAmount) as fee from  yxdcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(1,15) and DebitOrReplenish=1  and sourcetype = 1 group by sourceid)gskk on gskk.sourceid = a.id
left join(select sourceid,sum(TransactionAmount) as fee from  yxdcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(1,15) and DebitOrReplenish=2  and sourcetype = 1 group by sourceid)gsbk on gsbk.sourceid = a.id
----管理费扣补款
left join(select sourceid,sum(TransactionAmount) as fee from  yxdcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(3,17) and DebitOrReplenish=1  and sourcetype = 1 group by sourceid)glfkk on glfkk.sourceid = a.id
left join(select sourceid,sum(TransactionAmount) as fee from  yxdcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(3,17) and DebitOrReplenish=2  and sourcetype = 1 group by sourceid)glfbk on glfbk.sourceid = a.id
----其他扣补款合计
left join(select sourceid,sum(TransactionAmount) as fee from  yxdcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory not in(1,2,3,15,16,17) and DebitOrReplenish=1  and sourcetype = 1 group by sourceid)Otherkk on Otherkk.sourceid = a.id
left join(select sourceid,sum(TransactionAmount) as fee from  yxdcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory not in(1,2,3,15,16,17) and DebitOrReplenish=2  and sourcetype = 1 group by sourceid)Otherbk on Otherbk.sourceid = a.id
)totalall on totalall.id = a.id
left join  yxdcs.ServiceTripClaimBill stb on stb.Repairclaimbillid= a.id and stb.status = 6
left join(select sourcecode,sum(TransactionAmount) as TransactionAmount from  yxdcs.ExpenseAdjustmentBill
where status <>99 and TransactionCategory = 12 group by sourcecode)wckk on wckk.sourcecode = stb.code
left join  yxdcs.RepairOrder rpo on rpo.id = stb.RepairOrderId
left join  yxdcs.DealerServiceInfo dsi on dsi.dealerid = a.dealerid and dsi.partssalescategoryid = a.partssalescategoryid
left join  yxdcs.GradeCoefficient gcc on gcc.id = dsi.gradecoefficientid
left join (select b.sourcecode,a.status,a.createtime from  yxdcs.SsClaimSettlementBill a inner join
yxdcs.SsClaimSettlementDetail b on a.id = b.SsClaimSettlementBillId where a.status<> 99)scd on scd.sourcecode = a.ClaimBillCode
where a.status<>99
union all
-----2、没有源单据的扣补款费用
select scd.status as CacuStatus,case scd.status when 1 then '新增' when 2 then '已审批' when 3 then '发票登记' when 4 then '费用兑现'when null then '未结算'  end as CacuStatusValue
,b.branchcode,a.PartsSalesCategoryId as CategoryId,null as RepairType,decode(a.status,1,1,2,4) as status,spl.id as ProductLineId,a.code as Claimbillcode,decode(a.status,1,'新增',2,'生效') StatusValue,null as RepairClaimApplicationCode,
null as ServiceTripClaimAppCode,b.name as CategoryName,
null as Vin,spl.name as productlinename,null as ApprovalComment,null as ProductCategoryName,null as  OutFactoryCode,null as RepairTypeValue,b.BranchName,
mdt.name as MarKetName,null as MalfunctionCode,null as MalfunctionReason,null as FaultyPartsCode,null as FaultyPartsName,
case a.DebitOrReplenish when 1 then a.TransactionAmount else 0 end as kk1
,case a.DebitOrReplenish when 1 then a.TransactionReason else null end as kkReason1,
case a.DebitOrReplenish when 1 then lx1.value else null end as kklx1,0 as kk2,
null as kkReason2,null as kklx2,0 as kk3,case a.DebitOrReplenish when 2 then a.TransactionAmount else 0 end as bk1,
case a.DebitOrReplenish when 2 then a.TransactionReason else null end as bkReason1,case a.DebitOrReplenish when 2 then lx1.value else null end as bklx
,null as MaterialCost,null as ActualMaterialCost,0 as LaborCost,
0 as ActualLaborCost,case a.DebitOrReplenish when 1 then -a.TransactionAmount else a.TransactionAmount end as OtherCost,null as OtherCostReason,0 as TotalAmount,
null as tbrs,null as jsrs,null as tbts,
null as jsts,null as rybz,null as tblc,
null as jslc,null as clbz,
null as WcTotal,
0 as ActualWcTotal,0 as WcTotalW,0 as ActualWcTotalW,case a.DebitOrReplenish when 1 then -a.TransactionAmount else a.TransactionAmount end as ActualTotal
,0 as Mileage,0 as WorkingHours,null as Capacity,null as RepairRequestTime,null as MalfunctionDescription,null as VehicleContactPerson,
a.DealerPhoneNumber as ContactPhone,null as CellNumber,null as ApproveTime,null as ContactAddress,null as ServiceTripDuration,null as Servicetripperson,
null as DetailedAddress,null as Outservicecarunitprice,null as Settledistance,null as OutSubsidyPrice,null as Towcharge,null as InitialApprovertComment,
null as ServiceDepartmentComment,null as RejectReason,0 as RejectQty,null as TempSupplierCode,null as TempSupplierName,
null as SupplierConfirmStatus,
null as IfClaimToSupplier,a.DealerCode,a.DealerName,a.CreatorName,a.CreateTime
,null as EngineModel,null as EngineSerialNumber,null as Salesdate,null as OutOfFactoryDate,null as FirstStoppageMileage,null as Suppliercheckcomment,round(gcc.coefficient,2) as Grade,
null as FaultyPartsSupplierName,0 as PartsManagementCost,0 as ActualGlfCost,scd.createtime as CacuTime,
null as 产品线分类,   ----------------产品线分类
null as ProductLineType,null as 外出初审意见,null as 外出终审意见
from  yxdcs.ExpenseAdjustmentBill a
inner join  yxdcs.partssalescategory b on a.partssalescategoryid = b.id
left join (select key,value from  yxdcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx1 on lx1.key =a.TransactionCategory
left join  yxdcs.DealerServiceInfo dsi on dsi.dealerid = a.dealerid and dsi.partssalescategoryid= a.partssalescategoryid
left join  yxdcs.GradeCoefficient gcc on gcc.id = dsi.gradecoefficientid
left join  (
            select epl.id,epl.status,epl.enginename as name from yxdcs.engineproductline epl union all
            select spl1.id,spl1.status,spl1.name from yxdcs.serviceproductline spl1
       ) spl on spl.id = a.serviceproductlineid
left join (select b.branchid,b.name,b.partssalescategoryid,a.dealerid from  yxdcs.DealerMarketDptRelation a
inner join  yxdcs.marketingdepartment b on a.marketid = b.id)mdt on mdt.dealerid = a.dealerid
and mdt.partssalescategoryid= a.partssalescategoryid and mdt.branchid = a.branchid
left join (select b.sourcecode,a.status,a.Createtime from  yxdcs.SsClaimSettlementBill a inner join
 yxdcs.SsClaimSettlementDetail b on a.id = b.SsClaimSettlementBillId where a.status<> 99)scd on scd.sourcecode = a.code
where a.status<>99 and a.sourceid is null
/*union all
---3、外出服务费用
select scd.status as CacuStatus,case scd.status when 1 then '新增' when 2 then '已审批' when 3 then '发票登记' when 4 then '费用兑现' end as CacuStatusValue
,b.branchcode,b.id as CategoryId,rpo.RepairType,decode(a.status,1,1,2,2,4,3,6,4,10,6)as status,spl.id as ProductLineId,a.code as Claimbillcode,decode(a.status,1,'新增',2,'提交',4,'初审通过',6,'生效',10,'审核不通过') as StatusValue,null as RepairClaimApplicationCode,stca.code as ServiceTripClaimAppCode,b.name as CategoryName,
a.Vin,spl.name as productlinename,b.name as ProductCategoryName,substr(a.vin,-8) as OutFactoryCode,rt.value as RepairTypeValue,b.BranchName,
mdt.Name as MarKetName,faultreason.MalfunctionCode,faultreason.MalfunctionDescription as MalfunctionReason,null as FaultyPartsCode,null as FaultyPartsName,nvl(kk1.TransactionAmount,0) as kk1
,kk1.TransactionReason as kkReason1,lx1.value as kklx1,nvl(kk2.TransactionAmount,0) as kk2,
kk2.TransactionReason as kkReason2,lx2.value as kklx2,0 as kk3,nvl(bk1.TransactionAmount,0) as bk1,bk1.TransactionReason as bkReason1,lx3.value as bklx,
0 as MaterialCost,0 as ActualMaterialCost,0 as LaborCost,
0 as ActualLaborCost,a.OtherCost,a.OtherCostReason,0 as TotalAmount,
a.DealerTripPerson as tbrs,a.ServiceTripPerson as jsrs,a.DealerTripDuration as tbts,
a.ServiceTripDuration as jsts,a.OutSubsidyPrice as rybz,a.ServiceTripDistance as tblc,
a.SettleDistance as jslc,a.OutServiceCarUnitPrice as clbz,
case rpo.WarrantyStatus when 1 then nvl(a.DealerTripPerson,0)*nvl(a.DealerTripDuration,0)*nvl(a.OutSubsidyPrice,0) + nvl(a.ServiceTripDistance,0)*a.OutServiceCarUnitPrice+nvl(a.TowCharge,0)+nvl(a.OtherCost,0) else 0 end as WcTotal ,---保内
case rpo.WarrantyStatus when 1 then a.Totalamount - nvl(kk1.TransactionAmount,0) - nvl(kk2.TransactionAmount,0) + nvl(bk1.TransactionAmount,0) else 0 end as ActualWcTotal,---保内实际
case rpo.WarrantyStatus when 2 then nvl(a.DealerTripPerson,0)*nvl(a.DealerTripDuration,0)*nvl(a.OutSubsidyPrice,0) + nvl(a.ServiceTripDistance,0)*a.OutServiceCarUnitPrice+nvl(a.TowCharge,0)+nvl(a.OtherCost,0) else 0 end as WcTotalW ,---保外
case rpo.WarrantyStatus when 2 then a.Totalamount - nvl(kk1.TransactionAmount,0) - nvl(kk2.TransactionAmount,0) + nvl(bk1.TransactionAmount,0) else 0 end as ActualWcTotalW,---保外实际
a.Totalamount - nvl(kk1.TransactionAmount,0) - nvl(kk2.TransactionAmount,0) + nvl(bk1.TransactionAmount,0) as ActualTotal
,a.Mileage,0 as WorkingHours,a.Capacity,a.RepairRequestTime,null as MalfunctionDescription,a.VehicleContactPerson,
a.ContactPhone,a.ContactPhone as CellNumber,a.ApproveTime,a.ContactAddress,a.ServiceTripDuration,a.Servicetripperson,
a.DetailedAddress,a.Outservicecarunitprice,a.Settledistance,a.OutSubsidyPrice,a.Towcharge,null as InitialApprovertComment,
null as ServiceDepartmentComment,null as RejectReason,0 as RejectQty,null as TempSupplierCode,null as TempSupplierName,
case a.SupplierCheckStatus when 1 then '未确认' when 2 then '已确认' when 3 then '确认未通过' else '未确认' end as SupplierConfirmStatus,
case a.IfClaimToSupplier when 1 then '是' else '否' end as IfClaimToSupplier,a.DealerCode,a.DealerName,a.CreatorName,a.CreateTime
,vif.EngineModel,vif.EngineSerialNumber,vif.Salesdate,vif.OutOfFactoryDate,null as FirstStoppageMileage,
a.Suppliercheckcomment,round(gcc.coefficient,2) as Grade,
null as FaultyPartsSupplierName,0 as PartsManagementCost,0 as ActualGlfCost,scd.createtime as CacuTime
from yxdcs.ServiceTripClaimBill a
left join yxdcs.vehicleinformation vif on vif.id = a.vehicleid
left join yxdcs.serviceproductline spl on spl.id = a.ServiceProductLineId
left join (select b.sourcecode,a.status,a.createtime from yxdcs.SsClaimSettlementBill a inner join
yxdcs.SsClaimSettlementDetail b on a.id = b.SsClaimSettlementBillId where a.status<> 99)scd on scd.sourcecode = a.code
inner join yxdcs.RepairOrder rpo on rpo.id = a.RepairOrderId
inner join yxdcs.partssalescategory b on a.partssalescategoryid = b.id
inner join yxdcs.vehicleinformation c on c.id = a.vehicleid
left join (select b.branchid,b.name,b.partssalescategoryid,a.dealerid from yxdcs.DealerMarketDptRelation a
inner join yxdcs.marketingdepartment b on a.marketid = b.id)mdt on mdt.dealerid = a.dealerid
and mdt.partssalescategoryid= a.partssalescategoryid and mdt.branchid = a.branchid
left join (select b.MalfunctionDescription,b.MalfunctionCode,b.RepairOrderId
 from(select max(id) as id,RepairOrderId from yxdcs.RepairOrderFaultReason where OutFaultReason = 1 group by RepairOrderId) a
inner join yxdcs.RepairOrderFaultReason b on a.id = b.id)faultreason on faultreason.RepairOrderId = rpo.id
left join(select key,value from yxdcs.keyvalueitem where category = 'DCS' and name ='Repair_Tpye')rt on rt.key = rpo.repairtype
left join yxdcs.ServiceTripClaimApplication stca on   stca.id = a.Servicetripclaimappid
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from
(select max(a.id) id ,a.sourceid,a.DebitOrReplenish
from yxdcs.ExpenseAdjustmentBill a where a.status = 2 group by a.sourceid,a.DebitOrReplenish)a
inner join yxdcs.ExpenseAdjustmentBill b on a.id = b.id where b.DebitOrReplenish=1 and b.SourceType=5) kk1 on kk1.sourceid = a.id
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from(select min(a.id) id ,a.sourceid,a.DebitOrReplenish
from yxdcs.ExpenseAdjustmentBill a where a.status = 2 group by a.sourceid,a.DebitOrReplenish)a
inner join yxdcs.ExpenseAdjustmentBill b on a.id = b.id where b.DebitOrReplenish=1 and b.SourceType=5
) kk2 on kk2.sourceid = a.id and kk1.id<>kk2.id
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from(select max(a.id) id ,a.sourceid,a.DebitOrReplenish
from yxdcs.ExpenseAdjustmentBill a group by a.sourceid,a.DebitOrReplenish)a
inner join yxdcs.ExpenseAdjustmentBill b on a.id = b.id where b.status=2 and b.DebitOrReplenish=2 and b.SourceType=5) bk1 on bk1.sourceid = a.id
left join (select key,value from yxdcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx1 on lx1.key =kk1.TransactionCategory
left join (select key,value from yxdcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx2 on lx2.key =kk2.TransactionCategory
left join (select key,value from yxdcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx3 on lx3.key =bk1.TransactionCategory
left join yxdcs.DealerServiceInfo dsi on dsi.dealerid = a.dealerid and dsi.branchid = a.branchid and dsi.partssalescategoryid = a.partssalescategoryid
left join yxdcs.GradeCoefficient gcc on gcc.id = dsi.gradecoefficientid
left join yxdcs.RepairClaimBill rcb on rcb.id = a.RepairClaimBillId and rcb.status<>99
left join (select b.MalfunctionDescription,b.MalfunctionCode,b.RepairOrderId
 from(select max(id) as id,RepairOrderId from yxdcs.RepairOrderFaultReason where OutFaultReason = 1 group by RepairOrderId)a
inner join yxdcs.RepairOrderFaultReason b on a.id = b.id)faultreason on faultreason.RepairOrderId = rpo.id
where a.status <> 99 and rpo.WarrantyStatus =2*/
union all
-----4、售前检查单
select scd.status as CacuStatus,case scd.status when 1 then '新增' when 2 then '已审批' when 3 then '发票登记' when 4 then '费用兑现'when null then '未结算'  end as CacuStatusValue
,b.branchcode,b.id as CategoryId,null as RepairType,decode(a.status,1,1,2,2,3,4)as status,spl.id as ProductLineId,a.code as Claimbillcode,decode(a.status,1,'新增',2,'提交',3,'已审核',4,'审核不通过') as StatusValue,null as RepairClaimApplicationCode,null as ServiceTripClaimAppCode,b.name as CategoryName,
a.Vin,spl.name as productlinename,null as ApprovalComment,b.name as ProductCategoryName,substr(a.vin,-8) as OutFactoryCode,null as RepairTypeValue,b.BranchName,
mdt.Name as MarKetName,null as MalfunctionCode,null as MalfunctionReason,null as FaultyPartsCode,null as FaultyPartsName,0 as kk1
,null as kkReason1,null as kklx1,null as kk2,
null as kkReason2,null as kklx2,0 as kk3,null as bk1,null as bkReason1,null as bklx,
0 as MaterialCost,0 as ActualMaterialCost,0 as LaborCost,
0 as ActualLaborCost,a.cost as OtherCost,null as OtherCostReason,0 as TotalAmount,
null as tbrs,null as jsrs,null as tbts,
null as jsts,null as rybz,null as tblc,
null as jslc,null as clbz,
null as WcTotal ,---保内
null as ActualWcTotal,---保内实际
null as WcTotalW ,---保外
null as ActualWcTotalW,---保外实际
a.cost as ActualTotal
,0 as Mileage,0 as WorkingHours,null as Capacity,null as RepairRequestTime,null as MalfunctionDescription,a.DriverName as VehicleContactPerson,
a.DriverTel as ContactPhone,a.DriverTel as CellNumber,a.ApproveTime,null as ContactAddress,null as ServiceTripDuration,null as Servicetripperson,
null as DetailedAddress,null as Outservicecarunitprice,null as Settledistance,null as OutSubsidyPrice,null as Towcharge,null as InitialApprovertComment,
null as ServiceDepartmentComment,null as RejectReason,0 as RejectQty,null as TempSupplierCode,null as TempSupplierName,
null as SupplierConfirmStatus,
null as IfClaimToSupplier,a.DealerCode,a.DealerName,a.CreatorName,a.CreateTime
,vif.EngineModel,vif.EngineSerialNumber,vif.Salesdate,vif.OutOfFactoryDate,null as FirstStoppageMileage,
null as Suppliercheckcomment,round(gcc.coefficient,2) as Grade,
null as FaultyPartsSupplierName,0 as PartsManagementCost,0 as ActualGlfCost,scd.createtime as CacuTime,
null as 产品线分类,   ----------------产品线分类
null as ProductLineType,null as 外出初审意见,null as 外出终审意见
from yxdcs.PreSaleCheckOrder a
left join yxdcs.vehicleinformation vif on vif.id = a.vehicleid
left join (
            select epl.id,epl.status,epl.enginename as name from yxdcs.engineproductline epl union all
            select spl1.id,spl1.status,spl1.name from yxdcs.serviceproductline spl1
       ) spl on spl.id = a.ServiceProductLineId
left join (select b.sourcecode,a.status,a.createtime from yxdcs.SsClaimSettlementBill a inner join
yxdcs.SsClaimSettlementDetail b on a.id = b.SsClaimSettlementBillId where a.status<> 99)scd on scd.sourcecode = a.code
inner join yxdcs.partssalescategory b on a.partssalescategoryid = b.id
inner join yxdcs.vehicleinformation c on c.id = a.vehicleid
left join (select b.branchid,b.name,b.partssalescategoryid,a.dealerid from yxdcs.DealerMarketDptRelation a
inner join yxdcs.marketingdepartment b on a.marketid = b.id)mdt on mdt.dealerid = a.dealerid
and mdt.partssalescategoryid= a.partssalescategoryid and mdt.branchid = a.branchid
left join yxdcs.DealerServiceInfo dsi on dsi.dealerid = a.dealerid and dsi.branchid = a.branchid and dsi.partssalescategoryid = a.partssalescategoryid
left join yxdcs.GradeCoefficient gcc on gcc.id = dsi.gradecoefficientid
where a.status <> 99
union all
------------------------------------------------------------------------------------gc-------------------------------------------------
-----1、维修索赔费用
select scd.status as CacuStatus,case scd.status when 1 then '新增' when 2 then '已审批' when 3 then '发票登记' when 4 then '费用兑现'when null then '未结算'  end as CacuStatusValue
,b.branchcode,b.id as CategoryId,a.RepairType,a.status,spl.id as ProductLineId,a.Claimbillcode,st.value StatusValue,a.RepairClaimApplicationCode,a.ServiceTripClaimAppCode,b.name as CategoryName,
a.Vin,spl.name as productlinename,stb.ApprovalComment,c.ProductCategoryName,substr(a.vin,-8) as OutFactoryCode,rt.value as RepairTypeValue,b.BranchName,
d.Name as MarKetName,MalfunctionCode,a.MalfunctionDescription as MalfunctionReason,FaultyPartsCode,FaultyPartsName,nvl(kk1.TransactionAmount,0) as kk1
,kk1.TransactionReason as kkReason1,lx1.value as kklx1,nvl(kk2.TransactionAmount,0) as kk2,
kk2.TransactionReason as kkReason2,lx2.value as kklx2,nvl(wckk.TransactionAmount,0) as kk3,bk1.TransactionAmount as bk1,bk1.TransactionReason as bkReason1,lx3.value as bklx
,a.MaterialCost,a.materialcost - nvl(totalall.clkk,0) as ActualMaterialCost ---实际结算材料费
,round(a.LaborCost,2) as LaborCost,----工时费
a.LaborCost - nvl(totalall.gskk,0) as ActualLaborCost,a.OtherCost - nvl(totalall.Otherkk,0) as OtherCost,a.OtherCostReason,a.TotalAmount,
stb.DealerTripPerson as tbrs,stb.ServiceTripPerson as jsrs,stb.DealerTripDuration as tbts,
stb.ServiceTripDuration as jsts,stb.OutSubsidyPrice as rybz,stb.ServiceTripDistance as tblc,
stb.SettleDistance as jslc,stb.OutServiceCarUnitPrice as clbz,
case rpo.WarrantyStatus when 1 then nvl(stb.DealerTripPerson,0)*nvl(stb.DealerTripDuration,0)*nvl(stb.OutSubsidyPrice,0) + nvl(ServiceTripDistance,0)*stb.OutServiceCarUnitPrice+nvl(stb.TowCharge,0)+nvl(stb.OtherCost,0) else 0 end as WcTotal ,---保内
case rpo.WarrantyStatus when 1 then stb.Totalamount - nvl(wckk.TransactionAmount,0) else 0 end as ActualWcTotal,---保内实际
0 as WcTotalW ,---保外
0 as ActualWcTotalW,---保外实际
(a.materialcost - nvl(kk1.TransactionAmount,0)+a.LaborCost - nvl(kk2.TransactionAmount,0)+ nvl(bk1.TransactionAmount,0) +nvl(stb.Totalamount,0)- nvl(wckk.TransactionAmount,0)+a.OtherCost+a.PartsManagementCost) as ActualTotal
,a.Mileage,a.WorkingHours,a.Capacity,a.RepairRequestTime,a.MalfunctionReason as MalfunctionDescription,a.VehicleContactPerson,
a.ContactPhone,a.ContactPhone as CellNumber,a.ApproveTime,a.ContactAddress,stb.ServiceTripDuration,stb.Servicetripperson,
stb.DetailedAddress,stb.Outservicecarunitprice,stb.Settledistance,stb.OutSubsidyPrice,stb.Towcharge,a.InitialApprovertComment,
a.ServiceDepartmentComment as ServiceDepartmentComment,a.RejectReason,a.RejectQty,a.ClaimSupplierCode as TempSupplierCode,a.ClaimSupplierName as TempSupplierName,
case a.Supplierconfirmstatus when 1 then '未确认' when 2 then '已确认' when 3 then '确认未通过' end as SupplierConfirmStatus,
case a.IfClaimToSupplier when 1 then '是' else '否' end as IfClaimToSupplier,a.DealerCode,a.DealerName,a.CreatorName,a.CreateTime
,a.EngineModel,a.EngineSerialNumber,a.Salesdate,a.OutOfFactoryDate,c.FirstStoppageMileage,a.Suppliercheckcomment,round(gcc.coefficient,2) as Grade,
a.ClaimSupplierName as FaultyPartsSupplierName,a.PartsManagementCost,----配件管理费
nvl(a.PartsManagementCost,0) - nvl(totalall.glfkk,0) as ActualGlfCost,----实际结算管理费
scd.createtime as CacuTime,
substr(spl.name,0,2) as 产品线类型,   ----------------产品线类型
case substr(spl.name,0,2) when '金刚' then 1 when '瑞沃' then 2 end as ProductLineType,null as 外出初审意见,null as 外出终审意见
from gcdcs.RepairClaimBill a
inner join  gcdcs.partssalescategory b on a.partssalescategoryid = b.id
inner join  gcdcs.vehicleinformation c on c.id = a.vehicleid
left join  (
            select epl.id,epl.status,epl.enginename as name from gcdcs.engineproductline epl union all
            select spl1.id,spl1.status,spl1.name from gcdcs.serviceproductline spl1
       ) spl on spl.id = a.ServiceProductLineId and spl.status=1
left join  gcdcs.MarketingDepartment d on d.id = a.marketingdepartmentid
left join(select key,value from  gcdcs.keyvalueitem where category = 'DCS' and name ='RepairClaimBill_RepairClaimStatus') st on st.key = a.status
left join(select key,value from  gcdcs.keyvalueitem where category = 'DCS' and name ='Repair_Tpye')rt on rt.key = a.repairtype
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from
(select max(a.id) id ,a.sourceid,a.DebitOrReplenish
from  gcdcs.ExpenseAdjustmentBill a where a.status = 2 group by a.sourceid,a.DebitOrReplenish)a
inner join  gcdcs.ExpenseAdjustmentBill b on a.id = b.id where b.DebitOrReplenish=1 and b.SourceType in (1,2,3)) kk1 on kk1.sourceid = a.id
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from(select min(a.id) id ,a.sourceid,a.DebitOrReplenish
from  gcdcs.ExpenseAdjustmentBill a where a.status = 2 group by a.sourceid,a.DebitOrReplenish)a
inner join  gcdcs.ExpenseAdjustmentBill b on a.id = b.id where b.DebitOrReplenish=1 and b.SourceType in (1,2,3)
) kk2 on kk2.sourceid = a.id and kk1.id<>kk2.id
left join (select key,value from  gcdcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx1 on lx1.key =kk1.TransactionCategory
left join (select key,value from  gcdcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx2 on lx2.key =kk2.TransactionCategory
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from(select max(a.id) id ,a.sourceid,a.DebitOrReplenish
from  gcdcs.ExpenseAdjustmentBill a group by a.sourceid,a.DebitOrReplenish)a
inner join  gcdcs.ExpenseAdjustmentBill b on a.id = b.id where b.status=2 and b.DebitOrReplenish=2 and b.SourceType in (1,2,3)) bk1 on bk1.sourceid = a.id
left join (select key,value from  gcdcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx3 on lx3.key =bk1.TransactionCategory
----计算 材料费扣补款合计，工时费扣补款合计 + 配件管理费扣补款合计 + 其他 扣补款合计
left join (
select a.id,nvl(clkk.fee,0)-nvl(clbk.fee,0) as clkk,nvl(gskk.fee,0)-nvl(gsbk.fee,0) as gskk
,nvl(glfkk.fee,0)-nvl(glfbk.fee,0) as glfkk ,nvl(Otherkk.fee,0) - nvl(Otherbk.fee,0)as Otherkk
from
 gcdcs.RepairClaimBill a left join
(select sourceid,sum(TransactionAmount) as fee from  gcdcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(2,16)  and DebitOrReplenish=1 and sourcetype = 1 group by sourceid)clkk on clkk.sourceid = a.id
left join(select sourceid,sum(TransactionAmount) as fee from  gcdcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(2,16) and DebitOrReplenish=2  and sourcetype = 1 group by sourceid)clbk on clbk.sourceid = a.id
----工时扣补款
left join(select sourceid,sum(TransactionAmount) as fee from  gcdcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(1,15) and DebitOrReplenish=1  and sourcetype = 1 group by sourceid)gskk on gskk.sourceid = a.id
left join(select sourceid,sum(TransactionAmount) as fee from  gcdcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(1,15) and DebitOrReplenish=2  and sourcetype = 1 group by sourceid)gsbk on gsbk.sourceid = a.id
----管理费扣补款
left join(select sourceid,sum(TransactionAmount) as fee from  gcdcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(3,17) and DebitOrReplenish=1  and sourcetype = 1 group by sourceid)glfkk on glfkk.sourceid = a.id
left join(select sourceid,sum(TransactionAmount) as fee from  gcdcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(3,17) and DebitOrReplenish=2  and sourcetype = 1 group by sourceid)glfbk on glfbk.sourceid = a.id
----其他扣补款合计
left join(select sourceid,sum(TransactionAmount) as fee from  gcdcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory not in(1,2,3,15,16,17) and DebitOrReplenish=1  and sourcetype = 1 group by sourceid)Otherkk on Otherkk.sourceid = a.id
left join(select sourceid,sum(TransactionAmount) as fee from  gcdcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory not in(1,2,3,15,16,17) and DebitOrReplenish=2  and sourcetype = 1 group by sourceid)Otherbk on Otherbk.sourceid = a.id
)totalall on totalall.id = a.id
left join  gcdcs.ServiceTripClaimBill stb on stb.Repairclaimbillid= a.id and stb.status = 6
left join(select sourcecode,sum(TransactionAmount) as TransactionAmount from  gcdcs.ExpenseAdjustmentBill
where status <>99 and TransactionCategory = 12 group by sourcecode)wckk on wckk.sourcecode = stb.code
left join  gcdcs.RepairOrder rpo on rpo.id = stb.RepairOrderId
left join  gcdcs.DealerServiceInfo dsi on dsi.dealerid = a.dealerid and dsi.partssalescategoryid = a.partssalescategoryid
left join  gcdcs.GradeCoefficient gcc on gcc.id = dsi.gradecoefficientid
left join (select b.sourcecode,a.status,a.createtime from  gcdcs.SsClaimSettlementBill a inner join
gcdcs.SsClaimSettlementDetail b on a.id = b.SsClaimSettlementBillId where a.status<> 99)scd on scd.sourcecode = a.ClaimBillCode
where a.status<>99
union all
-----2、没有源单据的扣补款费用
select scd.status as CacuStatus,case scd.status when 1 then '新增' when 2 then '已审批' when 3 then '发票登记' when 4 then '费用兑现'when null then '未结算'  end as CacuStatusValue
,b.branchcode,a.PartsSalesCategoryId as CategoryId,null as RepairType,decode(a.status,1,1,2,4) as status,spl.id as ProductLineId,a.code as Claimbillcode,decode(a.status,1,'新增',2,'生效') StatusValue,null as RepairClaimApplicationCode,
null as ServiceTripClaimAppCode,b.name as CategoryName,
null as Vin,spl.name as productlinename,null as ApprovalComment,null as ProductCategoryName,null as  OutFactoryCode,null as RepairTypeValue,b.BranchName,
mdt.name as MarKetName,null as MalfunctionCode,null as MalfunctionReason,null as FaultyPartsCode,null as FaultyPartsName,
case a.DebitOrReplenish when 1 then a.TransactionAmount else 0 end as kk1
,case a.DebitOrReplenish when 1 then a.TransactionReason else null end as kkReason1,
case a.DebitOrReplenish when 1 then lx1.value else null end as kklx1,0 as kk2,
null as kkReason2,null as kklx2,0 as kk3,case a.DebitOrReplenish when 2 then a.TransactionAmount else 0 end as bk1,
case a.DebitOrReplenish when 2 then a.TransactionReason else null end as bkReason1,case a.DebitOrReplenish when 2 then lx1.value else null end as bklx
,null as MaterialCost,null as ActualMaterialCost,0 as LaborCost,
0 as ActualLaborCost,case a.DebitOrReplenish when 1 then -a.TransactionAmount else a.TransactionAmount end as OtherCost,null as OtherCostReason,0 as TotalAmount,
null as tbrs,null as jsrs,null as tbts,
null as jsts,null as rybz,null as tblc,
null as jslc,null as clbz,
null as WcTotal,
0 as ActualWcTotal,0 as WcTotalW,0 as ActualWcTotalW,case a.DebitOrReplenish when 1 then -a.TransactionAmount else a.TransactionAmount end as ActualTotal
,0 as Mileage,0 as WorkingHours,null as Capacity,null as RepairRequestTime,null as MalfunctionDescription,null as VehicleContactPerson,
a.DealerPhoneNumber as ContactPhone,null as CellNumber,null as ApproveTime,null as ContactAddress,null as ServiceTripDuration,null as Servicetripperson,
null as DetailedAddress,null as Outservicecarunitprice,null as Settledistance,null as OutSubsidyPrice,null as Towcharge,null as InitialApprovertComment,
null as ServiceDepartmentComment,null as RejectReason,0 as RejectQty,null as TempSupplierCode,null as TempSupplierName,
null as SupplierConfirmStatus,
null as IfClaimToSupplier,a.DealerCode,a.DealerName,a.CreatorName,a.CreateTime
,null as EngineModel,null as EngineSerialNumber,null as Salesdate,null as OutOfFactoryDate,null as FirstStoppageMileage,null as Suppliercheckcomment,round(gcc.coefficient,2) as Grade,
null as FaultyPartsSupplierName,0 as PartsManagementCost,0 as ActualGlfCost,scd.createtime as CacuTime,
substr(spl.name,0,2) as 产品线类型,   ----------------产品线类型
case substr(spl.name,0,2) when '金刚' then 1 when '瑞沃' then 2 end as ProductLineType,null as 外出初审意见,null as 外出终审意见
from  gcdcs.ExpenseAdjustmentBill a
inner join  gcdcs.partssalescategory b on a.partssalescategoryid = b.id
left join (select key,value from  gcdcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx1 on lx1.key =a.TransactionCategory
left join  gcdcs.DealerServiceInfo dsi on dsi.dealerid = a.dealerid and dsi.partssalescategoryid= a.partssalescategoryid
left join  gcdcs.GradeCoefficient gcc on gcc.id = dsi.gradecoefficientid
left join  (
            select epl.id,epl.status,epl.enginename as name from gcdcs.engineproductline epl union all
            select spl1.id,spl1.status,spl1.name from gcdcs.serviceproductline spl1
       ) spl on spl.id = a.serviceproductlineid
left join (select b.branchid,b.name,b.partssalescategoryid,a.dealerid from  gcdcs.DealerMarketDptRelation a
inner join  gcdcs.marketingdepartment b on a.marketid = b.id)mdt on mdt.dealerid = a.dealerid
and mdt.partssalescategoryid= a.partssalescategoryid and mdt.branchid = a.branchid
left join (select b.sourcecode,a.status,a.Createtime from  gcdcs.SsClaimSettlementBill a inner join
 gcdcs.SsClaimSettlementDetail b on a.id = b.SsClaimSettlementBillId where a.status<> 99)scd on scd.sourcecode = a.code
where a.status<>99 and a.sourceid is null
union all
-----4、售前检查单
select scd.status as CacuStatus,case scd.status when 1 then '新增' when 2 then '已审批' when 3 then '发票登记' when 4 then '费用兑现'when null then '未结算'  end as CacuStatusValue
,b.branchcode,b.id as CategoryId,null as RepairType,decode(a.status,1,1,2,2,3,4)as status,spl.id as ProductLineId,a.code as Claimbillcode,decode(a.status,1,'新增',2,'提交',3,'已审核',4,'审核不通过') as StatusValue,null as RepairClaimApplicationCode,null as ServiceTripClaimAppCode,b.name as CategoryName,
a.Vin,spl.name as productlinename,null as ApprovalComment,b.name as ProductCategoryName,substr(a.vin,-8) as OutFactoryCode,null as RepairTypeValue,b.BranchName,
mdt.Name as MarKetName,null as MalfunctionCode,null as MalfunctionReason,null as FaultyPartsCode,null as FaultyPartsName,0 as kk1
,null as kkReason1,null as kklx1,null as kk2,
null as kkReason2,null as kklx2,0 as kk3,null as bk1,null as bkReason1,null as bklx,
0 as MaterialCost,0 as ActualMaterialCost,0 as LaborCost,
0 as ActualLaborCost,a.cost as OtherCost,null as OtherCostReason,0 as TotalAmount,
null as tbrs,null as jsrs,null as tbts,
null as jsts,null as rybz,null as tblc,
null as jslc,null as clbz,----
null as WcTotal ,---保内
null as ActualWcTotal,---保内实际
null as WcTotalW ,---保外
null as ActualWcTotalW,---保外实际
a.cost as ActualTotal
,0 as Mileage,0 as WorkingHours,null as Capacity,null as RepairRequestTime,null as MalfunctionDescription,a.DriverName as VehicleContactPerson,
a.DriverTel as ContactPhone,a.DriverTel as CellNumber,a.ApproveTime,null as ContactAddress,null as ServiceTripDuration,null as Servicetripperson,
null as DetailedAddress,null as Outservicecarunitprice,null as Settledistance,null as OutSubsidyPrice,null as Towcharge,null as InitialApprovertComment,
null as ServiceDepartmentComment,null as RejectReason,0 as RejectQty,null as TempSupplierCode,null as TempSupplierName,
null as SupplierConfirmStatus,
null as IfClaimToSupplier,a.DealerCode,a.DealerName,a.CreatorName,a.CreateTime
,vif.EngineModel,vif.EngineSerialNumber,vif.Salesdate,vif.OutOfFactoryDate,null as FirstStoppageMileage,
null as Suppliercheckcomment,round(gcc.coefficient,2) as Grade,
null as FaultyPartsSupplierName,0 as PartsManagementCost,0 as ActualGlfCost,scd.createtime as CacuTime,
substr(spl.name,0,2) as 产品线类型,   ----------------产品线类型
case substr(spl.name,0,2) when '金刚' then 1 when '瑞沃' then 2 end as ProductLineType,null as 外出初审意见,null as 外出终审意见
from gcdcs.PreSaleCheckOrder a
left join gcdcs.vehicleinformation vif on vif.id = a.vehicleid
left join (
            select epl.id,epl.status,epl.enginename as name from gcdcs.engineproductline epl union all
            select spl1.id,spl1.status,spl1.name from gcdcs.serviceproductline spl1
       ) spl on spl.id = a.ServiceProductLineId
left join (select b.sourcecode,a.status,a.createtime from gcdcs.SsClaimSettlementBill a inner join
gcdcs.SsClaimSettlementDetail b on a.id = b.SsClaimSettlementBillId where a.status<> 99)scd on scd.sourcecode = a.code
inner join gcdcs.partssalescategory b on a.partssalescategoryid = b.id
inner join gcdcs.vehicleinformation c on c.id = a.vehicleid
left join (select b.branchid,b.name,b.partssalescategoryid,a.dealerid from gcdcs.DealerMarketDptRelation a
inner join gcdcs.marketingdepartment b on a.marketid = b.id)mdt on mdt.dealerid = a.dealerid
and mdt.partssalescategoryid= a.partssalescategoryid and mdt.branchid = a.branchid
left join gcdcs.DealerServiceInfo dsi on dsi.dealerid = a.dealerid and dsi.branchid = a.branchid and dsi.partssalescategoryid = a.partssalescategoryid
left join gcdcs.GradeCoefficient gcc on gcc.id = dsi.gradecoefficientid
where a.status <> 99
union all
------------------------------------------------------------------------------------SDDCS-------------------------------------------------
-----1、维修索赔费用
select scd.status as CacuStatus,case scd.status when 1 then '新增' when 2 then '已审批' when 3 then '发票登记' when 4 then '费用兑现'when null then '未结算'   end as CacuStatusValue
,b.branchcode,b.id as CategoryId,a.RepairType,a.status,spl.id as ProductLineId,a.Claimbillcode,st.value StatusValue,a.RepairClaimApplicationCode,a.ServiceTripClaimAppCode,b.name as CategoryName,
a.Vin,spl.name as productlinename,stb.ApprovalComment,c.ProductCategoryName,substr(a.vin,-8) as OutFactoryCode,rt.value as RepairTypeValue,b.BranchName,
d.Name as MarKetName,MalfunctionCode,a.MalfunctionDescription as MalfunctionReason,FaultyPartsCode,FaultyPartsName,nvl(kk1.TransactionAmount,0) as kk1
,kk1.TransactionReason as kkReason1,lx1.value as kklx1,nvl(kk2.TransactionAmount,0) as kk2,
kk2.TransactionReason as kkReason2,lx2.value as kklx2,nvl(wckk.TransactionAmount,0) as kk3,bk1.TransactionAmount as bk1,bk1.TransactionReason as bkReason1,lx3.value as bklx
,a.MaterialCost,a.materialcost - nvl(totalall.clkk,0) as ActualMaterialCost, ---实际结算材料费
round(a.LaborCost,2) as LaborCost,----工时费
a.LaborCost - nvl(totalall.gskk,0) as ActualLaborCost,a.OtherCost - nvl(totalall.Otherkk,0) as OtherCost,a.OtherCostReason,a.TotalAmount,
stb.DealerTripPerson as tbrs,stb.ServiceTripPerson as jsrs,stb.DealerTripDuration as tbts,
stb.ServiceTripDuration as jsts,stb.OutSubsidyPrice as rybz,stb.ServiceTripDistance as tblc,
stb.SettleDistance as jslc,stb.OutServiceCarUnitPrice as clbz,
case rpo.WarrantyStatus when 1 then nvl(stb.DealerTripPerson,0)*nvl(stb.DealerTripDuration,0)*nvl(stb.OutSubsidyPrice,0) + nvl(ServiceTripDistance,0)*stb.OutServiceCarUnitPrice+nvl(stb.TowCharge,0)+nvl(stb.OtherCost,0) else 0 end as WcTotal ,---保内
case rpo.WarrantyStatus when 1 then stb.Totalamount - nvl(wckk.TransactionAmount,0) else 0 end as ActualWcTotal,---保内实际
0 as WcTotalW ,---保外
0 as ActualWcTotalW,---保外实际
(a.materialcost - nvl(kk1.TransactionAmount,0)+a.LaborCost - nvl(kk2.TransactionAmount,0)+ nvl(bk1.TransactionAmount,0) +nvl(stb.Totalamount,0)- nvl(wckk.TransactionAmount,0)+a.OtherCost+a.PartsManagementCost) as ActualTotal
,a.Mileage,a.WorkingHours,a.Capacity,a.RepairRequestTime,a.MalfunctionReason as MalfunctionDescription,a.VehicleContactPerson,
a.ContactPhone,a.ContactPhone as CellNumber,a.ApproveTime,a.ContactAddress,stb.ServiceTripDuration,stb.Servicetripperson,
stb.DetailedAddress,stb.Outservicecarunitprice,stb.Settledistance,stb.OutSubsidyPrice,stb.Towcharge,a.InitialApprovertComment,
a.ServiceDepartmentComment as ServiceDepartmentComment,a.RejectReason,a.RejectQty,a.ClaimSupplierCode as TempSupplierCode,a.ClaimSupplierName as TempSupplierName,
case a.Supplierconfirmstatus when 1 then '未确认' when 2 then '已确认' when 3 then '确认未通过' end as SupplierConfirmStatus,
case a.IfClaimToSupplier when 1 then '是' else '否' end as IfClaimToSupplier,a.DealerCode,a.DealerName,a.CreatorName,a.CreateTime
,a.EngineModel,a.EngineSerialNumber,a.Salesdate,a.OutOfFactoryDate,c.FirstStoppageMileage,a.Suppliercheckcomment,round(gcc.coefficient,2) as Grade,
a.ClaimSupplierName as FaultyPartsSupplierName,a.PartsManagementCost,----配件管理费
nvl(a.PartsManagementCost,0) - nvl(totalall.glfkk,0) as ActualGlfCost,----实际结算管理费
scd.createtime as CacuTime,
null as 产品线分类,   ----------------产品线分类
null as ProductLineType,null as 外出初审意见,null as 外出终审意见
from sddcs.RepairClaimBill a
inner join  sddcs.partssalescategory b on a.partssalescategoryid = b.id
inner join  sddcs.vehicleinformation c on c.id = a.vehicleid
left join  (
            select epl.id,epl.status,epl.enginename as name from sddcs.engineproductline epl union all
            select spl1.id,spl1.status,spl1.name from sddcs.serviceproductline spl1
       ) spl on spl.id = a.ServiceProductLineId and spl.status=1
left join  sddcs.MarketingDepartment d on d.id = a.marketingdepartmentid
left join(select key,value from  sddcs.keyvalueitem where category = 'DCS' and name ='RepairClaimBill_RepairClaimStatus') st on st.key = a.status
left join(select key,value from  sddcs.keyvalueitem where category = 'DCS' and name ='Repair_Tpye')rt on rt.key = a.repairtype
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from
(select max(a.id) id ,a.sourceid,a.DebitOrReplenish
from  sddcs.ExpenseAdjustmentBill a where a.status = 2 group by a.sourceid,a.DebitOrReplenish)a
inner join  sddcs.ExpenseAdjustmentBill b on a.id = b.id where b.DebitOrReplenish=1 and b.SourceType in (1,2,3)) kk1 on kk1.sourceid = a.id
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from(select min(a.id) id ,a.sourceid,a.DebitOrReplenish
from  sddcs.ExpenseAdjustmentBill a where a.status = 2 group by a.sourceid,a.DebitOrReplenish)a
inner join  sddcs.ExpenseAdjustmentBill b on a.id = b.id where b.DebitOrReplenish=1 and b.SourceType in (1,2,3)
) kk2 on kk2.sourceid = a.id and kk1.id<>kk2.id
left join (select key,value from  sddcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx1 on lx1.key =kk1.TransactionCategory
left join (select key,value from  sddcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx2 on lx2.key =kk2.TransactionCategory
left join (select b.id,a.sourceid,b.TransactionAmount,a.DebitOrReplenish,b.TransactionReason,b.TransactionCategory from(select max(a.id) id ,a.sourceid,a.DebitOrReplenish
from  sddcs.ExpenseAdjustmentBill a group by a.sourceid,a.DebitOrReplenish)a
inner join  sddcs.ExpenseAdjustmentBill b on a.id = b.id where b.status=2 and b.DebitOrReplenish=2 and b.SourceType in (1,2,3)) bk1 on bk1.sourceid = a.id
left join (select key,value from  sddcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx3 on lx3.key =bk1.TransactionCategory
----计算 材料费扣补款合计，工时费扣补款合计 + 配件管理费扣补款合计 + 其他 扣补款合计
left join (
select a.id,nvl(clkk.fee,0)-nvl(clbk.fee,0) as clkk,nvl(gskk.fee,0)-nvl(gsbk.fee,0) as gskk
,nvl(glfkk.fee,0)-nvl(glfbk.fee,0) as glfkk ,nvl(Otherkk.fee,0) - nvl(Otherbk.fee,0)as Otherkk
from
 sddcs.RepairClaimBill a left join
(select sourceid,sum(TransactionAmount) as fee from  sddcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(2,16)  and DebitOrReplenish=1 and sourcetype = 1 group by sourceid)clkk on clkk.sourceid = a.id
left join(select sourceid,sum(TransactionAmount) as fee from  sddcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(2,16) and DebitOrReplenish=2  and sourcetype = 1 group by sourceid)clbk on clbk.sourceid = a.id
----工时扣补款
left join(select sourceid,sum(TransactionAmount) as fee from  sddcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(1,15) and DebitOrReplenish=1  and sourcetype = 1 group by sourceid)gskk on gskk.sourceid = a.id
left join(select sourceid,sum(TransactionAmount) as fee from  sddcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(1,15) and DebitOrReplenish=2  and sourcetype = 1 group by sourceid)gsbk on gsbk.sourceid = a.id
----管理费扣补款
left join(select sourceid,sum(TransactionAmount) as fee from  sddcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(3,17) and DebitOrReplenish=1  and sourcetype = 1 group by sourceid)glfkk on glfkk.sourceid = a.id
left join(select sourceid,sum(TransactionAmount) as fee from  sddcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory in(3,17) and DebitOrReplenish=2  and sourcetype = 1 group by sourceid)glfbk on glfbk.sourceid = a.id
----其他扣补款合计
left join(select sourceid,sum(TransactionAmount) as fee from  sddcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory not in(1,2,3,15,16,17) and DebitOrReplenish=1  and sourcetype = 1 group by sourceid)Otherkk on Otherkk.sourceid = a.id
left join(select sourceid,sum(TransactionAmount) as fee from  sddcs.ExpenseAdjustmentBill
where status=2 and TransactionCategory not in(1,2,3,15,16,17) and DebitOrReplenish=2  and sourcetype = 1 group by sourceid)Otherbk on Otherbk.sourceid = a.id
)totalall on totalall.id = a.id
left join  sddcs.ServiceTripClaimBill stb on stb.Repairclaimbillid= a.id and stb.status = 6
left join(select sourcecode,sum(TransactionAmount) as TransactionAmount from  sddcs.ExpenseAdjustmentBill
where status <>99 and TransactionCategory = 12 group by sourcecode)wckk on wckk.sourcecode = stb.code
left join  sddcs.RepairOrder rpo on rpo.id = stb.RepairOrderId
left join  sddcs.DealerServiceInfo dsi on dsi.dealerid = a.dealerid and dsi.partssalescategoryid = a.partssalescategoryid
left join  sddcs.GradeCoefficient gcc on gcc.id = dsi.gradecoefficientid
left join (select b.sourcecode,a.status,a.createtime from  sddcs.SsClaimSettlementBill a inner join
sddcs.SsClaimSettlementDetail b on a.id = b.SsClaimSettlementBillId where a.status<> 99)scd on scd.sourcecode = a.ClaimBillCode
where a.status<>99
union all
-----2、没有源单据的扣补款费用
select scd.status as CacuStatus,case scd.status when 1 then '新增' when 2 then '已审批' when 3 then '发票登记' when 4 then '费用兑现'when null then '未结算' end as CacuStatusValue
,b.branchcode,a.PartsSalesCategoryId as CategoryId,null as RepairType,decode(a.status,1,1,2,4) as status,spl.id as ProductLineId,a.code as Claimbillcode,decode(a.status,1,'新增',2,'生效') StatusValue,null as RepairClaimApplicationCode,
null as ServiceTripClaimAppCode,b.name as CategoryName,
null as Vin,spl.name as productlinename,null as ApprovalComment,null as ProductCategoryName,null as  OutFactoryCode,null as RepairTypeValue,b.BranchName,
mdt.name as MarKetName,null as MalfunctionCode,null as MalfunctionReason,null as FaultyPartsCode,null as FaultyPartsName,
case a.DebitOrReplenish when 1 then a.TransactionAmount else 0 end as kk1
,case a.DebitOrReplenish when 1 then a.TransactionReason else null end as kkReason1,
case a.DebitOrReplenish when 1 then lx1.value else null end as kklx1,0 as kk2,
null as kkReason2,null as kklx2,0 as kk3,case a.DebitOrReplenish when 2 then a.TransactionAmount else 0 end as bk1,
case a.DebitOrReplenish when 2 then a.TransactionReason else null end as bkReason1,case a.DebitOrReplenish when 2 then lx1.value else null end as bklx
,null as MaterialCost,null as ActualMaterialCost,0 as LaborCost,
0 as ActualLaborCost,case a.DebitOrReplenish when 1 then -a.TransactionAmount else a.TransactionAmount end as OtherCost,null as OtherCostReason,0 as TotalAmount,
null as tbrs,null as jsrs,null as tbts,
null as jsts,null as rybz,null as tblc,
null as jslc,null as clbz,
null as WcTotal,
0 as ActualWcTotal,0 as WcTotalW,0 as ActualWcTotalW,case a.DebitOrReplenish when 1 then -a.TransactionAmount else a.TransactionAmount end as ActualTotal
,0 as Mileage,0 as WorkingHours,null as Capacity,null as RepairRequestTime,null as MalfunctionDescription,null as VehicleContactPerson,
a.DealerPhoneNumber as ContactPhone,null as CellNumber,null as ApproveTime,null as ContactAddress,null as ServiceTripDuration,null as Servicetripperson,
null as DetailedAddress,null as Outservicecarunitprice,null as Settledistance,null as OutSubsidyPrice,null as Towcharge,null as InitialApprovertComment,
null as ServiceDepartmentComment,null as RejectReason,0 as RejectQty,null as TempSupplierCode,null as TempSupplierName,
null as SupplierConfirmStatus,
null as IfClaimToSupplier,a.DealerCode,a.DealerName,a.CreatorName,a.CreateTime
,null as EngineModel,null as EngineSerialNumber,null as Salesdate,null as OutOfFactoryDate,null as FirstStoppageMileage,null as Suppliercheckcomment,round(gcc.coefficient,2) as Grade,
null as FaultyPartsSupplierName,0 as PartsManagementCost,0 as ActualGlfCost,scd.createtime as CacuTime,
null as 产品线分类,   ----------------产品线分类
null as ProductLineType,null as 外出初审意见,null as 外出终审意见
from  sddcs.ExpenseAdjustmentBill a
inner join  sddcs.partssalescategory b on a.partssalescategoryid = b.id
left join (select key,value from  sddcs.keyvalueitem where category = 'DCS' and name ='ExpenseAdjustmentBill_TransactionCategory')lx1 on lx1.key =a.TransactionCategory
left join  sddcs.DealerServiceInfo dsi on dsi.dealerid = a.dealerid and dsi.partssalescategoryid= a.partssalescategoryid
left join  sddcs.GradeCoefficient gcc on gcc.id = dsi.gradecoefficientid
left join  (
            select epl.id,epl.status,epl.enginename as name from sddcs.engineproductline epl union all
            select spl1.id,spl1.status,spl1.name from sddcs.serviceproductline spl1
       ) spl on spl.id = a.serviceproductlineid
left join (select b.branchid,b.name,b.partssalescategoryid,a.dealerid from  sddcs.DealerMarketDptRelation a
inner join  sddcs.marketingdepartment b on a.marketid = b.id)mdt on mdt.dealerid = a.dealerid
and mdt.partssalescategoryid= a.partssalescategoryid and mdt.branchid = a.branchid
left join (select b.sourcecode,a.status,a.Createtime from  sddcs.SsClaimSettlementBill a inner join
 sddcs.SsClaimSettlementDetail b on a.id = b.SsClaimSettlementBillId where a.status<> 99)scd on scd.sourcecode = a.code
where a.status<>99 and a.sourceid is null;
