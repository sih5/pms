create or replace view warehouseagedetailsta as
select (select code from dcs.warehouse where warehouse.id = t.warehouseid) 仓库编号,
       (select name from dcs.warehouse where warehouse.id = t.warehouseid) 仓库名称,
       (select code from dcs.sparepart where sparepart.id = t.partid) 配件编号,
       (select name from dcs.sparepart where sparepart.id = t.partid) 配件名称,
        (select code from dcs.branch where branch.id= d.branchid) branchcode,
        e.plannedprice,
       h.salesprice,
       t.qty 库存数量,
       t.qty * e.plannedprice as 三个月未出库计划金额,
       t.qty * h.salesprice 三个月未出库销售金额,
       v.qty * e.plannedprice as 六个月未出库计划金额,
       v.qty * h.salesprice 六个月未出库销售金额,
       g.qty * e.plannedprice as 十二个月未出库计划金额,
       g.qty * h.salesprice 十二个月未出库销售金额,
       s.qty * e.plannedprice as 十二个月以上未出库计划金额,
       s.qty * h.salesprice 十二个月以上未出库销售金额

  from (select warehouseid, partid, sum(partsstock.quantity) qty
          from dcs.partsstock
         where not exists (select 1
                  from dcs.partsoutboundbill a
                 inner join dcs.partsoutboundbilldetail b
                    on a.id = b.partsoutboundbillid
                 where a.warehouseid = partsstock.warehouseid
                   and b.sparepartid = partsstock.partid
                   and a.createtime >= sysdate - 90)
         group by warehouseid, partid) t
  left join dcs.salesunitaffiwarehouse c
    on c.warehouseid = t.warehouseid
  left join dcs.salesunit d
    on d.id = c.salesunitid
  left join dcs.partsplannedprice e
    on e.partssalescategoryid = d.partssalescategoryid
   and t.partid = e.sparepartid
  left join dcs.partssalesprice h
    on h.sparepartid = t.partid
   and h.partssalescategoryid = d.partssalescategoryid
   and h.status = 1
  left join (select warehouseid, partid, sum(partsstock.quantity) qty
               from dcs.partsstock
              where not exists (select 1
                       from dcs.partsoutboundbill a
                      inner join dcs.partsoutboundbilldetail b
                         on a.id = b.partsoutboundbillid
                      where a.warehouseid = partsstock.warehouseid
                        and b.sparepartid = partsstock.partid
                        and a.createtime >= sysdate - 180)
              group by warehouseid, partid) v
    on t.warehouseid = v.warehouseid
   and t.partid = v.partid
  left join (select warehouseid, partid, sum(partsstock.quantity) qty
               from dcs.partsstock
              where not exists (select 1
                       from dcs.partsoutboundbill a
                      inner join dcs.partsoutboundbilldetail b
                         on a.id = b.partsoutboundbillid
                      where a.warehouseid = partsstock.warehouseid
                        and b.sparepartid = partsstock.partid
                        and a.createtime >= sysdate - 365)
              group by warehouseid, partid) g
    on t.warehouseid = g.warehouseid
   and t.partid = g.partid
  left join (select warehouseid, partid, sum(partsstock.quantity) qty
               from dcs.partsstock
              where not exists
              (select 1
                       from dcs.partsoutboundbill a
                      inner join dcs.partsoutboundbilldetail b
                         on a.id = b.partsoutboundbillid
                      where a.warehouseid = partsstock.warehouseid
                        and b.sparepartid = partsstock.partid)
              group by warehouseid, partid) s
    on t.warehouseid = s.warehouseid
   and t.partid = s.partid
 where t.qty > 0
--yxdcs
union
select (select code from yxdcs.warehouse where warehouse.id = t.warehouseid) 仓库编号,
       (select name from yxdcs.warehouse where warehouse.id = t.warehouseid) 仓库名称,
       (select code from yxdcs.sparepart where sparepart.id = t.partid) 配件编号,
       (select name from yxdcs.sparepart where sparepart.id = t.partid) 配件名称,
          (select code from yxdcs.branch where branch.id= d.branchid) branchcode,
        e.plannedprice,
       h.salesprice,
       t.qty 库存数量,
       t.qty * e.plannedprice as 三个月未出库计划金额,
       t.qty * h.salesprice 三个月未出库销售金额,
       v.qty * e.plannedprice as 六个月未出库计划金额,
       v.qty * h.salesprice 六个月未出库销售金额,
       g.qty * e.plannedprice as 十二个月未出库计划金额,
       g.qty * h.salesprice 十二个月未出库销售金额,
       s.qty * e.plannedprice as 十二个月以上未出库计划金额,
       s.qty * h.salesprice 十二个月以上未出库销售金额
  from (select warehouseid, partid, sum(partsstock.quantity) qty
          from yxdcs.partsstock
         where not exists (select 1
                  from yxdcs.partsoutboundbill a
                 inner join yxdcs.partsoutboundbilldetail b
                    on a.id = b.partsoutboundbillid
                 where a.warehouseid = partsstock.warehouseid
                   and b.sparepartid = partsstock.partid
                   and a.createtime >= sysdate - 90)
         group by warehouseid, partid) t
  left join yxdcs.salesunitaffiwarehouse c
    on c.warehouseid = t.warehouseid
  left join yxdcs.salesunit d
    on d.id = c.salesunitid
  left join yxdcs.partsplannedprice e
    on e.partssalescategoryid = d.partssalescategoryid
   and t.partid = e.sparepartid
  left join yxdcs.partssalesprice h
    on h.sparepartid = t.partid
   and h.partssalescategoryid = d.partssalescategoryid
   and h.status = 1
  left join (select warehouseid, partid, sum(partsstock.quantity) qty
               from yxdcs.partsstock
              where not exists (select 1
                       from yxdcs.partsoutboundbill a
                      inner join yxdcs.partsoutboundbilldetail b
                         on a.id = b.partsoutboundbillid
                      where a.warehouseid = partsstock.warehouseid
                        and b.sparepartid = partsstock.partid
                        and a.createtime >= sysdate - 180)
              group by warehouseid, partid) v
    on t.warehouseid = v.warehouseid
   and t.partid = v.partid
  left join (select warehouseid, partid, sum(partsstock.quantity) qty
               from yxdcs.partsstock
              where not exists (select 1
                       from yxdcs.partsoutboundbill a
                      inner join yxdcs.partsoutboundbilldetail b
                         on a.id = b.partsoutboundbillid
                      where a.warehouseid = partsstock.warehouseid
                        and b.sparepartid = partsstock.partid
                        and a.createtime >= sysdate - 365)
              group by warehouseid, partid) g
    on t.warehouseid = g.warehouseid
   and t.partid = g.partid
  left join (select warehouseid, partid, sum(partsstock.quantity) qty
               from yxdcs.partsstock
              where not exists
              (select 1
                       from yxdcs.partsoutboundbill a
                      inner join yxdcs.partsoutboundbilldetail b
                         on a.id = b.partsoutboundbillid
                      where a.warehouseid = partsstock.warehouseid
                        and b.sparepartid = partsstock.partid)
              group by warehouseid, partid) s
    on t.warehouseid = s.warehouseid
   and t.partid = s.partid
 where t.qty > 0
--gcdcs
union
select (select code from gcdcs.warehouse where warehouse.id = t.warehouseid) 仓库编号,
       (select name from gcdcs.warehouse where warehouse.id = t.warehouseid) 仓库名称,
       (select code from gcdcs.sparepart where sparepart.id = t.partid) 配件编号,
       (select name from gcdcs.sparepart where sparepart.id = t.partid) 配件名称,
          (select code from gcdcs.branch where branch.id= d.branchid) branchcode,

        e.plannedprice,
       h.salesprice,
       t.qty 库存数量,
       t.qty * e.plannedprice as 三个月未出库计划金额,
       t.qty * h.salesprice 三个月未出库销售金额,
       v.qty * e.plannedprice as 六个月未出库计划金额,
       v.qty * h.salesprice 六个月未出库销售金额,
       g.qty * e.plannedprice as 十二个月未出库计划金额,
       g.qty * h.salesprice 十二个月未出库销售金额,
       s.qty * e.plannedprice as 十二个月以上未出库计划金额,
       s.qty * h.salesprice 十二个月以上未出库销售金额
  from (select warehouseid, partid, sum(partsstock.quantity) qty
          from gcdcs.partsstock
         where not exists (select 1
                  from gcdcs.partsoutboundbill a
                 inner join gcdcs.partsoutboundbilldetail b
                    on a.id = b.partsoutboundbillid
                 where a.warehouseid = partsstock.warehouseid
                   and b.sparepartid = partsstock.partid
                   and a.createtime >= sysdate - 90)
         group by warehouseid, partid) t
  left join gcdcs.salesunitaffiwarehouse c
    on c.warehouseid = t.warehouseid
  left join gcdcs.salesunit d
    on d.id = c.salesunitid
  left join gcdcs.partsplannedprice e
    on e.partssalescategoryid = d.partssalescategoryid
   and t.partid = e.sparepartid
  left join gcdcs.partssalesprice h
    on h.sparepartid = t.partid
   and h.partssalescategoryid = d.partssalescategoryid
   and h.status = 1
  left join (select warehouseid, partid, sum(partsstock.quantity) qty
               from gcdcs.partsstock
              where not exists (select 1
                       from gcdcs.partsoutboundbill a
                      inner join gcdcs.partsoutboundbilldetail b
                         on a.id = b.partsoutboundbillid
                      where a.warehouseid = partsstock.warehouseid
                        and b.sparepartid = partsstock.partid
                        and a.createtime >= sysdate - 180)
              group by warehouseid, partid) v
    on t.warehouseid = v.warehouseid
   and t.partid = v.partid
  left join (select warehouseid, partid, sum(partsstock.quantity) qty
               from gcdcs.partsstock
              where not exists (select 1
                       from gcdcs.partsoutboundbill a
                      inner join gcdcs.partsoutboundbilldetail b
                         on a.id = b.partsoutboundbillid
                      where a.warehouseid = partsstock.warehouseid
                        and b.sparepartid = partsstock.partid
                        and a.createtime >= sysdate - 365)
              group by warehouseid, partid) g
    on t.warehouseid = g.warehouseid
   and t.partid = g.partid
  left join (select warehouseid, partid, sum(partsstock.quantity) qty
               from gcdcs.partsstock
              where not exists
              (select 1
                       from gcdcs.partsoutboundbill a
                      inner join gcdcs.partsoutboundbilldetail b
                         on a.id = b.partsoutboundbillid
                      where a.warehouseid = partsstock.warehouseid
                        and b.sparepartid = partsstock.partid)
              group by warehouseid, partid) s
    on t.warehouseid = s.warehouseid
   and t.partid = s.partid
 where t.qty > 0
union
--sddcs
select (select code from sddcs.warehouse where warehouse.id = t.warehouseid) 仓库编号,
       (select name from sddcs.warehouse where warehouse.id = t.warehouseid) 仓库名称,
       (select code from sddcs.sparepart where sparepart.id = t.partid) 配件编号,
       (select name from sddcs.sparepart where sparepart.id = t.partid) 配件名称,
          (select code from sddcs.branch where branch.id= d.branchid) branchcode,

        e.plannedprice,
       h.salesprice,
       t.qty 库存数量,
       t.qty * e.plannedprice as 三个月未出库计划金额,
       t.qty * h.salesprice 三个月未出库销售金额,
       v.qty * e.plannedprice as 六个月未出库计划金额,
       v.qty * h.salesprice 六个月未出库销售金额,
       g.qty * e.plannedprice as 十二个月未出库计划金额,
       g.qty * h.salesprice 十二个月未出库销售金额,
       s.qty * e.plannedprice as 十二个月以上未出库计划金额,
       s.qty * h.salesprice 十二个月以上未出库销售金额

  from (select warehouseid, partid, sum(partsstock.quantity) qty
          from sddcs.partsstock
          where not exists (select 1
                  from sddcs.partsoutboundbill a
                 inner join sddcs.partsoutboundbilldetail b
                    on a.id = b.partsoutboundbillid
                 where a.warehouseid = partsstock.warehouseid
                   and b.sparepartid = partsstock.partid
                   and a.createtime >= sysdate - 90)
         group by warehouseid, partid) t

  left join sddcs.salesunitaffiwarehouse c
    on c.warehouseid = t.warehouseid
  left join sddcs.salesunit d
    on d.id = c.salesunitid
  left join sddcs.partsplannedprice e
    on e.partssalescategoryid = d.partssalescategoryid
   and t.partid = e.sparepartid
  left join sddcs.partssalesprice h
    on h.sparepartid = t.partid
   and h.partssalescategoryid = d.partssalescategoryid
   and h.status = 1
  left join (select warehouseid, partid, sum(partsstock.quantity) qty
               from sddcs.partsstock
              where not exists (select 1
                       from sddcs.partsoutboundbill a
                      inner join sddcs.partsoutboundbilldetail b
                         on a.id = b.partsoutboundbillid
                      where a.warehouseid = partsstock.warehouseid
                        and b.sparepartid = partsstock.partid
                        and a.createtime >= sysdate - 180)
              group by warehouseid, partid) v
    on t.warehouseid = v.warehouseid
   and t.partid = v.partid
  left join (select warehouseid, partid, sum(partsstock.quantity) qty
               from sddcs.partsstock
              where not exists (select 1
                       from sddcs.partsoutboundbill a
                      inner join sddcs.partsoutboundbilldetail b
                         on a.id = b.partsoutboundbillid
                      where a.warehouseid = partsstock.warehouseid
                        and b.sparepartid = partsstock.partid
                        and a.createtime >= sysdate - 365)
              group by warehouseid, partid) g
    on t.warehouseid = g.warehouseid
   and t.partid = g.partid
  left join (select warehouseid, partid, sum(partsstock.quantity) qty
               from sddcs.partsstock
              where not exists
              (select 1
                       from sddcs.partsoutboundbill a
                      inner join sddcs.partsoutboundbilldetail b
                         on a.id = b.partsoutboundbillid
                      where a.warehouseid = partsstock.warehouseid
                        and b.sparepartid = partsstock.partid)
              group by warehouseid, partid) s
    on t.warehouseid = s.warehouseid
   and t.partid = s.partid
 where t.qty > 0;
