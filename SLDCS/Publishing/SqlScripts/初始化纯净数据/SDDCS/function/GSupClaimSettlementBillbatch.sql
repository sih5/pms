create or replace function GSupClaimSettlementBillbatch(FBranchId in integer, FStartTime in date, FEndTime in date,
  FCreatorId in integer, FCreatorName in varchar2,FPartsSalesCategoryId in integer,FServiceProductLineId in integer,FProductLineType in integer) return integer
as
  GeneratedBillQuantity integer;
  /*营销分公司编号*/
  FBranchCode VARCHAR2(50);
  /*营销分公司名称*/
  FBranchName VARCHAR2(100);
  /*编号模板Id*/
  FComplateId integer;
  /*编码规则相关设置*/
  FDateFormat date;
  FDateStr varchar2(8);
  FSerialLength integer;
  FNewCode varchar2(50);
  FSupplierClaimSettlementBillId integer;
    ifsuppilerbranchid  integer;
begin
  /*编码规则变更，需同步调整*/
  --FDateFormat:=trunc(sysdate,'YEAR');
  --FDateFormat:=trunc(sysdate,'MONTH');
  FDateFormat:=trunc(sysdate,'DD');
  FDateStr:=to_char(sysdate,'yyyymmdd');
  FSerialLength:=6;
  FNewCode:='SCSB{CORPCODE}'||FDateStr||'{SERIAL}';

  /*获取编号生成模板Id*/
  begin
    select Id
      into FComplateId
      from codetemplate
     where name = 'SupplierClaimSettlementBill'
       and IsActived = 1;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20001, '未找到名称为"SupplierClaimSettlementBill"的有效编码规则。');
    when others then
      raise;
  end;
 /* begin
    select Id
    into ifsuppilerbranchid
    from branchsupplierrelation t
    where t.branchid=FBranchId and( (FSupplierId is null) or (t.supplierid=FSupplierId)) and t.partssalescategoryid=FPartsSalesCategoryId;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20002,'未维护供应商的索赔结算相关系数，请维护分公司与供应商关系管理！');
      when others then
        raise;
  end;*/
  select Code,Name into FBranchCode, FBranchName from Branch where id = FBranchId;
  GeneratedBillQuantity := 0;

  /*维修保养索赔单*/
  insert into BILLFORSUPPLIERCLAIMSETTLEMENT
    (SourceId,
     SourceCode,
     SourceType,
     supplierId,
     BranchId,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OldPartTranseCost,
     OtherCost,
     Partssalescategoryid,
     ServiceProductLineId,
     ProductLineType)
    select RepairClaimBill.Id,
           RepairClaimBill.ClaimBillCode,
           decode(ClaimType, 1, 5, 2, 10, 3, 15, 10, 5, 0) SourceType, --维修保养索赔单索赔类型、服务站索赔结算源单据类型，可能存在差异
           RepairClaimBill.Claimsupplierid,
           RepairClaimBill.BranchId,
           decode(a.IfSPByLabor,
                  0,
                  (select sum(nvl(b.defaultlaborhour, 0) *
                              nvl(b.laborunitprice, 0))
                     from RepairClaimItemDetail b
                    where b.repairclaimbillid = RepairClaimBill.id) *
                  nvl(a.LaborCoefficient, 0),
                  1,
                  (select sum(nvl(b.defaultlaborhour, 0))
                     from RepairClaimItemDetail b
                    where b.repairclaimbillid = RepairClaimBill.id) *
                  nvl(a.LaborUnitPrice, 0),
                  (select sum(nvl(b.defaultlaborhour, 0) *
                              nvl(b.laborunitprice, 0))
                     from RepairClaimItemDetail b
                    where b.repairclaimbillid = RepairClaimBill.id) *
                  nvl(a.LaborCoefficient, 0)) LaborCost,
           --t.laborcost,
           RepairClaimBill.Materialcost * nvl(a.partsclaimcoefficient, 0),
           RepairClaimBill.Materialcost * nvl(a.partsclaimcoefficient, 0) *
           nvl((select distinct PartsManagementCostRate.Rate
                 from PartsManagementCostRate
                where PartsManagementCostRate.Partsmanagementcostgradeid =
                      a.partsmanagementcostgradeid
                  and rownum = 1),
               0) as PartsManagementCost,
           0 FieldServiceExpense,
           0 DebitAmount,
           0 ComplementAmount,
           --nvl(t.MaterialCost,0)*nvl(a.OldPartTransCoefficient,0)  as OldPartTranseCost,
           nvl(RepairClaimBill.Materialcost, 0) *
           nvl(a.partsclaimcoefficient, 0) *
           nvl(a.OldPartTransCoefficient, 0) as OldPartTranseCost,
           NVL(RepairClaimBill.OtherCost, 0) OtherCost,
           RepairClaimBill.Partssalescategoryid,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  RepairClaimBill.ServiceProductLineId) ServiceProductLineId,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From serviceproductlineview v
                    Where v.ProductLineId =
                          RepairClaimBill.ServiceProductLineId)) ProductLineType
      from RepairClaimBill
    /* --2014-06-13 add
    left join (select rcid.repairclaimbillid, sum(case when rcmd.UsedPartsReturnPolicy = 1 then rcmd.MaterialCost else 0 end) as MaterialCost
         from RepairClaimMaterialDetail rcmd inner join RepairClaimItemDetail rcid on rcmd.repairclaimitemdetailid = rcid.id
         inner join RepairClaimBill r on r.id = rcid.repairclaimbillid
         where r.CreateTime <= FEndTime and (FStartTime is null or r.CreateTime >= FStartTime)
         and r.BranchId = FBranchId and r.status = 4
         group by  rcid.repairclaimbillid
         ) dt on dt.repairclaimbillid = RepairClaimBill.Id*/ --取消
    /* inner join repairclaimsupplierdetail t
        on t.repairclaimid = RepairClaimBill.id*/
     inner join branchsupplierrelation a
        on a.branchid = RepairClaimBill.Branchid
       and a.partssalescategoryid = RepairClaimBill.Partssalescategoryid
       and a.supplierid = RepairClaimBill.ClaimSupplierId and a.status<>99
     where RepairClaimBill.CreateTime <= FEndTime
       and (FStartTime is null or RepairClaimBill.CreateTime >= FStartTime)
       and RepairClaimBill.SupplierSettleStatus = 2 and RepairClaimBill.Settlementstatus=3

       and RepairClaimBill.BranchId = FBranchId
    --   and (FSupplierId is null or RepairClaimBill.ClaimSupplierId = FSupplierId)
       and RepairClaimBill.status = 4
          --and RepairClaimBill.repairtype=15
       and (FPartsSalesCategoryId = 0 or RepairClaimBill.
            PartsSalesCategoryId = FPartsSalesCategoryId)
          --  and (FProductLineType is null or ProductLineType=FProductLineType)
       and (FServiceProductLineId is null or FServiceProductLineId = 0 or
           ServiceProductLineId = FServiceProductLineId);
  /*
  insert into BILLFORSUPPLIERCLAIMSETTLEMENT
    (SourceId,
     SourceCode,
     SourceType,
     supplierId,
     BranchId,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OldPartTranseCost,
     OtherCost,
     Partssalescategoryid,
     ServiceProductLineId,
     ProductLineType)
    select T.Id,
           RepairClaimBill.ClaimBillCode,
           decode(ClaimType, 1, 5, 2, 10, 3, 15, 10, 5, 0) SourceType, --维修保养索赔单索赔类型、服务站索赔结算源单据类型，可能存在差异
           t.supplierid,
           RepairClaimBill.BranchId,
           decode(a.IfSPByLabor,
                  0,
                  nvl(t.laborcost, 0) * nvl(a.LaborCoefficient, 0),
                  1,
                  (select sum(nvl(b.defaultlaborhour, 0))
                     from RepairClaimItemDetail b
                    where b.repairclaimbillid = RepairClaimBill.id) *
                  nvl(a.LaborUnitPrice, 0),
                  nvl(t.laborcost, 0) * nvl(a.LaborCoefficient, 0)) LaborCost,
           --t.laborcost,
           t.MaterialCost *,
           NVL(t.partsmanagementcost, 0) PartsManagementCost,
           0 FieldServiceExpense,
           0 DebitAmount,
           0 ComplementAmount,
           -- nvl(t.MaterialCost,0)*nvl(a.OldPartTransCoefficient,0)  as OldPartTranseCost,
           nvl(dt.MaterialCost, 0) * nvl(a.OldPartTransCoefficient, 0) as OldPartTranseCost,
           NVL(t.OtherCost, 0) OtherCost,
           RepairClaimBill.Partssalescategoryid,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  RepairClaimBill.ServiceProductLineId) ServiceProductLineId,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From serviceproductlineview v
                    Where v.ProductLineId =
                          RepairClaimBill.ServiceProductLineId)) ProductLineType
      from RepairClaimBill
    --2014-06-13 add
      left join (select rcid.repairclaimbillid,
                        sum(case
                              when rcmd.UsedPartsReturnPolicy = 1 then
                               rcmd.MaterialCost
                              else
                               0
                            end) as MaterialCost
                   from RepairClaimMaterialDetail rcmd
                  inner join RepairClaimItemDetail rcid
                     on rcmd.repairclaimitemdetailid = rcid.id
                  inner join RepairClaimBill r
                     on r.id = rcid.repairclaimbillid
                  where r.CreateTime <= FEndTime
                    and (FStartTime is null or r.CreateTime >= FStartTime)
                    and r.BranchId = FBranchId
                    and r.status = 4
                    and r.repairtype <> 15
                  group by rcid.repairclaimbillid) dt
        on dt.repairclaimbillid = RepairClaimBill.Id
     inner join repairclaimsupplierdetail t
        on t.repairclaimid = RepairClaimBill.id
     inner join branchsupplierrelation a
        on a.branchid = RepairClaimBill.Branchid
       and a.partssalescategoryid = RepairClaimBill.Partssalescategoryid
       and a.supplierid = t.supplierid
     where RepairClaimBill.CreateTime <= FEndTime
       and (FStartTime is null or RepairClaimBill.CreateTime >= FStartTime)
       and t.settlestatus = 2
       and RepairClaimBill.BranchId = FBranchId
       and (FSupplierId is null or T.SUPPLIERID = FSupplierId)
       and RepairClaimBill.status = 4
       and RepairClaimBill.repairtype <> 15
       and (FPartsSalesCategoryId = 0 or RepairClaimBill.
            PartsSalesCategoryId = FPartsSalesCategoryId)
          --and (FProductLineType is null or ProductLineType=FProductLineType)
       and (FServiceProductLineId is null or FServiceProductLineId = 0 or
           ServiceProductLineId = FServiceProductLineId);*/



  /*外出服务索赔单*/
  insert into BILLFORSUPPLIERCLAIMSETTLEMENT
    (SourceId,
     SourceCode,
     SourceType,
     SUPPLIERID,
     BranchId,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     OldPartTranseCost,
     Partssalescategoryid,
     ServiceProductLineId,
     ProductLineType)
    select ServiceTripClaimBill.Id SourceId,
           Code SourceCode,
           20 SourceType,
           ClaimSupplierId,
           ServiceTripClaimBill.BranchId,
           0 LaborCost,
           0 MaterialCost,
           0 PartsManagementCost,
           decode(a.IfHaveOutFee,
                  1,
                  (NVL(TowCharge, 0) + NVL(OtherCost, 0) +
                  NVL(ServiceTripClaimBill.SettleDistance, 0) *
                  NVL(ServiceTripClaimBill.OutServiceCarUnitPrice, 0) /**
                  NVL(IfUseOwnVehicle, 0)*/ +
                  NVL(ServiceTripClaimBill.OutSubsidyPrice, 0) *
                  nvl(ServiceTripClaimBill.ServiceTripPerson, 0) *
                  nvl(ServiceTripClaimBill.ServiceTripDuration, 0)),
                  0,
                  (NVL(TowCharge, 0) + NVL(OtherCost, 0) +
                  NVL(ServiceTripClaimBill.SettleDistance, 0) *
                  NVL(A.OutServiceCarUnitPrice, 0)/* *
                  NVL(IfUseOwnVehicle, 0)*/ +
                  NVL(A.OutSubsidyPrice, 0) *
                  nvl(ServiceTripClaimBill.ServiceTripPerson, 0) *
                  nvl(ServiceTripClaimBill.ServiceTripDuration, 0))) FieldServiceExpense,
           -- NVL(TowCharge, 0)+NVL(OtherCost, 0) + NVL(FieldServiceCharge, 0) FieldServiceExpense,
           0 DebitAmount,
           0 ComplementAmount,
          -- NVL(OtherCost, 0) OtherCost,
          0 OtherCost,
           0 OldPartTranseCost,
           ServiceTripClaimBill.Partssalescategoryid,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  ServiceProductLineId) ServiceProductLineId,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From serviceproductlineview v
                    Where v.ProductLineId =
                          ServiceTripClaimBill.ServiceProductLineId)) ProductLineType
      from ServiceTripClaimBill
     inner join branchsupplierrelation a
        on a.branchid = ServiceTripClaimBill.Branchid
       and a.partssalescategoryid =
           ServiceTripClaimBill.Partssalescategoryid
       and a.supplierid = ServiceTripClaimBill.ClaimSupplierId  and a.status<>99
     where ServiceTripClaimBill.CreateTime <= FEndTime
       and (FStartTime is null or
           ServiceTripClaimBill.CreateTime >= FStartTime)
       and SupplierSettleStatus = 2
       and ServiceTripClaimBill.BranchId = FBranchId
      /* and (FSupplierId is null or
           ServiceTripClaimBill.ClaimSupplierId = FSupplierId)*/
       and ServiceTripClaimBill.status = 6
       and (FPartsSalesCategoryId = 0 or ServiceTripClaimBill.PartsSalesCategoryId =
           FPartsSalesCategoryId)
          -- and (FProductLineType is null or ProductLineType=FProductLineType)
       and (FServiceProductLineId is null or FServiceProductLineId = 0 or
           ServiceProductLineId = FServiceProductLineId);



  /*扣补款单*/
  insert into BILLFORSUPPLIERCLAIMSETTLEMENT
    (SourceId,
     SourceCode,
     SourceType,
     SUPPLIERID,
     BranchId,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     OldPartTranseCost,
     Partssalescategoryid,
     ServiceProductLineId,
     ProductLineType)
    select supplierexpenseadjustbill.Id SourceId,
           Code SourceCode,
           25 SourceType,
           supplierexpenseadjustbill.SUPPLIERID,
           supplierexpenseadjustbill.BranchId,
           0 LaborCost,
           0 MaterialCost,
           0 PartsManagementCost,
           0 FieldServiceExpense,
           decode(DebitOrReplenish, 1, TransactionAmount, 0) DebitAmount,
           decode(DebitOrReplenish, 2, -TransactionAmount, 0) ComplementAmount,
           0 OtherCost,
           0 OldPartTranseCost,
           supplierexpenseadjustbill.Partssalescategoryid,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  ServiceProductLineId) ServiceProductLineId,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From serviceproductlineview v
                    Where v.ProductLineId =
                          supplierexpenseadjustbill.ServiceProductLineId)) ProductLineType
      from supplierexpenseadjustbill
      inner join  branchsupplierrelation a
        on a.branchid = supplierexpenseadjustbill.Branchid
       and a.partssalescategoryid =
           supplierexpenseadjustbill.Partssalescategoryid
       and a.supplierid = supplierexpenseadjustbill.SUPPLIERID  and a.status<>99
     where supplierexpenseadjustbill.CreateTime <= FEndTime
       and (FStartTime is null or supplierexpenseadjustbill.CreateTime >= FStartTime)
       and SettlementStatus = 2
       and supplierexpenseadjustbill.BranchId = FBranchId
     /*  and (FSupplierId is null or SUPPLIERID = FSupplierId)*/
       and supplierexpenseadjustbill.status = 2
       and (FPartsSalesCategoryId = 0 or
           supplierexpenseadjustbill.PartsSalesCategoryId = FPartsSalesCategoryId)
          -- and (FProductLineType is null or ProductLineType=FProductLineType)
       and (FServiceProductLineId is null or FServiceProductLineId = 0 or
           ServiceProductLineId = FServiceProductLineId);




declare
  cursor Cursor_role is(

     select
          0 IfInvoiced,
          5 Status,
          regexp_replace(
            regexp_replace(FNewCode, '{CORPCODE}', partssupplier.Code),
            '{SERIAL}',
            lpad(to_char(GetSerial(FComplateId, FDateFormat, partssupplier.Code)),FSerialLength,'0')
          ) Code,
          supplierid,
          partssupplier.Code SupplierCode,
          partssupplier.Name SupplierName,
          FBranchId BranchId,
          FBranchCode BranchCode,
          FBranchName BranchName,
          FStartTime SettlementStartTime,
          FEndTime SettlementEndTime,
          LaborCost,
          MaterialCost,
          PartsManagementCost,
          FieldServiceExpense,
          DebitAmount,
          ComplementAmount,
          OtherCost,
          OldPartTranseCost,
          LaborCost + MaterialCost + PartsManagementCost + FieldServiceExpense + DebitAmount + ComplementAmount +OtherCost+OLDPARTTRANSECOST TotalAmount,
          FCreatorId CreatorId,
          FCreatorName CreatorName,
          sysdate CreateTime,
          PARTSSALESCATEGORYID,
          ServiceProductLineId ,
          ProductLineType
     from (select supplierid,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  sum(MaterialCost) MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OLDPARTTRANSECOST) OLDPARTTRANSECOST,
                  sum(OtherCost) OtherCost
             from BILLFORSUPPLIERCLAIMSETTLEMENT
            group by supplierid, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join partssupplier on partssupplier.Id = t_date.supplierid
       );
begin
  for S_role in Cursor_role loop
    FSupplierClaimSettlementBillId:=S_SupplierClaimSettleBill.Nextval;
    /*主单生成*/
  if s_role.TotalAmount>=0 then
    begin

   update repairclaimbill T
      set T.Suppliersettlestatus = 3
    where T.Id in
          (select SourceId
             from BILLFORSUPPLIERCLAIMSETTLEMENT
            where SourceType in (5, 10, 15)
              and BILLFORSUPPLIERCLAIMSETTLEMENT.supplierid =
                  s_role.supplierid
              and BranchId = s_role.BranchId
              and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                  PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
              and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                  ServiceProductLineId = s_role.ServiceProductLineId));

   update ServiceTripClaimBill
      set SupplierSettleStatus = 3
    where ServiceTripClaimBill.Id in
          (select SourceId
             from BILLFORSUPPLIERCLAIMSETTLEMENT
            where SourceType = 20
              and BILLFORSUPPLIERCLAIMSETTLEMENT.supplierid =
                  s_role.supplierid
              and BranchId = s_role.BranchId
              and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                  PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
              and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                  ServiceProductLineId = s_role.ServiceProductLineId));

   update supplierexpenseadjustbill
      set SettlementStatus = 3
    where supplierexpenseadjustbill.Id in
          (select SourceId
             from BILLFORSUPPLIERCLAIMSETTLEMENT
            where SourceType = 25
              and BILLFORSUPPLIERCLAIMSETTLEMENT.supplierid =
                  s_role.supplierid
              and BranchId = s_role.BranchId
              and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                  PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
              and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                  ServiceProductLineId = s_role.ServiceProductLineId));

  insert into supplierclaimsettlebill
    (Id,
     IfInvoiced,
     Status,
     Code,
     SupplierId,
     SupplierCode,
     SupplierName,
     BranchId,
     BranchCode,
     BranchName,
     SettlementStartTime,
     SettlementEndTime,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     OldPartTranseCost,
     TotalAmount,
     CreatorId,
     CreatorName,
     CreateTime,
     PartsSalesCategoryId,
     ServiceProductLineId,
     ProductLineType)
     values(FSupplierClaimSettlementBillId,
     s_role.IfInvoiced,
     s_role.Status,
     s_role.Code,
     s_role.SupplierID,
     s_role.SupplierCode,
     s_role.SupplierName,
     s_role.BranchId,
     s_role.BranchCode,
     s_role.BranchName,
     s_role.SettlementStartTime,
     s_role.SettlementEndTime,
     s_role.LaborCost,
     s_role.MaterialCost,
     s_role.PartsManagementCost,
     s_role.FieldServiceExpense,
     s_role.DebitAmount,
     s_role.ComplementAmount,
     s_role.OtherCost,
     s_role. OldPartTranseCost,
     s_role.TotalAmount,
     s_role.CreatorId,
     s_role.CreatorName,
     s_role.CreateTime,
     s_role.PartsSalesCategoryId,
     s_role.ServiceProductLineId,
     s_role.ProductLineType);

   /*清单生成*/
      insert into supplierclaimsettledetail    (id,supplierclaimsettlementbillid,sourceid,sourcecode,sourcetype,laborcost,materialcost,partsmanagementcost,fieldserviceexpense,debitamount,complementamount,othercost,OldPartTranseCost,totalamount)
  select S_supplierclaimsettledetail.NEXTVAL,
         FSupplierClaimSettlementBillId,
         BILLFORSUPPLIERCLAIMSETTLEMENT.SourceId,
         BILLFORSUPPLIERCLAIMSETTLEMENT.SourceCode,
         SourceType,
         BILLFORSUPPLIERCLAIMSETTLEMENT.LaborCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.MaterialCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.PartsManagementCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.FieldServiceExpense,
         BILLFORSUPPLIERCLAIMSETTLEMENT.DebitAmount,
         BILLFORSUPPLIERCLAIMSETTLEMENT.ComplementAmount,
         BILLFORSUPPLIERCLAIMSETTLEMENT.OtherCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.OldPartTranseCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.LaborCost + BILLFORSUPPLIERCLAIMSETTLEMENT.MaterialCost + BILLFORSUPPLIERCLAIMSETTLEMENT.PartsManagementCost
         + BILLFORSUPPLIERCLAIMSETTLEMENT.FieldServiceExpense + BILLFORSUPPLIERCLAIMSETTLEMENT.DebitAmount + BILLFORSUPPLIERCLAIMSETTLEMENT.ComplementAmount
         + BILLFORSUPPLIERCLAIMSETTLEMENT.OtherCost+ BILLFORSUPPLIERCLAIMSETTLEMENT.OldPartTranseCost
    from BILLFORSUPPLIERCLAIMSETTLEMENT
   inner join SupplierClaimSettleBill on BILLFORSUPPLIERCLAIMSETTLEMENT.Supplierid = SupplierClaimSettleBill.supplierid
                                   and BILLFORSUPPLIERCLAIMSETTLEMENT.BranchId = SupplierClaimSettleBill.BranchId
                                   And BILLFORSUPPLIERCLAIMSETTLEMENT.PARTSSALESCATEGORYID = SupplierClaimSettleBill.PARTSSALESCATEGORYID
                                   And (BILLFORSUPPLIERCLAIMSETTLEMENT.ServiceProductLineId = SupplierClaimSettleBill.ServiceProductLineId or BILLFORSUPPLIERCLAIMSETTLEMENT.ServiceProductLineId  =0 or
                                   BILLFORSUPPLIERCLAIMSETTLEMENT.ServiceProductLineId is null)
                                   And (BILLFORSUPPLIERCLAIMSETTLEMENT.ProductLineType = SupplierClaimSettleBill.ProductLineType or BILLFORSUPPLIERCLAIMSETTLEMENT.ProductLineType is null
                                   or BILLFORSUPPLIERCLAIMSETTLEMENT.ProductLineType=0)
   where not exists (select * from supplierclaimsettledetail where supplierclaimsettledetail.supplierclaimsettlementbillid = SupplierClaimSettleBill.Id) AND SupplierClaimSettleBill.ID=FSupplierClaimSettlementBillId;



      end;
  /* raise_application_error(-20001, '存在结算汇总单总金额为负数的单据'); */
   end if;

  end loop;
end;

/*
  \*主单生成*\
   FSupplierClaimSettlementBillId:=S_SupplierClaimSettlementBill.Nextval;
  insert into SupplierClaimSettlementBill
    (Id,
     IfInvoiced,
     Status,
     Code,
     DealerId,
     DealerCode,
     DealerName,
     BranchId,
     BranchCode,
     BranchName,
     SettlementStartTime,
     SettlementEndTime,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     CreatorId,
     CreatorName,
     CreateTime,
     PartsSalesCategoryId,
     ServiceProductLineId,
     ProductLineType)
   select FSupplierClaimSettlementBillId,
          0,
          1,
          regexp_replace(
            regexp_replace(FNewCode, '{CORPCODE}', Dealer.Code),
            '{SERIAL}',
            lpad(to_char(GetSerial(FComplateId, FDateFormat, Dealer.Code)),FSerialLength,'0')
          ) Code,
          DealerId,
          Dealer.Code,
          Dealer.Name,
          FBranchId,
          FBranchCode,
          FBranchName,
          FStartTime,
          FEndTime,
          LaborCost,
          MaterialCost,
          PartsManagementCost,
          FieldServiceExpense,
          DebitAmount,
          ComplementAmount,
          OtherCost,
          PreSaleAmount,
          LaborCost + MaterialCost + PartsManagementCost + FieldServiceExpense + DebitAmount + ComplementAmount +OtherCost+PreSaleAmount,
          FCreatorId,
          FCreatorName,
          sysdate,
          PARTSSALESCATEGORYID,
          ServiceProductLineId,
          ProductLineType
     from (select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  sum(MaterialCost) MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(PreSaleAmount) PreSaleAmount
             from BILLFORSUPPLIERCLAIMSETTLEMENT
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join Dealer on Dealer.Id = t_date.DealerId;*/

 select count(*) into GeneratedBillQuantity from (select supplierid,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  sum(MaterialCost) MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(OldPartTranseCost) OldPartTranseCost
             from BILLFORSUPPLIERCLAIMSETTLEMENT
            group by supplierid, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join partssupplier on partssupplier.Id = t_date.supplierid where LaborCost + MaterialCost + PartsManagementCost + FieldServiceExpense + DebitAmount + ComplementAmount +OtherCost>=0;
  --GeneratedBillQuantity:=SQL%ROWCOUNT;

  /*清单生成*/
 /* insert into SsClaimSettlementDetail
    (Id,
     SupplierClaimSettlementBillId,
     SourceId,
     SourceCode,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     SourceType)
  select S_SsClaimSettlementDetail.NEXTVAL,
         FSupplierClaimSettlementBillId,
         BILLFORSUPPLIERCLAIMSETTLEMENT.SourceId,
         BILLFORSUPPLIERCLAIMSETTLEMENT.SourceCode,
         BILLFORSUPPLIERCLAIMSETTLEMENT.LaborCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.MaterialCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.PartsManagementCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.FieldServiceExpense,
         BILLFORSUPPLIERCLAIMSETTLEMENT.DebitAmount,
         BILLFORSUPPLIERCLAIMSETTLEMENT.ComplementAmount,
         BILLFORSUPPLIERCLAIMSETTLEMENT.OtherCost,
         BILLFORSUPPLIERCLAIMSETTLEMENT.PreSaleAmount,
         BILLFORSUPPLIERCLAIMSETTLEMENT.LaborCost + BILLFORSUPPLIERCLAIMSETTLEMENT.MaterialCost + BILLFORSUPPLIERCLAIMSETTLEMENT.PartsManagementCost
         + BILLFORSUPPLIERCLAIMSETTLEMENT.FieldServiceExpense + BILLFORSUPPLIERCLAIMSETTLEMENT.DebitAmount + BILLFORSUPPLIERCLAIMSETTLEMENT.ComplementAmount
         + BILLFORSUPPLIERCLAIMSETTLEMENT.OtherCost+BILLFORSUPPLIERCLAIMSETTLEMENT.PreSaleAmount,
         SourceType
    from BILLFORSUPPLIERCLAIMSETTLEMENT
   inner join SupplierClaimSettlementBill on BILLFORSUPPLIERCLAIMSETTLEMENT.DealerId = SupplierClaimSettlementBill.DealerId
                                   and BILLFORSUPPLIERCLAIMSETTLEMENT.BranchId = SupplierClaimSettlementBill.BranchId
                                   And BILLFORSUPPLIERCLAIMSETTLEMENT.PARTSSALESCATEGORYID = SupplierClaimSettlementBill.PARTSSALESCATEGORYID
                                   And (BILLFORSUPPLIERCLAIMSETTLEMENT.ServiceProductLineId = SupplierClaimSettlementBill.ServiceProductLineId or BILLFORSUPPLIERCLAIMSETTLEMENT.ServiceProductLineId  =0 or
                                   BILLFORSUPPLIERCLAIMSETTLEMENT.ServiceProductLineId is null)
                                   And (BILLFORSUPPLIERCLAIMSETTLEMENT.ProductLineType = SupplierClaimSettlementBill.ProductLineType or BILLFORSUPPLIERCLAIMSETTLEMENT.ProductLineType is null
                                   or BILLFORSUPPLIERCLAIMSETTLEMENT.ProductLineType=0)
   where not exists (select * from SsClaimSettlementDetail where SsClaimSettlementDetail.SupplierClaimSettlementBillid = SupplierClaimSettlementBill.Id) AND SupplierClaimSettlementBill.ID=FSupplierClaimSettlementBillId;
*/
  delete BILLFORSUPPLIERCLAIMSETTLEMENT;
  return(GeneratedBillQuantity);
 --return(1);
end;