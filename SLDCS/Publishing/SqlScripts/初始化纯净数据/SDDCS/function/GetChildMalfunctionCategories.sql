create or replace function GetChildMalfunctionCategories(Id integer) return SYS_REFCURSOR
as
  Result SYS_REFCURSOR;
  my_sql varchar2(4000);
begin
  my_sql := 'with tmp(id, parentid) as( '||
            '  select id, parentid from MalfunctionCategory '||
            '  where id = '||to_char(Id)||
            '  union all '||
            '  select MalfunctionCategory.id, MalfunctionCategory.parentid '||
            '  from MalfunctionCategory '||
            '  inner join tmp on tmp.id = MalfunctionCategory.parentid '||
            ')  cycle id set dup_id TO ''Y'' DEFAULT ''N'' '||
            'select * from MalfunctionCategory '||
            'where exists (select * from tmp where MalfunctionCategory.id = tmp.id) ';
  open Result for my_sql;
  return(Result);
end;