create or replace function GetSerialForDataBase(FTemplateId integer, FCreateDate date, FCorpCode varchar2,DataBasestr varchar2) return integer
as
  PRAGMA AUTONOMOUS_TRANSACTION;
  FId integer;
  FSerial integer;
begin
  if DataBasestr='DCS'then
  begin
    if FCorpCode is null then
      select id,serial into FId,FSerial from DCS.CodeTemplateSerial
       where TemplateId = FTemplateId and CreateDate = FCreateDate and CorpCode is null and rownum<2
         for update;
    else
      select id,serial into FId,FSerial from DCS.CodeTemplateSerial
       where TemplateId = FTemplateId and CreateDate = FCreateDate and CorpCode = FCorpCode and rownum<2
         for update;
    end if;
    FSerial:=FSerial+1;
    UPDATE DCS.CodeTemplateSerial SET Serial = FSerial WHERE Id = FId;
    commit;
  exception
    when NO_DATA_FOUND then
      FSerial:=1;
      INSERT INTO DCS.CodeTemplateSerial(Id ,TemplateId, CreateDate, CorpCode, Serial)
      VALUES(DCS.S_CodeTemplateSerial.Nextval, FTemplateId, FCreateDate, FCorpCode, FSerial);
      commit;
    when others then
      rollback;
      raise;
  end;
  return FSerial;
  end if;

  if DataBasestr='YXDCS'then
  begin
    if FCorpCode is null then
      select id,serial into FId,FSerial from YXDCS.CodeTemplateSerial
       where TemplateId = FTemplateId and CreateDate = FCreateDate and CorpCode is null and rownum<2
         for update;
    else
      select id,serial into FId,FSerial from YXDCS.CodeTemplateSerial
       where TemplateId = FTemplateId and CreateDate = FCreateDate and CorpCode = FCorpCode and rownum<2
         for update;
    end if;
    FSerial:=FSerial+1;
    UPDATE YXDCS.CodeTemplateSerial SET Serial = FSerial WHERE Id = FId;
    commit;
  exception
    when NO_DATA_FOUND then
      FSerial:=1;
      INSERT INTO YXDCS.CodeTemplateSerial(Id ,TemplateId, CreateDate, CorpCode, Serial)
      VALUES(YXDCS.S_CodeTemplateSerial.Nextval, FTemplateId, FCreateDate, FCorpCode, FSerial);
      commit;
    when others then
      rollback;
      raise;
  end;
  return FSerial;
  end if;
  if DataBasestr='GCDCS'then
  begin
    if FCorpCode is null then
      select id,serial into FId,FSerial from GCDCS.CodeTemplateSerial
       where TemplateId = FTemplateId and CreateDate = FCreateDate and CorpCode is null and rownum<2
         for update;
    else
      select id,serial into FId,FSerial from GCDCS.CodeTemplateSerial
       where TemplateId = FTemplateId and CreateDate = FCreateDate and CorpCode = FCorpCode and rownum<2
         for update;
    end if;
    FSerial:=FSerial+1;
    UPDATE GCDCS.CodeTemplateSerial SET Serial = FSerial WHERE Id = FId;
    commit;
  exception
    when NO_DATA_FOUND then
      FSerial:=1;
      INSERT INTO GCDCS.CodeTemplateSerial(Id ,TemplateId, CreateDate, CorpCode, Serial)
      VALUES(GCDCS.S_CodeTemplateSerial.Nextval, FTemplateId, FCreateDate, FCorpCode, FSerial);
      commit;
    when others then
      rollback;
      raise;
  end;
  return FSerial;
  end if;

   if DataBasestr='SDDCS'then
  begin
    if FCorpCode is null then
      select id,serial into FId,FSerial from SDDCS.CodeTemplateSerial
       where TemplateId = FTemplateId and CreateDate = FCreateDate and CorpCode is null and rownum<2
         for update;
    else
      select id,serial into FId,FSerial from SDDCS.CodeTemplateSerial
       where TemplateId = FTemplateId and CreateDate = FCreateDate and CorpCode = FCorpCode and rownum<2
         for update;
    end if;
    FSerial:=FSerial+1;
    UPDATE SDDCS.CodeTemplateSerial SET Serial = FSerial WHERE Id = FId;
    commit;
  exception
    when NO_DATA_FOUND then
      FSerial:=1;
      INSERT INTO SDDCS.CodeTemplateSerial(Id ,TemplateId, CreateDate, CorpCode, Serial)
      VALUES(SDDCS.S_CodeTemplateSerial.Nextval, FTemplateId, FCreateDate, FCorpCode, FSerial);
      commit;
    when others then
      rollback;
      raise;
  end;
  return FSerial;
  end if;
end;