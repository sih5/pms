
  
BEGIN 
dbms_scheduler.create_job('"AUTOSETACCOUNT_JOB"',
job_type=>'STORED_PROCEDURE', job_action=>
'AutoSetAccount'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('03-DEC-2015 11.00.00.000000000 PM +08:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=> 
'Freq=Daily'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.enable('"AUTOSETACCOUNT_JOB"');
COMMIT; 
END; 
