
  
BEGIN 
dbms_scheduler.create_job('"MOVEINVALIDPARTSEXGROUP_JOB"',
job_type=>'STORED_PROCEDURE', job_action=>
'PRO_MoveInvalidPartsExGroup'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('20-MAY-2017 10.00.00.000000000 PM +08:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=> 
'Freq=Daily;Interval=1'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'每天22点定时删除符合条件的互换组信息'
);
COMMIT; 
END; 
