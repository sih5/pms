
  
BEGIN 
dbms_scheduler.create_job('"PARTSSALESORDERCENNEL_JOB"',
job_type=>'STORED_PROCEDURE', job_action=>
'PartsSalesOrderCennel'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('05-NOV-2015 09.37.59.000000000 PM +08:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=> 
'FREQ=Daily;BYHOUR=3;BYMINUTE=0;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'根据品牌策略自动作废配件销售订单'
);
dbms_scheduler.enable('"PARTSSALESORDERCENNEL_JOB"');
COMMIT; 
END; 
