
  
BEGIN 
dbms_scheduler.create_job('"SAPPAYMENTINFO_JOB"',
job_type=>'STORED_PROCEDURE', job_action=>
'Prc_SapPaymentInfo'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('23-NOV-2016 07.58.05.000000000 PM +08:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=> 
'Freq=MONTHLY;ByMonthDay=1;ByHour=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'每月1号自动清理三月前的SAP收付款接口日志'
);
COMMIT; 
END; 
