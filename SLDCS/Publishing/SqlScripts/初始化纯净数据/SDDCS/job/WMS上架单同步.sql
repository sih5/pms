
  
BEGIN 
dbms_scheduler.create_job('"WMS上架单同步"',
job_type=>'STORED_PROCEDURE', job_action=>
'sddcs.syncwmsinout.Synctoshelve'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('13-MAY-2015 07.00.00.000000000 PM +08:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=> 
'Freq=Minutely;Interval=10;ByHour=02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.enable('"WMS上架单同步"');
COMMIT; 
END; 
