
  
BEGIN 
dbms_scheduler.create_job('"内部领入自动设置入库状态12点"',
job_type=>'STORED_PROCEDURE', job_action=>
'AutoSetICBinstatus_n'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('07-APR-2015 12.20.00.000000000 PM +08:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=> 
'Freq=Daily;Interval=1'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.enable('"内部领入自动设置入库状态12点"');
COMMIT; 
END; 
