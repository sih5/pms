
  
BEGIN 
dbms_scheduler.create_job('"每月自动结转客户账户26日"',
job_type=>'STORED_PROCEDURE', job_action=>
'EffectCustomerAccountHistory'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('26-SEP-2015 12.00.00.000000000 AM +08:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=> 
'Freq=Monthly;ByMonthDay=26'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.enable('"每月自动结转客户账户26日"');
COMMIT; 
END; 
