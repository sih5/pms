
  
BEGIN 
dbms_scheduler.create_job('"自动失效信用申请"',
job_type=>'STORED_PROCEDURE', job_action=>
'autoexpirecredit'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('07-AUG-2014 11.50.00.000000000 PM +08:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=> 
'Freq=Daily;Interval=1'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.enable('"自动失效信用申请"');
COMMIT; 
END; 
