
  
BEGIN 
dbms_scheduler.create_job('"自动审核紧急销售订单"',
job_type=>'STORED_PROCEDURE', job_action=>
'AutoCheckUrgentPartsOrders'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('09-AUG-2016 06.00.00.000000000 PM +08:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=> 
'Freq=Daily;Interval=1'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
COMMIT; 
END; 
