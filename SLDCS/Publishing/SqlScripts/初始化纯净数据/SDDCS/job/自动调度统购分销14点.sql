
  
BEGIN 
dbms_scheduler.create_job('"自动调度统购分销14点"',
job_type=>'STORED_PROCEDURE', job_action=>
'AutoFinishPurDistribution'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('15-JUN-2015 02.00.00.000000000 PM +08:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=> 
'Freq=Daily;Interval=1'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.enable('"自动调度统购分销14点"');
COMMIT; 
END; 
