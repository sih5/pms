
  
BEGIN 
dbms_scheduler.create_job('"采购退货自动更新出库状态12点"',
job_type=>'STORED_PROCEDURE', job_action=>
'AutoSetICBoutstatus_n'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('23-JUL-2015 12.20.00.000000000 PM +08:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=> 
'Freq=Daily;Interval=1'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.enable('"采购退货自动更新出库状态12点"');
COMMIT; 
END; 
