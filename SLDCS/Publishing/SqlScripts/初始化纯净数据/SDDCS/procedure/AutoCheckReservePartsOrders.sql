create or replace procedure AutoCheckReservePartsOrders is
  num            number;
  nRow           number;
  i              number;
  j              number;

  nOrderId       Number(9);
cursor ReservePartsOrders is(
    select id
      from PartsSalesOrder a
     where (a.status in (2, 4))
       and a.Partssalesordertypeid in
           (select id
              from PartsSalesOrderType
             where name in ('储备订单', '专项订单')
               and status = 1)
       and (SalesCategoryName <> '电子商务')
       and exists
     (select *
              from OrderApproveWeekday d
             inner join keyvalueitem e
                on e.caption = '储备订单审核日'
               and e.key = d.weeksname
               and e.value = (select to_char(sysdate, 'day','NLS_DATE_LANGUAGE=''SIMPLIFIED CHINESE''') from dual)
             where d.PartsSalesCategoryId = a.salescategoryid)
       and exists
     (select *
              from Partssalesorderdetail b, PartsBranch c
             where c.PartsSalesCategoryId = a.SalesCategoryId
               and c.partid = b.sparepartid
               and a.id = b.partssalesorderid
               and c.status = 1
               and c.AutoApproveUpLimit >=
                   (b.orderedquantity - nvl(b.approvequantity, 0))));
begin
  for s_ReservePartsOrders in ReservePartsOrders loop
  begin
    nOrderId:= s_ReservePartsOrders.Id;
    AutoCheckNoEcommerceOrder(nOrderId);
    /*2017-8-15 hr add 增加commit
    若存储过程AutoCheckNoEcommerceOrder中未commit时，tmp临时表没有清空
    导致下一次循环中，读取的是上一个tmp临时表中的数据*/
    commit;

  end;
  end loop;

end AutoCheckReservePartsOrders;
