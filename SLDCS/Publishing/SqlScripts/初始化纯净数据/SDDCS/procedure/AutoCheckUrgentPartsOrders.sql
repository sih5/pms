create or replace procedure AutoCheckUrgentPartsOrders is
  num            number;
  nRow           number;
  i              number;
  j              number;

  nOrderId       Number(9);
cursor UrgentPartsOrders is(
   select id
     from PartsSalesOrder a
    where (a.status in (2, 4))
      and a.Partssalesordertypeid in
          (select id
             from PartsSalesOrderType
            where status = 1
              and (name = '急需订单' or name = 'vor订单' or name = 'VOR订单'))
      and (SalesCategoryName <> '电子商务')
      and exists
    (select *
             from Partssalesorderdetail b, PartsBranch c
            where c.PartsSalesCategoryId = a.SalesCategoryId
              and c.partid = b.sparepartid
              and a.id = b.partssalesorderid
              and c.status = 1
              and c.AutoApproveUpLimit >=
                  (b.orderedquantity - nvl(b.approvequantity, 0))));
begin

  for s_UrgentPartsOrders in UrgentPartsOrders loop
  begin
    nOrderId:= s_UrgentPartsOrders.Id;
    AutoCheckNoEcommerceOrder(nOrderId);
    /*2017-8-15 hr add 增加commit
    若存储过程AutoCheckNoEcommerceOrder中未commit时，tmp临时表没有清空
    导致下一次循环中，读取的是上一个tmp临时表中的数据*/
    commit;

  end;
  end loop;

end AutoCheckUrgentPartsOrders;
