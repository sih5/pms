create or replace procedure AutoExpireCredit is
 cursor tmpCursor is
    select bs.* from CredenceApplication bs
    inner join Branchstrategy  on  bs.salescompanyid=Branchstrategy.Branchid
    where to_char(bs.expiredate,'yyyy-mm-dd') =
to_char(sysdate,'yyyy-mm-dd') and bs.status=3 and Branchstrategy.IsResetCreditStrategy=2;

  tmpCursorRow  tmpCursor%rowtype;

begin
  open tmpCursor;
    loop

      fetch tmpCursor into tmpCursorRow;
      exit when tmpCursor%notfound;
      update CredenceApplication
         set status = 4
       where CredenceApplication.id = tmpCursorRow.Id;
       update CustomerAccount t
          set CustomerCredenceAmount = 0
        where t.customercompanyid = tmpCursorRow.Customercompanyid
          and t.accountgroupid = tmpCursorRow.Accountgroupid;
   end loop;
 close tmpCursor;
end AutoExpireCredit;