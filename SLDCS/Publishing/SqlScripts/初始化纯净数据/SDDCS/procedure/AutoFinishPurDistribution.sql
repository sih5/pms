create or replace procedure AutoFinishPurDistribution is
  type outboundbilldetailset IS TABLE OF partsoutboundbilldetail%rowtype INDEX BY binary_integer;
  Vtable_outboundbilldetail outboundbilldetailset;
  datebase                  varchar2(100);
  FMyException EXCEPTION;
  rno                      number(9);
  Fpartssalescategoryid    number(9);
  Fpartssalescategoryname  varchar2(100);
  FNewCode                 varchar(100);
  FComplateId              NUMBER(9);
  NEWCODE                  VARCHAR2(50);
  NEWSUPCODE               VARCHAR2(50);
  NEWIPCODE                VARCHAR2(50);
  parentid                 number(9);
  FErrormsg                VARCHAR2(1000);
  vi                       number(9);
  outboundbilldetailno     number(9);
  FNewPurOrderId           number(9);
  FPurId                   number(9);
  FSupShipId               number(9);
  FIPId                    NUMBER(9);
  FWAREHOUSEID             NUMBER(9);
  FRECEIVINGWAREHOUSEID    NUMBER(9);
  FRECEIVINGCOMPANYID      NUMBER(9);
  FCUSTOMERACCOUNTID       NUMBER(9);
  Fpartssalesordertypeid   NUMBER(9);
  Fpartssalesordertypename VARCHAR2(50);
  FbranchId                number(9);
  FbranchCode              varchar2(50);
  FReceivingAddress        VARCHAR2(200);
  FICId                    NUMBER(9);
  FPMId                    NUMBER(9);
  FOPId                    NUMBER(9);
  FOBId                    number(9);
  FPSId                    NUMBER(9);
  FPLBId                   NUMBER(9);
  NEWOPCODE                VARCHAR2(50);
  NEWICCODE                VARCHAR2(50);
  NEWPMCODE                VARCHAR2(50);
  NEWOBCODE                VARCHAR2(50);
  NEWPSCODE                VARCHAR2(50);
  pendingamount            NUMBER(19, 4);
  releaseamount            number(19, 4);
  FCheckWarehouseAreaId    number(9);
  FStoreWarehouseAreaId    number(9);
  Fstdprice                NUMBER(19, 4);
  Fsalesprice              NUMBER(19, 4);
  cursor_name              INTEGER := dbms_sql.open_cursor;
  cursor outboundbill is(
    select partsoutboundbill.*
      from yxdcs.partsoutboundbill
     inner join branch
        on branch.code = partsoutboundbill.counterpartcompanycode
     where /*exists
     (select 1
              from yxdcs.warehouse
             where warehouse.id = partsoutboundbill.warehouseid
               and warehouse.wmsinterface = 0)
       and */((PurDistributionstatus = 1) or (PurDistributionstatus is null))
       and exists (select 1
              from yxdcs.partsoutboundplan t
             where t.id = partsoutboundbill.partsoutboundplanid
               and t.isPurDistribution = 1)
       and branch.status <> 99);
  cursor outboundbilldetail(FParentIds number) is(
    select partsoutboundbilldetail.*,
           row_number() over(order by partsoutboundbillid ASC) rno
      from yxdcs.partsoutboundbilldetail
     where partsoutboundbilldetail.partsoutboundbillid = FParentIds);
begin
  Vtable_outboundbilldetail.delete;
  for s_outboundbill in outboundbill loop
    begin
      /*begin
        select datebase
          into datebase
          from BranchDateBaseRel
         where branchcode = s_outboundbill.COUNTERPARTCOMPANYCODE;
      exception
        when no_data_found then
          begin
            FErrormsg := '对应企业与数据库关系不存在';
            raise FMyException;
          end;
      end;*/
      --取出库单id
      releaseamount := 0;
      rno:=0;
      parentid      := s_outboundbill.id;
      --生成采购订单编号，id
      FNewCode := 'PPO{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') ||
                  '{SERIAL}';
      BEGIN
        SELECT Id
          INTO FComplateId
          FROM CodeTemplate
         WHERE name = 'PartsPurchaseOrder'
           AND IsActived = 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          FErrormsg := '未找到名称为"PartsPurchaseOrder"的有效编码规则。';
          raise FMyException;
        WHEN OTHERS THEN
          raise;
      END;
      BEGIN
        select SalesCategoryId,
               SalesCategoryName,
               warehouseid,
               SalesUnitOwnerCompanyId,
               ReceivingAddress,
               (select code
                  from company
                 where company.id = SalesUnitOwnerCompanyId),
               RECEIVINGWAREHOUSEID,
               RECEIVINGCOMPANYID,
               CUSTOMERACCOUNTID,
               partssalesordertypeid,
               partssalesordertypename
          into Fpartssalescategoryid,
               Fpartssalescategoryname,
               FWAREHOUSEID,
               FbranchId,
               FReceivingAddress,
               FbranchCode,
               FRECEIVINGWAREHOUSEID,
               FRECEIVINGCOMPANYID,
               FCUSTOMERACCOUNTID,
               fpartssalesordertypeid,
               fpartssalesordertypename
          from partssalesorder
         where partssalesorder.code =
               s_outboundbill.OriginalRequirementBillCode;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          FErrormsg := '原始单据不存在';
          raise FMyException;
      END;
      FNewPurOrderId := s_partspurchaseorder.nextval;
      NEWCODE        := regexp_replace(regexp_replace(FNewCode,
                                                      '{CORPCODE}',
                                                      FbranchCode),
                                       '{SERIAL}',
                                       lpad(TO_CHAR(GetSerial(FComplateId,
                                                              TRUNC(sysdate,
                                                                    'DD'),
                                                              FbranchCode)),
                                            6,
                                            '0'));

      FPurId := s_partspurchaseorder.nextval;
      --生成采购订单
      insert into partspurchaseorder
        (id,
         code,
         branchid,
         branchcode,
         branchname,
         warehouseid,
         warehousename,
         partssalescategoryid,
         partssalescategoryname,
         receivingcompanyid, --branchid
         receivingcompanyname,
         receivingaddress,
         partssupplierid,
         partssuppliercode,
         partssuppliername,
         ifdirectprovision, --否
         originalrequirementbillid,
         originalrequirementbilltype,
         originalrequirementbillcode,
         totalamount,
         partspurchaseordertypeid, --紧急
         requesteddeliverytime, --sysdate
         shippingmethod, --出库单发运方式
         plansource,
         status,
         creatorid,
         creatorname,
         createtime)
      values
        (FPurId,
         NEWCODE,
         (select id
            from company
           where code = s_outboundbill.CounterpartCompanyCode),
         s_outboundbill.CounterpartCompanyCode,
         s_outboundbill.CounterpartCompanyName,
         FWAREHOUSEID,
         (SELECT name FROM WAREHOUSE WHERE id = FWAREHOUSEID),
         Fpartssalescategoryid,
         Fpartssalescategoryname,
         s_outboundbill.CounterpartCompanyId,
         s_outboundbill.CounterpartCompanyName,
         (select ReceivingAddress
            from partssalesorder
           where partssalesorder.code =
                 s_outboundbill.OriginalRequirementBillCode),
         (select id from company where code = '1101'),
         '1101',
         '北汽福田汽车股份有限公司北京配件销售分公司',
         0,
         s_outboundbill.originalrequirementbillid,
         s_outboundbill.originalrequirementbilltype,
         s_outboundbill.originalrequirementbillcode,
         (select sum(t.outboundamount * t.settlementprice)
            from yxdcs.partsoutboundbilldetail t
           where t.partsoutboundbillid = parentid),
         (select partspurchaseordertype.id
            from partspurchaseordertype
           inner join partssalesorder
              on partssalesorder.salescategoryid =
                 partspurchaseordertype.partssalescategoryid
           where partssalesorder.code =
                 s_outboundbill.OriginalRequirementBillCode
             and partspurchaseordertype.name = '紧急采购订单'),
         sysdate,
         s_outboundbill.shippingmethod,
         '',
         6,
         1,
         'Admin',
         sysdate);
      --生成供应商发运单编号，id
      FNewCode := 'PSS{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') ||
                  '{SERIAL}';
      BEGIN
        SELECT Id
          INTO FComplateId
          FROM CodeTemplate
         WHERE name = 'SupplierShippingOrder'
           AND IsActived = 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          FErrormsg := '未找到名称为"SupplierShippingOrder"的有效编码规则。';
          raise FMyException;
        WHEN OTHERS THEN
          raise;
      END;
      NEWSUPCODE := regexp_replace(regexp_replace(FNewCode,
                                                  '{CORPCODE}',
                                                  FbranchCode),
                                   '{SERIAL}',
                                   lpad(TO_CHAR(GetSerial(FComplateId,
                                                          TRUNC(sysdate, 'DD'),
                                                          FbranchCode)),
                                        6,
                                        '0'));
      FSupShipId := s_suppliershippingorder.nextval;
      --生成供应商发运单
      insert into suppliershippingorder
        (id,
         code,
         partssupplierid,
         partssuppliercode,
         partssuppliername,
         ifdirectprovision,
         branchid,
         branchcode,
         branchname,
         partspurchaseorderid,
         partssalescategoryid,
         partssalescategoryname,
         partspurchaseordercode,
         originalrequirementbillid,
         originalrequirementbilltype,
         originalrequirementbillcode,
         receivingwarehouseid,
         receivingwarehousename,
         receivingcompanyid,
         receivingcompanyname,
         receivingcompanycode,
         receivingaddress,
         shippingmethod,
         status,
         shippingdate,
         arrivaldate,
         requesteddeliverytime,
         plandeliverytime,
         creatorid,
         creatorname,
         createtime)
      values
        (FSupShipId,
         NEWSUPCODE,
         (select id from company where code = '1101'),
         '1101',
         '北汽福田汽车股份有限公司北京配件销售分公司',
         0,
         FbranchId,
         (select code from company where company.id = FbranchId),
         (select name from company where company.id = FbranchId),
         FPurId,
         Fpartssalescategoryid,
         Fpartssalescategoryname,
         NEWCODE,
         s_outboundbill.originalrequirementbillid,
         s_outboundbill.originalrequirementbilltype,
         s_outboundbill.originalrequirementbillcode,
         FWAREHOUSEID,
         (SELECT name FROM WAREHOUSE WHERE id = FWAREHOUSEID),
         FbranchId,
         (select code from company where company.id = FbranchId),
         (select name from company where company.id = FbranchId),
         FReceivingAddress,
         s_outboundbill.shippingmethod,
         2,
         sysdate,
         sysdate,
         sysdate,
         sysdate,
         1,
         'Admin',
         sysdate);
      --生成入库计划单编号，id
      FIPId    := s_partsinboundplan.nextval;
      FNewCode := 'PIP{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') ||
                  '{SERIAL}';
      BEGIN
        SELECT Id
          INTO FComplateId
          FROM CodeTemplate
         WHERE name = 'PartsInboundPlan'
           AND IsActived = 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          FErrormsg := '未找到名称为"PartsInboundPlan"的有效编码规则。';
          raise FMyException;
        WHEN OTHERS THEN
          raise;
      END;
      NEWIPCODE := regexp_replace(regexp_replace(FNewCode,
                                                 '{CORPCODE}',
                                                 FbranchCode),
                                  '{SERIAL}',
                                  lpad(TO_CHAR(GetSerial(FComplateId,
                                                         TRUNC(sysdate, 'DD'),
                                                         FbranchCode)),
                                       6,
                                       '0'));
      --生成入库计划单
      insert into partsinboundplan
        (id,
         code,
         warehouseid,
         warehousecode,
         warehousename,
         storagecompanyid,
         storagecompanycode,
         storagecompanyname,
         storagecompanytype,
         branchid,
         branchcode,
         branchname,
         partssalescategoryid,
         counterpartcompanyid,
         counterpartcompanycode,
         counterpartcompanyname,
         sourceid,
         sourcecode,
         inboundtype,
         customeraccountid,
         originalrequirementbillid,
         originalrequirementbilltype,
         originalrequirementbillcode,
         status,
         arrivaldate,
         ifwmsinterface,
         creatorid,
         creatorname,
         createtime)
      values
        (FIPId,
         NEWIPCODE,
         FWAREHOUSEID,
         (SELECT CODE FROM WAREHOUSE WHERE id = FWAREHOUSEID),
         (SELECT name FROM WAREHOUSE WHERE id = FWAREHOUSEID),

         FbranchId,
         (select code from company where company.id = FbranchId),
         (select name from company where company.id = FbranchId),
         (select type from company where company.id = FbranchId),
         FbranchId,
         (select code from company where company.id = FbranchId),
         (select name from company where company.id = FbranchId),
         Fpartssalescategoryid,

         (select id from company where code = '1101'),
         '1101',
         '北汽福田汽车股份有限公司北京配件销售分公司',
         FSupShipId,
         NEWSUPCODE,
         1,
         (select supplieraccount.id
            from supplieraccount
           inner join company suppliercompany
              on suppliercompany.id = supplieraccount.suppliercompanyid
           where suppliercompany.code = '1101'
             and supplieraccount.buyercompanyid = FbranchId
             and supplieraccount.partssalescategoryid =
                 Fpartssalescategoryid),
         s_outboundbill.originalrequirementbillid,
         s_outboundbill.originalrequirementbilltype,
         s_outboundbill.originalrequirementbillcode,
         2,
         sysdate,
         0,
         1,
         'Admin',
         sysdate);
      --生成入库检验单编号，id
      FICId    := S_PARTSINBOUNDCHECKBILL.NEXTVAL;
      FNewCode := 'RK{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') ||
                  '{SERIAL}';
      BEGIN
        SELECT Id
          INTO FComplateId
          FROM CodeTemplate
         WHERE name = 'PartsInboundCheckBill'
           AND IsActived = 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          FErrormsg := '未找到名称为"PartsInboundCheckBill"的有效编码规则。';
          raise FMyException;
        WHEN OTHERS THEN
          raise;
      END;
      NEWICCODE := regexp_replace(regexp_replace(FNewCode,
                                                 '{CORPCODE}',
                                                 FbranchCode),
                                  '{SERIAL}',
                                  lpad(TO_CHAR(GetSerial(FComplateId,
                                                         TRUNC(sysdate, 'DD'),
                                                         FbranchCode)),
                                       6,
                                       '0'));
      --生成入库检验单
      insert into partsinboundcheckbill
        (id,
         code,
         partsinboundplanid,
         warehouseid,
         warehousecode,
         warehousename,
         storagecompanyid,
         storagecompanycode,
         storagecompanyname,
         storagecompanytype,
         partssalescategoryid,
         branchid,
         branchcode,
         branchname,
         counterpartcompanyid,
         counterpartcompanycode,
         counterpartcompanyname,
         inboundtype,
         customeraccountid,
         originalrequirementbillid,
         originalrequirementbilltype,
         originalrequirementbillcode,
         status,
         settlementstatus,
         creatorid,
         creatorname,
         createtime)
      values
        (FICId,
         NEWICCODE,
         FIPId,
         FWAREHOUSEID,
         (SELECT CODE FROM WAREHOUSE WHERE id = FWAREHOUSEID),
         (SELECT name FROM WAREHOUSE WHERE id = FWAREHOUSEID),
         FbranchId,
         (select code from company where company.id = FbranchId),
         (select name from company where company.id = FbranchId),
         (select type from company where company.id = FbranchId),
         Fpartssalescategoryid,
         FbranchId,
         (select code from company where company.id = FbranchId),
         (select name from company where company.id = FbranchId),
         (select id from company where code = '1101'),
         '1101',
         '北汽福田汽车股份有限公司北京配件销售分公司',
         1,
         (select supplieraccount.id
            from supplieraccount
           inner join company suppliercompany
              on suppliercompany.id = supplieraccount.suppliercompanyid
           where suppliercompany.code = '1101'
             and supplieraccount.buyercompanyid = FbranchId
             and supplieraccount.partssalescategoryid =
                 Fpartssalescategoryid),
         s_outboundbill.originalrequirementbillid,
         s_outboundbill.originalrequirementbilltype,
         s_outboundbill.originalrequirementbillcode,
         3,
         2,
         1,
         'Admin',
         sysdate);
      --获取检验区随即库位
      begin
        select warehousearea.id
          into FCheckWarehouseAreaId
          from warehousearea
         inner join warehouseareacategory
            on warehousearea.areacategoryid = warehouseareacategory.id
         where warehouseid = FWAREHOUSEID
           and warehouseareacategory.category = 3
           and warehousearea.areakind = 3
           and rownum = 1;
      exception
        when no_data_found then
          begin
            FErrormsg := '入库仓库不存在检验区库位';
            raise FMyException;
          end;
      end;
      --获取保管区随即库位
      begin
        select warehousearea.id
          into FStoreWarehouseAreaId
          from warehousearea
         inner join warehouseareacategory
            on warehousearea.areacategoryid = warehouseareacategory.id
         where warehouseid = FWAREHOUSEID
           and warehouseareacategory.category = 1
           and warehousearea.areakind = 3
           and rownum = 1;
      exception
        when no_data_found then
          begin
            FErrormsg := '入库仓库不存在保管区库位';
            raise FMyException;
          end;
      end;
      --生成上架单编号id
      FPMId    := S_PARTSSHIFTORDER.NEXTVAL;
      FNewCode := 'PM{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') ||
                  '{SERIAL}';
      BEGIN
        SELECT Id
          INTO FComplateId
          FROM CodeTemplate
         WHERE name = 'PartsShiftOrder'
           AND IsActived = 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          FErrormsg := '未找到名称为"PartsShiftOrder"的有效编码规则。';
          raise FMyException;
        WHEN OTHERS THEN
          raise;
      END;
      NEWPMCODE := regexp_replace(regexp_replace(FNewCode,
                                                 '{CORPCODE}',
                                                 FbranchCode),
                                  '{SERIAL}',
                                  lpad(TO_CHAR(GetSerial(FComplateId,
                                                         TRUNC(sysdate, 'DD'),
                                                         FbranchCode)),
                                       6,
                                       '0'));
      --生成上架单
      INSERT INTO PARTSSHIFTORDER
        (ID,
         CODE,
         BRANCHID,
         WAREHOUSEID,
         WAREHOUSECODE,
         WAREHOUSENAME,
         STORAGECOMPANYID,
         STORAGECOMPANYCODE,
         STORAGECOMPANYNAME,
         STORAGECOMPANYTYPE,
         STATUS,
         TYPE,
         CREATORID,
         CREATORNAME,
         CREATETIME)
      values
        (FPMId,
         NEWPMCODE,
         FbranchId,
         FWAREHOUSEID,
         (SELECT CODE FROM WAREHOUSE WHERE id = FWAREHOUSEID),
         (SELECT name FROM WAREHOUSE WHERE id = FWAREHOUSEID),
         FbranchId,
         (select code from company where company.id = FbranchId),
         (select name from company where company.id = FbranchId),
         (select type from company where company.id = FbranchId),
         1,
         1,
         1,
         'Admin',
         sysdate);
      --生成出库计划单id编号
      FOPId    := S_PARTSOUTBOUNDPLAN.NEXTVAL;
      FNewCode := 'POP{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') ||
                  '{SERIAL}';
      BEGIN
        SELECT Id
          INTO FComplateId
          FROM CodeTemplate
         WHERE name = 'PartsOutboundPlan'
           AND IsActived = 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          FErrormsg := '未找到名称为"PartsOutboundPlan"的有效编码规则。';
          raise FMyException;
        WHEN OTHERS THEN
          raise;
      END;

      NEWOPCODE := regexp_replace(regexp_replace(FNewCode,
                                                 '{CORPCODE}',
                                                 FbranchCode),
                                  '{SERIAL}',
                                  lpad(TO_CHAR(GetSerial(FComplateId,
                                                         TRUNC(sysdate, 'DD'),
                                                         FbranchCode)),
                                       6,
                                       '0'));
      --生成出库计划单
      insert into partsoutboundplan
        (id,
         code,
         warehouseid,
         warehousecode,
         warehousename,
         storagecompanyid,
         storagecompanycode,
         storagecompanyname,
         storagecompanytype,
         branchid,
         branchcode,
         branchname,
         partssalescategoryid,
         partssalesordertypeid,
         partssalesordertypename,
         counterpartcompanyid,
         counterpartcompanycode,
         counterpartcompanyname,
         receivingcompanyid,
         receivingcompanycode,
         receivingcompanyname,
         receivingwarehouseid,
         receivingwarehousecode,
         receivingwarehousename,
         sourceid,
         sourcecode,
         outboundtype,
         shippingmethod,
         customeraccountid,
         originalrequirementbillid,
         originalrequirementbilltype,
         originalrequirementbillcode,
         status,
         ifwmsinterface,
         creatorid,
         creatorname,
         createtime)
      values
        (FOPId,
         NEWOPCODE,
         FWAREHOUSEID,
         (SELECT CODE FROM WAREHOUSE WHERE id = FWAREHOUSEID),
         (SELECT name FROM WAREHOUSE WHERE id = FWAREHOUSEID),
         FbranchId,
         (select code from company where company.id = FbranchId),
         (select name from company where company.id = FbranchId),
         (select type from company where company.id = FbranchId),
         FbranchId,
         (select code from company where company.id = FbranchId),
         (select name from company where company.id = FbranchId),
         Fpartssalescategoryid,
         Fpartssalesordertypeid,
         Fpartssalesordertypename,
         FRECEIVINGCOMPANYID,
         (select code from company where company.id = FRECEIVINGCOMPANYID),
         (select name from company where company.id = FRECEIVINGCOMPANYID),
         FRECEIVINGCOMPANYID,
         (select code from company where company.id = FRECEIVINGCOMPANYID),
         (select name from company where company.id = FRECEIVINGCOMPANYID),
         FRECEIVINGWAREHOUSEID,
         (SELECT CODE FROM WAREHOUSE WHERE id = FRECEIVINGWAREHOUSEID),
         (SELECT name FROM WAREHOUSE WHERE id = FRECEIVINGWAREHOUSEID),
         s_outboundbill.originalrequirementbillid,
         s_outboundbill.originalrequirementbillcode,
         1,
         s_outboundbill.shippingmethod,
         FCUSTOMERACCOUNTID,
         s_outboundbill.originalrequirementbillid,
         s_outboundbill.originalrequirementbilltype,
         s_outboundbill.originalrequirementbillcode,
         3,
         0,
         1,
         'Admin',
         sysdate);
      --生成出库单id编号
      FOBId    := s_partsoutboundbill.NEXTVAL;
      FNewCode := 'CK{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') ||
                  '{SERIAL}';
      BEGIN
        SELECT Id
          INTO FComplateId
          FROM CodeTemplate
         WHERE name = 'PartsOutboundBill'
           AND IsActived = 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          FErrormsg := '未找到名称为"PartsOutboundBill"的有效编码规则。';
          raise FMyException;
        WHEN OTHERS THEN
          raise;
      END;
      NEWobCODE := regexp_replace(regexp_replace(FNewCode,
                                                 '{CORPCODE}',
                                                 FbranchCode),
                                  '{SERIAL}',
                                  lpad(TO_CHAR(GetSerial(FComplateId,
                                                         TRUNC(sysdate, 'DD'),
                                                         FbranchCode)),
                                       6,
                                       '0'));
      --生成出库单
      insert into PartsOutboundBill
        (id,
         Code,
         Partsoutboundplanid,
         Warehouseid,
         Warehousecode,
         Warehousename,
         Storagecompanyid,
         Storagecompanycode,
         Storagecompanyname,
         Storagecompanytype,
         Branchid,
         Branchcode,
         Branchname,
         Partssalescategoryid,
         Partssalesordertypeid,
         Partssalesordertypename,
         Counterpartcompanyid,
         Counterpartcompanycode,
         Counterpartcompanyname,
         Receivingcompanyid,
         Receivingcompanycode,
         Receivingcompanyname,
         Receivingwarehouseid,
         Receivingwarehousecode,
         Receivingwarehousename,
         Outboundtype,
         Shippingmethod,
         Customeraccountid,
         Originalrequirementbillid,
         Originalrequirementbilltype,
         Originalrequirementbillcode,
         Settlementstatus,
         Creatorid,
         Creatorname,
         Createtime)
      values
        (FOBId,
         NEWOBCODE,
         FOPId,
         FWAREHOUSEID,
         (SELECT CODE FROM WAREHOUSE WHERE id = FWAREHOUSEID),
         (SELECT name FROM WAREHOUSE WHERE id = FWAREHOUSEID),
         FbranchId,
         (select code from company where company.id = FbranchId),
         (select name from company where company.id = FbranchId),
         (select type from company where company.id = FbranchId),
         FbranchId,
         (select code from company where company.id = FbranchId),
         (select name from company where company.id = FbranchId),
         Fpartssalescategoryid,
         Fpartssalesordertypeid,
         Fpartssalesordertypename,
         FRECEIVINGCOMPANYID,
         (select code from company where company.id = FRECEIVINGCOMPANYID),
         (select name from company where company.id = FRECEIVINGCOMPANYID),
         FRECEIVINGCOMPANYID,
         (select code from company where company.id = FRECEIVINGCOMPANYID),
         (select name from company where company.id = FRECEIVINGCOMPANYID),
         FRECEIVINGWAREHOUSEID,
         (SELECT CODE FROM WAREHOUSE WHERE id = FRECEIVINGWAREHOUSEID),
         (SELECT name FROM WAREHOUSE WHERE id = FRECEIVINGWAREHOUSEID),
         1,
         s_outboundbill.shippingmethod,
         FCUSTOMERACCOUNTID,
         s_outboundbill.originalrequirementbillid,
         s_outboundbill.originalrequirementbilltype,
         s_outboundbill.originalrequirementbillcode,
         2,
         1,
         'Admin',
         sysdate);
      --生成发运单id编号
      FPSId    := S_partsshippingorder.NEXTVAL;
      FNewCode := 'FY{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') ||
                  '{SERIAL}';
      BEGIN
        SELECT Id
          INTO FComplateId
          FROM CodeTemplate
         WHERE name = 'PartsShippingOrder'
           AND IsActived = 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          FErrormsg := '未找到名称为"PartsShippingOrder"的有效编码规则。';
          raise FMyException;
        WHEN OTHERS THEN
          raise;
      END;
      NEWPSCODE := regexp_replace(regexp_replace(FNewCode,
                                                 '{CORPCODE}',
                                                 FbranchCode),
                                  '{SERIAL}',
                                  lpad(TO_CHAR(GetSerial(FComplateId,
                                                         TRUNC(sysdate, 'DD'),
                                                         FbranchCode)),
                                       6,
                                       '0'));
      --生成发运单
      INSERT into partsshippingorder
        (id,
         code,
         type,
         branchid,
         partssalescategoryid,
         shippingcompanyid,
         shippingcompanycode,
         shippingcompanyname,
         settlementcompanyid,
         settlementcompanycode,
         settlementcompanyname,
         receivingcompanyid,
         receivingcompanycode,
         invoicereceivesalecateid,
         invoicereceivesalecatename,
         receivingcompanyname,
         warehouseid,
         warehousecode,
         warehousename,
         receivingwarehouseid,
         receivingwarehousecode,
         receivingwarehousename,
         receivingaddress,
         logisticcompanyid,
         logisticcompanycode,
         logisticcompanyname,
         originalrequirementbillid,
         originalrequirementbillcode,
         originalrequirementbilltype,
         shippingmethod,
         requestedarrivaldate,
         shippingdate,
         status,
         creatorid,
         creatorname,
         createtime)
      values
        (FPSId,
         NEWPSCODE,
         1,
         FbranchId,
         Fpartssalescategoryid,
         FbranchId,
         (select code from company where company.id = FbranchId),
         (select name from company where company.id = FbranchId),
         FbranchId,
         (select code from company where company.id = FbranchId),
         (select name from company where company.id = FbranchId),
         FRECEIVINGCOMPANYID,
         (select code from company where company.id = FRECEIVINGCOMPANYID),
         Fpartssalescategoryid,
         Fpartssalescategoryname,
         (select name from company where company.id = FRECEIVINGCOMPANYID),
         FWAREHOUSEID,
         (SELECT CODE FROM WAREHOUSE WHERE id = FWAREHOUSEID),
         (SELECT name FROM WAREHOUSE WHERE id = FWAREHOUSEID),
         FRECEIVINGWAREHOUSEID,
         (SELECT CODE FROM WAREHOUSE WHERE id = FRECEIVINGWAREHOUSEID),
         (SELECT name FROM WAREHOUSE WHERE id = FRECEIVINGWAREHOUSEID),
         FReceivingAddress,
         null,
         '空',
         '空',
         s_outboundbill.originalrequirementbillid,
         s_outboundbill.originalrequirementbillcode,
         s_outboundbill.originalrequirementbilltype,
         s_outboundbill.shippingmethod,
         sysdate,
         sysdate,
         1,
         1,
         'Admin',
         sysdate);

      --生成发运单关联单
      insert into partsshippingorderref
        (id, partsshippingorderid, partsoutboundbillid)
      values
        (s_partsshippingorderref.nextval, FPSId, FOBId);

      --生成物流批次
      FPLBId := s_PartsLogisticBatch.Nextval;
      insert into PartsLogisticBatch
        (id,
         Companyid,
         Status,
         Shippingstatus,
         Sourceid,
         Sourcetype,
         Creatorid,
         Creatorname,
         Createtime)
      values
        (FPLBId, FbranchId, 1, 2, FOPId, 1, 1, 'Admin', sysdate);
      --生成物流批次单据清单（出库单清单）
      insert into PartsLogisticBatchBillDetail
        (id,
         Partslogisticbatchid,
         Billid,
         Billtype,
         Creatorid,
         Creatorname,
         Createtime)
      values
        (s_PartsLogisticBatchBillDetail.Nextval,
         FPLBId,
         FOBId,
         1,
         1,
         'Admin',
         sysdate);
      --生成物流批次单据清单（发运单清单）
      insert into PartsLogisticBatchBillDetail
        (id,
         Partslogisticbatchid,
         Billid,
         Billtype,
         Creatorid,
         Creatorname,
         Createtime)
      values
        (s_PartsLogisticBatchBillDetail.Nextval,
         FPLBId,
         FPSId,
         2,
         1,
         'Admin',
         sysdate);

      --获取出库单清单数据
      for s_outboundbilldetail in outboundbilldetail(parentid) loop

        outboundbilldetailno := s_outboundbilldetail.rno;
        Vtable_outboundbilldetail(outboundbilldetailno).ID := s_outboundbilldetail.id;
        Vtable_outboundbilldetail(outboundbilldetailno).PARTSOUTBOUNDBILLID := s_outboundbilldetail.PARTSOUTBOUNDBILLID;
        --Vtable_outboundbilldetail(outboundbilldetailno).SPAREPARTID := s_outboundbilldetail.SPAREPARTID;
        Vtable_outboundbilldetail(outboundbilldetailno).SPAREPARTCODE := s_outboundbilldetail.SPAREPARTCODE;
        Vtable_outboundbilldetail(outboundbilldetailno).SPAREPARTNAME := s_outboundbilldetail.SPAREPARTNAME;
        Vtable_outboundbilldetail(outboundbilldetailno).OUTBOUNDAMOUNT := s_outboundbilldetail.OUTBOUNDAMOUNT;
        Vtable_outboundbilldetail(outboundbilldetailno).WAREHOUSEAREAID := s_outboundbilldetail.WAREHOUSEAREAID;
        Vtable_outboundbilldetail(outboundbilldetailno).WAREHOUSEAREACODE := s_outboundbilldetail.WAREHOUSEAREACODE;
        Vtable_outboundbilldetail(outboundbilldetailno).BATCHNUMBER := s_outboundbilldetail.BATCHNUMBER;
        Vtable_outboundbilldetail(outboundbilldetailno).SETTLEMENTPRICE := s_outboundbilldetail.SETTLEMENTPRICE;
        Vtable_outboundbilldetail(outboundbilldetailno).COSTPRICE := s_outboundbilldetail.COSTPRICE;
        Vtable_outboundbilldetail(outboundbilldetailno).REMARK := s_outboundbilldetail.REMARK;

      end loop;
      --循环出库单生成后续业务单据清单
      FOR Vi IN 1 .. Vtable_outboundbilldetail.count LOOP

        begin
          --判断出库单图号是否在系统内存在
          rno:=rno+1;
          begin
            select id
              into Vtable_outboundbilldetail(vi).SPAREPARTID
              from sparepart
             where sparepart.code = Vtable_outboundbilldetail(vi)
                  .SPAREPARTCODE;
          exception
            when no_data_found then
              begin
                FErrormsg := '配件图号：' || Vtable_outboundbilldetail(vi)
                            .SPAREPARTCODE || ' 不存在';
                raise FMyException;
              end;
          end;
          --生成采购订单清单
          insert into partspurchaseorderdetail
            (id,
             partspurchaseorderid,
             sparepartid,
             sparepartcode,
             sparepartname,
             unitprice,
             orderamount,
             confirmedamount,
             shippingamount,
             measureunit,
             promiseddeliverytime,
             SERIALNUMBER)
          values
            (s_partspurchaseorderdetail.nextval,
             FPurId,
             Vtable_outboundbilldetail(vi).SPAREPARTID,
             Vtable_outboundbilldetail(vi).SPAREPARTCODE,
             Vtable_outboundbilldetail(vi).SPAREPARTNAME,
             Vtable_outboundbilldetail(vi).SETTLEMENTPRICE,
             Vtable_outboundbilldetail(vi).OUTBOUNDAMOUNT,
             Vtable_outboundbilldetail(vi).OUTBOUNDAMOUNT,
             Vtable_outboundbilldetail(vi).OUTBOUNDAMOUNT,
             (SELECT SPAREPART.MEASUREUNIT
                FROM SPAREPART
               WHERE SPAREPART.id = Vtable_outboundbilldetail(vi)
                    .SPAREPARTID),
             SYSDATE,
             rno);
          --生成供应商发运单清单
          insert into suppliershippingdetail
            (id,
             suppliershippingorderid,
             sparepartid,
             sparepartcode,
             sparepartname,
             measureunit,
             quantity,
             confirmedamount,
             unitprice,SERIALNUMBER)
          values
            (s_suppliershippingdetail.nextval,
             FSupShipId,
             Vtable_outboundbilldetail(vi).SPAREPARTID,
             Vtable_outboundbilldetail(vi).SPAREPARTCODE,
             Vtable_outboundbilldetail(vi).SPAREPARTNAME,
             (SELECT SPAREPART.MEASUREUNIT
                FROM SPAREPART
               WHERE SPAREPART.id = Vtable_outboundbilldetail(vi)
                    .SPAREPARTID),
             Vtable_outboundbilldetail(vi).OUTBOUNDAMOUNT,
             Vtable_outboundbilldetail(vi).OUTBOUNDAMOUNT,
             Vtable_outboundbilldetail(vi).SETTLEMENTPRICE,rno);
          --生成入库计划单清单
          insert into partsinboundplandetail
            (id,
             partsinboundplanid,
             sparepartid,
             sparepartcode,
             sparepartname,
             plannedamount,
             inspectedquantity,
             price)
          values
            (s_partsinboundplandetail.nextval,
             FIPId,
             Vtable_outboundbilldetail       (vi).SPAREPARTID,
             Vtable_outboundbilldetail       (vi).SPAREPARTCODE,
             Vtable_outboundbilldetail       (vi).SPAREPARTNAME,
             Vtable_outboundbilldetail       (vi).OUTBOUNDAMOUNT,
             Vtable_outboundbilldetail       (vi).OUTBOUNDAMOUNT,
             Vtable_outboundbilldetail       (vi).SETTLEMENTPRICE);
          --获取计划价
          begin
            select t.plannedprice
              into Fstdprice
              from PARTSPLANNEDPRICE T

             where T.OWNERCOMPANYID = FbranchId
               and T.PARTSSALESCATEGORYID = Fpartssalescategoryid
               and T.SPAREPARTID = Vtable_outboundbilldetail(vi)
                  .SPAREPARTID;

          exception
            when no_data_found then
              begin
                FErrormsg := '配件图号 ' || Vtable_outboundbilldetail(vi)
                            .SPAREPARTCODE || '计划价不存在';
                raise FMyException;
              end;
          end;
          --生成入库检验单清单
          insert into partsinboundcheckbilldetail
            (id,
             partsinboundcheckbillid,
             sparepartid,
             sparepartcode,
             sparepartname,
             warehouseareaid,
             warehouseareacode,
             inspectedquantity,
             settlementprice,
             costprice)
          values
            (s_partsinboundcheckbilldetail.nextval,
             FICId,
             Vtable_outboundbilldetail(vi).SPAREPARTID,
             Vtable_outboundbilldetail(vi).SPAREPARTCODE,
             Vtable_outboundbilldetail(vi).SPAREPARTNAME,
             FCheckWarehouseAreaId,
             (select code
                from warehousearea
               where warehousearea.id = FCheckWarehouseAreaId),
             Vtable_outboundbilldetail(vi).OUTBOUNDAMOUNT,
             Vtable_outboundbilldetail(vi).SETTLEMENTPRICE,
             Fstdprice);
          --生成上架单清单
          insert into partsshiftorderdetail
            (id,
             partsshiftorderid,
             sparepartid,
             sparepartcode,
             sparepartname,
             originalwarehouseareaid,
             originalwarehouseareacode,
             originalwarehouseareacategory,
             destwarehouseareaid,
             destwarehouseareacode,
             destwarehouseareacategory,
             quantity)
          values
            (s_partsshiftorderdetail.nextval,
             FPMId,
             Vtable_outboundbilldetail(vi).SPAREPARTID,
             Vtable_outboundbilldetail(vi).SPAREPARTCODE,
             Vtable_outboundbilldetail(vi).SPAREPARTNAME,
             FCheckWarehouseAreaId,
             (select code
                from warehousearea
               where warehousearea.id = FCheckWarehouseAreaId),
             3,
             FStoreWarehouseAreaId,
             (select code
                from warehousearea
               where warehousearea.id = FStoreWarehouseAreaId),
             1,
             Vtable_outboundbilldetail(vi).OUTBOUNDAMOUNT);

          --获取出库计划单清单价格
          begin
            select v.orderprice
              into Fsalesprice
              from partssalesorder T
             inner join partssalesorderdetail v
                on t.id = v.partssalesorderid

             where t.code = s_outboundbill.OriginalRequirementBillCode
               and v.sparepartid = Vtable_outboundbilldetail(vi)
                  .SPAREPARTID;

          exception
            when no_data_found then
              begin
                select t.salesprice
                  into Fsalesprice
                  from partssalesprice T

                 where T.BRANCHID = FbranchId
                   and T.PARTSSALESCATEGORYID = Fpartssalescategoryid
                   and T.SPAREPARTID = Vtable_outboundbilldetail(vi)
                      .SPAREPARTID
                   and t.status <> 99
                   and rownum = 1;
              exception
                when no_data_found then
                  begin
                    FErrormsg := '配件价格不存在';
                    raise FMyException;
                  end;
              end;
          end;

          --生成出库计划单清单
          INSERT INTO PARTSOUTBOUNDPLANDETAIL
            (ID,
             PARTSOUTBOUNDPLANID,
             SPAREPARTID,
             SPAREPARTCODE,
             SPAREPARTNAME,
             PLANNEDAMOUNT,
             OUTBOUNDFULFILLMENT,
             PRICE)
          VALUES
            (s_PARTSOUTBOUNDPLANDETAIL.Nextval,
             FOPId,
             Vtable_outboundbilldetail        (vi).SPAREPARTID,
             Vtable_outboundbilldetail        (vi).SPAREPARTCODE,
             Vtable_outboundbilldetail        (vi).SPAREPARTNAME,
             Vtable_outboundbilldetail        (vi).OUTBOUNDAMOUNT,
             Vtable_outboundbilldetail        (vi).OUTBOUNDAMOUNT,
             Fsalesprice);
          --生成出库单清单
          insert into partsoutboundbilldetail
            (id,
             partsoutboundbillid,
             sparepartid,
             sparepartcode,
             sparepartname,
             outboundamount,
             warehouseareaid,
             warehouseareacode,
             settlementprice,
             costprice)
          values
            (s_partsoutboundbilldetail.nextval,
             FOBId,
             Vtable_outboundbilldetail(vi).SPAREPARTID,
             Vtable_outboundbilldetail(vi).SPAREPARTCODE,
             Vtable_outboundbilldetail(vi).SPAREPARTNAME,
             Vtable_outboundbilldetail(vi).OUTBOUNDAMOUNT,
             FStoreWarehouseAreaId,
             (select code
                from warehousearea
               where warehousearea.id = FStoreWarehouseAreaId),
             Fsalesprice,
             Fstdprice);
          --生成发运单清单
          insert into partsshippingorderdetail
            (id,
             partsshippingorderid,
             sparepartid,
             sparepartcode,
             sparepartname,
             shippingamount,
             settlementprice)
          values
            (s_partsshippingorderdetail.nextval,
             FPSId,
             Vtable_outboundbilldetail         (vi).SPAREPARTID,
             Vtable_outboundbilldetail         (vi).SPAREPARTCODE,
             Vtable_outboundbilldetail         (vi).SPAREPARTNAME,
             Vtable_outboundbilldetail         (vi).OUTBOUNDAMOUNT,
             Fsalesprice);
          --生成物流批次材料清单
          insert into partslogisticbatchitemdetail
            (id,
             partslogisticbatchid,
             counterpartcompanyid,
             receivingcompanyid,
             shippingcompanyid,
             sparepartid,
             sparepartcode,
             sparepartname,
             outboundamount)
          values
            (s_partslogisticbatchitemdetail.nextval,
             FPLBId,
             FRECEIVINGCOMPANYID,
             FRECEIVINGCOMPANYID,
             FbranchId,
             Vtable_outboundbilldetail             (vi).SPAREPARTID,
             Vtable_outboundbilldetail             (vi).SPAREPARTCODE,
             Vtable_outboundbilldetail             (vi).SPAREPARTNAME,
             Vtable_outboundbilldetail             (vi).OUTBOUNDAMOUNT);
          releaseamount := nvl(Fsalesprice,
                               0) *
                           nvl(Vtable_outboundbilldetail(vi).OUTBOUNDAMOUNT,
                               0) + releaseamount;
        end;
      end loop;
      begin
        select customeraccount.pendingamount
          into pendingamount
          from customeraccount
         where id = FCUSTOMERACCOUNTID;
      exception
        when no_data_found then
          begin
            FErrormsg := '客户账户不存在';
            raise FMyException;
          end;
      end;
      if pendingamount < releaseamount then
        begin
          FErrormsg := '客户账户审批待发金额小于解锁金额';
          raise FMyException;
        end;
      else
        update customeraccount
           set customeraccount.pendingamount = customeraccount.pendingamount -
                                               releaseamount,customeraccount.shippedproductvalue=customeraccount.shippedproductvalue+releaseamount
         where id = FCUSTOMERACCOUNTID;
      end if;
      --更新原出库单统购分销状态
      update yxdcs.partsoutboundbill
         set PurDistributionstatus = 2
       where code = s_outboundbill.code;
    exception
      when FMyException then
        begin
          ROLLBACK;
          insert into yxdcs.PurDistributionDealerLog
            (id, Code, SyncDate, OutBoundCode, Status)
          values
            (yxdcs.s_PurDistributionDealerLog.nextval,
             FErrormsg,
             sysdate,
             s_outboundbill.code,
             1);
          update yxdcs.partsoutboundbill
             set PurDistributionstatus = 3
           where code = s_outboundbill.code;
        end;

      WHEN OTHERS THEN
        BEGIN
          ROLLBACK;
          FErrormsg := sqlerrm;
          insert into yxdcs.PurDistributionDealerLog
            (id, Code, SyncDate, OutBoundCode, Status)
          values
            (yxdcs.s_PurDistributionDealerLog.nextval,
             FErrormsg,
             sysdate,
             s_outboundbill.code,
             1);
          update yxdcs.partsoutboundbill
             set PurDistributionstatus = 3
           where code = s_outboundbill.code;
        end;
    end;
  end loop;

end AutoFinishPurDistribution;