CREATE OR REPLACE Procedure AutoSetICBinstatus As
  Isallnew      Number(9);
  Isallfinish   Number(9);
  isPartfinish  Number(9);
  Isforcefinish Number(9);
  Iscanncel     Number(9);
Begin
  Declare
    Cursor Autoset Is(
      Select Id,code
        From INTERNALACQUISITIONBILL
       Where Status in( 2, 99)
         And Instatus In (5, 10));

  Begin
    For s_Autoset In Autoset Loop
      Begin
        select count(*)
          into Isallnew
          from partsinboundplan a
         where a.sourcecode = s_Autoset.code
           and a.status = 1;
        select count(*)
          into Isallfinish
          from partsinboundplan a
         where a.sourcecode = s_Autoset.code
           and a.status = 2;
        select count(*)
          into Isforcefinish
          from partsinboundplan a
         where a.sourcecode = s_Autoset.code
           and a.status = 3;
        select count(*)
          into isPartfinish
          from partsinboundplan a
         where a.sourcecode = s_Autoset.code
           and a.status = 4;
        select count(*)
          into Iscanncel
          from INTERNALACQUISITIONBILL a
         where a.code = s_Autoset.code
         and a.status = 99;

        if Isallnew > 0 then
          update INTERNALACQUISITIONBILL
             set InStatus = 5
           where INTERNALACQUISITIONBILL.ID = s_Autoset.id;
        end if;
        if Isallfinish > 0 then
          update INTERNALACQUISITIONBILL
             set InStatus = 15
           where INTERNALACQUISITIONBILL.ID = s_Autoset.id;
        end if;
        if Isforcefinish > 0 then
          update INTERNALACQUISITIONBILL
             set InStatus = 11
           where INTERNALACQUISITIONBILL.ID = s_Autoset.id;
        end if;
        if isPartfinish > 0 then
          update INTERNALACQUISITIONBILL
             set InStatus = 10
           where INTERNALACQUISITIONBILL.ID = s_Autoset.id;
        end if;
        if Iscanncel > 0 then
          update INTERNALACQUISITIONBILL
             set InStatus = 99
           where INTERNALACQUISITIONBILL.ID = s_Autoset.id;
        end if;
      end;
    end loop;
  end;
end;