CREATE OR REPLACE Procedure AutoSetICBinstatus_n As
  /* Isallnew      Number(9);
  Isallfinish   Number(9);
  isPartfinish  Number(9);
  Isforcefinish Number(9);
  Iscanncel     Number(9);*/
Begin

  update INTERNALACQUISITIONBILL
     set Instatus = 99
   where Instatus In (5, 10)
     and status = 99;
  commit;
  update INTERNALACQUISITIONBILL
     set Instatus = 10
   where Instatus = 5
     and exists
   (select 1
            from partsinboundplan
           where partsinboundplan.sourcecode = INTERNALACQUISITIONBILL.code
             and partsinboundplan.status = 4);
  commit;
  update INTERNALACQUISITIONBILL
     set Instatus = 11
   where Instatus in (5,10)
     and exists
   (select 1
            from partsinboundplan
           where partsinboundplan.sourcecode = INTERNALACQUISITIONBILL.code
             and partsinboundplan.status = 3);
  commit;
    update INTERNALACQUISITIONBILL
     set Instatus = 15
   where Instatus in  (5,10)
     and exists
   (select 1
            from partsinboundplan
           where partsinboundplan.sourcecode = INTERNALACQUISITIONBILL.code
             and partsinboundplan.status = 2);
  commit;
end;