create or replace procedure AutoSubmitSalesOrder as
begin
 declare
    cursor Autoadd is(
     select * from AutoSalesOrderProcess where status=0 );
begin
    for S_Autoadd in Autoadd loop
      begin
          autochecknoecommerceorder(S_Autoadd.orderid);
          update AutoSalesOrderProcess set status=1 where id=S_Autoadd.id;
          commit;
        end;
        end loop;
        end;
end AutoSubmitSalesOrder;