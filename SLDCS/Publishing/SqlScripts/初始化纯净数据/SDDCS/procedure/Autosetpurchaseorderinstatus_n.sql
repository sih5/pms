CREATE OR REPLACE Procedure Autosetpurchaseorderinstatus_n As
Begin
---作废
Update Partspurchaseorder
   Set Instatus = 99
 Where Status = 99
   And Instatus >= 5
   and Instatus <= 10;
commit;
----部分入库
Update Partspurchaseorder
   Set Instatus = 10
 Where Instatus =5
   and exists
 (select c.id
          From Suppliershippingorder c
         inner Join Partsinboundplan e
            On e.Sourcecode = c.Code
           and e.storagecompanyid = c.branchid
         Inner Join PARTSINBOUNDCHECKBILL f
            On f.partsinboundplanid = e.id
         Where c.Partspurchaseorderid = Partspurchaseorder.Id);
commit;
----强制完成
Update Partspurchaseorder
 Set Instatus = 11
Where Instatus >= 5
 and Instatus <= 10
 and (Status = 7 or exists
      (select c.id
         From Suppliershippingorder c
         left Join Partsinboundplan e
           On e.Sourcecode = c.Code
        where c.Partspurchaseorderid = Partspurchaseorder.id
          and (c.Status = 4 Or e.Status = 3)));
commit;
---全部入库完成
Update Partspurchaseorder
   Set Instatus = 15
 Where Instatus >= 5
   and Instatus <= 10 and status in(6,7)
   and exists
 (select d.sumconfirmedamount,
               t.suminspectedquantity,
               d.partspurchaseorderid
          from (select sum(nvl(confirmedamount, 0)) sumconfirmedamount,
                       partspurchaseorderid
                  from Partspurchaseorderdetail
                 group by partspurchaseorderid) d

         inner Join (Select sum(nvl(g.inspectedquantity, 0)) suminspectedquantity,
                           a.partspurchaseorderid
                      From Suppliershippingorder a
                     Inner Join Partsinboundplan b
                        On a.Code = b.Sourcecode
                       and b.storagecompanyid = a.branchid
                     inner join PARTSINBOUNDCHECKBILL f
                        On f.partsinboundplanid = b.id
                     inner join PARTSINBOUNDCHECKBILLDETAIL g
                        on g.partsinboundcheckbillid = f.id

                     Group By a.partspurchaseorderid) t
            on t.partspurchaseorderid = d.partspurchaseorderid
         where d.sumconfirmedamount = t.suminspectedquantity
           and d.partspurchaseorderid = Partspurchaseorder.id);
commit;

End Autosetpurchaseorderinstatus_n;