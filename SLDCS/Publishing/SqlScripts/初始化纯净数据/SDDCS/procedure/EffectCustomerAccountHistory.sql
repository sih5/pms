create or replace procedure EffectCustomerAccountHistory as
begin
    insert into CustomerAccountHistory
    (  ACCOUNTID             ,
  ACCOUNTBALANCE        ,
  SHIPPEDPRODUCTVALUE    ,
  PENDINGAMOUNT          ,
  CUSTOMERCREDENCEAMOUNT ,
  STATUS                 ,
  TYPE                   ,
  CODE                   ,
  NAME                   ,
  ACCOUNTGROUPCODE       ,
  ACCOUNTGROUPNAME       ,
  WSHBALANCE             ,
  KYBALANCE              ,
  YEBALANCE              ,
  SDBALANCE              ,
  FLBALANCE              ,
  THEDATE                ,
  ID                     ,
  CUSTOMERCOMPANYID ,ACCOUNTGROUPid
    )
     SELECT Extent1.Id Accountid,
       Extent1.AccountBalance,
       Extent1.ShippedProductValue,
       Extent1.PendingAmount,
       Extent1.CustomerCredenceAmount,
       Extent1.Status,
       Extent2.Type,
       Extent2.Code,
       Extent2.Name,
       Extent3.Code AS AccountGroupCode,
       Extent3.Name AS AccountGroupName,
       CASE
         WHEN GroupBy1.A1 IS NULL THEN
          0
         ELSE
          GroupBy1.A1
       END AS WSHBalance,
       ((((Extent1.AccountBalance + (CASE
         WHEN Extent4.AccountBalanceAmount IS NULL THEN
          0
         ELSE
          Extent4.AccountBalanceAmount
       END)) + Extent1.CustomerCredenceAmount) -
       Extent1.ShippedProductValue) - Extent1.PendingAmount) - (CASE
         WHEN GroupBy1.A1 IS NULL THEN
          0
         ELSE
          GroupBy1.A1
       END) AS KYBalance,
       Extent1.AccountBalance - Extent1.ShippedProductValue AS YEBalance,
       Extent1.ShippedProductValue + Extent1.PendingAmount AS SDBalance,
       Extent4.AccountBalanceAmount as FLBalance,trunc(sysdate,'dd'),s_CustomerAccountHistory.Nextval,
       Extent1.Customercompanyid,Extent3.id
  FROM CustomerAccount Extent1
 INNER JOIN Company Extent2
    ON Extent1.CustomerCompanyId = Extent2.Id
 INNER JOIN AccountGroup Extent3
    ON Extent1.AccountGroupId = Extent3.Id
  LEFT OUTER JOIN PartsRebateAccount Extent4
    ON (Extent1.AccountGroupId = Extent4.AccountGroupId)
   AND (Extent1.CustomerCompanyId = Extent4.CustomerCompanyId)
  LEFT OUTER JOIN (SELECT Filter1.SalesCategoryId AS K1,
                          Filter1.SubmitCompanyId AS K2,
                          Extent7.AccountGroupId AS K3,
                          Sum((TO_NUMBER(CASE
                                           WHEN Filter1.OrderedQuantity -
                                                Filter1.ApproveQuantity IS NULL THEN
                                            0
                                           ELSE
                                            Filter1.OrderedQuantity -
                                            Filter1.ApproveQuantity
                                         END)) * Filter1.OrderPrice) AS A1
                     FROM (SELECT
                                  Extent5.SalesCategoryId,
                                  Extent5.SalesUnitId,
                                  Extent5.SubmitCompanyId,
                                  Extent6.OrderedQuantity,
                                  Extent6.ApproveQuantity,
                                  Extent6.OrderPrice
                             FROM PartsSalesOrder Extent5
                            INNER JOIN PartsSalesOrderDetail Extent6
                               ON Extent5.Id = Extent6.PartsSalesOrderId
                            WHERE Extent5.Status IN (2, 4)) Filter1
                    INNER JOIN SalesUnit Extent7
                       ON Filter1.SalesUnitId = Extent7.Id
                    WHERE Extent7.OwnerCompanyId in(select id from branch)
                    GROUP BY Filter1.SalesCategoryId,
                             Filter1.SubmitCompanyId,
                             Extent7.AccountGroupId) GroupBy1
    ON (Extent1.CustomerCompanyId = GroupBy1.K2)
   AND (Extent3.Id = GroupBy1.K3)
 WHERE ((Extent2.Status = 1) AND (Extent1.Status = 1));
end;