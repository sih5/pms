create or replace procedure EffectPartsHistoryStock as
 cursor tmpCursor is
  select id from FinancialSnapshotSet a
   where a.status = '1'
       and to_date(to_char(a.snapshottime, 'yyyy-mm-dd'),'yyyy-mm-dd') =
           to_date(to_char(sysdate-1, 'yyyy-mm-dd'),'yyyy-mm-dd');
  tmpCursorRow  tmpCursor%rowtype;
begin
  open tmpCursor;
  loop
    fetch tmpCursor into tmpCursorRow;
    exit when tmpCursor%notfound;

    insert into PARTSHISTORYSTOCK
     select S_PARTSHISTORYSTOCK.Nextval,
            d.warehouseid,
            d.storagecompanyid,
            d.storagecompanytype,
            d.branchid,
            d.warehouseareaid,
            d.warehouseareacategoryid,
            d.partid,
            d.quantity,
            e.plannedprice,
            (d.quantity * e.plannedprice) as PLANNEDPRICEAMOUNT,
            d.remark,
            a.snapshottime,
            a.creatorid,
            a.creatorname,
            sysdate,
            null,
            null,
            null,f.id,
             nvl(g.salesprice, 0),
             nvl(g.salesprice, 0) * d.quantity
       from FinancialSnapshotSet a --财务快照
       inner  join partssalescategory f  on a.branchid=f.branchid
       inner join salesunit b --销售组织
         on b.partssalescategoryid = f.id
       inner join SalesUnitAffiWarehouse c --销售组织仓库关系
         on b.id = c.salesunitid
       inner join PartsStock d --配件库存
         on c.warehouseid = d.warehouseid
       inner join PartsPlannedPrice e --配件销售价
         on d.partid = e.Sparepartid
        and e.partssalescategoryid = f.id
         left join partssalesprice g
          on g.sparepartid = d.partid
         and g.partssalescategoryid = f.id
         and g.status <> 99
         and g.pricetype = 1
      where a.Id = tmpCursorRow.Id;
    update FinancialSnapshotSet set status = 2 where id = tmpCursorRow.Id;
 end loop;
 close tmpCursor;
end;