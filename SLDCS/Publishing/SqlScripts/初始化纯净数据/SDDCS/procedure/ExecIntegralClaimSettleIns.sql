create or replace procedure ExecIntegralClaimSettleIns as
  /*定义接收参数*/
  ErrorMsg    varchar2(200);
  FBranchCode VARCHAR2(50);
  /*营销分公司名称*/
  FBranchName VARCHAR2(100);
  /*编号模板Id*/
  FComplateId integer;
  /*编码规则相关设置*/
  FDateFormat               date;
  FDateStr                  varchar2(8);
  FSerialLength             integer;
  FNewCode                  varchar2(50);
  FSsClaimSettlementBillId  integer;
  FBILLFORCLAIMSETTLEMENTiD integer;
  /*定义游标*/
  cursor Instructions Is
    select Id,
           BranchId,
           SettlementStartTime,
           SettlementEndTime,
           CreatorId,
           CreatorName,
           nvl(brandid, 0) PartsSalesCategoryId,
           nvl(ServiceProductLineId, 0) ServiceProductLineId,
           type
      from IntegralSettleDictate
     where SettlementEndTime < trunc(sysdate, 'DD')
       and Status = 1
     order by SettlementEndTime;
  Instruction Instructions%rowtype;
begin
  for Instruction in Instructions loop
    ErrorMsg := '';
    begin
      FDateFormat   := trunc(sysdate, 'DD');
      FDateStr      := to_char(sysdate, 'yyyymmdd');
      FSerialLength := 6;
      FNewCode      := 'SET{CORPCODE}' || FDateStr || '{SERIAL}';

      /*获取编号生成模板Id*/
      begin
        select Id
          into FComplateId
          from codetemplate
         where name = 'IntegralClaimBillSettle'
           and IsActived = 1;
      exception
        when NO_DATA_FOUND then
          raise_application_error(-20001,
                                  '未找到名称为"SsClaimSettlementBill"的有效编码规则。');
        when others then
          raise;
      end;
      select Code, Name
        into FBranchCode, FBranchName
        from Branch
       where id = Instruction.BranchId;
      insert into BILLFORINSCLAIMSETTLEMENT
        (SourceId,
         SourceCode,
         SourceType,
         DealerId,
         BranchId,
         DealerBusinessCode,
         Partssalescategoryid,
         ServiceProductLineId,
         UsedPrice)
        select IntegralClaimBill.Id,
               IntegralClaimBill.IntegralClaimBillCode,
               IntegralClaimBillType,
               IntegralClaimBill.Dealerid,
               IntegralClaimBill.Branchid,
               IntegralClaimBill.Dealerbusinesscode,
               IntegralClaimBill.Brandid,
               decode(Instruction.Serviceproductlineid,
                      0,
                      null,null,null,
                      (select repairorder.serviceproductlineid
                         from repairorder
                        where repairorder.code = IntegralClaimBill.Repaircode)),
               decode(IntegralClaimBill.Integralclaimbilltype,
                      4,
                      IntegralClaimBill.MemberUsedPrice,
                      5,
                      IntegralClaimBill.RedPacketsUsedPrice)
          from IntegralClaimBill
         inner join repairorder
            on repairorder.code = IntegralClaimBill.Repaircode
         where IntegralClaimBill.CreateTime <=
               Instruction.SettlementEndTime
           and (Instruction.SettlementStartTime is null or
               IntegralClaimBill.CreateTime >=
               Instruction.SettlementStartTime)
           and IntegralClaimBill.SettlementStatus = 2
           and IntegralClaimBill.BranchId = Instruction.BranchId
           and IntegralClaimBill.Integralstatus = 2
           and IntegralClaimBill.Integralclaimbilltype = Instruction.type
           and (Instruction.PartsSalesCategoryId = 0 or
               IntegralClaimBill.Brandid =
               Instruction.PartsSalesCategoryId)
              /* and (FProductLineType is null or ProductLineType=FProductLineType)*/
           and (Instruction.ServiceProductLineId is null or
               Instruction.ServiceProductLineId = 0 or
               repairorder.ServiceProductLineId =
               Instruction.ServiceProductLineId)
           and not exists (select 1
                  from IntegralClaimBillSettle bb
                 inner join IntegralClaimBillSettleDtl dd
                    on bb.id = dd.IntegralClaimBillSettleId
                 where IntegralClaimBill.IntegralClaimBillCode =
                       dd.IntegralClaimBillCode
                   and bb.status <= 2);

      insert into BILLFORINSCLAIMSETTLEMENT
        (SourceId,
         SourceCode,
         SourceType,
         DealerId,
         BranchId,
         DealerBusinessCode,
         Partssalescategoryid,
         ServiceProductLineId,
         UsedPrice)
        select OutofWarrantyPayment.Id,
               OutofWarrantyPayment.Code,
               decode(OutofWarrantyPayment.TransactionCategory,1,4,2,5),
               OutofWarrantyPayment.Dealerid,
               OutofWarrantyPayment.Branchid,
               (select Company.CustomerCode
                  from Company
                 where Company.Id = OutofWarrantyPayment.Dealerid),
               OutofWarrantyPayment.PartsSalesCategoryId,
               decode(Instruction.Serviceproductlineid,
                      0,
                     null,null,null,
                       OutofWarrantyPayment.ServiceProductLineId),
               decode(OutofWarrantyPayment.DebitOrReplenish,
                      1,
                      -1*OutofWarrantyPayment.TransactionAmount,
                      2,
                      OutofWarrantyPayment.TransactionAmount)
          from OutofWarrantyPayment
         where OutofWarrantyPayment.CreateTime <=
               Instruction.SettlementEndTime
           and (Instruction.SettlementStartTime is null or
               OutofWarrantyPayment.CreateTime >=
               Instruction.SettlementStartTime)
           and OutofWarrantyPayment.SettlementStatus = 2
           and OutofWarrantyPayment.BranchId = Instruction.BranchId
           and OutofWarrantyPayment.Status = 2
           and OutofWarrantyPayment.TransactionCategory =decode(Instruction.type,4,15,2)
           and (Instruction.PartsSalesCategoryId = 0 or
               OutofWarrantyPayment.PartsSalesCategoryId = Instruction.PartsSalesCategoryId)
           and (Instruction.ServiceProductLineId is null or
               Instruction.ServiceProductLineId = 0 or
               OutofWarrantyPayment.ServiceProductLineId =
               Instruction.ServiceProductLineId)
           and not exists (select 1
                  from IntegralClaimBillSettle bb
                 inner join IntegralClaimBillSettleDtl dd
                    on bb.id = dd.IntegralClaimBillSettleId
                 where OutofWarrantyPayment.Code =
                       dd.IntegralClaimBillCode
                   and bb.status <= 2);

      declare
        cursor Cursor_role is(

          select 0 IfInvoiced,
                 1 Status,
                 regexp_replace(regexp_replace(FNewCode,
                                               '{CORPCODE}',
                                               Dealer.Code),
                                '{SERIAL}',
                                lpad(to_char(GetSerial(FComplateId,
                                                       FDateFormat,
                                                       Dealer.Code)),
                                     FSerialLength,
                                     '0')) Code,
                 DealerId,
                 Dealer.Code DealerCode,
                 Dealer.Name DealerName,
                 Dealerbusinesscode,
                 Instruction.BranchId BranchId,
                 FBranchCode BranchCode,
                 FBranchName BranchName,
                 Instruction. SettlementStartTime,
                 Instruction. SettlementEndTime,
                 Instruction. CreatorId,
                 Instruction. CreatorName,
                 sysdate CreateTime,
                 PARTSSALESCATEGORYID,
                 ServiceProductLineId,
                 UsedPrice,
                 Instruction.type
            from (select DealerId,
                         BranchId,
                         PARTSSALESCATEGORYID,
                         ServiceProductLineId,
                         sum(UsedPrice) UsedPrice,
                         Dealerbusinesscode
                    from BILLFORINSCLAIMSETTLEMENT
                   group by DealerId,
                            BranchId,
                            PARTSSALESCATEGORYID,
                            ServiceProductLineId,Dealerbusinesscode) t_date
           inner join Dealer
              on Dealer.Id = t_date.DealerId);
      begin
        for S_role in Cursor_role loop
        
          FSsClaimSettlementBillId := s_IntegralClaimBillSettle.Nextval;
            if s_role.UsedPrice>=0 then begin
          update IntegralClaimBill
             set IntegralClaimBill.SettlementStatus = 1
           where IntegralClaimBill.IntegralClaimBillCode in
                 (select SOURCECODE
                    from BILLFORINSCLAIMSETTLEMENT
                   where BILLFORINSCLAIMSETTLEMENT.Dealerid =
                         s_role.DealerId
                     and BranchId = s_role.BranchId
                     and (PARTSSALESCATEGORYID IS NULL OR
                         PARTSSALESCATEGORYID = 0 OR
                         PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
                     and (ServiceProductLineId is null or
                         ServiceProductLineId = 0 or
                         ServiceProductLineId = s_role.ServiceProductLineId));
     update OutofWarrantyPayment
             set OutofWarrantyPayment.SettlementStatus = 3
           where OutofWarrantyPayment.Code in
                 (select SOURCECODE
                    from BILLFORINSCLAIMSETTLEMENT
                   where BILLFORINSCLAIMSETTLEMENT.Dealerid =
                         s_role.DealerId
                     and BranchId = s_role.BranchId
                     and (PARTSSALESCATEGORYID IS NULL OR
                         PARTSSALESCATEGORYID = 0 OR
                         PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
                     and (ServiceProductLineId is null or
                         ServiceProductLineId = 0 or
                         ServiceProductLineId = s_role.ServiceProductLineId));

          insert into IntegralClaimBillSettle
            (ID,
             BONUSPOINTSSETTLECODE,
             DEALERCODE,
             DEALERNAME,
             DEALERBUSINESSCODE,
             BRANDID,
             BRANDNAME,
             SERVICEPRODUCTLINEID,
             SERVICEPRODUCTLINENAME,
             SETTLETYPE,
             BONUSPOINTSSUM,
             STATUS,
             SETTLEVALIDFROM,
             SETTLEVALIDTO,
             CREATORID,
             CREATORNAME,
             CREATETIME,
             MEMO,
             DEALERID,PARTSSALESCATEGORYID,BranchCode)
          values
            (FSsClaimSettlementBillId,
             s_role.Code,
             s_role.DealerCode,
             s_role.DealerName,
             s_role.DEALERBUSINESSCODE,
             s_role.branchid,
             s_role.branchname,
             s_role.ServiceProductLineId,
             (select serviceproductlineview.ProductLineName
                from serviceproductlineview
               where serviceproductlineview.ProductLineId =
                     s_role.ServiceProductLineId),
             s_role.type,
             s_role.UsedPrice,
             1,
             s_role. SettlementStartTime,
             s_role. SettlementEndTime,
             s_role.CreatorId,
             s_role.CREATORNAME,
             s_role.CreateTime,
             '自动索赔结算',
             s_role.dealerid,s_role.PARTSSALESCATEGORYID,s_role.branchcode);

          /*清单生成*/
          insert into IntegralClaimBillSettleDtl
            (Id,
             IntegralClaimBillSettleId,
             IntegralClaimBillId,
             IntegralClaimBillCode,
             UsedPrice)
            select S_IntegralClaimBillSettleDtl.NEXTVAL,
                   FSsClaimSettlementBillId,
                   BILLFORINSCLAIMSETTLEMENT.SOURCEID,
                   BILLFORINSCLAIMSETTLEMENT.SOURCECODE,
                   abs(BILLFORINSCLAIMSETTLEMENT.USEDPRICE)
              from BILLFORINSCLAIMSETTLEMENT
             inner join IntegralClaimBillSettle
                on BILLFORINSCLAIMSETTLEMENT.DealerId =
                   IntegralClaimBillSettle.DealerId

               And BILLFORINSCLAIMSETTLEMENT.PARTSSALESCATEGORYID =
                   IntegralClaimBillSettle.Partssalescategoryid
               And (BILLFORINSCLAIMSETTLEMENT.ServiceProductLineId =
                   IntegralClaimBillSettle.ServiceProductLineId or
                   BILLFORINSCLAIMSETTLEMENT.ServiceProductLineId = 0 or
                   BILLFORINSCLAIMSETTLEMENT.ServiceProductLineId is null)
             where not exists
             (select *
                      from IntegralClaimBillSettleDtl
                     where IntegralClaimBillSettleDtl.IntegralClaimBillSettleId =
                           IntegralClaimBillSettle.Id)
               AND IntegralClaimBillSettle.ID = FSsClaimSettlementBillId;
          end;
          end if;
        end loop;
      end;

      Update IntegralSettleDictate
         set IntegralSettleDictate.Status        = 2,
             IntegralSettleDictate.ExecutionTime = sysdate
       where Id = Instruction.Id;
    exception
      when others then
        rollback;
        ErrorMsg := Sqlerrm;
        Update IntegralSettleDictate
           set IntegralSettleDictate.Status          = 3,
               IntegralSettleDictate.ExecutionResult = ErrorMsg,
               IntegralSettleDictate.ExecutionTime   = sysdate
         where Id = Instruction.Id;
        commit;
        exit;
    end;
    commit;
  end loop;
end;