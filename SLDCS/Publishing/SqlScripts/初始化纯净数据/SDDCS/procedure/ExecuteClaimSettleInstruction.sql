create or replace procedure ExecuteClaimSettleInstruction as
  /*定义接收参数*/
  GeneratedBillQuantity integer;
  ErrorMsg varchar2(200);
  /*定义游标*/
  cursor Instructions Is
    select Id,
           BranchId,
           SettlementStartTime,
           SettlementEndTime,
           CreatorId,
           CreatorName,
           nvl(PartsSalesCategoryId,0) PartsSalesCategoryId,
           nvl(ServiceProductLineId,0) ServiceProductLineId,
           ProductLineType,SettleMethods
      from SsClaimSettleInstruction
     where SettlementEndTime < trunc(sysdate,'DD')
       and Status = 1
     order by SettlementEndTime;
   Instruction Instructions%rowtype;
begin
  for Instruction in Instructions loop
    ErrorMsg:='';
    GeneratedBillQuantity:=0;
    begin
      GeneratedBillQuantity:=GenerateSsClaimSettlementBill(Instruction.branchid,null,Instruction.settlementstarttime,Instruction.settlementendtime,Instruction.creatorid,Instruction.creatorname,Instruction.PartsSalesCategoryId,Instruction.ServiceProductLineId,Instruction.ProductLineType,Instruction.SettleMethods);
      Update SsClaimSettleInstruction set SsClaimSettleInstruction.Status = 2, SsClaimSettleInstruction.ExecutionTime = sysdate
       where Id = Instruction.Id;
    exception
      when others then
        rollback;
        ErrorMsg:=Sqlerrm;
        Update SsClaimSettleInstruction set SsClaimSettleInstruction.Status = 3, SsClaimSettleInstruction.ExecutionResult = ErrorMsg, SsClaimSettleInstruction.ExecutionTime = sysdate
         where Id = Instruction.Id;
        commit;
        exit;
    end;
    commit;
  end loop;
end;