CREATE OR REPLACE PROCEDURE ExecutePlannedPriceApp2(priceAppId IN NUMBER) AS
  /*变更前价格合计*/
  sumMoney_Before NUMBER(19, 4);
  /*变更后价格合计*/
  sumMoney_After NUMBER(19, 4);
  /*变更后价格差异合计*/
  sumMoney_Diff NUMBER(19, 4);
  /*仓库成本变更单自增长Id*/
  n INTEGER;
  m  INTEGER;
  /*计划价申请单*/
  FOwnerCompanyType INTEGER;
  FPlannedPriceApp  PlannedPriceApp%rowtype;
  /*自动获取单据编码相关*/
  FDateFormat   DATE;
  FDateStr      VARCHAR2(8);
  FSerialLength INTEGER;
  FNewCode      VARCHAR2(50);
  FComplateId   INTEGER;

BEGIN
  /*编码规则变更，需同步调整*/
  FDateFormat := TRUNC(sysdate, 'YEAR');
  --FDateFormat:=trunc(sysdate,'MONTH');
  --FDateFormat   := trunc(sysdate, 'DD');
  FDateStr      := TO_CHAR(sysdate, 'yyyy');
  FSerialLength := 4;
  FNewCode      := 'WCC{CORPCODE}' || FDateStr || '{SERIAL}';
  BEGIN
    SELECT *
      INTO FPlannedPriceApp
      FROM PlannedPriceApp
     WHERE Id = priceAppId
       AND Status = 2;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      raise_application_error(-20001,
                              '只允许执行处于“已审核”状态的计划价申请单。');
    WHEN OTHERS THEN
      raise;
  END;
  -- 获取隶属企业类型
  BEGIN
    SELECT Type
      INTO FOwnerCompanyType
      FROM Company
     WHERE Id = FPlannedPriceApp.OwnerCompanyId;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      raise_application_error(-20002, '计划价申请单隶属企业不存在。');
    WHEN OTHERS THEN
      raise;
  END;
  /*获取编号生成模板Id*/
  BEGIN
    SELECT Id
      INTO FComplateId
      FROM CodeTemplate
     WHERE name = 'WarehouseCostChangeBill'
       AND IsActived = 1;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      raise_application_error(-20003,
                              '未找到名称为"WarehouseCostChangeBill"的有效编码规则。');
    WHEN OTHERS THEN
      raise;
  END;
  m:=0;
  --当前用游标把对应计划价清单把所有相关库存锁住防止并发

  update partsstock a set a.modifytime=a.modifytime
 where exists (select 1
          from SalesUnitAffiWarehouse
         INNER JOIN Salesunit
            ON SalesUnitAffiWarehouse.Salesunitid = Salesunit.ID
         WHERE Salesunit.Partssalescategoryid = FPlannedPriceApp.Partssalescategoryid
           and a.warehouseid = SalesUnitAffiWarehouse.Warehouseid)
   and A.StorageCompanyType = 1
   and a.storagecompanyid = FPlannedPriceApp.Ownercompanyid
   and exists
 (select 1 from plannedpriceappdetail
         where plannedpriceappdetail.plannedpriceappid = FPlannedPriceApp.id
           and a.partid = plannedpriceappdetail.sparepartid);

  /*更新计划价申请单，清单赋值变更前计划价*/
  UPDATE PlannedPriceAppDetail detail
     SET (PriceBeforeChange, Quantity, AmountDifference) =
         (SELECT NVL(PlannedPrice, 0), 0, 0
            FROM PartsPlannedPrice price
           WHERE OwnerCompanyId = FPlannedPriceApp.OwnerCompanyId
             AND price.partssalescategoryid =
                 FPlannedPriceApp.partssalescategoryid
             AND price.SparePartId = detail.SparePartId)
   WHERE detail.PlannedPriceAppId = FPlannedPriceApp.Id
     AND EXISTS
   (SELECT 1
            FROM PartsPlannedPrice price
           WHERE price.OwnerCompanyId = FPlannedPriceApp.OwnerCompanyId
             AND price.partssalescategoryid =
                 FPlannedPriceApp.partssalescategoryid
             AND price.SparePartId = detail.SparePartId);
  UPDATE PlannedPriceAppDetail detail
     SET PriceBeforeChange = 0, Quantity = 0, AmountDifference = 0
   WHERE detail.PlannedPriceAppId = FPlannedPriceApp.Id
     AND NOT EXISTS
   (SELECT 1
            FROM PartsPlannedPrice price
           WHERE price.OwnerCompanyId = FPlannedPriceApp.OwnerCompanyId
             AND price.partssalescategoryid =
                 FPlannedPriceApp.partssalescategoryid
             AND price.SparePartId = detail.SparePartId);
  /*更新计划价申请单，清单赋值数量、差异金额(数量包括在库中的数量和在途的数量，在途数量从企业调拨在途配件成本中获取，在库中的数量从配件库存中获取）*/
  --第一次获取库存 0503byzxl
  UPDATE PlannedPriceAppDetail detail
     SET (quantity, Amountdifference) =
         (SELECT NVL(cost.Quantity, 0),
                 (detail.RequestedPrice - NVL(detail.PriceBeforeChange, 0)) *
                 cost.Quantity
            FROM (SELECT tmp.sparepartid, SUM(tmp.tempquantity) AS quantity
                    FROM (SELECT a.partid AS sparepartId,
                                 NVL(a.quantity, 0) AS tempquantity
                            FROM partsstock a
                           INNER JOIN SalesUnitAffiWarehouse
                              ON A.warehouseid =
                                 SalesUnitAffiWarehouse.Warehouseid
                           INNER JOIN Salesunit
                              ON SalesUnitAffiWarehouse.Salesunitid =
                                 Salesunit.ID
                           WHERE A.StorageCompanyType = 1 and a.storagecompanyid=FPlannedPriceApp.Ownercompanyid
                             AND Salesunit.Partssalescategoryid =
                                 FPlannedPriceApp.Partssalescategoryid
                          UNION ALL
                           SELECT
                       pob.SparePartId,
                       (pob.OutboundAmount - NVL((select picb.InspectedQuantity
                                                    from  (select sum(InspectedQuantity) InspectedQuantity,
                                                                 OriginalRequirementBillCode,
                                                                 sparepartid
                                                            from PartsInboundCheckBill picb
                                                           inner JOIN PartsInboundCheckBillDetail picbd
                                                              ON picb.Id =
                                                                 picbd.PartsInboundCheckBillId
                                                           where picb.InboundType = 3
                                                           group by OriginalRequirementBillCode,
                                                                    sparepartid) picb
                                                   where pob.sparepartid =
                                                         picb.sparepartid
                                                     and picb.OriginalRequirementBillCode =
                                                         pto.code),
                                                 0))
                        FROM PartsTransferOrder pto
                         INNER JOIN SalesUnitAffiWarehouse
                    ON pto.destwarehouseid = SalesUnitAffiWarehouse.Warehouseid
                 INNER JOIN Salesunit
                    ON SalesUnitAffiWarehouse.Salesunitid = Salesunit.ID
                       inner join (select OriginalRequirementBillCode,
                                          sum(OutboundAmount) OutboundAmount,
                                          SparePartId
                                     from PartsOutboundBill pob
                                    INNER JOIN PartsOutboundBillDetail pobd
                                       ON pob.Id = pobd.PartsOutboundBillId
                                    where pob.OutboundType = 4
                                    group by OriginalRequirementBillCode,
                                             SparePartId) pob
                          on pob.OriginalRequirementBillCode = pto.Code
                       WHERE Salesunit.Partssalescategoryid =FPlannedPriceApp.Partssalescategoryid and pto.storagecompanytype=1 and pto.storagecompanyid=FPlannedPriceApp.Ownercompanyid

                                 ) tmp
                   GROUP BY tmp.sparepartid) cost
           WHERE cost.sparepartid = detail.sparepartid)
   WHERE detail.PlannedPriceAppId = priceAppId;
  /*更新配件计划价，如果不存在则新增*/
  merge INTO PartsPlannedPrice price
  USING (SELECT *
           FROM PlannedPriceAppDetail
          WHERE PlannedPriceAppId = FPlannedPriceApp.Id) detail
  ON (price.OwnerCompanyId = FPlannedPriceApp.OwnerCompanyId AND price.SparePartId = detail.SparePartId AND price.partssalescategoryid = FPlannedPriceApp.partssalescategoryid)
  WHEN matched THEN
    UPDATE
       SET price.plannedprice = detail.RequestedPrice,
           price.modifierid   = FPlannedPriceApp.Modifierid,
           price.modifiername = FPlannedPriceApp.Modifiername,
           price.modifytime   = FPlannedPriceApp.Modifytime
  WHEN NOT matched THEN
    INSERT
      (Id,
       OwnerCompanyId,
       OwnerCompanyType,
       SparePartId,
       PlannedPrice,
       CreatorId,
       CreatorName,
       CreateTime,
       PartsSalesCategoryId,
       PartsSalesCategoryName)
    VALUES
      (S_PartsPlannedPrice.Nextval,
       FPlannedPriceApp.OwnerCompanyId,
       FOwnerCompanyType,
       detail.SparePartId,
       detail.RequestedPrice,
       FPlannedPriceApp.CreatorId,
       FPlannedPriceApp.CreatorName,
       sysdate,
       FPlannedPriceApp.PartsSalesCategoryId,
       FPlannedPriceApp.PartsSalesCategoryName);
  /*更新计划价申请单*/
  UPDATE PlannedPriceApp
     SET ActualExecutionTime = SYSDATE,
         Status = 3,
         (AmountBeforeChange, AmountAfterChange, AmountDifference) =
         (SELECT SUM(PriceBeforeChange * Quantity),
                 SUM(RequestedPrice * Quantity),
                 SUM(AmountDifference)
            FROM plannedPriceAppDetail
           WHERE plannedPriceAppDetail.PlannedPriceAppId =
                 FPlannedPriceApp.Id)
   WHERE ID = FPlannedPriceApp.ID;
  /*针对每一个仓库，生成仓库成本变更单和清单*/
  insert into BILLFORWAREHOUSE
  SELECT w.Id, w.Code, w.Name, w.Type,S_WAREHOUSECOSTCHANGEBILL.NEXTVAL
      FROM Warehouse w
     INNER JOIN salesunitaffiwarehouse sw
        ON w.Id = sw.warehouseid
     INNER JOIN salesunit s
        ON sw.salesunitid = s.id
     WHERE StorageCompanyId = FPlannedPriceApp.OwnerCompanyId
       AND s.partssalescategoryid = FPlannedPriceApp.Partssalescategoryid
       AND w.Status = 1
       AND w.Type IN (1, 2);
        INSERT INTO WarehouseCostChangeDetail
      (Id,
       WarehouseCostChangeBillId,
       WarehouseId,
       SparePartId,
       SparePartCode,
       SparePartName,
       Pricebeforechange,
       Quantity,
       PriceAfterChange,
       AmountDifference)
      SELECT S_WAREHOUSECOSTCHANGEDETAIL.NEXTVAL,
            nID,
            wid,
             SparePartId,
             SparePartCode,
             SparePartName,
             PriceBeforeChange,
             stock.TotalQuantity,
             RequestedPrice,
             (RequestedPrice - NVL(PriceBeforeChange, 0)) *
             stock.TotalQuantity
        FROM (SELECT t.PartId, SUM(Quantity) AS TotalQuantity,WarehouseId
                FROM (SELECT PartId, Quantity,WarehouseId
                        FROM PartsStock
                       WHERE StorageCompanyType = 1

                         AND EXISTS (SELECT 1
                                FROM SalesUnitAffiWarehouse
                               WHERE SalesUnitAffiWarehouse.Salesunitid IN
                                     (SELECT id
                                        FROM Salesunit
                                       WHERE Salesunit.Partssalescategoryid =
                                             FPlannedPriceApp.Partssalescategoryid))
                         AND EXISTS (SELECT 1
                                FROM PlannedPriceAppDetail
                               WHERE PlannedPriceAppDetail.PlannedPriceAppId =
                                     FPlannedPriceApp.Id
                                 AND PlannedPriceAppDetail.SparePartId =
                                     PartsStock.PartId)
                      UNION ALL
                      SELECT
                       pob.SparePartId,
                       (pob.OutboundAmount - NVL((select picb.InspectedQuantity
                                                    from
                                                         (select sum(InspectedQuantity) InspectedQuantity,
                                                                 OriginalRequirementBillCode,
                                                                 sparepartid
                                                            from PartsInboundCheckBill picb
                                                           inner JOIN PartsInboundCheckBillDetail picbd
                                                              ON picb.Id =
                                                                 picbd.PartsInboundCheckBillId
                                                           where picb.InboundType = 3
                                                           group by OriginalRequirementBillCode,
                                                                    sparepartid) picb

                                                   where pob.sparepartid =
                                                         picb.sparepartid

                                                     and picb.OriginalRequirementBillCode =
                                                         pto.code),
                                                 0)),pto.DestWarehouseId
                        FROM PartsTransferOrder pto
                       inner join (select OriginalRequirementBillCode,
                                          sum(OutboundAmount) OutboundAmount,
                                          SparePartId
                                     from PartsOutboundBill pob
                                    INNER JOIN PartsOutboundBillDetail pobd
                                       ON pob.Id = pobd.PartsOutboundBillId
                                    where pob.OutboundType = 4
                                    group by OriginalRequirementBillCode,
                                             SparePartId) pob
                          on pob.OriginalRequirementBillCode = pto.Code
                       WHERE EXISTS (SELECT 1
                                FROM PlannedPriceAppDetail
                               WHERE PlannedPriceAppDetail.PlannedPriceAppId =
                                     FPlannedPriceApp.Id
                                 AND PlannedPriceAppDetail.SparePartId = pob.SparePartId)) t
               GROUP BY t.PartId,WarehouseId) stock
       INNER JOIN PlannedPriceAppDetail
          ON PlannedPriceAppDetail.SparePartId = stock.PartId
        inner join BILLFORWAREHOUSE on stock.WarehouseId=BILLFORWAREHOUSE.wid
       WHERE PlannedPriceAppDetail.PlannedPriceAppId = FPlannedPriceApp.Id;
    INSERT INTO WarehouseCostChangeBill
      (Id,
       Code,
       PlannedPriceAppId,
       OwnerCompanyId,
       OwnerCompanyCode,
       OwnerCompanyName,
       WarehouseId,
       WarehouseCode,
       WarehouseName,
       AmountBeforeChange,
       AmountAfterChange,
       AmountDifference,
       RecordStatus,
       CreatorId,
       CreatorName,
       CreateTime)
    select
      nid,
       regexp_replace(regexp_replace(FNewCode,
                                     '{CORPCODE}',
                                     FPlannedPriceApp.OwnerCompanyCode),
                      '{SERIAL}',
                      lpad(TO_CHAR(GetSerial(FComplateId,
                                             FDateFormat,
                                             FPlannedPriceApp.OwnerCompanyCode)),
                           FSerialLength,
                           '0')),
       FPlannedPriceApp.Id,
       FPlannedPriceApp.OwnerCompanyId,
       FPlannedPriceApp.OwnerCompanyCode,
       FPlannedPriceApp.OwnerCompanyName,
       wId,
       wCode,
       wName,
       (select NVL(SUM(PriceBeforechange * Quantity), 0) from WarehouseCostChangeDetail where WarehouseCostChangeDetail.Warehousecostchangebillid=BILLFORWAREHOUSE.nid),
        (select NVL(SUM(PriceAfterChange * Quantity), 0) from WarehouseCostChangeDetail where WarehouseCostChangeDetail.Warehousecostchangebillid=BILLFORWAREHOUSE.nid),
        (select  NVL(SUM(Amountdifference), 0) from WarehouseCostChangeDetail where WarehouseCostChangeDetail.Warehousecostchangebillid=BILLFORWAREHOUSE.nid),
       2,
       FPlannedPriceApp.CreatorId,
       FPlannedPriceApp.CreatorName,
       sysdate from BILLFORWAREHOUSE ;
       commit;
END;