create or replace procedure ExecuteVehPifaPriceChangeApp as
  WholesalePrice_ID integer;
  cursor PifaPriceDetails is
    select pifa.Branchid,
           pifa.Branchcode,
           pifa.Branchname,
           pifa.VehicleSalesTypeId,
           pifa.VehicleSalesTypeCode,
           pifa.VehicleSalesTypeName,
           detail.Productid,
           detail.Productcode,
           detail.Productname,
           detail.priceafterchange as Price,
           pifa.ApproverId,
           pifa.ApproverName
      from VehPifaPriceChangeApp pifa, Vehpifapricechangeappdetail detail
     where pifa.id = detail.vehretailpricechangeappid
       and pifa.status = 2
       and to_char(pifa.ExecutionDate, 'yyyy-mm-dd') = to_char(sysdate, 'yyyy-mm-dd')
     order by pifa.ExecutionDate;
  PifaPriceDetailsRow PifaPriceDetails%rowtype;
begin
  for PifaPriceDetailsRow in PifaPriceDetails loop
    begin
      select id into WholesalePrice_ID from VehicleWholesalePrice
       where ProductId = PifaPriceDetailsRow.Productid
         and VehicleSalesTypeId = PifaPriceDetailsRow.VehicleSalesTypeId;

      update VehicleWholesalePrice
         set Price = PifaPriceDetailsRow.price, ExecutionDate = sysdate,
             ModifierId = PifaPriceDetailsRow.ApproverId, ModifierName = PifaPriceDetailsRow.ApproverName, ModifyTime = sysdate
       where id = WholesalePrice_ID;
    exception
      when NO_DATA_FOUND then
        insert into VehicleWholesalePrice
          (Id, Branchid, Branchcode, Branchname,
           VehicleSalesTypeId, VehicleSalesTypeCode, VehicleSalesTypeName,
           Productid, Productcode, Productname,
           Price, Status, ExecutionDate,
           CreatorId, CreatorName, CreateTime)
        values
          (S_VehicleRetailSuggestedPrice.Nextval, PifaPriceDetailsRow.Branchid, PifaPriceDetailsRow.Branchcode, PifaPriceDetailsRow.Branchname,
           PifaPriceDetailsRow.VehicleSalesTypeId, PifaPriceDetailsRow.VehicleSalesTypeCode, PifaPriceDetailsRow.VehicleSalesTypeName,
           PifaPriceDetailsRow.Productid, PifaPriceDetailsRow.Productcode, PifaPriceDetailsRow.Productname,
           PifaPriceDetailsRow.Price, 2, sysdate,
           PifaPriceDetailsRow.ApproverId, PifaPriceDetailsRow.ApproverName, sysdate);
      when others then
        raise;
    end;
  end loop;

  update VehPifaPriceChangeApp set status = 3
   where status = 2
     and to_char(ExecutionDate, 'yyyy-mm-dd') = to_char(sysdate, 'yyyy-mm-dd');
  commit;
end;