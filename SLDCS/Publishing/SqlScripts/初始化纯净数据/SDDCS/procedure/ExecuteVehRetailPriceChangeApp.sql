create or replace procedure ExecuteVehRetailPriceChangeApp as
  RetailSuggested_id integer;
  cursor RetailPriceDetails is
    select ret.Branchid,
           ret.Branchcode,
           ret.Branchname,
           detail.Productid,
           detail.Productcode,
           detail.Productname,
           detail.priceafterchange as Price,
           ret.ApproverId,
           ret.ApproverName
      from VehRetailPriceChangeApp ret,VehRetailPriceChangeAppdetail detail
     where ret.id = detail.vehretailpricechangeappid
       and ret.status = 2
       and to_char(ret.ExecutionDate, 'yyyy-mm-dd') = to_char(sysdate, 'yyyy-mm-dd')
     order by ret.ExecutionDate;
  retailPriceDetailsRow retailPriceDetails%rowtype;
begin
  for retailPriceDetailsRow in RetailPriceDetails loop
    begin
      select id into RetailSuggested_id
        from vehicleRetailSuggestedPrice
       where ProductId = retailPriceDetailsRow.Productid;

      update vehicleRetailSuggestedPrice
         set Price = retailPriceDetailsRow.price, ExecutionTime = sysdate,
             ModifierId = retailPriceDetailsRow.ApproverId, ModifierName = retailPriceDetailsRow.ApproverName, ModifyTime = sysdate
       where id = RetailSuggested_id;
    exception
      when NO_DATA_FOUND then
        insert into VehicleRetailSuggestedPrice
          (Id, Branchid, Branchcode, Branchname,
           Productid, Productcode, Productname,
           Price, Status, Executiontime,
           CreatorId, CreatorName, CreateTime)
        values
          (S_VehicleRetailSuggestedPrice.Nextval, retailPriceDetailsRow.Branchid, retailPriceDetailsRow.Branchcode, retailPriceDetailsRow.Branchname,
           retailPriceDetailsRow.Productid, retailPriceDetailsRow.Productcode, retailPriceDetailsRow.Productname,
           retailPriceDetailsRow.Price, 2, sysdate,
           retailPriceDetailsRow.ApproverId, retailPriceDetailsRow.ApproverName, sysdate);
      when others then
        raise;
    end;
  end loop;
  update VehRetailPriceChangeApp set status = 3
   where status = 2
     and to_char(ExecutionDate, 'yyyy-mm-dd') = to_char(sysdate, 'yyyy-mm-dd');
  commit;
end;