create or replace procedure InsertIntOrderMonthlyBase is
begin
  insert into intelligentordermonthlybase
    (id,
     branchid,
     branchcode,
     branchname,
     ordertypeid,
     ordertypename,
     ifdirectprovision,
     salescategoryid,
     salescategorycode,
     salescategoryname,
     partid,
     partcode,
     partname,
     supplierid,
     suppliercode,
     suppliername,
     partabc,
     productlifecycle,
     partsbranchcreatetime,
     plannedprice,
     plannedpricetype,
     stockquantity,
     stockamount,
     businesstype,
     totalquantity,
     totalmonthlyfrequency,
     occurmonth,
     occurmonth1)

    select s_intelligentordermonthlybase.nextval,
           branchid,
           branchcode,
           branchname,
           ordertypeid,
           ordertypename,
           ifdirectprovision,
           salescategoryid,
           salescategorycode,
           salescategoryname,
           partid,
           partcode,
           partname,
           partssupplierid supplierid,
           suppliercode,
           suppliername,
           partabc,
           productlifecycle,
           partsbranchcreatetime,
           plannedprice,
           plannedpricetype,
           stockquantity,
           stockamount,
           businesstype,
           totalquantity,
           totalmonthlyfrequency,
           occurmonth,
           occurmonth1
      from ---------------------------------------------销售出库--------------------------
            (select a.branchid,
                    bh.code branchcode,
                    bh.name branchname,
                    aa.partssalesordertypeid ordertypeid,
                    aa.partssalesordertypename ordertypename,
                    aa.ifdirectprovision,
                    su.partssalescategoryid salescategoryid,
                    ps.code salescategorycode,
                    ps.name salescategoryname,
                    a.partid,
                    sp.code partcode,
                    sp.name partname,
                    null as partssupplierid,
                    null suppliercode,
                    null suppliername,
                    null partabc,
                    null productlifecycle,
                    pb.createtime partsbranchcreatetime,
                    (select partsplannedprice.plannedprice
                       from partsplannedprice
                      where partsplannedprice.sparepartid = a.partid
                        and partsplannedprice.partssalescategoryid =
                            su.partssalescategoryid
                        and partsplannedprice.ownercompanyid = a.branchid) plannedprice,
                    null plannedpricetype,
                    sum(nvl(a.quantity, 0)) stockquantity,
                    sum(nvl(a.quantity, 0) *
                        nvl((select partsplannedprice.plannedprice
                              from partsplannedprice
                             where partsplannedprice.sparepartid = a.partid
                               and partsplannedprice.partssalescategoryid =
                                   su.partssalescategoryid
                               and partsplannedprice.ownercompanyid =
                                   a.branchid),
                            0)) stockamount,
                    2 businesstype,
                    sum(nvl(aa.totalquantity, 0)) totalquantity,
                    sum(nvl(aa.totalmonthlyfrequency, 0)) totalmonthlyfrequency,
                    a.occurmonth,
                    to_date(sysdate - 1, 'yyyy-mm') occurmonth1
               from (select p.branchid,
                            p.storagecompanyid,
                            p.partid,
                            -- p.partssalescategoryid,
                            p.warehouseid,
                            sum(p.quantity) quantity,
                            to_char(sysdate-1, 'yyyy-mm') occurmonth /*,
                                    sum(p.quantity * p.plannedprice)*/
                       from partsstock p where exists
                    (select 1
                             from warehouse
                            where warehouse.id = p.warehouseid
                              and( (warehouse.isqualitywarehouse = 0 )or (warehouse.isqualitywarehouse  is null)))
                      group by p.branchid,
                               p.storagecompanyid,
                               p.partid,
                               --p.partssalescategoryid,
                               p.warehouseid) a
              inner join salesunitaffiwarehouse sw
                 on sw.warehouseid = a.warehouseid
              inner join salesunit su
                 on su.id = sw.salesunitid
              inner join branch bh
                 on bh.id = a.branchid
              inner join partssalescategory ps
                 on ps.id = su.partssalescategoryid
              inner join sparepart sp
                 on sp.id = a.partid
              inner join partsbranch pb
                 on pb.partid = a.partid
                and pb.partssalescategoryid = su.partssalescategoryid
               left join (select b.branchid,
                                b.branchcode,
                                b.branchname,
                                e.partssalescategoryid,
                                e.partssalescategoryname,
                                d.partssalesordertypeid,
                                d.partssalesordertypename,
                                to_char(b.createtime, 'yyyy-mm') createtime,
                                c.sparepartid,
                                c.sparepartcode,
                                c.sparepartname,
                                d.ifdirectprovision,
                                sum(c.outboundamount) totalquantity,
                                count(c.sparepartcode) totalmonthlyfrequency
                           from partsoutboundbill b
                          inner join partsoutboundbilldetail c
                             on c.partsoutboundbillid = b.id
                          inner join partssalesorder d
                             on d.code = b.originalrequirementbillcode
                          inner join partsbranch e
                             on e.partid = c.sparepartid
                            and e.partssalescategoryid =
                                b.partssalescategoryid
                          where b.outboundtype = 1
                            and b.storagecompanyid = 2
                          group by b.branchid,
                                   b.branchcode,
                                   b.branchname,
                                   e.partssalescategoryid,
                                   e.partssalescategoryname,
                                   d.partssalesordertypeid,
                                   d.partssalesordertypename,
                                   to_char(b.createtime, 'yyyy-mm'),
                                   c.sparepartid,
                                   c.sparepartcode,
                                   c.sparepartname,
                                   d.ifdirectprovision) aa
                 on aa.sparepartid = a.partid
                and aa.partssalescategoryid = su.partssalescategoryid
                and aa.createtime = a.occurmonth
             ---------------------------------
              where a.storagecompanyid = a.branchid
              group by a.branchid,
                       bh.code,
                       bh.name,
                       aa.partssalesordertypeid,
                       aa.partssalescategoryname,
                       aa.ifdirectprovision,
                       su.partssalescategoryid,
                       ps.code,
                       ps.name,
                       a.partid,
                       sp.code,
                       sp.name,
                       pb.createtime,
                       a.occurmonth
             ---------------------------------------------------------------------统购调拨-----------------
             union all
             select a.branchid,
                    bh.code branchcode,
                    bh.name branchname,
                    aa.partssalesordertypeid ordertypeid,
                    aa.partssalesordertypename ordertypename,
                    aa.ifdirectprovision,
                    su.partssalescategoryid salescategoryid,
                    ps.code salescategorycode,
                    ps.name salescategoryname,
                    a.partid,
                    sp.code partcode,
                    sp.name partname,
                    null as partssupplierid,
                    null suppliercode,
                    null suppliername,
                    null partabc,
                    null productlifecycle,
                    pb.createtime partsbranchcreatetime,
                    (select partsplannedprice.plannedprice
                       from partsplannedprice
                      where partsplannedprice.sparepartid = a.partid
                        and partsplannedprice.partssalescategoryid =
                            su.partssalescategoryid
                        and partsplannedprice.ownercompanyid = a.branchid) plannedprice,
                    null plannedpricetype,
                    sum(nvl(a.quantity, 0)) stockquantity,
                    sum(nvl(a.quantity, 0) *
                        nvl((select partsplannedprice.plannedprice
                              from partsplannedprice
                             where partsplannedprice.sparepartid = a.partid
                               and partsplannedprice.partssalescategoryid =
                                   su.partssalescategoryid
                               and partsplannedprice.ownercompanyid =
                                   a.branchid),
                            0)) stockamount,
                    3 businesstype,
                    sum(nvl(aa.totalquantity, 0)) totalquantity,
                    sum(nvl(aa.totalmonthlyfrequency, 0)) totalmonthlyfrequency,
                    a.occurmonth,
                    to_date(sysdate - 1, 'yyyy-mm') occurmonth1

               from (select p.branchid,
                            p.storagecompanyid,
                            p.partid,
                            -- p.partssalescategoryid,
                            p.warehouseid,
                            sum(p.quantity) quantity,
                            to_char(sysdate - 1, 'yyyy-mm') occurmonth /*,
                                    sum(p.quantity * p.plannedprice)*/
                       from partsstock p  where exists
                    (select 1
                             from warehouse
                            where warehouse.id = p.warehouseid
                              and( (warehouse.isqualitywarehouse = 0 )or (warehouse.isqualitywarehouse  is null)))
                      group by p.branchid,
                               p.storagecompanyid,
                               p.partid,
                               -- p.partssalescategoryid,
                               p.warehouseid) a
              inner join salesunitaffiwarehouse sw
                 on sw.warehouseid = a.warehouseid
              inner join salesunit su
                 on su.id = sw.salesunitid
              inner join branch bh
                 on bh.id = a.branchid
              inner join partssalescategory ps
                 on ps.id = su.partssalescategoryid
              inner join sparepart sp
                 on sp.id = a.partid
              inner join partsbranch pb
                 on pb.partid = a.partid
                and pb.partssalescategoryid = su.partssalescategoryid
               left join (select b.branchid,
                                 b.branchcode,
                                 b.branchname,
                                 e.partssalescategoryid,
                                 e.partssalescategoryname,
                                 null partssalesordertypeid,
                                 null partssalesordertypename,
                                 to_char(b.createtime, 'yyyy-mm') createtime,
                                 c.sparepartid,
                                 c.sparepartcode,
                                 c.sparepartname,
                                 null ifdirectprovision,
                                 sum(c.outboundamount) totalquantity,
                                 count(c.sparepartcode) totalmonthlyfrequency
                            from partsoutboundbill b
                           inner join partsoutboundbilldetail c
                              on c.partsoutboundbillid = b.id
                           inner join partsbranch e
                              on e.partid = c.sparepartid
                             and e.partssalescategoryid =
                                 b.partssalescategoryid
                           where b.outboundtype = 4
                             and b.storagecompanyid = 2
                             and b.partssalescategoryid =
                                 (select id
                                    from partssalescategory
                                   where name = '统购')
                           group by b.branchid,
                                    b.branchcode,
                                    b.branchname,
                                    e.partssalescategoryid,
                                    e.partssalescategoryname,
                                    to_char(b.createtime, 'yyyy-mm'),
                                    c.sparepartid,
                                    c.sparepartcode,
                                    c.sparepartname) aa
                 on aa.sparepartid = a.partid
                and aa.partssalescategoryid = su.partssalescategoryid
                and aa.createtime = a.occurmonth
              where a.storagecompanyid = a.branchid
              group by a.branchid,
                       bh.code,
                       bh.name,
                       aa.partssalesordertypeid,
                       aa.partssalescategoryname,
                       aa.ifdirectprovision,
                       su.partssalescategoryid,
                       ps.code,
                       ps.name,
                       a.partid,
                       sp.code,
                       sp.name,
                       pb.createtime,
                       a.occurmonth
             -------------------------------------------------------------采购入库----------------------------------
             union all

             select a.branchid,
                    bh.code branchcode,
                    bh.name branchname,
                    aa.partssalesordertypeid ordertypeid,
                    aa.partssalesordertypename ordertypename,
                    aa.ifdirectprovision,
                    su.partssalescategoryid salescategoryid,
                    ps.code salescategorycode,
                    ps.name salescategoryname,
                    a.partid,
                    sp.code partcode,
                    sp.name partname,
                    aa.PartsSupplierId,
                    aa.PartsSupplierCode suppliercode,
                    aa.PartsSupplierName suppliername,
                    null partabc,
                    null productlifecycle,
                    pb.createtime partsbranchcreatetime,
                    (select partsplannedprice.plannedprice
                       from partsplannedprice
                      where partsplannedprice.sparepartid = a.partid
                        and partsplannedprice.partssalescategoryid =
                            su.partssalescategoryid
                        and partsplannedprice.ownercompanyid = a.branchid) plannedprice,
                    null plannedpricetype,
                    sum(nvl(a.quantity, 0)) stockquantity,
                    sum(nvl(a.quantity, 0) *
                        nvl((select partsplannedprice.plannedprice
                              from partsplannedprice
                             where partsplannedprice.sparepartid = a.partid
                               and partsplannedprice.partssalescategoryid =
                                   su.partssalescategoryid
                               and partsplannedprice.ownercompanyid =
                                   a.branchid),
                            0)) stockamount,
                    1 businesstype,
                    sum(nvl(aa.totalquantity, 0)) totalquantity,
                    sum(nvl(aa.totalmonthlyfrequency, 0)) totalmonthlyfrequency,
                    a.occurmonth,
                    to_date(sysdate - 1, 'yyyy-mm') occurmonth1

               from (select p.branchid,
                            p.storagecompanyid,
                            p.partid,
                            -- p.partssalescategoryid,
                            p.warehouseid,
                            sum(p.quantity) quantity,
                            to_char(sysdate - 1, 'yyyy-mm') occurmonth /*,
                                    sum(p.quantity * p.plannedprice)*/
                       from partsstock p  where exists
                    (select 1
                             from warehouse
                            where warehouse.id = p.warehouseid
                              and( (warehouse.isqualitywarehouse = 0 )or (warehouse.isqualitywarehouse  is null))) /* where p.snapshotime =
                     to_date('2014-11-30 23:59:59', 'yyyy-mm-dd hh24:mi:ss')*/
                      group by p.branchid,
                               p.storagecompanyid,
                               p.partid,
                               -- p.partssalescategoryid,
                               p.warehouseid) a
              inner join salesunitaffiwarehouse sw
                 on sw.warehouseid = a.warehouseid
              inner join salesunit su
                 on su.id = sw.salesunitid
              inner join branch bh
                 on bh.id = a.branchid
              inner join partssalescategory ps
                 on ps.id = su.partssalescategoryid
              inner join sparepart sp
                 on sp.id = a.partid
              inner join partsbranch pb
                 on pb.partid = a.partid
                and pb.partssalescategoryid = su.partssalescategoryid
               left join (select b.branchid,
                                 b.branchcode,
                                 b.branchname,
                                 e.partssalescategoryid,
                                 e.partssalescategoryname,
                                 d.partspurchaseordertypeid partssalesordertypeid,
                                 ppt.name partssalesordertypename,
                                 to_char(b.createtime, 'yyyy-mm') createtime,
                                 c.sparepartid,
                                 c.sparepartcode,
                                 c.sparepartname,
                                 d.ifdirectprovision,
                                 sum(c.inspectedquantity) totalquantity,
                                 count(c.sparepartcode) totalmonthlyfrequency,
                                 d.PartsSupplierId,
                                 d.PartsSupplierCode,
                                 d.PartsSupplierName
                            from partsinboundcheckbill b
                           inner join partsinboundcheckbilldetail c
                              on c.partsinboundcheckbillid = b.id
                           inner join partspurchaseorder d
                              on d.code = b.originalrequirementbillcode
                           inner join partsbranch e
                              on e.partid = c.sparepartid
                             and e.partssalescategoryid =
                                 b.partssalescategoryid
                           inner join partspurchaseordertype ppt
                              on ppt.id = d.partspurchaseordertypeid
                           where b.inboundtype = 1
                             and b.storagecompanyid = 2
                           group by b.branchid,
                                    b.branchcode,
                                    b.branchname,
                                    e.partssalescategoryid,
                                    e.partssalescategoryname,
                                    d.partspurchaseordertypeid,
                                    ppt.name,
                                    to_char(b.createtime, 'yyyy-mm'),
                                    c.sparepartid,
                                    c.sparepartcode,
                                    c.sparepartname,
                                    d.ifdirectprovision,
                                    d.PartsSupplierId,
                                    d.PartsSupplierCode,
                                    d.PartsSupplierName) aa
                 on aa.sparepartid = a.partid
                and aa.partssalescategoryid = su.partssalescategoryid
                and aa.createtime = a.occurmonth
              where a.storagecompanyid = a.branchid
              group by a.branchid,
                       bh.code,
                       bh.name,
                       aa.partssalesordertypeid,
                       aa.partssalescategoryname,
                       aa.ifdirectprovision,
                       su.partssalescategoryid,
                       ps.code,
                       ps.name,
                       a.partid,
                       sp.code,
                       sp.name,
                       aa.PartsSupplierId,
                       aa.PartsSupplierCode,
                       aa.PartsSupplierName,
                       pb.createtime,
                       a.occurmonth);

end InsertIntOrderMonthlyBase;