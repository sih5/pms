create or replace procedure Prc_IntelligentOrderWeekly is
begin
  insert into IntelligentOrderWeekly
    (id,
    branchid,
    branchcode,
    branchname,
    ordertypename,
    salescategoryid,
    salescategorycode,
    salescategoryname,
    partid,
    partcode,
    partname,
    supplierid,
    suppliercode,
    suppliername,
    partabc,
    productlifecycle,
    plannedpricetype,
    ORDERQUANTITY1,
    ORDERQUANTITY2,
    ORDERQUANTITY3,
    ORDERQUANTITY4,
    ORDERQUANTITY5,
    ORDERQUANTITY6,
    ORDERQUANTITY7,
    ORDERQUANTITY8,
    ORDERQUANTITY9,
    ORDERQUANTITY10,
    ORDERQUANTITY11,
    ORDERQUANTITY12,
    ORDERQUANTITY13,
    ORDERQUANTITY14,
    ORDERQUANTITY15,
    ORDERQUANTITY16,
    ORDERQUANTITY17,
    ORDERQUANTITY18,
    ORDERQUANTITY19,
    ORDERQUANTITY20,
    ORDERQUANTITY21,
    ORDERQUANTITY22,
    ORDERQUANTITY23,
    ORDERQUANTITY24,
    ORDERQUANTITY25,
    ORDERQUANTITY26,
    ORDERQUANTITY27,
    ORDERQUANTITY28,
    ORDERQUANTITY29,
    ORDERQUANTITY30,
    ORDERQUANTITY31,
    ORDERQUANTITY32,
    ORDERQUANTITY33,
    ORDERQUANTITY34,
    ORDERQUANTITY35,
    ORDERQUANTITY36,
    ORDERQUANTITY37,
    ORDERQUANTITY38,
    ORDERQUANTITY39,
    ORDERQUANTITY40,
    ORDERQUANTITY41,
    ORDERQUANTITY42,
    ORDERQUANTITY43,
    ORDERQUANTITY44,
    ORDERQUANTITY45,
    ORDERQUANTITY46,
    ORDERQUANTITY47,
    ORDERQUANTITY48,
    ORDERQUANTITY49,
    ORDERQUANTITY50,
    ORDERQUANTITY51,
    ORDERQUANTITY52,
    AVG
     )
    select S_intelligentordermonthly.Nextval,
    t1.BRANCHID,
    t1.BRANCHCODE,t1.BRANCHNAME,t1.ORDERTYPENAME,
    t1.SALESCATEGORYID,t1.SALESCATEGORYCODE,t1.SALESCATEGORYNAME,t1.PARTID,t1.PARTCODE,t1.PARTNAME,
    t1.SUPPLIERID,t1.SUPPLIERCODE,t1.SUPPLIERNAME,
    t1.PARTABC,t1.PRODUCTLIFECYCLE,t1.PLANNEDPRICETYPE,
    t1.qty,t2.qty,t3.qty,t4.qty,t5.qty,t6.qty,t7.qty,
    t8.qty,t9.qty,t10.qty,t11.qty,t12.qty,t13.qty,t14.qty,
    t15.qty,t16.qty,t17.qty,t18.qty,t19.qty,t20.qty,t21.qty,
    t22.qty,t23.qty,t24.qty,t25.qty,t26.qty,t27.qty,t28.qty,
    t29.qty,t30.qty,t31.qty,t32.qty,t33.qty,t34.qty,t35.qty，
    t36.qty,t37.qty,t38.qty,t39.qty,t40.qty,t41.qty,t42.qty,
    t43.qty,t44.qty,t45.qty,t46.qty,t47.qty,t48.qty,t49.qty,
    t50.qty,t51.qty,t52.qty,round（nvl(w13.qty,0)+nvl(w26.qty,0)+nvl(w52.qty,0),2） as avg
     FROM
       --T-1周销量
    (select a.BRANCHID,
    a.BRANCHCODE,a.BRANCHNAME,a.SALESCATEGORYID,a.SALESCATEGORYCODE,a.SALESCATEGORYNAME,a.PARTID,a.PARTCODE,a.PARTNAME,
    a.PARTABC,a.PRODUCTLIFECYCLE,a.PLANNEDPRICETYPE,b.SupplierId,c.code as SUPPLIERCODE,c.name as SUPPLIERNAME,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a  left join PartsSupplierRelation b
      left join PartsSupplier c on b.supplierid=c.id
     on a.branchid=b.Branchid and b.Partssalescategoryid=a.salescategoryid
     and a.partid=b.Partid and b.IsPrimary=1
    where    to_char(sysdate-7, 'yyyy') =a.yeard and  to_char(sysdate-7, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by  a.BRANCHID, a.BRANCHCODE,a.BRANCHNAME,a.SALESCATEGORYID,a.SALESCATEGORYCODE,a.SALESCATEGORYNAME,a.PARTID,a.PARTCODE,a.PARTNAME,
    a.PARTABC,a.PRODUCTLIFECYCLE,a.PLANNEDPRICETYPE,b.SupplierId,c.code,c.name,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t1
      left join
       --T-2周销量
    (select a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-14, 'yyyy') =a.yeard and  to_char(sysdate-14, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t2 on t1.BRANCHID=t2.BRANCHID and t1.SALESCATEGORYID=t2.SALESCATEGORYID and t1.PARTID=t2.PARTID and t1.ORDERTYPENAME=t2.ORDERTYPENAME
       left join
       --T-3周销量
    (select a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-21, 'yyyy') =a.yeard and  to_char(sysdate-21, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t3 on t1.BRANCHID=t3.BRANCHID and t1.SALESCATEGORYID=t3.SALESCATEGORYID and t1.PARTID=t3.PARTID and t1.ORDERTYPENAME=t3.ORDERTYPENAME
       left join
       --T-4周销量
    (select a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-28, 'yyyy') =a.yeard and  to_char(sysdate-28, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t4 on t1.BRANCHID=t4.BRANCHID and t1.SALESCATEGORYID=t4.SALESCATEGORYID and t1.PARTID=t4.PARTID   and t1.ORDERTYPENAME=t4.ORDERTYPENAME
       left join
       --T-5周销量
    (select a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-35, 'yyyy') =a.yeard and  to_char(sysdate-35, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t5 on t1.BRANCHID=t5.BRANCHID and t1.SALESCATEGORYID=t5.SALESCATEGORYID and t1.PARTID=t5.PARTID   and t1.ORDERTYPENAME=t5.ORDERTYPENAME
    left join
    --T-6周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-42, 'yyyy') =a.yeard and  to_char(sysdate-42, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t6 on t1.BRANCHID=t6.BRANCHID and t1.SALESCATEGORYID=t6.SALESCATEGORYID and t1.PARTID=t6.PARTID  and t1.ORDERTYPENAME=t6.ORDERTYPENAME
    left join
    --T-7周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-49, 'yyyy') =a.yeard and  to_char(sysdate-49, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t7 on t1.BRANCHID=t7.BRANCHID and t1.SALESCATEGORYID=t7.SALESCATEGORYID and t1.PARTID=t7.PARTID and t1.ORDERTYPENAME=t7.ORDERTYPENAME
    left join
    --T-8周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-56, 'yyyy') =a.yeard and  to_char(sysdate-56, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by    a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t8 on t1.BRANCHID=t8.BRANCHID and t1.SALESCATEGORYID=t8.SALESCATEGORYID and t1.PARTID=t8.PARTID and t1.ORDERTYPENAME=t8.ORDERTYPENAME
      left join
    --T-9周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-63, 'yyyy') =a.yeard and  to_char(sysdate-63, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t9 on t1.BRANCHID=t9.BRANCHID and t1.SALESCATEGORYID=t9.SALESCATEGORYID and t1.PARTID=t9.PARTID and t1.ORDERTYPENAME=t9.ORDERTYPENAME
      left join
    --T-10周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-70, 'yyyy') =a.yeard and  to_char(sysdate-70, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t10 on t1.BRANCHID=t10.BRANCHID and t1.SALESCATEGORYID=t10.SALESCATEGORYID and t1.PARTID=t10.PARTID and t1.ORDERTYPENAME=t10.ORDERTYPENAME
    left join
    --T-11周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-77, 'yyyy') =a.yeard and  to_char(sysdate-77, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t11 on t1.BRANCHID=t11.BRANCHID and t1.SALESCATEGORYID=t11.SALESCATEGORYID and t1.PARTID=t11.PARTID  and t1.ORDERTYPENAME=t11.ORDERTYPENAME
    left join
    --T-12周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-84, 'yyyy') =a.yeard and  to_char(sysdate-84, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t12 on t1.BRANCHID=t12.BRANCHID and t1.SALESCATEGORYID=t12.SALESCATEGORYID and t1.PARTID=t12.PARTID and t1.ORDERTYPENAME=t12.ORDERTYPENAME
    left join
    --T-13周销量
    (select a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-91, 'yyyy') =a.yeard and  to_char(sysdate-91, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t13 on t1.BRANCHID=t13.BRANCHID and t1.SALESCATEGORYID=t13.SALESCATEGORYID and t1.PARTID=t13.PARTID and t1.ORDERTYPENAME=t13.ORDERTYPENAME
    left join
    --T-14周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-98, 'yyyy') =a.yeard and  to_char(sysdate-98, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t14 on t1.BRANCHID=t14.BRANCHID and t1.SALESCATEGORYID=t14.SALESCATEGORYID and t1.PARTID=t14.PARTID and t1.ORDERTYPENAME=t14.ORDERTYPENAME
    left join
    --T-15周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-105, 'yyyy') =a.yeard and  to_char(sysdate-105, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t15 on t1.BRANCHID=t15.BRANCHID and t1.SALESCATEGORYID=t15.SALESCATEGORYID and t1.PARTID=t15.PARTID and t1.ORDERTYPENAME=t15.ORDERTYPENAME
    left join
    --T-16周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-112, 'yyyy') =a.yeard and  to_char(sysdate-112, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t16 on t1.BRANCHID=t16.BRANCHID and t1.SALESCATEGORYID=t16.SALESCATEGORYID and t1.PARTID=t16.PARTID and t1.ORDERTYPENAME=t16.ORDERTYPENAME
    left join
    --T-17周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-119, 'yyyy') =a.yeard and  to_char(sysdate-119, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t17 on t1.BRANCHID=t17.BRANCHID and t1.SALESCATEGORYID=t17.SALESCATEGORYID and t1.PARTID=t17.PARTID and t1.ORDERTYPENAME=t17.ORDERTYPENAME
     left join
    --T-18周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-126, 'yyyy') =a.yeard and  to_char(sysdate-126, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t18 on t1.BRANCHID=t18.BRANCHID and t1.SALESCATEGORYID=t18.SALESCATEGORYID and t1.PARTID=t18.PARTID and t1.ORDERTYPENAME=t18.ORDERTYPENAME
      left join
    --T-19周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-133, 'yyyy') =a.yeard and  to_char(sysdate-133, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t19 on t1.BRANCHID=t19.BRANCHID and t1.SALESCATEGORYID=t19.SALESCATEGORYID and t1.PARTID=t19.PARTID and t1.ORDERTYPENAME=t19.ORDERTYPENAME
       left join
    --T-20周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-140, 'yyyy') =a.yeard and  to_char(sysdate-140, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t20 on t1.BRANCHID=t20.BRANCHID and t1.SALESCATEGORYID=t20.SALESCATEGORYID and t1.PARTID=t20.PARTID and t1.ORDERTYPENAME=t20.ORDERTYPENAME
        left join
    --T-21周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-147, 'yyyy') =a.yeard and  to_char(sysdate-147, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t21 on t1.BRANCHID=t21.BRANCHID and t1.SALESCATEGORYID=t21.SALESCATEGORYID and t1.PARTID=t21.PARTID and t1.ORDERTYPENAME=t21.ORDERTYPENAME
        left join
    --T-22周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-154, 'yyyy') =a.yeard and  to_char(sysdate-154, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t22 on t1.BRANCHID=t22.BRANCHID and t1.SALESCATEGORYID=t22.SALESCATEGORYID and t1.PARTID=t22.PARTID and t1.ORDERTYPENAME=t22.ORDERTYPENAME
        left join
    --T-23周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-161, 'yyyy') =a.yeard and  to_char(sysdate-161, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t23 on t1.BRANCHID=t23.BRANCHID and t1.SALESCATEGORYID=t23.SALESCATEGORYID and t1.PARTID=t23.PARTID and t1.ORDERTYPENAME=t23.ORDERTYPENAME
        left join
    --T-24周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-168, 'yyyy') =a.yeard and  to_char(sysdate-168, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t24 on t1.BRANCHID=t24.BRANCHID and t1.SALESCATEGORYID=t24.SALESCATEGORYID and t1.PARTID=t24.PARTID and t1.ORDERTYPENAME=t24.ORDERTYPENAME
        left join
    --T-25周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-175, 'yyyy') =a.yeard and  to_char(sysdate-175, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t25 on t1.BRANCHID=t25.BRANCHID and t1.SALESCATEGORYID=t25.SALESCATEGORYID and t1.PARTID=t25.PARTID and t1.ORDERTYPENAME=t25.ORDERTYPENAME
         left join
    --T-26周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-182, 'yyyy') =a.yeard and  to_char(sysdate-182, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t26 on t1.BRANCHID=t26.BRANCHID and t1.SALESCATEGORYID=t26.SALESCATEGORYID and t1.PARTID=t26.PARTID and t1.ORDERTYPENAME=t26.ORDERTYPENAME
          left join
    --T-27周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-189, 'yyyy') =a.yeard and  to_char(sysdate-189, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t27 on t1.BRANCHID=t27.BRANCHID and t1.SALESCATEGORYID=t27.SALESCATEGORYID and t1.PARTID=t27.PARTID and t1.ORDERTYPENAME=t27.ORDERTYPENAME
          left join
    --T-28周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-196, 'yyyy') =a.yeard and  to_char(sysdate-196, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t28 on t1.BRANCHID=t28.BRANCHID and t1.SALESCATEGORYID=t28.SALESCATEGORYID and t1.PARTID=t28.PARTID and t1.ORDERTYPENAME=t28.ORDERTYPENAME
          left join
    --T-29周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-203, 'yyyy') =a.yeard and  to_char(sysdate-203, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t29 on t1.BRANCHID=t29.BRANCHID and t1.SALESCATEGORYID=t29.SALESCATEGORYID and t1.PARTID=t29.PARTID and t1.ORDERTYPENAME=t29.ORDERTYPENAME
          left join
    --T-30周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-210, 'yyyy') =a.yeard and  to_char(sysdate-210, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t30 on t1.BRANCHID=t30.BRANCHID and t1.SALESCATEGORYID=t30.SALESCATEGORYID and t1.PARTID=t30.PARTID and t1.ORDERTYPENAME=t30.ORDERTYPENAME
          left join
    --T-31周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-217, 'yyyy') =a.yeard and  to_char(sysdate-217, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t31 on t1.BRANCHID=t31.BRANCHID and t1.SALESCATEGORYID=t31.SALESCATEGORYID and t1.PARTID=t31.PARTID and t1.ORDERTYPENAME=t31.ORDERTYPENAME
           left join
    --T-32周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-224, 'yyyy') =a.yeard and  to_char(sysdate-224, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t32 on t1.BRANCHID=t32.BRANCHID and t1.SALESCATEGORYID=t32.SALESCATEGORYID and t1.PARTID=t32.PARTID and t1.ORDERTYPENAME=t32.ORDERTYPENAME
              left join
    --T-33周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-231, 'yyyy') =a.yeard and  to_char(sysdate-231, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t33 on t1.BRANCHID=t33.BRANCHID and t1.SALESCATEGORYID=t33.SALESCATEGORYID and t1.PARTID=t33.PARTID and t1.ORDERTYPENAME=t33.ORDERTYPENAME
           left join
        --T-34周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-238, 'yyyy') =a.yeard and  to_char(sysdate-238, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t34 on t1.BRANCHID=t34.BRANCHID and t1.SALESCATEGORYID=t34.SALESCATEGORYID and t1.PARTID=t34.PARTID and t1.ORDERTYPENAME=t34.ORDERTYPENAME
          left join
        --T-35周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-245, 'yyyy') =a.yeard and  to_char(sysdate-245, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t35 on t1.BRANCHID=t35.BRANCHID and t1.SALESCATEGORYID=t35.SALESCATEGORYID and t1.PARTID=t35.PARTID and t1.ORDERTYPENAME=t35.ORDERTYPENAME
        left join
        --T-36周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-252, 'yyyy') =a.yeard and  to_char(sysdate-252, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t36 on t1.BRANCHID=t36.BRANCHID and t1.SALESCATEGORYID=t36.SALESCATEGORYID and t1.PARTID=t36.PARTID and t1.ORDERTYPENAME=t36.ORDERTYPENAME
        left join
        --T-37周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-259, 'yyyy') =a.yeard and  to_char(sysdate-259, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t37 on t1.BRANCHID=t37.BRANCHID and t1.SALESCATEGORYID=t37.SALESCATEGORYID and t1.PARTID=t37.PARTID and t1.ORDERTYPENAME=t37.ORDERTYPENAME
        left join
        --T-38周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-266, 'yyyy') =a.yeard and  to_char(sysdate-266, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t38 on t1.BRANCHID=t38.BRANCHID and t1.SALESCATEGORYID=t38.SALESCATEGORYID and t1.PARTID=t38.PARTID and t1.ORDERTYPENAME=t38.ORDERTYPENAME
        left join
        --T-39周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-273, 'yyyy') =a.yeard and  to_char(sysdate-273, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t39 on t1.BRANCHID=t39.BRANCHID and t1.SALESCATEGORYID=t39.SALESCATEGORYID and t1.PARTID=t39.PARTID and t1.ORDERTYPENAME=t39.ORDERTYPENAME
        left join
        --T-40周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-280, 'yyyy') =a.yeard and  to_char(sysdate-280, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t40 on t1.BRANCHID=t40.BRANCHID and t1.SALESCATEGORYID=t40.SALESCATEGORYID and t1.PARTID=t40.PARTID and t1.ORDERTYPENAME=t40.ORDERTYPENAME
         left join
     --T-41周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-287, 'yyyy') =a.yeard and  to_char(sysdate-287, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t41 on t1.BRANCHID=t41.BRANCHID and t1.SALESCATEGORYID=t41.SALESCATEGORYID and t1.PARTID=t41.PARTID and t1.ORDERTYPENAME=t41.ORDERTYPENAME
         left join
     --T-42周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-294, 'yyyy') =a.yeard and  to_char(sysdate-294, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t42 on t1.BRANCHID=t42.BRANCHID and t1.SALESCATEGORYID=t42.SALESCATEGORYID and t1.PARTID=t42.PARTID and t1.ORDERTYPENAME=t42.ORDERTYPENAME
         left join
     --T-43周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-301, 'yyyy') =a.yeard and  to_char(sysdate-301, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t43 on t1.BRANCHID=t43.BRANCHID and t1.SALESCATEGORYID=t43.SALESCATEGORYID and t1.PARTID=t43.PARTID and t1.ORDERTYPENAME=t43.ORDERTYPENAME
         left join
     --T-44周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-308, 'yyyy') =a.yeard and  to_char(sysdate-308, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t44 on t1.BRANCHID=t44.BRANCHID and t1.SALESCATEGORYID=t44.SALESCATEGORYID and t1.PARTID=t44.PARTID and t1.ORDERTYPENAME=t44.ORDERTYPENAME
          left join
     --T-45周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-315, 'yyyy') =a.yeard and  to_char(sysdate-315, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t45 on t1.BRANCHID=t45.BRANCHID and t1.SALESCATEGORYID=t45.SALESCATEGORYID and t1.PARTID=t45.PARTID and t1.ORDERTYPENAME=t45.ORDERTYPENAME
          left join
     --T-46周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-322, 'yyyy') =a.yeard and  to_char(sysdate-322, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t46 on t1.BRANCHID=t46.BRANCHID and t1.SALESCATEGORYID=t46.SALESCATEGORYID and t1.PARTID=t46.PARTID and t1.ORDERTYPENAME=t46.ORDERTYPENAME
          left join
     --T-47周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-329, 'yyyy') =a.yeard and  to_char(sysdate-329, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t47 on t1.BRANCHID=t47.BRANCHID and t1.SALESCATEGORYID=t47.SALESCATEGORYID and t1.PARTID=t47.PARTID and t1.ORDERTYPENAME=t47.ORDERTYPENAME
          left join
     --T-48周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-336, 'yyyy') =a.yeard and  to_char(sysdate-336, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t48 on t1.BRANCHID=t48.BRANCHID and t1.SALESCATEGORYID=t48.SALESCATEGORYID and t1.PARTID=t48.PARTID and t1.ORDERTYPENAME=t48.ORDERTYPENAME
          left join
     --T-49周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-343, 'yyyy') =a.yeard and  to_char(sysdate-343, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t49 on t1.BRANCHID=t49.BRANCHID and t1.SALESCATEGORYID=t49.SALESCATEGORYID and t1.PARTID=t49.PARTID and t1.ORDERTYPENAME=t49.ORDERTYPENAME
          left join
     --T-50周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-350, 'yyyy') =a.yeard and  to_char(sysdate-350, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t50 on t1.BRANCHID=t50.BRANCHID and t1.SALESCATEGORYID=t50.SALESCATEGORYID and t1.PARTID=t50.PARTID and t1.ORDERTYPENAME=t50.ORDERTYPENAME
          left join
     --T-51周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-357, 'yyyy') =a.yeard and  to_char(sysdate-357, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t51 on t1.BRANCHID=t51.BRANCHID and t1.SALESCATEGORYID=t51.SALESCATEGORYID and t1.PARTID=t51.PARTID and t1.ORDERTYPENAME=t51.ORDERTYPENAME
          left join
     --T-52周销量
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty
     from  IntelligentOrderWeeklyBase a
    where    to_char(sysdate-364, 'yyyy') =a.yeard and  to_char(sysdate-364, 'ww') =a.weeklyd  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t52 on t1.BRANCHID=t52.BRANCHID and t1.SALESCATEGORYID=t52.SALESCATEGORYID and t1.PARTID=t52.PARTID and t1.ORDERTYPENAME=t52.ORDERTYPENAME
      left join
     --近13周周均
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY)*0.5/13 as qty
     from  IntelligentOrderWeeklyBase a
    where  thedate>=trunc(sysdate,'iw')-84 and thedate<=trunc(sysdate,'iw')
     and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) w13 on t1.BRANCHID=w13.BRANCHID and t1.SALESCATEGORYID=w13.SALESCATEGORYID and t1.PARTID=w13.PARTID and t1.ORDERTYPENAME=w13.ORDERTYPENAME
      left join
     --近14到26周周均
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY)*0.3/13 as qty
     from  IntelligentOrderWeeklyBase a
    where  thedate>=trunc(sysdate,'iw')-175 and thedate<=trunc(sysdate,'iw')-91
     and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) w26 on t1.BRANCHID=w26.BRANCHID and t1.SALESCATEGORYID=w26.SALESCATEGORYID and t1.PARTID=w26.PARTID and t1.ORDERTYPENAME=w26.ORDERTYPENAME
      left join
     --近27到52周周均
    (select  a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY)*0.2/26 as qty
     from  IntelligentOrderWeeklyBase a
    where  thedate>=trunc(sysdate,'iw')-357 and thedate<=trunc(sysdate,'iw')-182
     and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by   a.BRANCHID,a.SALESCATEGORYID,a.PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) w52 on t1.BRANCHID=w52.BRANCHID and t1.SALESCATEGORYID=w52.SALESCATEGORYID and t1.PARTID=w52.PARTID and t1.ORDERTYPENAME=w52.ORDERTYPENAME ;


      update IntelligentOrderWeekly set DEVIATION=
     sqrt(  (power((ORDERQUANTITY1-AVG),2)+
       power((ORDERQUANTITY2-AVG),2)+
       power((ORDERQUANTITY3-AVG),2)+
       power((ORDERQUANTITY4-AVG),2)+
       power((ORDERQUANTITY5-AVG),2)+
       power((ORDERQUANTITY6-AVG),2)+
       power((ORDERQUANTITY7-AVG),2)+
       power((ORDERQUANTITY8-AVG),2)+
       power((ORDERQUANTITY9-AVG),2)+
       power((ORDERQUANTITY10-AVG),2)+
       power((ORDERQUANTITY11-AVG),2)+
       power((ORDERQUANTITY12-AVG),2)+
       power((ORDERQUANTITY13-AVG),2)+
       power((ORDERQUANTITY14-AVG),2)+
       power((ORDERQUANTITY15-AVG),2)+
       power((ORDERQUANTITY16-AVG),2)+
       power((ORDERQUANTITY17-AVG),2)+
       power((ORDERQUANTITY18-AVG),2)+
       power((ORDERQUANTITY19-AVG),2)+
       power((ORDERQUANTITY20-AVG),2)+
       power((ORDERQUANTITY21-AVG),2)+
       power((ORDERQUANTITY22-AVG),2)+
       power((ORDERQUANTITY23-AVG),2)+
       power((ORDERQUANTITY24-AVG),2)+
       power((ORDERQUANTITY25-AVG),2)+
       power((ORDERQUANTITY26-AVG),2)+
       power((ORDERQUANTITY27-AVG),2)+
       power((ORDERQUANTITY28-AVG),2)+
       power((ORDERQUANTITY29-AVG),2)+
       power((ORDERQUANTITY30-AVG),2)+
       power((ORDERQUANTITY31-AVG),2)+
       power((ORDERQUANTITY32-AVG),2)+
       power((ORDERQUANTITY33-AVG),2)+
       power((ORDERQUANTITY34-AVG),2)+
       power((ORDERQUANTITY35-AVG),2)+
       power((ORDERQUANTITY36-AVG),2)+
       power((ORDERQUANTITY37-AVG),2)+
       power((ORDERQUANTITY38-AVG),2)+
       power((ORDERQUANTITY39-AVG),2)+
       power((ORDERQUANTITY40-AVG),2)+
       power((ORDERQUANTITY41-AVG),2)+
       power((ORDERQUANTITY42-AVG),2)+
       power((ORDERQUANTITY43-AVG),2)+
       power((ORDERQUANTITY44-AVG),2)+
       power((ORDERQUANTITY45-AVG),2)+
       power((ORDERQUANTITY46-AVG),2)+
       power((ORDERQUANTITY47-AVG),2)+
       power((ORDERQUANTITY48-AVG),2)+
       power((ORDERQUANTITY49-AVG),2)+
       power((ORDERQUANTITY50-AVG),2)+
       power((ORDERQUANTITY51-AVG),2)+
       power((ORDERQUANTITY52-AVG),2))/52);

end Prc_IntelligentOrderWeekly;