CREATE OR REPLACE PROCEDURE Prc_SATS_ForApprove IS
FComplateId integer;
FDateFormat date;
FDateStr varchar2(8);
FSerialLength integer;
FNewCode varchar2(50);
FcompanyCode varchar(50);
V_Code varchar2(50);
V_Id integer;
V_Num integer;
V_Sum integer;
V_status integer;
V_sbyy varchar(100);
vstatus integer;
FMyException                   EXCEPTION;
FErrormsg               VARCHAR2(1000);
fpartsRebateAccountid integer;
CURSOR SettlementAutomatic_CUR IS

--获取生成结算单需要的数据

select pss.TotalSettlementAmount,pss. code,pss.id, CustomerAccountId,pss.partssalescategoryname,
       pss.customercompanyid,pss.customercompanycode,pss.customercompanyname,pss.partssalescategoryid,
       sats.id rwid,pss.AccountGroupId,pss.rebatemethod,pss.settlementpath,pss.rebateamount
  from partssalessettlement pss
 inner join SettlementAutomaticTaskSet sats
    on sats.branchid = pss.salescompanyid
   and sats.brandid = pss.partssalescategoryid
   inner join branch on branch.id=pss.salescompanyid
 where pss.CreateTime > sats.settlestarttime
   and pss.CreateTime < sats.settleendtime
   and pss.status = 1 and to_char(sats.AutomaticSettleTime,'yyyymmdd')=to_char(sysdate,'yyyymmdd') and sats.Status=1;


SettlementAutomatic_RECORD SettlementAutomatic_CUR%ROWTYPE;

BEGIN

OPEN SettlementAutomatic_CUR;
LOOP
FETCH SettlementAutomatic_CUR
INTO SettlementAutomatic_RECORD;
EXIT WHEN SettlementAutomatic_CUR%NOTFOUND;
begin
     begin
       select status into vstatus from partssalessettlement where status=1 and code= SettlementAutomatic_RECORD.code;
       exception when no_data_found then
          FErrormsg:='单据状态已变更';
          raise FMyException;
          end;


        update partssalessettlement
           set status                          =decode(partssalessettlement.totalsettlementamount,0,3, 2),
               partssalessettlement.invoiceregistrationoperatorid=decode(partssalessettlement.totalsettlementamount,0,partssalessettlement.creatorid),
               partssalessettlement.invoiceregistrationoperator=decode(partssalessettlement.totalsettlementamount,0,partssalessettlement.creatorname),
               partssalessettlement.invoiceregistrationtime=decode(partssalessettlement.totalsettlementamount,0,partssalessettlement.createtime),
               partssalessettlement.modifierid = partssalessettlement.creatorid,
               partssalessettlement.modifiername=partssalessettlement.creatorname,
               partssalessettlement.modifytime=partssalessettlement.createtime,
               partssalessettlement.approverid=partssalessettlement.creatorid,
               partssalessettlement.approvername=partssalessettlement.creatorname,
               partssalessettlement.approvetime=partssalessettlement.createtime
         where partssalessettlement.code = SettlementAutomatic_RECORD.code;
     if SettlementAutomatic_RECORD.rebatemethod=2 then begin
        begin
          select partsRebateAccount.id
            into fpartsRebateAccountid
            from partsRebateAccount
           where partsRebateAccount.Accountgroupid =
                 SettlementAutomatic_RECORD.AccountGroupId
             and partsRebateAccount.Customercompanyid =
                 SettlementAutomatic_RECORD.Customercompanyid
             and status = 1 ;
        exception 
          when no_data_found then 
               FErrormsg := '返利账户不存在';
                raise FMyException;
          when too_many_rows then
               FErrormsg := '返利账户存在多个';
                raise FMyException;    
        end;
         if SettlementAutomatic_RECORD.Settlementpath=1
           then
             update partsRebateAccount
                set partsRebateAccount.Accountbalanceamount = partsRebateAccount.Accountbalanceamount -SettlementAutomatic_RECORD.rebateamount
              where id=fpartsRebateAccountid;
           end if;
           if SettlementAutomatic_RECORD.Settlementpath=2
           then
             update partsRebateAccount
                set partsRebateAccount.Accountbalanceamount = partsRebateAccount.Accountbalanceamount +SettlementAutomatic_RECORD.rebateamount
              where id=fpartsRebateAccountid;
           end if; 
        end;
      end if;
      --更新客户账户的账户金额
      update CustomerAccount
         set AccountBalance      = AccountBalance -
                                   SettlementAutomatic_RECORD.TotalSettlementAmount,
             ShippedProductValue = ShippedProductValue -
                                   SettlementAutomatic_RECORD.TotalSettlementAmount
       where id = SettlementAutomatic_RECORD.CustomerAccountId;


        --生产客户账户变更明细
        insert into CustomerAccountHisDetail
          (id,
           Customeraccountid,
           ChangeAmount,
           ProcessDate,
           BusinessType,
           SourceId,
           Sourcecode,
           BeforeChangeAmount,
           Afterchangeamount,
           debit,
           Creatorname)
          (select S_CustomerAccountHisDetail.Nextval,
                  SettlementAutomatic_RECORD.CustomerAccountId,
                  SettlementAutomatic_RECORD.TotalSettlementAmount,
                  sysdate,
                  4,
                  SettlementAutomatic_RECORD.id,
                  SettlementAutomatic_RECORD.code,
                  c.accountbalance +
                  SettlementAutomatic_RECORD.TotalSettlementAmount,
                  c.accountbalance,
                  SettlementAutomatic_RECORD.TotalSettlementAmount,
                  'admin'
             from CustomerAccount c
            where id = SettlementAutomatic_RECORD.CustomerAccountId);

        V_status:=1;
        V_sbyy:='';



--新增自动结算任务执行结果
insert into AutoTaskExecutResult
  (id,
   Settlementautotasksetid,
   Brandid,
   Brandname,
   CompanyId,
   CompanyCode,
   CompanyName,
   ExecutionStatus,
   ExecutionTime,
   ExecutionLoseReason,sourcecode)
values
  (S_AutoTaskExecutResult.Nextval,
   SettlementAutomatic_RECORD.rwid,
   SettlementAutomatic_RECORD.partssalescategoryid,
   SettlementAutomatic_RECORD.partssalescategoryname,
   SettlementAutomatic_RECORD.customercompanyid,
   SettlementAutomatic_RECORD.customercompanycode,
   SettlementAutomatic_RECORD.customercompanyname,
   V_status,
   sysdate,
   V_sbyy, SettlementAutomatic_RECORD.code);
--更新自动结算任务状态
exception
  when FMyException then
    begin
      ROLLBACK;
      insert into AutoTaskExecutResult
  (id,
   Settlementautotasksetid,
   Brandid,
   Brandname,
   CompanyId,
   CompanyCode,
   CompanyName,
   ExecutionStatus,
   ExecutionTime,
   ExecutionLoseReason,sourcecode)
values
  (S_AutoTaskExecutResult.Nextval,
   SettlementAutomatic_RECORD.rwid,
   SettlementAutomatic_RECORD.partssalescategoryid,
   SettlementAutomatic_RECORD.partssalescategoryname,
   SettlementAutomatic_RECORD.customercompanyid,
   SettlementAutomatic_RECORD.customercompanycode,
   SettlementAutomatic_RECORD.customercompanyname,
   3,
   sysdate,
   FErrormsg, SettlementAutomatic_RECORD.code);
   commit;
      end;
end;
commit;
END LOOP;
CLOSE SettlementAutomatic_CUR;
update SettlementAutomaticTaskSet set Status=2 where to_char(AutomaticSettleTime,'yyyymmdd')=to_char(sysdate,'yyyymmdd') and Status=1;




END Prc_SATS_ForApprove;