CREATE OR REPLACE PROCEDURE Prc_SapPaymentInfo IS

begin

delete from SAP_YX_PaymentInfo where HandleTime<add_months(sysdate,-3);

END Prc_SapPaymentInfo;