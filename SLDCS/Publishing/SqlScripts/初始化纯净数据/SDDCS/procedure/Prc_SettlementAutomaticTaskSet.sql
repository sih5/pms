CREATE OR REPLACE PROCEDURE Prc_SettlementAutomaticTaskSet IS
FComplateId integer;
FDateFormat date;
FDateStr varchar2(8);
FSerialLength integer;
FNewCode varchar2(50);
FcompanyCode varchar(50);
V_Code varchar2(50);
V_Id integer;
V_num integer;
V_status integer;
V_sbyy varchar(100);

CURSOR SettlementAutomatic_CUR IS

--获取生成结算单需要的数据
select rwid,CounterpartCompanyId,counterpartcompanycode,counterpartcompanyname,branchid,branchcode,branchname,partssalescategoryid,
partssalesordertypename,Customeraccountid,ag.id,ag.code,ag.name,sum(shuliang*SettlementPrice) as zje from (
select t.CounterpartCompanyId,t.counterpartcompanycode,t.counterpartcompanyname,t.ckid,t.rkid,t.branchid,t.branchcode,t.branchname,t.partssalescategoryid,
t.partssalesordertypename,t.Customeraccountid,pobd.sparepartid,nvl(sum(pobd.OutboundAmount),0)-nvl(sum(picb.InspectedQuantity),0) as shuliang,pobd.SettlementPrice,t.id as rwid
from TestTable t
inner join PartsOutboundBillDetail pobd on t.ckid=pobd.PartsOutboundBillId
left join PartsInboundCheckBillDetail picb on picb.partsinboundcheckbillid=t.rkid and  pobd.sparepartid=picb.sparepartid
group by t.id,t.CounterpartCompanyId,t.counterpartcompanycode,t.counterpartcompanyname,t.ckid,t.rkid,t.branchid,t.branchcode,t.branchname,t.partssalescategoryid,
t.partssalesordertypename,t.Customeraccountid,pobd.sparepartid,pobd.SettlementPrice
) a
inner join CustomerAccount ca on ca.id=a.Customeraccountid
inner join AccountGroup ag on ag.id=ca.Accountgroupid and AccountType=1
group by rwid,CounterpartCompanyId,counterpartcompanycode,counterpartcompanyname,branchid,branchcode,branchname,partssalescategoryid,
partssalesordertypename,Customeraccountid,ag.id,ag.code,ag.name;

SettlementAutomatic_RECORD SettlementAutomatic_CUR%ROWTYPE;

BEGIN

--获取能够合并结算的出入库单据信息，并存入临时表
insert into TestTable (
select sats.id,pob.CounterpartCompanyId,pob.counterpartcompanycode,pob.counterpartcompanyname,pob.id as ckid,picb.id as rkid,pob.branchid,pob.branchcode,pob.branchname,pob.partssalescategoryid,
pso.SalesCategoryName,pob.Customeraccountid
from PartsOutboundBill pob
inner join SettlementAutomaticTaskSet sats on sats.branchid=pob.branchid and sats.brandid=pob.partssalescategoryid
and pob.CreateTime>sats.settlestarttime and pob.CreateTime<sats.settleendtime
inner join PartsSalesOrder pso on pso.id in pob.OriginalRequirementBillId and pob.OriginalRequirementBillType=1
left join PartsSalesReturnBill psrb on psrb.PartsSalesOrderId in pob.OriginalRequirementBillId
left join PartsInboundCheckBill picb on picb.OriginalRequirementBillId in psrb.id and picb.OriginalRequirementBillType=2
and picb.branchid=pob.branchid and picb.partssalescategoryid=pob.partssalescategoryid
and picb.CreateTime>sats.settlestarttime and picb.CreateTime<sats.settleendtime and picb.SettlementStatus=2
where pob.SettlementStatus=2 and pob.StorageCompanyType=1 and to_char(sats.AutomaticSettleTime,'yyyymmdd')=to_char(sysdate,'yyyymmdd') and sats.Status=1
group by sats.id,pob.CounterpartCompanyId,pob.counterpartcompanycode,pob.counterpartcompanyname,pob.id,picb.id,pob.branchcode,pob.branchname,pob.partssalescategoryid,
pso.SalesCategoryName,pob.Customeraccountid,pob.branchid
);

OPEN SettlementAutomatic_CUR;
LOOP
FETCH SettlementAutomatic_CUR
INTO SettlementAutomatic_RECORD;
EXIT WHEN SettlementAutomatic_CUR%NOTFOUND;
--生成结算单编号
        FcompanyCode:=SettlementAutomatic_RECORD.branchcode;
        FDateFormat:=trunc(sysdate,'DD');
        FDateStr:=to_char(sysdate,'yyyymmdd');
        FSerialLength:=4;
        FNewCode:='PIS{CORPCODE}'||FDateStr||'{SERIAL}';
        /*获取编号生成模板Id*/
          begin
            select Id
              into FComplateId
              from codetemplate
             where name = 'PartsTransferOrder'
               and IsActived = 1;
          exception
            when NO_DATA_FOUND then
              raise_application_error(-20001, '未找到名称为"PartsTransferOrder"的有效编码规则。');
            when others then
              raise;
          end;

         select regexp_replace(
             regexp_replace(FNewCode, '{CORPCODE}', FcompanyCode),
              '{SERIAL}',
                lpad(to_char(GetSerial(FComplateId, FDateFormat, SettlementAutomatic_RECORD.branchcode)),FSerialLength,'0')
              ) Code into V_Code from dual;
--获取清单条数
select count(*) into V_Num from PartsOutboundBillDetail where PartsOutboundBillId in(select ckid from TestTable where CounterpartCompanyId=SettlementAutomatic_RECORD.CounterpartCompanyId);

--判断清单条数和结算总金额
if V_Num<=1000 and SettlementAutomatic_RECORD.zje<=1170000 then

--插入配件销售结算单
V_Id:=S_PartsSalesSettlement.Nextval;
insert into PartsSalesSettlement(id,Code,SettlementPath,Salescompanyid,Salescompanycode,Salescompanyname,PartsSalesCategoryId,PartsSalesCategoryName,AccountGroupId,AccountGroupCode,Accountgroupname,
CustomerCompanyId,CustomerCompanyCode,Customercompanyname,CustomerAccountId,TotalSettlementAmount,InvoiceAmountDifference,TaxRate,Tax,Status,Creatorid,Creatorname,Createtime,ApproverId,Approvername,Approvetime) values
(V_Id,V_Code,1,SettlementAutomatic_RECORD.branchid,SettlementAutomatic_RECORD.branchcode,SettlementAutomatic_RECORD.branchname,SettlementAutomatic_RECORD.partssalescategoryid,
SettlementAutomatic_RECORD.partssalesordertypename,SettlementAutomatic_RECORD.Id,SettlementAutomatic_RECORD.Code,SettlementAutomatic_RECORD.Name,SettlementAutomatic_RECORD.CounterpartCompanyId,SettlementAutomatic_RECORD.Counterpartcompanycode,
SettlementAutomatic_RECORD.Counterpartcompanyname,SettlementAutomatic_RECORD.Customeraccountid,SettlementAutomatic_RECORD.Zje,0,0.17,nvl(SettlementAutomatic_RECORD.zje,0)*0.17,2,1,'admin',sysdate,1,'admin',sysdate);

--插入结算单清单
insert into PartsSalesSettlementDetail(id,Serialnumber,PartsSalesSettlementId,SparePartId,SparePartCode,Sparepartname,PriceBeforeRebate,DiscountedPrice,SettlementPrice,QuantityToSettle,SettlementAmount)
(
select S_PartsSalesSettlementDetail.Nextval,rownum,V_Id,pobd.sparepartid,pobd.sparepartcode,pobd.sparepartname,pobd.Settlementprice,
0,pobd.Settlementprice,(nvl(pobd.OutboundAmount,0)-nvl(picb.InspectedQuantity,0)) as shuliang,(nvl(pobd.OutboundAmount,0)-nvl(picb.InspectedQuantity,0))*pobd.Settlementprice from TestTable t
inner join PartsOutboundBillDetail pobd on pobd.PartsOutboundBillId in t.ckid
left join PartsInboundCheckBillDetail picb on picb.partsinboundcheckbillid in t.rkid and  pobd.sparepartid=picb.sparepartid
where t.counterpartcompanyid=SettlementAutomatic_RECORD.CounterpartCompanyId
);

--插入结算单与出库单关联单
insert into PartsSalesSettlementRef(id,Partssalessettlementid,Warehouseid,Warehousename,Sourceid,Sourcecode,Sourcetype,Settlementamount,Sourcebillcreatetime)
(
select S_PartsSalesSettlementRef.Nextval,V_Id,Warehouseid,Warehousename,
id,code,1,jsje,createtime from
(select pob.id,pob.code,sum(nvl(pobd.OutboundAmount,0)*nvl(pobd.SettlementPrice,0)) as jsje,pob.createtime,pob.warehouseid,pob.warehousename from PartsOutboundBill pob
inner join PartsOutboundBillDetail pobd on pob.id=pobd.partsoutboundbillid
where pob.id in (select ckid from TestTable where CounterpartCompanyId=SettlementAutomatic_RECORD.CounterpartCompanyId)
group by pob.id,pob.code,pob.createtime,pob.warehouseid,pob.warehousename)
);

--插入结算单与入库单关联单
insert into PartsSalesSettlementRef(id,Partssalessettlementid,Warehouseid,Warehousename,Sourceid,Sourcecode,Sourcetype,Settlementamount,Sourcebillcreatetime)
(
select S_PartsSalesSettlementRef.Nextval,V_Id,Warehouseid,Warehousename,
id,code,5,jsje,createtime from
(
select  picb.id,picb.code,-sum(nvl(pic.InspectedQuantity,0)*nvl(pic.SettlementPrice,0)) as jsje,picb.createtime,pob.warehouseid,pob.warehousename from TestTable t
inner join PartsInboundCheckBill picb on picb.id in t.rkid
inner join PartsInboundCheckBillDetail pic on picb.id=pic.partsinboundcheckbillid
inner join PartsOutboundBill pob on pob.id in t.ckid
where t.Counterpartcompanyid=SettlementAutomatic_RECORD.CounterpartCompanyId
group by picb.id,picb.code,picb.createtime,pob.warehouseid,pob.warehousename
)
);

--更新客户账户的账户金额
update CustomerAccount set AccountBalance=AccountBalance-SettlementAutomatic_RECORD.zje where id=SettlementAutomatic_RECORD.CustomerAccountId;

--更新客户账户的发出商品金额
update CustomerAccount set ShippedProductValue=ShippedProductValue-(select sum(Settlementamount) from PartsSalesSettlementRef where Partssalessettlementid=V_Id) where id=SettlementAutomatic_RECORD.CustomerAccountId;

--生产客户账户变更明细
insert into CustomerAccountHisDetail(id,Customeraccountid,ChangeAmount,ProcessDate,BusinessType,SourceId,Sourcecode,BeforeChangeAmount,Afterchangeamount,debit,Creatorname)
(
select S_CustomerAccountHisDetail.Nextval,SettlementAutomatic_RECORD.CustomerAccountId,SettlementAutomatic_RECORD.zje,sysdate,4,V_Id,V_Code,c.accountbalance+SettlementAutomatic_RECORD.zje,
c.accountbalance,SettlementAutomatic_RECORD.zje,'admin'
from CustomerAccount c where id=SettlementAutomatic_RECORD.CustomerAccountId
);

--更新出库单结算状态
update PartsOutboundBill set SettlementStatus=3 where id in (select ckid from TestTable where CounterpartCompanyId=SettlementAutomatic_RECORD.CounterpartCompanyId);

--更新入库检验单结算状态
update PartsInboundCheckBill set SettlementStatus=3 where id in (select rkid from TestTable where CounterpartCompanyId=SettlementAutomatic_RECORD.CounterpartCompanyId);

V_status:=1;
V_sbyy:='';

else

V_status:=2;
V_sbyy:='清单条数过多，或总金额过多';

end if;

--新增自动结算任务执行结果
insert into AutoTaskExecutResult(id,Settlementautotasksetid,Brandid,Brandname,CompanyId,CompanyCode,CompanyName,ExecutionStatus,ExecutionTime,ExecutionLoseReason) values
(S_AutoTaskExecutResult.Nextval,SettlementAutomatic_RECORD.rwid,SettlementAutomatic_RECORD.partssalescategoryid,SettlementAutomatic_RECORD.partssalesordertypename,
SettlementAutomatic_RECORD.CounterpartCompanyId,SettlementAutomatic_RECORD.CounterpartCompanyCode,SettlementAutomatic_RECORD.CounterpartCompanyName,V_status,sysdate,V_sbyy
);

END LOOP;
CLOSE SettlementAutomatic_CUR;


--更新自动结算任务状态
update SettlementAutomaticTaskSet set Status=2 where id in (select id from TestTable group by id);

--清除临时表数据
delete TestTable;


END Prc_SettlementAutomaticTaskSet;