create or replace procedure addPartABC as
begin
  ------------------------------------------------统购、非统购按品牌，配件出库频次，分配ABC分类
  update PartsBranch
     set PartABC =
         (select abc.Category
            from (select p.BranchId,
                         p.PartsSalesCategoryId,
                         p.PartId,
                         nvl(outs.nums, 0) as nums
                    from PartsBranch p
                    left join (select pb.BranchId,
                                     pb.PartsSalesCategoryId,
                                     pd.SparePartId,
                                     count(*) as nums
                                from PartsOutboundBill pb
                               inner join PartsOutboundBillDetail pd
                                  on pb.id = pd.PartsOutboundBillId
                               where pb.PartsSalesOrderTypeName not in
                                     ('直供订单', '新产品订单', '专项订单')
                                 and pb.CreateTime >
                                     to_date(sysdate - 365, 'yyyy-mm-dd')
                               group by pb.BranchId,
                                        pb.PartsSalesCategoryId,
                                        pd.SparePartId) outs
                      on p.BranchId = outs.BranchId
                     and p.PartsSalesCategoryId = outs.PartsSalesCategoryId
                   where p.PartId = outs.SparePartId) allouts
            left join ABCStrategy abc
              on allouts.BranchId = abc.BranchId
           where allouts.nums >= abc.YeardOutBoundStartTime
             and allouts.nums <= abc.YeardOutBoundEndTime
          ------------------------------------------------统购品牌公司其他品牌，配件出库频次，分配ABC分类
          union all
          select abc.Category
            from (select p.BranchId,
                         p.PartsSalesCategoryId,
                         p.PartId,
                         nvl(outs.nums, 0) as nums
                    from PartsBranch p
                    left join (select pb.BranchId,
                                     pb.PartsSalesCategoryId,
                                     pd.SparePartId,
                                     count(*) as nums
                                from PartsOutboundBill pb
                               inner join PartsOutboundBillDetail pd
                                  on pb.id = pd.PartsOutboundBillId
                               where pb.PartsSalesOrderTypeName not in
                                     ('直供订单', '新产品订单', '专项订单')
                                 and pb.CreateTime >
                                     to_date(sysdate - 365, 'yyyy-mm-dd')
                               group by pb.BranchId,
                                        pb.PartsSalesCategoryId,
                                        pd.SparePartId) outs
                      on p.BranchId = outs.BranchId
                     and p.PartsSalesCategoryId = outs.PartsSalesCategoryId
                   where p.PartId = outs.SparePartId
                     and p.PartsSalesCategoryName <> '统购'
                     and p.branchid = (select branchid
                                         from partssalescategory
                                        where name = '统购')) allouts
            left join ABCStrategy abc
              on allouts.BranchId = abc.BranchId
           where allouts.nums >= abc.YeardOutBoundStartTime
             and allouts.nums <= abc.YeardOutBoundEndTime)
   where exists
   (select 1
            from (select p.BranchId,
                         p.PartsSalesCategoryId,
                         p.PartId,
                         nvl(outs.nums, 0) as nums
                    from PartsBranch p
                    left join (select pb.BranchId,
                                     pb.PartsSalesCategoryId,
                                     pd.SparePartId,
                                     count(*) as nums
                                from PartsOutboundBill pb
                               inner join PartsOutboundBillDetail pd
                                  on pb.id = pd.PartsOutboundBillId
                               where pb.PartsSalesOrderTypeName not in
                                     ('直供订单', '新产品订单', '专项订单')
                                 and pb.CreateTime >
                                     to_date(sysdate - 365, 'yyyy-mm-dd')
                               group by pb.BranchId,
                                        pb.PartsSalesCategoryId,
                                        pd.SparePartId) outs
                      on p.BranchId = outs.BranchId
                     and p.PartsSalesCategoryId = outs.PartsSalesCategoryId
                   where p.PartId = outs.SparePartId) allouts
            left join ABCStrategy abc
              on allouts.BranchId = abc.BranchId
           where allouts.nums >= abc.YeardOutBoundStartTime
             and allouts.nums <= abc.YeardOutBoundEndTime
          ------------------------------------------------统购品牌公司其他品牌，配件出库频次，分配ABC分类
          union all
          select abc.Category
            from (select p.BranchId,
                         p.PartsSalesCategoryId,
                         p.PartId,
                         nvl(outs.nums, 0) as nums
                    from PartsBranch p
                    left join (select pb.BranchId,
                                     pb.PartsSalesCategoryId,
                                     pd.SparePartId,
                                     count(*) as nums
                                from PartsOutboundBill pb
                               inner join PartsOutboundBillDetail pd
                                  on pb.id = pd.PartsOutboundBillId
                               where pb.PartsSalesOrderTypeName not in
                                     ('直供订单', '新产品订单', '专项订单')
                                 and pb.CreateTime >
                                     to_date(sysdate - 365, 'yyyy-mm-dd')
                               group by pb.BranchId,
                                        pb.PartsSalesCategoryId,
                                        pd.SparePartId) outs
                      on p.BranchId = outs.BranchId
                     and p.PartsSalesCategoryId = outs.PartsSalesCategoryId
                   where p.PartId = outs.SparePartId
                     and p.PartsSalesCategoryName <> '统购'
                     and p.branchid = (select branchid
                                         from partssalescategory
                                        where name = '统购')) allouts
            left join ABCStrategy abc
              on allouts.BranchId = abc.BranchId
           where allouts.nums >= abc.YeardOutBoundStartTime
             and allouts.nums <= abc.YeardOutBoundEndTime);
end addPartABC;