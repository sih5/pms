create or replace procedure addProductLifeCycle is
begin
  update PartsBranch set ProductLifeCycle = 5 where IsOrderable = 0; --��̭��
  update PartsBranch a --����̭��
     set a.ProductLifeCycle =
         (select pcc.CycleCategory
            from PartsBranch p
           inner join ABCStrategy a
              on p.branchid = a.branchid
           inner join PartABCDetail pabc
              on a.category = pabc.partabc
           inner join ProductLifeCycleCategory pcc
              on pabc.productlifecyclecategoryid = pcc.id
           where p.IsOrderable = 1
             and (a.SamplingDuration between pcc.creatorstarttime and
                 pcc.creatorendtime))
   where exists
   (select *
            from (select pcc.CycleCategory
                    from PartsBranch p
                   inner join ABCStrategy a
                      on p.branchid = a.branchid
                   inner join PartABCDetail pabc
                      on a.category = pabc.partabc
                   inner join ProductLifeCycleCategory pcc
                      on pabc.productlifecyclecategoryid = pcc.id
                   where p.IsOrderable = 1
                     and (a.SamplingDuration between pcc.creatorstarttime and
                         pcc.creatorendtime)));
end addProductLifeCycle;