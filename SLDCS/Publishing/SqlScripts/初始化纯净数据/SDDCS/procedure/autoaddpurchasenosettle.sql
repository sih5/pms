create or replace procedure autoaddpurchasenosettle as
begin
  insert into PURCHASENOSETTLETable
  select S_PURCHASENOSETTLETable.nextval,t.*from (
    select c.code as BranchCode,
           a.partssalescategoryid,
           psc.name as CategoryName,
           d.id CompanyId,
           d.code,
           d.name,
           a.code as OrderCode,
           sp.code as PartCode,
           sp.name as PartName,
           b.costprice as Jh,
           b.settlementprice as HT,
           b.inspectedquantity as Qty,
           case
             when a.createtime < to_date('2014-07-01', 'yyyy-mm-dd') then
              round(b.costprice * b.inspectedquantity / 1.17, 2)
             else
              round(b.costprice * b.inspectedquantity, 2)
           end as JHCB,
           round(b.settlementprice * b.inspectedquantity, 2) as HTJE,
           a.createtime,
           a.inboundtype,
           kv.value,
           warehouse.code as warehousecode,
           warehouse.name as warehousename,
           to_char(sysdate - 1, 'yyyy-mm') syncdate,
       to_date(to_char(sysdate - 1, 'yyyy-mm-dd') || ' 23:59:59',
               'yyyy-mm-dd hh24:mi:ss') syncdate2
      from PartsInboundCheckBill a
     inner join warehouse
        on a.warehouseid = warehouse.id
     inner join Partsinboundcheckbilldetail b
        on a.id = b.partsinboundcheckbillid
     inner join branch c
        on c.id = a.storagecompanyid
     inner join company d
        on d.id = a.counterpartcompanyid
     inner join sparepart sp
        on sp.id = b.sparepartid
     inner join partssalescategory psc
        on psc.id = a.partssalescategoryid
      left join keyvalueitem kv
        on kv.key = a.inboundtype
       and kv.name = 'Parts_InboundType'
     inner join branch
        on a.storagecompanyid = branch.id
       and branch.status <> 99
     where a.Settlementstatus <> 3
    union all
    select c.code as BranchCode,
           a.partssalescategoryid,
           psc.name as CategoryName,
           d.id CompanyId,
           d.code,
           d.name,
           a.code as OrderCode,
           sp.code as PartCode,
           sp.name as PartName,
           b.costprice as Jh,
           b.settlementprice as HT,
           b.inspectedquantity as Qty,
           case
             when a.createtime < to_date('2014-07-01', 'yyyy-mm-dd') then
              round(b.costprice * b.inspectedquantity / 1.17, 2)
             else
              round(b.costprice * b.inspectedquantity, 2)
           end as JHCB,
           round(b.settlementprice * b.inspectedquantity, 2) as HTJE,
           a.createtime,
           a.inboundtype,
           kv.value,
           warehouse.code as warehousecode,
           warehouse.name as warehousename,
           to_char(sysdate - 1, 'yyyy-mm') syncdate,
       to_date(to_char(sysdate - 1, 'yyyy-mm-dd') || ' 23:59:59',
               'yyyy-mm-dd hh24:mi:ss') syncdate2
      from PartsInboundCheckBill a
     inner join warehouse
        on a.warehouseid = warehouse.id
     inner join Partsinboundcheckbilldetail b
        on a.id = b.partsinboundcheckbillid
     inner join branch c
        on c.id = a.storagecompanyid
     inner join company d
        on d.id = a.counterpartcompanyid
     inner join PartsPurchaseSettleRef psr
        on psr.sourceid = a.id
       and psr.SourceType = 5
     inner join partspurchasesettlebill psb
        on psb.id = psr.partspurchasesettlebillid
     inner join sparepart sp
        on sp.id = b.sparepartid
     inner join partssalescategory psc
        on psc.id = a.partssalescategoryid
      left join keyvalueitem kv
        on kv.key = a.inboundtype
       and kv.name = 'Parts_InboundType'
     inner join branch
        on a.storagecompanyid = branch.id
       and branch.status <> 99
     where a.Settlementstatus = 3
       and psb.status in (1, 2, 3, 5)
    union all
    select c.code as BranchCode,
           a.partssalescategoryid,
           psc.name as CategoryName,
           d.id CompanyId,
           d.code,
           d.name,
           a.code as OrderCode,
           sp.code as PartCode,
           sp.name as PartName,
           b.costprice as Jh,
           b.settlementprice as HT,
           b.inspectedquantity as Qty,
           case
             when a.createtime < to_date('2014-07-01', 'yyyy-mm-dd') then
              round(b.costprice * b.inspectedquantity / 1.17, 2)
             else
              round(b.costprice * b.inspectedquantity, 2)
           end as JHCB,
           round(b.settlementprice * b.inspectedquantity, 2) as HTJE,
           a.createtime,
           a.inboundtype,
           kv.value,
           warehouse.code as warehousecode,
           warehouse.name as warehousename,
           to_char(sysdate - 1, 'yyyy-mm') syncdate,
       to_date(to_char(sysdate - 1, 'yyyy-mm-dd') || ' 23:59:59',
               'yyyy-mm-dd hh24:mi:ss') syncdate2
      from PartsInboundCheckBill a
     inner join warehouse
        on a.warehouseid = warehouse.id
     inner join Partsinboundcheckbilldetail b
        on a.id = b.partsinboundcheckbillid
     inner join branch c
        on c.id = a.storagecompanyid
     inner join company d
        on d.id = a.counterpartcompanyid
     inner join partssalesrtnsettlementref psr
        on psr.sourceid = a.id
     inner join partssalesrtnsettlement psb
        on psb.id = psr.partssalesrtnsettlementId
     inner join sparepart sp
        on sp.id = b.sparepartid
     inner join partssalescategory psc
        on psc.id = a.partssalescategoryid
      left join keyvalueitem kv
        on kv.key = a.inboundtype
       and kv.name = 'Parts_InboundType'
     inner join branch
        on a.storagecompanyid = branch.id
       and branch.status <> 99
     where a.Settlementstatus = 3
       and psb.status in (1, 2)
    ----销售退货入库包含销售合并结算
    union all
    select c.code as BranchCode,
           a.partssalescategoryid,
           psc.name as CategoryName,
           d.id CompanyId,
           d.code,
           d.name,
           a.code as OrderCode,
           sp.code as PartCode,
           sp.name as PartName,
           b.costprice as Jh,
           b.settlementprice as HT,
           b.inspectedquantity as Qty,
           case
             when a.createtime < to_date('2014-07-01', 'yyyy-mm-dd') then
              round(b.costprice * b.inspectedquantity / 1.17, 2)
             else
              round(b.costprice * b.inspectedquantity, 2)
           end as JHCB,
           round(b.settlementprice * b.inspectedquantity, 2) as HTJE,
           a.createtime,
           a.inboundtype,
           kv.value,
           warehouse.code as warehousecode,
           warehouse.name as warehousename,
           to_char(sysdate - 1, 'yyyy-mm') syncdate,
       to_date(to_char(sysdate - 1, 'yyyy-mm-dd') || ' 23:59:59',
               'yyyy-mm-dd hh24:mi:ss') syncdate2
      from PartsInboundCheckBill a
     inner join warehouse
        on a.warehouseid = warehouse.id
     inner join Partsinboundcheckbilldetail b
        on a.id = b.partsinboundcheckbillid
     inner join branch c
        on c.id = a.storagecompanyid
     inner join company d
        on d.id = a.counterpartcompanyid
     inner join PartsSalesSettlementRef psr
        on psr.sourceid = a.id
       and psr.SourceType = 5
     inner join partssalessettlement psb
        on psb.id = psr.partssalessettlementid
     inner join sparepart sp
        on sp.id = b.sparepartid
     inner join partssalescategory psc
        on psc.id = a.partssalescategoryid
      left join keyvalueitem kv
        on kv.key = a.inboundtype
       and kv.name = 'Parts_InboundType'
     inner join branch
        on a.storagecompanyid = branch.id
       and branch.status <> 99
     where a.Settlementstatus = 3
       and psb.status in (1, 2)
    union all
    select c.code as BranchCode,
           a.partssalescategoryid,
           psc.name as CategoryName,
           d.id CompanyId,
           d.code,
           d.name,
           a.code as OrderCode,
           sp.code as PartCode,
           sp.name as PartName,
           b.costprice as Jh,
           b.settlementprice as HT,
           b.inspectedquantity as Qty,
           case
             when a.createtime < to_date('2014-07-01', 'yyyy-mm-dd') then
              round(b.costprice * b.inspectedquantity / 1.17, 2)
             else
              round(b.costprice * b.inspectedquantity, 2)
           end as JHCB,
           round(b.settlementprice * b.inspectedquantity, 2) as HTJE,
           a.createtime,
           a.inboundtype,
           kv.value,
           warehouse.code as warehousecode,
           warehouse.name as warehousename,
           to_char(sysdate - 1, 'yyyy-mm') syncdate,
       to_date(to_char(sysdate - 1, 'yyyy-mm-dd') || ' 23:59:59',
               'yyyy-mm-dd hh24:mi:ss') syncdate2
      from PartsInboundCheckBill a
     inner join warehouse
        on a.warehouseid = warehouse.id
     inner join Partsinboundcheckbilldetail b
        on a.id = b.partsinboundcheckbillid
     inner join branch c
        on c.id = a.storagecompanyid
     inner join company d
        on d.id = a.counterpartcompanyid
     inner join PartsRequisitionSettleRef psr
        on psr.sourceid = a.id
       and psr.SourceType = 5
     inner join PartsRequisitionSettleBill psb
        on psb.id = psr.PartsRequisitionSettleBillID
     inner join sparepart sp
        on sp.id = b.sparepartid
     inner join partssalescategory psc
        on psc.id = a.partssalescategoryid
      left join keyvalueitem kv
        on kv.key = a.inboundtype
       and kv.name = 'Parts_InboundType'
     inner join branch
        on a.storagecompanyid = branch.id
       and branch.status <> 99
     where a.Settlementstatus = 3
       and psb.status = 1)t;
end autoaddpurchasenosettle;