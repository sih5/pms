create or replace procedure autosetsyncnum as
begin

        update SDDCS.PartsOutboundPlan_sync set PartsOutboundPlan_sync.Syncnum=PartsOutboundPlan_increase.Nextval where billid
        in (  select pop.id
  from SDDCS.PartsOutboundPlan pop
 inner join SDDCS.PartsOutboundPlan_sync
    on PartsOutboundPlan_sync.billid = pop.id
 inner join SDDCS.warehouse
    on warehouse.id = pop.Warehouseid
 where ((warehouse.WmsInterface = 1) and
       (pop.OutboundType in (2, 3) or
       (pop.OutboundType = 1 and pop.creatorid > 0) or
       (pop.OutboundType = 4 and
       (exists (select 1
                     from SDDCS.PartsTransferOrder pto
                    inner join SDDCS.Warehouse owt
                       on pto.OriginalWarehouseId = owt.Id
                    inner join SDDCS.Warehouse dwt
                       on pto.DestWarehouseId = dwt.Id
                    where pto.Id = pop.OriginalRequirementBillId
                      and pop.OriginalRequirementBillType = 6
                      and (owt.StorageCenter != dwt.StorageCenter or
                          dwt.WmsInterface = 0))))))
   and PartsOutboundPlan_sync.Syncnum <
       (select syncnum
          from wmsinf.syncstatu@wmsinf_206
         where objid = '{3B5CE3A4-55BF-4EAF-B828-E75172758148}')
   and PartsOutboundPlan_sync.Syncnum >
       ((select syncnum
           from wmsinf.syncstatu@wmsinf_206
          where objid = '{3B5CE3A4-55BF-4EAF-B828-E75172758148}') - 3000) ----- 一天3000条出库计划 guid 时代工程车营销各自不同
   and pop.status in(1,2)
   and not exists (select 1
          from t_shipmentheader@wmsinf_206 tmp
         where tmp.interface_record_id = pop.code) );
  
        update SDDCS.partsinboundplan_sync set partsinboundplan_sync.Syncnum=PartsinboundPlan_increase.Nextval where billid in(select pip.id from SDDCS.PartsInboundPlan pip
 inner join SDDCS.PartsInboundPlan_sync
    on PartsInboundPlan_sync.billid = pip.id
 inner join SDDCS.warehouse
    on warehouse.Id = pip.Warehouseid
 where ((warehouse.WmsInterface = 1) and
       (((pip.InboundType = 1 and exists(select 1
             from SDDCS. PartsPurchaseOrder ppo
            inner join SDDCS.PartsPurchaseOrderType ppot
               on ppo.PartsPurchaseOrderTypeId = ppot.Id
            where ppo.Id = pip.OriginalRequirementBillId))
            or (pip.InboundType = 2 and exists (select 1
             from SDDCS.PartsSalesReturnBill psrb
            where psrb.Id = pip.OriginalRequirementBillId))
             or (pip.InboundType =4)
       ) or
       (pip.InboundType = 3 and exists  (select 1
             from PartsTransferOrder ppo
            where ppo.Id = pip.OriginalRequirementBillId) and
       (exists (select 1
                     from SDDCS.PartsTransferOrder pto
                    inner join SDDCS.warehouse owt
                       on pto.OriginalWarehouseId = owt.Id
                    inner join SDDCS.warehouse dwt
                       on pto.DestWarehouseId = dwt.Id
                    where pto.Id = pip.OriginalRequirementBillId
                      and pip.Originalrequirementbilltype=6
                      and (owt.StorageCenter != dwt.StorageCenter or
                          owt.wmsinterface = 0))))))
                              and PartsInboundPlan_sync.Syncnum <
       (select syncnum
          from wmsinf.syncstatu@wmsinf_206
         where objid = '{8D187EDB-2D68-465A-B377-DEDED40426C0}')
   and PartsInboundPlan_sync.Syncnum >
       ((select syncnum
           from wmsinf.syncstatu@wmsinf_206
          where objid = '{8D187EDB-2D68-465A-B377-DEDED40426C0}') - 3000) ----- 一天3000条出库计划 guid 时代工程车营销各自不同
 and pip.status in(1,4) 
  and not exists (select 1
          from t_receiptheader@wmsinf_206 tmp
         where tmp.interface_record_id = pip.code));

  end;