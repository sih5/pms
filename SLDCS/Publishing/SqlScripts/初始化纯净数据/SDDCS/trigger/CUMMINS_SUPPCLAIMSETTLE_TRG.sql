CREATE OR REPLACE TRIGGER CUMMINS_SUPPCLAIMSETTLE_TRG
AFTER INSERT OR UPDATE ON SUPPLIERCLAIMSETTLEBILL
FOR EACH ROW
declare
idOfCummins number(9);

BEGIN
  select id into idOfCummins from partssupplier where code='LFT000332' and rownum=1 and status=1;

  IF (:NEW.STATUS=10 AND :NEW.SUPPLIERID=idOfCummins) THEN
    INSERT INTO CUMMINS_CLAIMSETTLE_SYNC VALUES (S_CUMMINS_CLAIMSETTLE.NEXTVAL,:NEW.ID,'SUPPLIERCLAIMSETTLEBILL');
  END IF;
END;