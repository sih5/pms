create or replace trigger Company_Trglog
   after insert or update on Company
   for each row
begin
  if :old.id is not null then
insert into company_log
  (ID,
   CODE,NAME,
   REGIONID,
   PROVINCENAME,
   CITYNAME,
   COUNTYNAME,STATUS,
   creatorid,
   creatorname,
   createtime,
   MODIFIERID,
   MODIFIERNAME,
   MODIFYTIME,
   systemdate,
   logtype)
values
  (:old.id,
  :old.code, :old.name,
   :old.regionid,
   :old.provincename,
   :old.cityname,
   :old.countyname,:old.STATUS,
   :old.CREATORID,
   :old.CREATORNAME,
   :old.CREATETIME,
   :old.MODIFIERID,
   :old.MODIFIERNAME,
   :old.MODIFYTIME,
   sysdate,
   2);
end if;
if :old.id is null then
  insert into company_log
    (ID,
     CODE,
     NAME,
     REGIONID,
     PROVINCENAME,
     CITYNAME,
     COUNTYNAME,
     STATUS,
     creatorid,
     creatorname,
     createtime,
     MODIFIERID,
     MODIFIERNAME,
     MODIFYTIME,
     systemdate,
     logtype)
  values
    (:new.id,
     :new.code,
     :new.name,
     :new.regionid,
     :new.provincename,
     :new.cityname,
     :new.countyname,
     :new.STATUS,
     :new.CREATORID,
     :new.CREATORNAME,
     :new.CREATETIME,
     :new.MODIFIERID,
     :new.MODIFIERNAME,
     :new.MODIFYTIME,
     sysdate,
     1);
  end if;

end Company_Trg;