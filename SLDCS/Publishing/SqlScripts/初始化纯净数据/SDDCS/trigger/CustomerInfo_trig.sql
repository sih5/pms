create or replace trigger CustomerInfo_trig
  after insert or update on CustomerInformation
  for each row
declare
begin
  declare
  begin
    insert into CustomerInfo_sync

      (billid,
       syncnum)
    values
      (:new.Id,
       CustomerInfo_increase.nextval);
  end;
end;