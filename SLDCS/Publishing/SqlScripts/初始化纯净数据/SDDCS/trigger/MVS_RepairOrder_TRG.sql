CREATE OR REPLACE TRIGGER MVS_RepairOrder_TRG
  AFTER INSERT OR UPDATE ON RepairOrder
  FOR EACH ROW
BEGIN
  IF (:old.id is null or (:new.branchid=12261 and:new.status<>:old.status and (:NEW.STATUS = 1 or :NEW.STATUS = 6 or :NEW.STATUS = 99))) THEN
    INSERT INTO MVS_RepairOrder_SYNC_SD  VALUES (S_MVS_RepairOrder_SYNC_SD.NEXTVAL, :NEW.ID,'REPAIRORDER');
  END IF;
   IF (:old.id is null or (:new.branchid=11181 and:new.status<>:old.status and (:NEW.STATUS = 1 or :NEW.STATUS = 6 or :NEW.STATUS = 99))) THEN
    INSERT INTO MVS_RepairOrder_SYNC_JT  VALUES (S_MVS_RepairOrder_SYNC_JT.NEXTVAL, :NEW.ID,'REPAIRORDER');
  END IF;
END RepairOrder_TRG;