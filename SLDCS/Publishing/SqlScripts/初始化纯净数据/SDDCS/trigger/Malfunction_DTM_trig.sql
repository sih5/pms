create or replace trigger Malfunction_DTM_trig
  after INSERT or update ON Malfunction
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    insert into Malfunction_DTM_sync
      (billid, syncnum)
    values
      (:new.Id,Malfunction_DTM_increase.nextval);
  END;
END;