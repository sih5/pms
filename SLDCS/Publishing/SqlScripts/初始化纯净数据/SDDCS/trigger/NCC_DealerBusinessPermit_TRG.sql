CREATE OR REPLACE TRIGGER NCC_DealerBusinessPermit_TRG
AFTER INSERT OR UPDATE OR DELETE ON DealerBusinessPermit
FOR EACH ROW
BEGIN

  if inserting then
         INSERT INTO NCC_DealerBusinessPermit_SYNC VALUES(S_NCC_DealerPermit_SYNC.NEXTVAL,:NEW.ID,'DealerBusinessPermit',sysdate,:NEW.DealerId,:new.SERVICEPRODUCTLINEID,:new.PRODUCTLINETYPE,
         :new.BRANCHID,:new.SERVICEFEEGRADEID,:new.CREATETIME,:new.CREATORID,:new.CREATORNAME,:new.ENGINEREPAIRPOWER);
  end if;
   if deleting then
        INSERT INTO NCC_DealerBusinessPermit_SYNC VALUES(S_NCC_DealerPermit_SYNC.NEXTVAL,:old.ID,'DealerBusinessPermit',sysdate,:old.DealerId,:old.SERVICEPRODUCTLINEID,:old.PRODUCTLINETYPE,
         :old.BRANCHID,:old.SERVICEFEEGRADEID,:old.CREATETIME,:old.CREATORID,:old.CREATORNAME,:old.ENGINEREPAIRPOWER);
  end if;
   if updating then
        INSERT INTO NCC_DealerBusinessPermit_SYNC VALUES(S_NCC_DealerPermit_SYNC.NEXTVAL,:NEW.ID,'DealerBusinessPermit',sysdate,:NEW.DealerId,:new.SERVICEPRODUCTLINEID,:new.PRODUCTLINETYPE,
         :new.BRANCHID,:new.SERVICEFEEGRADEID,:new.CREATETIME,:new.CREATORID,:new.CREATORNAME,:new.ENGINEREPAIRPOWER);
  end if;
END NCC_DealerBusinessPermit_TRG;