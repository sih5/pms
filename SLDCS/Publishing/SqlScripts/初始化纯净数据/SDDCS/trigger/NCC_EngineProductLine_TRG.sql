CREATE OR REPLACE TRIGGER NCC_EngineProductLine_TRG
AFTER INSERT  OR UPDATE ON EngineProductLine
FOR EACH ROW
BEGIN
  INSERT INTO NCC_EngineProductLine_SYNC VALUES(S_NCC_EngineProductLine_SYNC.NEXTVAL,:NEW.ID,'EngineProductLine',sysdate);
END NCC_EngineProductLine_TRG;