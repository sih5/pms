CREATE OR REPLACE TRIGGER NCC_ServiceProdLine_TRG
AFTER INSERT  OR UPDATE ON Serviceprodlineproduct
FOR EACH ROW
BEGIN
  INSERT INTO NCC_ServiceProdLine_SYNC VALUES(S_NCC_ServiceProdLine_SYNC.NEXTVAL,:NEW.ID,'Serviceprodlineproduct',sysdate);
END NCC_ServiceProdLine_TRG;