CREATE OR REPLACE TRIGGER NCC_VehicleInformation_TRG
AFTER INSERT OR UPDATE  ON VehicleInformation
FOR EACH ROW
BEGIN
  INSERT INTO NCC_VehicleInformation_SYNC VALUES(S_NCC_VehicleInformation_SYNC.NEXTVAL,:NEW.ID,'VehicleInformation',sysdate);
END NCC_VehicleInformation_TRG;