CREATE OR REPLACE TRIGGER OCC_COMPANY_TRG
  AFTER INSERT OR UPDATE ON COMPANY
  FOR EACH ROW
DECLARE
  FLAG NUMBER;
BEGIN
    --责任单位(责任单位信息)
    merge into OCC_RESPONSIBLEUNIT_SYNC bys
    using (select '1' from dual) N
    on (bys.BILLID = :new.ID)
    WHEN MATCHED THEN
      UPDATE set syncnum = S_OCC_RESPONSIBLEUNIT.nextval
    WHEN NOT MATCHED THEN
      INSERT
        (SYNCNUM, BILLID)
      VALUES
        (S_OCC_RESPONSIBLEUNIT.NEXTVAL, :NEW.ID);

  IF (:NEW.TYPE in(6,10)) THEN
    SELECT COUNT(*)
      INTO FLAG
      FROM BRANCHSUPPLIERRELATION
     WHERE /*BRANCHID in (11181,12261)
       AND*/ SUPPLIERID = :NEW.ID;
    IF (FLAG >= 1) THEN

      --责任单位(责任单位信息)
      merge into OCC_RESPONSIBLEUNIT_SYNC bys
      using (select '1' from dual) N
      on (bys.BILLID = :new.ID)
      WHEN MATCHED THEN
        UPDATE set syncnum = S_OCC_RESPONSIBLEUNIT.nextval
      WHEN NOT MATCHED THEN
        INSERT
          (SYNCNUM, BILLID)
        VALUES
          (S_OCC_RESPONSIBLEUNIT.NEXTVAL, :NEW.ID);
    END IF;
    SELECT COUNT(*)
      INTO FLAG
      FROM BRANCHSUPPLIERRELATION
     WHERE /*BRANCHID in (11181,12261)
       AND*/ SUPPLIERID = :NEW.ID;
    IF (FLAG >= 1) THEN

      --配件供应商(供应商信息)
      merge into OCC_PARTSSUPPLIER_SYNC bys
      using (select '1' from dual) N
      on (bys.BILLID = :new.ID)
      WHEN MATCHED THEN
        UPDATE set syncnum = S_OCC_PARTSSUPPLIER.nextval
      WHEN NOT MATCHED THEN
        INSERT
          (SYNCNUM, BILLID)
        VALUES
          (S_OCC_PARTSSUPPLIER.NEXTVAL, :NEW.ID);
    END IF;
  END IF;
  IF (:NEW.TYPE = 5) THEN
    SELECT COUNT(*)
      INTO FLAG
      FROM RESPONSIBLEUNITBRANCH
     WHERE /*BRANCHID in (11181,12261)
       AND*/ RESPONSIBLEUNITID = :NEW.ID;
    IF (FLAG >= 1) THEN
      --责任单位(责任单位信息)
      merge into OCC_RESPONSIBLEUNIT_SYNC bys
      using (select '1' from dual) N
      on (bys.BILLID = :new.ID)
      WHEN MATCHED THEN
        UPDATE set syncnum = S_OCC_RESPONSIBLEUNIT.nextval
      WHEN NOT MATCHED THEN
        INSERT
          (SYNCNUM, BILLID)
        VALUES
          (S_OCC_RESPONSIBLEUNIT.NEXTVAL, :NEW.ID);
    END IF;
  END IF;
  IF (:NEW.TYPE in (2,7,10)) THEN
   /* SELECT COUNT(*)
      INTO FLAG
      FROM DEALER
     WHERE  ID = :NEW.ID;*/
    /*IF (FLAG >= 1) THEN*/
      --服务站信息
      merge into OCC_DEALER_SYNC bys
      using (select '1' from dual) N
      on (bys.BILLID = :new.ID)
      WHEN MATCHED THEN
        UPDATE set syncnum = S_OCC_DEALER.nextval
      WHEN NOT MATCHED THEN
        INSERT (SYNCNUM, BILLID) VALUES (S_OCC_DEALER.NEXTVAL, :NEW.ID);
   /* END IF;*/

      --服务站清单也需要触发
      update DEALERSERVICEINFO set status=status where dealerid = :NEW.ID;
  END IF;
END;