CREATE OR REPLACE TRIGGER OCC_VEHICLEINFORMATION_TRG
AFTER INSERT OR UPDATE ON VEHICLEINFORMATION
FOR EACH ROW
BEGIN
    merge into OCC_VEHICLEINFORMATION_SYNC bys
    using (select '1' from dual) N
    on (bys.BILLID = :new.ID)
    WHEN MATCHED THEN
      UPDATE set syncnum = S_OCC_VEHICLEINFORMATION.nextval
    WHEN NOT MATCHED THEN
      INSERT
        (SYNCNUM, BILLID)
      VALUES
        (S_OCC_VEHICLEINFORMATION.NEXTVAL, :NEW.ID);

    /*改码车重新触发维修单--解决维修单生成后改码*/
    if updating and :new.vin <> :new.oldvin then
       update repairorder r
          set id = id
        where r.vehicleid = :new.id
          and r.vin <> :new.vin;
    end if;

END;