CREATE OR REPLACE TRIGGER OCC_customer_TRG
  AFTER INSERT OR UPDATE ON customer
  FOR EACH ROW
BEGIN
  update retainedcustomer
     set status = status
   where retainedcustomer.customerid = :new.ID;
END;