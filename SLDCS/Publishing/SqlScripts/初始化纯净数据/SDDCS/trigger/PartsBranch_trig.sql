create or replace trigger PartsBranch_trig
  after insert or update ON PartsBranch
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
     if updating('ProductLifeCycle') then
      return;
      end if;   
      INSERT into SparePart_sync (billid, syncnum) values (:new.PartId, SPAREPART_INCREASE.nextval);
  END;
END;