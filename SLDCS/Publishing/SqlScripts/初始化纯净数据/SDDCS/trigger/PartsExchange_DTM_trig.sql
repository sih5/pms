create or replace trigger PartsExchange_DTM_trig
  after INSERT or update ON PartsExchange
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    insert into PartsExchange_DTM_sync
      (billid, syncnum)
    values
      (:new.EXCHANGECODE,PartsExchange_DTM_increase.nextval);
  END;
END;