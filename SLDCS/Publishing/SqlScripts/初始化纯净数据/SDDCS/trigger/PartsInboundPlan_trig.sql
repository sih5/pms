create or replace trigger PartsInboundPlan_trig
  after INSERT ON PartsInboundPlan
  FOR EACH ROW
DECLARE
BEGIN
  insert into PartsInboundPlan_sync
    (billid, syncnum)
  values
    (:new.Id, PartsInboundPlan_increase.nextval);
END;