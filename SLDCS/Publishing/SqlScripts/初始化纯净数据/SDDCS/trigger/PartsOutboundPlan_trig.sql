create or replace trigger PartsOutboundPlan_trig
  after insert on PartsOutboundPlan
  for each row
declare
begin
  declare
  begin
    if (:new.status = 1)
    then
      insert into PartsOutboundPlan_sync
        (billid, syncnum)
      values
        (:new.Id, PartsOutboundPlan_increase.nextval);
    end if;
  end;
end;