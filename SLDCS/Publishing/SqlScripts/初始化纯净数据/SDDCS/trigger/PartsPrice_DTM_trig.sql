create or replace trigger PartsPrice_DTM_trig
  after INSERT or update ON PartsRetailGuidePrice
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
     merge into SparePart_DTM_sync bys
    using (select '1' from dual) N
    on (bys.BILLID = :new.sparepartid)
    WHEN MATCHED THEN
      UPDATE set syncnum = SparePart_DTM_increase.nextval
    WHEN NOT MATCHED THEN
      INSERT
        (SYNCNUM, BILLID)
      VALUES
        (SparePart_DTM_increase.NEXTVAL, :NEW.sparepartid);

         merge into PartsPrice_DTM_sync bys2
    using (select '1' from dual) N
    on (bys2.BILLID = :new.sparepartid)
    WHEN MATCHED THEN
      UPDATE set syncnum = PartsPrice_DTM_increase.nextval
    WHEN NOT MATCHED THEN
      INSERT
        (SYNCNUM, BILLID)
      VALUES
        (PartsPrice_DTM_increase.NEXTVAL, :NEW.sparepartid);
  END;
END;