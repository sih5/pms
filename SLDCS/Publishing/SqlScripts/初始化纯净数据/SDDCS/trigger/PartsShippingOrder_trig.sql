create or replace trigger PartsShippingOrder_trig
  after update ON PartsShippingOrder
  FOR EACH ROW
DECLARE
  FIsExists number(1);
BEGIN
  DECLARE
  BEGIN

    BEGIN
      select 1
        into FIsExists
        from PartsShippingOrder_sync
       where billid = :new.id;
    EXCEPTION
      WHEN others THEN
        FIsExists := null;
    END;

    if :new.Status =2 and FIsExists is null  then
    insert into PartsShippingOrder_sync
      (billid, syncnum)
    values
      (:new.Id,PartsShippingOrder_increase.nextval);
    end if ;
  END;
END;