create or replace trigger RepairCategory_DTM_trig
  after INSERT or update ON Repairitemcategory
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    insert into RepairCategory_DTM_sync
      (billid, syncnum)
    values
      (:new.Id,RepairCategory_DTM_increase.nextval);
  END;
END;