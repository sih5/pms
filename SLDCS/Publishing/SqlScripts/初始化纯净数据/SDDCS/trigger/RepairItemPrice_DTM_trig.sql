create or replace trigger RepairItemPrice_DTM_trig
  after INSERT or update ON Laborhourunitpricerate
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    insert into RepairItemPrice_DTM_sync
      (billid, syncnum)
    values
      (:new.Id,RepairItemPrice_DTM_increase.nextval);
  END;
END;