create or replace trigger RetainedVehicle_TRG
before INSERT   ON Retainedcustomervehiclelist
FOR EACH ROW
declare
  checkcount number;
  checkException exception;
begin
  begin
    select count(*)
      into checkcount
      from Retainedcustomervehiclelist
     where (status <> 99)
       and Retainedcustomervehiclelist.id <> :new.id

       and Retainedcustomervehiclelist.Vehicleid = :new.Vehicleid;
    if (checkcount > 0) and (:new.status = 1)  then
      raise checkException;
    end if;
  exception
    when checkException then
      RAISE_APPLICATION_ERROR(-20001, '异常：已存在非作废的保有客户车辆清单');
  end;
END RetainedVehicle_TRG;