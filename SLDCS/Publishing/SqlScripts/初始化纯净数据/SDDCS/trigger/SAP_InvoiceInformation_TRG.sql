CREATE OR REPLACE TRIGGER SAP_InvoiceInformation_TRG
  AFTER INSERT OR UPDATE ON InvoiceInformation
  FOR EACH ROW
declare
 partsCount number(9);
BEGIN

IF(:new.SourceType = 4) then
  select PartsPurchaseRtnSettleBillId
  into partsCount
  from (select case (select count(*)
                   from PartsPurchaseRtnSettleBill where InvoicePath = 1 and Id = :NEW.SourceId)
                 when 1 then
                  (select id
                     from PartsPurchaseRtnSettleBill where InvoicePath = 1 and Id = :NEW.SourceId)
                 else
                  -1
               end as PartsPurchaseRtnSettleBillId
          from dual);
  --时代
  IF(partsCount >0) then
    INSERT INTO SAP_27_SYNC_SD  VALUES (S_SAP_27_SYNC_SD.NEXTVAL, :NEW.SourceId,'InvoiceInformation');--SAP_27(采购退货结算开发票)
  END IF;
END IF;

END SAP_InvoiceInformation_TRG;