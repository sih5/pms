CREATE OR REPLACE TRIGGER SAP_PARTSINBOUNDCHECKBILL_TRG
AFTER INSERT ON PARTSINBOUNDCHECKBILL
FOR EACH ROW
DECLARE
 LSCOMPANYID NUMBER(9);
 YXCOMPANYID NUMBER(9);
 GCCOMPANYID NUMBER(9);
 FDCOMPANYID NUMBER(9);
 JTCOMPANYID NUMBER(9);
 SDCOMPANYID NUMBER(9);
BEGIN
SELECT COMPANYID
  INTO FDCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '2450'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '2450'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
SELECT COMPANYID  INTO LSCOMPANYID FROM (
SELECT CASE (SELECT COUNT(*)
           FROM COMPANY
          WHERE CODE = '2470'
            AND STATUS = 1
            AND ROWNUM = 1)
         WHEN 1 THEN
          (SELECT ID
             FROM COMPANY
            WHERE CODE = '2470'
              AND STATUS = 1
              AND ROWNUM = 1)
         ELSE
          -1
       END AS COMPANYID
  FROM DUAL);
SELECT COMPANYID
  INTO YXCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '1101'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '1101'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
SELECT COMPANYID
  INTO GCCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '2230'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '2230'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
SELECT COMPANYID
  INTO JTCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '2601'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '2601'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
SELECT COMPANYID
  INTO SDCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '2290'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '2290'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
  --营销
  IF (:NEW.INBOUNDTYPE=1 AND (:NEW.STORAGECOMPANYID =JTCOMPANYID OR :NEW.STORAGECOMPANYID =YXCOMPANYID OR :NEW.STORAGECOMPANYID =LSCOMPANYID OR :NEW.STORAGECOMPANYID =GCCOMPANYID)) THEN --SAP_9（采购入库)
    INSERT INTO SAP_9_SYNC VALUES(S_SAP_9_SYNC.NEXTVAL,:NEW.ID,'PARTSINBOUNDCHECKBILL');
  END IF;
  IF ((:NEW.INBOUNDTYPE=2 or :NEW.INBOUNDTYPE=8) AND (:NEW.STORAGECOMPANYID =JTCOMPANYID OR :NEW.STORAGECOMPANYID =YXCOMPANYID OR :NEW.STORAGECOMPANYID =LSCOMPANYID OR :NEW.STORAGECOMPANYID =GCCOMPANYID)) THEN --SAP_6(销售退货入库)
    INSERT INTO SAP_6_SYNC VALUES(S_SAP_6_SYNC.NEXTVAL,:NEW.ID,'PARTSINBOUNDCHECKBILL');
  END IF;
  --时代
  IF (:NEW.INBOUNDTYPE=1 AND (:NEW.STORAGECOMPANYID =SDCOMPANYID)) THEN --SAP_9（采购入库)
    INSERT INTO SAP_9_SYNC_SD VALUES(S_SAP_9_SYNC_SD.NEXTVAL,:NEW.ID,'PARTSINBOUNDCHECKBILL');
  END IF;
  IF ((:NEW.INBOUNDTYPE=2 or :NEW.INBOUNDTYPE=8) AND (:NEW.STORAGECOMPANYID =SDCOMPANYID)) THEN --SAP_6(销售退货入库)
    INSERT INTO SAP_6_SYNC_SD VALUES(S_SAP_6_SYNC_SD.NEXTVAL,:NEW.ID,'PARTSINBOUNDCHECKBILL');
  END IF;
END SAP_PARTSINBOUNDCHECKBILL_TRG;