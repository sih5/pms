CREATE OR REPLACE TRIGGER SAP_PARTSOUTBOUNDPLAN_TRG
  AFTER INSERT OR UPDATE ON PARTSOUTBOUNDPLAN
  FOR EACH ROW
DECLARE
 LSCOMPANYID NUMBER(9);
 YXCOMPANYID NUMBER(9);
 GCCOMPANYID NUMBER(9);
 FDCOMPANYID NUMBER(9);
 JTCOMPANYID NUMBER(9);
 SDCOMPANYID NUMBER(9);
BEGIN
SELECT COMPANYID
  INTO FDCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '2450'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '2450'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
SELECT COMPANYID  INTO LSCOMPANYID FROM (
SELECT CASE (SELECT COUNT(*)
           FROM COMPANY
          WHERE CODE = '2470'
            AND STATUS = 1
            AND ROWNUM = 1)
         WHEN 1 THEN
          (SELECT ID
             FROM COMPANY
            WHERE CODE = '2470'
              AND STATUS = 1
              AND ROWNUM = 1)
         ELSE
          -1
       END AS COMPANYID
  FROM DUAL);
SELECT COMPANYID
  INTO YXCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '1101'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '1101'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
SELECT COMPANYID
  INTO GCCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '2230'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '2230'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
SELECT COMPANYID
  INTO JTCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '2601'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '2601'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
SELECT COMPANYID
  INTO SDCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '2290'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '2290'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
  --营销
  IF( :NEW.OUTBOUNDTYPE=4 AND (:OLD.STATUS<>:NEW.STATUS) AND  (:NEW.STATUS=3 OR :NEW.STATUS=4) AND (:NEW.STORAGECOMPANYID=JTCOMPANYID OR :NEW.STORAGECOMPANYID=LSCOMPANYID OR  :NEW.STORAGECOMPANYID = YXCOMPANYID  OR :NEW.STORAGECOMPANYID=GCCOMPANYID) ) THEN
    INSERT INTO SAP_11_SYNC  VALUES  (S_SAP_11_SYNC.NEXTVAL, :NEW.SOURCEID, 'PARTSTRANSFERORDER');--SAP_11(调拨)
  END IF;
  --时代
  IF( :NEW.OUTBOUNDTYPE=4 AND (:OLD.STATUS<>:NEW.STATUS) AND  (:NEW.STATUS=3 OR :NEW.STATUS=4) AND (:NEW.STORAGECOMPANYID=SDCOMPANYID) ) THEN
    INSERT INTO SAP_11_SYNC_SD  VALUES  (S_SAP_11_SYNC_SD.NEXTVAL, :NEW.SOURCEID, 'PARTSTRANSFERORDER');--SAP_11(调拨)
  END IF;


END SAP_PARTSTRANSFERORDER_TRG;