CREATE OR REPLACE TRIGGER SAP_PARTSSALESSETTLEMENT_TRG
  AFTER INSERT OR UPDATE ON PARTSSALESSETTLEMENT
  FOR EACH ROW
DECLARE
 LSCOMPANYID NUMBER(9);
 YXCOMPANYID NUMBER(9);
 GCCOMPANYID NUMBER(9);
 FDCOMPANYID NUMBER(9);
 JTCOMPANYID NUMBER(9);
 SDCOMPANYID NUMBER(9);
BEGIN
SELECT COMPANYID
  INTO FDCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '2450'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '2450'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
SELECT COMPANYID  INTO LSCOMPANYID FROM (
SELECT CASE (SELECT COUNT(*)
           FROM COMPANY
          WHERE CODE = '2470'
            AND STATUS = 1
            AND ROWNUM = 1)
         WHEN 1 THEN
          (SELECT ID
             FROM COMPANY
            WHERE CODE = '2470'
              AND STATUS = 1
              AND ROWNUM = 1)
         ELSE
          -1
       END AS COMPANYID
  FROM DUAL);
SELECT COMPANYID
  INTO YXCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '1101'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '1101'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
SELECT COMPANYID
  INTO GCCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '2230'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '2230'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
SELECT COMPANYID
  INTO JTCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '2601'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '2601'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
SELECT COMPANYID
  INTO SDCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '2290'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '2290'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);        
  --营销
  IF(:NEW.STATUS=2 AND :NEW.SETTLEMENTPATH=1 AND :NEW.TOTALSETTLEMENTAMOUNT<>0 AND (:NEW.SALESCOMPANYID=LSCOMPANYID OR :NEW.SALESCOMPANYID = YXCOMPANYID OR :NEW.SALESCOMPANYID= GCCOMPANYID OR :NEW.SALESCOMPANYID= JTCOMPANYID)) THEN
    INSERT INTO SAP_14_SYNC  VALUES (S_SAP_14_SYNC.NEXTVAL, :NEW.ID,'PARTSSALESSETTLEMENT');--SAP_14(销售结算)
  END IF;
  IF(:NEW.STATUS=3 AND :NEW.SETTLEMENTPATH=1 AND (:NEW.SALESCOMPANYID=LSCOMPANYID OR :NEW.SALESCOMPANYID = YXCOMPANYID OR :NEW.SALESCOMPANYID = GCCOMPANYID OR :NEW.SALESCOMPANYID= JTCOMPANYID)) THEN --改成发票登记才传 成本结转
   INSERT INTO SAP_2_SYNC  VALUES (S_SAP_2_SYNC.NEXTVAL, :NEW.ID, 'PARTSSALESSETTLEMENT');--SAP_2(销售成本结转) 由于存在结算金额为0但是成本不为0的，所以以成本金额为准，这个放在代码里面过滤
  END IF;
  --时代
  IF(:NEW.STATUS=2 AND :NEW.SETTLEMENTPATH=1 AND :NEW.TOTALSETTLEMENTAMOUNT<>0 AND (:NEW.SALESCOMPANYID=SDCOMPANYID )) THEN
    INSERT INTO SAP_14_SYNC_SD  VALUES (S_SAP_14_SYNC_SD.NEXTVAL, :NEW.ID,'PARTSSALESSETTLEMENT');--SAP_14(销售结算)
  END IF;
  IF(:NEW.STATUS=3 AND :NEW.SETTLEMENTPATH=1 AND (:NEW.SALESCOMPANYID=SDCOMPANYID )) THEN --改成发票登记才传 成本结转
   INSERT INTO SAP_2_SYNC_SD  VALUES (S_SAP_2_SYNC_SD.NEXTVAL, :NEW.ID, 'PARTSSALESSETTLEMENT');--SAP_2(销售成本结转) 由于存在结算金额为0但是成本不为0的，所以以成本金额为准，这个放在代码里面过滤
  END IF;
  

END SAP_PARTSSALESSETTLEMENT_TRG;