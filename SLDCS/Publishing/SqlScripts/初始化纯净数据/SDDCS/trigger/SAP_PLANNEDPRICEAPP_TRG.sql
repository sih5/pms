CREATE OR REPLACE TRIGGER SAP_PLANNEDPRICEAPP_TRG
  AFTER UPDATE ON PLANNEDPRICEAPP
  FOR EACH ROW
DECLARE
 LSCOMPANYID NUMBER(9);
 YXCOMPANYID NUMBER(9);
 GCCOMPANYID NUMBER(9);
 FDCOMPANYID NUMBER(9);
 JTCOMPANYID NUMBER(9);
 SDCOMPANYID NUMBER(9);
BEGIN
SELECT COMPANYID
  INTO FDCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '2450'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '2450'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
SELECT COMPANYID  INTO LSCOMPANYID FROM (
SELECT CASE (SELECT COUNT(*)
           FROM COMPANY
          WHERE CODE = '2470'
            AND STATUS = 1
            AND ROWNUM = 1)
         WHEN 1 THEN
          (SELECT ID
             FROM COMPANY
            WHERE CODE = '2470'
              AND STATUS = 1
              AND ROWNUM = 1)
         ELSE
          -1
       END AS COMPANYID
  FROM DUAL);
SELECT COMPANYID
  INTO YXCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '1101'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '1101'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
SELECT COMPANYID
  INTO GCCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '2230'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '2230'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
SELECT COMPANYID
  INTO JTCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '2601'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '2601'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);
SELECT COMPANYID
  INTO SDCOMPANYID
  FROM (SELECT CASE (SELECT COUNT(*)
                   FROM COMPANY
                  WHERE CODE = '2290'
                    AND STATUS = 1
                    AND ROWNUM = 1)
                 WHEN 1 THEN
                  (SELECT ID
                     FROM COMPANY
                    WHERE CODE = '2290'
                      AND STATUS = 1
                      AND ROWNUM = 1)
                 ELSE
                  -1
               END AS COMPANYID
          FROM DUAL);         
  --营销
  IF(:NEW.STATUS=3 AND :OLD.STATUS=2 AND (:NEW.OWNERCOMPANYID=JTCOMPANYID OR :NEW.OWNERCOMPANYID=LSCOMPANYID OR :NEW.OWNERCOMPANYID=YXCOMPANYID OR :NEW.OWNERCOMPANYID=GCCOMPANYID)) THEN
    INSERT INTO SAP_13_SYNC VALUES (S_SAP_13_SYNC.NEXTVAL, :NEW.ID,  'PLANNEDPRICEAPP');--SAP_13(成本变更)
  END IF;
  
  --时代
  IF(:NEW.STATUS=3 AND :OLD.STATUS=2 AND (:NEW.OWNERCOMPANYID=SDCOMPANYID )) THEN
    INSERT INTO SAP_13_SYNC_SD VALUES (S_SAP_13_SYNC_SD.NEXTVAL, :NEW.ID,  'PLANNEDPRICEAPP');--SAP_13(成本变更)
  END IF;
END SAP_PLANNEDPRICEAPP_TRG;