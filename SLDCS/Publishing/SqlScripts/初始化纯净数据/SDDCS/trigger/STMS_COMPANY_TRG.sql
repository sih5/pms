CREATE OR REPLACE TRIGGER STMS_COMPANY_TRG
AFTER INSERT OR UPDATE ON COMPANY
FOR EACH ROW
BEGIN
  IF(:NEW.TYPE=2 OR :NEW.TYPE=7 OR :NEW.TYPE=10 ) THEN
  BEGIN
    INSERT INTO STMS_4_SYNC_SD VALUES(S_STMS_4_SYNC_SD.NEXTVAL,:NEW.ID,'COMPANY');
  END;
  END IF;
END STMS_COMPANY_TRG;