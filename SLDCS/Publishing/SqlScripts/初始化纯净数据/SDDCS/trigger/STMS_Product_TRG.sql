CREATE OR REPLACE TRIGGER STMS_Product_TRG
 AFTER INSERT OR UPDATE ON Product
 FOR EACH ROW

BEGIN
   INSERT INTO STMS_9_SYNC_SD VALUES(S_STMS_9_SYNC_SD.NEXTVAL,:NEW.ID,'Product');
 END STMS_Product_TRg ;