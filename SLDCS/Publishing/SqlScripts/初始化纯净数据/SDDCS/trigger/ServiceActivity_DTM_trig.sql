create or replace trigger ServiceActivity_DTM_trig
  after INSERT or update ON ServiceActivity
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    insert into ServiceActivity_DTM_sync
      (billid, syncnum)
    values
      (:new.Id,ServiceActivity_DTM_increase.nextval);
  END;
END;