create or replace trigger ServiceCondition_DTM_trig
  after INSERT or update ON Serviceactivitycondition
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    insert into ServiceCondition_DTM_sync
      (billid, syncnum)
    values
      (:new.Id,ServiceCondition_DTM_increase.nextval);
  END;
END;