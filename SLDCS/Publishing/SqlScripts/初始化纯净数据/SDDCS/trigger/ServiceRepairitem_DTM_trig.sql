create or replace trigger ServiceRepairitem_DTM_trig
  after INSERT or update ON ServiceActivityRepairitem
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    insert into ServiceRepairitem_DTM_sync
      (billid, syncnum)
    values
      (:new.Id,ServiceRepairitem_DTM_increase.nextval);
  END;
END;