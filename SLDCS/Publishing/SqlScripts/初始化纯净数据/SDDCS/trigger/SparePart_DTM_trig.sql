create or replace trigger SparePart_DTM_trig
  after INSERT or update ON SparePart
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
     merge into SparePart_DTM_sync bys
    using (select '1' from dual) N
    on (bys.BILLID = :new.Id)
    WHEN MATCHED THEN
      UPDATE set syncnum = SparePart_DTM_increase.nextval
    WHEN NOT MATCHED THEN
      INSERT
        (SYNCNUM, BILLID)
      VALUES
        (SparePart_DTM_increase.NEXTVAL, :NEW.id);

         merge into PartsPrice_DTM_sync bys2
    using (select '1' from dual) N
    on (bys2.BILLID = :new.Id)
    WHEN MATCHED THEN
      UPDATE set syncnum = PartsPrice_DTM_increase.nextval
    WHEN NOT MATCHED THEN
      INSERT
        (SYNCNUM, BILLID)
      VALUES
        (PartsPrice_DTM_increase.NEXTVAL, :NEW.id);
  END;
END;