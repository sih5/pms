create or replace trigger SparePart_Trg1
   after  update on SparePart
   for each row
begin

  if  (:new.Name<>:old.Name) then
     update  PartsSalesPrice b            set SparePartName=:new.name  where b.SparePartId=:new.id;
     update  PartsRetailGuidePrice b  set SparePartName=:new.name  where b.SparePartId=:new.id;
  end if;

end SparePart_Trg1;