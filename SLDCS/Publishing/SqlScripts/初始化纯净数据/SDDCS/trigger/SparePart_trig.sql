create or replace trigger SparePart_trig
  after insert or update ON SparePart
  FOR EACH ROW
DECLARE
BEGIN

    INSERT into SparePart_sync (billid, syncnum) values (:new.Id, SPAREPART_INCREASE.nextval);
END;