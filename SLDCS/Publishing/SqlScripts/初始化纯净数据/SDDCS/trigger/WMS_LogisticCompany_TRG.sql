CREATE OR REPLACE TRIGGER WMS_LogisticCompany_TRG
AFTER INSERT OR UPDATE ON LogisticCompany
FOR EACH ROW
BEGIN
  if(:new.StorageCenter <> 5 and :new.StorageCenter <> 6) then
  INSERT INTO WMS_LogisticCompany_SYNC VALUES(S_WMS_LogisticCompany.NEXTVAL,:NEW.ID,'LogisticCompany',sysdate);
  end if;
END WMS_LogisticCompany_TRG;