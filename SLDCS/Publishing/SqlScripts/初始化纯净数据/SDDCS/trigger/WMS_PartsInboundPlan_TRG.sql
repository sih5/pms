CREATE OR REPLACE TRIGGER WMS_PartsInboundPlan_TRG
AFTER INSERT   ON PartsInboundPlan
FOR EACH ROW
declare countnumFault number(9); 
BEGIN
  begin
             select count(*) 
             into countnumFault
               from warehouse r
               inner join  (select PMSStorageCenterId, warehousecode from t_warehouse) tw
               on  r.StorageCenter = tw.PMSStorageCenterId
             where r.Id=:new.WarehouseId ;
             EXCEPTION
             when no_data_found then
               countnumFault:=0;
         end ; 
if(:new.Status = 1 and countnumFault > 0 ) then
  INSERT INTO WMS_PartsInboundPlan_SYNC VALUES(S_WMS_PartsInboundPlan.NEXTVAL,:NEW.ID,'PartsInboundPlan',sysdate);
  end if;
END WMS_PartsInboundPlan_TRG;