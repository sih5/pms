create or replace trigger customeraccountlog_trg
  after update on customeraccount
  for each row
declare
  -- local variables here

begin
  insert into customeraccountlog
    (customeraccountid,
     pendingamountnew,
     pendingamountold,
     shippedproductvaluenew,
     shippedproductvalueold,accountbalancenew,accountbalanceold,
     logtime)
  values
    (:new.id,
     :new.pendingamount,
     :old.pendingamount,
     :new.shippedproductvalue,
     :old.shippedproductvalue,:new.accountbalance,:old.accountbalance,

     sysdate);
end customeraccountlog_trg;