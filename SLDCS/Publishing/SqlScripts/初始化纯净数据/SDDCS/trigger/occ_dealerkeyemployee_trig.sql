create or replace trigger occ_dealerkeyemployee_trig
  after INSERT or update ON dealerkeyemployee
  FOR EACH ROW
DECLARE
  iCount              NUMBER(4);
  DealerserviceinfoId NUMBER(9);
BEGIN
   select count(*)
      into iCount
      from Dealerserviceinfo d
     where d.dealerid = :new.dealerid
       and d.partssalescategoryid = :new.partssalescategoryid
       and exists (select b.id
              from DealerKeyPosition b
             where b.id = :new.KeyPositionId
               and b.positionname in ('站长', '服务协调人')
               and b.status = 1)and status=1;

  if iCount > 0 then
    select id
      into DealerserviceinfoId
      from Dealerserviceinfo d
     where d.dealerid = :new.dealerid
       and d.partssalescategoryid = :new.partssalescategoryid
       and exists (select b.id
              from DealerKeyPosition b
             where b.id = :new.KeyPositionId
               and b.positionname in ('站长', '服务协调人')
               and b.status = 1)and status=1;

    if inserting then
      merge into OCC_DEALERSERVICEINFO_SYNC bys
      using (select '1' from dual) N
      on (bys.BILLID = DealerserviceinfoId)
      WHEN MATCHED THEN
        UPDATE set syncnum = S_OCC_DEALERSERVICEINFO.nextval
      WHEN NOT MATCHED THEN
        INSERT
          (SYNCNUM, BILLID)
        VALUES
          (S_OCC_DEALERSERVICEINFO.NEXTVAL, DealerserviceinfoId);
    else
      if (:new.mobilenumber <> :old.mobilenumber) or
         (:new.name <> :old.name) then
        merge into OCC_DEALERSERVICEINFO_SYNC bys
        using (select '1' from dual) N
        on (bys.BILLID = DealerserviceinfoId)
        WHEN MATCHED THEN
          UPDATE set syncnum = S_OCC_DEALERSERVICEINFO.nextval
        WHEN NOT MATCHED THEN
          INSERT
            (SYNCNUM, BILLID)
          VALUES
            (S_OCC_DEALERSERVICEINFO.NEXTVAL, DealerserviceinfoId);
      end if;
    end if;
  end if;
END;