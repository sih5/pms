create or replace trigger product_DTM_trig
  after INSERT or update ON PRODUCT
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    insert into product_DTM_sync
      (billid, syncnum)
    values
      (:new.Id,product_DTM_increase.nextval);

  END;
END;