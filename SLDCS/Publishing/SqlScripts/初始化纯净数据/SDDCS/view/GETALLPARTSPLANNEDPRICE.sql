CREATE OR REPLACE VIEW GETALLPARTSPLANNEDPRICE AS
SELECT Extent1.PartsSalesCategoryName,
       Extent1.PlannedPrice,
       Extent1.CreatorName,
       Extent1.CreateTime,
       Extent1.ModifierName,
       Extent1.ModifyTime,
       Extent2.Code,
       Extent2.Name
  FROM dcs.PartsPlannedPrice Extent1
  LEFT OUTER JOIN dcs.SparePart Extent2
    ON Extent1.SparePartId = Extent2.Id
union
SELECT Extent1.PartsSalesCategoryName as 品牌,
       Extent1.PlannedPrice           as 计划价,
       Extent1.CreatorName            as 创建人,
       Extent1.CreateTime             as 创建时间,
       Extent1.ModifierName           as 修改人,
       Extent1.ModifyTime             as 修改时间,
       Extent2.Code                   as 配件编号,
       Extent2.Name                   as 配件名称
  FROM yxdcs.PartsPlannedPrice Extent1
  LEFT OUTER JOIN yxdcs.SparePart Extent2
    ON Extent1.SparePartId = Extent2.Id
union
SELECT Extent1.PartsSalesCategoryName as 品牌,
       Extent1.PlannedPrice           as 计划价,
       Extent1.CreatorName            as 创建人,
       Extent1.CreateTime             as 创建时间,
       Extent1.ModifierName           as 修改人,
       Extent1.ModifyTime             as 修改时间,
       Extent2.Code                   as 配件编号,
       Extent2.Name                   as 配件名称
  FROM sddcs.PartsPlannedPrice Extent1
  LEFT OUTER JOIN sddcs.SparePart Extent2
    ON Extent1.SparePartId = Extent2.Id
union
SELECT Extent1.PartsSalesCategoryName as 品牌,
       Extent1.PlannedPrice           as 计划价,
       Extent1.CreatorName            as 创建人,
       Extent1.CreateTime             as 创建时间,
       Extent1.ModifierName           as 修改人,
       Extent1.ModifyTime             as 修改时间,
       Extent2.Code                   as 配件编号,
       Extent2.Name                   as 配件名称
  FROM gcdcs.PartsPlannedPrice Extent1
  LEFT OUTER JOIN gcdcs.SparePart Extent2
    ON Extent1.SparePartId = Extent2.Id;
