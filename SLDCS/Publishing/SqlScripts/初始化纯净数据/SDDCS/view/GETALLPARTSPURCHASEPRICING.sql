CREATE OR REPLACE VIEW GETALLPARTSPURCHASEPRICING AS
SELECT distinct  Extent1.PartsSalesCategoryName as,
       Extent1.ValidFrom,
       Extent1.ValidTo,
       Extent1.PriceType,
       Extent1.PurchasePrice,
       Extent1.Status,
       Extent1.CreatorName,
       Extent1.CreateTime,
       Extent1.ModifierName,
       Extent1.ModifyTime,
       Extent1.AbandonerName,
       Extent1.AbandonTime,
       Extent1.Remark,
       Extent2.Code PartCode,
       Extent2.Name PartName,
       Extent3.Name BranchName,
       Extent4.Code PartsSupplierCode,
       Extent4.Name PartsSupplierName,
       Extent5.IsOrderable,
       Extent5.IsSalable
  FROM dcs.PartsPurchasePricing Extent1
  LEFT OUTER JOIN dcs.SparePart Extent2
    ON Extent1.PartId = Extent2.Id
  LEFT OUTER JOIN dcs.Branch Extent3
    ON Extent1.BranchId = Extent3.Id
  LEFT OUTER JOIN dcs.PartsSupplier Extent4
    ON Extent1.PartsSupplierId = Extent4.Id
  LEFT OUTER JOIN dcs.PartsBranch Extent5
    ON Extent1.PartId = Extent5.PartId AND Extent1.PartsSalesCategoryId=Extent5.PartsSalesCategoryId
 WHERE ((Extent1.Status <> 99))
union
SELECT distinct  Extent1.PartsSalesCategoryName as 品牌,
       Extent1.ValidFrom              as 生效时间,
       Extent1.ValidTo                as 失效时间,
       Extent1.PriceType              as 价格类型,
       Extent1.PurchasePrice          as 采购价格,
       Extent1.Status                 as 状态,
       Extent1.CreatorName            as 创建人,
       Extent1.CreateTime             as 创建时间,
       Extent1.ModifierName           as 修改人,
       Extent1.ModifyTime             as 修改时间,
       Extent1.AbandonerName          as 作废人,
       Extent1.AbandonTime            as 作废时间,
       Extent1.Remark                 as 备注,
       Extent2.Code                   as 配件编号,
       Extent2.Name                   as 配件名称,
       Extent3.Name                   as 分公司名称,
       Extent4.Code                   as 供应商编号,
       Extent4.Name                   as 供应商名称,
       Extent5.IsOrderable            as 是否可采购,
       Extent5.IsSalable              as 是否可销售
  FROM yxdcs.PartsPurchasePricing Extent1
  LEFT OUTER JOIN yxdcs.SparePart Extent2
    ON Extent1.PartId = Extent2.Id
  LEFT OUTER JOIN yxdcs.Branch Extent3
    ON Extent1.BranchId = Extent3.Id
  LEFT OUTER JOIN yxdcs.PartsSupplier Extent4
    ON Extent1.PartsSupplierId = Extent4.Id
  LEFT OUTER JOIN yxdcs.PartsBranch Extent5
    ON Extent1.PartId = Extent5.PartId AND Extent1.PartsSalesCategoryId=Extent5.PartsSalesCategoryId
 WHERE ((Extent1.Status <> 99))
union
SELECT distinct  Extent1.PartsSalesCategoryName as 品牌,
       Extent1.ValidFrom              as 生效时间,
       Extent1.ValidTo                as 失效时间,
       Extent1.PriceType              as 价格类型,
       Extent1.PurchasePrice          as 采购价格,
       Extent1.Status                 as 状态,
       Extent1.CreatorName            as 创建人,
       Extent1.CreateTime             as 创建时间,
       Extent1.ModifierName           as 修改人,
       Extent1.ModifyTime             as 修改时间,
       Extent1.AbandonerName          as 作废人,
       Extent1.AbandonTime            as 作废时间,
       Extent1.Remark                 as 备注,
       Extent2.Code                   as 配件编号,
       Extent2.Name                   as 配件名称,
       Extent3.Name                   as 分公司名称,
       Extent4.Code                   as 供应商编号,
       Extent4.Name                   as 供应商名称,
       Extent5.IsOrderable            as 是否可采购,
       Extent5.IsSalable              as 是否可销售
  FROM sddcs.PartsPurchasePricing Extent1
  LEFT OUTER JOIN sddcs.SparePart Extent2
    ON Extent1.PartId = Extent2.Id
  LEFT OUTER JOIN sddcs.Branch Extent3
    ON Extent1.BranchId = Extent3.Id
  LEFT OUTER JOIN sddcs.PartsSupplier Extent4
    ON Extent1.PartsSupplierId = Extent4.Id
  LEFT OUTER JOIN sddcs.PartsBranch Extent5
    ON Extent1.PartId = Extent5.PartId AND Extent1.PartsSalesCategoryId=Extent5.PartsSalesCategoryId
 WHERE ((Extent1.Status <> 99))
union
SELECT distinct  Extent1.PartsSalesCategoryName as 品牌,
       Extent1.ValidFrom              as 生效时间,
       Extent1.ValidTo                as 失效时间,
       Extent1.PriceType              as 价格类型,
       Extent1.PurchasePrice          as 采购价格,
       Extent1.Status                 as 状态,
       Extent1.CreatorName            as 创建人,
       Extent1.CreateTime             as 创建时间,
       Extent1.ModifierName           as 修改人,
       Extent1.ModifyTime             as 修改时间,
       Extent1.AbandonerName          as 作废人,
       Extent1.AbandonTime            as 作废时间,
       Extent1.Remark                 as 备注,
       Extent2.Code                   as 配件编号,
       Extent2.Name                   as 配件名称,
       Extent3.Name                   as 分公司名称,
       Extent4.Code                   as 供应商编号,
       Extent4.Name                   as 供应商名称,
       Extent5.IsOrderable            as 是否可采购,
       Extent5.IsSalable              as 是否可销售
  FROM gcdcs.PartsPurchasePricing Extent1
  LEFT OUTER JOIN gcdcs.SparePart Extent2
    ON Extent1.PartId = Extent2.Id
  LEFT OUTER JOIN gcdcs.Branch Extent3
    ON Extent1.BranchId = Extent3.Id
  LEFT OUTER JOIN gcdcs.PartsSupplier Extent4
    ON Extent1.PartsSupplierId = Extent4.Id
  LEFT OUTER JOIN gcdcs.PartsBranch Extent5
    ON Extent1.PartId = Extent5.PartId AND Extent1.PartsSalesCategoryId=Extent5.PartsSalesCategoryId
 WHERE ((Extent1.Status <> 99));
