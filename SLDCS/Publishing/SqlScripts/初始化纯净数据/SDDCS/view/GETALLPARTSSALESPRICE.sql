CREATE OR REPLACE VIEW GETALLPARTSSALESPRICE AS
SELECT distinct  Extent1.PartsSalesCategoryName,
       Extent1.SparePartCode,
       Extent1.SparePartName,
       Extent1.IfClaim,
       Extent1.SalesPrice,
       Extent1.PriceType,
       Extent1.Status,
       Extent1.Remark,
       Extent1.CreatorName,
       Extent1.CreateTime,
       Extent1.ModifierName,
       Extent1.ModifyTime,
       Extent1.ApproverName,
       Extent1.ApproveTime,
       Extent1.AbandonerName,
       Extent1.AbandonTime,
       Extent2.IsOrderable,
       Extent2.IsSalable
  FROM dcs.PartsSalesPrice Extent1
  LEFT OUTER JOIN dcs.PartsBranch Extent2
  ON Extent2.Partid=Extent1.Sparepartid
  and Extent1.Partssalescategoryid=Extent2.Partssalescategoryid
 WHERE Extent1.Status <> 99
union
SELECT distinct  Extent1.PartsSalesCategoryName as 品牌,
       Extent1.SparePartCode          as 配件编号,
       Extent1.SparePartName          as 配件名称,
       Extent1.IfClaim                as 是否用于索赔,
       Extent1.SalesPrice             as 销售价,
       Extent1.PriceType              as 价格类型,
       Extent1.Status                 as 状态,
       Extent1.Remark                 as 备注,
       Extent1.CreatorName            as 创建人,
       Extent1.CreateTime             as 创建时间,
       Extent1.ModifierName           as 修改人,
       Extent1.ModifyTime             as 修改时间,
       Extent1.ApproverName           as 审批人,
       Extent1.ApproveTime            as 审批时间,
       Extent1.AbandonerName          as 作废人,
       Extent1.AbandonTime            as 作废时间,
       Extent2.IsOrderable            as 是否可采购,
       Extent2.IsSalable              as 是否可销售
  FROM yxdcs.PartsSalesPrice Extent1
  LEFT OUTER JOIN yxdcs.PartsBranch Extent2
  ON Extent2.Partid=Extent1.Sparepartid
  and Extent1.Partssalescategoryid=Extent2.Partssalescategoryid
 WHERE Extent1.Status <> 99
union
SELECT distinct  Extent1.PartsSalesCategoryName as 品牌,
       Extent1.SparePartCode          as 配件编号,
       Extent1.SparePartName          as 配件名称,
       Extent1.IfClaim                as 是否用于索赔,
       Extent1.SalesPrice             as 销售价,
       Extent1.PriceType              as 价格类型,
       Extent1.Status                 as 状态,
       Extent1.Remark                 as 备注,
       Extent1.CreatorName            as 创建人,
       Extent1.CreateTime             as 创建时间,
       Extent1.ModifierName           as 修改人,
       Extent1.ModifyTime             as 修改时间,
       Extent1.ApproverName           as 审批人,
       Extent1.ApproveTime            as 审批时间,
       Extent1.AbandonerName          as 作废人,
       Extent1.AbandonTime            as 作废时间,
       Extent2.IsOrderable            as 是否可采购,
       Extent2.IsSalable              as 是否可销售
  FROM sddcs.PartsSalesPrice Extent1
  LEFT OUTER JOIN sddcs.PartsBranch Extent2
  ON Extent2.Partid=Extent1.Sparepartid
  and Extent1.Partssalescategoryid=Extent2.Partssalescategoryid
 WHERE Extent1.Status <> 99
union
SELECT distinct  Extent1.PartsSalesCategoryName as 品牌,
       Extent1.SparePartCode          as 配件编号,
       Extent1.SparePartName          as 配件名称,
       Extent1.IfClaim                as 是否用于索赔,
       Extent1.SalesPrice             as 销售价,
       Extent1.PriceType              as 价格类型,
       Extent1.Status                 as 状态,
       Extent1.Remark                 as 备注,
       Extent1.CreatorName            as 创建人,
       Extent1.CreateTime             as 创建时间,
       Extent1.ModifierName           as 修改人,
       Extent1.ModifyTime             as 修改时间,
       Extent1.ApproverName           as 审批人,
       Extent1.ApproveTime            as 审批时间,
       Extent1.AbandonerName          as 作废人,
       Extent1.AbandonTime            as 作废时间,
       Extent2.IsOrderable            as 是否可采购,
       Extent2.IsSalable              as 是否可销售
  FROM gcdcs.PartsSalesPrice Extent1
  LEFT OUTER JOIN gcdcs.PartsBranch Extent2
  ON Extent2.Partid=Extent1.Sparepartid
  and Extent1.Partssalescategoryid=Extent2.Partssalescategoryid
 WHERE Extent1.Status <> 99;
