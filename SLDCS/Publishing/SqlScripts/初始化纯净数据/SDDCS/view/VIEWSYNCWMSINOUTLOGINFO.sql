CREATE OR REPLACE VIEW VIEWSYNCWMSINOUTLOGINFO AS
SELECT
          t1.Objid,
          t1.SYNCDate,
          t1.Code,
          t1.SYNCType,
          t1.SYNCCode,
          t1.InOutCode,
          t1.Status,
          t3.Name as WarehouseName
          FROM   SYNCWMSINOUTLOGINFO t1
          left JOIN PartsInboundPlan t2 ON t1.InOutCode = t2.Code
          left JOIN Warehouse t3 ON t2.WarehouseId = t3.Id
          WHERE (t1.SYNCType = 1 or t1.synctype=5)
        UNION all
          SELECT
          t4.Objid,
          t4.SYNCDate,
          t4.Code,
          t4.SYNCType,
          t4.SYNCCode,
          t4.InOutCode,
          t4.Status,
          t6.Name as WarehouseName
          FROM   SYNCWMSINOUTLOGINFO t4
          left JOIN PartsOutboundPlan t5 ON t4.InOutCode = t5.Code
          left JOIN Warehouse t6 ON t5.WarehouseId = t6.Id
          WHERE t4.SYNCType = 2
        UNION all
           SELECT
                  t7.Objid,
                  t7.SYNCDate,
                  t7.Code,
                  t7.SYNCType,
                  t7.SYNCCode,
                  t7.InOutCode,
                  t7.Status,
                  to_char(t9.PMSWAREHOUSENAME) as WarehouseName
              FROM   SYNCWMSINOUTLOGINFO t7
              left JOIN  T_Inventory_Adjustment t8 ON t7.SYNCCode = t8.CODE
              left JOIN Wms2PmsWarehouse t9 ON (t8.BRANDNAME = t9.PMSPARTSSALESCATEGORYNAME) AND (t8.WAREHOUSECODE = t9.WMSWAREHOUSECODE)
              WHERE t7.SYNCType = 3;
