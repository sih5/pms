create or replace view partsorderconfirmgapstaviewsd as
select ss.BranchId,      --------------------------分公司Id
       ss.BranchCode,
       ss.partid,        ---------------------------配件Id
       ss.SaleOrderCode, ---------------------------销售订单编码
       ss.SpareCode,     ---------------------------配件图号
       ss.SpareName,     ---------------------------配件名称
       ss.Submitcompanycode,------------------------服务站编号
       ss.submitcompanyname,------------------------服务站名称
       ss.warehouseid,
       ss.WarehouseCode,    ------------------------仓库编码
       ss.WarehouseName,    ------------------------仓库名称
       case when ss.quantity is null then 0 else ss.quantity end as quantity,         ------------------------库存
       case when ss.AvailableStock is null then 0 else ss.AvailableStock end as AvailableStock,   ------------------------可用库存
       case when ss.TransferStock is null then 0 else ss.TransferStock end as TransferStock,    ------------------------调拨在途数量
       case when ss.SupplyStock is null then 0 else ss.SupplyStock end as SupplyStock,      ------------------------采购在途数量
       case when ss.SumOrderAmount is null then 0 else ss.SumOrderAmount end as SumOrderAmount,   ------------------------订单未满足数量
       case when ss.ReplaceStock is null then 0 else ss.ReplaceStock end as ReplaceStock,     ------------------------替换件可用库存
       case when ss.ReplaceSupplyStock is null then 0 else ss.ReplaceSupplyStock end as ReplaceSupplyStock,-----------------------替换件采购在途数量
       case when sum(ExchangeStocks.quantity) is null then 0 else sum(ExchangeStocks.quantity) end as ExchangeStock,-----------------互换件可用库存
       case when sum(ExchangeSupply.amount) is null then 0 else sum(ExchangeSupply.amount) end as ExchangeSupplyStock,--------------互换件采购在途数量
       ss.mdtname,
       ss.PartsSalesOrderTypeName,
       ss.SubmitTime,
       ss.ApproveTime,
       ss.ReferenceCode,     ----------------------零部件图号
       ss.salescategoryid,
       ss.salescategoryname,
       ss.businesscode,
       ss.businessname
  from (select psos.BranchId,   ---------------------------分公司Id
               psos.BranchCode,
               psos.sparepartid as partid, ---------------------------配件Id
               psos.SaleOrderCode,   ---------------------------销售订单编码
               psos.sparepartcode as SpareCode, ---------------------------配件图号
               psos.sparepartname as SpareName, ---------------------------配件名称
               psos.Submitcompanycode,      ------------------------服务站编号
               psos.submitcompanyname,      ------------------------服务站名称
               psos.warehouseid,
               psos.WarehouseCode,          ------------------------仓库编码
               psos.WarehouseName,          ------------------------仓库名称
               psos.ReferenceCode,
               psos.PartsSalesOrderTypeName,
               psos.SubmitTime,
               psos.ApproveTime,
               psos.mdtname,
               psos.salescategoryid,
               psos.salescategoryname,
               psos.businesscode,
               psos.businessname,
               sum(nvl(ps.quantity, 0)) as quantity,----------------库存
               sum(nvl(ps.AvailableStock, 0)) as AvailableStock,------------------------可用库存
               sum(psos.SumOrderAmount) as SumOrderAmount,  -----------------订单未满足数量
               sum((select sum(nvl(nvl(pobd.outboundamount, 0) - nvl(picbd.Inspectedquantity, 0),0))
                     from sddcs.PartsTransferOrder pto  --------------------配件调拨单
                    inner join sddcs.PartsOutboundBill pob  ----------------配件出库单
                       on pob.Originalrequirementbillcode = pto.code and pob.outboundtype = 4
                    inner join sddcs.PartsOutboundBillDetail pobd ----------配件出库单清单
                       on pobd.partsoutboundbillid = pob.id
                     left join sddcs.PartsInboundCheckBill picb   ----------配件入库检验单
                       on picb.Originalrequirementbillcode = pto.code and picb.inboundtype = 3
                     left join sddcs.PartsInboundCheckBillDetail picbd ----------配件入库检验清单
                       on picbd.PartsInboundCheckBillId = picb.id and picbd.sparepartid=pobd.sparepartid where pobd.sparepartid = psos.sparepartid
                    group by pobd.sparepartid)) TransferStock,------------------------调拨在途数量
               sum((select sum(nvl(ppod.ConfirmedAmount, 0) - nvl(pbcb.InspectedQuantity, 0))
                      from sddcs.PartsPurchaseOrder ppo       ------------------------配件采购订单
                     inner join sddcs.PartsPurchaseOrderDetail ppod  -----------------配件采购清单
                        on ppo.id = ppod.partspurchaseorderid
                      left join (select pb.originalrequirementbillcode,pc.sparepartid,sum(pc.inspectedquantity) as InspectedQuantity
                                   from sddcs.partsinboundcheckbill pb ---------------配件入库检验单
                                  inner join sddcs.partsinboundcheckbilldetail pc ----------配件入库检验清单
                                     on pc.partsinboundcheckbillid = pb.id where pb.inboundtype = 1
                                  group by pb.originalrequirementbillcode, pc.sparepartid) pbcb
                        on pbcb.originalrequirementbillcode = ppo.code
                       and pbcb.sparepartid = ppod.sparepartid
                     where ppo.status in (3, 4, 5, 6)
                     and psos.sparepartid=ppod.sparepartid
                     group by ppod.sparepartid
                    )) as SupplyStock,------------------------采购在途数量
               sum((select sum(nvl(psk.quantity, 0) - nvl(plsk.lockedquantity, 0))
                     from (select a.warehouseid,a.partid,sum(a.quantity)as quantity from sddcs.partsstock a where  WarehouseAreaCategoryId<>2 and storagecompanyid in (select id from sddcs.branch) group by a.warehouseid,a.partid
                      ) psk --------------配件库存
                    inner join sddcs.PartsReplacement b ------------配件替换信息
                       on psk.partid = b.newpartid
                    inner join sddcs.warehouse ws   --------------仓库
                       on ws.id = psk.warehouseid /*and ws.Iscentralizedpurchase = 1*/
                    inner join sddcs.SalesUnitAffiWarehouse sw
                     on sw.warehouseid=ws.id
                     inner join sddcs.salesunit st
                     on st.id=sw.salesunitid
                     left join sddcs.PartsLockedStock plsk  ---------------配件锁定库存
                       on ws.id = plsk.warehouseid and psk.partid = plsk.partid
                    where b.OldPartId = psos.sparepartid
                    and st.partssalescategoryid=psos.salescategoryid
            ---        group by psk.partid
            )) ReplaceStock,------------------------替换件可用库存
               sum((select sum(nvl(ppod.ConfirmedAmount, 0) - nvl(pbcb.InspectedQuantity, 0))
                     from sddcs.PartsPurchaseOrder ppo ------------------配件采购订单
                    inner join sddcs.PartsPurchaseOrderDetail ppod  ------------------配件采购清单
                       on ppo.id = ppod.partspurchaseorderid
                    inner join sddcs.PartsReplacement b -------------------配件替换信息
                       on ppod.sparepartid = b.newpartid
                     left join(select pb.originalrequirementbillcode,pc.sparepartid,sum(pc.inspectedquantity) as InspectedQuantity
                 from sddcs.partsinboundcheckbill pb   ---------------------配件入库检验单
                    inner join sddcs.partsinboundcheckbilldetail pc
                       on pc.partsinboundcheckbillid = pb.id where pb.inboundtype = 1
                    group by pb.originalrequirementbillcode, pc.sparepartid) pbcb
                    on pbcb.originalrequirementbillcode = ppo.code and pbcb.sparepartid = ppod.sparepartid
                      where ppo.status in (3, 4, 5,6) and b.OldPartId = psos.sparepartid
            ---        group by ppod.sparepartid
            )) as ReplaceSupplyStock -----------替换件采购在途数量
          from (select pso.branchid,---------------------------分公司Id
                       pso.BranchCode,
                       pso.warehouseid,------------------------仓库ID
                       pso.code as SaleOrderCode,---------------------------销售订单编码
                       w.code as WarehouseCode, ------------------------仓库编码
                       w.name as WarehouseName,------------------------仓库名称
                       pso.Submitcompanycode,------------------------服务站编号
                       pso.submitcompanyname,------------------------服务站名称
                       psod.sparepartid,    -------------------------配件ID
                       psod.sparepartcode,  -------------------------配件图号
                       psod.sparepartname,  -------------------------配件名称
                       sum(OrderedQuantity - nvl(ApproveQuantity, 0)) as SumOrderAmount,   ------------------------订单未满足数量
                       mdt.name as mdtname, ----------------------市场部名称
                       sp.ReferenceCode,     ----------------------零部件图号
                       pso.PartsSalesOrderTypeName,
                       pso.SubmitTime,
                       pso.ApproveTime,
                       pso.salescategoryid,
                       pso.salescategoryname,
                       dsi.businesscode,
                       dsi.businessname
                  from sddcs.PartsSalesOrder pso   ---------------配件销售订单
                   left join (select s.businesscode, s.businessname, a.id dealerid,null MarketingDepartmentId,s.partssalescategoryid
  from sddcs.SalesUnit s
 inner join sddcs.Agency a
    on a.id = s.OwnerCompanyId
union all
select businesscode, businessname, dealerid,MarketingDepartmentId,partssalescategoryid from sddcs.dealerserviceinfo
 inner join sddcs.company
               on company.id=dealerserviceinfo.dealerid
               where company.type=2 and dealerserviceinfo.status<>99) dsi
on pso.SubmitCompanyId = dsi.DealerId and pso.SalesCategoryId = dsi.partssalescategoryid
                   left join sddcs.marketingdepartment mdt on dsi.MarketingDepartmentId = mdt.id
                 inner join sddcs.partssalesorderdetail psod  ----------------------配件销售订单清单
                    on pso.id = psod.partssalesorderid
                 inner join sddcs.sparepart sp
                        on psod.sparepartid=sp.id
                /* inner join sddcs.PartsBranch pb
                    on pb.partid = psod.sparepartid and pb.Partssalescategoryid in
                       (select id from sddcs.partssalescategory )*/
                 inner join sddcs.warehouse w on w.id = pso.warehouseid where pso.status in (2,4) and pso.SalesUnitOwnerCompanycode in (select code from sddcs.branch) and OrderedQuantity - nvl(ApproveQuantity, 0)<>0
                 group by pso.branchid,
                          pso.BranchCode,
                          pso.warehouseid,
                          pso.code ,
                          w.code,
                          w.name,
                          pso.Submitcompanycode,
                          pso.submitcompanyname,
                          psod.sparepartid,
                          psod.sparepartcode,
                          psod.sparepartname,
                          pso.Partssalesordertypename,
                          pso.SubmitTime,
                          pso.ApproveTime,
                          pso.salescategoryid,
                          pso.salescategoryname,
                          mdt.name,
                          sp.ReferenceCode,
                          dsi.businesscode,
                          dsi.businessname) psos
          left join (select p.partid,
                            p.warehouseid,
                            st.partssalescategoryid,
                           sum(p.quantity) as quantity,
                           sum(p.quantity - nvl(plsk.lockedquantity, 0))as  AvailableStock
                      from (select a.warehouseid,a.partid,sum(a.quantity)as quantity from sddcs.partsstock a where  WarehouseAreaCategoryId<>2 and storagecompanyid in (select id from sddcs.branch) group by a.warehouseid,a.partid
                      ) p
                     inner join sddcs.warehouse ws on ws.id = p.warehouseid --and ws.Iscentralizedpurchase = 1
                      left join sddcs.PartsLockedStock plsk on ws.id = plsk.warehouseid and p.partid = plsk.partid
                     inner join sddcs.SalesUnitAffiWarehouse sw
                     on sw.warehouseid=ws.id
                     inner join sddcs.salesunit st
                     on st.id=sw.salesunitid
                     group by p.partid,p.warehouseid, st.partssalescategoryid) ps
            on psos.sparepartid = ps.partid
            and psos.warehouseid=ps.warehouseid
            and psos.salescategoryid=ps.partssalescategoryid/*
              where ps.quantity-psos.SumOrderAmount<0*/
         group by psos.branchid,
                  psos.branchcode,
                  psos.sparepartid,
                  psos.SaleOrderCode,
                  psos.sparepartcode,
                  psos.sparepartname,
                  psos.Submitcompanycode,
                  psos.submitcompanyname,
                  psos.warehouseid,
                  psos.WarehouseCode,
                  psos.WarehouseName,
                  psos.ReferenceCode,
                  psos.PartsSalesOrderTypeName,
                  psos.SubmitTime,
                  psos.ApproveTime,
                  psos.mdtname,
                  psos.salescategoryid,
                  psos.salescategoryname,
                  psos.businesscode,
                  psos.businessname
        ) ss  --------ss---------------------------------------------------------------------
  left join   (select dd.quantity,pes.partid,dd.partssalescategoryid from (select sum(nvl(psks.quantity, 0)) - sum(nvl(plsk.lockedquantity, 0)) as quantity,pe.exchangecode,psks.partssalescategoryid
               from (select sum(nvl(quantity, 0)) quantity,
                       partsstock.warehouseid,
                       partsstock.StorageCompanyType,
                       partsstock.partid,
                       st.partssalescategoryid
                  from sddcs.partsstock
                   inner join sddcs.SalesUnitAffiWarehouse sw
                     on sw.warehouseid=partsstock.warehouseid
                     inner join sddcs.salesunit st
                     on st.id=sw.salesunitid
                  where  WarehouseAreaCategoryId<>2 and storagecompanyid in (select id from sddcs.branch)
                 group by partsstock.warehouseid, partsstock.StorageCompanyType, partsstock.partid, st.partssalescategoryid)  psks ------------------------配件库存
               inner join sddcs.PartsExchange pe -----------------配件互换信息
                     on pe.partid = psks.partid
               inner join sddcs.warehouse ws on ws.id = psks.warehouseid and ws.Storagecompanytype = 1 /*and ws.iscentralizedpurchase=1*/
               left join sddcs.PartsLockedStock plsk on ws.id = plsk.warehouseid and psks.partid = plsk.partid
               where psks.StorageCompanyType = 1
               group by pe.exchangecode,psks.partssalescategoryid) dd inner join sddcs.PartsExchange pes on pes.exchangecode = dd.exchangecode
               and pes.status = 1)ExchangeStocks -----ExchangeStocks互换件可用
              on ss.partid = ExchangeStocks.partid
              and ExchangeStocks.partssalescategoryid=ss.salescategoryid
  left join (select sum(nvl(ppod.ConfirmedAmount, 0) - nvl(pbcb.InspectedQuantity, 0)) as amount,ppod.sparepartid
              from sddcs.PartsPurchaseOrder ppo  ------------------配件采购订单
            inner join sddcs.PartsPurchaseOrderType ppot on ppo.PartsPurchaseOrderTypeId = ppot.id
              inner join sddcs.PartsPurchaseOrderDetail ppod   ---------------------配件采购订单清单
                    on ppo.id = ppod.partspurchaseorderid
               left join(select pb.originalrequirementbillcode,pc.sparepartid,sum(pc.inspectedquantity) as InspectedQuantity
                    from sddcs.partsinboundcheckbill pb ----------------------配件入库检验单
                    inner join sddcs.partsinboundcheckbilldetail pc ---------------------配件入库检验单清单
                    on pc.partsinboundcheckbillid = pb.id
                    where pb.inboundtype = 1
                    group by pb.originalrequirementbillcode, pc.sparepartid) pbcb
              on pbcb.originalrequirementbillcode = ppo.code and pbcb.sparepartid = ppod.sparepartid
              where ppo.status in (3, 4, 5,6)
              group by ppod.sparepartid) ExchangeSupply ----------------ExchangeSupply互换件采购在途数量-------------------------------
    on ExchangeStocks.partid = ExchangeSupply.sparepartid
 group by ss.BranchId,
          ss.BranchCode,
          ss.partid,
          ss.SaleOrderCode,
          ss.SpareCode,
          ss.SpareName,
          ss.Submitcompanycode,
          ss.submitcompanyname,
          ss.warehouseid,
          ss.WarehouseCode,
          ss.WarehouseName,
          ss.quantity,
          ss.AvailableStock,
          ss.TransferStock,
          ss.SupplyStock,
          ss.SumOrderAmount,
          ss.ReplaceStock,
          ss.ReplaceSupplyStock,
          ss.mdtname,
          ss.ReferenceCode,
          ss.PartsSalesOrderTypeName,
          ss.SubmitTime,
          ss.ApproveTime,
          ss.salescategoryid,
          ss.salescategoryname,
          ss.businesscode,
          ss.businessname;
