create or replace view qtsdataqueryforqxc as
select a.Cscode,
         a.Zcbmcode,
         a.Vincode,
         b.Assembly_date,
         a.Bind_date,
         b.Factory,
         b.Mapid,
         b.Patch,
         b.Produceline,
         b.Mapname,
         a.submittime
    from qts.o_vinbind @sddgn_145 a
    left join qts.o_tmepassembly_cs @sddgn_145 b on a.cscode = b.cscode;
