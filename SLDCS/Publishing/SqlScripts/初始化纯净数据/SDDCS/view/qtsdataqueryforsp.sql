create or replace view qtsdataqueryforsp as
select a.Cscode,
         a.Zcbmcode,
         a.Vincode,
         b.Assembly_date,
         a.Bind_date,
         b.Factory,
         b.Mapid,
         b.Patch,
         b.Produceline,
         b.Mapname,
         b.submittime
    from qts.o_vinbind @qtsorcl_10 a
    left join qts.o_tmepassembly_cs @qtsorcl_10 b on a.cscode = b.cscode;
