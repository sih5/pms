create or replace view servprodlineaffiproduct as
select ProductId, ServiceProductLineId, ProductLinetype
  from (select Product.id as ProductId,
               case
                 when ServProdLineProductDetail.Serviceproductlineid is null then
                  ServiceProdLineProduct.Serviceproductlineid
                 else
                  ServProdLineProductDetail.Serviceproductlineid
               end as ServiceProductLineId,
               1 as ProductLinetype
          from Product
          left join (select *
                      from ServiceProdLineProduct
                     where exists
                     (select 1
                              from serviceproductlineview b
                             where b.ProductLineId =
                                   ServiceProdLineProduct.Serviceproductlineid)) ServiceProdLineProduct
            on Product.Brandid = ServiceProdLineProduct.Brandid
          left join (select *
                      from ServProdLineProductDetail
                     where exists
                     (select 1
                              from serviceproductlineview b
                             where b.ProductLineId =
                                   ServProdLineProductDetail.Serviceproductlineid)) ServProdLineProductDetail
            on Product.Id = ServProdLineProductDetail.Productid
        union
        select Product.id                                 as ProductId,
               EngineModelProductLine.Engineproductlineid as ServiceProductLineId,
               2                                          as ProductLinetype
          from EngineModelProductLine
         inner join EngineModel
            on EngineModelProductLine.Enginemodeid = EngineModel.Id
         inner join Product
            on Product.EngineTypeCode = EngineModel.Enginemodel
         where exists
         (select 1
                  from serviceproductlineview b
                 where b.ProductLineId =
                       EngineModelProductLine.Engineproductlineid))
 where ProductId is not null
   and ServiceProductLineId is not null;
