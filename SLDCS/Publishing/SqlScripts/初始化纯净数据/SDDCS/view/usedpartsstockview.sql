create or replace view usedpartsstockview as
Select a.Id,
       u.Id As Warehouseid,
       u.Code,
       u.Name,
       a.Usedpartsbarcode,
       Usedpartscode,
       Usedpartsname,
       s.Code As Areacode,
       Storagequantity,
       Lockedquantity,
       Settlementprice,
       Claimbillcode,
       Iffaultyparts,
       Faultypartscode,
       Usedpartssuppliercode,
       Faultypartssuppliercode,
       Responsibleunitcode,
       a.Creatorname,
       a.Createtime,
       Usedpartsid,
       Usedpartsbatchnumber,
       Usedpartsserialnumber,
       Claimbilltype,
       Claimbillid,
       Faultypartsid,
       Faultypartsname,
       Usedpartssupplierid,
       Usedpartssuppliername,
       Faultypartssupplierid,
       Faultypartssuppliername,
       Responsibleunitid,
       Responsibleunitname,
       a.Branchid,
       Cast(Case
              When Usedpt.Usedpartsbarcode Is Not Null Then
               1
              Else
               0
            End As Number(1)) As Istransfers
  From Usedpartsstock a
  Left Join (Select Uptd.Usedpartsbarcode,upt.originwarehouseid
               From Usedpartstransferdetail Uptd
              Inner Join Usedpartstransferorder Upt
                 On Uptd.Usedpartstransferorderid = Upt.Id
                And Upt.Status =1) Usedpt
    On a.Usedpartsbarcode = Usedpt.Usedpartsbarcode  and Usedpt.originwarehouseid=a.usedpartswarehouseid
 Inner Join Usedpartswarehouse u
    On u.Id = a.Usedpartswarehouseid
 Inner Join Usedpartswarehousearea s
    On s.Id = a.Usedpartswarehouseareaid;
