create or replace view warehouseareawithpartsstock as
select WarehouseArea.Id as  WarehouseAreaId,
       WarehouseArea.WarehouseId,
       WarehouseArea.ParentId as WarehouseAreaParentId,
       WarehouseArea.Code as WarehouseAreaCode,
       WarehouseArea.TopLevelWarehouseAreaId,
       WarehouseArea.AreaCategoryId,
       WarehouseArea.Status,
       WarehouseArea.Remark,
       WarehouseArea.AreaKind,
       WarehouseAreaCategory.Category as AreaCategory,
       Warehouse.Code as WarehouseCode,
       Warehouse.Name as WarehouseName,
       Warehouse.Type as WarehouseType,
       Warehouse.BranchId,
       Warehouse.StorageCompanyId,
       Warehouse.StorageCompanyType,
       nvl(PartsStock.PartId,0) as SparePartId,
       PartsStock.Quantity,
       SparePart.Code as SparePartCode,
       SparePart.Name as SparePartName
  from WarehouseArea
 inner join Warehouse on WarehouseArea.WarehouseId = Warehouse.Id
  left join WarehouseAreaCategory on WarehouseArea.AreaCategoryId = WarehouseAreaCategory.Id
  left join PartsStock on WarehouseArea.Id = PartsStock.WarehouseAreaId
  left join SparePart on PartsStock.PartId = SparePart.Id;
