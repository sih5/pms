-- 为 xxDcs 数据库的 Personnel 创建同义词

create or replace synonym DcsPersonnel for yxDcs.Personnel;

TRUNCATE TABLE PasswordPolicy
/

INSERT INTO PasswordPolicy (Id ,ValidPeriod ,RemindDays ,DefaultPassword ,IsEnabled) VALUES
(S_PasswordPolicy.Nextval, 60, 15, '1B2C99FF9656053462426C45B7FCFF504056F1DE',1);
/

-- 人员信息同步到Dcs
create or replace trigger Personnel_Trg
   after insert or update on Personnel
   for each row
declare
begin
  if :new.Id is not null and :old.Id is null then
    insert into DcsPersonnel(Id, LoginId, Name, Status, CellNumber, Remark, CorporationId, CorporationName, CreatorId, CreatorName, CreateTime)
    values(:new.Id, :new.LoginId, :new.Name,
           case :new.Status when 1 then 1 when 2 then 2 when 0 then 99 end,
           :new.CellNumber, :new.Remark, :new.EnterpriseId,
           (select Name from Enterprise where Enterprise.Id = :new.EnterpriseId),
           :new.CreatorId, :new.CreatorName, :new.CreateTime);

    insert into UserLoginInfo (id,LoginId,Enterprisecode,IsFisrt,ErrorPassNum,BeginLockTime,LockLenght)
    select
    s_UserLoginInfo .Nextval,
    :new.LoginId,
    Enterprise.Code,
    1,---是否首次登录=是
    0,--- 输错密码次数
    null,----锁定开始时间
    15--- 锁定时长(分钟)
    from  Enterprise where Enterprise .id=:new.EnterpriseId;

  else
    update DcsPersonnel set
      LoginId = :new.LoginId,
      Name = :new.Name,
      Status = case :new.Status when 1 then 1 when 2 then 2 when 0 then 99 end,
      CellNumber = :new.CellNumber,
      Remark = :new.Remark,
      CorporationId = :new.EnterpriseId,
      CorporationName = (select Name from Enterprise where Enterprise.Id = :new.EnterpriseId),
      ModifierId = :new.ModifierId,
      ModifierName = :new.ModifierName,
      ModifyTime = :new.ModifyTime
    where DcsPersonnel.Id = :new.Id;
  end if;
end Personnel_Trg;
/
