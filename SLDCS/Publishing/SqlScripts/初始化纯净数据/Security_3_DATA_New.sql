TRUNCATE TABLE Page

--权限

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1, 0, 0, 'Security', NULL, NULL, '权限管理', NULL, 'Client/Images/ShellView/security-s.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2, 1, 1, 'Security', NULL, NULL, '权限管理', NULL, NULL, 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (101, 2, 2, 'EntNodeTemplate', 'Released', NULL, '企业授权模板管理', '维护企业权限范围模板，用于绑定到不同的企业', 'Client/Security/Images/Menu/EntNodeTemplate.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (102, 2, 2, 'EntOrganizationTpl', 'Released', NULL, '企业组织模板管理', '维护组织模板，用于快速复制给不同的企业', 'Client/Security/Images/Menu/EntOrganizationTpl.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (103, 2, 2, 'RoleTemplate', 'Released', NULL, '角色模板管理', '维护角色模板，用于快速复制给不同的企业', 'Client/Security/Images/Menu/RoleTemplate.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (104, 2, 2, 'Enterprise', 'Released', NULL, '企业管理', '维护企业的基本信息、绑定权限模板、初始化组织及角色', 'Client/Security/Images/Menu/Enterprise.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (105, 2, 2, 'Organization', 'Released', NULL, '组织管理', '对企业的组织结构进行查询与编辑', 'Client/Security/Images/Menu/Organization.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (106, 2, 2, 'Personnel', 'Released', NULL, '人员管理', '对企业人员数据进行查询与编辑', 'Client/Security/Images/Menu/Personnel.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (107, 2, 2, 'Role', 'Released', NULL, '角色管理', '对用户所担任的角色进行查询与编辑', 'Client/Security/Images/Menu/Role.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (108, 2, 2, 'OrganizationGroup', 'Released', NULL, '组织管理(集中授权)', '对所有企业的组织结构进行查询与编辑', 'Client/Security/Images/Menu/Organization.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (109, 2, 2, 'PersonnelGroup', 'Released', NULL, '人员管理(集中授权)', '对所有企业人员数据进行查询与编辑', 'Client/Security/Images/Menu/Personnel.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (110, 2, 2, 'RoleGroup', 'Released', NULL, '角色管理(集中授权)', '对所有企业的用户所担任的角色进行查询与编辑', 'Client/Security/Images/Menu/Role.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4, 1, 1, 'Log', NULL, NULL, '系统日志', NULL, NULL, 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (401, 4, 2, 'Log', 'DataGrid', 'Sunlight.Silverlight.Log.LogInfoViewModel, Sunlight.Silverlight.Log, Version=1.0.0.0, Culture=neutral, PublicKeyToken=6fc94565b181c045', '系统日志查看', '查看系统服务端调度情况','Client/Security/Images/Menu/Log.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (402, 4, 2, 'RuleRecord', 'Released', null,null, '授权履历查询', '','Client/Security/Images/Menu/RuleRecord.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (403, 4, 2, 'LoginLog', 'Released', null,null, '登陆日志', '','Client/Security/Images/Menu/LoginLog.png', 2, 2);
 
--公共业务
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5314, 5300, 2, 'PackingTask', 'Released', NULL, '包装管理', '根据包装任务单包装登记', 'Client/DCS/Images/Menu/PartsStocking/PartsPacking.png', 11, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5316, 5300, 2, 'PartsShelvesTask', 'Released', NULL, '上架管理', '配件公司根据上架任务办理上架', 'Client/DCS/Images/Menu/PartsStocking/PartsShelves.png', 12, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5315, 5300, 2,'ReceiptManagement', 'Released', NULL, '收货管理', '配件公司检索入库计划办理配件收货','Client/Dcs/Images/Menu/PartsStocking/PartsInboundCheckBill.png', 13, 2);
--结算管理

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1000, 0, 0, 'Common', NULL, NULL, '公共数据管理', NULL, 'Client/Images/ShellView/common-s.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1100, 1000,1, 'Company', NULL, NULL, '企业信息管理', NULL, NULL, 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1101,1100,2, 'Agency', 'Released', NULL, '代理商/代理库管理', '维护代理商/代理库信息', 'Client/DCS/Images/Menu/Common/Agency.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1102,1100, 2, 'PartsSupplier', 'Released', NULL, '配件供应商管理', '查询和维护配件供应商信息', 'Client/DCS/Images/Menu/Common/PartsSupplier.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1103,1100, 2, 'TemporarySupplier', 'Released', NULL, '临时供应商管理', '查询、维护临时供应商信息', 'Client/DCS/Images/Menu/Common/TemporarySupplier.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1104,1100, 2, 'ResponsibleUnit', 'Released', NULL, '生产工厂管理', '维护生产工厂信息', 'Client/DCS/Images/Menu/Common/ResponsibleUnit.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1105,1100, 2, 'LogisticCompany', 'Released', NULL, '物流公司管理', '维护物流公司信息', 'Client/DCS/Images/Menu/Common/LogisticCompany.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1106,1100, 2, 'CompanyInvoiceInfo', 'Released', NULL, '企业开票信息管理', '维护增值税发票开票信息', 'Client/DCS/Images/Menu/Common/CompanyInvoiceInfo.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1107,1100, 2, 'CompanyLoginPicture', 'Released', NULL, '企业登录图片管理', '维护企业登录图片', 'Client/DCS/Images/Menu/Common/CompanyLoginPicture.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1108,1100, 2, 'ExpressToLogistics', 'Released', NULL, '快递物流关系管理', '查询、维护快递物流关系', 'Client/DCS/Images/Menu/Common/LogisticCompany.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1109,1100, 2, 'Express', 'Released', NULL, '快递公司管理', '查询、维护快递公司', 'Client/DCS/Images/Menu/Channels/DealerServiceInfoForBranchHistory.png', 8, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1200,1000, 1, 'EnterpriseAndInternalOrganization', 'Released', NULL, '营销公司信息管理', NULL, NULL,1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1201,1200, 2, 'Branch', 'Released', NULL, '营销分公司管理', '维护分公司信息', 'Client/DCS/Images/Menu/Common/Branch.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1202,1200, 2, 'PersonnelBrand', 'Released', NULL, '人员与品牌关系', '维护人员与品牌的关系', 'Client/DCS/Images/Menu/Common/PersonnelBrand.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1203,1200, 2, 'MarketingDepartment', 'Released', NULL, '市场部管理', '维护市场部信息', 'Client/DCS/Images/Menu/Common/MarketingDepartment.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1204,1200, 2, 'PersonnelMarketing', 'Released', NULL, '市场部与人员关系', '分公司维护市场部与人员的关系', 'Client/DCS/Images/Menu/Common/PersonnelMarketing.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1205,1200, 2, 'DepartmentInformation', 'Released', NULL, '部门管理','查询、维护内部部门信息', 'Client/DCS/Images/Menu/PartsPurchasing/DepartmentInformation.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1206,1200, 2, 'SalesRegion', 'Released', NULL, '大区管理', '维护大区信息', 'Client/DCS/Images/Menu/Common/MarketingDepartmentForCAM.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1207,1200, 2, 'PersonnelSaleRegion', 'Released', NULL, '大区与人员关系', '分公司维护大区与人员的关系', 'Client/DCS/Images/Menu/Common/PersonnelSaleRegion.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1208,1200, 2, 'SaleRegionMarketing', 'Released', NULL, '大区与市场部关系', '分公司维护大区与市场部的关系', 'Client/DCS/Images/Menu/Common/SaleRegionMarketing.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1209,1200, 2, 'CompanyAddress', 'Released', NULL, '企业收货地址管理', '查询、维护本企业收货地址', 'Client/DCS/Images/Menu/Common/CompanyAddress.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1210,1200, 2, 'CompanyAddressForBranch', 'Released', NULL, '收货地址管理-分公司', '分公司维护服务站收货地址', 'Client/DCS/Images/Menu/Common/CompanyAddressForBranch.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (16402,1200, 2, 'PersonnelSupplierRelation', 'Released', NULL, '人员与供应商关系维护', '维护供应商与人员的关系', 'Client/DCS/Images/Menu/Common/PersonnelSaleRegion.png', 9, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1300,1000, 1, 'Dealer', NULL, NULL, '服务站信息管理', NULL, NULL, 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1301,1300, 2, 'Dealer', 'Released', NULL, '服务站/专卖店企业信息管理', '查询和维护服务站/专卖店基本信息', 'Client/DCS/Images/Menu/Channels/Dealer.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1302,1300, 2, 'DealerServiceInfoForBranch', 'Released', NULL, '服务站/专卖店分品牌信息管理', '查询和维护服务站/专卖店分品牌信息', 'Client/DCS/Images/Menu/Channels/DealerServiceInfoForBranch.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1303,1300, 2, 'DealerServiceInfoForDealer', 'Released', NULL, '服务站分品牌信息管理-服务站', '查询和维护服务站分品牌管理信息维护（服务站)', 'Client/DCS/Images/Menu/Channels/DealerServiceInfoForDealer.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1304,1300, 2, 'DealerAddress', 'Released', NULL, '服务站配件发运地址维护-服务站', '查询和维护服务站地址维护（服务站）', 'Client/DCS/Images/Menu/Channels/DealerAddress.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1305,1300, 2, 'DealerKeyPosition', 'Released', NULL, '服务站/专卖店关键岗位管理', '查询和维护服务站/专卖店关键岗位', 'Client/DCS/Images/Menu/Channels/DealerKeyPosition.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1306,1300, 2, 'DealerKeyEmployeeQuery', 'Released', NULL, '服务站/专卖店关键岗位人员查询', '查询和维护服务站/专卖店关键岗位人员信息', 'Client/DCS/Images/Menu/Channels/DealerKeyEmployeeQuery.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1307,1300, 2, 'DealerBusinessPermitQuery', 'Released', NULL, '服务站经营权限查询', '集团或者分公司查询服务站经营权限', 'Client/DCS/Images/Menu/Channels/DealerBusinessPermitQuery.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1308,1300, 2, 'Subdealer', 'Released', NULL, '二级站管理', '维护二级站信息', 'Client/DCS/Images/Menu/Channels/DealerKeyEmployeeQuery.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1309,1300, 2, 'PersonSubDealer', 'Released', NULL, '二级站人员管理', '维护二级站人员关系', 'Client/DCS/Images/Menu/Channels/PersonSubDealer.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1310,1300, 2, 'DealerAgency', 'Released', NULL, '服务站隶属代理库管理', '查询和维护服务站隶属代理库关系', 'Client/DCS/Images/Menu/Channels/DealerAgency.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1311,1300, 2, 'ChannelCapability', 'Released', NULL, '网络业务能力管理', '新增、修改渠道业务能力', 'Client/DCS/Images/Menu/Channels/ChannelCapability.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1312,1300, 2, 'DealerBusinesspermithistoryQuery', 'Released', NULL, '服务站经营权限履历查询', '查询服务站经营权限变更履历', 'Client/DCS/Images/Menu/Channels/DealerBusinessPermitQuery.png', 11, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1313,1300, 2, 'DealerServiceInfoForBranchHistoryQuery', 'Released', NULL, '服务站/专卖店分品牌信息履历查询', '查询服务站/专卖店分公司信息变更履历', 'Client/DCS/Images/Menu/Channels/DealerServiceInfoForBranchHistory.png', 12, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1314,1300, 2, 'SubdealerForBranch', 'Released', NULL, '二级站管理-分公司', '维护二级站信息', 'Client/DCS/Images/Menu/Channels/DealerKeyEmployeeQuery.png', 13, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1315,1300, 2, 'DealerForDealer', 'Released', NULL, '服务站/专卖店企业信息管理-服务站', '', 'Client/DCS/Images/Menu/Channels/Dealer.png', 14, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1316,1300, 2, 'DealerHistoryQuery', 'Released', NULL, '服务站\专卖店企业信息履历查询', '查询服务站\专卖店企业信息履历查询', 'Client/DCS/Images/Menu/Channels/DealerServiceInfoForBranchHistory.png', 15, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1317,1300, 2, 'AuthenticationType', 'Released', NULL, '服务培训认证类型管理', '服务培训认证类型', 'Client/DCS/Images/Menu/Common/CompanyInvoiceInfo.png', 16, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1318,1300, 2, 'TrainingType', 'Released', NULL, '服务培训类型与课程管理', '服务培训类型与课程', 'Client/DCS/Images/Menu/Common/Branch.png', 17, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1319,1300, 2, 'DealerPerTrainAut', 'Released', NULL, '服务站人员培训认证信息维护(本部 事业本部)', '分公司查询及维护服务站人员培训及认证', 'Client/DCS/Images/Menu/Channels/DealerServiceInfoForBranchHistory.png', 18, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1320,1300, 2, 'DealerPerTrainAutForDealer', 'Released', NULL, '服务站人员培训及认证查询（服务站）', '查询服务站人员培训及认证', 'Client/DCS/Images/Menu/Channels/DealerServiceInfoForBranch.png', 19, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1321,1300, 2, 'BrandGradeMessage', 'Released', NULL, '星级信息管理', '', 'Client/DCS/Images/Menu/ServiceBasicData/GradeCoefficient.png', 20, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1322,1300, 2, 'DealerGradeInfo', 'Released', NULL, '服务站/专卖店分级管理', '', 'Client/DCS/Images/Menu/Channels/DealerServiceInfoForBranch.png', 21, 2);
 

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1400,1000, 1, 'DownLoad', NULL, NULL, '下载管理', NULL, NULL, 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1401,1400, 2, 'SchedulerDownLoad', 'Released', NULL, '定时下载', '下载定时导出文件', 'Client/DCS/Images/Menu/Common/SchedulerDownLoad.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1402,1400, 2, 'ReportDownloadMsg', 'Released', NULL, '报表下载管理', '下载报表文件', 'Client/Dcs/Images/Menu/Common/DownLoad.png', 2, 2);

--配件基础数据管理

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2000,0, 0, 'PartsBaseInfo', NULL, NULL, '配件基础数据管理', NULL, 'Client/Images/ShellView/PartsBaseInfo-s.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2100,2000, 1, 'SparePart', NULL, NULL, '配件信息', NULL, NULL, 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2101,2100, 2, 'SparePart', 'Released', NULL, '配件信息管理', '查询和维护配件信息', 'Client/DCS/Images/Menu/Common/SparePart.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2102,2100, 2, 'PartsBranch', 'Released', NULL, '配件分品牌商务信息管理', '查询和维护配件营销信息', 'Client/DCS/Images/Menu/Common/PartsBranch.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2103,2100, 2, 'PartsReplacement', 'Released', NULL, '配件替换件管理', '查询和维护配件的替换信息', 'Client/DCS/Images/Menu/Common/PartsReplacement.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2104,2100, 2, 'ABCStrategy', 'Released', NULL, '配件ABC分类策略管理', '查询和维护配件ABC分类策略', 'Client/DCS/Images/Menu/Common/ABCStrategy.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2105,2100, 2, 'PartsExchange', 'Released', NULL, '配件互换查询表', '用于配件互换关系的查询和导出', 'Client/DCS/Images/Menu/Common/PartsExchange.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2106,2100, 2, 'PartsBranchQuery', 'Released', NULL, '配件分品牌商务信息查询', '查询配件品牌营销信息', 'Client/DCS/Images/Menu/Common/PartsBranchQuery.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2107,2100, 2, 'SparePartQuery', 'Released', NULL, '配件信息查询', '查询配件信息', 'Client/DCS/Images/Menu/Common/SparePart.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2108,2100, 2, 'PartDeleaveInformation', 'Released', NULL, '配件拆散信息管理', '查询和维护配件的拆散信息', 'Client/DCS/Images/Menu/Common/PartsWarrantyStandard.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2109,2100, 2, 'PartsPriceHistoryQuery', 'Released', NULL, '配件价格查询', '查看配件价格', 'Client/DCS/Images/Menu/Common/PartsPriceHistoryQuery.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2110,2100, 2, 'SEPSparePartInfo', 'Released', NULL, 'EPC接口日志', '查看EPC接口日志', 'Client/DCS/Images/Menu/Common/PartsPriceHistoryQuery.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2111,2100, 2, 'GoldenTaxClassifyMsg', 'Released', NULL, '配件金税分类信息管理', '查看配件金税分类信息', 'Client/DCS/Images/Menu/Common/SparePart.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2112,2100, 2, 'PartsPriceIncreaseRate', 'Released', NULL, '配件加价策略管理', '查看和维护配件加价策略信息', 'Client/DCS/Images/Menu/Common/SparePart.png', 11, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2113,2100, 2, 'ProductExecutiveStandard', 'Released', NULL, '产品执行标准管理', '查询和维护产品执行标准信息', 'Client/DCS/Images/Menu/Common/SparePart.png', 12, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2114,2100, 2, 'PartsExchangeGroup', 'Released', NULL, '配件互换管理表', '用于配件互换关系的维护', 'Client/DCS/Images/Menu/Common/SparePart.png', 14, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2200,2000, 1, 'SparePartPurchase', NULL, NULL, '配件采购基础数据', NULL, NULL, 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2201,2200, 2, 'BranchSupplierRelation', 'Released', NULL, '分公司与供应商关系管理', '查询和维护分公司与供应商关系', 'Client/DCS/Images/Menu/Common/BranchSupplierRelation.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2202,2200, 2, 'PartsSupplierRelation', 'Released', NULL, '配件与供应商关系管理', '查询和维护配件与供应商的关系', 'Client/DCS/Images/Menu/Common/PartsSupplierRelation.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2203,2200, 2, 'PartsPurchasePricing', 'Released', NULL, '配件采购价格管理', '查询和维护配件采购价格', 'Client/DCS/Images/Menu/Common/PartsPurchasePricing.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2204,2200, 2, 'PartsPurchasePricingChange', 'Released', NULL, '配件采购价格变更申请管理', '查询和维护配件采购价格变更申请单', 'Client/DCS/Images/Menu/Common/PartsPurchasePricingChange.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2205,2200, 2, 'PartsPurchasePricingChangeQuery', 'Released', NULL, '配件采购价变更履历查询', '查询配件采购价变更履历', 'Client/DCS/Images/Menu/Common/PartsPurchasePricingChangeQuery.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2206,2200, 2, 'PartsPurchaseOrderType', 'Released', NULL, '配件采购订单类型管理', '查询和维护配件采购订单类型', 'Client/DCS/Images/Menu/PartsPurchasing/PartsPurchaseOrderType.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2207,2200, 2, 'SupplierSparePartQuery', 'Released', NULL, '供应配件信息查询-供应商', '查询供应商配件信息', 'Client/DCS/Images/Menu/Common/PartsSupplyQueryForSupplier.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2208,2200, 2, 'IMSPurchaseLoginInfoQuery', 'Released', NULL, 'IMS接口日志查询-采购订单', '查询IMS接口日志采购订单信息', 'Client/DCS/Images/Menu/Common/IMSPurchaseLoginInfo.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2209,2200, 2, 'IMSShippingLoginInfoQuery', 'Released', NULL, 'IMS接口日志查询-发运单', '查询IMS接口日志发运单信息', 'Client/DCS/Images/Menu/Common/IMSPurchaseLoginInfo.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2210,2200, 2, 'PartsPurchasePricingChangeYX', 'Released', NULL, '配件采购价格变更申请管理（营销）', '查询和维护配件采购价格变更申请单', 'Client/DCS/Images/Menu/Common/PartsPurchasePricingChange.png', 9, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2300,2000, 1, 'PartsSalesPrice', NULL, NULL, '配件销售基础数据', NULL, NULL, 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2301,2300, 2, 'CustomerInformation', 'Released', NULL, '配件客户信息', '查询或者维护往来客户信息', 'Client/DCS/Images/Menu/PartsSales/CustomerInformation.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2302,2300, 2, 'PartsSalesCategory', 'Released', NULL, '分公司品牌管理', '查询、维护分公司品牌信息', 'Client/DCS/Images/Menu/PartsSales/PartsRetailGuidePrice.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2303,2300, 2, 'CustomerOrderPriceGrade', 'Released', NULL, '客户企业订单价格等级管理', '查询、维护服务站配件销售订单的价格等级', 'Client/DCS/Images/Menu/PartsSales/CustomerOrderPriceGrade.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2304,2300, 2, 'PartsSalesPrice', 'Released', NULL, '配件销售价管理', '查询配件的销售价格', 'Client/DCS/Images/Menu/PartsSales/PartsSalesPrice.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2305,2300, 2, 'PartsSalesPriceChange', 'Released', NULL, '配件销售价变更申请管理', '查询、维护配件价格变更申请', 'Client/DCS/Images/Menu/PartsSales/PartsSalesPriceChange.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2324,2300, 2, 'SpecialTreatyPriceChange', 'Released', NULL, '配件特殊协议价申请管理', '查询、维护配件特殊协议价变更申请', 'Client/DCS/Images/Menu/PartsSales/PartsSpecialTreatyPrice.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2306,2300, 2, 'PartsSpecialTreatyPrice', 'Released', NULL, '配件特殊协议价管理', '查询、维护配件的协议价', 'Client/DCS/Images/Menu/PartsSales/PartsSpecialTreatyPrice.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2307,2300, 2, 'PartsRetailGuidePrice', 'Released', NULL, '配件零售指导价管理', '查询配件的零售指导价', 'Client/DCS/Images/Menu/PartsSales/PartsRetailGuidePrice.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2308,2300, 2, 'SalesUnit', 'Released', NULL, '配件销售组织管理', '维护营销分公司或者代理库的销售组织', 'Client/DCS/Images/Menu/PartsSales/SalesUnit.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2309,2300, 2, 'PartsSalesOrderType', 'Released', NULL, '分公司销售订单类型管理', '查询、维护分公司销售订单类型', 'Client/DCS/Images/Menu/PartsSales/PartsSalesOrderType.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2310,2300, 2, 'DealerPartsRetailGuidePriceSelect', 'Released', NULL, '配件零售指导价查询-服务站', '服务站查询配件的零售指导价', 'Client/DCS/Images/Menu/PartsSales/PartsRetailGuidePrice.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2311,2300, 2, 'AgencyPartsSalesPriceQuery', 'Released', NULL, '配件销售价查询-代理库', '代理库查询配件的销售价', 'Client/DCS/Images/Menu/PartsSales/PartsSalesPrice.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2312,2300, 2, 'AgencyPartsRetailGuidePriceSelect', 'Released', NULL, '配件零售指导价查询-代理库', '代理库查询配件的零售指导价', 'Client/DCS/Images/Menu/PartsSales/PartsRetailGuidePrice.png', 11, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2313,2300, 2, 'DealerPartsSalesPriceQuery', 'Released', NULL, '配件销售价查询-服务站', '服务站查询配件的销售价', 'Client/DCS/Images/Menu/PartsSales/PartsSalesPrice.png', 12, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2314,2300, 2, 'DealerPartsSalesPrice', 'Released', NULL, '服务站批发价管理', '查询服务站批发价', 'Client/DCS/Images/Menu/PartsSales/PartsSalesPrice.png', 13, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2315,2300, 2, 'DealerPartsSalesPriceForDQuery', 'Released', NULL, '服务站批发价查询-服务站', '查询服务站批发价', 'Client/DCS/Images/Menu/PartsSales/PartsSalesPrice.png', 14, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2316,2300, 2, 'DealerPartsSalesPriceForAQuery', 'Released', NULL, '服务站批发价查询-代理库', '查询服务站批发价', 'Client/DCS/Images/Menu/PartsSales/PartsSalesPrice.png', 15, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2317,2300, 2, 'PMSEPCBrandMapping', 'Released', NULL, 'EPC品牌映射管理', '查询、维护EPC品牌映射关系', 'Client/DCS/Images/Menu/PartsSales/PMSEPCBrandMapping.png', 16, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2318,2300, 2, 'PMSDMSBrandMapping', 'Released', NULL, 'DMS品牌映射管理', '查询、维护DMS品牌映射关系', 'Client/DCS/Images/Menu/PartsSales/PMSDMSBrandMapping.png', 17, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2319,2300, 2, 'PartsSalePriceIncreaseRate', 'Released', NULL, '配件销售加价率管理', '查询、维护配件销售加价率', 'Client/DCS/Images/Menu/PartsSales/PartsSalePriceIncreaseRate.png', 18, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2320,2300, 2, 'PartsSalesPriceHistory', 'Released', NULL, '配件销售价变更履历查询', '查询配件销售价变更履历查询', 'Client/DCS/Images/Menu/PartsSales/PartsSalePriceIncreaseRate.png', 19, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2321,2300, 2, 'OrderApproveWeekday', 'Released', NULL, '储备订单自动审核日与省份关系', '储备订单自动审核日与省份关系', 'Client/DCS/Images/Menu/PartsStocking/Warehouse.png', 20, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2322,2300, 2, 'WarehouseSequence', 'Released', NULL, '出库仓库优先顺序管理', '出库仓库优先顺序管理', 'Client/DCS/Images/Menu/PartsStocking/Warehouse.png', 21, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2323, 2300, 2, 'PartsSalesPriceChangeApplication', 'Released', NULL, '配件销售价格变更申请管理-营销','查询、维护配件价格变更申请', 'Client/DCS/Images/Menu/PartsPurchasing/PartsPurchaseSettleInvoice.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2325,2300, 2, 'PartsPackingPropertyApp', 'Released', NULL, '配件包装设置', '申请设置配件的最小销售量及包装属性', 'Client/DCS/Images/Menu/PartsSales/PartsSalesOrder.png', 22, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2328,2300, 2, 'PurchasePricingChangeHis', 'Released', NULL, '采购价格变动情况记录查询', '查询采购价格变动情况记录', 'Client/DCS/Images/Menu/PartsSales/PartsSalesReturnBillQuery.png', 23, 2);

--配件采购管理

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3000,0, 0, 'PartsPurchasing', NULL, NULL, '配件采购管理', NULL, 'Client/Images/ShellView/partsPurchasing-s.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3100,3000, 1, 'PartsPurchasing', NULL, NULL, '配件采购业务', NULL, NULL, 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3101,3100, 2, 'PartsPurchaseOrder', 'Released', NULL, '配件采购订单管理', '查询、维护配件采购订单', 'Client/DCS/Images/Menu/PartsPurchasing/PartsPurchaseOrder.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3102,3100, 2, 'SupplierPartsPurchaseOrder', 'Released', NULL, '配件采购订单管理-供应商', '供应商查询、审批配件采购订单', 'Client/DCS/Images/Menu/PartsPurchasing/SupplierPartsPurchaseOrder.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3103,3100, 2, 'PartsPurReturnOrderForBranch', 'Released', NULL, '配件采购退货管理', '查询、维护配件采购退货单', 'Client/DCS/Images/Menu/PartsPurchasing/PartsPurReturnOrderForBranch.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3104,3100, 2, 'PartsPurReturnOrder', 'Released', NULL, '配件采购退货管理-供应商 ', '供应商查询、审批配件采购退货单', 'Client/DCS/Images/Menu/PartsPurchasing/PartsPurReturnOrder.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3105,3100, 2, 'SupplierShippingOrder', 'Released', NULL, '供应商发运管理-供应商', '查询、维护供应商配件发运单', 'Client/DCS/Images/Menu/PartsPurchasing/SupplierShippingOrder.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3106,3100, 2, 'SupplierShippingOrderForBranch', 'Released', NULL, '供应商发运管理', '分公司查询、确认供应商发运单', 'Client/DCS/Images/Menu/PartsPurchasing/SupplierShippingOrderForBranch.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3107,3100, 2, 'SupplierShippingOrderForDirect', 'Released', NULL, '直供发运单确认管理', '服务站或代理库确认直供订单的发运单', 'Client/DCS/Images/Menu/PartsPurchasing/SupplierShippingOrderForDirect.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3108,3100, 2, 'PartsPurchasingOrderDownLoad', 'Released', NULL, '配件采购订单下载', '提供采购订单主清单数据订阅下载', 'Client/DCS/Images/Menu/PartsPurchasing/PartsPurchasingOrderDownLoad.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3109,3100, 2, 'SupplierPartsTagPrintting', 'Released', NULL, '供应商配件标签打印', '供应商打印配件条码', 'Client/DCS/Images/Menu/PartsPurchasing/SupplierPartsTagPrintting.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3110,3100, 2, 'SupplierPlanArrear', 'Released', NULL, '供应商计划欠款管理', '', 'Client/DCS/Images/Menu/PartsPurchasing/SupplierPlanArrear.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3111,3100, 2, 'SupplierPreApprovedLoan', 'Released', NULL, '供应商货款预批管理', '', 'Client/DCS/Images/Menu/PartsPurchasing/SupplierPreApprovedLoan.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3112, 3100, 2, 'PurchaseQuery', 'Released', NULL, '采购需求查询', '采购需求查询', 'Client/DCS/Images/Menu/PartsPurchasing/PurchaseQuery.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (15103,3100, 2, 'PartsPurchasePlanOrder', 'Released', NULL, '采购计划管理', '查询、维护、初审、终审配件采购计划信息', 'Client/DCS/Images/Menu/PartsPurchasing/PartConPurchasePlan.png', 12, 2);



INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3200,3000, 1, 'PartsPurchaseSettle', NULL, NULL, '配件采购结算业务', NULL, NULL, 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3201,3200, 2, 'PartsPurchaseSettleBill', 'Released', NULL, '配件采购结算管理','查询、维护配件采购结算单', 'Client/DCS/Images/Menu/PartsPurchasing/PartsPurchaseSettleBill.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3202,3200, 2, 'PartsPurchaseRtnSettleBill', 'Released', NULL, '配件采购退货结算管理', '查询、维护配件采购退货结算单', 'Client/DCS/Images/Menu/PartsPurchasing/PartsPurchaseRtnSettleBill.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3203,3200, 2, 'PartsPurchaseSettleInvoice', 'Released', NULL, '配件采购结算发票查询','查询配件采购结算发票', 'Client/DCS/Images/Menu/PartsPurchasing/PartsPurchaseSettleInvoice.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3204,3200, 2, 'PartsPurchaseSettleForSupplier', 'Released', NULL, '配件采购结算管理-供应商', '供应商查询、维护配件采购结算单', 'Client/DCS/Images/Menu/PartsPurchasing/PartsPurchaseSettleBill.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3205,3200, 2, 'PartsPurchaseRtnSettleQuery', 'Released', NULL, '配件采购退货结算查询-供应商', '供应商查询配件采购退货结算单', 'Client/DCS/Images/Menu/PartsPurchasing/PartsPurchaseRtnSettleQuery.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3206,3200, 2, 'PartsPurchaseSettleBillCIF','Released',NULL,'配件采购结算管理(福田总部)','查询、维护配件采购结算单','Client/DCS/Images/Menu/PartsPurchasing/PartsPurchaseSettleBill.png', 5,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3300,3000, 1, 'InternalRecipient', NULL, NULL, '内部领用业务', NULL, NULL, 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3301,3300, 2, 'InternalAcquisitionBill', 'Released', NULL, '内部领入管理', '查询、维护配件内部领入单', 'Client/DCS/Images/Menu/PartsPurchasing/InternalAcquisitionBill.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3302,3300, 2, 'InternalAllocationBill', 'Released', NULL, '内部领出管理','查询、维护配件内部领出单', 'Client/DCS/Images/Menu/PartsPurchasing/InternalAllocationBill.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3400,3000, 1, 'InternalSettlement', NULL, NULL, '内部领用结算', NULL, NULL, 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3401,3400, 2, 'PartsRequisitionSettleBill', 'Released', NULL, '内部领入结算管理','查询、新增部门领用结算单', 'Client/DCS/Images/Menu/PartsPurchasing/PartsRequisitionSettleBill.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3402,3400, 2, 'PartsRequisitionSettleBillForOut', 'Released', NULL, '内部领出结算管理','查询、新增部门领用结算单', 'Client/DCS/Images/Menu/PartsPurchasing/PartsRequisitionSettleBill.png', 0, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3500,3000, 1, 'IntelligentOrder', NULL, NULL, '智能订货', NULL, NULL, 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3501,3500, 2, 'ProductLifeCycleCategory', 'Released', NULL, '生命周期分类','查询、新增生命周期分类信息', 'Client/DCS/Images/Menu/PartsPurchasing/ProductLifeCycleCategory.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3502,3500, 2, 'PlanPriceCategory', 'Released', NULL, '计划价格分类','查询、新增计划价格分类信息', 'Client/DCS/Images/Menu/PartsPurchasing/PlanPriceCategory.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3503,3500, 2, 'PartsServiceLevel', 'Released', NULL, '服务水平管理','查询、新增配件服务水平信息', 'Client/DCS/Images/Menu/PartsPurchasing/PartsServiceLevel.png', 2, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3504,3500, 2, 'IntelligentOrderMonthlyBase', 'Released', NULL, '月度基础数据查询',NULL, 'Client/DCS/Images/Menu/PartsPurchasing/IntelligentOrderMonthlyBase.png',3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3505,3500, 2, 'IntelligentOrderMonthly', 'Released', NULL, '月度累计数据查询',NULL, 'Client/DCS/Images/Menu/PartsPurchasing/IntelligentOrderMonthly.png',4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3506,3500, 2, 'IntelligentOrderWeeklyBase', 'Released', NULL, '周度基础数据查询',NULL, 'Client/DCS/Images/Menu/PartsPurchasing/IntelligentOrderWeeklyBase.png',5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3507,3500, 2, 'IntelligentOrderWeekly', 'Released', NULL, '周度累计数据查询',NULL, 'Client/DCS/Images/Menu/PartsPurchasing/IntelligentOrderWeekly.png',6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3508,3500, 2, 'ViewPartNotConPurchasePlan', 'Released', NULL, '非常规采购计划管理','查询非常规采购计划', 'Client/DCS/Images/Menu/PartsPurchasing/ViewPartNotConPurchasePlan.png',7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3509,3500, 2, 'PartSafeStock', 'Released', NULL, '安全库存管理',NULL, 'Client/DCS/Images/Menu/PartsPurchasing/PartSafeStock.png',8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3510,3500, 2, 'PartConPurchasePlan', 'Released', NULL, '常规采购计划管理','查询常规采购计划', 'Client/DCS/Images/Menu/PartsPurchasing/PartConPurchasePlan.png',9, 2);
--配件销售管理

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4000,0, 0, 'PartsSales', NULL, NULL, '配件销售管理', NULL, 'Client/Images/ShellView/partsSales-s.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4100,4000, 1, 'PartsSales', NULL, NULL, '配件销售业务', NULL, NULL, 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4101,4100, 2, 'PartsSalesOrder', 'Released', NULL, '配件销售订单管理', '用于分公司或代理库对本企业的订单处理', 'Client/DCS/Images/Menu/PartsSales/PartsSalesOrder.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4102,4100, 2, 'PartsSalesOrderForReport', 'Released', NULL, '配件销售订单提报', '用于服务站向代理库或分公司提报销售订单', 'Client/DCS/Images/Menu/PartsSales/PartsSalesOrderForReport.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4103,4100, 2, 'PartsSalesReturnBill', 'Released', NULL, '配件销售退货管理', '用于分公司或代理库对本企业的退货单处理', 'Client/DCS/Images/Menu/PartsSales/PartsSalesReturnBill.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4104,4100, 2, 'PartsSalesReturnBillForReport', 'Released', NULL, '配件销售退货提报', '用于服务站向代理库或分公司提报退货单', 'Client/DCS/Images/Menu/PartsSales/PartsSalesReturnBillForReport.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4105,4100, 2, 'PartsSalesOrderDownLoad', 'Released', NULL, '配件销售订单下载', '用于订阅下载销售订单数据', 'Client/DCS/Images/Menu/PartsSales/PartsSalesOrderDownLoad.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4106,4100, 2, 'TowGradePartsNeedManager', 'Released', NULL, '二级站配件需求管理', '管理二级站配件需求计划', 'Client/DCS/Images/Menu/PartsSales/TowGradePartsNeedManager.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4107,4100, 2, 'TowGradePartsNeed', 'Released', NULL, '二级站配件需求提报', '提报二级站配件需求计划', 'Client/DCS/Images/Menu/PartsSales/TowGradePartsNeed.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4108,4100, 2, 'SparePartDirectory','Released', NULL, '查询电子目录图册', '查询电子目录图册', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4109,4100, 2, 'PartsSalesOrderForPartsCompany','Released', NULL, '销售订单管理-配件公司', '审核终止配件公司销售订单', 'Client/DCS/Images/Menu/PartsSales/PartsSalesOrder.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4110,4100, 2, 'PartsSalesOrderTraceQuery','Released', NULL, '销售订单跟踪查询', '查询销售订单信息', 'Client/DCS/Images/Menu/PartsSales/PartsSalesOrder.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4111,4100, 2, 'ERPDelivery','Released', NULL, 'ERP订单接口日志查询', 'ERP订单接口日志查询', 'Client/DCS/Images/Menu/PartsSales/ERPDelivery.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4112,4100, 2, 'ERPStock','Released', NULL, 'ERP盘点单新增接口日志查询', 'ERP盘点单新增接口日志查询', 'Client/DCS/Images/Menu/PartsSales/ERPStock.png', 11, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4113, 4100,2, 'ServiceAgentsHistoryQuery', 'Released', NULL, '服务站向代理库提报订单查询', '服务站向代理库提报订单查询', 'Client/DCS/Images/Menu/PartsSales/ServiceAgentsHistoryQuery.png', 12, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4114, 4100, 2, 'PartsSalesQuery', 'Released', NULL, '配件销售在途查询', '配件销售在途查询', 'Client/DCS/Images/Menu/PartsSales/PartsSalesQuery.png', 13, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4115, 4100, 2, 'BonusPointsOrder', 'Released', NULL, '积分订单管理', '查询订单积分', 'Client/DCS/Images/Menu/Common/BonusPointsOrder.png', 14, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4116,4100, 2, 'HwInterfaceLogQuery', 'Released', NULL, '海外接口日志查询', '海外接口日志查询', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 15, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4117,4100, 2, 'AgencyRetailerOrder', 'Released', NULL, '代理库电商订单管理', '代理库电商订单管理', 'Client/DCS/Images/Menu/PartsSales/AgencyRetailerOrder.png', 16, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4119,4100, 2, 'FollowingTrainPartsSalesOrder', 'Released', NULL, '随车行订单管理', '用于分发、导出订单处理', 'Client/DCS/Images/Menu/PartsSales/FollowingTrainPartsSalesOrder.png', 0, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4118,4100, 2, 'AgentsPartsSalesOrder', 'Released', NULL, '随车行代理库配件销售订单管理', '用于分公司或代理库对本企业的订单处理', 'Client/DCS/Images/Menu/PartsSales/PartsSalesOrder.png', 17, 2);

--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (41999,4100, 2, 'PartsSalesOrderDemo', 'Released', NULL, '配件销售订单管理Demo', '用于分公司或代理库对本企业的订单处理', 'Client/DCS/Images/Menu/PartsSales/PartsSalesOrder.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4200,4000, 1, 'PartsOuterPurchase', NULL, NULL, '配件外采业务', NULL, NULL, 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4201,4200, 2, 'PartsOuterPurchaseBillReport', 'Released', NULL, '配件外采提报','服务站或代理库提报外部采购订单', 'Client/DCS/Images/Menu/PartsPurchasing/PartsOuterPurchaseBillReport.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4202,4200, 2, 'PartsOuterPurchaseBill', 'Released', NULL, '配件外采管理','查询、审核服务站配件外采', 'Client/DCS/Images/Menu/PartsPurchasing/PartsOuterPurchaseBill.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4300, 4000, 1, 'PartsRetail', NULL, NULL, '配件零售业务', NULL, NULL, 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4301,4300, 2, 'PartsRetailOrder', 'Released', NULL, '配件零售订单管理', '分公司配件零售订单管理', 'Client/DCS/Images/Menu/PartsSales/PartsRetailOrder.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4302,4300, 2, 'PartsRetailReturnBill', 'Released', NULL, '配件零售退货管理', '分公司配件零售退货管理', 'Client/DCS/Images/Menu/PartsSales/PartsRetailReturnBill.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4303,4300, 2, 'DealerPartsRetailOrder', 'Released', NULL, '服务站/专卖店配件零售订单管理', '服务站/专卖店新增、修改保内保外配件零售订单', 'Client/DCS/Images/Menu/PartsSales/DealerPartsRetailOrder.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4304,4300, 2, 'DealerPartsRetailOrderForApprove', 'Released', NULL, '服务站配件零售审核', '分公司审核服务站保内配件零售订单', 'Client/DCS/Images/Menu/PartsSales/DealerPartsRetailOrderForApprove.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4305,4300, 2, 'TowGradePartsRetailOrder', 'Released', NULL, '二级站配件零售管理', '服务站新增、修改保内保外配件零售订单，并审核保外零售订单', 'Client/DCS/Images/Menu/PartsSales/TowGradePartsRetailOrder.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4306,4300, 2, 'TowGradePartsRetailOrderForApprove', 'Released', NULL, '二级站配件零售订单审核', '分公司审核服务站保内二级站配件零售订单','Client/DCS/Images/Menu/PartsSales/TowGradePartsRetailOrderForApprove.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4307,4300, 2, 'RetailerDeliveryQuery', 'Released', NULL, '新电商订单接口日志查询', '新电商订单接口日志查询', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4308,4300, 2, 'RetailerServiceApplyQuery', 'Released', NULL, '新电商退货接口日志查询', '新电商退货接口日志查询', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 8, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4400,4000, 1, 'BacklogPartsPlatform', NULL, NULL, '积压件平台', NULL, NULL, 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4401,4400, 2, 'OverstockPartsInformation', 'Released', NULL, '积压件信息管理', '查询、清除市场积压件库存信息', 'Client/DCS/Images/Menu/PartsStocking/OverstockPartsInformation.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4402,4400, 2, 'OverstockPartsInformationQuery', 'Released', NULL, '积压件信息查询', '查询市场积压件库存信息', 'Client/DCS/Images/Menu/PartsStocking/OverstockPartsInformationQuery.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4403,4400, 2, 'OverstockPartsApp', 'Released', NULL, '积压件申请管理', '代理库、服务站生成、修改、查询积压件申请单', 'Client/DCS/Images/Menu/PartsStocking/OverstockPartsApp.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4404,4400, 2, 'OverstockPartsAppForBranch', 'Released', NULL, '分公司积压件申请管理', '营销分公司查询、审批积压件申请单', 'Client/DCS/Images/Menu/PartsStocking/OverstockPartsAppForBranch.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4405,4400, 2, 'OverstockPartsAdjustBill', 'Released', NULL, '积压件调剂管理', '营销分公司、代理库查询查询、确认、完成积压件调剂单', 'Client/DCS/Images/Menu/PartsStocking/OverstockPartsAdjustBill.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4406,4400, 2, 'OverstockPartsAdjustBillForComplete', 'Released', NULL, '积压件调剂完成', NULL, 'Client/DCS/Images/Menu/PartsStocking/OverstockPartsApp.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4500,4000, 1, 'PartsSalesSettlement', NULL, NULL, '配件销售结算业务', NULL, NULL, 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4501,4500, 2, 'PartsSalesSettlement', 'Released', NULL, '配件销售结算管理', '查询、维护配件销售结算单', 'Client/DCS/Images/Menu/PartsSales/PartsSalesSettlement.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4502,4500, 2, 'PartsSalesRtnSettlement', 'Released', NULL, '配件销售退货结算管理', '查询、维护配件销售退货结算单', 'Client/DCS/Images/Menu/PartsSales/PartsSalesRtnSettlement.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4503,4500, 2, 'InvoiceInformationForPartsSalesSettlementQuery', 'Released', NULL, '配件销售结算发票查询', '查询配件销售结算发票', 'Client/DCS/Images/Menu/PartsSales/PartsSalesSettleInvoice.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4504,4500, 2, 'PartsSalesRtnSettlementQuery', 'Released', NULL, '配件销售退货结算查询', '经销商查询配件销售退货结算单', 'Client/DCS/Images/Menu/PartsSales/PartsSalesRtnSettlementQuery.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4505,4500, 2, 'PartsSalesSettlementQuery', 'Released', NULL, '配件销售结算单查询', '经销商查询配件销售结算单', 'Client/DCS/Images/Menu/PartsSales/PartsSalesSettlementQuery.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4506,4500, 2, 'BonusPointsSummary', 'Released', NULL, '积分汇总管理', '积分销售汇总单', 'Client/DCS/Images/Menu/Common/BonusPointsSummary.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4507,4500, 2, 'SettlementAutomaticTaskSet', 'Released', NULL, '销售结算自动任务设置', '销售结算自动任务设置', 'Client/DCS/Images/Menu/PartsSales/DealerPartsRetailOrderForApprove.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4508,4500, 2, 'SettlementAutomaticTaskSetQuery', 'Released', NULL, '销售结算自动任务执行结果', '销售结算自动任务执行结果', 'Client/DCS/Images/Menu/PartsPurchasing/IntelligentOrderWeeklyBase.png', 8, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4509,4500, 2, 'PartsSalesSettlementCIF', 'Released', NULL, '配件销售结算管理(福田总部)', '查询、维护配件销售结算单', 'Client/DCS/Images/Menu/PartsSales/PartsSalesSettlement.png', 9, 2);

--配件仓储物流管理

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5000,0, 0, 'PartsStockingAndLogistics', NULL, NULL, '配件仓储物流管理', NULL, 'Client/Images/ShellView/parts-stocking-s.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5100,5000, 1, 'PartsStocking', NULL, NULL, '配件仓储', NULL, NULL, 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5101,5100, 2, 'Warehouse', 'Released', NULL, '仓库管理', '维护配件仓库的基本信息，人员关系', 'Client/DCS/Images/Menu/PartsStocking/Warehouse.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5102,5100, 2, 'WarehouseArea', 'Released', NULL, '库区库位管理', '规划仓库结构，设置仓库库区库段库位信息，库段负责人', 'Client/DCS/Images/Menu/PartsStocking/WarehouseArea.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5103,5100, 2, 'PartsLocationAssignment', 'Released', NULL, '配件库位分配', '分配配件存储库位', 'Client/DCS/Images/Menu/PartsStocking/PartsLocationAssignment.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5104,5100, 2, 'PartsLocationChanges', 'Released', NULL, '配件库位变更履历查询', '查询配件的库位变更情况', 'Client/DCS/Images/Menu/PartsStocking/PartsLocationChanges.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5105,5100, 2, 'SYNCWMSINOUTLOGINFO', 'Released', NULL, 'WMS接口日志查询', '查询WMS接口同步日志', 'Client/DCS/Images/Menu/PartsStocking/SYNCWMSINOUTLOGINFO.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5106,5100, 2, 'WMSLabelPrintingSerialQuery', 'Released', NULL, '配件标签序列号查询', '查询配件标签序列号', 'Client/DCS/Images/Menu/PartsStocking/WMSLabelPrintingSerial.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5200,5000, 1, 'PartsStore', NULL, NULL, '配件库存', NULL, NULL, 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5201,5200, 2, 'PartsStockDetailDownLoad', 'Released', NULL, '配件库存明细下载', '提供配件库存明细数据订阅下载', 'Client/DCS/Images/Menu/PartsStocking/PartsStockDetailDownLoad.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5202,5200, 2, 'PartsStockDetailQuery', 'Released', NULL, '配件库存明细查询', '查询库位上的品种数量和批次明细', 'Client/DCS/Images/Menu/PartsStocking/PartsStockDetailQuery.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5203,5200, 2, 'DealerPartsStock', 'Released', NULL, '服务站配件库存管理', '服务站查询配件库存明细',  'Client/DCS/Images/Menu/PartsStocking/DealerPartsStock.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5204,5200, 2, 'PartsStockQuery', 'Released', NULL, '配件仓库库存查询', '查询配件的仓库库存','Client/DCS/Images/Menu/PartsStocking/PartsStockQuery.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5205,5200, 2, 'PartsStockForWarrantyQuery', 'Released', NULL, '配件仓库库存查询-保外', '查询配件的仓库库存','Client/DCS/Images/Menu/PartsStocking/PartsStockQuery.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5206,5200, 2, 'DealerPartsStockQueryView', 'Released', NULL, '服务站库存查询', '查询服务站库存','Client/DCS/Images/Menu/PartsStocking/PartsStockQuery.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5207, 5200, 2, 'AgencyStockQuery', 'Released', NULL, '代理库库存查询', '代理库库存查询', 'Client/DCS/Images/Menu/PartsStocking/AgencyStockQuery.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5208,5200, 2, 'VehiclePartsStockLevel', 'Released', NULL, '配件仓库高低限管理', '随车行配件仓库高低限', 'Client/DCS/Images/Menu/PartsStocking/DealerPartsStock.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5209,5200, 2, 'EnterprisePartsCost', 'Released', NULL, '企业库存管理', '企业库存管理', 'Client/DCS/Images/Menu/PartsStocking/PartsStockQuery.png',  9,  2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5300,5000, 1, 'PartsInStore', NULL, NULL, '配件入库业务', NULL, NULL, 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5301,5300, 2, 'PartsInboundCheckBill', 'Released', NULL, '配件检验入库', '库管员生成入库检验单', 'Client/DCS/Images/Menu/PartsStocking/PartsInboundCheckBill.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5302,5300, 2, 'PartsPacking', 'Released', NULL, '配件包装', '库管员在检验区完成包装、录入批次号', 'Client/DCS/Images/Menu/PartsStocking/PartsPacking.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5303,5300, 2, 'PartsShelves', 'Released', NULL, '配件上架', '库管员将完成包装的配件移库到保管区', 'Client/DCS/Images/Menu/PartsStocking/PartsShelves.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5304,5300, 2, 'PartsInboundPlanQuery', 'Released', NULL, '配件入库计划查询', '仓库人员查询辖内仓库入库计划单', 'Client/DCS/Images/Menu/PartsStocking/PartsInboundPlan.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5305,5300, 2, 'PartsInboundCheckBillQuery', 'Released', NULL, '配件入库检验单查询', '仓库人员查询辖内仓库入库检验单', 'Client/DCS/Images/Menu/PartsStocking/PartsInboundCheckBillQuery.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5306,5300, 2, 'PartsInboundCheckBillForSuuplierQuery', 'Released', NULL, '配件入库检验单查询-供应商', '供应商人员查询本企业配件入库检验单', 'Client/DCS/Images/Menu/PartsStocking/PartsInboundCheckBillQuery.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5307,5300, 2, 'PartsInboundCheckBillWithPlanPriceQuery', 'Released', NULL, '配件入库检验单查询(计划价)', '仓库人员查询辖内仓库入库检验单', 'Client/DCS/Images/Menu/PartsStocking/PartsInboundCheckBillQuery.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5308, 5300, 2, 'InboundHistoryQuery', 'Released', NULL, '历史入库记录查询', '查看历史入库记录', 'Client/DCS/Images/Menu/PartsStocking/InboundHistoryQuery.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5309, 5300, 2, 'AgentsHistoryIn', 'Released', NULL, '代理库入库记录查询', '查询某一时间段内代理库入库明细', 'Client/DCS/Images/Menu/PartsStocking/AgentsHistoryIn.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5310, 5300, 2, 'GCCPartsInboundCheckBillQuery', 'Released', NULL, '工程车配件入库检验单查询', '查询工程车配件入库检验单信息', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5311,5300, 2, 'PartsInboundCheckBillNoPriceQuery', 'Released', NULL, '配件入库检验单查询（无价格）', '仓库人员查询辖内仓库入库检验单', 'Client/DCS/Images/Menu/PartsStocking/PartsInboundCheckBillQuery.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5312,5300, 2, 'PartsInboundPlanNoPriceQuery', 'Released', NULL, '配件入库计划查询（无价格）', '仓库人员查询辖内仓库入库计划单', 'Client/DCS/Images/Menu/PartsStocking/PartsInboundPlan.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5313,5300, 2, 'PartsInboundCheckBillNoPrice', 'Released', NULL, '配件检验入库（无价格）', '库管员生成入库检验单', 'Client/DCS/Images/Menu/PartsStocking/PartsInboundCheckBill.png', 1, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5400,5000, 1, 'PartsOutStore', NULL, NULL, '配件出库业务', NULL, NULL, 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5401,5400, 2, 'PartsOutboundBill', 'Released', NULL, '配件出库', '库管员生成出库单', 'Client/DCS/Images/Menu/PartsStocking/PartsOutboundBill.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5402,5400, 2, 'PartsOutboundPlanQuery', 'Released', NULL, '配件出库计划查询', '仓库人员查询辖内仓库出库计划单', 'Client/DCS/Images/Menu/PartsStocking/PartsOutboundPlanQuery.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5403,5400, 2, 'PartsOutboundBillQuery', 'Released', NULL, '配件出库单查询', '仓库人员查询辖内仓库出库单', 'Client/DCS/Images/Menu/PartsStocking/PartsOutboundBillQuery.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5404,5400, 2, 'PartsOutboundBillReturn', 'Released', NULL, '配件出库退回', '配件出库退回单管理', 'Client/DCS/Images/Menu/PartsStocking/PartsOutboundBillReturn.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5405,5400, 2, 'AgentsHistoryOut', 'Released', NULL, '代理库出库记录查询', '查询某一时间段内代理库出库明细', 'Client/DCS/Images/Menu/PartsStocking/AgentsHistoryOut.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5408,5400, 2, 'GCCPartsOutboundBillQuery', 'Released', NULL, '工程车配件出库单查询', '仓库人员查询辖内仓库出库单', 'Client/DCS/Images/Menu/PartsStocking/PartsOutboundBillQuery.png',11, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5409,5400, 2, 'AgentsPartsOutboundPlanQuery', 'Released', NULL, '配件出库计划单查询-代理库随车行', '仓库人员查询辖内仓库出库计划单', 'Client/DCS/Images/Menu/PartsStocking/PartsOutboundPlanQuery.png', 12, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5410,5400, 2, 'AgentsPartsOutboundBillQuery', 'Released', NULL, '配件出库单查询-代理库随车行', '仓库人员查询辖内仓库出库单', 'Client/DCS/Images/Menu/PartsStocking/PartsOutboundBillQuery.png', 13, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5411,5400, 2, 'BoxUpTask',  'Released',  NULL,  '配件装箱管理',  '装箱任务管理',  'Client/DCS/Images/Menu/PartsStocking/PartsPacking.png',  14,  2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5412,5400, 2, 'PickingTask', 'Released', NULL, '配件拣货管理', '拣货单查询、拣货装箱', 'Client/DCS/Images/Menu/PartsStocking/PartsInboundPlan.png', 15, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5413,5400, 2, 'PickingTaskQuery', 'Released', NULL, '配件拣货单分配', '拣货单查询、分配责任人', 'Client/DCS/Images/Menu/PartsStocking/PartsInboundPlan.png', 16, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5500,5000, 1, 'PartsLogisticsRelated', NULL, NULL, '配件物流', NULL, NULL, 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5501,5500, 2, 'PartsShippingOrder', 'Released', NULL, '配件发运管理', '发货单位生成、查询、回执确认配件发运单', 'Client/DCS/Images/Menu/PartsStocking/PartsShippingOrderManagement.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5502,5500, 2, 'PartsShippingOrderForConfirm', 'Released', NULL, '配件收货管理', '收货单位查询、收货确认配件发运单', 'Client/DCS/Images/Menu/PartsStocking/PartsShippingOrderForConfirmManagement.png', 2, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5503,5500, 2, 'PartsLogisticLossBill', 'Released', NULL, '物流损失管理', '查询、编辑物流损失单', 'Client/DCS/Images/Menu/PartsStocking/PartsLogisticLossBill.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5504,5500, 2, 'HauDistanceInfor', 'Released', NULL, '配件发运运距管理', '查询、维护配件发运运距', 'Client/DCS/Images/Menu/PartsStocking/HauDistanceInfor.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5505,5500, 2, 'PartsShippingOrderForLogistic', 'Released', NULL, '配件发运管理-承运商', '承运商查询配件发运单', 'Client/DCS/Images/Menu/PartsStocking/PartsShippingOrderManagement.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5508,5500, 2, 'GCCReceiptQuery', 'Released', NULL, '瑞沃配件发运单查询', '查询、合并导出工程车配件收货单', 'Client/DCS/Images/Menu/PartsStocking/GCCReceiptQuery.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5509,5500, 2, 'LogisticsTracking', 'Released', NULL, '物流跟踪管理', '录入、查询、合并导出物流跟踪单', 'Client/DCS/Images/Menu/PartsStocking/PartsLogisticLossBill.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5510,5500, 2, 'AgentsPartsShippingOrder', 'Released', NULL, '代理库配件发运单查询', '查询、合并导出工程车配件收货单', 'Client/DCS/Images/Menu/PartsStocking/AgentsPartsShippingOrder.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5511,5500, 2, 'AgentsLogisticCompany', 'Released', NULL, '代理库物流公司管理', '维护物流公司信息', 'Client/DCS/Images/Menu/Common/LogisticCompany.png', 11, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5512,5500, 2, 'AgencyPartsShippingOrder', 'Released', NULL, '配件发运管理-代理库随车行', '', 'Client/DCS/Images/Menu/PartsStocking/PartsShippingOrderManagement.png', 12, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5600,5000, 1, 'WarehouseExecution', NULL, NULL, '配件库内业务', NULL, NULL, 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5601,5600, 2, 'PartsShiftOrder', 'Released', NULL, '配件移库管理', '库内配件在库位间转移', 'Client/DCS/Images/Menu/PartsStocking/PartsShiftOrder.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5602,5600, 2, 'PartsInventoryBill', 'Released', NULL, '配件盘点管理', '分公司生成、审核配件盘点单', 'Client/DCS/Images/Menu/PartsStocking/PartsInventoryBill.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5603,5600, 2, 'AgentsPartsInventoryBillReported', 'Released', NULL, '代理库配件盘点单提报', '代理库查询、提报配件盘点单', 'Client/DCS/Images/Menu/PartsStocking/PartsInventoryBillReported.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5604,5600, 2, 'AgentsPartsInventoryBill', 'Released', NULL, '代理库配件盘点管理', '分公司查询、审核代理库配件盘点单','Client/DCS/Images/Menu/PartsStocking/AgentsPartsInventoryBill.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5605,5600, 2, 'DealerPartsInventoryBill', 'Released', NULL, '服务站配件盘点管理', '分公司查询、审核服务站配件盘点单', 'Client/DCS/Images/Menu/PartsStocking/DealerPartsInventoryBill.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5606,5600, 2, 'DealerPartsInventoryBillForReport', 'Released', NULL, '服务站配件盘点提报', '服务站查询、维护配件盘点单',  'Client/DCS/Images/Menu/PartsStocking/DealerPartsInventoryBillForReport.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5607,5600, 2, 'WMSPartsInventoryBill', 'Released', NULL, 'WMS配件盘点管理', '分公司生成、审核配件盘点单', 'Client/DCS/Images/Menu/PartsStocking/PartsInventoryBill.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5608,5600, 2, 'PWMSPartsInventoryBill', 'Released', NULL, '代理库PWMS配件盘点管理', '审核配件盘点单', 'Client/DCS/Images/Menu/PartsStocking/PartsInventoryBill.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5700,3000, 1, 'Accessories', NULL, NULL, '配件调拨业务', NULL, NULL, 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5701,5700, 2, 'PartsTransferOrder', 'Released', NULL, '配件调拨管理', '生成、审批仓库间配件调拨', 'Client/DCS/Images/Menu/PartsStocking/PartsTransferOrder.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5702,5700, 2, 'CompanyTransferOrder', 'Released', NULL, '企业间配件调拨管理','查询、维护企业之间的配件调拨',  'Client/DCS/Images/Menu/PartsStocking/CompanyTransferOrder.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5703,5700, 2, 'DealerPartsTransferOrder', 'Released', NULL, '服务站配件调拨管理', '', 'Client/DCS/Images/Menu/PartsStocking/CompanyTransferOrder.png',6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5704,5700, 2, 'DealerPartsTransferOrderForReport', 'Released', NULL, '服务站配件调拨提报', '', 'Client/DCS/Images/Menu/PartsStocking/CompanyTransferOrder.png',5, 2);

--配件财务管理

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6000,0, 0, 'PartsFinancialAndControls', NULL, NULL, '配件财务管理', NULL, 'Client/Images/ShellView/parts-financial-s.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6100,6000, 1, 'StockAccounting', NULL, NULL, '存货核算', NULL, NULL, 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6101,6100, 2, 'PlannedPriceApp', 'Released', NULL, '配件计划价申请管理', '申请、执行计划价变更', 'Client/DCS/Images/Menu/Financial/PlannedPriceApp.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6105,6100, 2, 'PlannedPriceAppYX', 'Released', NULL, '配件计划价申请管理（营销）', '申请、执行计划价变更', 'Client/DCS/Images/Menu/Financial/PlannedPriceApp.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6102,6100, 2, 'PartsPlannedPrice', 'Released', NULL, '配件计划价查询', '查询本公司配件计划价', 'Client/DCS/Images/Menu/Financial/PartsPlannedPrice.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6103,6100, 2, 'FinancialSnapshotSet', 'Released', NULL, '财务快照设置', '查询、设置财务快照', 'Client/DCS/Images/Menu/Financial/FinancialSnapshotSet.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6104,6100, 2, 'PlannedPriceAppHistory', 'Released', NULL, '配件计划价变更履历查询', '查询本公司配件计划价变更履历查询', 'Client/DCS/Images/Menu/Financial/PartsPlannedPrice.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6106,6100, 2, 'AccountPeriod', 'Released', NULL, '账期设置', '查询、设置账期', 'Client/DCS/Images/Menu/Financial/CAReconciliation.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6200,6000, 1, 'PartFunctions', NULL, NULL, '应收管理', NULL, NULL, 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6201,6200, 2, 'CredenceApplication', 'Released', NULL, '信用管理', '新增、审批客户信用申请', 'Client/DCS/Images/Menu/Financial/CredenceApplication.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6202,6200, 2, 'AccountGroup', 'Released', NULL, '账户组管理', '设置、维护销售账户组', 'Client/DCS/Images/Menu/Financial/AccountGroup.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6203,6200, 2, 'CustomerOpenAccountApp', 'Released', NULL, '客户开户申请管理', '开设客户账户并初始化客户账户余额', 'Client/DCS/Images/Menu/Financial/CustomerOpenAccountApp.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6204,6200, 2, 'PaymentBill', 'Released', NULL, '来款单管理', '登记、审批、分割客户来款', 'Client/DCS/Images/Menu/Financial/PlannedPriceApp.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6205,6200, 2, 'PaymentBeneficiaryList', 'Released', NULL, '来款分割单查询', '查询来款分割单明细', 'Client/DCS/Images/Menu/Financial/PartsPlannedPrice.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6206,6200, 2, 'CustomerAccount', 'Released', NULL, '客户账户查询', '查询客户账户余额', 'Client/DCS/Images/Menu/Financial/CustomerAccount.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6207,6200, 2, 'CustomerAccountHisDetail', 'Released', NULL, '客户账户明细查询', '查询客户账户发生明细', 'Client/DCS/Images/Menu/Financial/CustomerAccountHisDetail.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6208,6200, 2, 'CustomerTransferBill', 'Released', NULL, '客户转账管理', '同一客户不同账户或者不同客户账户之间的转账', 'Client/DCS/Images/Menu/Financial/PartsPlannedPrice.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6209, 6200, 2, 'AccountQuery', 'Released', NULL, '账户查询', '查询账户', 'Client/DCS/Images/Menu/Financial/AccountQuery.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6210,6200, 2, 'AccountDetailQuery', 'Released', NULL, '账户明细查询', '查询本企业账户发生明细', 'Client/DCS/Images/Menu/Financial/AccountDetailQuery.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6211,6200, 2, 'SAPInvoiceInfoQuery', 'Released', NULL, 'SAP接口发票查询', '查询SAP接口发票', 'Client/DCS/Images/Menu/Financial/SAPInvoiceInfoQuery.png', 11, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6212,6200, 2, 'SAPYXPaymentInfo', 'Released', NULL, 'SAP收付款接口查询', '查询SAP接口收付款单', 'Client/DCS/Images/Menu/Financial/SAPYXPaymentInfo.png', 12, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6213,6200, 2, 'SAPYXErrorTable', 'Released', NULL, 'SAP业务接口查询', '查询SAP接口错误日志', 'Client/DCS/Images/Menu/Financial/SAPYXErrorTable.png', 13, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6214,6200, 2, 'FDSAPInvoiceInfoQuery', 'Released', NULL, '福戴SAP发票接口查询', '查询福戴SAP接口发票', 'Client/DCS/Images/Menu/Financial/SAPInvoiceInfoQuery.png', 14, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6215,6200, 2, 'SAPFDErrorTable', 'Released', NULL,'福戴SAP业务接口查询', '查询福戴SAP接口错误信息', 'Client/DCS/Images/Menu/Financial/SAPYXErrorTable.png', 15, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6216,6200, 2, 'AgencyCustomerAccount', 'Released', NULL,'服务站在代理库往来流水账查询', '服务站在代理库账户明细查询', 'Client/DCS/Images/Menu/Financial/AgencyCustomerAccount.png', 16, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6217,6200, 2, 'CustomerAccountHistory', 'Released', NULL,'客户账户历史查询', '查询客户账户历史记录', 'Client/DCS/Images/Menu/Financial/CustomerAccountHisDetail.png', 17, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6218,6200, 2, 'CustomerAccountHistoryQuery', 'Released', NULL,'服务站在代理库账面余额历史查询', '服务站在代理库账面余额历史查询', 'Client/DCS/Images/Menu/Financial/CustomerAccountHistoryQuery.png', 18, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6219,6200, 2, 'CAReconciliation', 'Released', NULL,'企业往来账对账函管理', '企业往来账对账函管理', 'Client/DCS/Images/Menu/Financial/CAReconciliation.png', 19, 2);


INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6300, 6000, 1, 'ToPay', NULL, NULL, '应付管理', NULL, NULL, 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6301, 6300, 2, 'SupplierOpenAccountApp', 'Released', NULL, '供应商开户申请管理', '开设供应商账户并初始化客户账户余额', 'Client/DCS/Images/Menu/Financial/SupplierOpenAccountApp.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6302, 6300, 2, 'SupplierAccount', 'Released', NULL, '供应商账户查询', '查询供应商账户余额', 'Client/DCS/Images/Menu/Financial/SupplierAccount.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6303, 6300, 2, 'SupplierAccountHisDetail', 'Released', NULL, '供应商账户明细查询', '查询供应商账户发生明细', 'Client/DCS/Images/Menu/Financial/SupplierAccountHisDetail.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6304, 6300, 2, 'PayManage', 'Released', NULL, '付款管理', '付款管理', 'Client/DCS/Images/Menu/Financial/PayManage.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6400, 6000, 1, 'PartsRebateManage', NULL, NULL, '配件返利管理', NULL, NULL, 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6401,6400, 2, 'PartsRebateType', 'Released', NULL, '配件返利类型管理', '分公司维护配件返利类型', 'Client/DCS/Images/Menu/Financial/PartsRebateType.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6402,6400, 2, 'PartsRebateApplication', 'Released', NULL, '配件返利申请管理', '分公司管理配件返利申请', 'Client/DCS/Images/Menu/Financial/PartsRebateApplication.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6403,6400, 2, 'PartsRebateChangeDetailQuery', 'Released', NULL, '配件返利变更明细查询', '查询配件返利账户变更明细', 'Client/DCS/Images/Menu/Financial/PartsRebateChangeDetailQuery.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6404,6400, 2, 'PartsRebateAccount', 'Released', NULL, '配件返利账户', '查询配件返利账户', 'Client/DCS/Images/Menu/Financial/PartsRebateAccount.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6405,6400, 2, 'PartsRebateChangeDetailForDealerQuery', 'Released', NULL, '配件返利变更明细查询-服务站', '查询配件返利账户变更明细', 'Client/DCS/Images/Menu/Financial/PartsRebateChangeDetailQuery.png', 5, 2);
--服务基础数据管理

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7000, 0, 0, 'ServiceBasicData', NULL, NULL, '服务基础数据管理', NULL, 'Client/Images/ShellView/service-basic-s.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7100, 7000, 1, 'QualityAssuranceStandards', NULL, NULL, '质保标准管理', NULL, NULL, 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7101,7100, 2, 'ServiceProductLine', 'Released', NULL, '服务产品线管理', '定义服务产品线，用于服务政策制定', 'Client/DCS/Images/Menu/Common/ServiceProductLine.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7102,7100, 2, 'ServProdLineAffiProduct', 'Released', NULL, '服务产品线与产品关系', '查询、维护服务产品线与产品关系', 'Client/DCS/Images/Menu/Common/ServProdLineAffiProduct.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7103,7100, 2, 'EngineProductLine', 'Released', NULL, '发动机产品线管理', '维护车辆发动机的产品线',  'Client/DCS/Images/Menu/Common/EngineProductLine.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7104,7100, 2, 'EngineModelProductLine', 'Released', NULL, '发动机产品线与发动机关系', '维护车辆发动机产品线与发动机型号的关系', 'Client/DCS/Images/Menu/Common/EngineModelProductLine.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7105, 7100, 2, 'PartsWarrantyCategory', 'Released', NULL, '配件保修分类管理', '维护配件保修分类', 'Client/DCS/Images/Menu/ServiceBasicData/PartsWarrantyCategory.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7106, 7100, 2, 'WarrantyPolicy', 'Released', NULL, '保修政策管理', '维护保修政策及保修条款、保养条款、配件保修条款', 'Client/DCS/Images/Menu/ServiceBasicData/WarrantyPolicy.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7107, 7100, 2, 'WarrantyPolicyNServProdLine', 'Released', NULL, '保修政策与产品线关系', '指定保修政策与产品线关系', 'Client/DCS/Images/Menu/ServiceBasicData/WarrantyPolicyNServProdLine.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7108, 7100, 2, 'PartsWarrantyStandard', 'Released', NULL, '配件保修分类标准管理', '维护配件保修分类标准', 'Client/DCS/Images/Menu/ServiceBasicData/PartsWarrantyStandard.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7109, 7100, 2, 'VehicleInfoProductQuery', 'Released', NULL, '车辆产品结构查询', '查询产品结构信息', 'Client/Dcs/Images/Menu/ServiceBasicData/VehProductStrucQuery.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7110, 7100, 2, 'AutoApproveStrategy', 'Released', NULL, '自动审核策略管理', '查询和维护自动审核策略', 'Client/DCS/Images/Menu/ServiceBasicData/AutoApproveStrategy.png', 11, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7200, 7000, 1, 'WarrantyCostManagement', NULL, NULL, '质保费用标准管理', NULL, NULL, 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7201, 7200, 2, 'PartsManagementCostGrade', 'Released', NULL, '配件管理费率等级管理', '查询和维护配件管理费率等级及区间单价', 'Client/DCS/Images/Menu/ServiceBasicData/PartsManagementCostGrade.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7202, 7200, 2, 'LaborHourUnitPriceGrade', 'Released', NULL, '工时单价等级管理', '查询和维护工时单价等级及工时单价', 'Client/DCS/Images/Menu/ServiceBasicData/LaborHourUnitPriceGrade.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7203, 7200, 2, 'ServiceTripPriceGrade', 'Released', NULL, '外出服务费等级管理', '查询和维护外出服务费等级及区间单价', 'Client/DCS/Images/Menu/ServiceBasicData/ServiceTripPriceGrade.png', 3, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7204, 7200, 2, 'ServiceTripPriceStandard', 'Released', NULL, '外出服务费标准管理', '', '', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7205, 7200, 2, 'GradeCoefficient', 'Released', NULL, '结算星级系数管理', '查询和维护品牌星级系数', 'Client/DCS/Images/Menu/ServiceBasicData/GradeCoefficient.png',5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7206, 7200, 2, 'ServiceTripArea', 'Released', NULL, '外出区域管理', '查询和维护外出区域', 'Client/DCS/Images/Menu/ServiceBasicData/ServiceTripArea.png',6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7207, 7100, 2, 'WarrantyPolicyQuery', 'Released', NULL, '保修政策查询', '查询保修政策', 'Client/DCS/Images/Menu/ServiceBasicData/WarrantyPolicy.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7300, 7000, 1, 'MaintenanceItemsAndFaultPhenomena', 'Released', NULL, '维修项目与故障现象', NULL, NULL, 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7301, 7300, 2, 'Malfunction', 'Released', NULL, '故障代码管理', '维护故障现象分类及故障现象', 'Client/DCS/Images/Menu/ServiceBasicData/Malfunction.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7302, 7300, 2, 'MalfunctionBrandRelation', 'Released', NULL, '故障代码与品牌对应关系', '查询和维护故障代码与品牌关系', 'Client/DCS/Images/Menu/ServiceBasicData/MalfunctionBrandRelation.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7303, 7300, 2, 'RepairItem', 'Released', NULL, '标准维修工时代码管理', '维护维修项目分类与维修项目', 'Client/DCS/Images/Menu/ServiceBasicData/RepairItem.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7304, 7300, 2, 'RepairItemPartsSalesCategory', 'Released', NULL, '标准维修工时代码与品牌对应关系', '查询和维护标准维修工时代码与品牌关系', 'Client/DCS/Images/Menu/ServiceBasicData/RepairItemPartsSalesCategory.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7305, 7300, 2, 'RepairItemAffiServProdLine', 'Released', NULL, '标准维修工时定额管理', '查询和维护维修项目工时定额','Client/DCS/Images/Menu/ServiceBasicData/RepairItemAffiServProdLine.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7306, 7300, 2, 'RepairMethodStandard', 'Released', NULL, ' 维修方式标准库管理', '查询和维护维修方法标准','Client/DCS/Images/Menu/ServiceBasicData/RepairItem.png' , 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7307, 7300, 2, 'FaultModeStandard', 'Released', NULL, '故障模式标准库管理', '查询和维护故障模式标准', 'Client/DCS/Images/Menu/ServiceBasicData/Malfunction.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7308, 7300, 2, 'RepairItemQuery', 'Released', NULL, ' 标准维修工时代码查询', '查询维修项目分类与维修项目','Client/DCS/Images/Menu/ServiceBasicData/RepairItemQuery.png' ,86, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7309, 7300, 2, 'MalfunctionQuery', 'Released', NULL, '故障代码查询', '查询故障现象分类及故障现象', 'Client/DCS/Images/Menu/ServiceBasicData/MalfunctionQuery.png', 9, 2);


INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7400, 7000, 1, 'VehicleAndCustomerDocument', 'Released', NULL, '车辆及客户档案', NULL, NULL, 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7401, 7400, 2, 'VehicleInfo', 'Released', NULL, '车辆信息管理', '查询和维护车辆信息', 'Client/DCS/Images/Menu/ServiceBasicData/VehicleInfo.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7402, 7400, 2, 'VehicleWarrantyCard', 'Released', NULL, '单车服务卡管理', '查询、维护车辆单车卡信息', 'Client/DCS/Images/Menu/ServiceBasicData/VehicleWarrantyCard.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7403, 7400, 2, 'CustomerInfo', 'Released', NULL, '客户信息管理', '查询和维护车辆客户信息', 'Client/DCS/Images/Menu/ServiceBasicData/CustomerInfo.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7404, 7400, 2, 'QTSDataQuery', 'Released', NULL, '装车明细查询', '查询和维护装车明细信息', 'Client/DCS/Images/Menu/ServiceBasicData/VehicleInfo.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7405, 7400, 2, 'dtp_vehicleinformation', 'Released', NULL, 'DMS接口日志查询-车辆信息', '', 'Client/DCS/Images/Menu/ServiceBasicData/DMSLogoInfo.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7406, 7400, 2, 'LoadingDetail', 'Released', NULL, '装车明细管理', '查询和维护装车明细信息', 'Client/DCS/Images/Menu/ServiceBasicData/VehicleInfo.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7407, 7400, 2, 'VehicleChangeMsg', 'Released', NULL, '车辆信息变更履历', '查询车辆信息变更履历', 'Client/DCS/Images/Menu/ServiceBasicData/VehicleChangeMsg.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7408, 7400, 2, 'MemberInfo', 'Released', NULL, '会员管理', '查询会员信息', 'Client/DCS/Images/Menu/Common/MemberInfo.png', 1, 2);


INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7500, 7000, 1, 'QTSDataQuerys', 'Released', NULL, 'QTS数据查询', NULL, NULL, 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7501, 7500, 2, 'QTSDataQueryForFD', 'Released', NULL, '车辆明细查询-福戴', '查询和维护车辆明细信息-福戴', 'Client/DCS/Images/Menu/ServiceBasicData/QTSDataQueryForFD.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7502, 7500, 2, 'QTSDataQueryForSP', 'Released', NULL, '车辆明细查询-萨普', '查询和维护车辆明细信息-萨普', 'Client/DCS/Images/Menu/ServiceBasicData/QTSDataQueryForSP.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7503, 7500, 2, 'QTSDataQueryForDGN', 'Released', NULL, '车辆明细查询-多功能', '查询和维护车辆明细信息-多功能', 'Client/DCS/Images/Menu/ServiceBasicData/QTSDataQueryForDGN.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7504, 7500, 2, 'QTSDataQueryForNFGC', 'Released', NULL, '车辆明细查询-南方工程车', '查询和维护车辆明细信息-南方工程车', 'Client/DCS/Images/Menu/ServiceBasicData/QTSDataQueryForDGN.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7505, 7500, 2, 'QTSDataQueryForSD', 'Released', NULL, '车辆明细查询-时代', '查询和维护车辆明细信息-时代', 'Client/DCS/Images/Menu/ServiceBasicData/QTSDataQueryForFD.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7506, 7500, 2, 'QTSDataQueryForQXC', 'Released', NULL, '车辆明细查询-轻型车', '查询和维护车辆明细信息-轻型车', 'Client/DCS/Images/Menu/ServiceBasicData/QTSDataQueryForFD.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7507, 7500, 2, 'QTSDataQueryForLS', 'Released', NULL, '车辆明细查询-雷萨', '查询和维护车辆明细信息-雷萨', 'Client/DCS/Images/Menu/ServiceBasicData/QTSDataQueryForFD.png', 7, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7600, 7000, 1, 'AfterSaleTecFileManagementSys', 'Released', NULL, '售后技术文件管理系统', NULL, NULL, 6, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7601, 7600, 2, 'AfterSaleTecFileManaSysQuery', 'Released', NULL, '售后技术文件管理系统查询', '查询售后技术文件管理系统', 'Client/Dcs/Images/Menu/ServiceBasicData/VehProductStrucQuery.png', 1, 2);



--维修索赔业务管理

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8000,0, 0, 'Service', NULL, NULL, '维修索赔业务管理', NULL, 'Client/Images/ShellView/service-s.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8100, 8000, 1, 'RepairServiceForBranch', 'Released', NULL, '维修单管理', NULL,'Client/DCS/Images/Menu/Service/', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8101, 8100, 2, 'RepairWorkOrderForBrachQuery', 'Released', NULL, '维修工单查询', '分公司查询维修工单', 'Client/DCS/Images/Menu/Service/RepairWorkOrderForBrachQuery.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8102, 8100, 2, 'RepairOrderForBranch', 'Released', NULL, '保内维修单管理', '分公司解锁、查看维修单信息', 'Client/DCS/Images/Menu/Service/RepairOrderForBranch.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8103, 8100, 2, 'RepairOrderForWarrantyBranch', 'Released', NULL, '保外维修单管理', '分公司查询和维护保外维修单', 'Client/DCS/Images/Menu/Service/RepairOrderForWarrantyBranch.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8104, 8100, 2, 'RepairMould', 'Released', NULL, '维修模板管理', '分公司查询和维护维修模板', 'Client/DCS/Images/Menu/Service/RepairMould.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8105, 8100, 2, 'SalesBeyoundCheck', 'Released', NULL, '售前检查管理', '分公司查询和维护售前检查', 'Client/DCS/Images/Menu/Service/SalesBeyoundCheck.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8106, 8100, 2, 'PreSaleItem', 'Released', NULL, '售前检查项目管理', '分公司查询和维护售前检查项目', 'Client/DCS/Images/Menu/Service/PreSaleItem.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8107, 8100, 2, 'ReturnVisitQuest', 'Released', NULL, '满意度问卷管理', '满意度问卷管理', 'Client/DCS/Images/Menu/Service/ReturnVisitQuest.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8108, 8100, 2, 'RepairWorkOrderMd', 'Released', NULL, '互动中心工单处理', '互动中心工单处理', 'Client/DCS/Images/Menu/Service/RepairWorkOrderMd.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8109, 8100, 2, 'TransactionInterfaceLog', 'Released', NULL, '保外会员交易接口日志查询', '保外会员交易接口处理', 'Client/DCS/Images/Menu/Common/MemberInfo.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8200, 8000, 1, 'RepairApplyForBranch', 'Released', NULL, '申请单管理', NULL, NULL, 2, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8201, 8200, 2, 'ApplyRules', 'Released', NULL, '预申请规则管理', '分公司查询和维护预申请规则', 'Client/DCS/Images/Menu/Service/ApplyRules.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8202,8200, 2, 'RepairClaimApplication', 'Released', NULL, '维修索赔申请管理', '审核三包维修索赔申请单', 'Client/DCS/Images/Menu/Service/RepairClaimApplication.png',2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8203,8200, 2, 'MaintainClaimApplication', 'Released', NULL, '保养索赔申请管理', '审核保养索赔申请单', 'Client/DCS/Images/Menu/Service/MaintainClaimApplication.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8204,8200, 2, 'ServiceTripClaimApplication', 'Released', NULL, '外出索赔申请管理', '审核外出索赔申请单', 'Client/DCS/Images/Menu/Service/ServiceTripClaimApplication.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8205,8200, 2, 'ServicesClaimApplication', 'Released', NULL, '服务活动索赔申请管理', '审核服务活动索赔申请单', 'Client/DCS/Images/Menu/Service/ServicesClaimApplication.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8206,8200, 2, 'PartsClaimApplication', 'Released', NULL, '配件索赔申请管理', '审核非三包配件索赔申请单', 'Client/DCS/Images/Menu/Service/PartsClaimApplication.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8207,8200, 2, 'PartsClaimApplicationForCountersign', 'Released', NULL, '维修索赔申请单会签', '会签维修索赔申请单', 'Client/DCS/Images/Menu/Service/PartsClaimApplication.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8208,8200, 2, 'ServiceTripClaimApplicationCountersign', 'Released', NULL, '外出索赔申请单会签', '会签外出索赔申请单', 'Client/DCS/Images/Menu/Service/ServiceTripClaimApplication.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8209, 8200, 2, 'MarketABQualityInformation', 'Released', NULL, '市场AB质量信息快报管理', '管理市场AB质量信息快报', 'Client/DCS/Images/Menu/MarketABQualityInformation.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8210, 8200, 2, 'MarketABQualityInformationCountersign', 'Released', NULL, '市场AB质量信息快报会签', '会签市场AB质量信息快报', 'Client/DCS/Images/Menu/MarketABQualityInformation.png', 10, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8300,8000, 1, 'ClaimsBusinessForHeadquarters', NULL, NULL, '索赔单管理', NULL, NULL, 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8301,8300, 2, 'RepairClaimBill', 'Released', NULL, '维修索赔管理', '审核三包维修索赔单', 'Client/DCS/Images/Menu/Service/RepairClaimBill.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8302,8300, 2, 'MaintainClaimBill', 'Released', NULL, '保养索赔管理', '审核保养索赔单', 'Client/DCS/Images/Menu/Service/MaintainClaimBill.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8303,8300, 2, 'ServiceTripClaimBill', 'Released', NULL, '外出服务索赔管理', '审核外出索赔单', 'Client/DCS/Images/Menu/Service/ServiceTripClaimBill.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8304,8300, 2, 'ServicesClaimBill', 'Released', NULL, '服务活动索赔管理', '审核服务活动索赔单', 'Client/DCS/Images/Menu/Service/ServicesClaimBill.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8305,8300, 2, 'PartsClaimOrder', 'Released', NULL, '配件索赔管理', '审核非三包配件索赔单', 'Client/DCS/Images/Menu/Service/PartsClaimOrder.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8306, 8300, 2, 'ExpenseAdjustmentBill', 'Released', NULL, '扣补款管理', '查询和维护扣补款', 'Client/DCS/Images/Menu/Service/ExpenseAdjustmentBill.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8316, 8300, 2, 'OutofWarrantyPayment', 'Released', NULL, '保外扣补款管理', '查询和维护保外扣补款', 'Client/DCS/Images/Menu/Service/ExpenseAdjustmentBill.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8317,8300, 2, 'FDWarrantyMessageMng', 'Released', NULL, '福戴三包信息管理', '查询、维护福戴三包信息', 'Client/DCS/Images/Menu/PartsSales/FDWarrantyMessageMng.png', 16, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8307,8300, 2, 'ServiceTripClaimBillForSupplier', 'Released', NULL, '外出服务索赔管理-供应商', '供应商复核，确认外出索赔单', 'Client/DCS/Images/Menu/Service/ServiceTripClaimBill.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8311,8300, 2, 'RepairClaimBillForSupplier', 'Released', NULL, '维修索赔管理-供应商', '供应商复核，确认维修索赔单', 'Client/DCS/Images/Menu/Service/RepairClaimBill.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8211, 8300, 2, 'PartsClaimOrderNew', 'Released', NULL, '配件索赔管理（新）', '分公司审核配件索赔单', 'Client/DCS/Images/Menu/Service/PartsClaimOrder.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8212, 8300, 2, 'PartsClaimOrderNewForAgency', 'Released', NULL, '配件索赔管理-代理库', '代理库发运配件索赔单', 'Client/DCS/Images/Menu/Service/PartsClaimOrder.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8312,8300, 2, 'PartsClaimOrderDealerInformation',  'Released', NULL, '配件索赔供应商信息查询', NULL, 'Client/DCS/Images/Menu/Service/RepairClaimBill.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8313,8300, 2, 'IntegralClaimBill',  'Released', NULL, '保外索赔管理', NULL, 'Client/DCS/Images/Menu/Common/IntegralClaimBill.png', 11, 2);
--8300下的节点下一个id为8312

--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8309, 8300, 2, 'SupplierExpenseAdjustBill', 'Released', NULL, '供应商扣补款管理', '', 'Client/DCS/Images/Menu/Service/SupplierBuckleFillingMoney.png',9, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8400, 8000, 1, 'RepairServiceForDealer', 'Released', NULL, '维修单管理-服务站', NULL, NULL, 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8402, 8400, 2, 'RepairOrderForDealer', 'Released', NULL, '保内维修单管理-服务站', '管理维修单信息', 'Client/DCS/Images/Menu/Service/RepairOrderForDealer.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8403, 8400, 2, 'RepairOrderForWarranty', 'Released', NULL, '保外维修单管理-服务站', '服务站填制、修改、维修完工保外维修单', 'Client/DCS/Images/Menu/Service/RepairOrderForWarranty.png', 3, 2); 
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8404, 8400, 2, 'RepairOrderForSecond', 'Released', NULL, '二级站维修单管理', '管理二级站提报维修信息', 'Client/DCS/Images/Menu/Service/TwoGradeRepairBook.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8405, 8400, 2, 'SalesBeyoundCheckForDealer', 'Released', NULL, '售前检查管理-服务站', '提报、修改、提交售前检查单', 'Client/DCS/Images/Menu/Service/SalesBeyoundCheckForDealer.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8406, 8400, 2, 'RepairWorkOrderForDealer', 'Released', NULL, '维修工单管理-服务站', '提交维修工单生成维修单', 'Client/DCS/Images/Menu/Service/RepairWorkOrderForDealer.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8407, 8400, 2, 'DTMRepairContract', 'Released', NULL, 'DTM上传维修单管理-服务站', '', 'Client/DCS/Images/Menu/Service/DTMRepairContract.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8408, 8400, 2, 'FaultyPartsSupplierAssembly', 'Released', NULL, '祸首件所属总成维护', '维护祸首件所属总成信息', 'Client/DCS/Images/Menu/Service/FaultyPartsSupplierAssembly.png', 8, 2);


INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8500, 8000, 1, 'RepairApplyForDealer', 'Released', NULL, '申请单管理-服务站', NULL, NULL, 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8501, 8500, 2, 'RepairClaimApplicationForDealer', 'Released', NULL, '维修索赔申请管理-服务站', '填制、修改、提交三包维修索赔预申请单', 'Client/DCS/Images/Menu/Service/RepairClaimApplication.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8502,8500, 2, 'MaintainClaimApplicationForDealer', 'Released', NULL, '保养索赔申请管理-服务站', '填制、修改、提交保养索赔预申请单', 'Client/DCS/Images/Menu/Service/MaintainClaimApplication.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8503,8500, 2, 'ServiceTripClaimApplicationForDealer', 'Released', NULL, '外出索赔申请管理-服务站', '填制、修改、提交外出索赔申请单', 'Client/DCS/Images/Menu/Service/ServiceTripClaimApplication.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8504,8500, 2, 'ServicesClaimApplicationForDealer', 'Released', NULL, '服务活动索赔申请管理-服务站', '填制、修改、提交服务活动索赔预申请单', 'Client/DCS/Images/Menu/Service/ServicesClaimApplication.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8505,8500, 2, 'PartsClaimApplicationForDealer', 'Released', NULL, '配件索赔申请管理-服务站', '填制、修改、提交非三包配件索赔预申请单', 'Client/DCS/Images/Menu/Service/ServicesClaimBill.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8506,8500, 2, 'MarketABQualityInformationForDealer', 'Released', NULL, '市场AB质量信息快报管理-服务站', '提报、修改、提交市场AB质量信息快报', 'Client/DCS/Images/Menu/MarketABQualityInformation.png', 5, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8600,8000, 1, 'ClaimsBusinessForServiceStation', NULL, NULL, '索赔单管理-服务站', NULL, NULL, 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8602,8600, 2, 'RepairClaimBillForDealer', 'Released', NULL, '维修索赔管理-服务站', '查询维修索赔单', 'Client/DCS/Images/Menu/Service/RepairClaimBill.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8601,8600, 2, 'ServiceTripClaimBillForDealer', 'Released', NULL, '外出服务索赔管理-服务站', '填制、修改、提交外出索赔单', 'Client/DCS/Images/Menu/Service/ServiceTripClaimBill.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8605,8600, 2, 'PartsClaimOrderForDealer', 'Released', NULL, '配件索赔管理-服务站', '填制、修改、提交非三包配件索赔单', 'Client/DCS/Images/Menu/Service/PartsClaimOrder.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8603,8600, 2, 'MaintainClaimBillForDealer', 'Released', NULL, '保养索赔管理-服务站', '填制、修改、提交保养索赔单', 'Client/DCS/Images/Menu/Service/MaintainClaimBill.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8604,8600, 2, 'ServicesClaimBillForDealer', 'Released', NULL, '服务活动索赔管理-服务站', '填制、修改、提交服务活动索赔单', 'Client/DCS/Images/Menu/Service/ServicesClaimBill.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8308, 8600, 2, 'ExpenseAdjustmentBillForSearch', 'Released', NULL, '保内扣补款查询', '查询保内扣补款', 'Client/DCS/Images/Menu/Service/ExpenseAdjustmentBill.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8314, 8600, 2, 'OutofWarrantyPaymentForSearch', 'Released', NULL, '保外扣补款查询', '查询保外扣补款', 'Client/DCS/Images/Menu/Service/ExpenseAdjustmentBill.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8606, 8600, 2, 'PartsClaimOrderNewForDealer', 'Released', NULL, '配件索赔单提报', '服务站、代理库提报配件索赔单', 'Client/DCS/Images/Menu/Service/PartsClaimOrder.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8609, 8600, 2, 'IntegralClaimBillForDealer', 'Released', NULL, '保外索赔单查询', '查询保外索赔单', 'Client/DCS/Images/Menu/Common/IntegralClaimBill.png', 8, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8700, 8000, 1, 'ServiceActivitiesManagement',NULL, NULL, '服务活动管理', NULL, NULL, 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8701, 8700, 2, 'ServiceActivity', 'Released', NULL, '服务活动管理', '定义召回、升级等服务活动', 'Client/DCS/Images/Menu/ServiceBasicData/ServiceActivity.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8702, 8700, 2 ,'RedPacketsMsg','Released', NULL, '红包管理','查询红包信息','Client/DCS/Images/Menu/Common/RedPacketsMsg.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8703,8700, 2, 'FundRedPostVehicleMsg', 'Released', NULL, '基金红包活动车辆管理', '', 'Client/DCS/Images/Menu/Common/RedPacketsMsg.png', 3, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8800, 8000, 1 ,'ServiceStationClaimSettlement',NULL, NULL, '服务站索赔结算', NULL, NULL, 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8801, 8800, 2, 'SsClaimSettlementBill', 'Released', NULL, '服务站索赔结算管理', '分公司查询、维护服务站索赔结算单', 'Client/DCS/Images/Menu/Service/SsClaimSettlement.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8802,8800, 2, 'SsClaimSettlementBillQuery', 'Released', NULL, '服务站保内索赔结算管理-服务站', '服务站查询保内索赔结算单并且登记发票', 'Client/DCS/Images/Menu/Service/SsClaimSettlementQuery.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8803,8800, 2, 'SsClaimSettleInstruction', 'Released', NULL, '服务站索赔结算指令管理', '服务站维护自动生成索赔结算单的规则', 'Client/DCS/Images/Menu/Service/SsClaimSettleInstruction.png', 3, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8804, 8800, 2 ,'ServiceEngineerClaimSettlement','Released', NULL, '服务工程师索赔结算','统计服务工程师维修工时及外出服务费', 'Client/DCS/Images/Menu/Service/ServiceEngineerClaimSettlement.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8805, 8800, 2 ,'DealerInvoiceInformation','Released', NULL, '服务站发票管理','供服务站及分公司管理服务站发票', 'Client/DCS/Images/Menu/Service/DealerInvoiceInformation.png', 5, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8806, 8800, 2, 'SupplierClaimSettleBill', 'Released', NULL, '供应商索赔结算管理', '生成、修改、审批供应商索赔结算单', 'Client/DCS/Images/Menu/Service/SupplierClaimSettle.png', 6, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8807,8800, 2, 'SupplierClaimSettleBillQuery', 'Released', NULL, '供应商索赔结算管理-供应商', '供应商查询供应商索赔结算单', 'Client/DCS/Images/Menu/Service/SupplierClaimSettleForSupplier.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8808,8800, 2, 'IntegralClaimBillSettle',  'Released', NULL, '保外索赔结算管理', NULL, 'Client/DCS/Images/Menu/Common/IntegralClaimBillSettle.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8809,8800, 2, 'IntegralSettleDictate', 'Released', NULL, '服务站保外索赔结算指令管理', '服务站维护自动生成保外索赔结算单的规则', 'Client/DCS/Images/Menu/Service/SsClaimSettleInstruction.png', 9, 2);
--******下一个节点ID为8808。sequence为6********
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8810,8800, 2, 'IntegralClaimInvoiceInformation', 'Released', NULL, '保外发票管理', '供服务站及分公司管理服务站保外发票', 'Client/DCS/Images/Menu/Service/DealerInvoiceInformation.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8811,8800, 2, 'SsIntegralClaimBillSettleQuery', 'Released', NULL, '服务站保外索赔结算管理-服务站', '服务站查询保外索赔结算单并且登记发票', 'Client/DCS/Images/Menu/Service/SsClaimSettlementQuery.png', 2, 2);
--******下一个节点ID为88012。sequence为8********


INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8900, 8000, 1 ,'SupplierClaimSettlement',NULL, NULL, '供应商索赔结算',NULL, NULL, 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8806, 8900, 2, 'SupplierClaimSettleBill', 'Released', NULL, '供应商索赔结算管理', '生成、修改、审批供应商索赔结算单', 'Client/DCS/Images/Menu/Service/SupplierClaimSettle.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8807,8900, 2, 'SupplierClaimSettleBillQuery', 'Released', NULL, '供应商索赔结算管理-供应商', '供应商查询供应商索赔结算单', 'Client/DCS/Images/Menu/Service/SupplierClaimSettleForSupplier.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8901, 8900, 2 ,'SupplierClaimSettlementQuery','Released', NULL, '供应商索赔结算查询','统计供应商索赔费用', 'Client/DCS/Images/Menu/Service/SupplierClaimSettlementQuery.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8309, 8900, 2, 'SupplierExpenseAdjustBill', 'Released', NULL, '供应商扣补款管理', '', 'Client/DCS/Images/Menu/Service/SupplierBuckleFillingMoney.png',4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8310, 8900, 2 ,'SupClaimSettleInstruction','Released', NULL, '供应商索赔结算指令管理','用于分公司维护、查看供应商索赔结算指令', 'Client/DCS/Images/Menu/Service/SupClaimSettleInstruction.png', 5, 2);
--******下一个节点ID为8902。sequence为4********

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (15100,8000, 1, 'EvaluationDetails', NULL, NULL, '客户服务评价管理', NULL, NULL, 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (15101,15100, 2, 'CustomerServiceEvaluate', 'Released', NULL, '客户服务评价明细管理', '查询和批量导入客户服务评价明细', 'Client/DCS/Images/Menu/Service/CustomerServiceEvaluate.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (15102,15100, 2, 'CustomerServiceEvaluateQuery', 'Released', NULL, '客户服务评价统计', '客户服务评价统计', 'Client/DCS/Images/Menu/Service/CustomerServiceEvaluateQuery.png', 2, 2);

--延保业务管理
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8110, 8000, 1, 'ExtendedWarranty', 'Released', NULL, '延保业务管理', NULL, NULL, 10, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8111, 8110, 2, 'EWProductMaintenance', 'Released', NULL, '延保产品维护', '延保产品维护', 'Client/Dcs/Images/Menu/ExtendedWarranty/EWProductMaintenance.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8112, 8110, 2, 'EWTypeMaintenance', 'Released', NULL, '延保折扣类型维护', '延保折扣类型维护', 'Client/Dcs/Images/Menu/ExtendedWarranty/EWTypeMaintenance.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8113, 8110, 2, 'EWOrder', 'Released', NULL, '延保订单管理', '延保订单管理', 'Client/Dcs/Images/Menu/ExtendedWarranty/EWOrder.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8114, 8110, 2, 'EWOrderSubmission', 'Released', NULL, '延保订单提报', '延保订单提报', 'Client/Dcs/Images/Menu/ExtendedWarranty/EWOrderSubmission.png', 1, 2);
--旧件业务管理

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9000, 0, 0, 'OldParts', NULL, NULL, '旧件业务管理', NULL, 'Client/Images/ShellView/OldParts-s.png', 11, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9100,9000, 1, 'UsedPartsTreatmentForServiceStation', NULL, NULL, '市场旧件处理-服务站', NULL, NULL, 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9101,9100, 2, 'UsedPartsShippingOrder', 'Released', NULL, '旧件发运管理', '服务站查询、维护旧件返回本部的发运信息', 'Client/DCS/Images/Menu/Service/UsedPartsShippingOrder.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9102,9100, 2, 'SsUsedPartsStorageQuery', 'Released', NULL, '服务站旧件库存查询', '查询服务站旧件库存', 'Client/DCS/Images/Menu/Service/SsUsedPartsStorageQuery.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9103,9100, 2, 'SsUsedPartsDisposalBillForDealer', 'Released', NULL, '服务站旧件处理管理', '查询、维护或审批服务站旧件的处理单', 'Client/DCS/Images/Menu/Service/SsUsedPartsDisposalBill2.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9200,9000, 1, 'UsedPartsTreatmentForBranch', NULL, NULL, '市场旧件处理-分公司', NULL, NULL, 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9201,9200, 2, 'UsedPartsShippingOrderForConfirm', 'Released', NULL, '旧件发运确认管理', '厂家确认市场返回的旧件发运单', 'Client/DCS/Images/Menu/Service/UsedPartsShippingOrderConfirm.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9202,9200, 2, 'UsedPartsLogisticLossBill', 'Released', NULL, '旧件物流差异管理', '查询、编辑旧件物流损失单', 'Client/DCS/Images/Menu/Service/UsedPartsLogisticLossBill.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9203,9200, 2, 'SsUsedPartsStorage', 'Released', NULL, '服务站旧件库存管理', '查询或修改服务站旧件库存', 'Client/DCS/Images/Menu/Service/SsUsedPartsStorage.png',3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9204,9200, 2, 'UsedPartsReturnPolicyHistoryQuery', 'Released', NULL, '旧件是否返回变更履历查询', '查询旧件返回政策变更履历', 'Client/DCS/Images/Menu/Service/UsedPartsReturnPolicyHistoryQuery.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9205,9200, 2, 'SsUsedPartsDisposalBillForBranch', 'Released', NULL, '服务站旧件处理管理-分公司', '查询或审批服务站的旧件处理单', 'Client/DCS/Images/Menu/Service/SsUsedPartsDisposalBill.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9206,9200, 2, 'UsedPartsStockForEdit', 'Released', NULL, '修改祸首件供应商', '修改旧件条码对应的祸首件供应商', 'Client/DCS/Images/Menu/Service/UsedPartsStockForEdit.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9300,9000, 1, 'UsedWarehouseStructure', NULL, NULL, '旧件仓库', NULL, NULL, 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9301,9300, 2, 'UsedPartsWarehouse', 'Released', NULL, '旧件仓库管理', '维护旧件仓库的基本信息，人员关系', 'Client/DCS/Images/Menu/Service/UsedPartsWarehouse.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9302,9300, 2, 'UsedPartsWarehouseArea', 'Released', NULL, '旧件库区库位管理', '规划旧件仓库结构，设置旧件仓库库区库段库位信息，库段负责人', 'Client/DCS/Images/Menu/Service/UsedPartsWarehouseArea.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9400, 9000, 1, 'OldPartsStock', NULL, NULL, '旧件库存', NULL, NULL, 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9401,9400, 2, 'UsedPartsStock', 'Released', NULL, '旧件库存查询', '分公司查询本部旧件库存', 'Client/DCS/Images/Menu/Service/UsedPartsStock.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9500, 9000, 1, 'UsedPartsInbound', NULL, NULL, '旧件入库业务','', '', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9501,9500, 2, 'UsedPartsInboundOrder', 'Released', NULL, '旧件入库', '分公司新增旧件入库单', 'Client/DCS/Images/Menu/Service/UsedPartsInboundOrder.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9502,9500, 2, 'UsedPartsInboundOrderQuery', 'Released', NULL, '旧件入库单查询', '分公司查询旧件入库单', 'Client/DCS/Images/Menu/Service/UsedPartsInboundOrderQuery.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9600, 9000, 1, 'UsedPartsOutbound', NULL, NULL, '旧件出库业务', NULL, NULL, 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9601,9600, 2, 'UsedPartsOutboundOrder', 'Released', NULL, '旧件出库', '本部新增旧件出库单', 'Client/DCS/Images/Menu/Service/UsedPartsOutboundOrder.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9602,9600, 2, 'UsedPartsOutboundOrderQuery', 'Released', NULL, '旧件出库单查询', '本部查询旧件出库单', 'Client/DCS/Images/Menu/Service/UsedPartsOutboundOrderQuery.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9700,9000, 1, 'UsedWarehouseExecution', NULL, NULL, '旧件库内业务', NULL, NULL, 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9701,9700, 2, 'UsedPartsTransferOrder', 'Released', NULL, '旧件调拨管理', '新增、审批旧件调拨', 'Client/DCS/Images/Menu/Service/UsedPartsTransferOrder.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9702,9700, 2, 'UsedPartsShiftOrder', 'Released', NULL, '旧件移库管理', '新增旧件移库单实现移库', 'Client/DCS/Images/Menu/Service/UsedPartsShiftOrder.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9703,9700, 2, 'UsedPartsDisposalBill', 'Released', NULL, '旧件残值处理管理', '新增、审批旧件库内旧件处理', 'Client/DCS/Images/Menu/Service/UsedPartsDisposalBill.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9704,9700, 2, 'UsedPartsLoanBill', 'Released', NULL, '旧件借用管理', '新增、审核库内旧件的外借', 'Client/DCS/Images/Menu/Service/UsedPartsLoanBill.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9705,9700, 2, 'UsedPartsReturnOrder', 'Released', NULL, '旧件清退管理', '新增、修改、审批、作废旧件清退单', 'Client/DCS/Images/Menu/Service/UsedPartsReturnOrder.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9800, 9000, 1, 'UsedPartsTransFee', NULL, NULL, '旧件运费','', '', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9801, 9800, 2, 'UsedPartsDistanceInfor', 'Released', NULL, '旧件仓储运距管理', '', 'Client/DCS/Images/Menu/Service/UsedPartsWarehouseDistanceManagement.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (9802, 9800, 2, 'UsedPartsTransFeeManagement', 'Released', NULL, '旧件运费管理', '查询和维护旧件运费', 'Client/DCS/Images/Menu/Service/UsedPartsTransFeeManagement.png', 2, 2);

--统计分析

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (10000, 0, 0, 'Statistics', NULL, NULL, '统计分析', NULL, 'Client/Images/ShellView/Statistics-s.png', 12, 2);

--待办事项

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (11000, 0, 0,  'Todo', NULL, NULL, '待办事项', NULL, 'Client/Images/ShellView/Todo-s.png', 13, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (11100,11000, 1, 'TodoList', NULL, NULL, '待办事项', NULL, NULL, 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (11101,11100, 2, 'NotificationToShow', 'Warning', '/notificationinfo/notification/notificationquery', '今日公告', '公告查看', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 1, 2);

--公告

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (12000, 0, 0, 'NotificationInfo', NULL, NULL, '公告管理', NULL, 'Client/Images/ShellView/Notification-s.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (12100,12000, 1, 'Notification', NULL, NULL, '公告管理', NULL, NULL, 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (12101,12100, 2, 'Notification', 'Released', NULL, '公告管理', '公告发布、管理', 'Client/DCS/Images/Menu/Channels/Notification.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (12102,12100, 2, 'NotificationQuery', 'Released', NULL, '公告查询', '公告查看', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 2, 2);
--报表

--配件
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13000, 0, 0,  'Report', NULL, NULL, '报表', NULL, 'Client/Images/ShellView/Todo-s.png', 14, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13100,13000, 1, 'SparePart', NULL, NULL, '配件', NULL, NULL, 0, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13101,13100, 2, 'InboundHistory', 'Released', NULL, '历史入库记录查询', '查看历史入库记录报表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 0, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13102,13100, 2, 'OutboundHistory', 'Released', NULL, '历史出库记录查询', '查看历史出库记录报表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 1, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13103,13100, 2, 'PartsPriceHistory', 'Released', NULL, '配件价格查询', '查看配件价格报表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 2, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13104,13100, 2, 'DealerBranch', 'Released', NULL, '服务站分公司信息查询', '查看服务站分公司信息报表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 3, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13105,13100, 2, 'DealerInformation', 'Released', NULL, '服务站基本信息查询', '查看服务站基本信息报表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 4, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13106,13100, 2, 'AgencyStock', 'Released', NULL, '代理库库存查询', '代理库库存查询报表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 5, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13107,13100, 2, 'PartsStockDetail', 'Released', NULL, '配件商混设备配件库库存明细查询', '配件商混设备配件库库存明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 6, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13108,13100, 2, 'PartsSaleOrderSatisfiedRate', 'Released', NULL, '配件订单满足率统计', '查询某时间段订单品种、数量、金额满足情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 7, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13109,13100, 2, 'PartsStockDetail', 'Released', NULL, '配件库存明细查询', '查询配件库存以及调拨在途情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 8, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13110,13100, 2, 'PartsStockAge', 'Released', NULL, '库龄统计', '查询一段时期内呆滞库存量', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 9, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13112,13100, 2, 'PartsSalesOnway', 'Released', NULL, '配件销售在途查询', '汇总查询未收货确认配件', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 10, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13113,13100, 2, 'DealerPartsStockStat', 'Released', NULL, '服务站库存查询', '实时查询服务站配件库存明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 11, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13125,13100, 2, 'DealerToAgentOrderStat', 'Released', NULL, '服务站向代理库销售订单查询', '查询服务站向代理库提报订单信息', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 12, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13126,13100, 2, 'NeedPurchaseDetail', 'Released', NULL, '采购需求报表', '查询配件预估库存量、缺货量，以及一段时间出库量', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 13, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13128,13100, 2, 'PartsUrgPercentStat', 'Released', NULL, '应急比例报表', '服务站应急比例汇总表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 13, 2);


--****配件下的脚本下一个	ID为 13137 ******** （ID13200的已定义。--为服务报表根节点）
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13137,13100, 2, 'PeriodSupplyAmount', 'Released', NULL, '期间供货额', '统计某一时间段内某个仓库销售出库、采购入库的配件明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 14, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13138,13100, 2, 'PurchasePriceFailureAMonth', 'Released', NULL, '采购价格失效前一个月统计', '统计截至目前之后30天将失效的采购价格配件明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 15, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13139,13100, 2, 'AgentsCreditQuotaQuery', 'Released', NULL, '代理库授信额度统计报表', '统计某一时间段内某一代理库给予服务站的授信金额明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 16, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13140,13100, 2, 'AgentsHistoryInQuery', 'Released', NULL, '代理库入库记录查询', '统计某一时间段内代理库入库明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 17, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13141,13100, 2, 'AgentsHistoryOutQuery', 'Released', NULL, '代理库出库记录查询', '统计某一时间段内代理库出库明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 18, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13142,13100, 2, 'PurchasePlaseRate', 'Released', NULL, '采购到位率', '统计某一时间段内某一仓库采购某一供应商的采购到位率', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 19, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13143,13100, 2, 'OutRelationalQuery', 'Released', NULL, '出库单据关联单查询', '根据单号查询出库业务的上下延单据信息', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 20, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13144,13100, 2, 'InRelationalQuery',  'Released', NULL, '入库单据关联单查询', '根据单号查询入库业务的上下延单据信息', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 21, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13145,13100, 2, 'CentralizedPurchaseSta',  'Released', NULL, '统购品牌采购需求报表', '查询统购品牌采购需求信息', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 22, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13146,13100, 2, 'PartsOrderFillRate',  'Released', NULL, '配件订单满足率统计（营销）', '统计营销PMS系统订单满足率', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 23, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13147,13100, 2, 'AgentShippingDetailSta',  'Released', NULL, '代理库发运单明细', '查询代理库向服务站发运配件的到位及时性', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 24, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13148,13100, 2, 'AgentShippingSta',  'Released', NULL, '代理库发运清单', '查询代理库向服务站发运配件的到位及时性', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 25, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13149,13100, 2, 'PartsOrderConfirmGapSta',  'Released', NULL, '订单审核缺口统计报表', '统计各品牌配件销售订单审核缺口情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 26, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13150,13100, 2, 'OverseaPartsOutboundSta', 'Released', NULL, '海外品牌配件出库统计表', '海外乘用车品牌、海外轻卡品牌、海外中重卡品牌配件出库单出库单明细查询', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 27, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13151,13100, 2, 'PartsPurchaseSalesSta', 'Released', NULL, '配件采购及销售情况汇总', '监控配件采购入库情况，为储备管理提供依据', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 15, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13152,13100, 2, 'AllBrandStockDetailSta', 'Released', NULL, '各品牌超X月库存明细表', '分析CDC库存情况，为储备管理提供依据', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 16, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13153,13100, 2, 'DealerSparePartRetailDetailSta', 'Released', NULL, '服务站配件零售清单', '查询服务站配件零售清单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 28, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13154,13100, 2, 'DealerSparePartTurnoverStaY', 'Released', NULL, '服务站配件周转情况汇总（分品牌）', '查询服务站配件周转情况汇总（分品牌）', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 29, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13155,13100, 2, 'DealerSparePartTurnoverStaN', 'Released', NULL, '服务站配件周转情况汇总（不分品牌）', '查询服务站配件周转情况汇总（不分品牌）', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 30, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13156,13100, 2, 'DealerSparePartInWHSta', 'Released', NULL, '服务站配件入库汇总', '查询服务站配件入库情况汇总', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 31, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13157,13100, 2, 'DealerSparePartOutWHSta', 'Released', NULL, '服务站配件出库汇总', '查询服务站配件出库情况汇总', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 32, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13158,13100, 2, 'AgencySparePartRetailDetailSta', 'Released', NULL, '代理库配件零售清单', '查询代理库配件零售清单', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 33, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13159,13100, 2, 'PartsSalesMonthlySta', 'Released', NULL, '配件销售月报表（明细表）', '汇总总部发给服务商配件销量明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 34, 2);

--服务
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13200,13000, 1, 'Service', NULL, NULL, '服务', NULL, NULL, 22, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13136,13200, 2, 'QualifyInefficientNetworkAnalysisReport', 'Released', NULL, '保外低效网络分析报表', '根据网络数量及维修量，查询各品牌低效网络数量（零维修量、月均不足10台）', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13137,13200, 2, 'InefficientNetworkAnalysisReport', 'Released', NULL, '低效网络分析报表', '根据网络数量及维修量，查询各品牌低效网络数量（零维修量、月均不足10台', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13138,13200, 2, 'DealerCustomerChurnRateSta', 'Released', NULL, '服务站客户流失率分析表', '本报表是对服务站客户流失率进行统计', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 11, 2);
--****服务下的脚本下一个ID为13203*****
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13216,13200, 2, 'DealerRepairDetailSta', 'Released', NULL, '服务站维修明细', '查询服务站维修明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 22, 2);
--服务商基本信息管理
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13400,13000, 1, 'ServiceInfo', NULL, NULL, '服务-服务商基本信息管理', NULL, NULL, 23, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13224,13400, 2, 'StarStatistics', 'Released', NULL, '星级统计报表', '查询区域内符合某项指标的服务商的数量和分布', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13226,13400, 2, 'StatisticalReportsOnTheServiceType', 'Released', NULL, '服务站类型统计报表', '查询区域内符合某项指标的服务商的数量和分布', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13220,13400, 2, 'RegisteredCapitalSta', 'Released', NULL, '注册资金统计报表', '根据服务网络基本信息中注册资金档案信息及分品牌信息中业务能力对特约服务站注册资金分区段进行统计', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13225,13400, 2, 'StatisticalReportsMainQuali', 'Released', NULL, '维修资质统计报表', '查询区域内符合某项指标的服务商的数量和分布', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 3, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (13104, 13400, 2, 'DealerBranch', 'Released', '', '服务站分公司信息查询', '', '', '查看服务站分公司信息报表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 4, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (13105, 13400, 2, 'DealerInformation', 'Released', '', '服务站基本信息查询', '', '', '查看服务站基本信息报表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 5, 2);

--服务-服务商业务能力管理
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13800,13000, 1, 'ServiceBusinessManagement', NULL, NULL, '服务-服务商业务能力管理', NULL, NULL, 23, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13227,13800, 2, 'BusinessAbilitySta', 'Released', NULL, '业务能力统计报表', '查询区域内符合某项指标的服务商的数量和分布', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13228,13800, 2, 'StarOutSerQualitySta', 'Released', NULL, '星级站保外服务量分析报表', '根据各品牌星级站数量及维修量分析某个时间段内星级站的服务集中度', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13229,13800, 2, 'StarInSerQualitySta', 'Released', NULL, '星级站保内服务量分析报表', '根据各品牌星级站数量及维修量分析某个时间段内星级站的服务集中度', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13218,13800, 2, 'DealerRepairOutInfoSta', 'Released', NULL, '保内保外服务量统计分析报表', '对各品牌服务站提报保内、保外维修信息数量进行统计，并计算出保外信息占保内信息比', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13221,13800, 2, 'QualifyInefficientNetworkAnalysisReport', 'Released', NULL, '保外低效网络分析报表', '根据网络数量及维修量，查询各品牌低效网络数量（零维修量、月均不足10台）', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13222,13800, 2, 'InefficientNetworkAnalysisReport', 'Released', NULL, '保内低效网络分析报表', '根据网络数量及维修量，查询各品牌低效网络数量（零维修量、月均不足10台', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 11, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13223,13800, 2, 'DealerCustomerChurnRateSta', 'Released', NULL, '服务站客户流失率分析表', '本报表是对服务站客户流失率进行统计', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 12, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13217,13800, 2, 'DealerClaimSalesRateSta', 'Released', NULL, '服务站保外保内销量比', '反映各个服务站的配件保内保外比情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 13, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13131,13800, 2, 'RepairAmountStat', 'Released', NULL, '维修量统计报表', '统计一段时间内服务站强保索赔、维修索赔数量', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 14, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13232,13800, 2, 'CoverageSta', 'Released', NULL, '覆盖率统计报表', '根据服务网络所在省、市、区（县）及城市级别划分，统计各级城市服务网络覆盖率', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 15, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13233,13800, 2, 'CustomerOutServeLoyalSta', 'Released', NULL, '保外客户售后服务忠诚度分析表', '本报表是对各品牌提报的保外信息进行统计，按月度分析服务网络忠诚客户情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 16, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13234,13800, 2, 'CustomerChurnRateSta', 'Released', NULL, '客户流失率分析表', '本报表是对服务站客户流失率进行统计', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 17, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13236,13800, 2, 'OutPurchasePartRateSta', 'Released', NULL, '外购配件占比', '外购配件占比', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 18, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13801,13800, 2, 'DealerCoincidenceSta', 'Released', NULL, '重合率统计报表', '根据各品牌服务网络名称或服务站编号统计各品牌服务网络重合建设情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 19, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13802,13800, 2, 'DealerCoincidenceDetailSta', 'Released', NULL, '重合率统计明细报表', '根据各品牌服务网络名称或服务站编号统计各品牌服务网络重合建设情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 19, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13803,13800, 2, 'RepairQuantityAnalysisSta', 'Released', NULL, '服务量分析报表', '根据各品牌服务网络数量及维修量分析某个时间段内服务网络的服务集中度', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 20, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13804,13800, 2, 'RepairWorkOrderMDReport', 'Released', NULL, '互动中心派工单汇总表', '统计互动中心派工单处理时长情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 21, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13805,13800, 2, 'RepairWorkOrderMDReportDtl', 'Released', NULL, '互动中心派工单明细表', '统计互动中心派工单处理时长情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 22, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13806,13800, 2, 'RepairQuantityAnalysisSta1', 'Released', NULL, '服务商会员招募信息查询报表', '根据服务网络会员招募情况及会员维修情况统计会员信息', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 23, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13807,13800, 2, 'RepairQuantityAnalysisSta2', 'Released', NULL, '服务商会员交易明细查询报表', '根据服务网络会员维修交易情况统计会员维修频次、消费习惯等', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 24, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13808,13800, 2, 'AuthenticationRepair', 'Released', NULL, '服务站人员培训认证与维修关联报表', '通过维修索赔单，统计出维修工的维修项目信息及认证培训信息', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 25, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13809,13800, 2, 'NotificationReport', 'Released', NULL, '公告查看信息（汇总表）', '查看公告情况汇总', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 26, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13810,13800, 2, 'NotificationReportDetail', 'Released', NULL, '公告查看信息（明细表）', '查看公告记录明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 27, 2);


--服务故障信息管理
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13500,13000, 1, 'FaultInfoManagement', NULL, NULL, '服务-故障信息管理', NULL, NULL, 24, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13203,13500, 2, 'PartsFailureRateDetails', 'Released', NULL, '配件故障率(配件明细)','统计某一时间段内某一供应商维修旧件明细的故障率', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13204,13500, 2, 'PartsFailureRate', 'Released', NULL, '配件故障率', '统计某一时间段内某一供应商维修旧件的故障率', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13208,13500, 2, 'SupplierFailureItemSta', 'Released', NULL, '各供应商故障项次统计', '统计某一时间段内某一供应商发生的故障项次、台次', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13209,13500, 2, 'FailureModeFailureItemSta', 'Released', NULL, '按故障模式统计故障项次统计', '统计某一时间段内某一故障原因的故障项次、台次', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13210,13500, 2, 'SeriesFailureItemSta', 'Released', NULL, '各系列故障分布统计', '统计某一时间段内各系列故障项次、台次，可以按车型平台、产品类别、驱动形式、子品牌、整车编号分类统计', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13501,13500, 2, 'FaultPartRecycleRateSta', 'Released', NULL, '故障件回收率', '故障件回收率', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 4, 2);

--服务-旧件管理
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13600,13000, 1, 'OldPartManagement', NULL, NULL, '服务-旧件管理', NULL, NULL, 25, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13214,13600, 2, 'UsedPartsRecoverSta', 'Released', NULL, '旧件回收统计表', '汇总各个分公司各市场部所属服务站旧件返回统计表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13215,13600, 2, 'UsedPartsDealerSta', 'Released', NULL, '旧件服务站回收情况统计表', '汇总各个品牌各个服务站某个时间段旧件回收情况统计表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13135,13600, 2, 'UsedPartsTransferOrderStop', 'Released', NULL, '旧件调拨终止查询', '统计旧件调拨终止后，未入库未出库的旧件明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13212,13600, 2, 'UsedPartsInboundSta', 'Released', NULL, '旧件接收入库统计', '汇总旧件仓库某一时间段接收入库的旧件明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13213,13600, 2, 'UsedPartsReturnSta', 'Released', NULL, '旧件清退出库统计', '汇总某一个时间段旧件仓库清退出库旧件明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 4, 2);
--服务-维修索赔管理
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13700,13000, 1, 'RepairClaimManagement', NULL, NULL, '服务-维修索赔管理', NULL, NULL, 26, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13219,13700, 2, 'RepairCheckTime', 'Released', NULL, '维修审批时长统计', '监控各事业本部对于服务站提报申请单据的审批时长', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13132,13700, 2, 'RepairCarFeeStat', 'Released', NULL, '保修台车费用统计', '统计搅拌车、泵车保修台次，保修费用', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13133,13700, 2, 'ClaimBillAccurate', 'Released', NULL, '索赔信息提报准确率', '服务站索赔单作废量、提报量、提报准确率', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13134,13700, 2, 'SupplierCarFee', 'Released', NULL, '供应商单台费用', '配件索赔金额，平均索赔金额统计', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13201,13700, 2, 'ClaimDepartment', 'Released', NULL, '索赔单事业部综合查询', '查看索赔单事业部综合报表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13111,13700, 2, 'ClaimCacuStat', 'Released', NULL, '索赔结算报表', '查询维修索赔单、索赔金额、外出金额等信息', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13127,13700, 2, 'RepairClaimAppStat', 'Released', NULL, '维修索赔申请报表', '查询维修索赔申请单及维修材料', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13205,13700, 2, 'RepairDetailsQuery', 'Released', NULL, '维修单综合查询报表(明细)', '统计某一时间段内维修内容明细含索赔单号', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13206,13700, 2, 'RepairIntegratedQuery', 'Released', NULL, '维修单综合查询报表', '统计某一时间段内维修单主单信息含索赔单号', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13207,13700, 2, 'WarrantyDetailsQuery', 'Released', NULL, '保修明细表', '统计某一时间段内服务站保修明细内容', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13211,13700, 2, 'ClaimSupplierDetails', 'Released', NULL, '供应商索赔结算统计', '统计期间内供应商索赔单信息', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13231,13700, 2, 'ClaimSettlementBillSplit', 'Released', NULL, '索赔结算报表(拆分结算)', '查询维修索赔单、索赔金额、外出金额等信息', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 11, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13701,13700, 2, 'DealerMainteProductCategorySta', 'Released', NULL, '服务站强保用油统计', '按产品线分类统计数据', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 12, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13702,13700, 2, 'ExtendedWarrantyMonthlySettlement','Released',null,'延保订单月度结算单','已确认的延保订单','Client/DCS/Images/Menu/Channels/NotificationQuery.png',13,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13703,13700, 2, 'VehicleLocationCheck','Released',null,'车联位置校验汇总表','车联位置校验汇总表','Client/DCS/Images/Menu/Channels/NotificationQuery.png',14,2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13704,13700, 2, 'VehicleLocationCheckDetail','Released',null,'车联位置校验明细表','车联位置校验明细表','Client/DCS/Images/Menu/Channels/NotificationQuery.png',15,2);

--财务
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13300,13000, 1, 'Financial', NULL, NULL, '财务', NULL, NULL, 27, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13305,13300, 2, 'PartsStockCheckSta', 'Released', NULL, '库存核对报表', '统计期间内入库结算的成本', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 0, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13306,13300, 2, 'PartsSendGoodsSta', 'Released', NULL, '发出商品核对报表', '统计期间内入库结算的成本', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 1, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13307,13300, 2, 'PartsPurchaseAssessmentSta', 'Released', NULL, '采购暂估核对报表', '统计期间内入库结算的成本', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 2, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13301,13300, 2, 'InboundEstimateCollect', 'Released', NULL, '配件暂估入库汇总表', '查看配件暂估入库汇总表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 3, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13302,13300, 2, 'InboundEstimateDetails', 'Released', NULL, '配件入库暂估明细表', '查看配件入库暂估明细表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 4, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13124,13300, 2, 'PartsStockSumStat', 'Released', NULL, '配件库存汇总', '商混设备配件库库存金额汇总', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13160,13300, 2, 'PartsStockSumStat-fd', 'Released', NULL, '配件库存汇总-福戴', '商混设备配件库库存金额汇总', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13161,13300, 2, 'PartsStockSumStat-yx', 'Released', NULL, '配件库存汇总-营销', '商混设备配件库库存金额汇总', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13162,13300, 2, 'PartsStockSumStat-sd', 'Released', NULL, '配件库存汇总-时代', '商混设备配件库库存金额汇总', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13163,13300, 2, 'PartsStockSumStat-gcc', 'Released', NULL, '配件库存汇总-工程车', '商混设备配件库库存金额汇总', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 5, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13129,13300, 2, 'PartsCostSettleDetail', 'Released', NULL, '配件成本统计表结算清单', '统计配件出库结算情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13130,13300, 2, 'PartsOutNoSettleStat', 'Released', NULL, '配件销售未结算明细', '查询所有出库但未结算配件明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13136,13300, 2, 'PartsInCostSettleDetail', 'Released', NULL, '入库成本结算清单', '统计期间内入库结算的成本', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 8, 2);
--******财务下的脚本下一个ID为13303*********
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13303,13300, 2, 'PurchasingCostDerate', 'Released', NULL, '采购成本降低额', '统计某一时间段内供应商出入库配件数量及相比去年年底采购价格出入库金额价差', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13304,13300, 2, 'DealerInAgencyAccountSta', 'Released', NULL, '服务站在代理库账面余额统计', '统计各服务站在代理库账面余额', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13308,13300, 2, 'PurchaseCostChangeSta', 'Released', NULL, '采购成本变动报表', '查询采购价格调整前后价格变动过程', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 11, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13309,13300, 2, 'TransferOnWayReport', 'Released', NULL, '调拨在途明细报表', '查询任何时间段的调拨明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 12, 2);

--配件-采购管理 下一个id 14101
insert into yxsecurity.Page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14100, 13000, 1, 'Purchase', '', '', '配件-采购管理', '', '', '', '', '', '', 11, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13101, 14100, 2, 'InboundHistory', 'Released', '', '历史入库记录查询', '', '', '查看历史入库记录报表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 6, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13103, 14100, 2, 'PartsPriceHistory', 'Released', '', '配件价格查询', '', '', '查看配件价格报表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 8, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13164, 14100, 2, 'PartsPricingALL', 'Released', '', '各品牌配件价格查询', '', '', '按配件图号跨库查询福田品牌的采购价、计划价、销售价、零售指导价信息', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 9, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13126, 14100, 2, 'NeedPurchaseDetail', 'Released', '', '采购需求报表', '', '', '查询配件预估库存量、缺货量，以及一段时间出库量', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 3, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13142, 14100, 2, 'PurchasePlaseRate', 'Released', '', '采购到位率', '', '', '统计某一时间段内某一仓库采购某一供应商的采购到位率', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 1, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13138, 14100, 2, 'PurchasePriceFailureAMonth', 'Released', '', '采购价格失效前一个月统计', '', '', '统计截至目前之后30天将失效的采购价格配件明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 2, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13144, 14100, 2, 'InRelationalQuery', 'Released', '', '入库单据关联单查询', '', '', '根据单号查询入库业务的上下延单据信息', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 9, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13145, 14100, 2, 'CentralizedPurchaseSta', 'Released', '', '统购品牌采购需求报表', '', '', '查询统购品牌采购需求信息', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 10, 2);
 
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14101, 14100, 2, 'PartsInfoSta', 'Released', '', '配件基本属性统计表', '', '', '汇总每个配件的基本属性', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 12, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14102, 14100, 2, 'AllBrandDiffMInPackNumSta', 'Released', '', '各品牌不同最小包装数量统计', '', '', '用于pms系统最小包装数量统一监控管理，与wms数据不一致，导致CDC仓库亏损', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 13, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14103, 14100, 2, 'OverseasStockCostControlSta', 'Released', '', '海外库存资金监控报表', '', '', '用于海外采购成本监控情况统计', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 14, 2);
INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status)VALUES(14104,14100,2,'UnmetOrderCollect','Released',NULL, '未满足订单汇总统计表','以系统内采购订单和配件图号为依据，统计到目前为止未满足情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',15,2);

--配件-销售管理 下一个id 14201
insert into yxsecurity.Page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14200, 13000, 1, 'Sale', '', '', '配件-销售管理', '', '', '', '', '', '', 14, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13112, 14200, 2, 'PartsSalesOnway', 'Released', '', '配件销售在途查询', '', '', '汇总查询未收货确认配件', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 21, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13102, 14200, 2, 'OutboundHistory', 'Released', '', '历史出库记录查询', '', '', '查看历史出库记录报表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 19, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13159, 14200, 2, 'PartsSalesMonthlySta', 'Released', '', '配件销售月报表（明细表）', '', '', '汇总总部发给服务商配件销量明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 25, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13128, 14200, 2, 'PartsUrgPercentStat', 'Released', '', '应急比例报表', '', '', '服务站应急比例汇总表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 20, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13137, 14200, 2, 'PeriodSupplyAmount', 'Released', '', '期间供货额', '', '', '统计某一时间段内某个仓库销售出库、采购入库的配件明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 22, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13143, 14200, 2, 'OutRelationalQuery', 'Released', '', '出库单据关联单查询', '', '', '根据单号查询出库业务的上下延单据信息', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 23, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13149, 14200, 2, 'PartsOrderConfirmGapSta', 'Released', '', '订单审核缺口统计报表', '', '', '统计各品牌配件销售订单审核缺口情况', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 24, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14203, 14200, 2, 'PartsOrderConfirmGapSta_fd', 'Released', '', '订单审核缺口统计报表-福戴', '', '', '统计各品牌配件销售订单审核缺口情况', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 27, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14204, 14200, 2, 'PartsOrderConfirmGapSta_yx', 'Released', '', '订单审核缺口统计报表-营销', '', '', '统计各品牌配件销售订单审核缺口情况', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 28, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14205, 14200, 2, 'PartsOrderConfirmGapSta_gc', 'Released', '', '订单审核缺口统计报表-工程车', '', '', '统计各品牌配件销售订单审核缺口情况', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 29, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14206, 14200, 2, 'PartsOrderConfirmGapSta_sd', 'Released', '', '订单审核缺口统计报表-时代', '', '', '统计各品牌配件销售订单审核缺口情况', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 30, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14202, 14200, 2, 'GCCCDCToFPartsInPlaceSta', 'Released', '', '总部发放服务站配件到位情况报表（明细表）', '', '', '汇总总部发给服务商配件到位监控明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 25, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14208, 14200, 2, 'BonusPointsOrderReport', 'Released', '', '积分订单明细报表', '', '', '汇总积分订单明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 31, 2);
--配件-RDC管理 下一个id 14301
insert into yxsecurity.Page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14300, 13000, 1, 'RDC', '', '', '配件-RDC管理', '', '', '', '', '', '', 15, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13106, 14300, 2, 'AgencyStock', 'Released', '', '代理库库存查询', '', '', '代理库库存查询报表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 26, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13125, 14300, 2, 'DealerToAgentOrderStat', 'Released', '', '服务站向代理库销售订单查询', '', '', '查询服务站向代理库提报订单信息', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 27, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13139, 14300, 2, 'AgentsCreditQuotaQuery', 'Released', '', '代理库授信额度统计报表', '', '', '统计某一时间段内某一代理库给予服务站的授信金额明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 28, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13140, 14300, 2, 'AgentsHistoryInQuery', 'Released', '', '代理库入库记录查询', '', '', '统计某一时间段内代理库入库明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 29, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13141, 14300, 2, 'AgentsHistoryOutQuery', 'Released', '', '代理库出库记录查询', '', '', '统计某一时间段内代理库出库明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 30, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13147, 14300, 2, 'AgentShippingDetailSta', 'Released', '', '代理库发运单明细', '', '', '查询代理库向服务站发运配件的到位及时性', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 32, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13148, 14300, 2, 'AgentShippingSta', 'Released', '', '代理库发运清单', '', '', '查询代理库向服务站发运配件的到位及时性', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 31, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13158, 14300, 2, 'AgencySparePartRetailDetailSta', 'Released', '', '代理库配件零售清单', '', '', '查询代理库配件零售清单', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 33, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14301, 14300, 2, 'AcrossBrandPartsTransferSta', 'Released', '', '跨品牌配件调拨报表', '', '', '统计代理库或服务站跨品牌配件调拨的清单', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 34, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14302, 14300, 2, 'DToFPartsInPlaceSta', 'Released', '', '代理库发放服务站到位情况报表（明细表）', '', '', '代理库发放服务站配件到位情况进行监控', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 35, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14303, 14300, 2, 'DLKPartOrderFillRateSta', 'Released', '', '代理库配件订单满足率报表（明细表）', '', '', '汇总代理库配件订单满足率情况', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 36, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14304, 14300, 2, 'AgencyPartsOutInSta', 'Released', '', '代理库配件出入库汇总统计', '', '', '查询代理库出入库汇总统计信息', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 36, 2);

--配件-服务站管理 下一个id 14401
insert into yxsecurity.Page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14400, 13000, 1, 'Dealer', '', '', '配件-服务站管理', '', '', '', '', '', '', 16, 2);

--insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
--values (13104, 14400, 2, 'DealerBranch', 'Released', '', '服务站分公司信息查询', '', '', '查看服务站分公司信息报表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 34, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13113, 14400, 2, 'DealerPartsStockStat', 'Released', '', '服务站库存查询', '', '', '实时查询服务站配件库存明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 36, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14404, 14400, 2, 'DealerPartsStockStat-fd', 'Released', '', '服务站库存查询-福戴', '', '', '实时查询服务站配件库存明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 36, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14405, 14400, 2, 'DealerPartsStockStat-yx', 'Released', '', '服务站库存查询-营销', '', '', '实时查询服务站配件库存明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 36, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14406, 14400, 2, 'DealerPartsStockStat-sd', 'Released', '', '服务站库存查询-时代', '', '', '实时查询服务站配件库存明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 36, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14407, 14400, 2, 'DealerPartsStockStat-gcc', 'Released', '', '服务站库存查询-工程车', '', '', '实时查询服务站配件库存明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 36, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13153, 14400, 2, 'DealerSparePartRetailDetailSta', 'Released', '', '服务站配件零售清单', '', '', '查询服务站配件零售清单', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 37, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13155, 14400, 2, 'DealerSparePartTurnoverStaN', 'Released', '', '服务站配件周转情况汇总（不分品牌）', '', '', '查询服务站配件周转情况汇总（不分品牌）', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 42, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13154, 14400, 2, 'DealerSparePartTurnoverStaY', 'Released', '', '服务站配件周转情况汇总（分品牌）', '', '', '查询服务站配件周转情况汇总（分品牌）', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 41, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13156, 14400, 2, 'DealerSparePartInWHSta', 'Released', '', '服务站配件入库汇总', '', '', '查询服务站配件入库情况汇总', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 39, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13157, 14400, 2, 'DealerSparePartOutWHSta', 'Released', '', '服务站配件出库汇总', '', '', '查询服务站配件出库情况汇总', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 40, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13216, 14400, 2, 'DealerRepairDetailSta', 'Released', '', '服务站维修明细', '', '', '查询服务站维修明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 38, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14401, 14400, 2, 'DealerROAcrossBrandSta', 'Released', '', '服务站维修单跨品牌配件使用报表', '', '', '统计在生成保内或保外维修单时跨品牌配件调拨的清单', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 39, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14402, 14400, 2, 'DealerStockPartGapSta', 'Released', '', '服务站库存配件缺口表', '', '', '反映服务站库存与近期实际用件差异情况，便于服务站进行配件储备', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 40, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14403, 14400, 2, 'PartBnOrBwRateSta', 'Released', '', '配件保外保内比报表', '', '', '反映各个服务站的配件保内保外比情况', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 41, 2);

--配件-库存管理 下一个id 14501
insert into yxsecurity.Page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14500, 13000, 1, 'Partsstock', '', '', '配件-库存管理', '', '', '', '', '', '', 13, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13109, 14500, 2, 'PartsStockDetail', 'Released', '', '配件库存明细查询', '', '', '查询配件库存以及调拨在途情况', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 14, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13110, 14500, 2, 'PartsStockAge', 'Released', '', '库龄统计', '', '', '查询一段时期内呆滞库存量', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 15, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13152, 14500, 2, 'AllBrandStockDetailSta', 'Released', '', '各品牌超X月库存明细表', '', '', '分析CDC库存情况，为储备管理提供依据', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 17, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13150, 14500, 2, 'OverseaPartsOutboundSta', 'Released', '', '海外品牌配件出库统计表', '', '', '海外乘用车品牌、海外轻卡品牌、海外中重卡品牌配件出库单出库单明细查询', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 16, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14501, 14500, 2, 'WarehouseAgeDetailSta', 'Released', '', '库龄明细统计报表', '', '', '统计各个配件在库时间', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 18, 2);

-- PDA 端节点权限
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS,PAGESYSTEM)
values (16000, 0, 0, 'SIH', null, null, 'PDA权限菜单', null, null, null, null, null, null, 16, 2, 1);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS,PAGESYSTEM)
values (16100, 16000, 1, 'SIH100', null, null, '收货模块', null, null, '描述', null, null, null, 1, 2, 1);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS,PAGESYSTEM)
values (16200, 16000, 1, 'SIH200', null, null, '出库业务', null, null, '描述', null, null, null, 2, 2, 1);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUs,PAGESYSTEM)
values (16300, 16000, 1, 'SIH300', null, null, '库内业务', null, null, '描述', null, null, null, 3, 2, 1);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS,PAGESYSTEM)
values (16400, 16000, 1, 'SIH400', null, null, '基础数据', null, null, '描述', null, null, null, 4, 2, 1);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS,PAGESYSTEM)
values (16101, 16100, 2, 'SIH101', null, null, '收货', null, null, '描述', null, null, null, 1, 2, 1);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS,PAGESYSTEM)
values (16102, 16100, 2, 'SIH102', null, null, '包装', null, null, '描述', null, null, null, 2, 2, 1);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS, PAGESYSTEM)
values (16103, 16100, 2, 'SIH103', null, null, '上架', null, null, '描述', null, null, null, 3, 2, 1);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS, PAGESYSTEM)
values (16201, 16200, 2, 'SIH201', null, null, '拣货', null, null, '描述', null, null, null, 1, 2, 1);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS, PAGESYSTEM)
values (16202, 16200, 2, 'SIH202', null, null, '装箱', null, null, '描述', null, null, null, 2, 2, 1);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS, PAGESYSTEM)
values (16301, 16300, 2, 'SIH301', null, null, '盘点', null, null, '描述', null, null, null, 1, 2, 1);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS, PAGESYSTEM)
values (16302, 16300, 2, 'SIH302', null, null, '移库', null, null, '描述', null, null, null, 1, 2, 1);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS, PAGESYSTEM)
values (16401, 16400, 2, 'SIH401', null, null, '库存查询', null, null, '描述', null, null, null, 1, 2, 1);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS,PAGESYSTEM)
values (16104, 16100, 2, 'SIH104', null, null, '收货', null, null, '中心库收货', null, null, null, 4, 2, 1);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS,PAGESYSTEM)
values (16105, 16100, 2, 'SIH105', null, null, '上架', null, null, '中心库上架', null, null, null, 5, 2, 1);

--配件-指标管理 下一个id 14601
insert into yxsecurity.Page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14600, 13000, 1, 'target', '', '', '配件-指标管理', '', '', '', '', '', '', 17, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13108, 14600, 2, 'PartsSaleOrderSatisfiedRate', 'Released', '', '配件订单满足率（二）', '', '', '查询某时间段订单品种、数量、金额满足情况', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 44, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13151, 14600, 2, 'PartsPurchaseSalesSta', 'Released', '', '配件采购及销售情况汇总', '', '', '监控配件采购入库情况，为储备管理提供依据', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 46, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13146, 14600, 2, 'PartsOrderFillRate', 'Released', '', '配件订单满足率（一）', '', '', '统计PMS系统订单满足率', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 43, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)

values (14610, 14600, 2, 'PartsOrderFillRate-fd', 'Released', '', '配件订单满足率（一）-福戴', '', '', '统计PMS系统订单满足率', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 43, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14611, 14600, 2, 'PartsOrderFillRate-yx', 'Released', '', '配件订单满足率（一）-营销', '', '', '统计PMS系统订单满足率', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 43, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14612, 14600, 2, 'PartsOrderFillRate-sd', 'Released', '', '配件订单满足率（一）-时代', '', '', '统计PMS系统订单满足率', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 43, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14613, 14600, 2, 'PartsOrderFillRate-gcc', 'Released', '', '配件订单满足率（一）-工程车', '', '', '统计PMS系统订单满足率', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 43, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14601, 14600, 2, 'AllBrandStockCollectSta', 'Released', '', '各品牌超X月库存汇总表', '', '', '各品牌超X月库存汇总表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 47, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14602, 14600, 2, 'AllBrandPartSupplySta', 'Released', '', '各品牌配件现货供应情况月度明细表', '', '', '各品牌配件现货供应情况月度明细表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 48, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14603, 14600, 2, 'AllBrandPartSupplyYearSta', 'Released', '', '各品牌配件现货供应情况年度分析表', '', '', '各品牌配件现货供应情况年度分析表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 49, 2);


insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14604, 14600, 2, 'AllBrandPartSupplyCurrSta', 'Released', '', '各配件现货供应能力实时查询表', '', '', '各配件现货供应能力实时查询表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 50, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14605, 14600, 2, 'AllBrandPartSupCollectSta', 'Released', '', '各品牌现货供应情况月度汇总表', '', '', '各品牌现货供应情况月度汇总表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 51, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14606, 14600, 2, 'AllBrandPartTOCollectSta', 'Released', '', '各品牌配件周转情况汇总表', '', '', '各品牌配件周转情况汇总表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 52, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14607, 14600, 2, 'AllBrandPartTODetailSta', 'Released', '', '各品牌配件周转情况明细表', '', '', '各品牌配件周转情况明细表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 53, 2);


--配件-储备管理 下一个id 14701
insert into yxsecurity.Page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14700, 13000, 1, 'Reserve', '', '', '配件-储备管理', '', '', '', '', '', '', 12, 2);

insert into Page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14701, 14700, 2, 'MonthDemandWaveCoeffSta', 'Released', '', '月度需求波动系数表（品牌）', '', '', '统计配件需求月度波动规律', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 13, 2);

--保底库存管理
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (16403,1300, 2, 'BottomStock', 'Released', NULL, '保底库存管理', '保底库存管理', 'Client/DCS/Images/Menu/PartsStocking/PartsStockQuery.png',16, 2);










TRUNCATE TABLE Action
/
drop sequence S_action;
create sequence S_action;
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 101, 'Security|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 101, 'Security|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 101, 'Security|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 102, 'Security|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 102, 'Security|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 102, 'Security|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 103, 'Security|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 103, 'Security|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 103, 'Security|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 104, 'Security|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 104, 'Security|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 104, 'Security|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 104, 'Security|Freeze', '冻结', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 104, 'Security|Recover', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 104, 'Security|Validation', '生效', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 104, 'Security|ModifyAdminPassword', '修改管理员密码', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 105, 'Security|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 105, 'Security|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 105, 'Security|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 105, 'Security|Reorder', '调整顺序', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 106, 'Security|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 106, 'Security|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 106, 'Security|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 106, 'Security|Freeze', '冻结', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 106, 'Security|Recover', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 106, 'Security|ModifyPassword', '修改密码', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 107, 'Security|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 107, 'Security|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 107, 'Security|Freeze', '冻结', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 107, 'Security|Recover', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 108, 'Security|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 108, 'Security|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 108, 'Security|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 108, 'Security|Reorder', '调整顺序', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 109, 'Security|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 109, 'Security|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 109, 'Security|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 109, 'Security|Freeze', '冻结', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 109, 'Security|Recover', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 109, 'Security|ModifyPassword', '修改密码', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 110, 'Security|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 110, 'Security|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 110, 'Security|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 110, 'Security|Recover', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 110, 'RoleGroup|BatchAddPermissions', '批量增加权限', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 110, 'RoleGroup|BatchCancelPermissions', '批量取消权限', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 402, 'Security|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 403, 'Security|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1301, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1301, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1301, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1301, 'Common|Pause', '停用', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1301, 'Common|Resume', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1301, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1301, 'Dealer|AddTogether', '新增(集中)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1301, 'Dealer|EditTogether', '修改(集中)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1301, 'Dealer|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1301, 'Dealer|DetailTogether', '查看明细(集中)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1301, 'Dealer|BatchBindingTel','视频手机批量绑定', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1301, 'Dealer|BatchImport','导入修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1301, 'Dealer|ImportEdit','批量导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1301, 'Dealer|UpdateDistance','更新站间距离', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1302, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1302, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1302, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1302, 'Common|Pause', '停用', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1302, 'Common|Resume', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1302, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1302, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1302, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1302, 'DealerServiceInfoForBranch|ImportOverlapInfo', '导入覆盖-经营权限', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1302, 'DealerServiceInfoForBranch|ImportMainInfo', '导入修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1302, 'DealerServiceInfoForBranch|ImportForEdito', '批量导入修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1302, 'DealerServiceInfoForBranch|BranchImport', '批量导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1302, 'DealerServiceInfoForBranch|UpdateDistance','更新站间距离', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1303, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1303, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1304, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1304, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1304, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1305, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1305, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1305, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1305, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1305, 'Common|KeyPositionRoot', '关键岗位权限', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1306, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1308, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1308, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1308, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1308, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1309, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1309, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1309, 'Common|Delete', '删除', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1309, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1310, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1310, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1310, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1310, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1310, 'Common|Import', '导入', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1314, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1314, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1314, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1314, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1314, 'Common|Import', '导入', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1315, 'Common|EditTogether', '修改(集中)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1315, 'Common|DetailTogether', '详细信息(集中)', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1317, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1317, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1317, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1317, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1317, 'Common|Import', '导入', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1318, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1318, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1318, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1318, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1318, 'Common|Import', '导入', 2);
  
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1319, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1319, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1319, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1319, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1319, 'Common|Import', '导入', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1402, 'Common|Download', '下载', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3101, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3101, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3101, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3101, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3101, 'Common|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3101, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3101, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3101, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3101, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3101, 'PartsPurchaseOrder|PendingApproval', '代确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3101, 'PartsPurchaseOrder|ChannelChange', '渠道变更', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3101, 'PartsPurchaseOrder|Sign', '签批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3101, 'Common|SchedulerExport', '下载', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3101, 'PartsPurchaseOrder|ReplaceShip', '代发运', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3102, 'Common|Confirm', '确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3102, 'Common|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3102, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3102, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3102, 'Common|ReplaceShip', '发运', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3102, 'Common|ReplaceShipAll', '汇总发运', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3105, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3105, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3105, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3105, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3105, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3105, 'Common|PrintLabel', '标签打印', 99);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3105, 'Common|PrintNoPrice', '打印（无价格）', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3106, 'Common|Confirm', '确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3106, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3106, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3106, 'Common|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3106, 'SupplierShippingOrder|PendingConfirmationSupplier', '代确认直供发运', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3106, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3106, 'Common|PrintLabel', '标签打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3106, 'Common|UsedPrintLabel', '标签打印(旧)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3104, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3104, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (s_action.nextval, 3104, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3103, 'PartsPurReturnOrderForBranch|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3103, 'PartsPurReturnOrderForBranch|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3103, 'PartsPurReturnOrderForBranch|FinalApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3103, 'PartsPurReturnOrderForBranch|InsteadApprove', '终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3103, 'PartsPurReturnOrderForBranch|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3103, 'PartsPurReturnOrderForBranch|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3103, 'PartsPurReturnOrderForBranch|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3103, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3107, 'Common|Confirm', '确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3107, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3107, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3109, 'Common|PrintLabel', '标签打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3109, 'Common|UsedPrintLabel', '标签打印(旧)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3109, 'Common|StandardPrintLabel', '标签打印（带标准）', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3110, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3110, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3110, 'Common|Abandon', '作废', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3111, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3111, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3111, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3111, 'SupplierPreApprovedLoan|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3111, 'SupplierPreApprovedLoan|PrintAll', '全部打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3111, 'SupplierPreApprovedLoan|ZeroPrint', '为零打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3111, 'SupplierPreApprovedLoan|NonZeroPrint', '非零打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3111, 'SupplierPreApprovedLoan|ExportDetail', '清单导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3112, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 15103, 'PartsPurchasePlanOrder|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 15103, 'PartsPurchasePlanOrder|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 15103, 'PartsPurchasePlanOrder|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 15103, 'PartsPurchasePlanOrder|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 15103, 'PartsPurchasePlanOrder|FinalApprove', '终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 15103, 'PartsPurchasePlanOrder|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 15103, 'PartsPurchasePlanOrder|Export', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 15103, 'PartsPurchasePlanOrder|Print', '打印', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2206, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2206, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2206, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2206, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2208, 'IMSPurchaseLoginInfo|Retransfer', '重新下传', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2208, 'IMSPurchaseLoginInfo|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2209, 'IMSShippingLoginInfo|Setup', '手动设置', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2209, 'IMSShippingLoginInfo|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1205, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1205, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1205, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1205, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3301, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3301, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3301, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3301, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3301, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3301, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3301, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3302, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3302, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3302, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3302, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3302, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3302, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3302, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3302, 'Common|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3201, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3201, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3201, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3201, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3201, 'PartsPurchaseSettleBill|InvoiceRegister', '登记发票', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3201, 'PartsPurchaseSettleBill|InvoiceEdit', '发票修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3201, 'PartsPurchaseSettleBill|InvoiceApprove', '发票审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3201, 'PartsPurchaseSettleBill|AntiSettlement', '反结算', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3201, 'PartsPurchaseSettleBill|PrintForGC', '工程车打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3201, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3201, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3201, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3201, 'PartsPurchaseSettleBill|InvoiceSettlement', '发票已审核反结算', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3202, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3202, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3202, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3202, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3202, 'PartsPurchaseRtnSettleBill|InvoiceRegister', '登记发票', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3202, 'PartsPurchaseRtnSettleBill|AntiSettlement', '反结算', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3202, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3202, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3202, 'PartsPurchaseRtnSettleBill|SettlementCostSearch', '结算成本查询', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3202, 'PartsPurchaseRtnSettleBill|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3203, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3203, 'Common|Print', '打印', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3205, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3205, 'Common|Print', '打印', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (yxSecurity.S_Action.Nextval, 3206, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (yxSecurity.S_Action.Nextval, 3206, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (yxSecurity.S_Action.Nextval, 3206, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (yxSecurity.S_Action.Nextval, 3206, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (yxSecurity.S_Action.Nextval, 3206, 'PartsPurchaseSettleBill|InvoiceRegister', '登记发票', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (yxSecurity.S_Action.Nextval, 3206, 'PartsPurchaseSettleBill|InvoiceEdit', '发票修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (yxSecurity.S_Action.Nextval, 3206, 'PartsPurchaseSettleBill|InvoiceApprove', '发票审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (yxSecurity.S_Action.Nextval, 3206, 'PartsPurchaseSettleBill|AntiSettlement', '反结算', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (yxSecurity.S_Action.Nextval, 3206, 'PartsPurchaseSettleBill|PrintForGC', '工程车打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (yxSecurity.S_Action.Nextval, 3206, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (yxSecurity.S_Action.Nextval, 3206, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (yxSecurity.S_Action.Nextval, 3206, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (yxSecurity.S_Action.Nextval, 3206, 'PartsPurchaseSettleBill|InvoiceSettlement', '发票已审核反结算', 2);



insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3401, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3401, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3401, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3401, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3401, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3401, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3402, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3402, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3402, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3402, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3402, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3402, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3501, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3501, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3502, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3502, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3503, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3503, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3504, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3505, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3506, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3507, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3508, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3509, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3510, 'PartConPurchasePlan|MainEdit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3510, 'PartConPurchasePlan|ImportEdit', '导入修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3510, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4201, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4201, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4201, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4201, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4201, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4201, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4202, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4202, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4202, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4202, 'Common|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4202, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4202, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2301, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2301, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2301, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2301, 'Common|Resume', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2301, 'Common|Pause', '停用', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2301, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2301, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2302, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2302, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2302, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2302, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2303, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2303, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2303, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2303, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2303, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2304, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2304, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2304, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2304, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2304, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2305, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2305, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2305, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2305, 'Workflow|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2305, 'Workflow|FinalApprove', '终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2305, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2305, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2305, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2306, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2306, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2306, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2306, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2306, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2306, 'Common|BatchAbandon', '批量作废', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status)VALUES (S_Action.Nextval, 2306, 'Common|ImportAbandon', '批量作废(导入)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2307, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2307, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2307, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2307, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2307, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2308, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2308, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2308, 'Common|Pause', '停用', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2308, 'Common|Resume', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2308, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2308, 'Common|Maintain', '仓库与人员维护', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2309, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2309, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2309, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2309, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2311, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2313, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2314, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2315, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2316, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2317, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2317, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2317, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2318, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2318, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2319, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2319, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2319, 'Common|Pause', '停用', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2321, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2321, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2321, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2321, 'OrderApproveWeekday|ImportEdit', '批量导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2321, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2322, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2322, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2322, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2322, 'WarehouseSequence|ImportEdit', '导入修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2322, 'Common|Export', '导出', 2);

INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'Common|Add', '新增', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'Common|Edit', '修改', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'Common|Abandon', '作废', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'Workflow|InitialApprove', '初审', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'Workflow|FinalApprove', '终审', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'Common|Import', '导入', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'Common|Export', '导出', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'Common|MergeExport', '合并导出', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'PartsSalesPriceChange|BatchInitialApprove', '批量初审', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'PartsSalesPriceChange|BatchFinalApprove', '批量终审', 2);

INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2324, 'Common|Add', '新增', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2324, 'Common|Edit', '修改', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2324, 'Common|InitialApprove', '初审', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2324, 'Common|Approve', '终审', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2324, 'Common|BatchApproval', '批量审核', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2324, 'Common|Abandon', '作废', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2324, 'Common|Export', '导出', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2324, 'Common|MergeExport', '合并导出', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2324, 'Common|Import', '导入', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2325,'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2325,'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2325,'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2325,'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2325,'Common|Detail', '明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2325,'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2325,'Common|Audit', '审核', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2328,'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2328,'Common|Abandon', '作废', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4101, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4101, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4101, 'Common|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4101, 'PartsSalesOrder|AddByDeputy', '新增(代做)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4101, 'PartsSalesOrder|RevokeByDeputy', '撤单(代做)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4101, 'PartsSalesOrder|RebateApprove', '折让审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4101, 'PartsSalesOrder|CheckStock', '库存校验', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4101, 'PartsSalesOrder|EditDetail', '清单修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4101, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4101, 'Common|SchedulereExport', '下载', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4101, 'Common|BATCHAPPROVAL', '批量审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4101, 'Logistics|LogisticsQuery', '物流信息查询', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4102, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4102, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4102, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4102, 'Common|Revoke', '撤单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4102, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4102, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4102, 'Logistics|LogisticsQuery', '物流信息查询', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4103, 'PartsSalesReturnBill|AddByDeputy', '新增(代做)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4103, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4103, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4103, 'Common|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4103, 'Common|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4103, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4103, 'PartsSalesReturnBill|FirstTrial ', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4103, 'PartsSalesReturnBill|FinalTrial ', '终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4103, 'PartsSalesReturnBill|Upload', '附件上传', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4103, 'PartsSalesReturnBill|BatchApprove', '批量审核', 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4104, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4104, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4104, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4104, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4104, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4109, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4109, 'Common|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4109, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4109, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4110, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4111, 'ERPDelivery|ManualProcessing', '手动处理', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4111, 'ERPDelivery|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4111, 'ERPDelivery|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4112, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4113, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4114, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4115, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4118, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4118, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4117, 'AgencyRetailerOrder|Confirm', '确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4117, 'AgencyRetailerOrder|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4117, 'AgencyRetailerOrder|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4117, 'AgencyRetailerOrder|MergeExport', '合并导出', 2);

INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 4119, 'Common|Distribute', '分发', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 4119, 'Common|Abandon', '作废', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 4119, 'Common|Export', '导出', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 4119, 'Common|DistributeExport', '分发导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4301, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4301, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4301, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4301, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4301, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4301, 'PartsRetailOrder|MakeInvoice', '开票', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4302, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4302, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4302, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4302, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4302, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4302, 'PartsRetailReturnBill|Refund', '退款', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4303, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4303, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4303, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4303, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4303, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4305, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4305, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4305, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4305, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4305, 'Common|Approve', '审批', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 4306, 'Common|Approve', '审批', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 4306, 'Common|Abandon', '作废', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 4306, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4307, 'ERPDelivery|ManualProcessing', '手动处理', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4307, 'ERPDelivery|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4307, 'ERPDelivery|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4308, 'ERPDelivery|ManualProcessing', '手动处理', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4308, 'ERPDelivery|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4308, 'ERPDelivery|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4501, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4501, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4501, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4501, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4501, 'Common|InvoiceRegister', '登记发票', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4501, 'Common|AntiSettlement', '反结算', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4501, 'Common|AppoveRebate', '审批（返利）', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4501, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4501, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4501, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4501, 'Common|SettlementCost', '查看结算单成本', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4501, 'Common|PrintInvoice', '发票清单打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4502, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4502, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4502, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4502, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4502, 'PartsSalesRtnSettlement|InvoiceRegister', '登记发票', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4502, 'PartsSalesRtnSettlement|AntiSettlement', '反结算', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4502, 'PartsSalesRtnSettlement|SettlementCostSearch', '结算成本查询', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4502, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4502, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4502, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4502, 'Common|GCPrint', '工程车打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4503, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4503, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4504, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4504, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4505, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4505, 'Common|MergeExport', '合并导出',  2);



insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4506, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4506, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4506, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4506, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4506, 'Common|MergeExport', '合并导出',  2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9301, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9301, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9301, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9301, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9302, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9302, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9302, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9302, 'Common|DefaultWarehouseArea', '指定为默认库位', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9401, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9401, 'UsedPartsStock|PrintBarCode', '条码打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9501, 'UsedPartsInboundOrder|ViewDetail', '查看清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9501, 'UsedPartsInboundOrder|WarehouseParts', '入库', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9501, 'UsedPartsInboundOrder|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9501,  'Common|ExportDetail', '导出清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9502, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9502, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9502,  'Common|ExportDetail', '导出清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9502, 'Common|PrintForGC', '工程车打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9502, 'Common|PrintForFinance', '入库单打印（财务）', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9601, 'UsedPartsOutboundOrder|ViewDetail', '查看清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9601, 'UsedPartsOutboundOrder|DeliverParts', '出库', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9601,  'Common|ExportDetail', '导出清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9602, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9602,  'Common|ExportDetail', '导出清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9602, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9602, 'UsedPartsOutboundOrderQuery|NotPartCodePrint', '无图号打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9701, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9701, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9701, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9701, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9701, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9701, 'Common|ExportDetail', '导出清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9701, 'Common|PartsTransferOrderPrint', '调拨单打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9701, 'Common|CodePrint', '条形码打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9701, 'Common|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9702, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9702, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9703, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9703, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9703, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9703, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9703, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9703, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9703, 'Common|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9703, 'Common|ExportDetail', '导出清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9704, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9704, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9704, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9704, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9704, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9704, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9704, 'Common|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9704, 'UsedPartsLoanBill|Revert', '归还', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9704, 'Common|ExportDetail', '导出清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9705, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9705, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9705, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9705, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9705, 'Common|ExportDetail', '导出清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9705, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9705, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9801, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9801, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9801, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9801, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9801, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9203, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9203, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9203, 'SsUsedPartsStorage|PrintBarCode', '条码打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9204, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9201, 'Common|Confirm', '确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9201, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9201, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9201, 'UsedPartsShippingOrder|ExportDetail', '导出清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9201, 'UsedPartsShippingOrder|PrintBarCode', '条码打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9201, 'UsedPartsShippingOrder|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9201, 'UsedPartsShippingOrder|PrintChooseBarCode', '条码选择打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9202, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9202, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9202, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9205, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9205, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9205, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9206, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9102, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9102, 'SsUsedPartsStorage|PrintBarCode', '条码打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9101, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9101, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9101, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9101, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9101, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9101, 'UsedPartsShippingOrder|ExportDetail', '导出清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9101, 'UsedPartsShippingOrder|PrintBarCode', '条码打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9103, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9103, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9103, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9103, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9103, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8102, 'RepairOrderForBranch|Detail', '查看详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8102, 'RepairOrderForBranch|Unlock', '解锁', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8102, 'RepairOrderForBranch|MVSPicture', '查看照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8102, 'RepairOrderForBranch|MVSVideo', '查看视频', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8102, 'RepairOrderForBranch|MVSRoute', '查看外出轨迹', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8102, 'RepairOrderForBranch|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8102, 'RepairOrderForBranch|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8102, 'RepairOrderForBranch|APPPicture', '查看APP照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8102, 'RepairOrderForBranch|APPRoute', '查看APP轨迹', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8103, 'Common|Detail', '查看详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8103, 'Common|MVSPicture', '查看照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8103, 'Common|MVSVideo', '查看视频', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8103, 'Common|MVSRoute', '查看外出轨迹', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8103, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8103, 'Common|Export', '导出', 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8204, 'ServiceTripClaimApplication|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8204, 'ServiceTripClaimApplication|FinalApprove', '终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8204, 'ServiceTripClaimApplication|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8204, 'ServiceTripClaimApplication|LookCountersign', '查看会签信息', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8204, 'ServiceTripClaimApplication|FaultQuery', '故障代码查询', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8204, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8204, 'Common|Detail', '详细信息', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8204, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8303, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8303, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8303, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8303, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8303, 'RepairClaimApplication|SupplierApprover', '供应商审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8303, 'RepairClaimApplication|InitialApprove', '初审', 2);
--insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8303, 'RepairClaimApplication|Countersign', '会签', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8303, 'RepairClaimApplication|DoubleApprove', '供应商复核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8303, 'RepairClaimApplication|FinalApprove', '终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8303, 'RepairClaimApplication|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8303, 'RepairClaimApplication|DetailApprove', '查看申请单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8303, 'RepairClaimApplication|DetailPartsClaim', '查看维修索赔单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8303, 'RepairClaimApplication|BatchApprove', '批量审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8303, 'RepairClaimApplication|VehicleRoute', '车辆行驶轨迹查询', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8303, 'RepairClaimApplication|FaultQuery', '故障代码查询', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8303, 'RepairClaimApplication|CheckRepair', '重复维修信息校验', 2);



insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8202, 'RepairClaimApplication|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8202, 'RepairClaimApplication|FinalApprove', '终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8202, 'RepairClaimApplication|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8202, 'RepairClaimApplication|LookCountersign', '查看会签信息', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8202, 'RepairClaimApplication|FaultQuery', '故障代码查询', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8202, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8202, 'Common|Detail', '详细信息', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8202, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8202, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8202, 'Common|View', 'APP照片查看', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8207, 'PartsClaimApplicationForCountersign|Countersign', '会签', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8208, 'ServiceTripClaimApplicationCountersign|Countersign', '会签', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8209, 'MarketABQualityInformation|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8209, 'MarketABQualityInformation|FinalApprove', '终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8209, 'MarketABQualityInformation|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8209, 'MarketABQualityInformation|LookCountersign', '查询会签信息', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8209, 'MarketABQualityInformation|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8209, 'MarketABQualityInformation|Detail', '显示详情', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8210, 'MarketABQualityInfoCountersign|Countersign', '会签', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8210, 'MarketABQualityInfoCountersign|LookCountersign', '查询会签信息', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8210, 'MarketABQualityInfoCountersign|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8210, 'MarketABQualityInfoCountersign|Detail', '显示详情', 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8211, 'PartsClaimOrderNew|Audit', '审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8211, 'PartsClaimOrderNew|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8211, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8211, 'Common|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8211, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8211, 'Common|Detail', '查看明细', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8212, 'PartsClaimOrderNew|DeliverParts', '出库', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8212, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|FinalApprove', '终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|SupplierInitialApprove', '供应商确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|SupplierReview', '供应商复核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|Detail', '详细信息', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|BatchInitialApprove', '批量初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|BatchApprove', '批量审批', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|Export1', '导出维修项目', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|Export2', '导出材料清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|Export1', '导出维修项目', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|Export2', '导出材料清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|MVSPicture', '查看照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|MVSVideo', '查看视频', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|MVSRoute', '查看外出轨迹', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|APPPicture', '查看APP照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|APPRoute', '查看APP轨迹', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|VehicleRoute', '车辆行驶轨迹查询', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|FaultQuery', '故障代码查询', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|CheckRepair', '重复维修信息校验', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8203, 'RepairClaimApplication|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8203, 'RepairClaimApplication|Countersign', '会签', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8203, 'RepairClaimApplication|Audit', '审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8203, 'RepairClaimApplication|Approval', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8203, 'RepairClaimApplication|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8203, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8203, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8302, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8302, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8302, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8205, 'ServicesClaimApplication|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8205, 'ServicesClaimApplication|Countersign', '会签', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8205, 'ServicesClaimApplication|Audit', '审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8205, 'ServicesClaimApplication|Approval', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8205, 'ServicesClaimApplication|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8205, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8205, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8304, 'ServicesClaimBill|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8304, 'ServicesClaimBill|FinalApprove', '终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8304, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8304, 'ServicesClaimBill|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8304, 'Common|Detail', '查看明细', 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8206, 'PartsClaimApplication|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8206, 'PartsClaimApplication|FinalApprove', '终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8206, 'PartsClaimApplication|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8206, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8206, 'Common|Detail', '查看明细', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8305, 'PartsClaimOrder|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8305, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8305, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8305, 'Common|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8305, 'Common|Detail', '查看明细', 2);

INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|GuestRegistration', '来客登记', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|GuestPickin', '来客并派工', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|RepairOrderRegistration', '维修单登记', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|RepairOrderEdit', '维修单修改', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|Abandon', '作废', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|RepairsCompleted', '维修完工', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|RepairsCompletedPicking', '指定领料完工', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|RepairOrderPrint', '打印维修单', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|RepairOrderPrintNew', '打印维修单(新)', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|Print', '打印结算单', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|GenerateClaims', '生成索赔', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|Reject', '驳回修改', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|Detail', '查看明细', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|GenerateOuterPurchase', '生成外采申请', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|GenerateSell', '生成紧急销售订单', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|LablePrint', '打印标签', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|SeePhotos', '查看照片', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|SeeVideo', '查看视频', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|SeeTrajectory', '查看外出轨迹', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|QueryOldPMSRepairOrder', '查询老PMS历史维修单', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|MergeExport', '合并导出', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|APPPicture', '查看APP照片', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|APPRoute', '查看APP轨迹', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 8402, 'RepairOrderForDealer|DealerPartsTransferOrder', '生成配件调拨单', 2);


INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|Add', '新增', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|Edit', '修改', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|Abandon', '作废', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|Detail', '查看明细', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|RepairsCompleted', '维修完工', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|GenerateClaims', '生成索赔', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|GenerateOuterPurchase', '生成外采申请', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|GenerateSell', '生成紧急销售订单', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|OutClaims', '生成外出索赔', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|RepairOrderPrint', '打印维修单', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|Print', '打印结算单', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|PrintBillMember', '打印结算单(会员)', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|RepairOrderPrintNew', '打印维修单(新)', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|Reject', '驳回修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|MVSPicture', '查看照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|MVSVideo', '查看视频', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|MVSRoute', '查看外出轨迹', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|MergeExport', '合并导出', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|APPPicture', '查看APP照片', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|APPRoute', '查看APP轨迹', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|MemberInformation', '会员管理', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|MemberInformationRepeat', '重发会员', 2);

INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8404, 'Common|Add', '新增', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8404, 'Common|Edit', '修改', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8404, 'Common|Submit', '提交', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8404, 'Common|Detail', '查看明细', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8404, 'Common|Abandon', '作废', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8404, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8404, 'RepairOrderForSecond|MVSPicture', '查看照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8404, 'RepairOrderForSecond|MVSVideo', '查看视频', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8404, 'RepairOrderForSecond|MVSRoute', '查看外出轨迹', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8404, 'RepairOrderForSecond|APPPicture', '查看APP照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8404, 'RepairOrderForSecond|APPRoute', '查看APP轨迹', 2);

INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8406, 'RepairWorkOrderForDealer|Close', '关闭', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8406, 'RepairWorkOrderForDealer|NewRepairOrder', '生成维修单', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8406, 'RepairWorkOrderForDealer|NewBeyoundCheck', '生成售前检查', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8406, 'RepairWorkOrderForDealer|WorkOrderFeedback', '工单反馈', 2);

INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8407, 'DTMRepairContract|RepairOrderRegistration', '保内维修单登记', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8407, 'DTMRepairContract|RepairOrderWarranty', '保外维修单登记', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8408, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8408, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8408, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8408, 'Common|Export', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8408, 'Common|Import', '导出', 2);

INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8506, 'Common|Add', '新增', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8506, 'Common|Edit', '修改', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8506, 'Common|Submit', '提交', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8506, 'Common|Abandon', '作废', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8506, 'Common|Export', '导出', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8506, 'Common|Detail', '查看明细', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8503, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8503, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8503, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8503, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8503, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8601, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8601, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8601, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8601, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8601, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8501, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8501, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8501, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8501, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8501, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8501, 'Common|View', 'APP照片查看', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8602, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8602, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8602, 'Common|MVSPicture', '查看照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8602, 'Common|MVSVideo', '查看视频', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8602, 'Common|MVSRoute', '查看外出轨迹', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8502, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8502, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8502, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8502, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8502, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8603, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8603, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8603, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8603, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8603, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8504, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8504, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8504, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8504, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8504, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8604, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8604, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8604, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8604, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8604, 'Common|Detail', '查看明细', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8505, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8505, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8505, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8505, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8505, 'Common|Detail', '查看明细', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8605, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8605, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8605, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8605, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8605, 'Common|Detail', '查看明细', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8606, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8606, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8606, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8606, 'PartsClaimOrderNewForDealer|Shipping', '发运', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8606, 'PartsClaimOrderNewForDealer|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8606, 'PartsClaimOrderNewForDealer|LabelPrint', '标签打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8606, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8606, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8609, 'IntegralClaimBillForDealer|Detail', '查看详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8609, 'IntegralClaimBillForDealer|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8609, 'IntegralClaimBillForDealer|APPPicture', '查看APP照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8609, 'IntegralClaimBillForDealer|APPRoute', '查看APP轨迹', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8801, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8801, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8801, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8801, 'Common|BatchApproval', '批量审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8801, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8801, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8801, 'SsClaimSettlementBill|ConvertFunds', '转配件款', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8801, 'SsClaimSettlementBill|InvoiceRegister', '登记发票', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8801, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8801, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8801, 'Common|ExportDetail', '导出清单', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8802, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8802, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8802, 'Common|ExportDetail', '导出清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8802, 'Common|InvoiceRegister', '登记发票', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8803, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8803, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8811, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8811, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8811, 'Common|ExportDetail', '导出清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8811, 'Common|InvoiceRegister', '登记发票', 2);

--insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'Common|Add', '新增', 2);
--insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'Common|Edit', '修改', 2);
--insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'Common|Approve', '审批', 2);
--insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'Common|BatchApproval', '批量审批', 2);
--insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'Common|Abandon', '作废', 2);
--insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'Common|Detail', '查看明细', 2);
--insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'Common|Export', '导出', 2);
--insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'Common|ExportDetail', '导出清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'DealerInvoiceInformation|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'DealerInvoiceInformation|FinalApprove', '终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'DealerInvoiceInformation|CompletePayment', '付款完成', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'DealerInvoiceInformation|BatchInitialApprove', '批量初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'DealerInvoiceInformation|BatchFinalApprove', '批量终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'Common|Detail', '查看详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8805, 'Common|Print', '打印', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8806, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8806, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8806, 'Common|ExportDetail', '导出清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8806, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8806, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8806, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8806, 'Common|BatchApproval', '批量审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8806, 'Common|Add', '新增', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8810, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8810, 'IntegralClaimInvoiceInfo|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8810, 'IntegralClaimInvoiceInfo|FinalApprove', '终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8810, 'IntegralClaimInvoiceInfo|CompletePayment', '付款完成', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8810, 'IntegralClaimInvoiceInfo|BatchInitialApprove', '批量初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8810, 'IntegralClaimInvoiceInfo|BatchFinalApprove', '批量终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8810, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8810, 'Common|Detail', '查看详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8810, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8810, 'Common|Print', '打印', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'Common|Pause', '停用', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'Common|Resume', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ReplaceName', '批量替换配件名称', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ReplaceSpecification', '批量替换规格型号', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ReplaceEnglishName', '批量替换英文名称', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ReplacePartType', '批量替换配件类型', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ReplaceFeature', '批量替换特征说明', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ReplaceReferenceCode', '批量替换零部件图号', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ReplaceMInPackingAmount', '批量替换最小包装数量', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ReplaceNextSubstitute', '批量替换下一替代件', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ReplaceIMSCompressionNumber', '批量替换IMS压缩号', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ReplaceIMSManufacturerNumber', '批量替换IMS厂商号', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ReplaceProductBrand', '批量替换产品商标', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ReplaceStandardName', '批量替换标准名称', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ReplaceImportEdit', '批量导入修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ImportEdit', '批量导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ResumeEdit', '批量恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ReplaceOverseasPartsFigure', '批量替换海外配件图号', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ImportGoldenTaxClassify', '批量导入修改金税分类编码', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ExportNoGoldenTaxClassify', '无金税分类导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|StandardImport', '批量替换产品执行标准代码', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ImportExchangeIdentification', '批量替换配件互换识别号', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ImportFactury', '批量替换厂商', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ImportImportCategoryCode', '批量替换配件分类编码', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ReplaceWarrantyTransfer', '批量替换保外调拨', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|Upload', '附件上传', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2104, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2104, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2104, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2104, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2103, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2103, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2103, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2103, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2103, 'Common|Import', '导入', 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2105, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2105, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2105, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2105, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2105, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2105, 'Common|ExportByPrice', '价格不一致导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2106, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2107, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2108, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2108, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2108, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2108, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2109, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2110, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2111, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2111, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2111, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2111, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2112, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2112, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2112, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2112, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2112, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2113, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2113, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2113, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2113, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2113, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2113, 'Common|BatchImport', '批量导入修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|Purchasing', '编辑采购属性', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|Marketing', '编辑销售属性', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|Serviceing', '编辑服务属性', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|ImportEdit', '导入修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|ReplaceIsOrderable', '批量替换可否采购', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|ReplaceIsSalableAndDirectSupply', '批量替换可否销售及直供', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|ReplaceIncreaseRateGroup', '批量替换加价率分组', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|ReplacePartsWarrantyCategory', '批量替换保修分类', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|ReplacePartsReturnPolicy', '批量替换旧件返回政策', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|ReplaceLossTypeAndStockLimit', '批量替换损耗类型及高低限', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|ReplaceRemark', '批量替换备注', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|ExportNoGoldenTaxClassifyMsg', '导出无金税信息的数据', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|ReplaceApproveLimit', '批量替换审核上限', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|ReplaceRepairClassify', '批量导入修改配件保修分类', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|PauseEdit', '批量停用', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|ResumeEdit', '批量恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|AbandonEdit', '批量作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'Common|Pause', '停用', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'Common|Resume', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102,  'Common|PrintLabel', '标签打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102,  'Common|UsedPrintLabel', '标签打印(旧)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102,  'Common|WmsLabel', 'wms标签打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102,  'Common|UsedWmsLabel', 'wms标签打印(旧)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'Common|StandardPrintLabel', '标签打印（带标准）', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'Common|WmsLabelOM', 'wms标签打印（欧曼）', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|ReplacePartABC', '批量导入配件ABC类型', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1102, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1102, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1102, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1102, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1102, 'Common|Pause', '停用', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1102, 'Common|Resume', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1102, 'Common|Detail', '详细信息', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2202, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2202, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2202, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2202, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2202, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2202, 'Common|ImportUpdate', '修改导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2204, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2204, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2204, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2204, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2204, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2204, 'Workflow|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2204, 'Workflow|FinalApprove', '终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2204, 'Common|ExportDetail', '导出清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2205, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2203, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2201, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2201, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2201, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2201, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2201, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2201, 'Common|ImportUpdate', '导入修改', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'Workflow|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'Workflow|FinalApprove', '终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'PartsPurchasePricingChange|BatchInitialApprove', '批量初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'PartsPurchasePricingChange|BatchFinalApprove', '批量终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'Common|ExportDetail', '导出清单', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1103, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1103, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1103, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1103, 'Common|Pause', '停用', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1103, 'Common|Resume', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1103, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1103, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1201, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1201, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1201, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1201, 'Common|Pause', '停用', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1201, 'Common|Resume', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1201, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1101, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1101, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1101, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1101, 'Common|Pause', '停用', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1101, 'Common|Resume', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1101, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1101, 'Common|ExportDetail', '导出详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1203, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1203, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1203, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1203, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1203, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1206, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1206, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1206, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1206, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1105, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1105, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1105, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1105, 'Common|Pause', '停用', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1105, 'Common|Resume', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1105, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1104, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1104, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1104, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1104, 'Common|Pause', '停用', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1104, 'Common|Resume', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1104, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1104, 'ResponsibleUnit|Setup', '设置', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1106, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1106, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1106, 'Common|Delete', '删除', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1106, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1106, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7101, 'Common|Add', '新增', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7102, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7102, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7101, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7101, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7101, 'ServiceProductLine|Setup', '设置', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7103, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7103, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7103, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7103, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7104, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7104, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7104, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7104, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7110, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7110, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7110, 'Common|Detail', '查看详细', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5101, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5101, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5101, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5101, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5102, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5102, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5102, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5102, 'WarehouseArea|ImportWarehouse', '导入库区', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5102, 'WarehouseArea|ImportArea', '导入库位', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5102, 'WarehouseArea|Print', '打印库位', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5103, 'PartsLocationAssignment|ImportArea', '导入库位分配', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5104, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5105, 'SYNCWMSINOUTLOGINFO|SetupState', '手工设置状态', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5105, 'SYNCWMSINOUTLOGINFO|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5204, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5204, 'Common|HistoricalStorage', '查询历史入库', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5204, 'Common|HistoricalDataBase', '查询历史出库', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5204, 'Common|HistoricalStorage', '查询历史出入库', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5204, 'Common|LocketUnit', '查询锁定单位', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5204, 'Common|PartsStockSync', '全量库存同步', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5205, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5206, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5207, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5208, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5208, 'VehiclePartsStockLevel|ImportEdit', '导入修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5208, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5208, 'VehiclePartsStockLevel|Invalid', '作废', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5209, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5301, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5301, 'Common|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5301, 'PartsInboundCheckBill|CheckInbound', '检验入库', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5301, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5301, 'Common|PrintLabel', '标签打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5301, 'Common|UsedPrintLabel', '标签打印(旧)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5301, 'Common|WMSPrintLabel', 'WMS标签打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5301, 'Common|UsedWMSPrintLabel', 'WMS标签打印(旧)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5301, 'Common|Partition', '分区打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5301, 'Common|StandardPrintLabel', '标签打印（带标准）', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5302, 'PartsPacking|PackingDetail', '查询已包装明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5302, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5304, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5304, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5304, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5304, 'Common|PrintLabel', '标签打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5304, 'Common|OMWMSPrintLabel', 'WMS标签打印(欧曼)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5304, 'Common|WMSPrintLabel', 'WMS标签打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5304, 'Common|Partition', '分区打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5304, 'Common|StandardPrintLabel', '标签打印（带标准）', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5305, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5305, 'PartsInboundCheckBillQuery|PackingDetail', '查看包装清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5305, 'PartsInboundCheckBillQuery|PrintForGC', '工程车打印(新)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5305, 'PartsInboundCheckBillQuery|NotSettle', '不可结算', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5305, 'PartsInboundCheckBillQuery|Settle', '可结算', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5305, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5305, 'Common|PrintLabel', '打印标签', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5305, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5305, 'PartsInboundCheckBillQuery|ChPrint', '箱标中文打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5305, 'PartsInboundCheckBillQuery|EnPrint', '箱标英文打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5305, 'PartsInboundCheckBillQuery|PcPrint', '件标打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5306, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5306, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5306, 'Common|Print', '打印', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5307, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5307, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5307, 'Common|PrintForGC', '工程车打印', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5308, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5309, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5310, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5311, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5311, 'PartsInboundCheckBillQuery|PackingDetail', '查看包装清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5311, 'PartsInboundCheckBillQuery|PrintForGC', '工程车打印(新)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5311, 'PartsInboundCheckBillQuery|NotSettle', '不可结算', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5311, 'PartsInboundCheckBillQuery|Settle', '可结算', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5311, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5311, 'Common|PrintLabel', '打印标签', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5311, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5312, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5312, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5312, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5312, 'Common|PrintLabel', '标签打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5312, 'Common|WMSPrintLabel', 'WMS标签打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5312, 'Common|Partition', '分区打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5312, 'Common|StandardPrintLabel', '标签打印（带标准）', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5313, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5313, 'Common|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5313, 'PartsInboundCheckBill|CheckInbound', '检验入库', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5313, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5313, 'Common|PrintLabel', '标签打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5313, 'Common|UsedPrintLabel', '标签打印(旧)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5313, 'Common|WMSPrintLabel', 'WMS标签打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5313, 'Common|UsedWMSPrintLabel', 'WMS标签打印(旧)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5313, 'Common|Partition', '分区打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5313, 'Common|StandardPrintLabel', '标签打印（带标准）', 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 5314,'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 5314,'PackingTask|Packing', '包装登记', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.nextval, 5315, 'ReceiptManagement|Receipt', '收货', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.nextval, 5315, 'ReceiptManagement|Mandatory', '强制完成', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.nextval, 5315, 'ReceiptManagement|Print', '打印收货指导单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5315,'ReceiptManagement|BatchReceipt', '批量收货', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.nextval, 5315, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 5316,'PartsShelvesTask|Shelves', '上架', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 5316,'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS) values (S_Action.Nextval, 5316,'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5405, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5401, 'PartsOutboundBill|DeliverParts', '出库', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5401, 'PartsOutboundBill|PartitionPrint', '分区打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5404, 'PartsOutboundBillReturn|ReturnBound', '退库', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5401, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5401, 'Common|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5401, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5402, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5402, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5402, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5402, 'Common|PartitionPrint', '分区打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5402, 'PartsOutboundPlanQuery|Packing', '生成捡货任务', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5402,'Common|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5403, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5403, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5403, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5403, 'PartsOutboundBillQuery|AgencyPrint', '代理库打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5403, 'PartsOutboundBillQuery|PrintForGC', '工程车打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5403, 'PartsOutboundBillQuery|PrintResume', '打印恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5409, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5409, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5409, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5410, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5410, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5410, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5411,'BoxUpTask|Create',  '生成装箱任务',  2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5411,'BoxUpTask|BoxUp',  '装箱',  2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5411,'BoxUpTask|Stop',  '终止',  2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5411,'Common|Abandon','作废',  2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5411,'Common|MergeExport',  '合并导出',  2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5411,'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5412,'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5412,'PickingTask|Picking', '拣货', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5412,'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5412,'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5413,'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5413,'PickingTask|Allot', '分配', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5701, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5701, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5701, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5701, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5701, 'PartsTransferOrder|TransferKanBan', '调拨看板', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5701, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5601, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5601, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5601, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5601, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5603, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5603, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5603, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5603, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5603, 'PartsInventoryBillReported|ViewDetail', '查看清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5603, 'PartsInventoryBillReported|ResultsInput', '结果录入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5603, 'PartsInventoryBillReported|InventoryCoverage', '库存覆盖', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5603, 'PartsInventoryBillReported|DetailExport', '清单导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5604, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5604, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5604, 'AgentsPartsInventoryBill|ViewDetail', '查看清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5604, 'AgentsPartsInventoryBill|InventoryCoverage', '库存覆盖', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5604, 'AgentsPartsInventoryBill|ResultsInput', '结果录入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5604, 'AgentsPartsInventoryBill|DetailExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5604, 'AgentsPartsInventoryBill|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5605, 'Common|Approve', '审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5605, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5605, 'DealerPartsInventoryBill|DetailExport', '清单导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5606, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5606, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5606, 'DealerPartsInventoryBill|DetailExport', '清单导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5606, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5606, 'Common|Approve', '审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5607, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5607, 'Common|Detail', '查看清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5607, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5607, 'PartsInventoryBill|InventoryCoverage', '库存覆盖', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5607, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5608, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5608, 'Common|Detail', '查看清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5608, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5608, 'PartsInventoryBill|InventoryCoverage', '库存覆盖', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5608, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5408, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5203, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5203, 'DealerPartsStock|InAndOutRecordQuery', '出入库记录查询', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4401, 'OverstockPartsInformation|Remove', '清除', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4401, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4402, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4403, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4403, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4403, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4403, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4403, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4403, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4404, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4404, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4404, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4404, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4404, 'Common|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4405, 'OverstockPartsAdjustBill|Confirm', '确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4405, 'OverstockPartsAdjustBill|Complete', '完成', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4405, 'OverstockPartsAdjustBill|AdjustKanban', '调剂看板', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4405, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4405, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4406, 'OverstockPartsAdjustBillForComplete|Complete', '完成', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4406, 'OverstockPartsAdjustBillForComplete|AdjustKanban', '调剂看板', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4406, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4406, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5202, 'Common|SchedulerExport', '下载', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5202, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5202, 'PartsStockDetailQuery|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5202, 'PartsStockDetailQuery|Delete', '删除', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5202, 'PartsStockDetailQuery|HistoricalStorage', '历史出入库记录查询', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5501, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5501, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5501, 'Common|MainEdit', '主单修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5501, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5501, 'PartsShippingOrder|ReceiptConfirm', '回执确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5501, 'PartsShippingOrder|PrintNoPrice', '打印（无价格）', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5501, 'PartsShippingOrder|TransportLossesDispose', '路损处理', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5501, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5501, 'Common|FDPrint', '打印-福戴', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5501, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5501, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5501, 'Common|ExportCustomerInfo', '导出客户信息', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5501, 'PartsShippingOrder|GPSLocationQuery', 'GPS定位器设备查询', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (s_action.nextval, 5501, 'PartsShippingOrder|ShippingConfirm', '发运确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5502, 'PartsShippingOrderForConfirm|ReceivingConfirm', '收货确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5502, 'PartsShippingOrderForConfirm|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5502, 'PartsShippingOrderForConfirm|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5502, 'PartsShippingOrderForConfirm|GPSLocationQuery', 'GPS定位器设备查询', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5504, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5504, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5504, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5504, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5504, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5505, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5505, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5505, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5505, 'Common|FDPrint', '打印-福戴', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5505, 'PartsShippingOrderForLogistic|SendingConfirm', '发货确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5505, 'PartsShippingOrderForLogistic|SendingConfirmBatch', '发货确认（批量）', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5505, 'PartsShippingOrderForLogistic|SendConfirm', '送达确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5505, 'PartsShippingOrderForLogistic|SendConfirmBatch', '送达确认（批量）', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5505, 'PartsShippingOrderForLogistic|ExtendedBackConfirm', '超期或电商运单流水反馈', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5505, 'PartsShippingOrderForLogistic|GPSLocationRegister', 'GPS定位器设备号登记', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5505, 'PartsShippingOrderForLogistic|GPSLocationQuery', 'GPS定位器设备查询', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5505, 'PartsShippingOrderForLogistic|PrintNoPrice', '打印（无价格）', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5511, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5511, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5511, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5511, 'Common|Detail', '查看详细', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5512, 'Common|SendingConfirm', '发货确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5512, 'Common|SendingConfirmBatch', '发货确认（批量）', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5512, 'Common|SendConfirm', '送达确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5512, 'Common|SendConfirmBatch', '送达确认（批量）', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5512, 'Common|ExtendedBackConfirm', '超期或电商运单流水反馈', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5512, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5512, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5512, 'Common|GPSLocationRegister', 'GPS定位器设备号登记', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5512, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5512, 'Common|Sending', '发货', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5512, 'Common|Send', '送达', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5512, 'Common|ReceivingConfirm', '收货确认', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1109, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1109, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1109, 'Common|Pause', '停用', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1109, 'Common|Resume', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1109, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1109, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1109, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1108, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1108, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1108, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1108, 'Common|Export', '导出', 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5508, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5509, 'LogisticsTracking|Input', '录入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5509, 'LogisticsTracking|InputByDeputy', '代录入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5509, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5509, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5510, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5510, 'Common|MergeExport', '合并导出', 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6101, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6101, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6101, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6101, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6101, 'PlannedPriceApp|WarehouseDetail', '查看仓库详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6101, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6101, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6101, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6101, 'Common|Abandon', '作废', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6102, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6103, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6103, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6103, 'Common|Abandon', '作废', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 6103, 'Common|HistoricalDataBase', '查询历史库存', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6103, 'Common|HistoryStockSum', '历史库存查询-汇总', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|BatchApproval', '批量审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'PlannedPriceApp|WarehouseDetail', '查看仓库详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|Abandon', '作废', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6106, 'AccountPeriod|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6106, 'AccountPeriod|Close', '关闭', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6106, 'AccountPeriod|Open', '开启', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6106, 'AccountPeriod|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6201, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6201, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6201, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6201, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6201, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6201, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6201, 'Common|InitialApprove', '初审', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6202, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6202, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6202, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6202, 'Common|Pause', '停用', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6202, 'Common|Resume', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6203, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6203, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6203, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6203, 'Common|Audit', '审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6204, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6204, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6204, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6204, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6204, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6204, 'PaymentBill|Beneficiary', '来款分割', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6204, 'Common|BatchApproval', '批量审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6205, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,6206, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,6207, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6208, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6208, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6208, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6208, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6208, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6208, 'Common|BatchApproval', '批量审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6208, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6210, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6211, 'Common|SetStatus', '手工设置状态', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6212, 'SAPYXPaymentInfo|SetStatus', '手工设置状态', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6213, 'Common|SetStatus', '手工设置状态', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6214, 'Common|SetStatus', '手工设置状态', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6215, 'Common|SetStatus', '手工设置状态', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,6216, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,6217, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,6218, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6219, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6219, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6219, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6219, 'Common|Confirm', '确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6219, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6219, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6219, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6219, 'Common|Detail', '详细信息', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6219, 'Common|Print', '打印', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6301, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6301, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6301, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6301, 'Common|Abandon', '作废', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6302, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6401, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6401, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6401, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6402, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6402, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6402, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6402, 'Common|Approve', '审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6402, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6402, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6402, 'Common|BatchApproval', '批量审核', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6403, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6404, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6405, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7105, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7105, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7105, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7106, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7106, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7106, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7106, 'Common|Pause', '停用', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7106, 'Common|Resume', '恢复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7106, 'Common|Detail', '详细信息', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7107, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7107, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7107, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7108, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7108, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7108, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7109, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7201, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7201, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7201, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7201, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7202, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7202, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7202, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7203, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7203, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7203, 'Common|StandardEdit', '外出标准维护', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7203, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7203, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7205, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7205, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7205, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7205, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7206, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7206, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7206, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7207, 'Common|Detail', '详细信息', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7302, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7302, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7302, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7302, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7302, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7303, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7303, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7303, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7303, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7301, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7301, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7301, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7301, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7301, 'Common|Import', '是否ISG批量维护', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7304, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7304, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7304, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7304, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7304, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7305, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7305, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7305, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7305, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7305, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7305, 'Common|BatchAbandon', '批量作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7305, 'Common|ExportEdit', '导入修改工时定额', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7306, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7306, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7306, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7306, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7307, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7307, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7307, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7307, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7308, 'Common|Detail', '查看详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7309, 'Common|Detail', '查看详细', 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7401, 'RetainedCustomerVehicleList|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7401, 'RetainedCustomerVehicleList|DealerEdit', '服务站修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7401, 'RetainedCustomerVehicleList|BranchEdit', '分公司修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7401, 'RetainedCustomerVehicleList|ChangetheOwner', '变更车主', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7401, 'RetainedCustomerVehicleList|EditVIN', '修改VIN', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7401, 'RetainedCustomerVehicleList|HistoryOwners', '历史车主', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7401, 'RetainedCustomerVehicleList|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7401, 'RetainedCustomerVehicleList|InformationEdit', '雷萨上装信息维护', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7401, 'RetainedCustomerVehicleList|VehicleLinkmanEdit', '维护车辆联系人', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7401, 'RetainedCustomerVehicleList|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7401, 'RetainedCustomerVehicleList|SetIsGuarantee', '是否保修修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7401, 'RetainedCustomerVehicleList|ExportIsGuarantee', '批导修改是否保修', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7401, 'RetainedCustomerVehicleList|ImportVip', '批量维护VIP车辆', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7401, 'RetainedCustomerVehicleList|ImportCustomerVehicle', '批量维护车队客户车辆', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7402, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7402, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7402, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7402, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7402, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7403, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7403, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7403, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7403, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7403, 'CustomerInfo|ServiceEdit', '服务站修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7403, 'CustomerInfo|ImportEdit', '批量导入', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7404, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7405, 'DTPVehicleinformation|ReTreatment', '重新处理', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7405, 'DTPVehicleinformation|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7406, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7406, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7406, 'Common|Import', '导入', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7407, 'Common|Detail', '查看详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7407, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7408, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7408, 'MemberInfo|VeriCodeEffectTime', '调整验证码有效时间', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7501, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7502, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7503, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7504, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7505, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7506, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7507, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8701, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8701, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8701, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8701, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8702, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8703, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8703, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8703, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8703, 'Common|Import', '批量导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8703, 'Common|ImportEdit', '批量导入修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8703, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1311, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1311, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1311, 'Common|Abandon', '作废', 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1313, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5602, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5602, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5602, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5602, 'Common|Detail', '查看清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5602, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5602, 'PartsInventoryBill|ResultsInput', '结果录入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5602, 'PartsInventoryBill|InventoryCoverage', '库存覆盖', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5602, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5602, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5602, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5602, 'PartsInventoryBill|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5702, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5702, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5702, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5702, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5703, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5703, 'Common|Approve', '审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5703, 'Common|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5703, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5703, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5704, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5704, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5704, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5704, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5704, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5704, 'Common|MergeExport', '合并导出', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8106, 'Common|Add', '新增', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8106, 'Common|Edit', '修改', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8106, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8106, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8106, 'Common|Import', '导入', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 1107, 'Common|Add', '新增', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 1107, 'Common|Edit', '修改', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 6304, 'Common|Add', '新增', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 6304, 'Common|Edit', '修改', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 6304, 'Common|Abandon', '作废', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 6304, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1204, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1204, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1204, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1204, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1204, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1207, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1207, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1207, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1207, 'Common|Export', '导出', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 4304, 'Common|Approve', '审批', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 4304, 'Common|Abandon', '作废', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 4304, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1208, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1208, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1208, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1208, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1209, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1209, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1209, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1210, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1210, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1210, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1210, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 16402, 'PersonnelSupplierRelation|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 16402, 'PersonnelSupplierRelation|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 16402, 'PersonnelSupplierRelation|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 16402, 'PersonnelSupplierRelation|Import',  '导入',  2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 16402, 'PersonnelSupplierRelation|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1202, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1202, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1202, 'Common|Delete', '删除', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1202, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4107, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4107, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4107, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4107, 'Common|Submit', '提交', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4106, 'Common|Audit', '审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4106, 'Common|Terminate', '终止', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3204, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3204, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3204, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3204, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3204, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3204, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3204, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3204, 'PartsPurchaseSettleBillForSupplier|InvoiceRegister', '发票登记', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3204, 'PartsPurchaseSettleBillForSupplier|InvoiceEdit', '发票修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3204, 'PartsPurchaseSettleBillForSupplier|PrintForGC', '工程车打印', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8405, 'Common|Add', '新增', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8405, 'Common|Edit', '修改', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8405, 'Common|Submit', '提交', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8405, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8405, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8405, 'Common|Abandon', '作废', 2);
INSERT INTO Action (ID, PAGEID, OPERATIONID, NAME, Status) VALUES (S_Action.Nextval, 8104, 'Common|Add', '新增', 2);
INSERT INTO Action (ID, PAGEID, OPERATIONID, NAME, Status) VALUES (S_Action.Nextval, 8104, 'Common|Edit', '修改', 2);
INSERT INTO Action (ID, PAGEID, OPERATIONID, NAME, Status) VALUES (S_Action.Nextval, 8104, 'Common|Detail', '查看明细', 2);
INSERT INTO Action (ID, PAGEID, OPERATIONID, NAME, STATUS) VALUES (S_Action.Nextval, 8104, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8306, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8306, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8306, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8306, 'Common|BatchApproval', '批量审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8306, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8306, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8306, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8306, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8307, 'ServiceTripClaimBillForSupplier|SupplierConfirm', '供应商确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8307, 'ServiceTripClaimBillForSupplier|SupplierCheck', '供应商复核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8307, 'ServiceTripClaimBillForSupplier|Detail', '查看详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8307, 'ServiceTripClaimBillForSupplier|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8307, 'ServiceTripClaimBillForSupplier|ClaimBillDetail', '查看维修索赔单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8307, 'ServiceTripClaimBillForSupplier|APPPicture', '查看APP照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8307, 'ServiceTripClaimBillForSupplier|APPRoute', '查看APP轨迹', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8311, 'RepairClaimBillForSupplier|SupplierConfirm', '供应商确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8311, 'RepairClaimBillForSupplier|SupplierCheck', '供应商复核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8311, 'RepairClaimBillForSupplier|Detail', '查看详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8311, 'RepairClaimBillForSupplier|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8311, 'RepairClaimBillForSupplier|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8311, 'RepairClaimBillForSupplier|APPPicture', '查看APP照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8311, 'RepairClaimBillForSupplier|APPRoute', '查看APP轨迹', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8602, 'RepairClaimBillForDealer|APPPicture', '查看APP照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8602, 'RepairClaimBillForDealer|APPRoute', '查看APP轨迹', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8312, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8808, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8808, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8808, 'Common|Approve', '审核生成扣补款', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8808, 'Common|BatchApproval', '批量审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8808, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8808, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8316, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8316, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8316, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8316, 'Common|BatchApproval', '批量审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8316, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8316, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8316, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8316, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8317, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8317, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8317, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8317, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8317, 'Common|Detail', '查看明细', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8308, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8314, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8309, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8309, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8309, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8309, 'Common|Abandon', '作废', 2)
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8309, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8309, 'Common|BatchApproval', '批量审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8309, 'Common|Import', '导入', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8310, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8310, 'Common|Abandon', '作废', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8105, 'Common|Audit', '审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8105, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8105, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8105, 'Common|BatchAudit', '批量审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8105, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8105, 'Common|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8105, 'Common|Detail', '显示详情', 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 12101, 'Notification|Top',
 '置顶',2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 12101, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 12101, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 12101, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 12101, 'Common|Edit', '修改', 2)
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 12101, 'Common|Reply', '文字回复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 12101, 'Common|Upload', '附件上传', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 12102, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 12102, 'Common|Reply', '文字回复', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 12102, 'Common|Upload', '附件上传', 2);
--Demo节点。只做演示--会删除
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (99999, 8400, 2, 'RepairOrderForDealerDemo', 'Released', NULL, '维修单管理-服务站demo', '管理维修单信息', 'Client/DCS/Images/Menu/Service/RepairOrderForDealer.png', 10, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (99998, 8400, 2, 'RepairClaimApplicationForDealerDemo', 'Released', NULL, '维修索赔申请管理-服务站demo', '填制、修改、提交三包维修索赔预申请单', 'Client/DCS/Images/Menu/Service/RepairClaimApplication.png',11, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (99997,8400, 2, 'ServiceTripClaimApplicationForDealerDemo', 'Released', NULL, '外出索赔申请管理-服务站demo', '填制、修改、提交外出索赔申请单', 'Client/DCS/Images/Menu/Service/ServiceTripClaimApplication.png',12, 2);
--INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (99996,8400, 2, 'RepairClaimBillDemo', 'Released', NULL, '维修索赔管理demo', '审核三包维修索赔单', 'Client/DCS/Images/Menu/Service/RepairClaimBill.png', 13, 2);

INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 99999, 'RepairOrderForDealerDemo|NewAdd', '维修单登记', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 99999, 'RepairOrderForDealerDemo|Add', '来客登记', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 99999, 'RepairOrderForDealerDemo|Edit', '修改', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 99999, 'RepairOrderForDealerDemo|Abandon', '作废', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 99999, 'RepairOrderForDealerDemo|Detail', '查看明细', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 99999, 'RepairOrderForDealerDemo|RepairsCompleted', '维修完工', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 99999, 'RepairOrderForDealerDemo|RepairsCompletedPicking', '维修完工(指定领料)', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 99999, 'RepairOrderForDealerDemo|Print', '打印', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 99999, 'RepairOrderForDealerDemo|Reject', '驳回修改', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 99999, 'RepairOrderForDealerDemo|GenerateClaims', '生成索赔', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 99999, 'RepairOrderForDealerDemo|GenerateOuterPurchase', '生成外采申请', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 99999, 'RepairOrderForDealerDemo|GenerateSell', '生成紧急销售订单', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 99998, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 99998, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 99998, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 99998, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 99998, 'Common|Detail', '查看明细', 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 99997, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 99997, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 99997, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 99997, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 99997, 'Common|Detail', '查看明细', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 99996, 'RepairClaimBillDemo|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 99996, 'RepairClaimBillDemo|FinalApprove', '终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 99996, 'RepairClaimBillDemo|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 99996, 'RepairClaimBillDemo|SupplierInitialApprove', '供应商初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 99996, 'RepairClaimBillDemo|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 99996, 'RepairClaimBillDemo|Detail', '详细信息', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 41999, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 41999, 'Common|BatchApproval', '批量审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 41999, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 41999, 'Common|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 41999, 'PartsSalesOrder|AddByDeputy', '新增(代做)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 41999, 'PartsSalesOrder|RevokeByDeputy', '撤单(代做)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 41999, 'PartsSalesOrder|RebateApprove', '折让审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 41999, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 15101, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 15101, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 15102, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2320, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6104, 'Common|Export', '导出', 2);

INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8103, 'RepairOrderForWarrantyBranch|APPPicture', '查看APP照片', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8103, 'RepairOrderForWarrantyBranch|APPRoute', '查看APP轨迹', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8107, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8107, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5402, 'Common|NotWarehousePrint', '未出库打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5402, 'Common|NotPartitionPrint', '未出库分区打印', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5401, 'PartsOutboundBill|NotPartitionPrint', '未出库分区打印', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Accept', '接受', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Refuse', '拒绝', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|SetOut', '出发', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Station', '到站', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Arrive', '到达', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|StartMaintenance', '开始维修', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|ProcessingCompleted', '处理完毕', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|HoursCompleted', '24小时是否可完成', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Revoke', '撤销', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|ViewDetails', '查看详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|VisitorRegistration', '来客登记', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|OursFinished', '24小时是否处理完毕', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8109, 'TransactionInterfaceLog|ManualProcessing', '手动处理', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8109, 'TransactionInterfaceLog|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5401, 'Common|NotPartitionPrint', '未出库分区打印', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Accept', '接受', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Refuse', '拒绝', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|SetOut', '出发', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Station', '到站', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Arrive', '到达', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|StartMaintenance', '开始维修', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|ProcessingCompleted', '处理完毕', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|HoursCompleted', '24小时是否可完成', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Revoke', '撤销', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|ViewDetails', '查看详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|VisitorRegistration', '来客登记', 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1321, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1321, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1321, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1321, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1322, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1322, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1322, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1322, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8313, 'IntegralClaimBill|Approve', '审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8313, 'IntegralClaimBill|BatchApprove', '批量审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8313, 'IntegralClaimBill|Detail', '查看详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8313, 'IntegralClaimBill|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8313, 'IntegralClaimBill|APPPicture', '查看APP照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8313, 'IntegralClaimBill|APPRoute', '查看APP轨迹', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8313, 'IntegralClaimBill|SettlementReport', '费用结算报表', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8313, 'IntegralClaimBill|SettlementPrint', '费用结算打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8313, 'IntegralClaimBill|Reject', '驳回', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8809, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8809, 'Common|Abandon', '作废', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4507, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4507, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4507, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4508, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|InvoiceRegister', '登记发票', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|AntiSettlement', '反结算', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|AppoveRebate', '审批（返利）', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|SettlementCost', '查看结算单成本', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4509, 'Common|PrintInvoice', '发票清单打印', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2114, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2114, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2114, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2114, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2114, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2114, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4116, 'AsapInterface|ManualProcessing', '手动处理', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4116, 'AsapInterface|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8111, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8111, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8111, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8111, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8111, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8111, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8112, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8112, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8112, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8112, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8112, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8113, 'Common|Audit', '审核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8113, 'Common|Reject', '驳回', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8113, 'Common|Print', '打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8113, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8113, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8113, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8114, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8114, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8114, 'Common|Commit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8114, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8114, 'Common|Print', '打印延保单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8114, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8114, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8114, 'Common|MergeExport', '合并导出', 2);

--保底库存管理
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 16403, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 16403, 'Common|Edit',  '修改',  2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 16403, 'Common|Abandon',  '作废',  2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 16403, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 16403, 'Common|Import', '导入', 2);



-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
