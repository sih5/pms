insert into Enterprise (ID, CODE, NAME, ENTERPRISECATEGORYID, ENTNODETEMPLATEID, STATUS, REMARK, CUSTOMPROPERTY1, CUSTOMPROPERTY2, CREATORID, CREATORNAME, CREATETIME, MODIFIERID, MODIFIERNAME, MODIFYTIME, ROWVERSION)
values (1, 'SDS', '上海晨阑数据技术有限公司', 0, 1, 3, '', '', '', null, '', null, null, '', null, '');
INSERT INTO Organization (Id, ParentId, Sequence, Code, Name, Type, Status, EnterpriseId, CreatorId, CreatorName, CreateTime) VALUES (S_Organization.Nextval, -1, 1, 'SDS', '上海晨阑数据技术有限公司', 0, 2, 1, 1, '系统管理员', sysdate);
INSERT INTO Role (Id, EnterpriseId, Name, IsAdmin, Status, CreatorId, CreatorName, CreateTime) VALUES (S_role.Nextval, 1, '系统管理员', 1, 1, 1, '系统管理员', sysdate);
INSERT INTO RolePersonnel (Id, RoleId, PersonnelId) VALUES (S_RolePersonnel.Nextval, 1, 1);
INSERT INTO Personnel (Id, EnterpriseId, LoginId, Name, Password, Status, CreatorId, CreatorName, CreateTime, PasswordModifyTime,CellNumber) VALUES (S_Personnel.Nextval, 1, 'admin', '系统管理员', '7C4A8D09CA3762AF61E59520943DC26494F8941B', 1, 1, '系统管理员', sysdate, sysdate,'13800000001');
INSERT INTO EntNodeTemplate (Id, code, name, Status, remark, enterpriseid, creatorid, creatorname,createtime) VALUES (S_EntNodeTemplate.Nextval, 'ALL', '所有权限', 2,'',1,1,'系统管理员',sysdate);
INSERT INTO EntNodeTemplateDetail select S_EntNodeTemplateDetail.Nextval,1,id from node;

insert into EnterpriseCategory (ID, CODE, NAME, STATUS, REMARK, CREATORID, CREATORNAME, CREATETIME, MODIFIERID, MODIFIERNAME, MODIFYTIME, ROWVERSION)
values (1, 'Branch', '分公司', 2, '', null, '', null, null, '', null, '');

insert into EnterpriseCategory (ID, CODE, NAME, STATUS, REMARK, CREATORID, CREATORNAME, CREATETIME, MODIFIERID, MODIFIERNAME, MODIFYTIME, ROWVERSION)
values (2, 'Dealer', '服务站', 2, '', null, '', null, null, '', null, '');

insert into EnterpriseCategory (ID, CODE, NAME, STATUS, REMARK, CREATORID, CREATORNAME, CREATETIME, MODIFIERID, MODIFIERNAME, MODIFYTIME, ROWVERSION)
values (3, 'Agency', '代理库', 2, '', null, '', null, null, '', null, '');

insert into EnterpriseCategory (ID, CODE, NAME, STATUS, REMARK, CREATORID, CREATORNAME, CREATETIME, MODIFIERID, MODIFIERNAME, MODIFYTIME, ROWVERSION)
values (4, 'LogisticCompany', '物流公司', 2, '', null, '', null, null, '', null, '');

insert into EnterpriseCategory (ID, CODE, NAME, STATUS, REMARK, CREATORID, CREATORNAME, CREATETIME, MODIFIERID, MODIFIERNAME, MODIFYTIME, ROWVERSION)
values (5, 'ResponsibleUnit', '责任单位', 2, '', null, '', null, null, '', null, '');

insert into EnterpriseCategory (ID, CODE, NAME, STATUS, REMARK, CREATORID, CREATORNAME, CREATETIME, MODIFIERID, MODIFIERNAME, MODIFYTIME, ROWVERSION)
values (6, 'PartsSupplier', '配件供应商', 2, '', null, '', null, null, '', null, '');

insert into EnterpriseCategory (ID, CODE, NAME, STATUS, REMARK, CREATORID, CREATORNAME, CREATETIME, MODIFIERID, MODIFIERNAME, MODIFYTIME, ROWVERSION)
values (7, 'Groups', '服务站兼代理库', 2, '', null, '', null, null, '', null, '');

insert into EnterpriseCategory (ID, CODE, NAME, STATUS, REMARK, CREATORID, CREATORNAME, CREATETIME, MODIFIERID, MODIFIERNAME, MODIFYTIME, ROWVERSION)
values (8, 'Groups', '集团企业', 2, '', null, '', null, null, '', null, '');

TRUNCATE TABLE Node
/
drop sequence  s_node;
create sequence s_node;
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
select S_Node.Nextval,x.* from 
(SELECT  0, Id, 2 FROM Page WHERE Type = 2 AND Status = 2 order by id) x;

INSERT INTO Node (Id, CategoryType, CategoryId, Status) 
select S_Node.Nextval,x.* from 
(SELECT  1, Id, 2 FROM Action WHERE  Status = 2 order by id) x;
/

-- 为所有IsAdmin角色增加Security权限
INSERT INTO Rule (Id, RoleId, NodeId)
SELECT S_Rule.Nextval, role.Id, Node.Id
  from node
 cross join role
 where ((node.CategoryType=0 and exists (select * from page where id < 1000 and page.Id=node.categoryid))
       or (node.CategoryType=1 and exists (select * from action where action.Id=node.categoryid and action.pageId in (select id from page where id < 1000))))
   and role.isadmin=1 and role.status=1
   and not exists (select * from rule where rule.nodeid=node.Id and rule.roleid=role.id)
