
  
BEGIN 
dbms_scheduler.create_job('"JOB_INSERTINTORDERMONTHLYBASE"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin
Prc_InsertIntOrderMonthlyBase;
end;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('13-NOV-2015 12.05.00.000000000 AM +08:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=> 
'Freq=Monthly;Interval=1;ByMonthDay=19'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.enable('"JOB_INSERTINTORDERMONTHLYBASE"');
COMMIT; 
END; 
