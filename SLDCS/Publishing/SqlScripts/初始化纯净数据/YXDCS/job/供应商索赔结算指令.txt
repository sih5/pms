
  
BEGIN 
dbms_scheduler.create_job('"供应商索赔结算指令"',
job_type=>'STORED_PROCEDURE', job_action=>
'ExecSupClaimSettleInstruction'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('09-MAR-2015 12.30.00.000000000 AM +08:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=> 
'Freq=Daily;Interval=1'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
COMMIT; 
END; 
