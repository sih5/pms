
  
BEGIN 
dbms_scheduler.create_job('"自动设置采购订单入库状态20点"',
job_type=>'STORED_PROCEDURE', job_action=>
'Autosetpurchaseorderinstatus_n'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('29-AUG-2014 08.30.00.000000000 PM +08:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=> 
'Freq=Daily;Interval=1'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.enable('"自动设置采购订单入库状态20点"');
COMMIT; 
END; 
