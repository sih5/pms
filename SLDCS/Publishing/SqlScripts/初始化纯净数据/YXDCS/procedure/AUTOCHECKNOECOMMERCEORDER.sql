create or replace procedure AutoCheckNoEcommerceOrder(PartCheckId number) is
  type outboundbilldetailset IS TABLE OF tempPartsOutboundBillDetail%rowtype INDEX BY binary_integer;
  Vtable_outboundbilldetail outboundbilldetailset;


  num     number;
  nP      number;
  nRow    number;
  i       number;
  j       number;
  nW      number;
  nBP     number;
  nli     number;
  nCreatorID number(9);
  vCreatorName Varchar2(50);

  nTestAmount             NUMBER(19,4);

  -- 用到销售订单表(PartsSalesOrder)中字段
  vCode                    Varchar2(50);
  nCustomerAccountId       number(9);
  iBranchId                Integer;
  vBranchCode              VARCHAR2(50);
  vBranchName              VARCHAR2(100);
  iSalesCategoryId         Integer;
  vSalesCategoryName       VARCHAR2(100);
  nAvailbleAmount          NUMBER(19,4);
  nTotalAmount             NUMBER(19,4);
  nIsautoapprove           NUMBER(1);
  vAutoapprovecomment      VARCHAR2(2000);
  dAutoapprovetime         DATE;
  iSubmitCompanyId         Integer;
  dRequestedShippingTime   Date;
  dRequestedDeliveryTime   date;
  iProvinceid              Integer;
  iPartsSalesOrderTypeId   Integer;
  vPartsSalesOrderTypeName VARCHAR2(100);
  iReceivingcompanyid      Integer;
  vReceivingcompanycode    VARCHAR2(50);
  vReceivingcompanyname    VARCHAR2(100);
  iReceivingwarehouseid    Integer;
  vReceivingwarehousecode  Varchar2(50);
  iShippingMethod          Integer;
  iOriginalrequirementbillid Integer;
  iOriginalrequirementbilltype Integer;
  iPartsSalesOrderStatus   Integer;
  oldrowversion              timestamp;
  newrowversion              timestamp;
  nSumTotalAmount          NUMBER(19,4);
  vRemark                  Varchar2(200);
  vCompanyAddressId        Integer;

  vAutoapprovecommentG     VARCHAR2(2000);

  --用到客户账户表(CustomerAccount)字段
  nAccountbalance          NUMBER(19,4);
  nShippedproductvalue     NUMBER(19,4);
  nPendingamount           NUMBER(19,4);
  nCustomercredenceamount  NUMBER(19,4);

  --用到销售订单明细表(PartsSalesOrderDetail)中字段
  nPartsSalesOrderDetailId number(9);
  nOrderedquantity         NUMBER(9);
  nApprovequantity         NUMBER(9);
  nOrderprice              NUMBER(19,4);
  nSum                     NUMBER(19,4);
  iSparePartId             Integer;
  vSparePartName           VARCHAR2(100);
  vSparepartcode           Varchar2(50);
  vMeasureUnit             Varchar2(20);
  nOrderSum                NUMBER(19,4);
  nOriginalPrice           NUMBER(19,4);
  nPriceGradeCoefficient   NUMBER(15,6);

  nTest                    NUMBER(9);
  ---CustomerOrderPriceGrade
  nCoefficient             NUMBER(15,6); ---客户企业订单价格等级.系数

  ---partsSalesPrice
  nPriceType               NUMBER(9);
  nSalesPrice              NUMBER(19,4);

  nTreatyPrice             NUMBER(19,4);    ---特殊协议价
  nDealerSalesPrice        NUMBER(19,4);    ---批发价

  iType                    Integer;
  nIsNotWarranty           Number(1);
  iCheckOrderDetailSum            Integer;

  --出库仓库优先顺序表（WarehouseSequence）
  nPartssalescategoryid    NUMBER(9);
  vPartssalescategoryname  VARCHAR2(100);
  nCustomercompanyid       NUMBER(9);
  vCustomercompanycode     VARCHAR2(50);
  vCustomercompanyname     VARCHAR2(100);
  nDefaultwarehouseid      NUMBER(9);  --首选仓库
  vDefaultwarehousename    VARCHAR2(100);
  nDefaultoutwarehouseid   NUMBER(9);  --首选出库仓库
  vDefaultoutwarehousename VARCHAR2(100);
  nFirstwarehouseid        NUMBER(9);  --第一仓库
  vFirstwarehousename      VARCHAR2(100);
  nFirstoutwarehouseid     NUMBER(9);  --第一出库仓库
  vFirstoutwarehousename   VARCHAR2(100);
  nSecondwarehouseid       NUMBER(9);  --第二仓库
  vSecondwarehousename     VARCHAR2(100);
  nSecondoutwarehouseid    NUMBER(9);  --第二出库仓库
  vSecondoutwarehousename  VARCHAR2(100);
  nThirdwarehouseid        NUMBER(9);  --第三仓库
  vThirdwarehousename      VARCHAR2(100);
  nThirdoutwarehouseid     NUMBER(9);  --第三出库仓库
  vThirdoutwarehousename   VARCHAR2(100);
  bIsAutoApprove           Integer;    --是否参与自动审核

  nPublicwarehouseid       NUMBER(9);  --公共
  nPublicwarehouseidG      Number(9);  --统购分销出库仓库

  bInsertOutboundPlanB     boolean;
  bInsertOutboundPlanD     boolean;
  bInsertOutboundPlanF     boolean;
  bInsertOutboundPlanS     boolean;
  bInsertOutboundPlanT     boolean;


  bInsertOutboundPlanDT    boolean;
  bInsertOutboundPlanFT    boolean;
  bInsertOutboundPlanST    boolean;
  bInsertOutboundPlanTT    boolean;


  nPartsoutboundplanidB    NUMBER(9);

  ---统购分销时几个单ID
  nPartsTransferOrderidD   NUMBER(9);
  nPartsoutboundplanidD    NUMBER(9);
  nPartsOutboundBillIdD    NUMBER(9);
  nPartsInboundPlanidD     NUMBER(9);
  nPartsInboundCheckBillidD NUMBER(9);
  nPartsshippingorderidD  NUMBER(9);
  nPartsshiftorderidD    NUMBER(9);
  nPartsoutboundplanidGD NUMBER(9);

  nPartsTransferOrderidF   NUMBER(9);
  nPartsoutboundplanidF    NUMBER(9);
  nPartsOutboundBillIdF    NUMBER(9);
  nPartsInboundPlanidF     NUMBER(9);
  nPartsInboundCheckBillidF NUMBER(9);
  nPartsshiftorderidF      NUMBER(9);
  nPartsshippingorderidF   NUMBER(9);
  nPartsoutboundplanidGF   NUMBER(9);

  nPartsTransferOrderidS   NUMBER(9);
  nPartsoutboundplanidS    NUMBER(9);
  nPartsOutboundBillIdS    NUMBER(9);
  nPartsInboundPlanidS     NUMBER(9);
  nPartsshiftorderidS      NUMBER(9);
  nPartsshippingorderidS   NUMBER(9);
  nPartsInboundCheckBillidS NUMBER(9);
  nPartsoutboundplanidGS NUMBER(9);

  nPartsTransferOrderidT   NUMBER(9);
  nPartsoutboundplanidT    NUMBER(9);
  nPartsOutboundBillIdT    NUMBER(9);
  nPartsInboundPlanidT     NUMBER(9);
  nPartsshiftorderidT      NUMBER(9);
  nPartsshippingorderidT   NUMBER(9);
  nPartsInboundCheckBillidT NUMBER(9);
  nPartsoutboundplanidGT NUMBER(9);

  vReceivingWarehouseName  VARCHAR2(100);

  ---几个仓库公用这四个变量
  iWarehouseidP            Integer;
  vWareHouseNameP          VARCHAR2(100);
  iStoragecompanyidP       Integer;
  vWareHouseCodeP          varchar2(50);
  iSalesCategoryIdP        Integer;--调拨出库的品牌
  vSalesCategoryNameP      VARCHAR2(100);
  nPlannedPriceP            NUMBER(19,4);---计划价

  vWareHouseCodeG          varchar2(50);
  vWareHouseNameG          VARCHAR2(100);
  iStoragecompanyidG       Integer;
  nStoragecompanytypeG     NUMBER(9);
  iSalesCategoryIdG        Integer;--调拨入库的品牌
  vSalesCategoryNameG      VARCHAR2(100);
  nPlannedPriceG            NUMBER(19,4);---计划价

  vStoragecompanycodeG     varchar2(50);
  vStoragecompanynameG     VARCHAR2(100);

  bIsCentralizedPurchase   integer; --是否为统购分销仓库
  nOrderProcessMethod      number(9);

  vStoragecompanycodeP     VARCHAR2(50);
  vStoragecompanynameP     VARCHAR2(100);

  ---配件库存表（PartsStock）中字段
  nStoragecompanyid        NUMBER(9);
  nStoragecompanytype      NUMBER(9);
  nWarehouseareaid         NUMBER(9);
  nWarehouseareacategoryid NUMBER(9);
  nQuantity                NUMBER(9);
  iPS                      Integer;
  nSumQuantity             NUMBER(9);

  ---库区用途
  iCount  Integer;

  nMaxQuantityD          NUMBER(9); ---统购分销时最大库区量
  nMaxQuantityF          NUMBER(9); ---统购分销时
  nMaxQuantityS          NUMBER(9); ---统购分销时
  nMaxQuantityT          NUMBER(9); ---统购分销时

  nWarehouseAreaIdD         NUMBER(9); ---统购分销时库位Id
  nWarehouseAreaIdF         NUMBER(9); ---统购分销时库位Id
  nWarehouseAreaIdS         NUMBER(9); ---统购分销时库位Id
  nWarehouseAreaIdT         NUMBER(9); ---统购分销时库位Id

  nWarehouseAreaCodeD       VARCHAR2(50); ---统购分销时库位
  nWarehouseAreaCodeF       VARCHAR2(50); ---统购分销时库位
  nWarehouseAreaCodeS       VARCHAR2(50); ---统购分销时库位
  nWarehouseAreaCodeT       VARCHAR2(50); ---统购分销时库位

  nTPartsStockidD         NUMBER(9); ---统购分销时最大库区ID
  nTPartsStockidF         NUMBER(9); ---统购分销时
  nTPartsStockidS         NUMBER(9); ---统购分销时
  nTPartsStockidT         NUMBER(9); ---统购分销时

  nLockedQuantityD          NUMBER(9); ---统购分销时最大库区出库量
  nLockedQuantityF          NUMBER(9); ---统购分销时
  nLockedQuantityS          NUMBER(9); ---统购分销时
  nLockedQuantityT          NUMBER(9); ---统购分销时

  nLockedQuantity          NUMBER(9); --库区锁定数量
  nSumLockedQuantity       NUMBER(9);

  iPartsLockedStock        integer; ---判断是否有锁定库存记录

  nWRemainQuantity         NUMBER(9);---最后仓库没有满足的数量
  nWRemainQuantity1        NUMBER(9);

  iWarehouseAreaIdC        NUMBER(9);
  vCodeC                   VARCHAR2(50);
  nWarehouseareacategoryidC NUMBER(9);
  iWarehouseAreaIdS        NUMBER(9);
  nQuantityS               NUMBER(9);
  vWarehouseAreaCodeS      VARCHAR2(50);
  nWarehouseareacategoryidS NUMBER(9);
  nPartsStockidS            NUMBER(9);

  nNewLockedQuantity       NUMBER(9); ---锁定库存


  --配件出库计划单
  vPartsOutboundPlanCode   VARCHAR2(50);
  iComplateOutPlanId       Integer;
  vOutBoundPlanNewCode     varchar2(100);
  nPartsoutboundplanid     NUMBER(9);

  --配件出库单
  vPartsOutboundBillCode    VARCHAR2(50);
  iComplateOutBillId        Integer;
  vOutboundBillNewCode      varchar2(100);
  nPartsOutboundBillId      NUMBER(9);

  --配件出库计划单（统购分销）
  vPartsOutboundPlanCodeG   VARCHAR2(50);
  iComplateOutPlanIdG       Integer;
  vOutBoundPlanNewCodeG     varchar2(100);
  nPartsoutboundplanidG     NUMBER(9);


  --PartsTransferOrder配件调拨单
  vPartsTransferOrderCode    VARCHAR2(50);
  iComplateTransferOrderId        Integer;
  vTransferOrderNewCode      varchar2(100);
  nPartsTransferOrderId      NUMBER(9);


  --PartsInboundPlan配件入库计划
  vPartsInboundPlanCode    VARCHAR2(50);
  iComplateInboundPlanId        Integer;
  vInboundPlanNewCode      varchar2(100);
  nPartsInboundPlanId      NUMBER(9);

  --PartsInboundCheckBill配件入库检验单
  vPartsInboundCheckBillCode    VARCHAR2(50);
  iComplateInboundCheckBillId        Integer;
  vInboundCheckBillNewCode      varchar2(100);
  nPartsInboundCheckBillId      NUMBER(9);


  ---PartsShiftOrder配件移库单
  vPartsShiftOrderCode    VARCHAR2(50);
  iComplateShiftOrderId        Integer;
  vShiftOrderNewCode      varchar2(100);
  nPartsShiftOrderId      NUMBER(9);


  ---PartsShippingOrder配件发运单
  vPartsShippingOrderCode    VARCHAR2(50);
  iComplateShippingOrderId        Integer;
  vShippingOrderNewCode      varchar2(100);
  nPartsShippingOrderId      NUMBER(9);


  --配件客户账户销售订单处理单
  vPartsSalesOrderProcessCode varchar2(50);
  iComplateOrderProcessId   Integer;
  VOrderProcessNewCode      varchar2(100);
  nPartsSalesOrderProcessId Number(9);
  mCurrentFulfilledAmount   NUMBER(19,4);

  ---配件出库处理单本次满足金额
  mCurrentFulfilledAmountP   NUMBER(19,4);

  ---用于统购分销那次满足金额
  mCurrentFulfilledAmountD   NUMBER(19,4);
  mCurrentFulfilledAmountF   NUMBER(19,4);
  mCurrentFulfilledAmountS   NUMBER(19,4);
  mCurrentFulfilledAmountT   NUMBER(19,4);

  nBillstatusafterprocess    Number(1);

  FMyException               EXCEPTION; ---异常处理

begin


  --FDateFormat   := trunc(sysdate, 'DD');
  -- FDateStr      := to_char(sysdate, 'yyyymmdd');
  -- FSerialLength := 6;
  -- FNewCode      := '{CORPCODE}OR' || FDateStr || '{SERIAL}';

  nCreatorID:=1;
  vCreatorName:='Admin';

  bInsertOutboundPlanB := false;
  bInsertOutboundPlanD := false;
  bInsertOutboundPlanF := false;
  bInsertOutboundPlanS := false;
  bInsertOutboundPlanT := false;

  bInsertOutboundPlanDT := false;
  bInsertOutboundPlanFT := false;
  bInsertOutboundPlanST := false;
  bInsertOutboundPlanTT := false;

  mCurrentFulfilledAmountD := 0;
  mCurrentFulfilledAmountF := 0;
  mCurrentFulfilledAmountS := 0;
  mCurrentFulfilledAmountT := 0;

  nSumLockedQuantity:=0;
  iCheckOrderDetailSum:=0;
 select rowversion into  oldrowversion from PartsSalesOrder  where (Id = PartCheckId);
  insert into TempPartsSalesOrder(rid,id,code,branchid,branchcode,branchname,salescategoryid,salescategoryname,salesunitid,salesunitcode,salesunitname,salesunitownercompanyid,salesunitownercompanycode,salesunitownercompanyname,sourcebillcode,
    sourcebillid,customeraccountid,isdebt,invoicereceivecompanyid,invoicereceivecompanycode,invoicereceivecompanyname,invoicereceivecompanytype,invoicereceivesalecateid,invoicereceivesalecatename,submitcompanyid,submitcompanycode,submitcompanyname,firstclassstationid,
    firstclassstationcode,firstclassstationname,partssalesordertypeid,partssalesordertypename,totalamount,ifagencyservice,salesactivitydiscountrate,salesactivitydiscountamount,requestedshippingtime,requesteddeliverytime,ifdirectprovision,customertype,secondlevelordertype,
    remark,approvalcomment,contactperson,contactphone,receivingcompanyid,receivingcompanycode,receivingcompanyname,shippingmethod,receivingwarehouseid,receivingwarehousename,receivingaddress,originalrequirementbillid,originalrequirementbilltype,warehouseid,stopcomment,
    rejectcomment,creatorid,creatorname,createtime,modifierid,modifiername,modifytime,submitterid,submittername,submittime,approverid,approvername,approvetime,rejecterid,rejectername,rejecttime,canceloperatorid,canceloperatorname,canceltime,abandonerid,abandonername,abandontime,
    status,rowversion,province,city,provinceid,companyaddressid,firstapprovetime,invoicecustomername,bankaccount,taxpayeridentification,invoiceadress,invoicephone,invoicetype,erpordercode,erpsourceordercode, isautoapprove,autoapprovecomment,autoapprovetime )
  select rownum,id,code,branchid,branchcode,branchname,salescategoryid,salescategoryname,salesunitid,salesunitcode,salesunitname,salesunitownercompanyid,salesunitownercompanycode,salesunitownercompanyname,sourcebillcode,
    sourcebillid,customeraccountid,isdebt,invoicereceivecompanyid,invoicereceivecompanycode,invoicereceivecompanyname,invoicereceivecompanytype,invoicereceivesalecateid,invoicereceivesalecatename,submitcompanyid,submitcompanycode,submitcompanyname,firstclassstationid,
    firstclassstationcode,firstclassstationname,partssalesordertypeid,partssalesordertypename,totalamount,ifagencyservice,salesactivitydiscountrate,salesactivitydiscountamount,requestedshippingtime,requesteddeliverytime,ifdirectprovision,customertype,secondlevelordertype,
    remark,approvalcomment,contactperson,contactphone,receivingcompanyid,receivingcompanycode,receivingcompanyname,shippingmethod,receivingwarehouseid,receivingwarehousename,receivingaddress,originalrequirementbillid,originalrequirementbilltype,warehouseid,stopcomment,
    rejectcomment,creatorid,creatorname,createtime,modifierid,modifiername,modifytime,submitterid,submittername,submittime,approverid,approvername,approvetime,rejecterid,rejectername,rejecttime,canceloperatorid,canceloperatorname,canceltime,abandonerid,abandonername,abandontime,
    status,rowversion,province,city,provinceid,companyaddressid,firstapprovetime,invoicecustomername,bankaccount,taxpayeridentification,invoiceadress,invoicephone,invoicetype,erpordercode,erpsourceordercode, isautoapprove,autoapprovecomment,autoapprovetime
  from PartsSalesOrder  where (Id = PartCheckId) and (SalesCategoryName <> '电子商务') and (IfDirectProvision<>1) and exists(select id from Branch where Branch.id = PartsSalesOrder.salesunitownercompanyid) and status in(2,4);-- and (IsAutoApprove=1) ;
  select COUNT(*) into num from TempPartsSalesOrder;
  if num>0 then  ---销售订单存在
  begin
    --从PartsSalesOrder配件销售订单临时表里取数据;
    /*2017-10-12 hr add 销售订单的备注，会在其他单据中用到*/
    select Code,CustomerAccountId,BranchId,BranchCode,BranchName,IsAutoApprove,AutoApproveComment,AutoApproveTime,SalesCategoryId,SalesCategoryName,SubmitCompanyId,ProvinceId,PartsSalesOrderTypeId,PartsSalesOrderTypeName,Receivingcompanyid,Receivingcompanycode,Receivingcompanyname,
     receivingwarehouseid,ShippingMethod,Status,RequestedShippingTime,RequestedDeliveryTime,Remark,CompanyAddressId
     into vCode,nCustomerAccountId,iBranchId,vBranchCode,vBranchName,nIsAutoApprove,vAutoApproveComment,dAutoApproveTime,iSalesCategoryId,vSalesCategoryName,iSubmitCompanyId,iProvinceId,iPartsSalesOrderTypeId,vPartsSalesOrderTypeName,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,
     iReceivingwarehouseid,iShippingMethod,iPartsSalesOrderStatus,dRequestedShippingTime,dRequestedDeliveryTime,vRemark,vCompanyAddressId
      from TempPartsSalesOrder where id=PartCheckId and rownum=1;
     begin
        /*2017-9-30 hr modify 收货仓库 编号与名称 赋值*/
        select  code,Name into vReceivingwarehousecode,vReceivingWarehouseName from warehouse where id=iReceivingwarehouseid;
      exception
        when no_data_found then
            vReceivingwarehousecode:=null;
            vReceivingWarehouseName:=null;
            /*2017-10-11 hr add 出库计划单的收货仓库ID不能为0，否则之后手工生成的发运单会报‘配件发运单对应的收货仓库不存在’*/
            iReceivingwarehouseid:=null;
      end;

      /*
      begin

      exception
        when no_data_found then
          begin

          end;
      end;

      */

    --获取客户账户余额
    /*2017-8-15 hr modify 当客户账户余额没有数据时，金额变量赋值为null*/
    begin
      select Accountbalance, ShippedProductValue,PendingAmount,CustomerCredenceAmount into nAccountbalance, nShippedProductValue,nPendingAmount,nCustomerCredenceAmount from CustomerAccount
      where id =nCustomerAccountId and Status=1;
    exception
      when no_data_found then
        nAccountbalance := null;
        nShippedProductValue := null;
        nPendingAmount := null;
        nCustomerCredenceAmount := null;
    end;
    nAvailbleAmount:=nvl(nAccountBalance,0)+nvl(nCustomerCredenceAmount,0)-nvl(nPendingAmount,0)-nvl(nShippedProductValue,0);
    --sResult:=nAvailbleAmount; --test

    --select count(*) into nRow from PARTSSALESORDERDETAIL  where (PartsSalesOrderId= PartCheckId） and (OrderedQuantity>nvl(ApproveQuantity,0));

    insert into tempPARTSSALESORDERDETAIL(rid,id,partssalesorderid,sparepartid,sparepartcode,sparepartname,measureunit,orderedquantity,approvequantity,originalprice,custorderpricegradecoefficient,ifcannewpart,discountedprice, orderprice,ordersum,estimatedfulfilltime,remark)
    select rownum,id,partssalesorderid,sparepartid,sparepartcode,sparepartname,measureunit,orderedquantity,nvl(approvequantity,0)/*2017-8-2 hr modify 审批数量为空时赋值0*/,originalprice,custorderpricegradecoefficient,ifcannewpart,discountedprice, orderprice,ordersum,estimatedfulfilltime,remark from PARTSSALESORDERDETAIL  where (PartsSalesOrderId= PartCheckId） and (OrderedQuantity>nvl(ApproveQuantity,0)); ---排除已经审核完成的

    /*
    --取价格改成取配件销售订单明细的时候取
    insert into tempPARTSSALESORDERDETAIL(rid,id,partssalesorderid,sparepartid,sparepartcode,sparepartname,measureunit,orderedquantity,approvequantity,originalprice,custorderpricegradecoefficient,ifcannewpart,discountedprice, orderprice,ordersum,estimatedfulfilltime,remark)
    select a.rownum,a.id,a.partssalesorderid,a.sparepartid,a.sparepartcode,a.sparepartname,a.measureunit,a.orderedquantity,a.approvequantity,a.originalprice,a.custorderpricegradecoefficient,a.ifcannewpart,a.discountedprice, a.orderprice,a.ordersum,a.estimatedfulfilltime,a.remark
    from PARTSSALESORDERDETAIL a  where (a.PartsSalesOrderId= PartCheckId） and (a.OrderedQuantity>nvl(a.ApproveQuantity,0)); ---排除已经审核完成的
    */


    select count(*) into nRow  from tempPARTSSALESORDERDETAIL where PartsSalesOrderId= PartCheckId;
    --此销售订单ID有对应的销售清单
    if nRow>0 then
    begin



      --获取提报金额
      select sum((nvl(OrderedQuantity,0)-nvl(ApproveQuantity,0))*nvl(OrderPrice,0)) into nSum from tempPARTSSALESORDERDETAIL where PartsSalesOrderId= PartCheckId;

      if nAvailbleAmount<nSum then
      begin
        vAutoapprovecommentG:='客户账户可用余额不足';
        raise FMyException;
        --update PartsSalesOrder set AutoApproveComment='客户账户可用余额不足' where Id = PartCheckId;
        --sResult:='客户账户可用余额不足';
      end;
      else --'客户账户可用余额充足';
      begin
       -----res:='客户账户可用余额充足';
        ---insert into tempWarehouseSequence select * from WarehouseSequence where PartsSalesCategoryId=iSalesCategoryId and CustomerCompanyId=iSubmitCompanyId and status=1;
        ---select count(*) into nW from tempWarehouseSequence where PartsSalesCategoryId=iSalesCategoryId and CustomerCompanyId=iSubmitCompanyId and status=1;
        select count(*) into nW from WarehouseSequence where PartsSalesCategoryId=iSalesCategoryId and CustomerCompanyId=iSubmitCompanyId and status=1;
        if nw>0 then
        begin
          -----res:='nw>0';
          /*select Partssalescategoryid, Partssalescategoryname, Customercompanyid,Customercompanycode,
          Customercompanyname,Isautoapprove,Defaultwarehouseid,Defaultwarehousename,Defaultoutwarehouseid,Defaultoutwarehousename,Firstwarehouseid,Firstwarehousename,Firstoutwarehouseid,
          Firstoutwarehousename,Secoundwarehouseid,Secoundwarehousename,Secoundoutwarehouseid,Secoundoutwarehousename,Thirdwarehouseid,Thirdwarehousename,Thirdoutwarehouseid,Thirdoutwarehousename
          into nPartssalescategoryid, vPartssalescategoryname, nCustomercompanyid,vCustomercompanycode,
          vCustomercompanyname,bIsAutoApprove,nDefaultwarehouseid,vDefaultwarehousename,nDefaultoutwarehouseid,vDefaultoutwarehousename,nFirstwarehouseid,vFirstwarehousename,nFirstoutwarehouseid,
          vFirstoutwarehousename,nSecondwarehouseid,vSecondwarehousename,nSecondoutwarehouseid,vSecondoutwarehousename,nThirdwarehouseid,vThirdwarehousename,nThirdoutwarehouseid,vThirdoutwarehousename
          from WarehouseSequence where PartsSalesCategoryId=iSalesCategoryId and CustomerCompanyId=iSubmitCompanyId and status=1;*/
           select Partssalescategoryid, Partssalescategoryname, Customercompanyid,Customercompanycode,
          Customercompanyname,Isautoapprove,Defaultwarehouseid,Defaultwarehousename,Defaultoutwarehouseid,Defaultoutwarehousename,Firstwarehouseid,Firstwarehousename,Firstoutwarehouseid,
          Firstoutwarehousename,Secoundwarehouseid,Secoundwarehousename,Secoundoutwarehouseid,Secoundoutwarehousename,Thirdwarehouseid,Thirdwarehousename,Thirdoutwarehouseid,Thirdoutwarehousename
          into nPartssalescategoryid, vPartssalescategoryname, nCustomercompanyid,vCustomercompanycode,
          vCustomercompanyname,bIsAutoApprove,nDefaultwarehouseid,vDefaultwarehousename,nDefaultoutwarehouseid,vDefaultoutwarehousename,nFirstwarehouseid,vFirstwarehousename,nFirstoutwarehouseid,
          vFirstoutwarehousename,nSecondwarehouseid,vSecondwarehousename,nSecondoutwarehouseid,vSecondoutwarehousename,nThirdwarehouseid,vThirdwarehousename,nThirdoutwarehouseid,vThirdoutwarehousename
          from WarehouseSequence where PartsSalesCategoryId=iSalesCategoryId and CustomerCompanyId=iSubmitCompanyId and status=1;

          select IsNotWarranty into nIsNotWarranty from partsSalesCategory where id=iSalesCategoryId;  ---IsNotWarranty=1是保外
          select type into iType from company where id=iSubmitCompanyId and status=1;  ---type=2 是服务站

          ------如果vPartsSalesCategoryName不等于'随车行' 更新销售订单明细的价格
          if  (vSalesCategoryName<>'随车行') and (iType is not null) then
          begin

          -----res:='更新销售订单明细的价格';
          for i in 1..nRow loop ---配件销售订单清单数
            select id,SparePartId,SparePartName,SparePartCode,MeasureUnit,OrderedQuantity,ApproveQuantity,OrderPrice,OriginalPrice,OrderSum,CustOrderPriceGradeCoefficient into nPartsSalesOrderDetailId,iSparePartId,vSparePartName,vSparePartCode,vMeasureUnit,nOrderedQuantity,nApproveQuantity,nOrderPrice,nOriginalPrice,nOrderSum, nPriceGradeCoefficient  from tempPARTSSALESORDERDETAIL where rid=i and PartsSalesOrderId= PartCheckId;


            ---取客户企业订单价格等级 系数
            begin
              select Coefficient into nCoefficient from CustomerOrderPriceGrade where Branchid=iBranchId and PartsSalesCategoryId=nPartsSalesCategoryId and CustomerCompanyId=iSubmitCompanyId and status=1 and PartsSalesOrderTypeId=iPartsSalesOrderTypeId; --- nCustomerCompanyId
            exception
            when no_data_found then
            begin
              nCoefficient:=null;
            end;
            end;

            ---取销售价格
            begin
              ---这样条件怎么查询出来多条记录？
              select c.PriceType,c.SalesPrice into nPriceType,nSalesPrice  from SparePart a, PartsBranch b, partsSalesPrice c where a.id=b.partid and b.status=1 and c.status=1 and c.sparePartid=a.id and c.partssalescategoryid=nPartsSalesCategoryId and a.id=iSparePartId and c.PartsSalesCategoryId=iPartsSalesOrderTypeId and rownum=1;
            exception
            when no_data_found then
            begin
              nPriceType:=null;
              nSalesPrice:=null;
            end;
            end;

            ---取配件特殊协议价
            begin
              select TreatyPrice into nTreatyPrice from PartsSpecialTreatyPrice where BranchId=iBranchId and PartsSalesCategoryId=nPartsSalesCategoryId and SparePartId=iSparePartId and status=1 and CompanyId=iSubmitCompanyId ; --- nCustomerCompanyId
            exception
              when no_data_found then
              begin
                nTreatyPrice:=null;
              end;
            end;

            ---取服务站配件批发价
            begin
              select DealerSalesPrice into nDealerSalesPrice from DealerPartsSalesPrice where BranchId=iBranchId and PartsSalesCategoryId=nPartsSalesCategoryId and SparePartId=iSparePartId and status=1;
            exception
              when no_data_found then
              begin
                nDealerSalesPrice:=null;
              end;
            end;

            ---销售价格
            if nSalesPrice is not null then
              nOriginalPrice:=nSalesPrice;
            end if;

            ---客户企业订单价格等级 系数
            if nCoefficient is not null then
              nPriceGradeCoefficient:=nCoefficient;
            end if;

            ---配件特殊协议价
            if nTreatyPrice is null then
            begin
              if nPriceType=1 then
                if nSalesPrice is not null and nCoefficient is not null then
                  nOrderPrice:=nSalesPrice*nCoefficient;
                end if;
              else
                if nSalesPrice is not null then
                  nOrderPrice:=nSalesPrice;
                end if;
              end if;
            end;
            else nOrderPrice:= nTreatyPrice;

              ---服务站配件批发价
              if iType=2 and nIsNotWarranty=1 and (nDealerSalesPrice is not null) then
                nOrderPrice:=nDealerSalesPrice;
              end if;

            end if;

            ---更新订单明细
            update  tempPARTSSALESORDERDETAIL set OriginalPrice=nOriginalPrice,CustOrderPriceGradeCoefficient=nPriceGradeCoefficient,orderprice=nOrderPrice,ordersum=nOrderPrice*nOrderedQuantity  where rid=i and PartsSalesOrderId= PartCheckId;

            update  PARTSSALESORDERDETAIL set OriginalPrice=nOriginalPrice,CustOrderPriceGradeCoefficient=nPriceGradeCoefficient,orderprice=nOrderPrice,ordersum=nOrderPrice*nOrderedQuantity  where Id=nPartsSalesOrderDetailId;

           end loop;

           ---test
           --select sum(ordersum) into nTestAmount from tempPARTSSALESORDERDETAIL where  PartsSalesOrderId= PartCheckId;


           ---更新订单数据(先注释掉，到这面运行停止)
           ---update PartsSalesOrder set TotalAmount=(select sum(ordersum) from tempPARTSSALESORDERDETAIL where  PartsSalesOrderId= PartCheckId) where id=PartCheckId;
           select sum(ordersum) into nSumTotalAmount from tempPARTSSALESORDERDETAIL where  PartsSalesOrderId= PartCheckId;
           update tempPartsSalesOrder set TotalAmount=nSumTotalAmount where id=PartCheckId;

          end;
          end if;

          if bIsAutoApprove =1 then --出库仓库优先顺序.是否参与自动审核 为参与自动审核
          begin
            nli:=nRow;
            -----res:='sAutoApprove =1';
            ---新建配件客户账户销售订单处理单(PartsSalesOrderProcess)
            /*获取PartsSalesOrderProcess编号生成模板Id*/
            --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
            vOrderProcessNewCode :='PSOP{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
            begin
              select Id into iComplateOrderProcessId from codetemplate where name = 'PartsSalesOrderProcess' and IsActived = 1;
            exception
            when NO_DATA_FOUND then
            raise_application_error(-20001,'未找到名称为"PartsSalesOrderProcess"的有效编码规则。');
            when others then
            raise;
            end;

            /*2017-10-16 hr modify 配件销售订单处理单编号的流水号应是4位*/
            vPartsSalesOrderProcessCode := regexp_replace(regexp_replace(VOrderProcessNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOrderProcessId,TRUNC(sysdate,'DD'),vBranchCode)),4/*6*/,'0'));
            nPartsSalesOrderProcessId := s_PartsSalesOrderProcess.Nextval;
            ---(没有生成销售订单处理清单的时候，也会生成销售订单处理单)
            insert into PartsSalesOrderProcess(id,code,originalsalesorderid,billstatusbeforeprocess,billstatusafterprocess,requestedshippingtime,requesteddeliverytime,partssalesactivityname,currentfulfilledamount,approvalcomment,shippingmethod,shippinginformationremark,createtime,creatorid,creatorname)
            values(nPartsSalesOrderProcessId,vPartsSalesOrderProcessCode,PartCheckId,iPartsSalesOrderStatus,4,dRequestedShippingTime,dRequestedDeliveryTime,'',0,'自动审核',iShippingMethod,'',sysdate,nCreatorid,vCreatorname);

            ---partssalesactivityname,currentfulfilledamount,approvalcomment,shippinginformationremark 不知道值，先赋空值，然后再更新。
            ---billstatusafterprocess:=4;--(部分审批)

            /*获取PartsOutboundPlan编号生成模板Id*/
            --POP{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} Example
            vOutboundPlanNewCode := 'POP{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
            begin
              select Id into iComplateOutPlanId from codetemplate where name = 'PartsOutboundPlan' and IsActived = 1;
            exception
            when NO_DATA_FOUND then
            raise_application_error(-20001,'未找到名称为"PartsOutboundPlan"的有效编码规则。');
            when others then
            raise;
            end;


            /*获取PartsOutboundBill编号生成模板Id*/
            vOutboundBillNewCode := 'CK{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
            begin
              select Id into iComplateOutBillId from codetemplate where name = 'PartsOutboundBill' and IsActived = 1;
            exception
            when NO_DATA_FOUND then
            raise_application_error(-20001,'未找到名称为"PartsOutboundBill"的有效编码规则。');
            when others then
            raise;
            end;


            /*获取PartsTransferOrder配件调拨单编号生成模板Id*/
            --PT{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} Example
            vTransferOrderNewCode := 'PT{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
            begin
              select Id into iComplateTransferOrderId from codetemplate where name = 'PartsTransferOrder' and IsActived = 1;
            exception
            when NO_DATA_FOUND then
            raise_application_error(-20001,'未找到名称为"PartsTransferOrder"的有效编码规则。');
            when others then
            raise;
            end;


            /*获取PartsInboundPlan配件入库计划编号生成模板Id*/
            --PIP{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} Example
            vInboundPlanNewCode := 'PIP{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
            begin
              select Id into iComplateInboundPlanId from codetemplate where name = 'PartsInboundPlan' and IsActived = 1;
            exception
            when NO_DATA_FOUND then
            raise_application_error(-20001,'未找到名称为"PartsInboundPlan"的有效编码规则。');
            when others then
            raise;
            end;

            /*获取PartsInboundCheckBill配件入库检验单编号生成模板Id*/
            --PK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} Example
            --vInboundCheckBillNewCode := 'PK{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
            /*2017-10-9 hr modify 入库检验单的编号规则是RK*/
            vInboundCheckBillNewCode := 'RK{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
            begin
              select Id into iComplateInboundCheckBillId from codetemplate where name = 'PartsInboundCheckBill' and IsActived = 1;
            exception
            when NO_DATA_FOUND then
            raise_application_error(-20001,'未找到名称为"PartsInboundPlan"的有效编码规则。');
            when others then
            raise;
            end;

            /*获取PartsShiftOrder配件移库单编号生成模板Id*/
            --PM{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} Example
            vShiftOrderNewCode := 'PM{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
            begin
              select Id into iComplateShiftOrderId from codetemplate where name = 'PartsShiftOrder' and IsActived = 1;
            exception
            when NO_DATA_FOUND then
            raise_application_error(-20001,'未找到名称为"PartsShiftOrder"的有效编码规则。');
            when others then
            raise;
            end;

            /*获取PartsShippingOrder配件发运单编号生成模板Id*/
            --FY{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} Example
            vShippingOrderNewCode := 'FY{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
            begin
              select Id into iComplateShippingOrderId from codetemplate where name = 'PartsShippingOrder' and IsActived = 1;
            exception
            when NO_DATA_FOUND then
            raise_application_error(-20001,'未找到名称为"PartsShippingOrder"的有效编码规则。');
            when others then
            raise;
            end;

            for i in 1..nRow loop ---配件销售订单清单数
              ---从配件销售订单临时表里获取数据。
              --select count(*) into nTest  from tempPARTSSALESORDERDETAIL where rid=i and PartsSalesOrderId= PartCheckId;
              -----res:=nTest;
              select id,SparePartId,SparePartName,SparePartCode,MeasureUnit,OrderedQuantity,nvl(ApproveQuantity,0),OrderPrice,OriginalPrice,OrderSum into nPartsSalesOrderDetailId,iSparePartId,vSparePartName,vSparePartCode,vMeasureUnit,nOrderedQuantity,nApproveQuantity,nOrderPrice,nOriginalPrice,nOrderSum  from tempPARTSSALESORDERDETAIL where rid=i and PartsSalesOrderId= PartCheckId;
              ---select OrderedQuantity into nOrderedQuantity from tempPARTSSALESORDERDETAIL where rid=i and PartsSalesOrderId= PartCheckId;
              -----res:=nOrderedQuantity;

              -- 配件营销信息表里的，批量审核上限>配件销售订单明细未审批数量
              select count(*) into nBP from PartsBranch where PartsBranch.AutoApproveUpLimit>=(nOrderedQuantity-nApproveQuantity) and PartsBranch.PartsSalesCategoryId=iSalesCategoryId and PartsBranch.PartId=iSparePartId and Status=1;
              -----res:=(nOrderedQuantity-nApproveQuantity);
              -----res:=nOrderedQuantity;
              -----res:=nBP;
              if nBP > 0 then
              begin

                nWRemainQuantity:= nOrderedQuantity-nApproveQuantity; ---此订单明细剩下还没有被审批的数量
                -----res:='test';
                nWRemainQuantity1:=nWRemainQuantity;
                nNewLockedQuantity:=nOrderedQuantity-nApproveQuantity; ---需要锁定库存
                iWarehouseidP:=0;
                /*2017-9-7 hr modify 当查询不到数据时，warehouseid赋值为0*/
                begin
                  select warehouseid into iWarehouseidP from warehousevsprovince where provinceid=iProvinceid;
                exception
                  when no_data_found then
                    iWarehouseidP:=0;
                end;

----------------存在匹配仓库

                if vCustomerCompanyCode='FT010468' and vPartsSalesCategoryName='随车行' and iWarehouseidP>0  then  --存在匹配仓库，按匹配仓库，首选仓库，第一仓库，第二仓库，第三仓库。
                begin
                  --res:='存在匹配仓库';
                  select Code,Name,storagecompanyid,storagecompanytype into vWareHouseCodeP,vWareHouseNameP,iStoragecompanyidP,nStoragecompanytype from warehouse where id=iWarehouseidP;

                  select code,name into vStoragecompanycodeP,vStoragecompanynameP from company where id=iStoragecompanyidP;

                  -- select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from PartsStock where
                  insert into tempPARTSSTOCK(rid,id,warehouseid,storagecompanyid,storagecompanytype,branchid,warehouseareaid,warehouseareacategoryid,partid,quantity,remark,creatorid,creatorname,createtime,modifierid,modifiername,modifytime,rowversion)
                  select rownum,id,warehouseid,storagecompanyid,storagecompanytype,branchid,warehouseareaid,warehouseareacategoryid,partid,quantity,remark,creatorid,creatorname,createtime,modifierid,modifiername,modifytime,rowversion
                   from PARTSSTOCK a   where WarehouseId=iWarehouseidP and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                  select sum(quantity) into nSumQuantity from tempPARTSSTOCK a  where WarehouseId=iWarehouseidP and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                  select sum(LockedQuantity) into nSumLockedQuantity from Partslockedstock where WarehouseId=iWarehouseidP and PartId=iSparePartId;
                  /*2017-8-3 hr add*/
                  insert into tempPARTSLockedSTOCK select id,warehouseid,storagecompanyid,branchid,partid,lockedquantity,creatorid,creatorname,createtime,modifierid,modifiername,modifytime from Partslockedstock where WarehouseId=iWarehouseidP and PartId=iSparePartId;


                  select count(*) into iPS from tempPARTSSTOCK a  where a.WarehouseId=iWarehouseidP and  partid=iSparePartId;

                  if (nSumQuantity-nvl(nSumLockedQuantity,0))< nWRemainQuantity1 and (nSumQuantity-nvl(nSumLockedQuantity,0)) > 0 then ---匹配仓库部分满足
                  begin
                    -----res:='匹配仓库部分满足';
                    nNewLockedQuantity:=nSumQuantity-nvl(nSumLockedQuantity,0);
                  /*
                    ---新建配件出库单(PartsOutboundBill)
                    ---creatorid,creatorname,createtime,获得
                    --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                    vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                    nPartsOutboundBillId := s_PartsOutboundBill.Nextval;

                    insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                      partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,receivingcompanyid,receivingcompanycode, receivingcompanyname,receivingwarehouseid,
                      receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                      --orderapprovecomment,remark,
                      creatorid,creatorname,createtime)

                    Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,iWarehouseidP,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,
                      iPartssalesordertypeid,vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingwarehouseid,
                      vReceivingwarehousecode,vReceivingWarehouseName,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);
                    */


                    ---新建出库计划单
                    if bInsertOutboundPlanB=false then ---如果此次审核匹配仓库已经生成出库计划主单就不再生成
                    begin
                      nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                      nPartsoutboundplanidB :=nPartsoutboundplanid;
                      vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                      /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                        2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                        2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                        2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                      insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                        partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                        sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                        createtime,orderapprovecomment,IfWmsInterface,Remark,Companyaddressid)
                      values(nPartsoutboundplanid,vPartsOutboundPlanCode,iWarehouseidP,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                        vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                        vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                        sysdate,'自动审核',0,vRemark,vCompanyAddressId);
                      bInsertOutboundPlanB:=true;
                    end;
                    end if;


                    --配件销售订单处理单清单(tPartsSalesOrderProcessDetail)
                    ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                    ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,匹配仓库，这些字段不填。
                    insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                      SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                    values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,iWarehouseidP,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                    vSparepartname,vMeasureUnit,nOrderedQuantity,(nSumQuantity-nvl(nSumLockedQuantity,0)),nvl(nOrderPrice,0),2,2,sysdate,nCreatorId,vCreatorName);

                    --匹配仓库此次满足金额
                    mCurrentFulfilledAmountP:=(nSumQuantity-nvl(nSumLockedQuantity,0))*nvl(nOrderPrice,0);

                    /*2017-8-4 hr modify 配件多库位循环赋值剩余未满足量有问题，当锁定库存大于单个库位库存时，剩余未满足量将不会赋值*/
                    nWRemainQuantity:= nWRemainQuantity-(nSumQuantity-nvl(nSumLockedQuantity,0));
                    /*
                    for j in 1..iPS loop
                      select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=iWarehouseidP and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                      select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=iWarehouseidP and PartId=iSparePartId;
                      if nQuantity-nvl(nLockedQuantity,0)>0 then --此库区可用数量
                      begin



                        nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --剩下没满足数量
                        ---nWRemainQuantity1:=nWRemainQuantity;

                        \*
                        --配件出库单清单（tempPartsOutboundBillDetail）记录
                        insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                        values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,(nQuantity-nvl(nLockedQuantity,0)),iWarehouseidP,vWareHouseCodeP,nvl(nOrderPrice,0),
                        (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));
                        *\
                      end;
                      end if;
                    end loop;
                    */

                    ---新建出库计划清单
                    insert into tempPartsOutboundPlanDetail (id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price)
                    /*2017-8-4 hr modify nQuantity在上面的循环中会取最后一条数据*/
                    /*2017-9-2 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                    values (s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidB/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,/*(nQuantity-nvl(nLockedQuantity,0))*/(nSumQuantity-nvl(nSumLockedQuantity,0)),nOrderPrice);

                    ---锁定库存
                    select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=iWarehouseidP and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                    if iPartsLockedStock>0 then
                      update PartsLockedStock set LockedQuantity=LockedQuantity+nNewLockedQuantity,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=iWarehouseidP and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                    else
                      insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                      values(s_PartsLockedStock.Nextval,iWarehouseidP,iStoragecompanyidP,iBranchId,iSparePartId,nNewLockedQuantity,nCreatorId,vCreatorName,sysdate);
                    end if;

                    ---锁定库存
                    --update PARTSLockedSTOCK set LockedQuantity=LockedQuantity+nNewLockedQuantity where WarehouseId=iWarehouseidP and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;

                    ---nWRemainQuantity:= nWRemainQuantity - (nSumQuantity-nvl(nSumLockedQuantity,0)); --剩下没满足数
                    nWRemainQuantity1:=nWRemainQuantity;
                    iCheckOrderDetailSum  := iCheckOrderDetailSum +1;

                  end;
                  elsif (nSumQuantity-nvl(nSumLockedQuantity,0)) >= nWRemainQuantity then
                    ---匹配仓库完全满足
                  begin
                    nNewLockedQuantity:=nWRemainQuantity; ---需要锁定库存
                    /*
                    --新建配件出库单
                    ---creatorid,creatorname,createtime,获得
                    --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                    vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                    nPartsOutboundBillId := s_PartsOutboundBill.Nextval;

                    insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                      partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,receivingcompanyid,receivingcompanycode, receivingcompanyname,receivingwarehouseid,
                      receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                      --orderapprovecomment,remark,
                      creatorid,creatorname,createtime)

                    Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,iWarehouseidP,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,
                      iPartssalesordertypeid,vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingwarehouseid,
                      vReceivingwarehousecode,vReceivingWarehouseName,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);
                    */


                    ---新建出库计划单
                    if bInsertOutboundPlanB=false then ---如果此次审核匹配仓库已经生成出库计划主单就不再生成
                    begin
                      nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                      nPartsoutboundplanidB :=nPartsoutboundplanid;
                      vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                      /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                        2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                        2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                        2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                      insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                        partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                        sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                        createtime,orderapprovecomment,IfWmsInterface,Remark,Companyaddressid)
                      values(nPartsoutboundplanid,vPartsOutboundPlanCode,iWarehouseidP,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                        vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                        vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                        sysdate,'自动审核',0,vRemark,vCompanyAddressId);

                      bInsertOutboundPlanB:=true;
                    end;
                    end if;

                    --配件销售订单处理单清单(PartsSalesOrderDetail)
                    ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                    ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,匹配仓库，这些字段不填。

                    insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                      SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                    values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,iWarehouseidP,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                    vSparepartname,vMeasureUnit,nOrderedQuantity,nWRemainQuantity,nvl(nOrderPrice,0),1,2,sysdate,nCreatorId,vCreatorName);

                    ---新建出库计划清单
                    insert into tempPartsOutboundPlanDetail (id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price)
                    /*2017-9-2 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                    values (s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidB/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nWRemainQuantity,nOrderPrice);

                    --匹配仓库此次满足金额
                    mCurrentFulfilledAmountP:=nWRemainQuantity*nvl(nOrderPrice,0);

                    /*2017-8-4 hr modify 配件多库位循环赋值剩余未满足量有问题，当锁定库存大于单个库位库存时，剩余未满足量将不会赋值*/
                    nWRemainQuantity:= 0;
                    nWRemainQuantity1:=nWRemainQuantity;
                    /*
                    for j in 1..iPS loop
                      select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=iWarehouseidP and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                      select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=iWarehouseidP and PartId=iSparePartId;
                      if nQuantity-nvl(nLockedQuantity,0)>0 then --此库区可用数量
                      begin

                        if (nQuantity-nvl(nLockedQuantity,0)) >nWRemainQuantity then
                        begin
                          nWRemainQuantity:= 0; --此库区剩下没满足数量,此次已经完全满足
                          ---update PARTSSTOCK set Quantity=nQuantity-nWRemainQuantity where WarehouseAreaId=nWarehouseAreaId and  WarehouseId=iWarehouseidP  and PartId=iSparePartId;
                        end;
                        else
                        begin
                          nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --此库区剩下没满足数量
                         ---update PARTSSTOCK set Quantity=0 where WarehouseAreaId=nWarehouseAreaId and  WarehouseId=iWarehouseidP  and PartId=iSparePartId;

                        end;
                        end if;

                        nWRemainQuantity1:=nWRemainQuantity;


                        \*
                        --配件出库单清单（tempPartsOutboundBillDetail）记录
                        insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                        values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,(nQuantity-nvl(nLockedQuantity,0)),iWarehouseidP,vWareHouseCodeP,nvl(nOrderPrice,0),
                        (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));
                        *\
                        exit when nWRemainQuantity= 0;
                      end;
                      end if;

                    end loop;
                    */
                    iCheckOrderDetailSum  := iCheckOrderDetailSum +1;

                    ---锁定库存
                    select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=iWarehouseidP and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                    if iPartsLockedStock>0 then
                      update PartsLockedStock set LockedQuantity=LockedQuantity+nNewLockedQuantity,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=iWarehouseidP and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                    else
                      insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                      values(s_PartsLockedStock.Nextval,iWarehouseidP,iStoragecompanyidP,iBranchId,iSparePartId,nNewLockedQuantity,nCreatorId,vCreatorName,sysdate);
                    end if;
                    ---锁定库存
                    ---update PARTSLockedSTOCK set LockedQuantity=LockedQuantity+(nQuantity-nvl(nLockedQuantity,0)) where WarehouseId=iWarehouseidP and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;

                  end;
                  end if;

                end;
                end if;


----------------首选仓库，存在首选仓库

                if  nWRemainQuantity>0 and nDefaultwarehouseid>0 then
                begin
                  ----res:='首选仓库';
                  nPublicwarehouseid:=nDefaultwarehouseid;


                  ---判断此仓库是否为统购分销仓库
                  select IsCentralizedPurchase into bIsCentralizedPurchase from warehouse where id=nPublicwarehouseid;

                  if  bIsCentralizedPurchase=1 then
                  begin

                    nPublicwarehouseidG:=nDefaultoutwarehouseid;
                    nOrderProcessMethod:=10;

                    select Code,Name,storagecompanyid,storagecompanytype into vWareHouseCodeG,vWareHouseNameG,iStoragecompanyidG,nStoragecompanytypeG from warehouse where id=nPublicwarehouseidG;
                    select code,name into vStoragecompanycodeG,vStoragecompanynameG from company where id=iStoragecompanyidG;
                    /*2017-10-11 hr add 增加入库品牌取值*/
                    begin
                      select b.partssalescategoryid,c.name into iSalesCategoryIdG,vSalesCategoryNameG from salesunitaffiwarehouse a left join salesunit b on a.salesunitid = b.id left join partssalescategory c on b.partssalescategoryid = c.id where a.warehouseid = nPublicwarehouseidG;
                    exception
                    when no_data_found then
                      begin
                        vAutoapprovecommentG:='统购分销仓库'||vWareHouseCodeG||'未维护销售组织';
                        raise FMyException;
                      end;
                    end;

                    select Code,Name,storagecompanyid,storagecompanytype into vWareHouseCodeP,vWareHouseNameP,iStoragecompanyidP,nStoragecompanytype from warehouse where id=nPublicwarehouseid;
                    select code,name into vStoragecompanycodeP,vStoragecompanynameP from company where id=iStoragecompanyidP;
                    /*2017-10-11 hr add 增加调拨出库发运品牌取值*/
                    begin
                      select b.partssalescategoryid,c.name into iSalesCategoryIdP,vSalesCategoryNameP from salesunitaffiwarehouse a left join salesunit b on a.salesunitid = b.id left join partssalescategory c on b.partssalescategoryid = c.id where a.warehouseid = nPublicwarehouseid;
                    exception
                    when no_data_found then
                      begin
                        vAutoapprovecommentG:='统购分销仓库'||vWareHouseCodeP||'未维护销售组织';
                        raise FMyException;
                      end;
                    end;

                    -- select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from PartsStock where
                    insert into tempPARTSSTOCK  select rownum,id,warehouseid,storagecompanyid,storagecompanytype,branchid,warehouseareaid,warehouseareacategoryid,partid,quantity,remark,creatorid,creatorname, createtime,modifierid,modifiername,modifytime,rowversion from PARTSSTOCK a  where WarehouseId=nPublicwarehouseid  and PartId=iSparePartId and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                    --insert into tempPARTSSTOCK  select rownum,id,warehouseid,storagecompanyid,storagecompanytype,branchid,warehouseareaid,warehouseareacategoryid,partid,quantity,remark,creatorid,creatorname, createtime,modifierid,modifiername,modifytime,rowversion from PARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId
                    --and (quantity=(select max(quantity) from PARTSSTOCK where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId));

                    select sum(nvl(quantity,0)) into nSumQuantity from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;

                    select max(nvl(quantity,0)) into nMaxQuantityD from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                    /*2017-8-4 hr modfiy 当库存不存在时，查不到统购分销仓库最大库存库区ID*/
                    begin
                    ---获取统购分销仓库最大库存库区ID
                      select id,WarehouseAreaId into nTPartsStockidD,nWarehouseAreaIdD from tempPARTSSTOCK a  where quantity=nMaxQuantityD and rownum=1 and WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                    exception
                      when no_data_found then
                        nTPartsStockidD := 0;
                    end;
                    /*2017-10-17 hr add 获取调拨出库仓库的库位*/
                    begin
                      select code into nWarehouseAreaCodeD from WarehouseArea a  where a.id = nWarehouseAreaIdD and a.areakind = 3;
                    exception
                      when no_data_found then
                        nWarehouseAreaCodeD := 0;
                    end;

                    select sum(nvl(LockedQuantity,0)) into nSumLockedQuantity from Partslockedstock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;

                    select count(*) into iPS from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                    -----res:=nSumLockedQuantity;
                    if (nSumQuantity-nvl(nSumLockedQuantity,0))< nWRemainQuantity1 and (nSumQuantity-nvl(nSumLockedQuantity,0)) > 0 then ---首选仓库部分满足
                    --if (nMaxQuantityD< nWRemainQuantity1) and (nSumQuantity-nvl(nSumLockedQuantity,0))< nWRemainQuantity1 and (nSumQuantity-nvl(nSumLockedQuantity,0)) > 0 then ---首选仓库部分满足
                    begin

                      --首选仓库此次满足数量和金额
                      if nMaxQuantityD <= (nSumQuantity-nvl(nSumLockedQuantity,0)) then
                      begin
                         mCurrentFulfilledAmountD:=nMaxQuantityD*nvl(nOrderPrice,0);
                         nLockedQuantityD := nMaxQuantityD;
                      end;
                      else
                      begin
                         mCurrentFulfilledAmountD:=(nSumQuantity-nvl(nSumLockedQuantity,0))*nvl(nOrderPrice,0);
                         nLockedQuantityD := (nSumQuantity-nvl(nSumLockedQuantity,0));
                      end;
                      end if;

                      /*2017-10-11 hr add 对出入库计划价进行校验
                      1）计划价必须存在，且>0
                      2）出入库仓库的计划价必须一致*/
                      begin
                        select PlannedPrice into nPlannedPriceG from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG;
                      exception
                      when no_data_found then
                        begin
                          vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameG||']没有计划价';
                          raise FMyException;
                        end;
                      end;
                      if nPlannedPriceG = 0 then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameG||']的计划价为0';
                        raise FMyException;
                      end;
                      end if;
                      begin
                        select PlannedPrice into nPlannedPriceP from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdP;
                      exception
                      when no_data_found then
                        begin
                          vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']没有计划价';
                          raise FMyException;
                        end;
                      end;
                      if nPlannedPriceP = 0 then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']的计划价为0';
                        raise FMyException;
                      end;
                      end if;
                      if nPlannedPriceP <> nPlannedPriceG then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']的计划价与品牌['||vSalesCategoryNameG||']的计划价不一致';
                        raise FMyException;
                      end;
                      end if;


                      if bInsertOutboundPlanDT = false then
                      begin
                        ---收货单位Id  ReceivingCompanyId,收货单位编号  ReceivingCompanyCode,收货单位名称  ReceivingCompanyName 填为  nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG


                        ---PartsTransferOrder配件调拨单
                        nPartsTransferOrderid := s_PartsTransferOrder.Nextval;
                        nPartsTransferOrderidD := nPartsTransferOrderid;
                        vPartsTransferOrderCode := regexp_replace(regexp_replace(vTransferOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateTransferOrderId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-8-7 hr modify 调拨单状态为 已审批
                          2017-10-12 hr modify 调拨单的调拨类型 应该是 3--急需调拨；发运方式设置为空
                          2017-10-13 hr add 修改人，审批人，修改时间，审批时间赋值*/
                        insert into PartsTransferOrder (id,code,originalwarehouseid,originalwarehousecode,originalwarehousename,storagecompanyid,storagecompanytype,destwarehouseid,destwarehousecode,destwarehousename,status,type,
                          shippingmethod,creatorid,creatorname,createtime,originalbillid,originalbillcode,totalamount,Modifierid,Modifiername,Modifytime,Approverid,Approvername,Approvetime)
                        values(nPartsTransferOrderid,vPartsTransferOrderCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,nStoragecompanytype,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,/*1*/2,/*1*/3,
                        null/*iShippingMethod*/,nCreatorID,vCreatorName,sysdate,PartCheckId,vCode,0,nCreatorID,vCreatorName,sysdate,nCreatorID,vCreatorName,sysdate);
                        --- totalamount,totalplanamount需要计算。

                        ----PartsOutboundPlan配件出库计划 (首选仓库)
                        nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                        nPartsoutboundplanidD :=nPartsoutboundplanid;
                        vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值，出库计划单的源单据 应是 调拨单
                          2017-10-12 hr modify 调拨出库生成的出库计划单的出库类型 应是 调拨出库；对方单位赋值有问题
                          2017-10-16 hr modify 发运方式设置为空，客户账户Id默认null*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface )
                        values(nPartsoutboundplanid,vPartsOutboundPlanCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdP/*iSalesCategoryId*/,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP, nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPartsTransferOrderid/*PartCheckId*/,
                          vPartsTransferOrderCode/*vCode*/,4/*1*/,null/*iShippingMethod*/,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,3,nCreatorID,vCreatorName,sysdate,0);


                        ---新建配件出库单
                        --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                        vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        nPartsOutboundBillId := s_PartsOutboundBill.Nextval;
                        nPartsOutboundBillIdD :=nPartsOutboundBillId;
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-13 hr modify 调拨出库生成的出库单的出库类型 应是 调拨出库 配件调拨；备注是'统购分销出库'；结算状态是 待结算；对方单位赋值有问题
                          2017-10-16 hr modify 发运方式设置为空，客户账户Id默认null*/
                        insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                          partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,receivingcompanyid,receivingcompanycode, receivingcompanyname,
                          receivingwarehouseid,receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                          creatorid,creatorname,createtime,Remark)
                        Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdP/*iSalesCategoryId*/,
                          iPartssalesordertypeid,vPartssalesordertypename,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,
                          nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,4/*1*/,null/*iShippingMethod*/,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,2/*1*/,
                          nCreatorID,vCreatorName,sysdate,'统购分销出库');


                        ---PartsShippingOrder配件发运单  /*入库计划单的源单据是 发运单，所以调整发运单生成的位置，放到入库计划单生成前*/
                        nPartsShippingOrderid := s_PartsShippingOrder.Nextval;
                        nPartsTransferOrderidD :=nPartsTransferOrderid;
                        /*2017-10-12 hr modify 发运单应是4位流水号*/
                        vPartsShippingOrderCode := regexp_replace(regexp_replace(vShippingOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateShippingOrderId,TRUNC(sysdate,'DD'),vBranchCode)),4/*6*/,'0'));
                        /*2017-9-6 hr modify 发运单的发货单位与结算单位赋值有问题，状态不应是新建，应该是 回执确认
                          2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 配件发运单增加品牌字段赋值
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-12 hr add 发运单备注默认'统购分销发运'
                          2017-10-13 hr add 发运方式默认 8--客户自行运输；发运日期 不赋值；收货单位赋值有问题；预计到货日期 不赋值*/
                        insert into PartsShippingOrder(id,code,type,branchid,shippingcompanyid,shippingcompanycode,shippingcompanyname,settlementcompanyid,settlementcompanycode,settlementcompanyname,receivingcompanyid,receivingcompanycode,invoicereceivesalecateid,invoicereceivesalecatename,receivingcompanyname, warehouseid,warehousecode,warehousename,
                          receivingwarehouseid,receivingwarehousecode,receivingwarehousename,originalrequirementbillid,originalrequirementbillcode,originalrequirementbilltype,shippingdate,status,creatorid,creatorname,createtime,expectedplacedate,PartsSalesCategoryId,Remark,Shippingmethod)
                        values(nPartsShippingOrderid,vPartsShippingOrderCode,4,ibranchid,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,*/iStoragecompanyidP,vStoragecompanycodeP,/*nPublicwarehouseidG,vWareHouseCodeG ,*/iPartssalesordertypeid,vPartsSalesOrderTypeName,vStoragecompanynameP/*vWareHouseNameP*/,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,
                        nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,PartCheckId,vCode,1/*nPartssalescategoryid*/,null/*sysdate*/,/*1*/3,nCreatorID,vCreatorName,sysdate,null/*sysdate*/,iSalesCategoryIdP/*iSalesCategoryId*/,'统购分销发运',8);


                        ---PartsInboundPlan配件入库计划
                        nPartsInboundPlanid := s_PartsInboundPlan.Nextval;
                        nPartsInboundPlanidD:=nPartsInboundPlanid;
                        vPartsInboundPlanCode := regexp_replace(regexp_replace(vInboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateInboundPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-9 hr modify 入库计划单 对方单位 赋值有问题，不应是仓库，品牌赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值，入库计划单的源单据 应是 发运单
                          2017-10-13 hr add 入库计划单备注默认'统购分销发运'；抵达日期 不赋值，客户账户Id不赋值*/
                        insert into PartsInboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,
                          sourceid,sourcecode,inboundtype,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,arrivaldate,creatorid,creatorname,createtime,IfWmsInterface,Remark)
                        values(nPartsInboundPlanid,vPartsInboundPlanCode,nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytypeG,ibranchid,vbranchcode,vbranchname,iSalesCategoryIdG/*nPartssalescategoryid*//*ipartssalesordertypeid*/,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,/*nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,*/
                        nPartsShippingOrderid/*PartCheckId*/,vPartsShippingOrderCode/*vCode*/,3,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,2,null/*sysdate*/,nCreatorID,vCreatorName,sysdate,0,'统购分销发运');
                        ---inboundtype:3 配件调拨

                        ---PartsInboundCheckBill配件入库检验单
                        nPartsInboundCheckBillid := s_PartsInboundCheckBill.Nextval;
                        nPartsInboundCheckBillidD:=nPartsInboundCheckBillid;
                        vPartsInboundCheckBillCode := regexp_replace(regexp_replace(vInboundCheckBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateInboundCheckBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-10 hr modify 入库检验单 仓库 仓储企业 品牌 对方单位 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-16 hr modify 客户账户Id不赋值，状态是 3-上架完成*/
                        insert into PartsInboundCheckBill(id,code,partsinboundplanid,warehouseid, warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,partssalescategoryid,branchid,branchcode,branchname,
                          counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,inboundtype,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,settlementstatus,creatorid,creatorname,createtime)
                        values (nPartsInboundCheckBillid,vPartsInboundCheckBillCode,nPartsInboundPlanid,nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytypeG,iSalesCategoryIdG/*nPartssalescategoryid*/,ibranchid,vbranchcode,vbranchname,
                        iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,/*iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,*/3,null/*ncustomeraccountid*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,3/*1*/,2,nCreatorID,vCreatorName,sysdate);
                        ---inboundtype:3 配件调拨. settlementstatus=2 待结算

                        ---PartsShiftOrder配件移库单
                        nPartsShiftOrderid := s_PartsShiftOrder.Nextval;
                        nPartsShiftOrderidD := nPartsShiftOrderid;
                        vPartsShiftOrderCode := regexp_replace(regexp_replace(vShiftOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateShiftOrderId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        insert into  PartsShiftOrder(id,code,branchid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,status,type,creatorid,creatorname,createtime)
                        values(nPartsShiftOrderid,vPartsShiftOrderCode,ibranchid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,nStoragecompanytype,1,2,nCreatorID,vCreatorName,sysdate);

                        ---PartsShippingOrderRef配件发运单关联单
                        insert into PartsShippingOrderRef(Id,PartsShippingOrderId,PartsOutboundBillId)
                        values(s_PartsShippingOrderRef.Nextval,nPartsShippingOrderid,nPartsOutboundBillId);

                        ----仓库PartsOutboundPlan配件出库计划 （首选出库仓库）
                        nPartsoutboundplanidG := s_PartsOutboundPlan.Nextval;
                        nPartsoutboundplanidGD :=nPartsoutboundplanidG;
                        vPartsOutboundPlanCodeG := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 出库计划单收货仓库id，编号，名称 需要赋值
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                          2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface,Remark,Companyaddressid)
                        values(nPartsoutboundplanidG,vPartsOutboundPlanCodeG,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdG/*iSalesCategoryId*/,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                          vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                          sysdate,0,vRemark,vCompanyAddressId);

                        /*values(nPartsoutboundplanid,vPartsOutboundPlanCode,iWarehouseidP,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,nPartssalescategoryid,
                        vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                        vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);*/
                        nPartsTransferOrderidD := nPartsTransferOrderid;
                        nPartsoutboundplanidD := nPartsoutboundplanid;
                        nPartsOutboundBillIdD := nPartsOutboundBillId;
                        nPartsInboundPlanidD := nPartsInboundPlanid;
                        nPartsInboundCheckBillidD := nPartsInboundCheckBillid;
                        nPartsoutboundplanidGD := nPartsoutboundplanidG;
                        nPartsshippingorderidD := nPartsshippingorderid;
                        nPartsshiftorderidD := nPartsshiftorderid;

                        bInsertOutboundPlanDT := true;
                      end;
                      end if;


                      ---PartsTransferOrderDetail配件调拨单清单
                      ---  remark,
                      insert into tempPartsTransferOrderDetail(id,partstransferorderid,sparepartid,sparepartcode,sparepartname,plannedamount,confirmedamount,price,plannprice)
                      /*2017-9-3 hr modify nPartsTransferOrderid会取最近一个新配件调拨单的ID
                        2017-10-10 hr modify 调拨单清单的计划价 取 配件计划价，品牌取调拨入库仓库的品牌*/
                      values(s_PartsTransferOrderDetail.Nextval,nPartsTransferOrderidD/*nPartsTransferOrderid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityD,nLockedQuantityD,nvl(nOrderPrice,0),/*nvl(nOrderPrice,0)*/
                      (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG/*iSalesCategoryId*/));

                      ---PartsOutboundPlanDetail配件出库计划清单 首选仓库
                      insert into tempPartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price)
                      /*2017-9-3 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                      values(s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidD/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityD,nLockedQuantityD,nvl(nOrderPrice,0));

                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,仓库，这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,nLockedQuantityD,nvl(nOrderPrice,0),2,nOrderProcessMethod,sysdate,nCreatorId,vCreatorName);

                      ---PartsInboundPlanDetail配件入库计划清单
                      insert into tempPartsInboundPlanDetail(id,partsinboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,inspectedquantity,price)
                      /*2017-9-3 hr modify nPartsInboundPlanid会取最近一个新配件入库计划单的ID*/
                      values(s_PartsInboundPlanDetail.Nextval,nPartsInboundPlanidD/*nPartsinboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityD,nLockedQuantityD,nvl(nOrderPrice,0));

                      --首选仓库此次满足金额
                      ---mCurrentFulfilledAmountP:=(nSumQuantity-nvl(nSumLockedQuantity,0))*nvl(nOrderPrice,0);


                      select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId
                      and quantity=(select max(quantity) from tempPARTSSTOCK where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rownum=1);
                      /*2017-8-4 hr modify nLockedQuantity 在统购分销中没有用到，且tempPARTSLockedSTOCK没有数据，查询插入变量会报错*/
                      begin
                        select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                      exception
                      when no_data_found then
                        nLockedQuantity := 0;
                      end;
                      --统购仓库配件出库单清单（tempPartsOutboundBillDetail）记录
                      /*2017-9-3 hr modify nPartsOutboundBillId会取最近一个新配件出库单的ID
                        2017-10-11 hr modify 品牌取调拨出库仓库的品牌
                        2017-10-17 hr modify 出库清单的库位ID与编号赋值有问题*/
                      insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                      values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillIdD/*nPartsOutboundBillId*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityD,nWarehouseAreaIdD,nWarehouseAreaCodeD,/*nPublicwarehouseid,vWareHouseCodeP,*/nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdP/*iSalesCategoryId*/));

                      /*
                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        if nQuantity-nvl(nLockedQuantity,0)>0 then --此库区可用数量
                        begin

                          nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --剩下没满足数量
                          ---nWRemainQuantity1:=nWRemainQuantity;

                          --统购仓库配件出库单清单（tempPartsOutboundBillDetail）记录
                          insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,(nQuantity-nvl(nLockedQuantity,0)),nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));

                        end;
                        end if;

                      end loop; */

                      ---根据首选出库仓库ID和配件ID，检验区
                      ---select WarehouseAreaId,Quantity into iWarehouseAreaIdC,iQuantityC from PartsStock where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaCategoryId=3 and StorageCompanyId=iStorageCompanyId and BranchId=iBranchId and rownum = 1;
                      ---select TopLevelWarehouseAreaId,Code into iWarehouseAreaIdC,vCodeC from WarehouseArea where WarehouseId=nPublicwarehouseidG and AreaCategoryId=3 and rownum = 1;
                      select a.id,a.Code into iWarehouseAreaIdC,vCodeC from WarehouseArea a, WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.AreaCategoryId=b.id and b.category=3 and rownum=1 and a.areakind=3;
                      if iWarehouseAreaIdC>0 then
                      begin
                        ---根据首选出库仓库ID和配件ID，保管区
                        ---select WarehouseAreaId,Quantity into iWarehouseAreaIdS,nQuantityS from PartsStock a where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and StorageCompanyId=nStorageCompanyId and BranchId=iBranchId and PartId=iSparePartId and rownum = 1;
                        select count(*) into iCount from PartsStock a,WarehouseAreaCategory b where  a.WarehouseId=nPublicwarehouseidG and a.WarehouseAreaCategoryId=b.id and b.category=1 and a.StorageCompanyId=iStoragecompanyidG and a.BranchId=iBranchId and a.PartId=iSparePartId;
                        --if iWarehouseAreaIdS>0 then
                        if iCount>0 then
                        begin
                          ---PartsShiftOrderDetail配件移库单清单
                          ---,batchnumber,remark
                          select a.WarehouseAreaId,a.Quantity,b.id,a.id into iWarehouseAreaIdS,nQuantityS,nWarehouseareacategoryidS,nPartsStockidS from PartsStock a,WarehouseAreaCategory b where a.partid=iSparePartId and a.WarehouseId=nPublicwarehouseidG and a.WarehouseAreaCategoryId=b.id and b.category=1 and a.StorageCompanyId=iStoragecompanyidG and a.BranchId=iBranchId and a.PartId=iSparePartId and rownum = 1;
                          insert into tempPartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
                          /*2017-9-3 hr modify nPartsshiftorderid会取最近一个新配件移库单的ID*/
                          values(s_PartsShiftOrderDetail.Nextval,nPartsshiftorderidD/*nPartsshiftorderid*/,iSparePartId,vSparepartcode,vSparepartname,iWarehouseAreaIdC,vCodeC,3,
                          iWarehouseAreaIdS,(select code from WarehouseArea where WarehouseId=nPublicwarehouseidG and id=iWarehouseAreaIdC),1,nLockedQuantityD);

                          --update PartsStock set Quantity=Quantity+nLockedQuantityD where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaId=iWarehouseAreaIdS and WarehouseAreaCategoryId=（select id from WarehouseAreaCategory where category=3 ） and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          update PartsStock set Quantity=Quantity+nLockedQuantityD where id=nPartsStockidS;

                          ---统购分销仓库减库存
                          update PartsStock set Quantity=Quantity-nLockedQuantityD where id=nTPartsStockidD;
                          select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          ---锁定库存
                          if iPartsLockedStock>0 then
                            update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityD,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          else
                            insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                            values(s_PartsLockedStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,iBranchId,iSparePartId,nLockedQuantityD,nCreatorId,vCreatorName,sysdate);
                          end if;
                          ---锁定库存
                          --update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityD where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;

                        end;
                       else
                        begin
                          ---select TopLevelWarehouseAreaId,code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea where WarehouseId=nPublicwarehouseidG and AreaCategoryId=1 and TopLevelWarehouseAreaId not in(
                          ---select a.\*TopLevelWarehouseAreaId*\,a.code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and a.\*TopLevelWarehouseAreaId*\ not in(
                          ---select WarehouseAreaId from PartsStock where  WarehouseId=nPublicwarehouseidG ) and  rownum = 1;

                          select a.id,a.code,b.id into iWarehouseAreaIdS,vWarehouseAreaCodeS,nWarehouseareacategoryidS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and rownum = 1;
                          --select a.TopLevelWarehouseAreaId,a.code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nDefaultoutwarehouseid and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and a.TopLevelWarehouseAreaId not in(
                          --select WarehouseAreaId from PartsStock where  WarehouseId=nDefaultoutwarehouseid ) and  rownum = 1;
                          if iWarehouseAreaIdS>0 then
                          begin
                            ---PartsShiftOrderDetail配件移库单清单
                            ---,batchnumber,remark
                            insert into tempPartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
                            /*2017-9-3 hr modify nPartsshiftorderid会取最近一个新配件移库单的ID*/
                            values(s_PartsShiftOrderDetail.Nextval,nPartsshiftorderidD/*nPartsshiftorderid*/,iSparePartId,vSparepartcode,vSparepartname,iWarehouseAreaIdC,vCodeC,3,
                              iWarehouseAreaIdS,(select code from WarehouseArea where WarehouseId=nPublicwarehouseidG and id=iWarehouseAreaIdC),nWarehouseareacategoryidS,nLockedQuantityD);

                            insert into PartsStock(id,warehouseid,storagecompanyid,storagecompanytype,branchid,warehouseareaid,warehouseareacategoryid,partid,quantity,creatorid,creatorname,createtime)
                            values(s_PartsStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,nstoragecompanytype,iBranchId,iWarehouseAreaIdS,nWarehouseareacategoryidS,iSparePartId,nLockedQuantityD,nCreatorId,vCreatorName,sysdate);
                            ---update PartsStock set Quantity:=Quantity+(nSumQuantity-nvl(nSumLockedQuantity,0)) where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaId=iWarehouseAreaIdS and WarehouseAreaCategoryId=3 and StorageCompanyId=iStorageCompanyId and BranchId=iBranchId;

                            ---统购分销仓库减库存
                            update PartsStock set Quantity=Quantity-nLockedQuantityD where id=nTPartsStockidD;

                            select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            ---锁定库存
                            if iPartsLockedStock>0 then
                              update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityD,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            else
                              insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                              values(s_PartsLockedStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,iBranchId,iSparePartId,nLockedQuantityD,nCreatorId,vCreatorName,sysdate);
                            end if;
                            ---锁定库存
                            --update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityD where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;

                          end;

                          end if;
                        end;
                        end if;

                      end;
                      end if;

                      ---PartsInboundCheckBillDetail配件入库检验单清单
                      /*2017-9-3 hr modify nPartsInboundCheckBillid会取最近一个新配件入库检验单的ID
                        2017-10-10 hr modify 入库检验单清单的成本价 取 配件计划价，品牌取调拨入库仓库的品牌
                        2017-10-17 hr modify 入库检验单清单的库位ID与编号赋值有问题，不应是 检验区的库位*/
                      insert into tPartsInboundCheckBillDetail(id,partsinboundcheckbillid,sparepartid,sparepartcode,sparepartname,warehouseareaid,warehouseareacode,inspectedquantity,settlementprice,costprice)
                      values (s_PartsInboundCheckBillDetail.Nextval,nPartsInboundCheckBillidD/*nPartsinboundcheckbillid*/,iSparePartId,vSparepartcode,vSparepartname,/*iWarehouseAreaIdC,vCodeC*/
                      iWarehouseAreaIdS,(select code from WarehouseArea where id =iWarehouseAreaIdS and areakind=3),nLockedQuantityD,nvl(nOrderPrice,0),/*nvl(nOrderPrice,0)*/
                      (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG/*iSalesCategoryId*/));

                      ---PartsShippingOrderDetail配件发运单清单
                      ---differenceclassification
                      insert into tempPartsShippingOrderDetail(id,partsshippingorderid,sparepartid,sparepartcode,sparepartname,shippingamount,confirmedamount,settlementprice)
                      /*2017-9-3 hr modify nPartsshippingorderidD会取最近一个新配件发运单的ID*/
                      values(s_PartsShippingOrderDetail.Nextval,nPartsshippingorderidD/*nPartsshippingorderid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityD,nLockedQuantityD,nvl(nOrderPrice,0));

                      ---PartsOutboundPlanDetail配件出库计划清单
                      insert into tempPartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,/*outboundfulfillment,*/price)
                      /*2017-9-3 hr modify nPartsoutboundplanidG会取最近一个新出库计划单的ID*/
                      values(s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidGD/*nPartsoutboundplanidG*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityD,/*nLockedQuantityD,*/nvl(nOrderPrice,0));

                      ---首先仓库剩下没有满足数量
                      /*2017-8-4 hr modify nWRemainQuantity注释取消*/
                      nWRemainQuantity:= nWRemainQuantity - nLockedQuantityD;
                      --nWRemainQuantity1:=nWRemainQuantity-nLockedQuantityD;
                      nWRemainQuantity1 := nWRemainQuantity;
                      iCheckOrderDetailSum  := iCheckOrderDetailSum +1;

                    end;
                    elsif (nSumQuantity-nvl(nSumLockedQuantity,0)) >= nWRemainQuantity then
                      ---首选仓库完全满足
                    begin

                      /*2017-10-11 hr add 对出入库计划价进行校验
                      1）计划价必须存在，且>0
                      2）出入库仓库的计划价必须一致*/
                      begin
                        select PlannedPrice into nPlannedPriceG from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG;
                      exception
                      when no_data_found then
                        begin
                          vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameG||']没有计划价';
                          raise FMyException;
                        end;
                      end;
                      if nPlannedPriceG = 0 then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameG||']的计划价为0';
                        raise FMyException;
                      end;
                      end if;
                      begin
                        select PlannedPrice into nPlannedPriceP from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdP;
                      exception
                      when no_data_found then
                        begin
                          vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']没有计划价';
                          raise FMyException;
                        end;
                      end;
                      if nPlannedPriceP = 0 then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']的计划价为0';
                        raise FMyException;
                      end;
                      end if;
                      if nPlannedPriceP <> nPlannedPriceG then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']的计划价与品牌['||vSalesCategoryNameG||']的计划价不一致';
                        raise FMyException;
                      end;
                      end if;

                      if bInsertOutboundPlanDT = false then
                      begin
                        ---收货单位Id  ReceivingCompanyId,收货单位编号  ReceivingCompanyCode,收货单位名称  ReceivingCompanyName 填为  nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG


                        ---PartsTransferOrder配件调拨单
                        nPartsTransferOrderid := s_PartsTransferOrder.Nextval;
                        vPartsTransferOrderCode := regexp_replace(regexp_replace(vTransferOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateTransferOrderId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-8-7 hr modify 调拨单状态为 已审批
                          2017-10-12 hr modify 调拨单的调拨类型 应该是 3--急需调拨；发运方式设置为空
                          2017-10-13 hr add 修改人，审批人，修改时间，审批时间赋值*/
                        insert into PartsTransferOrder (id,code,originalwarehouseid,originalwarehousecode,originalwarehousename,storagecompanyid,storagecompanytype,destwarehouseid,destwarehousecode,destwarehousename,status,type,
                          shippingmethod,creatorid,creatorname,createtime,originalbillid,originalbillcode,totalamount,Modifierid,Modifiername,Modifytime,Approverid,Approvername,Approvetime)
                        values(nPartsTransferOrderid,vPartsTransferOrderCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,nStoragecompanytype,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,/*1*/2,/*1*/3,
                        null/*iShippingMethod*/,nCreatorID,vCreatorName,sysdate,PartCheckId,vCode,0,nCreatorID,vCreatorName,sysdate,nCreatorID,vCreatorName,sysdate);
                        --- totalamount,totalplanamount需要计算。

                        ----PartsOutboundPlan配件出库计划 (首选仓库)
                        nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                        vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值，出库计划单的源单据 应是 调拨单
                          2017-10-12 hr modify 调拨出库生成的出库计划单的出库类型 应是 调拨出库；对方单位赋值有问题
                          2017-10-16 hr modify 发运方式设置为空，客户账户Id默认null*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface )
                        values(nPartsoutboundplanid,vPartsOutboundPlanCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdP/*iSalesCategoryId*/,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPartsTransferOrderid/*PartCheckId*/,
                          vPartsTransferOrderCode/*vCode*/,4/*1*/,null/*iShippingMethod*/,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,3,nCreatorID,vCreatorName,sysdate,0);

                        ---新建配件出库单
                        --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                        ---CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}
                        vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        nPartsOutboundBillId := s_PartsOutboundBill.Nextval;
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-13 hr modify 调拨出库生成的出库单的出库类型 应是 调拨出库 配件调拨；备注是'统购分销出库'；结算状态是 待结算；对方单位赋值有问题
                          2017-10-16 hr modify 发运方式设置为空，客户账户Id默认null*/
                        insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                          partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,receivingcompanyid,receivingcompanycode, receivingcompanyname,
                          receivingwarehouseid,receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                          creatorid,creatorname,createtime,Remark)
                        Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdP/*iSalesCategoryId*/,
                          iPartssalesordertypeid,vPartssalesordertypename,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,
                          nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,4/*1*/,null/*iShippingMethod*/,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,2/*1*/,
                          nCreatorID,vCreatorName,sysdate,'统购分销出库');


                        ---PartsShippingOrder配件发运单  /*入库计划单的源单据是 发运单，所以调整发运单生成的位置，放到入库计划单生成前*/
                        nPartsShippingOrderid := s_PartsShippingOrder.Nextval;
                        /*2017-10-12 hr modify 发运单应是4位流水号*/
                        vPartsShippingOrderCode := regexp_replace(regexp_replace(vShippingOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateShippingOrderId,TRUNC(sysdate,'DD'),vBranchCode)),4/*6*/,'0'));
                        /*2017-9-6 hr modify 发运单的发货单位与结算单位赋值有问题，状态不应是新建，应该是 回执确认
                          2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 配件发运单增加品牌字段赋值
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-12 hr add 发运单备注默认'统购分销发运'
                          2017-10-13 hr add 发运方式默认 8--客户自行运输；发运日期 不赋值；收货单位赋值有问题；预计到货日期 不赋值*/
                        insert into PartsShippingOrder(id,code,type,branchid,shippingcompanyid,shippingcompanycode,shippingcompanyname,settlementcompanyid,settlementcompanycode,settlementcompanyname,receivingcompanyid,receivingcompanycode,invoicereceivesalecateid,invoicereceivesalecatename,receivingcompanyname, warehouseid,warehousecode,warehousename,
                          receivingwarehouseid,receivingwarehousecode,receivingwarehousename,originalrequirementbillid,originalrequirementbillcode,originalrequirementbilltype,shippingdate,status,creatorid,creatorname,createtime,expectedplacedate,PartsSalesCategoryId,Remark,Shippingmethod)
                        values(nPartsShippingOrderid,vPartsShippingOrderCode,4,ibranchid,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,*/iStoragecompanyidP,vStoragecompanycodeP,/*nPublicwarehouseidG,vWareHouseCodeG ,*/iPartssalesordertypeid,vPartsSalesOrderTypeName,vStoragecompanynameP/*vWareHouseNameP*/,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,
                        nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,PartCheckId,vCode,1/*nPartssalescategoryid*/,null/*sysdate*/,/*1*/3,nCreatorID,vCreatorName,sysdate,null/*sysdate*/,iSalesCategoryIdP/*iSalesCategoryId*/,'统购分销发运',8);


                        ---PartsInboundPlan配件入库计划
                        nPartsInboundPlanid := s_PartsInboundPlan.Nextval;
                        vPartsInboundPlanCode := regexp_replace(regexp_replace(vInboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateInboundPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-9 hr modify 入库计划单 对方单位 赋值有问题，不应是仓库，品牌赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值，入库计划单的源单据 应是 发运单
                          2017-10-13 hr add 入库计划单备注默认'统购分销发运'；抵达日期 不赋值，客户账户Id不赋值*/
                        insert into PartsInboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,
                          sourceid,sourcecode,inboundtype,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,arrivaldate,creatorid,creatorname,createtime,IfWmsInterface,Remark)
                        values(nPartsInboundPlanid,vPartsInboundPlanCode,nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytypeG,ibranchid,vbranchcode,vbranchname,iSalesCategoryIdG/*nPartssalescategoryid*//*ipartssalesordertypeid*/,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,/*nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,*/
                        nPartsShippingOrderid/*PartCheckId*/,vPartsShippingOrderCode/*vCode*/,3,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,2,null/*sysdate*/,nCreatorID,vCreatorName,sysdate,0,'统购分销发运');
                        ---inboundtype:3 配件调拨

                        ---PartsInboundCheckBill配件入库检验单
                        nPartsInboundCheckBillid := s_PartsInboundCheckBill.Nextval;
                        vPartsInboundCheckBillCode := regexp_replace(regexp_replace(vInboundCheckBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateInboundCheckBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-10 hr modify 入库检验单 仓库 仓储企业 品牌 对方单位 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-16 hr modify 客户账户Id不赋值，状态是 3-上架完成*/
                        insert into PartsInboundCheckBill(id,code,partsinboundplanid,warehouseid, warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,partssalescategoryid,branchid,branchcode,branchname,
                          counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,inboundtype,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,settlementstatus,creatorid,creatorname,createtime)
                        values (nPartsInboundCheckBillid,vPartsInboundCheckBillCode,nPartsInboundPlanid,nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytypeG,iSalesCategoryIdG/*nPartssalescategoryid*/,ibranchid,vbranchcode,vbranchname,
                        iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,/*iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,*/3,null/*ncustomeraccountid*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,3/*1*/,2,nCreatorID,vCreatorName,sysdate);
                        ---inboundtype:3 配件调拨. settlementstatus=2 待结算

                        ---PartsShiftOrder配件移库单
                        nPartsShiftOrderid := s_PartsShiftOrder.Nextval;
                        nPartsShiftOrderidD := nPartsShiftOrderid;
                        vPartsShiftOrderCode := regexp_replace(regexp_replace(vShiftOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateShiftOrderId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        insert into  PartsShiftOrder(id,code,branchid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,status,type,creatorid,creatorname,createtime)
                        values(nPartsShiftOrderid,vPartsShiftOrderCode,ibranchid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,nStoragecompanytype,1,2,nCreatorID,vCreatorName,sysdate);

                        ---PartsShippingOrderRef配件发运单关联单
                        insert into PartsShippingOrderRef(Id,PartsShippingOrderId,PartsOutboundBillId)
                        values(s_PartsShippingOrderRef.Nextval,nPartsShippingOrderid,nPartsOutboundBillId);

                        ----仓库PartsOutboundPlan配件出库计划 （首选出库仓库）
                        nPartsoutboundplanidG := s_PartsOutboundPlan.Nextval;
                        vPartsOutboundPlanCodeG := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 出库计划单收货仓库id，编号，名称 需要赋值
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                          2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface,Remark,Companyaddressid)
                        values(nPartsoutboundplanidG,vPartsOutboundPlanCodeG,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdG/*iSalesCategoryId*/,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                          vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                          sysdate,0,vRemark,vCompanyAddressId);

                        /*values(nPartsoutboundplanid,vPartsOutboundPlanCode,iWarehouseidP,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,nPartssalescategoryid,
                        vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                        vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);*/
                        nPartsTransferOrderidD := nPartsTransferOrderid;
                        nPartsoutboundplanidD := nPartsoutboundplanid;
                        nPartsOutboundBillIdD := nPartsOutboundBillId;
                        nPartsInboundPlanidD := nPartsInboundPlanid;
                        nPartsInboundCheckBillidD := nPartsInboundCheckBillid;
                        nPartsoutboundplanidGD := nPartsoutboundplanidG;
                        nPartsshippingorderidD := nPartsshippingorderid;
                        nPartsshiftorderidD := nPartsshiftorderid;
                        bInsertOutboundPlanDT := true;
                      end;
                      end if;

                      --首选仓库此次满足数量和金额
                      ---(nSumQuantity-nvl(nSumLockedQuantity,0)) >= nWRemainQuantity
                      if nMaxQuantityD <= nWRemainQuantity then
                      begin
                         mCurrentFulfilledAmountD:=nMaxQuantityD*nvl(nOrderPrice,0);
                         nLockedQuantityD := nMaxQuantityD;
                      end;
                      else
                      begin
                         mCurrentFulfilledAmountD:=nWRemainQuantity*nvl(nOrderPrice,0);
                         nLockedQuantityD := nWRemainQuantity;
                      end;
                      end if;

                      ---PartsTransferOrderDetail配件调拨单清单
                      ---  remark,
                      insert into tempPartsTransferOrderDetail(id,partstransferorderid,sparepartid,sparepartcode,sparepartname,plannedamount,confirmedamount,price,plannprice)
                      /*2017-9-3 hr modify nPartsTransferOrderid会取最近一个新配件调拨单的ID
                        2017-10-10 hr modify 调拨单清单的计划价 取 配件计划价，品牌取调拨入库仓库的品牌*/
                      values(s_PartsTransferOrderDetail.Nextval,nPartsTransferOrderidD/*nPartsTransferOrderid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityD,nLockedQuantityD,nvl(nOrderPrice,0),/*nvl(nOrderPrice,0)*/
                      (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG/*iSalesCategoryId*/));

                      ---PartsOutboundPlanDetail配件出库计划清单 首选仓库
                      insert into tempPartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price)
                      /*2017-9-3 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                      values(s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidD/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityD,nLockedQuantityD,nvl(nOrderPrice,0));


                      ---PartsInboundPlanDetail配件入库计划清单
                      insert into tempPartsInboundPlanDetail(id,partsinboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,inspectedquantity,price)
                      /*2017-9-3 hr modify nPartsInboundPlanid会取最近一个新配件入库计划单的ID*/
                      values(s_PartsInboundPlanDetail.Nextval,nPartsInboundPlanidD/*nPartsinboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityD,nLockedQuantityD,nvl(nOrderPrice,0));



                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,匹配仓库，这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,nLockedQuantityD,nvl(nOrderPrice,0),1,nOrderProcessMethod,sysdate,nCreatorId,vCreatorName);

                      /*
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,(nSumQuantity-nvl(nSumLockedQuantity,0)),nvl(nOrderPrice,0),2,2,sysdate,nCreatorId,vCreatorName);
                      */


                      /*
                      mCurrentFulfilledAmountP:=nWRemainQuantity*nvl(nOrderPrice,0);

                      ----res:=mCurrentFulfilledAmountP;
                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        if nQuantity-nvl(nLockedQuantity,0)>0 then --此库区可用数量
                        begin

                          if nQuantity-nvl(nLockedQuantity,0)>= nWRemainQuantity then
                          begin

                            insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                            values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,nWRemainQuantity,nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));
                            nWRemainQuantity:= 0;
                            exit;
                          end;
                          else
                          begin
                            --统购仓库配件出库单清单（tempPartsOutboundBillDetail）记录
                            insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                            values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,(nQuantity-nvl(nLockedQuantity,0)),nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                              (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));

                              nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --剩下没满足数量
                          end;
                          ---nWRemainQuantity1:=nWRemainQuantity;
                          end if;
                        end;
                        end if;

                      end loop;
                      */

                      ---首先仓库剩下没有满足数量
                      nWRemainQuantity:= nWRemainQuantity-nLockedQuantityD;


                      ---select WarehouseAreaId,Quantity into iWarehouseAreaIdC,iQuantityC from PartsStock where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaCategoryId=3 and StorageCompanyId=iStorageCompanyId and BranchId=iBranchId and rownum = 1;
                      ---判断此仓库是否有检验区
                    /*
                     begin

                      exception
                        when no_data_found then
                           begin
                             raise_application_error(-20001,'未找到仓库检验区。');
                           end;
                      end;
                     */
                      ---根据首选出库仓库ID和配件ID，检验区
                      ---select a./*TopLevelWarehouseAreaId*/,a.Code into iWarehouseAreaIdC,vCodeC from WarehouseArea a, WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.AreaCategoryId=b.id and b.category=3 and rownum=1 and a.areakind=3;
                      select a.id,a.Code into iWarehouseAreaIdC,vCodeC from WarehouseArea a, WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.AreaCategoryId=b.id and b.category=3 and rownum=1 and a.areakind=3;
                      if iWarehouseAreaIdC>0 then
                      begin

                        ---根据首选出库仓库ID和配件ID，保管区
                        ---select WarehouseAreaId,Quantity into iWarehouseAreaIdS,nQuantityS from PartsStock a where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and StorageCompanyId=nStorageCompanyId and BranchId=iBranchId and PartId=iSparePartId and rownum = 1;
                        select count(*) into iCount from PartsStock a,WarehouseAreaCategory b where  a.WarehouseId=nPublicwarehouseidG and a.WarehouseAreaCategoryId=b.id and b.category=1 and a.StorageCompanyId=iStoragecompanyidG and a.BranchId=iBranchId and a.PartId=iSparePartId;
                        --if iWarehouseAreaIdS>0 then
                        if iCount>0 then
                        begin
                          ---PartsShiftOrderDetail配件移库单清单
                          ---,batchnumber,remark
                          select a.WarehouseAreaId,a.Quantity,b.id,a.id into iWarehouseAreaIdS,nQuantityS,nWarehouseareacategoryidS,nPartsStockidS from PartsStock a,WarehouseAreaCategory b where a.partid=iSparePartId and a.WarehouseId=nPublicwarehouseidG and a.WarehouseAreaCategoryId=b.id and b.category=1 and a.StorageCompanyId=iStoragecompanyidG and a.BranchId=iBranchId and a.PartId=iSparePartId and rownum = 1;
                          insert into tempPartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
                          /*2017-9-3 hr modify nPartsshiftorderid会取最近一个新配件移库单的ID*/
                          values(s_PartsShiftOrderDetail.Nextval,nPartsshiftorderidD/*nPartsshiftorderid*/,iSparePartId,vSparepartcode,vSparepartname,iWarehouseAreaIdC,vCodeC,3,
                          iWarehouseAreaIdS,(select code from WarehouseArea where WarehouseId=nPublicwarehouseidG and id=iWarehouseAreaIdC),1,nLockedQuantityD);

                          --update PartsStock set Quantity=Quantity+nLockedQuantityD where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaId=iWarehouseAreaIdS and WarehouseAreaCategoryId=（select id from WarehouseAreaCategory where category=3 ） and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          update PartsStock set Quantity=Quantity+nLockedQuantityD where id=nPartsStockidS;

                          ---统购分销仓库减库存
                          update PartsStock set Quantity=Quantity-nLockedQuantityD where id=nTPartsStockidD;
                          select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            ---锁定库存
                            if iPartsLockedStock>0 then
                              update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityD,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            else
                              insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                              values(s_PartsLockedStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,iBranchId,iSparePartId,nLockedQuantityD,nCreatorId,vCreatorName,sysdate);
                            end if;
                          ---锁定库存
                          --update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityD where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                        end;
                       else
                        begin
                          ---select TopLevelWarehouseAreaId,code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea where WarehouseId=nPublicwarehouseidG and AreaCategoryId=1 and TopLevelWarehouseAreaId not in(
                          ---select a.\*TopLevelWarehouseAreaId*\,a.code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and a.\*TopLevelWarehouseAreaId*\ not in(
                          ---select WarehouseAreaId from PartsStock where  WarehouseId=nPublicwarehouseidG ) and  rownum = 1;

                          select a.id,a.code,b.id into iWarehouseAreaIdS,vWarehouseAreaCodeS,nWarehouseareacategoryidS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and rownum = 1;
                          --select a.TopLevelWarehouseAreaId,a.code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nDefaultoutwarehouseid and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and a.TopLevelWarehouseAreaId not in(
                          --select WarehouseAreaId from PartsStock where  WarehouseId=nDefaultoutwarehouseid ) and  rownum = 1;
                          if iWarehouseAreaIdS>0 then
                          begin
                            ---PartsShiftOrderDetail配件移库单清单
                            ---,batchnumber,remark
                            insert into tempPartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
                            /*2017-9-3 hr modify nPartsshiftorderid会取最近一个新配件移库单的ID*/
                            values(s_PartsShiftOrderDetail.Nextval,nPartsshiftorderidD/*nPartsshiftorderid*/,iSparePartId,vSparepartcode,vSparepartname,iWarehouseAreaIdC,vCodeC,3,
                              iWarehouseAreaIdS,(select code from WarehouseArea where WarehouseId=nPublicwarehouseidG and id=iWarehouseAreaIdC),nWarehouseareacategoryidS,nLockedQuantityD);

                            insert into PartsStock(id,warehouseid,storagecompanyid,storagecompanytype,branchid,warehouseareaid,warehouseareacategoryid,partid,quantity,creatorid,creatorname,createtime)
                            values(s_PartsStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,nstoragecompanytype,iBranchId,iWarehouseAreaIdS,nWarehouseareacategoryidS,iSparePartId,nLockedQuantityD,nCreatorId,vCreatorName,sysdate);
                            ---update PartsStock set Quantity:=Quantity+(nSumQuantity-nvl(nSumLockedQuantity,0)) where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaId=iWarehouseAreaIdS and WarehouseAreaCategoryId=3 and StorageCompanyId=iStorageCompanyId and BranchId=iBranchId;

                            ---统购分销仓库减库存
                            update PartsStock set Quantity=Quantity-nLockedQuantityD where id=nTPartsStockidD;

                            select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            ---锁定库存
                            if iPartsLockedStock>0 then
                              update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityD,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            else
                              insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                              values(s_PartsLockedStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,iBranchId,iSparePartId,nLockedQuantityD,nCreatorId,vCreatorName,sysdate);
                            end if;
                            ---锁定库存
                            --update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityD where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                          end;

                          end if;
                        end;
                        end if;

                      end;
                      end if;

                      ---PartsInboundCheckBillDetail配件入库检验单清单
                      /*2017-9-3 hr modify nPartsInboundCheckBillid会取最近一个新配件入库检验单的ID
                        2017-10-10 hr modify 入库检验单清单的成本价 取 配件计划价，品牌取调拨入库仓库的品牌
                        2017-10-17 hr modify 入库检验单清单的库位ID与编号赋值有问题，不应是 检验区的库位*/
                      insert into tPartsInboundCheckBillDetail(id,partsinboundcheckbillid,sparepartid,sparepartcode,sparepartname,warehouseareaid,warehouseareacode,inspectedquantity,settlementprice,costprice)
                      values (s_PartsInboundCheckBillDetail.Nextval,nPartsInboundCheckBillidD/*nPartsinboundcheckbillid*/,iSparePartId,vSparepartcode,vSparepartname,/*iWarehouseAreaIdC,vCodeC*/
                      iWarehouseAreaIdS,(select code from WarehouseArea where id =iWarehouseAreaIdS and areakind=3),nLockedQuantityD,nvl(nOrderPrice,0),/*nvl(nOrderPrice,0)*/
                      (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG/*iSalesCategoryId*/));

                      ---PartsShippingOrderDetail配件发运单清单
                      ---differenceclassification
                      insert into tempPartsShippingOrderDetail(id,partsshippingorderid,sparepartid,sparepartcode,sparepartname,shippingamount,confirmedamount,settlementprice)
                      /*2017-9-3 hr modify nPartsshippingorderidD会取最近一个新配件发运单的ID*/
                      values(s_PartsShippingOrderDetail.Nextval,nPartsshippingorderidD/*nPartsshippingorderid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityD,nLockedQuantityD,nvl(nOrderPrice,0));

                      ---PartsOutboundPlanDetail配件出库计划清单
                      insert into tempPartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,/*outboundfulfillment,*/price)
                      /*2017-9-3 hr modify nPartsoutboundplanidG会取最近一个新出库计划单的ID*/
                      values(s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidGD/*nPartsoutboundplanidG*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityD,/*nLockedQuantityD,*/nvl(nOrderPrice,0));

                      /*2017-8-4 hr add*/
                      --统购仓库配件出库单清单（tempPartsOutboundBillDetail）记录
                      /*2017-9-3 hr modify nPartsOutboundBillId会取最近一个新配件出库单的ID
                        2017-10-11 hr modify 品牌取调拨出库仓库的品牌
                        2017-10-17 hr modify 出库清单的库位ID与编号赋值有问题*/
                      insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                      values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillIdD/*nPartsOutboundBillId*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityD,nWarehouseAreaIdD,nWarehouseAreaCodeD,/*nPublicwarehouseid,vWareHouseCodeP,*/nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdP/*iSalesCategoryId*/));

                      /*
                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        ---查询不到数据会报错
                        ---select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --res:='test';
                        select count(*) into nTest from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;

                        if nvl(nTest,0)>0 then
                           select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --else
                          -- nLockedQuantity:=0;
                        end if;

                        if nQuantity-nvl(nLockedQuantity,0)>0 then --此库区可用数量
                        begin
                          if (nQuantity-nvl(nLockedQuantity,0)) >nWRemainQuantity then
                          begin
                            nWRemainQuantity:= 0; --此库区剩下没满足数量,此次已经完全满足
                            ---update PARTSSTOCK set Quantity=nQuantity-nWRemainQuantity where WarehouseAreaId=nWarehouseAreaId and  WarehouseId=iWarehouseidP  and PartId=iSparePartId;

                          end;
                          else
                          begin
                            nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --此库区剩下没满足数量
                            ---update PARTSSTOCK set Quantity=0 where WarehouseAreaId=nWarehouseAreaId and  WarehouseId=iWarehouseidP  and PartId=iSparePartId;

                          end;
                          end if;

                          nWRemainQuantity1:=nWRemainQuantity;


                          --配件出库单清单（tempPartsOutboundBillDetail）记录
                          insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,(nQuantity-nvl(nLockedQuantity,0)),nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));

                          exit when nWRemainQuantity= 0;
                        end;
                        end if;

                      end loop; */

                    end;
                    end if;
                  end;
                  else ---不是统购分销仓库
                  begin

                    nOrderProcessMethod:=2;

                    select Code,Name,storagecompanyid,storagecompanytype into vWareHouseCodeP,vWareHouseNameP,iStoragecompanyidP,nStoragecompanytype from warehouse where id=nPublicwarehouseid;
                    select code,name into vStoragecompanycodeP,vStoragecompanynameP from company where id=iStoragecompanyidP;



                    -- select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from PartsStock where
                    insert into tempPARTSSTOCK  select rownum,a.* from PARTSSTOCK a  where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1;
                    select sum(quantity) into nSumQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                    select sum(LockedQuantity) into nSumLockedQuantity from Partslockedstock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                    /*2017-8-2 hr add*/
                    insert into tempPARTSLockedSTOCK select id,warehouseid,storagecompanyid,branchid,partid,lockedquantity,creatorid,creatorname,createtime,modifierid,modifiername,modifytime from Partslockedstock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;

                    select count(*) into iPS from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;

                    if (nSumQuantity-nvl(nSumLockedQuantity,0))< nWRemainQuantity1 and (nSumQuantity-nvl(nSumLockedQuantity,0)) > 0 then ---首选仓库部分满足
                    begin
                      nNewLockedQuantity:=nSumQuantity-nvl(nSumLockedQuantity,0);

                    /*
                      ---新建配件出库单
                      ---creatorid,creatorname,createtime,获得
                      --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                      vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                      nPartsOutboundBillId := s_PartsOutboundBill.Nextval;

                      insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                        partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,receivingcompanyid,receivingcompanycode, receivingcompanyname,receivingwarehouseid,
                        receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                        --orderapprovecomment,remark,
                        creatorid,creatorname,createtime)

                      Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,
                        iPartssalesordertypeid,vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingwarehouseid,
                        vReceivingwarehousecode,vReceivingWarehouseName,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);
                      */
                      ---新建出库计划单
                      if bInsertOutboundPlanD=false then ---如果此次审核首选仓库已经生成出库计划主单就不再生成
                      begin
                        nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                        nPartsoutboundplanidD := nPartsoutboundplanid;
                        vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-30 hr modify 出库计划单收货仓库id，编号，名称 需要赋值
                          2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                          2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface,Remark,Companyaddressid)
                        values(nPartsoutboundplanid,vPartsOutboundPlanCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                          vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                          sysdate,0,vRemark,vCompanyAddressId);
                        bInsertOutboundPlanD:=true;
                      end;
                      end if;

                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,仓库，这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,(nSumQuantity-nvl(nSumLockedQuantity,0)),nvl(nOrderPrice,0),2,nOrderProcessMethod,sysdate,nCreatorId,vCreatorName);


                      ---新建出库计划清单
                      insert into tempPartsOutboundPlanDetail (id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price)
                      /*(nQuantity-nvl(nLockedQuantity,0)) 2017-8-2 hr modify nQuantity没有被清空，使用上一个循环中的值*/
                      /*2017-9-2 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                      values (s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidD/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,/*(nQuantity-nvl(nLockedQuantity,0))*/(nSumQuantity-nvl(nSumLockedQuantity,0)),nOrderPrice);

                      --首选仓库此次满足金额
                      mCurrentFulfilledAmountP:=(nSumQuantity-nvl(nSumLockedQuantity,0))*nvl(nOrderPrice,0);

                      /*2017-8-4 hr modify 配件多库位循环赋值剩余未满足量有问题，当锁定库存大于单个库位库存时，剩余未满足量将不会赋值*/
                      nWRemainQuantity:= nWRemainQuantity-(nSumQuantity-nvl(nSumLockedQuantity,0));
                      /*
                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                         ---查询不到数据会报错
                        ---select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --res:='test';
                        select count(*) into nTest from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;

                        if nvl(nTest,0)>0 then
                           --select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                           select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --else
                          -- nLockedQuantity:=0;
                        end if;


                        if nQuantity-nvl(nLockedQuantity,0)>0 then --首选库区可用数量
                        begin

                          ---update PARTSSTOCK set Quantity=nQuantity-nWRemainQuantity where WarehouseAreaId=nWarehouseAreaId and  WarehouseId=iWarehouseidP  and PartId=iSparePartId;
                          nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --剩下没满足数量


                          ---nWRemainQuantity1:=nWRemainQuantity;
                          \*
                          --配件出库单清单（tempPartsOutboundBillDetail）记录
                          insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,(nQuantity-nvl(nLockedQuantity,0)),nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));
                          *\
                        end;
                        end if;

                      end loop;
                      */

                      ---锁定库存
                      select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      if iPartsLockedStock>0 then
                        update PartsLockedStock set LockedQuantity=LockedQuantity+nNewLockedQuantity,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      else
                        insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                        values(s_PartsLockedStock.Nextval,nPublicwarehouseid,iStoragecompanyidP,iBranchId,iSparePartId,nNewLockedQuantity,nCreatorId,vCreatorName,sysdate);
                      end if;

                      ---锁定库存
                      ---update PARTSLockedSTOCK set LockedQuantity=LockedQuantity+nNewLockedQuantity where WarehouseId=nPublicwarehouseid  and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      iCheckOrderDetailSum  := iCheckOrderDetailSum +1;

                      ---nWRemainQuantity:= nWRemainQuantity - (nSumQuantity-nvl(nSumLockedQuantity,0)); --剩下没满足数
                      nWRemainQuantity1:=nWRemainQuantity;

                    end;
                    elsif (nSumQuantity-nvl(nSumLockedQuantity,0)) >= nWRemainQuantity then
                      ---首选仓库完全满足
                    begin

                      nNewLockedQuantity:=nWRemainQuantity; ---需要锁定库存
                      /*
                      --新建配件出库单
                      ---creatorid,creatorname,createtime,获得
                      --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                      vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                      nPartsOutboundBillId := s_PartsOutboundBill.Nextval;

                      insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                        partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,receivingcompanyid,receivingcompanycode, receivingcompanyname,receivingwarehouseid,
                        receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                        --orderapprovecomment,remark,
                        creatorid,creatorname,createtime)

                      Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,
                        iPartssalesordertypeid,vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingwarehouseid,
                        vReceivingwarehousecode,vReceivingWarehouseName,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);
                      */
                      ---新建出库计划单
                      if bInsertOutboundPlanD=false then ---如果此次审首选仓库已经生成出库计划主单就不再生成
                      begin
                        nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                        nPartsoutboundplanidD :=nPartsoutboundplanid;
                        vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 出库计划单收货仓库id，编号，名称 需要赋值
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                          2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface,Remark,Companyaddressid)
                        values(nPartsoutboundplanid,vPartsOutboundPlanCode,nPublicwarehouseid,/*iWarehouseidP,*/vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                          vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                          sysdate,0,vRemark,vCompanyAddressId);
                        bInsertOutboundPlanD:=true;
                      end;
                      end if;

                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,nWRemainQuantity,nvl(nOrderPrice,0),1,2,sysdate,nCreatorId,vCreatorName);

                      ---新建出库计划清单
                      insert into tempPartsOutboundPlanDetail (id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price)
                      /*2017-9-2 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                      values (s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidD/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nWRemainQuantity,nOrderPrice);
                      --首选仓库此次满足金额
                      mCurrentFulfilledAmountP:=nWRemainQuantity*nvl(nOrderPrice,0);

                      /*2017-8-4 hr modify 配件多库位循环赋值剩余未满足量有问题，当锁定库存大于单个库位库存时，剩余未满足量将不会赋值*/
                      nWRemainQuantity:= 0;
                      nWRemainQuantity1:=nWRemainQuantity;
                      /*
                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        --select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        ---查询不到数据会报错
                        ---select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --res:='test';
                        select count(*) into nTest from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;

                        if nvl(nTest,0)>0 then
                           select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --else
                          -- nLockedQuantity:=0;
                        end if;
                        if nQuantity-nvl(nLockedQuantity,0)>0 then --此库区可用数量
                        begin

                          if (nQuantity-nvl(nLockedQuantity,0)) >nWRemainQuantity then
                          begin
                            ---update PARTSSTOCK set Quantity=nQuantity-nWRemainQuantity where WarehouseAreaId=nWarehouseAreaId and  WarehouseId=iWarehouseidP  and PartId=iSparePartId;
                            nWRemainQuantity:= 0; --此库区剩下没满足数量,此次已经完全满足

                          end;
                          else
                          begin
                            ---update PARTSSTOCK set Quantity=0 where WarehouseAreaId=nWarehouseAreaId and  WarehouseId=iWarehouseidP  and PartId=iSparePartId;
                            nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --此库区剩下没满足数量

                           end;
                           end if;

                          nWRemainQuantity1:=nWRemainQuantity;

                          \*
                          --配件出库单清单（tempPartsOutboundBillDetail）记录
                          insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,(nQuantity-nvl(nLockedQuantity,0)),nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));
                          *\
                          exit when nWRemainQuantity= 0;
                        end;
                        end if;

                      end loop;
                      */

                      ---锁定库存
                      select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      if iPartsLockedStock>0 then
                        update PartsLockedStock set LockedQuantity=LockedQuantity+nNewLockedQuantity,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      else
                        insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                        values(s_PartsLockedStock.Nextval,nPublicwarehouseid,iStoragecompanyidP,iBranchId,iSparePartId,nNewLockedQuantity,nCreatorId,vCreatorName,sysdate);
                      end if;
                      ---锁定库存
                      --update PARTSLockedSTOCK set LockedQuantity=LockedQuantity+nNewLockedQuantity where WarehouseId=nPublicwarehouseid  and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                    end;
                    end if;


                  end;
                  end if;


                end;
                end if;

---第一仓库
                if  nWRemainQuantity>0 and nFirstwarehouseid>0  then
                begin
                  --res:='第一仓库';
                  nPublicwarehouseid:=nFirstwarehouseid;
                  ---判断第一仓库是否为统购分销仓库
                  select IsCentralizedPurchase into bIsCentralizedPurchase from warehouse where id=nPublicwarehouseid;
                  if  bIsCentralizedPurchase=1 then
                  begin
                    nPublicwarehouseidG:=nFirstoutwarehouseid;
                    nOrderProcessMethod:=10;

                    select Code,Name,storagecompanyid,storagecompanytype into vWareHouseCodeG,vWareHouseNameG,iStoragecompanyidG,nStoragecompanytypeG from warehouse where id=nPublicwarehouseidG;
                    select code,name into vStoragecompanycodeG,vStoragecompanynameG from company where id=iStoragecompanyidG;
                    /*2017-10-11 hr add 增加入库品牌取值*/
                    begin
                      select b.partssalescategoryid,c.name into iSalesCategoryIdG,vSalesCategoryNameG from salesunitaffiwarehouse a left join salesunit b on a.salesunitid = b.id left join partssalescategory c on b.partssalescategoryid = c.id left join partssalescategory c on b.partssalescategoryid = c.id where a.warehouseid = nPublicwarehouseidG;
                    exception
                    when no_data_found then
                      begin
                        vAutoapprovecommentG:='统购分销仓库'||vWareHouseCodeG||'未维护销售组织';
                        raise FMyException;
                      end;
                    end;

                    select Code,Name,storagecompanyid,storagecompanytype into vWareHouseCodeP,vWareHouseNameP,iStoragecompanyidP,nStoragecompanytype from warehouse where id=nPublicwarehouseid;
                    select code,name into vStoragecompanycodeP,vStoragecompanynameP from company where id=iStoragecompanyidP;
                    /*2017-10-11 hr add 增加调拨出库发运品牌取值*/
                    begin
                      select b.partssalescategoryid,c.name into iSalesCategoryIdP,vSalesCategoryNameP from salesunitaffiwarehouse a left join salesunit b on a.salesunitid = b.id left join partssalescategory c on b.partssalescategoryid = c.id left join partssalescategory c on b.partssalescategoryid = c.id where a.warehouseid = nPublicwarehouseid;
                    exception
                    when no_data_found then
                      begin
                        vAutoapprovecommentG:='统购分销仓库'||vWareHouseCodeP||'未维护销售组织';
                        raise FMyException;
                      end;
                    end;

                    -- select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from PartsStock where
                    insert into tempPARTSSTOCK  select rownum,a.* from PARTSSTOCK a  where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1;
                    --insert into tempPARTSSTOCK  select rownum,a.* from PARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1
                    --and (quantity=(select max(quantity) from PARTSSTOCK where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId));

                    select sum(quantity) into nSumQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;

                    --select max(nvl(quantity,0)) into nMaxQuantityD from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                    --select id into nTPartsStockidD from tempPARTSSTOCK a  where quantity=nMaxQuantityD and rownum=1 and WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;


                    select max(nvl(quantity,0)) into nMaxQuantityF from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                    /*2017-8-4 hr modfiy 当库存不存在时，查不到统购分销仓库最大库存库区ID*/
                    begin
                    ---获取统购分销仓库最大库存库区ID
                      select id,WarehouseAreaId into nTPartsStockidF,nWarehouseAreaIdF from tempPARTSSTOCK a  where quantity=nMaxQuantityF and rownum=1 and WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                    exception
                      when no_data_found then
                        nTPartsStockidF := 0;
                    end;
                    /*2017-10-17 hr add 获取调拨出库仓库的库位*/
                    begin
                      select code into nWarehouseAreaCodeF from WarehouseArea a  where a.id = nWarehouseAreaIdF and a.areakind = 3;
                    exception
                      when no_data_found then
                        nWarehouseAreaCodeF := 0;
                    end;


                    select sum(LockedQuantity) into nSumLockedQuantity from Partslockedstock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;

                    select count(*) into iPS from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;

                    if (nSumQuantity-nvl(nSumLockedQuantity,0))< nWRemainQuantity1 and (nSumQuantity-nvl(nSumLockedQuantity,0)) > 0 then ---首选仓库部分满足
                    begin

                      /*2017-10-11 hr add 对出入库计划价进行校验
                      1）计划价必须存在，且>0
                      2）出入库仓库的计划价必须一致*/
                      begin
                        select PlannedPrice into nPlannedPriceG from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG;
                      exception
                      when no_data_found then
                        begin
                          vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameG||']没有计划价';
                          raise FMyException;
                        end;
                      end;
                      if nPlannedPriceG = 0 then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameG||']的计划价为0';
                        raise FMyException;
                      end;
                      end if;
                      begin
                        select PlannedPrice into nPlannedPriceP from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdP;
                      exception
                      when no_data_found then
                        begin
                          vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']没有计划价';
                          raise FMyException;
                        end;
                      end;
                      if nPlannedPriceP = 0 then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']的计划价为0';
                        raise FMyException;
                      end;
                      end if;
                      if nPlannedPriceP <> nPlannedPriceG then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']的计划价与品牌['||vSalesCategoryNameG||']的计划价不一致';
                        raise FMyException;
                      end;
                      end if;


                      if bInsertOutboundPlanFT = false then
                      begin
                        ---收货单位Id  ReceivingCompanyId,收货单位编号  ReceivingCompanyCode,收货单位名称  ReceivingCompanyName 填为  nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG


                        ---PartsTransferOrder配件调拨单
                        nPartsTransferOrderid := s_PartsTransferOrder.Nextval;
                        vPartsTransferOrderCode := regexp_replace(regexp_replace(vTransferOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateTransferOrderId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-8-7 hr modify 调拨单状态为 已审批
                          2017-10-12 hr modify 调拨单的调拨类型 应该是 3--急需调拨；发运方式设置为空
                          2017-10-13 hr add 修改人，审批人，修改时间，审批时间赋值*/
                        insert into PartsTransferOrder (id,code,originalwarehouseid,originalwarehousecode,originalwarehousename,storagecompanyid,storagecompanytype,destwarehouseid,destwarehousecode,destwarehousename,status,type,
                          shippingmethod,creatorid,creatorname,createtime,originalbillid,originalbillcode,totalamount,Modifierid,Modifiername,Modifytime,Approverid,Approvername,Approvetime)
                        values(nPartsTransferOrderid,vPartsTransferOrderCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,nStoragecompanytype,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,/*1*/2,/*1*/3,
                        null/*iShippingMethod*/,nCreatorID,vCreatorName,sysdate,PartCheckId,vCode,0,nCreatorID,vCreatorName,sysdate,nCreatorID,vCreatorName,sysdate);
                        --- totalamount,totalplanamount需要计算。

                        ----PartsOutboundPlan配件出库计划 (首选仓库)
                        nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                        vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值，出库计划单的源单据 应是 调拨单
                          2017-10-12 hr modify 调拨出库生成的出库计划单的出库类型 应是 调拨出库；对方单位赋值有问题
                          2017-10-16 hr modify 发运方式设置为空，客户账户Id默认null*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface )
                        values(nPartsoutboundplanid,vPartsOutboundPlanCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdP/*iSalesCategoryId*/,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPartsTransferOrderid/*PartCheckId*/,
                          vPartsTransferOrderCode/*vCode*/,4/*1*/,null/*iShippingMethod*/,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,3,nCreatorID,vCreatorName,sysdate,0);

                        ---新建配件出库单
                        --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                        vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        nPartsOutboundBillId := s_PartsOutboundBill.Nextval;
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-13 hr modify 调拨出库生成的出库单的出库类型 应是 调拨出库 配件调拨；备注是'统购分销出库'；结算状态是 待结算；对方单位赋值有问题
                          2017-10-16 hr modify 发运方式设置为空，客户账户Id默认null*/
                        insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                          partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,receivingcompanyid,receivingcompanycode, receivingcompanyname,
                          receivingwarehouseid,receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                          creatorid,creatorname,createtime,Remark)
                        Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdP/*iSalesCategoryId*/,
                          iPartssalesordertypeid,vPartssalesordertypename,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,
                          nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,4/*1*/,null/*iShippingMethod*/,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,2/*1*/,
                          nCreatorID,vCreatorName,sysdate,'统购分销出库');


                        ---PartsShippingOrder配件发运单  /*入库计划单的源单据是 发运单，所以调整发运单生成的位置，放到入库计划单生成前*/
                        nPartsShippingOrderid := s_PartsShippingOrder.Nextval;
                        /*2017-10-12 hr modify 发运单应是4位流水号*/
                        vPartsShippingOrderCode := regexp_replace(regexp_replace(vShippingOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateShippingOrderId,TRUNC(sysdate,'DD'),vBranchCode)),4/*6*/,'0'));
                        /*2017-9-6 hr modify 发运单的发货单位与结算单位赋值有问题，状态不应是新建，应该是 回执确认
                          2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 配件发运单增加品牌字段赋值
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-12 hr add 发运单备注默认'统购分销发运'
                          2017-10-13 hr add 发运方式默认 8--客户自行运输；发运日期 不赋值；收货单位赋值有问题；预计到货日期 不赋值*/
                        insert into PartsShippingOrder(id,code,type,branchid,shippingcompanyid,shippingcompanycode,shippingcompanyname,settlementcompanyid,settlementcompanycode,settlementcompanyname,receivingcompanyid,receivingcompanycode,invoicereceivesalecateid,invoicereceivesalecatename,receivingcompanyname, warehouseid,warehousecode,warehousename,
                          receivingwarehouseid,receivingwarehousecode,receivingwarehousename,originalrequirementbillid,originalrequirementbillcode,originalrequirementbilltype,shippingdate,status,creatorid,creatorname,createtime,expectedplacedate,PartsSalesCategoryId,Remark,Shippingmethod)
                        values(nPartsShippingOrderid,vPartsShippingOrderCode,4,ibranchid,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,*/iStoragecompanyidP,vStoragecompanycodeP,/*nPublicwarehouseidG,vWareHouseCodeG ,*/iPartssalesordertypeid,vPartsSalesOrderTypeName,vStoragecompanynameP/*vWareHouseNameP*/,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,
                        nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,PartCheckId,vCode,1/*nPartssalescategoryid*/,null/*sysdate*/,/*1*/3,nCreatorID,vCreatorName,sysdate,null/*sysdate*/,iSalesCategoryIdP/*iSalesCategoryId*/,'统购分销发运',8);


                        ---PartsInboundPlan配件入库计划
                        nPartsInboundPlanid := s_PartsInboundPlan.Nextval;
                        vPartsInboundPlanCode := regexp_replace(regexp_replace(vInboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateInboundPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-9 hr modify 入库计划单 对方单位 赋值有问题，不应是仓库，品牌赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值，入库计划单的源单据 应是 发运单
                          2017-10-13 hr add 入库计划单备注默认'统购分销发运'；抵达日期 不赋值，客户账户Id不赋值*/
                        insert into PartsInboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,
                          sourceid,sourcecode,inboundtype,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,arrivaldate,creatorid,creatorname,createtime,IfWmsInterface,Remark)
                        values(nPartsInboundPlanid,vPartsInboundPlanCode,nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytypeG,ibranchid,vbranchcode,vbranchname,iSalesCategoryIdG/*nPartssalescategoryid*//*ipartssalesordertypeid*/,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,/*nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,*/
                        nPartsShippingOrderid/*PartCheckId*/,vPartsShippingOrderCode/*vCode*/,3,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,2,null/*sysdate*/,nCreatorID,vCreatorName,sysdate,0,'统购分销发运');
                        ---inboundtype:3 配件调拨

                        ---PartsInboundCheckBill配件入库检验单
                        nPartsInboundCheckBillid := s_PartsInboundCheckBill.Nextval;
                        vPartsInboundCheckBillCode := regexp_replace(regexp_replace(vInboundCheckBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateInboundCheckBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-10 hr modify 入库检验单 仓库 仓储企业 品牌 对方单位 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-16 hr modify 客户账户Id不赋值，状态是 3-上架完成*/
                        insert into PartsInboundCheckBill(id,code,partsinboundplanid,warehouseid, warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,partssalescategoryid,branchid,branchcode,branchname,
                          counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,inboundtype,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,settlementstatus,creatorid,creatorname,createtime)
                        values (nPartsInboundCheckBillid,vPartsInboundCheckBillCode,nPartsInboundPlanid,nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytypeG,iSalesCategoryIdG/*nPartssalescategoryid*/,ibranchid,vbranchcode,vbranchname,
                        iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,/*iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,*/3,null/*ncustomeraccountid*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,3/*1*/,2,nCreatorID,vCreatorName,sysdate);
                        ---inboundtype:3 配件调拨. settlementstatus=2 待结算

                        ---PartsShiftOrder配件移库单
                        nPartsShiftOrderid := s_PartsShiftOrder.Nextval;
                        nPartsShiftOrderidF := nPartsShiftOrderid;
                        vPartsShiftOrderCode := regexp_replace(regexp_replace(vShiftOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateShiftOrderId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        insert into  PartsShiftOrder(id,code,branchid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,status,type,creatorid,creatorname,createtime)
                        values(nPartsShiftOrderid,vPartsShiftOrderCode,ibranchid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,nStoragecompanytype,1,2,nCreatorID,vCreatorName,sysdate);


                        ---PartsShippingOrderRef配件发运单关联单
                        insert into PartsShippingOrderRef(Id,PartsShippingOrderId,PartsOutboundBillId)
                        values(s_PartsShippingOrderRef.Nextval,nPartsShippingOrderid,nPartsOutboundBillId);

                        ----仓库PartsOutboundPlan配件出库计划 （第一选出库仓库）
                        nPartsoutboundplanidG := s_PartsOutboundPlan.Nextval;
                        vPartsOutboundPlanCodeG := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 出库计划单收货仓库id，编号，名称 需要赋值
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                          2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface,Remark,Companyaddressid)
                        values(nPartsoutboundplanidG,vPartsOutboundPlanCodeG,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdG/*iSalesCategoryId*/,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                          vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                          sysdate,0,vRemark,vCompanyAddressId);

                        /*values(nPartsoutboundplanid,vPartsOutboundPlanCode,iWarehouseidP,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,nPartssalescategoryid,
                        vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                        vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);*/
                        nPartsoutboundplanidF := nPartsoutboundplanid;
                        nPartsOutboundBillIdF := nPartsOutboundBillId;
                        nPartsInboundPlanidF := nPartsInboundPlanid;
                        nPartsInboundCheckBillidF := nPartsInboundCheckBillid;
                        nPartsTransferOrderidF := nPartsTransferOrderid;
                        nPartsoutboundplanidGF := nPartsoutboundplanidG;
                        nPartsshippingorderidF := nPartsshippingorderid;
                        nPartsshiftorderidF := nPartsshiftorderid;
                        bInsertOutboundPlanFT := true;
                      end;
                      end if;

                      --第一仓库此次满足数量和金额
                      if nMaxQuantityF <= (nSumQuantity-nvl(nSumLockedQuantity,0)) then
                      begin
                         mCurrentFulfilledAmountF:=nMaxQuantityF*nvl(nOrderPrice,0);
                         nLockedQuantityF := nMaxQuantityF;
                      end;
                      else
                      begin
                         mCurrentFulfilledAmountF:=(nSumQuantity-nvl(nSumLockedQuantity,0))*nvl(nOrderPrice,0);
                         nLockedQuantityF := (nSumQuantity-nvl(nSumLockedQuantity,0));
                      end;
                      end if;

                      ---PartsTransferOrderDetail配件调拨单清单
                      ---  remark,
                      insert into tempPartsTransferOrderDetail(id,partstransferorderid,sparepartid,sparepartcode,sparepartname,plannedamount,confirmedamount,price,plannprice)
                      /*2017-9-3 hr modify nPartsTransferOrderid会取最近一个新配件调拨单的ID
                        2017-10-10 hr modify 调拨单清单的计划价 取 配件计划价，品牌取调拨入库仓库的品牌*/
                      values(s_PartsTransferOrderDetail.Nextval,nPartsTransferOrderidF/*nPartsTransferOrderid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityF,nLockedQuantityF,nvl(nOrderPrice,0),/*nvl(nOrderPrice,0)*/
                      (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG/*iSalesCategoryId*/));

                      ---PartsOutboundPlanDetail配件出库计划清单 首选仓库
                      insert into tempPartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price)
                      /*2017-9-3 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                      values(s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidF/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityF,nLockedQuantityF,nvl(nOrderPrice,0));

                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,仓库，这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,nLockedQuantityF,nvl(nOrderPrice,0),2,nOrderProcessMethod,sysdate,nCreatorId,vCreatorName);

                      ---PartsInboundPlanDetail配件入库计划清单
                      insert into tempPartsInboundPlanDetail(id,partsinboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,inspectedquantity,price)
                      /*2017-9-3 hr modify nPartsInboundPlanid会取最近一个新配件入库计划单的ID*/
                      values(s_PartsInboundPlanDetail.Nextval,nPartsInboundPlanidF/*nPartsinboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityF,nLockedQuantityF,nvl(nOrderPrice,0));
                      /*2017-8-4 hr add*/
                      --统购仓库配件出库单清单（tempPartsOutboundBillDetail）记录
                      /*2017-9-3 hr modify nPartsOutboundBillId会取最近一个新配件出库单的ID
                        2017-10-11 hr modify 品牌取调拨出库仓库的品牌
                        2017-10-17 hr modify 出库清单的库位ID与编号赋值有问题*/
                      insert into tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillIdF/*nPartsOutboundBillId*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityF,nWarehouseAreaIdF,nWarehouseAreaCodeF,/*nPublicwarehouseid,vWareHouseCodeP,*/nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdP/*iSalesCategoryId*/));

                      /*
                      --第一仓库此次满足金额
                      mCurrentFulfilledAmountP:=(nSumQuantity-nvl(nSumLockedQuantity,0))*nvl(nOrderPrice,0);

                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        --select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        ---查询不到数据会报错
                        select count(*) into nTest from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        if nvl(nTest,0)>0 then
                           select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --else
                          -- nLockedQuantity:=0;
                        end if;
                        if nQuantity-nvl(nLockedQuantity,0)>0 then --首选库区可用数量
                        begin

                          nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --剩下没满足数量
                          ---nWRemainQuantity1:=nWRemainQuantity;

                          --统购仓库配件出库单清单（tempPartsOutboundBillDetail）记录
                          insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityF,nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));

                        end;
                        end if;

                      end loop;
                      */

                      ---根据第一出库仓库ID和配件ID，检验区
                      ---select WarehouseAreaId,Quantity into iWarehouseAreaIdC,iQuantityC from PartsStock where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaCategoryId=3 and StorageCompanyId=iStorageCompanyId and BranchId=iBranchId and rownum = 1;
                      select a.id,a.Code into iWarehouseAreaIdC,vCodeC from WarehouseArea a, WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.AreaCategoryId=b.id and b.category=3 and rownum=1 and a.areakind=3;
                      if iWarehouseAreaIdC>0 then
                      begin

                        ---根据首选出库仓库ID和配件ID，保管区
                        ---select WarehouseAreaId,Quantity into iWarehouseAreaIdS,nQuantityS from PartsStock a where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and StorageCompanyId=nStorageCompanyId and BranchId=iBranchId and PartId=iSparePartId and rownum = 1;
                        select count(*) into iCount from PartsStock a,WarehouseAreaCategory b where  a.WarehouseId=nPublicwarehouseidG and a.WarehouseAreaCategoryId=b.id and b.category=1 and a.StorageCompanyId=iStoragecompanyidG and a.BranchId=iBranchId and a.PartId=iSparePartId;
                        --if iWarehouseAreaIdS>0 then
                        if iCount>0 then
                        begin
                          ---PartsShiftOrderDetail配件移库单清单
                          ---,batchnumber,remark
                          select a.WarehouseAreaId,a.Quantity,b.id,a.id into iWarehouseAreaIdS,nQuantityS,nWarehouseareacategoryidS,nPartsStockidS from PartsStock a,WarehouseAreaCategory b where a.partid=iSparePartId and a.WarehouseId=nPublicwarehouseidG and a.WarehouseAreaCategoryId=b.id and b.category=1 and a.StorageCompanyId=iStoragecompanyidG and a.BranchId=iBranchId and a.PartId=iSparePartId and rownum = 1;
                          insert into tempPartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
                          /*2017-9-3 hr modify nPartsshiftorderid会取最近一个新配件移库单的ID*/
                          values(s_PartsShiftOrderDetail.Nextval,nPartsshiftorderidF/*nPartsshiftorderid*/,iSparePartId,vSparepartcode,vSparepartname,iWarehouseAreaIdC,vCodeC,3,
                          iWarehouseAreaIdS,(select code from WarehouseArea where WarehouseId=nPublicwarehouseidG and id=iWarehouseAreaIdC),1,nLockedQuantityF);

                          --update PartsStock set Quantity=Quantity+nLockedQuantityD where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaId=iWarehouseAreaIdS and WarehouseAreaCategoryId=（select id from WarehouseAreaCategory where category=3 ） and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          update PartsStock set Quantity=Quantity+nLockedQuantityF where id=nPartsStockidS;

                          ---统购分销仓库减库存
                          update PartsStock set Quantity=Quantity-nLockedQuantityF where id=nTPartsStockidF;
                          select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          ---锁定库存
                          if iPartsLockedStock>0 then
                            /*2017-8-4 hr modify 第一仓库锁定库存的变量应为nLockedQuantityF，而不是nLockedQuantityS*/
                            update PartsLockedStock set LockedQuantity=LockedQuantity+/*nLockedQuantityS*/nLockedQuantityF,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          else
                            insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                            values(s_PartsLockedStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,iBranchId,iSparePartId,/*nLockedQuantityS*/nLockedQuantityF,nCreatorId,vCreatorName,sysdate);
                          end if;
                          ---锁定库存
                          --update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityF where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                        end;
                       else
                        begin
                          ---select TopLevelWarehouseAreaId,code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea where WarehouseId=nPublicwarehouseidG and AreaCategoryId=1 and TopLevelWarehouseAreaId not in(
                          ---select a.\*TopLevelWarehouseAreaId*\,a.code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and a.\*TopLevelWarehouseAreaId*\ not in(
                          ---select WarehouseAreaId from PartsStock where  WarehouseId=nPublicwarehouseidG ) and  rownum = 1;

                          select a.id,a.code,b.id into iWarehouseAreaIdS,vWarehouseAreaCodeS,nWarehouseareacategoryidS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and rownum = 1;
                          --select a.TopLevelWarehouseAreaId,a.code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nDefaultoutwarehouseid and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and a.TopLevelWarehouseAreaId not in(
                          --select WarehouseAreaId from PartsStock where  WarehouseId=nDefaultoutwarehouseid ) and  rownum = 1;
                          if iWarehouseAreaIdS>0 then
                          begin
                            ---PartsShiftOrderDetail配件移库单清单
                            ---,batchnumber,remark
                            insert into tempPartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
                            /*2017-9-3 hr modify nPartsshiftorderid会取最近一个新配件移库单的ID*/
                            values(s_PartsShiftOrderDetail.Nextval,nPartsshiftorderidF/*nPartsshiftorderid*/,iSparePartId,vSparepartcode,vSparepartname,iWarehouseAreaIdC,vCodeC,3,
                              iWarehouseAreaIdS,(select code from WarehouseArea where WarehouseId=nPublicwarehouseidG and id=iWarehouseAreaIdC),nWarehouseareacategoryidS,nLockedQuantityF);

                            insert into PartsStock(id,warehouseid,storagecompanyid,storagecompanytype,branchid,warehouseareaid,warehouseareacategoryid,partid,quantity,creatorid,creatorname,createtime)
                            values(s_PartsStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,nstoragecompanytype,iBranchId,iWarehouseAreaIdS,nWarehouseareacategoryidS,iSparePartId,nLockedQuantityF,nCreatorId,vCreatorName,sysdate);
                            ---update PartsStock set Quantity:=Quantity+(nSumQuantity-nvl(nSumLockedQuantity,0)) where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaId=iWarehouseAreaIdS and WarehouseAreaCategoryId=3 and StorageCompanyId=iStorageCompanyId and BranchId=iBranchId;

                            ---统购分销仓库减库存
                            update PartsStock set Quantity=Quantity-nLockedQuantityF where id=nTPartsStockidF;
                            select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            if iPartsLockedStock>0 then
                              ---锁定库存
                              update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityF,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            else
                              insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                              values(s_PartsLockedStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,iBranchId,iSparePartId,nLockedQuantityF,nCreatorId,vCreatorName,sysdate);
                            end if;
                            ---锁定库存
                            --update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityF where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                          end;

                          end if;
                        end;
                        end if;

                      end;
                      end if;

                      ---PartsInboundCheckBillDetail配件入库检验单清单
                      /*2017-9-3 hr modify nPartsInboundCheckBillid会取最近一个新配件入库检验单的ID
                        2017-10-10 hr modify 入库检验单清单的成本价 取 配件计划价，品牌取调拨入库仓库的品牌
                        2017-10-17 hr modify 入库检验单清单的库位ID与编号赋值有问题，不应是 检验区的库位*/
                      insert into tPartsInboundCheckBillDetail(id,partsinboundcheckbillid,sparepartid,sparepartcode,sparepartname,warehouseareaid,warehouseareacode,inspectedquantity,settlementprice,costprice)
                      values (s_PartsInboundCheckBillDetail.Nextval,nPartsInboundCheckBillidF/*nPartsinboundcheckbillid*/,iSparePartId,vSparepartcode,vSparepartname,/*iWarehouseAreaIdC,vCodeC*/
                      iWarehouseAreaIdS,(select code from WarehouseArea where id =iWarehouseAreaIdS and areakind=3),nLockedQuantityF,nvl(nOrderPrice,0),/*nvl(nOrderPrice,0)*/
                      (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG/*iSalesCategoryId*/));

                      ---PartsShippingOrderDetail配件发运单清单
                      ---differenceclassification
                      insert into tempPartsShippingOrderDetail(id,partsshippingorderid,sparepartid,sparepartcode,sparepartname,shippingamount,confirmedamount,settlementprice)
                      /*2017-9-3 hr modify nPartsshippingorderidD会取最近一个新配件发运单的ID*/
                      values(s_PartsShippingOrderDetail.Nextval,nPartsshippingorderidF/*nPartsshippingorderid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityF,nLockedQuantityF,nvl(nOrderPrice,0));

                      ---PartsOutboundPlanDetail配件出库计划清单
                      insert into tempPartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,/*outboundfulfillment,*/price)
                      /*2017-9-3 hr modify nPartsoutboundplanidG会取最近一个新出库计划单的ID*/
                      values(s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidGF/*nPartsoutboundplanidG*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityF,/*nLockedQuantityF,*/nvl(nOrderPrice,0));


                      ---nWRemainQuantity:= nWRemainQuantity - (nSumQuantity-nvl(nSumLockedQuantity,0)); --剩下没满足数
                      --nWRemainQuantity1:=nWRemainQuantity-nLockedQuantityF;
                      /*2017-8-7 hr modify 剩余未满足量未赋值*/
                      nWRemainQuantity := nWRemainQuantity - nLockedQuantityF;--剩余未满足量
                      nWRemainQuantity1 := nWRemainQuantity;

                    end;
                    elsif (nSumQuantity-nvl(nSumLockedQuantity,0)) >= nWRemainQuantity then
                      ---第一仓库完全满足
                    begin

                      /*2017-10-11 hr add 对出入库计划价进行校验
                      1）计划价必须存在，且>0
                      2）出入库仓库的计划价必须一致*/
                      begin
                        select PlannedPrice into nPlannedPriceG from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG;
                      exception
                      when no_data_found then
                        begin
                          vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameG||']没有计划价';
                          raise FMyException;
                        end;
                      end;
                      if nPlannedPriceG = 0 then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameG||']的计划价为0';
                        raise FMyException;
                      end;
                      end if;
                      begin
                        select PlannedPrice into nPlannedPriceP from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdP;
                      exception
                      when no_data_found then
                        begin
                          vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']没有计划价';
                          raise FMyException;
                        end;
                      end;
                      if nPlannedPriceP = 0 then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']的计划价为0';
                        raise FMyException;
                      end;
                      end if;
                      if nPlannedPriceP <> nPlannedPriceG then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']的计划价与品牌['||vSalesCategoryNameG||']的计划价不一致';
                        raise FMyException;
                      end;
                      end if;

                      if bInsertOutboundPlanFT = false then
                      begin
                        ---收货单位Id  ReceivingCompanyId,收货单位编号  ReceivingCompanyCode,收货单位名称  ReceivingCompanyName 填为  nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG


                        ---PartsTransferOrder配件调拨单
                        nPartsTransferOrderid := s_PartsTransferOrder.Nextval;
                        vPartsTransferOrderCode := regexp_replace(regexp_replace(vTransferOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateTransferOrderId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-8-7 hr modify 调拨单状态为 已审批
                          2017-10-12 hr modify 调拨单的调拨类型 应该是 3--急需调拨；发运方式设置为空
                          2017-10-13 hr add 修改人，审批人，修改时间，审批时间赋值*/
                        insert into PartsTransferOrder (id,code,originalwarehouseid,originalwarehousecode,originalwarehousename,storagecompanyid,storagecompanytype,destwarehouseid,destwarehousecode,destwarehousename,status,type,
                          shippingmethod,creatorid,creatorname,createtime,originalbillid,originalbillcode,totalamount,Modifierid,Modifiername,Modifytime,Approverid,Approvername,Approvetime)
                        values(nPartsTransferOrderid,vPartsTransferOrderCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,nStoragecompanytype,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,/*1*/2,/*1*/3,
                        null/*iShippingMethod*/,nCreatorID,vCreatorName,sysdate,PartCheckId,vCode,0,nCreatorID,vCreatorName,sysdate,nCreatorID,vCreatorName,sysdate);
                        --- totalamount,totalplanamount需要计算。

                        ----PartsOutboundPlan配件出库计划 (第一仓库)
                        nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                        vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值，出库计划单的源单据 应是 调拨单
                          2017-10-12 hr modify 调拨出库生成的出库计划单的出库类型 应是 调拨出库；对方单位赋值有问题
                          2017-10-16 hr modify 发运方式设置为空，客户账户Id默认null*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface )
                        values(nPartsoutboundplanid,vPartsOutboundPlanCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdP/*iSalesCategoryId*/,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPartsTransferOrderid/*PartCheckId*/,
                          vPartsTransferOrderCode/*vCode*/,4/*1*/,null/*iShippingMethod*/,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,3,nCreatorID,vCreatorName,sysdate,0);

                        ---新建配件出库单
                        --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                        vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        nPartsOutboundBillId := s_PartsOutboundBill.Nextval;
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-13 hr modify 调拨出库生成的出库单的出库类型 应是 调拨出库 配件调拨；备注是'统购分销出库'；结算状态是 待结算；对方单位赋值有问题
                          2017-10-16 hr modify 发运方式设置为空，客户账户Id默认null*/
                        insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                          partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,receivingcompanyid,receivingcompanycode, receivingcompanyname,
                          receivingwarehouseid,receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                          creatorid,creatorname,createtime,Remark)
                        Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdP/*iSalesCategoryId*/,
                          iPartssalesordertypeid,vPartssalesordertypename,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,
                          nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,4/*1*/,null/*iShippingMethod*/,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,2/*1*/,
                          nCreatorID,vCreatorName,sysdate,'统购分销出库');


                        ---PartsShippingOrder配件发运单  /*入库计划单的源单据是 发运单，所以调整发运单生成的位置，放到入库计划单生成前*/
                        nPartsShippingOrderid := s_PartsShippingOrder.Nextval;
                        /*2017-10-12 hr modify 发运单应是4位流水号*/
                        vPartsShippingOrderCode := regexp_replace(regexp_replace(vShippingOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateShippingOrderId,TRUNC(sysdate,'DD'),vBranchCode)),4/*6*/,'0'));
                        /*2017-9-6 hr modify 发运单的发货单位与结算单位赋值有问题，状态不应是新建，应该是 回执确认
                          2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 配件发运单增加品牌字段赋值
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-12 hr add 发运单备注默认'统购分销发运'
                          2017-10-13 hr add 发运方式默认 8--客户自行运输；发运日期 不赋值；收货单位赋值有问题；预计到货日期 不赋值*/
                        insert into PartsShippingOrder(id,code,type,branchid,shippingcompanyid,shippingcompanycode,shippingcompanyname,settlementcompanyid,settlementcompanycode,settlementcompanyname,receivingcompanyid,receivingcompanycode,invoicereceivesalecateid,invoicereceivesalecatename,receivingcompanyname, warehouseid,warehousecode,warehousename,
                          receivingwarehouseid,receivingwarehousecode,receivingwarehousename,originalrequirementbillid,originalrequirementbillcode,originalrequirementbilltype,shippingdate,status,creatorid,creatorname,createtime,expectedplacedate,PartsSalesCategoryId,Remark,Shippingmethod)
                        values(nPartsShippingOrderid,vPartsShippingOrderCode,4,ibranchid,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,*/iStoragecompanyidP,vStoragecompanycodeP,/*nPublicwarehouseidG,vWareHouseCodeG ,*/iPartssalesordertypeid,vPartsSalesOrderTypeName,vStoragecompanynameP/*vWareHouseNameP*/,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,
                        nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,PartCheckId,vCode,1/*nPartssalescategoryid*/,null/*sysdate*/,/*1*/3,nCreatorID,vCreatorName,sysdate,null/*sysdate*/,iSalesCategoryIdP/*iSalesCategoryId*/,'统购分销发运',8);


                        ---PartsInboundPlan配件入库计划
                        nPartsInboundPlanid := s_PartsInboundPlan.Nextval;
                        vPartsInboundPlanCode := regexp_replace(regexp_replace(vInboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateInboundPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-9 hr modify 入库计划单 对方单位 赋值有问题，不应是仓库，品牌赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值，入库计划单的源单据 应是 发运单
                          2017-10-13 hr add 入库计划单备注默认'统购分销发运'；抵达日期 不赋值，客户账户Id不赋值*/
                        insert into PartsInboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,
                          sourceid,sourcecode,inboundtype,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,arrivaldate,creatorid,creatorname,createtime,IfWmsInterface,Remark)
                        values(nPartsInboundPlanid,vPartsInboundPlanCode,nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytypeG,ibranchid,vbranchcode,vbranchname,iSalesCategoryIdG/*nPartssalescategoryid*//*ipartssalesordertypeid*/,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,/*nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,*/
                        nPartsShippingOrderid/*PartCheckId*/,vPartsShippingOrderCode/*vCode*/,3,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,2,null/*sysdate*/,nCreatorID,vCreatorName,sysdate,0,'统购分销发运');
                        ---inboundtype:3 配件调拨

                        ---PartsInboundCheckBill配件入库检验单
                        nPartsInboundCheckBillid := s_PartsInboundCheckBill.Nextval;
                        vPartsInboundCheckBillCode := regexp_replace(regexp_replace(vInboundCheckBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateInboundCheckBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-10 hr modify 入库检验单 仓库 仓储企业 品牌 对方单位 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-16 hr modify 客户账户Id不赋值，状态是 3-上架完成*/
                        insert into PartsInboundCheckBill(id,code,partsinboundplanid,warehouseid, warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,partssalescategoryid,branchid,branchcode,branchname,
                          counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,inboundtype,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,settlementstatus,creatorid,creatorname,createtime)
                        values (nPartsInboundCheckBillid,vPartsInboundCheckBillCode,nPartsInboundPlanid,nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytypeG,iSalesCategoryIdG/*nPartssalescategoryid*/,ibranchid,vbranchcode,vbranchname,
                        iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,/*iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,*/3,null/*ncustomeraccountid*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,3/*1*/,2,nCreatorID,vCreatorName,sysdate);
                        ---inboundtype:3 配件调拨. settlementstatus=2 待结算

                        ---PartsShiftOrder配件移库单
                        nPartsShiftOrderid := s_PartsShiftOrder.Nextval;
                        nPartsShiftOrderidF := nPartsShiftOrderid;
                        vPartsShiftOrderCode := regexp_replace(regexp_replace(vShiftOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateShiftOrderId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        insert into  PartsShiftOrder(id,code,branchid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,status,type,creatorid,creatorname,createtime)
                        values(nPartsShiftOrderid,vPartsShiftOrderCode,ibranchid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,nStoragecompanytype,1,2,nCreatorID,vCreatorName,sysdate);


                        ---PartsShippingOrderRef配件发运单关联单
                        insert into PartsShippingOrderRef(Id,PartsShippingOrderId,PartsOutboundBillId)
                        values(s_PartsShippingOrderRef.Nextval,nPartsShippingOrderid,nPartsOutboundBillId);

                        ----仓库PartsOutboundPlan配件出库计划 （第一出库仓库）
                        nPartsoutboundplanidG := s_PartsOutboundPlan.Nextval;
                        vPartsOutboundPlanCodeG := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 出库计划单收货仓库id，编号，名称 需要赋值
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                          2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface,Remark,Companyaddressid)
                        values(nPartsoutboundplanidG,vPartsOutboundPlanCodeG,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdG/*iSalesCategoryId*/,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                          vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                          sysdate,0,vRemark,vCompanyAddressId);

                        /*values(nPartsoutboundplanid,vPartsOutboundPlanCode,iWarehouseidP,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,nPartssalescategoryid,
                        vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                        vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);*/
                        nPartsTransferOrderidF := nPartsTransferOrderid;
                        nPartsoutboundplanidF := nPartsoutboundplanid;
                        nPartsOutboundBillIdF := nPartsOutboundBillId;
                        nPartsInboundPlanidF := nPartsInboundPlanid;
                        nPartsInboundCheckBillidF := nPartsInboundCheckBillid;
                        nPartsoutboundplanidGF := nPartsoutboundplanidG;
                        nPartsshippingorderidF := nPartsshippingorderid;
                        nPartsshiftorderidF := nPartsshiftorderid;
                        bInsertOutboundPlanFT := true;
                      end;
                      end if;

                      --首选仓库此次满足数量和金额
                      ---(nSumQuantity-nvl(nSumLockedQuantity,0)) >= nWRemainQuantity
                      if nMaxQuantityF <= nWRemainQuantity then
                      begin
                         mCurrentFulfilledAmountF:=nMaxQuantityF*nvl(nOrderPrice,0);
                         nLockedQuantityF := nMaxQuantityF;
                      end;
                      else
                      begin
                         mCurrentFulfilledAmountF:=nWRemainQuantity*nvl(nOrderPrice,0);
                         nLockedQuantityF := nWRemainQuantity;
                      end;
                      end if;

                      ---PartsTransferOrderDetail配件调拨单清单
                      ---  remark,
                      insert into tempPartsTransferOrderDetail(id,partstransferorderid,sparepartid,sparepartcode,sparepartname,plannedamount,confirmedamount,price,plannprice)
                      /*2017-9-3 hr modify nPartsTransferOrderid会取最近一个新配件调拨单的ID
                        2017-10-10 hr modify 调拨单清单的计划价 取 配件计划价，品牌取调拨入库仓库的品牌*/
                      values(s_PartsTransferOrderDetail.Nextval,nPartsTransferOrderidF/*nPartsTransferOrderid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityF,nLockedQuantityF,nvl(nOrderPrice,0),/*nvl(nOrderPrice,0)*/
                      (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG/*iSalesCategoryId*/));

                      ---PartsOutboundPlanDetail配件出库计划清单 第一仓库
                      insert into tempPartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price)
                      /*2017-9-3 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                      values(s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidF/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityF,nLockedQuantityF,nvl(nOrderPrice,0));


                      ---PartsInboundPlanDetail配件入库计划清单
                      insert into tempPartsInboundPlanDetail(id,partsinboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,inspectedquantity,price)
                      /*2017-9-3 hr modify nPartsInboundPlanid会取最近一个新配件入库计划单的ID*/
                      values(s_PartsInboundPlanDetail.Nextval,nPartsInboundPlanidF/*nPartsinboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityF,nLockedQuantityF,nvl(nOrderPrice,0));

                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,匹配仓库，这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,nLockedQuantityF,nvl(nOrderPrice,0),1,nOrderProcessMethod,sysdate,nCreatorId,vCreatorName);


                     /*
                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,匹配仓库，这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,nWRemainQuantity,nvl(nOrderPrice,0),2,nOrderProcessMethod,sysdate,nCreatorId,vCreatorName);


                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,(nSumQuantity-nvl(nSumLockedQuantity,0)),nvl(nOrderPrice,0),2,2,sysdate,nCreatorId,vCreatorName);
                      */

                      /*2017-8-4 hr add*/
                      --统购仓库配件出库单清单（tempPartsOutboundBillDetail）记录
                      /*2017-9-3 hr modify nPartsOutboundBillId会取最近一个新配件出库单的ID
                        2017-10-11 hr modify 品牌取调拨出库仓库的品牌
                        2017-10-17 hr modify 出库清单的库位ID与编号赋值有问题*/
                      insert into tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillIdF/*nPartsOutboundBillId*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityF,nWarehouseAreaIdF,nWarehouseAreaCodeF,/*nPublicwarehouseid,vWareHouseCodeP,*/nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdP/*iSalesCategoryId*/));

                      /*
                      --第一仓库此次满足金额
                      mCurrentFulfilledAmountP:=nWRemainQuantity*nvl(nOrderPrice,0);

                      ----res:=mCurrentFulfilledAmountP;
                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        if nQuantity-nvl(nLockedQuantity,0)>0 then --此库区可用数量
                        begin

                          if nQuantity-nvl(nLockedQuantity,0)>= nWRemainQuantity then
                          begin

                            insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                            values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,nWRemainQuantity,nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));
                            nWRemainQuantity:= 0;
                            exit;
                          end;
                          else
                          begin
                            --统购仓库配件出库单清单（tempPartsOutboundBillDetail）记录
                            insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                            values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,(nQuantity-nvl(nLockedQuantity,0)),nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                              (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));

                              nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --剩下没满足数量
                          end;
                          ---nWRemainQuantity1:=nWRemainQuantity;
                          end if;
                        end;
                        end if;

                      end loop;
                      */

                      ---根据第一出库仓库ID和配件ID，检验区
                      ---select WarehouseAreaId,Quantity into iWarehouseAreaIdC,iQuantityC from PartsStock where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaCategoryId=3 and StorageCompanyId=iStorageCompanyId and BranchId=iBranchId and rownum = 1;
                      select a.id,a.Code into iWarehouseAreaIdC,vCodeC from WarehouseArea a, WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.AreaCategoryId=b.id and b.category=3 and rownum=1 and a.areakind=3;
                      if iWarehouseAreaIdC>0 then
                      begin

                        ---根据首选出库仓库ID和配件ID，保管区
                        ---select WarehouseAreaId,Quantity into iWarehouseAreaIdS,nQuantityS from PartsStock a where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and StorageCompanyId=nStorageCompanyId and BranchId=iBranchId and PartId=iSparePartId and rownum = 1;
                        select count(*) into iCount from PartsStock a,WarehouseAreaCategory b where  a.WarehouseId=nPublicwarehouseidG and a.WarehouseAreaCategoryId=b.id and b.category=1 and a.StorageCompanyId=iStoragecompanyidG and a.BranchId=iBranchId and a.PartId=iSparePartId;
                        --if iWarehouseAreaIdS>0 then
                        if iCount>0 then
                        begin
                          ---PartsShiftOrderDetail配件移库单清单
                          ---,batchnumber,remark
                          select a.WarehouseAreaId,a.Quantity,b.id,a.id into iWarehouseAreaIdS,nQuantityS,nWarehouseareacategoryidS,nPartsStockidS from PartsStock a,WarehouseAreaCategory b where a.partid=iSparePartId and a.WarehouseId=nPublicwarehouseidG and a.WarehouseAreaCategoryId=b.id and b.category=1 and a.StorageCompanyId=iStoragecompanyidG and a.BranchId=iBranchId and a.PartId=iSparePartId and rownum = 1;
                          insert into tempPartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
                          /*2017-9-3 hr modify nPartsshiftorderid会取最近一个新配件移库单的ID*/
                          values(s_PartsShiftOrderDetail.Nextval,nPartsshiftorderidF/*nPartsshiftorderid*/,iSparePartId,vSparepartcode,vSparepartname,iWarehouseAreaIdC,vCodeC,3,
                          iWarehouseAreaIdS,(select code from WarehouseArea where WarehouseId=nPublicwarehouseidG and id=iWarehouseAreaIdC),1,nLockedQuantityF);

                          --update PartsStock set Quantity=Quantity+nLockedQuantityD where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaId=iWarehouseAreaIdS and WarehouseAreaCategoryId=（select id from WarehouseAreaCategory where category=3 ） and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          update PartsStock set Quantity=Quantity+nLockedQuantityF where id=nPartsStockidS;

                          ---统购分销仓库减库存
                          update PartsStock set Quantity=Quantity-nLockedQuantityF where id=nTPartsStockidF;

                          select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          if iPartsLockedStock>0 then
                              ---锁定库存
                            update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityF,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          else
                            insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                            values(s_PartsLockedStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,iBranchId,iSparePartId,nLockedQuantityF,nCreatorId,vCreatorName,sysdate);
                          end if;
                          ---锁定库存
                          ---update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityF where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                        end;
                       else
                        begin
                          ---select TopLevelWarehouseAreaId,code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea where WarehouseId=nPublicwarehouseidG and AreaCategoryId=1 and TopLevelWarehouseAreaId not in(
                          ---select a.\*TopLevelWarehouseAreaId*\,a.code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and a.\*TopLevelWarehouseAreaId*\ not in(
                          ---select WarehouseAreaId from PartsStock where  WarehouseId=nPublicwarehouseidG ) and  rownum = 1;

                          select a.id,a.code,b.id into iWarehouseAreaIdS,vWarehouseAreaCodeS,nWarehouseareacategoryidS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and rownum = 1;
                          --select a.TopLevelWarehouseAreaId,a.code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nDefaultoutwarehouseid and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and a.TopLevelWarehouseAreaId not in(
                          --select WarehouseAreaId from PartsStock where  WarehouseId=nDefaultoutwarehouseid ) and  rownum = 1;
                          if iWarehouseAreaIdS>0 then
                          begin
                            ---PartsShiftOrderDetail配件移库单清单
                            ---,batchnumber,remark
                            insert into tempPartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
                            /*2017-9-3 hr modify nPartsshiftorderid会取最近一个新配件移库单的ID*/
                            values(s_PartsShiftOrderDetail.Nextval,nPartsshiftorderidF/*nPartsshiftorderid*/,iSparePartId,vSparepartcode,vSparepartname,iWarehouseAreaIdC,vCodeC,3,
                              iWarehouseAreaIdS,(select code from WarehouseArea where WarehouseId=nPublicwarehouseidG and id=iWarehouseAreaIdC),nWarehouseareacategoryidS,nLockedQuantityF);

                            insert into PartsStock(id,warehouseid,storagecompanyid,storagecompanytype,branchid,warehouseareaid,warehouseareacategoryid,partid,quantity,creatorid,creatorname,createtime)
                            values(s_PartsStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,nstoragecompanytype,iBranchId,iWarehouseAreaIdS,nWarehouseareacategoryidS,iSparePartId,nLockedQuantityF,nCreatorId,vCreatorName,sysdate);
                            ---update PartsStock set Quantity:=Quantity+(nSumQuantity-nvl(nSumLockedQuantity,0)) where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaId=iWarehouseAreaIdS and WarehouseAreaCategoryId=3 and StorageCompanyId=iStorageCompanyId and BranchId=iBranchId;

                            ---统购分销仓库减库存
                            update PartsStock set Quantity=Quantity-nLockedQuantityF where id=nTPartsStockidF;

                            select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            if iPartsLockedStock>0 then
                              ---锁定库存
                              update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityF,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            else
                              insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                              values(s_PartsLockedStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,iBranchId,iSparePartId,nLockedQuantityF,nCreatorId,vCreatorName,sysdate);
                            end if;

                            ---锁定库存
                            --update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityF where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                          end;

                          end if;
                        end;
                        end if;

                      end;
                      end if;

                      ---PartsInboundCheckBillDetail配件入库检验单清单
                      /*2017-9-3 hr modify nPartsInboundCheckBillid会取最近一个新配件入库检验单的ID
                        2017-10-10 hr modify 入库检验单清单的成本价 取 配件计划价，品牌取调拨入库仓库的品牌
                        2017-10-17 hr modify 入库检验单清单的库位ID与编号赋值有问题，不应是 检验区的库位*/
                      insert into tPartsInboundCheckBillDetail(id,partsinboundcheckbillid,sparepartid,sparepartcode,sparepartname,warehouseareaid,warehouseareacode,inspectedquantity,settlementprice,costprice)
                      values (s_PartsInboundCheckBillDetail.Nextval,nPartsInboundCheckBillidF/*nPartsinboundcheckbillid*/,iSparePartId,vSparepartcode,vSparepartname,/*iWarehouseAreaIdC,vCodeC*/
                      iWarehouseAreaIdS,(select code from WarehouseArea where id =iWarehouseAreaIdS and areakind=3),nLockedQuantityF,nvl(nOrderPrice,0),/*nvl(nOrderPrice,0)*/
                      (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG/*iSalesCategoryId*/));

                      ---PartsShippingOrderDetail配件发运单清单
                      ---differenceclassification
                      insert into tempPartsShippingOrderDetail(id,partsshippingorderid,sparepartid,sparepartcode,sparepartname,shippingamount,confirmedamount,settlementprice)
                      /*2017-9-3 hr modify nPartsshippingorderidD会取最近一个新配件发运单的ID*/
                      values(s_PartsShippingOrderDetail.Nextval,nPartsshippingorderidF/*nPartsshippingorderid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityF,nLockedQuantityF,nvl(nOrderPrice,0));

                      ---PartsOutboundPlanDetail配件出库计划清单
                      insert into tempPartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,/*outboundfulfillment,*/price)
                      /*2017-9-3 hr modify nPartsoutboundplanidG会取最近一个新出库计划单的ID*/
                      values(s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidGF/*nPartsoutboundplanidG*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityF,/*nLockedQuantityF,*/nvl(nOrderPrice,0));

                      /*2017-8-7 hr add 剩余未满足量未赋值*/
                      nWRemainQuantity := nWRemainQuantity - nLockedQuantityF;--剩余未满足量

                      /*
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,(nSumQuantity-nvl(nSumLockedQuantity,0)),nvl(nOrderPrice,0),2,2,sysdate,nCreatorId,vCreatorName);
                      */

                    /*
                      --第一仓库此次满足金额
                      mCurrentFulfilledAmountP:=nWRemainQuantity*nvl(nOrderPrice,0);

                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        --select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        select count(*) into nTest from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        if nvl(nTest,0)>0 then
                           select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --else
                          -- nLockedQuantity:=0;
                        end if;
                        if nQuantity-nvl(nLockedQuantity,0)>0 then --此库区可用数量
                        begin

                          if (nQuantity-nvl(nLockedQuantity,0)) >nWRemainQuantity then
                          begin
                            ---update PARTSSTOCK set Quantity=nQuantity-nWRemainQuantity where WarehouseAreaId=nWarehouseAreaId and  WarehouseId=iWarehouseidP  and PartId=iSparePartId;
                            nWRemainQuantity:= 0; --此库区剩下没满足数量,此次已经完全满足

                          end;
                          else
                          begin
                            ---update PARTSSTOCK set Quantity=0 where WarehouseAreaId=nWarehouseAreaId and  WarehouseId=iWarehouseidP  and PartId=iSparePartId;
                            nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --此库区剩下没满足数量

                           end;
                           end if;

                          nWRemainQuantity1:=nWRemainQuantity;

                          exit when nWRemainQuantity= 0;
                        end;
                        end if;

                      end loop;*/

                    end;
                    end if;
                  end;
                  else ---不是统购分销仓库
                  begin

                    nOrderProcessMethod:=2;

                    select Code,Name,storagecompanyid,storagecompanytype into vWareHouseCodeP,vWareHouseNameP,iStoragecompanyidP,nStoragecompanytype from warehouse where id=nPublicwarehouseid;
                    select code,name into vStoragecompanycodeP,vStoragecompanynameP from company where id=iStoragecompanyidP;



                    -- select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from PartsStock where
                    insert into tempPARTSSTOCK  select rownum,a.* from PARTSSTOCK a  where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1;
                    select sum(quantity) into nSumQuantity from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                    select sum(LockedQuantity) into nSumLockedQuantity from Partslockedstock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                    /*2017-8-3 hr add*/
                    insert into tempPARTSLockedSTOCK select id,warehouseid,storagecompanyid,branchid,partid,lockedquantity,creatorid,creatorname,createtime,modifierid,modifiername,modifytime from Partslockedstock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;

                    select count(*) into iPS from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;

                    if (nSumQuantity-nvl(nSumLockedQuantity,0))< nWRemainQuantity1 and (nSumQuantity-nvl(nSumLockedQuantity,0)) > 0 then ---首选仓库部分满足
                    begin
                      nNewLockedQuantity:=nSumQuantity-nvl(nSumLockedQuantity,0); ---需要锁定库存
                    /*
                      ---新建配件出库单
                      ---creatorid,creatorname,createtime,获得
                      --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                      vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                      nPartsOutboundBillId := s_PartsOutboundBill.Nextval;

                      insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                        partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,receivingcompanyid,receivingcompanycode, receivingcompanyname,receivingwarehouseid,
                        receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                        --orderapprovecomment,remark,
                        creatorid,creatorname,createtime)

                      Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,
                        iPartssalesordertypeid,vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingwarehouseid,
                        vReceivingwarehousecode,vReceivingWarehouseName,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);
                      */
                      ---新建出库计划单
                      if bInsertOutboundPlanF=false then ---如果此次审核第一仓库已经生成出库计划主单就不再生成
                      begin
                        nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                        nPartsoutboundplanidF :=nPartsoutboundplanid;
                        vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 出库计划单收货仓库id，编号，名称 需要赋值
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                          2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface,Remark,Companyaddressid)
                        values(nPartsoutboundplanid,vPartsOutboundPlanCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                          vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                          sysdate,0,vRemark,vCompanyAddressId);
                        bInsertOutboundPlanF:=true;
                      end;
                      end if;

                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,仓库，这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,(nSumQuantity-nvl(nSumLockedQuantity,0)),nvl(nOrderPrice,0),2,nOrderProcessMethod,sysdate,nCreatorId,vCreatorName);


                      ---新建出库计划清单
                      insert into tempPartsOutboundPlanDetail (id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price)
                      /*2017-8-3 hr modify nQuantity没有被清空，使用上一个循环中的值*/
                      /*2017-9-2 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                      values (s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidF/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,/*(nQuantity-nvl(nLockedQuantity,0))*/(nSumQuantity-nvl(nSumLockedQuantity,0)),nOrderPrice);

                      --第一仓库此次满足金额
                      mCurrentFulfilledAmountP:=(nSumQuantity-nvl(nSumLockedQuantity,0))*nvl(nOrderPrice,0);

                      /*2017-8-4 hr modify 配件多库位循环赋值剩余未满足量有问题，当锁定库存大于单个库位库存时，剩余未满足量将不会赋值*/
                      nWRemainQuantity:= nWRemainQuantity-(nSumQuantity-nvl(nSumLockedQuantity,0));
                      /*
                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        --select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        select count(*) into nTest from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        if nvl(nTest,0)>0 then
                           select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --else
                          -- nLockedQuantity:=0;
                        end if;
                        if nQuantity-nvl(nLockedQuantity,0)>0 then --首选库区可用数量
                        begin
                          ---update PARTSSTOCK set Quantity=0 where WarehouseAreaId=nWarehouseAreaId and  WarehouseId=iWarehouseidP  and PartId=iSparePartId;
                          nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --剩下没满足数量
                          ---nWRemainQuantity1:=nWRemainQuantity;
                          \*
                          --配件出库单清单（tempPartsOutboundBillDetail）记录
                          insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,(nQuantity-nvl(nLockedQuantity,0)),nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));
                          *\
                        end;
                        end if;

                      end loop;
                      */

                      ---锁定库存
                      select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      if iPartsLockedStock>0 then
                        update PartsLockedStock set LockedQuantity=LockedQuantity+nNewLockedQuantity,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      else
                        insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                        values(s_PartsLockedStock.Nextval,nPublicwarehouseid,iStoragecompanyidP,iBranchId,iSparePartId,nNewLockedQuantity,nCreatorId,vCreatorName,sysdate);
                      end if;
                      ---锁定库存
                      --update PARTSLockedSTOCK set LockedQuantity=LockedQuantity+nNewLockedQuantity where WarehouseId=nPublicwarehouseid  and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;

                      iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                      ---nWRemainQuantity:= nWRemainQuantity - (nSumQuantity-nvl(nSumLockedQuantity,0)); --剩下没满足数
                      nWRemainQuantity1:=nWRemainQuantity;

                    end;
                    elsif (nSumQuantity-nvl(nSumLockedQuantity,0)) >= nWRemainQuantity then
                      ---第一仓库完全满足
                    begin
                      nNewLockedQuantity:=nWRemainQuantity; ---需要锁定库存
                      /*
                      --新建配件出库单
                      ---creatorid,creatorname,createtime,获得
                      --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                      vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                      nPartsOutboundBillId := s_PartsOutboundBill.Nextval;

                      insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                        partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,receivingcompanyid,receivingcompanycode, receivingcompanyname,receivingwarehouseid,
                        receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                        --orderapprovecomment,remark,
                        creatorid,creatorname,createtime)

                      Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,
                        iPartssalesordertypeid,vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingwarehouseid,
                        vReceivingwarehousecode,vReceivingWarehouseName,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);
                      */
                      ---新建出库计划单
                      if bInsertOutboundPlanF=false then ---如果此次审核第一仓库已经生成出库计划主单就不再生成
                      begin
                        nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                        /*2017-8-3 hr modify 出库计划单ID变量没有赋值*/
                        nPartsOutboundPlanIdF/*nPartsoutboundplanid*/ :=nPartsoutboundplanid;
                        vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 出库计划单收货仓库id，编号，名称 需要赋值
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                          2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface,Remark,Companyaddressid)
                        values(nPartsoutboundplanid,vPartsOutboundPlanCode,nPublicwarehouseid ,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                          vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                          sysdate,0,vRemark,vCompanyAddressId);
                        bInsertOutboundPlanF:=true;
                      end;
                      end if;

                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,nWRemainQuantity,nvl(nOrderPrice,0),1,2,sysdate,nCreatorId,vCreatorName);

                      ---新建出库计划清单
                      insert into tempPartsOutboundPlanDetail (id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price)
                      /*2017-9-2 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                      values (s_PartsOutboundPlanDetail.Nextval,nPartsOutboundPlanIdF/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nWRemainQuantity,nOrderPrice);
                      --第一仓库此次满足金额
                      mCurrentFulfilledAmountP:=nWRemainQuantity*nvl(nOrderPrice,0);

                      /*2017-8-4 hr modify 配件多库位循环赋值剩余未满足量有问题，当锁定库存大于单个库位库存时，剩余未满足量将不会赋值*/
                      nWRemainQuantity:= 0;
                      nWRemainQuantity1:=nWRemainQuantity;
                      /*
                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        --select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        select count(*) into nTest from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        if nvl(nTest,0)>0 then
                           select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --else
                          -- nLockedQuantity:=0;
                        end if;
                        if nQuantity-nvl(nLockedQuantity,0)>0 then --此库区可用数量
                        begin

                          if (nQuantity-nvl(nLockedQuantity,0)) >nWRemainQuantity then
                          begin
                            ---update PARTSSTOCK set Quantity=nQuantity-nWRemainQuantity where WarehouseAreaId=nWarehouseAreaId and  WarehouseId=iWarehouseidP  and PartId=iSparePartId;
                            nWRemainQuantity:= 0; --此库区剩下没满足数量,此次已经完全满足

                          end;
                          else
                          begin
                            ---update PARTSSTOCK set Quantity=nLockedQuantity where WarehouseAreaId=nWarehouseAreaId and  WarehouseId=iWarehouseidP  and PartId=iSparePartId;
                            nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --此库区剩下没满足数量

                           end;
                           end if;

                          nWRemainQuantity1:=nWRemainQuantity;

                          \*
                          --配件出库单清单（tempPartsOutboundBillDetail）记录
                          insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,(nQuantity-nvl(nLockedQuantity,0)),nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));
                          *\
                          exit when nWRemainQuantity= 0;
                        end;
                        end if;

                      end loop;
                      */

                      ---锁定库存
                      select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      if iPartsLockedStock>0 then
                        update PartsLockedStock set LockedQuantity=LockedQuantity+nNewLockedQuantity,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      else
                        insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                        values(s_PartsLockedStock.Nextval,nPublicwarehouseid,iStoragecompanyidP,iBranchId,iSparePartId,nNewLockedQuantity,nCreatorId,vCreatorName,sysdate);
                      end if;
                      ---锁定库存
                      ---update PARTSLockedSTOCK set LockedQuantity=LockedQuantity+nNewLockedQuantity where WarehouseId=nPublicwarehouseid  and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                    end;
                    end if;


                  end;
                  end if;

                end;
                end if; --第一仓库


----------------第二仓库

                if  nWRemainQuantity>0 and nSecondwarehouseid>0  then
                begin
                  --res:='第二仓库';
                  nPublicwarehouseid:=nSecondwarehouseid;

                  ---判断第二仓库是否为统购分销仓库
                  select IsCentralizedPurchase into bIsCentralizedPurchase from warehouse where id=nPublicwarehouseid;
                  if  bIsCentralizedPurchase=1 then
                  begin
                    nPublicwarehouseidG:=nSecondoutwarehouseid;
                    nOrderProcessMethod:=10;

                    select Code,Name,storagecompanyid,storagecompanytype into vWareHouseCodeG,vWareHouseNameG,iStoragecompanyidG,nStoragecompanytypeG from warehouse where id=nPublicwarehouseidG;
                    select code,name into vStoragecompanycodeG,vStoragecompanynameG from company where id=iStoragecompanyidG;
                    /*2017-10-11 hr add 增加入库品牌取值*/
                    begin
                      select b.partssalescategoryid,c.name into iSalesCategoryIdG,vSalesCategoryNameG from salesunitaffiwarehouse a left join salesunit b on a.salesunitid = b.id left join partssalescategory c on b.partssalescategoryid = c.id where a.warehouseid = nPublicwarehouseidG;
                    exception
                    when no_data_found then
                      begin
                        vAutoapprovecommentG:='统购分销仓库'||vWareHouseCodeG||'未维护销售组织';
                        raise FMyException;
                      end;
                    end;

                    select Code,Name,storagecompanyid,storagecompanytype into vWareHouseCodeP,vWareHouseNameP,iStoragecompanyidP,nStoragecompanytype from warehouse where id=nPublicwarehouseid;
                    select code,name into vStoragecompanycodeP,vStoragecompanynameP from company where id=iStoragecompanyidP;
                    /*2017-10-11 hr add 增加调拨出库发运品牌取值*/
                    begin
                      select b.partssalescategoryid,c.name into iSalesCategoryIdP,vSalesCategoryNameP from salesunitaffiwarehouse a left join salesunit b on a.salesunitid = b.id left join partssalescategory c on b.partssalescategoryid = c.id where a.warehouseid = nPublicwarehouseid;
                    exception
                    when no_data_found then
                      begin
                        vAutoapprovecommentG:='统购分销仓库'||vWareHouseCodeP||'未维护销售组织';
                        raise FMyException;
                      end;
                    end;


                    -- select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from PartsStock where
                    insert into tempPARTSSTOCK  select rownum,a.* from PARTSSTOCK a  where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 ;
                    ---insert into tempPARTSSTOCK  select rownum,a.* from PARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1
                    ---and (quantity=(select max(quantity) from PARTSSTOCK where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId));

                    select sum(quantity) into nSumQuantity from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;

                    --select max(nvl(quantity,0)) into nMaxQuantityD from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                    --select id into nTPartsStockidD from tempPARTSSTOCK a  where quantity=nMaxQuantityD and rownum=1 and WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;

                    select max(nvl(quantity,0)) into nMaxQuantityS from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                    /*2017-8-7 hr modfiy 当库存不存在时，查不到统购分销仓库最大库存库区ID
                                         最大最大库存库区ID变量为nTPartsStockidS*/
                    begin
                    ---获取统购分销仓库最大库存库区ID
                      select id,WarehouseAreaId into /*nPartsStockidS*/nTPartsStockidS,nWarehouseAreaIdS from tempPARTSSTOCK a  where quantity=nMaxQuantityS and rownum=1 and WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                    exception
                      when no_data_found then
                        nTPartsStockidS := 0;
                    end;
                    /*2017-10-17 hr add 获取调拨出库仓库的库位*/
                    begin
                      select code into nWarehouseAreaCodeS from WarehouseArea a  where a.id = nWarehouseAreaIdS and a.areakind = 3;
                    exception
                      when no_data_found then
                        nWarehouseAreaCodeS := 0;
                    end;

                    select sum(LockedQuantity) into nSumLockedQuantity from Partslockedstock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;

                    select count(*) into iPS from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;

                    if (nSumQuantity-nvl(nSumLockedQuantity,0))< nWRemainQuantity1 and (nSumQuantity-nvl(nSumLockedQuantity,0)) > 0 then ---第二仓库部分满足
                    begin

                      /*2017-10-11 hr add 对出入库计划价进行校验
                      1）计划价必须存在，且>0
                      2）出入库仓库的计划价必须一致*/
                      begin
                        select PlannedPrice into nPlannedPriceG from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG;
                      exception
                      when no_data_found then
                        begin
                          vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameG||']没有计划价';
                          raise FMyException;
                        end;
                      end;
                      if nPlannedPriceG = 0 then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameG||']的计划价为0';
                        raise FMyException;
                      end;
                      end if;
                      begin
                        select PlannedPrice into nPlannedPriceP from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdP;
                      exception
                      when no_data_found then
                        begin
                          vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']没有计划价';
                          raise FMyException;
                        end;
                      end;
                      if nPlannedPriceP = 0 then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']的计划价为0';
                        raise FMyException;
                      end;
                      end if;
                      if nPlannedPriceP <> nPlannedPriceG then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']的计划价与品牌['||vSalesCategoryNameG||']的计划价不一致';
                        raise FMyException;
                      end;
                      end if;


                      if bInsertOutboundPlanST = false then
                      begin
                        ---收货单位Id  ReceivingCompanyId,收货单位编号  ReceivingCompanyCode,收货单位名称  ReceivingCompanyName 填为  nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG


                        ---PartsTransferOrder配件调拨单
                        nPartsTransferOrderid := s_PartsTransferOrder.Nextval;
                        vPartsTransferOrderCode := regexp_replace(regexp_replace(vTransferOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateTransferOrderId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-8-7 hr modify 调拨单状态为 已审批
                          2017-10-12 hr modify 调拨单的调拨类型 应该是 3--急需调拨；发运方式设置为空
                          2017-10-13 hr add 修改人，审批人，修改时间，审批时间赋值*/
                        insert into PartsTransferOrder (id,code,originalwarehouseid,originalwarehousecode,originalwarehousename,storagecompanyid,storagecompanytype,destwarehouseid,destwarehousecode,destwarehousename,status,type,
                          shippingmethod,creatorid,creatorname,createtime,originalbillid,originalbillcode,totalamount,Modifierid,Modifiername,Modifytime,Approverid,Approvername,Approvetime)
                        values(nPartsTransferOrderid,vPartsTransferOrderCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,nStoragecompanytype,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,/*1*/2,/*1*/3,
                        null/*iShippingMethod*/,nCreatorID,vCreatorName,sysdate,PartCheckId,vCode,0,nCreatorID,vCreatorName,sysdate,nCreatorID,vCreatorName,sysdate);
                        --- totalamount,totalplanamount需要计算。

                        ----PartsOutboundPlan配件出库计划 (第二仓库)
                        nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                        vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值，出库计划单的源单据 应是 调拨单
                          2017-10-12 hr modify 调拨出库生成的出库计划单的出库类型 应是 调拨出库；对方单位赋值有问题
                          2017-10-16 hr modify 发运方式设置为空，客户账户Id默认null*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface )
                        values(nPartsoutboundplanid,vPartsOutboundPlanCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdP/*iSalesCategoryId*/,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPartsTransferOrderid/*PartCheckId*/,
                          vPartsTransferOrderCode/*vCode*/,4/*1*/,null/*iShippingMethod*/,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,3,nCreatorID,vCreatorName,sysdate,0);

                        ---新建配件出库单
                        --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                        vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        nPartsOutboundBillId := s_PartsOutboundBill.Nextval;
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-13 hr modify 调拨出库生成的出库单的出库类型 应是 调拨出库 配件调拨；备注是'统购分销出库'；结算状态是 待结算；对方单位赋值有问题
                          2017-10-16 hr modify 发运方式设置为空，客户账户Id默认null*/
                        insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                          partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,receivingcompanyid,receivingcompanycode, receivingcompanyname,
                          receivingwarehouseid,receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                          creatorid,creatorname,createtime,Remark)
                        Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdP/*iSalesCategoryId*/,
                          iPartssalesordertypeid,vPartssalesordertypename,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,
                          nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,4/*1*/,null/*iShippingMethod*/,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,2/*1*/,
                          nCreatorID,vCreatorName,sysdate,'统购分销出库');


                        ---PartsShippingOrder配件发运单  /*入库计划单的源单据是 发运单，所以调整发运单生成的位置，放到入库计划单生成前*/
                        nPartsShippingOrderid := s_PartsShippingOrder.Nextval;
                        /*2017-10-12 hr modify 发运单应是4位流水号*/
                        vPartsShippingOrderCode := regexp_replace(regexp_replace(vShippingOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateShippingOrderId,TRUNC(sysdate,'DD'),vBranchCode)),4/*6*/,'0'));
                        /*2017-9-6 hr modify 发运单的发货单位与结算单位赋值有问题，状态不应是新建，应该是 回执确认
                          2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 配件发运单增加品牌字段赋值
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-12 hr add 发运单备注默认'统购分销发运'
                          2017-10-13 hr add 发运方式默认 8--客户自行运输；发运日期 不赋值；收货单位赋值有问题；预计到货日期 不赋值*/
                        insert into PartsShippingOrder(id,code,type,branchid,shippingcompanyid,shippingcompanycode,shippingcompanyname,settlementcompanyid,settlementcompanycode,settlementcompanyname,receivingcompanyid,receivingcompanycode,invoicereceivesalecateid,invoicereceivesalecatename,receivingcompanyname, warehouseid,warehousecode,warehousename,
                          receivingwarehouseid,receivingwarehousecode,receivingwarehousename,originalrequirementbillid,originalrequirementbillcode,originalrequirementbilltype,shippingdate,status,creatorid,creatorname,createtime,expectedplacedate,PartsSalesCategoryId,Remark,Shippingmethod)
                        values(nPartsShippingOrderid,vPartsShippingOrderCode,4,ibranchid,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,*/iStoragecompanyidP,vStoragecompanycodeP,/*nPublicwarehouseidG,vWareHouseCodeG ,*/iPartssalesordertypeid,vPartsSalesOrderTypeName,vStoragecompanynameP/*vWareHouseNameP*/,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,
                        nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,PartCheckId,vCode,1/*nPartssalescategoryid*/,null/*sysdate*/,/*1*/3,nCreatorID,vCreatorName,sysdate,null/*sysdate*/,iSalesCategoryIdP/*iSalesCategoryId*/,'统购分销发运',8);


                        ---PartsInboundPlan配件入库计划
                        nPartsInboundPlanid := s_PartsInboundPlan.Nextval;
                        vPartsInboundPlanCode := regexp_replace(regexp_replace(vInboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateInboundPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-9 hr modify 入库计划单 对方单位 赋值有问题，不应是仓库，品牌赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值，入库计划单的源单据 应是 发运单
                          2017-10-13 hr add 入库计划单备注默认'统购分销发运'；抵达日期 不赋值，客户账户Id不赋值*/
                        insert into PartsInboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,
                          sourceid,sourcecode,inboundtype,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,arrivaldate,creatorid,creatorname,createtime,IfWmsInterface,Remark)
                        values(nPartsInboundPlanid,vPartsInboundPlanCode,nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytypeG,ibranchid,vbranchcode,vbranchname,iSalesCategoryIdG/*nPartssalescategoryid*//*ipartssalesordertypeid*/,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,/*nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,*/
                        nPartsShippingOrderid/*PartCheckId*/,vPartsShippingOrderCode/*vCode*/,3,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,2,null/*sysdate*/,nCreatorID,vCreatorName,sysdate,0,'统购分销发运');
                        ---inboundtype:3 配件调拨

                        ---PartsInboundCheckBill配件入库检验单
                        nPartsInboundCheckBillid := s_PartsInboundCheckBill.Nextval;
                        vPartsInboundCheckBillCode := regexp_replace(regexp_replace(vInboundCheckBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateInboundCheckBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-10 hr modify 入库检验单 仓库 仓储企业 品牌 对方单位 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-16 hr modify 客户账户Id不赋值，状态是 3-上架完成*/
                        insert into PartsInboundCheckBill(id,code,partsinboundplanid,warehouseid, warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,partssalescategoryid,branchid,branchcode,branchname,
                          counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,inboundtype,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,settlementstatus,creatorid,creatorname,createtime)
                        values (nPartsInboundCheckBillid,vPartsInboundCheckBillCode,nPartsInboundPlanid,nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytypeG,iSalesCategoryIdG/*nPartssalescategoryid*/,ibranchid,vbranchcode,vbranchname,
                        iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,/*iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,*/3,null/*ncustomeraccountid*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,3/*1*/,2,nCreatorID,vCreatorName,sysdate);
                        ---inboundtype:3 配件调拨. settlementstatus=2 待结算

                        ---PartsShiftOrder配件移库单
                        nPartsShiftOrderid := s_PartsShiftOrder.Nextval;
                        nPartsShiftOrderidS := nPartsShiftOrderid;
                        vPartsShiftOrderCode := regexp_replace(regexp_replace(vShiftOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateShiftOrderId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        insert into  PartsShiftOrder(id,code,branchid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,status,type,creatorid,creatorname,createtime)
                        values(nPartsShiftOrderid,vPartsShiftOrderCode,ibranchid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,nStoragecompanytype,1,2,nCreatorID,vCreatorName,sysdate);


                        ---PartsShippingOrderRef配件发运单关联单
                        insert into PartsShippingOrderRef(Id,PartsShippingOrderId,PartsOutboundBillId)
                        values(s_PartsShippingOrderRef.Nextval,nPartsShippingOrderid,nPartsOutboundBillId);

                        ----仓库PartsOutboundPlan配件出库计划 （第二出库仓库）
                        nPartsoutboundplanidG := s_PartsOutboundPlan.Nextval;
                        vPartsOutboundPlanCodeG := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 出库计划单收货仓库id，编号，名称 需要赋值
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                          2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface,Remark,Companyaddressid)
                        values(nPartsoutboundplanidG,vPartsOutboundPlanCodeG,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdG/*iSalesCategoryId*/,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                          vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                          sysdate,0,vRemark,vCompanyAddressId);

                        /*values(nPartsoutboundplanid,vPartsOutboundPlanCode,iWarehouseidP,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,nPartssalescategoryid,
                        vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                        vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);*/

                        nPartsTransferOrderidS := nPartsTransferOrderid;
                        nPartsoutboundplanidS := nPartsoutboundplanid;
                        nPartsOutboundBillIdS := nPartsOutboundBillId;
                        nPartsInboundPlanidS := nPartsInboundPlanid;
                        nPartsInboundCheckBillidS := nPartsInboundCheckBillid;
                        nPartsoutboundplanidGS := nPartsoutboundplanidG;
                        nPartsshippingorderidS := nPartsshippingorderid;
                        nPartsshiftorderidS := nPartsshiftorderid;

                        bInsertOutboundPlanST := true;
                      end;
                      end if;

                      --第二仓库此次满足数量和金额
                      if nMaxQuantityS <= (nSumQuantity-nvl(nSumLockedQuantity,0)) then
                      begin
                         mCurrentFulfilledAmountS:=nMaxQuantityS*nvl(nOrderPrice,0);
                         nLockedQuantityS := nMaxQuantityS;
                      end;
                      else
                      begin
                         mCurrentFulfilledAmountS:=(nSumQuantity-nvl(nSumLockedQuantity,0))*nvl(nOrderPrice,0);
                         nLockedQuantityS := (nSumQuantity-nvl(nSumLockedQuantity,0));
                      end;
                      end if;

                      ---PartsTransferOrderDetail配件调拨单清单
                      ---  remark,
                      insert into tempPartsTransferOrderDetail(id,partstransferorderid,sparepartid,sparepartcode,sparepartname,plannedamount,confirmedamount,price,plannprice)
                      /*2017-9-3 hr modify nPartsTransferOrderid会取最近一个新配件调拨单的ID
                        2017-10-10 hr modify 调拨单清单的计划价 取 配件计划价，品牌取调拨入库仓库的品牌*/
                      values(s_PartsTransferOrderDetail.Nextval,nPartsTransferOrderidS/*nPartsTransferOrderid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityS,nLockedQuantityS,nvl(nOrderPrice,0),/*nvl(nOrderPrice,0)*/
                      (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG/*iSalesCategoryId*/));

                      ---PartsOutboundPlanDetail配件出库计划清单 第二仓库
                      insert into tempPartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price)
                      /*2017-9-3 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                      values(s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidS/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityS,nLockedQuantityS,nvl(nOrderPrice,0));

                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,仓库，这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,(nSumQuantity-nvl(nSumLockedQuantity,0)),nvl(nOrderPrice,0),2,nOrderProcessMethod,sysdate,nCreatorId,vCreatorName);

                      ---PartsInboundPlanDetail配件入库计划清单
                      insert into tempPartsInboundPlanDetail(id,partsinboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,inspectedquantity,price)
                      /*2017-9-3 hr modify nPartsInboundPlanid会取最近一个新配件入库计划单的ID*/
                      values(s_PartsInboundPlanDetail.Nextval,nPartsInboundPlanidS/*nPartsinboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityS,nLockedQuantityS,nvl(nOrderPrice,0));

                      /*2017-8-7 hr add*/
                      --统购仓库配件出库单清单（tempPartsOutboundBillDetail）记录
                      /*2017-9-3 hr modify nPartsOutboundBillId会取最近一个新配件出库单的ID
                        2017-10-11 hr modify 品牌取调拨出库仓库的品牌
                        2017-10-17 hr modify 出库清单的库位ID与编号赋值有问题*/
                      insert into tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillIdS/*nPartsOutboundBillId*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityS,nWarehouseAreaIdS,nWarehouseAreaCodeS,/*nPublicwarehouseid,vWareHouseCodeP,*/nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdP/*iSalesCategoryId*/));

                      /*
                      --第二仓库此次满足金额
                      mCurrentFulfilledAmountP:=(nSumQuantity-nvl(nSumLockedQuantity,0))*nvl(nOrderPrice,0);

                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        --select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        select count(*) into nTest from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        if nvl(nTest,0)>0 then
                           select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --else
                          -- nLockedQuantity:=0;
                        end if;

                        if nQuantity-nvl(nLockedQuantity,0)>0 then --库区可用数量
                        begin

                          nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --剩下没满足数量
                          ---nWRemainQuantity1:=nWRemainQuantity;

                          --统购仓库配件出库单清单（tempPartsOutboundBillDetail）记录
                          insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,(nQuantity-nvl(nLockedQuantity,0)),nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));

                        end;
                        end if;

                      end loop;
                      */

                      ---根据第二出库仓库ID和配件ID，检验区
                      ---select WarehouseAreaId,Quantity into iWarehouseAreaIdC,iQuantityC from PartsStock where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaCategoryId=3 and StorageCompanyId=iStorageCompanyId and BranchId=iBranchId and rownum = 1;
                      select a.id,a.Code into iWarehouseAreaIdC,vCodeC from WarehouseArea a, WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.AreaCategoryId=b.id and b.category=3 and rownum=1 and a.areakind=3;
                      if iWarehouseAreaIdC>0 then
                      begin

                        ---根据首选出库仓库ID和配件ID，保管区
                        ---select WarehouseAreaId,Quantity into iWarehouseAreaIdS,nQuantityS from PartsStock a where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and StorageCompanyId=nStorageCompanyId and BranchId=iBranchId and PartId=iSparePartId and rownum = 1;
                        select count(*) into iCount from PartsStock a,WarehouseAreaCategory b where  a.WarehouseId=nPublicwarehouseidG and a.WarehouseAreaCategoryId=b.id and b.category=1 and a.StorageCompanyId=iStoragecompanyidG and a.BranchId=iBranchId and a.PartId=iSparePartId;
                        --if iWarehouseAreaIdS>0 then
                        if iCount>0 then
                        begin
                          ---PartsShiftOrderDetail配件移库单清单
                          ---,batchnumber,remark
                          select a.WarehouseAreaId,a.Quantity,b.id,a.id into iWarehouseAreaIdS,nQuantityS,nWarehouseareacategoryidS,nPartsStockidS from PartsStock a,WarehouseAreaCategory b where a.partid=iSparePartId and a.WarehouseId=nPublicwarehouseidG and a.WarehouseAreaCategoryId=b.id and b.category=1 and a.StorageCompanyId=iStoragecompanyidG and a.BranchId=iBranchId and a.PartId=iSparePartId and rownum = 1;
                          insert into tempPartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
                          /*2017-9-3 hr modify nPartsshiftorderid会取最近一个新配件移库单的ID*/
                          values(s_PartsShiftOrderDetail.Nextval,nPartsshiftorderidS/*nPartsshiftorderid*/,iSparePartId,vSparepartcode,vSparepartname,iWarehouseAreaIdC,vCodeC,3,
                          iWarehouseAreaIdS,(select code from WarehouseArea where WarehouseId=nPublicwarehouseidG and id=iWarehouseAreaIdC),1,nLockedQuantityS);

                          --update PartsStock set Quantity=Quantity+nLockedQuantityD where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaId=iWarehouseAreaIdS and WarehouseAreaCategoryId=（select id from WarehouseAreaCategory where category=3 ） and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          update PartsStock set Quantity=Quantity+nLockedQuantityS where id=nPartsStockidS;

                          ---统购分销仓库减库存
                          update PartsStock set Quantity=Quantity-nLockedQuantityS where id=nTPartsStockidS;

                          select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          if iPartsLockedStock>0 then
                              ---锁定库存
                            update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityS,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          else
                            insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                            values(s_PartsLockedStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,iBranchId,iSparePartId,nLockedQuantityS,nCreatorId,vCreatorName,sysdate);
                          end if;
                          ---锁定库存
                          --update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityS where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                        end;
                       else
                        begin
                          ---select TopLevelWarehouseAreaId,code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea where WarehouseId=nPublicwarehouseidG and AreaCategoryId=1 and TopLevelWarehouseAreaId not in(
                          ---select a.\*TopLevelWarehouseAreaId*\,a.code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and a.\*TopLevelWarehouseAreaId*\ not in(
                          ---select WarehouseAreaId from PartsStock where  WarehouseId=nPublicwarehouseidG ) and  rownum = 1;

                          select a.id,a.code,b.id into iWarehouseAreaIdS,vWarehouseAreaCodeS,nWarehouseareacategoryidS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and rownum = 1;
                          --select a.TopLevelWarehouseAreaId,a.code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nDefaultoutwarehouseid and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and a.TopLevelWarehouseAreaId not in(
                          --select WarehouseAreaId from PartsStock where  WarehouseId=nDefaultoutwarehouseid ) and  rownum = 1;
                          if iWarehouseAreaIdS>0 then
                          begin
                            ---PartsShiftOrderDetail配件移库单清单
                            ---,batchnumber,remark
                            insert into tempPartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
                            /*2017-9-3 hr modify nPartsshiftorderid会取最近一个新配件移库单的ID*/
                            values(s_PartsShiftOrderDetail.Nextval,nPartsshiftorderidS/*nPartsshiftorderid*/,iSparePartId,vSparepartcode,vSparepartname,iWarehouseAreaIdC,vCodeC,3,
                              iWarehouseAreaIdS,(select code from WarehouseArea where WarehouseId=nPublicwarehouseidG and id=iWarehouseAreaIdC),nWarehouseareacategoryidS,nLockedQuantityS);

                            insert into PartsStock(id,warehouseid,storagecompanyid,storagecompanytype,branchid,warehouseareaid,warehouseareacategoryid,partid,quantity,creatorid,creatorname,createtime)
                            values(s_PartsStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,nstoragecompanytype,iBranchId,iWarehouseAreaIdS,nWarehouseareacategoryidS,iSparePartId,nLockedQuantityS,nCreatorId,vCreatorName,sysdate);
                            ---update PartsStock set Quantity:=Quantity+(nSumQuantity-nvl(nSumLockedQuantity,0)) where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaId=iWarehouseAreaIdS and WarehouseAreaCategoryId=3 and StorageCompanyId=iStorageCompanyId and BranchId=iBranchId;
                            ---统购分销仓库减库存
                            update PartsStock set Quantity=Quantity-nLockedQuantityS where id=nTPartsStockidS;

                            select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            if iPartsLockedStock>0 then
                              ---锁定库存
                              update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityS,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            else
                              insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                              values(s_PartsLockedStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,iBranchId,iSparePartId,nLockedQuantityS,nCreatorId,vCreatorName,sysdate);
                            end if;
                            ---锁定库存
                            --update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityS where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                          end;

                          end if;
                        end;
                        end if;

                      end;
                      end if;

                      ---PartsInboundCheckBillDetail配件入库检验单清单
                      /*2017-9-3 hr modify nPartsInboundCheckBillid会取最近一个新配件入库检验单的ID
                        2017-10-10 hr modify 入库检验单清单的成本价 取 配件计划价，品牌取调拨入库仓库的品牌
                        2017-10-17 hr modify 入库检验单清单的库位ID与编号赋值有问题，不应是 检验区的库位*/
                      insert into tPartsInboundCheckBillDetail(id,partsinboundcheckbillid,sparepartid,sparepartcode,sparepartname,warehouseareaid,warehouseareacode,inspectedquantity,settlementprice,costprice)
                      values (s_PartsInboundCheckBillDetail.Nextval,nPartsInboundCheckBillidS/*nPartsinboundcheckbillid*/,iSparePartId,vSparepartcode,vSparepartname,/*iWarehouseAreaIdC,vCodeC*/
                      iWarehouseAreaIdS,(select code from WarehouseArea where id =iWarehouseAreaIdS and areakind=3),nLockedQuantityS,nvl(nOrderPrice,0),/*nvl(nOrderPrice,0)*/
                      (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG/*iSalesCategoryId*/));

                      ---PartsShippingOrderDetail配件发运单清单
                      ---differenceclassification
                      insert into tempPartsShippingOrderDetail(id,partsshippingorderid,sparepartid,sparepartcode,sparepartname,shippingamount,confirmedamount,settlementprice)
                      /*2017-9-3 hr modify nPartsshippingorderidD会取最近一个新配件发运单的ID*/
                      values(s_PartsShippingOrderDetail.Nextval,nPartsshippingorderidS/*nPartsshippingorderid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityS,nLockedQuantityS,nvl(nOrderPrice,0));

                      ---PartsOutboundPlanDetail配件出库计划清单
                      insert into tempPartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,/*outboundfulfillment,*/price)
                      /*2017-9-3 hr modify nPartsoutboundplanidG会取最近一个新出库计划单的ID*/
                      values(s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidGS/*nPartsoutboundplanidG*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityS,/*nLockedQuantityS,*/nvl(nOrderPrice,0));


                      ---nWRemainQuantity:= nWRemainQuantity - (nSumQuantity-nvl(nSumLockedQuantity,0)); --剩下没满足数
                      --nWRemainQuantity1:=nWRemainQuantity-nLockedQuantityS;
                      /*2017-8-7 hr modify 剩余未满足量未赋值*/
                      nWRemainQuantity := nWRemainQuantity - nLockedQuantityS;--剩余未满足量
                      nWRemainQuantity1 := nWRemainQuantity;

                    end;
                    elsif (nSumQuantity-nvl(nSumLockedQuantity,0)) >= nWRemainQuantity then
                      ---第二仓库完全满足
                    begin

                      /*2017-10-11 hr add 对出入库计划价进行校验
                      1）计划价必须存在，且>0
                      2）出入库仓库的计划价必须一致*/
                      begin
                        select PlannedPrice into nPlannedPriceG from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG;
                      exception
                      when no_data_found then
                        begin
                          vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameG||']没有计划价';
                          raise FMyException;
                        end;
                      end;
                      if nPlannedPriceG = 0 then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameG||']的计划价为0';
                        raise FMyException;
                      end;
                      end if;
                      begin
                        select PlannedPrice into nPlannedPriceP from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdP;
                      exception
                      when no_data_found then
                        begin
                          vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']没有计划价';
                          raise FMyException;
                        end;
                      end;
                      if nPlannedPriceP = 0 then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']的计划价为0';
                        raise FMyException;
                      end;
                      end if;
                      if nPlannedPriceP <> nPlannedPriceG then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']的计划价与品牌['||vSalesCategoryNameG||']的计划价不一致';
                        raise FMyException;
                      end;
                      end if;

                      if bInsertOutboundPlanST = false then
                      begin
                        ---收货单位Id  ReceivingCompanyId,收货单位编号  ReceivingCompanyCode,收货单位名称  ReceivingCompanyName 填为  nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG


                        ---PartsTransferOrder配件调拨单
                        nPartsTransferOrderid := s_PartsTransferOrder.Nextval;
                        vPartsTransferOrderCode := regexp_replace(regexp_replace(vTransferOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateTransferOrderId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-8-7 hr modify 调拨单状态为 已审批
                          2017-10-12 hr modify 调拨单的调拨类型 应该是 3--急需调拨；发运方式设置为空
                          2017-10-13 hr add 修改人，审批人，修改时间，审批时间赋值*/
                        insert into PartsTransferOrder (id,code,originalwarehouseid,originalwarehousecode,originalwarehousename,storagecompanyid,storagecompanytype,destwarehouseid,destwarehousecode,destwarehousename,status,type,
                          shippingmethod,creatorid,creatorname,createtime,originalbillid,originalbillcode,totalamount,Modifierid,Modifiername,Modifytime,Approverid,Approvername,Approvetime)
                        values(nPartsTransferOrderid,vPartsTransferOrderCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,nStoragecompanytype,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,/*1*/2,/*1*/3,
                        null/*iShippingMethod*/,nCreatorID,vCreatorName,sysdate,PartCheckId,vCode,0,nCreatorID,vCreatorName,sysdate,nCreatorID,vCreatorName,sysdate);
                        --- totalamount,totalplanamount需要计算。

                        ----PartsOutboundPlan配件出库计划 (第二仓库)
                        nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                        vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值，出库计划单的源单据 应是 调拨单
                          2017-10-12 hr modify 调拨出库生成的出库计划单的出库类型 应是 调拨出库；对方单位赋值有问题
                          2017-10-16 hr modify 发运方式设置为空，客户账户Id默认null*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface )
                        values(nPartsoutboundplanid,vPartsOutboundPlanCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdP/*iSalesCategoryId*/,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPartsTransferOrderid/*PartCheckId*/,
                          vPartsTransferOrderCode/*vCode*/,4/*1*/,null/*iShippingMethod*/,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,3,nCreatorID,vCreatorName,sysdate,0);

                        ---新建配件出库单
                        --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                        vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        nPartsOutboundBillId := s_PartsOutboundBill.Nextval;
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-13 hr modify 调拨出库生成的出库单的出库类型 应是 调拨出库 配件调拨；备注是'统购分销出库'；结算状态是 待结算；对方单位赋值有问题
                          2017-10-16 hr modify 发运方式设置为空，客户账户Id默认null*/
                        insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                          partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,receivingcompanyid,receivingcompanycode, receivingcompanyname,
                          receivingwarehouseid,receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                          creatorid,creatorname,createtime,Remark)
                        Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdP/*iSalesCategoryId*/,
                          iPartssalesordertypeid,vPartssalesordertypename,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,
                          nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,4/*1*/,null/*iShippingMethod*/,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,2/*1*/,
                          nCreatorID,vCreatorName,sysdate,'统购分销出库');


                        ---PartsShippingOrder配件发运单  /*入库计划单的源单据是 发运单，所以调整发运单生成的位置，放到入库计划单生成前*/
                        nPartsShippingOrderid := s_PartsShippingOrder.Nextval;
                        /*2017-10-12 hr modify 发运单应是4位流水号*/
                        vPartsShippingOrderCode := regexp_replace(regexp_replace(vShippingOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateShippingOrderId,TRUNC(sysdate,'DD'),vBranchCode)),4/*6*/,'0'));
                        /*2017-9-6 hr modify 发运单的发货单位与结算单位赋值有问题，状态不应是新建，应该是 回执确认
                          2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 配件发运单增加品牌字段赋值
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-12 hr add 发运单备注默认'统购分销发运'
                          2017-10-13 hr add 发运方式默认 8--客户自行运输；发运日期 不赋值；收货单位赋值有问题；预计到货日期 不赋值*/
                        insert into PartsShippingOrder(id,code,type,branchid,shippingcompanyid,shippingcompanycode,shippingcompanyname,settlementcompanyid,settlementcompanycode,settlementcompanyname,receivingcompanyid,receivingcompanycode,invoicereceivesalecateid,invoicereceivesalecatename,receivingcompanyname, warehouseid,warehousecode,warehousename,
                          receivingwarehouseid,receivingwarehousecode,receivingwarehousename,originalrequirementbillid,originalrequirementbillcode,originalrequirementbilltype,shippingdate,status,creatorid,creatorname,createtime,expectedplacedate,PartsSalesCategoryId,Remark,Shippingmethod)
                        values(nPartsShippingOrderid,vPartsShippingOrderCode,4,ibranchid,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,*/iStoragecompanyidP,vStoragecompanycodeP,/*nPublicwarehouseidG,vWareHouseCodeG ,*/iPartssalesordertypeid,vPartsSalesOrderTypeName,vStoragecompanynameP/*vWareHouseNameP*/,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,
                        nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,PartCheckId,vCode,1/*nPartssalescategoryid*/,null/*sysdate*/,/*1*/3,nCreatorID,vCreatorName,sysdate,null/*sysdate*/,iSalesCategoryIdP/*iSalesCategoryId*/,'统购分销发运',8);


                        ---PartsInboundPlan配件入库计划
                        nPartsInboundPlanid := s_PartsInboundPlan.Nextval;
                        vPartsInboundPlanCode := regexp_replace(regexp_replace(vInboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateInboundPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-9 hr modify 入库计划单 对方单位 赋值有问题，不应是仓库，品牌赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值，入库计划单的源单据 应是 发运单
                          2017-10-13 hr add 入库计划单备注默认'统购分销发运'；抵达日期 不赋值，客户账户Id不赋值*/
                        insert into PartsInboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,
                          sourceid,sourcecode,inboundtype,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,arrivaldate,creatorid,creatorname,createtime,IfWmsInterface,Remark)
                        values(nPartsInboundPlanid,vPartsInboundPlanCode,nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytypeG,ibranchid,vbranchcode,vbranchname,iSalesCategoryIdG/*nPartssalescategoryid*//*ipartssalesordertypeid*/,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,/*nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,*/
                        nPartsShippingOrderid/*PartCheckId*/,vPartsShippingOrderCode/*vCode*/,3,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,2,null/*sysdate*/,nCreatorID,vCreatorName,sysdate,0,'统购分销发运');
                        ---inboundtype:3 配件调拨

                        ---PartsInboundCheckBill配件入库检验单
                        nPartsInboundCheckBillid := s_PartsInboundCheckBill.Nextval;
                        vPartsInboundCheckBillCode := regexp_replace(regexp_replace(vInboundCheckBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateInboundCheckBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-10 hr modify 入库检验单 仓库 仓储企业 品牌 对方单位 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-16 hr modify 客户账户Id不赋值，状态是 3-上架完成*/
                        insert into PartsInboundCheckBill(id,code,partsinboundplanid,warehouseid, warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,partssalescategoryid,branchid,branchcode,branchname,
                          counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,inboundtype,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,settlementstatus,creatorid,creatorname,createtime)
                        values (nPartsInboundCheckBillid,vPartsInboundCheckBillCode,nPartsInboundPlanid,nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytypeG,iSalesCategoryIdG/*nPartssalescategoryid*/,ibranchid,vbranchcode,vbranchname,
                        iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,/*iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,*/3,null/*ncustomeraccountid*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,3/*1*/,2,nCreatorID,vCreatorName,sysdate);
                        ---inboundtype:3 配件调拨. settlementstatus=2 待结算

                        ---PartsShiftOrder配件移库单
                        nPartsShiftOrderid := s_PartsShiftOrder.Nextval;
                        nPartsShiftOrderidS := nPartsShiftOrderid;
                        vPartsShiftOrderCode := regexp_replace(regexp_replace(vShiftOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateShiftOrderId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        insert into  PartsShiftOrder(id,code,branchid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,status,type,creatorid,creatorname,createtime)
                        values(nPartsShiftOrderid,vPartsShiftOrderCode,ibranchid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,nStoragecompanytype,1,2,nCreatorID,vCreatorName,sysdate);


                        ---PartsShippingOrderRef配件发运单关联单
                        insert into PartsShippingOrderRef(Id,PartsShippingOrderId,PartsOutboundBillId)
                        values(s_PartsShippingOrderRef.Nextval,nPartsShippingOrderid,nPartsOutboundBillId);

                        ----仓库PartsOutboundPlan配件出库计划 （第二出库仓库）
                        nPartsoutboundplanidG := s_PartsOutboundPlan.Nextval;
                        vPartsOutboundPlanCodeG := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 出库计划单收货仓库id，编号，名称 需要赋值
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                          2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface,Remark,Companyaddressid)
                        values(nPartsoutboundplanidG,vPartsOutboundPlanCodeG,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdG/*iSalesCategoryId*/,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,  iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                          vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                          sysdate,0,vRemark,vCompanyAddressId);

                        /*values(nPartsoutboundplanid,vPartsOutboundPlanCode,iWarehouseidP,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,nPartssalescategoryid,
                        vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                        vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);*/

                        nPartsTransferOrderidS := nPartsTransferOrderid;
                        nPartsoutboundplanidS := nPartsoutboundplanid;
                        nPartsOutboundBillIdS := nPartsOutboundBillId;
                        nPartsInboundPlanidS := nPartsInboundPlanid;
                        nPartsInboundCheckBillidS := nPartsInboundCheckBillid;
                        nPartsoutboundplanidGS := nPartsoutboundplanidG;
                        nPartsshippingorderidS := nPartsshippingorderid;
                        nPartsshiftorderidS := nPartsshiftorderid;

                        bInsertOutboundPlanST := true;
                      end;
                      end if;


                      --第二仓库此次满足数量和金额
                      /*2017-9-3 hr modify 第二仓库统购分销完全满足 if判断条件 与赋值 和 首选，第一，第三仓库不一致*/
                      --if nMaxQuantityS <= (nSumQuantity-nvl(nSumLockedQuantity,0)) then
                      if nMaxQuantityS <= nWRemainQuantity then
                      begin
                         mCurrentFulfilledAmountS:=nMaxQuantityS*nvl(nOrderPrice,0);
                         nLockedQuantityS := nMaxQuantityS;
                      end;
                      else
                      begin
                         --mCurrentFulfilledAmountS:=(nSumQuantity-nvl(nSumLockedQuantity,0))*nvl(nOrderPrice,0);
                         mCurrentFulfilledAmountS:=nWRemainQuantity*nvl(nOrderPrice,0);
                         nLockedQuantityS := nWRemainQuantity;
                      end;
                      end if;


                      ---PartsTransferOrderDetail配件调拨单清单
                      ---  remark,
                      insert into tempPartsTransferOrderDetail(id,partstransferorderid,sparepartid,sparepartcode,sparepartname,plannedamount,confirmedamount,price,plannprice)
                      /*2017-9-3 hr modify nPartsTransferOrderid会取最近一个新配件调拨单的ID
                        2017-10-10 hr modify 调拨单清单的计划价 取 配件计划价，品牌取调拨入库仓库的品牌*/
                      values(s_PartsTransferOrderDetail.Nextval,nPartsTransferOrderidS/*nPartsTransferOrderid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityS,nLockedQuantityS,nvl(nOrderPrice,0),/*nvl(nOrderPrice,0)*/
                      (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG/*iSalesCategoryId*/));

                      ---PartsOutboundPlanDetail配件出库计划清单 首选仓库
                      insert into tempPartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price)
                      /*2017-9-3 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                      values(s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidS/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityS,nLockedQuantityS,nvl(nOrderPrice,0));


                      ---PartsInboundPlanDetail配件入库计划清单
                      insert into tempPartsInboundPlanDetail(id,partsinboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,inspectedquantity,price)
                      /*2017-9-3 hr modify nPartsInboundPlanid会取最近一个新配件入库计划单的ID*/
                      values(s_PartsInboundPlanDetail.Nextval,nPartsInboundPlanidS/*nPartsinboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityS,nLockedQuantityS,nvl(nOrderPrice,0));

                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,匹配仓库，这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,nWRemainQuantity,nvl(nOrderPrice,0),1,nOrderProcessMethod,sysdate,nCreatorId,vCreatorName);

                      /*2017-8-7 hr add*/
                      --统购仓库配件出库单清单（tempPartsOutboundBillDetail）记录
                      /*2017-9-3 hr modify nPartsOutboundBillId会取最近一个新配件出库单的ID
                        2017-10-11 hr modify 品牌取调拨出库仓库的品牌
                        2017-10-17 hr modify 出库清单的库位ID与编号赋值有问题*/
                      insert into tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillIdS/*nPartsOutboundBillId*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityS,nWarehouseAreaIdS,nWarehouseAreaCodeS,/*nPublicwarehouseid,vWareHouseCodeP,*/nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdP/*iSalesCategoryId*/));


                     /*
                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,匹配仓库，这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,nWRemainQuantity,nvl(nOrderPrice,0),2,nOrderProcessMethod,sysdate,nCreatorId,vCreatorName);


                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,(nSumQuantity-nvl(nSumLockedQuantity,0)),nvl(nOrderPrice,0),2,2,sysdate,nCreatorId,vCreatorName);
                      */

                      /*
                      --第二仓库此次满足金额
                      mCurrentFulfilledAmountP:=nWRemainQuantity*nvl(nOrderPrice,0);

                      ----res:=mCurrentFulfilledAmountP;
                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        if nQuantity-nvl(nLockedQuantity,0)>0 then --此库区可用数量
                        begin

                          if nQuantity-nvl(nLockedQuantity,0)>= nWRemainQuantity then
                          begin

                            insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                            values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,nWRemainQuantity,nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));
                            nWRemainQuantity:= 0;
                            exit;
                          end;
                          else
                          begin
                            --统购仓库配件出库单清单（tempPartsOutboundBillDetail）记录
                            insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                            values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityS,nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                              (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));

                              nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --剩下没满足数量
                          end;
                          ---nWRemainQuantity1:=nWRemainQuantity;
                          end if;
                        end;
                        end if;

                      end loop;
                      */

                      ---根据第二出库仓库ID和配件ID，检验区
                      ---select WarehouseAreaId,Quantity into iWarehouseAreaIdC,iQuantityC from PartsStock where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaCategoryId=3 and StorageCompanyId=iStorageCompanyId and BranchId=iBranchId and rownum = 1;
                      select a.id,a.Code into iWarehouseAreaIdC,vCodeC from WarehouseArea a, WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.AreaCategoryId=b.id and b.category=3 and rownum=1 and a.areakind=3;
                      if iWarehouseAreaIdC>0 then
                      begin

                        ---根据首选出库仓库ID和配件ID，保管区
                        ---select WarehouseAreaId,Quantity into iWarehouseAreaIdS,nQuantityS from PartsStock a where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and StorageCompanyId=nStorageCompanyId and BranchId=iBranchId and PartId=iSparePartId and rownum = 1;
                        select count(*) into iCount from PartsStock a,WarehouseAreaCategory b where  a.WarehouseId=nPublicwarehouseidG and a.WarehouseAreaCategoryId=b.id and b.category=1 and a.StorageCompanyId=iStoragecompanyidG and a.BranchId=iBranchId and a.PartId=iSparePartId;
                        --if iWarehouseAreaIdS>0 then
                        if iCount>0 then
                        begin
                          ---PartsShiftOrderDetail配件移库单清单
                          ---,batchnumber,remark
                          select a.WarehouseAreaId,a.Quantity,b.id,a.id into iWarehouseAreaIdS,nQuantityS,nWarehouseareacategoryidS,nPartsStockidS from PartsStock a,WarehouseAreaCategory b where a.partid=iSparePartId and a.WarehouseId=nPublicwarehouseidG and a.WarehouseAreaCategoryId=b.id and b.category=1 and a.StorageCompanyId=iStoragecompanyidG and a.BranchId=iBranchId and a.PartId=iSparePartId and rownum = 1;
                          insert into tempPartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
                          /*2017-9-3 hr modify nPartsshiftorderid会取最近一个新配件移库单的ID*/
                          values(s_PartsShiftOrderDetail.Nextval,nPartsshiftorderidS/*nPartsshiftorderid*/,iSparePartId,vSparepartcode,vSparepartname,iWarehouseAreaIdC,vCodeC,3,
                          iWarehouseAreaIdS,(select code from WarehouseArea where WarehouseId=nPublicwarehouseidG and id=iWarehouseAreaIdC),1,nLockedQuantityS);

                          --update PartsStock set Quantity=Quantity+nLockedQuantityD where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaId=iWarehouseAreaIdS and WarehouseAreaCategoryId=（select id from WarehouseAreaCategory where category=3 ） and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          update PartsStock set Quantity=Quantity+nLockedQuantityS where id=nPartsStockidS;

                          ---统购分销仓库减库存
                          update PartsStock set Quantity=Quantity-nLockedQuantityS where id=nTPartsStockidS;

                          select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          if iPartsLockedStock>0 then
                              ---锁定库存
                            update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityS,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          else
                            insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                            values(s_PartsLockedStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,iBranchId,iSparePartId,nLockedQuantityS,nCreatorId,vCreatorName,sysdate);
                          end if;

                          ---锁定库存
                          --update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityS where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          /*2017-8-7 hr add*/
                          iCheckOrderDetailSum  := iCheckOrderDetailSum +1;

                        end;
                       else
                        begin
                          ---select TopLevelWarehouseAreaId,code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea where WarehouseId=nPublicwarehouseidG and AreaCategoryId=1 and TopLevelWarehouseAreaId not in(
                          ---select a.\*TopLevelWarehouseAreaId*\,a.code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and a.\*TopLevelWarehouseAreaId*\ not in(
                          ---select WarehouseAreaId from PartsStock where  WarehouseId=nPublicwarehouseidG ) and  rownum = 1;

                          select a.id,a.code,b.id into iWarehouseAreaIdS,vWarehouseAreaCodeS,nWarehouseareacategoryidS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and rownum = 1;
                          --select a.TopLevelWarehouseAreaId,a.code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nDefaultoutwarehouseid and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and a.TopLevelWarehouseAreaId not in(
                          --select WarehouseAreaId from PartsStock where  WarehouseId=nDefaultoutwarehouseid ) and  rownum = 1;
                          if iWarehouseAreaIdS>0 then
                          begin
                            ---PartsShiftOrderDetail配件移库单清单
                            ---,batchnumber,remark
                            insert into tempPartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
                            /*2017-9-3 hr modify nPartsshiftorderid会取最近一个新配件移库单的ID*/
                            values(s_PartsShiftOrderDetail.Nextval,nPartsshiftorderidS/*nPartsshiftorderid*/,iSparePartId,vSparepartcode,vSparepartname,iWarehouseAreaIdC,vCodeC,3,
                              iWarehouseAreaIdS,(select code from WarehouseArea where WarehouseId=nPublicwarehouseidG and id=iWarehouseAreaIdC),nWarehouseareacategoryidS,nLockedQuantityS);

                            insert into PartsStock(id,warehouseid,storagecompanyid,storagecompanytype,branchid,warehouseareaid,warehouseareacategoryid,partid,quantity,creatorid,creatorname,createtime)
                            values(s_PartsStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,nstoragecompanytype,iBranchId,iWarehouseAreaIdS,nWarehouseareacategoryidS,iSparePartId,nLockedQuantityS,nCreatorId,vCreatorName,sysdate);
                            ---update PartsStock set Quantity:=Quantity+(nSumQuantity-nvl(nSumLockedQuantity,0)) where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaId=iWarehouseAreaIdS and WarehouseAreaCategoryId=3 and StorageCompanyId=iStorageCompanyId and BranchId=iBranchId;

                            ---统购分销仓库减库存
                            update PartsStock set Quantity=Quantity-nLockedQuantityS where id=nTPartsStockidS;

                            select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            if iPartsLockedStock>0 then
                              ---锁定库存
                              update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityS,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            else
                              insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                              values(s_PartsLockedStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,iBranchId,iSparePartId,nLockedQuantityS,nCreatorId,vCreatorName,sysdate);
                            end if;

                            ---锁定库存
                            --update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityS where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                          end;

                          end if;
                        end;
                        end if;

                      end;
                      end if;

                      ---PartsInboundCheckBillDetail配件入库检验单清单
                      /*2017-9-3 hr modify nPartsInboundCheckBillid会取最近一个新配件入库检验单的ID
                        2017-10-10 hr modify 入库检验单清单的成本价 取 配件计划价，品牌取调拨入库仓库的品牌
                        2017-10-17 hr modify 入库检验单清单的库位ID与编号赋值有问题，不应是 检验区的库位*/
                      insert into tPartsInboundCheckBillDetail(id,partsinboundcheckbillid,sparepartid,sparepartcode,sparepartname,warehouseareaid,warehouseareacode,inspectedquantity,settlementprice,costprice)
                      values (s_PartsInboundCheckBillDetail.Nextval,nPartsInboundCheckBillidS/*nPartsinboundcheckbillid*/,iSparePartId,vSparepartcode,vSparepartname,/*iWarehouseAreaIdC,vCodeC*/
                      iWarehouseAreaIdS,(select code from WarehouseArea where id =iWarehouseAreaIdS and areakind=3),nLockedQuantityS,nvl(nOrderPrice,0),/*nvl(nOrderPrice,0)*/
                      (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG/*iSalesCategoryId*/));

                      ---PartsShippingOrderDetail配件发运单清单
                      ---differenceclassification
                      insert into tempPartsShippingOrderDetail(id,partsshippingorderid,sparepartid,sparepartcode,sparepartname,shippingamount,confirmedamount,settlementprice)
                      /*2017-9-3 hr modify nPartsshippingorderidD会取最近一个新配件发运单的ID*/
                      values(s_PartsShippingOrderDetail.Nextval,nPartsshippingorderidS/*nPartsshippingorderid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityS,nLockedQuantityS,nvl(nOrderPrice,0));

                      ---PartsOutboundPlanDetail配件出库计划清单
                      insert into tempPartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,/*outboundfulfillment,*/price)
                      /*2017-9-3 hr modify nPartsoutboundplanidG会取最近一个新出库计划单的ID*/
                      values(s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidGS/*nPartsoutboundplanidG*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityS,/*nLockedQuantityS,*/nvl(nOrderPrice,0));

                      /*2017-8-7 hr add 剩余未满足量未赋值*/
                      nWRemainQuantity := nWRemainQuantity - nLockedQuantityS;--剩余未满足量

                      /*
                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and rid=j;

                        --select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        select count(*) into nTest from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        if nvl(nTest,0)>0 then
                           select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --else
                          -- nLockedQuantity:=0;
                        end if;
                        if nQuantity-nvl(nLockedQuantity,0)>0 then --此库区可用数量
                        begin

                          if (nQuantity-nvl(nLockedQuantity,0)) >nWRemainQuantity then
                          begin
                            update PARTSSTOCK set Quantity=nQuantity-nWRemainQuantity where WarehouseAreaId=nWarehouseAreaId and  WarehouseId=iWarehouseidP  and PartId=iSparePartId;
                            nWRemainQuantity:= 0; --此库区剩下没满足数量,此次已经完全满足

                          end;
                          else
                          begin
                            update PARTSSTOCK set Quantity=nLockedQuantity where WarehouseAreaId=nWarehouseAreaId and  WarehouseId=iWarehouseidP  and PartId=iSparePartId;
                            nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --此库区剩下没满足数量

                           end;
                           end if;

                          nWRemainQuantity1:=nWRemainQuantity;

                          exit when nWRemainQuantity= 0;
                        end;
                        end if;

                      end loop;
                      */

                    end;
                    end if;
                  end;
                  else ---不是统购分销仓库
                  begin

                    nOrderProcessMethod:=2;

                    select Code,Name,storagecompanyid,storagecompanytype into vWareHouseCodeP,vWareHouseNameP,iStoragecompanyidP,nStoragecompanytype from warehouse where id=nPublicwarehouseid;
                    select code,name into vStoragecompanycodeP,vStoragecompanynameP from company where id=iStoragecompanyidP;



                    -- select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from PartsStock where
                    insert into tempPARTSSTOCK  select rownum,a.* from PARTSSTOCK a  where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1;
                    select sum(quantity) into nSumQuantity from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                    select sum(LockedQuantity) into nSumLockedQuantity from Partslockedstock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                    /*2017-8-3 hr add*/
                    insert into tempPARTSLockedSTOCK select id,warehouseid,storagecompanyid,branchid,partid,lockedquantity,creatorid,creatorname,createtime,modifierid,modifiername,modifytime from Partslockedstock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;

                    select count(*) into iPS from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;

                    if (nSumQuantity-nvl(nSumLockedQuantity,0))< nWRemainQuantity1 and (nSumQuantity-nvl(nSumLockedQuantity,0)) > 0 then ---第二仓库部分满足
                    begin
                      nNewLockedQuantity:=nSumQuantity-nvl(nSumLockedQuantity,0);
                    /*
                      ---新建配件出库单
                      ---creatorid,creatorname,createtime,获得
                      --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                      vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                      nPartsOutboundBillId := s_PartsOutboundBill.Nextval;

                      insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                        partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,receivingcompanyid,receivingcompanycode, receivingcompanyname,receivingwarehouseid,
                        receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                        --orderapprovecomment,remark,
                        creatorid,creatorname,createtime)

                      Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,
                        iPartssalesordertypeid,vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingwarehouseid,
                        vReceivingwarehousecode,vReceivingWarehouseName,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);
                      */
                      ---新建出库计划单
                      if bInsertOutboundPlanS=false then ---如果此次审核首选仓库已经生成出库计划主单就不再生成
                      begin
                        nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                        nPartsoutboundplanidS :=nPartsoutboundplanid;
                        vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 出库计划单收货仓库id，编号，名称 需要赋值
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                          2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface,Remark,Companyaddressid)
                        values(nPartsoutboundplanid,vPartsOutboundPlanCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                          vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                          sysdate,0,vRemark,vCompanyAddressId);
                        bInsertOutboundPlanS:=true;
                      end;
                      end if;

                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,仓库，这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,(nSumQuantity-nvl(nSumLockedQuantity,0)),nvl(nOrderPrice,0),2,nOrderProcessMethod,sysdate,nCreatorId,vCreatorName);


                      ---新建出库计划清单
                      insert into tempPartsOutboundPlanDetail (id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price)
                      /*2017-8-3 hr modify nQuantity没有被清空，使用上一个循环中的值*/
                      /*2017-9-2 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                      values (s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidS/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,/*(nQuantity-nvl(nLockedQuantity,0))*/(nSumQuantity-nvl(nSumLockedQuantity,0)),nOrderPrice);

                      --第二仓库此次满足金额
                      mCurrentFulfilledAmountP:=(nSumQuantity-nvl(nSumLockedQuantity,0))*nvl(nOrderPrice,0);

                      /*2017-8-4 hr modify 配件多库位循环赋值剩余未满足量有问题，当锁定库存大于单个库位库存时，剩余未满足量将不会赋值*/
                      nWRemainQuantity:= nWRemainQuantity-(nSumQuantity-nvl(nSumLockedQuantity,0));
                      /*
                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        --select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        select count(*) into nTest from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        if nvl(nTest,0)>0 then
                           select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --else
                          -- nLockedQuantity:=0;
                        end if;
                        if nQuantity-nvl(nLockedQuantity,0)>0 then --第二仓库库区可用数量
                        begin
                          ---update PARTSSTOCK set Quantity=nQuantity-nWRemainQuantity where WarehouseAreaId=nWarehouseAreaId and  WarehouseId=iWarehouseidP  and PartId=iSparePartId;
                          nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --剩下没满足数量
                          ---nWRemainQuantity1:=nWRemainQuantity;
                          \*
                          --配件出库单清单（tempPartsOutboundBillDetail）记录
                          insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,(nQuantity-nvl(nLockedQuantity,0)),nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));
                          *\
                        end;
                        end if;

                      end loop;
                      */
                      ---锁定库存
                      select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      if iPartsLockedStock>0 then
                        update PartsLockedStock set LockedQuantity=LockedQuantity+nNewLockedQuantity,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      else
                        insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                        values(s_PartsLockedStock.Nextval,nPublicwarehouseid,iStoragecompanyidP,iBranchId,iSparePartId,nNewLockedQuantity,nCreatorId,vCreatorName,sysdate);
                      end if;
                      ---锁定库存
                      --update PARTSLockedSTOCK set LockedQuantity=LockedQuantity+nNewLockedQuantity where WarehouseId=nPublicwarehouseid  and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;

                      ---nWRemainQuantity:= nWRemainQuantity - (nSumQuantity-nvl(nSumLockedQuantity,0)); --剩下没满足数
                      nWRemainQuantity1:=nWRemainQuantity;
                      /*2017-8-15 hr add 变量iCheckOrderDetailSum 没有赋值*/
                      iCheckOrderDetailSum  := iCheckOrderDetailSum +1;

                    end;
                    elsif (nSumQuantity-nvl(nSumLockedQuantity,0)) >= nWRemainQuantity then
                      ---第二仓库完全满足
                    begin
                      nNewLockedQuantity:=nWRemainQuantity; ---需要锁定库存
                      /*
                      --新建配件出库单
                      ---creatorid,creatorname,createtime,获得
                      --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                      vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                      nPartsOutboundBillId := s_PartsOutboundBill.Nextval;

                      insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                        partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,receivingcompanyid,receivingcompanycode, receivingcompanyname,receivingwarehouseid,
                        receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                        --orderapprovecomment,remark,
                        creatorid,creatorname,createtime)

                      Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,
                        iPartssalesordertypeid,vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingwarehouseid,
                        vReceivingwarehousecode,vReceivingWarehouseName,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);
                      */
                      ---新建出库计划单
                      /*2017-8-3 hr modify 第二仓库的出库计划是否生成的变量修改*/
                      if bInsertOutboundPlanS/*bInsertOutboundPlanD*/=false then ---如果此次审首选仓库已经生成出库计划主单就不再生成
                      begin
                        nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                        nPartsoutboundplanidS :=nPartsoutboundplanid;
                        vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 出库计划单收货仓库id，编号，名称 需要赋值
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                          2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface,Remark,Companyaddressid)
                        values(nPartsoutboundplanid,vPartsOutboundPlanCode,nPublicwarehouseid ,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                          vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                          sysdate,0,vRemark,vCompanyAddressId);
                        bInsertOutboundPlanS/*bInsertOutboundPlanD*/:=true;/*2017-8-3 hr modify 第二仓库的出库计划是否生成的变量修改*/
                      end;
                      end if;

                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,nWRemainQuantity,nvl(nOrderPrice,0),1,2,sysdate,nCreatorId,vCreatorName);

                      ---新建出库计划清单
                      insert into tempPartsOutboundPlanDetail (id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price)
                      /*2017-9-2 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                      values (s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidS/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nWRemainQuantity,nOrderPrice);
                      --第二仓库此次满足金额
                      mCurrentFulfilledAmountP:=nWRemainQuantity*nvl(nOrderPrice,0);

                      /*2017-8-4 hr modify 配件多库位循环赋值剩余未满足量有问题，当锁定库存大于单个库位库存时，剩余未满足量将不会赋值*/
                      nWRemainQuantity:= 0;
                      nWRemainQuantity1:=nWRemainQuantity;
                      /*
                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        --select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        select count(*) into nTest from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        if nvl(nTest,0)>0 then
                           select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --else
                          -- nLockedQuantity:=0;
                        end if;
                        if nQuantity-nvl(nLockedQuantity,0)>0 then --此库区可用数量
                        begin

                          if (nQuantity-nvl(nLockedQuantity,0)) >nWRemainQuantity then
                          begin
                            nWRemainQuantity:= 0; --此库区剩下没满足数量,此次已经完全满足

                          end;
                          else
                          begin
                            nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --此库区剩下没满足数量

                           end;
                           end if;

                          nWRemainQuantity1:=nWRemainQuantity;

                          exit when nWRemainQuantity= 0;
                        end;
                        end if;

                      end loop;
                      */
                      ---锁定库存
                      select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      if iPartsLockedStock>0 then
                        update PartsLockedStock set LockedQuantity=LockedQuantity+nNewLockedQuantity,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      else
                        insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                        values(s_PartsLockedStock.Nextval,nPublicwarehouseid,iStoragecompanyidP,iBranchId,iSparePartId,nNewLockedQuantity,nCreatorId,vCreatorName,sysdate);
                      end if;
                      ---锁定库存
                      --update PARTSLockedSTOCK set LockedQuantity=LockedQuantity+nNewLockedQuantity where WarehouseId=nPublicwarehouseid  and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                    end;
                    end if;


                  end;
                  end if;
                end;
                end if;  ---第二仓库

----------------第三仓库

                if  nWRemainQuantity>0 and nThirdwarehouseid>0  then
                begin
                  --res:='第三仓库';
                  nPublicwarehouseid:=nThirdwarehouseid;
                  ---判断此仓库是否为统购分销仓库
                  select IsCentralizedPurchase into bIsCentralizedPurchase from warehouse where id=nPublicwarehouseid;

                  if  bIsCentralizedPurchase=1 then
                  begin
                    nPublicwarehouseidG:=nThirdoutwarehouseid;
                    nOrderProcessMethod:=10;

                    select Code,Name,storagecompanyid,storagecompanytype into vWareHouseCodeG,vWareHouseNameG,iStoragecompanyidG,nStoragecompanytypeG from warehouse where id=nPublicwarehouseidG;
                    select code,name into vStoragecompanycodeG,vStoragecompanynameG from company where id=iStoragecompanyidG;
                    /*2017-10-11 hr add 增加入库品牌取值*/
                    begin
                      select b.partssalescategoryid,c.name into iSalesCategoryIdG,vSalesCategoryNameG from salesunitaffiwarehouse a left join salesunit b on a.salesunitid = b.id left join partssalescategory c on b.partssalescategoryid = c.id where a.warehouseid = nPublicwarehouseidG;
                    exception
                    when no_data_found then
                      begin
                        vAutoapprovecommentG:='统购分销仓库'||vWareHouseCodeG||'未维护销售组织';
                        raise FMyException;
                      end;
                    end;

                    select Code,Name,storagecompanyid,storagecompanytype into vWareHouseCodeP,vWareHouseNameP,iStoragecompanyidP,nStoragecompanytype from warehouse where id=nPublicwarehouseid;
                    select code,name into vStoragecompanycodeP,vStoragecompanynameP from company where id=iStoragecompanyidP;
                    /*2017-10-11 hr add 增加调拨出库发运品牌取值*/
                    begin
                      select b.partssalescategoryid,c.name into iSalesCategoryIdP,vSalesCategoryNameP from salesunitaffiwarehouse a left join salesunit b on a.salesunitid = b.id left join partssalescategory c on b.partssalescategoryid = c.id where a.warehouseid = nPublicwarehouseid;
                    exception
                    when no_data_found then
                      begin
                        vAutoapprovecommentG:='统购分销仓库'||vWareHouseCodeP||'未维护销售组织';
                        raise FMyException;
                      end;
                    end;


                    -- select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from PartsStock where
                    insert into tempPARTSSTOCK  select rownum,a.* from PARTSSTOCK a  where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1;
                    ---insert into tempPARTSSTOCK  select rownum,a.* from PARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1
                     ---and (quantity=(select max(quantity) from PARTSSTOCK where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId));

                    select sum(quantity) into nSumQuantity from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;

                    --select max(nvl(quantity,0)) into nMaxQuantityD from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                    --select id into nTPartsStockidD from tempPARTSSTOCK a  where quantity=nMaxQuantityD and rownum=1 and WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;

                    select max(nvl(quantity,0)) into nMaxQuantityT from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                    /*2017-8-7 hr modfiy 当库存不存在时，查不到统购分销仓库最大库存库区ID*/
                    begin
                    ---获取统购分销仓库最大库存库区ID
                      select id,WarehouseAreaId into nTPartsStockidT,nWarehouseAreaIdT from tempPARTSSTOCK a  where  quantity=nMaxQuantityT and rownum=1 and WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                    exception
                      when no_data_found then
                        nTPartsStockidT := 0;
                    end;
                    /*2017-10-17 hr add 获取调拨出库仓库的库位*/
                    begin
                      select code into nWarehouseAreaCodeT from WarehouseArea a  where a.id = nWarehouseAreaIdT and a.areakind = 3;
                    exception
                      when no_data_found then
                        nWarehouseAreaCodeT := 0;
                    end;

                    select sum(LockedQuantity) into nSumLockedQuantity from Partslockedstock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                    select count(*) into iPS from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;

                    if (nSumQuantity-nvl(nSumLockedQuantity,0))< nWRemainQuantity1 and (nSumQuantity-nvl(nSumLockedQuantity,0)) > 0 then ---第三仓库部分满足
                    begin

                      /*2017-10-11 hr add 对出入库计划价进行校验
                      1）计划价必须存在，且>0
                      2）出入库仓库的计划价必须一致*/
                      begin
                        select PlannedPrice into nPlannedPriceG from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG;
                      exception
                      when no_data_found then
                        begin
                          vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameG||']没有计划价';
                          raise FMyException;
                        end;
                      end;
                      if nPlannedPriceG = 0 then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameG||']的计划价为0';
                        raise FMyException;
                      end;
                      end if;
                      begin
                        select PlannedPrice into nPlannedPriceP from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdP;
                      exception
                      when no_data_found then
                        begin
                          vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']没有计划价';
                          raise FMyException;
                        end;
                      end;
                      if nPlannedPriceP = 0 then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']的计划价为0';
                        raise FMyException;
                      end;
                      end if;
                      if nPlannedPriceP <> nPlannedPriceG then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']的计划价与品牌['||vSalesCategoryNameG||']的计划价不一致';
                        raise FMyException;
                      end;
                      end if;


                      if bInsertOutboundPlanTT = false then
                      begin
                        ---收货单位Id  ReceivingCompanyId,收货单位编号  ReceivingCompanyCode,收货单位名称  ReceivingCompanyName 填为  nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG


                        ---PartsTransferOrder配件调拨单
                        nPartsTransferOrderid := s_PartsTransferOrder.Nextval;
                        vPartsTransferOrderCode := regexp_replace(regexp_replace(vTransferOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateTransferOrderId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-8-7 hr modify 调拨单状态为 已审批
                          2017-10-12 hr modify 调拨单的调拨类型 应该是 3--急需调拨；发运方式设置为空
                          2017-10-13 hr add 修改人，审批人，修改时间，审批时间赋值*/
                        insert into PartsTransferOrder (id,code,originalwarehouseid,originalwarehousecode,originalwarehousename,storagecompanyid,storagecompanytype,destwarehouseid,destwarehousecode,destwarehousename,status,type,
                          shippingmethod,creatorid,creatorname,createtime,originalbillid,originalbillcode,totalamount,Modifierid,Modifiername,Modifytime,Approverid,Approvername,Approvetime)
                        values(nPartsTransferOrderid,vPartsTransferOrderCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,nStoragecompanytype,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,/*1*/2,/*1*/3,
                        null/*iShippingMethod*/,nCreatorID,vCreatorName,sysdate,PartCheckId,vCode,0,nCreatorID,vCreatorName,sysdate,nCreatorID,vCreatorName,sysdate);
                        --- totalamount,totalplanamount需要计算。

                        ----PartsOutboundPlan配件出库计划 (第三仓库)
                        nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                        vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值，出库计划单的源单据 应是 调拨单
                          2017-10-12 hr modify 调拨出库生成的出库计划单的出库类型 应是 调拨出库；对方单位赋值有问题
                          2017-10-16 hr modify 发运方式设置为空，客户账户Id默认null*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface )
                        values(nPartsoutboundplanid,vPartsOutboundPlanCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdP/*iSalesCategoryId*/,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPartsTransferOrderid/*PartCheckId*/,
                          vPartsTransferOrderCode/*vCode*/,4/*1*/,null/*iShippingMethod*/,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,3,nCreatorID,vCreatorName,sysdate,0);

                        ---新建配件出库单
                        --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                        vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        nPartsOutboundBillId := s_PartsOutboundBill.Nextval;
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-13 hr modify 调拨出库生成的出库单的出库类型 应是 调拨出库 配件调拨；备注是'统购分销出库'；结算状态是 待结算；对方单位赋值有问题
                          2017-10-16 hr modify 发运方式设置为空，客户账户Id默认null*/
                        insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                          partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,
                          receivingwarehouseid,receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                          creatorid,creatorname,createtime,Remark)
                        Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdP/*iSalesCategoryId*/,
                          iPartssalesordertypeid,vPartssalesordertypename,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,
                          nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,4/*1*/,null/*iShippingMethod*/,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,2/*1*/,
                          nCreatorID,vCreatorName,sysdate,'统购分销出库');


                        ---PartsShippingOrder配件发运单  /*入库计划单的源单据是 发运单，所以调整发运单生成的位置，放到入库计划单生成前*/
                        nPartsShippingOrderid := s_PartsShippingOrder.Nextval;
                        /*2017-10-12 hr modify 发运单应是4位流水号*/
                        vPartsShippingOrderCode := regexp_replace(regexp_replace(vShippingOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateShippingOrderId,TRUNC(sysdate,'DD'),vBranchCode)),4/*6*/,'0'));
                        /*2017-9-6 hr modify 发运单的发货单位与结算单位赋值有问题，状态不应是新建，应该是 回执确认
                          2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 配件发运单增加品牌字段赋值
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-12 hr add 发运单备注默认'统购分销发运'
                          2017-10-13 hr add 发运方式默认 8--客户自行运输；发运日期 不赋值；收货单位赋值有问题；预计到货日期 不赋值*/
                        insert into PartsShippingOrder(id,code,type,branchid,shippingcompanyid,shippingcompanycode,shippingcompanyname,settlementcompanyid,settlementcompanycode,settlementcompanyname,receivingcompanyid,receivingcompanycode,invoicereceivesalecateid,invoicereceivesalecatename,receivingcompanyname, warehouseid,warehousecode,warehousename,
                          receivingwarehouseid,receivingwarehousecode,receivingwarehousename,originalrequirementbillid,originalrequirementbillcode,originalrequirementbilltype,shippingdate,status,creatorid,creatorname,createtime,expectedplacedate,PartsSalesCategoryId,Remark,Shippingmethod)
                        values(nPartsShippingOrderid,vPartsShippingOrderCode,4,ibranchid,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,*/iStoragecompanyidP,vStoragecompanycodeP,/*nPublicwarehouseidG,vWareHouseCodeG ,*/iPartssalesordertypeid,vPartsSalesOrderTypeName,vStoragecompanynameP/*vWareHouseNameP*/,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,
                        nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,PartCheckId,vCode,1/*nPartssalescategoryid*/,null/*sysdate*/,/*1*/3,nCreatorID,vCreatorName,sysdate,null/*sysdate*/,iSalesCategoryIdP/*iSalesCategoryId*/,'统购分销发运',8);


                        ---PartsInboundPlan配件入库计划
                        nPartsInboundPlanid := s_PartsInboundPlan.Nextval;
                        vPartsInboundPlanCode := regexp_replace(regexp_replace(vInboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateInboundPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-8-7 hr modify PartsInboundPlan 插入时IfWmsInterface 不可为空
                          2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-9 hr modify 入库计划单 对方单位 赋值有问题，不应是仓库，品牌赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值，入库计划单的源单据 应是 发运单
                          2017-10-13 hr add 入库计划单备注默认'统购分销发运'；抵达日期 不赋值，客户账户Id不赋值*/
                        insert into PartsInboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,
                          sourceid,sourcecode,inboundtype,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,arrivaldate,creatorid,creatorname,createtime,IfWmsInterface,Remark)
                        values(nPartsInboundPlanid,vPartsInboundPlanCode,nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytypeG,ibranchid,vbranchcode,vbranchname,iSalesCategoryIdG/*nPartssalescategoryid*//*ipartssalesordertypeid*/,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,/*nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,*/
                        nPartsShippingOrderid/*PartCheckId*/,vPartsShippingOrderCode/*vCode*/,3,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,2,null/*sysdate*/,nCreatorID,vCreatorName,sysdate,0,'统购分销发运');
                        ---inboundtype:3 配件调拨

                        ---PartsInboundCheckBill配件入库检验单
                        nPartsInboundCheckBillid := s_PartsInboundCheckBill.Nextval;
                        vPartsInboundCheckBillCode := regexp_replace(regexp_replace(vInboundCheckBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateInboundCheckBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-10 hr modify 入库检验单 仓库 仓储企业 品牌 对方单位 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-16 hr modify 客户账户Id不赋值，状态是 3-上架完成*/
                        insert into PartsInboundCheckBill(id,code,partsinboundplanid,warehouseid, warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,partssalescategoryid,branchid,branchcode,branchname,
                          counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,inboundtype,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,settlementstatus,creatorid,creatorname,createtime)
                        values (nPartsInboundCheckBillid,vPartsInboundCheckBillCode,nPartsInboundPlanid,nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytypeG,iSalesCategoryIdG/*nPartssalescategoryid*/,ibranchid,vbranchcode,vbranchname,
                        iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,/*iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,*/3,null/*ncustomeraccountid*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,3/*1*/,2,nCreatorID,vCreatorName,sysdate);
                        ---inboundtype:3 配件调拨. settlementstatus=2 待结算

                        ---PartsShiftOrder配件移库单
                        nPartsShiftOrderid := s_PartsShiftOrder.Nextval;
                        nPartsShiftOrderidT := nPartsShiftOrderid;
                        vPartsShiftOrderCode := regexp_replace(regexp_replace(vShiftOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateShiftOrderId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        insert into  PartsShiftOrder(id,code,branchid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,status,type,creatorid,creatorname,createtime)
                        values(nPartsShiftOrderid,vPartsShiftOrderCode,ibranchid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,nStoragecompanytype,1,2,nCreatorID,vCreatorName,sysdate);


                        ---PartsShippingOrderRef配件发运单关联单
                        insert into PartsShippingOrderRef(Id,PartsShippingOrderId,PartsOutboundBillId)
                        values(s_PartsShippingOrderRef.Nextval,nPartsShippingOrderid,nPartsOutboundBillId);

                        ----仓库PartsOutboundPlan配件出库计划 （第三出库仓库）
                        nPartsoutboundplanidG := s_PartsOutboundPlan.Nextval;
                        vPartsOutboundPlanCodeG := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                         /*2017-8-7 hr modify PartsOutboundPlan 插入时RECEIVINGCOMPANYID 不可为空
                           2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                           2017-9-30 hr modify 出库计划单收货仓库id，编号，名称 需要赋值
                           2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                           2017-10-11 hr modify 品牌应根据仓库赋值
                           2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                           2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface,Remark,Companyaddressid)
                        values(nPartsoutboundplanidG,vPartsOutboundPlanCodeG,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdG/*iSalesCategoryId*/,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                          vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                          sysdate,0,vRemark,vCompanyAddressId);

                        /*values(nPartsoutboundplanid,vPartsOutboundPlanCode,iWarehouseidP,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,nPartssalescategoryid,
                        vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                        vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);*/

                        nPartsTransferOrderidT := nPartsTransferOrderid;
                        nPartsoutboundplanidT := nPartsoutboundplanid;
                        nPartsOutboundBillIdT := nPartsOutboundBillId;
                        nPartsInboundPlanidT := nPartsInboundPlanid;
                        nPartsInboundCheckBillidT := nPartsInboundCheckBillid;
                        nPartsoutboundplanidGT := nPartsoutboundplanidG;
                        nPartsshippingorderidT := nPartsshippingorderid;
                        nPartsshiftorderidT := nPartsshiftorderid;

                        bInsertOutboundPlanTT := true;
                      end;
                      end if;


                      --第三仓库此次满足数量和金额
                      if nMaxQuantityT <= (nSumQuantity-nvl(nSumLockedQuantity,0)) then
                      begin
                         mCurrentFulfilledAmountT:=nMaxQuantityT*nvl(nOrderPrice,0);
                         nLockedQuantityT := nMaxQuantityT;
                      end;
                      else
                      begin
                         mCurrentFulfilledAmountT:=(nSumQuantity-nvl(nSumLockedQuantity,0))*nvl(nOrderPrice,0);
                         nLockedQuantityT := (nSumQuantity-nvl(nSumLockedQuantity,0));
                      end;
                      end if;

                      ---PartsTransferOrderDetail配件调拨单清单
                      ---  remark,
                      insert into tempPartsTransferOrderDetail(id,partstransferorderid,sparepartid,sparepartcode,sparepartname,plannedamount,confirmedamount,price,plannprice)
                      /*2017-9-3 hr modify nPartsTransferOrderid会取最近一个新配件调拨单的ID
                        2017-10-10 hr modify 调拨单清单的计划价 取 配件计划价，品牌取调拨入库仓库的品牌*/
                      values(s_PartsTransferOrderDetail.Nextval,nPartsTransferOrderidT/*nPartsTransferOrderid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityT,nLockedQuantityT,nvl(nOrderPrice,0),/*nvl(nOrderPrice,0)*/
                      (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG/*iSalesCategoryId*/));

                      ---PartsOutboundPlanDetail配件出库计划清单 首选仓库
                      insert into tempPartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price)
                      /*2017-9-3 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                      values(s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidT/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityT,nLockedQuantityT,nvl(nOrderPrice,0));

                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,仓库，这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,nLockedQuantityT,nvl(nOrderPrice,0),2,nOrderProcessMethod,sysdate,nCreatorId,vCreatorName);

                      ---PartsInboundPlanDetail配件入库计划清单
                      insert into tempPartsInboundPlanDetail(id,partsinboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,inspectedquantity,price)
                      /*2017-9-3 hr modify nPartsInboundPlanid会取最近一个新配件入库计划单的ID*/
                      values(s_PartsInboundPlanDetail.Nextval,nPartsInboundPlanidT/*nPartsinboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityT,nLockedQuantityT,nvl(nOrderPrice,0));

                      /*2017-8-7 hr add*/
                      --统购仓库配件出库单清单（tempPartsOutboundBillDetail）记录
                      /*2017-9-3 hr modify nPartsOutboundBillId会取最近一个新配件出库单的ID
                        2017-10-11 hr modify 品牌取调拨出库仓库的品牌
                        2017-10-17 hr modify 出库清单的库位ID与编号赋值有问题*/
                      insert into tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillIdT/*nPartsOutboundBillId*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityT,nWarehouseAreaIdT,nWarehouseAreaCodeT,/*nPublicwarehouseid,vWareHouseCodeP,*/nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdP/*iSalesCategoryId*/));

                      /*
                      --第三仓库此次满足金额
                      mCurrentFulfilledAmountP:=(nSumQuantity-nvl(nSumLockedQuantity,0))*nvl(nOrderPrice,0);

                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        --select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        select count(*) into nTest from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        if nvl(nTest,0)>0 then
                           select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --else
                          -- nLockedQuantity:=0;
                        end if;
                        if nQuantity-nvl(nLockedQuantity,0)>0 then --库区可用数量
                        begin

                          nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --剩下没满足数量
                          ---nWRemainQuantity1:=nWRemainQuantity;

                          --统购仓库配件出库单清单（tempPartsOutboundBillDetail）记录
                          insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,(nQuantity-nvl(nLockedQuantity,0)),nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));

                        end;
                        end if;

                      end loop;
                      */

                      ---根据第三出库仓库ID和配件ID，检验区
                      ---select WarehouseAreaId,Quantity into iWarehouseAreaIdC,iQuantityC from PartsStock where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaCategoryId=3 and StorageCompanyId=iStorageCompanyId and BranchId=iBranchId and rownum = 1;
                      select a.id,a.Code into iWarehouseAreaIdC,vCodeC from WarehouseArea a, WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.AreaCategoryId=b.id and b.category=3 and rownum=1 and a.areakind=3;
                      if iWarehouseAreaIdC>0 then
                      begin

                        ---根据首选出库仓库ID和配件ID，保管区
                        ---select WarehouseAreaId,Quantity into iWarehouseAreaIdS,nQuantityS from PartsStock a where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and StorageCompanyId=nStorageCompanyId and BranchId=iBranchId and PartId=iSparePartId and rownum = 1;
                        select count(*) into iCount from PartsStock a,WarehouseAreaCategory b where  a.WarehouseId=nPublicwarehouseidG and a.WarehouseAreaCategoryId=b.id and b.category=1 and a.StorageCompanyId=iStoragecompanyidG and a.BranchId=iBranchId and a.PartId=iSparePartId;
                        --if iWarehouseAreaIdS>0 then
                        if iCount>0 then
                        begin
                          ---PartsShiftOrderDetail配件移库单清单
                          ---,batchnumber,remark
                          select a.WarehouseAreaId,a.Quantity,b.id,a.id into iWarehouseAreaIdS,nQuantityS,nWarehouseareacategoryidS,nPartsStockidS from PartsStock a,WarehouseAreaCategory b where a.partid=iSparePartId and a.WarehouseId=nPublicwarehouseidG and a.WarehouseAreaCategoryId=b.id and b.category=1 and a.StorageCompanyId=iStoragecompanyidG and a.BranchId=iBranchId and a.PartId=iSparePartId and rownum = 1;
                          insert into tempPartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
                          /*2017-9-3 hr modify nPartsshiftorderid会取最近一个新配件移库单的ID*/
                          values(s_PartsShiftOrderDetail.Nextval,nPartsshiftorderidT/*nPartsshiftorderid*/,iSparePartId,vSparepartcode,vSparepartname,iWarehouseAreaIdC,vCodeC,3,
                          iWarehouseAreaIdS,(select code from WarehouseArea where WarehouseId=nPublicwarehouseidG and id=iWarehouseAreaIdC),1,nLockedQuantityT);

                          --update PartsStock set Quantity=Quantity+nLockedQuantityD where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaId=iWarehouseAreaIdS and WarehouseAreaCategoryId=（select id from WarehouseAreaCategory where category=3 ） and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          update PartsStock set Quantity=Quantity+nLockedQuantityT where id=nPartsStockidS;

                          ---统购分销仓库减库存
                          update PartsStock set Quantity=Quantity-nLockedQuantityT where id=nTPartsStockidT;
                          select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          if iPartsLockedStock>0 then
                              ---锁定库存
                              update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityT,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          else
                              insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                              values(s_PartsLockedStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,iBranchId,iSparePartId,nLockedQuantityT,nCreatorId,vCreatorName,sysdate);
                          end if;
                          ---锁定库存
                          --update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityT where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                        end;
                       else
                        begin
                          ---select TopLevelWarehouseAreaId,code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea where WarehouseId=nPublicwarehouseidG and AreaCategoryId=1 and TopLevelWarehouseAreaId not in(
                          ---select a.\*TopLevelWarehouseAreaId*\,a.code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and a.\*TopLevelWarehouseAreaId*\ not in(
                          ---select WarehouseAreaId from PartsStock where  WarehouseId=nPublicwarehouseidG ) and  rownum = 1;

                          select a.id,a.code,b.id into iWarehouseAreaIdS,vWarehouseAreaCodeS,nWarehouseareacategoryidS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and rownum = 1;
                          --select a.TopLevelWarehouseAreaId,a.code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nDefaultoutwarehouseid and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and a.TopLevelWarehouseAreaId not in(
                          --select WarehouseAreaId from PartsStock where  WarehouseId=nDefaultoutwarehouseid ) and  rownum = 1;
                          if iWarehouseAreaIdS>0 then
                          begin
                            ---PartsShiftOrderDetail配件移库单清单
                            ---,batchnumber,remark
                            insert into tempPartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
                            /*2017-9-3 hr modify nPartsshiftorderid会取最近一个新配件移库单的ID*/
                            values(s_PartsShiftOrderDetail.Nextval,nPartsshiftorderidT/*nPartsshiftorderid*/,iSparePartId,vSparepartcode,vSparepartname,iWarehouseAreaIdC,vCodeC,3,
                              iWarehouseAreaIdS,(select code from WarehouseArea where WarehouseId=nPublicwarehouseidG and id=iWarehouseAreaIdC),nWarehouseareacategoryidS,nLockedQuantityT);

                            insert into PartsStock(id,warehouseid,storagecompanyid,storagecompanytype,branchid,warehouseareaid,warehouseareacategoryid,partid,quantity,creatorid,creatorname,createtime)
                            values(s_PartsStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,nstoragecompanytype,iBranchId,iWarehouseAreaIdS,nWarehouseareacategoryidS,iSparePartId,nLockedQuantityT,nCreatorId,vCreatorName,sysdate);
                            ---update PartsStock set Quantity:=Quantity+(nSumQuantity-nvl(nSumLockedQuantity,0)) where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaId=iWarehouseAreaIdS and WarehouseAreaCategoryId=3 and StorageCompanyId=iStorageCompanyId and BranchId=iBranchId;
                            ---统购分销仓库减库存
                            update PartsStock set Quantity=Quantity-nLockedQuantityT where id=nTPartsStockidT;

                            select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            if iPartsLockedStock>0 then
                              ---锁定库存
                              update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityT,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            else
                              insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                              values(s_PartsLockedStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,iBranchId,iSparePartId,nLockedQuantityT,nCreatorId,vCreatorName,sysdate);
                            end if;

                            ---锁定库存
                            --update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityT where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                          end;

                          end if;
                        end;
                        end if;

                      end;
                      end if;

                      ---PartsInboundCheckBillDetail配件入库检验单清单
                      /*2017-9-3 hr modify nPartsInboundCheckBillid会取最近一个新配件入库检验单的ID
                        2017-10-10 hr modify 入库检验单清单的成本价 取 配件计划价，品牌取调拨入库仓库的品牌
                        2017-10-17 hr modify 入库检验单清单的库位ID与编号赋值有问题，不应是 检验区的库位*/
                      insert into tPartsInboundCheckBillDetail(id,partsinboundcheckbillid,sparepartid,sparepartcode,sparepartname,warehouseareaid,warehouseareacode,inspectedquantity,settlementprice,costprice)
                      values (s_PartsInboundCheckBillDetail.Nextval,nPartsInboundCheckBillidT/*nPartsinboundcheckbillid*/,iSparePartId,vSparepartcode,vSparepartname,/*iWarehouseAreaIdC,vCodeC*/
                      iWarehouseAreaIdS,(select code from WarehouseArea where id =iWarehouseAreaIdS and areakind=3),nLockedQuantityT,nvl(nOrderPrice,0),/*nvl(nOrderPrice,0)*/
                      (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG/*iSalesCategoryId*/));

                      ---PartsShippingOrderDetail配件发运单清单
                      ---differenceclassification
                      insert into tempPartsShippingOrderDetail(id,partsshippingorderid,sparepartid,sparepartcode,sparepartname,shippingamount,confirmedamount,settlementprice)
                      /*2017-9-3 hr modify nPartsshippingorderidD会取最近一个新配件发运单的ID*/
                      values(s_PartsShippingOrderDetail.Nextval,nPartsshippingorderidT/*nPartsshippingorderid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityT,nLockedQuantityT,nvl(nOrderPrice,0));

                      ---PartsOutboundPlanDetail配件出库计划清单
                      insert into tempPartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,/*outboundfulfillment,*/price)
                      /*2017-9-3 hr modify nPartsoutboundplanidG会取最近一个新出库计划单的ID*/
                      values(s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidGT/*nPartsoutboundplanidG*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityT,/*nLockedQuantityT,*/nvl(nOrderPrice,0));


                      ---nWRemainQuantity:= nWRemainQuantity - (nSumQuantity-nvl(nSumLockedQuantity,0)); --剩下没满足数
                      --nWRemainQuantity1:=nWRemainQuantity;
                      /*2017-8-7 hr modify 剩余未满足量未赋值*/
                      nWRemainQuantity := nWRemainQuantity - nLockedQuantityT;--剩余未满足量
                      nWRemainQuantity1 := nWRemainQuantity;

                    end;
                    elsif (nSumQuantity-nvl(nSumLockedQuantity,0)) >= nWRemainQuantity then
                      ---第三仓库完全满足
                    begin

                      /*2017-10-11 hr add 对出入库计划价进行校验
                      1）计划价必须存在，且>0
                      2）出入库仓库的计划价必须一致*/
                      begin
                        select PlannedPrice into nPlannedPriceG from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG;
                      exception
                      when no_data_found then
                        begin
                          vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameG||']没有计划价';
                          raise FMyException;
                        end;
                      end;
                      if nPlannedPriceG = 0 then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameG||']的计划价为0';
                        raise FMyException;
                      end;
                      end if;
                      begin
                        select PlannedPrice into nPlannedPriceP from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdP;
                      exception
                      when no_data_found then
                        begin
                          vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']没有计划价';
                          raise FMyException;
                        end;
                      end;
                      if nPlannedPriceP = 0 then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']的计划价为0';
                        raise FMyException;
                      end;
                      end if;
                      if nPlannedPriceP <> nPlannedPriceG then
                      begin
                        vAutoapprovecommentG:='配件'||vSparePartCode||'，品牌['||vSalesCategoryNameP||']的计划价与品牌['||vSalesCategoryNameG||']的计划价不一致';
                        raise FMyException;
                      end;
                      end if;

                      if bInsertOutboundPlanTT = false then
                      begin
                        ---收货单位Id  ReceivingCompanyId,收货单位编号  ReceivingCompanyCode,收货单位名称  ReceivingCompanyName 填为  nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG


                        ---PartsTransferOrder配件调拨单
                        nPartsTransferOrderid := s_PartsTransferOrder.Nextval;
                        vPartsTransferOrderCode := regexp_replace(regexp_replace(vTransferOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateTransferOrderId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-8-7 hr modify 调拨单状态为 已审批
                          2017-10-12 hr modify 调拨单的调拨类型 应该是 3--急需调拨；发运方式设置为空
                          2017-10-13 hr add 修改人，审批人，修改时间，审批时间赋值*/
                        insert into PartsTransferOrder (id,code,originalwarehouseid,originalwarehousecode,originalwarehousename,storagecompanyid,storagecompanytype,destwarehouseid,destwarehousecode,destwarehousename,status,type,
                          shippingmethod,creatorid,creatorname,createtime,originalbillid,originalbillcode,totalamount,Modifierid,Modifiername,Modifytime,Approverid,Approvername,Approvetime)
                        values(nPartsTransferOrderid,vPartsTransferOrderCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,nStoragecompanytype,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,/*1*/2,/*1*/3,
                        null/*iShippingMethod*/,nCreatorID,vCreatorName,sysdate,PartCheckId,vCode,0,nCreatorID,vCreatorName,sysdate,nCreatorID,vCreatorName,sysdate);
                        --- totalamount,totalplanamount需要计算。

                        ----PartsOutboundPlan配件出库计划 (第三仓库)
                        nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                        vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-8-7 hr modify PartsOutboundPlan 插入时RECEIVINGCOMPANYID 不可为空
                          2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值，出库计划单的源单据 应是 调拨单
                          2017-10-12 hr modify 调拨出库生成的出库计划单的出库类型 应是 调拨出库；对方单位赋值有问题
                          2017-10-16 hr modify 发运方式设置为空，客户账户Id默认null*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface )
                        values(nPartsoutboundplanid,vPartsOutboundPlanCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdP/*iSalesCategoryId*/,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP, nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPartsTransferOrderid/*PartCheckId*/,
                          vPartsTransferOrderCode/*vCode*/,4/*1*/,null/*iShippingMethod*/,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,3,nCreatorID,vCreatorName,sysdate,0);

                        ---新建配件出库单
                        --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                        vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        nPartsOutboundBillId := s_PartsOutboundBill.Nextval;
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-13 hr modify 调拨出库生成的出库单的出库类型 应是 调拨出库 配件调拨；备注是'统购分销出库'；结算状态是 待结算；对方单位赋值有问题
                          2017-10-16 hr modify 发运方式设置为空，客户账户Id默认null*/
                        insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                          partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,receivingcompanyid,receivingcompanycode, receivingcompanyname,
                          receivingwarehouseid,receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                          creatorid,creatorname,createtime,Remark)
                        Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdP/*iSalesCategoryId*/,
                          iPartssalesordertypeid,vPartssalesordertypename,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,
                          nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,4/*1*/,null/*iShippingMethod*/,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,2/*1*/,
                          nCreatorID,vCreatorName,sysdate,'统购分销出库');


                        ---PartsShippingOrder配件发运单  /*入库计划单的源单据是 发运单，所以调整发运单生成的位置，放到入库计划单生成前*/
                        nPartsShippingOrderid := s_PartsShippingOrder.Nextval;
                        /*2017-10-12 hr modify 发运单应是4位流水号*/
                        vPartsShippingOrderCode := regexp_replace(regexp_replace(vShippingOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateShippingOrderId,TRUNC(sysdate,'DD'),vBranchCode)),4/*6*/,'0'));
                        /*2017-9-6 hr modify 发运单的发货单位与结算单位赋值有问题，状态不应是新建，应该是 回执确认
                          2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 配件发运单增加品牌字段赋值
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-12 hr add 发运单备注默认'统购分销发运'
                          2017-10-13 hr add 发运方式默认 8--客户自行运输；发运日期 不赋值；收货单位赋值有问题；预计到货日期 不赋值*/
                        insert into PartsShippingOrder(id,code,type,branchid,shippingcompanyid,shippingcompanycode,shippingcompanyname,settlementcompanyid,settlementcompanycode,settlementcompanyname,receivingcompanyid,receivingcompanycode,invoicereceivesalecateid,invoicereceivesalecatename,receivingcompanyname, warehouseid,warehousecode,warehousename,
                          receivingwarehouseid,receivingwarehousecode,receivingwarehousename,originalrequirementbillid,originalrequirementbillcode,originalrequirementbilltype,shippingdate,status,creatorid,creatorname,createtime,expectedplacedate,PartsSalesCategoryId,Remark,Shippingmethod)
                        values(nPartsShippingOrderid,vPartsShippingOrderCode,4,ibranchid,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,*/iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,/*nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,*/iStoragecompanyidP,vStoragecompanycodeP,/*nPublicwarehouseidG,vWareHouseCodeG ,*/iPartssalesordertypeid,vPartsSalesOrderTypeName,vStoragecompanynameP/*vWareHouseNameP*/,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,
                        nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,PartCheckId,vCode,1/*nPartssalescategoryid*/,null/*sysdate*/,/*1*/3,nCreatorID,vCreatorName,sysdate,null/*sysdate*/,iSalesCategoryIdP/*iSalesCategoryId*/,'统购分销发运',8);


                        ---PartsInboundPlan配件入库计划
                        nPartsInboundPlanid := s_PartsInboundPlan.Nextval;
                        vPartsInboundPlanCode := regexp_replace(regexp_replace(vInboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateInboundPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-8-7 hr modify PartsInboundPlan 插入时IfWmsInterface 不可为空
                          2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-9 hr modify 入库计划单 对方单位 赋值有问题，不应是仓库，品牌赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值，入库计划单的源单据 应是 发运单
                          2017-10-13 hr add 入库计划单备注默认'统购分销发运'；抵达日期 不赋值，客户账户Id不赋值*/
                        insert into PartsInboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,
                          sourceid,sourcecode,inboundtype,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,arrivaldate,creatorid,creatorname,createtime,IfWmsInterface,Remark)
                        values(nPartsInboundPlanid,vPartsInboundPlanCode,nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytypeG,ibranchid,vbranchcode,vbranchname,iSalesCategoryIdG/*nPartssalescategoryid*//*ipartssalesordertypeid*/,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,/*nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,*/
                        nPartsShippingOrderid/*PartCheckId*/,vPartsShippingOrderCode/*vCode*/,3,null/*nCustomerAccountId*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,2,null/*sysdate*/,nCreatorID,vCreatorName,sysdate,0,'统购分销发运');
                        ---inboundtype:3 配件调拨

                        ---PartsInboundCheckBill配件入库检验单
                        nPartsInboundCheckBillid := s_PartsInboundCheckBill.Nextval;
                        vPartsInboundCheckBillCode := regexp_replace(regexp_replace(vInboundCheckBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateInboundCheckBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-10-10 hr modify 入库检验单 仓库 仓储企业 品牌 对方单位 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-16 hr modify 客户账户Id不赋值，状态是 3-上架完成*/
                        insert into PartsInboundCheckBill(id,code,partsinboundplanid,warehouseid, warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,partssalescategoryid,branchid,branchcode,branchname,
                          counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,inboundtype,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,settlementstatus,creatorid,creatorname,createtime)
                        values (nPartsInboundCheckBillid,vPartsInboundCheckBillCode,nPartsInboundPlanid,nPublicwarehouseidG,vWareHouseCodeG ,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytypeG,iSalesCategoryIdG/*nPartssalescategoryid*/,ibranchid,vbranchcode,vbranchname,
                        iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,/*iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,*/3,null/*ncustomeraccountid*/,PartCheckId,1/*nPartssalescategoryid*/,vCode,3/*1*/,2,nCreatorID,vCreatorName,sysdate);
                        ---inboundtype:3 配件调拨. settlementstatus=2 待结算

                        ---PartsShiftOrder配件移库单
                        nPartsShiftOrderid := s_PartsShiftOrder.Nextval;
                        nPartsShiftOrderidT := nPartsShiftOrderid;
                        vPartsShiftOrderCode := regexp_replace(regexp_replace(vShiftOrderNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateShiftOrderId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        insert into  PartsShiftOrder(id,code,branchid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,status,type,creatorid,creatorname,createtime)
                        values(nPartsShiftOrderid,vPartsShiftOrderCode,ibranchid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,nStoragecompanytype,1,2,nCreatorID,vCreatorName,sysdate);


                        ---PartsShippingOrderRef配件发运单关联单
                        insert into PartsShippingOrderRef(Id,PartsShippingOrderId,PartsOutboundBillId)
                        values(s_PartsShippingOrderRef.Nextval,nPartsShippingOrderid,nPartsOutboundBillId);

                        ----仓库PartsOutboundPlan配件出库计划 （第三出库仓库）
                        nPartsoutboundplanidG := s_PartsOutboundPlan.Nextval;
                        vPartsOutboundPlanCodeG := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 出库计划单收货仓库id，编号，名称 需要赋值
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-11 hr modify 品牌应根据仓库赋值
                          2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                          2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface,Remark,Companyaddressid)
                        values(nPartsoutboundplanidG,vPartsOutboundPlanCodeG,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,iStoragecompanyidG,vStoragecompanycodeG,vStoragecompanynameG,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryIdG/*iSalesCategoryId*/,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,  iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                          vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                          sysdate,0,vRemark,vCompanyAddressId);

                        /*values(nPartsoutboundplanid,vPartsOutboundPlanCode,iWarehouseidP,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,nPartssalescategoryid,
                        vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                        vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);*/

                        nPartsTransferOrderidT := nPartsTransferOrderid;
                        nPartsoutboundplanidT := nPartsoutboundplanid;
                        nPartsOutboundBillIdT := nPartsOutboundBillId;
                        nPartsInboundPlanidT := nPartsInboundPlanid;
                        nPartsInboundCheckBillidT := nPartsInboundCheckBillid;
                        nPartsoutboundplanidGT := nPartsoutboundplanidG;
                        nPartsshippingorderidT := nPartsshippingorderid;
                        nPartsshiftorderidT := nPartsshiftorderid;

                        bInsertOutboundPlanTT := true;
                      end;
                      end if;

                      --首选仓库此次满足数量和金额
                      ---(nSumQuantity-nvl(nSumLockedQuantity,0)) >= nWRemainQuantity
                      if nMaxQuantityT <= nWRemainQuantity then
                      begin
                         mCurrentFulfilledAmountT:=nMaxQuantityT*nvl(nOrderPrice,0);
                         nLockedQuantityT := nMaxQuantityT;
                      end;
                      else
                      begin
                         mCurrentFulfilledAmountT:=nWRemainQuantity*nvl(nOrderPrice,0);
                         nLockedQuantityT := nWRemainQuantity;
                      end;
                      end if;

                      ---PartsTransferOrderDetail配件调拨单清单
                      ---  remark,
                      insert into tempPartsTransferOrderDetail(id,partstransferorderid,sparepartid,sparepartcode,sparepartname,plannedamount,confirmedamount,price,plannprice)
                      /*2017-9-3 hr modify nPartsTransferOrderid会取最近一个新配件调拨单的ID
                        2017-10-10 hr modify 调拨单清单的计划价 取 配件计划价，品牌取调拨入库仓库的品牌*/
                      values(s_PartsTransferOrderDetail.Nextval,nPartsTransferOrderidT/*nPartsTransferOrderid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityT,nLockedQuantityT,nvl(nOrderPrice,0),/*nvl(nOrderPrice,0)*/
                      (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG/*iSalesCategoryId*/));

                      ---PartsOutboundPlanDetail配件出库计划清单 第三仓库
                      insert into tempPartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price)
                      /*2017-9-3 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                      values(s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidT/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityT,nLockedQuantityT,nvl(nOrderPrice,0));


                      ---PartsInboundPlanDetail配件入库计划清单
                      insert into tempPartsInboundPlanDetail(id,partsinboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,inspectedquantity,price)
                      /*2017-9-3 hr modify nPartsInboundPlanid会取最近一个新配件入库计划单的ID*/
                      values(s_PartsInboundPlanDetail.Nextval,nPartsInboundPlanidT/*nPartsinboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityT,nLockedQuantityT,nvl(nOrderPrice,0));

                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,匹配仓库，这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,nLockedQuantityT,nvl(nOrderPrice,0),1,nOrderProcessMethod,sysdate,nCreatorId,vCreatorName);

                      /*2017-8-7 hr add*/
                      --统购仓库配件出库单清单（tempPartsOutboundBillDetail）记录
                      /*2017-9-3 hr modify nPartsOutboundBillId会取最近一个新配件出库单的ID
                        2017-10-11 hr modify 品牌取调拨出库仓库的品牌
                        2017-10-17 hr modify 出库清单的库位ID与编号赋值有问题*/
                      insert into tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillIdT/*nPartsOutboundBillId*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityT,nWarehouseAreaIdT,nWarehouseAreaCodeT,/*nPublicwarehouseid,vWareHouseCodeP,*/nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdP/*iSalesCategoryId*/));

                     /*
                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,匹配仓库，这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseidG,vWareHouseCodeG,vWareHouseNameG,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,nWRemainQuantity,nvl(nOrderPrice,0),2,nOrderProcessMethod,sysdate,nCreatorId,vCreatorName);


                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,(nSumQuantity-nvl(nSumLockedQuantity,0)),nvl(nOrderPrice,0),2,2,sysdate,nCreatorId,vCreatorName);


                      --第三仓库此次满足金额
                      mCurrentFulfilledAmountP:=nWRemainQuantity*nvl(nOrderPrice,0);

                      ----res:=mCurrentFulfilledAmountP;
                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        if nQuantity-nvl(nLockedQuantity,0)>0 then --此库区可用数量
                        begin

                          if nQuantity-nvl(nLockedQuantity,0)>= nWRemainQuantity then
                          begin

                            insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                            values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,nWRemainQuantity,nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));
                            nWRemainQuantity:= 0;
                            exit;
                          end;
                          else
                          begin
                            --统购仓库配件出库单清单（tempPartsOutboundBillDetail）记录
                            insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                            values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,(nQuantity-nvl(nLockedQuantity,0)),nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                              (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));

                              nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --剩下没满足数量
                          end;
                          ---nWRemainQuantity1:=nWRemainQuantity;
                          end if;
                        end;
                        end if;

                      end loop;
                      */

                      ---根据第三出库仓库ID和配件ID，检验区
                      ---select WarehouseAreaId,Quantity into iWarehouseAreaIdC,iQuantityC from PartsStock where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaCategoryId=3 and StorageCompanyId=iStorageCompanyId and BranchId=iBranchId and rownum = 1;
                      select a.id,a.Code into iWarehouseAreaIdC,vCodeC from WarehouseArea a, WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.AreaCategoryId=b.id and b.category=3 and rownum=1 and a.areakind=3;
                      if iWarehouseAreaIdC>0 then
                      begin

                        ---根据首选出库仓库ID和配件ID，保管区
                        ---select WarehouseAreaId,Quantity into iWarehouseAreaIdS,nQuantityS from PartsStock a where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and StorageCompanyId=nStorageCompanyId and BranchId=iBranchId and PartId=iSparePartId and rownum = 1;
                        select count(*) into iCount from PartsStock a,WarehouseAreaCategory b where  a.WarehouseId=nPublicwarehouseidG and a.WarehouseAreaCategoryId=b.id and b.category=1 and a.StorageCompanyId=iStoragecompanyidG and a.BranchId=iBranchId and a.PartId=iSparePartId;
                        --if iWarehouseAreaIdS>0 then
                        if iCount>0 then
                        begin
                          ---PartsShiftOrderDetail配件移库单清单
                          ---,batchnumber,remark
                          select a.WarehouseAreaId,a.Quantity,b.id,a.id into iWarehouseAreaIdS,nQuantityS,nWarehouseareacategoryidS,nPartsStockidS from PartsStock a,WarehouseAreaCategory b where a.partid=iSparePartId and a.WarehouseId=nPublicwarehouseidG and a.WarehouseAreaCategoryId=b.id and b.category=1 and a.StorageCompanyId=iStoragecompanyidG and a.BranchId=iBranchId and a.PartId=iSparePartId and rownum = 1;
                          insert into tempPartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
                          /*2017-9-3 hr modify nPartsshiftorderid会取最近一个新配件移库单的ID*/
                          values(s_PartsShiftOrderDetail.Nextval,nPartsshiftorderidT/*nPartsshiftorderid*/,iSparePartId,vSparepartcode,vSparepartname,iWarehouseAreaIdC,vCodeC,3,
                          iWarehouseAreaIdS,(select code from WarehouseArea where WarehouseId=nPublicwarehouseidG and id=iWarehouseAreaIdC),1,nLockedQuantityT);

                          --update PartsStock set Quantity=Quantity+nLockedQuantityD where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaId=iWarehouseAreaIdS and WarehouseAreaCategoryId=（select id from WarehouseAreaCategory where category=3 ） and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          update PartsStock set Quantity=Quantity+nLockedQuantityT where id=nPartsStockidS;
                          ---统购分销仓库减库存
                          update PartsStock set Quantity=Quantity-nLockedQuantityT where id=nTPartsStockidT;

                          select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          if iPartsLockedStock>0 then
                            ---锁定库存
                            update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityT,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                          else
                            insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                            values(s_PartsLockedStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,iBranchId,iSparePartId,nLockedQuantityT,nCreatorId,vCreatorName,sysdate);
                          end if;

                          iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                        end;
                       else
                        begin
                          ---select TopLevelWarehouseAreaId,code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea where WarehouseId=nPublicwarehouseidG and AreaCategoryId=1 and TopLevelWarehouseAreaId not in(
                          ---select a.\*TopLevelWarehouseAreaId*\,a.code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and a.\*TopLevelWarehouseAreaId*\ not in(
                          ---select WarehouseAreaId from PartsStock where  WarehouseId=nPublicwarehouseidG ) and  rownum = 1;

                          select a.id,a.code,b.id into iWarehouseAreaIdS,vWarehouseAreaCodeS,nWarehouseareacategoryidS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nPublicwarehouseidG and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and rownum = 1;
                          --select a.TopLevelWarehouseAreaId,a.code into iWarehouseAreaIdS,vWarehouseAreaCodeS from WarehouseArea a ,WarehouseAreaCategory b where a.WarehouseId=nDefaultoutwarehouseid and a.areakind=3 and  a.AreaCategoryId=b.id and b.category=1 and a.TopLevelWarehouseAreaId not in(
                          --select WarehouseAreaId from PartsStock where  WarehouseId=nDefaultoutwarehouseid ) and  rownum = 1;
                          if iWarehouseAreaIdS>0 then
                          begin
                            ---PartsShiftOrderDetail配件移库单清单
                            ---,batchnumber,remark
                            insert into tempPartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
                            /*2017-9-3 hr modify nPartsshiftorderid会取最近一个新配件移库单的ID*/
                            values(s_PartsShiftOrderDetail.Nextval,nPartsshiftorderidT/*nPartsshiftorderid*/,iSparePartId,vSparepartcode,vSparepartname,iWarehouseAreaIdC,vCodeC,3,
                              iWarehouseAreaIdS,(select code from WarehouseArea where WarehouseId=nPublicwarehouseidG and id=iWarehouseAreaIdC),nWarehouseareacategoryidS,nLockedQuantityT);

                            insert into PartsStock(id,warehouseid,storagecompanyid,storagecompanytype,branchid,warehouseareaid,warehouseareacategoryid,partid,quantity,creatorid,creatorname,createtime)
                            values(s_PartsStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,nstoragecompanytype,iBranchId,iWarehouseAreaIdS,nWarehouseareacategoryidS,iSparePartId,nLockedQuantityT,nCreatorId,vCreatorName,sysdate);
                            ---update PartsStock set Quantity:=Quantity+(nSumQuantity-nvl(nSumLockedQuantity,0)) where partid=iSparePartId and WarehouseId=nPublicwarehouseidG and WarehouseAreaId=iWarehouseAreaIdS and WarehouseAreaCategoryId=3 and StorageCompanyId=iStorageCompanyId and BranchId=iBranchId;

                            ---统购分销仓库减库存
                            update PartsStock set Quantity=Quantity-nLockedQuantityT where id=nTPartsStockidT;
                            ---锁定库存
                            update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityT where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;

                            select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            if iPartsLockedStock>0 then
                              ---锁定库存
                              update PartsLockedStock set LockedQuantity=LockedQuantity+nLockedQuantityT,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseidG and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidG and BranchId=iBranchId;
                            else
                              insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                              values(s_PartsLockedStock.Nextval,nPublicwarehouseidG,iStoragecompanyidG,iBranchId,iSparePartId,nLockedQuantityT,nCreatorId,vCreatorName,sysdate);
                            end if;

                            iCheckOrderDetailSum  := iCheckOrderDetailSum +1;

                          end;

                          end if;
                        end;
                        end if;

                      end;
                      end if;

                      ---PartsInboundCheckBillDetail配件入库检验单清单
                      /*2017-9-3 hr modify nPartsInboundCheckBillid会取最近一个新配件入库检验单的ID
                        2017-10-10 hr modify 入库检验单清单的成本价 取 配件计划价，品牌取调拨入库仓库的品牌
                        2017-10-17 hr modify 入库检验单清单的库位ID与编号赋值有问题，不应是 检验区的库位*/
                      insert into tPartsInboundCheckBillDetail(id,partsinboundcheckbillid,sparepartid,sparepartcode,sparepartname,warehouseareaid,warehouseareacode,inspectedquantity,settlementprice,costprice)
                      values (s_PartsInboundCheckBillDetail.Nextval,nPartsInboundCheckBillidT/*nPartsinboundcheckbillid*/,iSparePartId,vSparepartcode,vSparepartname,/*iWarehouseAreaIdC,vCodeC*/
                      iWarehouseAreaIdS,(select code from WarehouseArea where id =iWarehouseAreaIdS and areakind=3),nLockedQuantityT,nvl(nOrderPrice,0),/*nvl(nOrderPrice,0)*/
                      (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryIdG/*iSalesCategoryId*/));

                      ---PartsShippingOrderDetail配件发运单清单
                      ---differenceclassification
                      insert into tempPartsShippingOrderDetail(id,partsshippingorderid,sparepartid,sparepartcode,sparepartname,shippingamount,confirmedamount,settlementprice)
                      /*2017-9-3 hr modify nPartsshippingorderidD会取最近一个新配件发运单的ID*/
                      values(s_PartsShippingOrderDetail.Nextval,nPartsshippingorderidT/*nPartsshippingorderid*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityT,nLockedQuantityT,nvl(nOrderPrice,0));

                      ---PartsOutboundPlanDetail配件出库计划清单
                      insert into tempPartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,/*outboundfulfillment,*/price)
                      /*2017-9-3 hr modify nPartsoutboundplanidG会取最近一个新出库计划单的ID*/
                      values(s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidGT/*nPartsoutboundplanidG*/,iSparePartId,vSparepartcode,vSparepartname,nLockedQuantityT,/*nLockedQuantityT,*/nvl(nOrderPrice,0));

                      /*2017-8-7 hr add 剩余未满足量未赋值*/
                      nWRemainQuantity := nWRemainQuantity - nLockedQuantityT;--剩余未满足量


                      /*
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,(nSumQuantity-nvl(nSumLockedQuantity,0)),nvl(nOrderPrice,0),2,2,sysdate,nCreatorId,vCreatorName);
                      */

                    /*
                      --第三仓库此次满足金额
                      mCurrentFulfilledAmountP:=nWRemainQuantity*nvl(nOrderPrice,0);

                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        --select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        select count(*) into nTest from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        if nvl(nTest,0)>0 then
                           select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --else
                          -- nLockedQuantity:=0;
                        end if;
                        if nQuantity-nvl(nLockedQuantity,0)>0 then --此库区可用数量
                        begin

                          if (nQuantity-nvl(nLockedQuantity,0)) >nWRemainQuantity then
                          begin
                            nWRemainQuantity:= 0; --此库区剩下没满足数量,此次已经完全满足

                          end;
                          else
                          begin
                            nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --此库区剩下没满足数量

                           end;
                           end if;

                          nWRemainQuantity1:=nWRemainQuantity;

                          exit when nWRemainQuantity= 0;
                        end;
                        end if;

                      end loop;
                      */

                    end;
                    end if;
                  end;
                  else ---不是统购分销仓库
                  begin

                    nOrderProcessMethod:=2;

                    select Code,Name,storagecompanyid,storagecompanytype into vWareHouseCodeP,vWareHouseNameP,iStoragecompanyidP,nStoragecompanytype from warehouse where id=nPublicwarehouseid;
                    select code,name into vStoragecompanycodeP,vStoragecompanynameP from company where id=iStoragecompanyidP;



                    -- select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from PartsStock where
                    insert into tempPARTSSTOCK  select rownum,a.* from PARTSSTOCK a  where WarehouseId=nPublicwarehouseid  and PartId=iSparePartId  and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1;
                    select sum(quantity) into nSumQuantity from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;
                    select sum(LockedQuantity) into nSumLockedQuantity from Partslockedstock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                    /*2017-8-3 hr add*/
                    insert into tempPARTSLockedSTOCK select id,warehouseid,storagecompanyid,branchid,partid,lockedquantity,creatorid,creatorname,createtime,modifierid,modifiername,modifytime from Partslockedstock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;

                    select count(*) into iPS from tempPARTSSTOCK a  where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId;

                    if (nSumQuantity-nvl(nSumLockedQuantity,0))< nWRemainQuantity1 and (nSumQuantity-nvl(nSumLockedQuantity,0)) > 0 then ---第三仓库部分满足
                    begin
                      nNewLockedQuantity:=nSumQuantity-nvl(nSumLockedQuantity,0);  --需要锁定库存
                    /*
                      ---新建配件出库单
                      ---creatorid,creatorname,createtime,获得
                      --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                      vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                      nPartsOutboundBillId := s_PartsOutboundBill.Nextval;

                      insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                        partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,receivingcompanyid,receivingcompanycode, receivingcompanyname,receivingwarehouseid,
                        receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                        --orderapprovecomment,remark,
                        creatorid,creatorname,createtime)

                      Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,
                        iPartssalesordertypeid,vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingwarehouseid,
                        vReceivingwarehousecode,vReceivingWarehouseName,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);
                      */
                      ---新建出库计划单
                      if bInsertOutboundPlanT=false then ---如果此次审核首选仓库已经生成出库计划主单就不再生成
                      begin
                        nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                        nPartsoutboundplanidT :=nPartsoutboundplanid;
                        vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 出库计划单收货仓库id，编号，名称 需要赋值
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                          2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface,Remark,Companyaddressid)
                        values(nPartsoutboundplanid,vPartsOutboundPlanCode,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                          vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                          sysdate,0,vRemark,vCompanyAddressId);
                        bInsertOutboundPlanT:=true;
                      end;
                      end if;

                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,仓库，这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,(nSumQuantity-nvl(nSumLockedQuantity,0)),nvl(nOrderPrice,0),2,nOrderProcessMethod,sysdate,nCreatorId,vCreatorName);


                      ---新建出库计划清单
                      insert into tempPartsOutboundPlanDetail (id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price)
                      /*2017-8-3 hr modify nQuantity没有被清空，使用上一个循环中的值*/
                      /*2017-9-2 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                      values (s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidT/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,/*(nQuantity-nvl(nLockedQuantity,0))*/(nSumQuantity-nvl(nSumLockedQuantity,0)),nOrderPrice);

                      --第三仓库此次满足金额
                      mCurrentFulfilledAmountP:=(nSumQuantity-nvl(nSumLockedQuantity,0))*nvl(nOrderPrice,0);

                      /*2017-8-4 hr modify 配件多库位循环赋值剩余未满足量有问题，当锁定库存大于单个库位库存时，剩余未满足量将不会赋值*/
                      nWRemainQuantity:= nWRemainQuantity-(nSumQuantity-nvl(nSumLockedQuantity,0));
                      /*
                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        --select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        select count(*) into nTest from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        if nvl(nTest,0)>0 then
                           select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --else
                          -- nLockedQuantity:=0;
                        end if;
                        if nQuantity-nvl(nLockedQuantity,0)>0 then --库区可用数量
                        begin

                          nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --剩下没满足数量
                          ---nWRemainQuantity1:=nWRemainQuantity;
                          \*
                          --配件出库单清单（tempPartsOutboundBillDetail）记录
                          insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,(nQuantity-nvl(nLockedQuantity,0)),nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));
                          *\
                        end;
                        end if;

                      end loop;
                      */
                      ---锁定库存
                      select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      if iPartsLockedStock>0 then
                        update PartsLockedStock set LockedQuantity=LockedQuantity+nNewLockedQuantity,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      else
                        insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                        values(s_PartsLockedStock.Nextval,nPublicwarehouseid,iStoragecompanyidP,iBranchId,iSparePartId,nNewLockedQuantity,nCreatorId,vCreatorName,sysdate);
                      end if;
                      ---锁定库存
                      ---update PARTSLockedSTOCK set LockedQuantity=LockedQuantity+nNewLockedQuantity where WarehouseId=nPublicwarehouseid  and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;

                      ---nWRemainQuantity:= nWRemainQuantity - (nSumQuantity-nvl(nSumLockedQuantity,0)); --剩下没满足数
                      nWRemainQuantity1:=nWRemainQuantity;
                      iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                    end;
                    elsif (nSumQuantity-nvl(nSumLockedQuantity,0)) >= nWRemainQuantity then
                      ---第三仓库完全满足
                    begin
                      nNewLockedQuantity:=nWRemainQuantity; ---需要锁定库存
                      /*
                      --新建配件出库单
                      ---creatorid,creatorname,createtime,获得
                      --- CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000} example:CK110120140710000081
                      vPartsOutboundBillCode := regexp_replace(regexp_replace(vOutboundBillNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutBillId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                      nPartsOutboundBillId := s_PartsOutboundBill.Nextval;

                      insert into PartsOutboundBill(id,code,partsoutboundplanid,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,
                        partssalescategoryid,partssalesordertypeid,partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,receivingcompanyid,receivingcompanycode, receivingcompanyname,receivingwarehouseid,
                        receivingwarehousecode,receivingwarehousename,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,settlementstatus,
                        --orderapprovecomment,remark,
                        creatorid,creatorname,createtime)

                      Values(nPartsOutboundBillId,vPartsOutboundBillCode,nPartsoutboundplanid,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,
                        iPartssalesordertypeid,vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingwarehouseid,
                        vReceivingwarehousecode,vReceivingWarehouseName,1,iShippingMethod,nCustomerAccountId,PartCheckId,nPartssalescategoryid,vCode,1,nCreatorID,vCreatorName,sysdate);
                      */
                      ---新建出库计划单
                      if bInsertOutboundPlanT=false then ---如果此次审核第三仓库已经生成出库计划主单就不再生成
                      begin
                        nPartsoutboundplanid := s_PartsOutboundPlan.Nextval;
                        nPartsoutboundplanidT :=nPartsoutboundplanid;
                        vPartsOutboundPlanCode := regexp_replace(regexp_replace(vOutboundPlanNewCode,'{CORPCODE}',vBranchCode),'{SERIAL}',lpad(TO_CHAR(GetSerial(iComplateOutPlanId,TRUNC(sysdate,'DD'),vBranchCode)),6,'0'));
                        /*2017-9-27 hr modify 原始需求单据类型 是 配件销售订单
                          2017-9-30 hr modify 出库计划单收货仓库id，编号，名称 需要赋值
                          2017-10-10 hr modify 出库计划单 配件销售订单类型Id 赋值有问题
                          2017-10-12 hr add 出库计划单的备注 取自于销售订单的备注
                          2017-10-16 hr add 出库计划单的企业收货地址Id 取销售订单的 企业收货地址Id*/
                        insert into PartsOutboundPlan(id,code,warehouseid,warehousecode,warehousename,storagecompanyid,storagecompanycode,storagecompanyname,storagecompanytype,branchid,branchcode,branchname,partssalescategoryid,partssalesordertypeid,
                          partssalesordertypename,counterpartcompanyid,counterpartcompanycode,counterpartcompanyname,ReceivingCompanyId,ReceivingCompanyCode,ReceivingCompanyName,receivingwarehouseid,receivingwarehousecode,receivingwarehousename,sourceid,
                          sourcecode,outboundtype,shippingmethod,customeraccountid,originalrequirementbillid,originalrequirementbilltype,originalrequirementbillcode,status,creatorid,creatorname,
                          createtime,IfWmsInterface,Remark,Companyaddressid)
                        values(nPartsoutboundplanid,vPartsOutboundPlanCode,nPublicwarehouseid ,vWareHouseCodeP ,vWareHouseNameP,iStoragecompanyidP,vStoragecompanycodeP,vStoragecompanynameP,nStoragecompanytype,iBranchId,vBranchCode,vBranchName,iSalesCategoryId,iPartsSalesOrderTypeId/*nPartssalescategoryid*/,
                          vPartssalesordertypename,iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname, iReceivingcompanyid,vReceivingcompanycode,vReceivingcompanyname,iReceivingwarehouseid,vReceivingwarehousecode,vReceivingWarehouseName,PartCheckId,
                          vCode,1,iShippingMethod,nCustomerAccountId,PartCheckId,1/*nPartssalescategoryid*/,vCode,1,nCreatorID,vCreatorName,
                          sysdate,0,vRemark,vCompanyAddressId);
                        bInsertOutboundPlanT:=true;
                      end;
                      end if;

                      --配件销售订单处理单清单(PartsSalesOrderDetail)
                      ---PurchaseWarehouseId,PurchaseWarehouseCode,PurchaseWarehouseName,NewPartId,NewPartCode,NewPartName,IfDirectSupply,SupplierCompanyId,
                      ---SupplierCompanyCode,SupplierCompanyName,SupplierWarehouseId,SupplierWarehouseCode,SupplierWarehouseName, AdjustPrice,这些字段不填。
                      insert into tPartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                        SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName)
                      values(s_PartsSalesOrderProcessDetail.Nextval,nPartsSalesOrderProcessId,nPublicwarehouseid,vWareHouseCodeP ,vWareHouseNameP,iSparePartId,vSparepartcode,
                        vSparepartname,vMeasureUnit,nOrderedQuantity,nWRemainQuantity,nvl(nOrderPrice,0),1,2,sysdate,nCreatorId,vCreatorName);

                      ---新建出库计划清单
                      insert into tempPartsOutboundPlanDetail (id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price)
                      /*2017-9-2 hr modify nPartsoutboundplanid会取最近一个新出库计划单的ID*/
                      values (s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanidT/*nPartsoutboundplanid*/,iSparePartId,vSparepartcode,vSparepartname,nWRemainQuantity,nOrderPrice);
                      --第三仓库此次满足金额
                      mCurrentFulfilledAmountP:=nWRemainQuantity*nvl(nOrderPrice,0);

                      /*2017-8-4 hr modify 配件多库位循环赋值剩余未满足量有问题，当锁定库存大于单个库位库存时，剩余未满足量将不会赋值*/
                      nWRemainQuantity:= 0;
                      nWRemainQuantity1:=nWRemainQuantity;
                      /*
                      for j in 1..iPS loop
                        select warehouseareaid,warehouseareacategoryid,quantity into nWarehouseareaid,nWarehouseareacategoryid,nQuantity from tempPARTSSTOCK a where WarehouseId=nPublicwarehouseid and (select Category from WarehouseAreaCategory  where id=a.WarehouseAreaCategoryId)=1 and PartId=iSparePartId and rid=j;

                        --select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        select count(*) into nTest from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        if nvl(nTest,0)>0 then
                           select lockedquantity into nLockedQuantity from tempPARTSLockedSTOCK where WarehouseId=nPublicwarehouseid and PartId=iSparePartId;
                        --else
                          -- nLockedQuantity:=0;
                        end if;
                        if nQuantity-nvl(nLockedQuantity,0)>0 then --此库区可用数量
                        begin

                          if (nQuantity-nvl(nLockedQuantity,0)) >nWRemainQuantity then
                          begin
                            nWRemainQuantity:= 0; --此库区剩下没满足数量,此次已经完全满足

                          end;
                          else
                          begin
                            nWRemainQuantity:= nWRemainQuantity-(nQuantity-nvl(nLockedQuantity,0)); --此库区剩下没满足数量

                           end;
                           end if;

                          nWRemainQuantity1:=nWRemainQuantity;

                          \*
                          --配件出库单清单（tempPartsOutboundBillDetail）记录
                          insert into  tempPartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
                          values(s_PartsOutboundBillDetail.Nextval,nPartsOutboundBillId,iSparePartId,vSparepartcode,vSparepartname,(nQuantity-nvl(nLockedQuantity,0)),nPublicwarehouseid,vWareHouseCodeP,nvl(nOrderPrice,0),
                            (select PlannedPrice from PartsPlannedPrice where SparePartId=iSparePartId and PartsSalesCategoryId=iSalesCategoryId));
                          *\
                          exit when nWRemainQuantity= 0;
                        end;
                        end if;

                      end loop;
                      */
                      ---锁定库存
                      select count(*) into iPartsLockedStock from  PartsLockedStock where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      if iPartsLockedStock>0 then
                        update PartsLockedStock set LockedQuantity=LockedQuantity+nNewLockedQuantity,modifierid=nCreatorId,modifiername=vCreatorName,modifytime=sysdate where WarehouseId=nPublicwarehouseid and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      else
                        insert into PartsLockedStock( id,warehouseid,storagecompanyid,branchid,partid,lockedquantity, creatorid,creatorname,createtime)
                        values(s_PartsLockedStock.Nextval,nPublicwarehouseid,iStoragecompanyidP,iBranchId,iSparePartId,nNewLockedQuantity,nCreatorId,vCreatorName,sysdate);
                      end if;
                      ---锁定库存
                      ---update PARTSLockedSTOCK set LockedQuantity=LockedQuantity+nNewLockedQuantity where WarehouseId=nPublicwarehouseid  and PartId=iSparePartId and StorageCompanyId=iStoragecompanyidP and BranchId=iBranchId;
                      iCheckOrderDetailSum  := iCheckOrderDetailSum +1;
                    end;
                    end if;

                  end;
                  end if;
                end;
                end if; ---第三仓库


                if nOrderedQuantity-nApproveQuantity>nWRemainQuantity then  ---如果有审核，更新PartsSalesOrderDetail 的审批数
                begin
                  update tempPartsSalesOrderDetail set ApproveQuantity=nApproveQuantity+(nOrderedQuantity-nApproveQuantity-nWRemainQuantity) where id=nPartsSalesOrderDetailId;
                  update PartsSalesOrderDetail set ApproveQuantity=nApproveQuantity+(nOrderedQuantity-nApproveQuantity-nWRemainQuantity) where id=nPartsSalesOrderDetailId;

                end;
                end if;

              end;
              else
              begin
                --一次清单中批量审核上限不满足要求
                nli:=nli-1;

              end;
              end if;
            end loop; ---配件销售订单清单数

            if iCheckOrderDetailSum =0 then
              begin
                vAutoapprovecommentG :='';
                raise FMyException;

              end;
              end if;

            select count(*) into  nP from tempPartsSalesOrderDetail where OrderedQuantity>ApproveQuantity;
            if nP>0 then
            begin
              nBillstatusafterprocess:=4;
              vAutoapprovecommentG:='部分审核';
            end;
            else
            begin
               nBillstatusafterprocess:=5;
               vAutoapprovecommentG:='完全审核';
            end;
            end if;


            ---存在匹配仓库
            if bInsertOutboundPlanB =true then
            begin
              ---insert into tempPartsOutboundPlanDetail (id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price)
              ---   values (s_PartsOutboundPlanDetail.Nextval,nPartsoutboundplanid,iSparePartId,vSparepartcode,vSparepartname,nWRemainQuantity,nOrderPrice);
              insert into PartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price)
              select id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price from tempPartsOutboundPlanDetail where PartsOutboundPlanId=nPartsOutboundPlanIdB;

            end;
            end if;


            ---存在首选仓库
            ---统购分销
            if bInsertOutboundPlanDT=true then
            begin

              update PartsTransferOrder set TotalAmount=(select sum(confirmedamount*price) from tempPartsTransferOrderDetail where partstransferorderid=nPartstransferorderidD),
              TotalPlanAmount=(select sum(plannedamount*plannprice) from tempPartsTransferOrderDetail where partstransferorderid=nPartstransferorderidD) where id= nPartstransferorderidD;

              ---PartsTransferOrderDetail配件调拨单清单
              insert into PartsTransferOrderDetail(id,partstransferorderid,sparepartid,sparepartcode,sparepartname,plannedamount,confirmedamount,price,plannprice)
              select id,partstransferorderid,sparepartid,sparepartcode,sparepartname,plannedamount,confirmedamount,price,plannprice from tempPartsTransferOrderDetail
                      where partstransferorderid=nPartstransferorderidD ;

              ---PartsOutboundPlanDetail配件出库计划清单
              insert into PartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price)
              select id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price from tempPartsOutboundPlanDetail
                      where  partsoutboundplanid=nPartsoutboundplanidD;


              ---PartsInboundPlanDetail配件入库计划清单
              insert into PartsInboundPlanDetail(id,partsinboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,inspectedquantity,price)
              select id,partsinboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,inspectedquantity,price from tempPartsInboundPlanDetail
                     where partsinboundplanid=nPartsinboundplanidD;

              --统购仓库配件出库单清单（PartsOutboundBillDetail）
              insert into  PartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
              select id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice from tempPartsOutboundBillDetail
                      where partsoutboundbillid=nPartsoutboundbillidD;

              ---PartsInboundCheckBillDetail配件入库检验单清单
              insert into PartsInboundCheckBillDetail(id,partsinboundcheckbillid,sparepartid,sparepartcode,sparepartname,warehouseareaid,warehouseareacode,inspectedquantity,settlementprice,costprice)
              select id,partsinboundcheckbillid,sparepartid,sparepartcode,sparepartname,warehouseareaid,warehouseareacode,inspectedquantity,settlementprice,costprice from tPartsInboundCheckBillDetail
                     where partsinboundcheckbillid=nPartsinboundcheckbillidD;

              ---PartsShiftOrderDetail配件移库单清单
              insert into PartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
              select id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity
                     from tempPartsShiftOrderDetail where partsshiftorderid=nPartsshiftorderidD;

              ---PartsShippingOrderDetail配件发运单清单
              insert into PartsShippingOrderDetail(id,partsshippingorderid,sparepartid,sparepartcode,sparepartname,shippingamount,confirmedamount,settlementprice)
              select id,partsshippingorderid,sparepartid,sparepartcode,sparepartname,shippingamount,confirmedamount,settlementprice from tempPartsShippingOrderDetail
                     where partsshippingorderid=nPartsshippingorderidD;

              ---PartsOutboundPlanDetail配件出库计划清单 (统购分销出库仓库)
              insert into PartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price)
              select id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price from tempPartsOutboundPlanDetail
                     where partsoutboundplanid=nPartsoutboundplanidGD;


            end;
            end if;

            if bInsertOutboundPlanD =true then
            begin
              insert into PartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price)
              select id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price from tempPartsOutboundPlanDetail where PartsOutboundPlanId=nPartsOutboundPlanIdD;
            end;
            end if;

            ---存在第一仓库
            ---统购分销
            if bInsertOutboundPlanFT = true then
            begin
              update PartsTransferOrder set TotalAmount=(select sum(confirmedamount*price) from tempPartsTransferOrderDetail where partstransferorderid=nPartstransferorderidF),
              TotalPlanAmount=(select sum(plannedamount*plannprice) from tempPartsTransferOrderDetail where partstransferorderid=nPartstransferorderidF) where id= nPartstransferorderidF;

              ---PartsTransferOrderDetail配件调拨单清单
              insert into PartsTransferOrderDetail(id,partstransferorderid,sparepartid,sparepartcode,sparepartname,plannedamount,confirmedamount,price,plannprice)
              select id,partstransferorderid,sparepartid,sparepartcode,sparepartname,plannedamount,confirmedamount,price,plannprice from tempPartsTransferOrderDetail
                      where partstransferorderid=nPartstransferorderidF;

              ---PartsOutboundPlanDetail配件出库计划清单
              insert into PartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price)
              select id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price from tempPartsOutboundPlanDetail
                      where  partsoutboundplanid=nPartsoutboundplanidF;


              ---PartsInboundPlanDetail配件入库计划清单
              insert into PartsInboundPlanDetail(id,partsinboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,inspectedquantity,price)
              select id,partsinboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,inspectedquantity,price from tempPartsInboundPlanDetail
                     where partsinboundplanid=nPartsinboundplanidF;

              --统购仓库配件出库单清单（PartsOutboundBillDetail）
              insert into  PartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
              select id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice from tempPartsOutboundBillDetail
                      where partsoutboundbillid=nPartsoutboundbillidF;

              ---PartsInboundCheckBillDetail配件入库检验单清单
              insert into PartsInboundCheckBillDetail(id,partsinboundcheckbillid,sparepartid,sparepartcode,sparepartname,warehouseareaid,warehouseareacode,inspectedquantity,settlementprice,costprice)
              select id,partsinboundcheckbillid,sparepartid,sparepartcode,sparepartname,warehouseareaid,warehouseareacode,inspectedquantity,settlementprice,costprice from tPartsInboundCheckBillDetail
                     where partsinboundcheckbillid=nPartsinboundcheckbillidF;

              ---PartsShiftOrderDetail配件移库单清单
              insert into PartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
              select id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity
                     from tempPartsShiftOrderDetail where partsshiftorderid=nPartsshiftorderidF;

              ---PartsShippingOrderDetail配件发运单清单
              insert into PartsShippingOrderDetail(id,partsshippingorderid,sparepartid,sparepartcode,sparepartname,shippingamount,confirmedamount,settlementprice)
              select id,partsshippingorderid,sparepartid,sparepartcode,sparepartname,shippingamount,confirmedamount,settlementprice from tempPartsShippingOrderDetail
                     where partsshippingorderid=nPartsshippingorderidF;

              ---PartsOutboundPlanDetail配件出库计划清单 (统购分销出库仓库)
              insert into PartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price)
              select id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price from tempPartsOutboundPlanDetail
                     where partsoutboundplanid=nPartsoutboundplanidGF;
            end;
            end if;
            if bInsertOutboundPlanF =true then
            begin
              insert into PartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price)
              select id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price from tempPartsOutboundPlanDetail where PartsOutboundPlanId=nPartsOutboundPlanIdF;
              end;
            end if;

            ---存在第二仓库
            ---统购分销
            if bInsertOutboundPlanST = true then
            begin

              update PartsTransferOrder set TotalAmount=(select sum(confirmedamount*price) from tempPartsTransferOrderDetail where partstransferorderid=nPartstransferorderidS),
              TotalPlanAmount=(select sum(plannedamount*plannprice) from tempPartsTransferOrderDetail where partstransferorderid=nPartstransferorderidS) where id= nPartstransferorderidS;

              ---PartsTransferOrderDetail配件调拨单清单
              insert into PartsTransferOrderDetail(id,partstransferorderid,sparepartid,sparepartcode,sparepartname,plannedamount,confirmedamount,price,plannprice)
              select id,partstransferorderid,sparepartid,sparepartcode,sparepartname,plannedamount,confirmedamount,price,plannprice from tempPartsTransferOrderDetail
                      where partstransferorderid=nPartstransferorderidS;


              ---PartsOutboundPlanDetail配件出库计划清单
              insert into PartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price)
              select id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price from tempPartsOutboundPlanDetail
                      where  partsoutboundplanid=nPartsoutboundplanidS;


              ---PartsInboundPlanDetail配件入库计划清单
              insert into PartsInboundPlanDetail(id,partsinboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,inspectedquantity,price)
              select id,partsinboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,inspectedquantity,price from tempPartsInboundPlanDetail
                     where partsinboundplanid=nPartsinboundplanidS;

              --统购仓库配件出库单清单（PartsOutboundBillDetail）
              insert into  PartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
              select id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice from tempPartsOutboundBillDetail
                      where partsoutboundbillid=nPartsoutboundbillidS;

              ---PartsInboundCheckBillDetail配件入库检验单清单
              insert into PartsInboundCheckBillDetail(id,partsinboundcheckbillid,sparepartid,sparepartcode,sparepartname,warehouseareaid,warehouseareacode,inspectedquantity,settlementprice,costprice)
              select id,partsinboundcheckbillid,sparepartid,sparepartcode,sparepartname,warehouseareaid,warehouseareacode,inspectedquantity,settlementprice,costprice from tPartsInboundCheckBillDetail
                     where partsinboundcheckbillid=nPartsinboundcheckbillidS;

              ---PartsShiftOrderDetail配件移库单清单
              insert into PartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
              select id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity
                     from tempPartsShiftOrderDetail where partsshiftorderid=nPartsshiftorderidS;

              ---PartsShippingOrderDetail配件发运单清单
              insert into PartsShippingOrderDetail(id,partsshippingorderid,sparepartid,sparepartcode,sparepartname,shippingamount,confirmedamount,settlementprice)
              select id,partsshippingorderid,sparepartid,sparepartcode,sparepartname,shippingamount,confirmedamount,settlementprice from tempPartsShippingOrderDetail
                     where partsshippingorderid=nPartsshippingorderidS;

              ---PartsOutboundPlanDetail配件出库计划清单 (统购分销出库仓库)
              insert into PartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price)
              select id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price from tempPartsOutboundPlanDetail
                     where partsoutboundplanid=nPartsoutboundplanidGS;
            end;
            end if;

            if bInsertOutboundPlanS = true then
            begin
              insert into PartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price)
              select id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price from tempPartsOutboundPlanDetail where PartsOutboundPlanId=nPartsOutboundPlanIdS;
            end;
            end if;

            ---存在第三仓库
            ---统购分销
            if bInsertOutboundPlanTT=true then
            begin

              update PartsTransferOrder set TotalAmount=(select sum(confirmedamount*price) from tempPartsTransferOrderDetail where partstransferorderid=nPartstransferorderidT),
              TotalPlanAmount=(select sum(plannedamount*plannprice) from tempPartsTransferOrderDetail where partstransferorderid=nPartstransferorderidT) where id= nPartstransferorderidT;

              ---PartsTransferOrderDetail配件调拨单清单
              insert into PartsTransferOrderDetail(id,partstransferorderid,sparepartid,sparepartcode,sparepartname,plannedamount,confirmedamount,price,plannprice)
              select id,partstransferorderid,sparepartid,sparepartcode,sparepartname,plannedamount,confirmedamount,price,plannprice from tempPartsTransferOrderDetail
                      where partstransferorderid=nPartstransferorderidT ;

              ---PartsOutboundPlanDetail配件出库计划清单
              insert into PartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price)
              select id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price from tempPartsOutboundPlanDetail
                      where  partsoutboundplanid=nPartsoutboundplanidT;


              ---PartsInboundPlanDetail配件入库计划清单
              insert into PartsInboundPlanDetail(id,partsinboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,inspectedquantity,price)
              select id,partsinboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,inspectedquantity,price from tempPartsInboundPlanDetail
                     where partsinboundplanid=nPartsinboundplanidT;

              --统购仓库配件出库单清单（PartsOutboundBillDetail）
              insert into  PartsOutboundBillDetail(id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice)
              select id,partsoutboundbillid,sparepartid,sparepartcode,sparepartname,outboundamount,warehouseareaid,warehouseareacode,settlementprice,costprice from tempPartsOutboundBillDetail
                      where partsoutboundbillid=nPartsoutboundbillidT;

              ---PartsInboundCheckBillDetail配件入库检验单清单
              insert into PartsInboundCheckBillDetail(id,partsinboundcheckbillid,sparepartid,sparepartcode,sparepartname,warehouseareaid,warehouseareacode,inspectedquantity,settlementprice,costprice)
              select id,partsinboundcheckbillid,sparepartid,sparepartcode,sparepartname,warehouseareaid,warehouseareacode,inspectedquantity,settlementprice,costprice from tPartsInboundCheckBillDetail
                     where partsinboundcheckbillid=nPartsinboundcheckbillidT;

              ---PartsShiftOrderDetail配件移库单清单
              insert into PartsShiftOrderDetail(id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity)
              select id,partsshiftorderid,sparepartid,sparepartcode,sparepartname,originalwarehouseareaid,originalwarehouseareacode,originalwarehouseareacategory,destwarehouseareaid,destwarehouseareacode,destwarehouseareacategory,quantity
                     from tempPartsShiftOrderDetail where partsshiftorderid=nPartsshiftorderidT;

              ---PartsShippingOrderDetail配件发运单清单
              insert into PartsShippingOrderDetail(id,partsshippingorderid,sparepartid,sparepartcode,sparepartname,shippingamount,confirmedamount,settlementprice)
              select id,partsshippingorderid,sparepartid,sparepartcode,sparepartname,shippingamount,confirmedamount,settlementprice from tempPartsShippingOrderDetail
                     where partsshippingorderid=nPartsshippingorderidT;

              ---PartsOutboundPlanDetail配件出库计划清单 (统购分销出库仓库)
              insert into PartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price)
              select id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,outboundfulfillment,price from tempPartsOutboundPlanDetail
                     where partsoutboundplanid=nPartsoutboundplanidGT;
            end;
            end if;

            if bInsertOutboundPlanT =true then
            begin
                insert into PartsOutboundPlanDetail(id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price)
              select id,partsoutboundplanid,sparepartid,sparepartcode,sparepartname,plannedamount,price from tempPartsOutboundPlanDetail where PartsOutboundPlanId=nPartsOutboundPlanIdT;
            end;
            end if;


            --- 更新处理单CurrentFulfilledAmount字段数据，更新CustomerAccount客户账户 数据
            select nvl(sum(CurrentFulfilledQuantity*(nvl(OrderPrice,0)-nvl(AdjustPrice,0))),0) into mCurrentFulfilledAmount from tPartsSalesOrderProcessDetail where PartsSalesOrderProcessId=nPartsSalesOrderProcessId;
            ---select nvl(sum(CurrentFulfilledQuantity*(nvl(OrderPrice,0)-nvl(AdjustPrice,0))),0) into mCurrentFulfilledAmount from tPartsSalesOrderProcessDetail where PartsSalesOrderProcessId=nPartsSalesOrderProcessId;
            update PartsSalesOrderProcess set CurrentFulfilledAmount= mCurrentFulfilledAmount,Billstatusafterprocess=nBillstatusafterprocess where Id=nPartsSalesOrderProcessId;

            ---PartsSalesOrderProcessDetail配件销售订单处理清单
            /*2017-10-17 hr add 销售订单处理清单 统购分销出库没有赋值*/
            insert into PartsSalesOrderProcessDetail (id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                      SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName,
                      Purchasewarehouseid,Purchasewarehousecode,Purchasewarehousename)
                select id,PartsSalesOrderProcessId,WarehouseId,WarehouseCode,WarehouseName,SparePartId,SparePartCode,
                      SparePartName,MeasureUnit,OrderedQuantity,CurrentFulfilledQuantity,OrderPrice,OrderProcessStatus,OrderProcessMethod,CreateTime,CreatorId,CreatorName,
                      Purchasewarehouseid,Purchasewarehousecode,Purchasewarehousename
                  from tPartsSalesOrderProcessDetail where PartsSalesOrderProcessId=nPartsSalesOrderProcessId;

            update CustomerAccount set PendingAmount=nvl(PendingAmount,0)+mCurrentFulfilledAmount,ModifierId=nCreatorId,ModifierName=vCreatorName,ModifyTime=sysdate where id=nCustomerAccountId;
            /*2017-8-8 hr modify 品牌是随车行时，nSumTotalAmount变量为空，所以此处按品牌更新字段
              2017-10-16 hr add 自动审核时，更新自动审核时间
              2018-2-24 hr modify 自动审核时 销售订单总金额不进行更新*/
            if  (vSalesCategoryName<>'随车行') then
              update PartsSalesOrder set status=nBillstatusafterprocess,/*TotalAmount=nSumTotalAmount,*/AutoApproveTime=sysdate where id=PartCheckId;
            else
              update PartsSalesOrder set status=nBillstatusafterprocess,AutoApproveTime=sysdate where id=PartCheckId;
            end if;


            --判断是不是所有清单中批量审核上限 都不满足要求
            if nli=0 then
            begin
              vAutoapprovecommentG:='配件清单自动审核上限均不满足需求';
              raise FMyException;
            end;
            end if;

            ---提交
            COMMIT;
          end;
          end if;
        end;
        else
        begin
          vAutoapprovecommentG:='出库仓库优先顺序未维护';
          raise FMyException;
        end;
        end if;
      end;
      end if; ---判断客户账户可用余额 if nAvailbleAmount<nSum
     -----res:=vAutoapprovecommentG;
      ---更新PartsSalesOrder自动审核情况
      if vAutoapprovecommentG is not null then
      begin
        select rowversion into newrowversion from PartsSalesOrder where  id=PartCheckId;
        if newrowversion<>oldrowversion then begin
               vAutoapprovecommentG:='该订单数据已经变更，自动审核失败';
          raise FMyException;
          end ;
          end if;
        /*2017-8-24 hr modify 自动审核意见 报错信息截取，保留老数据*/
        update PartsSalesOrder set Autoapprovecomment=substrb(Autoapprovecomment||chr(10)||vAutoapprovecommentG||chr(10)||',审核时间：'||sysdate,1,2000) ,AutoApproveTime=sysdate  where id=PartCheckId;
        ---提交
        COMMIT;
      end;
      end if;


      EXCEPTION
        WHEN FMyException THEN
        BEGIN
          ROLLBACK;
          /*2017-10-11 hr modify 使用<>''，不会进行下面的update，故使用了长度判断*/
          --if vAutoapprovecommentG<>'' then
          if lengthb(vAutoapprovecommentG) > 0 then
            /*2017-8-24 hr modify 自动审核意见 报错信息截取，保留老数据*/
            update PartsSalesOrder set Autoapprovecomment=substrb(Autoapprovecomment||chr(10)||vAutoapprovecommentG||chr(10)||',审核时间：'||sysdate,1,2000) ,AutoApproveTime=sysdate  where id=PartCheckId;
          end if;
        END;

      WHEN OTHERS THEN
        BEGIN
          ROLLBACK;
          vAutoapprovecommentG := sqlerrm;
          /*2017-8-24 hr modify 自动审核意见 报错信息截取，保留老数据*/
          update PartsSalesOrder set Autoapprovecomment=substrb(Autoapprovecomment||chr(10)||vAutoapprovecommentG||chr(10)||',审核时间：'||sysdate,1,2000) ,AutoApproveTime=sysdate  where id=PartCheckId;
        end;


    end;
    end if; --此销售订单ID有对应的销售清单    if nRow>0
  end;
  end if; ---销售订单存在 if num>0


end AutoCheckNoEcommerceOrder;
