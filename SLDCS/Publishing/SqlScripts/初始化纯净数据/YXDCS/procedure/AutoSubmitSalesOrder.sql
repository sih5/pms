create or replace procedure AutoSubmitSalesOrder as
  nRow number;
begin
  declare
    cursor Autoadd is(
      select t.* from AutoSalesOrderProcess t where t.status = 0);
  begin
    for S_Autoadd in Autoadd loop
      begin
        select count(1)
          into nRow
          from PartsSalesOrder s
         where s.status in (2, 4)
           and s.id = S_Autoadd.orderid;
        if nRow > 0 then
          autochecknoecommerceorder(S_Autoadd.orderid);
        end if;
      
        update AutoSalesOrderProcess
           set status = 1
         where id = S_Autoadd.id;
        commit;
      end;
    end loop;
  end;
end AutoSubmitSalesOrder;
