create or replace procedure Prc_InsertPartsExchangeGroup is
begin
  /*补互换组数据 FTDCS_ISS20171107012
    互换分组号 序列号以【8】开头表示 自动补录数据
    PS:        序列号以【9】开头表示 手工补录数据
               序列号以【0】开头表示 系统生成*/
  Insert into partsexchangegroup
    (Id,
     exgroupcode,
     exchangecode,
     status,
     creatorid,
     creatorname,
     createtime,
     remark)
    select s_partsexchangegroup.nextval,
           case
             when rownum < 10 then
              'SWP' || to_char(sysdate, 'yyyymmdd') || '80000' ||
              to_char(rownum)
             when rownum < 100 then
              'SWP' || to_char(sysdate, 'yyyymmdd') || '8000' ||
              to_char(rownum)
             when rownum < 1000 then
              'SWP' || to_char(sysdate, 'yyyymmdd') || '800' ||
              to_char(rownum)
             when rownum < 10000 then
              'SWP' || to_char(sysdate, 'yyyymmdd') || '80' ||
              to_char(rownum)
             when rownum < 100000 then
              'SWP' || to_char(sysdate, 'yyyymmdd') || '8' ||
              to_char(rownum)
           end as exgroupcode,
           exchangecode,
           1,
           7,
           '数据导入',
           sysdate,
           '数据导入' || to_char(sysdate, 'yyyymmdd')
      from (select distinct t.exchangecode
              from partsexchange t
             where t.status <> 99
               and t.exchangecode not in
                   (select exchangecode
                      from partsexchangegroup g
                     where g.status <> 99));

end Prc_InsertPartsExchangeGroup;
