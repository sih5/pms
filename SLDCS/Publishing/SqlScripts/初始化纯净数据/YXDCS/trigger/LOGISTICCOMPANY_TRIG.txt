
create or replace trigger YXDCS.LogisticCompany_trig
  after insert or update ON LogisticCompany
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    insert into LogisticCompany_sync
      (billid, syncnum,type)
    values
      (:new.Id,LogisticCompany_increase.nextval,'Logistic');
  END;
END;
/