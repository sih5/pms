create or replace trigger YXDCS.MalfunctionCategory_gcdcs_Trg
  after insert or update on MalfunctionCategory
  for each row

begin

  merge into gcdcs.MalfunctionCategory a
  using (select 1 from dual)
  on (a.id = :new.id)
  when matched then
    update
       set a.CODE            = :new.code,
           a.name            = :new.name,
           a.layernodetypeid = :new.LAYERNODETYPEID,
           a.parentid        = :new.parentid,
           a.rootgroupid     = :new.rootgroupid,
           a.status          = :new.status,
           a.remark          = :new.remark,
           a.ModifierId      = :new.ModifierId,
           a.ModifierName    = :new.ModifierName,
           a.ModifyTime      = :new.ModifyTime,
           a.stmscode        = :new.stmscode
  when not matched then
    insert /*into gcdcs.MalfunctionCategory*/
      (ID,
       CODE,
       NAME,
       LAYERNODETYPEID,
       PARENTID,
       ROOTGROUPID,
       STATUS,
       REMARK,
       CREATORID,
       CREATORNAME,
       CREATETIME,
       stmscode)
    values
      (:new.id,
       :new.code,
       :new.name,
       :new.LAYERNODETYPEID,
       :new.parentid,
       :new.rootgroupid,
       :new.status,
       :new.remark,
       :new.creatorid,
       :new.creatorname,
       :new.createtime,
       :new.stmscode);

end;
/