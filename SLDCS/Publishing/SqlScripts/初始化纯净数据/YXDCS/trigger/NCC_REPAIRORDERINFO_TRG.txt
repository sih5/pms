CREATE OR REPLACE TRIGGER YXDCS.NCC_RepairOrderInfo_TRG
AFTER  UPDATE  ON RepairOrder
FOR EACH ROW

declare
  countnumFault number(9);
   countnumItem number(9);
    countnumMaterial number(9);
BEGIN
  if :New.Status = 6  then
        --ά��
        INSERT INTO NCC_RepairOrderInfo_SYNC VALUES(S_NCC_RepairOrderInfo_SYNC.NEXTVAL,:NEW.ID,'RepairOrderInfo',sysdate);
        --����
        begin
             select count(*)
             into countnumFault
               from RepairOrderFaultReason r
             where r.RepairOrderId=:new.Id ;
             EXCEPTION
             when no_data_found then
               countnumFault:=0;
         end ;
        if (countnumFault != 0 )  then
            INSERT INTO NCC_FaultReason_SYNC VALUES(S_NCC_FaultReason_SYNC.NEXTVAL,:NEW.ID,'FaultReason',sysdate);
      end if;

      --��Ŀ
        begin
              select  count(*)
               into countnumItem
                 from RepairOrderFaultReason f
                 inner join RepairOrderItemDetail i on f.id = i.repairorderfaultreasonid
               where f.RepairOrderId=:new.Id;
               EXCEPTION
               when no_data_found then
                 countnumItem:=0;
         end ;
        if (countnumItem != 0 )  then
             INSERT INTO NCC_RepairItemDetail_SYNC VALUES(S_NCC_RepairItemDetail_SYNC.NEXTVAL,:NEW.ID,'RepairItemDetail',sysdate);
      end if;

      --����
        begin
              select  count(*)
             into countnumMaterial
               from RepairOrderFaultReason f
               inner join RepairOrderItemDetail i on f.id = i.repairorderfaultreasonid
               inner join RepairOrderMaterialDetail m on m.repairclaimitemdetailid = i.Id
             where f.RepairOrderId=:new.Id;
             EXCEPTION
             when no_data_found then
               countnumMaterial:=0;
         end ;
        if (countnumMaterial != 0 )  then
              INSERT INTO NCC_MaterialDetail_SYNC VALUES(S_NCC_MaterialDetail_SYNC.NEXTVAL,:NEW.ID,'MaterialDetail',sysdate);
      end if;
  end if;
END NCC_RepairOrderInfo_TRG;
/