
create or replace trigger YXDCS.Retailer_InventoryBill_TRG
    after insert or update on PartsInventoryBill
for each row
declare
  categoryname number(9);
begin
  begin
   select count(*)
   into categoryname
     from SalesUnitAffiWarehouse a inner join SalesUnit b on a.SalesUnitId=b.Id
   where a.WarehouseId=:new.WarehouseId and exists(select 1 from partssalescategory where
   partssalescategory.id=b.Partssalescategoryid and (name='随车行' or name='集团配件'));
   EXCEPTION
   when no_data_found then
     categoryname:=0;
     end ;
   if categoryname >0 and :new.StorageCompanyCode='1101' and :new.Status=3 then
        INSERT INTO Retailer_InventoryBill_SYNC VALUES(S_Retailer_InventoryBill_SYNC.NEXTVAL,:NEW.ID,'PartsInventoryBill',sysdate);   end if;
end Retailer_InventoryBill_TRG;
/