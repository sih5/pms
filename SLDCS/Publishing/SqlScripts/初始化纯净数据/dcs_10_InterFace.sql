create sequence s_statusofsync
minvalue 1
maxvalue 9999999999999999999999999999
start with 41
increment by 1
nocache;

-- Create table
create table STATUSOFSYNC
(
  ID            NUMBER,
  SYNCTYPENAME  VARCHAR2(100),
  NOWSYNCNUMBER NUMBER,
  EXTENDFIELD1  VARCHAR2(100),
  EXTENDFIELD2  VARCHAR2(100),
  EXTENDFIELD3  VARCHAR2(100),
  EXTENDFIELD4  VARCHAR2(100)
);


create table CLAIMSETTLEFILELOG
(
  ID            NUMBER not null,
  PATH          VARCHAR2(1000),
  SYSCODE	VARCHAR2(50),
  HANDLESTATUS  NUMBER,
  RECEIVETIME   DATE,
  HANDLEMESSAGE VARCHAR2(1000),
  HANDLETIME    DATE
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255;
-- Create/Recreate primary, unique and foreign key constraints 
alter table CLAIMSETTLEFILELOG
  add constraint PK_CLAIMSETTLEFILELOG primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255;

--建序列
create sequence S_CLAIMSETTLEFILELOG
minvalue 1
maxvalue 999999999999
start with 1
increment by 1
nocache;

-- Create table
create table CLAIMSETTLELOGLS
(
  ID          NUMBER not null,
  SYSCODE     VARCHAR2(4),
  CODE        VARCHAR2(70),
  CLAIMNUMBER VARCHAR2(40),
  ERRORMSG    VARCHAR2(2000),
  DTIME       DATE,
  CREATORID   NUMBER,
  CREATORNAME VARCHAR2(50),
  CREATETIME  DATE
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255;
-- Create/Recreate primary, unique and foreign key constraints 
alter table CLAIMSETTLELOGLS
  add constraint PK_CLAIMSETTLELOGLS primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255;
  
create sequence S_CLAIMSETTLELOGLS
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
nocache;  
  
  -- Create table
create table CUMMINS_CLAIMSETTLE_SYNC
(
  SYNCNUMBER         NUMBER,
  BUSSINESSID        NUMBER,
  BUSSINESSTABLENAME VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255;


-- Create sequence 
create sequence S_CUMMINS_CLAIMSETTLE
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
nocache;

Insert into statusofsync values(s_statusofsync.nextval,'CUMMINS_CLAIMSETTLE_SYNC',0,null,null,null,null);

-- Create table
create table MDMMAINDATA
(
  ID                   NUMBER(9) not null,
  MDMID                VARCHAR2(200),
  TYPE                 VARCHAR2(200),
  CODE                 VARCHAR2(50),
  NAME                 VARCHAR2(100),
  KUNNR                VARCHAR2(50),
  LIFNR                VARCHAR2(50),
  SHORTNAME            VARCHAR2(50),
  PROVINCENAME         VARCHAR2(50),
  CITYNAME             VARCHAR2(50),
  COUNTYNAME           VARCHAR2(50),
  CITYLEVEL            VARCHAR2(50),
  FOUNDDATE            VARCHAR2(50),
  CONTACTPERSON        VARCHAR2(100),
  CONTACTPHONE         VARCHAR2(50),
  CONTACTMOBILE        VARCHAR2(50),
  FAX                  VARCHAR2(50),
  CONTACTADDRESS       VARCHAR2(100),
  CONTACTPOSTCODE      VARCHAR2(100),
  CONTACTMAIL          VARCHAR2(50),
  CORPORATENATURE      VARCHAR2(50),
  LEGALREPRESENTATIVE  VARCHAR2(100),
  LEGALREPRESENTTEL    VARCHAR2(100),
  REGISTERCAPITAL      VARCHAR2(50),
  REGISTERDATE         VARCHAR2(50),
  BUSINESSSCOPE        VARCHAR2(200),
  BUSINESSADDRESS      VARCHAR2(200),
  SUPPLIERTYPE         VARCHAR2(50),
  TAXREGISTEREDNUMBER  VARCHAR2(50),
  INVOICETITLE         VARCHAR2(100),
  INVOICETYPE          VARCHAR2(50),
  TAXREGISTEREDADDRESS VARCHAR2(200),
  TAXREGISTEREDPHONE   VARCHAR2(50),
  BANKNAME             VARCHAR2(200),
  BANKACCOUNT          VARCHAR2(200),
  REMARK               VARCHAR2(200),
  STATUS               VARCHAR2(50),
  OPTTYPE              VARCHAR2(50),
  RECEIVETIME          DATE,
  HANDLESTATUS         NUMBER(9),
  HANDLETIME           DATE,
  HANDLEMESSAGE        VARCHAR2(500)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table MDMMAINDATA
  add constraint PK_MDMMAINDATA primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );




---建序列
create sequence s_MDMMAINDATA
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
nocache;


delete from statusofsync where synctypename='MDM_SUPPLIER_PAGEINFO' or synctypename='MDM_CUSTOMER_PAGEINFO';
drop table mdmcustomer;
drop table mdmsupplier;
drop sequence s_mdmcustomer;
drop sequence s_mdmsupplier;


insert into statusofsync (ID, SYNCTYPENAME, EXTENDFIELD1, EXTENDFIELD2, EXTENDFIELD3)
values (85, 'MDM_PAGEINFO', '1', '-1', '');

-- Create table
create table MDMCOMPANYTYPE
(
  ID           NUMBER(9) not null,
  PMSID        NUMBER(9) not null,
  PMSCODE      VARCHAR2(100) not null,
  MDMTYPE      VARCHAR2(100) not null,
  STATUS       NUMBER(9) not null,
  CREATORID    NUMBER(9),
  CREATORNAME  VARCHAR2(100),
  CREATETIME   DATE,
  MODIFIERID   NUMBER(9),
  MODIFIERNAME VARCHAR2(100),
  MODIFYTIME   DATE,
  REMARK       VARCHAR2(500)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table MDMCOMPANYTYPE
  add constraint PK_MDMCOMPANYTYPE primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );


-- Create sequence 
create sequence S_MDMCOMPANYTYPE
minvalue 1
maxvalue 9999999999999999999999999999
start with 5
increment by 1
nocache;

-- Create table
create table MVS_REPAIRORDER_SYNC
(
  SYNCNUMBER         NUMBER,
  BUSSINESSID        NUMBER,
  BUSSINESSTABLENAME VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );


create sequence S_MVS_REPAIRORDER_SYNC
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1;

-- Create table
create table OutInfo
(
  Id                       NUMBER(9) not null,
  SYSCODE                  VARCHAR2(200),
  RepairOrderCode          VARCHAR2(200),
  MVSOutRange              VARCHAR2(200),
  MVSOutTime               VARCHAR2(200),
  MVSOutDistance           VARCHAR2(200),
  MVSOutCoordinate         VARCHAR2(200),
  ProcessStatus            NUMBER(9),
  LastProcessTime          DATE,
  ErrorMessage             VARCHAR2(200)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 8K
    minextents 1
    maxextents unlimited
  );
  
-- Create sequence 
create sequence S_OutInfo
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;
-- Create/Recreate primary, unique and foreign key constraints 
alter table OutInfo
  add constraint PK_OutInfo primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );


insert into  statusofsync(Id,synctypename,Nowsyncnumber) values(s_statusofsync.nextval,'MVS_REPAIRORDER_SYNC',0);


-- Create table
create table CCDISPATCHINGINFO
(
  ID                        NUMBER(9) not null,
  BRANCHCODE                VARCHAR2(50),
  BRANCHNAME                VARCHAR2(100),
  DISPATCHINGCODE           VARCHAR2(50),
  CALLCENTERDISPATCHING     NUMBER(9),
  VIN                       VARCHAR2(50),
  WORKORDERCONTENT          VARCHAR2(500),
  POSITIONOFFAULT           VARCHAR2(50),
  CUSTOMER                  VARCHAR2(50),
  VEHICLECONTACTPERSON      VARCHAR2(50),
  DEALERCODE                VARCHAR2(50),
  DEALERNAME                VARCHAR2(50),
  ENGINEERPROCESSINGCONTENT VARCHAR2(500),
  ARRIVALDATE               DATE,
  FINISHINGTIME             DATE,
  CLOSEDLOOPTIME            DATE,
  ESTIMATEDFINISHINGTIME    DATE,
  CONFIRMDATE               DATE,
  REPAIRCLASSIFICATION      NUMBER(9),
  COMPLAINTCLASSIFICATION   VARCHAR2(50),
  ISCALLCENTERSUPPORT       VARCHAR2(50),
  FEEDBACKNUMBER            NUMBER(9),
  DISPATCHINGSOURCE         NUMBER(9),
  DISPATCHING               NUMBER(9),
  RECEIVETIME               DATE,
  HANDLESTATUS              NUMBER(9),
  HANDLEMESSAGE             VARCHAR2(500),
  HANDLETIME                DATE
);

alter table CCDISPATCHINGINFO add constraint PK_CCDISPATCHINGINFO primary key (ID);


create sequence S_CCDISPATCHINGINFO
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
nocache;

create table CC_VEHICLEINFORMATION_SYNC
(
  SYNCNUMBER         NUMBER,
  BUSSINESSID        NUMBER,
  BUSSINESSTABLENAME VARCHAR2(100)
);
create sequence  S_CC_VEHICLEINFORMATION
nocache;

create table CC_VEHICLELINKMAN_SYNC
(
  SYNCNUMBER         NUMBER,
  BUSSINESSID        NUMBER,
  BUSSINESSTABLENAME VARCHAR2(100)
);
create sequence  S_CC_VEHICLELINKMAN
nocache;

create table CC_RETAINEDCUSTVEHICLE_SYNC
(
  SYNCNUMBER         NUMBER,
  BUSSINESSID        NUMBER,
  BUSSINESSTABLENAME VARCHAR2(100)
);
create sequence  S_CC_RETAINEDCUSTVEHICLE
nocache;

create table CC_COMPANY_SYNC
(
  SYNCNUMBER         NUMBER,
  BUSSINESSID        NUMBER,
  BUSSINESSTABLENAME VARCHAR2(100)
);
create sequence  S_CC_COMPANY
nocache;

create table CC_CUSTOMER_SYNC
(
  SYNCNUMBER         NUMBER,
  BUSSINESSID        NUMBER,
  BUSSINESSTABLENAME VARCHAR2(100)
);
create sequence  S_CC_CUSTOMER 
nocache;

create table CC_DEALER_SYNC
(
  SYNCNUMBER         NUMBER,
  BUSSINESSID        NUMBER,
  BUSSINESSTABLENAME VARCHAR2(100)
);
create sequence  S_CC_DEALER
nocache;

create table CC_RETAINEDCUSTOMER_SYNC
(
  SYNCNUMBER         NUMBER,
  BUSSINESSID        NUMBER,
  BUSSINESSTABLENAME VARCHAR2(100)
);
create sequence  S_CC_RETAINEDCUSTOMER
nocache;

create table CC_DEALERSERVICEINFO_SYNC
(
  SYNCNUMBER         NUMBER,
  BUSSINESSID        NUMBER,
  BUSSINESSTABLENAME VARCHAR2(100)
);
create sequence  S_CC_DEALERSERVICEINFO
nocache;

create table CC_REPAIRORDER_SYNC
(
  SYNCNUMBER         NUMBER,
  BUSSINESSID        NUMBER,
  BUSSINESSTABLENAME VARCHAR2(100)
);
create sequence  S_CC_REPAIRORDER
nocache;

create table CC_REPAIRWORKORDER_SYNC
(
  SYNCNUMBER         NUMBER,
  BUSSINESSID        NUMBER,
  BUSSINESSTABLENAME VARCHAR2(100)
);
create sequence  S_CC_REPAIRWORKORDER 
nocache;

create table CC_SERVICEPRODLINEPRODUCT_SYNC
(
  SYNCNUMBER         NUMBER,
  BUSSINESSID        NUMBER,
  BUSSINESSTABLENAME VARCHAR2(100)
);
create sequence  S_CC_SERVICEPRODLINEPRODUCT 
nocache;



create table CC_SERVPRODLINEPRODETAIL_SYNC
(
  SYNCNUMBER         NUMBER,
  BUSSINESSID        NUMBER,
  BUSSINESSTABLENAME VARCHAR2(100)
);
create sequence  S_CC_SERVPRODLINEPRODETAIL
nocache;

create table CC_DEALERBUSINESSPERMIT_SYNC
(
  SYNCNUMBER         NUMBER,
  BUSSINESSID        NUMBER,
  BUSSINESSTABLENAME VARCHAR2(100)
);
create sequence  S_CC_DEALERBUSINESSPERMIT
nocache;


Insert into statusofsync values(s_statusofsync.nextval,'CC_VEHICLEINFORMATION_SYNC',0,null,null,null,null);
Insert into statusofsync values(s_statusofsync.nextval,'CC_VEHICLELINKMAN_SYNC',0,null,null,null,null);
Insert into statusofsync values(s_statusofsync.nextval,'CC_RETAINEDCUSTVEHICLE_SYNC',0,null,null,null,null);
Insert into statusofsync values(s_statusofsync.nextval,'CC_COMPANY_SYNC',0,null,null,null,null);
Insert into statusofsync values(s_statusofsync.nextval,'CC_CUSTOMER_SYNC',0,null,null,null,null);
Insert into statusofsync values(s_statusofsync.nextval,'CC_DEALER_SYNC',0,null,null,null,null);
Insert into statusofsync values(s_statusofsync.nextval,'CC_RETAINEDCUSTOMER_SYNC',0,null,null,null,null);
Insert into statusofsync values(s_statusofsync.nextval,'CC_DEALERSERVICEINFO_SYNC',0,null,null,null,null);
Insert into statusofsync values(s_statusofsync.nextval,'CC_REPAIRORDER_SYNC',0,null,null,null,null);
Insert into statusofsync values(s_statusofsync.nextval,'CC_REPAIRWORKORDER_SYNC',0,null,null,null,null);
Insert into statusofsync values(s_statusofsync.nextval,'CC_PRODUCT_SYNC',0,null,null,null,null);
Insert into statusofsync values(s_statusofsync.nextval,'CC_SERVICEPRODLINEPRODUCT_SYNC',0,null,null,null,null);
Insert into statusofsync values(s_statusofsync.nextval,'CC_SERVPRODLINEPRODETAIL_SYNC',0,null,null,null,null);
Insert into statusofsync values(s_statusofsync.nextval,'CC_DEALERBUSINESSPERMIT_SYNC',0,null,null,null,null);



-- Create table
create table DTP_PRODUCT
(
  ID                         NUMBER(9) not null,
  DMSID                      VARCHAR2(38) not null,
  CODE                       VARCHAR2(50) not null,
  PRODUCTCATEGORYCODE        VARCHAR2(50),
  PRODUCTCATEGORYNAME        VARCHAR2(200),
  BRANDCODE                  VARCHAR2(50) not null,
  BRANDNAME                  VARCHAR2(100) not null,
  SUBBRANDCODE               VARCHAR2(50) not null,
  SUBBRANDNAME               VARCHAR2(100) not null,
  TERRACECODE                VARCHAR2(50) not null,
  TERRACENAME                VARCHAR2(100) not null,
  PRODUCTLINE                VARCHAR2(50) not null,
  PRODUCTNAME                VARCHAR2(100) not null,
  OLDINTERNALCODE            VARCHAR2(50),
  TONNAGECODE                VARCHAR2(50),
  ENGINETYPECODE             VARCHAR2(50),
  ENGINEMANUFACTURERCODE     VARCHAR2(50),
  GEARSERIALTYPECODE         VARCHAR2(50),
  GEARSERIALMANUFACTURERCODE VARCHAR2(50),
  REARAXLEMANUFACTURERCODE   VARCHAR2(50),
  TIREFORMCODE               VARCHAR2(50),
  BRAKEMODECODE              VARCHAR2(50),
  DRIVEMODECODE              VARCHAR2(50),
  FRAMECONNETCODE            VARCHAR2(50),
  VEHICLESIZE                NUMBER(9),
  AXLEDISTANCECODE           VARCHAR2(50),
  EMISSIONSTANDARDCODE       VARCHAR2(50),
  PRODUCTFUNCTIONCODE        VARCHAR2(50),
  PRODUCTLEVELCODE           VARCHAR2(50),
  VEHICLETYPECODE            VARCHAR2(50),
  SEATINGCAPACITY            NUMBER(9),
  WEIGHT                     NUMBER(9),
  REMARK                     VARCHAR2(200),
  STATUS                     NUMBER(9) not null,
  PARAMETERDESCRIPTION       VARCHAR2(200),
  VERSION                    VARCHAR2(20),
  CONFIGURATION              VARCHAR2(50),
  ENGINECYLINDER             VARCHAR2(50),
  EMISSIONSTANDARD           VARCHAR2(50),
  MANUALAUTOMATIC            VARCHAR2(50),
  COLORCODE                  VARCHAR2(20),
  PICTURE                    VARCHAR2(200),
  CREATORID                  NUMBER(9),
  CREATORNAME                VARCHAR2(100),
  CREATETIME                 DATE,
  MODIFIERID                 NUMBER(9),
  MODIFIERNAME               VARCHAR2(100),
  MODIFYTIME                 DATE,
  ABANDONERID                NUMBER(9),
  ABANDONERNAME              VARCHAR2(100),
  ABANDONTIME                DATE,
  SYSCODE                    VARCHAR2(30) not null,
  THEDATE                    DATE not null,
  RECEIVETIME                DATE,
  HANDLESTATUS               NUMBER(9),
  HANDLETIME                 DATE,
  HANDLEMESSAGE              VARCHAR2(500)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 16
    next 8
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table DTP_PRODUCT
  add constraint PK_DTP_PRODUCT primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );



create sequence S_DTP_PRODUCT
maxvalue 9999999999999999999999999999
start with 1
increment by 1
nocache;

-- Create table
create table DTP_CUSTOMER
(
  ID                NUMBER(9),
  DMSID             VARCHAR2(38) not null,
  CODE              VARCHAR2(50) not null,
  NAME              VARCHAR2(100) not null,
  GENDER            NUMBER(9) not null,
  CUSTOMERTYPE      NUMBER(9) not null,
  CELLPHONENUMBER   VARCHAR2(50) not null,
  BIRTHDATE         DATE,
  AGE               NUMBER(9),
  IDDOCUMENTTYPE    NUMBER(9),
  IDDOCUMENTNUMBER  VARCHAR2(50),
  PROVINCENAME      VARCHAR2(100),
  CITYNAME          VARCHAR2(100),
  COUNTYNAME        VARCHAR2(100),
  FAX               VARCHAR2(50),
  IFCOMPLAIN        NUMBER(1),
  ADDRESS           VARCHAR2(200),
  POSTCODE          VARCHAR2(6),
  HOMEPHONENUMBER   VARCHAR2(50),
  OFFICEPHONENUMBER VARCHAR2(50),
  EMAIL             VARCHAR2(50),
  COMPANYNAME       VARCHAR2(100),
  BUSINESSTYPE      NUMBER(9),
  OCCUPATIONTYPE    NUMBER(9),
  JOBPOSITION       VARCHAR2(100),
  EDUCATIONLEVEL    NUMBER(9),
  DRIVINGSENIORITY  NUMBER(9),
  IFVISITBACKNEEDED NUMBER(1),
  IFTEXTACCEPTED    NUMBER(1),
  IFADACCEPTED      NUMBER(1),
  CARDNUMBER        VARCHAR2(50),
  IFVIP             NUMBER(1),
  VIPTYPE           NUMBER(9),
  BONUSPOINTS       NUMBER(9),
  PLATEOWNER        VARCHAR2(100),
  IFMARRIED         NUMBER(1),
  WEDDINGDAY        DATE,
  FAMILYINCOME      NUMBER(9),
  LOCATION          NUMBER(9),
  FAMILYCOMPOSITION VARCHAR2(100),
  REFERRAL          VARCHAR2(100),
  HOBBY             VARCHAR2(200),
  SPECIALDAY        DATE,
  SPECIALDAYDETAILS VARCHAR2(200),
  USERGENDER        NUMBER(9),
  COMPANYTYPE       NUMBER(9),
  ACQUISITORNAME    VARCHAR2(100),
  ACQUISITORPHONE   VARCHAR2(50),
  STATUS            NUMBER(9) not null,
  INCOME            NUMBER(9),
  CREATORID         NUMBER(9),
  CREATORNAME       VARCHAR2(100),
  CREATETIME        DATE,
  MODIFIERID        NUMBER(9),
  MODIFIERNAME      VARCHAR2(100),
  MODIFYTIME        DATE,
  REMARK            VARCHAR2(200),
  SYSCODE           VARCHAR2(30) not null,
  THEDATE           DATE not null,
  RECEIVETIME                DATE,
  HANDLESTATUS               NUMBER(9),
  HANDLETIME                 DATE,
  HANDLEMESSAGE              VARCHAR2(500)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 16
    next 1
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table DTP_CUSTOMER
  add constraint PK_DTP_CUSTOMER primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

-- Create sequence 
create sequence S_DTP_CUSTOMER
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
nocache;

-- Create table
create table DTP_VEHICLELINKMAN
(
  ID              NUMBER(9) not null,
  DMSID           VARCHAR2(38) not null,
  VIN             VARCHAR2(50) not null,
  CUSTOMCODE      VARCHAR2(50) not null,
  LINKMANTYPE     NUMBER(9) not null,
  NAME            VARCHAR2(100) not null,
  GENDER          NUMBER not null,
  CELLPHONENUMBER VARCHAR2(50),
  EMAIL           VARCHAR2(50),
  ADDRESS         VARCHAR2(100),
  STATUS          NUMBER(9) not null,
  CREATORID       NUMBER(9),
  CREATORNAME     VARCHAR2(100),
  CREATETIME      DATE,
  MODIFIERID      NUMBER(9),
  MODIFIERNAME    VARCHAR2(100),
  MODIFYTIME      DATE,
  SYSCODE         VARCHAR2(30) not null,
  THEDATE         DATE not null,
  RECEIVETIME     DATE,
  HANDLESTATUS    NUMBER(9),
  HANDLETIME      DATE,
  HANDLEMESSAGE   VARCHAR2(500)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 16
    next 8
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table DTP_VEHICLELINKMAN
  add constraint PK_DTP_VEHICLELINKMAN primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

-- Create sequence 
create sequence S_DTP_VEHICLELINKMAN
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
nocache;

insert into customer (ID, NAME, GENDER, CUSTOMERTYPE, AGE, REGIONID, REGIONNAME, PROVINCENAME, CITYNAME, FAX, IFCOMPLAIN, COUNTYNAME, ADDRESS, POSTCODE, CELLPHONENUMBER, HOMEPHONENUMBER, OFFICEPHONENUMBER, IDDOCUMENTTYPE, IDDOCUMENTNUMBER, BIRTHDATE, EMAIL, COMPANYNAME, BUSINESSTYPE, OCCUPATIONTYPE, JOBPOSITION, EDUCATIONLEVEL, DRIVINGSENIORITY, STATUS, INCOME, CREATORID, CREATORNAME, CREATETIME, MODIFIERID, MODIFIERNAME, MODIFYTIME, REMARK, ROWVERSION, CUSTOMERCODE)
values (-1, '无', 0, 0, null, null, '', '', '', '', 0, '', '', '', '', '', '', null, '', null, '', '', null, null, '', null, null, 1, null, 1, 'Admin', to_date('26-06-2014', 'dd-mm-yyyy'), null, '', null, '', '', '无');

insert into retainedcustomer (ID, CUSTOMERID, IFVISITBACKNEEDED, IFTEXTACCEPTED, IFADACCEPTED, CARDNUMBER, IFVIP, VIPTYPE, BONUSPOINTS, PLATEOWNER, IFMARRIED, WEDDINGDAY, FAMILYINCOME, LOCATION, FAMILYCOMPOSITION, REFERRAL, HOBBY, SPECIALDAY, SPECIALDAYDETAILS, USERGENDER, COMPANYTYPE, ACQUISITORNAME, ACQUISITORPHONE, CREATORID, CREATORNAME, CREATETIME, MODIFIERID, MODIFIERNAME, MODIFYTIME, STATUS, REMARK, ROWVERSION)
values (-1, -1, 0, 0, 0, '空', 0, 0, 0, '空', 0, to_date('01-01-1899', 'dd-mm-yyyy'), 0, 0, '空', '空', '空', to_date('01-01-1899', 'dd-mm-yyyy'), '空', 0, 0, '空', '空', 1, 'Admin', to_date('01-12-2013 22:49:18', 'dd-mm-yyyy hh24:mi:ss'), null, '', null, 1, '', '');

Insert into statusofsync values(S_STATUSOFSYNC.NEXTVAL,'DMS_PRODUCT_SYNC',0,NULL,NULL,NULL,NULL);
Insert into statusofsync values(S_STATUSOFSYNC.NEXTVAL,'DMS_VEHICLEINFORMATION_SYNC',0,NULL,NULL,NULL,NULL);
Insert into statusofsync values(S_STATUSOFSYNC.NEXTVAL,'DMS_VEHICLELINKMAN_SYNC',0,NULL,NULL,NULL,NULL);
Insert into statusofsync values(S_STATUSOFSYNC.NEXTVAL,'DMS_CUSTOMER_SYNC',0,NULL,NULL,NULL,NULL);

-- Create table
create table REPAIRWORKORDERCONFIRMINFO
(
  ID                   NUMBER(9) not null,
  VIN                  VARCHAR2(100),
  DISPATCHINGCODE      VARCHAR2(100),
  VPCDISPATCHINGCODE   VARCHAR2(100),
  DISTANCE             VARCHAR2(100),
  DEALERID             VARCHAR2(100),
  SERVICERXPOINT       VARCHAR2(100),
  SERVICERYPOINT       VARCHAR2(100),
  XPOINT               VARCHAR2(100),
  YPOINT               VARCHAR2(100),
  CONFIRMDATE          VARCHAR2(100),
  FINISHINGTIME        VARCHAR2(100),
  REPAIRCLASSIFICATION VARCHAR2(100),
  MEMO                 VARCHAR2(500),
  ARRIVALDATE          VARCHAR2(100),
  CREATORNAME          VARCHAR2(100),
  CREATETIME           VARCHAR2(100),
  MODIFIERNAME         VARCHAR2(100),
  MODIFYTIME           VARCHAR2(100),
  RECEIVETIME          DATE,
  HANDLESTATUS         NUMBER(9),
  HANDLETIME           DATE,
  HANDLEMESSAGE        VARCHAR2(500)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255;
-- Create/Recreate primary, unique and foreign key constraints 
alter table REPAIRWORKORDERCONFIRMINFO
  add constraint PK_REPAIRWORKORDERCONFIRMINFO primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255;

-- Create table
create table REPAIRWORKORDERFINISHINFO
(
  ID                 NUMBER(9) not null,
  VPCDISPATCHINGCODE VARCHAR2(100),
  VIN                VARCHAR2(100),
  REPAIRFINISHTIME   VARCHAR2(100),
  REPAIRFINISHNAME   VARCHAR2(100),
  RECEIVETIME        DATE,
  HANDLESTATUS       NUMBER(9),
  HANDLETIME         DATE,
  HANDLEMESSAGE      VARCHAR2(500)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255;
-- Create/Recreate primary, unique and foreign key constraints 
alter table REPAIRWORKORDERFINISHINFO
  add constraint PK_REPAIRWORKORDERFINISHINFO primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255;


-- Create sequence 
create sequence S_REPAIRWORKORDERFINISHINFO
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
nocache;

-- Create table
create table PartsPreSalesOrderInfo
(
  Id                NUMBER(9) not null,
  Code              VARCHAR2(200),
  DealerCode        VARCHAR2(200),
  SalesCategoryCode VARCHAR2(200),
  SparePartCode     VARCHAR2(200),
  SparePartName     VARCHAR2(200),
  OrderedQuantity   VARCHAR2(200),
  ProcessStatus     NUMBER(9),
  LastProcessTime   DATE,
  ErrorMessage      VARCHAR2(200)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 8K
    minextents 1
    maxextents unlimited
  );

-- Create/Recreate primary, unique and foreign key constraints 
alter table PartsPreSalesOrderInfo
  add constraint PK_PartsPreSalesOrderInfo primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

-- Create sequence 
create sequence S_PartsPreSalesOrderInfo
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;


-- Create table
create table SparePartInfo
(
  Id                       NUMBER(9) not null,
  Code                     VARCHAR2(200),
  Name                     VARCHAR2(200),
  CADCode                  VARCHAR2(200),
  PartsSalesCategoryName   VARCHAR2(200),
  Specification            VARCHAR2(200),
  MeasureUnit              VARCHAR2(200),
  EnglishName              VARCHAR2(100),
  Feature                  VARCHAR2(200),
  LastSubstitute           VARCHAR2(200),
  NextSubstitute           VARCHAR2(200),
  ProcessStatus            NUMBER(9),
  LastProcessTime          DATE,
  ErrorMessage             VARCHAR2(200)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 8K
    minextents 1
    maxextents unlimited
  );
  

-- Create/Recreate primary, unique and foreign key constraints 
alter table SparePartInfo
  add constraint PK_SparePartInfo primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );


-- Create sequence 
create sequence S_SparePartInfo
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;



create sequence S_SalesCategoryEPC2PMS
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

-- Create table
create table SalesCategoryEPC2PMS
(
  Id                    NUMBER(9)     not null,
  PMSSalesCategoryCode  NVARCHAR2(50) not null,
  EPCSalesCategoryName  NVARCHAR2(50) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    minextents 1
    maxextents unlimited
  );

insert into SalesCategoryEPC2PMS
  (Id,
   PMSSalesCategoryCode,
   EPCSalesCategoryName)
values
  (s_SalesCategoryEPC2PMS.nextval,
   '102111',
   '欧马可');

insert into SalesCategoryEPC2PMS
  (Id,
   PMSSalesCategoryCode,
   EPCSalesCategoryName)
values
  (s_SalesCategoryEPC2PMS.nextval,
   '104011',
   '奥铃');

insert into SalesCategoryEPC2PMS
  (Id,
   PMSSalesCategoryCode,
   EPCSalesCategoryName)
values
  (s_SalesCategoryEPC2PMS.nextval,
   '102012',
   '蒙派克');

insert into SalesCategoryEPC2PMS
  (Id,
   PMSSalesCategoryCode,
   EPCSalesCategoryName)
values
  (s_SalesCategoryEPC2PMS.nextval,
   '102012',
   '风景');

insert into SalesCategoryEPC2PMS
  (Id,
   PMSSalesCategoryCode,
   EPCSalesCategoryName)
values
  (s_SalesCategoryEPC2PMS.nextval,
   '102112',
   '萨普');

insert into SalesCategoryEPC2PMS
  (Id,
   PMSSalesCategoryCode,
   EPCSalesCategoryName)
values
  (s_SalesCategoryEPC2PMS.nextval,
   '102112',
   '拓陆者');

insert into SalesCategoryEPC2PMS
  (Id,
   PMSSalesCategoryCode,
   EPCSalesCategoryName)
values
  (s_SalesCategoryEPC2PMS.nextval,
   '999995',
   'View');

insert into SalesCategoryEPC2PMS
  (Id,
   PMSSalesCategoryCode,
   EPCSalesCategoryName)
values
  (s_SalesCategoryEPC2PMS.nextval,
   '999995',
   'Tunland');

insert into SalesCategoryEPC2PMS
  (Id,
   PMSSalesCategoryCode,
   EPCSalesCategoryName)
values
  (s_SalesCategoryEPC2PMS.nextval,
   '999995',
   'SUP');

insert into SalesCategoryEPC2PMS
  (Id,
   PMSSalesCategoryCode,
   EPCSalesCategoryName)
values
  (s_SalesCategoryEPC2PMS.nextval,
   '999996',
   'Aumark');

insert into SalesCategoryEPC2PMS
  (Id,
   PMSSalesCategoryCode,
   EPCSalesCategoryName)
values
  (s_SalesCategoryEPC2PMS.nextval,
   '999997',
   'Auman');

insert into SalesCategoryEPC2PMS
  (Id,
   PMSSalesCategoryCode,
   EPCSalesCategoryName)
values
  (s_SalesCategoryEPC2PMS.nextval,
   'A09',
   '欧曼');

insert into SalesCategoryEPC2PMS
  (Id,
   PMSSalesCategoryCode,
   EPCSalesCategoryName)
values
  (s_SalesCategoryEPC2PMS.nextval,
   'A0901',
   '欧曼');

insert into SalesCategoryEPC2PMS
  (Id,
   PMSSalesCategoryCode,
   EPCSalesCategoryName)
values
  (s_SalesCategoryEPC2PMS.nextval,
   'A16',
   '雷萨泵送');

insert into SalesCategoryEPC2PMS
  (Id,
   PMSSalesCategoryCode,
   EPCSalesCategoryName)
values
  (s_SalesCategoryEPC2PMS.nextval,
   'A1601',
   '雷萨泵送');


create table SAP_1_SYNC (syncnumber number(9),bussinessid number(9),bussinesstablename varchar2(100));
create sequence S_SAP_1_SYNC nocache;
create table SAP_2_SYNC (syncnumber number(9),bussinessid number(9),bussinesstablename varchar2(100));
create sequence S_SAP_2_SYNC nocache;
create table SAP_3_SYNC (syncnumber number(9),bussinessid number(9),bussinesstablename varchar2(100));
create sequence S_SAP_3_SYNC nocache;
create table SAP_4_SYNC (syncnumber number(9),bussinessid number(9),bussinesstablename varchar2(100));
create sequence S_SAP_4_SYNC nocache;
create table SAP_5_SYNC (syncnumber number(9),bussinessid number(9),bussinesstablename varchar2(100));
create sequence S_SAP_5_SYNC nocache;
create table SAP_6_SYNC (syncnumber number(9),bussinessid number(9),bussinesstablename varchar2(100));
create sequence S_SAP_6_SYNC nocache;
create table SAP_7_SYNC (syncnumber number(9),bussinessid number(9),bussinesstablename varchar2(100));
create sequence S_SAP_7_SYNC nocache;
create table SAP_8_SYNC (syncnumber number(9),bussinessid number(9),bussinesstablename varchar2(100));
create sequence S_SAP_8_SYNC nocache;
create table SAP_9_SYNC (syncnumber number(9),bussinessid number(9),bussinesstablename varchar2(100));
create sequence S_SAP_9_SYNC nocache;
create table SAP_10_SYNC (syncnumber number(9),bussinessid number(9),bussinesstablename varchar2(100));
create sequence S_SAP_10_SYNC nocache;
create table SAP_11_SYNC (syncnumber number(9),bussinessid number(9),bussinesstablename varchar2(100));
create sequence S_SAP_11_SYNC nocache;
create table SAP_12_SYNC (syncnumber number(9),bussinessid number(9),bussinesstablename varchar2(100));
create sequence S_SAP_12_SYNC nocache;
create table SAP_13_SYNC (syncnumber number(9),bussinessid number(9),bussinesstablename varchar2(100));
create sequence S_SAP_13_SYNC nocache;
create table SAP_14_SYNC (syncnumber number(9),bussinessid number(9),bussinesstablename varchar2(100));
create sequence S_SAP_14_SYNC nocache;
create table SAP_15_SYNC (syncnumber number(9),bussinessid number(9),bussinesstablename varchar2(100));
create sequence S_SAP_15_SYNC nocache;
create table SAP_16_SYNC (syncnumber number(9),bussinessid number(9),bussinesstablename varchar2(100));
create sequence S_SAP_16_SYNC nocache;

Insert into statusofsync values(s_statusofsync.nextval,'SAP_1_SYNC',0,'销售退货结算单结转','营销',null,null);
Insert into statusofsync values(s_statusofsync.nextval,'SAP_2_SYNC',0,'销售结算单结转','营销',null,null);
Insert into statusofsync values(s_statusofsync.nextval,'SAP_3_SYNC',0,'采购退货结算','营销',null,null);
Insert into statusofsync values(s_statusofsync.nextval,'SAP_4_SYNC',0,'销售退货结算','营销',null,null);
Insert into statusofsync values(s_statusofsync.nextval,'SAP_5_SYNC',0,'采购退货出库','营销',null,null);
Insert into statusofsync values(s_statusofsync.nextval,'SAP_6_SYNC',0,'销售退货入库','营销',null,null);
Insert into statusofsync values(s_statusofsync.nextval,'SAP_7_SYNC',0,'领用结算','营销',null,null);
Insert into statusofsync values(s_statusofsync.nextval,'SAP_8_SYNC',0,'采购结算','营销',null,null);
Insert into statusofsync values(s_statusofsync.nextval,'SAP_9_SYNC',0,'采购入库','营销',null,null);
Insert into statusofsync values(s_statusofsync.nextval,'SAP_10_SYNC',0,'销售出库','营销',null,null);
Insert into statusofsync values(s_statusofsync.nextval,'SAP_11_SYNC',0,'调拨','营销',null,null);
Insert into statusofsync values(s_statusofsync.nextval,'SAP_12_SYNC',0,'盘点','营销',null,null);
Insert into statusofsync values(s_statusofsync.nextval,'SAP_13_SYNC',0,'成本变更','营销',null,null);
Insert into statusofsync values(s_statusofsync.nextval,'SAP_14_SYNC',0,'销售结算','营销',null,null);
Insert into statusofsync values(s_statusofsync.nextval,'SAP_15_SYNC',0,'销售退货结算开红票','营销',null,null);
Insert into statusofsync values(s_statusofsync.nextval,'SAP_16_SYNC',0,'采购退货结算开红票','营销',null,null);


create table pmssapwarehouserelation
(
id number(9),
pmsid number(9),
pmscode varchar2(50),
pmsname varchar2(100),
brandname varchar2(100),
profitcenter varchar2(50),
sapcode varchar2(100)
);
create sequence s_pmssapwarehouserelation nocache;

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (1, 'AOLBJCK', '奥铃北京CDC仓库', '奥铃', '104011', '1502');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (2, 'AOLBJCK_Z', '奥铃北京CDC质量件仓库', '奥铃', '104011', '1502');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (3, 'AOLGDCK', '奥铃广东CDC仓库', '奥铃', '104011', '1505');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (4, 'AOLGDCK_Z', '奥铃广东CDC质量件仓库', '奥铃', '104011', '1505');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (5, 'AOLSDCK', '奥铃山东CDC仓库', '奥铃', '104011', '1504');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (6, 'AOLSDCK_Z', '奥铃山东CDC质量件仓库', '奥铃', '104011', '1504');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (7, 'HWCYCBJCK', '海外乘用车北京CDC仓库', '海外乘用车', '999995', '1502');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (8, 'HWCYCGDCK', '海外乘用车广东CDC仓库', '海外乘用车', '999995', '1505');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (9, 'HWCYCSDCK', '海外乘用车山东CDC仓库', '海外乘用车', '999995', '1504');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (10, 'HWQKBJCK', '海外轻卡北京CDC仓库', '海外轻卡', '999996', '1502');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (11, 'HWQKSDCK', '海外轻卡山东CDC仓库', '海外轻卡', '999996', '1504');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (12, 'HWQKGDCK', '海外轻卡广东CDC仓库', '海外轻卡', '999996', '1505');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (13, 'HWZZKBJCK', '海外中重卡北京CDC仓库', '海外中重卡', '999997', '1502');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (14, 'HWZZKSDCK', '海外中重卡山东CDC仓库', '海外中重卡', '999997', '1504');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (15, 'HWZZKGDCK', '海外中重卡广东CDC仓库', '海外中重卡', '999997', '1505');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (16, 'JPBJBCCK', '精品北京CDC包材仓库', '精品', '999998', '1502');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (17, 'JPBSCK', '精品标识仓库', '精品', '999998', '1507');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (18, 'JPGDBCCK', '精品广东CDC包材仓库', '精品', '999998', '1505');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (19, 'JPSDBCCK', '精品山东CDC包材仓库', '精品', '999998', '1504');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (20, 'JPBJCK', '精品北京CDC仓库', '精品', '999998', '1502');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (21, 'JPSDCK', '精品山东CDC仓库', '精品', '999998', '1504');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (22, 'JPGDCK', '精品广东CDC仓库', '精品', '999998', '1505');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (23, 'MPKGDCK', '蒙派克广东CDC仓库', '蒙派克风景', '102012', '1505');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (24, 'MPKGDCK_Z', '蒙派克广东CDC质量件仓库', '蒙派克风景', '102012', '1505');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (25, 'MPKBJCK', '蒙派克北京CDC仓库', '蒙派克风景', '102012', '1502');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (26, 'MPKBJCK_Z', '蒙派克北京CDC质量件仓库', '蒙派克风景', '102012', '1502');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (27, 'MPKSDCK', '蒙派克山东CDC仓库', '蒙派克风景', '102012', '1504');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (28, 'MPKSDCK_Z', '蒙派克山东CDC质量件仓库', '蒙派克风景', '102012', '1504');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (29, 'MPKJPGDCK', '蒙派克精品广东CDC仓库', '蒙派克风景', '102012', '1505');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (30, 'MPKJPBJCK', '蒙派克精品北京CDC仓库', '蒙派克风景', '102012', '1502');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (31, 'MPKJPSDCK', '蒙派克精品山东CDC仓库', '蒙派克风景', '102012', '1504');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (32, 'AUMARKBJCK', '欧马可北京CDC仓库', '欧马可', '102111', '1502');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (33, 'AUMARKBJCK_Z', '欧马可北京CDC质量件仓库', '欧马可', '102111', '1502');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (34, 'AUMARKGDCK', '欧马可广东CDC仓库', '欧马可', '102111', '1505');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (35, 'AUMARKGDCK_Z', '欧马可广东CDC质量件仓库', '欧马可', '102111', '1505');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (36, 'AUMARKSDCK', '欧马可山东CDC仓库', '欧马可', '102111', '1504');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (37, 'AUMARKSDCK_Z', '欧马可山东CDC质量件仓库', '欧马可', '102111', '1504');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (38, 'TLZBJCK', '拓陆者北京CDC仓库', '拓陆者萨普', '102112', '1502');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (39, 'TLZBJCK_Z', '拓陆者北京CDC质量件仓库', '拓陆者萨普', '102112', '1502');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (40, 'TLZSDCK', '拓陆者山东CDC仓库', '拓陆者萨普', '102112', '1504');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (41, 'TLZSDCK_Z', '拓陆者山东CDC质量件仓库', '拓陆者萨普', '102112', '1504');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (42, 'TLZGDCK', '拓陆者广东CDC仓库', '拓陆者萨普', '102112', '1505');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (43, 'TLZGDCK_Z', '拓陆者广东CDC质量件仓库', '拓陆者萨普', '102112', '1505');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (44, 'PJWLGSBJCK', '配件物流公司北京CDC仓库', '统购', '999999', '1502');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (45, 'PJWLGSBJCK_Z', '配件物流公司北京CDC质量件仓库', '统购', '999999', '1502');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (46, 'PJWLGSSDCK', '配件物流公司山东CDC仓库', '统购', '999999', '1504');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (47, 'PJWLGSSDCK_Z', '配件物流公司山东CDC质量件仓库', '统购', '999999', '1504');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (48, 'PJWLGSGDCK', '配件物流公司广东CDC仓库', '统购', '999999', '1505');

insert into pmssapwarehouserelation (ID, PMSCODE, PMSNAME, BRANDNAME, PROFITCENTER, SAPCODE)
values (49, 'PJWLGSGDCK_Z', '配件物流公司广东CDC质量件仓库', '统购', '999999', '1505');
-------------------------------------------------------------------------------------------------------------------------
update pmssapwarehouserelation set pmsid=(select Id from warehouse where code=pmssapwarehouserelation.pmscode and branchid=3007);--要区分分公司


create table SAP_YX_PAYMENTINFO
(
  ID                     NUMBER(9) not null,
  COMPANYCODE            VARCHAR2(50),
  FISCALYEAR             NUMBER(9),
  FISCALPERIOD           NUMBER(9),
  VOUCHERCREATETIME      DATE,
  TALLYTIME              DATE,
  SUPPLIERCOMPANYCODE    VARCHAR2(50),
  CUSTOMERCOMPANYCODE    VARCHAR2(50),
  PARTSSALESCATEGORYCODE VARCHAR2(50),
  GENERALLEDGER          VARCHAR2(50),
  CURRENCYCODE           VARCHAR2(5),
  AMOUNT                 NUMBER(9,2),
  ITEM1                  VARCHAR2(50),
  ITEM2                  VARCHAR2(50),
  CREATORNAME            VARCHAR2(100),
  SUMMARY                VARCHAR2(200),
  PAYMENTMETHOD          NUMBER(9),
  BILLNUMBER             VARCHAR2(50),
  BILLROWNUMBER          NUMBER(9),
  INTRUNDATE             DATE,
  INTRUNTIME             DATE,
  ISPASSBACK             NUMBER(1),
  TALLYRECORDNUMBER      VARCHAR2(50),
  ERRORMESSAGE           VARCHAR2(500),
  TALLYMARK              VARCHAR2(50),
  RECEIVETIME            DATE,
  HANDLESTATUS           NUMBER(9),
  HANDLETIME             DATE,
  HANDLEMESSAGE          VARCHAR2(500)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255;
-- Create/Recreate primary, unique and foreign key constraints 
alter table SAP_YX_PAYMENTINFO
  add constraint PK_SAP_YX_PAYMENTINFO primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255;




-- Create sequence 
create sequence S_SAP_YX_PAYMENTINFO
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
nocache;

create table ERRORTABLE_SAP_YX
(
  ID                 NUMBER(9),
  BUSSINESSID        NUMBER(9),
  BUSSINESSCODE      VARCHAR2(100),
  BUSSINESSTABLENAME VARCHAR2(100),
  SYNCTYPENAME       VARCHAR2(100),
  MESSAGE            VARCHAR2(100),
  STATUS             NUMBER(9),
  CREATORID          NUMBER(9),
  CREATORNAME        VARCHAR2(100),
  CREATETIME         DATE,
  MODIFIERID         NUMBER(9),
  MODIFIERNAME       VARCHAR2(100),
  MODIFYTIME         DATE
);
Create sequence S_ERRORTABLE_SAP_YX nocache;

-- Create table
create table SAP_YX_INVOICEVERACCOUNTINFO
(
  ID                      NUMBER(9) not null,
  BUKRS                   CHAR(4),
  GJAHR                   NUMBER(4),
  MONAT                   NUMBER(2),
  ACCOUNTINGINVOICENUMBER VARCHAR2(50),
  CODE                    VARCHAR2(50),
  INVOICEAMOUNT           NUMBER(8,2),
  INVOICETAX              NUMBER(8,2),
  INVOICEDATE             DATE,
  INVOICECODE             VARCHAR2(50),
  INVOICENUMBER           VARCHAR2(50),
  REVERSALINVOICENUMBER   VARCHAR2(50),
  CREATETIME              DATE,
  STATUS                  NUMBER(6),
  MOIDFYTIME              DATE,
  MESSAGE                 VARCHAR2(500)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255;
-- Create/Recreate primary, unique and foreign key constraints 
alter table SAP_YX_INVOICEVERACCOUNTINFO
  add constraint PK_SAPYXINVOICEVERACCOUNTINFO primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255;

create sequence s_sap_yx_invoiceveraccountinfo nocache;








-- Create table
create table SAP_YX_SALESBILLINFO
(
  ID                       NUMBER(6) not null,
  BUKRS                    CHAR(4),
  GJAHR                    NUMBER(4),
  MONAT                    NUMBER(2),
  SAPSYSINVOICENUMBER      VARCHAR2(50),
  CODE                     VARCHAR2(50),
  INVOICEAMOUNT            NUMBER(8,2),
  INVOICETAX               NUMBER(8,2),
  INVOICEDATE              DATE,
  INVOICECODE              VARCHAR2(50),
  INVOICENUMBER            VARCHAR2(50),
  REVERSALSYSINVOICENUMBER VARCHAR2(50),
  CREATETIME               DATE
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255;
-- Create/Recreate primary, unique and foreign key constraints 
alter table SAP_YX_SALESBILLINFO
  add constraint PK_SAPYXSALESBILLINFO primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255;


create sequence s_sap_yx_salesbillinfo nocache;

CREATE TABLE OCC_PARTSRETAILGUIDEPRICE_SYNC
(
  SYNCNUM NUMBER(9),
  BILLID NUMBER(9)
);
--建同步表序列
CREATE SEQUENCE S_OCC_PARTSRETAILGUIDEPRICE NOCACHE;

CREATE TABLE OCC_MARKETINGDEPARTMENT_SYNC
(
  SYNCNUM NUMBER(9),
  BILLID NUMBER(9)
);
--建同步表序列
CREATE SEQUENCE S_OCC_MARKETINGDEPARTMENT NOCACHE;

CREATE TABLE OCC_DEALERSERVICEINFO_SYNC
(
  SYNCNUM NUMBER(9),
  BILLID NUMBER(9)
);
--建同步表序列
CREATE SEQUENCE S_OCC_DEALERSERVICEINFO NOCACHE;

CREATE TABLE OCC_RETAINEDCUSTOMER_SYNC
(
  SYNCNUM NUMBER(9),
  BILLID NUMBER(9)

);
--建同步表序列
CREATE SEQUENCE S_OCC_RETAINEDCUSTOMER NOCACHE;


CREATE TABLE OCC_VEHICLEINFORMATION_SYNC
(
  SYNCNUM NUMBER(9),
  BILLID NUMBER(9)

);
--建同步表序列
CREATE SEQUENCE S_OCC_VEHICLEINFORMATION NOCACHE;

CREATE TABLE OCC_VEHICLELINKMAN_SYNC
(
  SYNCNUM NUMBER(9),
  BILLID NUMBER(9)

);
--建同步表序列
CREATE SEQUENCE S_OCC_VEHICLELINKMAN NOCACHE;

CREATE TABLE OCC_REPAIRORDER_SYNC
(
  SYNCNUM NUMBER(9),
  BILLID NUMBER(9)
);
--建同步表序列
CREATE SEQUENCE S_OCC_REPAIRORDER NOCACHE;

CREATE TABLE OCC_REPAIRCLAIMBILL_SYNC
(
  SYNCNUM NUMBER(9),
  BILLID NUMBER(9)
);
--建同步表序列
CREATE SEQUENCE S_OCC_REPAIRCLAIMBILL NOCACHE;


CREATE TABLE OCC_PARTSSUPPLIER_SYNC
(
  SYNCNUM NUMBER(9),
  BILLID NUMBER(9)
);
--建同步表序列
CREATE SEQUENCE S_OCC_PARTSSUPPLIER NOCACHE;
----------------------------------------------
 --建同步表 责任单位(责任单位信息)
CREATE TABLE OCC_RESPONSIBLEUNIT_SYNC
(
  SYNCNUM NUMBER(9),
  BILLID NUMBER(9)
);
--建同步表序列
CREATE SEQUENCE S_OCC_RESPONSIBLEUNIT NOCACHE;
----------------------------------------------
--建同步表 服务站(客户信息-服务站)
CREATE TABLE OCC_DEALER_SYNC
(
  SYNCNUM NUMBER(9),
  BILLID NUMBER(9)
);
--建同步表序列
CREATE SEQUENCE S_OCC_DEALER NOCACHE;

-- Create table
create table SparePart_SYNC
(
  BILLID  CHAR(38),
  SYNCNUM NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index SparePart_SYNCBILLID on SparePart_SYNC (BILLID)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index SparePart_SYNCSYNCNUM on SparePart_SYNC (SYNCNUM)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );


-- Create table
create table PartsSupplier_SYNC
(
  BILLID  CHAR(38),
  SYNCNUM NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index PartsSupplier_SYNCBILLID on PartsSupplier_SYNC (BILLID)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index PartsSupplier_SYNCSYNCNUM on PartsSupplier_SYNC (SYNCNUM)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

-- Create table
create table CustomerInfo_SYNC
(
  BILLID  CHAR(38),
  SYNCNUM NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index CustomerInfo_SYNCBILLID on CustomerInfo_SYNC (BILLID)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index CustomerInfo_SYNCSYNCNUM on CustomerInfo_SYNC (SYNCNUM)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

-- Create table
create table LogisticCompany_SYNC
(
  BILLID  CHAR(38),
  SYNCNUM NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index LogisticCompany_SYNCBILLID on LogisticCompany_SYNC (BILLID)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index LogisticCompany_SYNCSYNCNUM on LogisticCompany_SYNC (SYNCNUM)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

-- Create table
create table PartsPurchaseOrder_SYNC
(
  BILLID  CHAR(38),
  SYNCNUM NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index PartsPurchaseOrder_SYNCBILLID on PartsPurchaseOrder_SYNC (BILLID)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index PartsPurchaseOrder_SYNCSYNCNUM on PartsPurchaseOrder_SYNC (SYNCNUM)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

-- Create table
create table PartsSalesOrder_SYNC
(
  BILLID  CHAR(38),
  SYNCNUM NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index PartsSalesOrder_SYNCBILLID on PartsSalesOrder_SYNC (BILLID)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index PartsSalesOrder_SYNCSYNCNUM on PartsSalesOrder_SYNC (SYNCNUM)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Create table
create table PartsInboundPlan_SYNC
(
  BILLID  CHAR(38),
  SYNCNUM NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index PartsInboundPlan_SYNCBILLID on PartsInboundPlan_SYNC (BILLID)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index PartsInboundPlan_SYNCSYNCNUM on PartsInboundPlan_SYNC (SYNCNUM)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

-- Create table
create table PartsOutboundPlan_sync
(
  BILLID  CHAR(38),
  SYNCNUM NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index PartsOutboundPlan_SYNCBILLID on PartsOutboundPlan_SYNC (BILLID)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index PartsOutboundPlan_SYNCSYNCNUM on PartsOutboundPlan_SYNC (SYNCNUM)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

-- Create table
create table PartsShippingOrder_SYNC
(
  BILLID  CHAR(38),
  SYNCNUM NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index PartsShippingOrder_SYNCBILLID on PartsShippingOrder_SYNC (BILLID)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index PartsShippingOrder_SYNCSYNCNUM on PartsShippingOrder_SYNC (SYNCNUM)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

--递增数
create sequence CustomerInfo_increase
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;

--递增数
create sequence LogisticCompany_increase
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;

--递增数
create sequence  PartsInboundPlan_increase
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;

--递增数
create sequence  PartsOutboundPlan_increase
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;

--递增数
create sequence PartsPurchaseOrder_increase
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;

--递增数
create sequence  PartsSalesOrder_increase
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;

--递增数
create sequence  PartsShippingOrder_increase
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;

--递增数
create sequence PartsSupplier_increase
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;

--递增数
create sequence SparePart_increase
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;

-- Create table
create table t_in_SYNC
(
  BILLID  CHAR(38),
  SYNCNUM NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index t_in_SYNCBILLID on t_in_SYNC (BILLID)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index t_in_SYNCSYNCNUM on t_in_SYNC (SYNCNUM)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

-- Create table
create table t_inventory_adjustment_SYNC
(
  BILLID  CHAR(38),
  SYNCNUM NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index t_in_adjustment_SYNCBILLID on t_inventory_adjustment_SYNC (BILLID)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index t_in_adjustment_SYNCSYNCNUM on t_inventory_adjustment_SYNC (SYNCNUM)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );


-- Create table
create table t_shippingorderhead_SYNC
(
  BILLID  CHAR(38),
  SYNCNUM NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index t_shippingorderheadBILLID on t_shippingorderhead_SYNC (BILLID)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index t_shippingorderheadSYNCNUM on t_shippingorderhead_SYNC (SYNCNUM)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );


-- Create table
create table t_warehouse_SYNC
(
  BILLID  CHAR(38),
  SYNCNUM NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index t_warehouseBILLID on t_warehouse_SYNC (BILLID)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index t_warehouseSYNCNUM on t_warehouse_SYNC (SYNCNUM)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

--递增数
create sequence  t_in_increase
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;

--递增数
create sequence  t_adjustment_increase
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;


--递增数
create sequence  t_shippingorderhead_increase
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;

--递增数
create sequence  t_in_increase
minvalue 1
maxvalue 999999999999999999999999999
start with 1
increment by 1
nocache;

create or replace package SYNCWMSINOUT is
  procedure SYNCIn;
  procedure Synctraffic;
  procedure synccheck;
  Procedure Syncwarehouses;
end SYNCWMSINOUT;
/
create or replace package body SYNCWMSINOUT is

  procedure SYNCIn as
    type T_indetailsset is table of t_indetails%rowtype index by binary_integer;
    Vtable_indetails        T_indetailsset;
    Vdetailno               number(9, 0);
    Vi                      number(9, 0);
    FWarehouseAreaId        number(9);
    FWarehouseAreaCode      varchar2(40);
    FIPWarehouseCode        varchar2(25);
    FAreaCategoryId         number(9);
    FIPPartsSalesCategoryId number(9);
    FIPPartId               number(9);
    FIPDInspectedQuantity   number(9);
    FIPDPlannedAmount       number(9);
    FIPWarehouseId          number(9);
    FIPId                   number(9);
    FErrormsg               varchar2(1000);
    FPartsStockId           number(9);
    FIPStorageCompanyId     number(9);
    FIPStorageCompanyType   varchar2(40);
    FIPStatus               number(3);
    FIPDPrice               number(19, 4);
    FIPBranchId             number(9);
    FPPPrice                number(19, 4);
    FIsAllFinished          number(9);
    FParentId               nvarchar2(25);
    FMyException exception;
    FComplateId            integer;
    FNewCode               varchar2(50);
    FNewCodePICB           varchar2(50);
    FPIBStorageCompanyCode varchar2(50);
  
    cursor getT_ins is
      select *
        from t_in
       where isfinished = 0
         and syscode = 'NPMS'
       order by OBJID;
  
    cursor getT_indetails(FParentIds varchar2) is
      select t_indetails.*, row_number() over(order by ParentId asc) rno
        from t_indetails
       inner join t_in
          on t_indetails.ParentID = t_in.OBJID
       where t_in.isfinished = 0
         and t_in.syscode = 'NPMS'
         and t_indetails.ParentID = FParentIds;
  
    cursor getFinshDetails(FIPId varchar2) is
      select *
        from PartsInboundPlanDetail
       where PartsInboundPlanId = FIPId;
  
  begin
  
    Vtable_indetails.delete;
    FNewCode := 'RK{CORPCODE}' || to_char(sysdate, 'yyyymmdd') ||
                '{SERIAL}';
    begin
      select Id
        into FComplateId
        from CodeTemplate
       where name = 'PartsInboundCheckBill'
         and IsActived = 1;
    exception
      when NO_DATA_FOUND then
        FErrormsg := '未找到名称为"PartsInboundCheckBill"的有效编码规则。';
        raise FMyException;
      when others then
        raise;
    end;
  
    --循环处理主单数据
    for C_getT_ins in getT_ins loop
      begin
        FParentId := C_getT_ins.OBJID;
        for C_getT_indetails in getT_indetails(FParentId) loop
          Vdetailno := C_getT_indetails.rno;
          Vtable_indetails(Vdetailno).ParentID := C_getT_indetails.ParentID;
          Vtable_indetails(Vdetailno).Materialcode := C_getT_indetails.Materialcode;
          Vtable_indetails(Vdetailno).Materialname := C_getT_indetails.Materialname;
          Vtable_indetails(Vdetailno).Qty := C_getT_indetails.Qty;
          Vtable_indetails(Vdetailno).Save_Date_Time_Stamp := C_getT_indetails.Save_Date_Time_Stamp;
        end loop;
        FErrormsg := '';
        --生成编号
        FNewCodePICB := regexp_replace(regexp_replace(FNewCode,
                                                      '{CORPCODE}',
                                                      FPIBStorageCompanyCode),
                                       '{SERIAL}',
                                       lpad(to_char(GetSerial(FComplateId,
                                                              trunc(sysdate,
                                                                    'DD'),
                                                              FPIBStorageCompanyCode)),
                                            6,
                                            '0'));
      
        --校验并获取入库计划单
        --校验并获取入库仓库ID
        begin
          select PartsInboundPlan.Id     as FIPId,
                 PartsInboundPlan.Status as FIPStatus,
                 PartsInboundPlan.WarehouseId          as FIPWarehouseId,
                 PartsInboundPlan.StorageCompanyId     as FIPStorageCompanyId,
                 PartsInboundPlan.StorageCompanyType   as FIPStorageCompanyType,
                 PartsInboundPlan.Partssalescategoryid as FIPPartsSalesCategoryId,
                 PartsInboundPlan.Branchid as FIPBranchId
            into FIPId, FIPStatus,
                 FIPWarehouseId,
                 FIPStorageCompanyId,
                 FIPStorageCompanyType,
                 FIPPartsSalesCategoryId,
                 FIPBranchId
            from PartsInboundPlan
           where Code = C_getT_ins.Sourcecode;
        
        exception
          when no_data_found then
            FIPId := null;
            FIPWarehouseId        := null;
            FIPStorageCompanyId   := null;
            FIPStorageCompanyType := null;
        end;
      
        --校验并获取入库仓库编号
        begin
          select Warehouse.code
            into FIPWarehouseCode
            from Warehouse
           where Warehouse.Id = FIPWarehouseId;
        
        exception
          when no_data_found then
            FIPWarehouseCode := null;
        end;
      
        --如果入库计划单存在则进行处理否则记录错误日志
        if FIPId is not null then
          begin
          
            --校验入库计划单状态(1:新建 4:部分检验)
            if FIPStatus not in (1, 4) then
              begin
                FErrormsg := '入库异常[入库计划单状态错误]';
                raise FMyException;
              end;
            end if;
          
            --校验入库仓库
          
            if FIPWarehouseCode = C_getT_ins.WarehouseCode then
              begin
                if (Vtable_indetails.Count > 0) then
                  begin
                    --如果存在入库清单，则生成PMS入库单入库检验单。
                    insert into PartsInboundCheckBill
                      (Id,
                       Code,
                       PartsInboundPlanId,
                       WarehouseId,
                       WarehouseCode,
                       WarehouseName,
                       StorageCompanyId,
                       StorageCompanyCode,
                       StorageCompanyName,
                       StorageCompanyType,
                       PartsSalesCategoryId,
                       BranchId,
                       BranchCode,
                       BranchName,
                       CounterpartCompanyId,
                       CounterpartCompanyCode,
                       CounterpartCompanyName,
                       InboundType,
                       OriginalRequirementBillId,
                       OriginalRequirementBillCode,
                       OriginalRequirementBillType,
                       Status,
                       SettlementStatus,
                       Remark,
                       CreatorId,
                       CreatorName,
                       CreateTime)
                      select S_PartsInboundCheckBill.NEXTVAL,
                             FNewCodePICB,
                             PartsInboundPlan.Id,
                             PartsInboundPlan.WarehouseId,
                             PartsInboundPlan.WarehouseCode,
                             PartsInboundPlan.WarehouseName,
                             PartsInboundPlan.StorageCompanyId,
                             PartsInboundPlan.StorageCompanyCode,
                             PartsInboundPlan.StorageCompanyName,
                             PartsInboundPlan.StorageCompanyType,
                             PartsInboundPlan.PartsSalesCategoryId,
                             PartsInboundPlan.BranchId,
                             PartsInboundPlan.BranchCode,
                             PartsInboundPlan.BranchName,
                             PartsInboundPlan.CounterpartCompanyId,
                             PartsInboundPlan.CounterpartCompanyCode,
                             PartsInboundPlan.CounterpartCompanyName,
                             PartsInboundPlan.InboundType,
                             PartsInboundPlan.OriginalRequirementBillId,
                             PartsInboundPlan.OriginalRequirementBillCode,
                             PartsInboundPlan.OriginalRequirementBillType,
                             1, --新建
                             2, --待结算
                             PartsInboundPlan.Remark,
                             1,
                             'WMSPMSInterface',
                             sysdate
                        from PartsInboundPlan
                       where PartsInboundPlan.Id = FIPId;
                  
                    for Vi in 1 .. Vtable_indetails.count loop
                      --校验配件的有效
                      begin
                        select PartsInboundPlanDetail.SparePartId
                          into FIPPartId
                          from PartsInboundPlanDetail
                         where PartsInboundPlanDetail.PartsInboundPlanId =
                               FIPId
                           and PartsInboundPlanDetail.SparePartId =
                               (select Id
                                  from SparePart
                                 where Code = Vtable_indetails(Vi)
                                      .Materialcode);
                      
                      exception
                        when NO_Data_Found then
                          FIPPartId := null;
                        
                      end;
                    
                      --如果配件有效则开始处理清单数据。
                      if FIPPartId is not null then
                        begin
                          --获取入库计划单的确认数量
                          select nvl(PartsInboundPlanDetail.InspectedQuantity,
                                     0) as FIPDInspectedQuantity,
                                 nvl(PartsInboundPlanDetail.PlannedAmount,
                                     0) as FIPDPlannedAmount,
                                 nvl(PRICE, 0) as PRICE
                            into FIPDInspectedQuantity,
                                 FIPDPlannedAmount,
                                 FIPDPrice
                            from PartsInboundPlanDetail
                           where PartsInboundPlanDetail.PartsInboundPlanId =
                                 FIPId
                             and PartsInboundPlanDetail.SparePartId =
                                 FIPPartId;
                          if FIPDPrice < 0 then
                            begin
                              FErrormsg := '配件价格小于0，请进行核对！配件图号：' || Vtable_indetails(Vi)
                                          .Materialcode;
                              raise FMyException;
                            end;
                          end if;
                        
                          begin
                            --校验计划价
                            select t.plannedprice
                              into FPPPrice
                              from partsplannedprice t
                             where t.partssalescategoryid =
                                   FIPPartsSalesCategoryId
                               and t.SparePartId = FIPPartId;
                            if FPPPrice < 0 then
                              begin
                                FErrormsg := '配件计划价格小于0，请进行核对！配件图号：' || Vtable_indetails(Vi)
                                            .Materialcode;
                                raise FMyException;
                              end;
                            end if;
                          
                          exception
                            when no_data_found then
                              FErrormsg := '计划价不存在请维护' || Vtable_indetails(Vi)
                                          .Materialcode;
                              raise FMyException;
                          end;
                        
                          --清单数量校验
                          if (FIPDPlannedAmount >=
                             FIPDInspectedQuantity + Vtable_indetails(Vi).qty) then
                            begin
                              update PartsInboundPlanDetail
                                 set InspectedQuantity = nvl(InspectedQuantity,
                                                             0) + Vtable_indetails(Vi).qty
                               where PartsInboundPlanDetail.PartsInboundPlanId =
                                     FIPId
                                 and PartsInboundPlanDetail.SparePartId =
                                     FIPPartId;
                            end;
                          
                          end if;
                        
                          if (FIPDPlannedAmount <
                             FIPDInspectedQuantity + Vtable_indetails(Vi).qty) then
                            begin
                              FErrormsg := '确认量大于计划量，请进行核对！';
                              raise FMyException;
                            end;
                          
                          end if;
                        
                          begin
                            select WarehouseArea.Id, WarehouseArea.Code,WarehouseArea.AreaCategoryId
                              into FWarehouseAreaId, FWarehouseAreaCode,FAreaCategoryId
                              from WarehouseArea
                             inner join WarehouseAreaCategory
                                on WarehouseArea.AreaCategoryId =
                                   WarehouseAreaCategory.Id
                             where WarehouseArea.WarehouseId =
                                   FIPWarehouseId
                               and WarehouseArea.Areakind = 3
                               and WarehouseAreaCategory.Category = 1;
                          
                          exception
                            when no_data_found then
                              FWarehouseAreaId := null;
                          end;
                        
                          /*
                           1、更新配件入库计划、入库计划清单
                               检验量=配件入库检验单清单.检验量（根据 配件Id）
                          */
                          if FWarehouseAreaId is not null then
                            begin
                              begin
                                select PartsStock.Id
                                  into FPartsStockId
                                  from PartsStock
                                 where PartsStock.WarehouseId =
                                       FIPWarehouseId
                                   and PartsStock.PartId = FIPPartId
                                   and PartsStock.WarehouseAreaId =
                                       FWarehouseAreaId;
                              
                              exception
                                when no_data_found then
                                  FPartsStockId := null;
                              end;
                            
                              if FPartsStockId is null then
                                begin
                                  insert into PartsStock
                                    (Id,
                                     WarehouseId,
                                     StorageCompanyId,
                                     StorageCompanyType,
                                     BranchId,
                                     WarehouseAreaId,
                                     PartId,
                                     WarehouseAreaCategoryId,
                                     Quantity,
                                     CreatorId,
                                     CreateTime,
                                     CreatorName)
                                  values
                                    (S_PartsStock.NextVAL,
                                     FIPWarehouseId,
                                     FIPStorageCompanyId,
                                     FIPStorageCompanyType,
                                     FIPBranchId,
                                     FWarehouseAreaId,
                                     FIPPartId,
                                     FAreaCategoryId,
                                     Vtable_indetails(Vi).qty,
                                     1,
                                     sysdate,
                                     'WMS');
                                
                                end;
                              else
                                begin
                                  update PartsStock
                                     set Quantity = nvl(Quantity, 0) + Vtable_indetails(Vi).qty
                                   where PartsStock.Id = FPartsStockId;
                                
                                end;
                              end if;
                            
                              insert into PartsInboundCheckBillDetail
                                (Id,
                                 PartsInboundCheckBillId,
                                 SparePartId,
                                 SparePartCode,
                                 SparePartName,
                                 WarehouseAreaId,
                                 WarehouseAreaCode,
                                 InspectedQuantity,
                                 SettlementPrice,
                                 CostPrice,
                                 Remark)
                                select S_PartsInboundCheckBillDetail.NEXTVAL,
                                       S_PartsInboundCheckBill.CURRVAL,
                                       SparePartId,
                                       SparePartCode,
                                       SparePartName,
                                       FWarehouseAreaId,
                                       FWarehouseAreaCode,
                                       Vtable_indetails(Vi).qty,
                                       PartsInboundPlanDetail.Price,
                                       FPPPrice,
                                       PartsInboundPlanDetail.Remark
                                  from PartsInboundPlanDetail
                                 where PartsInboundPlanDetail.PartsInboundPlanId =
                                       FIPId
                                   and PartsInboundPlanDetail.SparePartId =
                                       FIPPartId;
                            end;
                          else
                            begin
                              FErrormsg := 'PMS仓库对应唯一库位未维护，请核对！';
                            
                              raise FMyException;
                            end;
                          
                          end if;
                        
                        end;
                      
                      else
                        begin
                          FErrormsg := '清单内配件与入库计划单内记录不符，请核对！';
                        
                          raise FMyException;
                        end;
                      
                      end if;
                    
                    end loop;
                  
                    --------------部分清单强制完成-------------------
                    if C_getT_ins.USER_DEF4 = 'Y' then
                      begin
                        for c_getFinshDetails in getFinshDetails(FIPId) loop
                          --获取入库计划单的确认数量
                          select nvl(PartsInboundPlanDetail.InspectedQuantity,
                                     0) as FIPDInspectedQuantity,
                                 nvl(PartsInboundPlanDetail.PlannedAmount,
                                     0) as FIPDPlannedAmount
                            into FIPDInspectedQuantity, FIPDPlannedAmount
                            from PartsInboundPlanDetail
                           where PartsInboundPlanDetail.PartsInboundPlanId =
                                 FIPId
                             and PartsInboundPlanDetail.SparePartId =
                                 c_getFinshDetails.SparePartId;
                        
                        end loop;
                      
                      end;
                    
                    end if;
                    --------------部分清单强制完成-------------------
                  end;
                end if;
              
                ---------------无清单强制完成--------------------
                if Vtable_indetails.count = 0 then
                  begin
                    if C_getT_ins.USER_DEF4 <> 'Y' then
                      begin
                        FErrormsg := '入库清单数量为空，请核对！';
                        raise FMyException;
                      end;
                    end if;
                  end;
                end if;
                ---------------无清单强制完成--------------------
              end;
            else
              begin
                FErrormsg := '仓库编号与入库计划单记录不符，请核对！';
                raise FMyException;
              end;
            
            end if;
          
          end;
        else
          begin
            FErrormsg := '入库计划单编号不存在，请核对！';
            raise FMyException;
          end;
        
        end if;
      
        --校验入库计划单是否已经全部完成
        select sum(PlannedAmount) - sum(nvl(InspectedQuantity, 0)) as isfinished
          into FIsAllFinished
          from PartsInboundPlanDetail
         where PartsInboundPlanId = FIPId
         group by PartsInboundPlanId;
      
        if C_getT_ins.USER_DEF4 = 'Y' then
          begin
            update PartsInboundPlan
               set PartsInboundPlan.Status       = 3,
                   PartsInboundPlan.ModifyTime   = sysdate,
                   PartsInboundPlan.ModifierId   = 1,
                   PartsInboundPlan.ModifierName = 'WMS',
                   Remark                        = Remark ||
                                                   ' [该单据已经由业务人员WMS强制完成]'
             where PartsInboundPlan.Id = FIPId;
          end;
        else
          begin
            begin
              if FIsAllFinished = 0 then
                begin
                  update PartsInboundPlan
                     set PartsInboundPlan.Status       = 2,
                         PartsInboundPlan.ModifyTime   = sysdate,
                         PartsInboundPlan.ModifierId   = 1,
                         PartsInboundPlan.ModifierName = 'WMS'
                   where PartsInboundPlan.Id = FIPId;
                end;
              else
                begin
                  update PartsInboundPlan
                     set PartsInboundPlan.Status       = 4,
                         PartsInboundPlan.ModifyTime   = sysdate,
                         PartsInboundPlan.ModifierId   = 1,
                         PartsInboundPlan.ModifierName = 'WMS'
                   where PartsInboundPlan.Id = FIPId;
                end;
              end if;
            end;
          end;
        end if;
      
        update t_in
           set t_in.Isfinished = 1
         where t_in.OBJID = C_getT_ins.OBJID;
      
      exception
        when FMyException then
          begin
            rollback;
            insert into SYNCWMSINOUTLOGINFOS
              (OBJID, syncdate, code, synctype, synccode, INOUTCODE)
            values
              (getregguidstring,
               sysdate,
               FErrormsg,
               1,
               C_getT_ins.OBJID,
               C_getT_ins.SourceCode);
          
            update t_in
               set t_in.Isfinished = 2
             where t_in.OBJID = C_getT_ins.OBJID;
          
          end;
        when others then
          begin
            rollback;
            FErrormsg := sqlerrm;
            insert into SYNCWMSINOUTLOGINFOS
              (OBJID, syncdate, code, synctype, synccode, INOUTCODE)
            values
              (getregguidstring,
               sysdate,
               FErrormsg,
               1,
               C_getT_ins.OBJID,
               C_getT_ins.SourceCode);
          
            update t_in
               set t_in.Isfinished = 2
             where t_in.OBJID = C_getT_ins.OBJID;
          
          end;
      end;
    
      Vtable_indetails.delete;
      commit;
    end loop;
  end;

  procedure Synctraffic as
    Vdetailno             number(9, 0);
    Vi                    number(9, 0);
    FWarehouseAreaId      number(9);
    FWarehouseAreaCode    varchar2(40);
    FNewcodepso           varchar2(40);
    FNewcodeck            varchar2(40);
    FOutplanid            varchar2(40);
    FStorageCompanyId     number(9, 0);
    FPartsSalesCategoryId number(9, 0);
    FOutwarehouseid       varchar2(40);
    FStoragecenter        varchar2(25);
    FFinishstatus         number(9, 0);
    FErrormsg             varchar2(1000);
    FShiptypecodevalue    number(9);
    FFinishqty            number(9, 0);
    FPlanqty              number(9, 0);
    FSparePartId          number(9, 0);
    FBranchid             number(9, 0);
    FCostPrice            number(9, 0);
    FAmount               number(9, 0);
    FEstimateddueamount   number(9, 0);
    FCustomerAccountId    number(9, 0);
    FOutboundType         number(9, 0);
    FComplateCKId         number(9, 0);
    FComplatePSOId        number(9, 0);
    FStorageCompanyCode   varchar2(40);
    FParentId             nvarchar2(25);
    FMyexception exception;
    type t_Shippingorderdetailset is table of t_Shippingorderdetails%rowtype index by binary_integer;
    Vtable_Shippingorderdetails t_Shippingorderdetailset;
  
    cursor Gett_Shippingorderhead is
      select t_Shippingorderhead.*
        from t_Shippingorderhead
       where User_Def1 = 'NPMS'
         and isfinished = 0
       order by Interface_Record_Id;
  
    cursor Gett_Shippingorderdetails(Parentid varchar2) is
      select t_Shippingorderdetails.*,
             Row_Number() Over(order by Parentid asc) Rno
        from t_Shippingorderdetails
       where t_Shippingorderdetails.Interface_Link_Id = Parentid;
  begin
    Vtable_Shippingorderdetails.delete;
    FNewCodeCK  := 'CK{CORPCODE}' || to_char(sysdate, 'yyyymmdd') ||
                   '{SERIAL}';
    FNewCodePSO := 'PSO{CORPCODE}' || to_char(sysdate, 'yyyymmdd') ||
                   '{SERIAL}';
    begin
      select Id
        into FComplateCKId
        from CodeTemplate
       where name = 'PartsOutboundBill'
         and IsActived = 1;
    exception
      when NO_DATA_FOUND then
        FErrormsg := '未找到名称为"PartsOutboundBill"的有效编码规则。';
        raise FMyException;
      when others then
        raise;
    end;
  
    begin
      select Id
        into FComplatePSOId
        from CodeTemplate
       where name = 'PartsShippingOrder'
         and IsActived = 1;
    exception
      when NO_DATA_FOUND then
        FErrormsg := '未找到名称为"PartsShippingOrder"的有效编码规则。';
        raise FMyException;
      when others then
        raise;
    end;
                   
    for c_Gett_Shippingorderhead in Gett_Shippingorderhead loop
      begin
        
         begin
          select pop.Id,
                 pop.Branchid,
                 pop.StorageCompanyId,
                 Pop.PartsSalesCategoryId,
                 Nvl(Pop.CustomerAccountId, 0),
                 Pop.OutboundType,
                 Pop.StorageCompanyCode,
                 pop.Warehouseid
            into FOutplanid,
                 FBranchid,
                 FStorageCompanyId,
                 FPartsSalesCategoryId,
                 FCustomerAccountId,
                 FOutboundType,
                 FStorageCompanyCode,
                 FOutwarehouseid
            from Partsoutboundplan Pop
           where pop.Code = c_Gett_Shippingorderhead.User_Def3;
        exception
          when No_Data_Found then
            FErrormsg := '未找到出库计划';
            raise FMyException;
          when others then
            raise;
        end;
        
        FNewcodeck  := regexp_replace(regexp_replace(FNewCodeCk,
                                                 '{CORPCODE}',
                                                 FStorageCompanyCode),
                                  '{SERIAL}',
                                  lpad(to_char(GetSerial(FComplateCKId,
                                                         trunc(sysdate, 'DD'),
                                                         FStorageCompanyCode)),
                                       6,
                                       '0'));
    FNewcodepso := regexp_replace(regexp_replace(FNewCodePSO,
                                                 '{CORPCODE}',
                                                 FStorageCompanyCode),
                                  '{SERIAL}',
                                  lpad(to_char(GetSerial(FComplatePSOId,
                                                         trunc(sysdate, 'DD'),
                                                         FStorageCompanyCode)),
                                       4,
                                       '0'));
        
        FFinishstatus := 1; --0:未出库,1:出库完成，>1:部分出库
        FErrormsg     := '';
        FParentId     := c_Gett_Shippingorderhead.Interface_Record_Id;
        for C_getT_Shippingorderdetails in Gett_Shippingorderdetails(FParentId) loop
          Vdetailno := C_getT_Shippingorderdetails.rno;
          Vtable_Shippingorderdetails(Vdetailno).Interface_Link_Id := C_getT_Shippingorderdetails.Interface_Link_Id;
          Vtable_Shippingorderdetails(Vdetailno).ITEM := C_getT_Shippingorderdetails.ITEM;
          Vtable_Shippingorderdetails(Vdetailno).TOTAL_QTY := C_getT_Shippingorderdetails.TOTAL_QTY;
        end loop;
      
        begin
          select w.storagecenter
            into FStoragecenter
            from Warehouse w
           inner join wms2pmswarehouse w2p
              on w.code = w2p.pmswarehousecode
           where w2p.pmspartssalescategoryname =
                 c_Gett_Shippingorderhead.Company
             and w2p.wmswarehousecode = c_Gett_Shippingorderhead.Warehouse;
        exception
          when No_Data_Found then
            FErrormsg := '仓库编号与出库计划单记录不符，请核对！';
            raise FMyException;
          when others then
            raise;
        end;
      
        if trim(c_Gett_Shippingorderhead.User_Def5) = '公路货运' then
          FShiptypecodevalue := 1;
        elsif trim(c_Gett_Shippingorderhead.User_Def5) = '公路客运' then
          FShiptypecodevalue := 2;
        elsif trim(c_Gett_Shippingorderhead.User_Def5) = '铁路集装箱' then
          FShiptypecodevalue := 3;
        elsif trim(c_Gett_Shippingorderhead.User_Def5) = '铁路快件' then
          FShiptypecodevalue := 4;
        elsif trim(c_Gett_Shippingorderhead.User_Def5) = '特快专递' then
          FShiptypecodevalue := 5;
        elsif trim(c_Gett_Shippingorderhead.User_Def5) = '邮寄包裹' then
          FShiptypecodevalue := 6;
        elsif trim(c_Gett_Shippingorderhead.User_Def5) = '空运' then
          FShiptypecodevalue := 7;
        elsif trim(c_Gett_Shippingorderhead.User_Def5) = '客户自提' then
          FShiptypecodevalue := 8;
        elsif trim(c_Gett_Shippingorderhead.User_Def5) = '专车发运' then
          FShiptypecodevalue := 9;
        end if;
      
        if (Vtable_Shippingorderdetails.Count > 0) then
          begin
            --配件出库单
            insert into Partsoutboundbill
              (Id,
               Code,
               Partsoutboundplanid,
               Warehouseid,
               Warehousecode,
               Warehousename,
               Storagecompanyid,
               Storagecompanycode,
               Storagecompanyname,
               Storagecompanytype,
               Branchid,
               Branchcode,
               Branchname,
               Partssalescategoryid,
               Partssalesordertypeid,
               Partssalesordertypename,
               Counterpartcompanyid,
               Counterpartcompanycode,
               Counterpartcompanyname,
               Receivingcompanyid,
               Receivingcompanycode,
               Receivingcompanyname,
               Receivingwarehouseid,
               Receivingwarehousecode,
               Receivingwarehousename,
               Outboundtype,
               Shippingmethod,
               Customeraccountid,
               Originalrequirementbillid,
               Originalrequirementbilltype,
               Originalrequirementbillcode,
               Settlementstatus,
               Creatorid,
               Creatorname,
               Createtime)
              select s_Partsoutboundbill.Nextval,
                     FNewcodeck,
                     Pop.Id,
                     Pop.Warehouseid,
                     Pop.Warehousecode,
                     Pop.Warehousename,
                     Pop.Storagecompanyid,
                     Pop.Storagecompanycode,
                     Pop.Storagecompanyname,
                     Pop.Storagecompanytype,
                     Pop.Branchid,
                     Pop.Branchcode,
                     Pop.Branchname,
                     Pop.Partssalescategoryid,
                     pop.Partssalesordertypeid,
                     pop.Partssalesordertypename,
                     Pop.Counterpartcompanyid,
                     Pop.Counterpartcompanycode,
                     Pop.Counterpartcompanyname,
                     Pop.ReceivingCompanyId,
                     Pop.ReceivingCompanyCode,
                     Pop.ReceivingCompanyName,
                     Pop.Receivingwarehouseid,
                     Pop.Receivingwarehousecode,
                     Pop.Receivingwarehousename,
                     Pop.Outboundtype,
                     Pop.Shippingmethod,
                     Pop.Customeraccountid,
                     Pop.Originalrequirementbillid,
                     Pop.Originalrequirementbilltype,
                     Pop.Originalrequirementbillcode,
                     2,
                     1,
                     'WMSPMSInterface',
                     sysdate
                from Partsoutboundplan Pop
               where Pop.Id = FOutplanid;
          
            if FOutboundType =1 or FOutboundType=4 then
            
              begin
                --配件发运单
                insert into Partsshippingorder
                  (Id,
                   Code,
                   type,
                   Branchid,
                   Shippingcompanyid,
                   Shippingcompanycode,
                   Shippingcompanyname,
                   Settlementcompanyid,
                   Settlementcompanycode,
                   Settlementcompanyname,
                   Receivingcompanyid,
                   Receivingcompanycode,
                   Invoicereceivesalecateid,
                   Invoicereceivesalecatename,
                   Receivingcompanyname,
                   Warehouseid,
                   Warehousecode,
                   Warehousename,
                   Receivingwarehouseid,
                   Receivingwarehousecode,
                   Receivingwarehousename,
                   Receivingaddress,
                   Originalrequirementbillid,
                   Originalrequirementbillcode,
                   Originalrequirementbilltype,
                   Shippingmethod,
                   Requestedarrivaldate,
                   Shippingdate,
                   Status,
                   Creatorid,
                   Creatorname,
                   Createtime)
                  select s_Partsshippingorder.Nextval,
                         FNewcodepso,
                         FOutboundType,
                         Pop.Branchid,
                         Pop.Storagecompanyid,
                         Pop.Storagecompanycode,
                         Pop.Storagecompanyname,
                         Pop.Counterpartcompanyid,
                         Pop.Counterpartcompanycode,
                         Pop.Counterpartcompanyname,
                         Pop.Receivingcompanyid,
                         Pop.Receivingcompanycode,
                         Pop.Partssalescategoryid,
                         (select name
                            from Partssalescategory
                           where Partssalescategory.Id =
                                 Pop.Partssalescategoryid),
                         Pop.Receivingcompanyname,
                         Pop.Warehouseid,
                         Pop.Warehousecode,
                         Pop.Warehousename,
                         Pop.Receivingwarehouseid,
                         Pop.Receivingwarehousecode,
                         Pop.Receivingwarehousename,
                         c_Gett_Shippingorderhead.Ship_To_Address1,
                         Pop.Originalrequirementbillid,
                         Pop.Originalrequirementbillcode,
                         Pop.Originalrequirementbilltype,
                         Pop.Shippingmethod,
                         c_Gett_Shippingorderhead.Actual_Departure_Date_Time,
                         c_Gett_Shippingorderhead.Scheduled_Ship_Date,
                         1,
                         1,
                         'WMSPMSInterface',
                         sysdate
                    from Partsoutboundplan Pop
                   where Pop.Id = FOutplanid;
              
                --配件发运单关联单
                insert into Partsshippingorderref
                  (Id,Partsshippingorderid, Partsoutboundbillid)
                values
                  (s_Partsshippingorderref.Nextval,s_Partsshippingorder.Currval,
                   s_Partsoutboundbill.Currval);
              
              end;
            end if;
          
             for Vi in 1 .. Vtable_Shippingorderdetails.count
                loop
              select Nvl(pd.OutboundFulfillment, 0) Finishqty,
                     Nvl(Pd.Plannedamount, 0) Planqty,
                     pd.sparepartid as FSparePartId
                into FFinishqty, FPlanqty, FSparePartId
                from Partsoutboundplandetail Pd
               INNER join Sparepart
                  on Sparepart.Id = Pd.Sparepartid
               where Pd.Partsoutboundplanid = FOutplanid
                 and Sparepart.Code = Vtable_Shippingorderdetails(Vi).Item;
            
              --查询成本价
              begin
                select ppp.PlannedPrice
                  into FCostPrice
                  from PartsPlannedPrice ppp
                 where ppp.OwnerCompanyId = FStorageCompanyId
                   and ppp.PartsSalesCategoryId = FPartsSalesCategoryId
                   and ppp.sparepartid = FSparePartId;
              exception
                when No_Data_Found then
                  BEGIN
                    FErrormsg := '找不到对应的配件计划价，请核对！';
                    raise FMyException;
                  END;
              end;
            
              begin
                select WarehouseArea.Id, WarehouseArea.Code
                  into FWarehouseAreaId, FWarehouseAreaCode
                  from WarehouseArea
                 inner join WarehouseAreaCategory
                    on WarehouseArea.AreaCategoryId =
                       WarehouseAreaCategory.Id
                 where WarehouseArea.WarehouseId = FOutwarehouseid
                   and WarehouseArea.Areakind = 3
                   and WarehouseAreaCategory.Category = 1;
              
              exception
                when no_data_found then
                  BEGIN
                    FErrormsg := 'PMS仓库对应唯一库位未维护，请核对！';
                    raise FMyException;
                  END;
              end;
            
             begin
                select Sparepart.Id
                  into FSparepartId
                  from Sparepart
                 where Sparepart.Status = 1 and Sparepart.Code=Vtable_Shippingorderdetails(Vi).ITEM;
              
              exception
                when no_data_found then
                  BEGIN
                    FErrormsg := 'PMS仓库对应唯一库位未维护，请核对！';
                    raise FMyException;
                  END;
              end;
              
            
              --配件出库清单
              insert into Partsoutboundbilldetail
                (Id,
                 PartsOutboundBillId,
                 SparePartId,
                 SparePartCode,
                 SparePartName,
                 OutboundAmount,
                 WarehouseAreaId,
                 WarehouseAreaCode,
                 BatchNumber,
                 SettlementPrice,
                 CostPrice)
                select s_Partsoutboundbilldetail.Nextval,
                       s_Partsoutboundbill.Currval,
                       pd.SparePartId,
                       pd.SparePartCode,
                       pd.SparePartName,
                       Vtable_Shippingorderdetails(Vi).TOTAL_QTY,
                       FWarehouseAreaId,
                       FWarehouseAreaCode,
                       null,
                       pd.Price,
                       decode(pop.StorageCompanyType, 1, FCostPrice, 0)
                  from PartsOutboundPlanDetail pd
                 inner join PartsOutboundPlan Pop
                    on pop.id = pd.PartsOutboundPlanId
                 where pd.PartsOutboundPlanId = FOutplanid
                   and pd.SparePartId = FSparepartId;
            
            
              if FOutboundType =1 or FOutboundType=4
                 then
                begin
                  --配件发运清单
                  insert into PartsShippingOrderDetail
                    (Id,
                     PartsShippingOrderId,
                     SparePartId,
                     SparePartCode,
                     SparePartName,
                     ShippingAmount,
                     ConfirmedAmount,
                     DifferenceClassification,
                     TransportLossesDisposeMethod,
                     InTransitDamageLossAmount,
                     SettlementPrice)
                    select s_PartsShippingOrderDetail.Nextval,
                           s_Partsshippingorder.Currval,
                           pd.SparePartId,
                           pd.SparePartCode,
                           pd.SparePartName,
                           Vtable_Shippingorderdetails(Vi).TOTAL_QTY,
                           Vtable_Shippingorderdetails(Vi).TOTAL_QTY,
                           null,
                           null,
                           null,
                           pd.Price
                      from PartsOutboundPlanDetail pd
                     where pd.PartsOutboundPlanId = FOutplanid
                       and pd.SparePartId = FSparepartId;
                end;
              end if;
            
              --减少配件锁定库存
              update PartsLockedStock
                 set PartsLockedStock.LockedQuantity = PartsLockedStock.LockedQuantity -
                                                       Vtable_Shippingorderdetails(Vi).Total_Qty
               where PartsLockedStock.WarehouseId = FOutwarehouseid
                 and PartsLockedStock.Partid = FSparepartId;
                 
              --减少配件库存
              update PartsStock
                 set PartsStock.Quantity = PartsStock.Quantity -
                                           Vtable_Shippingorderdetails(Vi).Total_Qty
               where PartsStock.WarehouseId = FOutwarehouseid
                 and PartsStock.Partid =FSparepartId;
            
              update Partsoutboundplandetail
                 set Partsoutboundplandetail.Outboundfulfillment = FFinishqty +
                                                                   Vtable_Shippingorderdetails(Vi).Total_Qty
               where Partsoutboundplandetail.Partsoutboundplanid =
                     FOutplanid
                 and Partsoutboundplandetail.Sparepartid = FSparepartId;
            
              if FPlanqty >
                 FFinishqty + Vtable_Shippingorderdetails(Vi).TOTAL_QTY then
                begin
                  FFinishstatus := FFinishstatus + 1;
                end;
              end if;
              if FPlanqty <
                 FFinishqty + Vtable_Shippingorderdetails(Vi).TOTAL_QTY then
                begin
                  FErrormsg := '完成量大于计划量，请进行核对！';
                  raise FMyexception;
                end;
              end if;
              
            end loop;
          
            select sum(pd.price * pd.outboundfulfillment)
              into FAmount
              from PartsOutboundPlan Pop
             inner join PartsOutboundPlanDetail pd
                on pop.id = pd.partsoutboundplanid
             where Pop.id = FOutplanid;
          
            if FCustomerAccountId > 0 then
              begin
                if FOutboundType = 1 then
                  begin
                    update CustomerAccount
                       set CustomerAccount.Pendingamount       = CustomerAccount.Pendingamount -
                                                                 FAmount,
                           CustomerAccount.Shippedproductvalue = CustomerAccount.Shippedproductvalue +
                                                                 FAmount,
                           CustomerAccount.Modifierid          = 1,
                           CustomerAccount.Modifiername        = 'WMSInterface',
                           CustomerAccount.Modifytime          = sysdate
                     where CustomerAccount.id = FCustomerAccountId
                       and CustomerAccount.status <> 99;
                  end;
                elsif FOutboundType = 2 then
                  begin
                    begin
                      select sa.estimateddueamount
                        into FEstimateddueamount
                        from supplieraccount sa
                       where sa.id = FCustomeraccountId
                         and sa.status = 1;
                    exception
                      when No_Data_Found then
                        FErrormsg := '未找到供应商账户，请核对！';
                        raise FMyexception;
                    end;
                    update supplieraccount
                       set supplieraccount.estimateddueamount = supplieraccount.estimateddueamount +
                                                                FAmount,
                           supplieraccount.Modifierid         = 1,
                           supplieraccount.Modifiername       = 'WMSInterface',
                           supplieraccount.Modifytime         = sysdate
                     where supplieraccount.id = FcustomeraccountId
                       and supplieraccount.status = 1;
                  end;
                end if;
              end;
            end if;
          
          end;
        end if;
      
        if (c_Gett_Shippingorderhead.User_Def6 = 'Y') then
          begin
            update PartsOutboundPlan
               set PartsOutboundPlan.status       = 4,
                   PartsOutboundPlan.Modifytime   = sysdate,
                   PartsOutboundPlan.ModifierName = 'WMS',
                   PartsOutboundPlan.Stoper       = 'WMS',
                   PartsOutboundPlan.StopTime     = sysdate,
                   PartsOutboundPlan.Remark       = PartsOutboundPlan.Remark ||
                                                    ' [该单据已经由业务人员强制完成]'
             where PartsOutboundPlan.id = FOutplanid;
          end;
        else
          --1  新建,2  部分出库,3  出库完成,4  终止
          if FFinishstatus > 1 then
            begin
              update PartsOutboundPlan
                 set PartsOutboundPlan.status = 2
               where PartsOutboundPlan.id = FOutplanid;
            end;
          elsif FFinishstatus = 1 then
            begin
              update PartsOutboundPlan
                 set PartsOutboundPlan.status = 3
               where PartsOutboundPlan.id = FOutplanid;
            end;
          else
            begin
              update PartsOutboundPlan
                 set PartsOutboundPlan.status = 4
               where PartsOutboundPlan.id = FOutplanid;
            end;
          end if;
        end if;
      
        update t_Shippingorderhead
           set t_Shippingorderhead.Isfinished = 1
         where t_Shippingorderhead.Interface_Record_Id =
               c_Gett_Shippingorderhead.Interface_Record_Id;
      
      exception
        when FMyexception then
          begin
            rollback;
            insert into Syncwmsinoutloginfos
              (Objid, Syncdate, Code, Synctype, Synccode, Inoutcode)
            values
              (Getregguidstring,
               sysdate,
               FErrormsg,
               2,
               c_Gett_Shippingorderhead.Interface_Record_Id,
               c_Gett_Shippingorderhead.User_Def3);
          
            update t_Shippingorderhead
               set t_Shippingorderhead.Isfinished = 2
             where t_Shippingorderhead.Interface_Record_Id =
                   c_Gett_Shippingorderhead.Interface_Record_Id
               and t_Shippingorderhead.Isfinished = 0;
          end;
        when others then
          begin
            rollback;
            FErrormsg := sqlerrm;
            insert into Syncwmsinoutloginfos
              (Objid, Syncdate, Code, Synctype, Synccode, Inoutcode)
            values
              (Getregguidstring,
               sysdate,
               FErrormsg,
               2,
               c_Gett_Shippingorderhead.Interface_Record_Id,
               c_Gett_Shippingorderhead.User_Def3);
          end;
          update t_Shippingorderhead
             set t_Shippingorderhead.Isfinished = 2
           where t_Shippingorderhead.Interface_Record_Id =
                 c_Gett_Shippingorderhead.Interface_Record_Id
             and t_Shippingorderhead.Isfinished = 0;
        
      end;
      Vtable_Shippingorderdetails.delete;
      commit;
    end loop;
  end;

  procedure synccheck as
    FDateFormat                 date;
    FDateStr                    varchar2(8);
    FSerialLength               integer;
    FComplateId                 integer;
    FNewCode                    varchar2(50);
    FNewCodePIB                 varchar2(50);
    FPIBBranchId                number(9);
    FPIBWarehouseId             number(9);
    FPIBWarehouseCode           varchar2(50);
    FPIBWarehouseName           varchar2(50);
    FPIBStorageCompanyId        number(9);
    FPIBStorageCompanyCode      varchar2(50);
    FPIBStorageCompanyName      varchar2(50);
    FPIBStorageCompanyType      number(9);
    FPIBWarehouseCount          number(9);
    FErrorMsg                   varchar2(1000);
    FPartsInventoryBillId       number(9);
    FPartsInventoryBillPartId   number(9);
    FPartsInventoryBillPartCode varchar2(50);
    FPartsInventoryBillPartName varchar2(50);
    FPIBWarehouseAreaId         number(9);
    FPIBWarehouseAreaCode       varchar2(50);
    FPartsStockId               number(9);
    FPartStockQuantity          number(9);
    FPlannedPrice               number(19, 4);
    FMyException exception;
    cursor Ft_inventory_adjustment is
      select *
        from t_inventory_adjustment
       where isfinished = 0
         and syscode = 'NPMS';
  begin
    FDateFormat   := trunc(sysdate, 'DD');
    FDateStr      := to_char(sysdate, 'yyyymmdd');
    FSerialLength := 6;
    FNewCode      := 'PI{CORPCODE}' || FDateStr || '{SERIAL}';
    begin
      select Id
        into FComplateId
        from CodeTemplate
       where name = 'PartsInventoryBill'
         and IsActived = 1;
    exception
      when NO_DATA_FOUND then
        raise_application_error(-20001,
                                '未找到名称为"PartsInventoryBill"的有效编码规则。');
      when others then
        raise;
    end;
  
    for C_t_inventory_adjustment in Ft_inventory_adjustment loop
      begin
        FErrorMsg := '';
      
        begin
          begin
            select count(1)
              into FPIBWarehouseCount
              from WMS2PMSWarehouse
             where WMS2PMSWarehouse.WMSWarehousecode =
                   C_t_inventory_adjustment.Warehousecode
               and WMS2PMSWarehouse.PMSPartsSalesCategoryName =
                   C_t_inventory_adjustment.BrandName;
          exception
            when no_data_found then
              FPIBWarehouseId := 'null';
          end;
        
          if FPIBWarehouseCount = 1 then
            begin
              select Company.Id,
                     Warehouse.Id,
                     Warehouse.Code,
                     Warehouse.Name,
                     Warehouse.StorageCompanyId,
                     Company.Code,
                     Company.Name,
                     Warehouse.StorageCompanyType
                into FPIBBranchId,
                     FPIBWarehouseId,
                     FPIBWarehouseCode,
                     FPIBWarehouseName,
                     FPIBStorageCompanyId,
                     FPIBStorageCompanyCode,
                     FPIBStorageCompanyName,
                     FPIBStorageCompanyType
                from WMS2PMSWarehouse
               inner join Warehouse
                  on Warehouse.CODE = WMS2PMSWarehouse.PMSWarehouseCode
                 and Warehouse.Storagecompanyid =
                     WMS2PMSWarehouse.Pmsstoragecompanyid
               inner join Company
                  on Warehouse.StorageCompanyId = Company.Id
                 AND COMPANY.Type = 1
               inner join PARTSSALESCATEGORY PSC
                  on psc.NAME = WMS2PMSWarehouse.PMSPartsSalesCategoryName
                 and psc.Status = 1
               where WMS2PMSWarehouse.WMSWarehouseCode =
                     C_t_inventory_adjustment.Warehousecode
                 and WMS2PMSWarehouse.PMSPartsSalesCategoryName =
                     C_t_inventory_adjustment.BrandName;
            
              FNewCodePIB           := regexp_replace(regexp_replace(FNewCode,
                                                                     '{CORPCODE}',
                                                                     FPIBStorageCompanyCode),
                                                      '{SERIAL}',
                                                      lpad(to_char(GetSerial(FComplateId,
                                                                             FDateFormat,
                                                                             FPIBStorageCompanyCode)),
                                                           FSerialLength,
                                                           '0'));
              FPartsInventoryBillId := s_PartsInventoryBill.NEXTVAL;
              insert into PartsInventoryBill
                (Id,
                 Code,
                 BranchId,
                 WarehouseAreaCategory,
                 WarehouseId,
                 WarehouseCode,
                 WarehouseName,
                 StorageCompanyId,
                 StorageCompanyCode,
                 StorageCompanyName,
                 StorageCompanyType,
                 Status,
                 CreatorId,
                 CreatorName,
                 CreateTime)
              values
                (FPartsInventoryBillId,
                 FNewCodePIB,
                 FPIBBranchId,
                 1,
                 FPIBWarehouseId,
                 FPIBWarehouseCode,
                 FPIBWarehouseName,
                 FPIBStorageCompanyId,
                 FPIBStorageCompanyCode,
                 FPIBStorageCompanyName,
                 FPIBStorageCompanyType,
                 3,
                 1,
                 'WMSPMSInterface',
                 sysdate);
            
              begin
                select SparePart.Id, SparePart.Code, SparePart.Name
                  into FPartsInventoryBillPartId,
                       FPartsInventoryBillPartCode,
                       FPartsInventoryBillPartName
                  from SparePart
                 where SparePart.Code =
                       C_t_inventory_adjustment.MaterialCode;
              exception
                when no_data_found then
                  FPartsInventoryBillPartId := null;
              end;
              if FPartsInventoryBillPartId is not null then
                begin
                  begin
                    select WarehouseArea.Id, WarehouseArea.Code
                      into FPIBWarehouseAreaId, FPIBWarehouseAreaCode
                      from WarehouseArea
                     inner join WarehouseAreaCategory
                        on WarehouseArea.AreaCategoryId =
                           WarehouseAreaCategory.Id
                     where WarehouseArea.WarehouseId = FPIBWarehouseId
                       and WarehouseArea.AreaKind = 3
                       and WarehouseArea.Status = 1
                       and WarehouseAreaCategory.Category = 1;
                  exception
                    when no_data_found then
                      FPIBWarehouseAreaId := null;
                  end;
                  if FPIBWarehouseAreaId is not null then
                    begin
                      begin
                        select PartsStock.Id
                          into FPartsStockId
                          from PartsStock
                         where PartsStock.WarehouseId = FPIBWarehouseId
                           and PartsStock.PartId =
                               FPartsInventoryBillPartId
                           and PartsStock.WarehouseAreaId =
                               FPIBWarehouseAreaId;
                      exception
                        when no_data_found then
                          FPartsStockId := null;
                      end;
                    
                      if FPartsStockId is not null then
                        begin
                        
                          select nvl(PartsStock.Quantity, 0) Quantity
                            into FPartStockQuantity
                            from PartsStock
                           where PartsStock.Id = FPartsStockId;
                        
                          if UPPER(trim(C_t_inventory_adjustment.Direction)) =
                             'FROM' then
                            begin
                            
                              if (FPartStockQuantity >=
                                 C_t_inventory_adjustment.Quantity) then
                                begin
                                  update PartsStock
                                     set PartsStock.Quantity = nvl(Quantity,
                                                                   0) -
                                                               C_t_inventory_adjustment.Quantity
                                   where PartsStock.Id = FPartsStockId;
                                end;
                              else
                                begin
                                  FErrormsg := '配件库存不足，请检查！';
                                  raise FMyException;
                                end;
                              end if;
                            
                              begin
                                select nvl(PartsPlannedPrice.PlannedPrice,
                                           0)
                                  into FPlannedPrice
                                  from PartsPlannedPrice
                                 where PartsPlannedPrice.SparePartId =
                                       FPartsInventoryBillPartId;
                              exception
                                when NO_DATA_FOUND then
                                  begin
                                    FErrormsg := '配件计划价不存在，请检查！';
                                    raise FMyException;
                                  end;
                              end;
                              FPlannedPrice := -C_t_inventory_adjustment.Quantity *
                                               FPlannedPrice;
                            
                              insert into PartsInventoryDetail
                                (Id,
                                 PartsInventoryBillId,
                                 SparePartId,
                                 SparePartCode,
                                 SparePartName,
                                 WarehouseAreaId,
                                 WarehouseAreaCode,
                                 CurrentStorage,
                                 StorageAfterInventory,
                                 Ifcover,
                                 StorageDifference,
                                 CostPrice,
                                 AmountDifference)
                              values
                                (S_PartsInventoryDetail.NEXTVAL,
                                 FPartsInventoryBillId,
                                 FPartsInventoryBillPartId,
                                 FPartsInventoryBillPartCode,
                                 FPartsInventoryBillPartName,
                                 FPIBWarehouseAreaId,
                                 FPIBWarehouseAreaCode,
                                 FPartStockQuantity,
                                 FPartStockQuantity -
                                 C_t_inventory_adjustment.Quantity,
                                 1,
                                 -C_t_inventory_adjustment.Quantity,
                                 FPlannedPrice,
                                 FPlannedPrice *
                                 -C_t_inventory_adjustment.Quantity);
                            
                              update PartsInventoryBill
                                 set AmountDifference = FPlannedPrice *
                                                        -C_t_inventory_adjustment.Quantity
                               where Id = FPartsInventoryBillId;
                              --因为当前PMS系统都是无批次管理所以不生成 配件库存批次明细
                            end;
                          end if;
                        
                          if UPPER(trim(C_t_inventory_adjustment.Direction)) = 'TO' then
                            begin
                              update PartsStock
                                 set Quantity = nvl(Quantity, 0) +
                                                C_t_inventory_adjustment.Quantity
                               where PartsStock.Id = FPartsStockId;
                            
                              begin
                                select nvl(PartsPlannedPrice.PlannedPrice,
                                           0)
                                  into FPlannedPrice
                                  from PartsPlannedPrice
                                 where PartsPlannedPrice.SparePartId =
                                       FPartsInventoryBillPartId;
                              exception
                                when NO_DATA_FOUND then
                                  begin
                                    FErrormsg := '配件计划价不存在，请检查！';
                                    raise FMyException;
                                  end;
                              end;
                              FPlannedPrice := C_t_inventory_adjustment.Quantity *
                                               FPlannedPrice;
                            
                              insert into PartsInventoryDetail
                                (Id,
                                 PartsInventoryBillId,
                                 SparePartId,
                                 SparePartCode,
                                 SparePartName,
                                 WarehouseAreaId,
                                 WarehouseAreaCode,
                                 CurrentStorage,
                                 StorageAfterInventory,
                                 Ifcover,
                                 StorageDifference,
                                 CostPrice,
                                 AmountDifference)
                              values
                                (s_PartsInventoryDetail.NEXTVAL,
                                 FPartsInventoryBillId,
                                 FPartsInventoryBillPartId,
                                 FPartsInventoryBillPartCode,
                                 FPartsInventoryBillPartName,
                                 FPIBWarehouseAreaId,
                                 FPIBWarehouseAreaCode,
                                 FPartStockQuantity,
                                 FPartStockQuantity +
                                 C_t_inventory_adjustment.Quantity,
                                 1,
                                 C_t_inventory_adjustment.Quantity,
                                 FPlannedPrice,
                                 FPlannedPrice *
                                 C_t_inventory_adjustment.Quantity);
                            
                              update PartsInventoryBill
                                 set AmountDifference = FPlannedPrice *
                                                        C_t_inventory_adjustment.Quantity
                               where Id = FPartsInventoryBillId;
                              --因为当前PMS系统都是无批次管理所以不生成 配件库存批次明细
                            end;
                          end if;
                        
                          if (Upper(trim(C_t_inventory_adjustment.Direction)) <>
                             'FROM') and
                             (uPPER(trim(C_t_inventory_adjustment.Direction)) <> 'TO') then
                            begin
                              FErrormsg := '库存调整方向非法，请检查！';
                              raise FMyException;
                            end;
                          end if;
                        end;
                      else
                        begin
                          FErrormsg := '库存信息不存在，请检查！';
                          raise FMyException;
                        end;
                      end if;
                    end;
                  else
                    begin
                      FErrormsg := '仓库库区库位不存在，请检查！';
                      raise FMyException;
                    end;
                  end if;
                end;
              else
                begin
                  FErrormsg := '盘点单配件不存在，请检查！';
                  raise FMyException;
                end;
              end if;
            end;
          else
            begin
              if FPIBWarehouseCount = 0 then
                begin
                  FErrormsg := '盘点仓库不存在，请检查！';
                  raise FMyException;
                end;
              else
                begin
                  FErrormsg := '品牌和wms仓库对应的仓库信息不唯一，请检查！';
                  raise FMyException;
                end;
              end if;
            end;
          end if;
        end;
      
        update t_inventory_adjustment
           set t_inventory_adjustment.IsFinished = 1
         where t_inventory_adjustment.Code = C_t_inventory_adjustment.Code;
      
        commit;
      exception
        when FMyException then
          begin
            rollback;
            insert into SYNCWMSINOUTLOGINFOS
              (objid, syncdate, code, synctype, synccode)
            values
              (getregguidstring,
               sysdate,
               FErrormsg,
               3,
               C_t_inventory_adjustment.Code);
            update t_inventory_adjustment
               set t_inventory_adjustment.IsFinished = 2
             where t_inventory_adjustment.Code =
                   C_t_inventory_adjustment.Code;
          exception
            when others then
              begin
                rollback;
                FErrorMsg := sqlerrm;
                insert into SYNCWMSINOUTLOGINFOS
                  (objid, syncdate, code, synctype, synccode)
                values
                  (getregguidstring,
                   sysdate,
                   FErrormsg,
                   3,
                   C_t_inventory_adjustment.Code);
                update t_inventory_adjustment
                   set t_inventory_adjustment.IsFinished = 2
                 where t_inventory_adjustment.Code =
                       C_t_inventory_adjustment.Code;
              end;
          end;
      end;
    end loop;
  end;

  procedure Syncwarehouses as
    FWmswarehousecode varchar2(25);
    FWmswarehousename varchar2(25);
    ExistIds          number(9);
    Finishstatus      number(9);
    Errormsg          varchar2(1000);
    cursor t_Warehouse is
      select *
        from t_Warehouse
       where Isfinished = 0
       order by INTERNAL_WAREHOUSE_NUM asc;
  
  begin
    for c_t_Warehouse in t_Warehouse loop
      begin
        Finishstatus      := 1;
        FWmswarehousecode := c_t_Warehouse.Warehousecode;
        FWmswarehousename := c_t_Warehouse.Warehousename;
        begin
          select StorageCenter.id
            into ExistIds
            from StorageCenter
           where Wmswarehousecode = FWmswarehousecode;
        exception
          when No_Data_Found then
            ExistIds := null;
        end;
      
        if ExistIds is null then
          begin
            insert into StorageCenter
              (Id, Wmswarehousecode, Wmswarehousename)
            values
              (s_StorageCenter.NEXTVAL,
               FWmswarehousecode,
               FWmswarehousename);
          end;
        else
          begin
            update StorageCenter
               set Wmswarehousename = FWmswarehousename
             where Id = ExistIds;
          end;
        end if;
      
        update t_Warehouse
           set t_Warehouse.Isfinished = 1
         where t_Warehouse.Warehousecode = c_t_Warehouse.Warehousecode;
      
        commit;
      exception
        when others then
          begin
            Errormsg := sqlerrm;
            insert into Syncwmsinoutloginfos
              (Objid, Syncdate, Code, Synctype, Synccode)
            values
              (Getregguidstring,
               sysdate,
               Errormsg,
               4,
               c_t_Warehouse.Warehousecode);
            update t_Warehouse
               set t_Warehouse.Isfinished = 2
             where t_Warehouse.Warehousecode = c_t_Warehouse.Warehousecode;
          end;
      end;
    end loop;
  end;

end SYNCWMSINOUT;


create or replace function GetRegGuidString return varchar2
is
  tempStr varchar2(40);
begin
  tempStr:=sys_guid();
  return '{'||SUBSTR(tempStr,1,8)||'-'||SUBSTR(tempstr,9,4)||'-'||SUBSTR(tempstr,13,4)||'-'||SUBSTR(tempStr,17,4)||'-'||SUBSTR(tempStr,21,12)||'}';
end;

begin
  sys.dbms_scheduler.create_job(job_name            => 'WMS接口入库数据同步',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'syncwmsinout.SYNCIn',
                                start_date          => to_date(null),
                                repeat_interval     => 'Freq=Minutely;Interval=10',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');

  sys.dbms_scheduler.create_job(job_name            => 'WMS接口出库数据同步',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'syncwmsinout.SYNCTraffic',
                                start_date          => to_date(null),
                                repeat_interval     => 'Freq=Minutely;Interval=10',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');

  sys.dbms_scheduler.create_job(job_name            => 'WMS接口入库检验数据同步',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'dcs.syncwmsinout.SYNCCheck',
                                start_date          => to_date(null),
                                repeat_interval     => 'Freq=Minutely;Interval=10',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');

  sys.dbms_scheduler.create_job(job_name            => 'WMS接口仓库数据同步',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'syncwmsinout.SYNCWarehouses',
                                start_date          => to_date(null),
                                repeat_interval     => 'Freq=Minutely;Interval=10',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/

create sequence S_SalesCategoryName2Company
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

-- Create table
create table SalesCategoryName2Company
(
  Id                         NUMBER(9)     not null,
  PMSPartsSalesCategoryName  NVARCHAR2(50) not null,
  WMSCompany                 NVARCHAR2(50) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    minextents 1
    maxextents unlimited
  );

insert into SalesCategoryName2Company
  (Id,
   PMSPartsSalesCategoryName,
   WMSCompany)
values
  (s_SalesCategoryName2Company.nextval,
   '奥铃',
   'foton');
insert into SalesCategoryName2Company
  (Id,
   PMSPartsSalesCategoryName,
   WMSCompany)
values
  (s_SalesCategoryName2Company.nextval,
   '欧马可',
   'foton');
insert into SalesCategoryName2Company
  (Id,
   PMSPartsSalesCategoryName,
   WMSCompany)
values
  (s_SalesCategoryName2Company.nextval,
   '蒙派克风景',
   'foton');
insert into SalesCategoryName2Company
  (Id,
   PMSPartsSalesCategoryName,
   WMSCompany)
values
  (s_SalesCategoryName2Company.nextval,
   '拓陆者萨普',
   'foton');
insert into SalesCategoryName2Company
  (Id,
   PMSPartsSalesCategoryName,
   WMSCompany)
values
  (s_SalesCategoryName2Company.nextval,
   '精品',
   'foton');
insert into SalesCategoryName2Company
  (Id,
   PMSPartsSalesCategoryName,
   WMSCompany)
values
  (s_SalesCategoryName2Company.nextval,
   '海外轻卡',
   'foton');
insert into SalesCategoryName2Company
  (Id,
   PMSPartsSalesCategoryName,
   WMSCompany)
values
  (s_SalesCategoryName2Company.nextval,
   '海外中重卡',
   'foton');
insert into SalesCategoryName2Company
  (Id,
   PMSPartsSalesCategoryName,
   WMSCompany)
values
  (s_SalesCategoryName2Company.nextval,
   '海外乘用车',
   'foton');
insert into SalesCategoryName2Company
  (Id,
   PMSPartsSalesCategoryName,
   WMSCompany)
values
  (s_SalesCategoryName2Company.nextval,
   '统购',
   'foton');
insert into SalesCategoryName2Company
  (Id,
   PMSPartsSalesCategoryName,
   WMSCompany)
values
  (s_SalesCategoryName2Company.nextval,
   '时代',
   'foton');
insert into SalesCategoryName2Company
  (Id,
   PMSPartsSalesCategoryName,
   WMSCompany)
values
  (s_SalesCategoryName2Company.nextval,
   '迷迪',
   'foton');
insert into SalesCategoryName2Company
  (Id,
   PMSPartsSalesCategoryName,
   WMSCompany)
values
  (s_SalesCategoryName2Company.nextval,
   '工程车',
   'foton');
insert into SalesCategoryName2Company
  (Id,
   PMSPartsSalesCategoryName,
   WMSCompany)
values
  (s_SalesCategoryName2Company.nextval,
   '欧曼',
   'ombn');
insert into SalesCategoryName2Company
  (Id,
   PMSPartsSalesCategoryName,
   WMSCompany)
values
  (s_SalesCategoryName2Company.nextval,
   '欧曼保外',
   'ombw');

-- Create table
create table SYNCWMSINOUTLOGINFOS
(
  OBJID     VARCHAR2(40) not null,
  SYNCDATE  DATE not null,
  CODE      VARCHAR2(1000) not null,
  SYNCTYPE  NUMBER(3) not null,
  SYNCCODE  VARCHAR2(25) not null,
  INOUTCODE VARCHAR2(25),
  STATUS    NUMBER(3) default 0 not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    minextents 1
    maxextents unlimited
  );


-- Create table
create table T_IN
(
  SYSCODE              NVARCHAR2(4) not null,
  INTERFACE_CONDITION  NVARCHAR2(25) not null,
  OBJID                NVARCHAR2(25) not null,
  SOURCECODE           NVARCHAR2(20) not null,
  TYPE                 NVARCHAR2(25) not null,
  WAREHOUSECODE        NVARCHAR2(25) not null,
  SAVE_DATE_TIME_STAMP DATE not null,
  USER_DEF1            NVARCHAR2(25),
  USER_DEF2            NVARCHAR2(25),
  USER_DEF3            NVARCHAR2(25),
  USER_DEF4            NVARCHAR2(25),
  USER_DEF5            NVARCHAR2(25),
  USER_DEF6            NVARCHAR2(25),
  USER_DEF7            NUMBER(19,5),
  USER_DEF8            NUMBER(19,5),
  ISFINISHED           NUMBER(3) default 0,
  TVERIFYID            NVARCHAR2(40),
  TINID                NVARCHAR2(40)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 16
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table T_IN
  add constraint T_IN_PK_OBJID primary key (OBJID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Create table
create table T_INDETAILS
(
  INTERFACE_RECORD_ID  NVARCHAR2(25) not null,
  PARENTID             NVARCHAR2(25) not null,
  MATERIALCODE         NVARCHAR2(25) not null,
  MATERIALNAME         NVARCHAR2(100) not null,
  QTY                  NUMBER(19,5) not null,
  SAVE_DATE_TIME_STAMP DATE not null,
  USER_DEF1            NVARCHAR2(25),
  USER_DEF2            NVARCHAR2(25),
  USER_DEF3            NVARCHAR2(25),
  USER_DEF4            NVARCHAR2(25),
  USER_DEF5            NVARCHAR2(25),
  USER_DEF6            NVARCHAR2(25),
  USER_DEF7            NUMBER(19,5),
  USER_DEF8            NUMBER(19,5)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 16
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table T_INDETAILS
  add constraint T_IND_INTERFACE_RECORD_ID primary key (INTERFACE_RECORD_ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

-- Create table
create table T_INVENTORY_ADJUSTMENT
(
  SYSCODE              NVARCHAR2(4) not null,
  CODE                 NVARCHAR2(25) not null,
  CREATETIME           DATE not null,
  WAREHOUSECODE        NVARCHAR2(25) not null,
  BRANDNAME            NVARCHAR2(25) not null,
  MATERIALCODE         NVARCHAR2(25) not null,
  TYPE                 NVARCHAR2(25) not null,
  QUANTITY             NUMBER(19,5) not null,
  MEMO                 NVARCHAR2(200),
  SAVE_DATE_TIME_STAMP DATE not null,
  DIRECTION            NVARCHAR2(25) not null,
  USER_DEF1            NVARCHAR2(25),
  USER_DEF2            NVARCHAR2(25),
  USER_DEF3            NVARCHAR2(25),
  USER_DEF4            NVARCHAR2(25),
  USER_DEF5            NVARCHAR2(25),
  USER_DEF6            NVARCHAR2(25),
  USER_DEF7            NUMBER(19,5),
  USER_DEF8            NUMBER(19,5),
  ISFINISHED           NUMBER(9) default 0,
  TCHECKID             NVARCHAR2(40)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 16
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table T_INVENTORY_ADJUSTMENT
  add constraint T_INVENTORY_ADJUSTMENT_PK_CODE primary key (CODE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

-- Create table
create table T_SHIPPINGORDERDETAILS
(
  INTERFACE_ACTION_CODE NVARCHAR2(25) not null,
  INTERFACE_CONDITION   NVARCHAR2(25) not null,
  INTERFACE_RECORD_ID   NVARCHAR2(25) not null,
  COMPANY               NVARCHAR2(25) not null,
  INTERFACE_LINK_ID     NVARCHAR2(25) not null,
  SHIPMENT_ID           NVARCHAR2(25) not null,
  ITEM                  NVARCHAR2(25) not null,
  QUANTITY_UM           NVARCHAR2(25) not null,
  TOTAL_QTY             NUMBER(28,5) not null,
  LOT_CONTROLLED        NVARCHAR2(1) not null,
  ERP_ORDER             NVARCHAR2(25),
  USER_STAMP            NVARCHAR2(30) not null,
  DATE_TIME_STAMP       DATE not null,
  ERP_ORDER_LINE_NUM    NUMBER(19,5) not null,
  SAVE_DATE_TIME_STAMP  DATE not null,
  USER_DEF1             NVARCHAR2(25),
  USER_DEF2             NVARCHAR2(25),
  USER_DEF3             NVARCHAR2(25),
  USER_DEF4             NVARCHAR2(25),
  USER_DEF5             NVARCHAR2(25),
  USER_DEF6             NVARCHAR2(25),
  USER_DEF7             NUMBER(19,5),
  USER_DEF8             NUMBER(19,5)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 16
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table T_SHIPPINGORDERDETAILS
  add constraint T_SHIPPINGORDERDETAILS_PK_ID primary key (INTERFACE_RECORD_ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );


-- Create table
create table T_SHIPPINGORDERHEAD
(
  THEDATE               DATE not null,
  INTERFACE_RECORD_ID   NVARCHAR2(25) not null,
  INTERFACE_CONDITION   NVARCHAR2(25) not null,
  INTERFACE_ACTION_CODE NVARCHAR2(25) not null,
  WAREHOUSE             NVARCHAR2(25) not null,
  COMPANY               NVARCHAR2(25) not null,
  SHIPMENT_ID           NVARCHAR2(25) not null,
  USER_STAMP            NVARCHAR2(30) not null,
  DATE_TIME_STAMP       DATE not null,
  SCHEDULED_SHIP_DATE   DATE not null,
  CUSTOMER              NVARCHAR2(25) not null,
  ORDER_TYPE            NVARCHAR2(25) not null,
  SHIP_TO               NVARCHAR2(25) not null,
  SHIP_TO_ADDRESS1      NVARCHAR2(50) not null,
  SHIP_TO_ADDRESS2      NVARCHAR2(50) not null,
  SHIP_TO_NAME          NVARCHAR2(50) not null,
  SHIP_TO_ATTENTION_TO  NVARCHAR2(25) not null,
  CARRIER               NVARCHAR2(50) not null,
  CARRIER_SERVICE       NVARCHAR2(50) not null,
  ALLOCATE_COMPLETE     NVARCHAR2(1) not null,
  ERP_ORDER             NVARCHAR2(25) not null,
  SHIP_TO_CITY          NVARCHAR2(30) not null,
  SHIP_TO_STATE         NVARCHAR2(30) not null,
  SHIP_TO_COUNTRY       NVARCHAR2(25) not null,
  SHIP_TO_POSTAL_CODE   NVARCHAR2(25) not null,
  SAVE_DATE_TIME_STAMP  DATE not null,
  USER_DEF1             NVARCHAR2(25),
  USER_DEF2             NVARCHAR2(25),
  USER_DEF3             NVARCHAR2(25),
  USER_DEF4             NVARCHAR2(25),
  USER_DEF5             NVARCHAR2(25),
  USER_DEF6             NVARCHAR2(25),
  USER_DEF7             NUMBER(19,5),
  USER_DEF8             NUMBER(19,5),
  ISFINISHED            NUMBER(9) default 0,
  TTRAFFICID            NVARCHAR2(40),
  TOUTID                NVARCHAR2(40)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 16
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table T_SHIPPINGORDERHEAD
  add constraint T_SHIPPINGORDERHEAD_PK_ID primary key (INTERFACE_RECORD_ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

-- Create table
create table T_WAREHOUSE
(
  WAREHOUSECODE          NVARCHAR2(25) not null,
  WAREHOUSENAME          NVARCHAR2(50) not null,
  THEDATE                DATE not null,
  INTERFACE_CONDITION    NVARCHAR2(25),
  INTERFACE_ACTION_CODE  NVARCHAR2(25),
  ISFINISHED             NUMBER(9) default 0,
  SYNCNUM                NUMBER(9),
  INTERNAL_WAREHOUSE_NUM NUMBER(18) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 16
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table T_WAREHOUSE
  add constraint T_WAREHOUSE_NUM primary key (INTERNAL_WAREHOUSE_NUM)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table T_WAREHOUSE
  add constraint T_WAREHOUSE_CODE unique (WAREHOUSECODE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

create sequence S_WMS2PMSWarehouse
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

-- Create table
create table WMS2PMSWarehouse
(
  Id                         NUMBER(9)     not null,
  PMSPartsSalesCategoryName  NVARCHAR2(100) not null,
  PMSWarehouseCode           NVARCHAR2(50) not null,
  PMSWarehouseName           NVARCHAR2(100) not null,
  PMSStorageCenterName       NVARCHAR2(50) not null,
  PMSWmsInterface            NUMBER(1)     not null,
  WMSWarehouseCode           NVARCHAR2(50) not null,
  WMSWarehouseName           NVARCHAR2(100) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    minextents 1
    maxextents unlimited
  );

insert into WMS2PMSWarehouse
  (Id,
   PMSPartsSalesCategoryName,
   PMSWarehouseCode,
   PMSWarehouseName,
   PMSStorageCenterName,
   PMSWmsInterface,
   WMSWarehouseCode,
   WMSWarehouseName)
values
  (s_WMS2PMSWarehouse.nextval,
   '奥铃',
   'AOLBJCK',
   '奥铃北京CDC仓库',
   '北京CDC',
   1,
   'BJ-A',
   '北京CDC');
insert into WMS2PMSWarehouse
  (Id,
   PMSPartsSalesCategoryName,
   PMSWarehouseCode,
   PMSWarehouseName,
   PMSStorageCenterName,
   PMSWmsInterface,
   WMSWarehouseCode,
   WMSWarehouseName)
values
  (s_WMS2PMSWarehouse.nextval,
   '海外乘用车',
   'HWCYCBJCK',
   '海外乘用车北京CDC仓库',
   '北京CDC',
   1,
   'BJ-A',
   '北京CDC');
insert into WMS2PMSWarehouse
  (Id,
   PMSPartsSalesCategoryName,
   PMSWarehouseCode,
   PMSWarehouseName,
   PMSStorageCenterName,
   PMSWmsInterface,
   WMSWarehouseCode,
   WMSWarehouseName)
values
  (s_WMS2PMSWarehouse.nextval,
  '海外轻卡',
   'HWQKBJCK',
   '海外轻卡北京CDC仓库',
   '北京CDC',
   1,
   'BJ-A',
   '北京CDC');
insert into WMS2PMSWarehouse
  (Id,
   PMSPartsSalesCategoryName,
   PMSWarehouseCode,
   PMSWarehouseName,
   PMSStorageCenterName,
   PMSWmsInterface,
   WMSWarehouseCode,
   WMSWarehouseName)
values
  (s_WMS2PMSWarehouse.nextval,
   '海外中重卡',
   'HWZZKBJCK',
   '海外中重卡北京CDC仓库',
   '北京CDC',
   1,
   'BJ-A',
   '北京CDC');
insert into WMS2PMSWarehouse
  (Id,
   PMSPartsSalesCategoryName,
   PMSWarehouseCode,
   PMSWarehouseName,
   PMSStorageCenterName,
   PMSWmsInterface,
   WMSWarehouseCode,
   WMSWarehouseName)
values
  (s_WMS2PMSWarehouse.nextval,
   '精品',
   'JPBJCK',
   '精品北京CDC仓库',
   '北京CDC',
   1,
   'BJ-A',
   '北京CDC');
insert into WMS2PMSWarehouse
  (Id,
   PMSPartsSalesCategoryName,
   PMSWarehouseCode,
   PMSWarehouseName,
   PMSStorageCenterName,
   PMSWmsInterface,
   WMSWarehouseCode,
   WMSWarehouseName)
values
  (s_WMS2PMSWarehouse.nextval,
   '蒙派克风景',
   'MPKBJCK',
   '蒙派克北京CDC仓库',
   '北京CDC',
   1,
   'BJ-A',
   '北京CDC');
insert into WMS2PMSWarehouse
  (Id,
   PMSPartsSalesCategoryName,
   PMSWarehouseCode,
   PMSWarehouseName,
   PMSStorageCenterName,
   PMSWmsInterface,
   WMSWarehouseCode,
   WMSWarehouseName)
values
  (s_WMS2PMSWarehouse.nextval,
   '蒙派克风景',
   'MPKJPBJCK',
   '蒙派克精品北京CDC仓库',
   '北京CDC',
   1,
   'BJ-A',
   '北京CDC');
insert into WMS2PMSWarehouse
  (Id,
   PMSPartsSalesCategoryName,
   PMSWarehouseCode,
   PMSWarehouseName,
   PMSStorageCenterName,
   PMSWmsInterface,
   WMSWarehouseCode,
   WMSWarehouseName)
values
  (s_WMS2PMSWarehouse.nextval,
   '欧马可',
   'AUMARKBJCK',
   '欧马可北京CDC仓库',
   '北京CDC',
   1,
   'BJ-A',
   '北京CDC');
insert into WMS2PMSWarehouse
  (Id,
   PMSPartsSalesCategoryName,
   PMSWarehouseCode,
   PMSWarehouseName,
   PMSStorageCenterName,
   PMSWmsInterface,
   WMSWarehouseCode,
   WMSWarehouseName)
values
  (s_WMS2PMSWarehouse.nextval,
   '统购',
   'PJWLGSBJCK',
   '配件物流公司北京CDC仓库',
   '北京CDC',
   1,
   'BJ-A',
   '北京CDC');
insert into WMS2PMSWarehouse
  (Id,
   PMSPartsSalesCategoryName,
   PMSWarehouseCode,
   PMSWarehouseName,
   PMSStorageCenterName,
   PMSWmsInterface,
   WMSWarehouseCode,
   WMSWarehouseName)
values
  (s_WMS2PMSWarehouse.nextval,
   '拓陆者萨普',
   'TLZBJCK',
   '拓陆者北京CDC仓库',
   '北京CDC',
   1,
   'BJ-A',
   '北京CDC');
insert into WMS2PMSWarehouse
  (Id,
   PMSPartsSalesCategoryName,
   PMSWarehouseCode,
   PMSWarehouseName,
   PMSStorageCenterName,
   PMSWmsInterface,
   WMSWarehouseCode,
   WMSWarehouseName)
values
  (s_WMS2PMSWarehouse.nextval,
   '奥铃',
   'AOLSDCK',
   '奥铃山东CDC仓库',
   '山东CDC',
   1,
   'SD',
   '山东CDC');
insert into WMS2PMSWarehouse
  (Id,
   PMSPartsSalesCategoryName,
   PMSWarehouseCode,
   PMSWarehouseName,
   PMSStorageCenterName,
   PMSWmsInterface,
   WMSWarehouseCode,
   WMSWarehouseName)
values
  (s_WMS2PMSWarehouse.nextval,
   '蒙派克风景',
   'MPKJPSDCK',
   '蒙派克精品山东CDC仓库',
   '山东CDC',
   1,
   'SD',
   '山东CDC');
insert into WMS2PMSWarehouse
  (Id,
   PMSPartsSalesCategoryName,
   PMSWarehouseCode,
   PMSWarehouseName,
   PMSStorageCenterName,
   PMSWmsInterface,
   WMSWarehouseCode,
   WMSWarehouseName)
values
  (s_WMS2PMSWarehouse.nextval,
   '蒙派克风景',
   'MPKSDCK',
   '蒙派克山东CDC仓库',
   '山东CDC',
   1,
   'SD',
   '山东CDC');
insert into WMS2PMSWarehouse
  (Id,
   PMSPartsSalesCategoryName,
   PMSWarehouseCode,
   PMSWarehouseName,
   PMSStorageCenterName,
   PMSWmsInterface,
   WMSWarehouseCode,
   WMSWarehouseName)
values
  (s_WMS2PMSWarehouse.nextval,
   '欧马可',
   'AUMARKSDCK',
   '欧马可山东CDC仓库',
   '山东CDC',
   1,
   'SD',
   '山东CDC');
insert into WMS2PMSWarehouse
  (Id,
   PMSPartsSalesCategoryName,
   PMSWarehouseCode,
   PMSWarehouseName,
   PMSStorageCenterName,
   PMSWmsInterface,
   WMSWarehouseCode,
   WMSWarehouseName)
values
  (s_WMS2PMSWarehouse.nextval,
   '统购',
   'PJWLGSSDCK',
   '配件物流公司山东CDC仓库',
   '山东CDC',
   1,
   'SD',
   '山东CDC');
insert into WMS2PMSWarehouse
  (Id,
   PMSPartsSalesCategoryName,
   PMSWarehouseCode,
   PMSWarehouseName,
   PMSStorageCenterName,
   PMSWmsInterface,
   WMSWarehouseCode,
   WMSWarehouseName)
values
  (s_WMS2PMSWarehouse.nextval,
   '拓陆者萨普',
   'TLZSDCK',
   '拓陆者山东CDC仓库',
   '山东CDC',
   1,
   'SD',
   '山东CDC');

create table SAP_1_SYNC
(
  syncnumber         NUMBER(9),
  bussinessid        NUMBER(9),
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );

create table SAP_2_SYNC
(
  syncnumber         NUMBER(9),
  bussinessid        NUMBER(9),
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );
create table SAP_3_SYNC
(
  syncnumber         NUMBER(9),
  bussinessid        NUMBER(9),
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );

create table SAP_4_SYNC
(
  syncnumber         NUMBER(9),
  bussinessid        NUMBER(9),
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );

create table SAP_5_SYNC
(
  syncnumber         NUMBER(9),
  bussinessid        NUMBER(9),
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );

create table SAP_6_SYNC
(
  syncnumber         NUMBER(9),
  bussinessid        NUMBER(9),
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );

create table SAP_7_SYNC
(
  syncnumber         NUMBER(9),
  bussinessid        NUMBER(9),
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );


create table SAP_8_SYNC
(
  syncnumber         NUMBER(9),
  bussinessid        NUMBER(9),
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );

create table SAP_9_SYNC
(
  syncnumber         NUMBER(9),
  bussinessid        NUMBER(9),
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );

create table SAP_10_SYNC
(
  syncnumber         NUMBER(9),
  bussinessid        NUMBER(9),
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );

create table SAP_11_SYNC
(
  syncnumber         NUMBER(9),
  bussinessid        NUMBER(9),
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );

create table SAP_12_SYNC
(
  syncnumber         NUMBER(9),
  bussinessid        NUMBER(9),
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );

create table SAP_13_SYNC
(
  syncnumber         NUMBER(9),
  bussinessid        NUMBER(9),
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );

create table SAP_14_SYNC
(
  syncnumber         NUMBER(9),
  bussinessid        NUMBER(9),
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );
create table SAP_15_SYNC
(
  syncnumber         NUMBER(9),
  bussinessid        NUMBER(9),
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );

create table SAP_16_SYNC
(
  syncnumber         NUMBER(9),
  bussinessid        NUMBER(9),
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );

create table SAP_INVENTORY_SYNC
(
  syncnumber         NUMBER,
  bussinessid        NUMBER,
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 8
    minextents 1
    maxextents unlimited
  );

create table SAP_PARTSINBOUND_SYNC
(
  syncnumber         NUMBER,
  bussinessid        NUMBER,
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 8
    minextents 1
    maxextents unlimited
  );

create table SAP_PARTSOUTBOUND_SYNC
(
  syncnumber         NUMBER,
  bussinessid        NUMBER,
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128
    next 8
    minextents 1
    maxextents unlimited
  );

create table SAP_PURCHASERTNOUTBOUND_SYNC
(
  syncnumber         NUMBER,
  bussinessid        NUMBER,
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 8
    minextents 1
    maxextents unlimited
  );

create table SAP_PURCHASERTNSETTLE_SYNC
(
  syncnumber         NUMBER,
  bussinessid        NUMBER,
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 8
    minextents 1
    maxextents unlimited
  );

create table SAP_PURCHASESETTLE_SYNC
(
  syncnumber         NUMBER,
  bussinessid        NUMBER,
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 8
    minextents 1
    maxextents unlimited
  );


create table SAP_REQUISITIONSETTLE_SYNC
(
  syncnumber         NUMBER,
  bussinessid        NUMBER,
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 8
    minextents 1
    maxextents unlimited
  );

create table SAP_SALESRTNINBOUND_SYNC
(
  syncnumber         NUMBER,
  bussinessid        NUMBER,
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 8
    minextents 1
    maxextents unlimited
  );

create table SAP_SALESRTNSETTLE_SYNC
(
  syncnumber         NUMBER,
  bussinessid        NUMBER,
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 8
    minextents 1
    maxextents unlimited
  );

create table SAP_SALESRTNSETTLEAPPROVE_SYNC
(
  syncnumber         NUMBER(9),
  bussinessid        NUMBER(9),
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255;

create table SAP_SALESSETTLE_SYNC
(
  syncnumber         NUMBER,
  bussinessid        NUMBER,
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 8
    minextents 1
    maxextents unlimited
  );

create table SAP_SALESSETTLEAPPROVE_SYNC
(
  syncnumber         NUMBER,
  bussinessid        NUMBER,
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );


create table SAP_TRANSFER_SYNC
(
  syncnumber         NUMBER,
  bussinessid        NUMBER,
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 8
    minextents 1
    maxextents unlimited
  );

create table SAP_WAREHOUSECOSTCHANGE_SYNC
(
  syncnumber         NUMBER,
  bussinessid        NUMBER,
  bussinesstablename VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 8
    minextents 1
    maxextents unlimited
  );


create sequence s_SAP_INVENTORY_SYNC
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

create sequence s_SAP_PARTSINBOUND_SYNC
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

create sequence s_SAP_PARTSOUTBOUND_SYNC
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

create sequence s_SAP_PURCHASERTNOUTBOUND_SYNC
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

create sequence s_SAP_PURCHASERTNSETTLE_SYNC
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

create sequence s_SAP_PURCHASESETTLE_SYNC
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

create sequence s_SAP_REQUISITIONSETTLE_SYNC
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

create sequence s_SAP_SALESRTNINBOUND_SYNC
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

create sequence s_SAP_SALESRTNSETTLE_SYNC
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

create sequence s_SAP_SALESRTNSETTLEAPP_SYNC
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;


create sequence s_SAP_SALESSETTLE_SYNC
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;


create sequence s_SAP_SALESSETTLEAPPROVE_SYNC
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;


create sequence s_SAP_TRANSFER_SYNC
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;


create sequence s_SAP_WAREHOUSECOSTCHANGE_SYNC
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

create table SYNCSTATU
(
  OBJID        VARCHAR2(38) not null,
  CODE         VARCHAR2(255),
  ACTIVEFIELD  VARCHAR2(255),
  SRCSYSNAME   VARCHAR2(255),
  SRCORGANNAME VARCHAR2(255),
  SRCBILLNAME  VARCHAR2(255),
  TAGBILLNAME  VARCHAR2(255),
  SYNCNUM      VARCHAR2(38),
  MODIFYTIME   DATE default sysdate,
  ERROSTRING   CLOB
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 16
    next 1
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table SYNCSTATU
  add constraint PK_SYNCSTATU_OBJID primary key (OBJID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

insert into syncstatu (OBJID, CODE, ACTIVEFIELD, SRCSYSNAME, SRCORGANNAME, SRCBILLNAME, TAGBILLNAME, SYNCNUM, MODIFYTIME)
values ('{6A3FC3A6-A7C0-468B-8724-FA43F0A40B43}', 'newpms_T_in', 'SYNCNUM', 'wmsinf', 'wmsinf', 't_in', 't_in', '1109', to_date('20-06-2014 12:15:07', 'dd-mm-yyyy hh24:mi:ss'));

insert into syncstatu (OBJID, CODE, ACTIVEFIELD, SRCSYSNAME, SRCORGANNAME, SRCBILLNAME, TAGBILLNAME, SYNCNUM, MODIFYTIME)
values ('{8C17555A-1FAE-4D55-AF0C-D5DE87F026E4}', 'newpms_t_warehouse', 'SYNCNUM', 'wmsinf', 'wmsinf', 't_warehouse', 't_warehouse', '0', to_date('22-12-2011 10:08:01', 'dd-mm-yyyy hh24:mi:ss'));

insert into syncstatu (OBJID, CODE, ACTIVEFIELD, SRCSYSNAME, SRCORGANNAME, SRCBILLNAME, TAGBILLNAME, SYNCNUM, MODIFYTIME)
values ('{D6F8772A-EB3C-4A43-8D84-EFE91DED5BDF}', 'newpms_t_shippingorderhead', 'SYNCNUM', 'wmsinf', 'wmsinf', 't_shippingorderhead', 't_shippingorderhead', '0', to_date('18-06-2014 17:07:00', 'dd-mm-yyyy hh24:mi:ss'));

insert into syncstatu (OBJID, CODE, ACTIVEFIELD, SRCSYSNAME, SRCORGANNAME, SRCBILLNAME, TAGBILLNAME, SYNCNUM, MODIFYTIME)
values ('{5316F1CC-1055-4701-89C2-A3CA75C6EC8E}', 'newpms_t_inventory_adjustment', 'SYNCNUM', 'wmsinf', 'wmsinf', 't_inventory_adjustment', 't_inventory_adjustment', '0', to_date('18-06-2014 10:22:06', 'dd-mm-yyyy hh24:mi:ss'));

