
  sys.dbms_scheduler.create_job(job_name            => 'JOB_BATCHPLANNEDPRICEAPP',
                                job_type            => 'EXECUTABLE',
                                job_action          => 'BatchPlannedPriceApp',
                                start_date          => to_date('18-09-2013 00:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Minutely;Interval=5',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/


  sys.dbms_scheduler.create_job(job_name            => 'WMS接口仓库数据同步',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'syncwmsinout.SYNCWarehouses',
                                start_date          => to_date('08-08-2014 10:30:36', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Minutely;Interval=10',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/

begin
  sys.dbms_scheduler.create_job(job_name            => 'WMS接口出库数据同步',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'syncwmsinout.SYNCTraffic',
                                start_date          => to_date('18-08-2014 19:11:48', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Minutely;Interval=10',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/

begin
  sys.dbms_scheduler.create_job(job_name            => 'WMS接口入库检验数据同步',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'syncwmsinout.SYNCCheck',
                                start_date          => to_date('08-08-2014 10:30:36', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Minutely;Interval=10',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/

begin
  sys.dbms_scheduler.create_job(job_name            => 'WMS接口入库数据同步',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'syncwmsinout.SYNCIn',
                                start_date          => to_date('08-08-2014 10:30:36', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Minutely;Interval=10',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/

begin
  sys.dbms_scheduler.create_job(job_name            => '财务接口快照',
                                job_type            => 'PLSQL_BLOCK',
                                job_action          => 'begin
    effectpartshistorystock;
  commit;
  end;',
                                start_date          => to_date('10-07-2014 00:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/

begin
  sys.dbms_scheduler.create_job(job_name            => '索赔结算指令',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'ExecuteClaimSettleInstruction',
                                start_date          => to_date('10-07-2014 00:33:19', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => '',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
begin
  sys.dbms_scheduler.create_job(job_name            => '自动设置采购订单入库状态12点',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'autosetpurchaseorderinstatus',
                                start_date          => to_date('29-08-2014 12:30:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/

begin
  sys.dbms_scheduler.create_job(job_name            => '自动设置采购订单入库状态20点',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'autosetpurchaseorderinstatus',
                                start_date          => to_date('29-08-2014 20:30:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
begin
  sys.dbms_scheduler.create_job(job_name            => '自动审核JOB',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoApproveClaimBill',
                                start_date          => to_date('26-05-2014 02:45:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
begin
  sys.dbms_scheduler.create_job(job_name            => '自动审核售前检查单JOB',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoApprovePreSaleCheckOrder',
                                start_date          => to_date('28-05-2014 01:30:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
begin
  sys.dbms_scheduler.create_job(job_name            => '自动失效信用申请',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'autoexpirecredit',
                                start_date          => to_date('07-08-2014 23:50:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
