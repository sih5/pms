

-- 根据Id查询旧件库区库位分类及其全部下级库区库位
create or replace function GetChildUsedPartsWhseAreas(Id integer) return SYS_REFCURSOR
as
  Result SYS_REFCURSOR;
  my_sql varchar2(4000);
begin
  my_sql := 'with tmp(id, parentid) as( '||
            '  select id, parentid from usedpartswarehousearea '||
            '  where id = '||to_char(Id)||
            '  union all '||
            '  select usedpartswarehousearea.id, usedpartswarehousearea.parentid '||
            '  from usedpartswarehousearea '||
            '  inner join tmp on tmp.id = usedpartswarehousearea.parentid '||
            ')  cycle id set dup_id TO ''Y'' DEFAULT ''N'' '||
            'select * from usedpartswarehousearea '||
            'where exists (select * from tmp where usedpartswarehousearea.id = tmp.id) ';
  open Result for my_sql;
  return(Result);
end;
/