


-- 根据Id查询库区库位分类及其全部下级库区库位
create or replace function GetChildWarehouseAreas(Id integer) return SYS_REFCURSOR
as
  Result SYS_REFCURSOR;
  my_sql varchar2(4000);
begin
  my_sql := 'with tmp(id, parentid) as( '||
            '  select id, parentid from warehousearea '||
            '  where id = '||to_char(Id)||
            '  union all '||
            '  select warehousearea.id, warehousearea.parentid '||
            '  from warehousearea '||
            '  inner join tmp on tmp.id = warehousearea.parentid '||
            ')  cycle id set dup_id TO ''Y'' DEFAULT ''N'' '||
            'select * from warehousearea '||
            'where exists (select * from tmp where warehousearea.id = tmp.id) ';
  open Result for my_sql;
  return(Result);
end;
/