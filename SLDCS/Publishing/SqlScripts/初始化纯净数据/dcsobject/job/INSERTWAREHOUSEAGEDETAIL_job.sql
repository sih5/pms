begin
  sys.dbms_scheduler.create_job(job_name            => 'INSERTWAREHOUSEAGEDETAIL_job',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'insertwarehouseagedetails',
                                start_date          => to_date('20-04-2016 21:30:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
