
  sys.dbms_scheduler.create_job(job_name            => 'JOB_BATCHPLANNEDPRICEAPP',
                                job_type            => 'EXECUTABLE',
                                job_action          => 'BatchPlannedPriceApp',
                                start_date          => to_date('18-09-2013 00:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Minutely;Interval=5',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;