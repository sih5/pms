-----------------每天定时删除符合条件的互换组信息
begin                
      sys.dbms_scheduler.create_job( 
                                job_name => 'MoveInvalidPartsExGroup_JOB',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'PRO_MoveInvalidPartsExGroup',
                                start_date          => to_date(to_char(sysdate,'yyyy-mm-dd')||' 22:00:00', 'yyyy-mm-dd hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                enabled             => true,
                                auto_drop           => false,
                               comments            => '每天22点定时删除符合条件的互换组信息');
end;
