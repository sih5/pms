

begin
  sys.dbms_scheduler.create_job(job_name            => 'WMS接口入库数据同步',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'syncwmsinout.SYNCIn',
                                start_date          => to_date('08-08-2014 10:30:36', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Minutely;Interval=10',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/