begin
  sys.dbms_scheduler.create_job(job_name            => '供应商索赔结算指令',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'ExecSupClaimSettleInstruction',
                                start_date          => to_date('09-03-2015 00:30:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => false,
                                auto_drop           => false,
                                comments            => '');
end;
/