begin
  sys.dbms_scheduler.create_job(job_name            => '内部领入自动设置入库状态',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoSetICBinstatus_n',
                                start_date          => to_date('07-04-2015 20:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;