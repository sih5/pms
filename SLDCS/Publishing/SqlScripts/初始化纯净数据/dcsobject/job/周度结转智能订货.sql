begin
  sys.dbms_scheduler.create_job(job_name            => '周度结转智能订货',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'InsertIntOrderWeeklyBase',
                                start_date          => to_date('02-11-2014 23:59:59', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Weekly;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => false,
                                auto_drop           => false,
                                comments            => '');
end;
/
