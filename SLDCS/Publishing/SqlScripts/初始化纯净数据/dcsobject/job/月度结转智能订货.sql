begin
  sys.dbms_scheduler.create_job(job_name            => '月度结转智能订货',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'InsertIntOrderMonthlyBase',
                                start_date          => to_date('30-11-2014 00:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Monthly;ByMonthDay=1;ByHour=0;ByMinute=0;BySecond=0',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
