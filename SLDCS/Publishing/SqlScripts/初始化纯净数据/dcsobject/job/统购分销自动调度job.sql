begin
  sys.dbms_scheduler.create_job(job_name            => '自动调度统购分销8点',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoFinishPurDistribution',
                                start_date          => to_date('15-06-2015 08:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
begin
  sys.dbms_scheduler.create_job(job_name            => '自动调度统购分销10点',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoFinishPurDistribution',
                                start_date          => to_date('15-06-2015 10:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
begin
  sys.dbms_scheduler.create_job(job_name            => '自动调度统购分销12点',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoFinishPurDistribution',
                                start_date          => to_date('15-06-2015 12:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
begin
  sys.dbms_scheduler.create_job(job_name            => '自动调度统购分销14点',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoFinishPurDistribution',
                                start_date          => to_date('15-06-2015 14:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
begin
  sys.dbms_scheduler.create_job(job_name            => '自动调度统购分销16点',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoFinishPurDistribution',
                                start_date          => to_date('15-06-2015 16:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
begin
  sys.dbms_scheduler.create_job(job_name            => '自动调度统购分销18点',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoFinishPurDistribution',
                                start_date          => to_date('15-06-2015 18:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
begin
  sys.dbms_scheduler.create_job(job_name            => '自动调度统购分销20点',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoFinishPurDistribution',
                                start_date          => to_date('15-06-2015 20:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/