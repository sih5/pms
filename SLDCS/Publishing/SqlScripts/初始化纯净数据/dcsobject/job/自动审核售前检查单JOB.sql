
begin
  sys.dbms_scheduler.create_job(job_name            => '自动审核售前检查单JOB',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoApprovePreSaleCheckOrder',
                                start_date          => to_date('28-05-2014 01:30:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/