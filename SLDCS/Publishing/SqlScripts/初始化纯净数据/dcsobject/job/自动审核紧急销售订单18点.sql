
begin
  sys.dbms_scheduler.create_job(job_name            => '自动审核紧急销售订单',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoCheckUrgentPartsOrders',
                                start_date          => to_date('09-08-2016 18:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => '',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/