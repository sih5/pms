
begin
  sys.dbms_scheduler.create_job(job_name            => '自动审核销售订单',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoSubmitSalesOrder',
                                start_date          => to_date('2016-10-09', 'yyyy-mm-dd'),
                                repeat_interval     => 'FREQ=MINUTELY;INTERVAL=5',
                                end_date            => to_date(null),
                                job_class           => '',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/