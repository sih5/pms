
  begin
  sys.dbms_scheduler.create_job(job_name            => '自动更新省市县',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoUpdateRegionID',
                                start_date          => to_date('16-12-2015 05:15:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;