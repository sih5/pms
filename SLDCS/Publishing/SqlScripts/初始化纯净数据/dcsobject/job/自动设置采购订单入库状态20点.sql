
begin
  sys.dbms_scheduler.create_job(job_name            => '自动设置采购订单入库状态20点',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'autosetpurchaseorderinstatus_n',
                                start_date          => to_date('29-08-2014 20:30:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/