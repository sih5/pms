

begin
  sys.dbms_scheduler.create_job(job_name            => '财务接口快照',
                                job_type            => 'PLSQL_BLOCK',
                                job_action          => 'begin
    effectpartshistorystock;
  commit;
  end;',
                                start_date          => to_date('10-07-2014 00:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
