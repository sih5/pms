CREATE OR REPLACE Procedure AutoSetICBoutstatus_n As
Begin
  update PartsPurReturnOrder
     set Outstatus = 99
   where Outstatus In (1, 2)
     and status = 99;
  commit;
  update PartsPurReturnOrder
     set Outstatus = 2
   where Outstatus = 1
     and exists
   (select 1
            from PartsOutboundPlan
           where PartsPurReturnOrder.code = PartsOutboundPlan.sourcecode and PartsOutboundPlan.status=2);
  commit;
    update PartsPurReturnOrder
     set Outstatus = 3
   where Outstatus  in(1,2)
     and exists
   (select 1
            from PartsOutboundPlan
           where PartsPurReturnOrder.code = PartsOutboundPlan.sourcecode and PartsOutboundPlan.status=3);
 commit;
      update PartsPurReturnOrder
     set Outstatus = 4
   where Outstatus  in(1,2)
     and exists
   (select 1
            from PartsOutboundPlan
           where PartsPurReturnOrder.code = PartsOutboundPlan.sourcecode and PartsOutboundPlan.status=4);
 commit;        
end;
