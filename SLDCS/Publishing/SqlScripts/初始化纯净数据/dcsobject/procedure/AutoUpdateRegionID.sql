create or replace procedure AutoUpdateRegionID as
begin
  update company
     set regionid =
         (select a.id
            from tiledregion a
           where a.provincename = company.provincename
             and a.cityname = company.cityname
             and a.countyname = company.countyname)
   where exists (select 1
            from tiledregion a
           where a.provincename = company.provincename
             and a.cityname = company.cityname
             and a.countyname = company.countyname)
     and company.provincename is not null
     and company.cityname is not null
     and company.countyname is not null and company.status<>99;
   update company
      set regionid =
          (select a.id
             from tiledregion a
            where a.provincename = company.provincename
              and a.cityname = company.cityname
              and a.countyname is null)
    where exists (select 1
             from tiledregion a
            where a.provincename = company.provincename
              and a.cityname = company.cityname
              and a.countyname is null)
      and company.provincename is not null
      and company.cityname is not null
      and company.countyname is null and company.status<>99;
    update company
       set regionid =
           (select a.id
              from tiledregion a
             where a.provincename = company.provincename
               and a.cityname  is null
               and a.countyname is null)
     where exists (select 1
              from tiledregion a
             where a.provincename = company.provincename
               and a.cityname  is null
               and a.countyname is null)
       and company.provincename is not null
       and company.cityname is null
       and company.countyname is null and company.status<>99 ;

  end;
  