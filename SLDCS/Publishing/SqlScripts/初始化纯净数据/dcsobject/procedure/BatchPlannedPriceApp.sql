
create or replace procedure BatchPlannedPriceApp
as
  cursor PlannedPriceApps is
    select Id from plannedpriceapp where Status = 2 and PlannedExecutionTime <= sysdate order by PlannedExecutionTime asc;
  PlannedpPriceAppRow PlannedPriceApps%rowtype;

begin
  for PlannedpPriceAppRow in PlannedPriceApps loop
    ExecutePlannedPriceApp(PlannedpPriceAppRow.id);
  end loop;
  commit;
end;