CREATE OR REPLACE PROCEDURE ExecutePlannedPriceApp(priceAppId IN NUMBER) AS
  /*变更前价格合计*/
  sumMoney_Before NUMBER(19, 4);
  /*变更后价格合计*/
  sumMoney_After NUMBER(19, 4);
  /*变更后价格差异合计*/
  sumMoney_Diff NUMBER(19, 4);
  /*仓库成本变更单自增长Id*/
  n INTEGER;
   a integer;
  /*计划价申请单*/
  FOwnerCompanyType INTEGER;
  FPlannedPriceApp  PlannedPriceApp%rowtype;
  /*自动获取单据编码相关*/
  FDateFormat   DATE;
  FDateStr      VARCHAR2(8);
  FSerialLength INTEGER;
  FNewCode      VARCHAR2(50);
  FComplateId   INTEGER;
  CURSOR warehouses IS
    SELECT w.Id, w.Code, w.Name, w.Type
      FROM Warehouse w
     INNER JOIN salesunitaffiwarehouse sw
        ON w.Id = sw.warehouseid
     INNER JOIN salesunit s
        ON sw.salesunitid = s.id
     WHERE StorageCompanyId = FPlannedPriceApp.OwnerCompanyId
       AND s.partssalescategoryid = FPlannedPriceApp.Partssalescategoryid
       AND w.Status = 1
       AND w.Type IN (1, 2);
       cursor PartsPlannedPrices is
       select id from PartsPlannedPrice price
   where exists
   (select 1
            from PlannedPriceAppDetail
           WHERE PlannedPriceAppId = FPlannedPriceApp.Id
             and price.sparepartid = PlannedPriceAppDetail.SparePartId) 
             and price.OwnerCompanyId = FPlannedPriceApp.OwnerCompanyId
             and price.partssalescategoryid = FPlannedPriceApp.partssalescategoryid
     for update;
  PartsPlannedPrice PartsPlannedPrices%rowtype;
  warehouse warehouses%rowtype;
BEGIN
  /*编码规则变更，需同步调整*/
  FDateFormat := TRUNC(sysdate, 'YEAR');
  --FDateFormat:=trunc(sysdate,'MONTH');
  --FDateFormat   := trunc(sysdate, 'DD');
  FDateStr      := TO_CHAR(sysdate, 'yyyy');
  FSerialLength := 4;
  FNewCode      := 'WCC{CORPCODE}' || FDateStr || '{SERIAL}';
  BEGIN
    SELECT *
      INTO FPlannedPriceApp
      FROM PlannedPriceApp
     WHERE Id = priceAppId
       AND Status = 2;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      raise_application_error(-20001,
                              '只允许执行处于“已审核”状态的计划价申请单。');
    WHEN OTHERS THEN
      raise;
  END;
  -- 获取隶属企业类型
  BEGIN
    SELECT Type
      INTO FOwnerCompanyType
      FROM Company
     WHERE Id = FPlannedPriceApp.OwnerCompanyId;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      raise_application_error(-20002, '计划价申请单隶属企业不存在。');
    WHEN OTHERS THEN
      raise;
  END;
  /*获取编号生成模板Id*/
  BEGIN
    SELECT Id
      INTO FComplateId
      FROM CodeTemplate
     WHERE name = 'WarehouseCostChangeBill'
       AND IsActived = 1;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      raise_application_error(-20003,
                              '未找到名称为"WarehouseCostChangeBill"的有效编码规则。');
    WHEN OTHERS THEN
      raise;
  END;
  for PartsPlannedPrice in PartsPlannedPrices loop
       a:=a+1;
       end loop;
  /*更新计划价申请单，清单赋值变更前计划价*/
  UPDATE PlannedPriceAppDetail detail
     SET (PriceBeforeChange, Quantity, AmountDifference) =
         (SELECT NVL(PlannedPrice, 0), 0, 0
            FROM PartsPlannedPrice price
           WHERE OwnerCompanyId = FPlannedPriceApp.OwnerCompanyId
             AND price.partssalescategoryid =
                 FPlannedPriceApp.partssalescategoryid
             AND price.SparePartId = detail.SparePartId)
   WHERE detail.PlannedPriceAppId = FPlannedPriceApp.Id
     AND EXISTS
   (SELECT 1
            FROM PartsPlannedPrice price
           WHERE price.OwnerCompanyId = FPlannedPriceApp.OwnerCompanyId
             AND price.partssalescategoryid =
                 FPlannedPriceApp.partssalescategoryid
             AND price.SparePartId = detail.SparePartId);
  UPDATE PlannedPriceAppDetail detail
     SET PriceBeforeChange = 0, Quantity = 0, AmountDifference = 0
   WHERE detail.PlannedPriceAppId = FPlannedPriceApp.Id
     AND NOT EXISTS
   (SELECT 1
            FROM PartsPlannedPrice price
           WHERE price.OwnerCompanyId = FPlannedPriceApp.OwnerCompanyId
             AND price.partssalescategoryid =
                 FPlannedPriceApp.partssalescategoryid
             AND price.SparePartId = detail.SparePartId);
  /*更新计划价申请单，清单赋值数量、差异金额(数量包括在库中的数量和在途的数量，在途数量从企业调拨在途配件成本中获取，在库中的数量从配件库存中获取）*/
  -----------------------------------------------
 begin
    declare
    type tempRecordStruct is record(
       sparepartid number ,
       quantity number
    );
    type tempTableStruct is table of tempRecordStruct index by binary_integer;
    tempTable  tempTableStruct;
    vkey number;
    vIndex number:=0;
    tempQuantity number:=0;
    begin
    for item in (SELECT tmp.sparepartid, SUM(tmp.tempquantity) AS quantity
                        FROM (SELECT a.partid AS sparepartId,
                                     NVL(a.quantity, 0) AS tempquantity
                                FROM partsstock a
                               INNER JOIN SalesUnitAffiWarehouse
                                  ON A.warehouseid =
                                     SalesUnitAffiWarehouse.Warehouseid
                               INNER JOIN Salesunit
                                  ON SalesUnitAffiWarehouse.Salesunitid =
                                     Salesunit.ID
                               WHERE A.StorageCompanyType = 1 and A.storagecompanyid=FPlannedPriceApp.Ownercompanyid
                                 AND Salesunit.Partssalescategoryid = 6
                                 and a.partid in (select sparepartid from PlannedPriceAppDetail where PlannedPriceAppid=FPlannedPriceApp.id )
                              UNION ALL
                              SELECT pob.SparePartId,
                                     (pob.OutboundAmount -
                                     NVL((select picb.InspectedQuantity
                                            from (select sum(InspectedQuantity) InspectedQuantity,
                                                         OriginalRequirementBillCode,
                                                         sparepartid
                                                    from PartsInboundCheckBill picb
                                                   inner JOIN PartsInboundCheckBillDetail picbd
                                                      ON picb.Id =
                                                         picbd.PartsInboundCheckBillId
                                                   where picb.InboundType = 3
                                                   group by OriginalRequirementBillCode,
                                                            sparepartid) picb
                                           where pob.sparepartid = picb.sparepartid
                                             and picb.OriginalRequirementBillCode =
                                                 pto.code),
                                          0))
                                FROM PartsTransferOrder pto
                               INNER JOIN SalesUnitAffiWarehouse
                                  ON pto.destwarehouseid =
                                     SalesUnitAffiWarehouse.Warehouseid
                               INNER JOIN Salesunit
                                  ON SalesUnitAffiWarehouse.Salesunitid =
                                     Salesunit.ID
                               inner join (select OriginalRequirementBillCode,
                                                  sum(OutboundAmount) OutboundAmount,
                                                  SparePartId
                                             from PartsOutboundBill pob
                                            INNER JOIN PartsOutboundBillDetail pobd
                                               ON pob.Id = pobd.PartsOutboundBillId
                                            where pob.OutboundType = 4
                                              and pobd.sparepartid in (select sparepartid from PlannedPriceAppDetail where PlannedPriceAppid=FPlannedPriceApp.id )
                                            group by OriginalRequirementBillCode,
                                                     SparePartId) pob
                                  on pob.OriginalRequirementBillCode = pto.Code
                               WHERE Salesunit.Partssalescategoryid = 6
                                 and pto.storagecompanytype = 1 AND PTO.STORAGECOMPANYID=FPlannedPriceApp.Ownercompanyid) tmp
                       GROUP BY tmp.sparepartid)
           loop
              tempTable(vIndex).sparepartid:= item.sparepartid;
              tempTable(vIndex).quantity:=item.quantity;
              vIndex:=vIndex+1;
           end loop;
           for tempdetailitem in (select * from PlannedPriceAppDetail where PlannedPriceAppid=FPlannedPriceApp.id)
           loop
              vkey:=tempTable.first;
              while(vkey is not null) loop
                 if(tempdetailitem.sparepartid=tempTable(vkey).sparepartid) then
                   tempQuantity:=tempTable(vkey).quantity;
                   exit;
                 end if;
                 vkey:=tempTable.next(vkey);
              end loop;
              update PlannedPriceAppDetail set quantity=tempQuantity,Amountdifference=(nvl(PlannedPriceAppDetail.Requestedprice,0)-nvl(Pricebeforechange,0))*tempQuantity where id=tempdetailitem.id;
           end loop;
     end;
  end;
  -----------------------------------------------
  /*更新配件计划价，如果不存在则新增*/
  merge INTO PartsPlannedPrice price
  USING (SELECT *
           FROM PlannedPriceAppDetail
          WHERE PlannedPriceAppId = FPlannedPriceApp.Id) detail
  ON (price.OwnerCompanyId = FPlannedPriceApp.OwnerCompanyId AND price.SparePartId = detail.SparePartId AND price.partssalescategoryid = FPlannedPriceApp.partssalescategoryid)
  WHEN matched THEN
    UPDATE
       SET price.plannedprice = detail.RequestedPrice,
           price.modifierid   = FPlannedPriceApp.Modifierid,
           price.modifiername = FPlannedPriceApp.Modifiername,
           price.modifytime   = FPlannedPriceApp.Modifytime
  WHEN NOT matched THEN
    INSERT
      (Id,
       OwnerCompanyId,
       OwnerCompanyType,
       SparePartId,
       PlannedPrice,
       CreatorId,
       CreatorName,
       CreateTime,
       PartsSalesCategoryId,
       PartsSalesCategoryName)
    VALUES
      (S_PartsPlannedPrice.Nextval,
       FPlannedPriceApp.OwnerCompanyId,
       FOwnerCompanyType,
       detail.SparePartId,
       detail.RequestedPrice,
       FPlannedPriceApp.CreatorId,
       FPlannedPriceApp.CreatorName,
       sysdate,
       FPlannedPriceApp.PartsSalesCategoryId,
       FPlannedPriceApp.PartsSalesCategoryName);
  /*更新计划价申请单*/
  UPDATE PlannedPriceApp
     SET ActualExecutionTime = SYSDATE,
         Status = 3,
         (AmountBeforeChange, AmountAfterChange, AmountDifference) =
         (SELECT SUM(PriceBeforeChange * Quantity),
                 SUM(RequestedPrice * Quantity),
                 SUM(AmountDifference)
            FROM plannedPriceAppDetail
           WHERE plannedPriceAppDetail.PlannedPriceAppId =
                 FPlannedPriceApp.Id)
   WHERE ID = FPlannedPriceApp.ID;
  /*针对每一个仓库，生成仓库成本变更单和清单*/
  FOR warehouse IN warehouses LOOP
    /*获取自增长Id，插入库存成本变更单*/
    SELECT S_WAREHOUSECOSTCHANGEBILL.NEXTVAL INTO n FROM dual;
    /*插入库存成本变更清单*/
    INSERT INTO WarehouseCostChangeDetail
      (Id,
       WarehouseCostChangeBillId,
       WarehouseId,
       SparePartId,
       SparePartCode,
       SparePartName,
       Pricebeforechange,
       Quantity,
       PriceAfterChange,
       AmountDifference)
      SELECT S_WAREHOUSECOSTCHANGEDETAIL.NEXTVAL,
             n,
             warehouse.Id,
             SparePartId,
             SparePartCode,
             SparePartName,
             PriceBeforeChange,
             stock.TotalQuantity,
             RequestedPrice,
             (RequestedPrice - NVL(PriceBeforeChange, 0)) *
             stock.TotalQuantity
        FROM (SELECT t.PartId, SUM(Quantity) AS TotalQuantity
                FROM (SELECT PartId, Quantity
                        FROM PartsStock
                       WHERE StorageCompanyType = 1
                         AND WarehouseId = warehouse.Id
                         AND EXISTS (SELECT 1
                                FROM SalesUnitAffiWarehouse
                               WHERE SalesUnitAffiWarehouse.Warehouseid =
                                     warehouse.Id
                                 AND SalesUnitAffiWarehouse.Salesunitid IN
                                     (SELECT id
                                        FROM Salesunit
                                       WHERE Salesunit.Partssalescategoryid =
                                             FPlannedPriceApp.Partssalescategoryid))
                         AND EXISTS (SELECT 1
                                FROM PlannedPriceAppDetail
                               WHERE PlannedPriceAppDetail.PlannedPriceAppId =
                                     FPlannedPriceApp.Id
                                 AND PlannedPriceAppDetail.SparePartId =
                                     PartsStock.PartId)
                      UNION ALL
                      SELECT -- pobd.SparePartId,
                      -- (pobd.OutboundAmount -
                       pob.SparePartId,
                       (pob.OutboundAmount - NVL( /*picbd.InspectedQuantity*/(select picb.InspectedQuantity
                                                    from /* PartsInboundCheckBill picb

                                                                                                                    inner JOIN PartsInboundCheckBillDetail picbd
                                                                                                                   ON picb.Id =
                                                                                                                      picbd.PartsInboundCheckBillId*/
                                                         (select sum(InspectedQuantity) InspectedQuantity,
                                                                 OriginalRequirementBillCode,
                                                                 sparepartid
                                                            from PartsInboundCheckBill picb
                                                           inner JOIN PartsInboundCheckBillDetail picbd
                                                              ON picb.Id =
                                                                 picbd.PartsInboundCheckBillId
                                                           where picb.InboundType = 3
                                                           group by OriginalRequirementBillCode,
                                                                    sparepartid) picb
                                                  --  where pobd.sparepartid =
                                                   where pob.sparepartid =
                                                         picb.sparepartid
                                                     --and picb.InboundType = 3
                                                     and picb.OriginalRequirementBillCode =
                                                         pto.code),
                                                 0))
                        FROM PartsTransferOrder pto
                      /*  INNER JOIN PartsOutboundBill pob
                         ON pob.OriginalRequirementBillCode = pto.Code
                        AND pob.OutboundType = 4
                      INNER JOIN PartsOutboundBillDetail pobd
                         ON pob.Id = pobd.PartsOutboundBillId*/
                      /* LEFT JOIN PartsInboundCheckBill picb
                      ON picb.OriginalRequirementBillCode = pto.code
                      AND picb.InboundType                = 3
                      LEFT JOIN PartsInboundCheckBillDetail picbd
                      ON picb.Id                  = picbd.PartsInboundCheckBillId
                      AND pobd.sparepartid        = picbd.sparepartid*/
                       inner join (select OriginalRequirementBillCode,
                                          sum(OutboundAmount) OutboundAmount,
                                          SparePartId/*,
                                          partssalescategoryid*/
                                     from PartsOutboundBill pob
                                    INNER JOIN PartsOutboundBillDetail pobd
                                       ON pob.Id = pobd.PartsOutboundBillId
                                    where pob.OutboundType = 4
                                    group by OriginalRequirementBillCode,
                                             SparePartId/*,
                                             partssalescategoryid*/) pob
                          on pob.OriginalRequirementBillCode = pto.Code
                       WHERE pto.DestWarehouseId = warehouse.Id
                        /* AND pob.partssalescategoryid =
                             FPlannedPriceApp.Partssalescategoryid*/
                         AND EXISTS (SELECT 1
                                FROM PlannedPriceAppDetail
                               WHERE PlannedPriceAppDetail.PlannedPriceAppId =
                                     FPlannedPriceApp.Id
                                 AND PlannedPriceAppDetail.SparePartId =
                                    --pobd.SparePartId)) t
                                     pob.SparePartId)) t
               GROUP BY t.PartId) stock
       INNER JOIN PlannedPriceAppDetail
          ON PlannedPriceAppDetail.SparePartId = stock.PartId
       WHERE PlannedPriceAppDetail.PlannedPriceAppId = FPlannedPriceApp.Id;
    /*不存在变更清单，仍然需要生成主单*/
    /*计算该仓库下的合计*/
    SELECT NVL(SUM(PriceBeforechange * Quantity), 0),
           NVL(SUM(PriceAfterChange * Quantity), 0),
           NVL(SUM(Amountdifference), 0)
      INTO sumMoney_Before, sumMoney_After, sumMoney_Diff
      FROM WarehouseCostChangeDetail
     WHERE WarehouseCostChangeBillId = n;
    /*插入主单*/
    INSERT INTO WarehouseCostChangeBill
      (Id,
       Code,
       PlannedPriceAppId,
       OwnerCompanyId,
       OwnerCompanyCode,
       OwnerCompanyName,
       WarehouseId,
       WarehouseCode,
       WarehouseName,
       AmountBeforeChange,
       AmountAfterChange,
       AmountDifference,
       RecordStatus,
       CreatorId,
       CreatorName,
       CreateTime)
    VALUES
      (n,
       regexp_replace(regexp_replace(FNewCode,
                                     '{CORPCODE}',
                                     FPlannedPriceApp.OwnerCompanyCode),
                      '{SERIAL}',
                      lpad(TO_CHAR(GetSerial(FComplateId,
                                             FDateFormat,
                                             FPlannedPriceApp.OwnerCompanyCode)),
                           FSerialLength,
                           '0')),
       FPlannedPriceApp.Id,
       FPlannedPriceApp.OwnerCompanyId,
       FPlannedPriceApp.OwnerCompanyCode,
       FPlannedPriceApp.OwnerCompanyName,
       warehouse.Id,
       warehouse.Code,
       warehouse.Name,
       sumMoney_Before,
       sumMoney_After,
       sumMoney_Diff,
       2,
       FPlannedPriceApp.CreatorId,
       FPlannedPriceApp.CreatorName,
       sysdate);
  END LOOP;
END;
