create or replace procedure InsertIntOrderWeeklyBase is
begin
  insert into IntelligentOrderWeeklyBase
( ID                    ,
  BRANCHID              ,
  BRANCHCODE            ,
  BRANCHNAME            ,
  ORDERTYPEID           ,
  ORDERTYPENAME         ,
  IFDIRECTPROVISION     ,
  SALESCATEGORYID       ,
  SALESCATEGORYCODE     ,
  SALESCATEGORYNAME     ,
  PARTID                ,
  PARTCODE              ,
  PARTNAME              ,
  SUPPLIERID            ,
  SUPPLIERCODE          ,
  SUPPLIERNAME          ,
  PARTABC               ,
  PRODUCTLIFECYCLE      ,
  PARTSBRANCHCREATETIME ,
  PLANNEDPRICE          ,
  PLANNEDPRICETYPE      ,
  STOCKQUANTITY         ,
  STOCKAMOUNT           ,
  BUSINESSTYPE          ,
  TOTALQUANTITY         ,
  TOTALWEEKLYFREQUENCY  ,
  YEARD                 ,
  WEEKLYD               )
select s_IntelligentOrderWeeklyBase.Nextval, BRANCHID              ,
  BRANCHCODE            ,
  BRANCHNAME            ,
  ORDERTYPEID           ,
  ORDERTYPENAME         ,
  IFDIRECTPROVISION     ,
  SALESCATEGORYID       ,
  SALESCATEGORYCODE     ,
  SALESCATEGORYNAME     ,
  PARTID                ,
  PARTCODE              ,
  PARTNAME              ,
  partssupplierid            ,
  SUPPLIERCODE          ,
  SUPPLIERNAME          ,
  PARTABC               ,
  PRODUCTLIFECYCLE      ,
  PARTSBRANCHCREATETIME ,
  PLANNEDPRICE          ,
  PLANNEDPRICETYPE      ,
  STOCKQUANTITY         ,
  STOCKAMOUNT           ,
  BUSINESSTYPE          ,
  TOTALQUANTITY         ,
  TOTALWEEKLYFREQUENCY  ,
  YEARD                 ,
  WEEKLYD         from (select branchid,
(select code from branch where id=branchid) branchcode,
(select name from branch where id=branchid) branchname,
 ORDERTYPEID,
 ORDERTYPENAME,
 IFDIRECTPROVISION,
 SALESCATEGORYID,
 (select code from partssalescategory where id=SALESCATEGORYID) SALESCATEGORYCODE,
 (select name from partssalescategory where id=SALESCATEGORYID) SALESCATEGORYname,
 partid,
 (select code from sparepart where id=partid) partcode,
 (select name from sparepart where id=partid) partname,
 partssupplierid,
 (select code from partssupplier where id=partssupplierid) suppliercode,
 (select name from partssupplier where id=partssupplierid) suppliername,
 (select PartABC from partsbranch where id=partsbranchid) PARTABC,
 (select ProductLifeCycle from partsbranch where id=partsbranchid) PRODUCTLIFECYCLE,
 (select createtime from partsbranch where id=partsbranchid) PARTSBRANCHCREATETIME,
  PLANNEDPRICE,
 (select null from partsbranch where id=partsbranchid) PLANNEDPRICETYPE,
 STOCKQUANTITY,
 nvl(STOCKQUANTITY,0)*nvl(PLANNEDPRICE,0) STOCKAMOUNT,BUSINESSTYPE,TOTALQUANTITY,TOTALWEEKLYFREQUENCY,YEARD��WEEKLYD
from (
select a.branchid,
       d.PartsPurchaseOrderTypeId ORDERTYPEID,
       d.ifdirectprovision IFDIRECTPROVISION,
       d.partssalescategoryid SALESCATEGORYID,
       (select name from PartsPurchaseOrderType where id= d.PartsPurchaseOrderTypeId ) ORDERTYPENAME, 
       a.partid,
       d.partssupplierid,
       e.id partsbranchid,
       (select partsplannedprice.plannedprice from partsplannedprice where partsplannedprice.sparepartid=  a.partid and partsplannedprice.partssalescategoryid=d.partssalescategoryid and partsplannedprice.ownercompanyid=a.branchid ) PLANNEDPRICE,
       sum(nvl(a.quantity,0)) STOCKQUANTITY,
       sum(c.inspectedquantity) TOTALQUANTITY,
       count(c.sparepartid) TOTALWEEKLYFREQUENCY,
       to_char(b.createtime, 'ww') WEEKLYD,
       to_char(b.createtime, 'yyyy') YEARD,1 BUSINESSTYPE
  from partsstock a
 inner join partsinboundcheckbill b
    on a.warehouseid = b.warehouseid
 inner join partsinboundcheckbilldetail c
    on c.sparepartid = a.partid
   and c.partsinboundcheckbillid = b.id
 inner join partspurchaseorder d
    on d.code = b.originalrequirementbillcode
 inner join partsbranch e
    on e.partid = a.partid
   and e.partssalescategoryid = d.partssalescategoryid
 group by a.BRANCHID,
          d.PartsPurchaseOrderTypeId,
          d.IFDIRECTPROVISION,
          d.partssalescategoryid,
          a.partid,
          d.partssupplierid,
          e.id,
          to_char(b.createtime, 'ww'),
          to_char(b.createtime, 'yyyy')
union
select a.BRANCHID,
       d.partssalesordertypeid ORDERTYPEID,
       null as IFDIRECTPROVISION,
       d.SALESCATEGORYID SALESCATEGORYID,
       (select name from PartsSalesOrderType where id= d.partssalesordertypeid ) ORDERTYPENAME, 
       a.partid,
       null as partssupplierid,
       e.id,
       (select partsplannedprice.plannedprice from partsplannedprice where partsplannedprice.sparepartid= a.partid and partsplannedprice.partssalescategoryid=d.SALESCATEGORYID and partsplannedprice.ownercompanyid=a.branchid ) PLANNEDPRICE,
       sum(nvl(a.quantity,0)) STOCKQUANTITY,
       sum(c.outboundamount) TOTALQUANTITY,
       count(c.sparepartid) TOTALWEEKLYFREQUENCY,
       to_char(b.createtime, 'ww') WEEKLYD,
       to_char(b.createtime, 'yyyy') YEARD,2
  from partsstock a
 inner join partsoutboundbill b
    on a.warehouseid = b.warehouseid
 inner join partsoutboundbilldetail c
    on c.sparepartid = a.partid
   and c.partsoutboundbillid = b.id
 inner join partssalesorder d
    on d.code = b.originalrequirementbillcode
 inner join partsbranch e
    on e.partid = a.partid
   and e.partssalescategoryid = d.SALESCATEGORYID
 group by a.BRANCHID,
          d.partssalesordertypeid,
          --d.IFDIRECTPROVISION,
          d.SALESCATEGORYID,
          a.partid,
          --d.partssupplierid,
          e.id,
          to_char(b.createtime, 'ww'),
          to_char(b.createtime, 'yyyy')
union
select a.BRANCHID,
       null ORDERTYPEID,
       null as IFDIRECTPROVISION,
       b.partssalescategoryid SALESCATEGORYID,
       null ORDERTYPENAME,
       a.partid,
       null as partssupplierid,
       e.id,
       (select partsplannedprice.plannedprice from partsplannedprice where partsplannedprice.sparepartid= a.partid and partsplannedprice.partssalescategoryid=b.partssalescategoryid and partsplannedprice.ownercompanyid=a.branchid ) PLANNEDPRICE,
       sum(nvl(a.quantity,0)) STOCKQUANTITY,
       sum(c.outboundamount) TOTALQUANTITY,
       count(c.sparepartid) TOTALWEEKLYFREQUENCY,
       to_char(b.createtime, 'ww') WEEKLYD,
       to_char(b.createtime, 'yyyy') YEARD,3
  from partsstock a
 inner join partsoutboundbill b
    on a.warehouseid = b.warehouseid
 inner join partsoutboundbilldetail c
    on c.sparepartid = a.partid
   and c.partsoutboundbillid = b.id
 inner join partsbranch e
    on e.partid = a.partid
   and e.partssalescategoryid = b.partssalescategoryid
 where exists (select 1
          from partssalescategory
         where name = 'ͳ��'
           and partssalescategory.id = b.id)
 group by a.BRANCHID,
          b.partssalescategoryid,
          a.partid,
          e.id,
          to_char(b.createtime, 'ww'),
          to_char(b.createtime, 'yyyy')
          ��
     )  where YEARD=to_char(sysdate,'yyyy') and WEEKLYD=to_char(sysdate,'ww');
end InsertIntOrderWeeklyBase;
/
