
create or replace procedure addPartsExchange as
  TCTABLE VARCHAR2(200) := 'truncate table ExchangeAdd_Tmp';
begin
  declare
    cursor Autoadd is(
      select id, code, referencecode
        from sparepart
       where not exists (select 1
                from PartsExchange
               where PartsExchange.Partid = sparepart.id)
         and sparepart.status = 1
         and exists (select 1
                from sparepart a
               where a.id <> sparepart.id
                 and a.referencecode = sparepart.referencecode
                 and a.status = 1)
         and rownum <= 1000);

  begin
    EXECUTE IMMEDIATE TCTABLE;


    for S_Autoadd in Autoadd loop
      begin
        insert into ExchangeAdd_Tmp
          (id,
           Exchangecode,
           Exchangename,
           Partid,
           Status,
           Creatorid,
           Creatorname,
           Createtime,
           remark)
          select s_PartsExchange.Nextval,
                 a.referencecode,
                 a.referencename,
                 a.id,
                 1,
                 1,
                 'Admin',
                 sysdate,
                 '自动批量处理互换号'
            from (select distinct case
                                    when tmp.referencecode is not null then
                                     tmp.referencecode
                                    else
                                     a.referencecode
                                  end as referencecode,
                                  case
                                    when tmp.referencecode is not null then
                                     tmp.referencecode
                                    else
                                     a.referencecode
                                  end as referencename,
                                  a.id
                    from sparepart a
                    left join (select a.exchangecode,
                                     a.exchangename,
                                     b.referencecode
                                from partsexchange a
                               inner join sparepart b
                                  on a.partid = b.id
                               where a.status = 1) tmp
                      on a.referencecode = tmp.referencecode
                   where not exists (select 1
                            from PartsExchange
                           where PartsExchange.Partid = a.id
                             and PartsExchange. status = 1)
                     and a.status = 1
                     and replace(a.referencecode, ' ', '') is not null
                     and a.id = S_Autoadd.id
                     and exists
                   (select 1
                            from sparepart p
                           where p.id <> a.id
                             and p.referencecode = a.referencecode
                             and p.status = 1)) a;



        insert into PartsExchange
          (id,
           Exchangecode,
           Exchangename,
           Partid,
           Status,
           Creatorid,
           Creatorname,
           Createtime,
           remark)
          select id,
                 Exchangecode,
                 Exchangename,
                 Partid,
                 Status,
                 Creatorid,
                 Creatorname,
                 Createtime,
                 remark
            from ExchangeAdd_Tmp;

        insert into PartsExchangeHistory
          (id,
           Partsexchangeid,
           Exchangecode,
           Exchangename,
           Partid,
           Status,
           Remark,
           Creatorid,
           Creatorname,
           Createtime)
          select s_PartsExchangeHistory.Nextval,
                 id,
                 Exchangecode,
                 Exchangename,
                 Partid,
                 Status,
                 remark,
                 Creatorid,
                 Creatorname,
                 Createtime
            from ExchangeAdd_Tmp;
                EXECUTE IMMEDIATE TCTABLE;
      end;
    end loop;
  end;
end addpartsexchange;