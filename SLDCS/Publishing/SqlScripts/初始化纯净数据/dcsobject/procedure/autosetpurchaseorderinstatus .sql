CREATE OR REPLACE Procedure Autosetpurchaseorderinstatus As
  Isallnew       Number(9);
  Isallfinish    Number(9);
  Hasforcefinish Number(9);
  Isforcefinish  Number(9);
  Iscanncel      Number(9);
Begin

  Declare
    Cursor Autoset Is(
      Select Id
        From Partspurchaseorder
       Where Status In (3, 4, 5, 6, 7, 99)
         And Instatus In (5, 10));
  
  Begin
    For s_Autoset In Autoset Loop
      Begin
        Select Count(*)
          Into Isallnew
          From Suppliershippingorder a
         Inner Join Partsinboundplan b
            On a.Code = b.Sourcecode
         Inner Join Partsinboundplandetail c
            On b.Id = c.Partsinboundplanid
         Where a.Partspurchaseorderid = s_Autoset.Id
           And Nvl(c.Inspectedquantity, 0) > 0;
        If Isallnew = 0 Then
          Update Partspurchaseorder
             Set Instatus = 5
           Where Id = s_Autoset.Id;
        End If;
        Select Count(*)
          Into Isallfinish
          From Partspurchaseorderdetail d
          Left Join (Select a.Partspurchaseorderid,
                            c.Sparepartid,
                            Sum(c.Inspectedquantity) Inspectedquantity,
                            Sum(c.Plannedamount) Plannedamount
                       From Suppliershippingorder a
                      Inner Join Partsinboundplan b
                         On a.Code = b.Sourcecode and b.storagecompanyid=a.branchid
                     /* Inner Join Warehouse w
                         On b.Warehouseid = w.Id*/
                      Inner Join Partsinboundplandetail c
                         On b.Id = c.Partsinboundplanid
                      Inner Join Partspurchaseorderdetail d
                         On d.Partspurchaseorderid = a.Partspurchaseorderid
                        And d.Sparepartid = c.Sparepartid
                      Where a.Partspurchaseorderid = s_Autoset.Id
                      --  And w.Type != 99 
                     -- and ((b.warehouseid is null)or exists(select 1 from warehouse where warehouse.id=b.warehouseid and warehouse.type<>99))
                      Group By c.Sparepartid, a.Partspurchaseorderid) t
            On d.Partspurchaseorderid = t.Partspurchaseorderid
           And d.Sparepartid = t.Sparepartid
         Where ((t.Inspectedquantity <> t.Plannedamount) Or
               (Nvl(d.Confirmedamount, 0) <> Nvl(t.Inspectedquantity, 0)) Or
               (d.Confirmedamount Is Null Or d.Confirmedamount = 0))
           And d.Partspurchaseorderid = s_Autoset.Id;
      
        Select Count(*)
          Into Iscanncel
          From Partspurchaseorder
         Where Id = s_Autoset.Id
           And Status = 99;
        Select Count(*)
          Into Hasforcefinish
          From Partspurchaseorder a
         Inner Join Partspurchaseorderdetail b
            On a.Id = b.Partspurchaseorderid
          Left Join Suppliershippingorder c
            On c.Partspurchaseordercode = a.Code
          Left Join Suppliershippingdetail d
            On d.Suppliershippingorderid = c.Id
           And b.Sparepartid = d.Sparepartid
          Left Join Partsinboundplan e
            On e.Sourcecode = c.Code and e.storagecompanyid=a.branchid
          Left Join Partsinboundplandetail f
            On f.Partsinboundplanid = e.Id
           And b.Sparepartid = f.Sparepartid
         Where (a.Status = 7 Or c.Status = 4 Or e.Status = 3)
           And a.Id = s_Autoset.Id
           -- and ((e.warehouseid is null)or exists(select 1 from warehouse where warehouse.id=e.warehouseid and warehouse.type<>99))
           ;
        Select Count(*)
          Into Isforcefinish
          From (Select *
                  From (Select a.Id          Aid,
                               c.Id          Cid,
                               e.Id          Eid,
                               b.Sparepartid,
                               -- d.sparepartid,
                               Sum(Nvl(b.Orderamount, 0) -
                                   Nvl(b.Confirmedamount, 0)) Partspurchaseorderamount,
                               Sum(Nvl(b.Confirmedamount, 0) -
                                   Nvl(b.Shippingamount, 0)) Shippingamount,
                               Sum(Nvl(d.Quantity, 0) -
                                   Nvl(d.Confirmedamount, 0)) Suppliershippingorderamount,
                               Sum(Nvl(f.Plannedamount, 0) -
                                   Nvl(f.Inspectedquantity, 0)) Partsinboundplanamount,
                               Sum(Nvl(f.Inspectedquantity, 0)) Inspectedquantity,
                               Sum(Nvl(b.Orderamount, 0)) Orderamount
                          From Partspurchaseorder a
                         Inner Join Partspurchaseorderdetail b
                            On a.Id = b.Partspurchaseorderid
                          Left Join Suppliershippingorder c
                            On c.Partspurchaseordercode = a.Code
                          Left Join Suppliershippingdetail d
                            On d.Suppliershippingorderid = c.Id
                           And b.Sparepartid = d.Sparepartid
                          Left Join Partsinboundplan e 
                            On e.Sourcecode = c.Code  and e.storagecompanyid=a.branchid
                          Left Join Partsinboundplandetail f
                            On f.Partsinboundplanid = e.Id
                           And b.Sparepartid = f.Sparepartid
                         Where (a.Status = 7 Or c.Status = 4 Or
                               (e.Status = 3 And f.Id Is Not Null))
                           And a.Id = s_Autoset.Id 
                          --  and ((e.warehouseid is null)or exists(select 1 from warehouse where warehouse.id=e.warehouseid and warehouse.type<>99))
                         Group By a.Id, c.Id, e.Id, b.Sparepartid --, d.sparepartid
                        )
                 Where Inspectedquantity + Partsinboundplanamount +
                       Suppliershippingorderamount +
                       Partspurchaseorderamount + Shippingamount <>
                       Orderamount
                Union
                Select *
                
                  From (Select a.Id          Aid,
                               c.Id          Cid,
                               e.Id          Eid,
                               b.Sparepartid,
                               -- d.sparepartid,
                               Sum(Nvl(b.Orderamount, 0) -
                                   Nvl(b.Confirmedamount, 0)) Partspurchaseorderamount,
                               Sum(Nvl(b.Confirmedamount, 0) -
                                   Nvl(b.Shippingamount, 0)) Shippingamount,
                               Sum(Nvl(d.Quantity, 0) -
                                   Nvl(d.Confirmedamount, 0)) Suppliershippingorderamount,
                               Sum(Nvl(f.Plannedamount, 0) -
                                   Nvl(f.Inspectedquantity, 0)) Partsinboundplanamount,
                               Sum(Nvl(f.Inspectedquantity, 0)) Inspectedquantity,
                               Sum(Nvl(b.Orderamount, 0)) Orderamount
                          From Partspurchaseorder a
                         Inner Join Partspurchaseorderdetail b
                            On a.Id = b.Partspurchaseorderid
                          Left Join Suppliershippingorder c
                            On c.Partspurchaseordercode = a.Code
                          Left Join Suppliershippingdetail d
                            On d.Suppliershippingorderid = c.Id
                           And b.Sparepartid = d.Sparepartid
                          Left Join Partsinboundplan e
                            On e.Sourcecode = c.Code  and e.storagecompanyid=a.branchid
                          Left Join Partsinboundplandetail f
                            On f.Partsinboundplanid = e.Id
                           And b.Sparepartid = f.Sparepartid
                         Where (a.Status = 7 Or c.Status = 4 Or e.Status = 3)
                           And (e.Status = 4 Or c.Status = 1 Or c.Status = 3 Or
                               e.Status = 1)
                               -- and ((e.warehouseid is null)or exists(select 1 from warehouse where warehouse.id=e.warehouseid and warehouse.type<>99))
                           And a.Id = s_Autoset.Id
                         Group By a.Id, c.Id, e.Id, b.Sparepartid --, d.sparepartid
                        ));
        If (Isallfinish = 0) And (Isallnew <> 0) Then
          Update Partspurchaseorder
             Set Instatus = 15
           Where Id = s_Autoset.Id;
        End If;
        If (Isallnew > 0) And (Isallfinish > 0) Then
          Update Partspurchaseorder
             Set Instatus = 10
           Where Id = s_Autoset.Id;
        End If;
        If (Hasforcefinish > 0) And (Isforcefinish = 0) /* and ( IsAllFinish<>0)*/
         Then
          Update Partspurchaseorder
             Set Instatus = 11
           Where Id = s_Autoset.Id;
        End If;
        If Iscanncel > 0 Then
          Update Partspurchaseorder
             Set Instatus = 99
           Where Id = s_Autoset.Id;
        End If;
      End;
    End Loop;
  End;
End Autosetpurchaseorderinstatus;
