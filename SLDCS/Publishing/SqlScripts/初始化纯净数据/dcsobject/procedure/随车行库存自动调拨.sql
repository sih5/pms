create table linshibiao(
code nvarchar2(50),
bcode nvarchar2(50),
warehouseid number,
tcwarehouseid number,
avalidstock number,
kykc number,
safestock number,
tcsafestock number,
partid number,
transfornum number,
WarehouseAreaId number,
Quantity number
)

create or replace procedure spCreateTestTable
is
    v_CreateString varchar2(1000);
begin
   
insert into linshibiao(code,bcode,warehouseid,tcwarehouseid,avalidstock,kykc,safestock,tcsafestock,partid,transfornum,WarehouseAreaId,Quantity)(
select tmp1.code,tmp1.bcode,tmp1.warehouseid,tmp2.tcwarehouseid,tmp1.avalidstock,tmp2.kykc,tmp1.safestock,tmp2.safestock as tcsafestock,tmp1.partid,
(case when (nvl(tmp1.stockmaximum,0)-nvl(avalidstock,0))<=(nvl(tmp2.kykc,0)-nvl(tmp2.safestock,0)) then nvl(tmp1.StockMaximum,0)-nvl(tmp1.avalidstock,0) else nvl(tmp2.kykc,0)-nvl(tmp2.SafeStock,0) end) as transfornum,
tmp2.WarehouseAreaId,tmp2.Quantity
 from
(
select ps.partid,vps.stockmaximum,vps.stockminimum,vps.safestock,c.code,b.code as bcode,w.id as warehouseid,
(nvl(sum(ps.quantity),0)-nvl(sum(pls.Lockedquantity),0)-nvl(sum(CongelationStockQty),0)-nvl(sum(DisabledStock),0)) as avalidstock from Warehouse w 
left join PartsStock ps on ps.warehouseid=w.id 
left join PartsLockedStock pls on pls.warehouseid=w.id and pls.partid=ps.partid
left join WmsCongelationStockView wcsv on wcsv.WarehouseId=w.id and wcsv.SparePartId=ps.partid
left join VehiclePartsStockLevel vps on vps.warehouseid=w.id and vps.partid=ps.partid
left join Branch b on b.id=w.branchid
left join Company c on c.id=w.storagecompanyid 
where w.id in(1263,1261,1262) and nvl(quantity,0)-nvl(Lockedquantity,0)-nvl(CongelationStockQty,0)-nvl(DisabledStock,0)<=vps.safestock  group by ps.partid,vps.stockmaximum,vps.stockminimum,vps.safestock,c.code,b.code,w.id 
) tmp1 inner join
(
select wh.id as tcwarehouseid,ps1.partid,ps1.WarehouseAreaId,ps1.Quantity,vps1.safestock,(case wh.id when 7 then 1263 when 22 then 1261 when 38 then 1262 end) as warehouseid,
(nvl(sum(ps1.quantity),0)-nvl(sum(pls1.Lockedquantity),0)-nvl(sum(CongelationStockQty),0)-nvl(sum(DisabledStock),0)) as kykc
from Warehouse wh 
left join PartsStock ps1 on ps1.warehouseid=wh.id 
left join PartsLockedStock pls1 on pls1.warehouseid=wh.id and pls1.partid=ps1.partid
left join WmsCongelationStockView wcsv1 on wcsv1.WarehouseId=wh.id and wcsv1.SparePartId=ps1.partid
left join VehiclePartsStockLevel vps1 on vps1.warehouseid=wh.id and vps1.partid=ps1.partid
where wh.id in(7,22,38) and (nvl(ps1.quantity,0)-nvl(pls1.Lockedquantity,0)-nvl(CongelationStockQty,0)-nvl(DisabledStock,0))>vps1.safestock group by wh.id,ps1.partid,vps1.safestock,ps1.WarehouseAreaId,ps1.Quantity,(case wh.id when 7 then 1263 when 22 then 1261 when 38 then 1262 end)
)tmp2 on tmp1.warehouseid=tmp2.warehouseid
);
end spCreateTestTable;
/

CREATE OR REPLACE PROCEDURE Prc_InsPartsInPlanToday IS
  FComplateId integer;
  FDateFormat date;
  FDateStr varchar2(8);
  FSerialLength integer;
  FNewCode varchar2(50);
  FcompanyCode varchar(50);
  v_Code varchar2(50);
  c_Code varchar2(50);
  cjh_Code varchar2(50);
  r_Code varchar2(50);
  rjh_Code varchar2(50);
  v_dbdid integer;
  v_ckjhid integer;
  v_ckdid integer;
  v_rkjhid integer;
  v_rkjyid integer;
  v_kcid integer;
  v_kccode varchar2(50);
  v_ckid integer;
  v_scid integer;
  v_sct varchar2(50);
CURSOR TPartsdispatch_CUR IS

select tmp1.warehouseid,tmp1.BCode,tmp1.code,tmp2.tcwarehouseid from (
select w1.id as warehouseid,b.code as BCode,c.code from Warehouse w1
left join Branch b on b.id=w1.branchid
left join Company c on c.id=w1.storagecompanyid
where w1.id in(1263,1261,1262)
)tmp1
inner join (
select w2.id as tcwarehouseid,(case w2.id when 7 then 1263 when 22 then 1261 when 38 then 1262 end) as warehouseid from Warehouse w2 where w2.id in(7,22,38)
) tmp2 on tmp1.warehouseid=tmp2.warehouseid;

TPartsdispatch_RECORD TPartsdispatch_CUR%ROWTYPE;

CURSOR TPPartsOutbound_CUR IS

select * from linshibiao;

TPPartsOutbound_RECORD TPPartsOutbound_CUR%ROWTYPE;

BEGIN
  OPEN TPartsdispatch_CUR;
  LOOP
    FETCH TPartsdispatch_CUR
      INTO TPartsdispatch_RECORD;
    EXIT WHEN TPartsdispatch_CUR%NOTFOUND;
     --生成调拨单编号
        FcompanyCode:=TPartsdispatch_RECORD.BCode;
        FDateFormat:=trunc(sysdate,'DD');
        FDateStr:=to_char(sysdate,'yyyymmdd');
        FSerialLength:=6;
        FNewCode:='PT{CORPCODE}'||FDateStr||'{SERIAL}';
        /*获取编号生成模板Id*/
          begin
            select Id
              into FComplateId
              from codetemplate
             where name = 'PartsTransferOrder'
               and IsActived = 1;
          exception
            when NO_DATA_FOUND then
              raise_application_error(-20001, '未找到名称为"PartsTransferOrder"的有效编码规则。');
            when others then
              raise;
          end;

         select regexp_replace(
             regexp_replace(FNewCode, '{CORPCODE}', FcompanyCode),
              '{SERIAL}',
                lpad(to_char(GetSerial(FComplateId, FDateFormat, TPartsdispatch_RECORD.BCode)),FSerialLength,'0')
              ) Code into v_Code from dual;
--生成出库计划编号
        FcompanyCode:=TPartsdispatch_RECORD.code;
        FDateFormat:=trunc(sysdate,'DD');
        FDateStr:=to_char(sysdate,'yyyymmdd');
        FSerialLength:=6;
        FNewCode:='POP{CORPCODE}'||FDateStr||'{SERIAL}';
        /*获取编号生成模板Id*/
          begin
            select Id
              into FComplateId
              from codetemplate
             where name = 'PartsOutboundPlan'
               and IsActived = 1;
          exception
            when NO_DATA_FOUND then
              raise_application_error(-20001, '未找到名称为"PartsTransferOrder"的有效编码规则。');
            when others then
              raise;
          end;

         select regexp_replace(
             regexp_replace(FNewCode, '{CORPCODE}', FcompanyCode),
              '{SERIAL}',
                lpad(to_char(GetSerial(FComplateId, FDateFormat, TPartsdispatch_RECORD.code)),FSerialLength,'0')
              ) Code into cjh_Code from dual;
--生成出库单编号
        FcompanyCode:=TPartsdispatch_RECORD.code;
        FDateFormat:=trunc(sysdate,'DD');
        FDateStr:=to_char(sysdate,'yyyymmdd');
        FSerialLength:=6;
        FNewCode:='PO{CORPCODE}'||FDateStr||'{SERIAL}';
        /*获取编号生成模板Id*/
          begin
            select Id
              into FComplateId
              from codetemplate
             where name = 'PartsOutboundBill'
               and IsActived = 1;
          exception
            when NO_DATA_FOUND then
              raise_application_error(-20001, '未找到名称为"PartsTransferOrder"的有效编码规则。');
            when others then
              raise;
          end;

         select regexp_replace(
             regexp_replace(FNewCode, '{CORPCODE}', FcompanyCode),
              '{SERIAL}',
                lpad(to_char(GetSerial(FComplateId, FDateFormat, TPartsdispatch_RECORD.code)),FSerialLength,'0')
              ) Code into c_Code from dual;
 --生成入库检验单编号
         FcompanyCode:=TPartsdispatch_RECORD.code;
        FDateFormat:=trunc(sysdate,'DD');
        FDateStr:=to_char(sysdate,'yyyymmdd');
        FSerialLength:=6;
        FNewCode:='PIC{CORPCODE}'||FDateStr||'{SERIAL}';
        /*获取编号生成模板Id*/
          begin
            select Id
              into FComplateId
              from codetemplate
             where name = 'PartsInboundCheckBill'
               and IsActived = 1;
          exception
            when NO_DATA_FOUND then
              raise_application_error(-20001, '未找到名称为"PartsTransferOrder"的有效编码规则。');
            when others then
              raise;
          end;

         select regexp_replace(
             regexp_replace(FNewCode, '{CORPCODE}', FcompanyCode),
              '{SERIAL}',
                lpad(to_char(GetSerial(FComplateId, FDateFormat, TPartsdispatch_RECORD.code)),FSerialLength,'0')
              ) Code into r_Code from dual;
--生成入库计划编号
        FcompanyCode:=TPartsdispatch_RECORD.code;
        FDateFormat:=trunc(sysdate,'DD');
        FDateStr:=to_char(sysdate,'yyyymmdd');
        FSerialLength:=6;
        FNewCode:='PIP{CORPCODE}'||FDateStr||'{SERIAL}';
        /*获取编号生成模板Id*/
          begin
            select Id
              into FComplateId
              from codetemplate
             where name = 'PartsInboundPlan'
               and IsActived = 1;
          exception
            when NO_DATA_FOUND then
              raise_application_error(-20001, '未找到名称为"PartsTransferOrder"的有效编码规则。');
            when others then
              raise;
          end;

         select regexp_replace(
             regexp_replace(FNewCode, '{CORPCODE}', FcompanyCode),
              '{SERIAL}',
                lpad(to_char(GetSerial(FComplateId, FDateFormat, TPartsdispatch_RECORD.code)),FSerialLength,'0')
              ) Code into rjh_Code from dual;

--插入调拨单
select S_PartsTransferOrder.Nextval into v_dbdid from dual;
insert into PartsTransferOrder(Id,Code,Originalwarehouseid,Originalwarehousecode,Originalwarehousename,Storagecompanyid,Storagecompanytype,Destwarehouseid,Destwarehousecode,Destwarehousename,Totalamount,Status,Type)
(select v_dbdid,v_Code,l.tcwarehouseid,w.code,w.name,w.StorageCompanyId,w.storagecompanytype,l.warehouseid,
whs.code,whs.name,(select sum(psp.SalesPrice*linshibiao.transfornum) from linshibiao inner join PartsSalesPrice psp on psp.sparepartid=linshibiao.partid group by linshibiao.tcwarehouseid),1,3 from linshibiao l
 inner join Warehouse w on w.id=l.tcwarehouseid inner join Warehouse whs on whs.id=l.warehouseid inner join PartsSalesPrice psp on psp.sparepartid=l.partid where l.tcwarehouseid is not null and l.tcwarehouseid=TPartsdispatch_RECORD.tcwarehouseid);

--插入调拨单清单
insert into PartsTransferOrderDetail(id,Partstransferorderid,Sparepartid,Sparepartcode,Sparepartname,PlannedAmount,Price)
(select S_PartsTransferOrderDetail.Nextval,v_dbdid,l.partid,sp.code,sp.name,l.transfornum,ps.salesprice from linshibiao l inner join PartsSalesPrice ps on ps.sparepartid=l.partid inner join SparePart sp on sp.id=l.partid);

--插入配件出库计划
select S_PartsOutboundPlan.Nextval into v_ckjhid from dual;
insert into PartsOutboundPlan(id,Code,Warehouseid,Warehousecode,Warehousename,Storagecompanyid,Storagecompanycode,Storagecompanyname,Storagecompanytype,Branchid,Branchcode,Branchname,PartsSalesCategoryId,Counterpartcompanyid,
Counterpartcompanycode,Counterpartcompanyname,Receivingcompanyid,Receivingcompanycode,Receivingcompanyname,Sourceid,Sourcecode,Outboundtype,Originalrequirementbillid,Originalrequirementbilltype,Originalrequirementbillcode,Status,Ifwmsinterface)
(select v_ckjhid,cjh_Code,l.tcwarehouseid,w.code,w.name,w.storagecompanyid,c.code,c.name,w.storagecompanytype,w.branchid,b.code,b.name,pb.PartsSalesCategoryId,cp.id,cp.code,cp.name,whs.storagecompanyid,cp.code,cp.name,
v_dbdid,v_Code,4,v_dbdid,6,v_Code,1,w.wmsinterface from linshibiao l inner join
warehouse w on l.tcwarehouseid=w.id inner join Company c on c.id=w.storagecompanyid inner join Branch b on b.id=w.branchid inner join PartsBranch pb on pb.partid=l.partid and pb.branchid=w.branchid inner join warehouse whs on whs.id =l.warehouseid
inner join Company cp on whs.storagecompanyid=cp.id where l.tcwarehouseid is not null and l.tcwarehouseid=TPartsdispatch_RECORD.tcwarehouseid
);

--插入配件出库计划清单
insert into PartsOutboundPlanDetail(id,Partsoutboundplanid,Sparepartid,Sparepartcode,Sparepartname,PlannedAmount,Price)
(select S_PartsOutboundPlanDetail.Nextval,v_ckjhid,l.partid,sp.code,sp.name,l.transfornum,ps.salesprice from linshibiao l inner join SparePart sp on sp.id=l.partid inner join PartsSalesPrice ps on  ps.sparepartid=l.PartId);

--插入配件出库单
select S_PartsOutboundBill.Nextval into v_ckdid from dual;
insert into PartsOutboundBill(id,Code,Partsoutboundplanid,Warehouseid,Warehousecode,Warehousename,Storagecompanyid,Storagecompanycode,Storagecompanyname,Storagecompanytype,Branchid,Branchcode,Branchname,partssalescategoryid,Counterpartcompanyid,
Counterpartcompanycode,Counterpartcompanyname,Receivingcompanyid,Receivingcompanycode,Receivingcompanyname,Outboundtype,Originalrequirementbillid,Originalrequirementbilltype,Originalrequirementbillcode,SettlementStatus)
(select v_ckdid,c_Code,v_ckjhid,l.tcwarehouseid,w.code,w.name,w.storagecompanyid,c.code,c.name,w.storagecompanytype,w.branchid,b.code,b.name,pb.PartsSalesCategoryId,cp.id,cp.code,cp.name,whs.storagecompanyid,cp.code,
cp.name,4,v_dbdid,6,v_Code,1 from linshibiao l inner join
warehouse w on l.tcwarehouseid=w.id inner join Company c on c.id=w.storagecompanyid inner join Branch b on b.id=w.branchid inner join PartsBranch pb on pb.partid=l.partid and pb.branchid=w.branchid inner join warehouse whs on whs.id =l.warehouseid
inner join Company cp on whs.storagecompanyid=cp.id where l.tcwarehouseid is not null and l.tcwarehouseid=TPartsdispatch_RECORD.tcwarehouseid
);

--插入配件入库计划
select S_PartsInboundPlan.Nextval into v_rkjhid from dual;
insert into PartsInboundPlan(id,Code,Warehouseid,Warehousecode,Warehousename,Storagecompanyid,Storagecompanycode,Storagecompanyname,Storagecompanytype,Branchid,Branchcode,Branchname,partssalescategoryid,Counterpartcompanyid,
Counterpartcompanycode,Counterpartcompanyname,Sourceid,Sourcecode,InboundType,Originalrequirementbillid,Originalrequirementbilltype,Originalrequirementbillcode,Status,Ifwmsinterface)
(select v_rkjhid,rjh_Code,TPartsdispatch_RECORD.tcwarehouseid,w.code,w.name,w.storagecompanyid,c.code,c.name,w.storagecompanytype,w.branchid,b.code,b.name,pb.PartsSalesCategoryId,cp.id,cp.code,cp.name,
v_dbdid,v_Code,4,v_dbdid,6,v_Code,1,w.wmsinterface from linshibiao l inner join
warehouse w on l.tcwarehouseid=w.id inner join Company c on c.id=w.storagecompanyid inner join Branch b on b.id=w.branchid inner join PartsBranch pb on pb.partid=l.partid and pb.branchid=w.branchid inner join warehouse whs on whs.id =l.warehouseid
inner join Company cp on whs.storagecompanyid=cp.id where l.warehouseid is not null and l.warehouseid=TPartsdispatch_RECORD.warehouseid
);

--插入配件入库计划清单
insert into PartsInboundPlanDetail(id,PartsInboundPlanId,Sparepartid,Sparepartcode,Sparepartname,PlannedAmount,Price)
(select S_PartsInboundPlanDetail.Nextval,v_rkjhid,l.PartId,sp.code,sp.name,l.transfornum,ps.salesprice
from linshibiao l inner join SparePart sp on sp.id=l.partid inner join PartsSalesPrice ps on  ps.sparepartid=l.PartId);

--插入配件入库检验单
select S_PartsInboundCheckBill.Nextval into v_rkjyid from dual;
insert into PartsInboundCheckBill(id,Code,PartsInboundPlanId,Warehouseid,Warehousecode,Warehousename,Storagecompanyid,Storagecompanycode,Storagecompanyname,Storagecompanytype,Branchid,Branchcode,Branchname,partssalescategoryid,Counterpartcompanyid,
Counterpartcompanycode,Counterpartcompanyname,InboundType,OriginalRequirementBillId,Originalrequirementbilltype,Originalrequirementbillcode,Status,SettlementStatus)
(select v_rkjyid,r_Code,v_rkjhid,l.tcwarehouseid,w.code,w.name,w.storagecompanyid,c.code,c.name,w.storagecompanytype,w.branchid,b.code,b.name,pb.PartsSalesCategoryId,cp.id,cp.code,cp.name,
4,v_dbdid,6,v_Code,1,1 from linshibiao l inner join
warehouse w on l.tcwarehouseid=w.id inner join Company c on c.id=w.storagecompanyid inner join Branch b on b.id=w.branchid inner join  PartsBranch pb on pb.partid=l.partid and pb.branchid=w.branchid inner join warehouse whs on whs.id =l.warehouseid
inner join Company cp on whs.storagecompanyid=cp.id where l.warehouseid is not null and l.warehouseid=TPartsdispatch_RECORD.warehouseid
);

--插入配件入库检验清单
select id,code into v_kcid,v_kccode from WarehouseArea wha where wha.warehouseid=TPartsdispatch_RECORD.warehouseid and rownum=1;
insert into PartsInboundCheckBillDetail(id,PartsInboundCheckBillId,Sparepartid,Sparepartcode,Sparepartname,WarehouseAreaId,Warehouseareacode,InspectedQuantity,SettlementPrice,CostPrice)
(select S_PartsInboundCheckBillDetail.Nextval,v_rkjyid,l.partid,sp.code,sp.name,v_kcid,v_kccode,l.transfornum,p.settlementprice,pctit.costprice from linshibiao l inner join
PartsPurchaseSettleDetail p on p.sparepartid=l.partid inner join PartsCostTransferInTransit pctit on pctit.sparepartid=l.partid
inner join SparePart sp on sp.id=l.partid);


OPEN TPPartsOutbound_CUR;
LOOP
  FETCH TPPartsOutbound_CUR
    INTO TPPartsOutbound_RECORD;
  EXIT WHEN TPPartsOutbound_CUR%NOTFOUND;

if TPPartsOutbound_RECORD.transfornum>TPPartsOutbound_RECORD.Quantity then
--插入配件出库清单
insert into PartsOutboundBillDetail(id,PartsOutboundBillId,Sparepartid,Sparepartcode,Sparepartname,OutboundAmount,WarehouseAreaId,SettlementPrice,CostPrice)
(select S_PartsOutboundBillDetail.Nextval,v_ckdid,l.partid,sp.code,sp.name,TPPartsOutbound_RECORD.Quantity,TPPartsOutbound_RECORD.Warehouseareaid,p.settlementprice,pctit.costprice from linshibiao l inner join
PartsPurchaseSettleDetail p on p.sparepartid=l.partid inner join PartsCostTransferInTransit pctit on pctit.sparepartid=l.partid inner join SparePart sp on sp.id=l.partid where l.tcwarehouseid=TPartsdispatch_RECORD.tcwarehouseid);


--更新库存
update PartsStock ps set Quantity=nvl(ps.quantity,0)-(TPPartsOutbound_RECORD.Quantity)
where ps.warehouseid=TPartsdispatch_RECORD.tcwarehouseid and
ps.WarehouseId = TPPartsOutbound_RECORD.tcwarehouseid and
ps.partid = TPPartsOutbound_RECORD.partid and
ps.warehouseareaid=TPPartsOutbound_RECORD.Warehouseareaid;


select StorageCompanyId,StorageCompanyType,BranchId into v_scid,v_sct,v_ckid from PartsStock where PartsStock.Warehouseid=TPPartsOutbound_RECORD.Warehouseid;
   MERGE INTO PartsStock p
      USING (select ps.warehouseid,ps.warehouseareaid,ps.partid,TPPartsOutbound_RECORD.Quantity as quantity
             from PartsStock ps
             where ps.warehouseid=TPartsdispatch_RECORD.warehouseid
             and ps.WarehouseId =TPPartsOutbound_RECORD.warehouseid 
             and ps.partid = TPPartsOutbound_RECORD.partid) np
      ON (p.warehouseid = np.warehouseid and p.warehouseareaid=np.warehouseareaid
         and p.PartId = np.partid)
      WHEN MATCHED THEN
        UPDATE
          SET p.quantity = p.Quantity + np.quantity
        WHERE  p.warehouseid = np.warehouseid and p.warehouseareaid=np.warehouseareaid and p.partid = np.partid
      WHEN NOT MATCHED THEN
        INSERT (id,WAREHOUSEID,STORAGECOMPANYID,STORAGECOMPANYTYPE,BRANCHID,WAREHOUSEAREAID,
                PARTID,QUANTITY) values
          ( s_PartsStock.Nextval,np.warehouseid,v_scid,v_sct,v_ckid,np.warehouseareaid,TPPartsOutbound_RECORD.Partid,np.quantity);
          

--更新临时表数据
update linshibiao set transfornum=transfornum-Quantity where linshibiao.tcwarehouseid=TPartsdispatch_RECORD.tcwarehouseid;

else
  --插入配件出库清单
insert into PartsOutboundBillDetail(id,PartsOutboundBillId,Sparepartid,Sparepartcode,Sparepartname,OutboundAmount,WarehouseAreaId,SettlementPrice,CostPrice)
(select S_PartsOutboundBillDetail.Nextval,v_ckdid,l.partid,sp.code,sp.name,TPPartsOutbound_RECORD.transfornum,TPPartsOutbound_RECORD.Warehouseareaid,p.settlementprice,pctit.costprice from linshibiao l inner join
PartsPurchaseSettleDetail p on p.sparepartid=l.partid inner join PartsCostTransferInTransit pctit on pctit.sparepartid=l.partid inner join SparePart sp on sp.id=l.partid where l.tcwarehouseid=TPartsdispatch_RECORD.tcwarehouseid);

--更新库存
update PartsStock ps set Quantity=nvl(ps.quantity,0)-(TPPartsOutbound_RECORD.Quantity)
where ps.warehouseid=TPartsdispatch_RECORD.tcwarehouseid and
ps.WarehouseId = TPPartsOutbound_RECORD.tcwarehouseid and
ps.partid = TPPartsOutbound_RECORD.partid and
ps.warehouseareaid=TPPartsOutbound_RECORD.Warehouseareaid;

select StorageCompanyId,StorageCompanyType,BranchId into v_scid,v_sct,v_ckid from PartsStock where PartsStock.Warehouseid=TPPartsOutbound_RECORD.Warehouseid;
   MERGE INTO PartsStock p
      USING (select ps.warehouseid,ps.warehouseareaid,ps.partid,TPPartsOutbound_RECORD.Quantity as quantity
             from PartsStock ps
             where ps.warehouseid=TPartsdispatch_RECORD.warehouseid
             and ps.WarehouseId =TPPartsOutbound_RECORD.warehouseid 
             and ps.partid = TPPartsOutbound_RECORD.partid) np
      ON (p.warehouseid = np.warehouseid and p.warehouseareaid=np.warehouseareaid
         and p.PartId = np.partid)
      WHEN MATCHED THEN
        UPDATE
          SET p.quantity = p.Quantity + np.quantity
        WHERE  p.warehouseid = np.warehouseid and p.warehouseareaid=np.warehouseareaid and p.partid = np.partid
      WHEN NOT MATCHED THEN
        INSERT (id,WAREHOUSEID,STORAGECOMPANYID,STORAGECOMPANYTYPE,BRANCHID,WAREHOUSEAREAID,
                PARTID,QUANTITY) values
          ( s_PartsStock.Nextval,np.warehouseid,v_scid,v_sct,v_ckid,np.warehouseareaid,TPPartsOutbound_RECORD.Partid,np.quantity);
          

end if;

END LOOP;
CLOSE TPPartsOutbound_CUR;

END LOOP;
CLOSE TPartsdispatch_CUR;

delete linshibiao;

END Prc_InsPartsInPlanToday;
/

create or replace procedure zhixing
is
  
BEGIN
  
spCreateTestTable;

Prc_InsPartsInPlanToday;

END zhixing;
