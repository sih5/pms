-- Create table
create global temporary table DCS.BILLFORCLAIMSETTLEMENT
(
  SOURCEID             NUMBER(9) not null,
  SOURCECODE           VARCHAR2(50) not null,
  SOURCETYPE           NUMBER(9) not null,
  DEALERID             NUMBER(9) not null,
  BRANCHID             NUMBER(9) not null,
  LABORCOST            NUMBER(19,4) not null,
  MATERIALCOST         NUMBER(19,4) not null,
  PARTSMANAGEMENTCOST  NUMBER(19,4) not null,
  FIELDSERVICEEXPENSE  NUMBER(19,4) not null,
  DEBITAMOUNT          NUMBER(19,4) not null,
  COMPLEMENTAMOUNT     NUMBER(19,4) not null,
  OTHERCOST            NUMBER(19,4) not null,
  PARTSSALESCATEGORYID NUMBER(9) not null,
  SERVICEPRODUCTLINEID NUMBER(9),
  PRODUCTLINETYPE      NUMBER(9),
  PRESALEAMOUNT        NUMBER(19,4) not null,
  DEBITTYPE            NUMBER(9),
  ISQB                 NUMBER(9)
)
on commit delete rows;
