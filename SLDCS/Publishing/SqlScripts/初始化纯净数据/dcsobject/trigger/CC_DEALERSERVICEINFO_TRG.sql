
CREATE OR REPLACE TRIGGER CC_DEALERSERVICEINFO_TRG
AFTER INSERT OR UPDATE ON DEALERSERVICEINFO
FOR EACH ROW
BEGIN
  INSERT INTO CC_DEALERSERVICEINFO_SYNC VALUES(S_CC_DEALERSERVICEINFO.NEXTVAL,:NEW.ID,'DEALERSERVICEINFO');
END;