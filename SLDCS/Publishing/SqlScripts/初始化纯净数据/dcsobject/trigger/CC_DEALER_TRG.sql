

CREATE OR REPLACE TRIGGER CC_DEALER_TRG
AFTER INSERT OR UPDATE ON DEALER
FOR EACH ROW
BEGIN
  INSERT INTO CC_DEALER_SYNC VALUES(S_CC_DEALER.NEXTVAL,:NEW.ID,'DEALER');
END;
