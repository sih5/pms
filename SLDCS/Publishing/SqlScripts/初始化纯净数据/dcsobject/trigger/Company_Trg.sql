


-- Dcs企业同步到Security
create or replace trigger Company_Trg
   after insert or update on Company
   for each row
begin
  if :old.id is null then
    AddEnterprise(:new.Id, :new.Code, :new.Name, :new.Type, :new.CreatorId, :new.CreatorName, :new.CreateTime);
  elsif :new.Status = 99 then
    AbandonEnterprise(:new.Id, :new.ModifierId, :new.ModifierName, :new.ModifyTime);
  else
    EditEnterprise(:new.Id, :new.Name, :new.Type, :new.ModifierId, :new.ModifierName, :new.ModifyTime);
  end if;
end Company_Trg;
/
