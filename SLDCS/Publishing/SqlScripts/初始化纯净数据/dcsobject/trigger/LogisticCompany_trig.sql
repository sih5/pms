
create or replace trigger LogisticCompany_trig
  after insert or update ON LogisticCompany
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    insert into LogisticCompany_sync
      (billid, syncnum)
    values
      (:new.Id,LogisticCompany_increase.nextval);
  END;
END;