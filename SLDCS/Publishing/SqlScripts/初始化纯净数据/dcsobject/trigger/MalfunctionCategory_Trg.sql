
create or replace trigger MalfunctionCategory_Trg
   after insert or update on MalfunctionCategory
   for each row
     declare N_layernodetypeid number(9);
begin
  select id into N_layernodetypeid from layernodetype where layerstructureid=1 and code = (select code from layernodetype where id=:new.layernodetypeid);
  if :old.id is null then
    begin
     insert into  RepairItemCategory (
ID,  CODE,  NAME , LAYERNODETYPEID , PARENTID,  ROOTGROUPID  ,STATUS  ,REMARK  ,CREATORID,	CREATORNAME,	CREATETIME) values
     (
     :new.id,
     :new.code,
     :new.name,
     N_layernodetypeid,
     :new.parentid,
     :new.rootgroupid,
     :new.status,
     :new.remark,
     :new.creatorid,
     :new.creatorname,
     :new.createtime
     );
    end;
  else
    begin
      update RepairItemCategory set
      ParentId=:new.ParentId,
      RootGroupId=:new.RootGroupId,
      Code=:new.Code,
      Name=:new.Name,
      LayerNodeTypeId=N_layernodetypeid,
      Status=:new.Status,
      Remark=:new.Remark,
      ModifierId=:new.ModifierId,
      ModifierName=:new.ModifierName,
      ModifyTime=:new.modifytime where id=:new.id ;
    end;
    end if;
    end;
/

