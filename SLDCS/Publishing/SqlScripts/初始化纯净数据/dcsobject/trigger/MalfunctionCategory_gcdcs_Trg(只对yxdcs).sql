create or replace trigger MalfunctionCategory_gcdcs_Trg
   after insert on MalfunctionCategory
   for each row

begin
  
     insert into  gcdcs.MalfunctionCategory (
ID,  CODE,  NAME , LAYERNODETYPEID , PARENTID,  ROOTGROUPID  ,STATUS  ,REMARK  ,CREATORID,  CREATORNAME,  CREATETIME) values
     (
     :new.id,
     :new.code,
     :new.name,
     :new.LAYERNODETYPEID,
     :new.parentid,
     :new.rootgroupid,
     :new.status,
     :new.remark,
     :new.creatorid,
     :new.creatorname,
     :new.createtime
     );
   
 
    end;
