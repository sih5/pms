

CREATE OR REPLACE TRIGGER OCC_COMPANY_TRG
  AFTER INSERT OR UPDATE ON COMPANY
  FOR EACH ROW
DECLARE
  FLAG NUMBER;
BEGIN
  IF (:NEW.ID = 3007) THEN

    --责任单位(责任单位信息)
    merge into OCC_RESPONSIBLEUNIT_SYNC bys
    using (select '1' from dual) N
    on (to_number(bys.BILLID) = :new.ID)
    WHEN MATCHED THEN
      UPDATE set syncnum = S_OCC_RESPONSIBLEUNIT.nextval
    WHEN NOT MATCHED THEN
      INSERT
        (SYNCNUM, BILLID)
      VALUES
        (S_OCC_RESPONSIBLEUNIT.NEXTVAL, :NEW.ID);

  END IF;
  IF (:NEW.TYPE = 6) THEN
    SELECT COUNT(*)
      INTO FLAG
      FROM BRANCHSUPPLIERRELATION
     WHERE BRANCHID = 3007
       AND SUPPLIERID = :NEW.ID;
    IF (FLAG >= 1) THEN

      --责任单位(责任单位信息)
      merge into OCC_RESPONSIBLEUNIT_SYNC bys
      using (select '1' from dual) N
      on (to_number(bys.BILLID) = :new.ID)
      WHEN MATCHED THEN
        UPDATE set syncnum = S_OCC_RESPONSIBLEUNIT.nextval
      WHEN NOT MATCHED THEN
        INSERT
          (SYNCNUM, BILLID)
        VALUES
          (S_OCC_RESPONSIBLEUNIT.NEXTVAL, :NEW.ID);
    END IF;
    SELECT COUNT(*)
      INTO FLAG
      FROM BRANCHSUPPLIERRELATION
     WHERE BRANCHID = 3007
       AND SUPPLIERID = :NEW.ID;
    IF (FLAG >= 1) THEN

      --配件供应商(供应商信息)
      merge into OCC_PARTSSUPPLIER_SYNC bys
      using (select '1' from dual) N
      on (to_number(bys.BILLID) = :new.ID)
      WHEN MATCHED THEN
        UPDATE set syncnum = S_OCC_PARTSSUPPLIER.nextval
      WHEN NOT MATCHED THEN
        INSERT
          (SYNCNUM, BILLID)
        VALUES
          (S_OCC_PARTSSUPPLIER.NEXTVAL, :NEW.ID);
    END IF;
  END IF;
  IF (:NEW.TYPE = 5) THEN
    SELECT COUNT(*)
      INTO FLAG
      FROM RESPONSIBLEUNITBRANCH
     WHERE BRANCHID = 3007
       AND RESPONSIBLEUNITID = :NEW.ID;
    IF (FLAG >= 1) THEN
      --责任单位(责任单位信息)
      merge into OCC_RESPONSIBLEUNIT_SYNC bys
      using (select '1' from dual) N
      on (to_number(bys.BILLID) = :new.ID)
      WHEN MATCHED THEN
        UPDATE set syncnum = S_OCC_RESPONSIBLEUNIT.nextval
      WHEN NOT MATCHED THEN
        INSERT
          (SYNCNUM, BILLID)
        VALUES
          (S_OCC_RESPONSIBLEUNIT.NEXTVAL, :NEW.ID);
    END IF;
  END IF;
  IF (:NEW.TYPE = 2) THEN
    SELECT COUNT(*)
      INTO FLAG
      FROM DEALERSERVICEINFO
     WHERE BRANCHID = 3007
       AND DEALERID = :NEW.ID;
    IF (FLAG >= 1) THEN
      --客户（服务站信息）
      merge into OCC_DEALER_SYNC bys
      using (select '1' from dual) N
      on (to_number(bys.BILLID) = :new.ID)
      WHEN MATCHED THEN
        UPDATE set syncnum = S_OCC_DEALER.nextval
      WHEN NOT MATCHED THEN
        INSERT (SYNCNUM, BILLID) VALUES (S_OCC_DEALER.NEXTVAL, :NEW.ID);
    END IF;
  END IF;
END;