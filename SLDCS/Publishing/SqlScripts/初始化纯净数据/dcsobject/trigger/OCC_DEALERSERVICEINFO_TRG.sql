
-----------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE TRIGGER OCC_DEALERSERVICEINFO_TRG
  AFTER INSERT OR UPDATE ON DEALERSERVICEINFO
  FOR EACH ROW
BEGIN
  IF (:NEW.BRANCHID = 3007) THEN
    --保存的是主单dealer表的ID
    merge into OCC_DEALERSERVICEINFO_SYNC bys
    using (select '1' from dual) N
    on (to_number(bys.BILLID) = :new.ID)
    WHEN MATCHED THEN
      UPDATE set syncnum = S_OCC_DEALERSERVICEINFO.nextval
    WHEN NOT MATCHED THEN
      INSERT
        (SYNCNUM, BILLID)
      VALUES
        (S_OCC_DEALERSERVICEINFO.NEXTVAL, :NEW.ID);
  END IF;
END;