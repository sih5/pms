
CREATE OR REPLACE TRIGGER OCC_REPAIRCLAIMBILL_TRG
  AFTER INSERT OR UPDATE ON REPAIRCLAIMBILL
  FOR EACH ROW
DECLARE
  FLAGCODE VARCHAR2(50);
BEGIN
  IF (:NEW.BRANCHID = 3007) AND (:NEW.STATUS = 1 OR :NEW.STATUS = 99) THEN
    SELECT NVL(SYSCODE,'NEWPMS')
      INTO FLAGCODE
      FROM REPAIRORDER
     WHERE ID = :NEW.REPAIRORDERID;
    IF ((:NEW.STATUS = 1 AND FLAGCODE <> 'FTCC' AND FLAGCODE IS NOT NULL) OR
       (:NEW.STATUS = 99)) THEN

      merge into OCC_REPAIRCLAIMBILL_SYNC bys
      using (select '1' from dual) N
      on (to_number(bys.BILLID) = :new.ID)
      WHEN MATCHED THEN
        UPDATE set syncnum = S_OCC_REPAIRCLAIMBILL.nextval
      WHEN NOT MATCHED THEN
        INSERT
          (SYNCNUM, BILLID)
        VALUES
          (S_OCC_REPAIRCLAIMBILL.NEXTVAL, :NEW.ID);

    END IF;
  END IF;
END;
