
CREATE OR REPLACE TRIGGER PARTSOUTBOUNDBILL_TRG
AFTER INSERT ON PARTSOUTBOUNDBILL
FOR EACH ROW
BEGIN
  --雷萨泵送
  IF(:NEW.OUTBOUNDTYPE=1 AND :NEW.STORAGECOMPANYID=2) THEN
     INSERT INTO SAP_PARTSOUTBOUND_SYNC VALUES(S_SAP_PARTSOUTBOUND_SYNC.NEXTVAL,:NEW.ID,'PARTSOUTBOUNDBILL');
  END IF;
  IF(:NEW.OUTBOUNDTYPE=2 AND :NEW.STORAGECOMPANYID=2) THEN
     INSERT INTO SAP_PURCHASERTNOUTBOUND_SYNC VALUES(S_SAP_PURCHASERTNOUTBOUND_SYNC.NEXTVAL,:NEW.ID,'PARTSOUTBOUNDBILL');
  END IF;
  --营销
  IF(:NEW.OUTBOUNDTYPE=1 AND :NEW.STORAGECOMPANYID=3007) THEN--SAP_10(销售出库)
     INSERT INTO SAP_10_SYNC VALUES(S_SAP_10_SYNC.NEXTVAL,:NEW.ID,'PARTSOUTBOUNDBILL');
  END IF;
  IF(:NEW.OUTBOUNDTYPE=2 AND :NEW.STORAGECOMPANYID=3007) THEN--SAP_5(采购退货出库)
     INSERT INTO SAP_5_SYNC VALUES(S_SAP_5_SYNC.NEXTVAL,:NEW.ID,'PARTSOUTBOUNDBILL');
  END IF;
END PARTSOUTBOUNDBILL_TRG;
