
create or replace trigger PartsInboundPlan_trig
  after INSERT ON PartsInboundPlan
  FOR EACH ROW
DECLARE
  warehouse varchar2(100);
BEGIN
  Begin

  select t.name
   into warehouse
   from dcs.warehouse t
   inner join dcs.WMS2PMSWarehouse
    on t.Name = WMS2PMSWarehouse.PMSWAREHOUSENAME
 inner join dcs.PartsSalesCategory psc
    on :new.PartsSalesCategoryId=psc.Id
 inner join SalesCategoryName2Company scn2c
    on psc.Name=scn2c.PMSPartsSalesCategoryName
where t.id=:new.Warehouseid and t.WmsInterface = 1;
exception
          when No_Data_Found then
            warehouse := null;
  end;
   
   begin
     
if warehouse is not null then
    insert into PartsInboundPlan_sync
      (billid, syncnum)
    values
      (:new.Id,PartsInboundPlan_increase.nextval);
end if;
  END;
END;