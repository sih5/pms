
create or replace trigger PartsPurchaseOrder_trig
  after insert ON PartsPurchaseOrder
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    if(:new.status=2) then
    insert into PartsPurchaseOrder_sync
      (billid, syncnum)
    values
      (:new.Id,PartsPurchaseOrder_increase.nextval);
    end if;
  END;
END;
