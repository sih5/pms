

create or replace trigger PartsSalesOrder_trig
  after INSERT ON PartsSalesOrder
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
  if(:new.status=2) then
    insert into PartsSalesOrder_sync
      (billid, syncnum)
    values
      (:new.Id,PartsSalesOrder_increase.nextval);
   end if;
  END;
END;

