
create or replace trigger PartsShippingOrder_trig
  after update ON PartsShippingOrder
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  BEGIN
    if :new.Status =3 then
    insert into PartsShippingOrder_sync
      (billid, syncnum)
    values
      (:new.Id,PartsShippingOrder_increase.nextval);
    end if ;
  END;
END;