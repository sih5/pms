
create or replace trigger SparePart_trig
  after insert or update ON SparePart
  FOR EACH ROW
DECLARE
  WMSCompany varchar2(100);
BEGIN
    
  BEGIN
    
    select scn2c.WMSCompany
      into WMSCompany
      from PartsBranch pb
     inner join PartsSalesCategory psc
        on pb.PartsSalesCategoryId = psc.Id
      left join SalesCategoryName2Company scn2c
        on psc.Name = scn2c.PMSPartsSalesCategoryName
     where :new.Id = pb.PartId;
      exception
          when No_Data_Found then
            WMSCompany := null;
   end;
   
   begin
    if WMSCompany is not null then
      insert into SparePart_sync
        (billid, syncnum)
      values
        (:new.Id, SparePart_increase.nextval);
    end if;
  END;
END;