create or replace trigger repairitem_gcdcs_Trg
   after insert /*or update */on repairitem
   for each row

begin

 /* merge into gcdcs.repairitem a
  using (select 1 from dual)
  on (a.code = :new.code)
  when matched then
    update
       set a.name                     = :new.name,
           a.repairitemcategoryid     = :new.repairitemcategoryid,
           a.quickcode                = :new.quickcode,
           a.jobtype                  = :new.jobtype,
           a.repairqualificationgrade = :new.repairqualificationgrade,
           a.status                   = :new.status,
           a.remark                   = :new.remark,
           a.ModifierId               = :new.ModifierId,
           a.ModifierName             = :new.ModifierName,
           a.ModifyTime               = :new.ModifyTime
    --where code = :new.code
  when not matched then*/
    
    insert into gcdcs.repairitem
      (id,
       code,
       name,
       repairitemcategoryid,
       quickcode,
       jobtype,
       repairqualificationgrade,
       status,
       remark,
       creatorid,
       creatorname,
       createtime)
    values
      (gcdcs.s_repairitem.nextval,
       :new.code,
       :new.name,
       :new.repairitemcategoryid,
       :new.quickcode,
       :new.jobtype,
       :new.repairqualificationgrade,
       :new.status,
       :new.id,
       :new.creatorid,
       :new.creatorname,
       :new.createtime);
end;
