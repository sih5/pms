create or replace view agencystockqryfrmorder as
select PartsSalesOrderId,PartsSalesOrderCode,SparePartCode,SparePartName,AgencyCode,AgencyName, OrderedQuantity,Usablequantity,
       case when Usablequantity>OrderedQuantity then OrderedQuantity else Usablequantity end as Fulfilquantity,
       case when Usablequantity>OrderedQuantity then 0 else OrderedQuantity-Usablequantity end as NoFulfilquantity
  from (
  select ps.id as PartsSalesOrderId,ps.code as PartsSalesOrderCode,psd.sparepartid,psd.SparePartCode,psd.SparePartName,ar.AgencyCode,ar.AgencyName,
  nvl(psd.OrderedQuantity,0) as OrderedQuantity,sum(nvl(s.quantity,0)-nvl(pls.lockedquantity,0)-nvl(wv.CongelationStockQty,0)-nvl(wv.DisabledStock,0)) as Usablequantity
  from PartsSalesOrder ps
  inner join PartsSalesOrderDetail psd on ps.id = psd.partssalesorderid
  inner join AgencyDealerRelation ar on ps.SubmitCompanyId = ar.DealerId and ps.salescategoryid =ar.partssalesordertypeid
  left join PartsStock s on psd.sparepartid = s.partid
  --and ps.WarehouseId = s.warehouseid
  and s.branchid = ps.branchid And s.storagecompanyid=ar.agencyid
  left join WarehouseAreaCategory kw on kw.id =s.WarehouseAreaCategoryId and  kw.category in (1,3)
  left join PartsLockedStock pls on pls.WarehouseId = s.warehouseid and s.partid =pls.partid And pls.Storagecompanyid=ar.agencyid
  left join WmsCongelationStockView wv on wv.WarehouseId = s.warehouseid and wv.SparePartId = s.partid And wv.StorageCompanyId=ar.agencyid
  where ps.status =2
  group by ps.id,ps.code,psd.sparepartid,psd.SparePartCode,psd.SparePartName,ar.AgencyCode,ar.AgencyName,psd.orderedquantity
  ) tmp
  where 1=1
;
