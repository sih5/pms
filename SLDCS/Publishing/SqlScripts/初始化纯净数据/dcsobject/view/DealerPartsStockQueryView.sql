Create Or Replace View DealerPartsStockQueryView As 
Select t.BranchId,t.BranchCode,t.PartsSalesCategoryId,t.PartsSalesCategoryName,t.DealerCode,t.DealerName,t.PartCode,t.PartName,
 t.Quantity,t.CustomerType,t.ProvinceName,t.SalesPrice,t.SalesPriceAmount,t.StockMaximum,t.PartsAttribution,Cast (Decode(t.Quantity, 0, Null, Null, Null, 1)AS NUMBER(1)) As IsStoreMoreThanZero
  From (Select a.BranchId,
               Br.Code As BranchCode,
               a.Salescategoryid As PartsSalesCategoryId,
               a.Salescategoryname As PartsSalesCategoryName,
               a.DealerCode,-- 服务站编码,
               a.DealerName,-- 服务站名称,
               b.Code As PartCode,--配件图号
               b.Name As PartName,--配件名称
               Sum(a.Quantity) As Quantity,--库存
               Nvl(c.Salesprice, 0) As SalesPrice,--批发价
               d.Provincename As ProvinceName,--省份
               Case
                 When Dsi.Channelcapabilityid = 3 Then
                  '专卖店'
                 Else
                  '服务站'
               End As CustomerType,--客户类型
               Sum(a.Quantity * Nvl(c.Salesprice, 0)) As SalesPriceAmount,--批发价金额
               Pb.StockMaximum,--库存高限
               Pb.Partsattribution--配件所属分类
                 From
                 Dealerpartsstock a
                 Inner Join Branch Br
                    On a.Branchid = Br.Id
                 Inner Join Sparepart b
                    On a.Sparepartid = b.Id
                  Left Join Partssalesprice c
                    On c.Partssalescategoryid = a.Salescategoryid
                   And c.Sparepartid = a.Sparepartid
                   And c.Status = 1
                  Left Join Company d
                    On a.Dealercode = d.Code
                  Left Join Dealer e
                    On e.Code = d.Code
                  Left Join Dealerserviceinfo Dsi
                    On e.Id = Dsi.Dealerid
                   And a.Salescategoryid = Dsi.Partssalescategoryid
                  Left Join Partsbranch Pb
                    On a.Salescategoryid = Pb.Partssalescategoryid
                   And b.Code = Pb.Partcode
                 Where a.Subdealerid = -1
                   And d.Type In (2, 7, 10)
                 Group By a.Branchid,
                          d.Provincename,
                          a.Salescategoryid,
                          a.Salescategoryname,
                          a.Dealercode,
                          a.Dealername,
                          b.Code,
                          b.Name,
                          c.Salesprice,
                          Dsi.Channelcapabilityid,
                          Br.Code,
                          Pb.Stockmaximum,
                          Pb.Partsattribution,
                          Dsi.Channelcapabilityid)t
