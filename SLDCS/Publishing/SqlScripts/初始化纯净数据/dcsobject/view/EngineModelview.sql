create or replace view EngineModelview as
select 
a.BranchId,
a.EngineModel,
a.status,
b.PartsSalesCategoryId as PartsSalesCategoryId,
a.Id as EngineModeId,
b.EngineProductLineId as EngineProductLineId,
decode (b.EngineProductLineId,null,0, 1) as IsSetRelation
from EngineModel a 
left join EngineModelProductLine b on b.EngineModeId=a.id
