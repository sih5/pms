create view PartsBranchView as
select a.code as PartCode,b.partssalescategoryname as PartsSalesCategoryName  from dcs.sparepart a inner join dcs.partsbranch  b on a.id=b.partid
where a.status=1 and b.status=1
union
select a.code as PartCode,b.partssalescategoryname as PartsSalesCategoryName  from yxdcs.sparepart a inner join yxdcs.partsbranch  b on a.id=b.partid
where a.status=1 and b.status=1
union
select a.code as PartCode,b.partssalescategoryname as PartsSalesCategoryName  from sddcs.sparepart a inner join sddcs.partsbranch  b on a.id=b.partid
where a.status=1 and b.status=1
union
select a.code as PartCode,b.partssalescategoryname as PartsSalesCategoryName  from gcdcs.sparepart a inner join gcdcs.partsbranch  b on a.id=b.partid
where a.status=1 and b.status=1;