CREATE OR REPLACE VIEW SAPINVOICEINFO_FD AS
SELECT  id,
        BUKRS         as CompanyCode,
        2             as BusinessType,
        code,
        InvoiceNumber,
        InvoiceAmount,
        a.invoicetax,
        CreateTime,
        Status,
        Message
   from Sap_FD_SalesBillInfo a
 union
 SELECT
        id,
        BUKRS         as CompanyCode,
        3             as BusinessType,
        b.jsdnum,
        InvoiceNumber,
        InvoiceAmount,
        b.invoicetax,
        CreateTime,
        Status,
        Message
   from Sap_FD_PurchaseRtnInvoice b
    union
 SELECT
        c.id,
        c.BUKRS         as CompanyCode,
        1             as BusinessType,
        c.jsdnum,
        (select  InvoiceNumber from InvoiceInformation where InvoiceInformation.id=e.invoiceid) InvoiceNumber,
         (select  InvoiceAmount from InvoiceInformation where InvoiceInformation.id=e.invoiceid)  InvoiceAmount,
        (select  invoicetax from InvoiceInformation where InvoiceInformation.id=e.invoiceid) invoicetax,
        c.CreateTime,
        c.Status,
        c.Message
   from Sap_FD_InvoiceVerAccountInfo c
   inner join PartsPurchaseSettleBill d on d.code=c.jsdnum
   inner join PurchaseSettleInvoiceRel e on e.partspurchasesettlebillid=d.id;
