

-- 销售车型以及零售指导价
create or replace view VehModelRetailSuggestedPrice as
select distinct 
       ProductCategory.Id as ProductCategoryId,
       ProductCategory.Code as ProductCategoryCode,
       ProductCategory.Name as ProductCategoryName,  
       VehicleRetailSuggestedPrice.Price
  from VehicleRetailSuggestedPrice
 inner join Product on Product.Id = VehicleRetailSuggestedPrice.ProductId
 inner join ProductAffiProductCategory on ProductAffiProductCategory.ProductId = Product.Id
 inner join ProductCategory on ProductCategory.Id = ProductAffiProductCategory.ProductCategoryId
 where Product.Status = 1 
   and ProductCategory.Status = 1 
   and VehicleRetailSuggestedPrice.Status = 2
/
