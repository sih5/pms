
--����ԭ����ͼ��
create or replace view malfunctionview as
select Malfunction.id               as MalfunctionId,
       Malfunction.Code             as Code,
       Malfunction.Description      as Description,
       MalfunctionCategory.id       as MalfunctionCategoryId,
       MalfunctionCategory.code     as MalfunctionCategoryCode,
       MalfunctionCategory.Name     as MalfunctionCategoryName,
       MalfunctionCategory.parentid as ParentId,
       Parent_MalfunctionCategory.Code as ParentCode,
       Parent_MalfunctionCategory.Name as ParentName,
       GParent_MalfunctionCategory.code as GrandParentCode,
       GParent_MalfunctionCategory.Name as GrandParentName,
       MalfunctionBrandRelation.Id as MalfunctionBrandRelationId,
       MalfunctionBrandRelation.Partssalescategoryid,
       Partssalescategory.Name     as PartssalescategoryName
  from MalfunctionCategory
 inner join MalfunctionCategory Parent_MalfunctionCategory
  on MalfunctionCategory.Parentid =Parent_MalfunctionCategory.Id
 inner join MalfunctionCategory GParent_MalfunctionCategory
  on Parent_MalfunctionCategory.Parentid=GParent_MalfunctionCategory.id
 inner join Malfunction
    on Malfunction.MalfunctionCategoryId = MalfunctionCategory.Id
   and Malfunction.status = 1
 inner join MalfunctionBrandRelation
    on MalfunctionBrandRelation.Malfunctionid = Malfunction.id
   and MalfunctionBrandRelation.Status = 1
 inner join Partssalescategory
 on Partssalescategory.Id=MalfunctionBrandRelation.Partssalescategoryid
 where MalfunctionCategory.status = 1
   and not exists
 (select *
          from LayerNodeType
         where MalfunctionCategory.Layernodetypeid = LayerNodeType.Id
           and LayerNodeType.Toplevelnode = 1);
/