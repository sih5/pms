
create or replace view productcategoryview as
select distinct t.productcategorycode,nvl(t.productcategoryname,' ') productcategoryname from product t;