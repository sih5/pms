

create or replace view qtsdataqueryfordgn as
select a.Cscode,
         a.Zcbmcode,
         a.Vincode,
         b.Assembly_date,
         a.Bind_date,
         b.Factory,
         b.Mapid,
         b.Patch,
         b.Produceline,
         b.Mapname
    from qts.o_vinbind @orcl_16 a
    left join qts.o_tmepassembly_cs @orcl_16 b on a.cscode = b.cscode;
