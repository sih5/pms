create or replace view qtsdataqueryforsd as
select detailid,
       productline as produceline,
       vincode,
       zcbmcode,
       assembly_date,
       mapid,
       mapname,
       patch,
       factory,
      chuchangid
  from wms.vw_pms @QTS_115;
