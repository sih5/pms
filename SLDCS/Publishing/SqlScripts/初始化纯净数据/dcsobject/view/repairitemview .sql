
--ά����Ŀ����ͼ��
create or replace view repairitemview as
select repairitem.id               as RepairItemId,
       RepairItem.Code             as Code,
       RepairItem.Name             as Name,
       RepairItemCategory.id       as RepairItemCategoryId,
       RepairItemCategory.code     as RepairItemCategoryCode,
       RepairItemCategory.Name     as RepairItemCategoryName,
       RepairItemCategory.parentid as ParentId,
       Parent_RepairItemCategory.Code as ParentCode,
        GParent_RepairItemCategory.Code as GrandParentCode,
       RepairItemBrandRelation.Id as MalfunctionBrandRelationId,
       Parent_RepairItemCategory.Name as ParentName,
       GParent_RepairItemCategory.Name as GrandParentName,       
       RepairItemBrandRelation.Partssalescategoryid,
       Partssalescategory.Name     as PartssalescategoryName
  from RepairItemCategory
 inner join RepairItemCategory Parent_RepairItemCategory
  on RepairItemCategory.Parentid =Parent_RepairItemCategory.Id
   inner join RepairItemCategory GParent_RepairItemCategory
  on Parent_RepairItemCategory.Parentid=GParent_RepairItemCategory.id
 inner join repairitem
    on repairitem.repairitemcategoryid = RepairItemCategory.Id
   and repairitem.status = 1
 inner join RepairItemBrandRelation
    on RepairItemBrandRelation.Repairitemid = repairitem.id
   and RepairItemBrandRelation.Status = 1
 inner join Partssalescategory
 on Partssalescategory.Id=RepairItemBrandRelation.Partssalescategoryid
 where RepairItemCategory.status = 1
   and not exists
 (select *
          from LayerNodeType
         where RepairItemCategory.Layernodetypeid = LayerNodeType.Id
           and LayerNodeType.Toplevelnode = 1);

/