

--服务产品线（视图）
create or replace view serviceproductlineview as
select ServiceProductLine.Id                   as ProductLineId,
       1                                       as ProductLineType,
       ServiceProductLine.Code as ProductLineCode,
       ServiceProductLine.Name as ProductLineName,
       ServiceProductLine.Partssalescategoryid,
       ServiceProductLine.Branchid
  from ServiceProductLine
 where ServiceProductLine.Status = 1
union all
select EngineProductLine.Id                   as ProductLineId,
       2 as ProductLineType,
       EngineProductLine.Enginecode as ProductLineCode,
       EngineProductLine.Enginename as ProductLineName,
       EngineProductLine.Partssalescategoryid,
       EngineProductLine.Branchid
  from EngineProductLine
 where EngineProductLine.Status=1;
/