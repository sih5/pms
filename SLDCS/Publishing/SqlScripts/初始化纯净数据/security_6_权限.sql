/* 脚本执行注意事项：
1.以下DCS、SECURITY，请调整为实际使用的数据库方案名
2.必须在对应数据库对象（表、序列）创建后执行
3.对象重新创建后，应重新执行此脚本
4.以下脚本可重复执行
*/


-- Dcs 数据库，增加针对Security数据库对象的权限
-- 使用 Security 账号登陆执行
GRANT SELECT ON xxsecurity.ORGANIZATION TO xxdcs;
GRANT SELECT ON xxsecurity.ORGANIZATIONPERSONNEL TO xxdcs;
GRANT SELECT ON xxsecurity.ROLE TO xxdcs;
GRANT SELECT ON xxsecurity.ROLEPERSONNEL TO xxdcs;
GRANT SELECT ON xxsecurity.EntOrganizationTpl TO xxdcs;
GRANT SELECT ON xxsecurity.EntOrganizationTplDetail TO xxdcs;
GRANT SELECT ON xxsecurity.RoleTemplate TO xxdcs;
GRANT EXECUTE ON xxsecurity.AddEnterprise TO xxdcs;
GRANT EXECUTE ON xxsecurity.EditEnterprise TO xxdcs;
GRANT EXECUTE ON xxsecurity.AbandonEnterprise TO xxdcs;

-- Security 数据库，增加针对Dcs数据库对象的权限
-- 使用 Dcs 账号登陆执行
GRANT INSERT ON xxdcs.PERSONNEL TO xxsecurity;
GRANT SELECT ON xxdcs.PERSONNEL TO xxsecurity;
GRANT UPDATE ON xxdcs.PERSONNEL TO xxsecurity;
