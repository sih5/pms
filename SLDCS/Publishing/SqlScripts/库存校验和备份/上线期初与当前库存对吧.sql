时代上线期初库存：sddcs.PartsHistoryStock
工程车期初上线库存：tmpdata.gccpartsstockinitial20141103
营销期初上线库存：tmpdata.bak_partsstock20140709
福戴期初上线备份： create table tmpdata.PartsStockOM20150128_BAK  as select * from partsstock;
                         create table tmpdata.PartsPlannedPrice20150128_BAK   as select * from partsplannedprice

--校验时用以上表替换

select *
  from (SELECT wid,
               partId,
               SUM(wmsin) wmsin,
               SUM(wmsout) wmsout,
               SUM(bg) bg,
               SUM(wt) wt,
               -- (SUM(qcbg)+SUM(wmsin)-SUM
               (wmsout) + SUM(pd)) q,
       SUM(jy) jy,
       SUM(qcbg) qcbg,
       SUM(qcjy) qcjy,
       SUM(pd) pd
  FROM ((SELECT picb.WAREHOUSEID AS wid,
                picbd.SPAREPARTID AS partId,
                SUM(NVL(picbd.INSPECTEDQUANTITY, 0)) AS wmsin,
                0 AS wmsout,
                0 AS bg,
                0 AS wt,
                0 AS jy,
                0 AS qcbg,
                0 AS qcjy,
                0 AS pd
           FROM PartsInboundCheckBill picb
          INNER JOIN PartsInboundCheckBillDetail picbd
             ON picb.ID = picbd.PARTSINBOUNDCHECKBILLID
          INNER JOIN warehouse w
             ON picb.warehouseid = w.id
          WHERE w.StorageCompanyType = 1 --w.id=9----w.WMSINTERFACE =1
          GROUP BY picb.WAREHOUSEID, picbd.SPAREPARTID, 0) UNION ALL
        (SELECT pob.WAREHOUSEID AS wid,
                pobd.SPAREPARTID AS partId,
                0 AS wmsin,
                SUM(NVL(pobd.OUTBOUNDAMOUNT, 0)) AS wmsout,
                0 AS bg,
                0 AS wt,
                0 AS jy,
                0 AS qcbg,
                0 AS qcjy,
                0 AS pd
           FROM PartsOutboundBill pob
          INNER JOIN PartsOutboundBillDetail pobd
             ON pob.ID = pobd.PARTSOUTBOUNDBILLID
          INNER JOIN warehouse w
             ON pob.warehouseid = w.id
          WHERE w.StorageCompanyType = 1 --w.id=9----w.WMSINTERFACE =1
          GROUP BY pob.WAREHOUSEID, pobd.SPAREPARTID, 0) UNION ALL
        (SELECT w.ID AS wid,
                ps.partid AS partId,
                0 AS wmsin,
                0 AS wmsout,
                SUM(NVL(ps.QUANTITY, 0)) AS bg,
                0 AS wt,
                0 AS jy,
                0 AS qcbg,
                0 AS qcjy,
                0 AS pd
           FROM partsstock ps
          INNER JOIN warehouse w
             ON ps.WAREHOUSEID = w.ID
          INNER JOIN warehousearea wa
             ON ps.warehouseareaid = wa.id
          INNER JOIN WarehouseAreaCategory wac
             ON wa.AreaCategoryId = wac.ID
          WHERE w.StorageCompanyType = 1 --w.id=9 ----w.WMSINTERFACE =1
            AND wac.CATEGORY = 1
          GROUP BY w.ID, ps.partid, 0)
       
        UNION ALL (SELECT w.ID AS wid,
                          ps.partid AS partid,
                          0 AS wmsin,
                          0 AS wmsout,
                          0 AS bg,
                          SUM(NVL(ps.QUANTITY, 0)) AS wt,
                          0 AS jy,
                          0 AS qcbg,
                          0 AS qcjy,
                          0 AS pd
                     FROM partsstock ps
                    INNER JOIN warehouse w
                       ON ps.WAREHOUSEID = w.ID
                    INNER JOIN warehousearea wa
                       ON ps.warehouseareaid = wa.id
                    INNER JOIN WarehouseAreaCategory wac
                       ON wa.AreaCategoryId = wac.ID
                    WHERE w.StorageCompanyType = 1 --w.id=9--w.WMSINTERFACE =1
                      AND wac.CATEGORY = 2
                    GROUP BY w.ID, wa.ID, ps.partid, 0) UNION ALL
        (SELECT w.ID AS wid,
                ps.partid AS partid,
                0 AS wmsin,
                0 AS wmsout,
                0 AS bg,
                0 AS wt,
                SUM(NVL(ps.QUANTITY, 0)) AS jy,
                0 AS qcbg,
                0 AS qcjy,
                0 AS pd
           FROM partsstock ps
          INNER JOIN warehouse w
             ON ps.WAREHOUSEID = w.ID
          INNER JOIN warehousearea wa
             ON ps.warehouseareaid = wa.id
          INNER JOIN WarehouseAreaCategory wac
             ON wa.AreaCategoryId = wac.ID
          WHERE w.StorageCompanyType = 1 --w.id=9--w.WMSINTERFACE =1
            AND wac.CATEGORY = 3
          GROUP BY w.ID, wa.ID, ps.partid, 0) UNION ALL
        (SELECT w.ID AS wid,
                ps.partid AS partId,
                0 AS wmsin,
                0 AS wmsout,
                0 AS bg,
                0 AS wt,
                0 AS jy,
                SUM(NVL(ps.QUANTITY, 0)) AS qcbg,
                0 AS qcjy,
                0 AS pd
           FROM tmpdata.bak_partsstock20140709 ps
          INNER JOIN warehouse w
             ON ps.WAREHOUSEID = w.ID
          INNER JOIN warehousearea wa
             ON ps.warehouseareaid = wa.id
          INNER JOIN WarehouseAreaCategory wac
             ON wa.AreaCategoryId = wac.ID
          WHERE w.StorageCompanyType = 1 --w.id=9--w.WMSINTERFACE =1
            AND wac.CATEGORY = 1
          GROUP BY w.ID, ps.partid, 0) UNION ALL
        (SELECT w.ID AS wid,
                ps.partid AS partid,
                0 AS wmsin,
                0 AS wmsout,
                0 AS bg,
                0 AS wt,
                0 AS jy,
                0 AS qcbg,
                SUM(NVL(ps.QUANTITY, 0)) AS qcjy,
                0 AS pd
           FROM tmpdata.bak_partsstock20140709 ps
          INNER JOIN warehouse w
             ON ps.WAREHOUSEID = w.ID
          INNER JOIN warehousearea wa
             ON ps.warehouseareaid = wa.id
          INNER JOIN WarehouseAreaCategory wac
             ON wa.AreaCategoryId = wac.ID
          WHERE w.StorageCompanyType = 1 --w.id=9--w.WMSINTERFACE =1
            AND wac.CATEGORY = 3
          GROUP BY w.ID, ps.partid, 0)      
        UNION ALL (SELECT pib.WAREHOUSEID AS wid,
                          pid.SPAREPARTID AS partId,
                          0 AS wmsin,
                          0 AS wmsout,
                          0 AS bg,
                          0 AS wt,
                          0 AS jy,
                          0 AS qcbg,
                          0 AS qcjy,
                          SUM(pid.StorageDifference) AS pd
                     FROM PartsInventoryBill pib
                    INNER JOIN PartsInventoryDetail pid
                       ON pib.ID = pid.PARTSINVENTORYBILLID
                    INNER JOIN warehouse w
                       ON pib.WAREHOUSEID = w.ID
                    WHERE w.storagecompanytype = 1 --w.id=9----w.WMSINTERFACE =1 
                      and pib.status = 3
                      and pid.ifcover = 1
                    GROUP BY pib.WAREHOUSEID, pid.SPAREPARTID, 0))

 GROUP BY wid, partid)
 where ((wmsin - wmsout) != (bg + wt + jy - qcbg - pd));
/*HAVING (SUM(wmsin)             !=0
OR SUM(wmsout)                 !=0)*/
