create table partsstocklog as select partsstock.*,sysdate syncdate from  partsstock where rownum<1;
/
create or replace procedure setpartstocklog is
begin
  insert into partsstocklog
  select partsstock.*,sysdate from partsstock ;
end setpartstocklog;
/

begin
  sys.dbms_scheduler.create_job(job_name            => 'YXDCS.每日结转配件库存',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'setpartstocklog',
                                start_date          => to_date('16-02-2016 00:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/


begin
  sys.dbms_scheduler.create_job(job_name            => 'YXDCS.每日结转配件库存12',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'setpartstocklog',
                                start_date          => to_date('16-02-2016 12:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/