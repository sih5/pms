
--01-车辆组成库（前8位）
--故障分类
create table tmpdata.MALFUNCTIONCATEGORY
(
  LV1     VARCHAR2(500),
  LV1NAME VARCHAR2(500),
  LV2     VARCHAR2(500),
  LV2NAME VARCHAR2(500),
  LV3     VARCHAR2(500),
  LV3NAME VARCHAR2(500),
  LV4     VARCHAR2(500),
  LV4NAME VARCHAR2(500)
);



--02-故障模式标准库（故障代码后2位）
create table tmpdata.FaultModeStandard
(故障模式编码 varchar2(50),
故障模式描述 varchar2(50)
);

--03-维修方式标准库（标准维修工时代码后2位）
create table tmpdata.RepairMethodStandard
(维修方式编码 varchar2(50),
维修方式名称 varchar2(100),
维修方式描述 varchar2(200)
);


--04-故障代码(组合10位)
create table tmpdata.Malfunction 
(MalfunctionCategoryCode varchar2(50),
code varchar2(50),
name varchar2(100)) ;

--05-故障代码与品牌关系
create table tmpdata.MalfunctionBrandRelation 
(MalfunctionCategoryCode varchar2(50),
code varchar2(50),
name varchar2(100),
brand varchar2(100)) ;

--06-康明斯故障代码与品牌关系-汇总

--07-标准维修工时代码（组合10位）
create table tmpdata.repairitem 
(repairitemcategorycode varchar2(20),
code varchar2(50),
name varchar2(600));


--08-标准维修工时代码与产品线关系
create table tmpdata.RepairItemAffiServProdLine 
(repairitemcategorycode varchar2(20),
code varchar2(50),
name varchar2(600),
RepairQualificationGrade varchar2 (50),
productlinecode varchar2 (50),
productlinename varchar2(100),
productlinetype varchar2(100),
DefaultLaborHour number(9,2));

--09-康明斯标准维修工时代码-汇总

--10-产品信息
create table tmpdata.product ( 
产品编号	VARCHAR2(50),	
车型编号	VARCHAR2(50),	
车型名称	VARCHAR2(50),		
品牌代码	VARCHAR2(30),		
品牌名称	VARCHAR2(100),			
子品牌代码	VARCHAR2(30),		
子品牌名称	VARCHAR2(100),		
平台代码	VARCHAR2(30),		
平台名称	VARCHAR2(100),		
产品线代码	VARCHAR2(50),			
产品线名称	VARCHAR2(100),		
原始内部编号	VARCHAR2(100),		
公告号	VARCHAR2(50),		
吨位代码	VARCHAR2(50),	
发动机型号代码	VARCHAR2(100),		
发动机生产厂家代码	VARCHAR2(50),		
变速箱型号代码	VARCHAR2(50),	
变速箱生产厂家代码	VARCHAR2(50),		
后桥型号代码	VARCHAR2(50),	
后桥生产厂家代码	VARCHAR2(50),		
轮胎型号代码	VARCHAR2(50),	
轮胎形式代码	VARCHAR2(50),	
制动方式代码	VARCHAR2(50),		
驱动方式代码	VARCHAR2(30),	
车架连接形式代码	VARCHAR2(50),			
整车尺寸	VARCHAR2(50),		
轴距代码	VARCHAR2(50),	
排放标准代码	VARCHAR2(50),	
产品功能代码	VARCHAR2(100)	,	
产品档次代码	VARCHAR2(50),		
车辆类型代码	VARCHAR2(100),		
车辆类型名称	VARCHAR2(100)	,		
颜色描述	VARCHAR2(100),		
载客个数	NUMBER(9),		
自重	NUMBER(20,4));

--11-服务产品线
create table tmpdata.serviceproductline
 (brandcode varchar2 (50),
 brandname varchar2(100),
 pruductcode varchar2(50),
 productname varchar2(100),
 pruducttype varchar2(50));
 
 
 
--12-服务站服务经营权限
create table tmpdata.DealerBusinessPermit
(服务站编号  varchar2(100),
服务站名称   varchar2(100),
分公司编号   varchar2(100), 
分公司名称  varchar2(100), 
服务产品线名称  varchar2(100),
工时单价  varchar2(100),
工时单价等级名称  varchar2(100),
工时单价等级编号 varchar2(100)
);

--13-工时单价等级
create table tmpdata.LaborHourUnitPriceGrade 
(
Code varchar2(50),
name varchar2(100),
productlinecode varchar2(50),
productlinename varchar2(100),
productlinetype varchar2(100),
labhour number(9,2),
remark varchar2 (200) 
);

--16-人员与品牌关系
create table tmpdata.personnelbrandreltmp
 (
 personnelid varchar2(100),
 personnelname varchar2(100),
 brandname varchar2(100));
 
 --17-市场部
create table tmpdata.MarketDepRegion(
brandcode varchar2(50),
regioncode varchar2(50),
regionname varchar2(100),
marketDepCode varchar2(50),
marketDepName varchar2(100),
personnelloginid varchar2(100),
personnelname varchar2(100),
phone varchar2(100),
personneltype varchar2(100),
fax varchar2(100),
postcode varchar2(100),
remark varchar2(200));

--18-外出服务费等级
create table tmpdata.ServiceTripPriceGrade
 (code varchar2(50),
 name varchar2(100),
 RegionDescription varchar2(200),
 productlinecode varchar2(50),
 productlinename varchar2(100),
 productlinetype varchar2(50) ,
 ServiceTripType varchar2(50),
 maxmile number(9,2),
 minmile number (9,2),
 FixedAmount number(9,4),
 IsSelfCar varchar2(50),
 SubsidyPrice number(9,4),
 PricePrice number(9,4),
 remark varchar2 (200) );
 
 --19-星级
create table tmpdata.GradeCoefficient(
brand varchar2(100),
name varchar2(100),
Coefficient number(9,2));

--20-整车保修条款

--21-整车保养条款
create table tmpdata.VehicleMainteTerm
(WarrantyPolicycode varchar2(50),
WarrantyPolicyname varchar2(100),
code varchar2(50),
name varchar2(100),
MainteType varchar2(10),
BillingMethod varchar2(20),
WorkingHours number(9),
Capacity number(9),
MaxMileage number(9),
minmileage number(9),
DaysUpperLimit number(9),
 DayslowerLimit number(9),
 LaborCost number(9,4),
 MaterialCost number(9,4) ,
 OtherCost number(9,4) ,
 remark varchar2(200));
 
 --22-整车品牌产品线与服务品牌产品线关系
 create table tmpdata.ServiceProdLineProduct
 (mainbrandcode varchar2(50),
 mainbrandname varchar2(100),
 subbrandcode varchar2(50),
 subbrandname varchar2(100),
 TerraceCode varchar2(50),
 TerraceName varchar2(100),
 productlinecode varchar2(50),
 productlinename varchar2(100),
 serviceproductlinecode varchar2(50),
 serviceproductlinename varchar2(100));
 

 --23-保修政策
 create table tmpdata.WarrantyPolicy
 (
BranchCode varchar2(50),
BranchName varchar2(100),
code varchar2(50),
name varchar2(100),
Category varchar(10),
RepairTermInvolved varchar2(2),
MainteTermInvolved varchar2(2),
PartsWarrantyTermInvolved varchar2(2),
ReleaseDate date);

--24-保修政策与服务产品线关系
create table tmpdata.WarrantyPolicyNServProdLine(
WarrantyPolicycode varchar2(50),
WarrantyPolicyname varchar2(100),
productlinecode varchar2(50), 
productlinename varchar2(100),
productlinetype varchar2(100),
salesstarttime date ,
saleendtime date,
remark varchar2（200));

--25-服务站分品牌管理信息  
create table tmpdata.DealerServiceInfo 
(
服务站编号 varchar2(100),
服务站名称 varchar2(100),
/*业务编码 varchar2(100),
业务名称 varchar2(100),*/
品牌 varchar2(100),
市场部 varchar2(100),
业务能力 varchar2(100),
服务站类别 varchar2(100),
区域 varchar2(100),
区域类别 varchar2(100),
维修权限 varchar2(100),
授权时间 date,
是否职守 varchar2(100),
小时热线 varchar2(100),
固定电话 varchar2(100),
传真 varchar2(100),
订货默认仓库  varchar2(100),
服务站 varchar2(100),,
配件管理费率 varchar2(100),
外出服务费等级 varchar2(100),
星级 varchar2(100),
服务站维修权限 varchar2(100)
外出服务半径 varchar2(100),
旧件仓库名称  varchar2(100),
配件储存金额 varchar2(100),
材料费开票类型 varchar2(100),
工时费开票类型 varchar2(100),
工时费开票系数 int,
材料费开票系数 int,
销售与服务场地布局关系 varchar2(100)
   );
   
--26-PMS企业主数据模板-服务站-专卖店

/*--27-二级站
create table tmpdata.SubDealer (
subdealername varchar2(100),
subdealercode varchar2(50),
dealercode varchar2(50),
dealername varchar2(100),
dealertype varchar2(100),
address varchar2(200),
DealerManager varchar2(200),
manager varchar2(50),
phone varchar2(50),
mobile varchar2(50),
email varchar2(100),
remark varchar2(200));*/

--28-强制保养条款车型清单数据
create table tmpdata.VehicleMainteProductCategory (
ProductCategoryCode varchar2(100),
productline varchar2(100),
materialfee number(9,4));

--29-供应商信息

--30-配件保修条款
create table tmpdata.PartsWarrantyTerm (
WarrantyPolicyCode varchar2(100),
WarrantyPolicyName varchar2(100),
PartsWarrantyCategoryCode varchar2(100),
PartsWarrantyCategoryName varchar2(100),
WarrantyPeriod int,
WarrantyMileage int,
Capacity int,
WorkingHours int);







 
 
 


