

--创建权限临时表
create table tmpdata.Roletmp(
一级分类 varchar2(100),
二级分类 varchar2(100),
三级分类 varchar2(100),
功能 varchar2(100),
R01 varchar2(10),
R02 varchar2(10),
R03 varchar2(10),
R04 varchar2(10),
R05 varchar2(10),
R06 varchar2(10),
R07 varchar2(10),
R08 varchar2(10),
R09 varchar2(10),
R10 varchar2(10),
R11 varchar2(10),
R12 varchar2(10));

--PMS企业主数据模板-承运商
create table tmpdata.LogisticCompanytmp
(
  企业编号   VARCHAR2(90),
  企业名称   VARCHAR2(90),
  MDM客户编码     VARCHAR2(100),
  MDM供应商编码    VARCHAR2(100),
  简称       VARCHAR2(90),
  省       VARCHAR2(90),
  市       VARCHAR2(90),
  县       VARCHAR2(90),
  城市级别   VARCHAR2(90),  
  成立日期   date,
  联系人       varchar2(90),
  联系电话      varchar2(90),
  联系人手机     varchar2(90),
  联系地址    VARCHAR2(200),
  传真        varchar2(90),
  邮编      VARCHAR2(90),
  电子邮件    VARCHAR2(90),
  企业性质        varchar2(90),
  法定代表人 VARCHAR2(200),
  法人代表电话 VARCHAR2(200) ,
  注册资本    VARCHAR2(200) ,
  注册时间    date, 
  经营范围    varchar2(200),
  营业地址    VARCHAR2(200),
  供应商状态  VARCHAR2(200),
  税务登记号 VARCHAR2(200),
  开票名称 VARCHAR2(200),
  开票类型 VARCHAR2(200),
  税务登记地址 VARCHAR2(200),
  税务登记电话 VARCHAR2(200),
  开户行名称 VARCHAR2(200),
  开户行账号 VARCHAR2(200),
  收货地址 VARCHAR2(200),
  状态 VARCHAR2(200),
  备注 VARCHAR2(200)
 -- 业务编码    VARCHAR2(90),
  --营业地址  VARCHAR2(200)
 -- 品牌      VARCHAR2(90),  
);

--PMS企业主数据模板-服务站-专卖店
create table tmpdata.dealertmp
(
 -- 业务类型        VARCHAR2(100),
  企业编号        VARCHAR2(100), 
  企业名称        VARCHAR2(100),
  MDM客户编码     VARCHAR2(100),
  MDM供应商编码    VARCHAR2(100),
  简称          VARCHAR2(100),
  --服务经理   VARCHAR2(100),
  省           VARCHAR2(100),
  市           VARCHAR2(100),
  县           VARCHAR2(100),
  城市级别        VARCHAR2(100),
  成立日期        VARCHAR2(100),   
  联系人         VARCHAR2(100),
  联系电话        VARCHAR2(100),
  联系人手机       VARCHAR2(100),
  --固定电话  VARCHAR2(100),
  传真          VARCHAR2(100), 
   联系地址        VARCHAR2(100),
  邮政编码        VARCHAR2(100),
  电子邮箱        VARCHAR2(100),
  企业性质        VARCHAR2(100),
  法定代表人       VARCHAR2(100),
  法人代表电话      VARCHAR2(100),
  注册资本        NUMBER(19,4),
  注册日期        VARCHAR2(100),
  经营范围        VARCHAR2(300),
  营业地址        VARCHAR2(100),
  税务登记号       VARCHAR2(100),  
  开票名称        VARCHAR2(100),
  开票类型        VARCHAR2(100),
  税务登记地址      VARCHAR2(100) ,
  开票电话        VARCHAR2(100),     
  银行名称        VARCHAR2(100),
  银行账号        VARCHAR2(100),
  状态          VARCHAR2(100),  
  维修资质等级      VARCHAR2(100), 
  隶属单位        VARCHAR2(100),
  建厂时间        VARCHAR2(100), 
  有二级服务站      VARCHAR2(100),
  车辆行驶路线      VARCHAR2(100),
  车辆使用特性      VARCHAR2(100), 
  车辆停靠点      VARCHAR2(100), 
  主营范围        VARCHAR2(300),
  兼营范围        VARCHAR2(100),
  福田授权品牌      VARCHAR2(100),
  非福田授权品牌     VARCHAR2(100), 
  交通限行描述      VARCHAR2(100),
  危险品运输车辆维修资质 VARCHAR2(100),
  服务站人数       VARCHAR2(100),
  维修车面积       VARCHAR2(100),
  接待室面积       VARCHAR2(100),
  停车场面积       VARCHAR2(100),
  配件库面积       VARCHAR2(100), 
  配件储存金额      NUMBER(19,4),
  视频手机号       VARCHAR2(100)
);


--仓库
create table  tmpdata.warehousetmp（
    warehousecode varchar2(100),
    warehousename varchar2(100),
    Type varchar2(100),
    Email varchar2(100),
    Fax varchar2(100),
    Contact varchar2(100),
    PhoneNumber varchar2(100),
    Address varchar2(200), 
    StorageStrategy varchar2(200),   
    StorageCenter  varchar2(200)
）;


--分公司仓库库区库位
create table  tmpdata.warehouseareatmp（
    warehousecode varchar2(100),
    warehousename varchar2(100),
    warehouseareacode varchar2(100),
    locationcode varchar2(100),
    AreaCategory varchar2(10),
    remark varchar2(200)
）;

--分公司仓库人员信息
create table tmpdata.WarehouseOperatortmp (
    warehousecode varchar2(100),
    warehousename varchar2(100),
    personnelname varchar2(100),
    personnelcode varchar2(100)
);

--分公司库区负责人
create table tmpdata.WAREHOUSEAREAMANAGERtmp
(
  WAREHOUSECODE     VARCHAR2(50),
  WAREHOUSENAME     VARCHAR2(100),
  WAREHOUSEAREACODE VARCHAR2(50),
  PERSONNNELCODE    VARCHAR2(50),
  PERSONNELNAME     VARCHAR2(100)
);

--配件分品牌信息
create table tmpdata.partsbranchtmp
(
    品牌    varchar2(200),
    配件图号    varchar2(200),
    配件名称    varchar2(200),
    配件参考图号    varchar2(200),
    损耗类型    varchar2(200),
    是否可采购    varchar2(200),
    是否可销售    varchar2(200),
    最小销售数量    varchar2(200),
    是否可直供    varchar2(200),
    旧件返回政策  varchar2(200),
    配件保修分类编号    varchar2(200),
    配件保修分类名称    varchar2(200),
    供应商编码    varchar2(200),
    供应商名称    varchar2(200),
    供应商图号	varchar2(200)
);

--配件信息
create table tmpdata.spareparttmp (
    partcode varchar2 (50),
    partname varchar2 (100),
    PartType varchar2(100),
    MInPackingAmount number(9) ,
    EnglishName varchar2(100),
    ReferenceCode varchar2(50),
    Specification varchar2(100),
    MeasureUnit varchar2(20),
    Feature varchar2(200),
    LastSubstitute varchar2(50),
    NextSubstitute varchar2(50),
    ShelfLife number(9)
);


--企业收货地址
create table tmpdata.CompanyAddresstmp (
Companycode varchar2 (100),
CompanyName varchar2 (100),
ProvinceName varchar2 (100) ,
CityName varchar2 (100) ,
CountyName varchar2 (100) ,
DetailAddress  varchar2 (200) ,
ContactPerson varchar2 (100) ,
ContactPhone varchar2 (100) 
);


--账户组
create table tmpdata.accountgrouptmp( 
companycode varchar2 (50),
companyname varchar2(100),
accountgroup varchar2(100)
);


--供应商信息
create table tmpdata.PartsSuppliertmp
( 
品牌 varchar2(100),
供应商编号 varchar2(100),
供应商名称 varchar2(100),
业务编码 varchar2(100),
业务名称 varchar2(100),
供应商类型 varchar2(100),
配件管理费系数 varchar2(100),
工时索赔系数 number(8),
配件索赔价格类型 varchar2(100),
是否按工时单价索赔 varchar2(100),
外出里程单价 varchar2(100),
外出人员单价 varchar2(100),
是否存在外出费用 varchar2(100),
旧件运费系数 number(16,5),
省 varchar2(100),
市 VARCHAR2(100),
县 varchar2(100),
邮编 VARCHAR2(100),
电子邮件 VARCHAR2(100),
传真 varchar2(100),
联系人 VARCHAR2(100),
联系电话 VARCHAR2(100),
联系地址 VARCHAR2(100),
注册地址 varchar2(100),
经营范围 varchar2(100),
注册日期 varchar2(100)
);

--企业价格等级
create table tmpdata.CustomerOrderPriceGradetmp(
brandname varchar2(100),
customercode varchar2(50),
customername varchar2(100),
salesordertypecode varchar2(50),
salesordertypename varchar2(100),
coefficient number(9,2)
);

--配件采购价
create table tmpdata.PartsPurchasePricingtmp
(
  brandname varchar2(100),      --品牌
  SpareCode   VARCHAR2(60),  --配件编号
  SpareName   VARCHAR2(60),  --配件名称
  SupplierCode     VARCHAR2(20), --供应商编号
  SupplierName     VARCHAR2(50), --供应商名称
  pricetype      VARCHAR2(20), --价格类型
  PurchasePrice   NUMBER(19,4),  --采购价
  ValidFrom      date  ,       --生效时间
  ValidTo        date         --失效时间
);

--配件计划价
create table tmpdata.PartsPlannedPricetmp
(
  brandname varchar2(100),      --品牌
  SpareCode   VARCHAR2(60),  --配件编号
  SpareName   VARCHAR2(60),  --配件名称
  PlannedPrice   NUMBER(19,4)  --计划价
);

--配件零售指导价
create table tmpdata.PartsRetailGuidePricetmp
(
  brandname varchar2(100),      --品牌
  SpareCode   VARCHAR2(60),  --配件编号
  SpareName   VARCHAR2(60),  --配件名称
  RetailGuidePrice   NUMBER(19,4)  --零售指导价
);

--配件销售价
create table tmpdata.PartsSalesPricetmp
(
  brandname varchar2(100),      --品牌
  SpareCode   VARCHAR2(60),  --配件编号
  SpareName   VARCHAR2(60),  --配件名称
  PartsSalesPrice   NUMBER(19,4)  --销售价
);

--人员与品牌关系
create table tmpdata.personnelbrandreltmp
 (
 personnelid varchar2(100),
 personnelname varchar2(100),
 brandname varchar2(100));

/*


--旧件库区库位
Create table UsedPartsWarehouseAreatmp
(
  warehousecode varchar2(20),
  warehousename varchar2(40),
  filialecode varchar2(20),
  filialename varchar2(50),
  areacode varchar2(100),
  locationcode varchar2(100),
  type varchar2(20),
  StorageCategory varchar2(20)
);

--替互换件
create table PartsExchange(
code varchar2(50),
referencecode varchar2(50),
name varchar2(100),
exchangecode varchar2(50)
);

--部门
create table departmentinformationtmp (
code varchar2 (50),
name varchar2(100)
);

--PMS企业主数据模板-代理库
create table Agenttmp
(
  企业编号   VARCHAR2(90),
  企业名称   VARCHAR2(90),
  --企业类型   VARCHAR2(90),
  --MDM客户编码     VARCHAR2(100),
  --MDM供应商编码    VARCHAR2(100),
  简称       VARCHAR2(90),
  省       VARCHAR2(90),
  市       VARCHAR2(90),
  县       VARCHAR2(90),
  城市级别   VARCHAR2(90),
  成立日期  date,  
  联系人       varchar2(90),
  联系电话      varchar2(90),
  联系人手机     varchar2(90),
    传真        varchar2(90),
  联系地址    VARCHAR2(200),
      邮编      VARCHAR2(90),
  电子邮件    VARCHAR2(90),
  企业性质 VARCHAR2(90),
    法定代表人 VARCHAR2(200),
  法人代表电话 VARCHAR2(200) ,
  注册资本 VARCHAR2(200),
    注册时间    date, 
  --注册地址    VARCHAR2(200),
  经营范围    varchar2(200),
  营业地址  VARCHAR2(200),
 -- 品牌      VARCHAR2(90),
 -- 业务编码    VARCHAR2(90),
  税务登记号 VARCHAR2(200),
  开票名称 VARCHAR2(200),
  开票类型 VARCHAR2(200),
  税务登记地址 VARCHAR2(200),
  税务登记电话 VARCHAR2(200),
  --收货地址 VARCHAR2(200),
  开户行名称 VARCHAR2(200),
  开户行账号 VARCHAR2(200),
 -- 状态 VARCHAR2(200),
  备注 VARCHAR2(200)
);

--代理库仓库库区库位
create table dlkwarehouseareatmp(
warehousecode varchar2(100),
warehousename varchar2(100),
storecompanycode varchar2(100),
storecompanyname varchar2(100),
storecompanytype varchar2(100),
warehouseareacode varchar2(100),
locationcode varchar2(100),
AreaCategory varchar2(10)
); 

--代理库服务站隶属关系
create table AgencyDealerRelationtmp(
brandname varchar2(50),
agencycode varchar2(50),
agencyname varchar2(100),
dealercode varchar2(50),
dealername varchar2(100)
);
*/

