
--PMS企业主数据模板-承运商
--区省市是否有效
select * from LogisticCompanytmp where 省 not in (select name from Region where Type=2);
select * from LogisticCompanytmp where 市 not in (select name from Region where Type=3);
select * from LogisticCompanytmp where 县 not in (select name from Region where Type=4);


--分公司仓库库区库位
--仓库是否有效
select * from warehouseareatmp where warehousecode not in  (select * from Warehouse where status=1);

--分公司仓库人员信息
--仓库是否有效
select * from WarehouseOperatortmp where warehousecode not in  (select * from Warehouse where status=1);

--分公司库区负责人
--仓库库区是否有效
select * from WAREHOUSEAREAMANAGERtmp a left join warehouseareatmp b on a.WAREHOUSECODE=b.warehousecode and a.WAREHOUSEAREACODE=b.warehouseareacode
where b.warehousecode is null

--配件信息
--配件是否重复
select PARTCODE,count(*) from spareparttmp   group by  PARTCODE having count(*)>1;

--各类价格
--配件是否有效
select * from PartsPurchasePricingtmp  where SpareCode not in (select partcode from spareparttmp );
select * from PartsPlannedPricetmp   where SpareCode not in (select partcode from spareparttmp );
select * from PartsRetailGuidePricetmp   where SpareCode not in (select partcode from spareparttmp );
select * from PartsSalesPricetmp   where SpareCode not in (select partcode from spareparttmp );
--配件是否重复
select SpareCode,count(*) from PartsPurchasePricingtmp  group by  SpareCode having count(*)>1;
select  SpareCode,count(*)  from PartsPlannedPricetmp     group by  SpareCode having count(*)>1;
select  SpareCode,count(*)  from PartsRetailGuidePricetmp    group by  SpareCode having count(*)>1;
select  SpareCode,count(*)  from PartsSalesPricetmp    group by  SpareCode having count(*)>1;
--供应商是否有效
select * from partsbranchtmp where 供应商编码 not in (select 供应商编号 from PartsSuppliertmp );


--配件分品牌信息
--配件是否有效
select * from partsbranchtmp where 配件图号 not in (select partcode from spareparttmp );
--供应商是否有效
select * from partsbranchtmp where 供应商编码 not in (select 供应商编号 from PartsSuppliertmp );

--供应商信息
--区省市是否有效
select * from PartsSuppliertmp where 省 not in (select name from Region where Type=2);
select * from PartsSuppliertmp where 市 not in (select name from Region where Type=3);
select * from PartsSuppliertmp where 县 not in (select name from Region where Type=4);

--企业收货地址
--区省市是否有效
select * from CompanyAddresstmp where PROVINCENAME not in (select name from Region where Type=2);
select * from CompanyAddresstmp where cityname not in (select name from Region where Type=3);
select * from CompanyAddresstmp where COUNTYNAME not in (select name from Region where Type=4);

----服务站信息
--区省市是否有效
select * from dealertmp where 省 not in (select name from Region where Type=2);
select * from dealertmp where 市 not in (select name from Region where Type=3);
select * from dealertmp where 县 not in (select name from Region where Type=4);

---企业价格等级
--企业是否有效
select * from CustomerOrderPriceGradetmp where customercode not in (select 企业编号 from dealertmp);








 


