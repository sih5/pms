
--1.服务总监**********************************************
--新增权限
INSERT INTO Rule(Id,Roleid,Nodeid)
Select S_Rule.Nextval as Id, 
role.id as RoleId,Node1.NodeId from (Select distinct NodeId from (
Select Node.Id as NodeId from Node --权限-节点
inner join Page on page.id = node.categoryid and node.categorytype = 0
inner join tmpdata.Roletmp tmp on tmp.三级分类 = page.name
where R01 = '1'
union all --权限-操作按钮
Select Node.Id as NodeId from Node 
inner join Action on Action.id = node.categoryid 
inner join Page on Page.Id = Action.Pageid
inner join tmpdata.Roletmp tmp on tmp.功能 = Action.name and tmp.三级分类 = Page.Name
where R01 = '1' and 功能 <> '查询' and node.categorytype = 1
)) Node1
cross join role where role.name = 
'服务总监' and role.enterpriseid in (select id from enterprise where code='2601');

--2.副部长**********************************************
--新增权限
INSERT INTO Rule(Id,Roleid,Nodeid)
Select S_Rule.Nextval as Id, 
role.id as RoleId,Node1.NodeId from (Select distinct NodeId from (
Select Node.Id as NodeId from Node --权限-节点
inner join Page on page.id = node.categoryid and node.categorytype = 0
inner join tmpdata.Roletmp tmp on tmp.三级分类 = page.name
where R02 = '1'
union all --权限-操作按钮
Select Node.Id as NodeId from Node 
inner join Action on Action.id = node.categoryid 
inner join Page on Page.Id = Action.Pageid
inner join tmpdata.Roletmp tmp on tmp.功能 = Action.name and tmp.三级分类 = Page.Name
where R02 = '1' and 功能 <> '查询' and node.categorytype = 1
)) Node1
cross join role where role.name = 
'副部长' and role.enterpriseid in (select id from enterprise where code='2601');

--3 配件采购二科科长**********************************************
--新增权限
INSERT INTO Rule(Id,Roleid,Nodeid)
Select S_Rule.Nextval as Id, 
role.id as RoleId,Node1.NodeId from (Select distinct NodeId from (
Select Node.Id as NodeId from Node --权限-节点
inner join Page on page.id = node.categoryid and node.categorytype = 0
inner join tmpdata.Roletmp tmp on tmp.三级分类 = page.name
where R03 = '1'
union all --权限-操作按钮
Select Node.Id as NodeId from Node 
inner join Action on Action.id = node.categoryid 
inner join Page on Page.Id = Action.Pageid
inner join tmpdata.Roletmp tmp on tmp.功能 = Action.name and tmp.三级分类 = Page.Name
where R03 = '1' and 功能 <> '查询' and node.categorytype = 1
)) Node1
cross join role where role.name = 
'配件采购二科科长' and role.enterpriseid in (select id from enterprise where code='2601');

--4 配件采购二科副科长**********************************************
--新增权限
INSERT INTO Rule(Id,Roleid,Nodeid)
Select S_Rule.Nextval as Id, 
role.id as RoleId,Node1.NodeId from (Select distinct NodeId from (
Select Node.Id as NodeId from Node --权限-节点
inner join Page on page.id = node.categoryid and node.categorytype = 0
inner join tmpdata.Roletmp tmp on tmp.三级分类 = page.name
where R04 = '1'
union all --权限-操作按钮
Select Node.Id as NodeId from Node 
inner join Action on Action.id = node.categoryid 
inner join Page on Page.Id = Action.Pageid
inner join tmpdata.Roletmp tmp on tmp.功能 = Action.name and tmp.三级分类 = Page.Name
where R04 = '1' and 功能 <> '查询' and node.categorytype = 1
)) Node1
cross join role where role.name = 
'配件采购二科副科长' and role.enterpriseid in (select id from enterprise where code='2601');

--5 采购计划管理**********************************************
--新增权限
INSERT INTO Rule(Id,Roleid,Nodeid)
Select S_Rule.Nextval as Id, 
role.id as RoleId,Node1.NodeId from (Select distinct NodeId from (
Select Node.Id as NodeId from Node --权限-节点
inner join Page on page.id = node.categoryid and node.categorytype = 0
inner join tmpdata.Roletmp tmp on tmp.三级分类 = page.name
where R05 = '1'
union all --权限-操作按钮
Select Node.Id as NodeId from Node 
inner join Action on Action.id = node.categoryid 
inner join Page on Page.Id = Action.Pageid
inner join tmpdata.Roletmp tmp on tmp.功能 = Action.name and tmp.三级分类 = Page.Name
where R05 = '1' and 功能 <> '查询' and node.categorytype = 1
)) Node1
cross join role where role.name = 
'采购计划管理' and role.enterpriseid in (select id from enterprise where code='2601');

--6 故障件管理**********************************************
--新增权限
INSERT INTO Rule(Id,Roleid,Nodeid)
Select S_Rule.Nextval as Id, 
role.id as RoleId,Node1.NodeId from (Select distinct NodeId from (
Select Node.Id as NodeId from Node --权限-节点
inner join Page on page.id = node.categoryid and node.categorytype = 0
inner join tmpdata.Roletmp tmp on tmp.三级分类 = page.name
where R06 = '1'
union all --权限-操作按钮
Select Node.Id as NodeId from Node 
inner join Action on Action.id = node.categoryid 
inner join Page on Page.Id = Action.Pageid
inner join tmpdata.Roletmp tmp on tmp.功能 = Action.name and tmp.三级分类 = Page.Name
where R06 = '1' and 功能 <> '查询' and node.categorytype = 1
)) Node1
cross join role where role.name = 
'故障件管理' and role.enterpriseid in (select id from enterprise where code='2601');

--7 配件技术二科副科长**********************************************
--新增权限
INSERT INTO Rule(Id,Roleid,Nodeid)
Select S_Rule.Nextval as Id, 
role.id as RoleId,Node1.NodeId from (Select distinct NodeId from (
Select Node.Id as NodeId from Node --权限-节点
inner join Page on page.id = node.categoryid and node.categorytype = 0
inner join tmpdata.Roletmp tmp on tmp.三级分类 = page.name
where R07 = '1'
union all --权限-操作按钮
Select Node.Id as NodeId from Node 
inner join Action on Action.id = node.categoryid 
inner join Page on Page.Id = Action.Pageid
inner join tmpdata.Roletmp tmp on tmp.功能 = Action.name and tmp.三级分类 = Page.Name
where R07 = '1' and 功能 <> '查询' and node.categorytype = 1
)) Node1
cross join role where role.name = 
'配件技术二科副科长' and role.enterpriseid in (select id from enterprise where code='2601');

--8 配件技术管理**********************************************	配件价格管理	配件销售二科副科长	订单管理	配件销售管理

--新增权限
INSERT INTO Rule(Id,Roleid,Nodeid)
Select S_Rule.Nextval as Id, 
role.id as RoleId,Node1.NodeId from (Select distinct NodeId from (
Select Node.Id as NodeId from Node --权限-节点
inner join Page on page.id = node.categoryid and node.categorytype = 0
inner join tmpdata.Roletmp tmp on tmp.三级分类 = page.name
where R08 = '1'
union all --权限-操作按钮
Select Node.Id as NodeId from Node 
inner join Action on Action.id = node.categoryid 
inner join Page on Page.Id = Action.Pageid
inner join tmpdata.Roletmp tmp on tmp.功能 = Action.name and tmp.三级分类 = Page.Name
where R08 = '1' and 功能 <> '查询' and node.categorytype = 1
)) Node1
cross join role where role.name = 
'配件技术管理' and role.enterpriseid in (select id from enterprise where code='2601');


--9 配件价格管理**********************************************		配件销售二科副科长	订单管理	配件销售管理

--新增权限
INSERT INTO Rule(Id,Roleid,Nodeid)
Select S_Rule.Nextval as Id, 
role.id as RoleId,Node1.NodeId from (Select distinct NodeId from (
Select Node.Id as NodeId from Node --权限-节点
inner join Page on page.id = node.categoryid and node.categorytype = 0
inner join tmpdata.Roletmp tmp on tmp.三级分类 = page.name
where R09 = '1'
union all --权限-操作按钮
Select Node.Id as NodeId from Node 
inner join Action on Action.id = node.categoryid 
inner join Page on Page.Id = Action.Pageid
inner join tmpdata.Roletmp tmp on tmp.功能 = Action.name and tmp.三级分类 = Page.Name
where R09 = '1' and 功能 <> '查询' and node.categorytype = 1
)) Node1
cross join role where role.name = 
'配件价格管理' and role.enterpriseid in (select id from enterprise where code='2601');

--10 配件销售二科副科长**********************************************		订单管理	配件销售管理

--新增权限
INSERT INTO Rule(Id,Roleid,Nodeid)
Select S_Rule.Nextval as Id, 
role.id as RoleId,Node1.NodeId from (Select distinct NodeId from (
Select Node.Id as NodeId from Node --权限-节点
inner join Page on page.id = node.categoryid and node.categorytype = 0
inner join tmpdata.Roletmp tmp on tmp.三级分类 = page.name
where R10 = '1'
union all --权限-操作按钮
Select Node.Id as NodeId from Node 
inner join Action on Action.id = node.categoryid 
inner join Page on Page.Id = Action.Pageid
inner join tmpdata.Roletmp tmp on tmp.功能 = Action.name and tmp.三级分类 = Page.Name
where R10 = '1' and 功能 <> '查询' and node.categorytype = 1
)) Node1
cross join role where role.name = 
'配件销售二科副科长' and role.enterpriseid in (select id from enterprise where code='2601');

--11 订单管理**********************************************			配件销售管理

--新增权限
INSERT INTO Rule(Id,Roleid,Nodeid)
Select S_Rule.Nextval as Id, 
role.id as RoleId,Node1.NodeId from (Select distinct NodeId from (
Select Node.Id as NodeId from Node --权限-节点
inner join Page on page.id = node.categoryid and node.categorytype = 0
inner join tmpdata.Roletmp tmp on tmp.三级分类 = page.name
where R11 = '1'
union all --权限-操作按钮
Select Node.Id as NodeId from Node 
inner join Action on Action.id = node.categoryid 
inner join Page on Page.Id = Action.Pageid
inner join tmpdata.Roletmp tmp on tmp.功能 = Action.name and tmp.三级分类 = Page.Name
where R11 = '1' and 功能 <> '查询' and node.categorytype = 1
)) Node1
cross join role where role.name = 
'订单管理' and role.enterpriseid in (select id from enterprise where code='2601');


--12 配件销售管理**********************************************			

--新增权限
INSERT INTO Rule(Id,Roleid,Nodeid)
Select S_Rule.Nextval as Id, 
role.id as RoleId,Node1.NodeId from (Select distinct NodeId from (
Select Node.Id as NodeId from Node --权限-节点
inner join Page on page.id = node.categoryid and node.categorytype = 0
inner join tmpdata.Roletmp tmp on tmp.三级分类 = page.name
where R12 = '1'
union all --权限-操作按钮
Select Node.Id as NodeId from Node 
inner join Action on Action.id = node.categoryid 
inner join Page on Page.Id = Action.Pageid
inner join tmpdata.Roletmp tmp on tmp.功能 = Action.name and tmp.三级分类 = Page.Name
where R12 = '1' and 功能 <> '查询' and node.categorytype = 1
)) Node1
cross join role where role.name = 
'配件销售管理' and role.enterpriseid in (select id from enterprise where code='2601');

