


Insert into malfunctioncategory
  select s_malfunctioncategory.nextval id, 
  v.*
    from (select (select id from malfunctioncategory where parentid is null) as PARENTID,
                 1 as ROOTGROUPID,
                 lv1 as CODE,
                 lv1name as NAME,
                 5 as LAYERNODETYPEID,
                 1 as STATUS,
                 null as REMARK,
                 1 as CREATORID,
                 '系统管理员' as CREATORNAME,
                 sysdate as CREATETIME,
                 null as MODIFIERID,
                 null as MODIFIERNAME,
                 null as MODIFYTIME,
                 null as ROWVERSION
            from tmpdata.MALFUNCTIONCATEGORY
           group by lv1, lv1name) v;
  
Insert into malfunctioncategory
  select s_malfunctioncategory.nextval as id,
   v.*
    from (select (select id from malfunctioncategory  where code = a.lv1) as parentid,
                 1 as ROOTGROUPID,
                 a.LV2 as CODE,
                 a.LV2NAME as NAME,
                 5 as LAYERNODETYPEID,
                 1 as STATUS,
                 null as REMARK,
                 1 as CREATORID,
                 '系统管理员' as CREATORNAME,
                 sysdate as CREATETIME,
                 null as MODIFIERID,
                 null as MODIFIERNAME,
                 null as MODIFYTIME,
                 null as ROWVERSION
            from tmpdata.MALFUNCTIONCATEGORY a
           group by a.lv1, a.lv2, a.lv2name) v;
   

Insert into  malfunctioncategory
  select s_malfunctioncategory.nextval as id, 
  v.*
    from (select  (select  b.id
                    from malfunctioncategory b
                    inner join  malfunctioncategory c on c.id=b.parentid
                   where b.code=a.lv2 and c.code=a.lv1 and c.code <>'WXXM根') as parentid,
                 1 as ROOTGROUPID,
                 a.LV3 as CODE,
                 a.LV3NAME as NAME,
                 6 as LAYERNODETYPEID,
                 1 as STATUS,
                 null as REMARK,
                 1 as CREATORID,
                 '系统管理员' as CREATORNAME,
                 sysdate as CREATETIME,
                 null as MODIFIERID,
                 null as MODIFIERNAME,
                 null as MODIFYTIME,
                 null as ROWVERSION
            from tmpdata.MALFUNCTIONCATEGORY a
           group by a.lv1, a.lv2, a.lv3, a.lv3name) v;
           
           
     