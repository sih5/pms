
insert into DealerBusinessPermit
  (Id,
   DealerId,
   ServiceProductLineId,
   ProductLineType,
   BranchId,
   ServiceFeeGradeId,
   CreateTime,
   CreatorName)
  select s_DealerBusinessPermit.Nextval,
         b.id,
         c.ProductLineId,
         c.ProductLineType,
         (select id from company where code='2601'),
         e.id,
         sysdate,
         'Admin'
    from tmpdata.DealerBusinessPermit a
   inner join dealer b
      on a.服务站编号 = b.code
   inner join serviceproductlineview c
      on c.ProductLineName = a.服务产品线名称
   inner join LaborHourUnitPriceRate d
      on d.ServiceProductLineId = c.ProductLineId
     and d.Price = a.工时单价
   inner join LaborHourUnitPriceGrade e
      on e.id = d.laborhourunitpricegradeid
     and e.code = a.工时单价等级名称
   where not exists (select 1
            from DealerBusinessPermit tmp
           where tmp.dealerid = b.id
             and tmp.serviceproductlineid = c.ProductLineId);
      
