


--工时单价等级
insert into LaborHourUnitPriceGrade
  (id,
   branchid,
   code,
   name,
   status,
   remark,
   creatorid,
   creatorname,
   createtime)
  select s_LaborHourUnitPriceGrade.Nextval,
         (select id from company where code='2601'),
         t.code,
         t.name,
         1,
         '',
         1,
         'Admin',
         sysdate
    from (select distinct v.code, v.name from tmpdata.LaborHourUnitPriceGrade v) t;

--工时单价标准
insert into LaborHourUnitPriceRate
  (id,
   laborhourunitpricegradeid,
   code,
   serviceproductlineid,
   productlinetype,
   price,
   status,
   remark,
   creatorid,
   creatorname,
   createtime)
  select s_LaborHourUnitPriceRate.Nextval,
   b.id  as laborhourunitpricegradeid,
   'WHP1000' || decode(length(rownum), 1, '0' || rownum, 2, rownum) CODE,
   c.id as serviceproductlineid,
   decode(a.productlinetype, '服务产品线', 1, 2) as productlinetype,
   a.labhour,
   1,
   b.REMARK,
   b.creatorid,
   b.creatorname,
   b.createtime
    from tmpdata.LaborHourUnitPriceGrade a inner join LaborHourUnitPriceGrade b on a.code =b.code
    inner join serviceproductline c on a.productlinecode=c.code;
    
  
    
    
    
    