--销售大区
insert into SalesRegion
  (id,
   branchid,
   partssalescategoryid,
   regioncode,
   regionname,
   status,
   remark,
   createtime,
   creatorid,
   creatorname)
  select s_salesregion.nextval,  (select id from company where code='2601'）as branchid,
         (select id from partssalescategory where name = t.brandcode),
         t.regioncode,
         t.regionname,
         1,
         '',
         sysdate,
         1,
         'Admin'
    from (select distinct brandcode, regioncode, regionname
            from tmpdata.MarketDepRegion) t
   where t.regioncode is not null;


   --营销分公司市场部
   
   insert into marketingdepartment
     (id,
      branchid,
      partssalescategoryid,
      businesstype,
      code,
      name,
      address,
      phone,
      fax,
      postcode,
      status,
      remark,
      creatorid,
      creatorname,
      createtime)
     select s_marketingdepartment.nextval,
            (select id from company where code='2601'）,
            (select id from partssalescategory where name = v.brandcode),
            decode(v.personneltype, '销售', 1, '服务', 2),
            v.marketdepcode,
            v.marketdepname,
            '',
            v.phone,
            v.fax,
            v.postcode,
            1,
            '',
            1,
            'Admin',
            sysdate
       from (select distinct t.brandcode,
                    t.marketdepcode,
                    t.marketdepname,
                    t.fax,
                    t.postcode,
                    t.phone,
                    t.personneltype 
               from tmpdata.MarketDepRegion t) v;
               
  --市场部与人员关系             
insert into MarketDptPersonnelRelation
  (id, branchid, marketdepartmentid, personnelid, status, type)
  select s_MarketDptPersonnelRelation.Nextval,
                (select id from company where code='2601') as branchid,
         (select id from marketingdepartment where code = t.marketdepcode) as marketdepartmentid,
         (select id
            from sdsecurity.personnel
           where loginid = t.personnelloginid
             and personnel.name = t.personnelname) as personnelid,
         1 as status,
         decode(t.personneltype,
                '服务',
                1,
                '服务后台人员',
                2,
                '配件后台人员',
                3) as type
    from tmpdata.MarketDepRegion t;
    