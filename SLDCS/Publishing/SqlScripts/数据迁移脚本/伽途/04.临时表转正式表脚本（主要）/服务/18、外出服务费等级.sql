--外出服务费等级
 insert into ServiceTripPriceGrade
   (id,
    branchid,
    code,
    name,
    regiondescription,
    status,
    remark,
    creatorid,
    creatorname,
    createtime)
   select s_ServiceTripPriceGrade.Nextval,(select id from company where code='2601'),
          t.code,
          t.name,
          t.regiondescription,
          1,
          '',
          1,
          'Admin',
          sysdate
     from (select distinct v.code, v.name, v.regiondescription
             from tmpdata.ServiceTripPriceGrade v) t;
             
         
   --外出服务费标准          
insert into ServiceTripPriceRate
  (id,
   servicetrippricegradeid,
   serviceproductlineid,
   productlinetype,
   code,
   servicetriptype,
   maxmileage,
   minmileage,
   fixedamount,
   isselfcar,
   subsidyprice,
   price,
   status,
   remark,
   creatorid,
   creatorname,
   createtime)
  select s_ServiceTripPriceRate.Nextval,
         (select id
            from ServiceTripPriceGrade a
           where a.code = t.code) as servicetrippricegradeid,
         (select b.ProductLineId
            from serviceproductlineview b
           where b.ProductLineCode = t.productlinecode) as serviceproductlineid,
         decode(t.productlinetype, '服务产品线', 1, 2),
         s_ServiceTripPriceRate.Currval,
         decode(t.servicetriptype, '白天', 1, '夜间', 2),
         t.maxmile,
         t.minmile,
         t.fixedamount,
         decode (t.isselfcar,'是',1,'否',0),
         t.subsidyprice,
         t.priceprice,
         1,
         t.remark,
         1,
         'Admin',
         sysdate
    from tmpdata.ServiceTripPriceGrade t;
  

    

    
    

