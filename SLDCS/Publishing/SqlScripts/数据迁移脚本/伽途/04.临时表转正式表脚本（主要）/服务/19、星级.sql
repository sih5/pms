
insert into gradecoefficient
  (id,
   branchid,
   branchname,
   partssalescategoryid,
   partssalescategoryname,
   grade,
   coefficient,
   status,
   creatorid,
   creatorname,
   createtime)
  select  s_gradecoefficient.nextval,
         (select id from company where code='2601'),
         (select name from company where code='2601'),
         (select id from partssalescategory where name = t.brand),
         t.brand,
         t.name,
         t.coefficient,
         1,
         1,
         'Admin',
         sysdate
    from tmpdata.gradecoefficient t;

