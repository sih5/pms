

--配件保修分类
insert into PartsWarrantyCategory(id,Branchid,Code,Name,Status,Creatorid,Creatorname,Createtime)
select s_PartsWarrantyCategory.Nextval,(select id from company where code='2601'),PartsWarrantyCategoryCode,PartsWarrantyCategoryname
,1,1,'Admin',sysdate
from (select distinct PartsWarrantyCategoryCode,PartsWarrantyCategoryname from tmpdata.PartsWarrantyTerm);



--配件保修条款
insert into PartsWarrantyTerm
（Id,
WarrantyPolicyId,
PartsWarrantyType,
PartsWarrantyCategoryId,
WarrantyPeriod,
WarrantyMileage,
Capacity,
WorkingHours,
Status,
CreatorId,
CreatorName,
CreateTime)
select s_PartsWarrantyTerm.Nextval,
b.id,1,c.id ,WarrantyPeriod,
WarrantyMileage,Capacity,WorkingHours,1,1,'Admin',sysdate
from tmpdata.PartsWarrantyTerm a inner join WarrantyPolicy b on a.warrantypolicycode=b.code
inner join PartsWarrantyCategory c on a.partswarrantycategorycode=c.code;

--配件保修条款履历
insert into PartsWarrantyTermHistory
（Id,PartsWarrantyTermId,
WarrantyPolicyId,
PartsWarrantyType,
PartsWarrantyCategoryId,
WarrantyPeriod,
WarrantyMileage,
Capacity,
WorkingHours,
Status,
CreatorId,
CreatorName,
CreateTime)
select s_PartsWarrantyTermHistory.Nextval,b.id,
b.WarrantyPolicyId,
b.PartsWarrantyType,
b.PartsWarrantyCategoryId,
b.WarrantyPeriod,
b.WarrantyMileage,
b.Capacity,
b.WorkingHours,
b.Status,
b.CreatorId,
b.CreatorName,
b.CreateTime
from PartsWarrantyTerm b inner join PartsWarrantyCategory a on b.PartsWarrantyCategoryId=a.id
 inner join tmpdata.PartsWarrantyTerm c on c.partswarrantycategorycode=a.code


