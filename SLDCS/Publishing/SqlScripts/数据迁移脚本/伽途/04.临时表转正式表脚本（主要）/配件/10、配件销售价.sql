--配件销售价
insert into PartsSalesPrice
(id,
   branchid,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   sparepartid,
   sparepartcode,
   sparepartname,
   ifclaim,
   salesprice,
   pricetype,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_PartsSalesPrice.Nextval,
         (select id from company where code='2601'),
       b.id,
       b.code,
        b.name,
         v.id,
         v.code,
         v.name,
         1,
         nvl(t.PARTSSALESPRICE, 0),
         1,
         1,
         1,
         'Admin',
         sysdate
    from tmpdata.PartsSalesPricetmp t 
   inner join sparepart v
      on t.sparecode = v.code
      inner join partssalescategory b on t.brandname=b.name;
      
      
  --配件销售价变更履历
  insert into PartsSalesPriceHistory
  (id,
   branchid,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   sparepartid,
   sparepartcode,
   sparepartname,
   ifclaim,
   salesprice,
   pricetype,
   status,
   creatorid,
   creatorname,
   createtime)
   select s_PartsSalesPriceHistory.Nextval,
      b.branchid,
   b.PartsSalesCategoryId,
   b.partssalescategorycode,
   b.partssalescategoryname,
   b.sparepartid,
   b.sparepartcode,
   b.sparepartname,
   b.ifclaim,
   b.salesprice,
   b.pricetype,
   b.status,1,'Admin',sysdate from tmpdata.PartsSalesPricetmp a inner join PartsSalesPrice b on a.sparecode=b.sparepartcode;
   
   
   --配件销售价变更申请
   insert into PartsSalesPriceChange
    (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,status)     
      select s_PartsSalesPriceChange.Nextval,(select id from company where code='2601') as branchid,'PSPC201411070001',
   b.id as partssalescategoryid ,b.code as partssalescategorycode,b.name as partssalescategoryname,1, 1 as creatorid, 'Admin'as creatorname, sysdate as createtime,3 as status
   from (select distinct brandname from  tmpdata.PartsSalesPricetmp) a
   inner join partssalescategory b on b.name=a.brandname;
   
 --PartsSalesPriceChangeDetail  
    insert into PartsSalesPriceChangeDetail
           (id,
            parentid,
            sparepartid,
            sparepartcode,
            sparepartname,
            salesprice,
            pricetype)
   select s_PartsSalesPriceChangeDetail.Nextval,
             d.id,b.id,b.code,b.name,a.PARTSSALESPRICE,1
    from tmpdata.PartsSalesPricetmp a inner join sparepart b on a.SpareCode=b.code
    inner join PartsSalesPriceChange d on 1=1 and d.code='PSPC201411070001';
    
    
   