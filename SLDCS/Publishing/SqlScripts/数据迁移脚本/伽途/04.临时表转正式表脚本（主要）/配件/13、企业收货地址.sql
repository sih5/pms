
--企业收货地址
insert into CompanyAddress
  (id,
   companyid,
   regionid,
   provincename,
   cityname,
   countyname,
   contactperson,
   contactphone,
   detailaddress,
   createtime,
   creatorid,
   creatorname,status)
  select s_CompanyAddress.Nextval,
         (select Id from company where company.code = t.companycode),
         (select t2.id
            from tiledregion t2
           where t.cityname = t2.cityname
             and t.provincename = t2.provincename
             and t.countyname = t2.countyname) RegionId,
         t.provincename,
         t.cityname,
         t.countyname,
         CONTACTPERSON,
         substrb(contactphone,1,50),
         t.detailaddress,
         sysdate,
         1,
         'Admin',1
    from tmpdata.CompanyAddresstmp t where t.countyname is not null;
    
    
    
--企业收货地址
insert into CompanyAddress
  (id,
   companyid,
   regionid,
   provincename,
   cityname,
   countyname,
   contactperson,
   contactphone,
   detailaddress,
   createtime,
   creatorid,
   creatorname,status)
  select s_CompanyAddress.Nextval,
         (select Id from company where company.code = t.companycode),
         (select t2.id
            from tiledregion t2
           where t.cityname = t2.cityname
             and t.provincename = t2.provincename
             and t2.countyname is null) RegionId,
         t.provincename,
         t.cityname,
         t.countyname,
         CONTACTPERSON,
         substrb(contactphone,1,50),
         t.detailaddress,
         sysdate,
         1,
         'Admin',1
    from tmpdata.CompanyAddresstmp t where t.countyname is  null;

