insert into warehouseareamanager
  (Id, WarehouseAreaId, ManagerId,CreatorId, CreatorName, CreateTime)

  select s_warehouseareamanager.nextval,
        b.id as WarehouseAreaId,
        d.id as ManagerId,
        1,
         'admin',
         sysdate
    from tmpdata.warehouseareamanagertmp a left join warehousearea b on a.warehouseareacode=b.code and b.areakind=2 
    left join warehouse c on a.warehousecode=c.code and b.warehouseid=c.id
    left join sdsecurity.personnel d on d.loginid=a.personnnelcode
where d.enterpriseid in (select id from company where code='2601')
    
    
  