--分公司仓库信息

insert into Warehouse
(    id,Code,
    Name,
    Type,
    Email,
    Fax,
    Contact,
    PhoneNumber,
    Address,
    StorageStrategy,
    StorageCenter,BranchId,StorageCompanyId,StorageCompanyType,status)
    select s_Warehouse.Nextval, warehousecode,
    warehousename,
        case type when '总库' then 1 when '分库' then 2 when '虚拟库' then 99 end, --,
    Email,
    Fax,
    Contact,
    PhoneNumber,
    Address,
    1 as StorageStrategy, --定位存储
    case StorageCenter when '北京CDC' then 1 when '山东CDC' then 2 when '广东CDC' then 3 end, --
    (select id from company where code ='2601') ,(select id from company where code ='2601'),1,1
    from tmpdata.Warehousetmp ;
    
    