


insert into warehouseareacategory (ID, CATEGORY)
values (87, 2);

insert into warehouseareacategory (ID, CATEGORY)
values (88, 3);

insert into warehouseareacategory (ID, CATEGORY)
values (89, 1);


--仓库
insert into warehousearea
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
        b.id as  warehouseid,
         null,
         t.warehousecode,
         null,
         89,
         1,
         '伽途库区库位批量处理',
         1,
         1,
         'Admin',
         sysdate
    from (select distinct warehousecode from tmpdata.warehouseareatmp) t left join warehouse b on t.warehousecode=b.code
            
--库区
   insert into warehousearea
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
   b.id as  warehouseid,
   c.id as parentid,
   t.warehouseareacode,
   null,
   decode(AreaCategory, '保管区', 89, '问题区', 87, '检验区', 88),
   1,
   '伽途库区库位批量处理',
   2,
   1,
   'Admin',
   sysdate
    from (select distinct warehouseareacode,warehousecode,AreaCategory from tmpdata.warehouseareatmp) t left join warehouse b on t.warehousecode=b.code
    inner join warehousearea c on t.warehousecode=c.code and c.areakind=1

--库位      
insert into warehousearea
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
         b.id as warehouseid,
         c.id as parentid,
         t.locationcode,
         c.id as toplevelwarehouseareaid,
   decode(AreaCategory, '保管区', 89, '问题区', 87, '检验区', 88),
         1,
         '伽途库区库位批量处理',
         3,
         1,
         'Admin',
         sysdate
      from tmpdata.warehouseareatmp t left join warehouse b on t.warehousecode=b.code
      left join warehousearea c on t.WAREHOUSEAREACODE=c.code and b.id=c.warehouseid and c.AreaKind=2
      
      

  
  