--配件计划价
insert into PartsPlannedPrice
  (id,
   ownercompanyid,
   ownercompanytype,
   partssalescategoryid,
   partssalescategoryname,
   sparepartid,
   plannedprice,
   creatorid,
   creatorname,
   createtime)
select s_partsplannedprice.nextval,
         (select id from company where code='2601') as branchid,
 1,m.id,m.name,
 s.id,
 t.PlannedPrice,
 1,
 'Admin',
 sysdate
  from tmpdata.PartsPlannedPricetmp t
 inner join sparepart s
    on s.code = t.SpareCode
       inner join partssalescategory m on m.name=t.brandname;

--计划价申请单
     insert into PlannedPriceApp
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,RecordStatus,PlannedExecutionTime,ActualExecutionTime)
   select s_PlannedPriceApp.Nextval,(select id from company where code='2601') as ownercompanyid,
   (select code from company where code='2601') as ownercompanycode,
   (select name from company where code='2601') as ownercompanyname,
   b.id as partssalescategoryid ,b.name as partssalescategoryname, 'PPA201411070001',  0,  0, 0,  3, 1,
   'Admin', sysdate,3,sysdate,sysdate
   from (select distinct brandname from tmpdata.PartsPlannedPricetmp) a
   inner join partssalescategory b on b.name=a.brandname;
       
   --计划价申请单清单
   insert into PlannedPriceAppDetail
    (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             d.id,b.id,b.code,b.name,a.PlannedPrice,0,0,0
    from tmpdata.PartsPlannedPricetmp a inner join sparepart b on a.SpareCode=b.code
    inner join PlannedPriceApp d on 1=1 and d.code='PPA201411070001';
    
    
