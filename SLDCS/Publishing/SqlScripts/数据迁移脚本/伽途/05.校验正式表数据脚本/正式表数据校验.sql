--供应商不存在企业信息
select * from PartsSupplier where code not in (select code from company);
--供应商与分公司不存在关系
select * from PartsSupplier where id not in (select SupplierId  from BranchSupplierRelation);
--配件与供应商不存在关系
select * from sparepart where id not in (select partid  from PartsSupplierRelation);
--配件无采购价
select * from sparepart where id not in (select partid  from PartsPurchasePricing);
--配件无销售价
select * from sparepart where id not in (select    sparepartid  from PartsSalesPrice);
--配件无计划价
select * from sparepart where id not in (select    sparepartid  from PartsPlannedPrice);
--配件无零售指导价
select * from sparepart where id not in (select    sparepartid  from PartsRetailGuidePrice);
--配件无营销信息
select * from sparepart where id not in (select partid  from partsbranch);