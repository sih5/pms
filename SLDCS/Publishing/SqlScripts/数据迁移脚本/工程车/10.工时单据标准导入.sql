
create table tmpdata.LaborHour140919 (brandcode varchar2 (50),brandname varchar2(100),Code varchar2(50),name varchar2(100),productlinecode varchar2(50),productlinename varchar2(100),productlinetype varchar2(100),labhour number(9,2),remark varchar2 (200) )

create table tmpdata.LaborHourUnitPriceGrade140919 as select * from LaborHourUnitPriceGrade where rownum<1;

insert into tmpdata.LaborHourUnitPriceGrade140919
  (id,
   branchid,
   code,
   name,
   status,
   remark,
   creatorid,
   creatorname,
   createtime)
  select s_LaborHourUnitPriceGrade.Nextval,
         '',
         t.code,
         t.name,
         1,
         '',
         1,
         'Admin',
         sysdate
    from (select distinct v.code, v.name from tmpdata.LaborHour140919 v) t;

insert into LaborHourUnitPriceGrade select * from  tmpdata.LaborHourUnitPriceGrade140919;

create table tmpdata.LaborHourUnitPriceRate140919 as select * from LaborHourUnitPriceRate where rownum<1;

insert into tmpdata.LaborHourUnitPriceRate140919
  (id,
   laborhourunitpricegradeid,
   code,
   serviceproductlineid,
   productlinetype,
   price,
   status,
   remark,
   creatorid,
   creatorname,
   createtime)
  select s_LaborHourUnitPriceRate.Nextval,
   (select id
      from tmpdata.LaborHourUnitPriceGrade140919 v
     where v.code = t.code),
   'WHP1000' || decode(length(rownum), 1, '0' || rownum, 2, rownum) CODE,
   (SELECT ID FROM serviceproductlineview where code = t.productlinecode),
   decode(t.productlinetype, '服务产品线', 1, 2),
   t.labhour,
   REMARK,
   1,
   'Admin',
   sysdate
    from tmpdata.LaborHour140919 t
insert into LaborHourUnitPriceRate select * from tmpdata.LaborHourUnitPriceRate140919;
