create table tmpdata.personnelbrandrel140919 (brandname varchar2(100),personnelid varchar2(100),personnelname varchar2(100))
create table tmpdata.personnelbrandrel140919_1 as select * from PersonSalesCenterLink where rownum<1;

insert into tmpdata.personnelbrandrel140919_1
  (id,
   companyid,
   personid, 
   partssalescategoryid,
   persontype,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_PersonSalesCenterLink.Nextval,8481,
         (select id
            from gcsecurity.personnel
           where loginid = t.personnelid
             and name = t.personnelname),
         (select id from partssalescategory where name = t.brandname),1,
         1,
         1,
         'Admin',
         sysdate
    from tmpdata.personnelbrandrel140919 t
     where  exists(select id
            from gcsecurity.personnel
           where loginid = t.personnelid
             and name = t.personnelname);
             
insert into PersonSalesCenterLink select * from  tmpdata.personnelbrandrel140919_1;


select /*--s_PersonSalesCenterLink.Nextval,,
         (select id
            from gcsecurity.personnel
           where loginid = t.personnelid
             and name = t.personnelname),
         (select id from partssalescategory where name = t.brandname),1,
         1,
         1,
         'Admin',
         sysdate,*/t.personnelname,t.brandname
    from tmpdata.personnelbrandrel140919 t
    where not exists(select id
            from gcsecurity.personnel
           where loginid = t.personnelid
             and name = t.personnelname);
             
