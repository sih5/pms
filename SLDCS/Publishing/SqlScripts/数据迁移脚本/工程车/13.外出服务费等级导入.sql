create table tmpdata.ServiceTrip140919
 (brandname varchar2(100),code varchar2(50),name varchar2(100),RegionDescription varchar2(200),productlinecode varchar2(50),productlinename varchar2(100),productlinetype varchar2(50)
 ,ServiceTripType varchar2(50),maxmile number(9,2),minmile number (9,2),FixedAmount number(9,4),IsSelfCar varchar2(50),SubsidyPrice number(9,4),PricePrice number(9,4),remark varchar2 (200) )

create table tmpdata.ServiceTripPriceGrade140919
 as select * from ServiceTripPriceGrade where rownum<1;
 insert into tmpdata.ServiceTripPriceGrade140919
   (id,
    branchid,
    code,
    name,
    regiondescription,
    status,
    remark,
    creatorid,
    creatorname,
    createtime)
   select s_ServiceTripPriceGrade.Nextval,8481,
          t.code,
          t.name,
          t.regiondescription,
          1,
          '',
          1,
          'Admin',
          sysdate
     from (select distinct v.code, v.name, v.regiondescription
             from tmpdata.ServiceTrip140919 v) t
insert into ServiceTripPriceGrade 
select * from  tmpdata.ServiceTripPriceGrade140919;

create table tmpdata.ServiceTripPriceRate140919 as select * from ServiceTripPriceRate where rownuM<1;
insert into tmpdata.ServiceTripPriceRate140919
  (id,
   servicetrippricegradeid,
   serviceproductlineid,
   productlinetype,
   code,
   servicetriptype,
   maxmileage,
   minmileage,
   fixedamount,
   isselfcar,
   subsidyprice,
   price,
   status,
   remark,
   creatorid,
   creatorname,
   createtime)
  select s_ServiceTripPriceRate.Nextval,
         (select id
            from tmpdata.ServiceTripPriceGrade140919 a
           where a.code = t.code),
         (select b.ProductLineId
            from serviceproductlineview b
           where b.ProductLineCode = t.productlinecode),
         decode(t.productlinetype, '服务产品线', 1, 2),
         s_ServiceTripPriceRate.Currval,
         decode(t.servicetriptype, '白天', 1, '夜间', 2),
         t.maxmile,
         t.minmile,
         t.fixedamount,
         decode (t.isselfcar,'是',1,'否',0),
         t.subsidyprice,
         t.priceprice,
         1,
         t.remark,
         1,
         'Admin',
         sysdate
    from tmpdata.ServiceTrip140919 t;

insert into
 ServiceTripPriceRate select * from tmpdata.ServiceTripPriceRate140919;
