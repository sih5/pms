create table tmpdata.WPolicyNServProdLine140922(WarrantyPolicycode varchar2(50),WarrantyPolicyname varchar2(100),productlinecode varchar2(50), productlinename varchar2(100),productlinetype varchar2(100),salesstarttime date ,saleendtime date,remark varchar2��200))
create table tmpdata.WPolicyNServProdLine140922_1 as select * from warrantypolicynservprodline where rownum<1;
insert into tmpdata.wpolicynservprodline140922_1
  (id,
   warrantypolicyid,
   serviceproductlineid,
   validationdate,
   expiredate,
   productlinetype,
   remark,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_warrantypolicynservprodline.nextval,
         (select id from warrantypolicy where code = t.warrantypolicycode),
         (select b.ProductLineId
            from serviceproductlineview b
           where b.ProductLineCode = t.productlinecode),
         t.salesstarttime,
         t.saleendtime,
         (select b.ProductLineType
            from serviceproductlineview b
           where b.ProductLineCode = t.productlinecode),
         t.remark,
         1,
         1,
         'Admin',
         sysdate
    from tmpdata.WPolicyNServProdLine140922 t;
insert into warrantypolicynservprodline select * from  tmpdata.wpolicynservprodline140922_1;

create table tmpdata.warrpolnservprodlinehis140922  as select * from warrpolnservprodlinehistory where rownum<1;
insert into tmpdata.warrpolnservprodlinehis140922
  (id,
   warrantypolicynservprodlineid,
   warrantypolicyid,
   serviceproductlineid,
   validationdate,
   expiredate,
   productlinetype,
   remark,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_warrpolnservprodlinehistory.nextval,
         id,
         warrantypolicyid,
         serviceproductlineid,
         validationdate,
         expiredate,
         productlinetype,
         remark,
         status,
         1,
         'Admin',
         sysdate
    from tmpdata.wpolicynservprodline140922_1;
insert into warrpolnservprodlinehistory select * from tmpdata.warrpolnservprodlinehis140922 ;
