create table tmpdata.ServiceProdLineProduct140922(mainbrandcode varchar2(50),mainbrandname varchar2(100),subbrandcode varchar2(50),subbrandname varchar2(100),productlinecode varchar2(50),productlinename varchar2(100),serviceproductlinecode varchar2(50),serviceproductlinename varchar2(100))

--整车品牌编号,整车品牌名称,子品牌编号,子品牌名称,平台编号,平台名称,产品线编号,产品线名称,服务产品线编号,服务产品线名称
/*
select *
  from serviceprodlineproduct t
 where not exists (select 1
          from tmpdata.ServiceProdLineProduct140922 v
         where t.vehiclebrandcode = v.mainbrandcode
           and t.vehiclebrandname = v.mainbrandname
           and t.subbrandcode = v.subbrandcode
           and t.subbrandname = v.subbrandname
           and t.vehicleproductline = v.productlinecode
           and t.vehicleproductname = v.productlinename)
*/
create table tmpdata.serviceprodlineproduct140922_1 as 
select t.id,
       (select v.serviceproductlinecode
          from tmpdata.ServiceProdLineProduct140922 v
         where t.vehiclebrandcode = v.mainbrandcode
           and t.vehiclebrandname = v.mainbrandname
           and t.subbrandcode = v.subbrandcode
           and t.subbrandname = v.subbrandname
           and t.vehicleproductline = v.productlinecode
           and t.vehicleproductname = v.productlinename) serviceproductlinecode,
       '    ' as serviceproductlineid,
       '    ' as serviceproductlinename,
       '    ' as brandid,
       '    ' as brandname,
       '    ' as brandcode
  from serviceprodlineproduct t
 where exists (select 1
          from tmpdata.ServiceProdLineProduct140922 v
         where t.vehiclebrandcode = v.mainbrandcode
           and t.vehiclebrandname = v.mainbrandname
           and t.subbrandcode = v.subbrandcode
           and t.subbrandname = v.subbrandname
           and t.vehicleproductline = v.productlinecode
           and t.vehicleproductname = v.productlinename);
           
           update tmpdata.serviceprodlineproduct140922_1 t
              set t.serviceproductlineid  =
                  (select productlineid
                     from serviceproductlineview v
                    where v.ProductLineCode = t.serviceproductlinecode),
                  t.serviceproductlinename =
                  (select v.ProductLineName
                     from serviceproductlineview v
                    where v.ProductLineCode = t.serviceproductlinecode),
                    t.brandid=(select v.Partssalescategoryid from serviceproductlineview v where v.ProductLineCode=t.serviceproductlinecode);
                    
                    update tmpdata.serviceprodlineproduct140922_1 t
                       set t.brandname =
                           (select v.name
                              from partssalescategory v
                             where v.id = t.brandid),
                           t.brandcode =
                           (select v.code
                              from partssalescategory v
                             where v.id = t.brandid);

update serviceprodlineproduct t
   set t.serviceproductlineid  =
       (select v.serviceproductlineid
          from tmpdata.serviceprodlineproduct140922_1 v
         where v.id = t.id),
       t.serviceproductlinecode =
       (select v.serviceproductlinecode
          from tmpdata.serviceprodlineproduct140922_1 v
         where v.id = t.id),
       t.serviceproductlinename =
       (select v.serviceproductlinename
          from tmpdata.serviceprodlineproduct140922_1 v
         where v.id = t.id),
       t.partssalescategoryid  =
       (select v.brandid
          from tmpdata.serviceprodlineproduct140922_1 v
         where v.id = t.id),
       t.salescategorycode     =
       (select v.brandcode
          from tmpdata.serviceprodlineproduct140922_1 v
         where v.id = t.id),
       t.salescategoryname     =
       (select v.brandname
          from tmpdata.serviceprodlineproduct140922_1 v
         where v.id = t.id)
 where exists (select 1
          from tmpdata.serviceprodlineproduct140922_1 v
         where v.id = t.id)
 
