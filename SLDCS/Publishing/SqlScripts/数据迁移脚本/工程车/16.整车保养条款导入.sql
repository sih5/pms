
create table tmpdata.VehicleMainteTerm140922
(WarrantyPolicycode varchar2(50),WarrantyPolicyname varchar2(100),code varchar2(50),name varchar2(100),MainteType varchar2(10),BillingMethod varchar2(10),
WorkingHours number(9),Capacity number(9),MaxMileage number(9),minmileage number(9),DaysUpperLimit number(9), DayslowerLimit number(9),LaborCost number(9,4),MaterialCost number(9,4) ,OtherCost number(9,4) ,remark varchar2(200))

create table tmpdata.VehicleMainteTerm140922_1 as 
select * from VehicleMainteTerm where rownum<1;

insert into tmpdata.VehicleMainteTerm140922_1
  (id,
   code,
   name,
   warrantypolicyid,
   maintetype,
   workinghours,
   capacity,
   maxmileage,
   minmileage,
   daysupperlimit,
   dayslowerlimit,
   billingmethod,
   laborcost,
   materialcost,
   othercost,
   status,
   remark,
   creatorid,
   creatorname,
   createtime)
  select s_VehicleMainteTerm.Nextval,
         t.code,
         t.name,
         (select id
            from warrantypolicy v
           where t.warrantypolicycode = v.code),
         decode(MainteType, '强制保养', 1, '定期保养', 2),
         nvl（t.workinghours,0),
         nvl(t.capacity, 0),
         t.maxmileage,
         t.minmileage,
         t.daysupperlimit,
         t.dayslowerlimit,
         decode(BillingMethod, '固定模式', 1, '按实际发生收取', 2),
         t.laborcost,
         t.materialcost,
         t.othercost,
         1,
         t.remark,
         1,
         'Admin',
         sysdate
    from tmpdata.VehicleMainteTerm140922 t;
insert into VehicleMainteTerm select * from  tmpdata.VehicleMainteTerm140922_1;

create table tmpdata.VehicleMainteTermHistory140922 as select * from VehicleMainteTermHistory where rownum<1
insert into tmpdata.VehicleMainteTermHistory140922
  (id,
   vehiclemaintetermid,
   code,
   name,
   warrantypolicyid,
   maintetype,
   workinghours,
   --capacity,
   maxmileage,
   minmileage,
   daysupperlimit,
   dayslowerlimit,
   billingmethod,
   laborcost,
   materialcost,
   othercost,
   status,
   remark,
   creatorid,
   creatorname,
   createtime)
  select s_VehicleMainteTermHistory.Nextval,
         id,
         code,
         name,
         warrantypolicyid,
         maintetype,
         workinghours,
        -- capacity,
         maxmileage,
         minmileage,
         daysupperlimit,
         dayslowerlimit,
         billingmethod,
         laborcost,
         materialcost,
         othercost,
         status,
         remark,
         1,
         'Admin',
         sysdate
    from tmpdata.VehicleMainteTerm140922_1;

insert into VehicleMainteTermHistory select * from tmpdata.VehicleMainteTermHistory140922;
