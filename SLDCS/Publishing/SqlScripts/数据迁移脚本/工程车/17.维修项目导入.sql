序号,零部件编码,工时代码,工时代码描述,维修资质,产品线编号,产品线名称,产品线类型,工时定额
create table tmpdata.repairitem140922 (repairitemcategorycode varchar2(20),code varchar2(50),name varchar2(600),RepairQualificationGrade varchar2 (50),productlinecode varchar2 (50),productlinename varchar2(100),productlinetype varchar2(100),DefaultLaborHour number(9,2))

create table tmpdata.repairitem140922_1 as select * from repairitem 
 where rownum<1;
 insert into tmpdata.repairitem140922_1
   (id,
    code,
    name,
    repairitemcategoryid,
    jobtype,
    repairqualificationgrade,
    status,
    remark,
    creatorid,
    creatorname,
    createtime)
select --s_repairitem.nextval, 
v.CODE,v.NAME,(select distinct id
    from gcdcs.repairitemcategory t
   where t.code = v.repairitemcategorycode
     and t.status = 1
     and t.layernodetypeid = 3),1,1,1,'',1,'Admin',sysdate from
(select distinct code, name,repairitemcategorycode   
          from tmpdata.repairitem140922) v
where  exists (select 1
          from gcdcs.Repairitemcategory a
         where v.repairitemcategorycode = a.code
           and a.status = 1
           and a.layernodetypeid =3)                   
   and not  exists (select distinct b.code, b.name
          from gcdcs.Repairitemcategory t
         inner join tmpdata.repairitem140922 b
            on b.repairitemcategorycode = t.code
         where t.code in (select code
                            from gcdcs.Repairitemcategory t
                           where t.status = 1
                             and t.layernodetypeid = 3
                           group by code
                          having count(code) > 1)
           and b.code = v.code
           and b.name = v.name);
           

insert into tmpdata.repairitem140922_1
  (id,
   code,
   name,
   repairitemcategoryid,
   jobtype,
   repairqualificationgrade,
   status,
   remark,
   creatorid,
   creatorname,
   createtime)
  select s_repairitem.nextval,
         v.CODE,
         v.NAME,
          (select distinct T.id
    from gcdcs.Repairitemcategory t
    inner join  gcdcs.Repairitemcategory a on a.id=t.parentid inner join  gcdcs.Repairitemcategory b on b.id=a.parentid
   where t.code = v.repairitemcategorycode
     and t.status = 1
     and t.layernodetypeid = 3 and b.code='K1'),
         1,
         1,
         1,
         '',
         1,
         'Admin',
         sysdate
    from (select distinct code, name, repairitemcategorycode
            from tmpdata.repairitem140922) v
   where exists (select 1
            from gcdcs.Repairitemcategory a
           where v.repairitemcategorycode = a.code
             and a.status = 1
             and a.layernodetypeid = 3)
     and exists (select distinct b.code, b.name
            from gcdcs.Repairitemcategory t
           inner join tmpdata.repairitem140922 b
              on b.repairitemcategorycode = t.code
           where t.code in (select code
                              from gcdcs.Repairitemcategory t
                             where t.status = 1
                               and t.layernodetypeid = 3
                             group by code
                            having count(code) > 1)
             and b.code = v.code
             and b.name = v.name);
         insert into repairitem select * from   tmpdata.repairitem140922_1;
         
         
update  repairitem t  set t.name= replace(name,CHR(9),'')


update  repairitem t  set t.name= replace(name,CHR(10),'')

create table tmpdata.RItemAffiServProdLine140922 as select * from RepairItemAffiServProdLine where rownum<1;
insert into tmpdata.RItemAffiServProdLine140922
  (id,
   branchid,
   repairitemid,
   serviceproductlineid,
   productlinetype,
   remark,
   status,
   creatorid,
   creatorname,
   createtime,
   defaultlaborhour,
   PartsSalesCategoryId)
  select s_RepairItemAffiServProdLine.Nextval,
         8481,
         (select id from repairitem v where v.code = t.code),
         (select productlineid
            from serviceproductlineview a
           where a.ProductLineCode = t.productlinecode),
         (select productlinetype
            from serviceproductlineview a
           where a.ProductLineCode = t.productlinecode),
         '',
         1,
         1,
         'Admin',
         sysdate,
         t.defaultlaborhour,
         (select a.Partssalescategoryid
            from serviceproductlineview a
           where a.ProductLineCode = t.productlinecode)
    from tmpdata.repairitem140922 t
   where exists (select 1 from repairitem v where v.code = t.code);
   insert into RepairItemAffiServProdLine select * from tmpdata.RItemAffiServProdLine140922;
   
   
  create table tmpdata.RepairItemBrandRelation140924 as select * from RepairItemBrandRelation;
 insert into tmpdata.RepairItemBrandRelation140924
   (id,
    branchid,
    repairitemid,
    repairitemcode,
    repairitemname,
    partssalescategoryid,
    repairqualificationgrade,
    creatorid,
    creatorname,
    createtime,status)
   select s_RepairItemBrandRelation.Nextval,
          8481,
          (select id from repairitem v where v.code = t.code),
          t.code,
          t.name,
          (select a.Partssalescategoryid
            from serviceproductlineview a
           where a.ProductLineCode = t.productlinecode), 1, 1, 'Admin', sysdate,1 from tmpdata.repairitem140922 t
           where exists (select 1 from repairitem v where v.code = t.code);
           
           insert into RepairItemBrandRelation select * from  tmpdata.RepairItemBrandRelation140924;
   --重复数据处理
   /*
   select* from repairitem where code in(
select code from repairitem v group by code having count(code)>1)
create table tmpdata.repairitem140924save(code varchar2 (50),name varchar2(100));

create table tmpdata.repairitem140924 as 
select * from repairitem v where code in(
select code from repairitem v group by code having count(code)>1)
and not exists(select 1 from tmpdata.repairitem140924save t where t.code=v.code and t.name=v.name)

delete from  repairitem v where code in(
select code from repairitem v group by code having count(code)>1)
and not exists(select 1 from tmpdata.repairitem140924save t where t.code=v.code and t.name=v.name)
*/
