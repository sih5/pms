create table tmpdata.department140923 (code varchar2 (50),name varchar2(100))
create table tmpdata.DepartmentInformation140923 as select * from DepartmentInformation where rownum<1;

insert into tmpdata.departmentinformation140923
  (id,
   branchid,
   branchname,
   code,
   name,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_DepartmentInformation.Nextval,
         8481,
         (select name from branch where id = 8481),
         t.code,
         t.name,
         1,
         1,
         'Admin',
         sysdate
    from tmpdata.department140923 t;
insert into departmentinformation select * from tmpdata.departmentinformation140923;
