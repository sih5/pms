create table tmpdata.dlkwarehousearea140923(
warehousecode varchar2(100),
warehousename varchar2(100),
storecompanycode varchar2(100),
storecompanyname varchar2(100),
storecompanytype varchar2(100),
warehouseareacode varchar2(100),
locationcode varchar2(100),
AreaCategory varchar2(10),
partcode varchar2(100),
partname varchar2(100),
userableqty number(9),partcodenew varchar2(100));

create table tmpdata.dlkwarehouse140923 as select * from warehouse where rownum<1;
insert into tmpdata.dlkwarehouse140923
  (ID,
   CODE,
   NAME,
   TYPE,
   STATUS,
   STORAGESTRATEGY,
   BRANCHID,
   STORAGECOMPANYID,
   STORAGECOMPANYTYPE,
   WMSINTERFACE,
   creatorid,
   creatorname,
   createtime)
  select S_warehouse.nextval,
         t.warehousecode,
         t.warehousename,
         2,
         1,
         1,
         8481,
         (select id from company where code = t.newcode),
         3,
         0,
         1,
         'Admin',
         sysdate
    from (select distinct warehousecode,
                          warehousename,
                          v.newcode,
                          (select name
                             from company
                            where company.code = v.newcode),
                          storecompanytype
            from tmpdata.dlkwarehousearea140923 t
           inner join tmpdata.agentnewoldrel v
              on t.storecompanycode = v.OLDCODE
           where AreaCategory in ('保管区', '问题区', '检验区')) t
   where exists (select id from company where code = t.newcode);
insert into warehouse select * from tmpdata.dlkwarehouse140923;
select * from tmpdata.dlkwarehouse140620 for update
--delete from warehouse where exists(select 1 from tmpdata.dlkwarehouse140519 t where t.id=warehouse.id)
--delete from dlkwarehousearea140505
create table tmpdata.dlkwarehousearea140923_1 as select * from warehousearea where rownum<1;
insert into tmpdata.
dlkwarehousearea140923_1
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
         (select id
            from warehouse
           where warehouse.code = t.warehousecode and branchid=8481) warehouseid,
         null,
         t.warehousecode,
         null,
         89,
         1,
         '南北方代理库库区库位批量处理',
         1,
         1,
         'Admin',
         sysdate
    from (select distinct warehousecode
            from tmpdata. dlkwarehousearea140923 t
            inner join tmpdata.agentnewoldrel v
              on t.storecompanycode = v.OLDCODE
             where exists(select id from company where code =v.newcode) and AreaCategory in('保管区','问题区','检验区')) t 
            
            /*select * 
            from tmpdata. dlkwarehousearea140620 where warehousecode='SY'
            
            UPDATE tmpdata. dlkwarehousearea140620 SET warehousecode='SYOMK' WHERE warehousename='沈阳欧马可仓库'
              UPDATE tmpdata. dlkwarehousearea140620 SET warehousecode='SYFJ' WHERE warehousename='沈阳风景仓库'
              UPDATE tmpdata. dlkwarehousearea140620 SET warehousecode='SYPK' WHERE warehousename='沈阳皮卡仓库'
              UPDATE tmpdata. dlkwarehousearea140620 SET warehousecode='SYAL' WHERE warehousename='沈阳奥铃仓库'
            select  * from dcs.warehouse where branchid=3007 and code ='SY' for update
            UPDATE WAREHOUSE SET CODE='SYFJ'WHERE branchid=3007 and code ='SY'  AND NAME='沈阳风景仓库'
            select distinct code from dcs.warehouse where branchid=3007 group by code having count(code)>*/1
            
            
           
/*select *from tmpdata.dlkwarehousearea140605_1
update tmpdata.dlkwarehousearea140605_1 set areacategoryid=null*/
insert into tmpdata. dlkwarehousearea140923_1
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
         (select id
            from warehouse
           where warehouse.code = t.warehousecode
             and branchid = 8481) warehouseid,
         (select id
            from tmpdata.dlkwarehousearea140923_1 v
           where v.code = t.warehousecode),
         t.warehouseareacode,
         null,
         decode(AreaCategory ,'保管区',89,'问题区',87,'检验区',88),
         1,
         '南北方代理库库区库位批量处理',
         2,
         1,
         'Admin',
         sysdate
    from (select distinct warehousecode, warehouseareacode,areacategory
            from tmpdata. dlkwarehousearea140923 t
             inner join tmpdata.agentnewoldrel v
              on t.storecompanycode = v.OLDCODE
             where exists(select id from company where code = v.newcode) and AreaCategory in('保管区','问题区','检验区')) t

insert into tmpdata. dlkwarehousearea140923_1
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
         (select id
            from warehouse
           where warehouse.code = t.warehousecode
             and branchid =8481) warehouseid,
         (select id
            from tmpdata.dlkwarehousearea140923_1 v
           where v.code = t.warehouseareacode
             and v.warehouseid = warehouse.id
             and warehouse.branchid = 8481 and v.areacategoryid=decode(AreaCategory, '保管区', 89, '问题区', 87, '检验区', 88)),
         t.locationcode,
         (select id
            from tmpdata.dlkwarehousearea140923_1 v
           where v.code = t.warehouseareacode
             and v.warehouseid = warehouse.id
             and warehouse.branchid = 8481 and v.areacategoryid=decode(AreaCategory, '保管区', 89, '问题区', 87, '检验区', 88)),
         decode(AreaCategory, '保管区', 89, '问题区', 87, '检验区', 88),
         1,
         '南北方代理库库区库位批量处理',
         3,
         1,
         'Admin',
         sysdate
    from (select distinct warehousecode,
                          warehouseareacode,
                          locationcode,
                          AreaCategory
            from tmpdata. dlkwarehousearea140923 t
             inner join tmpdata.agentnewoldrel v
              on t.storecompanycode = v.OLDCODE
           where exists (select id from company where code = v.newcode)
             and AreaCategory in ('保管区', '问题区', '检验区')) t
   inner join warehouse
      on warehouse.code = t.warehousecode
     and warehouse.branchid =8481;
    --27927
    
    /*select * from  tmpdata. dlkwarehousearea140620 where warehousecode='GZOMKCK' and warehouseareacode='2B'
    
    delete from tmpdata. dlkwarehousearea140620  where locationcode is null
    */
    
    --402656
  insert into   warehousearea select * from tmpdata. dlkwarehousearea140923_1;
  /*
   create table tmpdata.PartsStock140608  as select * from dcs.partsstock where rownum<1;
   insert into tmpdata.PartsStock140608
  (id,
   warehouseid,
   storagecompanyid,
   storagecompanytype,
   branchid,
   warehouseareaid,
   warehouseareacategoryid,
   partid,
   quantity,
   remark,
   creatorid,
   creatorname,
   createtime)
    select S_PartsStock.Nextval,
         (select id
            from warehouse v
           where v.code = t.warehousecode
             and branchid = 3007),
         3007,
         1,
         3007,
         (select id
            from tmpdata.dlkwarehousearea140605_1 s
           where s.warehouseid = warehouse.id
             and rownum=1 and s.areacategoryid=89 and areakind=3),
         89,
        partsbranch.partid ,
         200,
         '营销库存批量处理',
         1,
         'Admin',
         sysdate
          from （select distinct warehousecode
            from tmpdata. dlkwarehousearea140605） t
            inner join warehouse on warehouse.code=t.warehousecode and warehouse.status<>99   and branchid=3007
            inner join dcs.partsbranch on warehouse.branchid=partsbranch.branchid where warehousecode<>'HLJALCK'
            insert into PartsStock select * from   tmpdata.PartsStock140608 ;
-- select * from tmpdata.PartsStock140608
  \*
  create table tmpdata.PartsStock140519  as select * from dcs.partsstock where rownum<1;

insert into tmpdata.PartsStock140519
  (id,
   warehouseid,
   storagecompanyid,
   storagecompanytype,
   branchid,
   warehouseareaid,
   warehouseareacategoryid,
   partid,
   quantity,
   remark,
   creatorid,
   creatorname,
   createtime)
  select S_PartsStock.Nextval,
         (select id
            from warehouse v
           where v.code = t.warehousecode
             and branchid = (select id from dcs.branch where code = '2450')),
         (select id from company where code = t.storecompanycode),
         3,
         (select id from dcs.branch where code = '2450'),
         (select id
            from tmpdata.dlkwarehousearea140519_1 s
           where s.warehouseid = warehouse.id
             and s.code = t.locationcode),
         89,
         (SELECT ID FROM SPAREPART WHERE SPAREPART.CODE = T.PARTCODENEW),
         t.userableqty,
         '欧曼保外代理库库区库位批量处理',
         1,
         'Admin',
         sysdate
    from tmpdata. dlkwarehousearea140519 t
   inner join warehouse
      on warehouse.code = t.warehousecode
     and warehouse.branchid =
         (select id from dcs.branch where code = '2450')
   where t.PARTCODENEW is not null
     and exists (select id
            from tmpdata.dlkwarehousearea140519_1 s
           where s.warehouseid = warehouse.id
             and s.code = t.locationcode)
             and exists(select 1 from SPAREPART WHERE SPAREPART.CODE = T.PARTCODENEW);
             --12613
insert into PartsStock select * from tmpdata.PartsStock140519;

select * from tmpdata. dlkwarehousearea140505 t where  exists(select 1 from  dcs.sparepart v where t.partcode=v.code)
dlkwarehousearea140505_1

select * from  tmpdata.dlkwarehousearea140505 T where code='H4812060005A0A0943A';*\
\*

UPDATE tmpdata.dlkwarehousearea140505 T
   SET PARTCODEnew =
       (SELECT PARTCODEnew
          FROM tmpdata.Dlkwarehousearea140505_Update V
         WHERE T.WAREHOUSECODE = V.WAREHOUSECODE
           AND T.WAREHOUSEAREACODE = V.WAREHOUSEAREACODE
           AND T.LOCATIONCODE = V.LOCATIONCODE
           AND T.PARTCODE = V.PARTCODE)
OM567 1A 1A0303503 H4811050001A0L2036A
SELECT PARTCODE
          FROM tmpdata.Dlkwarehousearea140505_Update
         
         select *
           from tmpdata.dlkwarehousearea140505_1
          where code in (select code
                           from tmpdata.dlkwarehousearea140505_1 s
                          group by warehouseid, code
                         having count(code) > 1)
            and warehouseid = 614
            for update
*\


*/
