Create table tmpdata.toldpartwarehousearea140923
(
  warehousecode varchar2(20),
  warehousename varchar2(40),
  filialecode varchar2(20),
  filialename varchar2(50),
  areacode varchar2(100),
  locationcode varchar2(100),
  type varchar2(20),
  StorageCategory varchar2(20)
)
update tmpdata.toldpartwarehousearea140923 set toldpartwarehousearea140923.filialecode=2230,toldpartwarehousearea140923.filialename='北汽福田汽车股份有限公司工程车事业部';
select * from branch where id=8481
--drop table tmpdata.UsedPartsWarehouseArea140430

create table tmpdata.UsedPartsWarehouseArea140923 as 
select * from dcs.UsedPartsWarehouseArea where rownum<1;

/*insert into tmpdata.UsedPartsWarehouseArea140923
  (Id,
   UsedPartsWarehouseId,
   ParentId,
   TopLevelUsedPartsWhseAreaId,
   Code,
   IfDefaultStoragePosition,
   StorageCategory,
   Status,
   StorageAreaType,
   CreatorName,
   CreateTime)
  select s_Usedpartswarehousearea.nextval as id,
         Usedpartswarehouse.id as UsedPartsWarehouseId,
         null,
         null,
         temp.warehousecode code,
         0,
         1 as StorageCategory,
         1 as status,
         1 as StorageAreaType,
         'admin' as CreatorName,
         sysdate as CreateTime

    from (select distinct warehousecode,filialename
            from tmpdata.toldpartwarehousearea140923) temp
   inner join dcs.Usedpartswarehouse
      on UsedPartsWarehouse.code = temp.warehousecode
   inner join dcs.branch
     on branch.id = UsedPartsWarehouse.Branchid
     and branch.name = temp.filialename;*/

insert into tmpdata.UsedPartsWarehouseArea140923
  (Id,
   UsedPartsWarehouseId,
   ParentId,
   TopLevelUsedPartsWhseAreaId,
   Code,
   IfDefaultStoragePosition,
   StorageCategory,
   Status,
   StorageAreaType,
   CreatorName,
   CreateTime)
  select s_Usedpartswarehousearea.nextval as id,
         Usedpartswarehouse.id as UsedPartsWarehouseId,
         (select id
            from usedpartswarehousearea
           where usedpartswarehousearea.Usedpartswarehouseid =
                 Usedpartswarehouse.id
             and usedpartswarehousearea. code= temp.warehousecode),
        null,
         temp.areacode code,
         0,
         1 as StorageCategory,
         1 as status,
         2 as StorageAreaType,
         'admin' as CreatorName,
         sysdate as CreateTime  
    from (select distinct warehousecode, areacode, filialename
            from tmpdata.toldpartwarehousearea140923) temp
   inner join Usedpartswarehouse
      on UsedPartsWarehouse.code = temp.warehousecode
   inner join branch
      on branch.id = UsedPartsWarehouse.Branchid
     and branch.name = temp.filialename;

insert into tmpdata.UsedPartsWarehouseArea140923
  (Id,
   UsedPartsWarehouseId,
   ParentId,
   TopLevelUsedPartsWhseAreaId,
   Code,
   IfDefaultStoragePosition,
   StorageCategory,
   Status,
   StorageAreaType,
   CreatorName,
   CreateTime)
  select s_Usedpartswarehousearea.nextval as id,
         Usedpartswarehouse.id as UsedPartsWarehouseId,
         (select id
            from tmpdata.UsedPartsWarehouseArea140923
           where UsedPartsWarehouseArea140923.Usedpartswarehouseid =
                 Usedpartswarehouse.id
             and UsedPartsWarehouseArea140923.Code = temp.areacode and UsedPartsWarehouseArea140923.StorageAreaType=2) as ParentId,
         (select id
            from tmpdata.UsedPartsWarehouseArea140923
           where UsedPartsWarehouseArea140923.Usedpartswarehouseid =
                 Usedpartswarehouse.id
             and UsedPartsWarehouseArea140923.Code = temp.areacode and UsedPartsWarehouseArea140923.StorageAreaType=2) as TopLevelUsedPartsWhseAreaId,
         temp.locationcode as Code,
         0 as IfDefaultStoragePosition,
         1 as StorageCategory,
         1 as status,
         3 as StorageAreaType,
         'admin' as CreatorName,
         sysdate as CreateTime
    from tmpdata.toldpartwarehousearea140923 temp
   inner join Usedpartswarehouse
      on UsedPartsWarehouse.code = temp.warehousecode
   inner join branch
      on branch.id = UsedPartsWarehouse.Branchid
     and temp.filialename = branch.name
 /*  where temp.type = '库位'*/;
--340
insert into UsedPartsWarehouseArea
  select * from tmpdata.UsedPartsWarehouseArea140923;
