
create table tmpdata.spartpart140923 (partcode varchar2 (50),partname varchar2 (100),PartType varchar2(100),MInPackingAmount number(9) ,EnglishName varchar2(100), ReferenceCode varchar2(50)
,Specification varchar2(100),Feature varchar2(200),MeasureUnit varchar2(20),LastSubstitute varchar2(50),NextSubstitute varchar2(50),ShelfLife number(9))

create table tmpdata.spartpart140923_1 as select * from sparepart where rownum<1;
insert into tmpdata.spartpart140923_1
  (id,
   code,
   name,
   lastsubstitute,
   nextsubstitute,
   shelflife,
   englishname,
   referencecode,
   parttype,
   specification,
   feature,
   status,
   measureunit,
   creatorid,
   creatorname,
   createtime,
   minpackingamount)
  select s_sparepart.nextval,
         t.partcode,
         t.partname,
         t.lastsubstitute,
         t.nextsubstitute,
         t.shelflife,
         t.englishname,
         t.referencecode,
         decode(t.parttype, '总成件', 1, '配件', 2, '精品件', 3, '辅料', 4),
         t.specification,
         t.feature,
         1,
         nvl(t.measureunit,'件'),
         1,
         'Admin',
         sysdate,
         t.minpackingamount
from tmpdata.spartpart140923 t 

insert into sparepart select * from tmpdata.spartpart140923_1;

create table tmpdata.spareparthistory140923 as select * from spareparthistory;
insert into tmpdata.spareparthistory140923
  ("ID", --0
   "SPAREPARTID", --1
   "CODE", --2
   "NAME", --3
   "LASTSUBSTITUTE", --4
   "NEXTSUBSTITUTE", --5
   "SHELFLIFE", --6
   "ENGLISHNAME", --7
   "PINYINCODE", --8
   "REFERENCECODE", --9
   "REFERENCENAME", --10
   "CADCODE", --11
   "CADNAME", --12
   "PARTTYPE", --13
   "SPECIFICATION", --14,
   "FEATURE", --15
   "STATUS", --16
   "LENGTH", --17
   "WIDTH", --18
   "HEIGHT", --19
   "VOLUME", --20
   "WEIGHT", --21
   "MATERIAL", --22
   "PACKINGAMOUNT", --23
   "PACKINGSPECIFICATION", --24
   "PARTSOUTPACKINGCODE", --25
   "PARTSINPACKINGCODE", --26
   "MEASUREUNIT", --27
   "CREATORID", --28
   "CREATORNAME", --29
   "CREATETIME") --30
  select s_spareparthistory.nextval, --0
         b.id, --1
         b.code, --2
         b.name, --3
         b.lastsubstitute, --4
         b.nextsubstitute, --5
         b.shelflife, --6
         b.englishname, --7
         b.pinyincode, --8
         b.referencecode, --9
         b.referencename, --10
         b.cadcode, --11
         b.cadname, --12
         b.parttype, --13
         b.specification, --14
         b.feature, --15
         b.status, --16
         b.length, --17
         b.width, --18
         b.height, --19
         b.volume, --20
         b.weight, --21
         b.material, --22
         b.packingamount, --23
         b.packingspecification, --24
         b.partsoutpackingcode, --25
         b.partsinpackingcode, --26
         b.measureunit, --27
         b.creatorid, --28
         b.creatorname, --29
         b.createtime --30
    from tmpdata.spartpart140923_1 b;
insert into spareparthistory select * from tmpdata.spareparthistory140923 ;
