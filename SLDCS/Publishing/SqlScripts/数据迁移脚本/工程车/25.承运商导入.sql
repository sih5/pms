create table tmpdata.LogisticCompany140924
(
  
  企业编号   VARCHAR2(90),
  企业名称   VARCHAR2(90),
  企业类型   VARCHAR2(90),
  简称       VARCHAR2(90),
  省       VARCHAR2(90),
  市       VARCHAR2(90),
  县       VARCHAR2(90),
  城市级别   VARCHAR2(90),  
  邮编      VARCHAR2(90),
  电子邮件    VARCHAR2(90),
  传真        varchar2(90),
  联系人       varchar2(90),
  联系电话      varchar2(90),
  联系人手机     varchar2(90),
  联系地址    VARCHAR2(200),
  注册地址    VARCHAR2(200),
  经营范围    varchar2(200),
  注册时间    date, 
  品牌      VARCHAR2(90),
  业务编码    VARCHAR2(90),
  税务登记号 VARCHAR2(200),
  开票名称 VARCHAR2(200),
  开票类型 VARCHAR2(200),
  税务登记地址 VARCHAR2(200),
  税务登记电话 VARCHAR2(200),
  收货地址 VARCHAR2(200),
  开户行名称 VARCHAR2(200),
  开户行账号 VARCHAR2(200),
  状态 VARCHAR2(200),
  备注 VARCHAR2(200),
  法定代表人 VARCHAR2(200),
  法人代表电话 VARCHAR2(200) ,
  注册资本 VARCHAR2(200),
  营业地址  VARCHAR2(200)
  
)

create table tmpdata.Company140924_4 as select * from company where rownum<1;
insert into tmpdata.Company140924_4
  (ID,
   TYPE,
   CODE,
   NAME,
   REGIONID,
   PROVINCENAME,
   CITYNAME,
   COUNTYNAME,
   CONTACTADDRESS,
   CONTACTPOSTCODE,
   CONTACTMAIL,
   ContactPerson,
   ContactPhone,
   RegisteredAddress,
   BusinessScope,
   RegisterDate,
   ShortName,
   CityLevel,
   LegalRepresentative,
   LegalRepresentTel,
   RegisterCapital,
   BusinessAddress,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select S_Company.Nextval,
         4,
         "企业编号",
         "企业名称",
         (select Id
            from TiledRegion
           where provincename = "省"
             and cityname = "市"
             and countyname = "县"),
         "省",
         "市",
         "县",
         "联系地址",
         "邮编",
         "电子邮件",
         t.联系人,
         t.联系电话,
         t.注册地址,
         t.经营范围,
         t.注册时间,
         t.简称,
         decode(t.城市级别, '省级', 1, '地级', 2, '县级', 3),
         t.法定代表人,
         t.法人代表电话,
         0,
         t.营业地址, 1,
         1,
         'Admin',
         sysdate
    from tmpdata.LogisticCompany140924 t;

insert into company
  select *
    from tmpdata.Company140924_4
 /*
   where not exists (select *
            from security.enterprise
           where Company140606.name = enterprise.name)
     and not exists
   (select *
            from security.enterprise
           where Company140606.code = enterprise.code);
           
         insert into  tmpdata.Company140606_1  select * from  tmpdata.Company140606_4  where  exists (select *
            from security.enterprise
           where Company140606_4.name = enterprise.name)
           insert into tmpdata. Company140606_1 select * from  tmpdata.Company140606_4 where  exists
   (select *
            from security.enterprise
           where Company140606_4.code = enterprise.code);
           delete from  tmpdata.Company140606_4  where  exists (select *
            from security.enterprise
           where Company140606_4.name = enterprise.name)
           delete from  tmpdata.Company140606_4 where  exists
   (select *
            from security.enterprise
           where Company140606_4.code = enterprise.code);
           
        update company set type=2 where exists(select 1 from tmpdata.Company140606_2 where Company140606_2.id=company.id)      
          update security.enterprise set enterprise.EnterpriseCategoryid=2 where exists(select 1 from tmpdata.Company140606_2 where Company140606_2.id=enterprise.id) */
/*or not exists(select * from security.enterprise where Company140606.code=enterprise.code)）*/;
select * from company where exists(select 1 from  tmpdata.Company140606  where Company140606.id=company.id)
--select name from  tmpdata.Company140519 group by name having count(name)>1
/*select *
  from security.enterprise t
 where exists (select 1 from tmpdata.Company140606 v where t.name = v.name)
  ;*/
  
 /* select * from  tmpdata.Company140519 t where  exists(select * from security.enterprise where t.code=enterprise.code) and
 select * from  tmpdata.Company140430 t where exists(select * from security.enterprise where t.name=enterprise.name)*/
 --select * from tmpdata.Company140430 where code in( select code from tmpdata.Company140430 group by code having count(code)>1)for update;
 --61
create table tmpdata.LogisticCompany140924_1 as select * from dcs.LogisticCompany where rownum<1;
insert into tmpdata.LogisticCompany140924_1
 

(
  ID,
  CODE,
  NAME,
  
  CREATETIME,
  CREATORID,
  CREATORNAME,
  status
)

  select (select id from tmpdata.Company140924_4 where Company140924_4.code = "企业编号"),
         "企业编号",
         "企业名称", 
       
         sysdate,1,
         'Admin',1
         /*,
         是否可索赔*/
    from tmpdata.LogisticCompany140924 t
    where exists(select 1 from  tmpdata.Company140924_4 t where t.code= "企业编号" );
    insert into LogisticCompany select * from  tmpdata.LogisticCompany140924_1; 
    /*
    select * from tmpdata.Company140519 where exists(select 1 from   company  where Company140519.id=company.id)
        */
  
--979
create table   tmpdata.companyinvoiceinfo140924_3 as select * from companyinvoiceinfo where rownum<1;
insert into tmpdata.companyinvoiceinfo140924_3
  (id,
   Invoicecompanyid,
   Companyid,
   Companycode,
   Companyname,
   Isvehiclesalesinvoice,
   Ispartssalesinvoice,
   TaxRegisteredNumber,
   InvoiceTitle,
   InvoiceType,
   TaxRegisteredAddress,
   TaxRegisteredPhone,
   BankName,
   BankAccount,
   status,
   remark)
  select s_CompanyInvoiceInfo.Nextval,
         (select id from branch where code = '2230'),
         (select id
            from tmpdata.Company140924_4
           where Company140924_4.code = t.企业编号),
         t.企业编号,
         t.企业名称,
         0,
         1,
         t.税务登记号,
         t.开票名称,
         decode(t.开票类型,'增值税发票',1,2),
         t.税务登记地址,
         t.税务登记电话,
         t.开户行名称,
         t.开户行账号,
         1,
         ''
    from tmpdata.LogisticCompany140924 t where exists(select 1 from  tmpdata.Company140924_4
           where Company140924_4.code = t.企业编号);
           insert into companyinvoiceinfo select * from  tmpdata.companyinvoiceinfo140924_3;
