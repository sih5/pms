
create table tmpdata.company140925 as select * from company where rownuM<1;
insert into tmpdata.company140925
  (ID,
   TYPE,
   CODE,
   NAME,
   CustomerCode,
   SupplierCode,
   REGIONID,
   PROVINCENAME,
   CITYNAME,
   COUNTYNAME,
   CityLevel,
   FoundDate,
   ContactPerson,
   ContactPhone,
   ContactMobile,
   Fax,
   CONTACTADDRESS,
   CONTACTPOSTCODE,
   CONTACTMAIL,
   RegisterCode,
   RegisterName,
   CorporateNature,
   LegalRepresentative,
   LegalRepresentTel,
   IdDocumentType,
   IdDocumentNumber,
   RegisterCapital,
   RegisterDate,
   BusinessScope,
   BusinessAddress,
   RegisteredAddress,
   Remark,
   STATUS,
   CREATORNAME,
   CREATETIME)
  select s_company.nextval,
         2,
         企业编号,
         substr(企业名称, 1, 25),
         MDM客户编码,
         MDM供应商编码,
         (select Id
            from TiledRegion
           where provincename = 省
             and cityname = 市
             and countyname = 县),
         省,
         市,
         县,
         decode(城市级别, '省级', 1, '地级', 2, '县级', 3),
         to_date(成立日期, 'yyyy-mm-dd'),
         联系人,
         固定电话,
         联系人手机,
         传真,
         联系地址,
         邮政编码,
         substrb(电子邮箱, 1, 50),
         '',
         '',
         '', ---企业性质先不导
         法定代表人,
         法人代表电话,
         '',
         '',
         注册资本,
         /*case 注册日期
         when null then
          to_date('1885-1-1', 'yyyy-mm-dd')
         when '' then
          to_date('1885-1-1', 'yyyy-mm-dd')
         else
          to_date(substr(注册日期, 1, 4) ||'-' || substr(注册日期, 6, instr(注册日期,'月') - instr(注册日期,'年') - 1) ||'-' || substr(注册日期, instr(注册日期,'月') + 1, instr(注册日期,'日') - instr(注册日期,'月') - 1),'yyyy-mm-dd') end as RegisterDate*/
         注册日期,
         经营范围,
         营业地址,
         税务登记地址,
         备注,
         1,
         'Admin',
         sysdate
    from tmpdata.dealer140924
    --where not exists(select 1 from company where company.code= dealer140924.企业编号);
    insert into company select * from tmpdata.company140925;
   select * from  company where code='FT001765'
   delete from company where id=9085
   delete from dealer where id=9085

    /*
    select * from tmpdata.dealer140924 t
    where  exists(select 1 from company where company.code= t.企业编号);
   select * from tmpdata.dealer140924 where 企业编号='FT004692' for update
select 企业编号 from tmpdata.dealer140924 t group by 企业编号 having count (企业编号)>1 */
create table tmpdata.dealer140925 as select * from dealer where rownum<1;
insert into tmpdata.dealer140925 
(ID,CODE,NAME,ISVIRTUALDEALER,CREATETIME,CREATORNAME,status
)
select b.id,b.code,b.name,0,sysdate,'Admin',1
from tmpdata.dealer140924 a 
inner join company b on a.企业编号 = b.code

insert into dealer select * from  tmpdata.dealer140925;

create table tmpdata.CompanyInvoiceInfo140925 as select * from CompanyInvoiceInfo where rownum<1;
insert into tmpdata.CompanyInvoiceInfo140925
  (id,Invoicecompanyid,Companyid,Companycode,Companyname,Isvehiclesalesinvoice,Ispartssalesinvoice,TaxRegisteredNumber,InvoiceTitle,
InvoiceType,TaxRegisteredAddress,TaxRegisteredPhone, BankName, BankAccount,status,remark)
select s_CompanyInvoiceInfo.Nextval,
(select id from branch where code = '2230'),b.id,b.code,b.name,1,1,税务登记号,开票名称,
decode(nvl(a.开票类型,'空'),'增值税发票',1,'空',4,2),税务登记地址,开票电话,银行名称,银行账号,1,'09-25导入'
from tmpdata.dealer140924 a 
inner join company b on a.企业编号 = b.code
where 1=1

insert into CompanyInvoiceInfo select * from tmpdata.CompanyInvoiceInfo140925 ;

create table tmpdata.DealerServiceExt140925 as select * from DealerServiceExt where rownum<1
insert into tmpdata.DealerServiceExt140925
  (Id,
   RepairQualification,
   HasBranch,
   DangerousRepairQualification,
   BrandScope,
   CompetitiveBrandScope,
   ParkingArea,
   RepairingArea,
   EmployeeNumber,
   PartWarehouseArea,
   GeographicPosition,
   OwnerCompany,
   MainBusinessAreas,
   AndBusinessAreas,
   BuildTime,
   TrafficRestrictionsdescribe,
   ManagerPhoneNumber,
   ManagerMobile,
   ManagerMail,
   ReceptionRoomArea,
   Remark,
   Status,
   CreateTime,
   CreatorName)
  select b.id,
         decode(维修资质等级, '一类', '1', '二类', 2, '三类', 3, 1),
         decode(有二级服务站, '有', 1, 2),
         decode(危险品运输车辆维修资质, '有', 1, 2),
         substrb(福田授权品牌,1,50),
         substr(非福田授权品牌, 0, 25),
         停车场面积,
         维修车面积,
         decode(服务站人数, '10人以下', 10, '30-50人', 50, '50人以上', 51),
         配件库面积,
         null,
         隶属单位,
         主营范围,
         兼营范围,
         to_date(建厂时间,'yyyy-mm-dd'),
         交通限行描述,
         null,
         null,
         null,
         接待室面积,
         '2014-09-25导入',
         1,
         sysdate,
         'Admin'
    from tmpdata.dealer140924 a
   inner join dealer b
      on a.企业编号 = b.code
     
insert into  DealerServiceExt select * from tmpdata.DealerServiceExt140925;
