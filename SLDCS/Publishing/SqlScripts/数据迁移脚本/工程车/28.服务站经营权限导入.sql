create table tmpdata.DealerBusinessPermit140926
(服务站编号  varchar2(100),
服务站名称   varchar2(100),
分公司编号   varchar2(100), 分公司名称  varchar2(100), 
服务产品线名称  varchar2(100),
工时单价  varchar2(100),
工时单价等级名称  varchar2(100),
工时单价等级编号 varchar2(100)
);
create table tmpdata.DealerBusinessPermit140926_1  as select * from DealerBusinessPermit;
insert into tmpdata.DealerBusinessPermit140926_1
  (Id,
   DealerId,
   ServiceProductLineId,
   ProductLineType,
   BranchId,
   ServiceFeeGradeId,
   CreateTime,
   CreatorName)
  select s_DealerBusinessPermit.Nextval,
         b.id,
         c.ProductLineId,
         c.ProductLineType,
         8481,
         e.id,
         sysdate,
         'Admin'
    from tmpdata.DealerBusinessPermit140926 a
   inner join dealer b
      on a.服务站编号 = b.code
   inner join serviceproductlineview c
      on c.ProductLineName = a.服务产品线名称
   inner join LaborHourUnitPriceRate d
      on d.ServiceProductLineId = c.ProductLineId
     and d.Price = a.工时单价
   inner join LaborHourUnitPriceGrade e
      on e.id = d.laborhourunitpricegradeid
     and e.code = a.工时单价等级编号
   where not exists (select 1
            from DealerBusinessPermit tmp
           where tmp.dealerid = b.id
             and tmp.serviceproductlineid = c.ProductLineId)
insert into DealerBusinessPermit select * from tmpdata.DealerBusinessPermit140926_1 ;

