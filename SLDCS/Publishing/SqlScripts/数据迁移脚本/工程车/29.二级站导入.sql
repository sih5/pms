create table tmpdata.subdealer140926 (subdealername varchar2(100),subdealercode varchar2(50),dealercode varchar2(50),dealername varchar2(100),dealertype varchar2(100),address varchar2(200),DealerManager varchar2(200),manager varchar2(50),phone varchar2(50),mobile varchar2(50),email varchar2(100),remark varchar2(200))
create table tmpdata.subdealer140926_1 as select * from subdealer where rownum<1;
insert into tmpdata.subdealer140926_1
  (id,
   name,
   code,
   subdealertype,
   dealerid,
   address,
   createtime,
   creatorid,
   creatorname,
   status,
   dealermanager,
   manager,
   managerphonenumber,
   managermobile,
   remark,
   managermail)
  select s_subdealer.nextval,
         t.subdealername,
         t.subdealercode,
         decode(dealertype, '认证二级站', 1, '非认证二级站', 2),
         (select id from dealer where dealer.code = t.dealercode),
         t.address,
         sysdate,
         1,
         'Admin',
         1,
         t.dealermanager,
         t.manager,
         t.phone,
         t.mobile,
         t.remark,
         t.email
    from tmpdata.subdealer140926 t;
insert into subdealer select * from  tmpdata.subdealer140926_1;
