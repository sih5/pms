create table tmpdata.Malfunctionandbrand140918 (MalfunctionCategoryCode varchar2(50),code varchar2(50),name varchar2(100),brand varchar2(50),memo varchar2(200), TYPE  VARCHAR2(100)) 
update tmpdata.Malfunctionandbrand140918 set type='kms' where TYPE IS NULL
select count(*) from tmpdata.Malfunctionandbrand140918 t WHERE TYPE='kms'
--select code ,brand from tmpdata.Malfunctionandbrand140918 group by code, brand having count (code)>1
select * from tmpdata.Malfunctionandbrand140918 t where not exists(select 1 from gcdcs.malfunctioncategory v where t.malfunctioncategorycode=v.code and v.status=1 and v.layernodetypeid=6)
create  table tmpdata.Malfunction140918 as select * from Malfunction where rownum<1;

insert into tmpdata.Malfunction140918
  (id,
   code,
   description,
   malfunctioncategoryid,
   remark,
   status,
   creatorid,
   creatorname,
   createtime)
select s_Malfunction.Nextval,
 v.code,
 v.name,
 (select distinct id
    from gcdcs.malfunctioncategory t
   where t.code = v.malfunctioncategorycode
     and t.status = 1
     and t.layernodetypeid = 6),
 v.remark,
 1,
 1,
 'Admin',
 sysdate
  from (select distinct code, name, malfunctioncategorycode, memo remark
          from tmpdata.Malfunctionandbrand140918) v
 where exists (select 1
          from gcdcs.malfunctioncategory a
         where v.malfunctioncategorycode = a.code
           and a.status = 1
           and a.layernodetypeid = 6)
   and not exists (select distinct a.code,a.name
          from gcdcs.malfunctioncategory t
         inner join tmpdata.Malfunctionandbrand140918 a
            on a.malfunctioncategorycode = t.code
         where t.code in (select code
                            from gcdcs.malfunctioncategory t
                           where t.status = 1
                             and t.layernodetypeid = 6
                           group by code
                          having count(code) > 1)
           and a.code = v.code
           and a.name = v.name);
                
      
insert into tmpdata.Malfunction140918
  (id,
   code,
   description,
   malfunctioncategoryid,
   remark,
   status,
   creatorid,
   creatorname,
   createtime)
select s_Malfunction.Nextval,
 v.code,
 v.name,
 (select distinct T.id
    from gcdcs.malfunctioncategory t
    inner join  gcdcs.malfunctioncategory a on a.id=t.parentid inner join  gcdcs.malfunctioncategory b on b.id=a.parentid
   where t.code = v.malfunctioncategorycode
     and t.status = 1
     and t.layernodetypeid = 6 and b.code='K0'),
 v.remark,
 1,
 1,
 'Admin',
 sysdate
  from (select distinct code, name, malfunctioncategorycode, memo remark
          from tmpdata.Malfunctionandbrand140918) v
 where exists (select 1
          from gcdcs.malfunctioncategory a
         where v.malfunctioncategorycode = a.code
           and a.status = 1
           and a.layernodetypeid = 6)
   and  exists (select distinct a.code,a.name
          from gcdcs.malfunctioncategory t
         inner join tmpdata.Malfunctionandbrand140918 a
            on a.malfunctioncategorycode = t.code
         where t.code in (select code
                            from gcdcs.malfunctioncategory t
                           where t.status = 1
                             and t.layernodetypeid = 6
                           group by code
                          having count(code) > 1)
           and a.code = v.code
           and a.name = v.name);
              
           
             select v.* from gcdcs.malfunctioncategory t
             inner join tmpdata.Malfunctionandbrand140918 v on v.malfunctioncategorycode=t.code  where t.code in(
select code  from  gcdcs.malfunctioncategory t where  t.status = 1
             and t.layernodetypeid = 6   group by code having count (code)>1)
             
             create table tmpdata.MalfunctionBrandRelation140918 as select * from MalfunctionBrandRelation where rownum<1;
          
          insert into tmpdata.MalfunctionBrandRelation140918
            (id,
             branchid,
             malfunctionid,
             malfunctioncode,
             malfunctionname,
             partssalescategoryid,
             isapplication,
             creatorid,
             creatorname,
             createtime,
             status,
             remark)
            select s_MalfunctionBrandRelation.Nextval,8481,
                   (select distinct id
                      from tmpdata.Malfunction140918
                     where Malfunction140918.code =
                           Malfunctionandbrand140918.code  and rownum=1),
                   code,
                   name,
                   (select id
                      from partssalescategory
                     where name = Malfunctionandbrand140918.brand),
                   0,
                   1,
                   'Admin',
                   sysdate,
                   1,
                   Malfunctionandbrand140918.memo
              from tmpdata.Malfunctionandbrand140918
              where exists(select 1 from tmpdata.Malfunction140918
                     where Malfunction140918.code =
                           Malfunctionandbrand140918.code)
  
              insert into MalfunctionBrandRelation select * from tmpdata.MalfunctionBrandRelation140918;
       
              insert into Malfunction select count(*) from  tmpdata.Malfunction140918;
              select * from tmpdata.Malfunction140918 where code in(
select code
                      from tmpdata.Malfunction140918 group by code having count (code)>1)
                     where Malfunction140918.code =
                           Malfunctionandbrand140918.code
update  Malfunction t  set t.description= replace(description,CHR(9),'')

select distinct brand from tmpdata.Malfunctionandbrand140918
update tmpdata.Malfunctionandbrand140918 set brand= replace(brand,CHR(10),'')
update  Malfunction t  set t.description= replace(description,CHR(10),'')
