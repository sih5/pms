
create table tmpdata.CustomerOrderPriceGrade140926(brandname varchar2(100),customercode varchar2(50),customername varchar2(100),salesordertypename varchar2(100),salesordertypecode varchar2(50),coefficient number(9,2))

create table tmpdata.partssalesordertype140926 as select * from partssalesordertype where rownum<1;
insert into tmpdata.partssalesordertype140926
  (id,
   branchid,
   code,
   name,
   partssalescategoryid,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_partssalesordertype.nextval,
         8481,
         t.salesordertypecode,
         t.salesordertypename,
         (select id from partssalescategory where name = t.brandname),
         1,
         1 ,'Admin',
         sysdate
    from (select distinct t.brandname,
                          t.salesordertypename,
                          t.salesordertypecode
            from tmpdata.CustomerOrderPriceGrade140926 t) t
insert into partssalesordertype select * from tmpdata.partssalesordertype140926;

create table tmpdata.COrderPriceGrade140926_1 as select * from CustomerOrderPriceGrade where rownum<1;
insert into tmpdata.COrderPriceGrade140926_1
  (id,
   branchid,
   partssalescategoryid,
   customercompanyid,
   customercompanycode,
   customercompanyname,
   partssalesordertypeid,
   partssalesordertypecode,
   partssalesordertypename,
   coefficient,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_CustomerOrderPriceGrade.Nextval,
         8481,
         v.partssalescategoryid,
         t.id,
         t.code,
         t.name,
         v.id,
         v.code,
         v.name,
         1,
         1,
         1,
         'Admin',
         sysdate
    from dealer t
   inner join DealerServiceInfo a
      on a.dealerid = t.id
   inner join tmpdata.partssalesordertype140926 v
      on v.partssalescategoryid = a.partssalescategoryid;

insert into tmpdata.COrderPriceGrade140926_1
  (id,
   branchid,
   partssalescategoryid,
   customercompanyid,
   customercompanycode,
   customercompanyname,
   partssalesordertypeid,
   partssalesordertypecode,
   partssalesordertypename,
   coefficient,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_CustomerOrderPriceGrade.Nextval,
         8481,e.id,
         t.id,t.code,t.name,
         v.id,v.code,v.name,
         1,
         1,
         1,
         'Admin',
         sysdate
   -- from agency t cross join tmpdata.partssalesordertype140926 v;     
  from  Agency t 
inner join partssalescategory e on 1=1
inner join PartsSalesOrderType v on v.partssalescategoryid = e.id
select count(*)from agency t cross join tmpdata.partssalesordertype140926 v;     
    insert into CustomerOrderPriceGrade select * from  tmpdata.COrderPriceGrade140926_1;
    
