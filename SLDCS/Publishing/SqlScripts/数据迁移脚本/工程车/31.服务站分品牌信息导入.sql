
   ---服务站分品牌
   create table tmpdata.DealerServiceInfo140925 
   (
   服务站编号 varchar2(100),
服务站名称 varchar2(100),
业务编码 varchar2(100),
业务名称 varchar2(100),
品牌 varchar2(100),
市场部 varchar2(100),
市场部编号 varchar2(100),
业务能力 varchar2(100),
服务站类别 varchar2(100),
固定电话 varchar2(100),
小时热线 varchar2(100),
传真 varchar2(100),
维修权限 varchar2(100),
服务站维修权限 varchar2(100),
配件管理费率 varchar2(100),
外出服务费等级 varchar2(100),
星级 varchar2(100),
旧件仓库名称  varchar2(100),
外出服务半径 varchar2(100),
是否职守 varchar2(100),
配件储存金额 varchar2(100)
   );
select *from  tmpdata.DealerServiceInfo140925  where 服务站编号= 'FT004817'

 insert into ChannelCapability
 (id,name,ChannelGrade,description,issale,ispart,isservice,issurvey,status
 )
 select id,name,ChannelGrade,description,issale,ispart,isservice,issurvey,status
 from dcs.ChannelCapability
 
 create table tmpdata.DealerServiceInfo140925_1 as select * from  DealerServiceInfo;
insert into tmpdata.DealerServiceInfo140925_1
  (Id,
   BranchId,
   PartsSalesCategoryId,
   DealerId,
   MarketingDepartmentId,
   UsedPartsWarehouseId,
   ChannelCapabilityId,
   PartsManagingFeeGradeId,
   GradeCoefficientId,
   OutServiceradii,
   BusinessDivision,
   AccreditTime,
   ServicePermission,
   ServiceStationType,
   OutFeeGradeId,
   RepairAuthorityGrade,
   Status,
   CreateTime,
   CreatorName,
   isonduty,
   remark,
   businesscode,
   businessname,
   PartReserveAmount)
  select s_dealerserviceinfo.nextval,
         8481,
         c.id partssalescategoryid,
         b.id dealerid,
         nvl(d.id, -1) marketingdepartmentid,
         e.id usedpartswarehouseid,
         f.id channelcapabilityid,
         g.id partsmanagementcostgradeid,
         nvl(h.id,
         -1) gradecoefficientid,
         nvl(a.外出服务半径, 1),
         c.name,
         dsc.BuildTime,
         a.维修权限,
         decode(a.服务站类别,
                '模拟站',
                1,
                '星级服务中心',
                2,
                '快修店',
                4,
                '服务工程师',
                5,
                '标杆服务站',
                6,
                '自保站',
                7,
                3),
         i.id,
         decode(a.服务站维修权限, '大修', 1, '小修',2,1),
         1,
         sysdate,
         'Admin',
         decode(a.是否职守, '是', 1, 0),
         '2014-09-25导入',
         a.业务编码,
         nvl(a.业务名称,'空'),
         a.配件储存金额
    from tmpdata.DealerServiceInfo140925 a
   inner join dealer b
      on a.服务站编号 = b.code
    left join dealerserviceext dsc
      on dsc.id = b.id
   inner join partssalescategory c
      on c.name = a.品牌
   inner join company
      on company.id = b.id
    left join marketingdepartment d
      on d.partssalescategoryid = c.id
     and d.name = a.市场部
    left join usedpartswarehouse e
      on e.name = a.旧件仓库名称
    left join partsmanagementcostgrade g
      on g.name = a.配件管理费率
    left join gradecoefficient h
      on h.grade = a.星级
     and h.partssalescategoryid = c.id
    left join channelcapability f
      on f.name = a.业务能力
    left join ServiceTripPriceGrade i
      on i.code = a.外出服务费等级
   where not exists (select 1
            from DealerServiceInfo tmp
           where tmp.partssalescategoryid = c.id
             and tmp.dealerid = company.id);
insert into DealerServiceInfo select * from tmpdata.DealerServiceInfo140925_1 a ;
--delete from DealerServiceInfo
/*update tmpdata.DealerServiceInfo140925 a  set a.业务能力='专卖店'where a.业务能力='快修店'
select c.id partssalescategoryid,
         b.id dealerid,
         nvl(d.id, -1) marketingdepartmentid,
         e.id usedpartswarehouseid,
         f.id channelcapabilityid,
         g.id partsmanagementcostgradeid,
         nvl(h.id,
         -1) gradecoefficientid,a.服务站编号,a.业务能力 from tmpdata.DealerServiceInfo140925 a 

　 inner join dealer b
      on a.服务站编号 = b.code
          left join dealerserviceext dsc
      on dsc.id = b.id
   inner join partssalescategory c
      on c.name = a.品牌
   inner join company
      on company.id = b.id
    left join marketingdepartment d
      on d.partssalescategoryid = c.id
     and d.code = a.市场部编号
    left join usedpartswarehouse e
      on e.name = a.旧件仓库名称
    left join partsmanagementcostgrade g
      on g.code = a.配件管理费率
    left join gradecoefficient h
      on h.grade = a.星级
     and h.partssalescategoryid = c.id
    left join channelcapability f
      on f.name = a.业务能力
    left join ServiceTripPriceGrade i
      on i.code = a.外出服务费等级
update tmpdata.DealerServiceInfo140925 a set a.品牌='工程车(北区)'where  a.品牌='工程车（北区）'
update tmpdata.DealerServiceInfo140925 a set a.品牌='工程车(南区)'where a.品牌='工程车（南区）'
select * 
from tmpdata.DealerServiceInfo140925 a
   inner join dealer b
      on a.服务站编号 = b.code
    left join dealerserviceext dsc
      on dsc.id = b.id
   inner join partssalescategory c
      on c.name = a.品牌
   inner join company
      on company.id = b.id
    left join marketingdepartment d
      on d.partssalescategoryid = c.id
     and d.code = a.市场部编号
    left join usedpartswarehouse e
      on e.name = a.旧件仓库名称
    left join partsmanagementcostgrade g
      on g.code = a.配件管理费率
    left join gradecoefficient h
      on h.grade = a.星级
     and h.partssalescategoryid = c.id
    left join channelcapability f
      on f.name = a.业务能力
    left join ServiceTripPriceGrade i
      on i.code = a.外出服务费等级
   where not exists (select 1
            from DealerServiceInfo tmp
           where tmp.partssalescategoryid = c.id
             and tmp.dealerid = company.id)
delete from tmpdata.DealerServiceInfo140925 a where a.服务站编号='FT004817' and 星级='一星级'
tmpdata.agentnewoldrel*/
