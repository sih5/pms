create table tmpdata.AgencyDealerRelation140926(brandname varchar2(50),agencycode varchar2(50),agencyname varchar2(100),dealercode varchar2(50),dealername varchar2(100))
create table tmpdata.AgencyDealerRelation140926_1 as select * from AgencyDealerRelation where rownum<1;
insert into tmpdata.agencydealerrelation140926_1
  (id,
   branchid,
   partssalesordertypeid,
   agencyid,
   agencycode,
   agencyname,
   dealerid,
   dealercode,
   dealername,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_AgencyDealerRelation.Nextval,
         8481,
         (select id
            from partssalescategory
           where partssalescategory.name = t.brandname),
         (select id from agency where agency.code = t.agencycode),
         t.agencycode,
         t.agencyname,
         nvl((select id from dealer where dealer.code = t.dealercode ),(select id from dealer where dealer.name = t.dealername)),
         t.dealercode,
         t.dealername,1,
         1,
         'Admin',
         sysdate
    from tmpdata.AgencyDealerRelation140926 t;
    insert into AgencyDealerRelation select * from   tmpdata.agencydealerrelation140926_1 t;
    /*select * from tmpdata.AgencyDealerRelation140926 t where dealername='林州市万通汽车贸易有限责任公司' for update
select * from company where code='FT002787'*/
