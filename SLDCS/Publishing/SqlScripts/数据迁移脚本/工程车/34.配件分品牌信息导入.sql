--1.将配件分品牌信息导入临时表：

delete  from tmpdata.partbranchfromyx140923
create table tmpdata.partbranchfromyx140923
(
       品牌    varchar2(200),
       配件图号    varchar2(200),
       配件名称    varchar2(200),
       配件参考图号    varchar2(200),
       损耗类型    varchar2(200),
       是否可采购    varchar2(200),
       是否可销售    varchar2(200),
       最小销售数量    varchar2(200),
       是否可直供    varchar2(200),
       旧件返回政策  varchar2(200),
       配件保修分类编号    varchar2(200),
       配件保修分类名称    varchar2(200),
       供应商编码    varchar2(200),
       供应商名称    varchar2(200),
       供应商图号	varchar2(200)
);
create table tmpdata.partsbranch140923 as select * from partsbranch;
insert into tmpdata.partsbranch140923(
       "ID",--1
       "PARTID",--2
       "PARTCODE",--3
       "PARTNAME",--4
       "REFERENCECODE",--5
       "STOCKMAXIMUM",--6
       "STOCKMINIMUM",--7
       "BRANCHID",--8
       "BRANCHNAME",--9
       "PARTSSALESCATEGORYID",--10
       "PARTSSALESCATEGORYNAME",--11
       "ABCSTRATEGYID",--12
       "PRODUCTLIFECYCLE",--13
       "LOSSTYPE",--14
       "ISORDERABLE",--15
       "PURCHASECYCLE",--16
       "ISSERVICE",--17
       "ISSALABLE",--18
       "PARTSRETURNPOLICY",--19
       "ISDIRECTSUPPLY",--20
       "WARRANTYSUPPLYSTATUS",--21
       "MINSALEQUANTITY",--22
       "PARTABC",--23
       "PURCHASEROUTE",--24
       "REPAIRMATMINUNIT",--25
       "PARTSWARRANTYCATEGORYID",--26
       "PARTSWARRANTYCATEGORYCODE",--27
       "PARTSWARRANTYCATEGORYNAME",--28
       "PARTSWARHOUSEMANAGEGRANULARITY",--29
       "STATUS",--30
       "CREATORID",--31
       "CREATORNAME",--32
       "CREATETIME",--33
       "MODIFIERID",--34
       "MODIFIERNAME",--35
       "MODIFYTIME",--36
       "ABANDONERID",--37
       "ABANDONERNAME",--38
       "ABANDONTIME",--39
       "REMARK")--40
select s_partsbranch.nextval,--1
       (select s.id from sparepart s where a.配件图号=s.code) partid,--2
       a.配件图号,--3
       (select s.name from sparepart s where a.配件图号=s.code) partname,--4
       a.配件参考图号,--5
       9999999,--6
       0,--7
       8481,--8
       (select name from company where id=8481) branchid, --9
       (select id from partssalescategory p where p.name=a.品牌) partssalescategoryid,--10
       a.品牌,--11
       '',--12
       '',--13
       case a.损耗类型 when '非常用件' then 1 when '常用件' then 2 when '易耗件' then 3 when '大总成' then 4 end losstype,--14
       NVL  (case a.是否可采购 when '是' then 1 when '否' then 0 end ,1)isorderable,--15
       '',--16
       '',--17
       nvl(case a.是否可销售 when '是' then 1 when '否' then 0 end,1) issaleable,--18
       case a.旧件返回政策 when '返回本部' then 1 when '供应商自行回收' then 2 when '监督处理' then 3 when '自行处理' then 4 when '总部调件' then 5 end partsreturnpolicy,--19
       nvl(case a.是否可直供 when '是' then 1 when '否' then 0 end ,0) isdirectsupply,--20
       '',--21
       a.最小销售数量,--22
       '',--23
       '',--24
       '',--25
       (select id from partswarrantycategory p where p.code=a.配件保修分类编号) partswarrantycategorydi ,--26
       a.配件保修分类编号,--27
       (select name from partswarrantycategory p where p.code=a.配件保修分类编号) partswarrantycategoryname,--28
       '',--29
       1,--30
       7, --31
       '晨阑测试', --32
       sysdate, --33
       '',--34
       '',--35
       '',--36
       '',--37
       '',--38
       '',--39
       ''
 from tmpdata.partbranchfromyx140923 a where exists 
 (select * from sparepart s where a.配件图号=s.code);   
 
 insert into partsbranch select * from tmpdata.partsbranch140923 t
 where not exists(select 1 from partsbranch v where t.partid=v.partid and t.partssalescategoryid=v.partssalescategoryid and t.status=v.status);
  
create table tmpdata.partsbranch140925del
as select * from tmpdata.partsbranch140923  v where exists(
select * from ( select partssalescategoryid,partid,status from  tmpdata.partsbranch140923  group by  partssalescategoryid,partid,status having count(partid)>1) t where t.partid=v.partid
and t.partssalescategoryid=v.partssalescategoryid and t.status=v.status)

create table tmpdata.partsbranch140925del_1 as
 select *  from tmpdata.partsbranch140925del t
 where t.rowid not in(select min(partsbranch140925del.rowid)
          from  tmpdata.partsbranch140925del 
         group by partssalescategoryid,partid,status);
         
         delete from tmpdata.partsbranch140923 t
         where exists(select 1 from tmpdata.partsbranch140925del_1 where t.id=partsbranch140925del_1.id)
         
 create table tmpdata.sparepart140923del as select * from  sparepart
 where sparepart.rowid not in
       (select min(sparepart.rowid)
          from sparepart
         group by code);
 
 
delete sparepart
 where sparepart.rowid not in
       (select min(sparepart.rowid)
          from sparepart
         group by code,name);
commit;
create table tmpdata.sparepart140925remain(code varchar2(50),name varchar2(100));
i
 create  table sparepart140825del as select * from sparepart t where code in(
 select code from sparepart group by code having count (code)>1) and 
 not exists(select 1 from tmpdata.sparepart140925remain v where t.code=v.code and t.name=v.name);
 delete from sparepart t where code in(
 select code from sparepart group by code having count (code)>1) and 
 not exists(select 1 from tmpdata.sparepart140925remain v where t.code=v.code and t.name=v.name);
 
 --2.将配件分品牌信息履历导入临时表：
 insert into tmpdata.partsbranchhistory140706
  ("ID",
   "PARTSBRANCHID",
   "PARTID",
   "PARTCODE",
   "PARTNAME",
   "REFERENCECODE",
   "STOCKMAXIMUM",
   "STOCKMINIMUM",
   "BRANCHID",
   "BRANCHNAME",
   "LOSSTYPE",
   "PARTSSALESCATEGORYID",
   "PARTSSALESCATEGORYNAME",
   "ABCSTRATEGYID",
   "PRODUCTLIFECYCLE",
   "ISORDERABLE",
   "PURCHASECYCLE",
   "ISSERVICE",
   "ISSALABLE",
   "PARTSRETURNPOLICY",
   "ISDIRECTSUPPLY",
   "WARRANTYSUPPLYSTATUS",
   "MINSALEQUANTITY",
   "PARTABC",
   "PURCHASEROUTE",
   "PARTSWARRANTYCATEGORYID",
   "PARTSWARRANTYCATEGORYCODE",
   "PARTSWARRANTYCATEGORYNAME",
   "PARTSWARHOUSEMANAGEGRANULARITY",
   "STATUS",
   "CREATORID",
   "CREATORNAME",
   "CREATETIME",
   "REMARK",
   "REPAIRMATMINUNIT")
  select yxdcs.s_partsbranchhistory.nextval,
         b."ID",
         b."PARTID",
         b."PARTCODE",
         b."PARTNAME",
         b."REFERENCECODE",
         b."STOCKMAXIMUM",
         b."STOCKMINIMUM",
         b."BRANCHID",
         b."BRANCHNAME",
         b."LOSSTYPE",
         b."PARTSSALESCATEGORYID",
         b."PARTSSALESCATEGORYNAME",
         b."ABCSTRATEGYID",
         b."PRODUCTLIFECYCLE",
         b."ISORDERABLE",
         b."PURCHASECYCLE",
         b."ISSERVICE",
         b."ISSALABLE",
         b."PARTSRETURNPOLICY",
         b."ISDIRECTSUPPLY",
         b."WARRANTYSUPPLYSTATUS",
         b."MINSALEQUANTITY",
         b."PARTABC",
         b."PURCHASEROUTE",
         b."PARTSWARRANTYCATEGORYID",
         b."PARTSWARRANTYCATEGORYCODE",
         b."PARTSWARRANTYCATEGORYNAME",
         b."PARTSWARHOUSEMANAGEGRANULARITY",
         b."STATUS",
         b."CREATORID",
         b."CREATORNAME",
         b."CREATETIME",
         b."REMARK",
         b."REPAIRMATMINUNIT"
    from tmpdata.partsbranch140706 b;


--3.将配件基础信息导入正式表：

insert into yxdcs.partsbranch
select * from tmpdata.partsbranch140706 ;


--4.将配件基础信息履历导入正式表：

insert into yxdcs.partsbranchhistory
select * from tmpdata.partsbranchhistory140706 ;

