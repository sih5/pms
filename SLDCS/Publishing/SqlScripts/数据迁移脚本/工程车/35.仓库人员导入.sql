create table tmpdata.WM140929 (warehousecode varchar2(100),warehousename varchar2(100),personnelname varchar2(100),personnelcode varchar2(100))

create table tmpdata.WarehouseOperator140929  as select * from WarehouseOperator where rownum<1;
insert into tmpdata.WarehouseOperator140929
  (id, warehouseid, operatorid, creatorid, creatorname, createtime)
  select S_WarehouseOperator.Nextval,
         (select id
            from gcdcs.warehouse v
           where v.code = t.warehousecode
             and branchid = 8481),
         (select id
            from gcsecurity.personnel v
           where v.loginid = t.personnelcode
             and v.enterpriseid = 8481),
         1,
         'Admin',
         sysdate
    from tmpdata.WM140929 t
where exists(select id
            from gcsecurity.personnel v
           where v.loginid = t.personnelcode
             and v.enterpriseid = 8481);
             
             insert into WarehouseOperator select * from tmpdata.WarehouseOperator140708;
