
create table tmpdata.MarketDepRegion140930 (brandcode varchar2(50),regioncode varchar2(50),regionname varchar2(100),marketDepCode varchar2(50),marketDepName varchar2(100),personnelloginid varchar2(100),personnelname varchar2(100),personneltype varchar2(100),remark varchar2(200),fax varchar2(100),postcode varchar2(100),phone varchar2(100),type varchar2(50))

create table tmpdata.salesregion140930 as select * from salesregion where rownum<1;
insert into tmpdata.salesregion140930
  (id,
   branchid,
   partssalescategoryid,
   regioncode,
   regionname,
   status,
   remark,
   createtime,
   creatorid,
   creatorname)
  select s_salesregion.nextval,,
         (select id from partssalescategory where code = t.brandcode),
         t.regioncode,
         t.regionname,
         1,
         '',
         sysdate,
         1,
         'Admin'
    from (select distinct brandcode, regioncode, regionname
            from tmpdata.MarketDepRegion140930) t
   where t.regioncode is not null;


   
   insert into salesregion select * from tmpdata.salesregion140919;
   
   create table tmpdata.marketingdepartment140919 as select * from marketingdepartment where rownum<1;
   
   insert into tmpdata.marketingdepartment140919
     (id,
      branchid,
      partssalescategoryid,
      businesstype,
      code,
      name,
      address,
      phone,
      fax,
      postcode,
      status,
      remark,
      creatorid,
      creatorname,
      createtime)
     select s_marketingdepartment.nextval,
            '8481',
            (select id from partssalescategory where code = t.brandcode),
            decode(t.type, '销售', 1, '服务', 2),
            v.marketdepcode,
            v.marketdepname,
            '',
            v.phone,
            v.fax,
            v.postcode,
            1,
            '',
            1,
            'Admin',
            sysdate
       from (select distinct t.brandcode,
                    t.marketdepcode,
                    t.marketdepname,
                    t.fax,
                    t.postcode,
                    t.phone,
                    t.type
               from tmpdata.MarketDepRegion140919 t) v;
insert into marketingdepartment select * from tmpdata.marketingdepartment140919;

select * from tmpdata.MarketDepRegion140919 t where marketdepcode in('GCCPJ01','GCCPJ02','GCCSX01','GCCSX02')
select  marketdepcode from (select distinct t.brandcode,
                    t.marketdepcode,
                    t.marketdepname,
                    t.fax,
                    t.postcode,
                    t.phone,
                    t.type
               from tmpdata.MarketDepRegion140919 t) t group by marketdepcode having count( marketdepcode)>1
            
delete from marketingdepartment where exists(select 1 from tmpdata.marketingdepartment140919 where marketingdepartment140919.id=marketingdepartment.id)
create table tmpdata.MDptPersonnelRelation140919 as select * from MarketDptPersonnelRelation where rownum<1;
insert into tmpdata.MDptPersonnelRelation140919
  (id, branchid, marketdepartmentid, personnelid, status, type)
  select s_MarketDptPersonnelRelation.Nextval,
         '8481',
         (select id from marketingdepartment where code = t.marketdepcode),
         (select id
            from gcsecurity.personnel
           where loginid = t.personnelloginid
             and personnel.name = t.personnelname),
         1,
         decode(t.personneltype,
                '服务经理',
                1,
                '服务后台人员',
                2,
                '配件后台人员',
                3)
    from tmpdata.MarketDepRegion140919 t
         --select * from tmpdata.MarketDepRegion140919 t
         where exists(select id
            from gcsecurity.personnel
           where loginid = t.personnelloginid
             and personnel.name = t.personnelname);
         insert into MarketDptPersonnelRelation
          select * from tmpdata.MDptPersonnelRelation140919;
          select * from  marketingdepartment where code in(
select code from marketingdepartment group by code having count(code)>1)
