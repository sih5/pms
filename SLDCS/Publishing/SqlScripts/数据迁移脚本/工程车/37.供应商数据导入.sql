
       create table tmpdata.partssupplier141010
( 
品牌 varchar2(100),
供应商编号 varchar2(100),
供应商名称 varchar2(100),
业务编码 varchar2(100),
业务名称 varchar2(100),
供应商类型 varchar2(100),
配件管理费系数 varchar2(100),
工时索赔系数 number(8),
配件索赔价格类型 varchar2(100),
是否按工时单价索赔 varchar2(100),
外出里程单价 varchar2(100),
外出人员单价 varchar2(100),
是否存在外出费用 varchar2(100),
旧件运费系数 number(16,5),
省 varchar2(100),
市 VARCHAR2(100),
县 varchar2(100),
邮编 VARCHAR2(100),
电子邮件 VARCHAR2(100),
传真 varchar2(100),
联系人 VARCHAR2(100),
联系电话 VARCHAR2(100),
联系地址 VARCHAR2(100),
注册地址 varchar2(100),
经营范围 varchar2(100),
注册日期 varchar2(100)
)
create table tmpdata.company141010_1 as select * from company where rownum<1;

insert into tmpdata.company141010_1
  (ID,
   TYPE,
   CODE,
   NAME,
  -- CustomerCode,
  -- SupplierCode,
   REGIONID,
   PROVINCENAME,
   CITYNAME,
   COUNTYNAME,
  -- CityLevel,
   --FoundDate,
   ContactPerson,
   ContactPhone,
   --ContactMobile,
   Fax,
   --CONTACTADDRESS,
   CONTACTPOSTCODE,
   CONTACTMAIL,
   RegisterCode,
   RegisterName,
   CorporateNature,
   --LegalRepresentative,
   --LegalRepresentTel,
   IdDocumentType,
   IdDocumentNumber,
   --RegisterCapital,
   RegisterDate,
   BusinessScope,
   BusinessAddress,
   --RegisteredAddress,
   Remark,
   STATUS,
   CREATORNAME,
   CREATETIME)
  select s_company.nextval,
         6,
         供应商编号,
         substr(供应商名称, 1, 25),
        -- MDM客户编码,
        -- MDM供应商编码,
         (select Id
            from TiledRegion
           where provincename = 省
             and cityname = 市
             and countyname = 县),
         省,
         市,
         县,
         --decode(城市级别, '省级', 1, '地级', 2, '县级', 3),
       --  to_date(成立日期, 'yyyy-mm-dd'),
         联系人,
         t.联系电话,
         --电子邮件,
         t.传真,
         --联系地址,
         t.邮编,
         t.电子邮件,
         '',
         '',
         '', ---企业性质先不导
        -- 法定代表人,
         --法人代表电话,
         '',
         '',
         --t.,
         case 注册日期
           when null then
            to_date('1885-1-1', 'yyyy-mm-dd')
           when '' then
            to_date('1885-1-1', 'yyyy-mm-dd')
           else
            to_date(注册日期, 'yyyy-mm-dd')
         end as RegisterDate,
         t.经营范围,
         t.联系地址,
         --税务登记地址,
         '2014-10-10导入供应商',
         1,
         'Admin',
         sysdate
    from tmpdata.partssupplier141010 t
   where 供应商编号 not in (select code from company);
   insert into company select * from tmpdata.company141010_1;
   
   create table tmpdata.PartsSupplier141010_1 as select * from PartsSupplier where rownum<1;
insert into tmpdata.PartsSupplier141010_1
(Id,Code,Name,ShortName,SupplierType,IsCanClaim,Status,Remark,CreatorName,CreateTime
)
select b.id,b.code,b.name,'',1,1,1,'2014-10-10导入','Admin',sysdate from 
tmpdata.partssupplier141010 a 
inner join company b on a.供应商编号 = b.code

insert into PartsSupplier select * from tmpdata.partssupplier141010_1;

create table tmpdata.BranchSupplierRelation141010 as select * from BranchSupplierRelation where rownum<1;
insert into tmpdata.BranchSupplierRelation141010
  (Id,
   BranchId,
   PartsSalesCategoryId,
   SupplierId,
   BusinessCode,
   BusinessName,
   PurchasingCycle,
   PartsManagementCostGradeId,
   PartsClaimCoefficient,
   ClaimPriceCategory,
   LaborUnitPrice,
   IfSPByLabor,
   LaborCoefficient,
   IfHaveOutFee,
   OldPartTransCoefficient,
   OutServiceCarUnitPrice,
   OutSubsidyPrice,
   Status,
   CreatorName,
   CreateTime,
   Remark)
  select s_BranchSupplierRelation.Nextval,
         8481,
         b.id,
         c.id,
         a.业务编码,
         a.业务名称,
         null,
         d.id,
         --a.工时索赔系数,
         null,1,
         null,
         DECODE(A.是否按工时单价索赔,'是',1,'否',0),
         工时索赔系数,
         decode(a.是否存在外出费用,'是',1,'否',0),
         round(旧件运费系数, 2),
         a.外出里程单价,
         a.外出人员单价,
         1,       
         'Admin',
         sysdate,
         '2014-07-09导入'
    from tmpdata.partssupplier141010 a
   cross join partssalescategory b
   inner join company c
      on c.code = a.供应商编号
    LEFT JOIN partsmanagementcostgrade d
      on d.NAME like '%供应商管理费等级%' AND D.PARTSSALESCATEGORYID=B.ID
   where 1 = 1;
insert into BranchSupplierRelation select * from  tmpdata.BranchSupplierRelation141010;
/*
select * from tmpdata.partssupplier141010 a --949
cross join partssalescategory b --1898
 inner join company c
      on c.code = a.供应商编号
       LEFT JOIN partsmanagementcostgrade d 
      on d.NAME like '%供应商管理费等级%' AND D.PARTSSALESCATEGORYID=B.ID where a.业务编码 is null
     select * from tmpdata.partssupplier141010 a where a.供应商编号 in('FT000123','FT000953')  for update
FT000123
FT000953
FT000123
FT000953
*/
