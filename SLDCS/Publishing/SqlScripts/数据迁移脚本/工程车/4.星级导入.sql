create table tmpdata.GradeCoefficient140918(brand varchar2(100),name varchar2(100),Coefficient number(9,2));

create table tmpdata.gradecoefficient140918_1 as select * from gradecoefficient where rownum<1;
insert into tmpdata.gradecoefficient140918_1
  (id,
   branchid,
   branchname,
   partssalescategoryid,
   partssalescategoryname,
   grade,
   coefficient,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_gradecoefficient.nextval,
         '',
         '',
         (select id from partssalescategory where name = t.brand),
         t.brand,
         t.name,
         t.coefficient,
         1,
         1,
         'Admin',
         sysdate
    from tmpdata.GradeCoefficient140918 t

insert into gradecoefficient
  select * from tmpdata.gradecoefficient140918_1;
