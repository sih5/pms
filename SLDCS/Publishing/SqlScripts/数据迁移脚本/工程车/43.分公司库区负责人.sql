/*select * from tmpdata.warehouseareamanager140929 where warehousecode is not null
select * from tmpdata.warehouseareamanager140929_1
*/
create table TMPDATA.WAREHOUSEAREAMANAGER140929
(
  WAREHOUSECODE     VARCHAR2(50),
  WAREHOUSENAME     VARCHAR2(100),
  WAREHOUSEAREACODE VARCHAR2(50),
  PERSONNNELCODE    VARCHAR2(50),
  PERSONNELNAME     VARCHAR2(100)
)
create table  tmpdata.warehouseareamanager140929_1  as select * from warehouseareamanager where rownum<1;
insert into tmpdata.warehouseareamanager140929_1
  (Id, WarehouseAreaId, ManagerId, CreatorName, CreateTime)

  select s_warehouseareamanager.nextval,
         (select Warehousearea.id
            from Warehousearea inner join warehouse on warehouse.id=Warehousearea.Warehouseid
           where Warehousearea.Code = warehouseareamanager140929.WAREHOUSECODE
             and Warehousearea.Areakind = 1 and warehouse.code=warehouseareamanager140929.warehousecode),
         (select id
            from gcsecurity.personnel t
           where t.loginid = warehouseareamanager140929.PERSONNNELCODE and t.enterpriseid=8481),
         'admin',
         sysdate
    from tmpdata.warehouseareamanager140929
    where exists(select 1 from gcsecurity.personnel where personnel.loginid=warehouseareamanager140929.personnnelcode);
    insert into warehouseareamanager select * from tmpdata.warehouseareamanager140929_1;/*
select * from tmpdata.warehouseareamanager140929 t
where exists(select 1 from gcsecurity.personnel where personnel.loginid=t.personnnelcode)*/
