

create table tmpdata.purchasesaleprice140709 (code varchar2(100),name varchar2(100),suppliercode  varchar2(100),suppliername varchar2(100),purchaseprice varchar2(100),saleprice varchar2(100) )

create table tmpdata.PartsSalesPrice141028 as select * from PartsSalesPrice where rownum<1; 
--130469
insert into tmpdata.PartsSalesPrice141028
  (id,
   branchid,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   sparepartid,
   sparepartcode,
   sparepartname,
   ifclaim,
   salesprice,
   pricetype,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_PartsSalesPrice.Nextval,
         8481,
       (select id
            from partssalescategory
           where partssalescategory.code =decode (t.syscode,'nf','105011','bf','106011')) ,
         (select code
            from partssalescategory
           where partssalescategory.code =decode (t.syscode,'nf','105011','bf','106011')),
         (select name
            from partssalescategory
           where partssalescategory.code =decode (t.syscode,'nf','105011','bf','106011')),
         v.id,
         v.code,
         v.name,
         1,
         nvl(t.wholesaleprice, 0),
         1,
         1,
         1,
         'Admin',
         sysdate
    from (select distinct a.materialcode,wholesaleprice,syscode from tmpdata.tmaterialprices_141028_nfbf a) t
   inner join sparepart v
      on t.materialcode = v.code;
  /* inner join yxdcs.partsbranch s
      on s.partid = v.id*/;
select count(*)from tmpdata.tmaterialprices_141028_nfbf 98340
select count(*) from tmpdata.PartsSalesPrice141028 98338
insert into PartsSalesPrice select * from tmpdata.PartsSalesPrice141028;

create table tmpdata.PartsSalesPriceHistory141028 as select * from PartsSalesPriceHistory where rownum<1;

insert into tmpdata.PartsSalesPriceHistory141028 (id,
   branchid,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   sparepartid,
   sparepartcode,
   sparepartname,
   ifclaim,
   salesprice,
   pricetype,
   status,
   creatorid,
   creatorname,
   createtime)
   select s_PartsSalesPriceHistory.Nextval,branchid,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   sparepartid,
   sparepartcode,
   sparepartname,
   ifclaim,
   salesprice,
   pricetype,
   status,1,'Admin',sysdate from tmpdata.PartsSalesPrice141028 t;
   insert into PartsSalesPriceHistory select * from tmpdata.PartsSalesPriceHistory141028;
   select * from tmpdata.PartsSalesPriceChange140709 for update;
   create table tmpdata.PartsSalesPriceChange141028 as select * From PartsSalesPriceChange where rownum<1;
   create table tmpdata.PartsSalesPriceChangeDt141028 as select * from PartsSalesPriceChangeDetail where rownum<1;
21  106011  工程车(北区)
22  105011  工程车(南区)

   
insert into tmpdata.PartsSalesPriceChange141028
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,status)
  values( s_PartsSalesPriceChange.Nextval,
         8481,
         'PSPC201410280001',
         22,
         '105011',
         '工程车(南区)',
         
         1,
         1,
         'Admin',
         sysdate ,3);
         
         insert into tmpdata.PartsSalesPriceChangeDt141028
           (id,
            parentid,
            sparepartid,
            sparepartcode,
            sparepartname,
            salesprice,
            pricetype)
           select s_PartsSalesPriceChangeDetail.Nextval,
                  s_PartsSalesPriceChange.Currval,
                  t.sparepartid,
                  t.sparepartcode,
                  t.sparepartname,
                  t.salesprice,
                  1
             from tmpdata.PartsSalesPrice141028 t
            where t.partssalescategoryid = 22;
            
            
insert into tmpdata.PartsSalesPriceChange141028
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,status)
  values( s_PartsSalesPriceChange.Nextval,
         8481,
         'PSPC201410280002',
         21,
         '106011',
         '工程车(北区)',
         
         1,
         1,
         'Admin',
         sysdate ,3);
         
         insert into tmpdata.PartsSalesPriceChangeDt141028
           (id,
            parentid,
            sparepartid,
            sparepartcode,
            sparepartname,
            salesprice,
            pricetype)
           select s_PartsSalesPriceChangeDetail.Nextval,
                  s_PartsSalesPriceChange.Currval,
                  t.sparepartid,
                  t.sparepartcode,
                  t.sparepartname,
                  t.salesprice,
                  1
             from tmpdata.PartsSalesPrice141028 t
            where t.partssalescategoryid = 21;
             
            insert into PartsSalesPriceChange select * from tmpdata.PartsSalesPriceChange141028;
                        insert into PartsSalesPriceChangeDetail select * from tmpdata.PartsSalesPriceChangeDt141028;
                        
create table tmpdata.PartsPurchasePricing141028 as select * from PartsPurchasePricing where rownum<1;      
/*
create table tmpdata.newoldrelate140709 (newcode varchar2(100),newname varchar2(100),oldcode varchar2(100),oldname varchar2(100));
select * from tmpdata.newoldrelate140709;
*/
insert into tmpdata.PartsPurchasePricing141028
  (id,
   branchid,
   partid,
   partssupplierid,
   partssalescategoryid,
   partssalescategoryname,
   validfrom,
   validto,
   pricetype,
   purchaseprice,
   status)
  select s_PartsPurchasePricing.Nextval,
         8481,
         v.id,
         w.id,
         (select id
            from partssalescategory
           where partssalescategory.code =decode (t.syscode,'nf','105011','bf','106011')) ,
           (select name
            from partssalescategory
           where partssalescategory.code =decode (t.syscode,'nf','105011','bf','106011')),
         to_date('2014-1-1', 'yyyy-mm-dd'),
         to_date('2014-12-31', 'yyyy-mm-dd'),
         1,
         nvl(t.compactprice, 0),
         2
    from tmpdata.tmaterialprices_141028_nfbf t
   inner join sparepart v
      on t.materialcode = v.code
   inner join partssupplier w
      on w.code =t.ftsupplycode ;

21  106011  
22  105011          
       insert into PartsPurchasePricing select * from  tmpdata.PartsPurchasePricing141028;
select * from tmpdata.PartsPurchasePricingC140709
create table tmpdata.PartsPurchasePricingC141028 as select * from PartsPurchasePricingChange where rownum<1; 
create table tmpdata.PartsPurchasePricingD141028 as select * from PartsPurchasePricingDetail where rownum<1;
insert into tmpdata.PartsPurchasePricingC141028
  (id,
   code,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   creatorid,
   creatorname,
   createtime,status)
values
  (s_PartsPurchasePricingChange.Nextval,'PPPC201410280001', 8481, 21, '工程车(北区)', 1, 'Admin', sysdate,2);

insert into tmpdata.PartsPurchasePricingD141028
  (id,
   parentid,
   partid,
   partcode,
   partname,
   supplierid,
   suppliercode,
   suppliername,
   pricetype,
   price,VALIDFROM,validto)
  select s_PartsPurchasePricingDetail.Nextval,
         s_PartsPurchasePricingChange.Currval,
         t.partid,
         (select code from sparepart where id = t.partid),
         (select name from sparepart where id = t.partid),
         t.partssupplierid,
         (select code from partssupplier where id = t.partssupplierid),
         (select name from partssupplier where id = t.partssupplierid),
         1,
         t.purchaseprice, to_date('2014-1-1', 'yyyy-mm-dd'),
         to_date('2014-12-31', 'yyyy-mm-dd')
    from tmpdata.PartsPurchasePricing141028 t where t.partssalescategoryid=21;

insert into tmpdata.PartsPurchasePricingC141028
  (id,
   code,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   creatorid,
   creatorname,
   createtime,status)
values
  (s_PartsPurchasePricingChange.Nextval,'PPPC201410280002', 8481, 22, '工程车(南区)', 1, 'Admin', sysdate,2);

insert into tmpdata.PartsPurchasePricingD141028
  (id,
   parentid,
   partid,
   partcode,
   partname,
   supplierid,
   suppliercode,
   suppliername,
   pricetype,
   price,VALIDFROM,validto)
  select s_PartsPurchasePricingDetail.Nextval,
         s_PartsPurchasePricingChange.Currval,
         t.partid,
         (select code from sparepart where id = t.partid),
         (select name from sparepart where id = t.partid),
         t.partssupplierid,
         (select code from partssupplier where id = t.partssupplierid),
         (select name from partssupplier where id = t.partssupplierid),
         1,
         t.purchaseprice, to_date('2014-1-1', 'yyyy-mm-dd'),
         to_date('2014-12-31', 'yyyy-mm-dd')
    from tmpdata.PartsPurchasePricing141028 t where t.partssalescategoryid= 22;
    
 
    insert into PartsPurchasePricingChange select * from tmpdata.PartsPurchasePricingC141028;
    insert into PartsPurchasePricingDetail select * from tmpdata.PartsPurchasePricingD141028;


create table tmpdata.stdprice141028_1 as select * from partsplannedprice where rownum<1;

insert into tmpdata.stdprice141028_1
  (id,
   ownercompanyid,
   ownercompanytype,
   partssalescategoryid,
   partssalescategoryname,
   sparepartid,
   plannedprice,
   creatorid,
   creatorname,
   createtime)
select s_partsplannedprice.nextval,
 8481,
 1,
(select id
            from partssalescategory
           where partssalescategory.code =decode (t.syscode,'nf','105011','bf','106011')) ,
    (select name
            from partssalescategory
           where partssalescategory.code =decode (t.syscode,'nf','105011','bf','106011')),
 s.id,
 t.stdprice,
 1,
 'Admin',
 sysdate
  from (select distinct a.materialcode,stdprice,syscode from tmpdata.tmaterialprices_141028_nfbf a) t
 inner join sparepart s
    on s.code = t.materialcode;
 
 insert into partsplannedprice select * from  tmpdata.stdprice141028_1;
 create table tmpdata.PlannedPriceApp141028 as select * from PlannedPriceApp where rownum<1;
create table tmpdata.PlannedPriceAppDetail141028 as select * from PlannedPriceAppDetail where rownum<1;

insert into tmpdata.PlannedPriceApp141028
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,RecordStatus,PlannedExecutionTime,ActualExecutionTime)
values
  (s_PlannedPriceApp.Nextval,
   8481,
   '2230',
   '北汽福田汽车股份有限公司工程车事业部', 21,
   '工程车(北区)',
   'PPA2203201410280001',
   0,
   0,
   0,
   3,
   1,
   'Admin',
   sysdate,3,sysdate,sysdate);
  
    insert into tmpdata.PlannedPriceAppDetail141028
      (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             s_PlannedPriceApp.Currval,
             t.sparepartid,
             (select code
                from sparepart
               where t.sparepartid = sparepart.id),
             (select name
                from sparepart
               where t.sparepartid = sparepart.id),t.plannedprice,0,0,0
        from tmpdata.stdprice141028_1 t where t.partssalescategoryid=21;
   
   insert into tmpdata.PlannedPriceApp141028
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,RecordStatus,PlannedExecutionTime,ActualExecutionTime)
values
  (s_PlannedPriceApp.Nextval,
    8481,
   '2230',
   '北汽福田汽车股份有限公司工程车事业部', 22,
   '工程车(南区)',
   'PPA2203201410280002',
   0,
   0,
   0,
   3,
   1,
   'Admin',
   sysdate,3,sysdate,sysdate);
    insert into tmpdata.plannedpriceappdetail141028
      (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             s_PlannedPriceApp.Currval,
             t.sparepartid,
             (select code
                from sparepart
               where t.sparepartid = sparepart.id),
             (select name
                from sparepart
               where t.sparepartid = sparepart.id),t.plannedprice,0,0,0
        from tmpdata.stdprice141028_1 t where t.partssalescategoryid=22;
   
   
insert into  PlannedPriceApp select * from  tmpdata.PlannedPriceApp141028;
insert into  plannedpriceappdetail select * from tmpdata.plannedpriceappdetail141028;



create table tmpdata.partsretailguideprice141028 as select * from partsretailguideprice where rownum<1; 
insert into tmpdata.partsretailguideprice141028
  (id,
   branchid,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   sparepartid,
   sparepartcode,
   sparepartname,
   retailguideprice,
   creatorid,
   creatorname,
   createtime,status)
  select s_partsretailguideprice.Nextval,
         8481,
        (select id
            from partssalescategory
           where partssalescategory.code =decode (t.syscode,'nf','105011','bf','106011')) ,
           (select code
            from partssalescategory
           where partssalescategory.code =decode (t.syscode,'nf','105011','bf','106011')),
    (select name
            from partssalescategory
           where partssalescategory.code =decode (t.syscode,'nf','105011','bf','106011')),
         v.id,
         v.code,
         v.name,
         nvl(t.retailprice, 0),
         1,
         'Admin',
         sysdate,1
    from (select distinct a.materialcode,retailprice,syscode from tmpdata.tmaterialprices_141028_nfbf a) t
   inner join sparepart v
      on t.materialcode = v.code
  select * from tmpdata.tmaterialprices_141028_nfbf t 
  where not exists(select 1 from sparepart where t.materialcode=sparepart.code)
  
insert into partsretailguideprice select * from tmpdata.partsretailguideprice141028;


create table tmpdata.PRetailGuidePriceHistory141028 as select * from PartsRetailGuidePriceHistory where rownum<1;

insert into tmpdata.PRetailGuidePriceHistory141028
  (id,
   branchid,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   sparepartid,
   sparepartcode,
   sparepartname,
   retailguideprice,
   validationtime,
   expiretime,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_PartsRetailGuidePriceHistory.Nextval,
         branchid,
         partssalescategoryid,
         partssalescategorycode,
         partssalescategoryname,
         sparepartid,
         sparepartcode,
         sparepartname,
         retailguideprice,
         validationtime,
         expiretime,
         status,
         1,
         'Admin',
         sysdate
    from tmpdata.partsretailguideprice141028 t;
insert into PartsRetailGuidePriceHistory
  select * from tmpdata.PRetailGuidePriceHistory141028;
  21    
22    
   create table tmpdata.PSalesPriceChange141028_1rp as select * From PartsSalesPriceChange where rownum<1;
   create table tmpdata.PSalesPriceChangeDt141028_1rp as select * from PartsSalesPriceChangeDetail where rownum<1;
   insert into tmpdata.PSalesPriceChange141028_1rp
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,status)
  values( s_PartsSalesPriceChange.Nextval,
         8481,
         'PSPC201410281001',
         21,
         '106011',
         '工程车(北区)',
         
         1,
         1,
         'Admin',
         sysdate ,1);
         
         insert into tmpdata.PSalesPriceChangeDt141028_1rp
           (id,
            parentid,
            sparepartid,
            sparepartcode,
            sparepartname,
            salesprice,
            RETAILGUIDEPRICE ,
            pricetype)
           select s_PartsSalesPriceChangeDetail.Nextval,
                  s_PartsSalesPriceChange.Currval,
                  t.sparepartid,
                  t.sparepartcode,
                  t.sparepartname,0,
                  t.retailguideprice,
                  1
             from tmpdata.partsretailguideprice141028 t
            where t.partssalescategoryid = 21;
            
            
insert into tmpdata.PSalesPriceChange141028_1rp
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,status)
  values( s_PartsSalesPriceChange.Nextval,
         8481,
         'PSPC201410281002',
         22,
         '105011',
         '工程车(南区)',
         
         1,
         1,
         'Admin',
         sysdate ,3);
         
        insert into tmpdata.PSalesPriceChangeDt141028_1rp
           (id,
            parentid,
            sparepartid,
            sparepartcode,
            sparepartname,
            salesprice,
            RETAILGUIDEPRICE ,
            pricetype)
           select s_PartsSalesPriceChangeDetail.Nextval,
                  s_PartsSalesPriceChange.Currval,
                  t.sparepartid,
                  t.sparepartcode,
                  t.sparepartname,0,
                  t.retailguideprice,
                  1
             from tmpdata.partsretailguideprice141028 t
            where t.partssalescategoryid = 22;
            
            insert into PartsSalesPriceChange select * from tmpdata.PSalesPriceChange141028_1rp;
            insert into PartsSalesPriceChangeDetail select * from  tmpdata.PSalesPriceChangeDt141028_1rp;
