--插入临时表
create table TMPDATA.NODEACTION141022
(
  LV1    VARCHAR2(100),
  LV2    VARCHAR2(100),
  LV3    VARCHAR2(100),
  ACTION VARCHAR2(100),
  ROLE   VARCHAR2(100)
)
create table tmpdata.rule141022
as
select * from rule where 1=0;
--插入角色
create table tmpdata.role141022 as select * from role where rownum<1;
insert into tmpdata.role141022
  (id,
   enterpriseid,
   name,
   isadmin,
   status,
   remark,
   creatorid,
   creatorname,
   createtime)
  select s_role.nextval,
         8481,
         role,
         0,
         1,
         '初始化角色',
         1,
         '系统管理员',
         sysdate
    from (select distinct role from tmpdata.NODEACTION141022);
insert into role select * from tmpdata.role141022;
select * from role where id=15685 for update
/*select * from action a
left join page p on p.id = a.pageid
where a.name='驳回' and p.name='配件外采管理'
group by a.name,p.name having count(*)>1


delete from action where id=1083*/

--插入授权
--
insert into tmpdata.rule141022
  (id, roleid, nodeid)
  select s_rule.nextval,
         (select id
            from role r
           where r.name = t.role
             and r.enterpriseid =
                 8481) roleid,
         (select n.id
            from page p
            left join action a
              on p.id = a.pageid
            left join node n
              on n.categoryid = a.id
             and n.categorytype = 1
           where a.name = t.action
             and p.name = t.lv3) as nodeid
    from tmpdata.nodeaction141022 t
   where t.action is not null
     and exists (select n.id
            from page p
            left join action a
              on p.id = a.pageid
            left join node n
              on n.categoryid = a.id
             and n.categorytype = 1
           where a.name = t.action
             and p.name = t.lv3);
--
insert into tmpdata.rule141022
  (id, roleid, nodeid)
  select s_rule.nextval,
         (select id
            from role r
           where r.name = t.role
             and r.enterpriseid =8481) roleid,
         (select n.id
            from page p
           left join node n
              on n.categoryid = p.id
             and n.categorytype = 0
           where p.name = t.lv3) as nodeid 
    from tmpdata.nodeaction141022 t
   where t.action = '查询'
     and exists (select n.id
            from page p
           left join node n
              on n.categoryid = p.id
             and n.categorytype = 0
           where p.name = t.lv3)
           and t.lv3 not in (select p.name from page p
group by p.name having count(*)>1);
--
insert into rule
  (id, roleid, nodeid)
  select id, roleid, nodeid
    from tmpdata.rule141022 t
   where not exists (select 1
            from rule v
           where v.nodeid = t.nodeid
             and v.roleid = t.roleid);

create table tmpdata.rule141023 as select * from rule where rownum<1;
insert into tmpdata.rule141023
select s_rule.nextval,
15685,node.id
from page inner join node on node.categoryid=page.id and node.categorytype=0 where page .name IN(
'大区管理',
'大区与市场部关系',
'服务站/专卖店分品牌信息管理',
'服务站/专卖店企业信息管理',
'大区与人员关系'
)
insert into tmpdata.rule141023
select s_rule.nextval,
15685,
node.id
from page inner join action on action.pageid=page.id 
inner join node on node.categoryid=action.id and node.categorytype=1
where page.name='服务站/专卖店分品牌信息管理' and action .name='查看明细'

insert into rule select * from tmpdata.rule141023
--人员信息
/*select *  
 from tmpdata.nodeaction141022 t --57
   where t.action = '查询'
     and not exists (select n.id
            from page p
           left join node n
              on n.categoryid = p.id
             and n.categorytype = 0
           where p.name = t.lv3);--43
           select *
            from tmpdata.nodeaction141022 t
   where t.action <> '查询'
     and not exists (select n.id
            from page p
            left join action a
              on p.id = a.pageid
            left join node n
              on n.categoryid = a.id
             and n.categorytype = 1
           where a.name = t.action
             and p.name = t.lv3);8
             select * from page where name like'%销售%'
             
             select * from  page p
            left join action a
              on p.id = a.pageid
            left join node n
              on n.categoryid = a.id
             and n.categorytype = 1 where p.name='服务站/专卖店分公司信息管理'*/
