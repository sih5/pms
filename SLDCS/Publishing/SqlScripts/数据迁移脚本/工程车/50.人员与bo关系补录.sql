create table tmpdata.rolepersonnel141027 (role  varchar2(100),name varchar2(100),loginid varchar2(100))
create table tmpdata.personnel141027 as select * from personnel where rownum<1;
insert into  tmpdata.personnel141027 (id,enterpriseid,loginid,password,name,status,creatorid,creatorname,createtime,passwordmodifytime)
select s_personnel.nextval,8481,t.loginid,(select passwordpolicy.defaultpassword from passwordpolicy),t.name,1,1,'Admin',sysdate,sysdate from  tmpdata.rolepersonnel141027 t;

insert into gcsecurity.personnel select * from tmpdata.personnel141027 ;

create table tmpdata.rolepersonnel141027_1 as select * from rolepersonnel where rownum<1;
insert into tmpdata.rolepersonnel141027_1
  (id, roleid, personnelid)
  select s_rolepersonnel.nextval,
         (select id
            from role
           where enterpriseid = 8481
             and name = t.role),
         v.id
    from tmpdata.rolepersonnel141027 t
   inner join tmpdata.personnel141027 v
      on t.loginid = v.loginid

insert into gcsecurity.rolepersonnel select * from tmpdata.rolepersonnel141027_1 ;


create table tmpdata.personnelbrandrel141027_1 as select * from PersonSalesCenterLink where rownum<1;

insert into tmpdata.personnelbrandrel141027_1
  (id,
   companyid,
   personid, 
   partssalescategoryid,
   persontype,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_PersonSalesCenterLink.Nextval,
         8481,
         v.id,
         (select id from partssalescategory where name = a.brandname),1,
         1,
         1,
         'Admin',
         sysdate
   from tmpdata.personnelnotexists140929 t
 inner join gcsecurity.personnel v
    on t.personnelid = v.loginid
   and v.enterpriseid = 8481
 inner join tmpdata.personnelbrandrel140919 a
    on a.personnelid = t.personnelid
 where exists (select name
          from gcsecurity.personnel
         where t.personnelid = personnel.loginid
           and personnel.enterpriseid = 8481)
   and not exists (select 1
          from tmpdata.personnelbrandrel140919_1 b
          inner join partssalescategory c on c.id=b.partssalescategoryid
         where b.personid = v.id and a.brandname=c.name)
   and t.type = '人员与品牌关系';
insert into PersonSalesCenterLink select * from tmpdata.personnelbrandrel141027_1 ;
/*
select t.*,v.id--,a.brandname
  from tmpdata.personnelnotexists140929 t
 inner join gcsecurity.personnel v
    on t.personnelid = v.loginid
   and v.enterpriseid = 8481
 inner join tmpdata.personnelbrandrel140919 a
    on a.personnelid = t.personnelid
 where exists (select name
          from gcsecurity.personnel
         where t.personnelid = personnel.loginid
           and personnel.enterpriseid = 8481)
   and not exists (select 1
          from tmpdata.personnelbrandrel140919_1 b
         where b.personid = v.id)
   and t.type = '人员与品牌关系'
*/
create table tmpdata.MDptPersonnelRelation141027 as  select * from MarketDptPersonnelRelation where rownum<1;
insert into tmpdata.MDptPersonnelRelation141027
  (id, branchid, marketdepartmentid, personnelid, status, type)
  select s_MarketDptPersonnelRelation.Nextval,
         '8481',
         (select id from marketingdepartment where code = a.marketdepcode and name=a.marketdepname),
         v.id,
         1,
         decode(a.personneltype,
                '服务经理',
                1,
                '服务后台人员',
                2,
                '配件后台人员',
                3)
    from tmpdata.personnelnotexists140929 t
   inner join gcsecurity.personnel v
      on t.personnelid = v.loginid
     and v.enterpriseid = 8481
   inner join tmpdata.MarketDepRegion140919 a
      on a.personnelloginid = t.personnelid
      inner join marketingdepartment on marketingdepartment.code = a.marketdepcode and marketingdepartment.name=a.marketdepname
     and not exists (select 1
            from MarketDptPersonnelRelation b
           where b.personnelid = v.id and marketingdepartment.id=b.marketdepartmentid)
     and t.type = '人员与市场部关系';
     insert into tmpdata.MDptPersonnelRelation141027
  (id, branchid, marketdepartmentid, personnelid, status, type)
  select s_MarketDptPersonnelRelation.Nextval,
         '8481',
         (select id from marketingdepartment where code = a.marketdepcode and name=a.marketdepname),
         v.id,
         1,
         decode(a.personneltype,
                '服务经理',
                1,
                '服务后台人员',
                2,
                '配件后台人员',
                3)
    from tmpdata.personnelnotexists140929 t
   inner join gcsecurity.personnel v
      on t.personnelid = v.loginid
     and v.enterpriseid = 8481
   inner join tmpdata.marketdepregion140930 a
      on a.personnelloginid = t.personnelid
      inner join marketingdepartment on marketingdepartment.code = a.marketdepcode and marketingdepartment.name=a.marketdepname
     and not exists (select 1
            from MarketDptPersonnelRelation b
           where b.personnelid = v.id and marketingdepartment.id=b.marketdepartmentid)
     and t.type = '人员与市场部关系';
     
     
delete tmpdata.MDptPersonnelRelation141027
 where MDptPersonnelRelation141027.rowid not in
       (select min(MDptPersonnelRelation141027.rowid)
          from tmpdata.MDptPersonnelRelation141027
         group by marketdepartmentid,personnelid);

     insert into MarketDptPersonnelRelation select * from tmpdata.MDptPersonnelRelation141027;
    -- delete from MarketDptPersonnelRelation where exists(select * from tmpdata.MDptPersonnelRelation141027  where MDptPersonnelRelation141027.id=MarketDptPersonnelRelation.id)
