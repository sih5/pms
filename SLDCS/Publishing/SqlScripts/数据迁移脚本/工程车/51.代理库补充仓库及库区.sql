-----工程车更改代理库仓库名称
update gcdcs.warehouse set name ='工程车配件杭州代理库'   where name ='瑞沃配件杭州代理库';
update gcdcs.warehouse set name ='工程车配件郑州代理库'   where name ='瑞沃配件郑州代理库';
update gcdcs.warehouse set name ='工程车配件西安代理库'   where name ='西安瑞沃仓库';
update gcdcs.warehouse set name ='工程车配件哈尔滨代理库'   where name ='瑞沃配件哈尔滨代理库';
update gcdcs.warehouse set name ='工程车配件沈阳代理库'   where name ='沈阳代理库';
update gcdcs.warehouse set name ='工程车配件长春代理库'   where name ='瑞沃配件长春中心库';
update gcdcs.warehouse set name ='工程车配件太原代理库'   where name ='瑞沃配件太原代理库';
update gcdcs.warehouse set name ='工程车配件合肥代理库'   where name ='瑞沃配件合肥代理库';
update gcdcs.warehouse set name ='工程车配件北京代理库'   where name ='瑞沃配件北京代理库';
update gcdcs.warehouse set name ='工程车配件镇江代理库'   where name ='镇江代理库';
update gcdcs.warehouse set name ='工程车配件兰州代理库'   where name ='瑞沃配件兰州新代理库';
update gcdcs.warehouse set name ='工程车配件成都代理库'   where name ='瑞沃配件成都代理库';
update gcdcs.warehouse set name ='工程车配件南昌代理库'   where name ='瑞沃配件南昌代理库';
update gcdcs.warehouse set name ='工程车配件泉州代理库'   where name ='瑞沃配件泉州代理库';
update gcdcs.warehouse set name ='工程车配件海口代理库'   where name ='瑞沃配件海南代理库';
update gcdcs.warehouse set name ='工程车配件武汉代理库'   where name ='瑞沃配件武汉代理库';
update gcdcs.warehouse set name ='工程车配件广州代理库'   where name ='瑞沃配件广州代理库';
update gcdcs.warehouse set name ='工程车配件重庆代理库'   where name ='瑞沃配件重庆代理库';
update gcdcs.warehouse set name ='工程车配件昆明代理库'   where name ='瑞沃配件昆明代理库';
update gcdcs.warehouse set name ='工程车配件南宁代理库'   where name = '瑞沃配件南宁代理库';
update gcdcs.warehouse set name ='工程车配件贵阳代理库'   where name ='瑞沃配件贵阳代理库';
-----update gcdcs.warehouse set name ='工程车配件榆林代理库'   where name ='工程车配件榆林代理库';

update gcdcs.warehouse set code='HZDLK'where 	name ='工程车配件杭州代理库';
update gcdcs.warehouse set code='ZZDLK'where 	name ='工程车配件郑州代理库';
update gcdcs.warehouse set code='XADLK'where 	name ='工程车配件西安代理库';
update gcdcs.warehouse set code='HEBDLK'where 	name ='工程车配件哈尔滨代理库';
update gcdcs.warehouse set code='SYDLK'where 	name ='工程车配件沈阳代理库';
update gcdcs.warehouse set code='CCDLK'where 	name ='工程车配件长春代理库';
update gcdcs.warehouse set code='TYDLK'	where 	name ='工程车配件太原代理库';
update gcdcs.warehouse set code='HFDLK'	where 	name ='工程车配件合肥代理库';
update gcdcs.warehouse set code='BJDLK'	where 	name ='工程车配件北京代理库';
update gcdcs.warehouse set code='ZJDLK'	where 	name ='工程车配件镇江代理库';
update gcdcs.warehouse set code='LZDLK'	where 	name ='工程车配件兰州代理库';
update gcdcs.warehouse set code='CDDLK'	where 	name ='工程车配件成都代理库';
update gcdcs.warehouse set code='NCDLK'	where 	name ='工程车配件南昌代理库';
update gcdcs.warehouse set code='QZDLK'	where 	name ='工程车配件泉州代理库';
update gcdcs.warehouse set code='HNDLK'	where 	name ='工程车配件海口代理库';
update gcdcs.warehouse set code='WHDLK'	where 	name ='工程车配件武汉代理库';
update gcdcs.warehouse set code='GZDLK'	where 	name ='工程车配件广州代理库';
update gcdcs.warehouse set code='CQDLK'	where 	name ='工程车配件重庆代理库';
update gcdcs.warehouse set code='KMDLK'	where 	name ='工程车配件昆明代理库';
update gcdcs.warehouse set code='NNDLK'	where 	name ='工程车配件南宁代理库';
update gcdcs.warehouse set code='GYDLK'	where 	name ='工程车配件贵阳代理库';

insert into gcdcs.warehouse (ID,
   CODE,
   NAME,
   TYPE,
   STATUS,
   STORAGESTRATEGY,
   BRANCHID,
   STORAGECOMPANYID,
   STORAGECOMPANYTYPE,
   WMSINTERFACE,
   creatorid,
   creatorname,
   createtime)values(s_warehouse.nextval,'YLDLK','工程车配件榆林代理库',2,1,
         1,
         8481,
         (select id from company where code ='FT001639'),
         3,
         0,
         1,
         'Admin',
         sysdate)

insert into gcdcs.warehousearea
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
values
  (s_warehousearea.nextval,
   (select id
      from warehouse
     where warehouse.code = 'YLDLK'
       and branchid = 8481) ,
   null,
   'YLDLK',
   null,
   89,
   1,
   '南北方代理库库区库位批量处理',
   1,
   1,
   'Admin',
   sysdate);
   SELECT * FROM GCDCS.WAREHOUSEAREA WHERE WAREHOUSEID=681 for update
   
insert into gcdcs.warehousearea
   (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
values
  (s_warehousearea.nextval,
   (select id
      from warehouse
     where warehouse.code = 'YLDLK'
       and branchid = 8481) ,
   (select id from WAREHOUSEAREA WHERE WAREHOUSEID=681),
   'Y',
   null,
   89,
   1,
   '南北方代理库库区库位批量处理',
   2,
   1,
   'Admin',
   sysdate);
