-----工程车更改代理库仓库名称
update gcdcs.warehouse set name ='工程车配件杭州代理库'   where name ='瑞沃配件杭州代理库';
update gcdcs.warehouse set name ='工程车配件郑州代理库'   where name ='瑞沃配件郑州代理库';
update gcdcs.warehouse set name ='工程车配件西安代理库'   where name ='西安瑞沃仓库';
update gcdcs.warehouse set name ='工程车配件哈尔滨代理库'   where name ='瑞沃配件哈尔滨代理库';
update gcdcs.warehouse set name ='工程车配件沈阳代理库'   where name ='沈阳代理库';
update gcdcs.warehouse set name ='工程车配件长春代理库'   where name ='瑞沃配件长春中心库';
update gcdcs.warehouse set name ='工程车配件太原代理库'   where name ='瑞沃配件太原代理库';
update gcdcs.warehouse set name ='工程车配件合肥代理库'   where name ='瑞沃配件合肥代理库';
update gcdcs.warehouse set name ='工程车配件北京代理库'   where name ='瑞沃配件北京代理库';
update gcdcs.warehouse set name ='工程车配件镇江代理库'   where name ='镇江代理库';
update gcdcs.warehouse set name ='工程车配件兰州代理库'   where name ='瑞沃配件兰州新代理库';
update gcdcs.warehouse set name ='工程车配件成都代理库'   where name ='瑞沃配件成都代理库';
update gcdcs.warehouse set name ='工程车配件南昌代理库'   where name ='瑞沃配件南昌代理库';
update gcdcs.warehouse set name ='工程车配件泉州代理库'   where name ='瑞沃配件泉州代理库';
update gcdcs.warehouse set name ='工程车配件海口代理库'   where name ='瑞沃配件海南代理库';
update gcdcs.warehouse set name ='工程车配件武汉代理库'   where name ='瑞沃配件武汉代理库';
update gcdcs.warehouse set name ='工程车配件广州代理库'   where name ='瑞沃配件广州代理库';
update gcdcs.warehouse set name ='工程车配件重庆代理库'   where name ='瑞沃配件重庆代理库';
update gcdcs.warehouse set name ='工程车配件昆明代理库'   where name ='瑞沃配件昆明代理库';
update gcdcs.warehouse set name ='工程车配件南宁代理库'   where name = '瑞沃配件南宁代理库';
update gcdcs.warehouse set name ='工程车配件贵阳代理库'   where name ='瑞沃配件贵阳代理库';

update gcdcs.warehouse set code='HZDLK'where   name ='工程车配件杭州代理库';
update gcdcs.warehouse set code='ZZDLK'where   name ='工程车配件郑州代理库';
update gcdcs.warehouse set code='XADLK'where   name ='工程车配件西安代理库';
update gcdcs.warehouse set code='HEBDLK'where   name ='工程车配件哈尔滨代理库';
update gcdcs.warehouse set code='SYDLK'where   name ='工程车配件沈阳代理库';
update gcdcs.warehouse set code='CCDLK'where   name ='工程车配件长春代理库';
update gcdcs.warehouse set code='TYDLK'  where   name ='工程车配件太原代理库';
update gcdcs.warehouse set code='HFDLK'  where   name ='工程车配件合肥代理库';
update gcdcs.warehouse set code='BJDLK'  where   name ='工程车配件北京代理库';
update gcdcs.warehouse set code='ZJDLK'  where   name ='工程车配件镇江代理库';
update gcdcs.warehouse set code='LZDLK'  where   name ='工程车配件兰州代理库';
update gcdcs.warehouse set code='CDDLK'  where   name ='工程车配件成都代理库';
update gcdcs.warehouse set code='NCDLK'  where   name ='工程车配件南昌代理库';
update gcdcs.warehouse set code='QZDLK'  where   name ='工程车配件泉州代理库';
update gcdcs.warehouse set code='HNDLK'  where   name ='工程车配件海口代理库';
update gcdcs.warehouse set code='WHDLK'  where   name ='工程车配件武汉代理库';
update gcdcs.warehouse set code='GZDLK'  where   name ='工程车配件广州代理库';
update gcdcs.warehouse set code='CQDLK'  where   name ='工程车配件重庆代理库';
update gcdcs.warehouse set code='KMDLK'  where   name ='工程车配件昆明代理库';
update gcdcs.warehouse set code='NNDLK'  where   name ='工程车配件南宁代理库';
update gcdcs.warehouse set code='GYDLK'  where   name ='工程车配件贵阳代理库';

insert into gcdcs.warehouse (ID,
   CODE,
   NAME,
   TYPE,
   STATUS,
   STORAGESTRATEGY,
   BRANCHID,
   STORAGECOMPANYID,
   STORAGECOMPANYTYPE,
   WMSINTERFACE,
   creatorid,
   creatorname,
   createtime)values(s_warehouse.nextval,'YLDLK','工程车配件榆林代理库',2,1,
         1,
         8481,
         (select id from company where code ='FT001639'),
         3,
         0,
         1,
         'Admin',
         sysdate)

insert into gcdcs.warehousearea
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
values
  (s_warehousearea.nextval,
   (select id
      from warehouse
     where warehouse.code = 'YLDLK'
       and branchid = 8481) ,
   null,
   'YLDLK',
   null,
   89,
   1,
   '南北方代理库库区库位批量处理',
   1,
   1,
   'Admin',
   sysdate)
   SELECT * FROM GCDCS.WAREHOUSEAREA WHERE WAREHOUSEID=662
   
insert into gcdcs.warehousearea
   (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
values
  (s_warehousearea.nextval,
   (select id
      from warehouse
     where warehouse.code = 'YLDLK'
       and branchid = 8481) ,
   null,
   'Y',
   null,
   89,
   1,
   '南北方代理库库区库位批量处理',
   2,
   1,
   'Admin',
   sysdate);
   
   select distinct t.warehousename,t.warehousecode,t.agentname,t.agentcode,t.areacode from tmpdata.tagentstores_141022_nfbf t
where  exists(select 1 from warehouse v where t.warehousename=v.name)

select * from warehousearea  where warehouseid=616 and areakind=2
select * from warehouse where code='CCDLK'
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件杭州代理库' where warehousename='瑞沃配件杭州代理库'  ;
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件郑州代理库' where warehousename='瑞沃配件郑州代理库';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件西安代理库'  where warehousename='西安瑞沃仓库';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件哈尔滨代理库'  where warehousename='瑞沃配件哈尔滨代理库';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件沈阳代理库'  where warehousename='沈阳代理库';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件长春代理库'  where warehousename='瑞沃配件长春中心库';--
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件太原代理库'  where warehousename='瑞沃配件太原代理库';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件合肥代理库'  where warehousename='瑞沃配件合肥代理库';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件北京代理库'  where warehousename='瑞沃配件北京代理库';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件镇江代理库'  where warehousename='镇江代理库';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件兰州代理库'  where warehousename='瑞沃配件兰州新代理库';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件成都代理库'  where warehousename='成都代理库';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件南昌代理库'  where warehousename='南昌代理库';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件泉州代理库'  where warehousename='泉州运通代理库';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件海口代理库'  where warehousename='海南福田代理库';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件武汉代理库'  where warehousename='武汉知音仓库';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件广州代理库'  where warehousename='广州运通四方';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件重庆代理库'  where warehousename='重庆代理库';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件昆明代理库'  where warehousename='昆明代理库';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件南宁代理库'  where warehousename='DLK111Admin';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件贵阳代理库'  where warehousename='贵阳代理库';
update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件榆林代理库'  where warehousename='瑞沃配件西安仓库榆林分库';

update  tmpdata.tagentstores_141101_nfbf set warehousecode='HZDLK'where   warehousename ='工程车配件杭州代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='ZZDLK'where   warehousename ='工程车配件郑州代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='XADLK'where   warehousename ='工程车配件西安代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='HEBDLK'where   warehousename ='工程车配件哈尔滨代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='SYDLK'where   warehousename ='工程车配件沈阳代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='CCDLK'where   warehousename ='工程车配件长春代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='TYDLK'  where   warehousename ='工程车配件太原代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='HFDLK'  where   warehousename ='工程车配件合肥代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='BJDLK'  where   warehousename ='工程车配件北京代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='ZJDLK'  where   warehousename ='工程车配件镇江代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='LZDLK'  where   warehousename ='工程车配件兰州代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='CDDLK'  where   warehousename ='工程车配件成都代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='NCDLK'  where   warehousename ='工程车配件南昌代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='QZDLK'  where   warehousename ='工程车配件泉州代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='HNDLK'  where   warehousename ='工程车配件海口代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='WHDLK'  where   warehousename ='工程车配件武汉代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='GZDLK'  where   warehousename ='工程车配件广州代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='CQDLK'  where   warehousename ='工程车配件重庆代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='KMDLK'  where   warehousename ='工程车配件昆明代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='NNDLK'  where   warehousename ='工程车配件南宁代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='GYDLK'  where   warehousename ='工程车配件贵阳代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='YLDLK'  where   warehousename ='工程车配件榆林代理库';

create table tmpdata.agentwlrel141027(warehousecode varchar2(100),warehousename varchar2(100),warehouseareacode varchar2(100));

--164568
select count(*) from tmpdata.tagentstores_141101_nfbf a 
inner join tmpdata.agentwlrel141027 b on a.warehousecode=b.warehousecode and a.AREACODE=b.warehouseareacode  --45107
select count(*) from tmpdata.tagentstores_141101_nfbf a inner join tmpdata.agentwlrel141027 b on a.warehousecode=b.warehousecode   --111068
where not exists(select 1 from tmpdata.agentwlrel141027  c where c.warehouseareacode=a.areacode and a.warehousecode=c.warehousecode)

create table tmpdata.warehousearea141101_1 as select * from warehousearea where rownum<1;
insert into tmpdata.warehousearea141101_1(id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
select s_warehousearea.nextval,
       warehouseid,
       parentid,
       locationcode,
       parentid,
       89,
       1,
       '工程车代理库库位批量新增',
       3,
       1,
       'Admin',
       sysdate
  from (select distinct a.warehousecode,
                        b.warehouseareacode,
                        a.locationcode,
                        warehouse.id        warehouseid,
                        warehousearea.id    parentid --,warehouse.code
          from tmpdata.tagentstores_141101_nfbf a
         inner join tmpdata.agentwlrel141027 b
            on a.warehousecode = b.warehousecode
         inner join warehouse
            on warehouse.code = a.warehousecode
         inner join warehousearea
            on warehouse.id = warehousearea.warehouseid
         where not exists (select 1
                  from warehousearea
                 inner join warehousearea parent
                    on warehousearea.parentid = parent.id
                 inner join warehouse
                    on warehouse.id = warehousearea.warehouseid
                 where warehousearea.code = a.locationcode
                   and b.warehouseareacode = parent.code
                   and warehouse.code = a.warehousecode)
           and warehousearea.areacategoryid = 89
           and warehousearea.areakind = 2
           and b.warehouseareacode = warehousearea.code and locationcode is not null);
--  and a.warehousecode='SYDLK'
insert into warehousearea select * from tmpdata.warehousearea141101_1;
delete from warehousearea where exists(select 1 from  tmpdata.warehousearea141027 where warehousearea141027.id=warehousearea.id)
   /*
select * from  tmpdata.tagentstores_141022_nfbf a
 inner join tmpdata.agentwlrel141027 b  on a.warehousecode = b.warehousecode
 inner join warehouse
    on warehouse.code = a.warehousecode
 inner join warehousearea
    on warehouse.id = warehousearea.warehouseid
    where not exists (select 1
          from warehousearea
         inner join warehousearea parent
            on warehousearea.parentid = parent.id
         inner join warehouse
            on warehouse.id = warehousearea.id
         where warehousearea.code = a.locationcode
           and b.warehouseareacode = parent.code
           and warehouse.code = a.warehousecode)
           and warehousearea.areacategoryid = 89
              and warehousearea.areakind = 2
               and b.warehouseareacode = warehousearea.code
    and a.warehousecode='SYDLK'
    select * from warehousearea where */

create table tmpdata.partsstock141101_1 as select * from partsstock where rownum<1;
insert into tmpdata.partsstock141101_1
  (id,
   warehouseid,
   storagecompanyid,
   storagecompanytype,
   branchid,
   warehouseareaid,
   warehouseareacategoryid,
   partid,
   quantity,
   creatorid,
   creatorname,
   createtime)
  select s_partsstock.nextval,
         warehouse.id,
         warehouse.storagecompanyid,
         warehouse.storagecompanytype,
         8481,
         (select warehousearea.id
            from warehousearea
           inner join warehousearea parent
              on warehousearea.parentid = parent.id
           where a.locationcode = warehousearea.code
             and b.warehouseareacode = parent.code
             and warehousearea.areakind = 3
             and warehousearea.warehouseid = warehouse.id
             and warehousearea.areacategoryid = 89)
         -- wa
        ,
         89,
         (select id from sparepart where code = a.materialcode),
         a.usableqty,
         1,
         'Admin',
         sysdate
    from tmpdata.tagentstores_141101_nfbf a
   inner join tmpdata.agentwlrel141027 b
      on a.warehousecode = b.warehousecode
  -- and a.areacode = b.warehouseareacode
   inner join warehouse
      on warehouse.code = a.warehousecode
  -- inner join warehousearea
  --   on warehouse.id = warehousearea.warehouseid
   where exists (select 1
            from warehousearea
           inner join warehousearea parent
              on warehousearea.parentid = parent.id         
           where warehousearea.code = a.locationcode
             and b.warehouseareacode = parent.code
             and warehouse.id = warehousearea.warehouseid
             and warehousearea.areakind = 3
             and warehousearea.areacategoryid = 89);
             
insert into partsstock select * from  tmpdata.partsstock141101_1;
 /*and warehousearea.areacategoryid = 89
           and warehousearea.areakind =3
           and a.locationcode = warehousearea.code and locationcode is not null*/
     
    /* select *
            from warehousearea
           inner join warehouse
              on warehouse.id = warehousearea.warehouseid
           where warehousearea.code='1'
             and warehousearea.areakind = 2
             and warehouse.code = 'BJDLK'
             and warehousearea.areacategoryid=89

select  a.locationcode,a.warehousecode from tmpdata.tagentstores_141022_nfbf a
   inner join tmpdata.agentwlrel141027 b
      on a.warehousecode = b.warehousecode
  -- and a.areacode = b.warehouseareacode
   inner join warehouse
      on warehouse.code = a.warehousecode

 select * from tmpdata.warehousearea141027 where code='WA甲四0030';

select * from warehousearea
 inner join warehouse
              on warehouse.id = warehousearea.id
              where warehousearea.code='WA甲四0030' and warehouse.code='CCDLK' 

select distinct a.warehousecode,
                        b.warehouseareacode,
                        a.locationcode,
                        warehouse.id        warehouseid,
                        warehousearea.id    parentid --,warehouse.code
          from tmpdata.tagentstores_141022_nfbf a
         inner join tmpdata.agentwlrel141027 b
            on a.warehousecode = b.warehousecode
         inner join warehouse
            on warehouse.code = a.warehousecode
         inner join warehousearea
            on warehouse.id = warehousearea.warehouseid
         where not exists (select 1
                  from warehousearea
                 inner join warehousearea parent
                    on warehousearea.parentid = parent.id
                 inner join warehouse
                    on warehouse.id = warehousearea.id
                 where warehousearea.code = a.locationcode
                   and b.warehouseareacode = parent.code
                   and warehouse.code = a.warehousecode)
           and warehousearea.areacategoryid = 89
           and warehousearea.areakind = 2
           and b.warehouseareacode = warehousearea.code and locationcode is null


*/
select *
from tmpdata.tagentstores_141101_nfbf a
   inner join tmpdata.agentwlrel141027 b
      on a.warehousecode = b.warehousecode
  -- and a.areacode = b.warehouseareacode
   inner join warehouse
      on warehouse.code = a.warehousecode
  -- inner join warehousearea
  --   on warehouse.id = warehousearea.warehouseid
   where  exists (select 1
            from warehousearea
           inner join warehousearea parent
              on warehousearea.parentid = parent.id         
           where warehousearea.code = a.locationcode
             and b.warehouseareacode = parent.code
             and warehouse.id = warehousearea.warehouseid
             and warehousearea.areakind = 3
             and warehousearea.areacategoryid = 89);
             156175
