/*1. 根据来款分割单逐条变更账户金额：
  1.）查询客户账户（账户组Id，客户企业Id），没有记录提示，“客户xxx账户不存在”
  2.）账户金额增加来款分割单.金额，状态=已审核。
  3.)新增客户账户变更明细：
  客户账户Id=客户账户.Id；
  变更金额=来款分割单.金额；
  发生日期=当前日期（精确到时分秒）；
  业务类型=来款；
  账户变更单据Id=来款分割单.Id；
  账户变更单据编号=来款分割单.编号；
  账户变更摘要=来款分割单.摘要；
变更后金额=当前客户账户金额
  变更前金额=当前客户账户金额-来款分割单.金额
 贷方=来款分割单.金额
2. 更新来款单，状态=已分割，同时新增来款分割单。
*/
create table tmpdata.customeraccount141101( 客户编号 varchar2(100),  客户名称  varchar2(100), 客户类型 varchar2(100),  账户金额 number(19,2),  可用额度 number(19,4),  锁定金额 number(19,4),  信用额度 number(19,4),   返利 number(19,4),  品牌编号 varchar2(100),  品牌名称 varchar2(100))

update tmpdata.customeraccount141101 set 客户类型='服务站' where 客户类型 is null;

update tmpdata.customeraccount141101 set 客户类型='代理库' where 客户类型 is null;


update tmpdata.customeraccount141101 set 客户类型='供应商' where 客户类型 is null;

select distinct 品牌名称 from tmpdata.customeraccount141101;
update tmpdata.customeraccount141101 set 品牌名称='工程车(南区)'where 品牌名称='工程车南区';
select count(*)
from tmpdata.customeraccount141101 t  --1495
inner join partssalescategory a on a.name=t.品牌名称 --1495
inner join company on t.客户编号=company.code
inner join salesunit d on d.partssalescategoryid=a.id and d.ownercompanyid=8481
inner join accountgroup c on c.id=d.accountgroupid
select * from tmpdata.customeraccount141101  where 客户编号 in('FT000525',
'FT005329',
'FT005213',
'FT005316',
'FT005209',
'FT005266')
create table tmpdata.customeraccount141101_1 as select * from customeraccount where rownum<1;
--1483
insert into tmpdata.customeraccount141101_1
  (id,
   accountgroupid,
   customercompanyid,
   accountbalance,
   shippedproductvalue,
   pendingamount,
   customercredenceamount,
   status,
   creatorid,
   creatorname,
   createtime)
select 
s_customeraccount.nextval,
c.id,
b.id,
t.账户金额,
0,0,0,1,1,'Admin',sysdate
from tmpdata.customeraccount141101 t
inner join partssalescategory a on a.name=t.品牌名称
--where not exists(select 1 from dealerserviceinfo b where b.businesscode=t.客户编号 and b.partssalescategoryid=a.id)
inner join company b on t.客户编号=b.code
inner join salesunit d on d.partssalescategoryid=a.id and d.ownercompanyid=8481
inner join accountgroup c on c.id=d.accountgroupid
where 客户编号 not in('FT000525',
'FT005329',
'FT005213',
'FT005316',
'FT005209',
'FT005266');
insert into customeraccount select * from tmpdata.customeraccount141101_1;

delete from customeraccount where exists(select 1 from tmpdata.customeraccount141101_1 where customeraccount141101_1.id =customeraccount.id)
create table tmpdata.PaymentBill141101 as select * from PaymentBill where rownum<1; 
insert into tmpdata.PaymentBill141101
  (id,
   code,
   salescompanyid,
   accountgroupid,
   customercompanyid,
   amount,
   paymentmethod,
   status,
   creatorid,
   creatorname,
   createtime,Summary)
  select s_PaymentBill.Nextval,
         'PPB223020141101' || decode(length(rownum),
                                     1,
                                     '000' || rownum,
                                     2,
                                     '00' || rownum,
                                     3,
                                     '0' || rownum,
                                     4,
                                     rownum),
         8481,
         t.accountgroupid,
         t.customercompanyid,
         t.accountbalance,
         1,
         1,
         1,
         'Admin',
         sysdate,'资金台账初始化导入'
    from tmpdata.customeraccount141101_1 t;
                                
    insert into PaymentBill select * from tmpdata.PaymentBill141101;
    
    
delete from PaymentBill where exists(select 1 from tmpdata.PaymentBill141101 where PaymentBill141101.id =PaymentBill.id)
    create table tmpdata.PaymentBeneficiaryList141101 as select * from PaymentBeneficiaryList where rownum<1;
    
insert into tmpdata.PaymentBeneficiaryList141101
  (id,
   code,
   salescompanyid,
   accountgroupid,
   customercompanyid,
   paymentmethod,
   amount,
   paymentbillid,
   paymentbillcode,
   creatorid,
   creatorname,
   createtime,
   status,Summary)
  select s_PaymentBeneficiaryList.Nextval,
         'PPBL223020141101' || decode(length(rownum),
                                      1,
                                      '000' || rownum,
                                      2,
                                      '00' || rownum,
                                      3,
                                      '0' || rownum,
                                      4,
                                      rownum),
         t.salescompanyid,
         t.accountgroupid,
         t.customercompanyid,
         t.paymentmethod,t.amount,
         t.id,
         t.code,
         1,
         'Admin',
         sysdate,
         1,Summary
    from tmpdata.PaymentBill141101 t;

insert into PaymentBeneficiaryList select * from tmpdata.PaymentBeneficiaryList141101;

delete from PaymentBeneficiaryList where exists(select 1 from tmpdata.PaymentBeneficiaryList141101 where PaymentBeneficiaryList141101.id =PaymentBeneficiaryList.id)

create table tmpdata.CustomerAccountHisDetail141101 as select * from CustomerAccountHisDetail where rownum<1;
insert into tmpdata.CustomerAccountHisDetail141101
  (id,
   customeraccountid,
   changeamount,
   debit,
   credit,
   beforechangeamount,
   afterchangeamount,
   ProcessDate,
   businesstype,
   sourceid,
   sourcecode,Summary)
  select s_CustomerAccountHisDetail.Nextval,
         (select id
            from tmpdata.customeraccount141101_1 v
           where t.accountgroupid = v.accountgroupid
             and t.customercompanyid = v.customercompanyid),
         t.amount,
         0,
         t.amount,
         0,
         t.amount,
         t.createtime,
         10,
         t.id,
         t.code,Summary
    from tmpdata.PaymentBeneficiaryList141101 t;
insert into CustomerAccountHisDetail select * from tmpdata.CustomerAccountHisDetail141101;

/*
create table tmpdata.PartsRebateAccount141101_1 as select * from PartsRebateAccount where rownum<1;
insert into tmpdata.PartsRebateAccount141101_1 
  (id,
   Branchid,
   Accountgroupid,
   Customercompanyid,
   Customercompanycode,
   Customercompanyname,
   Accountbalanceamount,
   Status,
   Creatorid,
   Creatorname,
   Createtime)
  select s_PartsRebateAccount.Nextval,
         8481,
         t.accountgroupid,
         t.customercompanyid,
         (select code from company where id = t.customercompanyid),
         (select name from company where id = t.customercompanyid),t.customercredenceamount,1,1,'Admin',sysdate
    from tmpdata.customeraccount141029_1 t;

insert into PartsRebateAccount select * from  tmpdata.PartsRebateAccount141101_1;*/
