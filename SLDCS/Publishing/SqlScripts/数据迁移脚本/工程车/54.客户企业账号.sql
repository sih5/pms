create table tmpdata.personneldealer141101 as select * from personnel where rownum<1;
insert into tmpdata.personneldealer141101
  (id,
   enterpriseid,
   loginid,
   password,
   name,
   status,
   passwordmodifytime,
   creatorid,
   creatorname,
   createtime)
  select s_personnel.nextval,
         dealer.id,
         dealer.code,
         '7C4A8D09CA3762AF61E59520943DC26494F8941B',
         dealer.code,
         1,
         sysdate,
         1,
         'Admin',
         sysdate
    from gcdcs.dealer
     where not exists(select 1 from gcsecurity.personnel t where t.enterpriseid=dealer.id and t.loginid=dealer.code)
;--密码123456

insert into personnel select * from tmpdata.personneldealer141101 ;

create table tmpdata.roledealer141101 as select * from role where rownum<1;


update enterprise t set status=3 where exists (select 1 from gcdcs.dealer where dealer.id=t.id)and status=1;

select* from  entnodetemplate 
select * from enterprisecategory;

insert into tmpdata.roledealer141101(id,enterpriseid,name,isadmin,status,creatorid,creatorname,createtime)
select s_role.nextval,t.enterpriseid,t.loginid||'企业角色',0,1,1,'Admin',sysdate from tmpdata.personneldealer141101 t
insert into role select * from  tmpdata.roledealer141101;

create table tmpdata.ruledealer141101 as select *from rule where rownum<1;

insert into tmpdata.ruledealer141101(id,roleid,nodeid)
select s_rule.nextval,t.id,s.nodeid from tmpdata.roledealer141101 t inner join enterprise v on t.enterpriseid=v.id 
inner join entnodetemplatedetail s on s.entnodetemplateid=v.entnodetemplateid /*where v.entnodetemplateid is null*/

insert into rule select * from tmpdata.ruledealer141101;

create table tmpdata.rolepersonneldealer141101 as select * from rolepersonnel where rownum<1;
insert into tmpdata.rolepersonneldealer141101 (id,roleid,personnelid)
select s_rolepersonnel.nextval,v.id,t.id from tmpdata.personneldealer141101 t inner join  tmpdata.roledealer141101 v on t.enterpriseid=v.enterpriseid;

insert into rolepersonnel select * from tmpdata.rolepersonneldealer141101;

create table tmpdata.personnelagency141101 as select * from personnel where rownum<1;
insert into tmpdata.personnelagency141101
  (id,
   enterpriseid,
   loginid,
   password,
   name,
   status,
   passwordmodifytime,
   creatorid,
   creatorname,
   createtime)
  select s_personnel.nextval,
         agency.id,
         agency.code,
         '7C4A8D09CA3762AF61E59520943DC26494F8941B',
         agency.code,
         1,
         sysdate,
         1,
         'Admin',
         sysdate
    from gcdcs.agency
   where not exists(select 1 from gcsecurity.personnel t where t.enterpriseid=agency.id and t.loginid=agency.code);
insert into personnel select * from tmpdata.personnelagency141101 ;

update enterprise t
   set t.status=3
 where exists (select 1 from gcdcs.agency where agency.id = t.id)and status=1


create table tmpdata.roleagency141101 as select * from yxsecurity.role where rownum<1;
--
insert into tmpdata.roleagency141101(id,enterpriseid,name,isadmin,status,creatorid,creatorname,createtime)
select s_role.nextval,t.enterpriseid,t.loginid||'企业角色',0,1,1,'Admin',sysdate from tmpdata.personnelagency141101 t
insert into role select * from  tmpdata.roleagency141101;


create table tmpdata.ruleagency141101 as select *from rule where rownum<1;

insert into tmpdata.ruleagency141101(id,roleid,nodeid)
select s_rule.nextval,t.id,s.nodeid from tmpdata.roleagency141101 t inner join enterprise v on t.enterpriseid=v.id 
inner join entnodetemplatedetail s on s.entnodetemplateid=v.entnodetemplateid 

insert into rule select * from tmpdata.ruleagency141101;


create table tmpdata.rolepersonnelagency141101 as select * from rolepersonnel where rownum<1;
insert into tmpdata.rolepersonnelagency141101 (id,roleid,personnelid)
select s_rolepersonnel.nextval,v.id,t.id from tmpdata.personnelagency141101 t inner join  tmpdata.roleagency141101 v on t.enterpriseid=v.enterpriseid;

insert into rolepersonnel select * from tmpdata.rolepersonnelagency141101;

create table tmpdata.supplier141101 (code varchar2(100),name varchar2(100))

create table tmpdata.personnelsupplier141101 as select * from yxsecurity.personnel where rownum<1;

insert into tmpdata.personnelsupplier141101
  (id,
   enterpriseid,
   loginid,
   password,
   name,
   status,
   passwordmodifytime,
   creatorid,
   creatorname,
   createtime)
  select s_personnel.nextval,
         t.id,
         t.code,
         '7C4A8D09CA3762AF61E59520943DC26494F8941B',
         t.code,
         1,
         sysdate,
         1,
         'Admin',
         sysdate
    from gcdcs.partssupplier t 
    --inner join  tmpdata.supplier141101 v on t.code=v.code
 where not exists(select 1 from gcsecurity.personnel v where v.enterpriseid=t.id and v.loginid=t.code);;
insert into personnel select * from tmpdata.personnelsupplier141101 ;


update enterprise t
   set t./*entnodetemplateid = 5,*/status=3
 where exists (select 1 from tmpdata.supplier141101 v where v.code=t.code)and status=1
 
 
 

create table tmpdata.rolesupplier141101 as select * from yxsecurity.role where rownum<1;
insert into tmpdata.rolesupplier141101(id,enterpriseid,name,isadmin,status,creatorid,creatorname,createtime)
select s_role.nextval,t.enterpriseid,t.loginid||'企业角色',0,1,1,'Admin',sysdate from tmpdata.personnelsupplier141101 t 
insert into role select * from  tmpdata.rolesupplier141101;


create table tmpdata.rulesupplier141101 as select *from rule where rownum<1;

insert into tmpdata.rulesupplier141101(id,roleid,nodeid)
select s_rule.nextval,t.id,s.nodeid from tmpdata.rolesupplier141101 t inner join enterprise v on t.enterpriseid=v.id 
inner join entnodetemplatedetail s on s.entnodetemplateid=v.entnodetemplateid 

insert into rule select * from tmpdata.rulesupplier141101;



create table tmpdata.rolepersonnelsupplier141101 as select * from rolepersonnel where rownum<1;
insert into tmpdata.rolepersonnelsupplier141101 (id,roleid,personnelid)
select s_rolepersonnel.nextval,v.id,t.id from tmpdata.personnelsupplier141101 t inner join  tmpdata.rolesupplier141101 v on t.enterpriseid=v.enterpriseid;

insert into rolepersonnel select * from tmpdata.rolepersonnelsupplier141101;
select * from personnel  where loginid='admin'and enterpriseid='9204'
select * from enterprise where code='FT001765'

select * from userlogininfo  where enterprisecode='FT001765' for update
select t.loginid,t.enterprisecode from userlogininfo t group by t.loginid,t.enterprisecode having count (loginid)>1
