

Create table tmpdata.tstdproductin141101
as
select v.ProductCode       as 产品编号,
       v.ProductName       as 车型名称,
       v.MainBrandCode     as 品牌代码,
       v.MainBrandname     as 品牌名称,
       v.SubBrandCode      as 子品牌代码,
       v.SubBrandName      as 子品牌名称,
       v.PlatFormCode      as 平台代码,
       v.PlatFormName      as 平台名称,
       v.PKind             as 产品线代码,
       v.LineName          as 产品线名称,
       v.InnerCode         as 原始内部编号,
       v.BulletinCode      as 公告号,
       v.Load              as 吨位代码,
       v.EngineType        as 发动机型号代码,
       v.EngineFactoryCode as 发动机生产厂家代码,
       v.GearType          as 变速箱型号代码,
       v.GearFactoryCode   as 变速箱生产厂家代码,
       v.BrideType         as 后桥型号代码,
       v.BrideFactoryCode  as 后桥生产厂家代码,
       v.TyreType          as 轮胎型号代码,
       v.TyreFactoryCode   as 轮胎形式代码,
       v.brakeType         as 制动方式代码,
       v.DriveType         as 驱动方式代码,
       v.FrameType         as 车架连接形式代码,
       v.FSize             as 整车尺寸,
       v.wheelbase         as 轴距代码,
       v.emissionType      as 排放标准代码,
       v.PFunctionType     as 产品功能代码,
       v.Pstand            as 产品档次代码,
       nvl(t.vehiclecode,v.vtype)           as 车辆类型代码,
       nvl(t.vehiclename,v.vtype)           as 车辆类型名称,
       v.Color             as 颜色描述,
       v.SEATCOUNTER       as 载客个数,
       v.WEIGHT            as 自重
      -- tbrands.name                    as 服务品牌,
       --tproductlines.name              as 服务产品线
  from tmpdata.tstdproductin_GCCDMS_141031
 v
  left join tmpdata.tbrandlinkin_nfbf_141031 t on t.vehiclecode=v.vtype;
--where exists(select 1 from tmpdata.TPURCHASEINFOS140712 t where v.productcode = t.产品编号)
   
--select * from dcs.product_temp

--创建导入临时表
create table tmpdata.product141101 as select * from product where rownum<0;
--select count(*)  from tmpdata.TSTDPRODUCTIN140429 t  where MainBrandCode is null and  SubBrandCode is null  and  PlatFormCode is null and PKind is null for update
 --  where not exists
 --  (select 1 from dcs.product where t.ProductCode = product.code);
 select distinct t.品牌代码 || t.子品牌代码 || t.平台代码 || t.产品线代码 ||产品功能代码,t.品牌代码 , t.子品牌代码 , t.平台代码, t.产品线代码,产品功能代码
  from tmpdata.tstdproductin141101 t
  where not exists
  
 
--插入导入临时表 插入2362条
insert into tmpdata.product141101
  (ID,
   CODE,
   PRODUCTCATEGORYCODE,
   PRODUCTCATEGORYNAME,
   BRANDID,
   BRANDCODE,
   BRANDNAME,
   SUBBRANDCODE,
   SUBBRANDNAME,
   TERRACECODE,
   TERRACENAME,
   PRODUCTLINE,
   PRODUCTNAME,
   OLDINTERNALCODE,
   ANNOUCEMENTNUMBER,
   TONNAGECODE,
   ENGINETYPECODE,
   ENGINEMANUFACTURERCODE,
   GEARSERIALTYPECODE,
   GEARSERIALMANUFACTURERCODE,
   REARAXLETYPECODE,
   REARAXLEMANUFACTURERCODE,
   TIRETYPECODE,
   TIREFORMCODE,
   BRAKEMODECODE,
   DRIVEMODECODE,
   FRAMECONNETCODE,
   VEHICLESIZE,
   AXLEDISTANCECODE,
   EMISSIONSTANDARDCODE,
   PRODUCTFUNCTIONCODE,
   PRODUCTLEVELCODE,
   VEHICLETYPECODE,
   COLORDESCRIBE,
   SEATINGCAPACITY,
   WEIGHT ,STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_product.nextval,
         t.产品编号 as 整车编码,
         nvl(t.车辆类型代码,'无'),
         nvl(t.车辆类型名称,'无') as 车型名称,
         nvl(t.品牌代码 || t.子品牌代码 || t.平台代码 || t.产品线代码 ||产品功能代码,0) as brandid,
         t.品牌代码 as 品牌代码,
         t.品牌名称 as 品牌名称,
         t.子品牌代码 as 子品牌代码,
         t.子品牌名称 as 子品牌名称,
         t.平台代码 as 平台代码,
         t.平台名称 as 平台名称,
         t.产品线代码 as 产品线代码,
         t.产品线名称 as 产品线名称,
         substrb(t.原始内部编号,1,50) as 原始内部编号,
         t.公告号 as 公告号,
         t.吨位代码 as 吨位代码,
         t.发动机型号代码 as 发动机型号代码,
         t.发动机生产厂家代码 as 发动机生成厂家代码,
         t.变速箱型号代码 as 变速箱型号代码,
         t.变速箱生产厂家代码 as 变速箱生成厂家代码,
         t.后桥型号代码 as 后桥型号代码,
         t.后桥生产厂家代码 as 后桥生产厂家代码,
         t.轮胎型号代码 as 轮胎型号代码,
         t.轮胎形式代码 as 轮胎形式代码,
         t.制动方式代码 as 制动方式代码,
         t.驱动方式代码 as 驱动方式代码,
         t.车架连接形式代码 as 车架连接形式代码,
         0 as 整车尺寸,
         t.轴距代码 as 轴距代码,
         t.排放标准代码 as 排放标准代码,
         t.产品功能代码 as 产品功能代码,
         t.产品档次代码 as 产品档次代码,
         t.车辆类型代码 as 车辆类型代码,
         t.颜色描述 as 颜色描述,
         t.载客个数 as 载客个数,
         t.自重 as 自重,
         1,
         1,
         'Admin',
         sysdate
    from tmpdata.tstdproductin141101 t where not exists(select 1 from product where product.code=t.产品编号);
    
    insert into tmpdata.product141101
  (ID,
   CODE,
   PRODUCTCATEGORYCODE,
   PRODUCTCATEGORYNAME,
   BRANDID,
   BRANDCODE,
   BRANDNAME,
   SUBBRANDCODE,
   SUBBRANDNAME,
   TERRACECODE,
   TERRACENAME,
   PRODUCTLINE,
   PRODUCTNAME,
   OLDINTERNALCODE,
   ANNOUCEMENTNUMBER,
   TONNAGECODE,
   ENGINETYPECODE,
   ENGINEMANUFACTURERCODE,
   GEARSERIALTYPECODE,
   GEARSERIALMANUFACTURERCODE,
   REARAXLETYPECODE,
   REARAXLEMANUFACTURERCODE,
   TIRETYPECODE,
   TIREFORMCODE,
   BRAKEMODECODE,
   DRIVEMODECODE,
   FRAMECONNETCODE,
   VEHICLESIZE,
   AXLEDISTANCECODE,
   EMISSIONSTANDARDCODE,
   PRODUCTFUNCTIONCODE,
   PRODUCTLEVELCODE,
   VEHICLETYPECODE,
   COLORDESCRIBE,
   SEATINGCAPACITY,
   WEIGHT ,STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select (select id from  product where product.code=t.产品编号),
         t.产品编号 as 整车编码,
         nvl(t.车辆类型代码,'无'),
         nvl(t.车辆类型名称,'无') as 车型名称,
         nvl(t.品牌代码 || t.子品牌代码 || t.平台代码 || t.产品线代码 ||产品功能代码,0) as brandid,
         t.品牌代码 as 品牌代码,
         t.品牌名称 as 品牌名称,
         t.子品牌代码 as 子品牌代码,
         t.子品牌名称 as 子品牌名称,
         t.平台代码 as 平台代码,
         t.平台名称 as 平台名称,
         t.产品线代码 as 产品线代码,
         t.产品线名称 as 产品线名称,
         substrb(t.原始内部编号,1,50) as 原始内部编号,
         t.公告号 as 公告号,
         t.吨位代码 as 吨位代码,
         t.发动机型号代码 as 发动机型号代码,
         t.发动机生产厂家代码 as 发动机生成厂家代码,
         t.变速箱型号代码 as 变速箱型号代码,
         t.变速箱生产厂家代码 as 变速箱生成厂家代码,
         t.后桥型号代码 as 后桥型号代码,
         t.后桥生产厂家代码 as 后桥生产厂家代码,
         t.轮胎型号代码 as 轮胎型号代码,
         t.轮胎形式代码 as 轮胎形式代码,
         t.制动方式代码 as 制动方式代码,
         t.驱动方式代码 as 驱动方式代码,
         t.车架连接形式代码 as 车架连接形式代码,
         0 as 整车尺寸,
         t.轴距代码 as 轴距代码,
         t.排放标准代码 as 排放标准代码,
         t.产品功能代码 as 产品功能代码,
         t.产品档次代码 as 产品档次代码,
         t.车辆类型代码 as 车辆类型代码,
         t.颜色描述 as 颜色描述,
         t.载客个数 as 载客个数,
         t.自重 as 自重,
         1,
         1,
         'Admin',
         sysdate
    from tmpdata.tstdproductin141101 t where  exists(select 1 from product where product.code=t.产品编号);
   /*where not exists
   (select 1 from dcs.product where t.ProductCode = product.code) 
   and ((MainBrandCode is not null) or ( SubBrandCode is not null)  or  (PlatFormCode is not null) or (PKind is not null));*/

--插入产品表
--select brandcode||subbrandcode||terracecode||productline from product_temp
update product t set t.productcategorycode=nvl((select productcategorycode from tmpdata.product141101 v where v.id=t.id),0),
PRODUCTCATEGORYNAME=nvl((select PRODUCTCATEGORYNAME from tmpdata.product141101 v where v.id=t.id),0),
   BRANDID=nvl((select BRANDID from tmpdata.product141101 v where v.id=t.id),0),
   BRANDCODE=(select BRANDCODE from tmpdata.product141101 v where v.id=t.id),
   BRANDNAME=(select BRANDNAME from tmpdata.product141101 v where v.id=t.id),
   SUBBRANDCODE=(select SUBBRANDCODE from tmpdata.product141101 v where v.id=t.id),
   SUBBRANDNAME=(select SUBBRANDNAME from tmpdata.product141101 v where v.id=t.id),
   TERRACECODE=(select TERRACECODE from tmpdata.product141101 v where v.id=t.id),
   TERRACENAME=(select TERRACECODE from tmpdata.product141101 v where v.id=t.id),
   PRODUCTLINE=(select PRODUCTLINE from tmpdata.product141101 v where v.id=t.id),
   PRODUCTNAME=(select PRODUCTNAME from tmpdata.product141101 v where v.id=t.id),
   OLDINTERNALCODE=(select OLDINTERNALCODE from tmpdata.product141101 v where v.id=t.id),
   ANNOUCEMENTNUMBER=(select ANNOUCEMENTNUMBER from tmpdata.product141101 v where v.id=t.id),
   TONNAGECODE=(select TONNAGECODE from tmpdata.product141101 v where v.id=t.id),
   ENGINETYPECODE=(select ENGINETYPECODE from tmpdata.product141101 v where v.id=t.id),
   ENGINEMANUFACTURERCODE=(select ENGINEMANUFACTURERCODE from tmpdata.product141101 v where v.id=t.id),
   GEARSERIALTYPECODE=(select GEARSERIALTYPECODE from tmpdata.product141101 v where v.id=t.id),
   GEARSERIALMANUFACTURERCODE=(select GEARSERIALMANUFACTURERCODE from tmpdata.product141101 v where v.id=t.id),
   REARAXLETYPECODE=(select REARAXLETYPECODE from tmpdata.product141101 v where v.id=t.id),
   REARAXLEMANUFACTURERCODE=(select REARAXLEMANUFACTURERCODE from tmpdata.product141101 v where v.id=t.id),
   TIRETYPECODE=(select TIRETYPECODE from tmpdata.product141101 v where v.id=t.id),
   TIREFORMCODE=(select TIREFORMCODE from tmpdata.product141101 v where v.id=t.id),
   BRAKEMODECODE=(select BRAKEMODECODE from tmpdata.product141101 v where v.id=t.id),
   DRIVEMODECODE=(select DRIVEMODECODE from tmpdata.product141101 v where v.id=t.id),
   FRAMECONNETCODE=(select FRAMECONNETCODE from tmpdata.product141101 v where v.id=t.id),
   VEHICLESIZE=(select VEHICLESIZE from tmpdata.product141101 v where v.id=t.id),
   AXLEDISTANCECODE=(select AXLEDISTANCECODE from tmpdata.product141101 v where v.id=t.id),
   EMISSIONSTANDARDCODE=(select EMISSIONSTANDARDCODE from tmpdata.product141101 v where v.id=t.id),
   PRODUCTFUNCTIONCODE=(select PRODUCTFUNCTIONCODE from tmpdata.product141101 v where v.id=t.id),
   PRODUCTLEVELCODE=(select PRODUCTLEVELCODE from tmpdata.product141101 v where v.id=t.id),
   VEHICLETYPECODE=(select VEHICLETYPECODE from tmpdata.product141101 v where v.id=t.id),
   COLORDESCRIBE=(select COLORDESCRIBE from tmpdata.product141101 v where v.id=t.id),
   SEATINGCAPACITY=(select SEATINGCAPACITY from tmpdata.product141101 v where v.id=t.id),
   WEIGHT =(select WEIGHT from tmpdata.product141101 v where v.id=t.id)
where   exists(select 1 from tmpdata.product141101 where product141101.id=t.id);

insert into  product select * from tmpdata.product141101 t where  not exists(select 1 from product where product.id=t.id);

--创建产品产品线关系临时表
create table tmpdata.ServiceProdLineProduct141101 as select * from ServiceProdLineProduct where rownum<0;

--插入94条
insert into tmpdata.ServiceProdLineProduct141101 (id,Brandid,Vehiclebrandcode,Vehiclebrandname,Subbrandcode,Subbrandname,Terracecode,Terracename,Vehicleproductline,Vehicleproductname,ProductFunctionCode)
select s_ServiceProdLineProduct.Nextval,
       v.brandid,
       v.brandcode,
       v.brandname,
       v.subbrandcode,
       v.subbrandname,
       v.terracecode,
       v.terracename,
       v.productline,
       v.productname,
       v.ProductFunctionCode
  from (select distinct brandid,
                        BRANDCODE,
                        BRANDNAME,
                        SUBBRANDCODE,
                        SUBBRANDNAME,
                        TERRACECODE,
                        TERRACENAME,
                        PRODUCTLINE,
                        PRODUCTNAME,
                        ProductFunctionCode
          from tmpdata.product141101) v where not exists(select 1 from ServiceProdLineProduct
          where v.brandid=ServiceProdLineProduct.Brandid);
select *from tmpdata.ServiceProdLineProduct140917 for update
--插入产品产品线关系表
insert into ServiceProdLineProduct SELECT * FROM tmpdata.ServiceProdLineProduct141101;

delete from ServiceProdLineProduct where exists(select 1 from  tmpdata.ServiceProdLineProduct141101 where ServiceProdLineProduct.id=ServiceProdLineProduct141101.id)
tpurchaseinfos2012temp
