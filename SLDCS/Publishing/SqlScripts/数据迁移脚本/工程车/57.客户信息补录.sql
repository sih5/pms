
create table tmpdata.TCONSUMERS141101
as
  select distinct CUSTOMER_NAME 客户姓名,
                  性别,
                  case
                    when namelength <= 6 then
                     '个人客户'
                    else
                     '单位客户'
                  end as 客户类型,
                  年龄,
                  证件类型,
                  证件号码,
                  区域,
                  省份,
                  城市,
                  县,
                  邮政编码,
                  家庭电话,
                  单位电话,
                  手机号码,
                  电子邮件,
                  职务,
                  文化程度,
                  客户爱好,
                  详细地址,
                  客户id,'nf' as type 
    from (select tconsumers_nf_141031.customer_name as CUSTOMER_NAME,
                 decode(tconsumers_nf_141031.Sex, 0, '女', 1, '男', '未填') as 性别,
                 lengthb(tconsumers_nf_141031.customer_name) as namelength,
                 tconsumers_nf_141031.age as  年龄,
                 '居民身份证' as  证件类型,
                 tconsumers_nf_141031.ID_card as  证件号码,
                 ' ' as  区域,
                 tconsumers_nf_141031.prov_name as  省份,
                 tconsumers_nf_141031.city_name as  城市,
                  tconsumers_nf_141031.town_name as  县,
                 tconsumers_nf_141031.post 邮政编码,
                 tconsumers_nf_141031.home_phone 家庭电话,
                 tconsumers_nf_141031.office_phone 单位电话,
                 tconsumers_nf_141031.mobile_phone 手机号码,
                 tconsumers_nf_141031.email 电子邮件,
                 tconsumers_nf_141031.duty 职务,
                 decode( tconsumers_nf_141031.education,5,'博士研究生',4,'硕士研究生',3,'本科',2,'中学',1,'小学') as 文化程度,
                 tconsumers_nf_141031.liking as 客户爱好,
                 tconsumers_nf_141031.address as 详细地址,
                 tconsumers_nf_141031.row_id as 客户id
            from tmpdata.tconsumers_nf_141031
           inner join tmpdata.tpurchaseinfos141101
              on tpurchaseinfos141101.客户Row_id = tconsumers_nf_141031.row_id
              where exists(select 1 from tmpdata.VEHICLEINFORMATION141101ins where VEHICLEINFORMATION141101ins.vin=tpurchaseinfos141101.vin码)
           );
------------
insert into tmpdata.TCONSUMERS141101

  select distinct CUSTOMER_NAME 客户姓名,
                  性别,
                  case
                    when namelength <= 6 then
                     '个人客户'
                    else
                     '单位客户'
                  end as 客户类型,
                  年龄,
                  证件类型,
                  证件号码,
                  区域,
                  省份,
                  城市,
                  县,
                  邮政编码,
                  家庭电话,
                  单位电话,
                  手机号码,
                  电子邮件,
                  职务,
                  文化程度,
                  客户爱好,
                  详细地址,
                  客户id,'bf' as type 
    from (select tconsumers_bf_141031.customer_name as CUSTOMER_NAME,
                 decode(tconsumers_bf_141031.Sex, 0, '女', 1, '男', '未填') as 性别,
                 lengthb(tconsumers_bf_141031.customer_name) as namelength,
                 tconsumers_bf_141031.age as  年龄,
                 '居民身份证' as  证件类型,
                 tconsumers_bf_141031.ID_card as  证件号码,
                 ' ' as  区域,
                 tconsumers_bf_141031.prov_name as  省份,
                 tconsumers_bf_141031.city_name as  城市,
                  tconsumers_bf_141031.town_name as  县,
                 tconsumers_bf_141031.post 邮政编码,
                 tconsumers_bf_141031.home_phone 家庭电话,
                 tconsumers_bf_141031.office_phone 单位电话,
                 tconsumers_bf_141031.mobile_phone 手机号码,
                 tconsumers_bf_141031.email 电子邮件,
                 tconsumers_bf_141031.duty 职务,
                 decode( tconsumers_bf_141031.education,5,'博士研究生',4,'硕士研究生',3,'本科',2,'中学',1,'小学') as 文化程度,
                 tconsumers_bf_141031.liking as 客户爱好,
                 tconsumers_bf_141031.address as 详细地址,
                 tconsumers_bf_141031.row_id as 客户id
            from tmpdata.tconsumers_bf_141031
           inner join tmpdata.tpurchaseinfos141101
              on tpurchaseinfos141101.客户Row_id = tconsumers_bf_141031.row_id
               where exists(select 1 from tmpdata.VEHICLEINFORMATION141101ins where VEHICLEINFORMATION141101ins.vin=tpurchaseinfos141101.vin码)
           );
           
create index TCONSUMERS140705_ROWID on tmpdata.TCONSUMERS140705  (客户id);
select count (distinct tconsumers_nf_141031.row_id)from tmpdata.tconsumers_nf_141031   --362264
 inner join tmpdata.tpurchaseinfos140917
              on tpurchaseinfos140917.客户Row_id = tconsumers_nf_141031.row_id --317210
              
              select count(distinct tconsumers_bf_141031.row_id) from tmpdata.tconsumers_bf_141031   --558388
 inner join tmpdata.tpurchaseinfos140917
              on tpurchaseinfos140917.客户Row_id = tconsumers_bf_141031.row_id --535402




select * from  tmpdata.tPurchaseLinkers_bf_140927 


select count(*) from TMPDATA.Customer140705
create table TMPDATA.Customer141101 as SELECT * FROM Customer WHERE ROWNUM<0;
create index tpurchaseinfos140917_cidx on tmpdata.tpurchaseinfos140917 (客户row_id);
 TPURCHASEINFOS140705
--插入基础客户信息临时表 852612
INSERT INTO TMPDATA.Customer141101
  (ID,
   NAME,
   GENDER,
   CUSTOMERTYPE,
   CELLPHONENUMBER,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME,
   Ifcomplain,
   customercode,
   AGE,   
   ADDRESS,
   POSTCODE,
   HOMEPHONENUMBER,
   OFFICEPHONENUMBER,
   IDDOCUMENTNUMBER,
   EMAIL,
   IDDOCUMENTTYPE,remark,PROVINCENAME,CITYNAME,COUNTYNAME)
  SELECT S_Customer.Nextval,
         nvl(t.客户姓名, '空'),
         decode(t.性别, '未填', 0, '男', 1, 2),
         decode(t.客户类型, '个人客户', 1, 2),       
         substrb(t.手机号码, 1, 50),         
         1 STATUS,        
         1 CREATORID,
         'Admin' CREATORNAME,
         sysdate CREATETIME,    
         0 Ifcomplain,
         'CSM20141101' || decode(length(rownum),
                                 1,
                                 '00000' || rownum,
                                 2,
                                 '0000' || rownum,
                                 3,
                                 '000' || rownum,
                                 4,
                                 '00' || rownum,
                                 5,
                                 '0' || rownum,
                                 6,
                                 rownum) CODE,
         t.年龄,
         substrb(t.详细地址,1,200),
         substrb(t.邮政编码,1,6),
         substrb(t.家庭电话,1,50),
         substrb(t.单位电话,1,50),
         t.证件号码,
         t.电子邮件,
         decode(t.证件类型,'居民身份证',1),t.客户id,t.省份,t.城市,t.县
    FROM (select distinct t.* from tmpdata.TCONSUMERS141101 t
    inner join  tmpdata.tpurchaseinfos141101 v on t.客户id=v.客户row_id) t;
    select count(*)from tmpdata.tconsumers140918 t 
     inner join  tmpdata.tpurchaseinfos140917 v on t.客户id=v.客户row_id;
-- SELECT * FROM DCS.TILEDREGION 

--插入基础客户表 
insert into CUSTOMER select * from  TMPDATA.Customer141101;
delete from CUSTOMER where exists(select 1 from  TMPDATA.Customer140918 t where t.id=CUSTOMER.id)
select * from TMPDATA.Customer140918 t where remark in(
select t.remark from  TMPDATA.Customer140918 t group by t.remark having count (remark)>1) order by remark

select t.客户id from tmpdata.tconsumers140918 t group by t.客户id having count(t.客户id)>1

--创建保有客户临时表
create table TMPDATA.RetainedCustomer141101  as select * from RetainedCustomer where rownum<0;
create index tmpdata.CUSTOMER140918_rmk on TMPDATA.CUSTOMER140918 (remark);
--插入保有客户临时表 852612
insert into TMPDATA.RetainedCustomer141101(
  ID                ,
  CUSTOMERID        ,
  IFVISITBACKNEEDED ,
  IFTEXTACCEPTED    ,
  IFADACCEPTED      ,
  CARDNUMBER        ,
  IFVIP             ,
  VIPTYPE           ,
  BONUSPOINTS       ,
  PLATEOWNER        ,
  IFMARRIED         ,
  WEDDINGDAY        ,
  FAMILYINCOME      ,
  LOCATION          ,
  FAMILYCOMPOSITION ,
  REFERRAL          ,
  HOBBY             ,
  SPECIALDAY        ,
  SPECIALDAYDETAILS ,
  USERGENDER        ,
  COMPANYTYPE       ,
  ACQUISITORNAME    ,
  ACQUISITORPHONE   ,
  CREATORID         ,
  CREATORNAME       ,
  CREATETIME        ,
  STATUS            ,
  REMARK            )
  select s_RetainedCustomer.Nextval,
  (select id from tmpdata.Customer141101 v where remark=t.客户id),
  0 IFVISITBACKNEEDED ,
  0 IFTEXTACCEPTED    ,
  0 IFADACCEPTED      ,
  '空' CARDNUMBER        ,
  0 IFVIP             ,
  0 VIPTYPE           ,
  0 BONUSPOINTS       ,
  '空' PLATEOWNER        ,
  0 IFMARRIED         ,
  to_date('1899-1-1','yyyy-mm-dd') WEDDINGDAY        ,
  0 FAMILYINCOME      ,
  0 LOCATION          ,
  '空' FAMILYCOMPOSITION ,
  '空' REFERRAL          ,
substrb(t.客户爱好,1,200)  HOBBY             ,
  to_date('1899-1-1','yyyy-mm-dd') SPECIALDAY        ,
  '空' SPECIALDAYDETAILS ,
  0 USERGENDER        ,
  0 COMPANYTYPE       ,
  '空' ACQUISITORNAME    ,
  '空' ACQUISITORPHONE   ,
  1 CREATORID,
  'Admin' CREATORNAME,
  sysdate CREATETIME,
  1 STATUS,
  t.客户id
  from TMPDATA.TCONSUMERS141101 t/*;
  commit;*/
  where exists(select 1 from   tmpdata.TPURCHASEINFOS141101 v where t.客户id=v.客户row_id);

632735
select count(*)  from TMPDATA.TCONSUMERS140705 t
  where exists(select 1 from   tmpdata.TPURCHASEINFOS140705 v where t.客户id=v.客户row_id)
--插入保有客户Id
insert into RetainedCustomer select * from TMPDATA.RetainedCustomer141101;
gcdcs.retainedcustomervehiclelist
create table tmpdata.rcustomervehiclelist141101 as select * from gcdcs.retainedcustomervehiclelist where rownum<1;
--应该5948，实际插入4917
insert into tmpdata.rcustomervehiclelist141101(id,retainedcustomerid,vehicleid,status)
select s_retainedcustomervehiclelist.nextval,t.id,v.id,1 from
tmpdata.VEHICLEINFORMATION141101ins t inner join retainedcustomer v
on t.remark=v.remark

delete from retainedcustomervehiclelist t where exists(select 1 from tmpdata.rcustomervehiclelist141101 v where t.id=v.id)

insert into retainedcustomervehiclelist select * from tmpdata.rcustomervehiclelist141101;

select count (*)from tmpdata.rcustomervehiclelist140918 --916905
create table tmpdata.vehiclelinkman141101 as select * from vehiclelinkman where rownum<1;


update CUSTOMER v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname = v.cityname
           and a.countyname is null),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname = v.cityname
                          and a.countyname is null)
 where v.provincename is not null
   and v.cityname is not null
   and v.countyname is null
   and exists
 (select * from tmpdata.vehiclelinkman140806_1 t where t.id = v.id);

update CUSTOMER v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname = v.cityname
           and a.countyname =v.countyname),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname = v.cityname
                          and a.countyname =v.countyname)
 where v.provincename is not null
   and v.cityname is not null
   and v.countyname is not null
   and regionid is null
  
 
update CUSTOMER v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname is null
           and a.countyname is null),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname is null
                          and a.countyname is null)
 where v.provincename is not null
   and v.cityname is  null
   and v.countyname is  null
   and regionid is null;

insert into tmpdata.vehiclelinkman141101
  (id,
   retainedcustomerid,
   vehicleinformationid,
   customercode,
   linkmantype,
   name,
   gender,
   cellphonenumber,
   email,
   address,
   status,
   creatorid,
   creatorname,
   createtime,
   regionid,
   regionname,
   provincename,
   cityname,
   countyname)
  select s_vehiclelinkman.nextval,
         v.id,
         t.id,
         'VL20141101' || decode(length(rownum),
                                1,
                                '00000' || rownum,
                                2,
                                '0000' || rownum,
                                3,
                                '000' || rownum,
                                4,
                                '00' || rownum,
                                5,
                                '0' || rownum,
                                6,
                                rownum),
         1,
         a.name,
         a.gender,
         a.cellphonenumber,
         a.email,
         a.address,
         1,
         1,
         'Admin',
         sysdate,
         a.regionid,
         a.regionname,
         a.provincename,
         a.cityname,
         a.countyname
    from tmpdata.vehicleinformation141101ins t
   inner join retainedcustomer v
      on t.remark = v.remark
   inner join customer a
      on t.remark = a.remark
select count(*)from  tmpdata.vehiclelinkman140918

insert into vehiclelinkman select * from tmpdata.vehiclelinkman141101;
create table tmpdata.vehiclelinkman141101_1 as select * from vehiclelinkman where rownum<1;
insert into tmpdata.vehiclelinkman141101_1
  (id,
   retainedcustomerid,
   vehicleinformationid,
   customercode,
   linkmantype,
   name,
   gender,
   cellphonenumber,
   email,
   address,
   status,
   creatorid,
   creatorname,
   createtime,
 
   provincename,
   cityname,
   countyname)
   select  s_vehiclelinkman.nextval,
         c.id,
         e.id,
         'VL20141101' || decode(length(rownum),
                                1,
                                '00000' || rownum,
                                2,
                                '0000' || rownum,
                                3,
                                '000' || rownum,
                                4,
                                '00' || rownum,
                                5,
                                '0' || rownum,
                                6,
                                rownum),
         2,
         a.name,
         0,
         a.custmobile,
         '',
         a.address,
         1,
         1,
         'Admin',
         sysdate,
        
         a.prov_name,
         a.city_name,
         a.town_name
   from tmpdata.tPurchaseLinkers_bf_141031 a
   inner join tmpdata.tpurchaseinfos_bf_141031 b on a.parentid=b.row_id
   inner join tmpdata.vehicleinformation141101ins e on e.vin=b.Bone_Code 
    inner join retainedcustomer c
      on e.remark = c.remark
   inner join customer d
      on c.remark = d.remark where a.type<>0;

insert into tmpdata.vehiclelinkman141101_1
  (id,
   retainedcustomerid,
   vehicleinformationid,
   customercode,
   linkmantype,
   name,
   gender,
   cellphonenumber,
   email,
   address,
   status,
   creatorid,
   creatorname,
   createtime,
 
   provincename,
   cityname,
   countyname)
   select  s_vehiclelinkman.nextval,
         c.id,
         e.id,
         'VL20141101' || decode(length(rownum),
                                1,
                                '00000' || rownum,
                                2,
                                '0000' || rownum,
                                3,
                                '000' || rownum,
                                4,
                                '00' || rownum,
                                5,
                                '0' || rownum,
                                6,
                                rownum),
         2,
         a.name,
         0,
         a.custmobile,
         '',
         a.address,
         1,
         1,
         'Admin',
         sysdate,
        
         a.prov_name,
         a.city_name,
         a.town_name
   from tmpdata.tPurchaseLinkers_nf_141031 a
   inner join tmpdata.tpurchaseinfos_nf_141031 b on a.parentid=b.row_id
   inner join tmpdata.vehicleinformation141101ins e on e.vin=b.Bone_Code 
    inner join retainedcustomer c
      on e.remark = c.remark
   inner join customer d
      on c.remark = d.remark  where a.type<>0 ;
  
  
update TMPDATA.vehiclelinkman141101_1 v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname = v.cityname
           and a.countyname is null),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname = v.cityname
                          and a.countyname is null)
 where v.provincename is not null
   and v.cityname is not null
   and v.countyname is null
   ;

update TMPDATA.vehiclelinkman141101_1 v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname = v.cityname
           and a.countyname =v.countyname),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname = v.cityname
                          and a.countyname =v.countyname)
 where v.provincename is not null
   and v.cityname is not null
   and v.countyname is not null
   and regionid is null
  ;
 
update TMPDATA.vehiclelinkman141101_1 v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname is null
           and a.countyname is null),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname is null
                          and a.countyname is null)
 where v.provincename is not null
   and v.cityname is  null
   and v.countyname is  null
   and regionid is null;    
   
   update TMPDATA.vehiclelinkman141101 v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname = v.cityname
           and a.countyname is null),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname = v.cityname
                          and a.countyname is null)
 where v.provincename is not null
   and v.cityname is not null
   and v.countyname is null
   ;

update TMPDATA.vehiclelinkman141101 v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname = v.cityname
           and a.countyname =v.countyname),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname = v.cityname
                          and a.countyname =v.countyname)
 where v.provincename is not null
   and v.cityname is not null
   and v.countyname is not null
   and regionid is null
  ;
 
update TMPDATA.vehiclelinkman141101 v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname is null
           and a.countyname is null),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname is null
                          and a.countyname is null)
 where v.provincename is not null
   and v.cityname is  null
   and v.countyname is  null
   and regionid is null;   
   
      insert into vehiclelinkman select * from  TMPDATA.vehiclelinkman141101_1 v;
      --  insert into vehiclelinkman select * from  TMPDATA.vehiclelinkman141101 v;
        delete from  vehiclelinkman where exists(select 1 from TMPDATA.vehiclelinkman140918_1 v  where v.id=vehiclelinkman.id)


 
create table tmpdata.rcustomervehiclelist141105 as select * from gcdcs.retainedcustomervehiclelist where rownum<1;

insert into tmpdata.rcustomervehiclelist141105(id,retainedcustomerid,vehicleid,status)
select s_retainedcustomervehiclelist.nextval,v.id,t.id,1 from
tmpdata.VEHICLEINFORMATION141101ins t inner join retainedcustomer v
on t.remark=v.remark
select count(*) from tmpdata.VEHICLEINFORMATION141101ins t
inner join  retainedcustomer v
on t.remark=v.remark
insert into retainedcustomervehiclelist select * from tmpdata.rcustomervehiclelist141101;


select *from 
