----备份老系统账面和可用不同的记录  43rows
create table tmpdata.gcstorestz as 
select * from tmpdata.tstores_141101_nfbf t
where
accountqty<>usableqty

----备份新系统库存
create table tmpdata.gcpartstock20141102 as select a.* from gcdcs.partsstock  a 
inner join gcdcs.warehouse b on a.warehouseid = b.id
inner join gcdcs.sparepart c on c.id = a.partid
inner join tmpdata.gcstorestz d on d.warehousecode = b.code and d.materialcode = c.code

----按照备份更新新PMS系统库存 43 rows
update  gcdcs.partsstock tmp set tmp.quantity = (select d.accountqty from partsstock a  
inner join gcdcs.warehouse b on a.warehouseid = b.id
inner join gcdcs.sparepart c on c.id = a.partid
inner join tmpdata.gcstorestz d on d.warehousecode = b.code and d.materialcode = c.code
where a.id = tmp.id)
where exists
(select 1 from partsstock a  
inner join gcdcs.warehouse b on a.warehouseid = b.id
inner join gcdcs.sparepart c on c.id = a.partid
inner join tmpdata.gcstorestz d on d.warehousecode = b.code and d.materialcode = c.code
where a.id = tmp.id)

-----老系统库存
select warehousecode,sum(accountprice),sum(usableprice),sum(accountqty*stdprice) from tmpdata.tstores_141101_nfbf 
group by warehousecode
-----新系统库存
select f.code,sum(a.quantity*e.plannedprice) from partsstock a 
inner join salesunitaffiwarehouse b on a.warehouseid = b.warehouseid
inner join salesunit c on c.id = b.salesunitid
inner join sparepart d on d.id = a.partid
inner join partsplannedprice e on e.partssalescategoryid = c.partssalescategoryid and e.sparepartid = d.id
inner join warehouse f on f.id = a.warehouseid
inner join partssalescategory g on g.id = c.partssalescategoryid
where a.storagecompanyid = 8481
group by f.code

-----导出新系统库存明细
select g.name,f.code,f.name,d.code,d.name,a.quantity,e.plannedprice,a.quantity*e.plannedprice from partsstock a 
inner join salesunitaffiwarehouse b on a.warehouseid = b.warehouseid
inner join salesunit c on c.id = b.salesunitid
inner join sparepart d on d.id = a.partid
inner join partsplannedprice e on e.partssalescategoryid = c.partssalescategoryid and e.sparepartid = d.id
inner join warehouse f on f.id = a.warehouseid
inner join partssalescategory g on g.id = c.partssalescategoryid
where f.code = 'GCCCSPJCK' and f.storagecompanyid = 8481
