create table tmpdata.EngineModelProductLine141022 as select * from EngineModelProductLine where rownum<1;
insert into tmpdata.EngineModelProductLine141022(id,branchid,partssalescategoryid,engineproductlineid,enginemodeid,creatorid,creatorname,createtime)
select s_EngineModelProductLine.Nextval,8481,engineproductline.partssalescategoryid,engineproductline.id,enginemodel.id,1,'Admin',sysdate from engineproductline
cross join enginemodel where enginemodel.enginemodel in
(
'ISDe185 30',
'ISDe185 30(르영)',
'ISF2.8s3129T',
'ISF2.8s4129T',
'ISF3.8S3 168',
'ISF3.8s3141',
'ISF3.8S3154',
'ISF3.8-S3154',
'ISF3.8-S3154(르영)',
'ISF3.8s3168',
'ISF3.8-S3168',
'ISF3.8s4141',
'ISF3.8s4154',
'ISF3.8s4168（르영）',
'ISF3.8s4R141',
'ISF3.8s4R168',
'ISF3.8-SR4168');
insert into EngineModelProductLine select * from tmpdata.EngineModelProductLine141022 ;
