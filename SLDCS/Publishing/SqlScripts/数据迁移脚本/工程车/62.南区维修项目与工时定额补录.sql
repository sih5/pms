
create table tmpdata.repairitem141105 (repairitemcategorycode varchar2(20),code varchar2(50),name varchar2(600),RepairQualificationGrade varchar2 (50),productlinecode varchar2 (50),productlinename varchar2(100),productlinetype varchar2(100),DefaultLaborHour number(9,2))
 select distinct productlinecode from tmpdata.repairitem141105 t
   where exists (select 1 from repairitem v where v.code = t.code)
   and not exists(select productlineid
            from serviceproductlineview a
           where a.ProductLineCode = t.productlinecode);

create table tmpdata.RItemAffiServProdLine141105 as select * from RepairItemAffiServProdLine where rownum<1;
insert into tmpdata.RItemAffiServProdLine141105
  (id,
   branchid,
   repairitemid,
   serviceproductlineid,
   productlinetype,
   remark,
   status,
   creatorid,
   creatorname,
   createtime,
   defaultlaborhour,
   PartsSalesCategoryId)
  select s_RepairItemAffiServProdLine.Nextval,
         8481,
         (select id from repairitem v where v.code = t.code),
         (select productlineid
            from serviceproductlineview a
           where a.ProductLineCode = t.productlinecode),
         (select productlinetype
            from serviceproductlineview a
           where a.ProductLineCode = t.productlinecode),
         '',
         1,
         1,
         'Admin',
         sysdate,
         t.defaultlaborhour,
         (select a.Partssalescategoryid
            from serviceproductlineview a
           where a.ProductLineCode = t.productlinecode)
    from tmpdata.repairitem141105 t
   where exists (select 1 from repairitem v where v.code = t.code)
   and  exists(select productlineid
            from serviceproductlineview a
           where a.ProductLineCode = t.productlinecode);
   insert into RepairItemAffiServProdLine select * from tmpdata.RItemAffiServProdLine141105;
   
   select *    from tmpdata.repairitem141105 t
   where not  exists (select 1 from repairitem v where v.code = t.code)
   
   
   
  create table tmpdata.RepairItemBrandRelation141105 as select * from RepairItemBrandRelation where rownum<1;
 insert into tmpdata.RepairItemBrandRelation141105
   (id,
    branchid,
    repairitemid,
    repairitemcode,
    repairitemname,
    partssalescategoryid,
    repairqualificationgrade,
    creatorid,
    creatorname,
    createtime,status)
   select s_RepairItemBrandRelation.Nextval,
          8481,
          (select id from repairitem v where v.code = t.code),
          t.code,
          t.name,
          t.Partssalescategoryid, 1, 1, 'Admin', sysdate,1 from (select distinct code,name,(select a.Partssalescategoryid
            from serviceproductlineview a
           where a.ProductLineCode = repairitem141105.productlinecode) Partssalescategoryid from tmpdata.repairitem141105) t
           where exists (select 1 from repairitem v where v.code = t.code);
          
           insert into RepairItemBrandRelation select * from  tmpdata.RepairItemBrandRelation141105 t;
          delete from RepairItemBrandRelation t where exists(select * from  tmpdata.RepairItemBrandRelation141105 v where t.id=v.id )
