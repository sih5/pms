


create table tmpdata.partsstock141103 as select * from gcdcs.partsstock where rownum<1;
   
    insert into tmpdata.partsstock141103
  (id,
   warehouseid,
   storagecompanyid,
   storagecompanytype,
   branchid,
   warehouseareaid,
   warehouseareacategoryid,
   partid,
   quantity,
   creatorid,
   creatorname,
   createtime)
 select s_partsstock.nextval,
        562,
        8481,
        1,
        8481,
        (select warehousearea.id from warehousearea inner join warehouse on warehouse.id=warehousearea.warehouseid where warehousearea.code='2B1B'and warehouse.code='BGDBCK'),
        89,
        c.id,     
        a.账面库存,
        1,
        'Admin',
        sysdate
    from tmpdata.gclxtst a 
left join tmpdata.tstores_141101_nfbf b on a.图号= b.materialcode
left join gcdcs.sparepart c on c.code = a.图号
where b.materialcode is null ;
 /* inner join tmpdata.warehouseareapartrel141031 v
     on t.materialcode = v.配件图号
    and t.warehousecode = v.仓库编号*/
/*  where  t.warehousecode = 'BGDBCK'*/

insert into partsstock select * from   tmpdata.partsstock141103;


create table tmpdata.partsstock141103_1 as select * from gcdcs.partsstock where rownum<1;
  
  insert into tmpdata.partsstock141103_1
  (id,
   warehouseid,
   storagecompanyid,
   storagecompanytype,
   branchid,
   warehouseareaid,
   warehouseareacategoryid,
   partid,
   quantity,
   creatorid,
   creatorname,
   createtime)
   select  s_partsstock.nextval,
        561,
        8481,
        1,
        8481,
        (select id from warehousearea where code= 'NGB1I6000000'),
         89,
        c.id,     
        a.accountqty,
        1,
        'Admin',
        sysdate
          from tmpdata.tstores_141103_nf a
          left join tmpdata.tstores_141101_nfbf b
            on a.materialcode = b.materialcode
          left join gcdcs.sparepart c
            on c.code = a.materialcode
         where b.materialcode is null
           and c.id is not null
           and a.accountqty > 0;
           insert into partsstock select * from   tmpdata.partsstock141103_1;



create table tmpdata.stdprice141103_1 as select * from partsplannedprice where rownum<1;

insert into tmpdata.stdprice141103_1
  (id,
   ownercompanyid,
   ownercompanytype,
   partssalescategoryid,
   partssalescategoryname,
   sparepartid,
   plannedprice,
   creatorid,
   creatorname,
   createtime)
select s_partsplannedprice.nextval,
 8481,
 1,
(select id
            from partssalescategory
           where partssalescategory.code =decode (t.syscode,'nf','105011','bf','106011')) ,
    (select name
            from partssalescategory
           where partssalescategory.code =decode (t.syscode,'nf','105011','bf','106011')),
 s.id,
 t.stdprice,
 1,
 'Admin',
 sysdate
  from ( select distinct materialcode,stdprice,syscode from tmpdata.stdprice141103 a) t
 inner join sparepart s
    on s.code = t.materialcode;
 
 insert into partsplannedprice select * from  tmpdata.stdprice141103_1;
 create table tmpdata.PlannedPriceApp141103 as select * from PlannedPriceApp where rownum<1;
create table tmpdata.PlannedPriceAppDetail141103 as select * from PlannedPriceAppDetail where rownum<1;

insert into tmpdata.PlannedPriceApp141103
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,RecordStatus,PlannedExecutionTime,ActualExecutionTime)
values
  (s_PlannedPriceApp.Nextval,
   8481,
   '2230',
   '北汽福田汽车股份有限公司工程车事业部', 21,
   '工程车(北区)',
   'PPA2203201411030001',
   0,
   0,
   0,
   3,
   1,
   'Admin',
   sysdate,3,sysdate,sysdate);
  
    insert into tmpdata.PlannedPriceAppDetail141103
      (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             s_PlannedPriceApp.Currval,
             t.sparepartid,
             (select code
                from sparepart
               where t.sparepartid = sparepart.id),
             (select name
                from sparepart
               where t.sparepartid = sparepart.id),t.plannedprice,0,0,0
        from tmpdata.stdprice141103_1 t where t.partssalescategoryid=21;
   
   insert into tmpdata.PlannedPriceApp141103
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,RecordStatus,PlannedExecutionTime,ActualExecutionTime)
values
  (s_PlannedPriceApp.Nextval,
    8481,
   '2230',
   '北汽福田汽车股份有限公司工程车事业部', 22,
   '工程车(南区)',
   'PPA2203201411030002',
   0,
   0,
   0,
   3,
   1,
   'Admin',
   sysdate,3,sysdate,sysdate);
    insert into tmpdata.plannedpriceappdetail141103
      (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             s_PlannedPriceApp.Currval,
             t.sparepartid,
             (select code
                from sparepart
               where t.sparepartid = sparepart.id),
             (select name
                from sparepart
               where t.sparepartid = sparepart.id),t.plannedprice,0,0,0
        from tmpdata.stdprice141103_1 t where t.partssalescategoryid=22;
   
   
insert into  PlannedPriceApp select * from  tmpdata.PlannedPriceApp141103;
insert into  plannedpriceappdetail select * from tmpdata.plannedpriceappdetail141103;


