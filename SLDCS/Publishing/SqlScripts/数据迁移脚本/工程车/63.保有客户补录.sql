
/*

select v.id,a.id,b.id,t.remark
  from    vehicleinformation t 
    left  join retainedcustomervehiclelist v on t.id=v.vehicleid
 left join retainedcustomer a on a.id=v.retainedcustomerid
 left join customer b on b.id=a.customerid 
 where t.salesdate>=to_date('2013-01-01','yyyy-mm-dd') and t.id in('4288533',  
'4280444',    
'4258557',    
'4276520',     
'4276520',     
'4270156',     
'4273528',     
'4286918',     
'4288913'    
)
and exists(select distinct d.Category
            from WarrantyPolicy d
           inner join WarrantyPolicyNServProdLine a
              on a. WarrantyPolicyId = d.id
           inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1          
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate and rownum=1);
*/
select * from tmpdata.tconsumers_nf_141105_1 t where t.row_id='nf{DCE3E363-41A3-54FB-E040-007F0100318A}';

--1815
create table tmpdata.TCONSUMERS141105_1
as
  select distinct CUSTOMER_NAME 客户姓名,
                  性别,
                  case
                    when namelength <= 6 then
                     '个人客户'
                    else
                     '单位客户'
                  end as 客户类型,
                  年龄,
                  证件类型,
                  证件号码,
                  区域,
                  省份,
                  城市,
                  县,
                  邮政编码,
                  家庭电话,
                  单位电话,
                  手机号码,
                  电子邮件,
                  职务,
                  文化程度,
                  客户爱好,
                  详细地址,
                  客户id,'nf' as type 
    from (select tconsumers_bf_141105_1.customer_name as CUSTOMER_NAME,
                 decode(tconsumers_bf_141105_1.Sex, 0, '女', 1, '男', '未填') as 性别,
                 lengthb(tconsumers_bf_141105_1.customer_name) as namelength,
                 tconsumers_bf_141105_1.age as  年龄,
                 '居民身份证' as  证件类型,
                 tconsumers_bf_141105_1.ID_card as  证件号码,
                 ' ' as  区域,
                 tconsumers_bf_141105_1.prov_name as  省份,
                 tconsumers_bf_141105_1.city_name as  城市,
                  tconsumers_bf_141105_1.town_name as  县,
                 tconsumers_bf_141105_1.post 邮政编码,
                 tconsumers_bf_141105_1.home_phone 家庭电话,
                 tconsumers_bf_141105_1.office_phone 单位电话,
                 tconsumers_bf_141105_1.mobile_phone 手机号码,
                 tconsumers_bf_141105_1.email 电子邮件,
                 tconsumers_bf_141105_1.duty 职务,
                 decode( tconsumers_bf_141105_1.education,5,'博士研究生',4,'硕士研究生',3,'本科',2,'中学',1,'小学') as 文化程度,
                 tconsumers_bf_141105_1.liking as 客户爱好,
                 tconsumers_bf_141105_1.address as 详细地址,
                 tconsumers_bf_141105_1.row_id as 客户id
            from tmpdata.tconsumers_bf_141105_1);
            
            
insert into tmpdata.TCONSUMERS141105_1

  select distinct CUSTOMER_NAME 客户姓名,
                  性别,
                  case
                    when namelength <= 6 then
                     '个人客户'
                    else
                     '单位客户'
                  end as 客户类型,
                  年龄,
                  证件类型,
                  证件号码,
                  区域,
                  省份,
                  城市,
                  县,
                  邮政编码,
                  家庭电话,
                  单位电话,
                  手机号码,
                  电子邮件,
                  职务,
                  文化程度,
                  客户爱好,
                  详细地址,
                  客户id,'nf' as type 
    from (select tconsumers_nf_141105_1.customer_name as CUSTOMER_NAME,
                 decode(tconsumers_nf_141105_1.Sex, 0, '女', 1, '男', '未填') as 性别,
                 lengthb(tconsumers_nf_141105_1.customer_name) as namelength,
                 tconsumers_nf_141105_1.age as  年龄,
                 '居民身份证' as  证件类型,
                 tconsumers_nf_141105_1.ID_card as  证件号码,
                 ' ' as  区域,
                 tconsumers_nf_141105_1.prov_name as  省份,
                 tconsumers_nf_141105_1.city_name as  城市,
                  tconsumers_nf_141105_1.town_name as  县,
                 tconsumers_nf_141105_1.post 邮政编码,
                 tconsumers_nf_141105_1.home_phone 家庭电话,
                 tconsumers_nf_141105_1.office_phone 单位电话,
                 tconsumers_nf_141105_1.mobile_phone 手机号码,
                 tconsumers_nf_141105_1.email 电子邮件,
                 tconsumers_nf_141105_1.duty 职务,
                 decode( tconsumers_nf_141105_1.education,5,'博士研究生',4,'硕士研究生',3,'本科',2,'中学',1,'小学') as 文化程度,
                 tconsumers_nf_141105_1.liking as 客户爱好,
                 tconsumers_nf_141105_1.address as 详细地址,
                 tconsumers_nf_141105_1.row_id as 客户id
            from tmpdata.tconsumers_nf_141105_1);
                        
            create table TMPDATA.Customer141105_1 as SELECT * FROM Customer WHERE ROWNUM<0;
--1815            
INSERT INTO TMPDATA.Customer141105_1
  (ID,
   NAME,
   GENDER,
   CUSTOMERTYPE,
   CELLPHONENUMBER,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME,
   Ifcomplain,
   customercode,
   AGE,   
   ADDRESS,
   POSTCODE,
   HOMEPHONENUMBER,
   OFFICEPHONENUMBER,
   IDDOCUMENTNUMBER,
   EMAIL,
   IDDOCUMENTTYPE,remark,PROVINCENAME,CITYNAME,COUNTYNAME)
  SELECT S_Customer.Nextval,
         nvl(t.客户姓名, '空'),
         decode(t.性别, '未填', 0, '男', 1, 2),
         decode(t.客户类型, '个人客户', 1, 2),       
         substrb(t.手机号码, 1, 50),         
         1 STATUS,        
         1 CREATORID,
         'Admin' CREATORNAME,
         sysdate CREATETIME,    
         0 Ifcomplain,
         'CSM20141101' || decode(length(rownum),
                                 1,
                                 '00000' || rownum,
                                 2,
                                 '0000' || rownum,
                                 3,
                                 '000' || rownum,
                                 4,
                                 '00' || rownum,
                                 5,
                                 '0' || rownum,
                                 6,
                                 rownum) CODE,
         t.年龄,
         substrb(t.详细地址,1,200),
         substrb(t.邮政编码,1,6),
         substrb(t.家庭电话,1,50),
         substrb(t.单位电话,1,50),
         t.证件号码,
         t.电子邮件,
         decode(t.证件类型,'居民身份证',1),t.客户id,t.省份,t.城市,t.县
    FROM (select distinct t.* from tmpdata.TCONSUMERS141105_1 t
    ) t where not exists(select 1 from CUSTOMER where CUSTOMER.Remark=t.客户id);
    
  --1815  
insert into CUSTOMER select * from  TMPDATA.Customer141105_1;


create table TMPDATA.RetainedCustomer141105_1  as select * from RetainedCustomer where rownum<0;

--1815
insert into TMPDATA.RetainedCustomer141105_1(
  ID                ,
  CUSTOMERID        ,
  IFVISITBACKNEEDED ,
  IFTEXTACCEPTED    ,
  IFADACCEPTED      ,
  CARDNUMBER        ,
  IFVIP             ,
  VIPTYPE           ,
  BONUSPOINTS       ,
  PLATEOWNER        ,
  IFMARRIED         ,
  WEDDINGDAY        ,
  FAMILYINCOME      ,
  LOCATION          ,
  FAMILYCOMPOSITION ,
  REFERRAL          ,
  HOBBY             ,
  SPECIALDAY        ,
  SPECIALDAYDETAILS ,
  USERGENDER        ,
  COMPANYTYPE       ,
  ACQUISITORNAME    ,
  ACQUISITORPHONE   ,
  CREATORID         ,
  CREATORNAME       ,
  CREATETIME        ,
  STATUS            ,
  REMARK            )
  select s_RetainedCustomer.Nextval,
  (select id from tmpdata.Customer141105_1 v where remark=t.客户id),
  0 IFVISITBACKNEEDED ,
  0 IFTEXTACCEPTED    ,
  0 IFADACCEPTED      ,
  '空' CARDNUMBER        ,
  0 IFVIP             ,
  0 VIPTYPE           ,
  0 BONUSPOINTS       ,
  '空' PLATEOWNER        ,
  0 IFMARRIED         ,
  to_date('1899-1-1','yyyy-mm-dd') WEDDINGDAY        ,
  0 FAMILYINCOME      ,
  0 LOCATION          ,
  '空' FAMILYCOMPOSITION ,
  '空' REFERRAL          ,
substrb(t.客户爱好,1,200)  HOBBY             ,
  to_date('1899-1-1','yyyy-mm-dd') SPECIALDAY        ,
  '空' SPECIALDAYDETAILS ,
  0 USERGENDER        ,
  0 COMPANYTYPE       ,
  '空' ACQUISITORNAME    ,
  '空' ACQUISITORPHONE   ,
  1 CREATORID,
  'Admin' CREATORNAME,
  sysdate CREATETIME,
  1 STATUS,
  t.客户id
  from TMPDATA.TCONSUMERS141105_1 t
  where not exists(select 1 from retainedcustomer where retainedcustomer.Remark=t.客户id);
 
--1815 
insert into RetainedCustomer select * from TMPDATA.RetainedCustomer141105_1;

--2054
create table tmpdata.rcustomervehiclelist141105ins as select * from retainedcustomervehiclelist where rownum<1;
insert into tmpdata.rcustomervehiclelist141105ins
  (id, retainedcustomerid, vehicleid, status)
  select s_retainedcustomervehiclelist.nextval,v.id, t.id, 1
    from vehicleinformation t
   inner join tmpdata.RetainedCustomer141105_1 v
      on t.remark = v.remark
  and not exists(select 1 from retainedcustomervehiclelist a where a.vehicleid=t.id);

/*select * from retainedcustomervehiclelist t where exists(select 1 from tmpdata.rcustomervehiclelist141105ins v where t.vehicleid=v.vehicleid) ;
select vehicleid from tmpdata.rcustomervehiclelist141105ins group by vehicleid having count(vehicleid)>1*/
--2054
insert into retainedcustomervehiclelist select * from  tmpdata.rcustomervehiclelist141105ins t
where not exists(select 1 from retainedcustomervehiclelist a where a.vehicleid=t.vehicleid);

create table TMPDATA.Vehiclewarrantycard141105_1 as select * from vehiclewarrantycard where rownum<1;
--2052
insert into TMPDATA.Vehiclewarrantycard141105_1
  (ID,
   CODE,
   VEHICLEID,
   VIN,
   VEHICLECATEGORYID,
   VEHICLECATEGORYNAME,
   SERVICEPRODUCTLINEID,
   SERVICEPRODUCTLINENAME,
   PRODUCTID,
   PRODUCTCODE,
   SALESDATE,
   WARRANTYSTARTDATE,
   RETAINEDCUSTOMERID,
   CUSTOMERNAME,
   CUSTOMERGENDER,
   IDDOCUMENTTYPE,
   IDDOCUMENTNUMBER,
   WARRANTYPOLICYCATEGORY,
   WARRANTYPOLICYID,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_VehicleWarrantyCard.Nextval,
         'VWC20141105' || decode(length(rownum),
                                 1,
                                 '20000' || rownum,
                                 2,
                                 '2000' || rownum,
                                 3,
                                 '200' || rownum,
                                 4,
                                 '20' || rownum,
                                 5,
                                 '2' || rownum,rownum+200000) CODE,
         t.id,
         t.vin,
         1 VEHICLECATEGORYID,
         2 VEHICLECATEGORYNAME,
         (select a.Serviceproductlineid from ServiceProdLineProduct a
             inner join product b on b.brandid = a.brandid where
             b.id = t.productid)SERVICEPRODUCTLINEID,
         (select a.Serviceproductlinename from ServiceProdLineProduct a
             inner join product b on b.brandid = a.brandid where
             b.id = t.productid） SERVICEPRODUCTLINENAME,
         (select id from product a where a.id = t.productid) PRODUCTID,
         (select a.code from product a where a.id = t.productid) PRODUCTCODE,
         t.salesdate,
         nvl(t.salesdate,to_date('1899-1-1','yyyy-mm-dd')) WARRANTYSTARTDATE,
       a.id,
        b.name,
         b.gender,
           b.iddocumenttype,
 b.iddocumentnumber,
      nvl((select distinct d.Category
            from WarrantyPolicy d
           inner join WarrantyPolicyNServProdLine a
              on a. WarrantyPolicyId = d.id
           inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1          
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate and rownum=1),0) WARRANTYPOLICYCATEGORY,
       nvl((select a.WarrantyPolicyId
            from WarrantyPolicyNServProdLine a
          inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1    
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate and rownum=1),0) WARRANTYPOLICYID,
         1 STATUS,
         1 CREATORID,
         'Admin' CREATORNAME,
         sysdate CREATETIME
    from    vehicleinformation t 
    inner join retainedcustomervehiclelist v on t.id=v.vehicleid
 inner join retainedcustomer a on a.id=v.retainedcustomerid
 inner join customer b on b.id=a.customerid where t.salesdate>=to_date('2013-01-01','yyyy-mm-dd')
and exists(select distinct d.Category
            from WarrantyPolicy d
           inner join WarrantyPolicyNServProdLine a
              on a. WarrantyPolicyId = d.id
           inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1          
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate and rownum=1)
          and not exists(select 1 from vehiclewarrantycard where vehiclewarrantycard.vin=t.vin);
          
--2052          
insert into VehicleWarrantyCard
  select * from TMPDATA.VehicleWarrantyCard141105_1  t where t.warrantypolicycategory <>0 ;
--select vehicleid from VehicleWarrantyCard group by vehicleid having count(vehicleid)>1;
--select * from 

create table TMPDATA.VMaintenancePoilcy141105_1 as select * from VehicleMaintenancePoilcy where rownum<0;

--2052
declare
  cursor Cursor_WARRANTYPOLICYID is(
    select WARRANTYPOLICYID,id VEHICLEWARRANTYCARDID from TMPDATA.VehicleWarrantyCard141105_1 where VehicleWarrantyCard141105_1.WARRANTYPOLICYID is not null);
begin
  for S_WARRANTYPOLICYID in Cursor_WARRANTYPOLICYID loop
    insert into TMPDATA.VMaintenancePoilcy141105_1
      select s_VehicleMaintenancePoilcy.Nextval,
             S_WARRANTYPOLICYID.VEHICLEWARRANTYCARDID VEHICLEWARRANTYCARDID,
             t.id                               VEHICLEMAINTETERMID,
             t.code                             VEHICLEMAINTETERMCODE,
             t.name                             VEHICLEMAINTETERMNAME,
             t.maintetype,
             t.workinghours,
             t.capacity,
             t.maxmileage,
             t.MINMILEAGE,
             t.DAYSUPPERLIMIT,
             t.DAYSLOWERLIMIT,
             0 IFEMPLOYED,
             t.DISPLAYORDER
        from VehicleMainteTerm t
       where t.Warrantypolicyid = S_WARRANTYPOLICYID.WARRANTYPOLICYID;
  end loop;
end;
--2052
insert into VehicleMaintenancePoilcy select * from TMPDATA.VMaintenancePoilcy141105_1;

create table TMPDATA.VMPH141105_1 as
select * from VehicleMaintePoilcyHistroy where rownum < 0;

--2052
insert into TMPDATA.VMPH141105_1
  (ID,
   VEHICLEMAINTENANCEPOILCYID,
   VEHICLEWARRANTYCARDID,
   VEHICLEMAINTETERMID,
   VEHICLEMAINTETERMCODE,
   VEHICLEMAINTETERMNAME,
   MAINTETYPE,
   WORKINGHOURS,
   CAPACITY,
   MAXMILEAGE,
   MINMILEAGE,
   DAYSUPPERLIMIT,
   DAYSLOWERLIMIT,
   IFEMPLOYED,
   DISPLAYORDER,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_VehicleMaintePoilcyHistroy.Nextval,
         id,
         VEHICLEWARRANTYCARDID,
         VEHICLEMAINTETERMID,
         VEHICLEMAINTETERMCODE,
         VEHICLEMAINTETERMNAME,
         MAINTETYPE,
         WORKINGHOURS,
         CAPACITY,
         MAXMILEAGE,
         MINMILEAGE,
         DAYSUPPERLIMIT,
         DAYSLOWERLIMIT,
         IFEMPLOYED,
         DISPLAYORDER,
         1,
         1,
         'Admin',
         sysdate
    from TMPDATA.VMaintenancePoilcy141105_1;

--2052
insert into VehicleMaintePoilcyHistroy
  select * from TMPDATA.VMPH141105_1;
  
  

create table tmpdata.vehiclelinkman141101del as
select *
  from vehiclelinkman a
 where linkmantype = 1
   and status <> 99
   and not exists (select 1
          from retainedcustomervehiclelist b
         where b.status = 1
           and a.retainedcustomerid = b.retainedcustomerid
           and a.vehicleinformationid = b.vehicleid);
           
           delete from vehiclelinkman a
 where linkmantype = 1
   and status <> 99
   and not exists (select 1
          from retainedcustomervehiclelist b
         where b.status = 1
           and a.retainedcustomerid = b.retainedcustomerid
           and a.vehicleinformationid = b.vehicleid);

  
   
create table tmpdata.vehiclelinkman141105 as select * from vehiclelinkman where rownum<1;
 select count (*)from vehicleinformation t
   inner join tmpdata.RetainedCustomer141104 v
    on t.remark = v.remark
  --2054 
insert into tmpdata.vehiclelinkman141105
  (id,
   retainedcustomerid,
   vehicleinformationid,
   customercode,
   linkmantype,
   name,
   gender,
   cellphonenumber,
   email,
   address,
   status,
   creatorid,
   creatorname,
   createtime,
   regionid,
   regionname,
   provincename,
   cityname,
   countyname)
  select s_vehiclelinkman.nextval,
         v.id,
         t.id,
         'VL20141105' || decode(length(rownum),
                                1,
                                '00000' || rownum,
                                2,
                                '0000' || rownum,
                                3,
                                '000' || rownum,
                                4,
                                '00' || rownum,
                                5,
                                '0' || rownum,
                                6,
                                rownum),
         1,
         a.name,
         a.gender,
         a.cellphonenumber,
         a.email,
         a.address,
         1,
         1,
         'Admin',
         sysdate,
         a.regionid,
         a.regionname,
         a.provincename,
         a.cityname,
         a.countyname
    from vehicleinformation t
   inner join tmpdata.RetainedCustomer141105_1 v
      on t.remark = v.remark
   inner join customer a
      on a.id=v.customerid
      where not exists(select 1 from vehiclelinkman b where t.id=b.vehicleinformationid and v.id=b.id)
      and exists(select 1 from RetainedCustomer where RetainedCustomer.id=v.id);
      
      insert into tmpdata.vehiclelinkman141105
  (id,
   retainedcustomerid,
   vehicleinformationid,
   customercode,
   linkmantype,
   name,
   gender,
   cellphonenumber,
   email,
   address,
   status,
   creatorid,
   creatorname,
   createtime,
   regionid,
   regionname,
   provincename,
   cityname,
   countyname)
  select s_vehiclelinkman.nextval,
         v.id,
         t.id,
         'VL20141105' || decode(length(rownum),
                                1,
                                '00000' || rownum,
                                2,
                                '0000' || rownum,
                                3,
                                '000' || rownum,
                                4,
                                '00' || rownum,
                                5,
                                '0' || rownum,
                                6,
                                rownum),
         1,
         a.name,
         a.gender,
         a.cellphonenumber,
         a.email,
         a.address,
         1,
         1,
         'Admin',
         sysdate,
         a.regionid,
         a.regionname,
         a.provincename,
         a.cityname,
         a.countyname
    from vehicleinformation t
     inner join tmpdata.RetainedCustomer141101_1 v
      on t.remark = v.remark
   inner join customer a
      on a.id=v.customerid
      where not exists(select 1 from vehiclelinkman b where t.id=b.vehicleinformationid and v.id=b.id)
      and exists(select 1 from RetainedCustomer where RetainedCustomer.id=v.id);
      TMPDATA.RetainedCustomer141101_1
      
      insert into vehiclelinkman select * from  tmpdata.vehiclelinkman141105;
    /* select * from vehiclelinkman where  vehicleinformationid in(select vehicleinformationid from (
      select t.vehicleinformationid, t.linkmantype,status
        from vehiclelinkman t
       where linkmantype = 1
       group by t.vehicleinformationid, t.linkmantype,status
      having count(vehicleinformationid) > 1)) and linkmantype = 1;*/



select * from retainedcustomervehiclelist  t where t.vehicleid
=4280245
create table tmpdata.vehiclelinkman141101del as
select *
  from vehiclelinkman a
 where linkmantype = 1
   and status <> 99
   and not exists (select 1
          from retainedcustomervehiclelist b
         where b.status = 1
           and a.retainedcustomerid = b.retainedcustomerid
           and a.vehicleinformationid = b.vehicleid);
           
           delete from vehiclelinkman a
 where linkmantype = 1
   and status <> 99
   and not exists (select 1
          from retainedcustomervehiclelist b
         where b.status = 1
           and a.retainedcustomerid = b.retainedcustomerid
           and a.vehicleinformationid = b.vehicleid);


insert into tmpdata.vehiclelinkman141101del
select * from vehiclelinkman where  vehicleinformationid in(select vehicleinformationid from (
      select t.vehicleinformationid, t.linkmantype,status
        from vehiclelinkman t
       where linkmantype = 1
       group by t.vehicleinformationid, t.linkmantype,status
      having count(vehicleinformationid) > 1)) and linkmantype = 1;
     
delete from vehiclelinkman where  vehicleinformationid in(select vehicleinformationid from (
      select t.vehicleinformationid, t.linkmantype,status
        from vehiclelinkman t
       where linkmantype = 1
       group by t.vehicleinformationid, t.linkmantype,status
      having count(vehicleinformationid) > 1)) and linkmantype = 1;
      
      
create table tmpdata.vehiclelinkman141105_1 as select * from vehiclelinkman where rownum<1;
--1766
  insert into tmpdata.vehiclelinkman141105_1
    (id,
     retainedcustomerid,
     vehicleinformationid,
     customercode,
     linkmantype,
     name,
     gender,
     cellphonenumber,
     email,
     address,
     status,
     creatorid,
     creatorname,
     createtime,
     regionid,
     regionname,
     provincename,
     cityname,
     countyname)
    select s_vehiclelinkman.nextval,
           v.id,
           t.id,
           'VL20141105' || decode(length(rownum),
                                  1,
                                  '00000' || rownum,
                                  2,
                                  '0000' || rownum,
                                  3,
                                  '000' || rownum,
                                  4,
                                  '00' || rownum,
                                  5,
                                  '0' || rownum,
                                  6,
                                  rownum),
           1,
           a.name,
           a.gender,
           a.cellphonenumber,
           a.email,
           a.address,
           1,
           1,
           'Admin',
           sysdate,
           a.regionid,
           a.regionname,
           a.provincename,
           a.cityname,
           a.countyname
      from vehicleinformation t
     inner join retainedcustomervehiclelist c
        on c.vehicleid = t.id
       and c.status = 1
     inner join retainedcustomer v
        on c.retainedcustomerid = v.id
     inner join customer a
        on a.id = v.customerid
     where not exists (select 1
              from vehiclelinkman b
             where t.id = b.vehicleinformationid
               and v.id = b.retainedcustomerid
               and b.linkmantype = 1
               and status = 1);
               --1766
               insert into vehiclelinkman select * from tmpdata.vehiclelinkman141105_1;
select * from vehiclelinkman where exists(select 1 from )

select distinct createtime,customer.creatorname from customer where remark in(
select  remark from customer where remark is not null group by remark having count (remark)>1)
1611
create table tmpdata.customer141105del as select * from 
customer where remark is not null
 and customer.id not in
       (select max(customer.id)
          from customer where remark is not null
         group by remark);
         
delete customer where remark is not null
 and customer.id not in
       (select max(customer.id)
          from customer where remark is not null
         group by remark)
insert into customer select * from   tmpdata.customer141105del;

--
create table tmpdata.retainedcustomer141105up as select id,customerid ,remark from retainedcustomer t 
where not exists(select 1 from customer where t.customerid =customer.id and t.remark=customer.remark);
select * from tmpdata.retainedcustomer141105up;

--635
update retainedcustomer t set t.customerid=(select id from  customer  v where v.remark=t.remark)
where not exists(select 1 from customer where t.customerid =customer.id and t.remark=customer.remark)
and t.remark is not null;
create table tmpdata.vehiclelinkman1105up as
select id,retainedcustomerid,vehicleinformationid,linkmantype
  from vehiclelinkman a
 where linkmantype <> 1
   and not exists (select 1
          from retainedcustomervehiclelist b
         where a.retainedcustomerid = b.retainedcustomerid
           and a.vehicleinformationid = b.vehicleid
           and status = 1)
   and  exists (select 1
          from retainedcustomervehiclelist b
         where a.vehicleinformationid = b.vehicleid
           and status = 1);

update vehiclelinkman a
   set a.retainedcustomerid =
       (select b.retainedcustomerid
          from retainedcustomervehiclelist b
         where a.vehicleinformationid = b.vehicleid
           and status = 1)
 where linkmantype <> 1
   and not exists (select 1
          from retainedcustomervehiclelist b
         where a.retainedcustomerid = b.retainedcustomerid
           and a.vehicleinformationid = b.vehicleid
           and status = 1)
   and exists (select 1
          from retainedcustomervehiclelist b
         where a.vehicleinformationid = b.vehicleid
           and status = 1);
