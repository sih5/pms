update tmpdata.tagentstores_141101_nfbf set warehousename= '工程车配件西安代理库'  where warehousename='瑞沃配件西安代理库';
update  tmpdata.tagentstores_141101_nfbf set warehousecode='XADLK'where   warehousename ='工程车配件西安代理库';

--864
create table tmpdata.warehousearea141105_1 as select * from warehousearea where rownum<1;
insert into tmpdata.warehousearea141105_1(id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
select s_warehousearea.nextval,
       warehouseid,
       parentid,
       locationcode,
       parentid,
       89,
       1,
       '工程车代理库库位批量新增',
       3,
       1,
       'Admin',
       sysdate
  from (select distinct a.warehousecode,
                        b.warehouseareacode,
                        a.locationcode,
                        warehouse.id        warehouseid,
                        warehousearea.id    parentid --,warehouse.code
          from tmpdata.tagentstores_141101_nfbf a
         inner join tmpdata.agentwlrel141027 b
            on a.warehousecode = b.warehousecode
         inner join warehouse
            on warehouse.code = a.warehousecode
         inner join warehousearea
            on warehouse.id = warehousearea.warehouseid
         where not exists (select 1
                  from warehousearea
                 inner join warehousearea parent
                    on warehousearea.parentid = parent.id
                 inner join warehouse
                    on warehouse.id = warehousearea.warehouseid
                 where warehousearea.code = a.locationcode
                   and b.warehouseareacode = parent.code
                   and warehouse.code = a.warehousecode)
           and warehousearea.areacategoryid = 89
           and warehousearea.areakind = 2
           and b.warehouseareacode = warehousearea.code and locationcode is not null
           and a.warehousecode='XADLK');
--  and a.warehousecode='SYDLK'
insert into warehousearea select * from tmpdata.warehousearea141105_1;/*
delete from warehousearea where exists(select 1 from  tmpdata.warehousearea141027 where warehousearea141027.id=warehousearea.id)*/
   /*
select * from  tmpdata.tagentstores_141022_nfbf a
 inner join tmpdata.agentwlrel141027 b  on a.warehousecode = b.warehousecode
 inner join warehouse
    on warehouse.code = a.warehousecode
 inner join warehousearea
    on warehouse.id = warehousearea.warehouseid
    where not exists (select 1
          from warehousearea
         inner join warehousearea parent
            on warehousearea.parentid = parent.id
         inner join warehouse
            on warehouse.id = warehousearea.id
         where warehousearea.code = a.locationcode
           and b.warehouseareacode = parent.code
           and warehouse.code = a.warehousecode)
           and warehousearea.areacategoryid = 89
              and warehousearea.areakind = 2
               and b.warehouseareacode = warehousearea.code
    and a.warehousecode='SYDLK'
    select * from warehousearea where */

create table tmpdata.partsstock141105_1 as select * from partsstock where rownum<1;
insert into tmpdata.partsstock141105_1
  (id,
   warehouseid,
   storagecompanyid,
   storagecompanytype,
   branchid,
   warehouseareaid,
   warehouseareacategoryid,
   partid,
   quantity,
   creatorid,
   creatorname,
   createtime)
  select s_partsstock.nextval,
         warehouse.id,
         warehouse.storagecompanyid,
         warehouse.storagecompanytype,
         8481,
         (select warehousearea.id
            from warehousearea
           inner join warehousearea parent
              on warehousearea.parentid = parent.id
           where a.locationcode = warehousearea.code
             and b.warehouseareacode = parent.code
             and warehousearea.areakind = 3
             and warehousearea.warehouseid = warehouse.id
             and warehousearea.areacategoryid = 89)
         -- wa
        ,
         89,
         (select id from sparepart where code = a.materialcode),
         a.usableqty,
         1,
         'Admin',
         sysdate
    from tmpdata.tagentstores_141101_nfbf a
   inner join tmpdata.agentwlrel141027 b
      on a.warehousecode = b.warehousecode
  -- and a.areacode = b.warehouseareacode
   inner join warehouse
      on warehouse.code = a.warehousecode
  -- inner join warehousearea
  --   on warehouse.id = warehousearea.warehouseid
   where exists (select 1
            from warehousearea
           inner join warehousearea parent
              on warehousearea.parentid = parent.id         
           where warehousearea.code = a.locationcode
             and b.warehouseareacode = parent.code
             and warehouse.id = warehousearea.warehouseid
             and warehousearea.areakind = 3
             and warehousearea.areacategoryid = 89)
             and a.warehousecode='XADLK';
             
insert into partsstock select * from  tmpdata.partsstock141105_1;


    select  a.warehousecode,sum(nvl(a.usableqty,0)),sum (a.usableqty*a.stdprice) from tmpdata.tagentstores_141101_nfbf a
    inner join  tmpdata.agentwlrel141027 b
      on a.warehousecode = b.warehousecode
    where a.locationcode is not null group by a.warehousecode;
    select sum(nvl(t.quantity, 0)),
           (select code from warehouse where id = warehouseid) warehousecode
      from tmpdata.partsstock141105_1 t
     group by warehouseid;
