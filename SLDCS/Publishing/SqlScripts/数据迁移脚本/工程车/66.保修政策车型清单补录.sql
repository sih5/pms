create table tmpdata.VMainteProductCategory141106 (ProductCategoryCode varchar2(100),productline varchar2(100),materialfee number(9,4))
create table tmpdata.VMainteProductCategory141106_1 as select * from VehicleMainteProductCategory where rownum<1;
--760
insert into tmpdata.VMainteProductCategory141106_1
  (id,
   vehiclemaintetermid,
   productcategoryname,
   materialcost,
   productcategorycode)
  select s_VehicleMainteProductCategory.Nextval,
         e.id,
         a.productcategorycode,
         a.materialfee,
         a.productcategorycode
    from tmpdata.VMainteProductCategory141106 a
   inner join serviceproductlineview b
      on b.ProductLineName = a.productline
   inner join WarrantyPolicyNServProdLine c
      on c.serviceproductlineid = b.ProductLineId
  --inner join WarrantyPolicy d on d.id=c.warrantypolicyid
   inner join VehicleMainteTerm e
      on e.warrantypolicyid = c.warrantypolicyid
   where e.maintetype = 1;
   
   select vehiclemaintetermid,productcategorycode from tmpdata.VMainteProductCategory141106_1 group by vehiclemaintetermid,productcategorycode having count(productcategorycode)>1
   insert into VehicleMainteProductCategory select *from tmpdata.VMainteProductCategory141106_1 t
   where not exists(select 1 from VehicleMainteProductCategory v where v.vehiclemaintetermid=t.vehiclemaintetermid and v.productcategorycode=t.productcategorycode);
