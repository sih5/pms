create table tmpdata.dealerserviceinfo141107 (品牌 varchar2(100),	服务站编号 varchar2(100),	市场部	varchar2(100), 星级 varchar2(100),
外出服务费等级 varchar2(100),	配件管理费率等级 varchar2(100),partssalescategoryid varchar2(100),
dealerid varchar2(100),marketingdepartmentid varchar2(100),partsmanagingfeegradeid  varchar2(100),gradecoefficientid  varchar2(100),OutFeeGradeId  varchar2(100)
);
update tmpdata.dealerserviceinfo141107
   set partssalescategoryid =
       (select id
          from partssalescategory
         where name = dealerserviceinfo141107.品牌),
       dealerid            =
       (select id
          from dealer
         where code = dealerserviceinfo141107.服务站编号);
         
update tmpdata.dealerserviceinfo141107 a
set marketingdepartmentid=(select id from marketingdepartment b where a.partssalescategoryid=b.partssalescategoryid and a.市场部=b.name);

update tmpdata.dealerserviceinfo141107 a
set partsmanagingfeegradeid=(select id from PartsManagementCostGrade b where a.partssalescategoryid=b.partssalescategoryid and a.配件管理费率等级=b.name);

select distinct 外出服务费等级 from tmpdata.dealerserviceinfo141107

update  tmpdata.dealerserviceinfo141107 set 外出服务费等级='GCCBQ-WCDJ' where 外出服务费等级= 'GCCBQWCDJ';
 
update tmpdata.dealerserviceinfo141107 a
set OutFeeGradeId=(select id from ServiceTripPriceGrade b where a.外出服务费等级=b.code);

select distinct 星级,partssalescategoryid from tmpdata.dealerserviceinfo141107;
select * from GradeCoefficient;
update tmpdata.dealerserviceinfo141107 set 星级 ='达标'where 星级='达标站';

update tmpdata.dealerserviceinfo141107 a
set gradecoefficientid=(select id from GradeCoefficient b where b.grade=a.星级 and b.partssalescategoryid=a.partssalescategoryid);

create table tmpdata.dealerserviceinfo141107up 
as 
select t.id,
       t.partssalescategoryid,
       t.dealerid,
       t.marketingdepartmentid,
       t.partsmanagingfeegradeid,
       t.gradecoefficientid,
       t.OutFeeGradeId
  from dealerserviceinfo t where exists(select 1 from tmpdata.dealerserviceinfo141107  a inner join partssalescategory b on a.品牌=b.name
  inner join dealer c on c.code=a.服务站编号
  where t.dealerid=c.id and t.partssalescategoryid=b.id);

update dealerserviceinfo t
   set marketingdepartmentid =
       (select a.marketingdepartmentid
           from tmpdata.dealerserviceinfo141107 a
         where t.dealerid = a.dealerid
           and t.partssalescategoryid = a.partssalescategoryid)
 where exists
 (select 1
          from tmpdata.dealerserviceinfo141107 a
         where t.dealerid = a.dealerid
           and t.partssalescategoryid = a.partssalescategoryid and a.marketingdepartmentid is not null);
           
--328
update dealerserviceinfo t
   set partsmanagingfeegradeid =
       (select a.partsmanagingfeegradeid
           from tmpdata.dealerserviceinfo141107 a
         where t.dealerid = a.dealerid
           and t.partssalescategoryid = a.partssalescategoryid)
 where exists
 (select 1
          from tmpdata.dealerserviceinfo141107 a
         where t.dealerid = a.dealerid
           and t.partssalescategoryid = a.partssalescategoryid and a.partsmanagingfeegradeid is not null);

--397           
update dealerserviceinfo t
   set gradecoefficientid =
       (select a.gradecoefficientid
           from tmpdata.dealerserviceinfo141107 a
         where t.dealerid = a.dealerid
           and t.partssalescategoryid = a.partssalescategoryid)
 where exists
 (select 1
          from tmpdata.dealerserviceinfo141107 a
         where t.dealerid = a.dealerid
           and t.partssalescategoryid = a.partssalescategoryid and a.gradecoefficientid is not null); 
 
--702           
  update dealerserviceinfo t
   set OutFeeGradeId =
       (select a.OutFeeGradeId
           from tmpdata.dealerserviceinfo141107 a
         where t.dealerid = a.dealerid
           and t.partssalescategoryid = a.partssalescategoryid)
 where exists
 (select 1
          from tmpdata.dealerserviceinfo141107 a
         where t.dealerid = a.dealerid
           and t.partssalescategoryid = a.partssalescategoryid and a.OutFeeGradeId is not null); 
           
                   
