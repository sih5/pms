select * from tmpdata.tpurchaseinfos_20141110
--918300
update vehicleinformation t
   set t.invoicedate =
       (select invoicedate
          from tmpdata.tpurchaseinfos_20141110 v
         where v.vincode = t.vin)
 where exists (select 1
          from tmpdata.tpurchaseinfos_20141110 v
         where v.vincode = t.vin
           and v.invoicedate is not null)
   and t.invoicedate is null;
