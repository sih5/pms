create table tmpdata.WarrantyPolicy140918(BranchCode varchar2(50),BranchName varchar2(100),code varchar2(50),name varchar2(100),Category varchar(10),RepairTermInvolved varchar2(2),MainteTermInvolved varchar2(2),PartsWarrantyTermInvolved varchar2(2),ReleaseDate date)
create table tmpdata.warrantypolicy140918_1 as select * from warrantypolicy where rownum<1;
insert into tmpdata.warrantypolicy140918_1
  (id,
   branchid,
   branchcode,
   branchname,
   code,
   name,
   category,
   applicationscope,
   repairterminvolved,
   mainteterminvolved,
   partswarrantyterminvolved,
   releasedate,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_warrantypolicy.nextval,,
         t.branchcode,
         t.branchname,
         t.code,
         t.name,
         decode(t.category, '标准保修', 1),
         1,
         decode(t.repairterminvolved, '是', 1, '否', 0),
         decode(t.mainteterminvolved, '是', 1, '否', 0),
         decode(t.partswarrantyterminvolved, '是', 1, '否', 0),
         t.releasedate,
         1,
         1,
         'Admin',
         sysdate
    from tmpdata.WarrantyPolicy140918 t;
    insert into warrantypolicy select * from  tmpdata.warrantypolicy140918_1;
--select * from keyvalueitem where caption='服务适用范围'

create table tmpdata.warrantypolicyhistory140922 as select * from warrantypolicyhistory where rownum<1;
insert into tmpdata.warrantypolicyhistory140922
  (id,
   masterrecordid,
   code,
   name,
   category,
   applicationscope,
   repairterminvolved,
   mainteterminvolved,
   partswarrantyterminvolved,
   releasedate,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_warrantypolicyhistory.nextval,
         id,
         code,
         name,
         category,
         applicationscope,
         repairterminvolved,
         mainteterminvolved,
         partswarrantyterminvolved,
         releasedate,
         status,
         1,
         'Admin',
         sysdate
    from tmpdata.warrantypolicy140918_1;
insert into warrantypolicyhistory select * from tmpdata.warrantypolicyhistory140922 ;
