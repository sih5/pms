--插入临时表
create table  tmpdata.nodeaction141111 (LV3 varchar2(100),	按钮 varchar2(100),	角色 varchar2(100))
create table tmpdata.rule141111
as
select * from rule where 1=0;
--插入角色
--22
create table tmpdata.role141111 as select * from role where rownum<1;
insert into tmpdata.role141111
  (id,
   enterpriseid,
   name,
   isadmin,
   status,
   remark,
   creatorid,
   creatorname,
   createtime)
  select s_role.nextval,
         8481,
         角色,
         0,
         1,
         '初始化角色',
         1,
         '系统管理员',
         sysdate
    from (select distinct 角色 from tmpdata.nodeaction141111);
insert into role select * from tmpdata.role141111;

update role set enterpriseid=8481 where exists(select 1 from  tmpdata.role141111 where role141111.id=role.id);

--4313
select * from tmpdata.nodeaction141111 t where
t.按钮='查询'
select count(*) from tmpdata.rule141111

select distinct lv3 from  tmpdata.nodeaction141111 t where not exists(select  1 from page where page.name=t.lv3)

select distinct lv3,t.按钮 from  tmpdata.nodeaction141111 t where not exists (select n.id
            from page p
            left join action a
              on p.id = a.pageid
            left join node n
              on n.categoryid = a.id
             and n.categorytype = 1
           where a.name = t.按钮
             and p.name = t.lv3);and t.按钮<>'查询'

--插入授权
--2157
create table tmpdata.rule141111 as select * from rule where rownum<1;
insert into tmpdata.rule141111
  (id, roleid, nodeid)
  select s_rule.nextval,
         (select id
            from tmpdata.role141111 r
           where r.name = t.角色
             and r.enterpriseid =
                 8481) roleid,
         (select n.id
            from page p
            left join action a
              on p.id = a.pageid
            left join node n
              on n.categoryid = a.id
             and n.categorytype = 1
           where a.name = t.按钮
             and p.name = t.lv3) as nodeid
    from tmpdata.nodeaction141111 t
   where t.按钮 <>'查询'
     and exists (select n.id
            from page p
            left join action a
              on p.id = a.pageid
            left join node n
              on n.categoryid = a.id
             and n.categorytype = 1
           where a.name = t.按钮
             and p.name = t.lv3);
--2042
insert into tmpdata.rule141111
  (id, roleid, nodeid)
  select --s_rule.nextval,
         (select id
            from tmpdata.role141111 r
           where r.name = t.角色
             and r.enterpriseid =8481) roleid,
         (select n.id
            from page p
           left join node n
              on n.categoryid = p.id
             and n.categorytype = 0
           where p.name = t.lv3) as nodeid 
    from tmpdata.nodeaction141111 t
   where t.按钮 = '查询'
    /* and exists (select n.id
            from page p
           left join node n
              on n.categoryid = p.id
             and n.categorytype = 0
           where p.name = t.lv3)*/
           and t.lv3 not in (select p.name from page p
group by p.name having count(*)>1);
--4199
insert into rule
  (id, roleid, nodeid)
  select id, roleid, nodeid
    from tmpdata.rule141111 t
   where not exists (select 1
            from rule v
           where v.nodeid = t.nodeid
             and v.roleid = t.roleid);


--人员信息
