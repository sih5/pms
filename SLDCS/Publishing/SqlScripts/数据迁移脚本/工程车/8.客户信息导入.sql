
select count(*) from TMPDATA.Customer140705
create table TMPDATA.Customer140918 as SELECT * FROM Customer WHERE ROWNUM<0;
create index tpurchaseinfos140917_cidx on tmpdata.tpurchaseinfos140917 (客户row_id);
 TPURCHASEINFOS140705
--插入基础客户信息临时表 852612
INSERT INTO TMPDATA.Customer140918
  (ID,
   NAME,
   GENDER,
   CUSTOMERTYPE,
   CELLPHONENUMBER,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME,
   Ifcomplain,
   customercode,
   AGE,   
   ADDRESS,
   POSTCODE,
   HOMEPHONENUMBER,
   OFFICEPHONENUMBER,
   IDDOCUMENTNUMBER,
   EMAIL,
   IDDOCUMENTTYPE,remark,PROVINCENAME,CITYNAME,COUNTYNAME)
  SELECT S_Customer.Nextval,
         nvl(t.客户姓名, '空'),
         decode(t.性别, '未填', 0, '男', 1, 2),
         decode(t.客户类型, '个人客户', 1, 2),       
         substrb(t.手机号码, 1, 50),         
         1 STATUS,        
         1 CREATORID,
         'Admin' CREATORNAME,
         sysdate CREATETIME,    
         0 Ifcomplain,
         'CSM20140918' || decode(length(rownum),
                                 1,
                                 '00000' || rownum,
                                 2,
                                 '0000' || rownum,
                                 3,
                                 '000' || rownum,
                                 4,
                                 '00' || rownum,
                                 5,
                                 '0' || rownum,
                                 6,
                                 rownum) CODE,
         t.年龄,
         substrb(t.详细地址,1,200),
         substrb(t.邮政编码,1,6),
         substrb(t.家庭电话,1,50),
         substrb(t.单位电话,1,50),
         t.证件号码,
         t.电子邮件,
         decode(t.证件类型,'居民身份证',1),t.客户id,t.省份,t.城市,t.县
    FROM (select distinct t.* from tmpdata.tconsumers140918 t
    inner join  tmpdata.tpurchaseinfos140917 v on t.客户id=v.客户row_id) t;
    select count(*)from tmpdata.tconsumers140918 t 
     inner join  tmpdata.tpurchaseinfos140917 v on t.客户id=v.客户row_id;
-- SELECT * FROM DCS.TILEDREGION 

--插入基础客户表 
insert into CUSTOMER select * from  TMPDATA.Customer140918;
delete from CUSTOMER where exists(select 1 from  TMPDATA.Customer140918 t where t.id=CUSTOMER.id)
select * from TMPDATA.Customer140918 t where remark in(
select t.remark from  TMPDATA.Customer140918 t group by t.remark having count (remark)>1) order by remark

select t.客户id from tmpdata.tconsumers140918 t group by t.客户id having count(t.客户id)>1

--创建保有客户临时表
create table TMPDATA.RetainedCustomer140918  as select * from RetainedCustomer where rownum<0;
create index tmpdata.CUSTOMER140918_rmk on TMPDATA.CUSTOMER140918 (remark);
--插入保有客户临时表 852612
insert into TMPDATA.RetainedCustomer140918(
  ID                ,
  CUSTOMERID        ,
  IFVISITBACKNEEDED ,
  IFTEXTACCEPTED    ,
  IFADACCEPTED      ,
  CARDNUMBER        ,
  IFVIP             ,
  VIPTYPE           ,
  BONUSPOINTS       ,
  PLATEOWNER        ,
  IFMARRIED         ,
  WEDDINGDAY        ,
  FAMILYINCOME      ,
  LOCATION          ,
  FAMILYCOMPOSITION ,
  REFERRAL          ,
  HOBBY             ,
  SPECIALDAY        ,
  SPECIALDAYDETAILS ,
  USERGENDER        ,
  COMPANYTYPE       ,
  ACQUISITORNAME    ,
  ACQUISITORPHONE   ,
  CREATORID         ,
  CREATORNAME       ,
  CREATETIME        ,
  STATUS            ,
  REMARK            ）
  select s_RetainedCustomer.Nextval,
  (select id from tmpdata.Customer140918 v where remark=t.客户id),
  0 IFVISITBACKNEEDED ,
  0 IFTEXTACCEPTED    ,
  0 IFADACCEPTED      ,
  '空' CARDNUMBER        ,
  0 IFVIP             ,
  0 VIPTYPE           ,
  0 BONUSPOINTS       ,
  '空' PLATEOWNER        ,
  0 IFMARRIED         ,
  to_date('1899-1-1','yyyy-mm-dd') WEDDINGDAY        ,
  0 FAMILYINCOME      ,
  0 LOCATION          ,
  '空' FAMILYCOMPOSITION ,
  '空' REFERRAL          ,
substrb(t.客户爱好,1,200)  HOBBY             ,
  to_date('1899-1-1','yyyy-mm-dd') SPECIALDAY        ,
  '空' SPECIALDAYDETAILS ,
  0 USERGENDER        ,
  0 COMPANYTYPE       ,
  '空' ACQUISITORNAME    ,
  '空' ACQUISITORPHONE   ,
  1 CREATORID,
  'Admin' CREATORNAME,
  sysdate CREATETIME,
  1 STATUS,
  t.客户id
  from TMPDATA.TCONSUMERS140918 t/*;
  commit;*/
  where exists(select 1 from   tmpdata.TPURCHASEINFOS140917 v where t.客户id=v.客户row_id)

632735
select count(*)  from TMPDATA.TCONSUMERS140705 t
  where exists(select 1 from   tmpdata.TPURCHASEINFOS140705 v where t.客户id=v.客户row_id)
--插入保有客户Id
insert into RetainedCustomer select * from TMPDATA.RetainedCustomer140918;
gcdcs.retainedcustomervehiclelist
create table tmpdata.rcustomervehiclelist140918 as select * from gcdcs.retainedcustomervehiclelist where rownum<1;
insert into tmpdata.rcustomervehiclelist140918(id,retainedcustomerid,vehicleid,status)
select s_retainedcustomervehiclelist.nextval,t.id,v.id,1 from
tmpdata.vehicleinformation14917 t inner join tmpdata.retainedcustomer140918 v
on t.remark=v.remark
select count (*)from tmpdata.rcustomervehiclelist140918 --916905
create table tmpdata.vehiclelinkman140918 as select * from vehiclelinkman where rownum<1;


update TMPDATA.Customer140918 v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname = v.cityname
           and a.countyname is null),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname = v.cityname
                          and a.countyname is null)
 where v.provincename is not null
   and v.cityname is not null
   and v.countyname is null
   and exists
 (select * from tmpdata.vehiclelinkman140806_1 t where t.id = v.id);

update TMPDATA.Customer140918 v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname = v.cityname
           and a.countyname =v.countyname),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname = v.cityname
                          and a.countyname =v.countyname)
 where v.provincename is not null
   and v.cityname is not null
   and v.countyname is not null
   and regionid is null
  
 
update TMPDATA.Customer140918 v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname is null
           and a.countyname is null),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname is null
                          and a.countyname is null)
 where v.provincename is not null
   and v.cityname is  null
   and v.countyname is  null
   and regionid is null;

insert into tmpdata.vehiclelinkman140918
  (id,
   retainedcustomerid,
   vehicleinformationid,
   customercode,
   linkmantype,
   name,
   gender,
   cellphonenumber,
   email,
   address,
   status,
   creatorid,
   creatorname,
   createtime,
   regionid,
   regionname,
   provincename,
   cityname,
   countyname)
  select s_vehiclelinkman.nextval,
         v.id,
         t.id,
         'VL20140918' || decode(length(rownum),
                                1,
                                '00000' || rownum,
                                2,
                                '0000' || rownum,
                                3,
                                '000' || rownum,
                                4,
                                '00' || rownum,
                                5,
                                '0' || rownum,
                                6,
                                rownum),
         1,
         a.name,
         a.gender,
         a.cellphonenumber,
         a.email,
         a.address,
         1,
         1,
         'Admin',
         sysdate,
         a.regionid,
         a.regionname,
         a.provincename,
         a.cityname,
         a.countyname
    from tmpdata.vehicleinformation14917 t
   inner join tmpdata.retainedcustomer140918 v
      on t.remark = v.remark
   inner join TMPDATA.Customer140918 a
      on t.remark = a.remark
select count(*)from  tmpdata.vehiclelinkman140918

insert into vehiclelinkman select * from tmpdata.vehiclelinkman140918
create table tmpdata.vehiclelinkman140918_1 as select * from vehiclelinkman where rownum<1;
insert into tmpdata.vehiclelinkman140918_1
  (id,
   retainedcustomerid,
   vehicleinformationid,
   customercode,
   linkmantype,
   name,
   gender,
   cellphonenumber,
   email,
   address,
   status,
   creatorid,
   creatorname,
   createtime,
 
   provincename,
   cityname,
   countyname)
   select  s_vehiclelinkman.nextval,
         c.id,
         e.id,
         'VL20140917' || decode(length(rownum),
                                1,
                                '00000' || rownum,
                                2,
                                '0000' || rownum,
                                3,
                                '000' || rownum,
                                4,
                                '00' || rownum,
                                5,
                                '0' || rownum,
                                6,
                                rownum),
         2,
         a.name,
         0,
         a.custmobile,
         '',
         a.address,
         1,
         1,
         'Admin',
         sysdate,
        
         a.prov_name,
         a.city_name,
         a.town_name
   from tmpdata.tPurchaseLinkers_bf_140927 a
   inner join tmpdata.tpurchaseinfos_bf_140916 b on a.parentid=b.row_id
   inner join tmpdata.vehicleinformation14917 e on e.vin=b.Bone_Code 
    inner join tmpdata.retainedcustomer140918 c
      on e.remark = c.remark
   inner join TMPDATA.Customer140918 d
      on c.remark = d.remark where a.type<>0;

insert into tmpdata.vehiclelinkman140918_1
  (id,
   retainedcustomerid,
   vehicleinformationid,
   customercode,
   linkmantype,
   name,
   gender,
   cellphonenumber,
   email,
   address,
   status,
   creatorid,
   creatorname,
   createtime,
 
   provincename,
   cityname,
   countyname)
   select  s_vehiclelinkman.nextval,
         c.id,
         e.id,
         'VL20140916' || decode(length(rownum),
                                1,
                                '00000' || rownum,
                                2,
                                '0000' || rownum,
                                3,
                                '000' || rownum,
                                4,
                                '00' || rownum,
                                5,
                                '0' || rownum,
                                6,
                                rownum),
         2,
         a.name,
         0,
         a.custmobile,
         '',
         a.address,
         1,
         1,
         'Admin',
         sysdate,
        
         a.prov_name,
         a.city_name,
         a.town_name
   from tmpdata.tPurchaseLinkers_nf_140927 a
   inner join tmpdata.tpurchaseinfos_nf_140915 b on a.parentid=b.row_id
   inner join tmpdata.vehicleinformation14917 e on e.vin=b.Bone_Code 
    inner join tmpdata.retainedcustomer140918 c
      on e.remark = c.remark
   inner join TMPDATA.Customer140918 d
      on c.remark = d.remark  where a.type<>0 ;
  
  
update TMPDATA.vehiclelinkman140918_1 v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname = v.cityname
           and a.countyname is null),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname = v.cityname
                          and a.countyname is null)
 where v.provincename is not null
   and v.cityname is not null
   and v.countyname is null
   ;

update TMPDATA.vehiclelinkman140918_1 v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname = v.cityname
           and a.countyname =v.countyname),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname = v.cityname
                          and a.countyname =v.countyname)
 where v.provincename is not null
   and v.cityname is not null
   and v.countyname is not null
   and regionid is null
  ;
 
update TMPDATA.vehiclelinkman140918_1 v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname is null
           and a.countyname is null),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname is null
                          and a.countyname is null)
 where v.provincename is not null
   and v.cityname is  null
   and v.countyname is  null
   and regionid is null;    
      insert into vehiclelinkman select * from  TMPDATA.vehiclelinkman140918_1 v;
        insert into vehiclelinkman select * from  TMPDATA.vehiclelinkman140918 v;
        delete from  vehiclelinkman where exists(select 1 from TMPDATA.vehiclelinkman140918_1 v  where v.id=vehiclelinkman.id)
