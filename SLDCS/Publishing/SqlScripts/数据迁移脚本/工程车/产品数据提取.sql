

Create table tmpdata.tstdproductin140917
as
select v.ProductCode       as 产品编号,
       v.ProductName       as 车型名称,
       v.MainBrandCode     as 品牌代码,
       v.MainBrandname     as 品牌名称,
       v.SubBrandCode      as 子品牌代码,
       v.SubBrandName      as 子品牌名称,
       v.PlatFormCode      as 平台代码,
       v.PlatFormName      as 平台名称,
       v.PKind             as 产品线代码,
       v.LineName          as 产品线名称,
       v.InnerCode         as 原始内部编号,
       v.BulletinCode      as 公告号,
       v.Load              as 吨位代码,
       v.EngineType        as 发动机型号代码,
       v.EngineFactoryCode as 发动机生产厂家代码,
       v.GearType          as 变速箱型号代码,
       v.GearFactoryCode   as 变速箱生产厂家代码,
       v.BrideType         as 后桥型号代码,
       v.BrideFactoryCode  as 后桥生产厂家代码,
       v.TyreType          as 轮胎型号代码,
       v.TyreFactoryCode   as 轮胎形式代码,
       v.brakeType         as 制动方式代码,
       v.DriveType         as 驱动方式代码,
       v.FrameType         as 车架连接形式代码,
       v.FSize             as 整车尺寸,
       v.wheelbase         as 轴距代码,
       v.emissionType      as 排放标准代码,
       v.PFunctionType     as 产品功能代码,
       v.Pstand            as 产品档次代码,
       nvl(t.vehiclecode,v.vtype)           as 车辆类型代码,
       nvl(t.vehiclename,v.vtype)           as 车辆类型名称,
       v.Color             as 颜色描述,
       v.SEATCOUNTER       as 载客个数,
       v.WEIGHT            as 自重
      -- tbrands.name                    as 服务品牌,
       --tproductlines.name              as 服务产品线
  from tmpdata.tstdproductin_nfbfall_140917
 v
  left join tmpdata.tbrandlinkin_nfbf_140917 t on t.vehiclecode=v.vtype;
--where exists(select 1 from tmpdata.TPURCHASEINFOS140712 t where v.productcode = t.产品编号)
   
