create table tmpdata.TCONSUMERS140918
as
  select distinct CUSTOMER_NAME 客户姓名,
                  性别,
                  case
                    when namelength <= 6 then
                     '个人客户'
                    else
                     '单位客户'
                  end as 客户类型,
                  年龄,
                  证件类型,
                  证件号码,
                  区域,
                  省份,
                  城市,
                  县,
                  邮政编码,
                  家庭电话,
                  单位电话,
                  手机号码,
                  电子邮件,
                  职务,
                  文化程度,
                  客户爱好,
                  详细地址,
                  客户id,'nf' as type 
    from (select tconsumers_nf_140927.customer_name as CUSTOMER_NAME,
                 decode(tconsumers_nf_140927.Sex, 0, '女', 1, '男', '未填') as 性别,
                 lengthb(tconsumers_nf_140927.customer_name) as namelength,
                 tconsumers_nf_140927.age as  年龄,
                 '居民身份证' as  证件类型,
                 tconsumers_nf_140927.ID_card as  证件号码,
                 ' ' as  区域,
                 tconsumers_nf_140927.prov_name as  省份,
                 tconsumers_nf_140927.city_name as  城市,
                  tconsumers_nf_140927.town_name as  县,
                 tconsumers_nf_140927.post 邮政编码,
                 tconsumers_nf_140927.home_phone 家庭电话,
                 tconsumers_nf_140927.office_phone 单位电话,
                 tconsumers_nf_140927.mobile_phone 手机号码,
                 tconsumers_nf_140927.email 电子邮件,
                 tconsumers_nf_140927.duty 职务,
                 decode( tconsumers_nf_140927.education,5,'博士研究生',4,'硕士研究生',3,'本科',2,'中学',1,'小学') as 文化程度,
                 tconsumers_nf_140927.liking as 客户爱好,
                 tconsumers_nf_140927.address as 详细地址,
                 tconsumers_nf_140927.row_id as 客户id
            from tmpdata.tconsumers_nf_140927
           inner join tmpdata.tpurchaseinfos140917
              on tpurchaseinfos140917.客户Row_id = tconsumers_nf_140927.row_id
           );
------------
insert into tmpdata.TCONSUMERS140918

  select distinct CUSTOMER_NAME 客户姓名,
                  性别,
                  case
                    when namelength <= 6 then
                     '个人客户'
                    else
                     '单位客户'
                  end as 客户类型,
                  年龄,
                  证件类型,
                  证件号码,
                  区域,
                  省份,
                  城市,
                  县,
                  邮政编码,
                  家庭电话,
                  单位电话,
                  手机号码,
                  电子邮件,
                  职务,
                  文化程度,
                  客户爱好,
                  详细地址,
                  客户id,'bf' as type 
    from (select tconsumers_bf_140927.customer_name as CUSTOMER_NAME,
                 decode(tconsumers_bf_140927.Sex, 0, '女', 1, '男', '未填') as 性别,
                 lengthb(tconsumers_bf_140927.customer_name) as namelength,
                 tconsumers_bf_140927.age as  年龄,
                 '居民身份证' as  证件类型,
                 tconsumers_bf_140927.ID_card as  证件号码,
                 ' ' as  区域,
                 tconsumers_bf_140927.prov_name as  省份,
                 tconsumers_bf_140927.city_name as  城市,
                  tconsumers_bf_140927.town_name as  县,
                 tconsumers_bf_140927.post 邮政编码,
                 tconsumers_bf_140927.home_phone 家庭电话,
                 tconsumers_bf_140927.office_phone 单位电话,
                 tconsumers_bf_140927.mobile_phone 手机号码,
                 tconsumers_bf_140927.email 电子邮件,
                 tconsumers_bf_140927.duty 职务,
                 decode( tconsumers_bf_140927.education,5,'博士研究生',4,'硕士研究生',3,'本科',2,'中学',1,'小学') as 文化程度,
                 tconsumers_bf_140927.liking as 客户爱好,
                 tconsumers_bf_140927.address as 详细地址,
                 tconsumers_bf_140927.row_id as 客户id
            from tmpdata.tconsumers_bf_140927
           inner join tmpdata.tpurchaseinfos140917
              on tpurchaseinfos140917.客户Row_id = tconsumers_bf_140927.row_id
           );
create index TCONSUMERS140705_ROWID on tmpdata.TCONSUMERS140705  (客户id);
select count (distinct tconsumers_nf_140927.row_id)from tmpdata.tconsumers_nf_140927   --362264
 inner join tmpdata.tpurchaseinfos140917
              on tpurchaseinfos140917.客户Row_id = tconsumers_nf_140927.row_id --317210
              
              select count(distinct tconsumers_bf_140927.row_id) from tmpdata.tconsumers_bf_140927   --558388
 inner join tmpdata.tpurchaseinfos140917
              on tpurchaseinfos140917.客户Row_id = tconsumers_bf_140927.row_id --535402
select count(*) from  tmpdata.TCONSUMERS140918 where type='nf' 
select count(*) from  tmpdata.TCONSUMERS140918 where type='bf' 



select * from  tmpdata.tPurchaseLinkers_bf_140927 
