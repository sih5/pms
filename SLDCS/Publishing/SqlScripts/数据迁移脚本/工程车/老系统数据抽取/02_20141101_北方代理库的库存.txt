--�������(110007)
create table tmpdata.tagentstores_141101_bf as
select (select m.code from npms.tmaterials m where m.objid = s.materialid) as materialcode,
       (select m.name from npms.tmaterials m where m.objid = s.materialid) as materialname,
       (select code from npms.tagents ag where ag.objid = s.entercode) as agentcode,
       (select name from npms.tagents ag where ag.objid = s.entercode) as agentname,
       (select w.code from npms.twarehouses w where w.objid = s.warehouseid) as warehousecode,
       (select w.name from npms.twarehouses w where w.objid = s.warehouseid) as warehousename,
       (select a.code
          from npms.tlocations l
         inner join npms.tareafields af
            on af.objid = l.areafieldid
         inner join npms.tareas a
            on a.objid = af.areaid
         where l.objid = s.locationid) as areacode,
       (select af.code
          from npms.tlocations l
         inner join npms.tareafields af
            on af.objid = l.areafieldid
         where l.objid = s.locationid) as tareafieldcode,
       (select l.code from npms.tlocations l where l.objid = s.locationid) as locationcode,
       s.accountqty,
       s.physicalqty,
       s.usableqty,
       s.batchcode,
       /*       s.*,*/
       'bf' as syscode
  from npms.tstores s
 where s.warehouseid in (select objid
                           from npms.twarehouses w
                          where w.isvalid = 1
                            and exists (select objid
                                   from npms.tagents ta
                                  where ta.objid = w.entercode
                                    and ta.isvalid = 1))
   and s.isvalid = 1
   and exists (select bf.objid_bf
          from tmpdata.spartpart141028 bf
         where /*bf.remark is null and*/
         bf.objid_bf is not null
      and s.materialid = bf.objid_bf);
