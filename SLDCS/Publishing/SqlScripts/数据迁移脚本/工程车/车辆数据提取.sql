select count(*) from  tmpdata.TPURCHASEINFOS140917 where product_whole_code is null
where not exists (select 1 from tmpdata.tstdproductin140917 t where  t.产品编号 = tpurchaseinfos_bf_140916.product_whole_code)
Create table tmpdata.TPURCHASEINFOS140917
as
select tpurchaseinfos_bf_140916.Bone_Code VIN码,
       tpurchaseinfos_bf_140916.Leave_Factory_Code 出厂编号,
       tpurchaseinfos_bf_140916.Auto_No 车牌号,
       tpurchaseinfos_bf_140916.Sales_Flag 是否售出,
       tpurchaseinfos_bf_140916.Product_Type_Name 销售车型,
       tpurchaseinfos_bf_140916.Product_Out_Type_Name 公告号,
       tpurchaseinfos_bf_140916.Engine_Model 发动机型号,
       tpurchaseinfos_bf_140916.Engine_Code 发动机序列号,
       tpurchaseinfos_bf_140916.Leave_Factory_Date 出厂日期,
       tpurchaseinfos_bf_140916.Sales_Date 销售日期,
       tpurchaseinfos_bf_140916.invoices 销售发票号,
       tpurchaseinfos_bf_140916.sales_price 销售价格,
       tpurchaseinfos_bf_140916.Product_Whole_Code 产品编号,
       tpurchaseinfos_bf_140916.Customer_Name 客户名称,
       tpurchaseinfos_bf_140916.Customer_ID 客户Row_id,
       ' ' as 车辆种类,
       ' ' as 车辆类型,
       ' ' 车系,
       tpurchaseinfos_bf_140916.Product_Line_Name 产品线名称,
       tpurchaseinfos_bf_140916.Cab_Model 驾驶室型号,
       ' ' as 前桥号,
       ' 'as  中桥号,
       ' 'as 后桥号,
       ' 'as 平衡轴号,
       ' 'as 变速箱序列号,
       tpurchaseinfos_bf_140916.color 颜色,
       tpurchaseinfos_bf_140916.Franchiser_Name, t.品牌代码 品牌 ,tpurchaseinfos_bf_140916.mile,t.车辆类型代码,t.车辆类型名称
  from tmpdata.tpurchaseinfos_bf_140916
 inner join  tmpdata.tstdproductin140917 t
    on t.产品编号 = tpurchaseinfos_bf_140916.product_whole_code
    
    insert into  tmpdata.TPURCHASEINFOS140917     
    select tpurchaseinfos_nf_140915.Bone_Code VIN码,
       tpurchaseinfos_nf_140915.Leave_Factory_Code 出厂编号,
       tpurchaseinfos_nf_140915.Auto_No 车牌号,
       tpurchaseinfos_nf_140915.Sales_Flag 是否售出,
       tpurchaseinfos_nf_140915.Product_Type_Name 销售车型,
       tpurchaseinfos_nf_140915.Product_Out_Type_Name 公告号,
       tpurchaseinfos_nf_140915.Engine_Model 发动机型号,
       tpurchaseinfos_nf_140915.Engine_Code 发动机序列号,
       tpurchaseinfos_nf_140915.Leave_Factory_Date 出厂日期,
       tpurchaseinfos_nf_140915.Sales_Date 销售日期,
       tpurchaseinfos_nf_140915.invoices 销售发票号,
       tpurchaseinfos_nf_140915.sales_price 销售价格,
       tpurchaseinfos_nf_140915.Product_Whole_Code 产品编号,
       tpurchaseinfos_nf_140915.Customer_Name 客户名称,
       tpurchaseinfos_nf_140915.Customer_ID 客户Row_id,
       ' ' as 车辆种类,
       ' ' as 车辆类型,
       ' ' 车系,
       tpurchaseinfos_nf_140915.Product_Line_Name 产品线名称,
       tpurchaseinfos_nf_140915.Cab_Model 驾驶室型号,
       ' ' as 前桥号,
       ' 'as  中桥号,
       ' 'as 后桥号,
       ' 'as 平衡轴号,
       ' 'as 变速箱序列号,
       tpurchaseinfos_nf_140915.color 颜色,
       tpurchaseinfos_nf_140915.Franchiser_Name, t.品牌代码 品牌 ,tpurchaseinfos_nf_140915.mile,t.车辆类型代码,t.车辆类型名称
  from tmpdata.tpurchaseinfos_nf_140915
 inner join  tmpdata.tstdproductin140917 t
    on t.产品编号 = tpurchaseinfos_nf_140915.product_whole_code
    583284 336753 920037
