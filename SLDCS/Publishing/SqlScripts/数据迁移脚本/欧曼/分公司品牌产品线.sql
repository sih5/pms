select tfiliales.code     as filialecode,
       tfiliales.name     as filialename,
       tbrands.code       as brandcode,
       tbrands.name       as brandname,
       tproductlines.code as productlinecode,
       tproductlines.name as prodcutlinename
  from ft.tfiliales
 inner join ft.tbrands
    on tbrands.objid = tfiliales.brandid
 inner join ft.tproductlines
    on tproductlines.brandid = tbrands.objid
 where tfiliales.isvalid > 0
   and tproductlines.isvalid > 0
