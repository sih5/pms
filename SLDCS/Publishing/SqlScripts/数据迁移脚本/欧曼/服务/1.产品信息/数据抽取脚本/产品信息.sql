select tstdproductin.ProductCode       as 产品编号,
       tstdproductin.ProductName       as 车型名称,
       tstdproductin.MainBrandCode     as 品牌代码,
       tstdproductin.MainBrandname     as 品牌名称,
       tstdproductin.SubBrandCode      as 子品牌代码,
       tstdproductin.SubBrandName      as 子品牌名称,
       tstdproductin.PlatFormCode      as 平台代码,
       tstdproductin.PlatFormName      as 平台名称,
       tstdproductin.PKind             as 产品线代码,
       tstdproductin.LineName          as 产品线名称,
       tstdproductin.InnerCode         as 原始内部编号,
       tstdproductin.BulletinCode      as 公告号,
       tstdproductin.Load              as 吨位代码,
       tstdproductin.EngineType        as 发动机型号代码,
       tstdproductin.EngineFactoryCode as 发动机生产厂家代码,
       tstdproductin.GearType          as 变速箱型号代码,
       tstdproductin.GearFactoryCode   as 变速箱生产厂家代码,
       tstdproductin.BrideType         as 后桥型号代码,
       tstdproductin.BrideFactoryCode  as 后桥生产厂家代码,
       tstdproductin.TyreType          as 轮胎型号代码,
       tstdproductin.TyreFactoryCode   as 轮胎形式代码,
       tstdproductin.brakeType         as 制动方式代码,
       tstdproductin.DriveType         as 驱动方式代码,
       tstdproductin.FrameType         as 车架连接形式代码,
       tstdproductin.FSize             as 整车尺寸,
       tstdproductin.wheelbase         as 轴距代码,
       tstdproductin.emissionType      as 排放标准代码,
       tstdproductin.PFunctionType     as 产品功能代码,
       tstdproductin.Pstand            as 产品档次代码,
       tstdproductin.Vtype             as 车辆类型代码,
       tstdproductin.Color             as 颜色描述,
       tstdproductin.SEATCOUNTER       as 载客个数,
       tstdproductin.WEIGHT            as 自重,
       tbrands.name                    as 服务品牌,
       tproductlines.name              as 服务产品线
  from ft.tstdproductin
 inner join ft.tbrandin
    on tbrandin.mainbrandid = tstdproductin.mainbrandid
 inner join ft.tbrands
    on tbrands.objid = tbrandin.ssbrandid
 inner join ft.tproductlines
    on tproductlines.objid = tbrandin.ssproductlineid
 where (tbrands.name = '商混设备' or tbrands.name = '欧曼')
   and tproductlines.isvalid > 0;
