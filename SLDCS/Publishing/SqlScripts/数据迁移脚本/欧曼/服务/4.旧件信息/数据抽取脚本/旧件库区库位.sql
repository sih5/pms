--分公司旧件仓库库区库位
select code,
       name,
       分公司编号,
       分公司名称,
       库区编号,
       库位编号,
       库区库位类型,
       库区用途
  from (select twarehouses.code,
               twarehouses.name,
               'SHSB' 分公司编号,
               '商混设备销售分公司' 分公司名称,
               tareas.code || tareafields.code as 库区编号,
               tlocations.code as 库位编号,
               '库位' 库区库位类型,
               '保管区' 库区用途
          from ft.twarehouses
         inner join ft.tareas
            on tareas.warehouseid = twarehouses.objid
         inner join ft.tareafields
            on tareafields.areaid = tareas.objid
         inner join ft.tlocations
            on tlocations.areafieldid = tareafields.objid
         where twarehouses.entercode =
               '{3CFD3185-C122-4383-A142-2A8AF6E1E03E}'
           and twarehouses.isvalid > 0
           and tareas.isvalid > 0
           and tareafields.isvalid > 0
           and tlocations.isvalid > 0
        union all
        select code,
               name,
               'SHSB' 分公司编号,
               '商混设备销售分公司' 分公司名称,
               code 库区编号,
               '' 库位编号,
               '仓库' 库区库位类型,
               '保管区' 库区用途
          from ft.twarehouses
         where twarehouses.entercode =
               '{3CFD3185-C122-4383-A142-2A8AF6E1E03E}'
           and twarehouses.isvalid > 0);
