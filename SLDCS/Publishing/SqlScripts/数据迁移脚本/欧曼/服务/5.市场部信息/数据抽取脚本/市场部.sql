select tmarkets.code    as marketcode,
       tmarkets.name    as marketname,
       tfiliales.code   as filialecode,
       tfiliales.name   as filialename,
       tmarkets.tel,
       tmarkets.address
  from ft.tmarkets
 inner join ft.tfiliales
    on tfiliales.objid = tmarkets.filialeid
 where tfiliales.isvalid > 0
   and tmarkets.isvalid > 0
   and tfiliales.brandid =
       (select objid from ft.tbrands where tbrands.name = 'ŷ��')
