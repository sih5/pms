create table tenterprises_temp_om
as
select tstations.objid, tstations.code, tstations.name
  from ft.tstations@fservice
 inner join ft.tstationlists@fservice
    on tstationlists.stationid = tstations.objid
 where tstations.isvalid > 0
   and tstationlists.isvalid > 0
   and tstationlists.brandid =
       (select objid
          from ft.tbrands@fservice
         where tbrands.name = '欧曼')
union all
select objid, code, name
  from ft.tagents@fspare t1
 where t1.isvalid > 0
   and t1.brandid =
       (select objid from ft.tbrands@fspare where tbrands.name = '欧曼')
union all

select objid, code, name
  from ft.tfiliales@fspare t1
 where isvalid > 0
   and t1.brandid =
       (select objid from ft.tbrands@fspare where tbrands.name = '欧曼')
union all
select objid, code, name
  from ft."tEnterprise"
 where (COMPANYPROPERTY = 5 or COMPANYPROPERTY = 6 /*or COMPANYPROPERTY = 4 营销时代的代理库未分品牌*/)
   and isvalid > 0;
   
create index TENTERPRISES_TEMP_OM_objid on TENTERPRISES_TEMP_OM (objid);   
