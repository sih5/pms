select temp.objid as enterpriseid,
       temp.code as enterprisecode,
       temp.name as enterprisename,
       (select code
          from ft."tOrganize" uorg
         where uorg.objid = org.parentid
           and uorg.type = '部门') as uporgcode,
       (select name
          from ft."tOrganize" uorg
         where uorg.objid = org.parentid
           and uorg.type = '部门') as uporgname,
       org.code as orgcode,
       org.name as orgname,
       org.oindex
  from ft."tOrganize" org
 inner join ft.tenterprises_temp_om temp
    on temp.objid = org.entercode
 where org.type = '部门'
   and (org.olevel = 2 or org.olevel = 3)
   and org.isvalid > 0;
