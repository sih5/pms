--新增企业默认组织
insert into security.Organization
  (Id,
   ParentId,
   Code,
   Name,
   Type,
   Sequence,
   Status,
   EnterpriseId,
   CreatorName,
   CreateTime)
  select security.s_Organization.nextval as Id,
         -1 as ParentId,
         Code,
         substrb(name, 1, 100) as Name,
         0 as Type,
         0 as Sequence,
         2 as Status,
         id as EnterpriseId,
         'admin' as CreatorName,
         sysdate as CreateTime
    from security.enterprise
   where not exists (select *
            from security.Organization
           where Organization.Enterpriseid = enterprise.id
             and Organization.Code = enterprise.code
             and Organization.Name = enterprise.name);

--第一层组织             
insert into security.Organization
  (Id,
   ParentId,
   Code,
   Name,
   Type,
   Sequence,
   Status,
   EnterpriseId,
   CreatorName,
   CreateTime)
  select security.s_organization.nextval as Id,
         (select id
            from security.Organization
           where Organization.code = temp.enterprisecode
             /*and Organization.name = temp.enterprisename*/) as ParentId,
         temp.orgcode as Code,
         temp.orgname as Name,
         1 as Type,
         0 as Sequence,
         2 as Status,
         enterprise.id as EnterpriseId,
         'admin' as CreatorName,
         sysdate as CreateTime
    from torganize_temp_om temp
   inner join security.enterprise
      on enterprise.code = temp.enterprisecode
   where temp.uporgname is null;

--第二层组织         
insert into security.Organization
  (Id,
   ParentId,
   Code,
   Name,
   Type,
   Sequence,
   Status,
   EnterpriseId,
   CreatorName,
   CreateTime)
  select security.s_organization.nextval as Id,
         (select id
            from security.Organization
           where Organization.code = temp.uporgcode
             and Organization.name = temp.uporgname
             and Organization.Enterpriseid=enterprise.id 
             and rownum=1) as ParentId,
         temp.orgcode as Code,
         temp.orgname as Name,
         1 as Type,
         0 as Sequence,
         2 as Status,
         enterprise.id as EnterpriseId,
         'admin' as CreatorName,
         sysdate as CreateTime
    from torganize_temp_om temp
   inner join security.enterprise
      on enterprise.code = temp.enterprisecode
   where temp.uporgname is not null;   
   
 
