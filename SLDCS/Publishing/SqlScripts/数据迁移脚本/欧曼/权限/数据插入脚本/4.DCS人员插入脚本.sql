--人员信息
insert into security.personnel
  (id,
   loginid,
   name,
   status,
   cellnumber,
   enterpriseid,
   password,
   creatorname,
   createtime,
   passwordmodifytime)
  select security.s_personnel.nextval as id,
         temp.personcode as loginid,
         nvl(temp.personname,temp.personcode) as name,
         1 as status,
         temp.linktel as cellnumber,
         enterprise.id as enterpriseid,
         '7C4A8D09CA3762AF61E59520943DC26494F8941B' as password,
         'admin' as creatorname,
         sysdate as createtime,
         sysdate as passwordmodifytime
    from tpersons_temp_om temp
   inner join security.enterprise
      on enterprise.code = temp.enterprisecode;

--人员与组织的关系
insert into OrganizationPersonnel
  (id, organizationid, personnelid)
  select security.s_Organizationpersonnel.nextval as id,
         organization.id                          as organizationid,
         Personnel.Id                             as personnelid
    from tpersons_temp_om temp
   inner join security.enterprise
      on enterprise.code = temp.enterprisecode
   inner join organization
      on organization.enterpriseid = enterprise.id
     and organization.code = temp.orgcode
     and organization.name = temp.orgname
   inner join Personnel
      on Personnel.Corporationid = enterprise.id
     and Personnel.Loginid = temp.personcode
     and Personnel.Name = temp.personname;
