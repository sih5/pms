--批量生效企业
update security.enterprise set status=3  where enterprise.status=1;

update security.enterprise set entnodetemplateid=1 where entnodetemplateid is null;

--批量默认授权
insert into security.rule
  (id, roleid, nodeid)
  select security.s_rule.nextval as id, M.roleid, MM.nodeid
    from (select role.id as roleid
            from security.enterprise
           inner join security.role
              on role.enterpriseid = enterprise.id
           where role.name = '企业管理员'
             and not exists
           (select * from security.rule where rule.roleid = role.id)) M,
         (select nodeid from security.rule where roleid = 1) MM

 