
select *
  from (select org.olevel,
               (select (select ((select code
                                   from ft."tOrganize" uorgss
                                  where uorgss.objid = uorgs.parentid
                                    and uorgss.type = '部门')) code
                          from ft."tOrganize" uorgs
                         where uorgs.objid = uorg.parentid
                           and uorgs.type = '部门') code
                  from ft."tOrganize" uorg
                 where uorg.objid = org.parentid
                   and uorg.type = '部门') as uuuporgcode,
               
               (select (select code
                          from ft."tOrganize" uorgs
                         where uorgs.objid = uorg.parentid
                           and uorgs.type = '部门') code
                  from ft."tOrganize" uorg
                 where uorg.objid = org.parentid
                   and uorg.type = '部门') as uuporgcode,
               (select (select name
                          from ft."tOrganize" uorgs
                         where uorgs.objid = uorg.parentid
                           and uorgs.type = '部门') name
                  from ft."tOrganize" uorg
                 where uorg.objid = org.parentid
                   and uorg.type = '部门') as uuporgname,
               
               (select code
                  from ft."tOrganize" uorg
                 where uorg.objid = org.parentid
                   and uorg.type = '部门') as uporgcode,
               (select name
                  from ft."tOrganize" uorg
                 where uorg.objid = org.parentid
                   and uorg.type = '部门') as uporgname,
               org.code as orgcode,
               org.name as orgname
          from ft."tOrganize" org
         where org.type = '部门') M
 where M.orgcode is not null
