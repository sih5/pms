--分公司仓库人员信息
select * from (select twarehouses.code,
       twarehouses.name,
       '' 仓储企业编号,
       '' 仓储企业名称,
       '分公司' 仓储企业类型,
       (select code
          from ft."tPerson"@fpurview p
         where p.objid = links.operatorid) as 仓库人员编号,
       (select name
          from ft."tPerson"@fpurview p
         where p.objid = links.operatorid) as 仓库人员名称
  from ft.twarehouses
 inner join ft.twarehouseoperator_Links links
    on links.warehouseid = twarehouses.objid
 where twarehouses.brandid =
       (select objid from ft.tbrands where name = '欧曼')
       and twarehouses.isvalid>0) where 仓库人员编号 is not null;
