--����վ������

select tstations.code as ����վ���,
       tstations.name as ����վ����,
       'ŷ��' as Ʒ��,
       (select code
          from ft.tmaterials
         where tmaterials.objid = tstores.materialid) as ������,
       (select name
          from ft.tmaterials
         where tmaterials.objid = tstores.materialid) as �������,
       sum(tstores.usableqty) as ����
  from ft.tstores
 inner join ft.tmaterials t1
    on t1.objid = tstores.materialid
 inner join ft.twarehouses
    on twarehouses.objid = tstores.warehouseid
 inner join ft.tstations
    on tstations.objid = twarehouses.entercode
 inner join ft.tstationlists
    on tstationlists.stationid = tstations.objid
 where twarehouses.isvalid > 0  
   and tstations.isvalid > 0
   and tstores.isvalid > 0
   and tstationlists.isvalid > 0
   and tstationlists.brandid =
       (select objid from ft.tbrands where tbrands.name = 'ŷ��')
   and t1.brandid =
       (select objid from ft.tbrands where tbrands.name = 'ŷ��')
   and t1.isvalid > 0
 group by tstores.entercode,
          tstores.materialid,
          tstations.code,
          tstations.name;
