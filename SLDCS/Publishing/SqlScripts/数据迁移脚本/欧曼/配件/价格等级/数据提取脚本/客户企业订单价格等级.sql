
select distinct tstations.code customercode,
                tstations.name customername,
                t1.saletypecode,
                (select tt.paramvalue
                   from ft.tparams@fspare tt
                  where tt.paramtype = '销售订单类型'
                    and t1.saletypecode = tt.paramcode) saletypename,
                (select name
                   from ft.tbrands
                  where tbrands.objid = tstationlists.brandid) as brandname,
                (select rate
                   from ft.TPRICEGRADES@fspare t2
                  where t1.pricegradeid = t2.objid) crate
  from ft.TCUSTOMERPRICES@fspare t1
 inner join ft.tstations
    on tstations.objid = t1.customterid
 inner join ft.tstationlists
    on tstationlists.stationid = tstations.objid
 where tstations.isvalid > 0
   and tstationlists.isvalid > 0
   and t1.isvalid > 0
   and tstationlists.brandid =
       (select objid from ft.tbrands where tbrands.name = '欧曼')
