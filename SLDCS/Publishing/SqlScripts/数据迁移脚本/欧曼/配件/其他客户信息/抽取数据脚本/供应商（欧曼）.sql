select code,
       name,
       decode(IsFormal, 1, '正式供应商', 2, '临时供应商',0, '临时供应商','临时供应商') as 供应商类型,
       province,
       city,
       '' county, --县
       zip,
       E_Mail,
       Fax,
       Linker,
       Tel,
       Address,
       (select ADDRESS
          from ft."tEnterprise"@fpurview t2
         where t1.objid = t2.objid) QYaddress,
       (select BUSINESS
          from ft."tEnterprise"@fpurview t2
         where t1.objid = t2.objid) BUSINESS,
       (select establishdate
          from ft."tEnterprise"@fpurview t2
         where t1.objid = t2.objid) establishdate,
         'OMYXKHB' as 隶属分公司编号,
         '欧曼营销服务部' as 隶属分公司名称,
         '欧曼' as 品牌
  from ft.tsuppliers t1
 where isvalid > 0
