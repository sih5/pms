--插入配件供应商基本信息

insert into dcs.Partssupplier
  (Id, code, Name, SupplierType, Status, CreatorName, CreateTime)
  select (select id
            from dcs.company
           where type = 6
             and company.code = tSupplierTemp.Suppliercode),
         SupplierCode,
         SupplierName,
         decode(tSupplierTemp.IsFormalSup, '正式供应商', 1, '临时供应商', 2),
         1,
         'admin',
         sysdate
    from tSupplierTemp
where not exists (select 1 from dcs.Partssupplier a where a.code = tSupplierTemp.SupplierCode)
