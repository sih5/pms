--插入供应商到企业信息，需要校验数据中供应商名称、编号 不能重复

insert into dcs.company
  (Id,
   Type,
   Code,
   Name,
   ProvinceName,
   CityName,
   CountyName,
   ContactPerson,
   ContactPhone,
   --ContactMobile,
   Fax,
   ContactAddress,
   ContactPostCode,
   ContactMail,
   
   RegisterDate,
   BusinessScope,
   RegisteredAddress,
   Status,
   CreatorName,
   CreateTime)
  select dcs.s_company.nextval,
         6, --企业类型-供应商
         SupplierCode,
         SupplierName,
         Province,
         city,
         county,
         Linker,
         tel,
         Fax,
         Address,
         zip,
         E_Mail,
         establishdate,
         Business,
         QYaddress,
         1,
         'admin',
         sysdate
    from tSupplierTemp
where not exists (select 1 from dcs.company a where a.code = tSupplierTemp.SupplierCode
or a.name = tSupplierTemp.SupplierName)
