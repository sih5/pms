select tstations.code,
       tstations.name,
       tstations.FACTORYTIME, --成立时间
       tstations.ProvinceName, --省
       (Select REGION.CityName
          From ft.THZONE REGION
         Where TSTATIONS.RegionID = REGION.NumCode) As RegionName, --市
       (Select THZONE.TOWNNAME
          From ft.THZONE
         Where TSTATIONS.County = THZONE.NumCode) as CountyName, --县(区)
       tstations.EMAIL,
       tstations.POSTCODE,
       tstations.MASTERCODE, --联系人
       tstations.PHONE, --联系电话
       tstations.ADDRESS, --联系地址
       tstations.Fax, --传真
       tstations.REGISTERCODE, --注册证号
       tstations.name, --注册名称
       (select establishdate
          from ft."tEnterprise"@fpurview t2
         where tstations.objid = t2.objid) establishdate, --注册日期
       tstations.REGISTERFUND, --注册资金
       tstations.ENTERKIND, --企业性质
       tstations.CORPORATION, --法人代表
       PRIBUSINESS, --经营范围
       -- SECBUSINESS,--兼营范围,
       (select ADDRESS
          from ft."tEnterprise"@fpurview t2
         where tstations.objid = t2.objid) QYaddress, --注册地址
       tstations.REPQUA, --维修资质
       tstations.License, --危险品运输车车辆维修资质
       tstations.AccreditBrand, --服务品牌
       tstations.PARKAREA, --停车场面积   
       tstations.REPPLANTAREA, --维修车间面积
       tstations.ANTEROOMAREA, --接待室面积
       tstations.WAREHOUSEAREA --配件库面积                          
  from ft.tstations
 inner join ft.tstationlists
    on tstationlists.stationid = tstations.objid
 where tstations.isvalid > 0
   and tstationlists.isvalid > 0
   and tstationlists.brandid =
       (select objid from ft.tbrands where tbrands.name = '欧曼');
