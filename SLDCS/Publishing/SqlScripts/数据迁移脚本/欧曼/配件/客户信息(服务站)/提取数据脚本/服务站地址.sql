select tstations.code,
       tstations.name,
       tstations.ProvinceName, --省
       (Select REGION.CityName
          From ft.THZONE REGION
         Where TSTATIONS.RegionID = REGION.NumCode) As RegionName, --市
       (Select THZONE.TOWNNAME
          From ft.THZONE
         Where TSTATIONS.County = THZONE.NumCode) as CountyName, --县(区)
       tstationlists.Sparelinkerman, --联系人
       tstationlists.Sparelinkertel, --联系电话
       tstationlists.Sparelinkeraddress, --详细地址
       '' as 地址用途
  from ft.tstations
 inner join ft.tstationlists
    on tstationlists.stationid = tstations.objid
 where tstations.isvalid > 0
   and tstationlists.isvalid > 0
   and tstationlists.brandid =
       (select objid from ft.tbrands where tbrands.name = '欧曼');
