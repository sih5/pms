
--3新增经销商扩展信息
insert into DealerServiceExt
(
    Id, --Id
    RepairQualification, --	维修资质
    HasBranch, --	有二级服务站
    DangerousRepairQualification, --	危险品运输车辆维修资质
    BrandScope, --	服务品牌
    --CompetitiveBrandScope, --	服务竞争品牌
    ParkingArea, --	停车场面积
    RepairingArea, --	维修车间面积
    --EmployeeNumber, --	员工人数
    PartWarehouseArea, --	配件库面积
    --Manager, --	服务经理
    ManagerPhoneNumber, --	固定电话
    ManagerMobile, --	移动电话
    ManagerMail, --	电子邮箱
    ReceptionRoomArea, --	接待室面积
    --Remark, --	备注
    Status, --	状态
    CreateTime, --	创建时间
    CreatorId, --	创建人Id
    CreatorName, --	创建人
    --ModifierId	修改人Id
    --ModifyTime	修改时间
    --ModifierName	修改人

      SubjectUnit, --隶属单位
      PRIBUSINESS,   --主营范围    
      SECBUSINESS,    --兼营范围
      FactoryTime,    --建厂时间
      TRAFFICLIMIT, --交通限行描述
      FTMRESERVE    --配件贮备金额

)
(
   select 
     (select id from Enterprise t2 where t1.code=t2.code) ID,
     decode(REPQUA, '一级',1,'一类', 1, '二级',2,'二类', 2,'三类',3) RepairQualification,
     0,
     license,
     accreditbrand,
     parkarea,
     repplantarea,
     warehousearea,
     --mastercode,
     fax,
     phone,
     email,
     anteroomarea,
     1,
     sysdate,
     1,
     '系统管理员',

      OwnerCompany,        --隶属单位
      MainBusinessAreas,   --主营范围    
      AndBusinessAreas,    --兼营范围
      BuildTime,           --建厂时间
      TrafficRestrictionsdescribe, --交通限行描述
      PartReserveAmount    --配件贮备金额
     from tstationstemp t1
where not exists (select * from Company t2 where t1.code=t2.code)
)








--select * from DealerServiceExt

--select * from tstationstemp