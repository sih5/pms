--供应商账户
select tsuppliers.code,
       tsuppliers.name,
       'OMYXKHB' as 购货方企业编号,
       '欧曼营销服务部' as 购货方企业名称,
       (select name
          from ft.tbrands
         where tbrands.objid = tcustaccounts.brandid) as brandname,
       tcustaccounts.balance
  from ft.tcustaccounts
 inner join ft.tsuppliers
    on tsuppliers.objid = tcustaccounts.customerid
 where tsuppliers.isvalid > 0
   and tcustaccounts.isvalid > 0
   and tcustaccounts.brandid =
       (select objid from ft.tbrands where tbrands.name = '欧曼');
