--雷萨欧曼
select tSWapMaterials.Code,
       tSWapMaterials.Name,
       tmaterials.Code partcode,
       tmaterials.Name partname
  from ft.tSWapMaterials
 inner join ft.tmaterials
    on tmaterials.swapcode = tSWapMaterials.Code
 where tSWapMaterials.isvalid > 0
   and tmaterials.brandid =
       (select objid from ft.tbrands where name = '欧曼');

--营销时代
select tSWapMaterials.Code,
       tSWapMaterials.Name,
       tmaterials.Code partcode,
       tmaterials.Name partname
  from ft.tSWapMaterials
 inner join ft.tmaterials
    on tmaterials.swapcode = tSWapMaterials.Code
 inner join ft.tmaterialbrand
    on tmaterialbrand.materialid = tmaterials.objid
 where tSWapMaterials.isvalid > 0
   and tmaterialbrand.brandid =
       (select objid from ft.tbrands where name = '时代');
