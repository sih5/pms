--雷萨欧曼
select code, --配件图号
       name, --配件名称
       Ename, --英文名称
       (select code
          from ft.tcadmaterials
         where t1.cadid = tcadmaterials.objid) cadcode, --设计图号
       (select name
          from ft.tcadmaterials
         where t1.cadid = tcadmaterials.objid) cadname, --设计名称
       '' as parttype,--配件类型
       Spec, --规格型号
       UnitName, --计量单位
       memosp, --配件特征说明
       '' as packingamount,--包装数量,
       '' as packingspecification,--包装规格,
       upparts, --上一替代件
       nextparts, --下一替代件
       Shelflife --保质期
  from ft.tmaterials t1
 where t1.brandid =
       (select objid from ft.tbrands where tbrands.name = '欧曼');

--营销时代

select t1.code, --配件图号
       t1.name, --配件名称
       t1.Ename, --英文名称
       (select code
          from ft.tcadmaterials
         where t1.cadid = tcadmaterials.objid) cadcode, --设计图号
       (select name
          from ft.tcadmaterials
         where t1.cadid = tcadmaterials.objid) cadname, --设计名称
       '' as 配件类型,
       t1.Spec, --规格型号
       t1.UnitName, --计量单位
       t1.memosp, --配件特征说明
       '' as 包装数量,
       '' as 包装规格,
       t1.upparts, --上一替代件
       t1.nextparts, --下一替代件
       t1.Shelflife --保质期
  from ft.tmaterials t1
 inner join ft.tmaterialbrand
    on tmaterialbrand.materialid = t1.objid
 where tmaterialbrand.brandid =
       (select objid from ft.tbrands where tbrands.name = '时代');
