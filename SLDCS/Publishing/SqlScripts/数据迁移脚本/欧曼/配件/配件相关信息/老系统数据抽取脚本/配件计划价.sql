--雷萨欧曼
select tmaterials.code,
       tmaterials.name,
       tmaterials.brandname,
       tmaterialprices.stdprice
  from ft.tmaterials
 inner join ft.tmaterialprices
    on tmaterialprices.materialid = tmaterials.objid
 where tmaterials.brandid =
       (select objid from ft.tbrands where name = '欧曼');

--营销时代
select tmaterials.code,
       tmaterials.name,
       (select name
          from ft.tbrands
         where tbrands.objid = tmaterialbrand.brandid) as brandname,
       tmaterialprices.stdprice
  from ft.tmaterials
 inner join ft.tmaterialbrand
    on tmaterialbrand.materialid = tmaterials.objid
 inner join ft.tmaterialprices
    on tmaterialprices.materialid = tmaterials.objid
 where tmaterialbrand.brandid =
       (select objid from ft.tbrands where name = '时代');
