
create table tmpdata.spartpart150116 (partcode varchar2 (50),partname varchar2 (100),PartType varchar2(100),MInPackingAmount number(9) ,EnglishName varchar2(100), ReferenceCode varchar2(50)
,Specification varchar2(100),Feature varchar2(200),MeasureUnit varchar2(20),LastSubstitute varchar2(50),NextSubstitute varchar2(50),ShelfLife number(9))

--38783
create table tmpdata.spartpart150116_1 as select * from sparepart where rownum<1;
insert into tmpdata.spartpart150116_1
  (id,
   code,
   name,
   lastsubstitute,
   nextsubstitute,
   shelflife,
   englishname,
   referencecode,
   parttype,
   specification,
   feature,
   status,
   measureunit,
   creatorid,
   creatorname,
   createtime,
   minpackingamount)
  select s_sparepart.nextval,
         t.partcode,
         t.partname,
         t.lastsubstitute,
         t.nextsubstitute,
         t.shelflife,
         t.englishname,
         t.referencecode,
         decode(t.parttype, '总成件', 1, '配件', 2, '精品件', 3, '辅料', 4),
         t.specification,
         t.feature,
         1,
         nvl(t.measureunit,'件'),
         1,
         'Admin',
         sysdate,
         t.minpackingamount
from tmpdata.spartpart150116 t  where not exists(select 1 from sparepart where code=t.partcode );
insert into sparepart select * from tmpdata.spartpart150116_1;
-------------------------------正式上执行----------------------------------------------
/*
create table tmpdata.spartpart150116_1 as select * from sparepart where rownum<1;
--30668
insert into tmpdata.spartpart150116_1
(id,
   code,
   name,
   lastsubstitute,
   nextsubstitute,
   shelflife,
   englishname,
   referencecode,
   parttype,
   specification,
   feature,
   status,
   measureunit,
   creatorid,
   creatorname,
   createtime,
   minpackingamount) select s_sparepart.nextval,
   code,
   name,
   lastsubstitute,
   nextsubstitute,
   shelflife,
   englishname,
   referencecode,
   parttype,
   specification,
   feature,
   status,
   measureunit,
   creatorid,
   creatorname,
   createtime,
   minpackingamount from tmpdata.om_spartpart150116_1;
insert into sparepart select * from tmpdata.spartpart150116_1;

create table tmpdata.spareparthistory150116 as select * from spareparthistory where rownum<1;
insert into tmpdata.spareparthistory150116
  ("ID", --0
   "SPAREPARTID", --1
   "CODE", --2
   "NAME", --3
   "LASTSUBSTITUTE", --4
   "NEXTSUBSTITUTE", --5
   "SHELFLIFE", --6
   "ENGLISHNAME", --7
   "PINYINCODE", --8
   "REFERENCECODE", --9
   "REFERENCENAME", --10
   "CADCODE", --11
   "CADNAME", --12
   "PARTTYPE", --13
   "SPECIFICATION", --14,
   "FEATURE", --15
   "STATUS", --16
   "LENGTH", --17
   "WIDTH", --18
   "HEIGHT", --19
   "VOLUME", --20
   "WEIGHT", --21
   "MATERIAL", --22
   "PACKINGAMOUNT", --23
   "PACKINGSPECIFICATION", --24
   "PARTSOUTPACKINGCODE", --25
   "PARTSINPACKINGCODE", --26
   "MEASUREUNIT", --27
   "CREATORID", --28
   "CREATORNAME", --29
   "CREATETIME")
  select s_spareparthistory.nextval,
         "ID", --1
         "CODE", --2
         "NAME", --3
         "LASTSUBSTITUTE", --4
         "NEXTSUBSTITUTE", --5
         "SHELFLIFE", --6
         "ENGLISHNAME", --7
         "PINYINCODE", --8
         "REFERENCECODE", --9
         "REFERENCENAME", --10
         "CADCODE", --11
         "CADNAME", --12
         "PARTTYPE", --13
         "SPECIFICATION", --14,
         "FEATURE", --15
         "STATUS", --16
         "LENGTH", --17
         "WIDTH", --18
         "HEIGHT", --19
         "VOLUME", --20
         "WEIGHT", --21
         "MATERIAL", --22
         "PACKINGAMOUNT", --23
         "PACKINGSPECIFICATION", --24
         "PARTSOUTPACKINGCODE", --25
         "PARTSINPACKINGCODE", --26
         "MEASUREUNIT", --27
         "CREATORID", --28
         "CREATORNAME", --29
         "CREATETIME"
    from tmpdata.spartpart150116_1;
    insert into spareparthistory
     select * from tmpdata.spareparthistory150116 ;
-------------------------------正式上执行----------------------------------------------end

*/
