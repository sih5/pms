 create table tmpdata.om_tstoresdlk150121 as --动态
select t.locationcode,
       t.areacode,
       t.partcode,
       t.partname,
       t.code warehousecode,
       t.name warehousename,
       t.qty,
       cast('保内' as varchar2(10)) brand,
       cast(null as varchar2(100)) newwarehousecode,
       cast(null as varchar2(100)) newpartcode,
       cast(null as varchar2(10)) newagentcode,
       t.storagecompanycode,
       t.storagecompanyname,cast(null as number(19, 4)) oldsaleprice,
       cast(null as number(19, 4)) newsaleprice
  from tmpdata.tstores_ombnfw t;
 
    create index TMPDATA.om_tstoresdlk150121IDX on TMPDATA.om_tstoresdlk150121  (BRAND, PARTCODE);
update tmpdata.om_tstoresdlk150121 v set v.warehousecode='2' where v.warehousecode='002';

update tmpdata.om_tstoresdlk150121 v set v.warehousecode='3' where v.warehousecode='003';

update tmpdata.om_tstoresdlk150121 v set v.warehousecode='1' where v.warehousecode='01';

update tmpdata.om_tstoresdlk150121 v set v.warehousecode='4' where v.warehousecode='004';
insert into tmpdata.om_tstoresdlk150121
  select t.locationcode,
         t.areacode,
         t.partcode,
         t.partname,
         t.code               warehousecode,
         t.name               warehousename,
         t.qty,
         '保外',
         null,
         null,
         null,
         t.storagecompanycode,
         t.storagecompanyname,
         null,
         null
    from tmpdata.tstores_ombwfw t;


select distinct b.* from partsstock a 
inner join agency b on a.storagecompanyid = b.id
where a.createtime>to_date('2015-1-26','yyyy-mm-dd')
and storagecompanytype <>1

select * from  tmpdata.om_tstoresdlk150121 t where newagentcode is not null
update tmpdata.om_tstoresdlk150121 t
   set newwarehousecode =
       (select newcode
          from tmpdata.om_dlkoldnewwarehouserel150120
         where t.warehousecode = om_dlkoldnewwarehouserel150120.oldcode
           and om_dlkoldnewwarehouserel150120.brand = t.brand and t.storagecompanycode= om_dlkoldnewwarehouserel150120.oldagentcode),newagentcode =
       (select newagentcode
          from tmpdata.om_dlkoldnewwarehouserel150120
         where t.warehousecode = om_dlkoldnewwarehouserel150120.oldcode
           and om_dlkoldnewwarehouserel150120.brand = t.brand and t.storagecompanycode= om_dlkoldnewwarehouserel150120.oldagentcode);
           
           select count(*) from tmpdata.om_tstoresdlk150121  where newwarehousecode is not null and newagentcode is not null;

           select count(*) from tmpdata.om_tstoresdlk150121  where newwarehousecode is not null or newagentcode is not null;
           
 update tmpdata.om_tstoresdlk150121  set newpartcode=null;
 update tmpdata.om_tstoresdlk150121
   set newpartcode =
       (select newcode
           from tmpdata.om_newoldpartrel150121
          where om_newoldpartrel150121.oldcode = om_tstoresdlk150121.partcode
            and om_tstoresdlk150121.brand = om_newoldpartrel150121.brand);
update tmpdata.om_tstoresdlk150121 set newpartcode=partcode
where exists(select 1 from sparepart where sparepart.code=partcode) and newpartcode is null;


update tmpdata.om_tstoresdlk150121 t
   set oldsaleprice =
       (select distinct v.saleprice
          from tmpdata.om_purchasesaleprice150121 v
         where v.code = t.partcode
           and v.brandname = t.brand),
       newsaleprice =
       (select distinct v.saleprice
          from tmpdata.om_purchasesaleprice150121 v
         where v.code = t.newpartcode
           and v.brandname = t.brand);
  select count(*) from  tmpdata.om_tstoresdlk150121  where newpartcode is not null;
             
          
create table tmpdata.partsstockdlk150121 as select * from partsstock where rownum<1;
 
insert into tmpdata.partsstockdlk150121
  (id,
   warehouseid,
   storagecompanyid,
   storagecompanytype,
   branchid,
   warehouseareaid,
   warehouseareacategoryid,
   partid,
   quantity,
   creatorid,
   creatorname,
   createtime)
 select s_partsstock.nextval,
        warehouse.id,
        (select id from company where code=t.newagentcode),
       (select company.type from company where code=t.newagentcode)  ,
        (select id from company where code='2450'),
        (select warehousearea.id
           from warehousearea        
         /* inner join warehousearea parent
             on parent.id = warehousearea.parentid*/
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                --  and parent.code = t.areacode
                  and warehousearea.areakind = 3),
        89,
        sparepart.id,       
        t.qty,
        1,
        'Admin',
        sysdate
   from tmpdata.om_tstoresdlk150121  t
   inner join warehouse on warehouse.code=t.newwarehousecode and warehouse.branchid=2203
   inner join sparepart on sparepart.code = t.newpartcode
  where exists (select warehousearea.id
           from warehousearea        
        /*  inner join warehousearea parent
             on parent.id = warehousearea.parentid*/
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                 -- and parent.code = t.areacode
                  and warehousearea.areakind = 3) and t.newagentcode is not null
                  and exists(select id from company where code=t.newagentcode) and t.brand='保外';
         /*select *
         from tmpdata.om_tstoresdealer150121  t
   inner join warehouse on warehouse.code=t.newwarehousecode and warehouse.branchid=2203
   inner join sparepart on sparepart.code = t.newpartcode
  where exists (select warehousearea.id
           from warehousearea        
        \*  inner join warehousearea parent
             on parent.id = warehousearea.parentid*\
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                 -- and parent.code = t.areacode
                  and warehousearea.areakind = 3) and t.newagentcode is not null
                  and exists(select id from company where code=t.newagentcode)  and  t.newagentcode='FT00067'*/
        

insert into tmpdata.partsstockdlk150121
  (id,
   warehouseid,
   storagecompanyid,
   storagecompanytype,
   branchid,
   warehouseareaid,
   warehouseareacategoryid,
   partid,
   quantity,
   creatorid,
   creatorname,
   createtime)
 select s_partsstock.nextval,
        warehouse.id,
        (select id from company where code=t.newagentcode),
       (select company.type from company where code=t.newagentcode)  ,
        (select id from company where code='2450'),
        (select warehousearea.id
           from warehousearea        
         /* inner join warehousearea parent
             on parent.id = warehousearea.parentid*/
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                --  and parent.code = t.areacode
                  and warehousearea.areakind = 3),
        89,
        sparepart.id,       
        t.qty,
        1,
        'Admin',
        sysdate
   from tmpdata.om_tstoresdlk150121  t
   inner join warehouse on warehouse.code=t.newwarehousecode and warehouse.branchid=2203
   inner join sparepart on sparepart.code = t.newpartcode
  where exists (select warehousearea.id
           from warehousearea        
        /*  inner join warehousearea parent
             on parent.id = warehousearea.parentid*/
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                 -- and parent.code = t.areacode
                  and warehousearea.areakind = 3) and t.newagentcode is not null
                  and exists(select id from company where code=t.newagentcode) and t.brand='保内' and nvl(oldsaleprice,0)<=nvl(newsaleprice,0);
                                  
         
create index tmpdata.partsstockdlk150121_idx on TMPDATA.partsstockdlk150121 (warehouseid, warehouseareaid, partid);         
    update tmpdata.partsstockdlk150121 v
   set v.quantity =
       (select sum(quantity)
          from tmpdata.partsstockdlk150121 a
         where a.warehouseid = v.warehouseid
           and a.warehouseareaid = v.warehouseareaid
           and a.partid = v.partid)
 where exists (select *
          from (select t.warehouseid, t.warehouseareaid, t.partid
                  from tmpdata.partsstockdlk150121 t
                  
                 group by t.warehouseid, t.warehouseareaid, t.partid
                having count(partid) > 1) t
         where t.warehouseid = v.warehouseid
           and t.partid = v.partid
           and t.warehouseareaid = v.warehouseareaid);
    
delete tmpdata.partsstockdlk150121 
 where partsstockdlk150121.rowid not in
       (select min(partsstockdlk150121.rowid)
          from tmpdata.partsstockdlk150121 
         group by warehouseid, warehouseareaid, partid)  ;
         --415386
               insert into partsstock select * from tmpdata.partsstockdlk150121;
               delete from partsstock where exists( select * from tmpdata.partsstockdlk150121 t where t.id=partsstock.id)
                    select * from partsstock where storagecompanyid=
                    (select id from company where code=    'FT006232')
delete from partsstock where exists(select * from tmpdata.partsstockdlk150121 where partsstockdlk150121.id=partsstock.id);
DELETE FROM partsstock T WHERE T.BRANCHID =2203 AND T.STORAGECOMPANYID <>2203

3482

                  

 inner join warehouse
    on warehouse.code = t.newwarehousecode
 where t.qty > 0
   and exists (select warehousearea.id
          from warehousearea
        /* inner join warehousearea parent
        on parent.id = warehousearea.parentid*/
         where warehouse.id = warehousearea.warehouseid
           and warehousearea.code = t.locationcode
              --  and parent.code = t.areacode
           and warehousearea.areakind = 3)
   and exists
 (select 1 from sparepart where sparepart.code = t.newpartcode) and nvl(t.oldstdprice,0)<>nvl(t.oldstdprice,0);
SELECT * FROM partsstock where partsstock.storagecompanyid=3482
select *from company where code='FT000067'
/*
create table tmpdata.warehouse150130 as select * from warehouse;
delete from tmpdata.partsstockdlk150121
 where exists (select 1
          from tmpdata.warehouse150130 
         where warehouse150130.id = partsstockdlk150121.warehouseid
           and warehouse150130.branchid <> 2203);
*/
