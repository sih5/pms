/*create table tmpdata.entnodetemplate150122 as select * from yxsecurity.entnodetemplate a  where rownum<1;
insert into tmpdata.entnodetemplate150122
  (id,
   code,
   name,
   status,
   remark,
   enterpriseid,
   creatorid,
   creatorname,
   createtime)
  select s_entnodetemplate.nextval,
         decode(CODE,
                '服务站',
                'FDFWZ',
                '代理库',
                'FDDLK',
                '供应商',
                'FDGYS',
                '专卖店',
                'FDZMD',
                '服务站兼代理库',
                'FDFWZJDLK'),
         '福戴' || NAME,
         2,
         ID,
         (SELECT ID FROM ENTERPRISE WHERE CODE = '2450'),
         1,
         'Admin',
         SYSDATE
    from entnodetemplate
   WHERE CODE IN ('服务站', '代理库', '供应商', '专卖店', '服务站兼代理库')

CREATE TABLE TMPDATA.Entnodetemplatedetail150122 AS 
SELECT * FROM yxsecurity.Entnodetemplatedetail V WHERE ROWNUM<1;
INSERT INTO TMPDATA.Entnodetemplatedetail150122 (ID,ENTNODETEMPLATEID,NODEID)
SELECT S_Entnodetemplatedetail.Nextval,
       T.REMARK,
      Entnodetemplatedetail.Nodeid
  FROM tmpdata.entnodetemplate150122 T
  INNER JOIN Entnodetemplatedetail ON
  Entnodetemplatedetail.Entnodetemplateid = T.REMARK;
INSERT INTO Entnodetemplatedetail SELECT * FROM TMPDATA.Entnodetemplatedetail150122;
INSERT INTO entnodetemplate SELECT * FROM tmpdata.entnodetemplate150122;
delete from entnodetemplate where exists (select 1 from tmpdata.entnodetemplate150122 where entnodetemplate150122.id=entnodetemplate.id)
*/


create table tmpdata.entnodetemplate140126(code varchar2(100),name varchar2(100),oldid number (9));

create table tmpdata.Entnodetemplatedetail140126(nodeid number(9),entnodetemplateid number (9));


create table tmpdata.entnodetemplate150122 as select * from yxsecurity.entnodetemplate a  where rownum<1;

insert into tmpdata.entnodetemplate150122
  (id,
   code,
   name,
   status,
   remark,
   enterpriseid,
   creatorid,
   creatorname,
   createtime)
  select s_entnodetemplate.nextval,
         t.code,
         t.name,
         2,
         '',
         (SELECT ID FROM ENTERPRISE WHERE CODE = '2450'),
         1,
         'Admin',
         SYSDATE
    from tmpdata.entnodetemplate140126 t
insert into entnodetemplate select * from  tmpdata.entnodetemplate150122;


CREATE TABLE TMPDATA.Entnodetemplatedetail150122 AS 
SELECT * FROM yxsecurity.Entnodetemplatedetail V WHERE ROWNUM<1; 
INSERT INTO TMPDATA.Entnodetemplatedetail150122 (ID,ENTNODETEMPLATEID,NODEID)
SELECT S_Entnodetemplatedetail.Nextval,
       c.id,
      t.Nodeid
  FROM tmpdata.Entnodetemplatedetail140126 T inner join  tmpdata.entnodetemplate140126 b on t.entnodetemplateid=b.oldid
  inner join tmpdata.entnodetemplate150122 c on c.code=b.code;
  --1972
  insert into Entnodetemplatedetail select * from TMPDATA.Entnodetemplatedetail150122;






create table tmpdata.entnodetemplate150127 as select * from yxsecurity.entnodetemplate a  where rownum<1;

insert into tmpdata.entnodetemplate150127
  (id,
   code,
   name,
   status,
   remark,
   enterpriseid,
   creatorid,
   creatorname,
   createtime)
  select s_entnodetemplate.nextval,
         t.code,
         t.name,
         2,
         '',
         (SELECT ID FROM ENTERPRISE WHERE CODE = '2450'),
         1,
         'Admin',
         SYSDATE
    from tmpdata.entnodetemplate140126 t where t.oldid in(122,123);
insert into entnodetemplate select * from  tmpdata.entnodetemplate150127;


CREATE TABLE TMPDATA.Entnodetemplatedetail150127 AS 
SELECT * FROM yxsecurity.Entnodetemplatedetail V WHERE ROWNUM<1; 
INSERT INTO TMPDATA.Entnodetemplatedetail150127 (ID,ENTNODETEMPLATEID,NODEID)
SELECT S_Entnodetemplatedetail.Nextval,
       c.id,
      t.Nodeid
  FROM tmpdata.Entnodetemplatedetail140126 T inner join  tmpdata.entnodetemplate140126 b on t.entnodetemplateid=b.oldid
  inner join tmpdata.entnodetemplate150127 c on c.code=b.code;
  --1972
  insert into Entnodetemplatedetail select * from TMPDATA.Entnodetemplatedetail150127;


