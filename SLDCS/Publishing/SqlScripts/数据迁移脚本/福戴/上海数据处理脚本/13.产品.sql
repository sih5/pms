
create table tmpdata.product150128ls as
select id,code from product where code in(
select t.productcode
              from tmpdata.tStdProductin_OM_150120 t --12787 t
             where exists
             (select 1 from product where product.code = t.ProductCode)
               and not exists (select 1
                      from tmpdata.Om_tstdproductin150122 v
                     where t.productcode = v.产品编号))
                     
create table tmpdata.vehicleinformation150128ls as
select id, t.productcode
  from vehicleinformation t
 where t.productcode in (select code from tmpdata.product150128ls);


update product set code='LS'||CODE
WHERE EXISTS(SELECT 1 FROM  tmpdata.product150128ls WHERE product150128ls.ID=product.ID);

UPDATE vehicleinformation
   SET PRODUCTCODE = 'LS' || PRODUCTCODE
 WHERE EXISTS (SELECT 1
          FROM tmpdata.vehicleinformation150128ls
         WHERE vehicleinformation150128ls.ID = vehicleinformation.ID);

select distinct MainBrandCode,MainBrandname from  ft.tStdProductin v
Create table tmpdata.Om_tstdproductin150122--实时
as
select v.ProductCode       as 产品编号,
       v.ProductName       as 车型名称,
       v.MainBrandCode     as 品牌代码,
       v.MainBrandname     as 品牌名称,
       v.SubBrandCode      as 子品牌代码,
       v.SubBrandName      as 子品牌名称,
       v.PlatFormCode      as 平台代码,
       v.PlatFormName      as 平台名称,
       v.PKind             as 产品线代码,
       v.LineName          as 产品线名称,
       v.InnerCode         as 原始内部编号,
       v.BulletinCode      as 公告号,
       v.Load              as 吨位代码,
       v.EngineType        as 发动机型号代码,
       v.EngineFactoryCode as 发动机生产厂家代码,
       v.GearType          as 变速箱型号代码,
       v.GearFactoryCode   as 变速箱生产厂家代码,
       v.BrideType         as 后桥型号代码,
       v.BrideFactoryCode  as 后桥生产厂家代码,
       v.TyreType          as 轮胎型号代码,
       v.TyreFactoryCode   as 轮胎形式代码,
       v.brakeType         as 制动方式代码,
       v.DriveType         as 驱动方式代码,
       v.FrameType         as 车架连接形式代码,
       v.FSize             as 整车尺寸,
       v.wheelbase         as 轴距代码,
       v.emissionType      as 排放标准代码,
       v.PFunctionType     as 产品功能代码,
       v.Pstand            as 产品档次代码,
       nvl(v.vtype,'空')           as 车辆类型代码,
       nvl(v.vtype,'空')           as 车辆类型名称,
       v.Color             as 颜色描述,
       v.SEATCOUNTER       as 载客个数,
       v.WEIGHT            as 自重
      -- tbrands.name                    as 服务品牌,
       --tproductlines.name              as 服务产品线
  from tmpdata.tStdProductin_OM_150120 v where not exists(select 1 from product where product.code=v.productcode)-- and 其他条件
  ;
--where exists(select 1 from tmpdata.TPURCHASEINFOS140712 t where v.productcode = t.产品编号)
   create table tmpdata.product150122 as select * from product where rownum<0;
insert into tmpdata.product150122 
(ID,
   CODE,
   PRODUCTCATEGORYCODE,
   PRODUCTCATEGORYNAME,
   BRANDID,
   BRANDCODE,
   BRANDNAME,
   SUBBRANDCODE,
   SUBBRANDNAME,
   TERRACECODE,
   TERRACENAME,
   PRODUCTLINE,
   PRODUCTNAME,
   OLDINTERNALCODE,
   ANNOUCEMENTNUMBER,
   TONNAGECODE,
   ENGINETYPECODE,
   ENGINEMANUFACTURERCODE,
   GEARSERIALTYPECODE,
   GEARSERIALMANUFACTURERCODE,
   REARAXLETYPECODE,
   REARAXLEMANUFACTURERCODE,
   TIRETYPECODE,
   TIREFORMCODE,
   BRAKEMODECODE,
   DRIVEMODECODE,
   FRAMECONNETCODE,
   VEHICLESIZE,
   AXLEDISTANCECODE,
   EMISSIONSTANDARDCODE,
   PRODUCTFUNCTIONCODE,
   PRODUCTLEVELCODE,
   VEHICLETYPECODE,
   COLORDESCRIBE,
   SEATINGCAPACITY,
   WEIGHT ,STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_product.nextval,
         t.产品编号 as 整车编码,
         nvl(t.车辆类型代码,'无'),
         nvl(t.车辆类型名称,'无') as 车型名称,
         nvl(t.品牌代码 || t.子品牌代码 || t.平台代码 || t.产品线代码 ||产品功能代码,0) as brandid,
         t.品牌代码 as 品牌代码,
         t.品牌名称 as 品牌名称,
         t.子品牌代码 as 子品牌代码,
         t.子品牌名称 as 子品牌名称,
         t.平台代码 as 平台代码,
         t.平台名称 as 平台名称,
         t.产品线代码 as 产品线代码,
         t.产品线名称 as 产品线名称,
         substrb(t.原始内部编号,1,50) as 原始内部编号,
         t.公告号 as 公告号,
         t.吨位代码 as 吨位代码,
         t.发动机型号代码 as 发动机型号代码,
         t.发动机生产厂家代码 as 发动机生成厂家代码,
         t.变速箱型号代码 as 变速箱型号代码,
         t.变速箱生产厂家代码 as 变速箱生成厂家代码,
         t.后桥型号代码 as 后桥型号代码,
         t.后桥生产厂家代码 as 后桥生产厂家代码,
         t.轮胎型号代码 as 轮胎型号代码,
         t.轮胎形式代码 as 轮胎形式代码,
         t.制动方式代码 as 制动方式代码,
         t.驱动方式代码 as 驱动方式代码,
         t.车架连接形式代码 as 车架连接形式代码,
         0 as 整车尺寸,
         t.轴距代码 as 轴距代码,
         t.排放标准代码 as 排放标准代码,
         t.产品功能代码 as 产品功能代码,
         t.产品档次代码 as 产品档次代码,
         t.车辆类型代码 as 车辆类型代码,
         t.颜色描述 as 颜色描述,
         t.载客个数 as 载客个数,
         t.自重 as 自重,
         1,
         1,
         'Admin',
         sysdate
    from tmpdata.Om_tstdproductin150122 t;
    insert into product select * from tmpdata.product150122 ;
create table tmpdata.ServiceProdLineProduct150122 as select * from ServiceProdLineProduct where rownum<0;

--插入94条
insert into tmpdata.ServiceProdLineProduct150122 (id,Brandid,Vehiclebrandcode,Vehiclebrandname,Subbrandcode,Subbrandname,Terracecode,Terracename,Vehicleproductline,Vehicleproductname,ProductFunctionCode)
select s_ServiceProdLineProduct.Nextval,
       v.brandid,
       v.brandcode,
       v.brandname,
       v.subbrandcode,
       v.subbrandname,
       v.terracecode,
       v.terracename,
       v.productline,
       v.productname,
       v.ProductFunctionCode
  from (select distinct brandid,
                        BRANDCODE,
                        BRANDNAME,
                        SUBBRANDCODE,
                        SUBBRANDNAME,
                        TERRACECODE,
                        TERRACENAME,
                        PRODUCTLINE,
                        PRODUCTNAME,
                        ProductFunctionCode
          from tmpdata.product150122) v where not exists(select 1 from ServiceProdLineProduct
          where v.brandid=ServiceProdLineProduct.Brandid);
--插入产品产品线关系表
insert into ServiceProdLineProduct SELECT * FROM tmpdata.ServiceProdLineProduct150122;
