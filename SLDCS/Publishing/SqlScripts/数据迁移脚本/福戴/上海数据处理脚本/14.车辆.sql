create table tmpdata.oldnewvinrel150122(oldcode varchar2(100),newcode varchar2(100));
select oldcode from tmpdata.oldnewvinrel150122 group by oldcode having count(oldcode)>1;

select newcode from tmpdata.oldnewvinrel150122 group by newcode having count(newcode)>1;

select * from tmpdata.oldnewvinrel150122 where newcode='LVBS6PEB69H000032'

delete tmpdata.oldnewvinrel150122
 where oldnewvinrel150122.rowid not in
       (select min(oldnewvinrel150122.rowid)
          from tmpdata.oldnewvinrel150122
         group by  oldcode,newcode);
alter table TMPDATA.TPURCHASEINFOS_OM_150120 add newvin VARCHAR2(40);
update TMPDATA.TPURCHASEINFOS_OM_150120
   set newvin = nvl((select newcode
                      from tmpdata.oldnewvinrel150122 v
                     where v.oldcode = tPurchaseinfos_OM_150120.VINCODE),
                    VINCODE);
Create table tmpdata.om_TPURCHASEINFOS150122
as
select newvin VIN码,
       tPurchaseinfos_OM_150120.Leave_Factory_Code 出厂编号,
       tPurchaseinfos_OM_150120.Auto_No 车牌号,
       tPurchaseinfos_OM_150120.Sales_Flag 是否售出,
       tPurchaseinfos_OM_150120.Product_Type_Name 销售车型,
       tPurchaseinfos_OM_150120.Product_Out_Type_Name 公告号,
       tPurchaseinfos_OM_150120.Engine_Model 发动机型号,
       tPurchaseinfos_OM_150120.Engine_Code 发动机序列号,
       tPurchaseinfos_OM_150120.Leave_Factory_Date 出厂日期,
       tPurchaseinfos_OM_150120.Sales_Date 销售日期,
       tPurchaseinfos_OM_150120.invoices 销售发票号,
       tPurchaseinfos_OM_150120.sales_price 销售价格,
       tPurchaseinfos_OM_150120.Product_Whole_Code 产品编号,
       tPurchaseinfos_OM_150120.Customer_Name 客户名称,
       tPurchaseinfos_OM_150120.Customer_ID 客户Row_id,
       ' ' as 车辆种类,
       ' ' as 车辆类型,
       ' ' 车系,
       tPurchaseinfos_OM_150120.Product_Line_Name 产品线名称,
       tPurchaseinfos_OM_150120.Cab_Model 驾驶室型号,
       ' ' as 前桥号,
       ' ' as 中桥号,
       ' ' as 后桥号,
       ' ' as 平衡轴号,
       ' ' as 变速箱序列号,
       tPurchaseinfos_OM_150120.color 颜色,
       tPurchaseinfos_OM_150120.Franchiser_Name,
       t.brandcode 品牌,
       tPurchaseinfos_OM_150120.mile,
       t.PRODUCTCATEGORYCODE,
       t.PRODUCTCATEGORYNAME
  from tmpdata.tPurchaseinfos_OM_150120
 inner join tmpdata.product150122 t
    on t.code = tPurchaseinfos_OM_150120.product_whole_code
 where not exists
 (select 1
          from vehicleinformation
         where vehicleinformation.vin = tPurchaseinfos_OM_150120.VINCODE);
    
    
    create table tmpdata.VEHICLEINFORMATION150122 as 
select * from VEHICLEINFORMATION where rownum<0;


create table tmpdata.vi_obj150122 as 
select t.vin码, rownum sn
  from tmpdata.om_TPURCHASEINFOS150122 t;
  
   declare
  i number;
  j number;
begin
  j:=1;
  For i in 1 ..  loop  
insert into tmpdata.VEHICLEINFORMATION150122
  (ID,
   VIN,
   SERIALNUMBER,
   OLDVIN,
   RESPONSIBLEUNITID,
   RESPONSIBLEUNITNAME,
   VEHICLECATEGORYID,
   VEHICLECATEGORYNAME,
   VEHICLELICENSEPLATE,
   VIPVEHICLE,
   VEHICLESERIES,
   DEALERNAME,
   PRODUCTLINENAME,
   PRODUCTCATEGORYTERRACE,
   VEHICLEFUNCTION,
  -- VEHICLECOLOR,
   TOPSCODE,
   ADAPTTYPE,
   ADAPTCOMPANYNAME,
   CAGECATEGORY,
   DRIVEMODECODE,
   BRIDGETYPE,
   ISROADVEHICL,
   FRONTAXLECODE,
   SECONDFRONTAXLECODE,
   CENTREAXLECODE,
   BEHINDAXLECODE,
   BEHINDAXLETYPTONNELEVEL,
   BALANCESHAFTCODE,
   STEERINGENGINE,
   CHASSISBRANDNAME,
   CHASSISCODE,
   PRODUCTID,
   PRODUCTCODE,
   BRANDNAME,
   
   ENGINEMANUFACTURE,
   ANNOUCEMENTNUMBER,
   ENGINEMODEL,
   ENGINEMODELID,
   GEARMODEL,
   ENGINESERIALNUMBER,
   GEARSERIALNUMBER,
   FIRSTSTOPPAGEMILEAGE,
   OUTOFFACTORYdate,
   ISWORKOFF,
   SALESdate,
   CAPACITY,
   WORKINGHOURS,
   MILEAGE,
   LASTMAINTENANCEWORKINGHOURS,
   LASTMAINTENANCECAPACITY,
   LASTMAINTENANCEMILEAGE,
   LASTMAINTENANCETIME,
   SALESINVOICENUMBER,
  INVOICEdate,
  -- VEHICLETYPE,
  VehicleColor,
   REMARK,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME,PRODUCTCATEGORYCODE,PRODUCTCATEGORYname)
  select s_VEHICLEINFORMATION.Nextval,
         t.vin码,
         nvl(substr(t.vin码, 10, 8),0) SERIALNUMBER,
         t.vin码,
         null ,
         null,
         1 VEHICLECATEGORYID,
         2 VEHICLECATEGORYNAME,
          t.车牌号,
         0,
         t.车系 VEHICLESERIES,
         t.franchiser_name,
         t.产品线名称,
         null ,
         null,
         --null,
         null TOPSCODE,
         null ADAPTTYPE,
         null ADAPTCOMPANYNAME,
         t.驾驶室型号 Cab_Model,
          null,
         null BRIDGETYPE,
         0,
         t.前桥号 FRONTAXLECODE,
         null SECONDFRONTAXLECODE,
         t.中桥号 CENTREAXLECODE,
         t.后桥号 BEHINDAXLECODE,
         null BEHINDAXLETYPTONNELEVEL,
         t.平衡轴号 BALANCESHAFTCODE,
         null STEERINGENGINE,
         t.品牌 CHASSISBRANDNAME,
          t.产品编号  CHASSISCODE,
         b.id PRODUCTID,
         t.产品编号,
         t.品牌,
         
         null ENGINEMANUFACTURE,
         t.公告号 Product_Out_Type_Name,
         t.发动机型号,
         0,
         0,
         t.发动机序列号,
         t.变速箱序列号 GEARSERIALNUMBER,
         t.mile FIRSTSTOPPAGEMILEAGE,
         t.出厂日期 OUTOFFACTORYdate,
         nvl(t.是否售出,0) ISWORKOFF,
         t.销售日期 SALESdate,
         0 CAPACITY,
         0 WORKINGHOURS,
         mile,
         0 LASTMAINTENANCEWORKINGHOURS,
         0 LASTMAINTENANCECAPACITY,
         0 LASTMAINTENANCEMILEAGE,
         null LASTMAINTENANCETIME,
         t.销售发票号 SALESINVOICENUMBER,
        销售日期,
        -- t.,
        t.颜色,
         t.客户row_id,
         1,
         1,
         'Admin',
         sysdate,PRODUCTCATEGORYCODE,PRODUCTCATEGORYname
    from tmpdata.om_TPURCHASEINFOS150122 t 
    inner join product  b on  b.code=t.产品编号 where    
    exists (select 1 from tmpdata.vi_obj150122 v where v.vin码=t.vin码 and v.sn>=j and v.sn<(j+10000) );
                  j := j + 10000;
    commit;
     end loop;
  end; 
