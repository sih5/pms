select a.id,a.chassisbrandname,a.chassiscode from vehicleinformation a where exists(select 1 from tmpdata.tPurchaseinfos_OM_150120 b where b.newvin=a.vin)
and not exists(select 1 from tmpdata.vehicleinformation150122 c where c.vin=a.vin)
and a.chassisbrandname is null and a.chassiscode is null

update vehicleinformation a
   set a.chassisbrandname = '킹참',
       a.chassiscode     =
       (select b.product_whole_code
          from tmpdata.tPurchaseinfos_OM_150120 b
         where b.newvin = a.vin)
 where exists (select 1
          from tmpdata.tPurchaseinfos_OM_150120 b
         where b.newvin = a.vin)
   and not exists
 (select 1 from tmpdata.vehicleinformation150122 c where c.vin = a.vin)
   and a.chassisbrandname is null
   and a.chassiscode is null


select count(*) from vehicleinformation where chassisbrandname = '킹참' and chassiscode is not null and  exists(select 1 from tmpdata.VEHICLEINFORMATION150122 t  
where t.id= vehicleinformation.id)


create table TMPDATA.Vehiclewarrantycard150122_1 as
select * from yxdcs.vehiclewarrantycard where rownum<0;

--243039
insert into TMPDATA.Vehiclewarrantycard150122_1
  (ID,
   CODE,
   VEHICLEID,
   VIN,
   VEHICLECATEGORYID,
   VEHICLECATEGORYNAME,
   SERVICEPRODUCTLINEID,
   SERVICEPRODUCTLINENAME,
   PRODUCTID,
   PRODUCTCODE,
   SALESDATE,
   WARRANTYSTARTDATE,
   RETAINEDCUSTOMERID,
   CUSTOMERNAME,
   CUSTOMERGENDER,
   IDDOCUMENTTYPE,
   IDDOCUMENTNUMBER,
   WARRANTYPOLICYCATEGORY,
   WARRANTYPOLICYID,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_VehicleWarrantyCard.Nextval,
         'VWC20150122' || decode(length(rownum),
                                 1,
                                 '10000' || rownum,
                                 2,
                                 '1000' || rownum,
                                 3,
                                 '100' || rownum,
                                 4,
                                 '10' || rownum,
                                 5,
                                 '1' || rownum,rownum+100000) CODE,
         t.id,
         t.vin,
         1 VEHICLECATEGORYID,
         2 VEHICLECATEGORYNAME,
         (select a.Serviceproductlineid from ServiceProdLineProduct a
             inner join product b on b.brandid = a.brandid where
             b.id = t.productid)SERVICEPRODUCTLINEID,
         (select a.Serviceproductlinename from ServiceProdLineProduct a
             inner join product b on b.brandid = a.brandid where
             b.id = t.productid) SERVICEPRODUCTLINENAME,
         (select id from product a where a.id = t.productid) PRODUCTID,
         (select a.code from product a where a.id = t.productid) PRODUCTCODE,
         t.salesdate,
         nvl(t.salesdate,to_date('1899-1-1','yyyy-mm-dd')) WARRANTYSTARTDATE,
       a.id,
        b.name,
         b.gender,
           b.iddocumenttype,
 b.iddocumentnumber,
      nvl((select distinct d.Category
            from WarrantyPolicy d
           inner join WarrantyPolicyNServProdLine a
              on a. WarrantyPolicyId = d.id
           inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1          
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate /*and rownum=1*/),0) WARRANTYPOLICYCATEGORY,
       nvl((select a.WarrantyPolicyId
            from WarrantyPolicyNServProdLine a
          inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1    
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate /*and rownum=1*/),0) WARRANTYPOLICYID,
         1 STATUS,
         1 CREATORID,
         'Admin' CREATORNAME,
         sysdate CREATETIME
    from    tmpdata.VEHICLEINFORMATION150122 t 
    inner join retainedcustomervehiclelist v on t.id=v.vehicleid
 inner join retainedcustomer a on a.id=v.retainedcustomerid
 inner join customer b on b.id=a.customerid where t.salesdate>=to_date('2012-01-01','yyyy-mm-dd')
and exists(select distinct d.Category
            from WarrantyPolicy d
           inner join WarrantyPolicyNServProdLine a
              on a. WarrantyPolicyId = d.id
           inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1          
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate );

--5631
create table TMPDATA.Vehiclewarrantycard150122_2 as
select * from yxdcs.vehiclewarrantycard where rownum<0;
insert into TMPDATA.Vehiclewarrantycard150122_2
  (ID,
   CODE,
   VEHICLEID,
   VIN,
   VEHICLECATEGORYID,
   VEHICLECATEGORYNAME,
   SERVICEPRODUCTLINEID,
   SERVICEPRODUCTLINENAME,
   PRODUCTID,
   PRODUCTCODE,
   SALESDATE,
   WARRANTYSTARTDATE,
   RETAINEDCUSTOMERID,
   CUSTOMERNAME,
   CUSTOMERGENDER,
   IDDOCUMENTTYPE,
   IDDOCUMENTNUMBER,
   WARRANTYPOLICYCATEGORY,
   WARRANTYPOLICYID,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_VehicleWarrantyCard.Nextval,
         'VWC20150122' || decode(length(rownum),
                                 1,
                                 '20000' || rownum,
                                 2,
                                 '2000' || rownum,
                                 3,
                                 '200' || rownum,
                                 4,
                                 '20' || rownum,
                                 5,
                                 '2' || rownum,rownum+200000) CODE,
         t.id,
         t.vin,
         1 VEHICLECATEGORYID,
         2 VEHICLECATEGORYNAME,
         (select a.Serviceproductlineid from ServiceProdLineProduct a
             inner join product b on b.brandid = a.brandid where
             b.id = c.id)SERVICEPRODUCTLINEID,
         (select a.Serviceproductlinename from ServiceProdLineProduct a
             inner join product b on b.brandid = a.brandid where
             b.id = c.id) SERVICEPRODUCTLINENAME,
         c.id PRODUCTID,
         c.code PRODUCTCODE,
         t.salesdate,
         nvl(t.salesdate,to_date('1899-1-1','yyyy-mm-dd')) WARRANTYSTARTDATE,
       a.id,
        b.name,
         b.gender,
           b.iddocumenttype,
 b.iddocumentnumber,
      nvl((select distinct d.Category
            from WarrantyPolicy d
           inner join WarrantyPolicyNServProdLine a
              on a. WarrantyPolicyId = d.id
           inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1          
           where b.ProductId = c.id and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate /*and rownum=1*/),0) WARRANTYPOLICYCATEGORY,
       nvl((select a.WarrantyPolicyId
            from WarrantyPolicyNServProdLine a
          inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1    
           where b.ProductId = c.id and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate /*and rownum=1*/),0) WARRANTYPOLICYID,
         1 STATUS,
         1 CREATORID,
         'Admin' CREATORNAME,
         sysdate CREATETIME
    from    vehicleinformation t 
    inner join retainedcustomervehiclelist v on t.id=v.vehicleid and v.status<>99
 inner join retainedcustomer a on a.id=v.retainedcustomerid
 inner join customer b on b.id=a.customerid
 inner join product c on c.code=t.chassiscode  where t.salesdate>=to_date('2012-01-01','yyyy-mm-dd')
and exists(select distinct d.Category
            from WarrantyPolicy d
           inner join WarrantyPolicyNServProdLine a
              on a. WarrantyPolicyId = d.id
           inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1          
           where b.ProductId = c.id and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate )
           and t.chassisbrandname = '킹참' and t.chassiscode is not null
           and not exists (select 1 from vehiclewarrantycard where vehiclewarrantycard.vin=t.vin and vehiclewarrantycard.productcode=c.code and vehiclewarrantycard.status<>99);
        
select * from vehicleinformation t  inner join retainedcustomervehiclelist v on t.id=v.vehicleid
        -- inner join retainedcustomer a on a.id=v.retainedcustomerid
          where vin='LRDV6PEC6DR025301'
          
          
             
insert into VehicleWarrantyCard
  select * from TMPDATA.Vehiclewarrantycard150122_1  t where t.warrantypolicycategory <>0 ;


             
insert into VehicleWarrantyCard
  select * from TMPDATA.Vehiclewarrantycard150122_2  t where t.warrantypolicycategory <>0 ;
--delete from VehicleWarrantyCard where exists(select 1 from TMPDATA.Vehiclewarrantycard150122_2 a where a.id=VehicleWarrantyCard.id)
create table TMPDATA.VMaintenancePoilcy150122_1 as select * from VehicleMaintenancePoilcy where rownum<0;
--delete from VehicleMaintenancePoilcy where exists(select 1 from TMPDATA.VMaintenancePoilcy150122_1 t where t.id=VehicleMaintenancePoilcy.id)
declare
  cursor Cursor_WARRANTYPOLICYID is(
    select WARRANTYPOLICYID,id VEHICLEWARRANTYCARDID from TMPDATA.Vehiclewarrantycard150122_1 where Vehiclewarrantycard150122_1.WARRANTYPOLICYID is not null
    union 
    select WARRANTYPOLICYID,id VEHICLEWARRANTYCARDID from TMPDATA.Vehiclewarrantycard150122_2 where Vehiclewarrantycard150122_2.WARRANTYPOLICYID is not null);
begin
  for S_WARRANTYPOLICYID in Cursor_WARRANTYPOLICYID loop
    insert into TMPDATA.VMaintenancePoilcy150122_1
      select s_VehicleMaintenancePoilcy.Nextval,
             S_WARRANTYPOLICYID.VEHICLEWARRANTYCARDID VEHICLEWARRANTYCARDID,
             t.id                               VEHICLEMAINTETERMID,
             t.code                             VEHICLEMAINTETERMCODE,
             t.name                             VEHICLEMAINTETERMNAME,
             t.maintetype,
             t.workinghours,
             t.capacity,
             t.maxmileage,
             t.MINMILEAGE,
             t.DAYSUPPERLIMIT,
             t.DAYSLOWERLIMIT,
             0 IFEMPLOYED,
             t.DISPLAYORDER
        from VehicleMainteTerm t
       where t.Warrantypolicyid = S_WARRANTYPOLICYID.WARRANTYPOLICYID;
  end loop;
end;
--248670
insert into VehicleMaintenancePoilcy select * from TMPDATA.VMaintenancePoilcy150122_1;
create table TMPDATA.VMPH150122_1 as
select * from VehicleMaintePoilcyHistroy where rownum < 0;

insert into TMPDATA.VMPH150122_1
  (ID,
   VEHICLEMAINTENANCEPOILCYID,
   VEHICLEWARRANTYCARDID,
   VEHICLEMAINTETERMID,
   VEHICLEMAINTETERMCODE,
   VEHICLEMAINTETERMNAME,
   MAINTETYPE,
   WORKINGHOURS,
   CAPACITY,
   MAXMILEAGE,
   MINMILEAGE,
   DAYSUPPERLIMIT,
   DAYSLOWERLIMIT,
   IFEMPLOYED,
   DISPLAYORDER,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_VehicleMaintePoilcyHistroy.Nextval,
         id,
         VEHICLEWARRANTYCARDID,
         VEHICLEMAINTETERMID,
         VEHICLEMAINTETERMCODE,
         VEHICLEMAINTETERMNAME,
         MAINTETYPE,
         WORKINGHOURS,
         CAPACITY,
         MAXMILEAGE,
         MINMILEAGE,
         DAYSUPPERLIMIT,
         DAYSLOWERLIMIT,
         IFEMPLOYED,
         DISPLAYORDER,
         1,
         1,
         'Admin',
         sysdate
    from TMPDATA.VMaintenancePoilcy150122_1;

insert into VehicleMaintePoilcyHistroy
  select * from TMPDATA.VMPH150122_1;
delete from VehicleMaintePoilcyHistroy where exists(select 1 from TMPDATA.VMPH150122_1 t where t.id=VehicleMaintePoilcyHistroy.id);
