create table tmpdata.OM_TCONSUMERS150124
as
  select distinct CUSTOMER_NAME 客户姓名,
                  性别,
                  case
                    when namelength <= 6 then
                     '个人客户'
                    else
                     '单位客户'
                  end as 客户类型,
                  年龄,
                  证件类型,
                  证件号码,
                  区域,
                  省份,
                  城市,
                  县,
                  邮政编码,
                  家庭电话,
                  单位电话,
                  手机号码,
                  电子邮件,
                  职务,
                  文化程度,
                  客户爱好,
                  详细地址,
                  客户id,'nf' as type 
    from (select tConsumers_OM_150120.customer_name as CUSTOMER_NAME,
                 decode(tConsumers_OM_150120.Sex, 0, '女', 1, '男', '未填') as 性别,
                 lengthb(tConsumers_OM_150120.customer_name) as namelength,
                 tConsumers_OM_150120.age as  年龄,
                 '居民身份证' as  证件类型,
                 tConsumers_OM_150120.ID_card as  证件号码,
                 ' ' as  区域,
                 tConsumers_OM_150120.PROVINCENAME as  省份,
                 tConsumers_OM_150120.CITYNAME as  城市,
                  tConsumers_OM_150120.COUNTYNAME as  县,
                 tConsumers_OM_150120.post 邮政编码,
                 tConsumers_OM_150120.home_phone 家庭电话,
                 tConsumers_OM_150120.office_phone 单位电话,
                 tConsumers_OM_150120.mobile_phone 手机号码,
                 tConsumers_OM_150120.email 电子邮件,
                 tConsumers_OM_150120.duty 职务,
                 decode( tConsumers_OM_150120.education,5,'博士研究生',4,'硕士研究生',3,'本科',2,'中学',1,'小学') as 文化程度,
                 tConsumers_OM_150120.liking as 客户爱好,
                 tConsumers_OM_150120.address as 详细地址,
                 tConsumers_OM_150120.row_id as 客户id
            from tmpdata.tConsumers_OM_150120
           inner join tmpdata.vehicleinformation150122
              on tmpdata.vehicleinformation150122.remark = tConsumers_OM_150120.row_id
           );
           
           -------------------------------------------------------------------------------------------
           
create table TMPDATA.Customer150124 as SELECT * FROM Customer WHERE ROWNUM<0;
create index vehicleinformation150122_cidx on tmpdata.vehicleinformation150122 (remark);
 TPURCHASEINFOS140705
--插入基础客户信息临时表 441259
INSERT INTO TMPDATA.Customer150124
  (ID,
   NAME,
   GENDER,
   CUSTOMERTYPE,
   CELLPHONENUMBER,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME,
   Ifcomplain,
   customercode,
   AGE,   
   ADDRESS,
   POSTCODE,
   HOMEPHONENUMBER,
   OFFICEPHONENUMBER,
   IDDOCUMENTNUMBER,
   EMAIL,
   IDDOCUMENTTYPE,remark,PROVINCENAME,CITYNAME,COUNTYNAME)
  SELECT S_Customer.Nextval,
         nvl(t.客户姓名, '空'),
         decode(t.性别, '未填', 0, '男', 1, 2),
         decode(t.客户类型, '个人客户', 1, 2),       
         substrb(t.手机号码, 1, 50),         
         1 STATUS,        
         1 CREATORID,
         'Admin' CREATORNAME,
         sysdate CREATETIME,    
         0 Ifcomplain,
         'CSM20150124' || decode(length(rownum),
                                 1,
                                 '00000' || rownum,
                                 2,
                                 '0000' || rownum,
                                 3,
                                 '000' || rownum,
                                 4,
                                 '00' || rownum,
                                 5,
                                 '0' || rownum,
                                 6,
                                 rownum) CODE,
         t.年龄,
         substrb(t.详细地址,1,200),
         substrb(t.邮政编码,1,6),
         substrb(t.家庭电话,1,50),
         substrb(t.单位电话,1,50),
         t.证件号码,
         t.电子邮件,
         decode(t.证件类型,'居民身份证',1),t.客户id,t.省份,t.城市,t.县
    FROM (select distinct t.* from tmpdata.OM_TCONSUMERS150124 t
    inner join  tmpdata.vehicleinformation150122 v on t.客户id=v.remark) t;
    select count(*)from tmpdata.OM_TCONSUMERS150124 t 
     inner join  tmpdata.om_TPURCHASEINFOS150122 v on t.客户id=v.客户row_id;
-- SELECT * FROM DCS.TILEDREGION 

--插入基础客户表 
update  TMPDATA.Customer150124 v
   set v.regionid =
       (select a.id
          from tiledregion a
         where a.provincename = v.provincename
           and a.cityname = v.cityname
           and a.countyname is null),
       v.regionname = (select a.regionname
                         from tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname = v.cityname
                          and a.countyname is null)
 where v.provincename is not null
   and v.cityname is not null
   and v.countyname is null;
   
update  TMPDATA.Customer150124 v
   set v.regionid =
       (select a.id
          from tiledregion a
         where a.provincename = v.provincename
           and a.cityname = v.cityname
           and a.countyname =v.countyname),
       v.regionname = (select a.regionname
                         from tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname = v.cityname
                          and a.countyname =v.countyname)
 where v.provincename is not null
   and v.cityname is not null
   and v.countyname is not null
   and regionid is null;
  
 
update  TMPDATA.Customer150124 v
   set v.regionid =
       (select a.id
          from tiledregion a
         where a.provincename = v.provincename
           and a.cityname is null
           and a.countyname is null),
       v.regionname = (select a.regionname
                         from tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname is null
                          and a.countyname is null)
 where v.provincename is not null
   and v.cityname is  null
   and v.countyname is  null
   and regionid is null;
   
   --441259
insert into CUSTOMER select * from  TMPDATA.Customer150124;

/*
delete from CUSTOMER where exists(select 1 from  TMPDATA.Customer140918 t where t.id=CUSTOMER.id)
select * from TMPDATA.Customer140918 t where remark in(
select t.remark from  TMPDATA.Customer140918 t group by t.remark having count (remark)>1) order by remark

select t.客户id from tmpdata.tconsumers140918 t group by t.客户id having count(t.客户id)>1
*/
--创建保有客户临时表
create table TMPDATA.RetainedCustomer150124  as select * from RetainedCustomer where rownum<0;
create index tmpdata.Customer150124_rmk on TMPDATA.Customer150124 (remark);
--插入保有客户临时表 441259
insert into TMPDATA.RetainedCustomer150124(
  ID                ,
  CUSTOMERID        ,
  IFVISITBACKNEEDED ,
  IFTEXTACCEPTED    ,
  IFADACCEPTED      ,
  CARDNUMBER        ,
  IFVIP             ,
  VIPTYPE           ,
  BONUSPOINTS       ,
  PLATEOWNER        ,
  IFMARRIED         ,
  WEDDINGDAY        ,
  FAMILYINCOME      ,
  LOCATION          ,
  FAMILYCOMPOSITION ,
  REFERRAL          ,
  HOBBY             ,
  SPECIALDAY        ,
  SPECIALDAYDETAILS ,
  USERGENDER        ,
  COMPANYTYPE       ,
  ACQUISITORNAME    ,
  ACQUISITORPHONE   ,
  CREATORID         ,
  CREATORNAME       ,
  CREATETIME        ,
  STATUS            ,
  REMARK            )
  select s_RetainedCustomer.Nextval,
 t.id,
  0 IFVISITBACKNEEDED ,
  0 IFTEXTACCEPTED    ,
  0 IFADACCEPTED      ,
  '空' CARDNUMBER        ,
  0 IFVIP             ,
  0 VIPTYPE           ,
  0 BONUSPOINTS       ,
  '空' PLATEOWNER        ,
  0 IFMARRIED         ,
  to_date('1899-1-1','yyyy-mm-dd') WEDDINGDAY        ,
  0 FAMILYINCOME      ,
  0 LOCATION          ,
  '空' FAMILYCOMPOSITION ,
  '空' REFERRAL          ,
substrb((select 客户爱好 from  tmpdata.OM_TCONSUMERS150124  v where v.客户id=remark),1,200)  HOBBY             ,
  to_date('1899-1-1','yyyy-mm-dd') SPECIALDAY        ,
  '空' SPECIALDAYDETAILS ,
  0 USERGENDER        ,
  0 COMPANYTYPE       ,
  '空' ACQUISITORNAME    ,
  '空' ACQUISITORPHONE   ,
  1 CREATORID,
  'Admin' CREATORNAME,
  sysdate CREATETIME,
  1 STATUS,
  t.remark
  from TMPDATA.Customer150124 t/*;
  commit;*/
  where exists(select 1 from   tmpdata.vehicleinformation150122 v where t.remark=v.remark);


/*select count(*)  from TMPDATA.RetainedCustomer150124 t
  where exists(select 1 from   tmpdata.om_TPURCHASEINFOS150122 v where t.客户id=v.客户row_id)*/
--插入保有客户Id
insert into RetainedCustomer select * from TMPDATA.RetainedCustomer150124;


--535167
create table tmpdata.rcustomervehiclelist150124 as select * from retainedcustomervehiclelist where rownum<1;
--
insert into tmpdata.rcustomervehiclelist150124(id,retainedcustomerid,vehicleid,status)
select s_retainedcustomervehiclelist.nextval，v.id,t.id,1 from
tmpdata.VEHICLEINFORMATION150122 t inner join TMPDATA.RetainedCustomer150124 v
on t.remark=v.remark
/*
delete from retainedcustomervehiclelist t where exists(select 1 from tmpdata.rcustomervehiclelist141101 v where t.id=v.id)
*/
insert into retainedcustomervehiclelist select * from tmpdata.rcustomervehiclelist150124;

create table tmpdata.vehiclelinkman150124 as select * from vehiclelinkman where rownum<1;



  /* and exists
 (select * from tmpdata.vehiclelinkman140806_1 t where t.id = v.id)*/;


--535167
insert into tmpdata.vehiclelinkman150124
  (id,
   retainedcustomerid,
   vehicleinformationid,
   customercode,
   linkmantype,
   name,
   gender,
   cellphonenumber,
   email,
   address,
   status,
   creatorid,
   creatorname,
   createtime,
   regionid,
   regionname,
   provincename,
   cityname,
   countyname)
  select s_vehiclelinkman.nextval,
         v.id,
         t.id,
         'VL20150124' || decode(length(rownum),
                                1,
                                '00000' || rownum,
                                2,
                                '0000' || rownum,
                                3,
                                '000' || rownum,
                                4,
                                '00' || rownum,
                                5,
                                '0' || rownum,
                                6,
                                rownum),
         1,
         a.name,
         a.gender,
         a.cellphonenumber,
         a.email,
         a.address,
         1,
         1,
         'Admin',
         sysdate,
         a.regionid,
         a.regionname,
         a.provincename,
         a.cityname,
         a.countyname
    from tmpdata.VEHICLEINFORMATION150122 t
    inner join retainedcustomervehiclelist b on b.vehicleid=t.id
   inner join retainedcustomer v
      on v.id=b.retainedcustomerid and b.status<>99
   inner join customer a
      on v.customerid=a.id
select count(*)from  tmpdata.vehiclelinkman140918


--535167
insert into vehiclelinkman select * from tmpdata.vehiclelinkman150124;


create table tmpdata.vehiclelinkman150124_1 as select * from vehiclelinkman where rownum<1;
insert into tmpdata.vehiclelinkman150124_1
  (id,
   retainedcustomerid,
   vehicleinformationid,
   customercode,
   linkmantype,
   name,
   gender,
   cellphonenumber,
   email,
   address,
   status,
   creatorid,
   creatorname,
   createtime,
 
   provincename,
   cityname,
   countyname)
   select  s_vehiclelinkman.nextval,
         c.id,
         e.id,
         'VL20150125' || decode(length(rownum),
                                1,
                                '00000' || rownum,
                                2,
                                '0000' || rownum,
                                3,
                                '000' || rownum,
                                4,
                                '00' || rownum,
                                5,
                                '0' || rownum,
                                6,
                                rownum),
         2,
         a.name,
         0,
         a.custmobile,
         '',
         a.address,
         1,
         1,
         'Admin',
         sysdate,
        
         a.provincename,
         a.cityname,
         a.countyname
   from tmpdata.tPurchaseLinkers_OM_150120 a
   inner join tmpdata.tPurchaseinfos_OM_150120 b on a.parentid=b.objid
   inner join tmpdata.VEHICLEINFORMATION150122 e on e.vin=b.newvin 
    inner join retainedcustomer c
      on e.remark = c.remark
   inner join customer d
      on c.remark = d.remark where a.type<>0;

select a.*/*,b.newvin*/  from tmpdata.tPurchaseLinkers_OM_150120 a
 inner join tmpdata.tPurchaseinfos_OM_150120 b on a.parentid=b.objid
where  a.type<>0;
update TMPDATA.vehiclelinkman150124_1 v
   set v.regionid =
       (select a.id
          from tiledregion a
         where a.provincename = v.provincename
           and a.cityname = v.cityname
           and a.countyname is null),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname = v.cityname
                          and a.countyname is null)
 where v.provincename is not null
   and v.cityname is not null
   and v.countyname is null
   ;
--114580
update TMPDATA.vehiclelinkman150124_1 v
   set v.regionid =
       (select a.id
          from tiledregion a
         where a.provincename = v.provincename
           and a.cityname = v.cityname
           and a.countyname =v.countyname),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname = v.cityname
                          and a.countyname =v.countyname)
 where v.provincename is not null
   and v.cityname is not null
   and v.countyname is not null
   and regionid is null
  ;
 
update TMPDATA.vehiclelinkman150124_1 v
   set v.regionid =
       (select a.id
          from tiledregion a
         where a.provincename = v.provincename
           and a.cityname is null
           and a.countyname is null),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname is null
                          and a.countyname is null)
 where v.provincename is not null
   and v.cityname is  null
   and v.countyname is  null
   and regionid is null;    
   /*
   update TMPDATA.vehiclelinkman141101 v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname = v.cityname
           and a.countyname is null),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname = v.cityname
                          and a.countyname is null)
 where v.provincename is not null
   and v.cityname is not null
   and v.countyname is null
   ;

update TMPDATA.vehiclelinkman141101 v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname = v.cityname
           and a.countyname =v.countyname),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname = v.cityname
                          and a.countyname =v.countyname)
 where v.provincename is not null
   and v.cityname is not null
   and v.countyname is not null
   and regionid is null
  ;
 
update TMPDATA.vehiclelinkman141101 v
   set v.regionid =
       (select a.id
          from yxdcs.tiledregion a
         where a.provincename = v.provincename
           and a.cityname is null
           and a.countyname is null),
       v.regionname = (select a.regionname
                         from yxdcs.tiledregion a
                        where a.provincename = v.provincename
                          and a.cityname is null
                          and a.countyname is null)
 where v.provincename is not null
   and v.cityname is  null
   and v.countyname is  null
   and regionid is null;   
   */
      insert into vehiclelinkman select * from  TMPDATA.vehiclelinkman150124_1 v;
      --  insert into vehiclelinkman select * from  TMPDATA.vehiclelinkman141101 v;
        delete from  vehiclelinkman where exists(select 1 from TMPDATA.vehiclelinkman140918_1 v  where v.id=vehiclelinkman.id)

/*
 
create table tmpdata.rcustomervehiclelist141105 as select * from gcdcs.retainedcustomervehiclelist where rownum<1;

insert into tmpdata.rcustomervehiclelist141105(id,retainedcustomerid,vehicleid,status)
select s_retainedcustomervehiclelist.nextval,v.id,t.id,1 from
tmpdata.VEHICLEINFORMATION141101ins t inner join retainedcustomer v
on t.remark=v.remark
select count(*) from tmpdata.VEHICLEINFORMATION141101ins t
inner join  retainedcustomer v
on t.remark=v.remark
insert into retainedcustomervehiclelist select * from tmpdata.rcustomervehiclelist141101;

*/
