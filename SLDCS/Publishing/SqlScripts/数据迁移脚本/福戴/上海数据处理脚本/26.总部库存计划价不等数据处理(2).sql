create table tmpdata.om_tstores150128error as
select t.* from tmpdata.om_tstores150121 t
 inner join warehouse
    on warehouse.code = t.newwarehousecode
 where t.qty > 0
   and exists (select warehousearea.id
          from warehousearea
        /* inner join warehousearea parent
        on parent.id = warehousearea.parentid*/
         where warehouse.id = warehousearea.warehouseid
           and warehousearea.code = t.locationcode
              --  and parent.code = t.areacode
           and warehousearea.areakind = 3)
   and exists
 (select 1 from sparepart where sparepart.code = t.newpartcode) and nvl(t.newstdprice,0)<>nvl(t.oldstdprice,0);
/*create table tmpdata.om_newoldpartrel150127del as 
select * from tmpdata.om_newoldpartrel150121 t where exists(
select * from  tmpdata.om_tstores150121error v where t.oldcode=v.partcode and t.brand=v.brand and v.newstdprice is not null) ;

--insert into tmpdata.om_newoldpartrel150121 select * from tmpdata.om_newoldpartrel150127del
delete from  tmpdata.om_newoldpartrel150121 t where exists(
select * from  tmpdata.om_tstores150121error v where t.oldcode=v.partcode and t.brand=v.brand and v.newstdprice is not null)*/

PQ340B12T13F2SYB30  .7  保内
S1B24981110031Y3A2036  95.3846  保外
S1B24981220074Y3A2036  47.01  保内
FNV1AV13X1085CA8051  18.6325  保外
FNV1AV13X1260CA8051  18.6325  保外
S1B24981210074Y3A2036  95.38  保内
FH0101020010A0A1714  60.8205  保外
FNV1AV13X1130CA0883  21.9829  保外
S1B24981100000Y2A2036  46.15  保内
S1B24981110031Y1A2036  47.01  保内
RH1100CK2BZFT-1A6888  3418.8034  保外
PQ40312SYB30  .05  保内
F1417031100001A8026  318.8034  保外
F1425135643004A1285  372.6496  保外
F1SB2136100101AZ8610  2222.2222  保外
FNV1AV13*1085CA0943  21.9145  保外

/*
 update tmpdata.om_tstores150121  set newpartcode=null;
 update tmpdata.om_tstores150121
   set newpartcode =
       (select newcode
           from tmpdata.om_newoldpartrel150121
          where om_newoldpartrel150121.oldcode = om_tstores150121.partcode
            and om_tstores150121.brand = om_newoldpartrel150121.brand);
update tmpdata.om_tstores150121 set newpartcode=partcode
where exists(select 1 from dcs.sparepart where sparepart.code=partcode) and newpartcode is null;*/
/*update tmpdata.om_tstores150121 t
   set 
       newstdprice =
       (select a.plannedprice from tmpdata.stdprice150127_1 a
           inner join sparepart s on s.id = a.sparepartid
           inner join partssalescategory
              on partssalescategory.id =a.partssalescategoryid 
              where partssalescategory.name=decode(t.brand, '保内', '欧曼', '保外', '欧曼保外')
              and t.newpartcode=s.code)
           where exists(select 1 from tmpdata.stdprice150127_1 a
           inner join sparepart s on s.id = a.sparepartid
           inner join partssalescategory
              on partssalescategory.id =a.partssalescategoryid 
              where partssalescategory.name=decode(t.brand, '保内', '欧曼', '保外', '欧曼保外')
              and t.newpartcode=s.code)
           ;
           */
           
           
           
           

create table tmpdata.stdprice150128_2 as 
select id,
       t.partssalescategoryid,
       t.sparepartid,
       t.plannedprice
  from partsplannedprice t
 where exists (select 1
          from tmpdata.om_tstores150128error v
         inner join partssalescategory
            on partssalescategory.name =
               decode(v.brand, '保内', '欧曼', '保外', '欧曼保外')
         inner join sparepart
            on sparepart.code = v.newpartcode
         where t.sparepartid = sparepart.id
           and t.partssalescategoryid = partssalescategory.id);
           
           
           update partsplannedprice t
           set t.plannedprice=(select v.oldstdprice
          from tmpdata.om_tstores150128error v
         inner join partssalescategory
            on partssalescategory.name =
               decode(v.brand, '保内', '欧曼', '保外', '欧曼保外')
         inner join sparepart
            on sparepart.code = v.newpartcode
         where t.sparepartid = sparepart.id
           and t.partssalescategoryid = partssalescategory.id)
           where exists(select 1 from tmpdata.stdprice150128_2 where t.id=stdprice150128_2.id)
           /*
 select sparepartid,partssalescategoryid from tmpdata.stdprice150127_1 group by sparepartid,partssalescategoryid having count(*)>1
 insert into partsplannedprice select * from  tmpdata.stdprice150127_1;*/
 create table tmpdata.PlannedPriceApp150128_2 as select * from PlannedPriceApp where rownum<1;
create table tmpdata.PlannedPriceAppDetail150128_2 as select * from PlannedPriceAppDetail where rownum<1;

insert into tmpdata.PlannedPriceApp150128_2
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,RecordStatus,PlannedExecutionTime,ActualExecutionTime)
values
  (s_PlannedPriceApp.Nextval,
   (select id from company where code='2450'),
   '2450',
   (select name from company where code='2450'), (select id from partssalescategory where name='欧曼'),
   '欧曼',
   'PPA2450201501282001',
   0,
   0,
   0,
   3,
   1,
   'Admin',
   sysdate,3,sysdate,sysdate);
  
    insert into tmpdata.PlannedPriceAppDetail150128_2
      (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             s_PlannedPriceApp.Currval,
             t.sparepartid,
             (select code
                from sparepart
               where t.sparepartid = sparepart.id),
             (select name
                from sparepart
               where t.sparepartid = sparepart.id),t.plannedprice,0,0,0
        from tmpdata.stdprice150127_1 t where t.partssalescategoryid=(select id from partssalescategory where name='欧曼');
   
insert into tmpdata.PlannedPriceApp150128_2
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,
   RecordStatus,
   PlannedExecutionTime,
   ActualExecutionTime)
values
  (s_PlannedPriceApp.Nextval,
   (select id from company where code = '2450'),
   '2450',
   (select name from company where code = '2450'),
   (select id from partssalescategory where name = '欧曼保外'),
   '欧曼保外',
   'PPA2450201501282002',
   0,
   0,
   0,
   3,
   1,
   'Admin',
   sysdate,
   3,
   sysdate,
   sysdate);
     insert into tmpdata.PlannedPriceAppDetail150128_2
      (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             s_PlannedPriceApp.Currval,
             t.sparepartid,
             (select code
                from sparepart
               where t.sparepartid = sparepart.id),
             (select name
                from sparepart
               where t.sparepartid = sparepart.id),t.plannedprice,0,0,0
        from tmpdata.stdprice150127_1 t where t.partssalescategoryid=(select id from partssalescategory where name='欧曼保外');
   
   
   
insert into  PlannedPriceApp select * from  tmpdata.PlannedPriceApp150128_2;
insert into  plannedpriceappdetail select * from tmpdata.PlannedPriceAppDetail150128_2;


update  tmpdata.om_tstores150121 t
set t .newstdprice=()
 where t.qty > 0
 and exists (select 1 from warehouse where warehouse.code = t.newwarehousecode)
   and exists (select warehousearea.id
          from warehousearea
        /* inner join warehousearea parent
        on parent.id = warehousearea.parentid*/
         where warehouse.id = warehousearea.warehouseid
           and warehousearea.code = t.locationcode
              --  and parent.code = t.areacode
           and warehousearea.areakind = 3)
   and exists
 (select 1 from sparepart where sparepart.code = t.newpartcode) and nvl(t.newstdprice,0)<>nvl(t.oldstdprice,0);
