create table tmpdata.om_personsalescenterlink150127 ( loginid varchar2(100),name varchar2(100),brand varchar2(100));
create table tmpdata.personsalescenterlink150127  as select * from personsalescenterlink where rownum<1;
insert into tmpdata.personsalescenterlink150127
  (id,
   companyid,
   personid,
   partssalescategoryid,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_personsalescenterlink.nextval,
         (select id from company where code = '2450'),
         (select id
            from security.personnel t
           where t.loginid = v.loginid
             and t.enterpriseid = 2203),
         (select id
            from partssalescategory
           where partssalescategory.name = v.brand),
         1,
         1,
         'Admin',
         sysdate
    from tmpdata.om_personsalescenterlink150127 v
   where not exists (select id
            from security.personnel t
           where t.loginid = v.loginid
             and t.enterpriseid = 2203)
     and exists (select id
            from partssalescategory
           where partssalescategory.name = v.brand);
           
