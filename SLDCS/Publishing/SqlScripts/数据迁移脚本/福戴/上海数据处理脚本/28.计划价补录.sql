select distinct a.code,a.saleprice,brandname from tmpdata.om_purchasesaleprice150128 a



create table tmpdata.PartsSalesPrice150128 as select * from PartsSalesPrice where rownum<1; 
--123647
insert into tmpdata.PartsSalesPrice150128
  (id,
   branchid,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   sparepartid,
   sparepartcode,
   sparepartname,
   ifclaim,
   salesprice,
   pricetype,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_PartsSalesPrice.Nextval,
         (select id from company where code='2450'),
       (select id
            from partssalescategory
           where partssalescategory.name =decode (brandname,'保内','欧曼','保外','欧曼保外')) ,
         (select code
            from partssalescategory
           where partssalescategory.name =decode (brandname,'保内','欧曼','保外','欧曼保外')),
         (select name
            from partssalescategory
           where partssalescategory.name =decode (brandname,'保内','欧曼','保外','欧曼保外')),
         v.id,
         v.code,
         v.name,
         1,
         nvl(t.saleprice, 0),
         1,
         1,
         1,
         'Admin',
         sysdate
    from (select distinct a.code,a.saleprice,brandname from tmpdata.om_purchasesaleprice150128 a ) t
   inner join sparepart v
      on t.code = v.code
      and not exists(select 1 from PartsSalesPrice  c  
      inner join partssalescategory b on b.id=c.partssalescategoryid where  b.name=decode (brandname,'保内','欧曼','保外','欧曼保外') and v.id=c.sparepartid);
  /* inner join yxdcs.partsbranch s
      on s.partid = v.id*/
 /*select sparepartid,partssalescategoryid from tmpdata.PartsSalesPrice150128 group by sparepartid,partssalescategoryid having count(sparepartid)>1
 delete from PartsSalesPrice where exists(select 1 from tmpdata.PartsSalesPrice150128 where PartsSalesPrice150128.id=PartsSalesPrice.id)*/
insert into PartsSalesPrice select * from tmpdata.PartsSalesPrice150128;


create table tmpdata.PartsSalesPriceHistory150128 as select * from PartsSalesPriceHistory where rownum<1;

insert into tmpdata.PartsSalesPriceHistory150128 (id,
   branchid,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   sparepartid,
   sparepartcode,
   sparepartname,
   ifclaim,
   salesprice,
   pricetype,
   status,
   creatorid,
   creatorname,
   createtime)
   select s_PartsSalesPriceHistory.Nextval,branchid,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   sparepartid,
   sparepartcode,
   sparepartname,
   ifclaim,
   salesprice,
   pricetype,
   status,1,'Admin',sysdate from tmpdata.PartsSalesPrice150128 t;
   insert into PartsSalesPriceHistory select * from tmpdata.PartsSalesPriceHistory150128;
  -- delete from PartsSalesPriceHistory where exists(select * from tmpdata.PartsSalesPriceHistory150128 where PartsSalesPriceHistory150128.id=PartsSalesPriceHistory.id)
   
 create table tmpdata.PartsSalesPriceChange150128 as select * From PartsSalesPriceChange where rownum<1;
   create table tmpdata.PartsSalesPriceChangeDt150128 as select * from PartsSalesPriceChangeDetail where rownum<1;

insert into tmpdata.PartsSalesPriceChange150128
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,
   status)
values
  (s_PartsSalesPriceChange.Nextval,
   (select id from company where code = '2450'),
   'PSPC201501281001',
   (select id from partssalescategory where name = '欧曼保外'),
   '831000',
   '欧曼保外',
   
   1,
   1,
   'Admin',
   sysdate,
   3);

insert into tmpdata.PartsSalesPriceChangeDt150128
  (id,
   parentid,
   sparepartid,
   sparepartcode,
   sparepartname,
   salesprice,
   pricetype)
  select s_PartsSalesPriceChangeDetail.Nextval,
         s_PartsSalesPriceChange.Currval,
         t.sparepartid,
         t.sparepartcode,
         t.sparepartname,
         t.salesprice,
         1
    from tmpdata.PartsSalesPrice150128 t
   where t.partssalescategoryid =
         (select id from partssalescategory where name = '欧曼保外');
         
         
insert into tmpdata.PartsSalesPriceChange150128
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,
   status)
values
  (s_PartsSalesPriceChange.Nextval,
   (select id from company where code = '2450'),
   'PSPC201501281002',
   (select id from partssalescategory where name = '欧曼'),
   '830601',
   '欧曼',
   
   1,
   1,
   'Admin',
   sysdate,
   3);

insert into tmpdata.PartsSalesPriceChangeDt150128
  (id,
   parentid,
   sparepartid,
   sparepartcode,
   sparepartname,
   salesprice,
   pricetype)
  select s_PartsSalesPriceChangeDetail.Nextval,
         s_PartsSalesPriceChange.Currval,
         t.sparepartid,
         t.sparepartcode,
         t.sparepartname,
         t.salesprice,
         1
    from tmpdata.PartsSalesPrice150128 t
   where t.partssalescategoryid =
         (select id from partssalescategory where name = '欧曼');
insert into PartsSalesPriceChange
  select * from tmpdata.PartsSalesPriceChange150128;
insert into PartsSalesPriceChangeDetail
  select * from tmpdata.PartsSalesPriceChangeDt150128;

/* 
 delete from PartsSalesPriceChange
  where exists
  (select 1
           from tmpdata.PartsSalesPriceChange150128
          where PartsSalesPriceChange150128.id = PartsSalesPriceChange.id);
 
 delete from PartsSalesPriceChangeDetail
  where exists (select 1
           from tmpdata.PartsSalesPriceChangeDt150128
          where PartsSalesPriceChangeDt150128.id =
                PartsSalesPriceChangeDetail.id);*/
    
create table tmpdata.PartsPurchasePricing150128 as select * from PartsPurchasePricing where rownum<1;      
--114282
insert into tmpdata.PartsPurchasePricing150128
  (id,
   branchid,
   partid,
   partssupplierid,
   partssalescategoryid,
   partssalescategoryname,
   validfrom,
   validto,
   pricetype,
   purchaseprice,
   status)
  select s_PartsPurchasePricing.Nextval,
         (select id from company where code='2450'),
         v.id,
         w.id,
         (select id
            from partssalescategory
           where partssalescategory.name =decode (brandname,'保内','欧曼','保外','欧曼保外')) ,
          decode (brandname,'保内','欧曼','保外','欧曼保外') ,
         to_date('2015-1-1', 'yyyy-mm-dd'),
         to_date('2015-12-31', 'yyyy-mm-dd'),
         1,
         nvl(t.purchaseprice, 0),
         2
    from (select distinct a.purchaseprice,a.newsuppliercode,a.code,a.brandname from tmpdata.om_purchasesaleprice150128 a  )t
   inner join sparepart v
      on t.code = v.code
   inner join partssupplier w
      on w.code =t.newsuppliercode 
      and not exists(select 1 from PartsPurchasePricing  c  
      inner join partssalescategory b on b.id=c.partssalescategoryid where  b.name=decode (brandname,'保内','欧曼','保外','欧曼保外') and v.id=c.partid and c.partssupplierid
      =w.id) ;
select * from sparepart where id=298903 
select * from tmpdata.om_purchasesaleprice150128 t where t.newcode='R300S50-3502007A1282'
        select distinct a.purchaseprice,a.newsuppliercode,a.newcode,a.brandname from tmpdata.om_purchasesaleprice150128 a  where newcode='R300S50-3502007A1282'
       insert into PartsPurchasePricing select * from  tmpdata.PartsPurchasePricing150128;
select PARTID,
       PARTSSUPPLIERID,
       PARTSSALESCATEGORYID,
       VALIDFROM,
       VALIDTO,
       STATUS
  from tmpdata.PartsPurchasePricing150128
 group by PARTID,
       PARTSSUPPLIERID,
       PARTSSALESCATEGORYID,
       VALIDFROM,
       VALIDTO,
       STATUS having count(PARTID)>1
create table tmpdata.PartsPurchasePricingC150128 as select * from PartsPurchasePricingChange where rownum<1; 
create table tmpdata.PartsPurchasePricingD150128 as select * from PartsPurchasePricingDetail where rownum<1;
insert into tmpdata.PartsPurchasePricingC150128
  (id,
   code,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   creatorid,
   creatorname,
   createtime,
   status)
values
  (s_PartsPurchasePricingChange.Nextval,
   'PPPC201501281001',
   (select id from company where code = '2450'),
   (select id from partssalescategory where name = '欧曼'),
   '欧曼',
   1,
   'Admin',
   sysdate,
   2);

insert into tmpdata.PartsPurchasePricingD150128
  (id,
   parentid,
   partid,
   partcode,
   partname,
   supplierid,
   suppliercode,
   suppliername,
   pricetype,
   price,
   VALIDFROM,
   validto)
  select s_PartsPurchasePricingDetail.Nextval,
         s_PartsPurchasePricingChange.Currval,
         t.partid,
         (select code from sparepart where id = t.partid),
         (select name from sparepart where id = t.partid),
         t.partssupplierid,
         (select code from partssupplier where id = t.partssupplierid),
         (select name from partssupplier where id = t.partssupplierid),
         1,
         t.purchaseprice,
         to_date('2015-1-1', 'yyyy-mm-dd'),
         to_date('2015-12-31', 'yyyy-mm-dd')
    from tmpdata.PartsPurchasePricing150128 t
   where t.partssalescategoryid =
         (select id from partssalescategory where name = '欧曼');
insert into tmpdata.PartsPurchasePricingC150128
  (id,
   code,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   creatorid,
   creatorname,
   createtime,
   status)
values
  (s_PartsPurchasePricingChange.Nextval,
   'PPPC201501281002',
   (select id from company where code = '2450'),
   (select id from partssalescategory where name = '欧曼保外'),
   '欧曼保外',
   1,
   'Admin',
   sysdate,
   2);

insert into tmpdata.PartsPurchasePricingD150128
  (id,
   parentid,
   partid,
   partcode,
   partname,
   supplierid,
   suppliercode,
   suppliername,
   pricetype,
   price,
   VALIDFROM,
   validto)
  select s_PartsPurchasePricingDetail.Nextval,
         s_PartsPurchasePricingChange.Currval,
         t.partid,
         (select code from sparepart where id = t.partid),
         (select name from sparepart where id = t.partid),
         t.partssupplierid,
         (select code from partssupplier where id = t.partssupplierid),
         (select name from partssupplier where id = t.partssupplierid),
         1,
         t.purchaseprice,
         to_date('2015-1-1', 'yyyy-mm-dd'),
         to_date('2015-12-31', 'yyyy-mm-dd')
    from tmpdata.PartsPurchasePricing150128 t
   where t.partssalescategoryid =
         (select id from partssalescategory where name = '欧曼保外');
    
 
    insert into PartsPurchasePricingChange select * from tmpdata.PartsPurchasePricingC150128;
    insert into PartsPurchasePricingDetail select * from tmpdata.PartsPurchasePricingD150128;




create table tmpdata.stdprice150128_1 as select * from partsplannedprice where rownum<1;
--123647
insert into tmpdata.stdprice150128_1
  (id,
   ownercompanyid,
   ownercompanytype,
   partssalescategoryid,
   partssalescategoryname,
   sparepartid,
   plannedprice,
   creatorid,
   creatorname,
   createtime)
select s_partsplannedprice.nextval,
 (select id from company where code='2450'),
 1,
(select id
            from partssalescategory
           where partssalescategory.name =decode (brandname,'保内','欧曼','保外','欧曼保外')) ,
    decode (brandname,'保内','欧曼','保外','欧曼保外'),
 s.id,
 nvl(t.stprice,0),
 1,
 'Admin',
 sysdate  from (select distinct a.code,a.stprice,brandname from tmpdata.om_purchasesaleprice150128 a) t
 inner join sparepart s
    on s.code = t.code
    and not exists(select 1 from partsplannedprice  c  
      inner join partssalescategory b on b.id=c.partssalescategoryid where  b.name=decode (brandname,'保内','欧曼','保外','欧曼保外') and s.id=c.sparepartid);
   
 
 
 insert into partsplannedprice select * from  tmpdata.stdprice150128_1;
 create table tmpdata.PlannedPriceApp150128 as select * from PlannedPriceApp where rownum<1;
create table tmpdata.PlannedPriceAppDetail150128 as select * from PlannedPriceAppDetail where rownum<1;

insert into tmpdata.PlannedPriceApp150128
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,RecordStatus,PlannedExecutionTime,ActualExecutionTime)
values
  (s_PlannedPriceApp.Nextval,
   (select id from company where code='2450'),
   '2450',
   (select name from company where code='2450'), (select id from partssalescategory where name='欧曼'),
   '欧曼',
   'PPA2450201501281001',
   0,
   0,
   0,
   3,
   1,
   'Admin',
   sysdate,3,sysdate,sysdate);
  
    insert into tmpdata.PlannedPriceAppDetail150128
      (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             s_PlannedPriceApp.Currval,
             t.sparepartid,
             (select code
                from sparepart
               where t.sparepartid = sparepart.id),
             (select name
                from sparepart
               where t.sparepartid = sparepart.id),t.plannedprice,0,0,0
        from tmpdata.stdprice150128_1 t where t.partssalescategoryid=(select id from partssalescategory where name='欧曼');
   
insert into tmpdata.PlannedPriceApp150128
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,
   RecordStatus,
   PlannedExecutionTime,
   ActualExecutionTime)
values
  (s_PlannedPriceApp.Nextval,
   (select id from company where code = '2450'),
   '2450',
   (select name from company where code = '2450'),
   (select id from partssalescategory where name = '欧曼保外'),
   '欧曼保外',
   'PPA2450201501281002',
   0,
   0,
   0,
   3,
   1,
   'Admin',
   sysdate,
   3,
   sysdate,
   sysdate);
     insert into tmpdata.PlannedPriceAppDetail150128
      (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             s_PlannedPriceApp.Currval,
             t.sparepartid,
             (select code
                from sparepart
               where t.sparepartid = sparepart.id),
             (select name
                from sparepart
               where t.sparepartid = sparepart.id),t.plannedprice,0,0,0
        from tmpdata.stdprice150128_1 t where t.partssalescategoryid=(select id from partssalescategory where name='欧曼保外');
   
   
   
insert into  PlannedPriceApp select * from  tmpdata.PlannedPriceApp150128;
insert into  plannedpriceappdetail select * from tmpdata.PlannedPriceAppDetail150128;

4  61  831000  欧曼保外  2207  2450  北京福田戴姆勒汽车有限公司  0  1  1    4750  数据处理  2015-1-16 15:45:08              16-JAN-15 03.50.50.646057 PM  
5  62  830601  欧曼  2207  2450  北京福田戴姆勒汽车有限公司  0  0  1    4750  数据处理  2015-1-16 15:45:34              16-JAN-15 03.51.17.254530 PM  



create table tmpdata.partsretailguideprice150128 as select * from partsretailguideprice where rownum<1; 
--123647
insert into tmpdata.partsretailguideprice150128
  (id,
   branchid,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   sparepartid,
   sparepartcode,
   sparepartname,
   retailguideprice,
   creatorid,
   creatorname,
   createtime,status)
  select s_partsretailguideprice.Nextval,
         (select id from company where code='2450'),
        (select id
            from partssalescategory
           where partssalescategory.name =decode (brandname,'保内','欧曼','保外','欧曼保外')) ,
           (select code
            from partssalescategory
           where partssalescategory.name =decode (brandname,'保内','欧曼','保外','欧曼保外')),
   decode (brandname,'保内','欧曼','保外','欧曼保外'),
         v.id,
         v.code,
         v.name,
         nvl(t.retailprice, 0),
         1,
         'Admin',
         sysdate,1
    from (select distinct a.code,a.retailprice,brandname from tmpdata.om_purchasesaleprice150128 a) t
   inner join sparepart v
      on t.code = v.code
          and not exists(select 1 from partsretailguideprice  c  
      inner join partssalescategory b on b.id=c.partssalescategoryid where  b.name=decode (brandname,'保内','欧曼','保外','欧曼保外') and v.id=c.sparepartid);
   
insert into partsretailguideprice select * from tmpdata.partsretailguideprice150128;


create table tmpdata.PRetailGuidePriceHistory150128 as select * from PartsRetailGuidePriceHistory where rownum<1;

insert into tmpdata.PRetailGuidePriceHistory150128
  (id,
   branchid,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   sparepartid,
   sparepartcode,
   sparepartname,
   retailguideprice,
   validationtime,
   expiretime,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_PartsRetailGuidePriceHistory.Nextval,
         branchid,
         partssalescategoryid,
         partssalescategorycode,
         partssalescategoryname,
         sparepartid,
         sparepartcode,
         sparepartname,
         retailguideprice,
         validationtime,
         expiretime,
         status,
         1,
         'Admin',
         sysdate
    from tmpdata.partsretailguideprice150128 t;
insert into PartsRetailGuidePriceHistory
  select * from tmpdata.PRetailGuidePriceHistory150128;
  
   create table tmpdata.PSalesPriceChange150128_1rp as select * From PartsSalesPriceChange where rownum<1;
   create table tmpdata.PSalesPriceChangeDt150128_1rp as select * from PartsSalesPriceChangeDetail where rownum<1;
   insert into tmpdata.PSalesPriceChange150128_1rp
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,status)
  values( s_PartsSalesPriceChange.Nextval,
         (select id from company where code='2450'),
         'PSPC201501282001',
         (select id from partssalescategory where name='欧曼'),
         (select code from partssalescategory where name='欧曼'),
         '欧曼',
         
         1,
         1,
         'Admin',
         sysdate ,1);
         
         insert into tmpdata.PSalesPriceChangeDt150128_1rp
           (id,
            parentid,
            sparepartid,
            sparepartcode,
            sparepartname,
            salesprice,
            RETAILGUIDEPRICE ,
            pricetype)
           select s_PartsSalesPriceChangeDetail.Nextval,
                  s_PartsSalesPriceChange.Currval,
                  t.sparepartid,
                  t.sparepartcode,
                  t.sparepartname,0,
                  t.retailguideprice,
                  1
             from tmpdata.partsretailguideprice150128 t
            where t.partssalescategoryid = (select id from partssalescategory where name='欧曼');
            
           insert into tmpdata.PSalesPriceChange150128_1rp
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,status)
  values( s_PartsSalesPriceChange.Nextval,
         (select id from company where code='2450'),
         'PSPC201501282002',
         (select id from partssalescategory where name='欧曼保外'),
         (select code from partssalescategory where name='欧曼保外'),
         '欧曼保外',
         
         1,
         1,
         'Admin',
         sysdate ,1);
         
         insert into tmpdata.PSalesPriceChangeDt150128_1rp
           (id,
            parentid,
            sparepartid,
            sparepartcode,
            sparepartname,
            salesprice,
            RETAILGUIDEPRICE ,
            pricetype)
           select s_PartsSalesPriceChangeDetail.Nextval,
                  s_PartsSalesPriceChange.Currval,
                  t.sparepartid,
                  t.sparepartcode,
                  t.sparepartname,0,
                  t.retailguideprice,
                  1
             from tmpdata.partsretailguideprice150128 t
            where t.partssalescategoryid = (select id from partssalescategory where name='欧曼保外');
            
            insert into PartsSalesPriceChange select * from tmpdata.PSalesPriceChange150128_1rp;
            insert into PartsSalesPriceChangeDetail select * from  tmpdata.PSalesPriceChangeDt150128_1rp;
