select *
  from (select (select distinct dealerserviceinfo.businesscode
                  from dealerserviceinfo
                 where dealerserviceinfo.dealerid = company.id
                   and dealerserviceinfo.branchid = 2203) businesscode
          from company
         where company.type in (2, 7)) t
 where businesscode is not null
   and not exists (select 1
          from tmpdata.OM_DealerWarehouse150127 v
         where v.代理库编号 = t.businesscode)
create table tmpdata.dealerpartsstock150128 as 
select t.*, cast('保内' as varchar2(10)) brand,cast(null as varchar2(100)) newdealercode,cast(null as varchar2(100)) newpartcode from 
tmpdata.tstationstores_omfw t;
 
insert into tmpdata.dealerpartsstock150128
select t.*, cast('保外' as varchar2(10)) brand,cast(null as varchar2(100)) newdealercode,cast(null as varchar2(100)) newpartcode from 
tmpdata.tstationstores_bwfw t;

update  tmpdata.dealerpartsstock150128 t set t.newdealercode=(select distinct company.code from dealerserviceinfo
inner join company on company.id=dealerserviceinfo .dealerid where dealerserviceinfo.businesscode=t.stationcode and dealerserviceinfo.branchid = 2203);

update tmpdata.dealerpartsstock150128  set newpartcode=null;
 update tmpdata.dealerpartsstock150128
   set newpartcode =
       (select newcode
           from tmpdata.om_newoldpartrel150121
          where om_newoldpartrel150121.oldcode = dealerpartsstock150128.MATERIALCODE
            and dealerpartsstock150128.brand = om_newoldpartrel150121.brand);
update tmpdata.dealerpartsstock150128 set newpartcode=MATERIALCODE
where exists(select 1 from sparepart where sparepart.code=MATERIALCODE) and newpartcode is null;
commit;

--1274637
select *from tmpdata.dealerpartsstock150128 t  where t.newpartcode is null
create table tmpdata.dealerpartsstock150128_1 as select * from DealerPartsStock where rownum<1;
insert into tmpdata.dealerpartsstock150128_1
  (id,
   dealerid,
   dealercode,
   dealername,
   subdealerid,
   branchid,
   salescategoryid,
   salescategoryname,
   sparepartid,
   sparepartcode,
   quantity,
   creatorid,
   creatorname,
   createtime)
  select s_DealerPartsStock.Nextval,
         (select id from company where company.code = t.newdealercode),
         t.newdealercode,
         (select id from company where company.code = t.newdealercode),
         -1,
         2203,
         (select id
            from partssalescategory
           where name = decode(t.brand, '保内', '欧曼', '保外', '欧曼保外')),
         decode(t.brand, '保内', '欧曼', '保外', '欧曼保外'),
         (select id from sparepart where sparepart.code = t.newpartcode),
         t.newpartcode,
         t.sumqty,
         1,
         'Admin',
         sysdate
    from tmpdata.dealerpartsstock150128 t
   where t.newpartcode is not null;
   
   
create index tmpdata.dealerpartsstock150128_1_idx on TMPDATA.dealerpartsstock150128_1 (dealerid, salescategoryid, sparepartid);
update tmpdata.dealerpartsstock150128_1 v
   set v.quantity =
       (select sum(quantity)
          from tmpdata.dealerpartsstock150128_1 a
         where a.dealerid = v.dealerid
           and a.salescategoryid = v.salescategoryid
           and a.sparepartid = v.sparepartid)
 where exists (select *
          from (select dealerid, salescategoryid, sparepartid
                  from tmpdata.dealerpartsstock150128_1 t
                  
                 group by dealerid, salescategoryid, sparepartid
                having count(sparepartid) > 1) t
         where t.dealerid = v.dealerid
           and t.salescategoryid = v.salescategoryid
           and t.sparepartid = v.sparepartid);
    
delete tmpdata.dealerpartsstock150128_1 
 where dealerpartsstock150128_1.rowid not in
       (select min(dealerpartsstock150128_1.rowid)
          from tmpdata.dealerpartsstock150128_1  
         group by dealerid, salescategoryid, sparepartid)  ;

select from  tmpdata.om_tstores150121 t where t.newwarehousecode is not null and t.newpartcode is not null
group by t.newwarehousecode,t.newpartcode
insert into DealerPartsStock select * from tmpdata.dealerpartsstock150128_1;

--54560
create table tmpdata.company150128 as select * from company
delete from DealerPartsStock where exists(select 1 from company where company.id=DealerPartsStock.dealerid and company.type=7)
and exists(select * from tmpdata.dealerpartsstock150128_1 where dealerpartsstock150128_1.id=DealerPartsStock.id)

delete from tmpdata.dealerpartsstock150128_1 where 
exists(select 1 from tmpdata.company150128 where company150128.id=dealerpartsstock150128_1.dealerid and company150128.type=7)


 select * from DealerPartsStock where dealerid in (2645,242)
  select * from DealerPartsStock where dealerid =2645
  
  update DealerPartsStock  a set dealername=(select name from company where company.id=a.Dealerid)
  where exists(select 1  from tmpdata.dealerpartsstock150128_1 b where a.id=b.id)
select count(*) from tmpdata.dealerpartsstselect * from company where code='FT000011'
ock150128_1;
select * from company where type=10



select * from  tmpdata.dealerpartsstock150128 t
where not exists(select 1 from sparepart where sparepart.code=t.newpartcode)
