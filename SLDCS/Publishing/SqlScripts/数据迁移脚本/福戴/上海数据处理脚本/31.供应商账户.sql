create table tmpdata.gcsupplieracount150128
(客户编码  varchar2(100),
客户名称   varchar2(100),
配件账户余额 varchar2(100),
品牌 varchar2(100)
);


where b.id is null
create table tmpdata. supplieraccount150128 as select * from  supplieraccount where rownum<1;
---插入供应商账户 596rows
insert into  tmpdata. supplieraccount150128
( id,BuyerCompanyId,PartsSalesCategoryId,PartsSalesCategoryName,SupplierCompanyId,DueAmount,EstimatedDueAmount,PaymentLimit,Status,CreatorId,CreatorName,CreateTime
)
select s_supplieraccount.nextval,(select id from branch where code ='2230'),c.id,c.name,b.id,a.配件账户余额,0,0,1,null,'Admin',sysdate
from tmpdata.gcsupplieracount150128 a 
inner join partssupplier b on a.供应商编号 = b.code
inner join partssalescategory c on c.name = a.品牌;


insert into supplieraccount select * from tmpdata. supplieraccount150128;


create table tmpdata.SupplierOpenAccountApp150128 as select * from SupplierOpenAccountApp where rownum<1;
insert into tmpdata.SupplierOpenAccountApp150128
  (id,
   code,
   buyercompanyid,
   partssalescategoryid,
   partssalescategoryname,
   suppliercompanyid,
   accountinitialdeposit,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_SupplierOpenAccountApp.Nextval,
         'SOA' || b.code || '20150128' ||
         decode(length(rownum),
                1,
                '000' || rownum,
                2,
                '00' || rownum,
                3,
                '0' || rownum,
                4,
                rownum),
         2203,
         c.id,
         c.name,
         b.id,
         0,
         2,
         1,
         'Admin',
         sysdate
    from  partssupplier b  
      inner join BranchSupplierRelation a  on a.supplierid=b.id and a.branchid=2203
   inner join partssalescategory c
      on c.id = a.partssalescategoryid

insert into SupplierOpenAccountApp select * from tmpdata.SupplierOpenAccountApp150128;

---生成往来账
create table tmpdata.APayableHistoryDetail150128 as select * from   AccountPayableHistoryDetail where rownum<1;
insert into tmpdata.APayableHistoryDetail150128
  (id,
   supplieraccountid,
   changeamount,
   debit,
   credit,
   beforechangeamount,
   afterchangeamount,
   processdate,
   businesstype,
   sourceid,
   sourcecode,
   summary)
  select s_AccountPayableHistoryDetail.Nextval,
         d.id,
         a.配件账户余额,
         a.配件账户余额,
         0,
         0,
         a.配件账户余额，to_date(to_char(sysdate, 'yyyy-mm-dd'), 'yyyy-mm-dd'),
         1,
         e.id,
         e.code,
         '系统初始化导入'
    from tmpdata.gcsupplieracount150128 a
   inner join partssupplier b
      on a.供应商编号 = b.code
   inner join partssalescategory c
      on c.name = a.品牌
   inner join supplieraccount d
      on d.suppliercompanyid = b.id
     and d.partssalescategoryid = c.id
   inner join tmpdata.SupplierOpenAccountApp150128 e
      on e.suppliercompanyid = b.id
     and e.partssalescategoryid = c.id;

insert into AccountPayableHistoryDetail select * from tmpdata.APayableHistoryDetail150128;
