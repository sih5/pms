/*1. 根据来款分割单逐条变更账户金额：
  1.）查询客户账户（账户组Id，客户企业Id），没有记录提示，“客户xxx账户不存在”
  2.）账户金额增加来款分割单.金额，状态=已审核。
  3.)新增客户账户变更明细：
  客户账户Id=客户账户.Id；
  变更金额=来款分割单.金额；
  发生日期=当前日期（精确到时分秒）；
  业务类型=来款；
  账户变更单据Id=来款分割单.Id；
  账户变更单据编号=来款分割单.编号；
  账户变更摘要=来款分割单.摘要；
变更后金额=当前客户账户金额
  变更前金额=当前客户账户金额-来款分割单.金额
 贷方=来款分割单.金额
2. 更新来款单，状态=已分割，同时新增来款分割单。
*/
create table tmpdata.om_customeraccount150128(客户描述 varchar2(100),	欧曼保外 number(19,4)	,欧曼 number(19,4),	PMS对应FT码 varchar2(100))


create table tmpdata.customeraccount150128
( 客户编号 varchar2(100),  客户名称  varchar2(100), 客户类型 varchar2(100),  账户金额 number(19,2),  
可用额度 number(19,4),  锁定金额 number(19,4),  信用额度 number(19,4),   返利 number(19,4),  品牌编号 varchar2(100),  品牌名称 varchar2(100))

insert into tmpdata.customeraccount150128 (客户编号, 账户金额,品牌名称)
select a.pms对应ft码,a.欧曼保外,'欧曼保外' from tmpdata.om_customeraccount150128 a;

insert into tmpdata.customeraccount150128 (客户编号, 账户金额,品牌名称)
select a.pms对应ft码,a.欧曼,'欧曼' from tmpdata.om_customeraccount150128 a;


update tmpdata.customeraccount150128 set 客户类型='服务站' where 客户类型 is null;

update tmpdata.customeraccount150128 set 客户类型='代理库' where 客户类型 is null;


--update tmpdata.customeraccount141101 set 客户类型='供应商' where 客户类型 is null;

select distinct 品牌名称 from tmpdata.customeraccount141101;
update tmpdata.customeraccount141101 set 品牌名称='工程车(南区)'where 品牌名称='工程车南区';
select *
from tmpdata.customeraccount150128 t  --1495
inner join partssalescategory a on a.name=t.品牌名称 --1495
inner join company on t.客户编号=company.code
inner join salesunit d on d.partssalescategoryid=a.id and d.ownercompanyid=2203
inner join accountgroup c on c.id=d.accountgroupid
select * from tmpdata.customeraccount141101  where 客户编号 in('FT000525',
'FT005329',
'FT005213',
'FT005316',
'FT005209',
'FT005266')
create table tmpdata.customeraccount150128_1 as select * from customeraccount where rownum<1;
--1483
insert into tmpdata.customeraccount150128_1
  (id,
   accountgroupid,
   customercompanyid,
   accountbalance,
   shippedproductvalue,
   pendingamount,
   customercredenceamount,
   status,
   creatorid,
   creatorname,
   createtime)
select 
s_customeraccount.nextval,
c.id,
b.id,
t.账户金额,
0,0,0,1,1,'Admin',sysdate
from tmpdata.customeraccount150128 t
inner join partssalescategory a on a.name=t.品牌名称
--where not exists(select 1 from dealerserviceinfo b where b.businesscode=t.客户编号 and b.partssalescategoryid=a.id)
inner join company b on t.客户编号=b.code
inner join salesunit d on d.partssalescategoryid=a.id and d.ownercompanyid=2203
inner join accountgroup c on c.id=d.accountgroupid/*
where 客户编号 not in('FT000525',
'FT005329',
'FT005213',
'FT005316',
'FT005209',
'FT005266')*/;
insert into customeraccount select * from tmpdata.customeraccount150128_1;
select * from accountgroup where id=383

--delete from customeraccount where exists(select 1 from tmpdata.customeraccount141101_1 where customeraccount141101_1.id =customeraccount.id)
create table tmpdata.Customeropenaccountapp150128 as select * from CustomerOpenAccountApp where rownum<1;
insert into tmpdata.Customeropenaccountapp150128
  (id,
   code,
   accountgroupid,
   customercompanyid,
   status,
   remark,
   creatorid,
   creatorname,
   createtime,
   accountinitialdeposit)
  select s_CustomerOpenAccountApp.Nextval,
         'COA' ||
         (SELECT C.CODE FROM ACCOUNTGROUP C WHERE C.ID = T.accountgroupid) ||
         '20150128' || decode(length(rownum),
                              1,
                              '000' || rownum,
                              2,
                              '00' || rownum,
                              3,
                              '0' || rownum,
                              4,
                              rownum),
         T.accountgroupid,
         T.customercompanyid,
         2,
         '资金台账初始化导入',
         1,
         'Admin',
         sysdate,
         t.accountbalance
    from tmpdata.customeraccount150128_1 T;
    insert into CustomerOpenAccountApp select * from tmpdata.Customeropenaccountapp150128;
/*
create table tmpdata.PaymentBill150128 as select * from PaymentBill where rownum<1; 
insert into tmpdata.PaymentBill150128
  (id,
   code,
   salescompanyid,
   accountgroupid,
   customercompanyid,
   amount,
   paymentmethod,
   status,
   creatorid,
   creatorname,
   createtime,Summary)
  select s_PaymentBill.Nextval,
         'PPB245020150128' || decode(length(rownum),
                                     1,
                                     '000' || rownum,
                                     2,
                                     '00' || rownum,
                                     3,
                                     '0' || rownum,
                                     4,
                                     rownum),
         2203,
         t.accountgroupid,
         t.customercompanyid,
         t.accountbalance,
         1,
         1,
         1,
         'Admin',
         sysdate,'资金台账初始化导入'
    from tmpdata.customeraccount150128_1 t;
                                
    insert into PaymentBill select * from tmpdata.PaymentBill150128;
    
    
delete from PaymentBill where exists(select 1 from tmpdata.PaymentBill141101 where PaymentBill141101.id =PaymentBill.id)
    create table tmpdata.PaymentBeneficiaryList150128 as select * from PaymentBeneficiaryList where rownum<1;
    
insert into tmpdata.PaymentBeneficiaryList150128
  (id,
   code,
   salescompanyid,
   accountgroupid,
   customercompanyid,
   paymentmethod,
   amount,
   paymentbillid,
   paymentbillcode,
   creatorid,
   creatorname,
   createtime,
   status,Summary)
  select s_PaymentBeneficiaryList.Nextval,
         'PPBL245020150128' || decode(length(rownum),
                                      1,
                                      '000' || rownum,
                                      2,
                                      '00' || rownum,
                                      3,
                                      '0' || rownum,
                                      4,
                                      rownum),
         t.salescompanyid,
         t.accountgroupid,
         t.customercompanyid,
         t.paymentmethod,t.amount,
         t.id,
         t.code,
         1,
         'Admin',
         sysdate,
         1,Summary
    from tmpdata.PaymentBill150128 t;

insert into PaymentBeneficiaryList select * from tmpdata.PaymentBeneficiaryList150128;*/

delete from PaymentBeneficiaryList where exists(select 1 from tmpdata.PaymentBeneficiaryList141101 where PaymentBeneficiaryList141101.id =PaymentBeneficiaryList.id)

create table tmpdata.CustomerAccountHisDetail150128 as select * from CustomerAccountHisDetail where rownum<1;
insert into tmpdata.CustomerAccountHisDetail150128
  (id,
   customeraccountid,
   changeamount,
   debit,
   credit,
   beforechangeamount,
   afterchangeamount,
   ProcessDate,
   businesstype,
   sourceid,
   sourcecode,Summary)
  select s_CustomerAccountHisDetail.Nextval,
         (select id
            from tmpdata.customeraccount150128_1 v
           where t.accountgroupid = v.accountgroupid
             and t.customercompanyid = v.customercompanyid),
         t.accountinitialdeposit,
         0,
         t.accountinitialdeposit,
         0,
         t.accountinitialdeposit,
         t.createtime,
         12,
         t.id,
         t.code,remark
    from tmpdata.Customeropenaccountapp150128 t;
insert into CustomerAccountHisDetail select * from tmpdata.CustomerAccountHisDetail150128;

/*
create table tmpdata.PartsRebateAccount141101_1 as select * from PartsRebateAccount where rownum<1;
insert into tmpdata.PartsRebateAccount141101_1 
  (id,
   Branchid,
   Accountgroupid,
   Customercompanyid,
   Customercompanycode,
   Customercompanyname,
   Accountbalanceamount,
   Status,
   Creatorid,
   Creatorname,
   Createtime)
  select s_PartsRebateAccount.Nextval,
         8481,
         t.accountgroupid,
         t.customercompanyid,
         (select code from company where id = t.customercompanyid),
         (select name from company where id = t.customercompanyid),t.customercredenceamount,1,1,'Admin',sysdate
    from tmpdata.customeraccount141029_1 t;

insert into PartsRebateAccount select * from  tmpdata.PartsRebateAccount141101_1;*/

