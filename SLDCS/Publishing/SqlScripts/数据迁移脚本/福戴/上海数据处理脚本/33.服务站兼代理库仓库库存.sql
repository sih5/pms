 create table tmpdata.om_tstoresdealer150121 as --动态
select t.locationcode,
       t.areacode,
       t.partcode,
       t.partname,
       t.code warehousecode,
       t.name warehousename,
       t.qty,
       cast('保内' as varchar2(10)) brand,
       cast(null as varchar2(100)) newwarehousecode,
       cast(null as varchar2(100)) newpartcode,
       cast(null as varchar2(100)) newagentcode,
       t.storagecompanycode,
       t.storagecompanyname,cast(null as number(19, 4)) oldsaleprice,
       cast(null as number(19, 4)) newsaleprice
  from tmpdata.tstores_ombnfw t where rownum<1;
 
SELECT * FROM tmpdata.om_tstoresdealer150121 T WHERE T.STORAGECOMPANYCODE='ANH00092'


    create index TMPDATA.om_tstoresdealer150121IDX on TMPDATA.om_tstoresdealer150121  (BRAND, PARTCODE);

select * from tmpdata.om_tstoresdealer150121 t where t.newagentcode is not null

update tmpdata.om_tstoresdealer150121 t
   set newwarehousecode =decode(t.brand,'保内','BN'||t.storagecompanycode,'保外','BW'||t.storagecompanycode)
       ,newagentcode =
             (select distinct company.code
                from dealerserviceinfo inner join company on company.id=dealerserviceinfo.dealerid
               where dealerserviceinfo.businesscode =
                     t.storagecompanycode
                 and dealerserviceinfo.branchid = 2203 and dealerserviceinfo.status<>99);
           select branchid,businesscode from dealerserviceinfo where branchid=2203 group by branchid,businesscode having count (businesscode)>1
           
           select count(*) from tmpdata.om_tstoresdlk150121  where newwarehousecode is not null and newagentcode is not null;

           select count(*) from tmpdata.om_tstoresdlk150121  where newwarehousecode is not null or newagentcode is not null;
           
 update tmpdata.om_tstoresdealer150121  set newpartcode=null;
 update tmpdata.om_tstoresdealer150121
   set newpartcode =
       (select newcode
           from tmpdata.om_newoldpartrel150121
          where om_newoldpartrel150121.oldcode = om_tstoresdealer150121.partcode
            and om_tstoresdealer150121.brand = om_newoldpartrel150121.brand);
update tmpdata.om_tstoresdealer150121 set newpartcode=partcode
where exists(select 1 from sparepart where sparepart.code=partcode) and newpartcode is null;

select * from tmpdata.om_tstoresdealer150121 t where t. newagentcode=''

select * from tmpdata.om_tstoresdealer150121 where storagecompanycode='ANH00092'

update tmpdata.om_tstoresdealer150121 t
   set oldsaleprice =
       (select distinct v.saleprice
          from tmpdata.om_purchasesaleprice150121 v
         where v.code = t.partcode
           and v.brandname = t.brand),
       newsaleprice =
       (select distinct v.saleprice
          from tmpdata.om_purchasesaleprice150121 v
         where v.code = t.newpartcode
           and v.brandname = t.brand);
  select count(*) from  tmpdata.om_tstoresdealer150121  where newpartcode is not null;
             
          
create table tmpdata.partsstockdealer150121 as select * from partsstock where rownum<1;
 --84910
insert into tmpdata.partsstockdealer150121
  (id,
   warehouseid,
   storagecompanyid,
   storagecompanytype,
   branchid,
   warehouseareaid,
   warehouseareacategoryid,
   partid,
   quantity,
   creatorid,
   creatorname,
   createtime)
 select s_partsstock.nextval,
        warehouse.id,
        (select id from company where code=t.newagentcode),
       (select company.type from company where code=t.newagentcode)  ,
        (select id from company where code='2450'),
        (select warehousearea.id
           from warehousearea        
          inner join warehousearea parent
             on parent.id = warehousearea.parentid
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                  and parent.code = t.areacode
                  and warehousearea.areakind = 3),
        89,
        sparepart.id,       
        t.qty,
        1,
        'Admin',
        sysdate
   from tmpdata.om_tstoresdealer150121  t
   inner join warehouse on warehouse.code=t.newwarehousecode
   inner join sparepart on sparepart.code = t.newpartcode
  where exists (select warehousearea.id
           from warehousearea        
          inner join warehousearea parent
             on parent.id = warehousearea.parentid
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                  and parent.code = t.areacode
                  and warehousearea.areakind = 3) and t.newagentcode is not null
                  and exists(select id from company where code=t.newagentcode) and t.brand='保外';
         
--229320
insert into tmpdata.partsstockdealer150121
  (id,
   warehouseid,
   storagecompanyid,
   storagecompanytype,
   branchid,
   warehouseareaid,
   warehouseareacategoryid,
   partid,
   quantity,
   creatorid,
   creatorname,
   createtime)
 select s_partsstock.nextval,
        warehouse.id,
        (select id from company where code=t.newagentcode),
       (select company.type from company where code=t.newagentcode)  ,
        (select id from company where code='2450'),
        (select warehousearea.id
           from warehousearea        
          inner join warehousearea parent
             on parent.id = warehousearea.parentid
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                  and parent.code = t.areacode
                  and warehousearea.areakind = 3),
        89,
        sparepart.id,       
        t.qty,
        1,
        'Admin',
        sysdate
   from tmpdata.om_tstoresdealer150121  t
   inner join warehouse on warehouse.code=t.newwarehousecode
   inner join sparepart on sparepart.code = t.newpartcode
  where exists (select warehousearea.id
           from warehousearea        
          inner join warehousearea parent
             on parent.id = warehousearea.parentid
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                  and parent.code = t.areacode
                  and warehousearea.areakind = 3) and t.newagentcode is not null
                  and exists(select id from company where code=t.newagentcode) and t.brand='保内' and nvl(oldsaleprice,0)<=nvl(newsaleprice,0);
                                  
         
create index tmpdata.partsstockdealer150121_idx on TMPDATA.partsstockdealer150121 (warehouseid, warehouseareaid, partid);         
    update tmpdata.partsstockdealer150121 v
   set v.quantity =
       (select sum(quantity)
          from tmpdata.partsstockdealer150121 a
         where a.warehouseid = v.warehouseid
           and a.warehouseareaid = v.warehouseareaid
           and a.partid = v.partid)
 where exists (select *
          from (select t.warehouseid, t.warehouseareaid, t.partid
                  from tmpdata.partsstockdealer150121 t
                  
                 group by t.warehouseid, t.warehouseareaid, t.partid
                having count(partid) > 1) t
         where t.warehouseid = v.warehouseid
           and t.partid = v.partid
           and t.warehouseareaid = v.warehouseareaid);
    
delete tmpdata.partsstockdealer150121 
 where partsstockdealer150121.rowid not in
       (select min(partsstockdealer150121.rowid)
          from tmpdata.partsstockdealer150121 
         group by warehouseid, warehouseareaid, partid)  ;
         --299673
               insert into partsstock select * from tmpdata.partsstockdealer150121;
                        delete from 
delete from partsstock where exists(select * from tmpdata.partsstockdealer150121 where partsstockdealer150121.id=partsstock.id);

SELECT * FROM PARTSSTOCK A WHERE A.STORAGECOMPANYID=3482

select * from company where code='FT000067'
                  
                 select * from tmpdata.om_tstoresdlk150121 t
 inner join warehouse
    on warehouse.code = t.newwarehousecode
 where t.qty > 0
   and exists (select warehousearea.id
          from warehousearea
        /* inner join warehousearea parent
        on parent.id = warehousearea.parentid*/
         where warehouse.id = warehousearea.warehouseid
           and warehousearea.code = t.locationcode
              --  and parent.code = t.areacode
           and warehousearea.areakind = 3)
   and exists
 (select 1 from sparepart where sparepart.code = t.newpartcode) and nvl(t.oldstdprice,0)<>nvl(t.oldstdprice,0);
select distinct b.* from partsstock a 
inner join agency b on a.storagecompanyid = b.id
where a.createtime>to_date('2015-1-26','yyyy-mm-dd')
and storagecompanytype <>1
