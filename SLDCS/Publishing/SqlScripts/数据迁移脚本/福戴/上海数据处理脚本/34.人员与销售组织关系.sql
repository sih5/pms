create table tmpdata.SalesUnitAffiPersonnel150128 as select * from SalesUnitAffiPersonnel where rownum<1;
insert into tmpdata.SalesUnitAffiPersonnel150128
  (id, personnelid, salesunitid)
  select s_SalesUnitAffiPersonnel.Nextval,
         personsalescenterlink.Personid,
         salesunit.id
    from personsalescenterlink
   inner join salesunit
      on salesunit.partssalescategoryid =
         personsalescenterlink.Partssalescategoryid
   where companyid = 2203
     and salesunit.ownercompanyid = 2203;
insert into tmpdata.SalesUnitAffiPersonnel150128
  (id, personnelid, salesunitid)
  select s_SalesUnitAffiPersonnel.Nextval,a.id,salesunit.id
  from salesunit
   inner join company
   inner join security.personnel a on a.enterpriseid=company.id 
      on salesunit.ownercompanyid = company.id
   where ownercompanyid <> branchid
     and branchid = 2203
 and a.loginid<>'admin';
 --295
 delete from SalesUnitAffiPersonnel a where 
 exists(select * from  tmpdata.SalesUnitAffiPersonnel150128 b where a.personnelid=b.personnelid and a.salesunitid=b.salesunitid)
 insert into SalesUnitAffiPersonnel select * from tmpdata.SalesUnitAffiPersonnel150128;
