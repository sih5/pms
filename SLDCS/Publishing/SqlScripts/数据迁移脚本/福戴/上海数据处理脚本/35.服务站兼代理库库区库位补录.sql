欧曼保内仓库:  tmpdata.tWarehouses_OM_150129

欧曼保内库区库位: tmpdata.tWareLocation_OM_150129


-----------

欧曼保外仓库: tmpdata.tWarehouses_OMBW_150129


欧曼保外库区库位 :tmpdata.tWareLocation_OMBW_150129


欧曼 保外库存: tmpdata.tStaStores_OMBW_150129 





create table tmpdata.OM_DealerWarehouse150129
(仓库编号 varchar2(90),
 仓库名称 varchar2(90),
 代理库编号 varchar2(90),
 代理库名称 VARCHAR2(90),
 仓库类型 varchar2(90),
 电子邮件 varchar2(90),
 传真 varchar2(90),
 联系人 varchar2(90),
 联系电话 varchar2(90),
 地址 varchar2(90),
 储运中心 varchar2(90),
 是否wms接口 varchar2(10),
 存储策略 varchar2(10),品牌 varchar2(100),newcode varchar2(100)
);
insert into tmpdata.OM_DealerWarehouse150129(仓库编号,仓库名称,代理库编号,代理库名称,仓库类型,电子邮件,传真,联系人,联系电话,地址,储运中心,是否wms接口,存储策略,品牌) select
t.仓库编号,t.仓库名称,t.仓储企业编号,t.仓储企业名称,t.仓库类型,t.e_mail,t.fax,t.linker,t.tel,t.address,t.储运中心,t.是否与wms接口对接,t.存储策略,'保外'
from tmpdata.tWarehouses_OMBW_150129 t;

insert into tmpdata.OM_DealerWarehouse150129(仓库编号,仓库名称,代理库编号,代理库名称,仓库类型,电子邮件,传真,联系人,联系电话,地址,储运中心,是否wms接口,存储策略,品牌) select
t.仓库编号,t.仓库名称,t.仓储企业编号,t.仓储企业名称,t.仓库类型,t.e_mail,t.fax,t.linker,t.tel,t.address,t.储运中心,t.是否与wms接口对接,t.存储策略,'保内'
from tmpdata.tWarehouses_OM_150129 t;

  update tmpdata.OM_DealerWarehouse150129 set newcode=decode(品牌,'保内','BN'||代理库编号,'保外','BW'||代理库编号)
  --select * from tmpdata.OM_DealerWarehouse150129 t where not exists(select 1 from  tmpdata.OM_DealerWarehouse150127  v where t.newcode=v.newcode)
create table tmpdata.om_Dealerwarehousearea150129(
warehousecode varchar2(100),
warehousename varchar2(100),
storecompanycode varchar2(100),
storecompanyname varchar2(100),
storecompanytype varchar2(100),
warehouseareacode varchar2(100),
locationcode varchar2(100),
AreaCategory varchar2(10),newwarehousecode varchar2(100),newstorecompanycode varchar2(100),brand varchar(10));
insert into tmpdata.om_Dealerwarehousearea150129(warehousecode,warehousename,storecompanycode,storecompanyname,storecompanytype,warehouseareacode,locationcode,areacategory,brand)
 select t.仓库编号,t.仓库名称,t.仓储企业编号,t.仓储企业名称,t.仓储企业类型,t.库区编号,t.库位编号,t.库区用途,'保内' from tmpdata.tWareLocation_OM_150129 t;


insert into tmpdata.om_Dealerwarehousearea150129(warehousecode,warehousename,storecompanycode,storecompanyname,storecompanytype,warehouseareacode,locationcode,areacategory,brand)
 select t.仓库编号,t.仓库名称,t.仓储企业编号,t.仓储企业名称,t.仓储企业类型,t.库区编号,t.库位编号,t.库区用途,'保外' from tmpdata.tWareLocation_OMBW_150129 t;

  
  update tmpdata.om_Dealerwarehousearea150129 set newwarehousecode=decode(brand,'保内','BN'||storecompanycode,'保外','BW'||storecompanycode)
CREATE TABLE TMPDATA.WAREHOUSE150129Dealer as select * from warehouse where rownum<1;

INSERT INTO TMPDATA.WAREHOUSE150129Dealer
  (id,
   CODE,
   NAME,
   TYPE,
   STATUS,
   ADDRESS,
   PHONENUMBER,
   CONTACT,
   FAX,
   EMAIL,
   STORAGESTRATEGY,
   BRANCHID,
   STORAGECOMPANYID,
   STORAGECOMPANYTYPE,
   WMSINTERFACE,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_warehouse.nextval,
         newcode,
         仓库名称,
         1,
         1,
         地址,
         联系电话,
         联系人,
         传真,
         电子邮件,
         1,
         (select id from company where code ='2450'),
         (select distinct dealerserviceinfo.dealerid from dealerserviceinfo where dealerserviceinfo.businesscode = OM_DealerWarehouse150129.代理库编号 and dealerserviceinfo.branchid=2203),
         1,
         decode(是否wms接口, '是', 1, '否', 0,0),
         1,
         'Admin',
         sysdate
    from tmpdata.OM_DealerWarehouse150129  
    where not exists(select 1 from warehouse where warehouse.code=OM_DealerWarehouse150129.newcode);
      insert into warehouse select 　* from TMPDATA.WAREHOUSE150129Dealer;
      
      update tmpdata.om_Dealerwarehousearea150129 t
         set t.newstorecompanycode =
             (select distinct company.code
                from dealerserviceinfo inner join company on company.id=dealerserviceinfo.dealerid
               where dealerserviceinfo.businesscode =
                     t.storecompanycode
                 and dealerserviceinfo.branchid = 2203);

select count (*)from tmpdata.om_Dealerwarehousearea150120 t where t.newwarehousecode is not null and t.newstorecompanycode is not null;

select count (*)from tmpdata.om_Dealerwarehousearea150120 t where t.newwarehousecode is not null or t.newstorecompanycode is not null;


create table tmpdata.Dealerwarehousearea150129bw_1 as select * from warehousearea where rownum<1;
--53
insert into tmpdata.Dealerwarehousearea150129bw_1
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
         (select id
            from TMPDATA.WAREHOUSE150129Dealer
           where WAREHOUSE150129Dealer.code = t.newwarehousecode
             and branchid = (select id from company where code = '2450')) warehouseid,
         null,
         t.newwarehousecode,
         null,
         89,
         1,
         '欧曼服务站兼代理库库区库位批量处理',
         1,
         1,
         'Admin',
         sysdate
    from (select distinct newwarehousecode, newstorecompanycode
            from tmpdata. om_Dealerwarehousearea150129 t
          
           where exists
           (select id from company where code = t.newstorecompanycode)
             and AreaCategory in ('保管区', '问题区', '检验区')) t
             
             update tmpdata.Dealerwarehousearea150129bw_1 set remark='欧曼服务站兼代理库库区库位批量处理'
 --829         
insert into tmpdata. Dealerwarehousearea150129bw_1
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
         (select id
            from TMPDATA.WAREHOUSE150129Dealer
           where WAREHOUSE150129Dealer.code = t.newwarehousecode
             and branchid = (select id from company where code = '2450')) warehouseid,
         (select id
            from tmpdata.Dealerwarehousearea150129bw_1 v
           where v.code = t.newwarehousecode),
         t.warehouseareacode,
         null,
         decode(AreaCategory, '保管区', 89, '问题区', 87, '检验区', 88),
         1,
         '欧曼服务站兼代理库库区库位批量处理',
         2,
         1,
         'Admin',
         sysdate
    from (select distinct newwarehousecode,
                          warehouseareacode,
                          areacategory,
                          newstorecompanycode
            from tmpdata. om_Dealerwarehousearea150129 t
           where exists
           (select id from company where code = t.newstorecompanycode)
             and AreaCategory in ('保管区', '问题区', '检验区')) t
delete from 
--169120
insert into tmpdata. Dealerwarehousearea150129bw_1
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
         (select id              
            from TMPDATA.WAREHOUSE150129Dealer
           where WAREHOUSE150129Dealer.code = t.newwarehousecode
             and branchid = (select id from company where code = '2450')) warehouseid,
         (select id
            from tmpdata.Dealerwarehousearea150129bw_1 v
           where v.code = t.warehouseareacode
             and v.warehouseid = WAREHOUSE150129Dealer.id
             and WAREHOUSE150129Dealer.branchid =
                 (select id from company where code = '2450')
             ),
         t.locationcode,
         (select id
            from tmpdata.Dealerwarehousearea150129bw_1 v
           where v.code = t.warehouseareacode
             and v.warehouseid = WAREHOUSE150129Dealer.id
             and WAREHOUSE150129Dealer.branchid =
                 (select id from company where code = '2450')
             ),
         decode(AreaCategory, '保管区', 89, '问题区', 87, '检验区', 88),
         1,
         '欧曼服务站兼代理库库区库位批量处理',
         3,
         1,
         'Admin',
         sysdate
    from (select distinct newwarehousecode,
                          warehouseareacode,
                          locationcode,
                          AreaCategory,
                          newstorecompanycode
            from tmpdata. om_Dealerwarehousearea150129 t
           where exists
           (select id from company where code = t.newstorecompanycode)
             and AreaCategory in ('保管区', '问题区', '检验区')) t
   inner join TMPDATA.WAREHOUSE150129Dealer
      on WAREHOUSE150129Dealer.code = t.newwarehousecode
     and WAREHOUSE150129Dealer.branchid = (select id from company where code = '2450');\
     --170002
insert into warehousearea select * from tmpdata. Dealerwarehousearea150129bw_1;
select  count(distinct warehouseid) from tmpdata. Dealerwarehousearea150129bw_1
        --delete from warehousearea where exists(select 1 from tmpdata. Dealerwarehousearea150120bw_1 where Dealerwarehousearea150120bw_1.id=warehousearea.id);



  create table tmpdata.Dealerwarehousearea150129bw_2 as select * from warehousearea where rownum<1;
     
     insert into  tmpdata.Dealerwarehousearea150129bw_2 (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
   (select id
      from TMPDATA.WAREHOUSE150129Dealer
     where WAREHOUSE150129Dealer.code = t.newwarehousecode
       and status <> 99
       and branchid =  (select id from company where code = '2450')) warehouseid,
   (select id
      from tmpdata.Dealerwarehousearea150129bw_1 v
     where v.code = t.newwarehousecode),
   'JY',
   null,
   88,
   1,
   '欧曼服务站兼代理库库区库位批量处理',
   2,
   1,
   'Admin',
   sysdate
    from (select distinct newwarehousecode
            from tmpdata. om_Dealerwarehousearea150129 WHERE newwarehousecode IS NOT NULL) t;
            
              insert into  tmpdata.Dealerwarehousearea150129bw_2 (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
   (select id
      from tmpdata.WAREHOUSE150129Dealer
     where WAREHOUSE150129Dealer.code = t.newwarehousecode
       and status <> 99
       and branchid =  (select id from company where code = '2450')) warehouseid,
   (select id
      from tmpdata.Dealerwarehousearea150129bw_1 v
     where v.code = t.newwarehousecode),
   'WT',
   null,
   87,
   1,
   '欧曼服务站兼代理库库区库位批量处理',
   2,
   1,
   'Admin',
   sysdate
    from (select distinct newwarehousecode
            from tmpdata. om_Dealerwarehousearea150129 WHERE newwarehousecode IS NOT NULL) t;
            
            
                 insert into  tmpdata.Dealerwarehousearea150129bw_2 (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
   (select id
      from tmpdata.WAREHOUSE150129Dealer
     where WAREHOUSE150129Dealer.code = t.newwarehousecode
       and status <> 99
       and branchid =  (select id from company where code = '2450')) warehouseid,
   ( select V.id
      from tmpdata.Dealerwarehousearea150129bw_2 v
      INNER JOIN tmpdata.WAREHOUSE150129Dealer ON V.WAREHOUSEID=WAREHOUSE150129Dealer.ID
     where WAREHOUSE150129Dealer.code = t.newwarehousecode AND V.CODE='WT'),
   'WT',
( select V.id
      from tmpdata.Dealerwarehousearea150129bw_2 v
      INNER JOIN tmpdata.WAREHOUSE150129Dealer ON V.WAREHOUSEID=WAREHOUSE150129Dealer.ID
     where WAREHOUSE150129Dealer.code = t.newwarehousecode AND V.CODE='WT'),
   87,
   1,
   '欧曼服务站兼代理库库区库位批量处理',
   3,
   1,
   'Admin',
   sysdate
    from (select distinct newwarehousecode
            from tmpdata. om_Dealerwarehousearea150129 WHERE newwarehousecode IS NOT NULL) t;
            
            
                          insert into  tmpdata.Dealerwarehousearea150129bw_2 (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
   (select id
      from tmpdata.WAREHOUSE150129Dealer
     where WAREHOUSE150129Dealer.code = t.newwarehousecode
       and status <> 99
       and branchid =  (select id from company where code = '2450')) warehouseid,
   ( select V.id
      from tmpdata.Dealerwarehousearea150129bw_2 v
      INNER JOIN tmpdata.WAREHOUSE150129Dealer ON V.WAREHOUSEID=WAREHOUSE150129Dealer.id
     where WAREHOUSE150129Dealer.code = t.newwarehousecode AND V.CODE='JY'),
   'JY',
   ( select V.id
      from tmpdata.Dealerwarehousearea150129bw_2 v
      INNER JOIN tmpdata.WAREHOUSE150129Dealer ON V.WAREHOUSEID=WAREHOUSE150129Dealer.id
     where WAREHOUSE150129Dealer.code = t.newwarehousecode AND V.CODE='JY'),
   88,
   1,
   '欧曼服务站兼代理库库区库位批量处理',
   3,
   1,
   'Admin',
   sysdate
    from (select distinct newwarehousecode
            from tmpdata. om_Dealerwarehousearea150129 WHERE newwarehousecode IS NOT NULL) t;
            --212
            INSERT INTO warehousearea SELECT * FROM  tmpdata.Dealerwarehousearea150129bw_2;
             delete from warehousearea where exists(select 1 from tmpdata. warehouseareadlk150121_1 where warehouseareadlk150121_1.id=warehousearea.id);
          update   tmpdata. dlkwarehousearea150120bw_1 set toplevelwarehouseareaid=parentid where areakind=3;
