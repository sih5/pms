

 create table tmpdata.om_tstoresdealer150129_1 as
select t.locationcode,
       t.areacode,
       t.partcode,
       t.partname,
       t.code warehousecode,
       t.name warehousename,
       t.qty,
       cast('保内' as varchar2(10)) brand,
       cast(null as varchar2(100)) newwarehousecode,
       cast(null as varchar2(100)) newpartcode,
       cast(null as varchar2(100)) newagentcode,
       t.storagecompanycode,
       t.storagecompanyname,cast(null as number(19, 4)) oldsaleprice,
       cast(null as number(19, 4)) newsaleprice
  from tmpdata.tstores_ombnfw t where rownum<1;
--47504
  insert into tmpdata.om_tstoresdealer150129_1(locationcode,areacode,partcode,partname,warehousecode,warehousename,qty,brand,storagecompanycode,storagecompanyname)
select t.库位编号, t.库区编号,t.配件编号,t.配件名称,t.code,t.name,t.数量,'保内',t.仓储企业编号,t.仓储企业名称 from   tmpdata.tStastores_OM_150129 t;
--10795
  insert into tmpdata.om_tstoresdealer150129_1(locationcode,areacode,partcode,partname,warehousecode,warehousename,qty,brand,storagecompanycode,storagecompanyname)
select t.库位编号, t.库区编号,t.配件编号,t.配件名称,t.code,t.name,t.数量,'保外',t.仓储企业编号,t.仓储企业名称 from   tmpdata.tStaStores_OMBW_150129 t;


update tmpdata.om_tstoresdealer150129_1 t
   set newwarehousecode =decode(t.brand,'保内','BN'||t.storagecompanycode,'保外','BW'||t.storagecompanycode)
       ,newagentcode =
             (select distinct company.code
                from dealerserviceinfo inner join company on company.id=dealerserviceinfo.dealerid
               where dealerserviceinfo.businesscode =
                     t.storagecompanycode
                 and dealerserviceinfo.branchid = 2203 and dealerserviceinfo.status<>99);
                 

 update tmpdata.om_tstoresdealer150129_1  set newpartcode=null;
 update tmpdata.om_tstoresdealer150129_1
   set newpartcode =
       (select newcode
           from tmpdata.om_newoldpartrel150121
          where om_newoldpartrel150121.oldcode = om_tstoresdealer150129_1.partcode
            and om_tstoresdealer150129_1.brand = om_newoldpartrel150121.brand);
update tmpdata.om_tstoresdealer150129_1 set newpartcode=partcode
where exists(select 1 from sparepart where sparepart.code=partcode) and newpartcode is null;                 
       


update tmpdata.om_tstoresdealer150129_1 t
   set oldsaleprice =
       (select distinct v.saleprice
          from tmpdata.om_purchasesaleprice150121 v
         where v.code = t.partcode
           and v.brandname = t.brand),
       newsaleprice =
       (select distinct v.saleprice
          from tmpdata.om_purchasesaleprice150121 v
         where v.code = t.newpartcode
           and v.brandname = t.brand);    
           
           
           
create table tmpdata.partsstockdealer150129_1 as select * from partsstock where rownum<1;
--20659

insert into tmpdata.partsstockdealer150129_1
  (id,
   warehouseid,
   storagecompanyid,
   storagecompanytype,
   branchid,
   warehouseareaid,
   warehouseareacategoryid,
   partid,
   quantity,
   creatorid,
   creatorname,
   createtime)
 select s_partsstock.nextval,
        warehouse.id,
        (select id from company where code=t.newagentcode),
       (select company.type from company where code=t.newagentcode)  ,
        (select id from company where code='2450'),
        (select warehousearea.id
           from warehousearea        
          inner join warehousearea parent
             on parent.id = warehousearea.parentid
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                  and parent.code = t.areacode
                  and warehousearea.areakind = 3),
        89,
        sparepart.id,       
        t.qty,
        1,
        'Admin',
        sysdate
   from tmpdata.om_tstoresdealer150129_1  t
   inner join warehouse on warehouse.code=t.newwarehousecode
   inner join sparepart on sparepart.code = t.newpartcode
  where exists (select warehousearea.id
           from warehousearea        
          inner join warehousearea parent
             on parent.id = warehousearea.parentid
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                  and parent.code = t.areacode
                  and warehousearea.areakind = 3) and t.newagentcode is not null
                  and exists(select id from company where code=t.newagentcode) and t.brand='保外';
       
--46137
insert into  tmpdata.partsstockdealer150129_1
  (id,
   warehouseid,
   storagecompanyid,
   storagecompanytype,
   branchid,
   warehouseareaid,
   warehouseareacategoryid,
   partid,
   quantity,
   creatorid,
   creatorname,
   createtime)
 select s_partsstock.nextval,
        warehouse.id,
        (select id from company where code=t.newagentcode),
       (select company.type from company where code=t.newagentcode)  ,
        (select id from company where code='2450'),
        (select warehousearea.id
           from warehousearea        
          inner join warehousearea parent
             on parent.id = warehousearea.parentid
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                  and parent.code = t.areacode
                  and warehousearea.areakind = 3),
        89,
        sparepart.id,       
        t.qty,
        1,
        'Admin',
        sysdate
   from tmpdata.om_tstoresdealer150129_1  t
   inner join warehouse on warehouse.code=t.newwarehousecode
   inner join sparepart on sparepart.code = t.newpartcode
  where exists (select warehousearea.id
           from warehousearea        
          inner join warehousearea parent
             on parent.id = warehousearea.parentid
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                  and parent.code = t.areacode
                  and warehousearea.areakind = 3) and t.newagentcode is not null
                  and exists(select id from company where code=t.newagentcode) and t.brand='保内' and nvl(oldsaleprice,0)<=nvl(newsaleprice,0);
                  --
                  insert into partsstock select * from  tmpdata.partsstockdealer150129_1; 
                  select * from partsstock where createtime>to_date('2015-1-29','yyyy-mm-dd')
                  
                  
                          select * from partsstock where exists(select 1 from    tmpdata.partsstockdealer150129_1 v where v.id=partsstock.id)
                          
                     select *     from tmpdata.om_tstoresdealer150129_1  t
   inner join warehouse on warehouse.code=t.newwarehousecode
   inner join sparepart on sparepart.code = t.newpartcode
  where exists (select warehousearea.id
           from warehousearea        
          inner join warehousearea parent
             on parent.id = warehousearea.parentid
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                  and parent.code = t.areacode
                  and warehousearea.areakind = 3) and t.newagentcode is not null
                  and exists(select id from company where code=t.newagentcode) and t.brand='保内' and nvl(oldsaleprice,0)<=nvl(newsaleprice,0)
                  and t.storagecompanycode='BEJ00073';
