create table tmpdata.oM_nodeaction150129(node varchar2(100),action varchar2(100),role varchar2(100))
select distinct node
  from tmpdata.oM_nodeaction150129
 where not exists (select * from page where page.name = oM_nodeaction150129.node);

 
select distinct node,action
  from tmpdata.oM_nodeaction150129
 where not exists (select * from page
 inner join action on action .pageid=page.id where page.name = oM_nodeaction150129.node and action.name=oM_nodeaction150129.action)and action <>'查询';

  
  
  
  
create table tmpdata.rule150129
as
select * from rule where 1=0;
--插入角色
--插入授权
--15
insert into tmpdata.rule150129
  (id, roleid, nodeid)
  select s_rule.nextval,
         (select id
            from role r
           where r.name = t.role
             and r.enterpriseid =
                 (select id from enterprise where code = '2450')) roleid,
         (select n.id
            from page p
            left join action a
              on p.id = a.pageid
            left join node n
              on n.categoryid = a.id
             and n.categorytype = 1
           where a.name = t.action
             and p.name = t.node
             and type = 2) as nodeid
    from tmpdata.oM_nodeaction150129 t
   where t.action is not null
     and exists (select n.id
            from page p
            left join action a
              on p.id = a.pageid
            left join node n
              on n.categoryid = a.id
             and n.categorytype = 1
           where a.name = t.action
             and p.name = t.node
             and p.type = 2)
     and t.node not in ('维修量统计报表', '配件库存明细查询')
     and not exists
   (select *
            from (select name,
                         (select name from page where page.id = action.pageid) page
                    from action
                   group by action.name, pageid
                  having count(pageid) > 1) v
           where v.name = t.action
             and v.page = t.node);
        ;
--47
insert into tmpdata.rule150129
  (id, roleid, nodeid)
  select s_rule.nextval,
         (select id
            from role r
           where r.name = t.role
             and r.enterpriseid =(select id from enterprise where code = '2450')) roleid,
         (select n.id
            from page p
           left join node n
              on n.categoryid = p.id
             and n.categorytype = 0
           where p.name = t.node) as nodeid 
    from tmpdata.oM_nodeaction150129 t
   where t.action = '查询'
     and exists (select n.id
            from page p
           left join node n
              on n.categoryid = p.id
             and n.categorytype = 0
           where p.name = t.node)
           and t.node not in (select p.name from page p
group by p.name having count(*)>1);
--19392
insert into rule
  (id, roleid, nodeid)
  select id, roleid, nodeid
    from tmpdata.rule150129 t
   where not exists (select 1
            from rule v
           where v.nodeid = t.nodeid
             and v.roleid = t.roleid);


--人员信息
