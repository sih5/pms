select * from tmpdata.tStationStores_OM_150130
select * from tmpdata.tStationStores_ombw_150130
select *
  from (select (select distinct dealerserviceinfo.businesscode
                  from dealerserviceinfo
                 where dealerserviceinfo.dealerid = company.id
                   and dealerserviceinfo.branchid = 2203) businesscode
          from company
         where company.type in (2, 7)) t
 where businesscode is not null
   and not exists (select 1
          from tmpdata.OM_DealerWarehouse150127 v
         where v.代理库编号 = t.businesscode)
         
create table tmpdata.dealerpartsstock150130 as 
select t.*, cast('保内' as varchar2(10)) brand,cast(null as varchar2(100)) newdealercode,cast(null as varchar2(100)) newpartcode from 
tmpdata.tStationStores_OM_150130 t;
 
insert into tmpdata.dealerpartsstock150130
select t.*, cast('保外' as varchar2(10)) brand,cast(null as varchar2(100)) newdealercode,cast(null as varchar2(100)) newpartcode from 
tmpdata.tStationStores_ombw_150130 t;

update  tmpdata.dealerpartsstock150130 t set t.newdealercode=(select distinct company.code from dealerserviceinfo
inner join company on company.id=dealerserviceinfo .dealerid where dealerserviceinfo.businesscode=t.stationcode and dealerserviceinfo.branchid = 2203);

update tmpdata.dealerpartsstock150130  set newpartcode=null;
 update tmpdata.dealerpartsstock150130
   set newpartcode =
       (select newcode
           from tmpdata.om_newoldpartrel150121
          where om_newoldpartrel150121.oldcode = dealerpartsstock150130.MATERIALCODE
            and dealerpartsstock150130.brand = om_newoldpartrel150121.brand);
update tmpdata.dealerpartsstock150130 set newpartcode=MATERIALCODE
where exists(select 1 from sparepart where sparepart.code=MATERIALCODE) and newpartcode is null;
commit;

--21576
select *from tmpdata.dealerpartsstock150128 t  where t.newpartcode is null
create table tmpdata.dealerpartsstock150130_1 as select * from DealerPartsStock where rownum<1;
insert into tmpdata.dealerpartsstock150130_1
  (id,
   dealerid,
   dealercode,
   dealername,
   subdealerid,
   branchid,
   salescategoryid,
   salescategoryname,
   sparepartid,
   sparepartcode,
   quantity,
   creatorid,
   creatorname,
   createtime)
  select s_DealerPartsStock.Nextval,
         (select id from company where company.code = t.newdealercode),
         t.newdealercode,
         (select id from company where company.code = t.newdealercode),
         -1,
         2203,
         (select id
            from partssalescategory
           where name = decode(t.brand, '保内', '欧曼', '保外', '欧曼保外')),
         decode(t.brand, '保内', '欧曼', '保外', '欧曼保外'),
         (select id from sparepart where sparepart.code = t.newpartcode),
         t.newpartcode,
         t.sumqty,
         1,
         'Admin',
         sysdate
    from tmpdata.dealerpartsstock150130 t
   where t.newpartcode is not null;
   
   
create index tmpdata.dealerpartsstock150130_1_idx on TMPDATA.dealerpartsstock150130_1 (dealerid, salescategoryid, sparepartid);
update tmpdata.dealerpartsstock150130_1 v
   set v.quantity =
       (select sum(quantity)
          from tmpdata.dealerpartsstock150130_1 a
         where a.dealerid = v.dealerid
           and a.salescategoryid = v.salescategoryid
           and a.sparepartid = v.sparepartid)
 where exists (select *
          from (select dealerid, salescategoryid, sparepartid
                  from tmpdata.dealerpartsstock150130_1 t
                  
                 group by dealerid, salescategoryid, sparepartid
                having count(sparepartid) > 1) t
         where t.dealerid = v.dealerid
           and t.salescategoryid = v.salescategoryid
           and t.sparepartid = v.sparepartid);
    
delete tmpdata.dealerpartsstock150130_1 
 where dealerpartsstock150130_1.rowid not in
       (select min(dealerpartsstock150130_1.rowid)
          from tmpdata.dealerpartsstock150130_1  
         group by dealerid, salescategoryid, sparepartid)  ;

select from  tmpdata.om_tstores150121 t where t.newwarehousecode is not null and t.newpartcode is not null
group by t.newwarehousecode,t.newpartcode
--19435
insert into DealerPartsStock select * from tmpdata.dealerpartsstock150130_1;

select * from tmpdata.dealerpartsstock150130 t where t.newpartcode is null
