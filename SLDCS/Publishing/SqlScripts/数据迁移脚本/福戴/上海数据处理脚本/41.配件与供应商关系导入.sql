/*
CREATE TABLE TMPDATA.PARTSSUPPLIERRELATION1501027 AS SELECT * FROM PARTSSUPPLIERRELATION WHERE ROWNUM<1;
INSERT INTO TMPDATA.PARTSSUPPLIERRELATION1501027
  (ID,
   BRANCHID,
   PARTSSALESCATEGORYID,
   PARTSSALESCATEGORYNAME,
   PARTID,
   SUPPLIERID,
   SUPPLIERPARTCODE,
   SUPPLIERPARTNAME,
   ISPRIMARY,
   ISCANCLAIM,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_PARTSSUPPLIERRELATION.Nextval,
         (select id from company where code = '2450'),(select id from partssalescategory where name=a.品牌),a.品牌,
         (select id from sparepart s where a.配件图号 = s.code),
         (select id from partssupplier ps where ps.code = a.供应商编码),
         a.供应商编码,
         (select name from partssupplier ps where ps.code = a.供应商编码),
         1,1,1,1,'Admin',sysdate
    from tmpdata.om_partbranchfrom150116 a
   where exists (select * from sparepart s where a.配件图号 = s.code)
     and exists
   (select 1 from partssupplier ps where ps.code = a.供应商编码);
insert into PARTSSUPPLIERRELATION select * from  TMPDATA.PARTSSUPPLIERRELATION1501027;


create table tmpdata.PSupplierRelationHistory150127 as select * from PartsSupplierRelationHistory where rownum<1;
insert into tmpdata.PSupplierRelationHistory150127
  (id,
   partssupplierrelationid,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   partid,
   supplierid,
   supplierpartcode,
   supplierpartname,
   isprimary,
   status,
   creatorid,
   creatorname,
   createtime)
select s_PartsSupplierRelationHistory.Nextval,id,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   partid,
   supplierid,
   supplierpartcode,
   supplierpartname,
   isprimary,
   status,1,'Admin',sysdate from  TMPDATA.PARTSSUPPLIERRELATION1501027;
   insert into PartsSupplierRelationHistory select * from  tmpdata.PSupplierRelationHistory150127;
   
   
   
CREATE TABLE TMPDATA.PSUPPLIERRELATION1501027bw AS SELECT * FROM PARTSSUPPLIERRELATION WHERE ROWNUM<1;
INSERT INTO TMPDATA.PSUPPLIERRELATION1501027bw
  (ID,
   BRANCHID,
   PARTSSALESCATEGORYID,
   PARTSSALESCATEGORYNAME,
   PARTID,
   SUPPLIERID,
   SUPPLIERPARTCODE,
   SUPPLIERPARTNAME,
   ISPRIMARY,
   ISCANCLAIM,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_PARTSSUPPLIERRELATION.Nextval,
         (select id from company where code = '2450'),(select id from partssalescategory where name=a.品牌),a.品牌,
         (select id from sparepart s where a.配件图号 = s.code),
         (select id from partssupplier ps where ps.code = a.供应商编码),
         a.供应商编码,
         (select name from partssupplier ps where ps.code = a.供应商编码),
         1,1,1,1,'Admin',sysdate
    from tmpdata.om_partbranchfrom150119bw a
   where exists (select * from sparepart s where a.配件图号 = s.code)
     and exists
   (select 1 from partssupplier ps where ps.code = a.供应商编码);
insert into PARTSSUPPLIERRELATION select * from  TMPDATA.PSUPPLIERRELATION1501027bw;

create table tmpdata.PSRelationHistory150127bw as select * from PartsSupplierRelationHistory where rownum<1;
insert into tmpdata.PSRelationHistory150127bw
  (id,
   partssupplierrelationid,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   partid,
   supplierid,
   supplierpartcode,
   supplierpartname,
   isprimary,
   status,
   creatorid,
   creatorname,
   createtime)
select s_PartsSupplierRelationHistory.Nextval,id,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   partid,
   supplierid,
   supplierpartcode,
   supplierpartname,
   isprimary,
   status,1,'Admin',sysdate from  TMPDATA.PSUPPLIERRELATION1501027bw;
   insert into PartsSupplierRelationHistory select * from  tmpdata.PSRelationHistory150127bw;
   
    select * from tmpdata.om_partbranchfrom150116 a
   where exists (select * from sparepart s where a.配件图号 = s.code)
     and not exists
   (select 1 from partssupplier ps where ps.code = a.供应商编码);
   */
   
delete from PARTSSUPPLIERRELATION v where exists(select 1 from  TMPDATA.PARTSSUPPLIERRELATION1501027 t where t.id=v.id);

delete from PartsSupplierRelationHistory v where exists(select 1 from   tmpdata.PSupplierRelationHistory150127 t where t.id=v.id);
  
delete from PARTSSUPPLIERRELATION v where exists(select 1 from  TMPDATA.PSUPPLIERRELATION1501027bw t where t.id=v.id);

delete from PartsSupplierRelationHistory v where exists(select 1 from   TMPDATA.PSRelationHistory150127bw t where t.id=v.id);


create table tmpdata.PARTSSUPPLIERRELATION150131(partcode varchar2(100),partname varchar2(100),suppliercode varchar2(100),brand varchar2(100));

select count(*)from  tmpdata.PARTSSUPPLIERRELATION150131
select partcode,suppliercode ,brand from  tmpdata.PARTSSUPPLIERRELATION150131 group by partcode,suppliercode ,brand having count (brand)>1


delete from PARTSSUPPLIERRELATION v where exists(select 1 from  TMPDATA.PARTSSUPPLIERRELATION1501027 t where t.id=v.id);

delete from PartsSupplierRelationHistory v where exists(select 1 from   tmpdata.PSupplierRelationHistory150127 t where t.id=v.id);
  
delete from PARTSSUPPLIERRELATION v where exists(select 1 from  TMPDATA.PSUPPLIERRELATION1501027bw t where t.id=v.id);

delete from PartsSupplierRelationHistory v where exists(select 1 from   TMPDATA.PSRelationHistory150127bw t where t.id=v.id);


CREATE TABLE TMPDATA.PSUPPLIERRELATION1501031 AS SELECT * FROM PARTSSUPPLIERRELATION WHERE ROWNUM<1;
--147182
INSERT INTO TMPDATA.PSUPPLIERRELATION1501031
  (ID,
   BRANCHID,
   PARTSSALESCATEGORYID,
   PARTSSALESCATEGORYNAME,
   PARTID,
   SUPPLIERID,
   SUPPLIERPARTCODE,
   SUPPLIERPARTNAME,
   ISPRIMARY,
   ISCANCLAIM,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_PARTSSUPPLIERRELATION.Nextval,
         (select id from company where code = '2450'),(select id from partssalescategory where name=a.brand),a.brand,
         (select id from sparepart s where a.partcode = s.code),
         (select id from partssupplier ps where ps.code = a.suppliercode),
         a.suppliercode,
         (select name from partssupplier ps where ps.code = a.suppliercode),
         1,1,1,1,'Admin',sysdate
    from tmpdata.PARTSSUPPLIERRELATION150131 a
   where exists (select * from sparepart s where a.partcode = s.code)
     and exists
   (select 1 from partssupplier ps where ps.code = a.suppliercode)
   and not exists(select 1 from PARTSSUPPLIERRELATION
   inner join company on company.id=PARTSSUPPLIERRELATION.SUPPLIERID
   inner join sparepart on sparepart.id=PARTSSUPPLIERRELATION.PARTID
   inner join partssalescategory on partssalescategory.id=PARTSSUPPLIERRELATION.PARTSSALESCATEGORYID
   where a.partcode=sparepart.code and a.suppliercode=company.code and partssalescategory.name=a.brand and PARTSSUPPLIERRELATION.status<>99);
insert into PARTSSUPPLIERRELATION select * from  TMPDATA.PSUPPLIERRELATION1501031;
create table tmpdata.PSRelationHistory150131 as select * from PartsSupplierRelationHistory where rownum<1;
insert into tmpdata.PSRelationHistory150131
  (id,
   partssupplierrelationid,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   partid,
   supplierid,
   supplierpartcode,
   supplierpartname,
   isprimary,
   status,
   creatorid,
   creatorname,
   createtime)
select s_PartsSupplierRelationHistory.Nextval,id,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   partid,
   supplierid,
   supplierpartcode,
   supplierpartname,
   isprimary,
   status,1,'Admin',sysdate from  TMPDATA.PSUPPLIERRELATION1501031;
   insert into PartsSupplierRelationHistory select * from  tmpdata.PSRelationHistory150131;
   delete from PartsSupplierRelationHistory where exists(select 1 from TMPDATA.PSRelationHistory150131 where PSRelationHistory150131.id=PartsSupplierRelationHistory.id)

select *  from   tmpdata.PARTSSUPPLIERRELATION150131 a
   where not  exists (select * from sparepart s where a.partcode = s.code);
   
select *  from   tmpdata.PARTSSUPPLIERRELATION150131 a
   where 
     exists(select 1 from PARTSSUPPLIERRELATION
   inner join company on company.id=PARTSSUPPLIERRELATION.SUPPLIERID
   inner join sparepart on sparepart.id=PARTSSUPPLIERRELATION.PARTID
   inner join partssalescategory on partssalescategory.id=PARTSSUPPLIERRELATION.PARTSSALESCATEGORYID
   where a.partcode=sparepart.code and a.suppliercode=company.code and partssalescategory.name=a.brand and PARTSSUPPLIERRELATION.status<>99)
   and not exists(select 1 from TMPDATA.PSUPPLIERRELATION1501031
   inner join company on company.id=PSUPPLIERRELATION1501031.SUPPLIERID
   inner join sparepart on sparepart.id=PSUPPLIERRELATION1501031.PARTID
   inner join partssalescategory on partssalescategory.id=PSUPPLIERRELATION1501031.PARTSSALESCATEGORYID
   where a.partcode=sparepart.code and a.suppliercode=company.code and partssalescategory.name=a.brand and PSUPPLIERRELATION1501031.status<>99
   );
