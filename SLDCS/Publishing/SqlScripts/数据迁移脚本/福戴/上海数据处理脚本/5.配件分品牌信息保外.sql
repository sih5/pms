

create table tmpdata.om_partbranchfrom150119bw
(
       品牌    varchar2(200),
       配件图号    varchar2(200),
       配件名称    varchar2(200),
       配件参考图号    varchar2(200),
       损耗类型    varchar2(200),
       是否可采购    varchar2(200),
       是否可销售    varchar2(200),
       最小销售数量    varchar2(200),
       是否可直供    varchar2(200),
       旧件返回政策  varchar2(200),
       配件保修分类编号    varchar2(200),
       配件保修分类名称    varchar2(200),
       供应商编码    varchar2(200),
       供应商名称    varchar2(200),
       供应商图号  varchar2(200)
);
create table tmpdata.partsbranch150119bw as select * from partsbranch where rownum<1;

update  tmpdata.om_partbranchfrom150119bw set  旧件返回政策=1 where 旧件返回政策='1 ';

update  tmpdata.om_partbranchfrom150119bw set  旧件返回政策=3 where 旧件返回政策='3 '; 

update  tmpdata.om_partbranchfrom150119bw set  旧件返回政策=2 where 旧件返回政策='2 '; 

update  tmpdata.om_partbranchfrom150119bw set  旧件返回政策=4 where 旧件返回政策='4 '; 
select 配件图号 from tmpdata.om_partbranchfrom150119bw a group by a.配件图号 having count(配件图号)>1
select * from  tmpdata.om_partbranchfrom150119bw a where a.配件图号 in (
select partcode from tmpdata.partsbranch150119bw  group by partcode having count(partcode)>1)

delete partsbranch150119bw
 where partsbranch150119bw.rowid not in
       (select min(partsbranch150119bw.rowid)
          from partsbranch150119bw
         group by partcode);

select * from tmpdata.om_partbranchfrom150119bw a
  where not exists (select * from sparepart s where a.配件图号 = s.code)
  select * from tmpdata.om_partbranchfrom150119bw a
     where  exists
   (select 1
            from partsbranch v
           inner join sparepart
              on sparepart.id = v.partid
           inner join partssalescategory
              on partssalescategory.id = v.partssalescategoryid
           where a.配件图号 = sparepart.code
             and a.品牌 = partssalescategory.name
             and v.status = 1);
             select distinct 旧件返回政策  from tmpdata.om_partbranchfrom150116 a
             
             --88026
insert into tmpdata.partsbranch150119bw
  ("ID", --1
   "PARTID", --2
   "PARTCODE", --3
   "PARTNAME", --4
   "REFERENCECODE", --5
   "STOCKMAXIMUM", --6
   "STOCKMINIMUM", --7
   "BRANCHID", --8
   "BRANCHNAME", --9
   "PARTSSALESCATEGORYID", --10
   "PARTSSALESCATEGORYNAME", --11
   "ABCSTRATEGYID", --12
   "PRODUCTLIFECYCLE", --13
   "LOSSTYPE", --14
   "ISORDERABLE", --15
   "PURCHASECYCLE", --16
   "ISSERVICE", --17
   "ISSALABLE", --18
   "PARTSRETURNPOLICY", --19
   "ISDIRECTSUPPLY", --20
   "WARRANTYSUPPLYSTATUS", --21
   "MINSALEQUANTITY", --22
   "PARTABC", --23
   "PURCHASEROUTE", --24
   "REPAIRMATMINUNIT", --25
   "PARTSWARRANTYCATEGORYID", --26
   "PARTSWARRANTYCATEGORYCODE", --27
   "PARTSWARRANTYCATEGORYNAME", --28
   "PARTSWARHOUSEMANAGEGRANULARITY", --29
   "STATUS", --30
   "CREATORID", --31
   "CREATORNAME", --32
   "CREATETIME", --33
   "MODIFIERID", --34
   "MODIFIERNAME", --35
   "MODIFYTIME", --36
   "ABANDONERID", --37
   "ABANDONERNAME", --38
   "ABANDONTIME", --39
   "REMARK") --40
  select s_partsbranch.nextval, --1
         (select s.id from sparepart s where a.配件图号 = s.code) partid, --2
         a.配件图号, --3
         (select s.name from sparepart s where a.配件图号 = s.code) partname, --4
         a.配件参考图号, --5
         9999999, --6
         0, --7
         (select id from company where code = '2450') branchid,  --8
         (select name from company where code = '2450') branchid, --9
         (select id from partssalescategory p where p.name = a.品牌) partssalescategoryid, --10
         a.品牌, --11
         '', --12
         '', --13
         case a.损耗类型
           when '非常用件' then
            1
           when '常用件' then
            2
           when '易耗件' then
            3
           when '大总成' then
            4
         end losstype, --14
         NVL(case a.是否可采购
               when '是' then
                1
               when '否' then
                0
             end,
             1) isorderable, --15
         '', --16
         '', --17
         nvl(case a.是否可销售
               when '是' then
                1
               when '否' then
                0
             end,
             1) issaleable, --18
          a.旧件返回政策, --19
         nvl(case a.是否可直供
               when '是' then
                1
               when '否' then
                0
             end,
             0) isdirectsupply, --20
         '', --21
         a.最小销售数量, --22
         '', --23
         '', --24
         '', --25
         (select id
            from partswarrantycategory p
           where p.code = a.配件保修分类编号) partswarrantycategorydi, --26
         a.配件保修分类编号, --27
         (select name
            from partswarrantycategory p
           where p.code = a.配件保修分类编号) partswarrantycategoryname, --28
         '', --29
         1, --30
         1, --31
         'Admin', --32
         sysdate, --33
         '', --34
         '', --35
         '', --36
         '', --37
         '', --38
         '', --39
         ''
    from tmpdata.om_partbranchfrom150119bw a
   where exists (select * from sparepart s where a.配件图号 = s.code)
     and not exists
   (select 1
            from partsbranch v
           inner join sparepart
              on sparepart.id = v.partid
           inner join partssalescategory
              on partssalescategory.id = v.partssalescategoryid
           where a.配件图号 = sparepart.code
             and a.品牌 = partssalescategory.name
             and v.status = 1);
            
             
             
 insert into partsbranch select * from tmpdata.partsbranch150119bw t;
 create table tmpdata.partsbranchhistory150119bw as select * from partsbranchhistory where rownum<1;
 
 --2.将配件分品牌信息履历导入临时表：
 insert into tmpdata.partsbranchhistory150119bw
  ("ID",
   "PARTSBRANCHID",
   "PARTID",
   "PARTCODE",
   "PARTNAME",
   "REFERENCECODE",
   "STOCKMAXIMUM",
   "STOCKMINIMUM",
   "BRANCHID",
   "BRANCHNAME",
   "LOSSTYPE",
   "PARTSSALESCATEGORYID",
   "PARTSSALESCATEGORYNAME",
   "ABCSTRATEGYID",
   "PRODUCTLIFECYCLE",
   "ISORDERABLE",
   "PURCHASECYCLE",
   "ISSERVICE",
   "ISSALABLE",
   "PARTSRETURNPOLICY",
   "ISDIRECTSUPPLY",
   "WARRANTYSUPPLYSTATUS",
   "MINSALEQUANTITY",
   "PARTABC",
   "PURCHASEROUTE",
   "PARTSWARRANTYCATEGORYID",
   "PARTSWARRANTYCATEGORYCODE",
   "PARTSWARRANTYCATEGORYNAME",
   "PARTSWARHOUSEMANAGEGRANULARITY",
   "STATUS",
   "CREATORID",
   "CREATORNAME",
   "CREATETIME",
   "REMARK",
   "REPAIRMATMINUNIT")
  select yxdcs.s_partsbranchhistory.nextval,
         b."ID",
         b."PARTID",
         b."PARTCODE",
         b."PARTNAME",
         b."REFERENCECODE",
         b."STOCKMAXIMUM",
         b."STOCKMINIMUM",
         b."BRANCHID",
         b."BRANCHNAME",
         b."LOSSTYPE",
         b."PARTSSALESCATEGORYID",
         b."PARTSSALESCATEGORYNAME",
         b."ABCSTRATEGYID",
         b."PRODUCTLIFECYCLE",
         b."ISORDERABLE",
         b."PURCHASECYCLE",
         b."ISSERVICE",
         b."ISSALABLE",
         b."PARTSRETURNPOLICY",
         b."ISDIRECTSUPPLY",
         b."WARRANTYSUPPLYSTATUS",
         b."MINSALEQUANTITY",
         b."PARTABC",
         b."PURCHASEROUTE",
         b."PARTSWARRANTYCATEGORYID",
         b."PARTSWARRANTYCATEGORYCODE",
         b."PARTSWARRANTYCATEGORYNAME",
         b."PARTSWARHOUSEMANAGEGRANULARITY",
         b."STATUS",
         b."CREATORID",
         b."CREATORNAME",
         b."CREATETIME",
         b."REMARK",
         b."REPAIRMATMINUNIT"
    from tmpdata.partsbranch150119bw b;


--3.将配件基础信息导入正式表：
/*
insert into partsbranch
select * from tmpdata.partsbranch150116 ;
*/

--4.将配件基础信息履历导入正式表：
insert into partsbranchhistory
select * from tmpdata.partsbranchhistory150119bw ;


CREATE TABLE TMPDATA.PSUPPLIERRELATION1501027bw AS SELECT * FROM PARTSSUPPLIERRELATION WHERE ROWNUM<1;
INSERT INTO TMPDATA.PSUPPLIERRELATION1501027bw
  (ID,
   BRANCHID,
   PARTSSALESCATEGORYID,
   PARTSSALESCATEGORYNAME,
   PARTID,
   SUPPLIERID,
   SUPPLIERPARTCODE,
   SUPPLIERPARTNAME,
   ISPRIMARY,
   ISCANCLAIM,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_PARTSSUPPLIERRELATION.Nextval,
         (select id from company where code = '2450'),(select id from partssalescategory where name=a.品牌),a.品牌,
         (select id from sparepart s where a.配件图号 = s.code),
         (select id from partssupplier ps where ps.code = a.供应商编码),
         a.供应商编码,
         (select name from partssupplier ps where ps.code = a.供应商编码),
         1,1,1,1,'Admin',sysdate
    from tmpdata.om_partbranchfrom150119bw a
   where exists (select * from sparepart s where a.配件图号 = s.code)
     and exists
   (select 1 from partssupplier ps where ps.code = a.供应商编码);
insert into PARTSSUPPLIERRELATION select * from  TMPDATA.PSUPPLIERRELATION1501027bw;

create table tmpdata.PSRelationHistory150127bw as select * from PartsSupplierRelationHistory where rownum<1;
insert into tmpdata.PSRelationHistory150127bw
  (id,
   partssupplierrelationid,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   partid,
   supplierid,
   supplierpartcode,
   supplierpartname,
   isprimary,
   status,
   creatorid,
   creatorname,
   createtime)
select s_PartsSupplierRelationHistory.Nextval,id,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   partid,
   supplierid,
   supplierpartcode,
   supplierpartname,
   isprimary,
   status,1,'Admin',sysdate from  TMPDATA.PSUPPLIERRELATION1501027bw;
   insert into PartsSupplierRelationHistory select * from  tmpdata.PSRelationHistory150127bw;
   
    select * from tmpdata.om_partbranchfrom150116 a
   where exists (select * from sparepart s where a.配件图号 = s.code)
     and not exists
   (select 1 from partssupplier ps where ps.code = a.供应商编码);
   
   
   
