zhangxiaoliang(张晓亮) 14:55:16
保内保外都提供了替换件信息，如果有互换号重复了怎么处理
liuchunlei(刘春雷) 14:56:38
重复的，只导一个
zhangxiaoliang(张晓亮) 14:57:35
重复的标准，互换号+配件编号？
liuchunlei(刘春雷) 14:58:32
以配件编号来。 保内的导入以后，如果 保外的互换号配件在保内的里面有，就不导了
zhangxiaoliang(张晓亮) 14:59:39
如果保内配件图号在互换号表里存在呢
liuchunlei(刘春雷) 15:01:27
保内导的时候以互换号为唯一性依据
liuchunlei(刘春雷) 15:01:32
有的互换号就不导。
zhangxiaoliang(张晓亮) 15:03:21
如果是互换号存在但配件不在配件互换信息里的呢？
liuchunlei(刘春雷) 15:04:17
互换号存在就不管。
liuchunlei(刘春雷) 15:04:30
把这部分数据单独保存出来，以后跟他们说
select count(*)from tmpdata.om_spareexchange150119
create table tmpdata.om_spareexchange150119(code varchar2(50),referencecode varchar2(50),name varchar2(100),exchangecode varchar2(50));

select * from   tmpdata.om_spareexchange150119 t  where t.exchangecode<> replace(exchangecode,CHR(9),'')

select * from   tmpdata.om_spareexchange150119 t  where t.exchangecode<> replace(exchangecode,CHR(10),'')

select * from   tmpdata.om_spareexchange150119 t  where t.exchangecode<> replace(exchangecode,CHR(13),'')

         create table tmpdata.spareexchange150119 as select * from PartsExchange where rownum<1;
         

select * from  tmpdata.om_spareexchange150119 t
where exists(select 1 from PartsExchange where PartsExchange.Exchangecode=t.exchangecode);         
select * from  tmpdata.om_spareexchange150119 t         
         where not exists
            (select 1 from sparepart where sparepart.code = t.code);
            
            --63073
         insert into tmpdata.spareexchange150119
           (id,
            exchangecode,
            exchangename,
            partid,
            status,
            remark,
            creatorid,
            creatorname,
            createtime)
           select s_PartsExchange.Nextval,
                  t.exchangecode,
                  t.exchangecode,
                  (select id from sparepart where sparepart.code = t.code),
                  1,
                  '',
                  1,
                  'Admin',
                  sysdate
             from tmpdata.om_spareexchange150119 t
            where exists
            (select 1 from sparepart where sparepart.code = t.code)
            and not exists(select 1 from PartsExchange where PartsExchange.Exchangecode=t.exchangecode);
           
           create table tmpdata.PartsExchangeHistory150119 as select * from PartsExchangeHistory where rownum<1;
           insert into tmpdata.PartsExchangeHistory150119
             (id,
              partsexchangeid,
              exchangecode,
              exchangename,
              partid,
              status,
              remark,
              creatorid,
              creatorname,
              createtime)
             select s_PartsExchangeHistory.Nextval,
                    id,
                    exchangecode,
                    exchangename,
                    partid,
                    status,
                    remark,
                    creatorid,
                    creatorname,
                    createtime
               from tmpdata.spareexchange150119 t;
               insert into PartsExchangeHistory select * from tmpdata.PartsExchangeHistory150119;
            20514    21204
 select count(*) from tmpdata.om_spareexchange150119bw t
 where not exists(
             select spareexchange150119.*
               from tmpdata.spareexchange150119
              inner join sparepart
                 on sparepart.id = spareexchange150119.partid
              where t.code = sparepart.code)
              and not  exists(select 1
                       from PartsExchange
                      where PartsExchange.Exchangecode = t.exchangecode);
          select count(*)from     tmpdata.om_spareexchange150119bw t
          where not exists
              (select 1 from sparepart where sparepart.code = t.code)
create table tmpdata.om_spareexchange150119bw(code varchar2(50),referencecode varchar2(50),name varchar2(100),exchangecode varchar2(50));

         create table tmpdata.spareexchange150119bw as select * from PartsExchange where rownum<1;
         --18712
           insert into tmpdata.spareexchange150119bw
             (id,
              exchangecode,
              exchangename,
              partid,
              status,
              remark,
              creatorid,
              creatorname,
              createtime)
             select s_PartsExchange.Nextval,
                    t.exchangecode,
                    t.exchangecode,
                    (select id from sparepart where sparepart.code = t.code),
                    1,
                    '',
                    1,
                    'Admin',
                    sysdate
               from tmpdata.om_spareexchange150119bw t
              where exists
              (select 1 from sparepart where sparepart.code = t.code)
                and not exists
              (select 1
                       from PartsExchange
                      where PartsExchange.Exchangecode = t.exchangecode)
           and not exists(
             select 1
               from tmpdata.spareexchange150119
              inner join sparepart
                 on sparepart.id = spareexchange150119.partid
              where t.code = sparepart.code);
               
               
                insert into PartsExchange select * from tmpdata.spareexchange150119bw;
                insert into PartsExchange select * from tmpdata.spareexchange150119;
                
                
           create table tmpdata.PartsExchangeHistory150119bw as select * from PartsExchangeHistory where rownum<1;
           insert into tmpdata.PartsExchangeHistory150119bw
             (id,
              partsexchangeid,
              exchangecode,
              exchangename,
              partid,
              status,
              remark,
              creatorid,
              creatorname,
              createtime)
             select s_PartsExchangeHistory.Nextval,
                    id,
                    exchangecode,
                    exchangename,
                    partid,
                    status,
                    remark,
                    creatorid,
                    creatorname,
                    createtime
               from tmpdata.spareexchange150119bw t;
               insert into PartsExchangeHistory select * from tmpdata.PartsExchangeHistory150119bw;
