create table tmpdata.OM_Warehouse150120
(仓库编号 varchar2(90),
 仓库名称 varchar2(90),
 仓库类型 varchar2(90),
 电子邮件 varchar2(90),
 传真 varchar2(90),
 联系人 varchar2(90),
 联系电话 varchar2(90),
 地址 varchar2(90),
 储运中心 varchar2(90),
 是否wms接口 varchar2(10),
 存储策略 varchar2(10),品牌 varchar2(10)
)
create table tmpdata.om_oldnewwarehouserel150120
(oldcode varchar2 (100),oldname varchar2(100),newcode varchar2(100),newname varchar2(100),brand varchar2(100),remark varchar2(100))


CREATE TABLE TMPDATA.WAREHOUSE150120 as select * from warehouse where rownum<1;

INSERT INTO TMPDATA.WAREHOUSE150120
  (id,
   CODE,
   NAME,
   TYPE,
   STATUS,
   ADDRESS,
   PHONENUMBER,
   CONTACT,
   FAX,
   EMAIL,
   STORAGESTRATEGY,
   BRANCHID,
   STORAGECOMPANYID,
   STORAGECOMPANYTYPE,
   WMSINTERFACE,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_warehouse.nextval,
         newcode,
         newname,
         decode(仓库类型,'总库', 1, '分库', 2),
         1,
         地址,
         联系电话,
         联系人,
         传真,
         电子邮件,
         1,
         (select id from company where code = '2450'),
         (select id from company where code = '2450'),
         1,
         decode(是否wms接口, '是', 1, '否', 0),
         1,
         'Admin',
         sysdate
    from tmpdata.OM_Warehouse150120
   inner join tmpdata.om_oldnewwarehouserel150120
      on oldcode = 仓库编号 and brand=品牌;
      insert into warehouse select * from  TMPDATA.WAREHOUSE150120;
      
      
      create table  TMPDATA.om_warehousearea150120(
warehousecode varchar2(100),
warehousename varchar2(100),
warehouseareacode varchar2(100),
locationcode varchar2(100),
AreaCategory varchar2(10),remark varchar2(200),brand varchar2(10),newwarehousecode varchar2(100))


update TMPDATA.om_warehousearea150120 t
   set t.newwarehousecode =
       (select  newcode from tmpdata.om_oldnewwarehouserel150120
         where t.warehousecode = om_oldnewwarehouserel150120.oldcode
           and om_oldnewwarehouserel150120.brand = t.brand);
select count(*) from TMPDATA.om_warehousearea150120 t where newwarehousecode is not null
create table tmpdata.warehousearea150120_1 as select * from warehousearea where rownum<1;
--43
insert into tmpdata.
warehousearea150120_1
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
   (select id
      from warehouse
     where warehouse.code = t.newwarehousecode
       and status <> 99
       and branchid = (select id from company where code = '2450')) warehouseid,
   null,
   t.newwarehousecode,
   null,
   89,
   1,
   '欧曼库区库位批量处理',
   1,
   1,
   'Admin',
   sysdate
    from (select distinct newwarehousecode
            from tmpdata. om_warehousearea150120) t
   where exists
   (select id
            from warehouse
           where warehouse.code = t.newwarehousecode
             and status <> 99
             and branchid = (select id from company where code = '2450'));
             

--181
   insert into tmpdata.
warehousearea150120_1
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
   (select id
      from warehouse
     where warehouse.code = t.newwarehousecode
       and status <> 99
       and branchid =  (select id from company where code = '2450')) warehouseid,
   (select id
      from tmpdata.warehousearea150120_1 v
     where v.code = t.newwarehousecode),
   t.warehouseareacode,
   null,
   decode(AreaCategory, '保管区', 89, '问题区', 87, '检验区', 88),
   1,
   '欧曼库区库位批量处理',
   2,
   1,
   'Admin',
   sysdate
    from (select distinct newwarehousecode, warehouseareacode, areacategory
            from tmpdata. om_warehousearea150120) t
   where exists (select id
            from warehouse
           where warehouse.code = t.newwarehousecode
             and status <> 99
             and branchid = (select id from company where code = '2450'))     ; 
             
            
--203895
insert into tmpdata. warehousearea150120_1
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
         warehouse.id,
         (select id
            from tmpdata.warehousearea150120_1 v
           where v.code = t.warehouseareacode
             and v.warehouseid = warehouse.id),
         t.locationcode,
         (select id
            from tmpdata.warehousearea150120_1 v
           where v.code = t.warehouseareacode
             and v.warehouseid = warehouse.id),
         decode(AreaCategory, '保管区', 89, '问题区', 87, '检验区', 88),
         1,
         '欧曼库区库位批量处理',
         3,
         1,
         'Admin',
         sysdate
    from tmpdata.om_warehousearea150120 t
   inner join warehouse
      on warehouse.code = t.newwarehousecode
     and warehouse.status <> 99
     and branchid = (select id from company where code = '2450');
     insert into warehousearea select * from tmpdata. warehousearea150120_1;
     delete from warehousearea where exists(select 1 from tmpdata. warehousearea150120_1 where warehousearea150120_1.id=warehousearea.id);
     create table tmpdata.warehousearea150121_1 as select * from warehousearea where rownum<1;
     
     insert into  tmpdata.warehousearea150121_1 (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
   (select id
      from warehouse
     where warehouse.code = t.newwarehousecode
       and status <> 99
       and branchid =  (select id from company where code = '2450')) warehouseid,
   (select id
      from tmpdata.warehousearea150120_1 v
     where v.code = t.newwarehousecode),
   'JY',
   null,
   88,
   1,
   '欧曼库区库位批量处理',
   2,
   1,
   'Admin',
   sysdate
    from (select distinct newwarehousecode
            from tmpdata. om_warehousearea150120 WHERE newwarehousecode IS NOT NULL) t;
            
              insert into  tmpdata.warehousearea150121_1 (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
   (select id
      from warehouse
     where warehouse.code = t.newwarehousecode
       and status <> 99
       and branchid =  (select id from company where code = '2450')) warehouseid,
   (select id
      from tmpdata.warehousearea150120_1 v
     where v.code = t.newwarehousecode),
   'WT',
   null,
   87,
   1,
   '欧曼库区库位批量处理',
   2,
   1,
   'Admin',
   sysdate
    from (select distinct newwarehousecode
            from tmpdata. om_warehousearea150120 WHERE newwarehousecode IS NOT NULL) t;
            
            
                 insert into  tmpdata.warehousearea150121_1 (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
   (select id
      from warehouse
     where warehouse.code = t.newwarehousecode
       and status <> 99
       and branchid =  (select id from company where code = '2450')) warehouseid,
   ( select V.id
      from tmpdata.warehousearea150121_1 v
      INNER JOIN WAREHOUSE ON V.WAREHOUSEID=WAREHOUSE.ID
     where WAREHOUSE.code = t.newwarehousecode AND V.CODE='WT'),
   'WT',
  ( select V.id
      from tmpdata.warehousearea150121_1 v
      INNER JOIN WAREHOUSE ON V.WAREHOUSEID=WAREHOUSE.ID
     where WAREHOUSE.code = t.newwarehousecode AND V.CODE='WT'),
   87,
   1,
   '欧曼库区库位批量处理',
   3,
   1,
   'Admin',
   sysdate
    from (select distinct newwarehousecode
            from tmpdata. om_warehousearea150120 WHERE newwarehousecode IS NOT NULL) t;
            
            
                          insert into  tmpdata.warehousearea150121_1 (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
   (select id
      from warehouse
     where warehouse.code = t.newwarehousecode
       and status <> 99
       and branchid =  (select id from company where code = '2450')) warehouseid,
   ( select V.id
      from tmpdata.warehousearea150121_1 v
      INNER JOIN WAREHOUSE ON V.WAREHOUSEID=WAREHOUSE.id
     where WAREHOUSE.code = t.newwarehousecode AND V.CODE='JY'),
   'JY',
  ( select V.id
      from tmpdata.warehousearea150121_1 v
      INNER JOIN WAREHOUSE ON V.WAREHOUSEID=WAREHOUSE.id
     where WAREHOUSE.code = t.newwarehousecode AND V.CODE='JY'),
   88,
   1,
   '欧曼库区库位批量处理',
   3,
   1,
   'Admin',
   sysdate
    from (select distinct newwarehousecode
            from tmpdata. om_warehousearea150120 WHERE newwarehousecode IS NOT NULL) t;
            --172
            INSERT INTO warehousearea SELECT * FROM  tmpdata.warehousearea150121_1;
            
            create table tmpdata.warehousearea150130up as 
            select *
              from warehousearea
             where exists (select 1
                      from tmpdata. warehousearea150120_1 v
                     where v.id = warehousearea.id)
               and warehousearea.areakind = 3;
            
            update warehousearea set toplevelwarehouseareaid=parentid 
            where exists(select 1 from tmpdata. warehousearea150120_1 v where v.id=warehousearea.id)
            and warehousearea.areakind=3;
            
            
            
           /* delete from warehousearea where exists(select 1 from  tmpdata.warehousearea150121_1 where  warehousearea150121_1.id=warehousearea.id)
            
    select * from warehousearea where code=   '6A0101101' and warehouseid=662;
select * from warehousearea where code='cs1'
select * from warehousearea where id in(112181,111919)*/
d/*elete from warehousearea t
where exists
 (SELECT * FROM tmpdata.warehousearea150120_1 v where t.id = v.id)
update warehousearea t
   set t.toplevelwarehouseareaid = parentid
 where exists
 (SELECT * FROM tmpdata.warehousearea150120_1 v where t.id = v.id)and  areakind=3；
 update warehousearea t
   set t.areakind=3 where
exists
 (SELECT * FROM tmpdata.warehousearea150121_1 v where t.id = v.id)and areakind=2;
 update warehousearea t
   set t.areakind=2 where
exists
 (SELECT * FROM tmpdata.warehousearea150121_1 v where t.id = v.id)and areakind=1;
 
update tmpdata.warehousearea150120_1 t  set t.toplevelwarehouseareaid = parentid where areakind=3*/
