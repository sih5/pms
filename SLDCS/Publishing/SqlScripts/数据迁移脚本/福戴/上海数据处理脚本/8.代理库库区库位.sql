
create table tmpdata.OM_AGENCYWarehouse150120
(仓库编号 varchar2(90),
 仓库名称 varchar2(90),
 代理库编号 varchar2(90),
 代理库名称 VARCHAR2(90),
 仓库类型 varchar2(90),
 电子邮件 varchar2(90),
 传真 varchar2(90),
 联系人 varchar2(90),
 联系电话 varchar2(90),
 地址 varchar2(90),
 储运中心 varchar2(90),
 是否wms接口 varchar2(10),
 存储策略 varchar2(10),品牌 varchar2(100)
)

create table tmpdata.om_dlkwarehousearea150120(
warehousecode varchar2(100),
warehousename varchar2(100),
storecompanycode varchar2(100),
storecompanyname varchar2(100),
storecompanytype varchar2(100),
warehouseareacode varchar2(100),
locationcode varchar2(100),
AreaCategory varchar2(10),newwarehousecode varchar2(100),newstorecompanycode varchar2(100),brand varchar(10));


create table tmpdata.om_dlkoldnewwarehouserel150120
(oldcode varchar2 (100),oldname varchar2(100),newcode varchar2(100),
newname varchar2(100),brand varchar2(100),remark varchar2(100),
newagentcode varchar2 (100),newagentname varchar2(100),oldagentcode varchar2(100),oldagentname varchar2(100))

  
CREATE TABLE TMPDATA.WAREHOUSE150120dlk as select * from warehouse where rownum<1;

INSERT INTO TMPDATA.WAREHOUSE150120dlk
  (id,
   CODE,
   NAME,
   TYPE,
   STATUS,
   ADDRESS,
   PHONENUMBER,
   CONTACT,
   FAX,
   EMAIL,
   STORAGESTRATEGY,
   BRANCHID,
   STORAGECOMPANYID,
   STORAGECOMPANYTYPE,
   WMSINTERFACE,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_warehouse.nextval,
         newcode,
         newname,
         1,
         1,
         地址,
         联系电话,
         联系人,
         传真,
         电子邮件,
         1,
         (select id from company where code ='2450'),
         (select id from company where code = om_dlkoldnewwarehouserel150120.newagentcode),
         1,
         decode(是否wms接口, '是', 1, '否', 0,0),
         1,
         'Admin',
         sysdate
    from tmpdata.OM_AGENCYWarehouse150120
   inner join tmpdata.om_dlkoldnewwarehouserel150120
      on oldcode = 仓库编号 and brand=品牌 and  oldagentcode=代理库编号;
      insert into warehouse select 　* from TMPDATA.WAREHOUSE150120dlk;
      
      update tmpdata.om_dlkwarehousearea150120 t
         set t.newwarehousecode =
             (select v.newcode from tmpdata.om_dlkoldnewwarehouserel150120 v
               where v.oldcode = t.warehousecode
                 and v.brand = t.brand
                 and v.oldagentcode = t.storecompanycode),t.newstorecompanycode =
             (select v.newagentcode from tmpdata.om_dlkoldnewwarehouserel150120 v
               where v.oldcode = t.warehousecode
                 and v.brand = t.brand
                 and v.oldagentcode = t.storecompanycode);

select count (*)from tmpdata.om_dlkwarehousearea150120 t where t.newwarehousecode is not null and t.newstorecompanycode is not null;

select count (*)from tmpdata.om_dlkwarehousearea150120 t where t.newwarehousecode is not null or t.newstorecompanycode is not null;


create table tmpdata.dlkwarehousearea150120bw_1 as select * from warehousearea where rownum<1;
--114
insert into tmpdata.
dlkwarehousearea150120bw_1
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
         (select id
            from warehouse
           where warehouse.code = t.newwarehousecode and branchid=(select id from company where code='2450')) warehouseid,
         null,
         t.newwarehousecode,
         null,
         89,
         1,
         '欧曼代理库库区库位批量处理',
         1,
         1,
         'Admin',
         sysdate
    from (select distinct newwarehousecode,newstorecompanycode
            from tmpdata. om_dlkwarehousearea150120 t
            
             where exists(select id from company where code =t.newstorecompanycode) and AreaCategory in('保管区','问题区','检验区')) t 
             
 --2235            
insert into tmpdata. dlkwarehousearea150120bw_1
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
         (select id
            from warehouse
           where warehouse.code = t.newwarehousecode
             and branchid = (select id from company where code = '2450')) warehouseid,
         (select id
            from tmpdata.dlkwarehousearea150120bw_1 v
           where v.code = t.newwarehousecode),
         t.warehouseareacode,
         null,
         decode(AreaCategory, '保管区', 89, '问题区', 87, '检验区', 88),
         1,
         '欧曼代理库库区库位批量处理',
         2,
         1,
         'Admin',
         sysdate
    from (select distinct newwarehousecode,
                          warehouseareacode,
                          areacategory,
                          newstorecompanycode
            from tmpdata. om_dlkwarehousearea150120 t
           where exists
           (select id from company where code = t.newstorecompanycode)
             and AreaCategory in ('保管区', '问题区', '检验区')) t

--783656
insert into tmpdata. dlkwarehousearea150120bw_1
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
         (select id
            from warehouse
           where warehouse.code = t.newwarehousecode
             and branchid = (select id from company where code = '2450')) warehouseid,
         (select id
            from tmpdata.dlkwarehousearea150120bw_1 v
           where v.code = t.warehouseareacode
             and v.warehouseid = warehouse.id
             and warehouse.branchid =
                 (select id from company where code = '2450')
             ),
         t.locationcode,
         (select id
            from tmpdata.dlkwarehousearea150120bw_1 v
           where v.code = t.warehouseareacode
             and v.warehouseid = warehouse.id
             and warehouse.branchid =
                 (select id from company where code = '2450')
             ),
         decode(AreaCategory, '保管区', 89, '问题区', 87, '检验区', 88),
         1,
         '欧曼代理库库区库位批量处理',
         3,
         1,
         'Admin',
         sysdate
    from (select distinct newwarehousecode,
                          warehouseareacode,
                          locationcode,
                          AreaCategory,
                          newstorecompanycode
            from tmpdata. om_dlkwarehousearea150120 t
           where exists
           (select id from company where code = t.newstorecompanycode)
             and AreaCategory in ('保管区', '问题区', '检验区')) t
   inner join warehouse
      on warehouse.code = t.newwarehousecode
     and warehouse.branchid = (select id from company where code = '2450');
insert into warehousearea select * from tmpdata. dlkwarehousearea150120bw_1;




  create table tmpdata.warehouseareadlk150121_1 as select * from warehousearea where rownum<1;
     
     insert into  tmpdata.warehouseareadlk150121_1 (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
   (select id
      from warehouse
     where warehouse.code = t.newwarehousecode
       and status <> 99
       and branchid =  (select id from company where code = '2450')) warehouseid,
   (select id
      from tmpdata.dlkwarehousearea150120bw_1 v
     where v.code = t.newwarehousecode),
   'JY',
   null,
   88,
   1,
   '欧曼代理库库区库位批量处理',
   2,
   1,
   'Admin',
   sysdate
    from (select distinct newwarehousecode
            from tmpdata. om_dlkwarehousearea150120 WHERE newwarehousecode IS NOT NULL) t;
            
              insert into  tmpdata.warehouseareadlk150121_1 (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
   (select id
      from warehouse
     where warehouse.code = t.newwarehousecode
       and status <> 99
       and branchid =  (select id from company where code = '2450')) warehouseid,
   (select id
      from tmpdata.dlkwarehousearea150120bw_1 v
     where v.code = t.newwarehousecode),
   'BG',
   null,
   87,
   1,
   '欧曼代理库库区库位批量处理',
   2,
   1,
   'Admin',
   sysdate
    from (select distinct newwarehousecode
            from tmpdata. om_dlkwarehousearea150120 WHERE newwarehousecode IS NOT NULL) t;
            
            
                 insert into  tmpdata.warehouseareadlk150121_1 (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
   (select id
      from warehouse
     where warehouse.code = t.newwarehousecode
       and status <> 99
       and branchid =  (select id from company where code = '2450')) warehouseid,
   ( select V.id
      from tmpdata.warehouseareadlk150121_1 v
      INNER JOIN WAREHOUSE ON V.WAREHOUSEID=WAREHOUSE.ID
     where WAREHOUSE.code = t.newwarehousecode AND V.CODE='BG'),
   'BG',
( select V.id
      from tmpdata.warehouseareadlk150121_1 v
      INNER JOIN WAREHOUSE ON V.WAREHOUSEID=WAREHOUSE.ID
     where WAREHOUSE.code = t.newwarehousecode AND V.CODE='BG'),
   87,
   1,
   '欧曼代理库库区库位批量处理',
   3,
   1,
   'Admin',
   sysdate
    from (select distinct newwarehousecode
            from tmpdata. om_dlkwarehousearea150120 WHERE newwarehousecode IS NOT NULL) t;
            
            
                          insert into  tmpdata.warehouseareadlk150121_1 (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
   (select id
      from warehouse
     where warehouse.code = t.newwarehousecode
       and status <> 99
       and branchid =  (select id from company where code = '2450')) warehouseid,
   ( select V.id
      from tmpdata.warehouseareadlk150121_1 v
      INNER JOIN WAREHOUSE ON V.WAREHOUSEID=WAREHOUSE.id
     where WAREHOUSE.code = t.newwarehousecode AND V.CODE='JY'),
   'JY',
   ( select V.id
      from tmpdata.warehouseareadlk150121_1 v
      INNER JOIN WAREHOUSE ON V.WAREHOUSEID=WAREHOUSE.id
     where WAREHOUSE.code = t.newwarehousecode AND V.CODE='JY'),
   88,
   1,
   '欧曼代理库库区库位批量处理',
   3,
   1,
   'Admin',
   sysdate
    from (select distinct newwarehousecode
            from tmpdata. om_dlkwarehousearea150120 WHERE newwarehousecode IS NOT NULL) t;
            --456
            INSERT INTO warehousearea SELECT * FROM  tmpdata.warehouseareadlk150121_1;
            
          update   tmpdata. dlkwarehousearea150120bw_1 set toplevelwarehouseareaid=parentid where areakind=3;
