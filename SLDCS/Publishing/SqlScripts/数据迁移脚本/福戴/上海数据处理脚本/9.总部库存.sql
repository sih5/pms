create table tmpdata.om_tstores150121 as --动态
select t.locationcode,
       t.areacode,
       t.partcode,
       t.partname,
       t.code warehousecode,
       t.name warehousename,
       t.qty,
       cast('保内' as varchar2(10)) brand,
       cast(null as varchar2(100)) newwarehousecode,
       cast(null as varchar2(100)) newpartcode,
       cast(null as number(19, 4)) oldstdprice,
       cast(null as number(19, 4)) newstdprice
  from tmpdata.tstores_ombnpj t;
       
   
  insert into tmpdata.om_tstores150121 
select t.locationcode,
       t.areacode,
       t.partcode,
       t.partname,
       t.code warehousecode,
       t.name warehousename,
       t.qty,
       '保外',null,null,null,null from 
tmpdata.tstores_ombwpj t;

update tmpdata.om_tstores150121 t
   set newwarehousecode =
       (select newcode
          from tmpdata.om_oldnewwarehouserel150120
         where t.warehousecode = om_oldnewwarehouserel150120.oldcode
           and om_oldnewwarehouserel150120.brand = t.brand);

 create index TMPDATA.OM_TSTORES150121IDX on TMPDATA.OM_TSTORES150121 (BRAND, PARTCODE);
 update tmpdata.om_tstores150121  set newpartcode=null;
 update tmpdata.om_tstores150121
   set newpartcode =
       (select newcode
           from tmpdata.om_newoldpartrel150121
          where om_newoldpartrel150121.oldcode = om_tstores150121.partcode
            and om_tstores150121.brand = om_newoldpartrel150121.brand);
update tmpdata.om_tstores150121 set newpartcode=partcode
where exists(select 1 from sparepart where sparepart.code=partcode) and newpartcode is null;
update tmpdata.om_tstores150121 t
   set oldstdprice =
       (select distinct v.stprice
          from tmpdata.om_purchasesaleprice150121 v
         where v.code = t.partcode
           and v.brandname = t.brand),
       newstdprice =
       (select distinct v.stprice
          from tmpdata.om_purchasesaleprice150121 v
         where v.code = t.newpartcode
           and v.brandname = t.brand);
           
           select * from tmpdata.om_tstores150121 where partcode='RH1100CK2BZFT-1A6888'

select *
  from tmpdata.om_tstores150121
 where newpartcode is null
   and newwarehousecode is not null
           
           select count(*) from tmpdata.om_tstores150121  where newwarehousecode is not null
\
--153711
  select count(*) from tmpdata.om_tstores150121 t
   inner join warehouse on warehouse.code=t.newwarehousecode
   inner join sparepart on sparepart.code = t.newpartcode 
   where exists (select warehousearea.id
           from warehousearea        
          inner join warehousearea parent
             on parent.id = warehousearea.parentid
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                  and parent.code = t.areacode
                  and warehousearea.areakind = 3);
   
   select * from warehousearea where code='4B0000000'
   select *
     from warehousearea
    inner join warehousearea parent
       on parent.id = warehousearea.parentid
    where warehousearea.warehouseid = 662 and warehousearea.code='4B0000000' and parent.code='4B'
             
create table tmpdata.partsstock150121 as select * from partsstock where rownum<1;

insert into tmpdata.partsstock150121
  (id,
   warehouseid,
   storagecompanyid,
   storagecompanytype,
   branchid,
   warehouseareaid,
   warehouseareacategoryid,
   partid,
   quantity,
   creatorid,
   creatorname,
   createtime)
 select s_partsstock.nextval,
        warehouse.id,
        (select id from company where code='2450'),
        1,
        (select id from company where code='2450'),
        (select warehousearea.id
           from warehousearea        
          inner join warehousearea parent
             on parent.id = warehousearea.parentid
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                  and parent.code = t.areacode
                  and warehousearea.areakind = 3),
        89,
        sparepart.id,       
        t.qty,
        1,
        'Admin',
        sysdate
   from tmpdata.om_tstores150121  t
   inner join warehouse on warehouse.code=t.newwarehousecode
   inner join sparepart on sparepart.code = t.newpartcode
  where exists (select warehousearea.id
           from warehousearea        
          inner join warehousearea parent
             on parent.id = warehousearea.parentid
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                  and parent.code = t.areacode
                  and warehousearea.areakind = 3);
                  
                /*     select count(*) from tmpdata.om_tstores150121 t
      inner join warehouse on warehouse.code=t.newwarehousecode
   where newpartcode is null and qty>0   
select * from tmpdata.om_tstores150121 where qty>0
                select *  from tmpdata.om_tstores150121 t where t.newpartcode is null*/
--delete from partsstock where exists(select 1 from tmpdata.partsstock150121 where partsstock150121.id=partsstock.id);


create index tmpdata.PARTSSTOCK150121_idx on TMPDATA.PARTSSTOCK150121 (warehouseid, warehouseareaid, partid);
update tmpdata.partsstock150121 v
   set v.quantity =
       (select sum(quantity)
          from tmpdata.partsstock150121 a
         where a.warehouseid = v.warehouseid
           and a.warehouseareaid = v.warehouseareaid
           and a.partid = v.partid)
 where exists (select *
          from (select t.warehouseid, t.warehouseareaid, t.partid
                  from tmpdata.partsstock150121 t
                  
                 group by t.warehouseid, t.warehouseareaid, t.partid
                having count(partid) > 1) t
         where t.warehouseid = v.warehouseid
           and t.partid = v.partid
           and t.warehouseareaid = v.warehouseareaid);
    
delete tmpdata.partsstock150121 
 where partsstock150121.rowid not in
       (select min(partsstock150121.rowid)
          from tmpdata.partsstock150121  
         group by warehouseid, warehouseareaid, partid)  ;

select from  tmpdata.om_tstores150121 t where t.newwarehousecode is not null and t.newpartcode is not null
group by t.newwarehousecode,t.newpartcode


-----------------------------
--95993
insert into partsstock select * from tmpdata.partsstock150121;
select sum(quantity) from tmpdata.partsstock150121;
select warehouse.code warehousecode,
       warehouse.name warehousename,
       (select code
          from warehousearea
         where t.warehouseareaid = warehousearea.id) warehouseareacode,
       (select code from sparepart where id = t.partid) partcode
  from tmpdata.partsstock150121 t
 inner join warehouse
    on warehouse.id = t.warehouseid
 inner join tmpdata.om_oldnewwarehouserel150120 b
    on b.newcode = warehouse.code
 inner join partssalescategory
    on partssalescategory.name =
       decode(b.brand, '保内', '欧曼', '保外', '欧曼保外')
 where not exists (select *
          from tmpdata.stdprice150121_1 v
         where v.partssalescategoryid = partssalescategory.id
           and t.partid = v.sparepartid) and t.quantity>0;

select stprice, tmpdata.om_purchasesaleprice150121

create table tmpdata.om_tstores150121check as
select t.*,
       (select distinct v.stprice from tmpdata.om_purchasesaleprice150121 v where v.code = t.partcode and v.brandname = t.brand) oldstdprice,
       (select distinct v.stprice from tmpdata.om_purchasesaleprice150121 v where v.code = t.newpartcode and v.brandname = t.brand) newstdprice
  from tmpdata.om_tstores150121 t
 inner join warehouse
    on warehouse.code = t.newwarehousecode
 where t.qty > 0
   and exists (select warehousearea.id
          from warehousearea
        /* inner join warehousearea parent
        on parent.id = warehousearea.parentid*/
         where warehouse.id = warehousearea.warehouseid
           and warehousearea.code = t.locationcode
              --  and parent.code = t.areacode
           and warehousearea.areakind = 3)
   and exists
 (select 1 from sparepart where sparepart.code = t.newpartcode);

select t.*
from tmpdata.om_tstores150121  t 
inner join warehouse on warehouse.code=t.newwarehousecode
where t.qty>0
and not  exists
 (select 1 from sparepart where sparepart.code = t.newpartcode);
 
select t.* from tmpdata.om_tstores150121 t
 inner join warehouse
    on warehouse.code = t.newwarehousecode
 inner join partssalescategory
    on partssalescategory.name =
       decode(t.brand, '保内', '欧曼', '保外', '欧曼保外')
 where not exists (select *
          from tmpdata.stdprice150121_1 v inner join sparepart b on b.id=v.sparepartid
         where v.partssalescategoryid = partssalescategory.id
           and t.newpartcode = b.code) and t.qty>0
              and exists (select warehousearea.id
          from warehousearea
         inner join warehousearea parent
        on parent.id = warehousearea.parentid
         where warehouse.id = warehousearea.warehouseid
           and warehousearea.code = t.locationcode
               and parent.code = t.areacode
           and warehousearea.areakind = 3)
           and exists
 (select 1 from sparepart where sparepart.code = t.newpartcode) and nvl(t.oldstdprice,0)<>nvl(t.oldstdprice,0);
 



select t.*
from tmpdata.om_tstores150121  t 
inner join warehouse on warehouse.code=t.newwarehousecode
where t.qty>0
and not exists (select warehousearea.id
          from warehousearea
         inner join warehousearea parent
        on parent.id = warehousearea.parentid
         where warehouse.id = warehousearea.warehouseid
           and warehousearea.code = t.locationcode
               and parent.code = t.areacode
           and warehousearea.areakind = 3);
           select * from warehousearea where code like'%E+%'
select * from             TMPDATA.om_warehousearea150120 where locationcode='3E000007'
   /*
   inner join sparepart on sparepart.code = t.newpartcode
  where exists (select warehousearea.id
           from warehousearea        
         \* inner join warehousearea parent
             on parent.id = warehousearea.parentid*\
           where warehouse.id = warehousearea.warehouseid
                  and warehousearea.code = t.locationcode
                --  and parent.code = t.areacode
                  and warehousearea.areakind = 3);
                  
                  */
                  
                  
                 select t.* from tmpdata.om_tstores150121 t
 inner join warehouse
    on warehouse.code = t.newwarehousecode
 where t.qty > 0
   and exists (select warehousearea.id
          from warehousearea
        /* inner join warehousearea parent
        on parent.id = warehousearea.parentid*/
         where warehouse.id = warehousearea.warehouseid
           and warehousearea.code = t.locationcode
              --  and parent.code = t.areacode
           and warehousearea.areakind = 3)
   and exists
 (select 1 from sparepart where sparepart.code = t.newpartcode) and nvl(t.newstdprice,0)<>nvl(t.oldstdprice,0);



  select t.* from tmpdata.om_tstores150121 t
 inner join warehouse
    on warehouse.code = t.newwarehousecode
 where t.qty > 0
   and exists (select warehousearea.id
          from warehousearea
        /* inner join warehousearea parent
        on parent.id = warehousearea.parentid*/
         where warehouse.id = warehousearea.warehouseid
           and warehousearea.code = t.locationcode
              --  and parent.code = t.areacode
           and warehousearea.areakind = 3) and t.newpartcode='SG70*95*12A0633A'
           select * from sparepart where code='SG70*95*12A0633A'
           select * from partsplannedprice where partsplannedprice.sparepartid=165319
           
           select * from  tmpdata.stdprice150121_1 inner join sparepart s on s.id=stdprice150121_1.sparepartid where s.code='SG70*95*12A0633A';
   select *         from (select distinct a.code,a.stprice,brandname from tmpdata.om_purchasesaleprice150121 a) t
 inner join sparepart s
    on s.code = t.code where s.code='SG70*95*12A0633A';
