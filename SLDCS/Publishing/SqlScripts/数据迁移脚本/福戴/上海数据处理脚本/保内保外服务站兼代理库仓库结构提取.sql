--(欧曼保外-20150129)服务站库区库位
--33457
create table tmpdata.tWareLocation_OMBW_150129
as select --品牌,
       code 仓库编号,
       name 仓库名称,
       仓储企业编号,
       仓储企业名称,
       仓储企业类型,
       库区编号,
       库位编号,
       --decode(库位编号, '', '库区', '库位') 库区库位类型,
       库区用途
  from (select --(select name from ft.tbrands where tbrands.objid = tstationlists.brandid) as 品牌,
               twarehouses.code,
               twarehouses.name,
               tstations.code 仓储企业编号,
               tstations.name 仓储企业名称,
               '服务站' 仓储企业类型,
               tareafields.code as 库区编号,
               tlocations.code as 库位编号,
               --'' 库区库位类型,
               '保管区' 库区用途
          from ft.twarehouses@wservice_65
         inner join ft.tareas@wservice_65
            on tareas.warehouseid = twarehouses.objid
         inner join ft.tareafields@wservice_65
            on tareafields.areaid = tareas.objid
         inner join ft.tlocations@wservice_65
            on tlocations.areafieldid = tareafields.objid
         inner join ft.tstations@wservice_65
            on tstations.objid = twarehouses.entercode
         /*inner join ft.tstationlists
            on tstationlists.stationid = tstations.objid*/
         where twarehouses.isvalid > 0
           and tareas.isvalid > 0
           and tAreafields.Isvalid >0
           and tlocations.isvalid > 0
           and tstations.isvalid > 0
         /*  and tstationlists.brandid =
               (select objid from ft.tbrands where tbrands.name = '欧曼')*/
           and tStations.Code in ('CHQ00011',
'ANH00092',
'BEJ00073',
'FDJIL002',
'FDGUD002',
'GUD00099',
'FDHEB008',
'HEB00213',
'HEB00242',
'GUD00147',
'HEB00104',
'HEN00033',
'HEB00146',
'JIS00082',
'JIS00206',
'LIN00156',
'LIN00158',
'HEB00164',
'LIN00159',
'NEM00104',
'HEN00089',
'NEM00024',
'NIX00005',
'SIC00118',
'QIH00016',
'SIC00130',
'SHD00331',
'XIJ00077',
'XIJ00072',
'YUN00053',
'XIZ00016',
'YUN00045',
'ZHJ00141'))



--（欧曼保外-20150129）服务站仓库信息
--16
create table  tmpdata.tWarehouses_OMBW_150129
as
select twarehouses.code 仓库编号,
       twarehouses.name 仓库名称,
       '分库' as 仓库类型,
       ' ' as 分公司编号,
       ' ' as 分公司名称,
       tstations.code as 仓储企业编号,
       tstations.name as 仓储企业名称,
       twarehouses.E_mail,
       twarehouses.Fax,
       twarehouses.Linker,
       twarehouses.Tel,
       twarehouses.Address,
       '定位存储' as 存储策略,
       ' ' as 储运中心,
       ' ' as 是否与WMS接口对接
  from ft.twarehouses@wservice_65
 inner join ft.tstations@wservice_65
    on tstations.objid = twarehouses.entercode
 /*inner join ft.tstationlists@wservice_65
    on tstationlists.stationid = tstations.objid*/
 where twarehouses.isvalid > 0
   and tstations.isvalid > 0
 /*  --and tstationlists.isvalid>0
   and tstationlists.brandid =
       (select objid from ft.tbrands where tbrands.name = '欧曼')*/
   and tstations.code in('CHQ00011',
'ANH00092',
'BEJ00073',
'FDJIL002',
'FDGUD002',
'GUD00099',
'FDHEB008',
'HEB00213',
'HEB00242',
'GUD00147',
'HEB00104',
'HEN00033',
'HEB00146',
'JIS00082',
'JIS00206',
'LIN00156',
'LIN00158',
'HEB00164',
'LIN00159',
'NEM00104',
'HEN00089',
'NEM00024',
'NIX00005',
'SIC00118',
'QIH00016',
'SIC00130',
'SHD00331',
'XIJ00077',
'XIJ00072',
'YUN00053',
'XIZ00016',
'YUN00045',
'ZHJ00141')




create table tmpdata.tWareLocation_OM_150129
as 
select --品牌,
       code 仓库编号,
       name 仓库名称,
       仓储企业编号,
       仓储企业名称,
       仓储企业类型,
       库区编号,
       库位编号,
       --decode(库位编号, '', '库区', '库位') 库区库位类型,
       库区用途
  from (select --(select name from ft.tbrands where tbrands.objid = tstationlists.brandid) as 品牌,
               twarehouses.code,
               twarehouses.name,
               tstations.code 仓储企业编号,
               tstations.name 仓储企业名称,
               '服务站' 仓储企业类型,
               tareafields.code as 库区编号,
               tlocations.code as 库位编号,
               --'' 库区库位类型,
               '保管区' 库区用途
          from ft.twarehouses@oservice_65
         inner join ft.tareas@oservice_65
            on tareas.warehouseid = twarehouses.objid
         inner join ft.tareafields@oservice_65
            on tareafields.areaid = tareas.objid
         inner join ft.tlocations@oservice_65
            on tlocations.areafieldid = tareafields.objid
         inner join ft.tstations@oservice_65
            on tstations.objid = twarehouses.entercode
     /*    inner join ft.tstationlists
            on tstationlists.stationid = tstations.objid*/
         where twarehouses.isvalid > 0
           and tareas.isvalid > 0
           and tAreafields.Isvalid >0
           and tlocations.isvalid > 0
           and tstations.isvalid > 0
           /*and tstationlists.brandid =
               (select objid from ft.tbrands where tbrands.name = '欧曼')*/
           and tStations.Code in ('ANH00092',
'BEJ00073',
'FDJIL002',
'CHQ00011',
'GUD00099',
'GUD00147',
'HEB00104',
'HEN00033',
'HEB00146',
'JIS00082',
'HEB00164',
'HEN00089',
'NEM00104',
'NEM00024',
'NIX00005',
'QIH00016',
'SIC00118',
'SIC00130',
'XIJ00077',
'XIZ00016',
'YUN00053',
'YUN00045'));



 create table  tmpdata.tWarehouses_OM_150129
as

select twarehouses.code 仓库编号,
       twarehouses.name 仓库名称,
       '分库' as 仓库类型,
       ' ' as 分公司编号,
       ' ' as 分公司名称,
       tstations.code as 仓储企业编号,
       tstations.name as 仓储企业名称,
       twarehouses.E_mail,
       twarehouses.Fax,
       twarehouses.Linker,
       twarehouses.Tel,
       twarehouses.Address,
       '定位存储' as 存储策略,
       ' ' as 储运中心,
       ' ' as 是否与WMS接口对接
  from ft.twarehouses@oservice_65
 inner join ft.tstations@oservice_65
    on tstations.objid = twarehouses.entercode
 /*inner join ft.tstationlists
    on tstationlists.stationid = tstations.objid*/
 where twarehouses.isvalid > 0
   and tstations.isvalid > 0
/*   and tstationlists.isvalid>0
   and tstationlists.brandid =
       (select objid from ft.tbrands where tbrands.name = '欧曼')*/
   and tstations.code in('ANH00092',
'BEJ00073',
'FDJIL002',
'CHQ00011',
'GUD00099',
'GUD00147',
'HEB00104',
'HEN00033',
'HEB00146',
'JIS00082',
'HEB00164',
'HEN00089',
'NEM00104',
'NEM00024',
'NIX00005',
'QIH00016',
'SIC00118',
'SIC00130',
'XIJ00077',
'XIZ00016',
'YUN00053',
'YUN00045')
