Create table tmpdata.toldpartwarehousearea140708
(
  warehousecode varchar2(20),
  warehousename varchar2(40),
  filialecode varchar2(20),
  filialename varchar2(50),
  areacode varchar2(100),
  locationcode varchar2(100),
  type varchar2(20),
  StorageCategory varchar2(20)
)
