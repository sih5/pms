update tmpdata.toldpartwarehousearea140708 set toldpartwarehousearea140708.filialecode=1101,toldpartwarehousearea140708.filialename='北汽福田汽车股份有限公司北京配件销售分公司';
select * from branch where id=3007
--drop table tmpdata.UsedPartsWarehouseArea140430

create table tmpdata.toldpartwarehousearea140708_1 as 
select * from dcs.UsedPartsWarehouseArea where rownum<1;

insert into tmpdata.toldpartwarehousearea140708_1
  (Id,
   UsedPartsWarehouseId,
   ParentId,
   TopLevelUsedPartsWhseAreaId,
   Code,
   IfDefaultStoragePosition,
   StorageCategory,
   Status,
   StorageAreaType,
   CreatorName,
   CreateTime)
  select s_Usedpartswarehousearea.nextval as id,
         Usedpartswarehouse.id as UsedPartsWarehouseId,
         null,
         null,
         temp.warehousecode code,
         0,
         1 as StorageCategory,
         1 as status,
         1 as StorageAreaType,
         'admin' as CreatorName,
         sysdate as CreateTime

    from (select distinct warehousecode,filialename
            from tmpdata.toldpartwarehousearea140708) temp
   inner join yxdcs.Usedpartswarehouse
      on UsedPartsWarehouse.code = temp.warehousecode
   inner join yxdcs.branch
     on branch.id = UsedPartsWarehouse.Branchid
     and branch.name = temp.filialename;

insert into tmpdata.toldpartwarehousearea140708_1
  (Id,
   UsedPartsWarehouseId,
   ParentId,
   TopLevelUsedPartsWhseAreaId,
   Code,
   IfDefaultStoragePosition,
   StorageCategory,
   Status,
   StorageAreaType,
   CreatorName,
   CreateTime)
  select s_Usedpartswarehousearea.nextval as id,
         Usedpartswarehouse.id as UsedPartsWarehouseId,
         (select id
            from tmpdata.toldpartwarehousearea140708_1
           where toldpartwarehousearea140708_1.Usedpartswarehouseid =
                 Usedpartswarehouse.id
             and toldpartwarehousearea140708_1.Code = temp.warehousecode),
        null,
         temp.areacode code,
         0,
         1 as StorageCategory,
         1 as status,
         2 as StorageAreaType,
         'admin' as CreatorName,
         sysdate as CreateTime  
    from (select distinct warehousecode, areacode, filialename
            from tmpdata.toldpartwarehousearea140708) temp
   inner join yxdcs.Usedpartswarehouse
      on UsedPartsWarehouse.code = temp.warehousecode
   inner join yxdcs.branch
      on branch.id = UsedPartsWarehouse.Branchid
     and branch.name = temp.filialename;

insert into tmpdata.toldpartwarehousearea140708_1
  (Id,
   UsedPartsWarehouseId,
   ParentId,
   TopLevelUsedPartsWhseAreaId,
   Code,
   IfDefaultStoragePosition,
   StorageCategory,
   Status,
   StorageAreaType,
   CreatorName,
   CreateTime)
  select s_Usedpartswarehousearea.nextval as id,
         Usedpartswarehouse.id as UsedPartsWarehouseId,
         (select id
            from tmpdata.toldpartwarehousearea140708_1
           where toldpartwarehousearea140708_1.Usedpartswarehouseid =
                 Usedpartswarehouse.id
             and toldpartwarehousearea140708_1.Code = temp.areacode and toldpartwarehousearea140708_1.StorageAreaType=2) as ParentId,
         (select id
            from tmpdata.toldpartwarehousearea140708_1
           where toldpartwarehousearea140708_1.Usedpartswarehouseid =
                 Usedpartswarehouse.id
             and toldpartwarehousearea140708_1.Code = temp.areacode and toldpartwarehousearea140708_1.StorageAreaType=2) as TopLevelUsedPartsWhseAreaId,
         temp.locationcode as Code,
         0 as IfDefaultStoragePosition,
         1 as StorageCategory,
         1 as status,
         3 as StorageAreaType,
         'admin' as CreatorName,
         sysdate as CreateTime
    from tmpdata.toldpartwarehousearea140708 temp
   inner join yxdcs.Usedpartswarehouse
      on UsedPartsWarehouse.code = temp.warehousecode
   inner join yxdcs.branch
      on branch.id = UsedPartsWarehouse.Branchid
     and temp.filialename = branch.name
   where temp.type = '库位';
--340
insert into UsedPartsWarehouseArea
  select * from tmpdata.toldpartwarehousearea140708_1;
