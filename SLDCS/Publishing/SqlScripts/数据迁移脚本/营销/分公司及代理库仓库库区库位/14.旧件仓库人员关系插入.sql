create table tmpdata.UsedPartsWarehouseStaff140708 as select * from UsedPartsWarehouseStaff;
insert into tmpdata.UsedPartsWarehouseStaff140708
  (id,
   usedpartswarehouseid,
   personnelid,
   creatorid,
   creatorname,
   createtime)
  select s_UsedPartsWarehouseStaff.Nextval,
         (select id
            from yxdcs.usedpartswarehouse v
           where v.code = t.warehousecode),
         (select id
            from yxsecurity.personnel v
           where v.loginid = t.personnelcode
             and v.enterpriseid = 2),
         1,
         'Admin',
         sysdate
    from tmpdata.UsedPartsWM140708 t
 where exists (select id
            from yxsecurity.personnel v
           where v.loginid = t.personnelcode
             and v.enterpriseid = 2);
             
             select * from tmpdata.UsedPartsWM140708 t where  not exists (select id
            from yxsecurity.personnel v
           where v.loginid = t.personnelcode
             and v.enterpriseid = 2);
            insert into UsedPartsWarehouseStaff select * from tmpdata.UsedPartsWarehouseStaff140708;
