create table tmpdata.WarehouseOperator140708  as select * from WarehouseOperator where rownum<1;
insert into tmpdata.WarehouseOperator140708
  (id, warehouseid, operatorid, creatorid, creatorname, createtime)
  select S_WarehouseOperator.Nextval,
         (select id
            from yxdcs.warehouse v
           where v.code = t.warehousecode
             and branchid = 2),
         (select id
            from yxsecurity.personnel v
           where v.loginid = t.personnelcode
             and v.enterpriseid = 2),
         1,
         'Admin',
         sysdate
    from tmpdata.WM140708 t
where exists(select id
            from yxsecurity.personnel v
           where v.loginid = t.personnelcode
             and v.enterpriseid = 2);
             
             insert into WarehouseOperator select * from tmpdata.WarehouseOperator140708;
