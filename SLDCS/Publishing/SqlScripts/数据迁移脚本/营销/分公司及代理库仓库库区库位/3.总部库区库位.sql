create table tmpdata.warehousearea140707_1 as select * from warehousearea where rownum<1;
--47
insert into tmpdata.
warehousearea140707_1
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
         (select id
            from warehouse
           where warehouse.code = t.warehousecode and status<>99 and branchid=2) warehouseid,
         null,
         t.warehousecode,
         null,
         89,
         1,
         '营销库区库位批量处理',
         1,
         1,
         'Admin',
         sysdate
    from (select distinct warehousecode
            from yxdcs. warehousearea140707) t where exists(select id
            from warehouse
           where warehouse.code = t.warehousecode and status<>99 and branchid=2)
            
            --select * from 
           
   insert into tmpdata.
warehousearea140707_1
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
   (select id
      from warehouse
     where warehouse.code = t.warehousecode
       and status <> 99
       and branchid = 2) warehouseid,
   (select id
      from tmpdata.warehousearea140707_1 v
     where v.code = t.warehousecode),
   t.warehouseareacode,
   null,
   decode(AreaCategory, '保管区', 89, '问题区', 87, '检验区', 88),
   1,
   '营销库区库位批量处理',
   2,
   1,
   'Admin',
   sysdate
    from (select distinct warehousecode, warehouseareacode, areacategory
            from yxdcs. warehousearea140707) t
   where exists (select id
            from warehouse
           where warehouse.code = t.warehousecode
             and status <> 99
             and branchid = 2)
            
insert into tmpdata. warehousearea140707_1
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   remark,
   areakind,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
         warehouse.id,
         (select id
            from tmpdata.warehousearea140707_1 v
            where v.code = t.warehouseareacode and v.warehouseid=warehouse.id),
         t.locationcode,
         (select id
            from tmpdata.warehousearea140707_1 v
           where v.code = t.warehouseareacode and v.warehouseid=warehouse.id),
         decode(AreaCategory ,'保管区',89,'问题区',87,'检验区',88),
         1,
         t.remark,
         3,
         1,
         'Admin',
         sysdate
    from  warehousearea140707 t
    inner join warehouse on warehouse.code=t.warehousecode and warehouse.status<>99 and branchid=2
    --6316
   insert into  warehousearea select * from  tmpdata. warehousearea140707_1 t
   
   
   create table tmpdata.PartsStock140620zk_1 as select * from dcs.PartsStock where rownum<1;
    insert into tmpdata.PartsStock140620zk_1(id,
   warehouseid,
   storagecompanyid,
   storagecompanytype,
   branchid,
   warehouseareaid,
   warehouseareacategoryid,
   partid,
   quantity,
   remark,
   creatorid,
   creatorname,
   createtime)
   select S_PartsStock.Nextval,
         warehouse.id,3007,
         1,
         3007,(select id
            from warehousearea s
           where s.warehouseid = warehouse.id
             and s.code = t.locationcode),89,(select id from dcs.sparepart where code=t.partcode),200,'营销库存批量导入', 1,
         'Admin',
         sysdate
           from tmpdata. partstock140620 t inner join warehouse on warehouse.code=t.warehousecode and warehouse.status<>99   and branchid=3007 where  exists(select id from dcs.sparepart where code=t.partcode)
   insert  into PartsStock select * from  tmpdata.PartsStock140620zk_1;
   
   select t.* from  tmpdata. partstock140620 t inner join warehouse on warehouse.code=t.warehousecode and warehouse.status<>99
    where not exists(select id
            from tmpdata.warehousearea140620_1 s
           where s.warehouseid = warehouse.id
             and s.code = t.locationcode)
   select * from dcs.warehousearea t where t.code='1BP11132'
   
   /*
   create table tmpdata.PartsStock140506zk_1 as select * from dcs.PartsStock where rownum<1;
   
   insert into tmpdata.PartsStock140506zk_1(id,
   warehouseid,
   storagecompanyid,
   storagecompanytype,
   branchid,
   warehouseareaid,
   warehouseareacategoryid,
   partid,
   quantity,
   remark,
   creatorid,
   creatorname,
   createtime)
   select S_PartsStock.Nextval,
         (select id
            from warehouse v
           where v.code = t.warehousecode
             and branchid =3007),
         3007,
         1,
         3007,
         (select id
            from tmpdata.warehousearea140607 s
           where s.warehouseid = warehouse.id
             and s.code = t.locationcode),
         89,
        partbranch.partid ,
         2000,
         '营销库存批量处理',
         1,
         'Admin',
         sysdate
          from （select distinct warehousecode
            from tmpdata. warehousearea140607） t
            inner join warehouse on warehouse.code=t.warehousecode and warehouse<>99   and branchid=3007
            inner join partbranch on warehouse.branchid=partbranch.branchid*/
   /*
--select * from WarehouseAreaCategory
update warehousearea set areakind=3 where remark= '欧曼库区库位批量处理' and parentid is not null and toplevelwarehouseareaid is not null

select * from tmpdata.warehousearea140505_1 where code in(
select code from tmpdata.warehousearea140505_1 group by warehouseid ,code having count (code)>1) and warehouseid=603 for update
select *
  from warehousearea
 where remark = '欧曼库区库位批量处理'
   and not exists (select 1
          from tmpdata. warehousearea140505_1 t
         where t.id = warehousearea.id)and warehouseid in (603,604) for update
--
insert into tmpdata.PartsStock140506zk_1
  (id,
   warehouseid,
   storagecompanyid,
   storagecompanytype,
   branchid,
   warehouseareaid,
   warehouseareacategoryid,
   partid,
   quantity,
   remark,
   creatorid,
   creatorname,
   createtime)
  select S_PartsStock.Nextval,
         (select id
            from warehouse v
           where v.code = t.warehousecode
             and branchid = (select id from dcs.branch where code = '2450')),
         (select id from company where code = '2450'),
         1,
         (select id from dcs.branch where code = '2450'),
         (select id
            from tmpdata.warehousearea140505_1 s
           where s.warehouseid = warehouse.id
             and s.code = t.locationcode),
         89,
         (select id from dcs.sparepart v where v.code = t.partcode),
         t.usableqty,
         '欧曼库存批量处理',
         1,
         'Admin',
         sysdate
    from tmpdata. partsstock140505zb t
   inner join warehouse
      on warehouse.code = t.warehousecode
     and warehouse.branchid =
         (select id from dcs.branch where code = '2450')
   where t.partcode is not null
     and  exists (select id
            from tmpdata.warehousearea140505_1 s
           where s.warehouseid = warehouse.id
             and s.code = t.locationcode);
*/
delete from warehousearea140620_1

delete tmpdata.warehousearea140620_1
 where tmpdata.warehousearea140620_1.rowid not in
       (select min(tmpdata.warehousearea140620_1.rowid)
          from tmpdata.warehousearea140620_1
         group by warehouseid,parentid, code,areacategoryid,areakind);

--1204
delete warehousearea
 where warehousearea.rowid not in
       (select min(warehousearea.rowid)
          from warehousearea
         group by warehousearea.warehouseid,parentid, code,areacategoryid,areakind);
