--select * from dcs.product_temp

--创建导入临时表
create table tmpdata.product140705 as select * from product where rownum<0;
--select count(*)  from tmpdata.TSTDPRODUCTIN140429 t  where MainBrandCode is null and  SubBrandCode is null  and  PlatFormCode is null and PKind is null for update
 --  where not exists
 --  (select 1 from dcs.product where t.ProductCode = product.code);
 select distinct t.品牌代码 || t.子品牌代码 || t.平台代码 || t.产品线代码
  from tmpdata.tstdproductin140705 t
  
 
--插入导入临时表 插入31451条
insert into tmpdata.product140705
  (ID,
   CODE,
   PRODUCTCATEGORYCODE,
   PRODUCTCATEGORYNAME,
   BRANDID,
   BRANDCODE,
   BRANDNAME,
   SUBBRANDCODE,
   SUBBRANDNAME,
   TERRACECODE,
   TERRACENAME,
   PRODUCTLINE,
   PRODUCTNAME,
   OLDINTERNALCODE,
   ANNOUCEMENTNUMBER,
   TONNAGECODE,
   ENGINETYPECODE,
   ENGINEMANUFACTURERCODE,
   GEARSERIALTYPECODE,
   GEARSERIALMANUFACTURERCODE,
   REARAXLETYPECODE,
   REARAXLEMANUFACTURERCODE,
   TIRETYPECODE,
   TIREFORMCODE,
   BRAKEMODECODE,
   DRIVEMODECODE,
   FRAMECONNETCODE,
   VEHICLESIZE,
   AXLEDISTANCECODE,
   EMISSIONSTANDARDCODE,
   PRODUCTFUNCTIONCODE,
   PRODUCTLEVELCODE,
   VEHICLETYPECODE,
   COLORDESCRIBE,
   SEATINGCAPACITY,
   WEIGHT ,STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_product.nextval,
         t.产品编号 as 整车编码,
         '#N/A ',
         '#N/A ' as 车型名称,
         nvl(t.品牌代码 || t.子品牌代码 || t.平台代码 || t.产品线代码 /*||ProductFunctionCode*/,0) as brandid,
         t.品牌代码 as 品牌代码,
         t.品牌名称 as 品牌名称,
         t.子品牌代码 as 子品牌代码,
         t.子品牌名称 as 子品牌名称,
         t.平台代码 as 平台代码,
         t.平台名称 as 平台名称,
         t.产品线代码 as 产品线代码,
         t.产品线名称 as 产品线名称,
         substrb(t.原始内部编号,1,50) as 原始内部编号,
         t.公告号 as 公告号,
         t.吨位代码 as 吨位代码,
         t.发动机型号代码 as 发动机型号代码,
         t.发动机生产厂家代码 as 发动机生成厂家代码,
         t.变速箱型号代码 as 变速箱型号代码,
         t.变速箱生产厂家代码 as 变速箱生成厂家代码,
         t.后桥型号代码 as 后桥型号代码,
         t.后桥生产厂家代码 as 后桥生产厂家代码,
         t.轮胎型号代码 as 轮胎型号代码,
         t.轮胎形式代码 as 轮胎形式代码,
         t.制动方式代码 as 制动方式代码,
         t.驱动方式代码 as 驱动方式代码,
         t.车架连接形式代码 as 车架连接形式代码,
         0 as 整车尺寸,
         t.轴距代码 as 轴距代码,
         t.排放标准代码 as 排放标准代码,
         t.产品功能代码 as 产品功能代码,
         t.产品档次代码 as 产品档次代码,
         t.车辆类型代码 as 车辆类型代码,
         t.颜色描述 as 颜色描述,
         t.载客个数 as 载客个数,
         t.自重 as 自重,
         1,
         1,
         'Admin',
         sysdate
    from tmpdata.TSTDPRODUCTIN140705 t
   /*where not exists
   (select 1 from dcs.product where t.ProductCode = product.code) 
   and ((MainBrandCode is not null) or ( SubBrandCode is not null)  or  (PlatFormCode is not null) or (PKind is not null));*/

select * from tmpdata.TSTDPRODUCTIN140705 t where((t.品牌代码 is  null) or ( t.子品牌代码 is  null)  or  (t.平台代码 is  null) or (t.产品线代码 is  null))

--插入产品表
--select brandcode||subbrandcode||terracecode||productline from product_temp
insert into  product select * from tmpdata.product140705;

--创建产品产品线关系临时表
create table tmpdata.ServiceProdLineProduct140430 as select * from ServiceProdLineProduct where rownum<0;

--插入条
insert into tmpdata.ServiceProdLineProduct140430 (id,Brandid,Vehiclebrandcode,Vehiclebrandname,Subbrandcode,Subbrandname,Terracecode,Terracename,Vehicleproductline,Vehicleproductname/*,ProductFunctionCode*/)
select s_ServiceProdLineProduct.Nextval,
       v.brandid,
       v.brandcode,
       v.brandname,
       v.subbrandcode,
       v.subbrandname,
       v.terracecode,
       v.terracename,
       v.productline,
       v.productname/*,
       v.ProductFunctionCode*/
  from (select distinct brandid,
                        BRANDCODE,
                        BRANDNAME,
                        SUBBRANDCODE,
                        SUBBRANDNAME,
                        TERRACECODE,
                        TERRACENAME,
                        PRODUCTLINE,
                        PRODUCTNAME,
                        ProductFunctionCode
          from tmpdata.product140430) v where not exists(select 1 from dcs.ServiceProdLineProduct
          where v.brandid=ServiceProdLineProduct.Brandid);
select *from ServiceProdLineProduct140430 for update
--插入产品产品线关系表
insert into ServiceProdLineProduct SELECT * FROM tmpdata.ServiceProdLineProduct140430;


tpurchaseinfos2012temp
