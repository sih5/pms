--创建车辆信息临时表
create table TMPDATA.VehicleWarrantyCard140709 as
select * from yxdcs.vehiclewarrantycard where rownum<0;
create index VehicleWarrantyCard140707_IDX on tmpdata.VehicleWarrantyCard140707 (vin);
create table tmpdata.vi_obj140709 as 
select t.id, rownum sn  from TMPDATA.Vehicleinformation140705 t where t.salesdate is not null
    and not exists(select 1 from tmpdata.VehicleWarrantyCard140707 where t.vin=VehicleWarrantyCard140707.vin)
    and  exists  (select a.Serviceproductlineid from ServiceProdLineProduct a
             inner join product b on b.brandid = a.brandid where
             b.id = t.productid and a.serviceproductlineid is not null)
     create index vi_obj140709_IDX on tmpdata.vi_obj140709(id);        
    select max(sn)from  tmpdata.vi_obj140709
    
         create table tmpdata.vi_obj140707 as 
         select t.id, rownum sn       from TMPDATA.Vehicleinformation140705 t where t.salesdate is not null
    and  exists  (select a.Serviceproductlineid from ServiceProdLineProduct a
             inner join product b on b.brandid = a.brandid where
             b.id = t.productid and a.serviceproductlineid is not null);
         create index vi_obj140707_IDX on tmpdata.vi_obj140707 (id);
 --其中 TMPDATA.Vehicleinformation140705这张表为车辆信息原始数据
 --TMPDATA.VehicleWarrantyCard140707为已导入的单车保修卡

 select count(*) from TMPDATA.VehicleWarrantyCard140709  t
create index IDX_SERPRODLINEPRO_Brandid on SERVICEPRODLINEPRODUCT (brandid);

  declare
  i number;
  j number;
begin
  j:=1;
  For i in 1 .. 27 loop  

insert into TMPDATA.VehicleWarrantyCard140709
  (ID,
   CODE,
   VEHICLEID,
   VIN,
   VEHICLECATEGORYID,
   VEHICLECATEGORYNAME,
   SERVICEPRODUCTLINEID,
   SERVICEPRODUCTLINENAME,
   PRODUCTID,
   PRODUCTCODE,
   SALESDATE,
   WARRANTYSTARTDATE,
   RETAINEDCUSTOMERID,
   CUSTOMERNAME,
   CUSTOMERGENDER,
   IDDOCUMENTTYPE,
   IDDOCUMENTNUMBER,
   WARRANTYPOLICYCATEGORY,
   WARRANTYPOLICYID,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_VehicleWarrantyCard.Nextval,
         'VWC20140709' || decode(length(rownum),
                                 1,
                                 '00000' || rownum,
                                 2,
                                 '0000' || rownum,
                                 3,
                                 '000' || rownum,
                                 4,
                                 '00' || rownum,
                                 5,
                                 '0' || rownum) CODE,
         t.id,
         t.vin,
         1 VEHICLECATEGORYID,
         2 VEHICLECATEGORYNAME,
         (select a.Serviceproductlineid from ServiceProdLineProduct a
             inner join product b on b.brandid = a.brandid where
             b.id = t.productid)SERVICEPRODUCTLINEID,
         (select a.Serviceproductlinename from ServiceProdLineProduct a
             inner join product b on b.brandid = a.brandid where
             b.id = t.productid） SERVICEPRODUCTLINENAME,
         (select id from product a where a.id = t.productid) PRODUCTID,
         (select a.code from product a where a.id = t.productid) PRODUCTCODE,
         t.salesdate,
         nvl(t.salesdate,to_date('1899-1-1','yyyy-mm-dd')) WARRANTYSTARTDATE,
         nvl((select distinct a.id
            from RetainedCustomer a
           inner join TMPDATA.Customer140705 b
              on a.CUSTOMERID = b.id
           where b.remark = t.remark and rownum=1),0) RETAINEDCUSTOMERID,
         nvl((select distinct a.NAME
            from TMPDATA.Customer140705 a
           where a.remark = t.remark),'空') CUSTOMERNAME,
         1 CUSTOMERGENDER,
          (select IDDOCUMENTTYPE
            from TMPDATA.Customer140705 a
           where a.remark = t.remark),
          (select IDDOCUMENTNUMBER
            from TMPDATA.Customer140705 a
           where a.remark = t.remark),
      nvl((select distinct d.Category
            from WarrantyPolicy d
           inner join WarrantyPolicyNServProdLine a
              on a. WarrantyPolicyId = d.id
           inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1          
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate and rownum=1),0) WARRANTYPOLICYCATEGORY,
       nvl((select a.WarrantyPolicyId
            from WarrantyPolicyNServProdLine a
          inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1    
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate and rownum=1),0) WARRANTYPOLICYID,
         1 STATUS,
         1 CREATORID,
         'Admin' CREATORNAME,
         sysdate CREATETIME
    from  TMPDATA.Vehicleinformation140705 t where t.salesdate is not null
    and not exists(select 1 from tmpdata.VehicleWarrantyCard140707 where t.vin=VehicleWarrantyCard140707.vin)
    and  exists  (select a.Serviceproductlineid from ServiceProdLineProduct a
             inner join product b on b.brandid = a.brandid where
             b.id = t.productid and a.serviceproductlineid is not null)
              and exists(select 1 from tmpdata.vi_obj140709 v where v.id=t.id and v.sn>=j and v.sn<(j+1000) );
  
                  j := j + 1000;
    commit;
     end loop;
  end; 
---------------------------------------------------------------    
--650866 单车保修卡 275283 未取到政策 375583插入
--26148 倒入 13558  未取到政策12590 
insert into VehicleWarrantyCard
  select * from TMPDATA.VehicleWarrantyCard140709  t where t.warrantypolicycategory <>0 ;


(select distinct d.Category
            from WarrantyPolicy d
           inner join WarrantyPolicyNServProdLine a
              on a. WarrantyPolicyId = d.id
           inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1          
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate and rownum=1),0)
select* from ServProdLineAffiProduct where productid=13
select * from WarrantyPolicy where id in(
select t.warrantypolicyid from WarrantyPolicyNServProdLine t where t.serviceproductlineid=10)

create table TMPDATA.VehicleMaintenancePoilcy140709 as select * from VehicleMaintenancePoilcy where rownum<0;
select Salesdate from tmpdata.Vehicleinformation140705 where id= 88255
2009-10-25
declare
  cursor Cursor_WARRANTYPOLICYID is(
    select WARRANTYPOLICYID,id VEHICLEWARRANTYCARDID from TMPDATA.VehicleWarrantyCard140709 where VehicleWarrantyCard140709.WARRANTYPOLICYID is not null);
begin
  for S_WARRANTYPOLICYID in Cursor_WARRANTYPOLICYID loop
    insert into TMPDATA.VehicleMaintenancePoilcy140709
      select s_VehicleMaintenancePoilcy.Nextval,
             S_WARRANTYPOLICYID.VEHICLEWARRANTYCARDID VEHICLEWARRANTYCARDID,
             t.id                               VEHICLEMAINTETERMID,
             t.code                             VEHICLEMAINTETERMCODE,
             t.name                             VEHICLEMAINTETERMNAME,
             t.maintetype,
             t.workinghours,
             t.capacity,
             t.maxmileage,
             t.MINMILEAGE,
             t.DAYSUPPERLIMIT,
             t.DAYSLOWERLIMIT,
             0 IFEMPLOYED,
             t.DISPLAYORDER
        from VehicleMainteTerm t
       where t.Warrantypolicyid = S_WARRANTYPOLICYID.WARRANTYPOLICYID;
  end loop;
end;
--221240
--6717
insert into VehicleMaintenancePoilcy select * from TMPDATA.VehicleMaintenancePoilcy140709;

create table TMPDATA.VMPH140709 as
select * from VehicleMaintePoilcyHistroy where rownum < 0;

insert into TMPDATA.VMPH140709
  (ID,
   VEHICLEMAINTENANCEPOILCYID,
   VEHICLEWARRANTYCARDID,
   VEHICLEMAINTETERMID,
   VEHICLEMAINTETERMCODE,
   VEHICLEMAINTETERMNAME,
   MAINTETYPE,
   WORKINGHOURS,
   CAPACITY,
   MAXMILEAGE,
   MINMILEAGE,
   DAYSUPPERLIMIT,
   DAYSLOWERLIMIT,
   IFEMPLOYED,
   DISPLAYORDER,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_VehicleMaintePoilcyHistroy.Nextval,
         id,
         VEHICLEWARRANTYCARDID,
         VEHICLEMAINTETERMID,
         VEHICLEMAINTETERMCODE,
         VEHICLEMAINTETERMNAME,
         MAINTETYPE,
         WORKINGHOURS,
         CAPACITY,
         MAXMILEAGE,
         MINMILEAGE,
         DAYSUPPERLIMIT,
         DAYSLOWERLIMIT,
         IFEMPLOYED,
         DISPLAYORDER,
         1,
         1,
         'Admin',
         sysdate
    from TMPDATA.VehicleMaintenancePoilcy140709;

insert into VehicleMaintePoilcyHistroy
  select * from TMPDATA.VMPH140709;



create table TMPDATA. retainrvehiclelist140709 as 
select * from dcs.retainedcustomervehiclelist where rownum<1;

insert into TMPDATA.retainrvehiclelist140709
  (id, retainedcustomerid, vehicleid, status)
  select s_retainedcustomervehiclelist.nextval,
         t1.retainedcustomerid,
         t1.vehicleid,
         1 status
    from (select distinct retainedcustomerid, vehicleid
            from tmpdata.VehicleWarrantyCard140709 c)t1
         /*  where not exists
           (select id
                    from retainedcustomervehiclelist v
                   where v.vehicleid = c.vehicleid
                     and v.retainedcustomerid = c.retainedcustomerid) and c.vehicleid is not null and c.retainedcustomerid is not null) t1;*/

insert into retainedcustomervehiclelist
  select * from tmpdata.retainrvehiclelist140709;
