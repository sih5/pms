--创建车辆信息临时表
create table tmpdata.VEHICLEINFORMATION140705 as 
select * from dcs.VEHICLEINFORMATION where rownum<0;
select * from tmpdata.TPURCHASEINFOS140705 t where not exists(select 1 from tmpdata.tstdproductin140705  b where b.产品编号=t.产品编号)


--插入车辆信息临时表 709203
create table tmpdata.vi_obj140705 as 
select t.vin码, rownum sn
  from tmpdata.TPURCHASEINFOS140705 t;
  select max(sn)from  tmpdata.vi_obj140705
  create index vi_obj140705_IDX on tmpdata.vi_obj140705 (vin码);


  declare
  i number;
  j number;
begin
  j:=460000;
  For i in 1 .. 71 loop  
insert into tmpdata.VEHICLEINFORMATION140705
  (ID,
   VIN,
   SERIALNUMBER,
   OLDVIN,
   RESPONSIBLEUNITID,
   RESPONSIBLEUNITNAME,
   VEHICLECATEGORYID,
   VEHICLECATEGORYNAME,
   VEHICLELICENSEPLATE,
   VIPVEHICLE,
   VEHICLESERIES,
   DEALERNAME,
   PRODUCTLINENAME,
   PRODUCTCATEGORYTERRACE,
   VEHICLEFUNCTION,
   VEHICLECOLOR,
   TOPSCODE,
   ADAPTTYPE,
   ADAPTCOMPANYNAME,
   CAGECATEGORY,
   DRIVEMODECODE,
   BRIDGETYPE,
   ISROADVEHICL,
   FRONTAXLECODE,
   SECONDFRONTAXLECODE,
   CENTREAXLECODE,
   BEHINDAXLECODE,
   BEHINDAXLETYPTONNELEVEL,
   BALANCESHAFTCODE,
   STEERINGENGINE,
   CHASSISBRANDNAME,
   CHASSISCODE,
   PRODUCTID,
   PRODUCTCODE,
   BRANDNAME,
   
   ENGINEMANUFACTURE,
   ANNOUCEMENTNUMBER,
   ENGINEMODEL,
   ENGINEMODELID,
   GEARMODEL,
   ENGINESERIALNUMBER,
   GEARSERIALNUMBER,
   FIRSTSTOPPAGEMILEAGE,
   OUTOFFACTORYdate,
   ISWORKOFF,
   SALESdate,
   CAPACITY,
   WORKINGHOURS,
   MILEAGE,
   LASTMAINTENANCEWORKINGHOURS,
   LASTMAINTENANCECAPACITY,
   LASTMAINTENANCEMILEAGE,
   LASTMAINTENANCETIME,
   SALESINVOICENUMBER,
  -- INVOICEdate,
  -- VEHICLETYPE,
   REMARK,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_VEHICLEINFORMATION.Nextval,
         t.vin码,
         nvl(t.出厂编号,0) SERIALNUMBER,
         t.vin码,
         null ,
         null,
         1 VEHICLECATEGORYID,
         2 VEHICLECATEGORYNAME,
          t.车牌号,
         0,
         t.车系 VEHICLESERIES,
         t.franchiser_name,
         t.产品线名称,
         null ,
         null,
         null,
         null TOPSCODE,
         null ADAPTTYPE,
         null ADAPTCOMPANYNAME,
         t.驾驶室型号 Cab_Model,
          null,
         null BRIDGETYPE,
         0,
         t.前桥号 FRONTAXLECODE,
         null SECONDFRONTAXLECODE,
         t.中桥号 CENTREAXLECODE,
         t.后桥号 BEHINDAXLECODE,
         null BEHINDAXLETYPTONNELEVEL,
         t.平衡轴号 BALANCESHAFTCODE,
         null STEERINGENGINE,
         t.品牌 CHASSISBRANDNAME,
          t.产品编号  CHASSISCODE,
         (select id from product where product.code=t.产品编号 and product.brandname=t.品牌) PRODUCTID,
         t.产品编号,
         t.品牌,
         
         null ENGINEMANUFACTURE,
         substr(t.公告号,1,50) Product_Out_Type_Name,
         substrb(t.发动机型号,1,50),
         0,
         0,
         substr(t.发动机序列号,1,50),
         t.变速箱序列号 GEARSERIALNUMBER,
         t.mile FIRSTSTOPPAGEMILEAGE,
         t.出厂日期 OUTOFFACTORYdate,
         nvl(t.是否售出,0) ISWORKOFF,
         t.销售日期 SALESdate,
         0 CAPACITY,
         0 WORKINGHOURS,
         mile,
         0 LASTMAINTENANCEWORKINGHOURS,
         0 LASTMAINTENANCECAPACITY,
         0 LASTMAINTENANCEMILEAGE,
         null LASTMAINTENANCETIME,
         t.销售发票号 SALESINVOICENUMBER,
        -- INVOICEdate,
        -- t.,
         t.客户row_id,
         1,
         1,
         'Admin',
         sysdate
    from tmpdata.TPURCHASEINFOS140705 t where  exists(select 1 from tmpdata.tstdproductin140705  b where b.产品编号=t.产品编号) and 
    exists (select 1 from tmpdata.vi_obj140705 v where v.vin码=t.vin码 and v.sn>=j and v.sn<(j+10000) );
  
                  j := j + 10000;
    commit;
     end loop;
  end; 
  create index VEHICLEINFORMATION140430_idx on tmpdata.VEHICLEINFORMATION140430 (vin);

create index TPURCHASEINFOS140705_cidx on tmpdata.TPURCHASEINFOS140705 (客户row_id);
  
select distinct OUTOFFACTORYdate from  tmpdata.VEHICLEINFORMATION_140429temp
--插入车辆信息表 709203
insert into VEHICLEINFORMATION select * from tmpdata.VEHICLEINFORMATION140705;
/*
--创建基础客户信息临时表 
create table TMPDATA.Customer140429 as SELECT * FROM Customer WHERE ROWNUM<0;

--插入基础客户信息临时表 144760
INSERT INTO TMPDATA.Customer140429
  (ID,
   NAME,
   GENDER,
   CUSTOMERTYPE,
   CELLPHONENUMBER,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME,
   
   Ifcomplain)
  SELECT S_Customer.Nextval,
         nvl(t.name, '空'),
         1 GENDER,
         1,
         
         t.cellphonenumber,
         
         1 STATUS,
         
         1 CREATORID,
         'Admin' CREATORNAME,
         sysdate CREATETIME,
         
         0 Ifcomplain
    FROM (SELECT DISTINCT PRODUCTCATEGORYCODE name,
                          PRODUCTCATEGORYNAME cellphonenumber
            from tmpdata.VEHICLEINFORMATION_140429temp) T;
-- SELECT * FROM DCS.TILEDREGION 

--插入基础客户表 
insert into CUSTOMER select * from  TMPDATA.Customer140429;

--创建保有客户临时表
create table TMPDATA.RetainedCustomer140429  as select * from RetainedCustomer where rownum<0;

--插入保有客户临时表 144760
insert into TMPDATA.RetainedCustomer140429(
  ID                ,
  CUSTOMERID        ,
  IFVISITBACKNEEDED ,
  IFTEXTACCEPTED    ,
  IFADACCEPTED      ,
  CARDNUMBER        ,
  IFVIP             ,
  VIPTYPE           ,
  BONUSPOINTS       ,
  PLATEOWNER        ,
  IFMARRIED         ,
  WEDDINGDAY        ,
  FAMILYINCOME      ,
  LOCATION          ,
  FAMILYCOMPOSITION ,
  REFERRAL          ,
  HOBBY             ,
  SPECIALDAY        ,
  SPECIALDAYDETAILS ,
  USERGENDER        ,
  COMPANYTYPE       ,
  ACQUISITORNAME    ,
  ACQUISITORPHONE   ,
  CREATORID         ,
  CREATORNAME       ,
  CREATETIME        ,
  STATUS            ,
  REMARK            ）
  select s_RetainedCustomer.Nextval,
  T.ID,
  0 IFVISITBACKNEEDED ,
  0 IFTEXTACCEPTED    ,
  0 IFADACCEPTED      ,
  '空' CARDNUMBER        ,
  0 IFVIP             ,
  0 VIPTYPE           ,
  0 BONUSPOINTS       ,
  '空' PLATEOWNER        ,
  0 IFMARRIED         ,
  to_date('1899-1-1','yyyy-mm-dd') WEDDINGDAY        ,
  0 FAMILYINCOME      ,
  0 LOCATION          ,
  '空' FAMILYCOMPOSITION ,
  '空' REFERRAL          ,
 '空'  HOBBY             ,
  to_date('1899-1-1','yyyy-mm-dd') SPECIALDAY        ,
  '空' SPECIALDAYDETAILS ,
  0 USERGENDER        ,
  0 COMPANYTYPE       ,
  '空' ACQUISITORNAME    ,
  '空' ACQUISITORPHONE   ,
  1 CREATORID,
  'Admin' CREATORNAME,
  sysdate CREATETIME,
  1 STATUS,
  t.REMARK
  from TMPDATA.Customer140429 t

--插入保有客户Id
insert into RetainedCustomer select * from TMPDATA.RetainedCustomer140429;

create table TMPDATA.VehicleLinkman140429 as select * from VehicleLinkman where rownum<0;

--TMPDATA用户下执行
create index retaincustomer140429_idx on RETAINEDCUSTOMER140429 (customerid);
create index Customer140429_idx on Customer140429 (id);
create index Customer140429_nameidx on Customer140429 (name);
create index vi_PRODUCTCATEGORYCODEidx on VEHICLEINFORMATION_140429temp(PRODUCTCATEGORYCODE);
create index VI140430_PCCIDX on VEHICLEINFORMATION140430 (PRODUCTCATEGORYCODE);
--TMPDATA用户下执行

  declare
  i number;
  j number;
begin
  j:=1;
  For i in 1 .. 350 loop  
insert into TMPDATA.VehicleLinkman140429
  (ID,
   RETAINEDCUSTOMERID,
   VEHICLEINFORMATIONID,
   LINKMANTYPE,
   NAME,
   GENDER,
   CELLPHONENUMBER,
   
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_VehicleLinkman.Nextval,
           (select distinct RetainedCustomer140429.id
            from TMPDATA.RetainedCustomer140429
           inner join TMPDATA.Customer140429
              on TMPDATA.RetainedCustomer140429.CUSTOMERID =
                 TMPDATA.Customer140429.id         
           where   TMPDATA.Customer140429.name = v.PRODUCTCATEGORYCODE and rownum=1
          )  RETAINEDCUSTOMERID,
         (select id from TMPDATA.VEHICLEINFORMATION140430 a where a.vin=v.vin) VEHICLEINFORMATIONID,
         1 LINKMANTYPE,
         v.PRODUCTCATEGORYCODE name,
         1 GENDER,
         v.PRODUCTCATEGORYNAME CELLPHONENUMBER,
         1 STATUS,
         1 CREATORID,
         'Admin' CREATORNAME,
         sysdate CREATETIME
    from TMPDATA.VEHICLEINFORMATION_140429temp v where exists(select 1 from TMPDATA.VEHICLEINFORMATION140430  b where b.vin=v.vin)
    and exists(select 1 from tmpdata.vi_obj140429 t where v.vin=t.vin and t.sn>=j and t.sn<(j+1000) );
  
                  j := j + 1000;
    commit;
     end loop;
  end; 

  
  
  
  
  insert into VehicleLinkman select * from TMPDATA.VehicleLinkman140429;*/

create table TMPDATA.VehicleWarrantyCard140707 as select * from dcs.VehicleWarrantyCard where rownum<0;
--没执行
/*insert into VehicleWarrantyCard140429
  (ID,
   CODE,
   VEHICLEID,
   VIN,
   VEHICLECATEGORYID,
   VEHICLECATEGORYNAME,
   SERVICEPRODUCTLINEID,
   SERVICEPRODUCTLINENAME,
   PRODUCTID,
   PRODUCTCODE,
   SALESDATE,
   WARRANTYSTARTDATE,
   RETAINEDCUSTOMERID,
   CUSTOMERNAME,
   CUSTOMERGENDER,
   IDDOCUMENTTYPE,
   IDDOCUMENTNUMBER,
   WARRANTYPOLICYCATEGORY,
   WARRANTYPOLICYID,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_VehicleWarrantyCard.Nextval,
         'VWC2013140429' || decode(length(rownum),
                                 1,
                                 '00000' || rownum,
                                 2,
                                 '0000' || rownum,
                                 3,
                                 '000' || rownum,
                                 4,
                                 '00' || rownum,
                                 5,
                                 '0' || rownum) CODE,
         t.id,
         t.vin,
         2 VEHICLECATEGORYID,
         2 VEHICLECATEGORYNAME,
         (select a.Serviceproductlineid from ServProdLineAffiProduct a 
            where t.productid=a.ProductId)SERVICEPRODUCTLINEID,
         (select nvl(b.name,c.enginename) from ServProdLineAffiProduct a  left join dcs.serviceproductline b on a.Serviceproductlineid=b.id left join dcs.EngineProductLine c on c.id=a.Serviceproductlineid        
            where a.ProductId = t.productid） SERVICEPRODUCTLINENAME,
         (select id from product a where a.id = t.productid) PRODUCTID,
         (select a.code from product a where a.id = t.productid) PRODUCTCODE,
         t.salesdate,
         nvl(t.salesdate,to_date('1899-1-1','yyyy-mm-dd')) WARRANTYSTARTDATE,
         nvl((select distinct a.id
            from RetainedTMPDATA.Customer140429 a
           inner join customer_temp b
              on a.REMARK = b.CODE
           where b.vin = t.vin),0) RETAINEDCUSTOMERID,
         nvl((select distinct a.NAME
            from TMPDATA.Customer140429 a
           inner join customer_temp b
              on a.REMARK = b.CODE
           where b.vin = t.vin),'空') CUSTOMERNAME,
         1 CUSTOMERGENDER,
         0 IDDOCUMENTTYPE,
         '空' IDDOCUMENTNUMBER,
         (select distinct d.Category
            from WarrantyPolicy d
           inner join WarrantyPolicyNServProdLine a
              on a. WarrantyPolicyId = d.id
           inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1          
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate) WARRANTYPOLICYCATEGORY,
       (select a.WarrantyPolicyId
            from WarrantyPolicyNServProdLine a
          inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1    
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate) WARRANTYPOLICYID,
         1 STATUS,
         1 CREATORID,
         'Admin' CREATORNAME,
         sysdate CREATETIME
    from VEHICLEINFORMATION140429 t where t.salesdate is not null;*/
--------------------------------------------------------------
/*update dcs.vehicleinformation v
   set salesdate =
       (select to_date(salesdate, 'yyyy-mm-dd hh24:mi:ss')
          from TMPDATA.VEHICLEINFORMATION140430 t
         where t.vin = v.vin)
where exists( select  vin
          from TMPDATA.VEHICLEINFORMATION140430 t1
         where t1.vin = v.vin)  */    
update tmpdata. VEHICLEINFORMATION140430 t
   set t.PRODUCTCATEGORYCODE =
       (select PRODUCTCATEGORYCODE
          from tmpdata.VEHICLEINFORMATION_140429temp v
         where v.vin = t.vin);  
            
         create table tmpdata.vi_obj140707 as 
         select t.id, rownum sn       from TMPDATA.Vehicleinformation140705 t where t.salesdate is not null
    and  exists  (select a.Serviceproductlineid from ServiceProdLineProduct a
             inner join product b on b.brandid = a.brandid where
             b.id = t.productid and a.serviceproductlineid is not null);
         create index vi_obj140707_IDX on tmpdata.vi_obj140707 (id);
         
create index IDX_SERPRODLINEPRO_Brandid on SERVICEPRODLINEPRODUCT (brandid);

  declare
  i number;
  j number;
begin
  j:=1;
  For i in 1 .. 651 loop  

insert into TMPDATA.VehicleWarrantyCard140707
  (ID,
   CODE,
   VEHICLEID,
   VIN,
   VEHICLECATEGORYID,
   VEHICLECATEGORYNAME,
   SERVICEPRODUCTLINEID,
   SERVICEPRODUCTLINENAME,
   PRODUCTID,
   PRODUCTCODE,
   SALESDATE,
   WARRANTYSTARTDATE,
   RETAINEDCUSTOMERID,
   CUSTOMERNAME,
   CUSTOMERGENDER,
   IDDOCUMENTTYPE,
   IDDOCUMENTNUMBER,
   WARRANTYPOLICYCATEGORY,
   WARRANTYPOLICYID,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_VehicleWarrantyCard.Nextval,
         'VWC20140707' || decode(length(rownum),
                                 1,
                                 '00000' || rownum,
                                 2,
                                 '0000' || rownum,
                                 3,
                                 '000' || rownum,
                                 4,
                                 '00' || rownum,
                                 5,
                                 '0' || rownum) CODE,
         t.id,
         t.vin,
         1 VEHICLECATEGORYID,
         2 VEHICLECATEGORYNAME,
         (select a.Serviceproductlineid from ServiceProdLineProduct a
             inner join product b on b.brandid = a.brandid where
             b.id = t.productid)SERVICEPRODUCTLINEID,
         (select a.Serviceproductlinename from ServiceProdLineProduct a
             inner join product b on b.brandid = a.brandid where
             b.id = t.productid） SERVICEPRODUCTLINENAME,
         (select id from product a where a.id = t.productid) PRODUCTID,
         (select a.code from product a where a.id = t.productid) PRODUCTCODE,
         t.salesdate,
         nvl(t.salesdate,to_date('1899-1-1','yyyy-mm-dd')) WARRANTYSTARTDATE,
         nvl((select distinct a.id
            from RetainedCustomer a
           inner join TMPDATA.Customer140705 b
              on a.CUSTOMERID = b.id
           where b.remark = t.remark and rownum=1),0) RETAINEDCUSTOMERID,
         nvl((select distinct a.NAME
            from TMPDATA.Customer140705 a
           where a.remark = t.remark),'空') CUSTOMERNAME,
         1 CUSTOMERGENDER,
          (select IDDOCUMENTTYPE
            from TMPDATA.Customer140705 a
           where a.remark = t.remark),
          (select IDDOCUMENTNUMBER
            from TMPDATA.Customer140705 a
           where a.remark = t.remark),
      nvl((select distinct d.Category
            from WarrantyPolicy d
           inner join WarrantyPolicyNServProdLine a
              on a. WarrantyPolicyId = d.id
           inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1          
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate and rownum=1),0) WARRANTYPOLICYCATEGORY,
       nvl((select a.WarrantyPolicyId
            from WarrantyPolicyNServProdLine a
          inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1    
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate and rownum=1),0) WARRANTYPOLICYID,
         1 STATUS,
         1 CREATORID,
         'Admin' CREATORNAME,
         sysdate CREATETIME
    from TMPDATA.Vehicleinformation140705 t where t.salesdate is not null
    and  exists  (select a.Serviceproductlineid from ServiceProdLineProduct a
             inner join product b on b.brandid = a.brandid where
             b.id = t.productid and a.serviceproductlineid is not null)
              and exists(select 1 from tmpdata.vi_obj140707 v where v.id=t.id and v.sn>=j and v.sn<(j+1000) );
  
                  j := j + 1000;
    commit;
     end loop;
  end; 
---------------------------------------------------------------    
--650866 单车保修卡 275283 未取到政策 375583插入

insert into VehicleWarrantyCard
  select * from TMPDATA.VehicleWarrantyCard140707  t where t.warrantypolicycategory <>0 ;


(select distinct d.Category
            from WarrantyPolicy d
           inner join WarrantyPolicyNServProdLine a
              on a. WarrantyPolicyId = d.id
           inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1          
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate and rownum=1),0)
select* from ServProdLineAffiProduct where productid=13
select * from WarrantyPolicy where id in(
select t.warrantypolicyid from WarrantyPolicyNServProdLine t where t.serviceproductlineid=10)

create table TMPDATA.VehicleMaintenancePoilcy140707 as select * from VehicleMaintenancePoilcy where rownum<0;
select Salesdate from tmpdata.Vehicleinformation140705 where id= 88255
2009-10-25
declare
  cursor Cursor_WARRANTYPOLICYID is(
    select WARRANTYPOLICYID,id VEHICLEWARRANTYCARDID from TMPDATA.VehicleWarrantyCard140707 where VehicleWarrantyCard140707.WARRANTYPOLICYID is not null);
begin
  for S_WARRANTYPOLICYID in Cursor_WARRANTYPOLICYID loop
    insert into TMPDATA.VehicleMaintenancePoilcy140707
      select s_VehicleMaintenancePoilcy.Nextval,
             S_WARRANTYPOLICYID.VEHICLEWARRANTYCARDID VEHICLEWARRANTYCARDID,
             t.id                               VEHICLEMAINTETERMID,
             t.code                             VEHICLEMAINTETERMCODE,
             t.name                             VEHICLEMAINTETERMNAME,
             t.maintetype,
             t.workinghours,
             t.capacity,
             t.maxmileage,
             t.MINMILEAGE,
             t.DAYSUPPERLIMIT,
             t.DAYSLOWERLIMIT,
             0 IFEMPLOYED,
             t.DISPLAYORDER
        from VehicleMainteTerm t
       where t.Warrantypolicyid = S_WARRANTYPOLICYID.WARRANTYPOLICYID;
  end loop;
end;
--221240
insert into VehicleMaintenancePoilcy select * from TMPDATA.VehicleMaintenancePoilcy140707;

create table TMPDATA.VMPH140707 as
select * from VehicleMaintePoilcyHistroy where rownum < 0;

insert into TMPDATA.VMPH140707
  (ID,
   VEHICLEMAINTENANCEPOILCYID,
   VEHICLEWARRANTYCARDID,
   VEHICLEMAINTETERMID,
   VEHICLEMAINTETERMCODE,
   VEHICLEMAINTETERMNAME,
   MAINTETYPE,
   WORKINGHOURS,
   CAPACITY,
   MAXMILEAGE,
   MINMILEAGE,
   DAYSUPPERLIMIT,
   DAYSLOWERLIMIT,
   IFEMPLOYED,
   DISPLAYORDER,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select s_VehicleMaintePoilcyHistroy.Nextval,
         id,
         VEHICLEWARRANTYCARDID,
         VEHICLEMAINTETERMID,
         VEHICLEMAINTETERMCODE,
         VEHICLEMAINTETERMNAME,
         MAINTETYPE,
         WORKINGHOURS,
         CAPACITY,
         MAXMILEAGE,
         MINMILEAGE,
         DAYSUPPERLIMIT,
         DAYSLOWERLIMIT,
         IFEMPLOYED,
         DISPLAYORDER,
         1,
         1,
         'Admin',
         sysdate
    from TMPDATA.VehicleMaintenancePoilcy140707;

insert into VehicleMaintePoilcyHistroy
  select * from TMPDATA.VMPH140707;



create table TMPDATA. retainrvehiclelist140707 as 
select * from dcs.retainedcustomervehiclelist where rownum<1;

insert into TMPDATA.retainrvehiclelist140707
  (id, retainedcustomerid, vehicleid, status)
  select s_retainedcustomervehiclelist.nextval,
         t1.retainedcustomerid,
         t1.vehicleid,
         1 status
    from (select distinct retainedcustomerid, vehicleid
            from tmpdata.VehicleWarrantyCard140707 c)t1
         /*  where not exists
           (select id
                    from retainedcustomervehiclelist v
                   where v.vehicleid = c.vehicleid
                     and v.retainedcustomerid = c.retainedcustomerid) and c.vehicleid is not null and c.retainedcustomerid is not null) t1;*/

insert into retainedcustomervehiclelist
  select * from tmpdata.retainrvehiclelist140707;
