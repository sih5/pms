

create table TMPDATA.VehicleLinkman140705 as select * from VehicleLinkman where rownum<0;

--TMPDATA用户下执行
create index retaincustomer140429_idx on RETAINEDCUSTOMER140429 (customerid);
create index Customer140705_idx on tmpdata.Customer140705 (id);
--create index Customer140429_nameidx on Customer140429 (name);
create index VEHICLEINFORMATION140705_rmkidx on tmpdata.VEHICLEINFORMATION140705(remark);
create index VI140430_PCCIDX on VEHICLEINFORMATION140430 (PRODUCTCATEGORYCODE);
--TMPDATA用户下执行
create table tmpdata.vi_obj140705_1 as 
select t.vin, rownum sn
  from tmpdata.VEHICLEINFORMATION140705 t;
  select max(sn)from  tmpdata.vi_obj140705_1
  create index vi_obj140705_1_IDX on tmpdata.vi_obj140705_1 (vin);
  
  declare
  i number;
  j number;
begin
  j:=2001;
  For i in 1 .. 708 loop  
insert into TMPDATA.VehicleLinkman140705
  (ID,
   RETAINEDCUSTOMERID,
   VEHICLEINFORMATIONID,
   LINKMANTYPE,
   NAME,
   GENDER,
   CELLPHONENUMBER,
   
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME,customercode)
  select s_VehicleLinkman.Nextval,
           (select distinct RetainedCustomer140705.id
            from TMPDATA.RetainedCustomer140705
           inner join TMPDATA.Customer140705
              on RetainedCustomer140705.CUSTOMERID =
                 TMPDATA.Customer140705.id         
           where   TMPDATA.Customer140705.REMARK = v.remark and rownum=1
          )  RETAINEDCUSTOMERID,
         v.id VEHICLEINFORMATIONID,
         1 LINKMANTYPE,
          ( select Customer140705.NAME from tmpdata.Customer140705 where Customer140705.REMARK = v.remark),
        ( select Customer140705.GENDER from tmpdata.Customer140705 where Customer140705.REMARK = v.remark) ,
         ( select Customer140705.CELLPHONENUMBER from tmpdata.Customer140705 where Customer140705.REMARK = v.remark),
         1 STATUS,
         1 CREATORID,
         'Admin' CREATORNAME,
         sysdate CREATETIME, 'VL20140705' || decode(length(rownum),
                                 1,
                                 '00000' || rownum,
                                 2,
                                 '0000' || rownum,
                                 3,
                                 '000' || rownum,
                                 4,
                                 '00' || rownum,
                                 5,
                                 '0' || rownum,
                                 6,
                                 rownum) CODE
    from TMPDATA.VEHICLEINFORMATION140705 v where EXISTS(SELECT 1 FROM  tmpdata.Customer140705 WHERE Customer140705.REMARK = v.remark)
    AND exists(select 1 from tmpdata.vi_obj140705_1 t where v.vin=t.vin and t.sn>=j and t.sn<(j+1000) );
  
                  j := j + 1000;
    commit;
     end loop;
  end; 
 
--执行完请执行以下脚本 735160
 insert into VehicleLinkman select * from TMPDATA.VehicleLinkman140705;
  commit;
  select count(*) from TMPDATA.VehicleLinkman140705;
  create table tmpdata.vl_obj140707_1 as 
select count(*)
  from yxdcs.tpurchaselinkers a
    inner join yxdcs.tpurchaseinfos b
       on a.parentid = b.objid
    inner join tmpdata.Customer140705 c
       on c.remark = b.customer_id
    inner join tmpdata.retainedcustomer140705 d
       on d.customerid = c.id
       
  where b.isvalid<>99 and a.type<>0 and exists (select id
                from TMPDATA.VEHICLEINFORMATION140705 v
               where v.vin = b.vincode);
  select max (sn)from tmpdata.vl_obj140707_1;
  create index vl_obj140707_1_IDX on tmpdata.vl_obj140707_1 (objid);

declare
  i number;
  j number;
begin
  j := 1;
  For i in 1 .. 88 loop
    insert into TMPDATA.VehicleLinkman140705
      (ID,
       RETAINEDCUSTOMERID,
       VEHICLEINFORMATIONID,
       LINKMANTYPE,
       NAME,
       GENDER,
       CELLPHONENUMBER,
       
       STATUS,
       CREATORID,
       CREATORNAME,
       CREATETIME,
       customercode)
      select s_VehicleLinkman.Nextval,
             d.id,
             (select id
                from TMPDATA.VEHICLEINFORMATION140705 v
               where v.vin = b.vincode),
             2,
             a.name,
             1,
             a.custmobile,
             1,
             1,
             'Admin',
             sysdate,
             'VL20140707' || decode(length(rownum),
                                    1,
                                    '00000' || rownum,
                                    2,
                                    '0000' || rownum,
                                    3,
                                    '000' || rownum,
                                    4,
                                    '00' || rownum,
                                    5,
                                    '0' || rownum,
                                    6,
                                    rownum) CODE
        from yxdcs.tpurchaselinkers a
       inner join yxdcs.tpurchaseinfos b
          on a.parentid = b.objid
       inner join tmpdata.Customer140705 c
          on c.remark = b.customer_id
       inner join tmpdata.retainedcustomer140705 d
          on d.customerid = c.id
       where b.isvalid <> 99
         and a.type <> 0
         and exists (select 1
                from tmpdata.vl_obj140707_1 t
               where t.objid = a.objid
                 and t.sn >= j
                 and t.sn < (j + 1000))
         and exists (select id
                from TMPDATA.VEHICLEINFORMATION140705 v
               where v.vin = b.vincode);
    j := j + 1000;
    commit;
  end loop;
end;
  /*
  SELECT *FROM TMPDATA.VEHICLEINFORMATION140705 v 
  WHERE NOT EXISTS(SELECT 1FROM  tmpdata.Customer140705 WHERE Customer140705.REMARK = v.remark)*/
  
 
