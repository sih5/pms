--插入临时表
create table tmpdata.rule140707
as
select * from rule where 1=0;
--插入角色
create table tmpdata.role140707 as select * from role where rownum<1;
insert into tmpdata.role140707
  (id,
   enterpriseid,
   name,
   isadmin,
   status,
   remark,
   creatorid,
   creatorname,
   createtime)
  select s_role.nextval,
         2,
         role,
         0,
         1,
         '初始化角色',
         1,
         '系统管理员',
         sysdate
    from (select distinct role from tmpdata.NodeActionPage140707);
insert into role select * from tmpdata.role140707;
/*select * from action a
left join page p on p.id = a.pageid
where a.name='驳回' and p.name='配件外采管理'
group by a.name,p.name having count(*)>1


delete from action where id=1083*/

--插入授权
--2665
insert into tmpdata.rule140707
  (id, roleid, nodeid)
  select s_rule.nextval,
         (select id
            from role r
           where r.name = t.role
             and r.enterpriseid =
                 2) roleid,
         (select n.id
            from page p
            left join action a
              on p.id = a.pageid
            left join node n
              on n.categoryid = a.id
             and n.categorytype = 1
           where a.name = t.action
             and p.name = t.node) as nodeid
    from tmpdata.NodeActionPage140707 t
   where t.action is not null
     and exists (select n.id
            from page p
            left join action a
              on p.id = a.pageid
            left join node n
              on n.categoryid = a.id
             and n.categorytype = 1
           where a.name = t.action
             and p.name = t.node);
--997
insert into tmpdata.rule140707
  (id, roleid, nodeid)
  select s_rule.nextval,
         (select id
            from role r
           where r.name = t.role
             and r.enterpriseid =2) roleid,
         (select n.id
            from page p
           left join node n
              on n.categoryid = p.id
             and n.categorytype = 0
           where p.name = t.node) as nodeid 
    from tmpdata.NodeActionPage140707 t
   where t.action = '查询'
     and exists (select n.id
            from page p
           left join node n
              on n.categoryid = p.id
             and n.categorytype = 0
           where p.name = t.node)
           and t.node not in (select p.name from page p
group by p.name having count(*)>1);
--3362
insert into rule
  (id, roleid, nodeid)
  select id, roleid, nodeid
    from tmpdata.rule140707 t
   where not exists (select 1
            from rule v
           where v.nodeid = t.nodeid
             and v.roleid = t.roleid);
