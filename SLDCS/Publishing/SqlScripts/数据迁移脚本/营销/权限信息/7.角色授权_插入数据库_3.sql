create table tmpdata.NodeActionPage140709 as select * from tmpdata.NodeActionPage140707 where rownum<1;

--插入临时表
create table tmpdata.role140709
as
select * from role where 1=0;

create table tmpdata.rule140709
as
select * from rule where 1=0;

update tmpdata.NodeActionPage140709 a set a.node='服务站/专卖店分品牌信息管理' where a.node='服务站/专卖店分公司信息管理';
update tmpdata.NodeActionPage140709 a set a.node='大区与人员关系' where a.node='销售区域与人员关系';
update tmpdata.NodeActionPage140709 a set a.node='服务站/专卖店企业信息管理' where a.node='服务站/专卖店信息管理';
update tmpdata.NodeActionPage140709 a set a.node='大区与市场部关系' where a.node='销售区域与市场部关系';
update tmpdata.NodeActionPage140709 a set a.node='故障代码与品牌对应关系' where a.node='故障代码与品牌关系';
update tmpdata.NodeActionPage140709 a set a.node='生产工厂管理' where a.node='责任单位管理';
update tmpdata.NodeActionPage140709 a set a.node='大区管理' where a.node='销售区域管理';
update tmpdata.NodeActionPage140709 a set a.node='标准维修工时代码与品牌对应关系' where a.node='标准维修工时代码与品牌关系';


--插入角色
create table tmpdata.role140709 as select * from role where rownum<1;
insert into tmpdata.role140709
  (id,
   enterpriseid,
   name,
   isadmin,
   status,
   remark,
   creatorid,
   creatorname,
   createtime)
  select s_role.nextval,
         2,
         role,
         0,
         1,
         '初始化角色',
         1,
         '系统管理员',
         sysdate
    from (select distinct role from tmpdata.NodeActionPage140709);
insert into role select * from tmpdata.role140709;
/*select * from action a
left join page p on p.id = a.pageid
where a.name='驳回' and p.name='配件外采管理'
group by a.name,p.name having count(*)>1


delete from action where id=1083*/
select * from tmpdata.NodeActionPage140709 t where not exists(select * from page v where t.node=v.name)
--插入授权
--1010
insert into tmpdata.rule140709
  (id, roleid, nodeid)
  select s_rule.nextval,
         (select id
            from role r
           where r.name = t.role
             and r.enterpriseid =
                 2) roleid,
         (select n.id
            from page p
            left join action a
              on p.id = a.pageid
            left join node n
              on n.categoryid = a.id
             and n.categorytype = 1
           where a.name = t.action
             and p.name = t.node) as nodeid
    from tmpdata.NodeActionPage140709 t
   where t.action is not null
     and exists (select n.id
            from page p
            left join action a
              on p.id = a.pageid
            left join node n
              on n.categoryid = a.id
             and n.categorytype = 1
           where a.name = t.action
             and p.name = t.node);
--839
insert into tmpdata.rule140709
  (id, roleid, nodeid)
  select s_rule.nextval,
         (select id
            from role r
           where r.name = t.role
             and r.enterpriseid =2) roleid,
         (select n.id
            from page p
           left join node n
              on n.categoryid = p.id
             and n.categorytype = 0
           where p.name = t.node) as nodeid 
    from tmpdata.NodeActionPage140709 t
   where t.action = '查询'
     and exists (select n.id
            from page p
           left join node n
              on n.categoryid = p.id
             and n.categorytype = 0
           where p.name = t.node)
           and t.node not in (select p.name from page p
group by p.name having count(*)>1);
--1849
insert into rule
  (id, roleid, nodeid)
  select id, roleid, nodeid
    from tmpdata.rule140709 t
   where not exists (select 1
            from rule v
           where v.nodeid = t.nodeid
             and v.roleid = t.roleid);
delete from rule where exists(select * from tmpdata.rule140709 t where t.id=rule.id)