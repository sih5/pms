create table tmpdata.personneldealer140707 as select * from yxsecurity.personnel where rownum<1;
insert into tmpdata.personneldealer140707
  (id,
   enterpriseid,
   loginid,
   password,
   name,
   status,
   passwordmodifytime,
   creatorid,
   creatorname,
   createtime)
  select s_personnel.nextval,
         dealer.id,
         dealer.code,
         '7C4A8D09CA3762AF61E59520943DC26494F8941B',
         dealer.code,
         1,
         sysdate,
         1,
         'Admin',
         sysdate
    from yxdcs.dealer;--密码123456
insert into personnel select * from tmpdata.personneldealer140707 ;

create table tmpdata.roledealer140707 as select * from yxsecurity.role where rownum<1;

update enterprise t set t.entnodetemplateid=8 where code in('FT002591',
'FT003251',
'FT003252',
'FT003253',
'FT003254',
'FT003255',
'FT003267',
'FT003268',
'FT003269',
'FT003270',
'FT003271',
'FT003272',
'FT003273',
'FT003582',
'FT003661',
'FT003664',
'FT004108');
update enterprise t set t.entnodetemplateid=4 where exists (select 1 from yxdcs.dealer where dealer.id=t.id)and code not in('FT002591',
'FT003251',
'FT003252',
'FT003253',
'FT003254',
'FT003255',
'FT003267',
'FT003268',
'FT003269',
'FT003270',
'FT003271',
'FT003272',
'FT003273',
'FT003582',
'FT003661',
'FT003664',
'FT004108');

update enterprise t set status=3 where exists (select 1 from yxdcs.dealer where dealer.id=t.id)and status=1;

select* from  entnodetemplate 
select * from enterprisecategory;

insert into tmpdata.roledealer140707(id,enterpriseid,name,isadmin,status,creatorid,creatorname,createtime)
select s_role.nextval,t.enterpriseid,t.loginid||'企业角色',0,1,1,'Admin',sysdate from tmpdata.personneldealer140707 t
insert into role select * from  tmpdata.roledealer140707;

create table tmpdata.ruledealer140707 as select *from rule where rownum<1;

insert into tmpdata.ruledealer140707(id,roleid,nodeid)
select s_rule.nextval,t.id,s.nodeid from tmpdata.roledealer140707 t inner join enterprise v on t.enterpriseid=v.id 
inner join entnodetemplatedetail s on s.entnodetemplateid=v.entnodetemplateid

insert into rule select * from tmpdata.ruledealer140707;

create table tmpdata.rolepersonneldealer140707 as select * from rolepersonnel where rownum<1;
insert into tmpdata.rolepersonneldealer140707 (id,roleid,personnelid)
select s_rolepersonnel.nextval,v.id,t.id from tmpdata.personneldealer140707 t inner join  tmpdata.roledealer140707 v on t.enterpriseid=v.enterpriseid;

insert into rolepersonnel select * from tmpdata.rolepersonneldealer140707;

create table tmpdata.personnelagency140707 as select * from yxsecurity.personnel where rownum<1;
insert into tmpdata.personnelagency140707
  (id,
   enterpriseid,
   loginid,
   password,
   name,
   status,
   passwordmodifytime,
   creatorid,
   creatorname,
   createtime)
  select s_personnel.nextval,
         agency.id,
         agency.code,
         '7C4A8D09CA3762AF61E59520943DC26494F8941B',
         agency.code,
         1,
         sysdate,
         1,
         'Admin',
         sysdate
    from yxdcs.agency;
insert into personnel select * from tmpdata.personnelagency140707 ;

update enterprise t
   set t.entnodetemplateid = 10,status=3
 where exists (select 1 from yxdcs.agency where agency.id = t.id)and status=1


create table tmpdata.roleagency140707 as select * from yxsecurity.role where rownum<1;
insert into tmpdata.roleagency140707(id,enterpriseid,name,isadmin,status,creatorid,creatorname,createtime)
select s_role.nextval,t.enterpriseid,t.loginid||'企业角色',0,1,1,'Admin',sysdate from tmpdata.personnelagency140707 t
insert into role select * from  tmpdata.roleagency140707;


create table tmpdata.ruleagency140707 as select *from rule where rownum<1;

insert into tmpdata.ruleagency140707(id,roleid,nodeid)
select s_rule.nextval,t.id,s.nodeid from tmpdata.roleagency140707 t inner join enterprise v on t.enterpriseid=v.id 
inner join entnodetemplatedetail s on s.entnodetemplateid=v.entnodetemplateid

insert into rule select * from tmpdata.ruleagency140707;


create table tmpdata.rolepersonnelagency140707 as select * from rolepersonnel where rownum<1;
insert into tmpdata.rolepersonnelagency140707 (id,roleid,personnelid)
select s_rolepersonnel.nextval,v.id,t.id from tmpdata.personnelagency140707 t inner join  tmpdata.roleagency140707 v on t.enterpriseid=v.enterpriseid;

insert into rolepersonnel select * from tmpdata.rolepersonnelagency140707;

create table tmpdata.supplier140707 (code varchar2(100),name varchar2(100))

create table tmpdata.personnelsupplier140707 as select * from yxsecurity.personnel where rownum<1;

insert into tmpdata.personnelsupplier140707
  (id,
   enterpriseid,
   loginid,
   password,
   name,
   status,
   passwordmodifytime,
   creatorid,
   creatorname,
   createtime)
  select s_personnel.nextval,
         t.id,
         t.code,
         '7C4A8D09CA3762AF61E59520943DC26494F8941B',
         t.code,
         1,
         sysdate,
         1,
         'Admin',
         sysdate
    from yxdcs.partssupplier t inner join  tmpdata.supplier140707 v on t.code=v.code;
insert into personnel select * from tmpdata.personnelsupplier140707 ;


update enterprise t
   set t.entnodetemplateid = 5,status=3
 where exists (select 1 from tmpdata.supplier140707 v where v.code=t.code)and status=1
 
 
 

create table tmpdata.rolesupplier140707 as select * from yxsecurity.role where rownum<1;
insert into tmpdata.rolesupplier140707(id,enterpriseid,name,isadmin,status,creatorid,creatorname,createtime)
select s_role.nextval,t.enterpriseid,t.loginid||'企业角色',0,1,1,'Admin',sysdate from tmpdata.personnelsupplier140707 t
insert into role select * from  tmpdata.rolesupplier140707;


create table tmpdata.rulesupplier140707 as select *from rule where rownum<1;

insert into tmpdata.rulesupplier140707(id,roleid,nodeid)
select s_rule.nextval,t.id,s.nodeid from tmpdata.rolesupplier140707 t inner join enterprise v on t.enterpriseid=v.id 
inner join entnodetemplatedetail s on s.entnodetemplateid=v.entnodetemplateid

insert into rule select * from tmpdata.rulesupplier140707;



create table tmpdata.rolepersonnelsupplier140707 as select * from rolepersonnel where rownum<1;
insert into tmpdata.rolepersonnelsupplier140707 (id,roleid,personnelid)
select s_rolepersonnel.nextval,v.id,t.id from tmpdata.personnelsupplier140707 t inner join  tmpdata.rolesupplier140707 v on t.enterpriseid=v.enterpriseid;

insert into rolepersonnel select * from tmpdata.rolepersonnelsupplier140707;
