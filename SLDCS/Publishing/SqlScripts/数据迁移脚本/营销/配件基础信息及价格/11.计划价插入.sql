create table tmpdata.stdprice140708_1 as select * from yxdcs.partsplannedprice where rownum<1;

insert into tmpdata.stdprice140708_1
  (id,
   ownercompanyid,
   ownercompanytype,
   partssalescategoryid,
   partssalescategoryname,
   sparepartid,
   plannedprice,
   creatorid,
   creatorname,
   createtime)
select s_partsplannedprice.nextval,
 2,
 1,
 v.partssalescategoryid,
 v.partssalescategoryname,
 s.id,
 t.stdprice,
 1,
 'Admin',
 sysdate
  from tmpdata.stdprice140708 t
 inner join yxdcs.sparepart s
    on s.code = t.partcode
 inner join yxdcs.partsbranch v
    on s.id = v.partid 
update tmpdata.stdprice140708 t set t.stdprice=19000 where partcode='R5129VECEG0FD0098VA2139';

insert into partsplannedprice select * from tmpdata.stdprice140708_1;

select distinct t.partssalescategoryid,t.partssalescategoryname from tmpdata.stdprice140708_1 t
create table tmpdata.PlannedPriceApp140708 as select * from PlannedPriceApp where rownum<1;
create table tmpdata.PlannedPriceAppDetail140708 as select * from PlannedPriceAppDetail where rownum<1;
insert into tmpdata.PlannedPriceApp140708
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,RecordStatus,PlannedExecutionTime,ActualExecutionTime)
values
  (s_PlannedPriceApp.Nextval,
   2,
   '1101',
   '北汽福田汽车股份有限公司北京配件销售分公司' ，1,
   '奥铃',
   'PPA1101201407080001',
   0,
   0,
   0,
   3,
   1,
   'Admin',
   sysdate,3,sysdate,sysdate);
    insert into tmpdata.plannedpriceappdetail140708
      (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             s_PlannedPriceApp.Currval,
             t.sparepartid,
             (select code
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),
             (select name
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),t.plannedprice,0,0,0
        from tmpdata.stdprice140708_1 t where t.partssalescategoryid=1;
   
   insert into tmpdata.PlannedPriceApp140708
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,RecordStatus,PlannedExecutionTime,ActualExecutionTime)
values
  (s_PlannedPriceApp.Nextval,
   2,
   '1101',
   '北汽福田汽车股份有限公司北京配件销售分公司' ，2,
   '欧马可',
   'PPA1101201407080002',
   0,
   0,
   0,
   3,
   1,
   'Admin',
   sysdate,3,sysdate,sysdate);
    insert into tmpdata.plannedpriceappdetail140708
      (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             s_PlannedPriceApp.Currval,
             t.sparepartid,
             (select code
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),
             (select name
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),t.plannedprice,0,0,0
        from tmpdata.stdprice140708_1 t where t.partssalescategoryid=2;
   
   
   insert into tmpdata.PlannedPriceApp140708
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,RecordStatus,PlannedExecutionTime,ActualExecutionTime)
values
  (s_PlannedPriceApp.Nextval,
   2,
   '1101',
   '北汽福田汽车股份有限公司北京配件销售分公司' ，3,
   '拓陆者萨普',
   'PPA1101201407080003',
   0,
   0,
   0,
   3,
   1,
   'Admin',
   sysdate,3,sysdate,sysdate);
   
    insert into tmpdata.plannedpriceappdetail140708
      (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             s_PlannedPriceApp.Currval,
             t.sparepartid,
             (select code
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),
             (select name
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),t.plannedprice,0,0,0
        from tmpdata.stdprice140708_1 t where t.partssalescategoryid=3;
   
   insert into tmpdata.PlannedPriceApp140708
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,RecordStatus,PlannedExecutionTime,ActualExecutionTime)
values
  (s_PlannedPriceApp.Nextval,
   2,
   '1101',
   '北汽福田汽车股份有限公司北京配件销售分公司' ，4,
   '蒙派克风景',
   'PPA1101201407080004',
   0,
   0,
   0,
   3,
   1,
   'Admin',
   sysdate,3,sysdate,sysdate);
      insert into tmpdata.plannedpriceappdetail140708
      (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             s_PlannedPriceApp.Currval,
             t.sparepartid,
             (select code
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),
             (select name
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),t.plannedprice,0,0,0
        from tmpdata.stdprice140708_1 t where t.partssalescategoryid=4;
   
   
   insert into tmpdata.PlannedPriceApp140708
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,RecordStatus,PlannedExecutionTime,ActualExecutionTime)
values
  (s_PlannedPriceApp.Nextval,
   2,
   '1101',
   '北汽福田汽车股份有限公司北京配件销售分公司' ，5,
   '精品',
   'PPA1101201407080005',
   0,
   0,
   0,
   3,
   1,
   'Admin',
   sysdate,3,sysdate,sysdate);
   
   insert into tmpdata.plannedpriceappdetail140708
      (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             s_PlannedPriceApp.Currval,
             t.sparepartid,
             (select code
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),
             (select name
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),t.plannedprice,0,0,0
        from tmpdata.stdprice140708_1 t where t.partssalescategoryid=5;
  
   insert into tmpdata.PlannedPriceApp140708
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,RecordStatus,PlannedExecutionTime,ActualExecutionTime)
values
  (s_PlannedPriceApp.Nextval,
   2,
   '1101',
   '北汽福田汽车股份有限公司北京配件销售分公司' ，6,
   '统购',
   'PPA1101201407080006',
   0,
   0,
   0,
   3,
   1,
   'Admin',
   sysdate,3,sysdate,sysdate);
   
   insert into tmpdata.plannedpriceappdetail140708
      (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             s_PlannedPriceApp.Currval,
             t.sparepartid,
             (select code
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),
             (select name
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),t.plannedprice,0,0,0
        from tmpdata.stdprice140708_1 t where t.partssalescategoryid=6;
  
   
   insert into tmpdata.PlannedPriceApp140708
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,RecordStatus,PlannedExecutionTime,ActualExecutionTime)
values
  (s_PlannedPriceApp.Nextval,
   2,
   '1101',
   '北汽福田汽车股份有限公司北京配件销售分公司' ，7,
   '海外乘用车',
   'PPA1101201407080007',
   0,
   0,
   0,
   3,
   1,
   'Admin',
   sysdate,3,sysdate,sysdate);
   
   insert into tmpdata.plannedpriceappdetail140708
      (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             s_PlannedPriceApp.Currval,
             t.sparepartid,
             (select code
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),
             (select name
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),t.plannedprice,0,0,0
        from tmpdata.stdprice140708_1 t where t.partssalescategoryid=7;
  
   
   insert into tmpdata.PlannedPriceApp140708
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,RecordStatus,PlannedExecutionTime,ActualExecutionTime)
values
  (s_PlannedPriceApp.Nextval,
   2,
   '1101',
   '北汽福田汽车股份有限公司北京配件销售分公司' ，8,
   '海外轻卡',
   'PPA1101201407080008',
   0,
   0,
   0,
   3,
   1,
   'Admin',
   sysdate,3,sysdate,sysdate);
   
    insert into tmpdata.plannedpriceappdetail140708
      (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             s_PlannedPriceApp.Currval,
             t.sparepartid,
             (select code
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),
             (select name
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),t.plannedprice,0,0,0
        from tmpdata.stdprice140708_1 t where t.partssalescategoryid=8;
  
   
   insert into tmpdata.PlannedPriceApp140708
  (id,
   ownercompanyid,
   ownercompanycode,
   ownercompanyname,
   partssalescategoryid,
   partssalescategoryname,
   code,
   amountbeforechange,
   amountafterchange,
   amountdifference,
   status,
   creatorid,
   creatorname,
   createtime,RecordStatus,PlannedExecutionTime,ActualExecutionTime)
values
  (s_PlannedPriceApp.Nextval,
   2,
   '1101',
   '北汽福田汽车股份有限公司北京配件销售分公司' ，9,
   '海外中重卡',
   'PPA1101201407080009',
   0,
   0,
   0,
   3,
   1,
   'Admin',
   sysdate,3,sysdate,sysdate);
   
      insert into tmpdata.plannedpriceappdetail140708
      (id,
       plannedpriceappid,
       sparepartid,
       sparepartcode,
       sparepartname,
       requestedprice,
       quantity,
       pricebeforechange,
       amountdifference)
      select s_PlannedPriceAppDetail.Nextval,
             s_PlannedPriceApp.Currval,
             t.sparepartid,
             (select code
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),
             (select name
                from yxdcs.sparepart
               where t.sparepartid = sparepart.id),t.plannedprice,0,0,0
        from tmpdata.stdprice140708_1 t where t.partssalescategoryid=9;
  
   
insert into  PlannedPriceApp select * from  tmpdata.PlannedPriceApp140708;
insert into  plannedpriceappdetail select * from tmpdata.plannedpriceappdetail140708;
