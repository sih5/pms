create table tmpdata.purchasesaleprice140709 (code varchar2(100),name varchar2(100),suppliercode  varchar2(100),suppliername varchar2(100),purchaseprice varchar2(100),saleprice varchar2(100) )

create table tmpdata.PartsSalesPrice140709 as select * from PartsSalesPrice where rownum<1; 
insert into tmpdata.PartsSalesPrice140709
  (id,
   branchid,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   sparepartid,
   sparepartcode,
   sparepartname,
   ifclaim,
   salesprice,
   pricetype,
   status,
   creatorid,
   creatorname,
   createtime)
select s_PartsSalesPrice.Nextval,2,s.partssalescategoryid,(select code from yxdcs.partssalescategory where partssalescategory.id=s.partssalescategoryid),s.partssalescategoryname,v.id
,v.code,v.name,1,nvl(t.saleprice,0),1,1,1,'Admin',sysdate
from tmpdata.purchasesaleprice140709 t inner join yxdcs.sparepart v on t.code=v.code
inner join yxdcs.partsbranch s on s.partid=v.id;

insert into PartsSalesPrice select * from tmpdata.PartsSalesPrice140709;

create table tmpdata.PartsSalesPriceHistory140709 as select * from PartsSalesPriceHistory where rownum<1;

insert into tmpdata.PartsSalesPriceHistory140709 (id,
   branchid,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   sparepartid,
   sparepartcode,
   sparepartname,
   ifclaim,
   salesprice,
   pricetype,
   status,
   creatorid,
   creatorname,
   createtime)
   select s_PartsSalesPriceHistory.Nextval,branchid,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   sparepartid,
   sparepartcode,
   sparepartname,
   ifclaim,
   salesprice,
   pricetype,
   status,1,'Admin',sysdate from tmpdata.PartsSalesPrice140709 t;
   insert into PartsSalesPriceHistory select * from tmpdata.PartsSalesPriceHistory140709;
   select * from tmpdata.PartsSalesPriceChange140709 for update;
   create table tmpdata.PartsSalesPriceChange140709 as select * From PartsSalesPriceChange where rownum<1;
   create table tmpdata.PartsSalesPriceChangeDt140709 as select * from PartsSalesPriceChangeDetail where rownum<1;
insert into tmpdata.PartsSalesPriceChange140709
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,status)
  values( s_PartsSalesPriceChange.Nextval,
         2,
         'PSPC201407080001',
         1,
         '104011',
         '奥铃',
         
         1,
         1,
         'Admin',
         sysdate ,1);
         
         insert into tmpdata.PartsSalesPriceChangeDt140709
           (id,
            parentid,
            sparepartid,
            sparepartcode,
            sparepartname,
            salesprice,
            pricetype)
           select s_PartsSalesPriceChangeDetail.Nextval,
                  22,
                  t.sparepartid,
                  t.sparepartcode,
                  t.sparepartname,
                  t.salesprice,
                  1
             from tmpdata.PartsSalesPrice140709 t
            where t.partssalescategoryid = 1;
            
            
insert into tmpdata.PartsSalesPriceChange140709
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,status)
  values( s_PartsSalesPriceChange.Nextval,
         2,
         'PSPC201407080002',
         2,
         '102111',
         '欧马可',
         
         1,
         1,
         'Admin',
         sysdate ,3);
         
         insert into tmpdata.PartsSalesPriceChangeDt140709
           (id,
            parentid,
            sparepartid,
            sparepartcode,
            sparepartname,
            salesprice,
            pricetype)
           select s_PartsSalesPriceChangeDetail.Nextval,
                  23,
                  t.sparepartid,
                  t.sparepartcode,
                  t.sparepartname,
                  t.salesprice,
                  1
             from tmpdata.PartsSalesPrice140709 t
            where t.partssalescategoryid = 2;
            
            
insert into tmpdata.PartsSalesPriceChange140709
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,status)
  values( s_PartsSalesPriceChange.Nextval,
         2,
         'PSPC201407080002',
         2,
         '102111',
         '欧马可',
         
         1,
         1,
         'Admin',
         sysdate ,3);
         
         insert into tmpdata.PartsSalesPriceChangeDt140709
           (id,
            parentid,
            sparepartid,
            sparepartcode,
            sparepartname,
            salesprice,
            pricetype)
           select s_PartsSalesPriceChangeDetail.Nextval,
                  23,
                  t.sparepartid,
                  t.sparepartcode,
                  t.sparepartname,
                  t.salesprice,
                  1
             from tmpdata.PartsSalesPrice140709 t
            where t.partssalescategoryid = 2;
            
              
            
insert into tmpdata.PartsSalesPriceChange140709
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,status)
  values( s_PartsSalesPriceChange.Nextval,
         2,
         'PSPC201407080003',
         3,
         '102112',
         '拓陆者萨普',
         
         1,
         1,
         'Admin',
         sysdate ,3);
         
         insert into tmpdata.PartsSalesPriceChangeDt140709
           (id,
            parentid,
            sparepartid,
            sparepartcode,
            sparepartname,
            salesprice,
            pricetype)
           select s_PartsSalesPriceChangeDetail.Nextval,
                  24,
                  t.sparepartid,
                  t.sparepartcode,
                  t.sparepartname,
                  t.salesprice,
                  1
             from tmpdata.PartsSalesPrice140709 t
            where t.partssalescategoryid = 3;
            
                
            
insert into tmpdata.PartsSalesPriceChange140709
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,status)
  values( s_PartsSalesPriceChange.Nextval,
         2,
         'PSPC201407080004',
         4,
         '102012',
         '蒙派克风景',
         
         1,
         1,
         'Admin',
         sysdate ,3);
         
         insert into tmpdata.PartsSalesPriceChangeDt140709
           (id,
            parentid,
            sparepartid,
            sparepartcode,
            sparepartname,
            salesprice,
            pricetype)
           select s_PartsSalesPriceChangeDetail.Nextval,
                  25,
                  t.sparepartid,
                  t.sparepartcode,
                  t.sparepartname,
                  t.salesprice,
                  1
             from tmpdata.PartsSalesPrice140709 t
            where t.partssalescategoryid = 4;
            
            insert into tmpdata.PartsSalesPriceChange140709
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,status)
  values( s_PartsSalesPriceChange.Nextval,
         2,
         'PSPC201407080005',
         5,
         '999998',
         '精品',
         
         1,
         1,
         'Admin',
         sysdate ,3);
         
         insert into tmpdata.PartsSalesPriceChangeDt140709
           (id,
            parentid,
            sparepartid,
            sparepartcode,
            sparepartname,
            salesprice,
            pricetype)
           select s_PartsSalesPriceChangeDetail.Nextval,
                  26,
                  t.sparepartid,
                  t.sparepartcode,
                  t.sparepartname,
                  t.salesprice,
                  1
             from tmpdata.PartsSalesPrice140709 t
            where t.partssalescategoryid = 5;

 insert into tmpdata.PartsSalesPriceChange140709
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,status)
  values( s_PartsSalesPriceChange.Nextval,
         2,
         'PSPC201407080006',
         6,
         '999999',
         '统购',
         
         1,
         1,
         'Admin',
         sysdate ,3);
         
         insert into tmpdata.PartsSalesPriceChangeDt140709
           (id,
            parentid,
            sparepartid,
            sparepartcode,
            sparepartname,
            salesprice,
            pricetype)
           select s_PartsSalesPriceChangeDetail.Nextval,
                  27,
                  t.sparepartid,
                  t.sparepartcode,
                  t.sparepartname,
                  t.salesprice,
                  1
             from tmpdata.PartsSalesPrice140709 t
            where t.partssalescategoryid = 6;          


 insert into tmpdata.PartsSalesPriceChange140709
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,status)
  values( s_PartsSalesPriceChange.Nextval,
         2,
         'PSPC201407080007',
         7,
         '999995',
         '海外乘用车',
         
         1,
         1,
         'Admin',
         sysdate ,3);
         
         insert into tmpdata.PartsSalesPriceChangeDt140709
           (id,
            parentid,
            sparepartid,
            sparepartcode,
            sparepartname,
            salesprice,
            pricetype)
           select s_PartsSalesPriceChangeDetail.Nextval,
                  28,
                  t.sparepartid,
                  t.sparepartcode,
                  t.sparepartname,
                  t.salesprice,
                  1
             from tmpdata.PartsSalesPrice140709 t
            where t.partssalescategoryid = 7;                                
            
insert into tmpdata.PartsSalesPriceChange140709
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,status)
  values( s_PartsSalesPriceChange.Nextval,
         2,
         'PSPC201407080008',
         8,
         '999996',
         '海外轻卡',
         
         1,
         1,
         'Admin',
         sysdate ,3);
         
         insert into tmpdata.PartsSalesPriceChangeDt140709
           (id,
            parentid,
            sparepartid,
            sparepartcode,
            sparepartname,
            salesprice,
            pricetype)
           select s_PartsSalesPriceChangeDetail.Nextval,
                  29,
                  t.sparepartid,
                  t.sparepartcode,
                  t.sparepartname,
                  t.salesprice,
                  1
             from tmpdata.PartsSalesPrice140709 t
            where t.partssalescategoryid = 8;  

     
insert into tmpdata.PartsSalesPriceChange140709
  (id,
   branchid,
   code,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   ifclaim,
   creatorid,
   creatorname,
   createtime,status)
  values( s_PartsSalesPriceChange.Nextval,
         2,
         'PSPC201407080009',
         9,
         '999997',
         '海外中重卡',
         
         1,
         1,
         'Admin',
         sysdate ,3);
         
         insert into tmpdata.PartsSalesPriceChangeDt140709
           (id,
            parentid,
            sparepartid,
            sparepartcode,
            sparepartname,
            salesprice,
            pricetype)
           select s_PartsSalesPriceChangeDetail.Nextval,
                  30,
                  t.sparepartid,
                  t.sparepartcode,
                  t.sparepartname,
                  t.salesprice,
                  1
             from tmpdata.PartsSalesPrice140709 t
            where t.partssalescategoryid =9;                                                                                  
            insert into PartsSalesPriceChange select * from tmpdata.PartsSalesPriceChange140709;
                        insert into PartsSalesPriceChangeDetail select * from tmpdata.PartsSalesPriceChangeDt140709;
                        
create table tmpdata.PartsPurchasePricing140709 as select * from PartsPurchasePricing where rownum<1;      

create table tmpdata.newoldrelate140709 (newcode varchar2(100),newname varchar2(100),oldcode varchar2(100),oldname varchar2(100));
select * from tmpdata.newoldrelate140709;

insert into tmpdata.PartsPurchasePricing140709
  (id,
   branchid,
   partid,
   partssupplierid,
   partssalescategoryid,
   partssalescategoryname,
   validfrom,
   validto,
   pricetype,
   purchaseprice,
   status)
  select s_PartsPurchasePricing.Nextval,
         2,
         v.id,
         w.id,
         s.partssalescategoryid,
         s.partssalescategoryname,
         to_date('2014-1-1', 'yyyy-mm-dd'),
         to_date('2014-12-31', 'yyyy-mm-dd'),
         1,
         nvl(t.purchaseprice, 0),
         2
    from tmpdata.purchasesaleprice140709 t
   inner join yxdcs.sparepart v
      on t.code = v.code
   inner join yxdcs.partsbranch s
      on s.partid = v.id
   inner join tmpdata.newoldrelate140709 u
      on u.oldcode = t.suppliercode
   inner join yxdcs.partssupplier w
      on w.code = u.newcode;

select * from tmpdata.PartsPurchasePricingC140709
create table tmpdata.PartsPurchasePricingC140709 as select * from PartsPurchasePricingChange where rownum<1; 
create table tmpdata.PartsPurchasePricingD140709 as select * from PartsPurchasePricingDetail where rownum<1;
insert into tmpdata.PartsPurchasePricingC140709
  (id,
   code,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   creatorid,
   creatorname,
   createtime,status)
values
  (s_PartsPurchasePricingChange.Nextval,'PPPC201407080001', 2, 1, '奥铃', 1, 'Admin', sysdate,2);

insert into tmpdata.PartsPurchasePricingD140709
  (id,
   parentid,
   partid,
   partcode,
   partname,
   supplierid,
   suppliercode,
   suppliername,
   pricetype,
   price,VALIDFROM,validto)
  select s_PartsPurchasePricingDetail.Nextval,
         s_PartsPurchasePricingChange.Currval,
         t.partid,
         (select code from yxdcs.sparepart where id = t.partid),
         (select name from yxdcs.sparepart where id = t.partid),
         t.partssupplierid,
         (select code from yxdcs.partssupplier where id = t.partssupplierid),
         (select name from yxdcs.partssupplier where id = t.partssupplierid),
         1,
         t.purchaseprice, to_date('2014-1-1', 'yyyy-mm-dd'),
         to_date('2014-12-31', 'yyyy-mm-dd')
    from tmpdata.PartsPurchasePricing140709 t where t.partssalescategoryid=1;

insert into tmpdata.PartsPurchasePricingC140709
  (id,
   code,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   creatorid,
   creatorname,
   createtime,status)
values
  (s_PartsPurchasePricingChange.Nextval,'PPPC201407080002', 2, 2, '欧马可', 1, 'Admin', sysdate,2);

insert into tmpdata.PartsPurchasePricingD140709
  (id,
   parentid,
   partid,
   partcode,
   partname,
   supplierid,
   suppliercode,
   suppliername,
   pricetype,
   price,VALIDFROM,validto)
  select s_PartsPurchasePricingDetail.Nextval,
         s_PartsPurchasePricingChange.Currval,
         t.partid,
         (select code from yxdcs.sparepart where id = t.partid),
         (select name from yxdcs.sparepart where id = t.partid),
         t.partssupplierid,
         (select code from yxdcs.partssupplier where id = t.partssupplierid),
         (select name from yxdcs.partssupplier where id = t.partssupplierid),
         1,
         t.purchaseprice, to_date('2014-1-1', 'yyyy-mm-dd'),
         to_date('2014-12-31', 'yyyy-mm-dd')
    from tmpdata.PartsPurchasePricing140709 t where t.partssalescategoryid= 2;
    
 
insert into tmpdata.PartsPurchasePricingC140709
  (id,
   code,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   creatorid,
   creatorname,
   createtime,status)
values
  (s_PartsPurchasePricingChange.Nextval,'PPPC201407080003', 2, 3, '拓陆者萨普', 1, 'Admin', sysdate,2);

insert into tmpdata.PartsPurchasePricingD140709
  (id,
   parentid,
   partid,
   partcode,
   partname,
   supplierid,
   suppliercode,
   suppliername,
   pricetype,
   price,VALIDFROM,validto)
  select s_PartsPurchasePricingDetail.Nextval,
         s_PartsPurchasePricingChange.Currval,
         t.partid,
         (select code from yxdcs.sparepart where id = t.partid),
         (select name from yxdcs.sparepart where id = t.partid),
         t.partssupplierid,
         (select code from yxdcs.partssupplier where id = t.partssupplierid),
         (select name from yxdcs.partssupplier where id = t.partssupplierid),
         1,
         t.purchaseprice, to_date('2014-1-1', 'yyyy-mm-dd'),
         to_date('2014-12-31', 'yyyy-mm-dd')
    from tmpdata.PartsPurchasePricing140709 t where t.partssalescategoryid= 3;   
    

insert into tmpdata.PartsPurchasePricingC140709
  (id,
   code,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   creatorid,
   creatorname,
   createtime,status)
values
  (s_PartsPurchasePricingChange.Nextval,'PPPC20140708004', 2, 4, '蒙派克风景', 1, 'Admin', sysdate,2);

insert into tmpdata.PartsPurchasePricingD140709
  (id,
   parentid,
   partid,
   partcode,
   partname,
   supplierid,
   suppliercode,
   suppliername,
   pricetype,
   price,VALIDFROM,validto)
  select s_PartsPurchasePricingDetail.Nextval,
         s_PartsPurchasePricingChange.Currval,
         t.partid,
         (select code from yxdcs.sparepart where id = t.partid),
         (select name from yxdcs.sparepart where id = t.partid),
         t.partssupplierid,
         (select code from yxdcs.partssupplier where id = t.partssupplierid),
         (select name from yxdcs.partssupplier where id = t.partssupplierid),
         1,
         t.purchaseprice, to_date('2014-1-1', 'yyyy-mm-dd'),
         to_date('2014-12-31', 'yyyy-mm-dd')
    from tmpdata.PartsPurchasePricing140709 t where t.partssalescategoryid= 4;       
    


insert into tmpdata.PartsPurchasePricingC140709
  (id,
   code,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   creatorid,
   creatorname,
   createtime,status)
values
  (s_PartsPurchasePricingChange.Nextval,'PPPC20140708005', 2, 5, '精品', 1, 'Admin', sysdate,2);

insert into tmpdata.PartsPurchasePricingD140709
  (id,
   parentid,
   partid,
   partcode,
   partname,
   supplierid,
   suppliercode,
   suppliername,
   pricetype,
   price,VALIDFROM,validto)
  select s_PartsPurchasePricingDetail.Nextval,
         s_PartsPurchasePricingChange.Currval,
         t.partid,
         (select code from yxdcs.sparepart where id = t.partid),
         (select name from yxdcs.sparepart where id = t.partid),
         t.partssupplierid,
         (select code from yxdcs.partssupplier where id = t.partssupplierid),
         (select name from yxdcs.partssupplier where id = t.partssupplierid),
         1,
         t.purchaseprice, to_date('2014-1-1', 'yyyy-mm-dd'),
         to_date('2014-12-31', 'yyyy-mm-dd')
    from tmpdata.PartsPurchasePricing140709 t where t.partssalescategoryid= 5;      
    


insert into tmpdata.PartsPurchasePricingC140709
  (id,
   code,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   creatorid,
   creatorname,
   createtime,status)
values
  (s_PartsPurchasePricingChange.Nextval,'PPPC20140708006', 2, 6, '统购', 1, 'Admin', sysdate,2);

insert into tmpdata.PartsPurchasePricingD140709
  (id,
   parentid,
   partid,
   partcode,
   partname,
   supplierid,
   suppliercode,
   suppliername,
   pricetype,
   price,VALIDFROM,validto)
  select s_PartsPurchasePricingDetail.Nextval,
         s_PartsPurchasePricingChange.Currval,
         t.partid,
         (select code from yxdcs.sparepart where id = t.partid),
         (select name from yxdcs.sparepart where id = t.partid),
         t.partssupplierid,
         (select code from yxdcs.partssupplier where id = t.partssupplierid),
         (select name from yxdcs.partssupplier where id = t.partssupplierid),
         1,
         t.purchaseprice, to_date('2014-1-1', 'yyyy-mm-dd'),
         to_date('2014-12-31', 'yyyy-mm-dd')
    from tmpdata.PartsPurchasePricing140709 t where t.partssalescategoryid= 6;       
    


insert into tmpdata.PartsPurchasePricingC140709
  (id,
   code,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   creatorid,
   creatorname,
   createtime,status)
values
  (s_PartsPurchasePricingChange.Nextval,'PPPC20140708007', 2, 7, '海外乘用车', 1, 'Admin', sysdate,2);

insert into tmpdata.PartsPurchasePricingD140709
  (id,
   parentid,
   partid,
   partcode,
   partname,
   supplierid,
   suppliercode,
   suppliername,
   pricetype,
   price,VALIDFROM,validto)
  select s_PartsPurchasePricingDetail.Nextval,
         s_PartsPurchasePricingChange.Currval,
         t.partid,
         (select code from yxdcs.sparepart where id = t.partid),
         (select name from yxdcs.sparepart where id = t.partid),
         t.partssupplierid,
         (select code from yxdcs.partssupplier where id = t.partssupplierid),
         (select name from yxdcs.partssupplier where id = t.partssupplierid),
         1,
         t.purchaseprice, to_date('2014-1-1', 'yyyy-mm-dd'),
         to_date('2014-12-31', 'yyyy-mm-dd')
    from tmpdata.PartsPurchasePricing140709 t where t.partssalescategoryid= 7;        
    


insert into tmpdata.PartsPurchasePricingC140709
  (id,
   code,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   creatorid,
   creatorname,
   createtime,status)
values
  (s_PartsPurchasePricingChange.Nextval,'PPPC20140708008', 2, 8, '海外轻卡', 1, 'Admin', sysdate,2);

insert into tmpdata.PartsPurchasePricingD140709
  (id,
   parentid,
   partid,
   partcode,
   partname,
   supplierid,
   suppliercode,
   suppliername,
   pricetype,
   price,VALIDFROM,validto)
  select s_PartsPurchasePricingDetail.Nextval,
         s_PartsPurchasePricingChange.Currval,
         t.partid,
         (select code from yxdcs.sparepart where id = t.partid),
         (select name from yxdcs.sparepart where id = t.partid),
         t.partssupplierid,
         (select code from yxdcs.partssupplier where id = t.partssupplierid),
         (select name from yxdcs.partssupplier where id = t.partssupplierid),
         1,
         t.purchaseprice, to_date('2014-1-1', 'yyyy-mm-dd'),
         to_date('2014-12-31', 'yyyy-mm-dd')
    from tmpdata.PartsPurchasePricing140709 t where t.partssalescategoryid= 8;         
    


insert into tmpdata.PartsPurchasePricingC140709
  (id,
   code,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   creatorid,
   creatorname,
   createtime,status)
values
  (s_PartsPurchasePricingChange.Nextval,'PPPC20140708009', 2, 9, '海外中重卡', 1, 'Admin', sysdate,2);

insert into tmpdata.PartsPurchasePricingD140709
  (id,
   parentid,
   partid,
   partcode,
   partname,
   supplierid,
   suppliercode,
   suppliername,
   pricetype,
   price,VALIDFROM,validto)
  select s_PartsPurchasePricingDetail.Nextval,
         s_PartsPurchasePricingChange.Currval,
         t.partid,
         (select code from yxdcs.sparepart where id = t.partid),
         (select name from yxdcs.sparepart where id = t.partid),
         t.partssupplierid,
         (select code from yxdcs.partssupplier where id = t.partssupplierid),
         (select name from yxdcs.partssupplier where id = t.partssupplierid),
         1,
         t.purchaseprice, to_date('2014-1-1', 'yyyy-mm-dd'),
         to_date('2014-12-31', 'yyyy-mm-dd')
    from tmpdata.PartsPurchasePricing140709 t where t.partssalescategoryid= 9;                                                                       
    
    insert into PartsPurchasePricingChange select * from tmpdata.PartsPurchasePricingC140709;
    insert into PartsPurchasePricingDetail select * from tmpdata.PartsPurchasePricingD140709;
