select distinct 证件类型 from   tmpdata.TCONSUMERS140705
select count(*) from TMPDATA.Customer140705
create table TMPDATA.Customer140705 as SELECT * FROM Customer WHERE ROWNUM<0;
create index TPURCHASEINFOS140705_cidx on tmpdata.TPURCHASEINFOS140705 (客户row_id);
 TPURCHASEINFOS140705
--插入基础客户信息临时表 632735
INSERT INTO TMPDATA.Customer140705
  (ID,
   NAME,
   GENDER,
   CUSTOMERTYPE,
   CELLPHONENUMBER,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME,
   Ifcomplain,
   customercode,
   AGE,
   
   ADDRESS,
   POSTCODE,
   HOMEPHONENUMBER,
   
   IDDOCUMENTNUMBER,
   EMAIL,
   IDDOCUMENTTYPE,remark)
  SELECT S_Customer.Nextval,
         nvl(t.客户姓名, '空'),
         decode(t.性别, '未填', 0, '男', 1, 2),
         decode(t.客户类型, '个人客户', 1, 2),
         
         substrb(t.手机号码, 1, 50),
         
         1 STATUS,
         
         1 CREATORID,
         'Admin' CREATORNAME,
         sysdate CREATETIME,
         
         0 Ifcomplain,
         'CSM20140705' || decode(length(rownum),
                                 1,
                                 '00000' || rownum,
                                 2,
                                 '0000' || rownum,
                                 3,
                                 '000' || rownum,
                                 4,
                                 '00' || rownum,
                                 5,
                                 '0' || rownum,
                                 6,
                                 rownum) CODE,
         t.年龄,
         substrb(t.详细地址,1,200),
         substrb(t.邮政编码,1,6),
         substrb(t.家庭电话,1,50),
         t.证件号码,
         t.电子邮件,
         decode(t.证件类型,'居民身份证',1),t.客户id
    FROM tmpdata.TCONSUMERS140705 t;
-- SELECT * FROM DCS.TILEDREGION 

--插入基础客户表 
insert into CUSTOMER select * from  TMPDATA.Customer140705;
delete from CUSTOMER where exists(select 1 from  TMPDATA.Customer140705 t where t.id=CUSTOMER.id)
--创建保有客户临时表
create table TMPDATA.RetainedCustomer140705  as select * from RetainedCustomer where rownum<0;

--插入保有客户临时表 144760
insert into TMPDATA.RetainedCustomer140705(
  ID                ,
  CUSTOMERID        ,
  IFVISITBACKNEEDED ,
  IFTEXTACCEPTED    ,
  IFADACCEPTED      ,
  CARDNUMBER        ,
  IFVIP             ,
  VIPTYPE           ,
  BONUSPOINTS       ,
  PLATEOWNER        ,
  IFMARRIED         ,
  WEDDINGDAY        ,
  FAMILYINCOME      ,
  LOCATION          ,
  FAMILYCOMPOSITION ,
  REFERRAL          ,
  HOBBY             ,
  SPECIALDAY        ,
  SPECIALDAYDETAILS ,
  USERGENDER        ,
  COMPANYTYPE       ,
  ACQUISITORNAME    ,
  ACQUISITORPHONE   ,
  CREATORID         ,
  CREATORNAME       ,
  CREATETIME        ,
  STATUS            ,
  REMARK            ）
  select s_RetainedCustomer.Nextval,
  (select id from tmpdata.Customer140705 v where remark=t.客户id),
  0 IFVISITBACKNEEDED ,
  0 IFTEXTACCEPTED    ,
  0 IFADACCEPTED      ,
  '空' CARDNUMBER        ,
  0 IFVIP             ,
  0 VIPTYPE           ,
  0 BONUSPOINTS       ,
  '空' PLATEOWNER        ,
  0 IFMARRIED         ,
  to_date('1899-1-1','yyyy-mm-dd') WEDDINGDAY        ,
  0 FAMILYINCOME      ,
  0 LOCATION          ,
  '空' FAMILYCOMPOSITION ,
  '空' REFERRAL          ,
substrb(t.客户爱好,1,200)  HOBBY             ,
  to_date('1899-1-1','yyyy-mm-dd') SPECIALDAY        ,
  '空' SPECIALDAYDETAILS ,
  0 USERGENDER        ,
  0 COMPANYTYPE       ,
  '空' ACQUISITORNAME    ,
  '空' ACQUISITORPHONE   ,
  1 CREATORID,
  'Admin' CREATORNAME,
  sysdate CREATETIME,
  1 STATUS,
  t.客户id
  from TMPDATA.TCONSUMERS140705 t;
  commit;
  where exists(select 1 from   tmpdata.TPURCHASEINFOS140705 v where t.客户id=v.客户row_id)

632735
select count(*)  from TMPDATA.TCONSUMERS140705 t
  where exists(select 1 from   tmpdata.TPURCHASEINFOS140705 v where t.客户id=v.客户row_id)
--插入保有客户Id
insert into RetainedCustomer select * from TMPDATA.RetainedCustomer140705;


