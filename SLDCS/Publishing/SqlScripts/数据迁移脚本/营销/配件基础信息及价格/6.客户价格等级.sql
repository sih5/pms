
create table tmpdata.customerorderpricegrade140707 as select * from yxdcs.customerorderpricegrade where rownum<1;
insert into tmpdata.customerorderpricegrade140707
  (id,
   branchid,
   partssalescategoryid,
   customercompanyid,
   customercompanycode,
   customercompanyname,
   partssalesordertypeid,
   partssalesordertypecode,
   partssalesordertypename,
   coefficient,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_customerorderpricegrade.nextval,
         2,
         v.id,
         t.id,
         (select company.code
            from company
           where company.id = t.customercompanyid),
         (select company.name
            from company
           where company.id = t.customercompanyid),
         t.name,
         v.id,
         v.code,
         v.name,
         1,
         1,
         1,
         'Admin',
         sysdate
    from yxdcs.CustomerInformation t
   cross join yxdcs.partssalesordertype v
   where customer.status <> 99 ;
   insert into customerorderpricegrade select * from tmpdata.customerorderpricegrade140707;
   
    select * from tmpdata.customerorderpricegrade140707
