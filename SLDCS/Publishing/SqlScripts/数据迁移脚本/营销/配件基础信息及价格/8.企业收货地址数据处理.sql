create table tmpdata.CompanyAddress140707_1 as select * from dcs.CompanyAddress where rownum<1;
delete from tmpdata.CompanyAddress140619 where company is null
select * from tmpdata.CompanyAddress140619  for update;
select *
  from tmpdata.CompanyAddress140707 t 
 where not exists (select 1
          from yxdcs.company
         where company.code = t.company) 

--1081
insert into tmpdata.CompanyAddress140707_1
  (id,
   companyid,
   regionid,
   provincename,
   cityname,
   countyname,
   contactperson,
   contactphone,
   detailaddress,
   createtime,
   creatorid,
   creatorname,status)
  select s_CompanyAddress.Nextval,
         (select Id from company where company.code = t.company),
         (select t2.id
            from dcs.tiledregion t2
           where t.cityname = t2.cityname
             and t.provincename = t2.provincename
             and t.countyname = t2.countyname) RegionId,
         t.provincename,
         t.cityname,
         t.countyname,
         CONTACTPERSON,
         contactphone,
         t.detailaddress,
         sysdate,
         1,
         'Admin',1
    from tmpdata.CompanyAddress140707 t where   exists(select 1 from company where company.code = t.company)
    and not exists(select 1 from CompanyAddress v where v.companyid=(select id from company where company.code = t.company ));
    
insert into CompanyAddress select * from tmpdata.CompanyAddress140707_1;
