-- 1.创建临时表,存放客户提供的数据。

create table tmpdata.sparepartfromyx140706
(
       图号  varchar2(200),
       配件名称    varchar2(200),
       配件类型    varchar2(200),
       英文名称    varchar2(200),
       研究院图号    varchar2(200),
       规格型号    varchar2(200),
       计量单位    varchar2(200),
       配件特征说明    varchar2(200),
       上一替代件    varchar2(200),
       下一替代件    varchar2(200),
       保质期    varchar2(200),
       最小包装数量  varchar2(200)
);


create table tmpdata.partbranchfromyx140706
(
       品牌    varchar2(200),
       配件图号    varchar2(200),
       配件名称    varchar2(200),
       配件参考图号    varchar2(200),
       损耗类型    varchar2(200),
       是否可采购    varchar2(200),
       是否可销售    varchar2(200),
       最小销售数量    varchar2(200),
       是否可直供    varchar2(200),
       旧件返回政策  varchar2(200),
       配件保修分类编号    varchar2(200),
       配件保修分类名称    varchar2(200),
       供应商编码    varchar2(200),
       供应商名称		varchar2(200),
       供应商图号	varchar2(200)
);



-- 2.创建临时表，存放需要导入正式库的数据。

create table tmpdata.sparepart140706 as
select * from yxdcs.sparepart where 1=2;

create table tmpdata.spareparthistory140706 as
select * from yxdcs.spareparthistory where 1=2;

create table tmpdata.partsbranch140706 as
select * from yxdcs.partsbranch where 1=2;

create table tmpdata.partsbranchhistory140706 as
select * from yxdcs.partsbranchhistory where 1=2;