-- 1.将配件基础信息插入临时表
insert into tmpdata.sparepart140706
  (ID, --1
   CODE, --2
   NAME, --3
   LASTSUBSTITUTE, --4
   NEXTSUBSTITUTE, --5
   SHELFLIFE, --6
   CADCODE, --7
   CADNAME, --8
   PARTTYPE, --9
   SPECIFICATION, --10
   FEATURE, --11
   STATUS, --12
   LENGTH, --13
   WIDTH, --14
   HEIGHT, --15
   VOLUME, --16
   WEIGHT, --17
   MATERIAL, --18
   PACKINGAMOUNT, --19
   PACKINGSPECIFICATION, --20
   PARTSOUTPACKINGCODE, --21
   PARTSINPACKINGCODE, --22
   MEASUREUNIT, --23
   CREATORID, --24
   CREATORNAME, --25
   CREATETIME, --26
   MODIFIERID, --27
   MODIFIERNAME, --28
   MODIFYTIME, --29
   ABANDONERID, --30
   ABANDONERNAME, --31
   ABANDONTIME) --32
  select yxdcs.s_sparepart.nextval, --1
         a.图号, --2
         a.配件名称, --3
         a.上一替代件, --4
         a.下一替代件, --5
         a.保质期, --6
         a.研究院图号, --7
         '', --8
         case a.配件类型
           when '总成件' then
            1
           when '配件' then
            2
           when '精品件' then
            3
           when '辅料' then
            4
         end parttype, --9
         a.规格型号, --10
         a.配件特征说明, --11
         1, --12
         '', --13
         '', --14
         '', --15
         '', --16
         '', --17
         '', --18
         a.最小包装数量, --19
         '', --20
         '', --21
         '', --22
         a.计量单位, --23
         7, --24
         '晨阑测试', --25
         sysdate, --26
         '', --27
         '', --28
         '', --29
         '', --30
         '', --31
         '' --32
    from tmpdata.sparepartfromyx140706 a;



-- 2.将配件基础信息履历导入临时表
insert into tmpdata.spareparthistory140706
  ("ID", --0
   "SPAREPARTID", --1
   "CODE", --2
   "NAME", --3
   "LASTSUBSTITUTE", --4
   "NEXTSUBSTITUTE", --5
   "SHELFLIFE", --6
   "ENGLISHNAME", --7
   "PINYINCODE", --8
   "REFERENCECODE", --9
   "REFERENCENAME", --10
   "CADCODE", --11
   "CADNAME", --12
   "PARTTYPE", --13
   "SPECIFICATION", --14,
   "FEATURE", --15
   "STATUS", --16
   "LENGTH", --17
   "WIDTH", --18
   "HEIGHT", --19
   "VOLUME", --20
   "WEIGHT", --21
   "MATERIAL", --22
   "PACKINGAMOUNT", --23
   "PACKINGSPECIFICATION", --24
   "PARTSOUTPACKINGCODE", --25
   "PARTSINPACKINGCODE", --26
   "MEASUREUNIT", --27
   "CREATORID", --28
   "CREATORNAME", --29
   "CREATETIME") --30
  select yxdcs.s_spareparthistory.nextval, --0
         b.id, --1
         b.code, --2
         b.name, --3
         b.lastsubstitute, --4
         b.nextsubstitute, --5
         b.shelflife, --6
         b.englishname, --7
         b.pinyincode, --8
         b.referencecode, --9
         b.referencename, --10
         b.cadcode, --11
         b.cadname, --12
         b.parttype, --13
         b.specification, --14
         b.feature, --15
         b.status, --16
         b.length, --17
         b.width, --18
         b.height, --19
         b.volume, --20
         b.weight, --21
         b.material, --22
         b.packingamount, --23
         b.packingspecification, --24
         b.partsoutpackingcode, --25
         b.partsinpackingcode, --26
         b.measureunit, --27
         b.creatorid, --28
         b.creatorname, --29
         b.createtime --30
    from tmpdata.sparepart140706 b;


--3.将配件基础信息导入正式表：

insert into yxdcs.sparepart
select * from tmpdata.sparepart140706;


--4.将配件基础信息履历导入正式表：

insert into yxdcs.spareparthistory
select * from tmpdata.spareparthistory140706;

