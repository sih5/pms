--1.将配件分品牌信息导入临时表：

insert into tmpdata.partsbranch140706(
       "ID",--1
       "PARTID",--2
       "PARTCODE",--3
       "PARTNAME",--4
       "REFERENCECODE",--5
       "STOCKMAXIMUM",--6
       "STOCKMINIMUM",--7
       "BRANCHID",--8
       "BRANCHNAME",--9
       "PARTSSALESCATEGORYID",--10
       "PARTSSALESCATEGORYNAME",--11
       "ABCSTRATEGYID",--12
       "PRODUCTLIFECYCLE",--13
       "LOSSTYPE",--14
       "ISORDERABLE",--15
       "PURCHASECYCLE",--16
       "ISSERVICE",--17
       "ISSALABLE",--18
       "PARTSRETURNPOLICY",--19
       "ISDIRECTSUPPLY",--20
       "WARRANTYSUPPLYSTATUS",--21
       "MINSALEQUANTITY",--22
       "PARTABC",--23
       "PURCHASEROUTE",--24
       "REPAIRMATMINUNIT",--25
       "PARTSWARRANTYCATEGORYID",--26
       "PARTSWARRANTYCATEGORYCODE",--27
       "PARTSWARRANTYCATEGORYNAME",--28
       "PARTSWARHOUSEMANAGEGRANULARITY",--29
       "STATUS",--30
       "CREATORID",--31
       "CREATORNAME",--32
       "CREATETIME",--33
       "MODIFIERID",--34
       "MODIFIERNAME",--35
       "MODIFYTIME",--36
       "ABANDONERID",--37
       "ABANDONERNAME",--38
       "ABANDONTIME",--39
       "REMARK")--40
select yxdcs.s_partsbranch.nextval,--1
       (select s.id from yxdcs.sparepart s where a.配件图号=s.code) partid,--2
       a.配件图号,--3
       (select s.name from yxdcs.sparepart s where a.配件图号=s.code) partname,--4
       a.配件参考图号,--5
       9999999,--6
       0,--7
       2,--8
       (select name from yxdcs.company where id=2) branchid, --9
       (select id from yxdcs.partssalescategory p where p.name=a.品牌) partssalescategoryid,--10
       a.品牌,--11
       '',--12
       '',--13
       case a.损耗类型 when '非常用件' then 1 when '常用件' then 2 when '易耗件' then 3 when '大总成' then 4 end losstype,--14
       case a.是否可采购 when '是' then 1 when '否' then 0 end isorderable,--15
       '',--16
       '',--17
       case a.是否可销售 when '是' then 1 when '否' then 0 end issaleable,--18
       case a.旧件返回政策 when '返回本部' then 1 when '供应商自行回收' then 2 when '监督处理' then 3 when '自行处理' then 4 when '总部调件' then 5 end partsreturnpolicy,--19
       case a.是否可直供 when '是' then 1 when '否' then 0 end isdirectsupply,--20
       '',--21
       a.最小销售数量,--22
       '',--23
       '',--24
       '',--25
       (select id from yxdcs.partswarrantycategory p where p.code=a.配件保修分类编号) partswarrantycategorydi ,--26
       a.配件保修分类编号,--27
       (select name from yxdcs.partswarrantycategory p where p.code=a.配件保修分类编号) partswarrantycategoryname,--28
       '',--29
       1,--30
       7, --31
       '晨阑测试', --32
       sysdate, --33
       '',--34
       '',--35
       '',--36
       '',--37
       '',--38
       '',--39
       ''
 from tmpdata.partbranchfromyx140706 a where exists 
 (select * from yxdcs.sparepart s where a.配件图号=s.code);   
 
 
 
 --2.将配件分品牌信息履历导入临时表：
 insert into tmpdata.partsbranchhistory140706
  ("ID",
   "PARTSBRANCHID",
   "PARTID",
   "PARTCODE",
   "PARTNAME",
   "REFERENCECODE",
   "STOCKMAXIMUM",
   "STOCKMINIMUM",
   "BRANCHID",
   "BRANCHNAME",
   "LOSSTYPE",
   "PARTSSALESCATEGORYID",
   "PARTSSALESCATEGORYNAME",
   "ABCSTRATEGYID",
   "PRODUCTLIFECYCLE",
   "ISORDERABLE",
   "PURCHASECYCLE",
   "ISSERVICE",
   "ISSALABLE",
   "PARTSRETURNPOLICY",
   "ISDIRECTSUPPLY",
   "WARRANTYSUPPLYSTATUS",
   "MINSALEQUANTITY",
   "PARTABC",
   "PURCHASEROUTE",
   "PARTSWARRANTYCATEGORYID",
   "PARTSWARRANTYCATEGORYCODE",
   "PARTSWARRANTYCATEGORYNAME",
   "PARTSWARHOUSEMANAGEGRANULARITY",
   "STATUS",
   "CREATORID",
   "CREATORNAME",
   "CREATETIME",
   "REMARK",
   "REPAIRMATMINUNIT")
  select yxdcs.s_partsbranchhistory.nextval,
         b."ID",
         b."PARTID",
         b."PARTCODE",
         b."PARTNAME",
         b."REFERENCECODE",
         b."STOCKMAXIMUM",
         b."STOCKMINIMUM",
         b."BRANCHID",
         b."BRANCHNAME",
         b."LOSSTYPE",
         b."PARTSSALESCATEGORYID",
         b."PARTSSALESCATEGORYNAME",
         b."ABCSTRATEGYID",
         b."PRODUCTLIFECYCLE",
         b."ISORDERABLE",
         b."PURCHASECYCLE",
         b."ISSERVICE",
         b."ISSALABLE",
         b."PARTSRETURNPOLICY",
         b."ISDIRECTSUPPLY",
         b."WARRANTYSUPPLYSTATUS",
         b."MINSALEQUANTITY",
         b."PARTABC",
         b."PURCHASEROUTE",
         b."PARTSWARRANTYCATEGORYID",
         b."PARTSWARRANTYCATEGORYCODE",
         b."PARTSWARRANTYCATEGORYNAME",
         b."PARTSWARHOUSEMANAGEGRANULARITY",
         b."STATUS",
         b."CREATORID",
         b."CREATORNAME",
         b."CREATETIME",
         b."REMARK",
         b."REPAIRMATMINUNIT"
    from tmpdata.partsbranch140706 b;


--3.将配件基础信息导入正式表：

insert into yxdcs.partsbranch
select * from tmpdata.partsbranch140706 ;


--4.将配件基础信息履历导入正式表：

insert into yxdcs.partsbranchhistory
select * from tmpdata.partsbranchhistory140706 ;

