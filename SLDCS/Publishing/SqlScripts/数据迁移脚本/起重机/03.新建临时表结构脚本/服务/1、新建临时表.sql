

--01-车辆组成库（前8位）
--故障分类
create table tmpdata.MALFUNCTIONCATEGORYQZJ
(
  LV1     VARCHAR2(500),
  LV1NAME VARCHAR2(500),
  LV2     VARCHAR2(500),
  LV2NAME VARCHAR2(500),
  LV3     VARCHAR2(500),
  LV3NAME VARCHAR2(500),
  LV4     VARCHAR2(500),
  LV4NAME VARCHAR2(500)
);



--02-故障模式标准库（故障代码后2位）
create table tmpdata.FaultModeStandardQZJ
(故障模式编码 varchar2(50),
故障模式描述 varchar2(50)
);

--03-维修方式标准库（标准维修工时代码后2位）
create table tmpdata.RepairMethodStandardQZJ
(维修方式编码 varchar2(50),
维修方式名称 varchar2(100),
维修方式描述 varchar2(200)
);


--04-故障代码(组合10位)
create table tmpdata.MalfunctionQZJ 
(MalfunctionCategoryCode varchar2(50),
故障模式编码  varchar2(50),
code varchar2(50),
name varchar2(100)) ;

--05-故障代码与品牌关系
create table tmpdata.MalfunctionBrandRelationQZJ
(MalfunctionCategoryCode varchar2(50),
code varchar2(50),
name varchar2(100),
brand varchar2(100),
remark varchar2(100)) ;


--07-标准维修工时代码（组合10位）
create table tmpdata.repairitemQZJ
(repairitemcategorycode varchar2(20),
维修方式编码 varchar2(20),
code varchar2(50),
name varchar2(600));


--08-标准维修工时代码与产品线关系
create table tmpdata.RepairItemAffiServProdLineQZJ 
(repairitemcategorycode varchar2(20),
code varchar2(50),
name varchar2(600),
RepairQualificationGrade varchar2 (50),
productlinecode varchar2 (50),
productlinename varchar2(100),
productlinetype varchar2(100),
DefaultLaborHour number(9,2));


--10-产品信息
create table tmpdata.productQZJ ( 
产品编号	VARCHAR2(50),	
车型编号	VARCHAR2(50),	
车型名称	VARCHAR2(50),		
品牌代码	VARCHAR2(30),		
品牌名称	VARCHAR2(100),			
子品牌代码	VARCHAR2(30),		
子品牌名称	VARCHAR2(100),		
平台代码	VARCHAR2(30),		
平台名称	VARCHAR2(100),		
产品线代码	VARCHAR2(50),			
产品线名称	VARCHAR2(100),		
原始内部编号	VARCHAR2(100),		
公告号	VARCHAR2(50),		
吨位代码	VARCHAR2(50),	
发动机型号代码	VARCHAR2(100),		
发动机生产厂家代码	VARCHAR2(50),		
变速箱型号代码	VARCHAR2(50),	
变速箱生产厂家代码	VARCHAR2(50),		
后桥型号代码	VARCHAR2(50),	
后桥生产厂家代码	VARCHAR2(50),		
轮胎型号代码	VARCHAR2(50),	
轮胎形式代码	VARCHAR2(50),	
制动方式代码	VARCHAR2(50),		
驱动方式代码	VARCHAR2(30),	
车架连接形式代码	VARCHAR2(50),			
整车尺寸	VARCHAR2(50),		
轴距代码	VARCHAR2(50),	
排放标准代码	VARCHAR2(50),	
产品功能代码	VARCHAR2(100)	,	
产品档次代码	VARCHAR2(50),		
车辆类型代码	VARCHAR2(100),		
--车辆类型名称	VARCHAR2(100)	,		
颜色描述	VARCHAR2(100),		
载客个数	NUMBER(9),		
自重	NUMBER(20,4));

--11-服务产品线
create table tmpdata.serviceproductlineQZJ
 (brandcode varchar2 (50),
 brandname varchar2(100),
 pruductcode varchar2(50),
 productname varchar2(100),
 pruducttype varchar2(50));
 
 
 
--12-服务站服务经营权限
create table tmpdata.DealerBusinessPermitQZJ
(服务站编号  varchar2(100),
服务站名称   varchar2(100),
分公司编号   varchar2(100), 
分公司名称  varchar2(100), 
服务产品线编号  varchar2(100),
服务产品线名称  varchar2(100),
工时单价  varchar2(100),
工时单价等级名称  varchar2(100),
工时单价等级编号 varchar2(100)
);

--13-工时单价等级
create table tmpdata.LaborHourUnitPriceGradeQZJ
(
品牌编号 varchar2(50),
品牌名称 varchar2(100),
Code varchar2(50),
name varchar2(100),
productlinecode varchar2(50),
productlinename varchar2(100),
productlinetype varchar2(100),
labhour number(9,2),
remark varchar2 (300) 
);

/*
--16-人员与品牌关系
create table tmpdata.personnelbrandreltmp
 (
 personnelid varchar2(100),
 personnelname varchar2(100),
 brandname varchar2(100));
 */
 
 --17-市场部
create table tmpdata.MarketDepRegionqzj(
brandcode varchar2(50),
regioncode varchar2(50),
regionname varchar2(100),
marketDepCode varchar2(50),
marketDepName varchar2(100),
personnelloginid varchar2(100),
personnelname varchar2(100),
phone varchar2(100),
personneltype varchar2(100),
fax varchar2(100),
postcode varchar2(100),
remark varchar2(200));


--18-外出服务费等级
create table tmpdata.ServiceTripPriceGradeQZJ
 (code varchar2(50),
 name varchar2(100),
 RegionDescription varchar2(200),
 productlinecode varchar2(50),
 productlinename varchar2(100),
 productlinetype varchar2(50) ,
 ServiceTripType varchar2(50),
 maxmile number(9,2),
 minmile number (9,2),
 FixedAmount number(9,4),
 IsSelfCar varchar2(50),
 SubsidyPrice number(9,4),
 PricePrice number(9,4),
 remark varchar2 (200) );
 
 --19-星级
create table tmpdata.GradeCoefficientQZJ(
brand varchar2(100),
name varchar2(100),
Coefficient number(9,2));

--20-整车保修条款
create table tmpdata.VehicleWarrantyTermqzj (
WarrantyPolicyCode varchar2(100),
WarrantyPolicyName varchar2(100),
PartsWarrantyCategoryCode varchar2(100),
PartsWarrantyCategoryName varchar2(100),
WorkingHours int,
Capacity int,
MaxMileage int,
MinMileage int, 
DaysUpperLimit int, 
DaysLowerLimit int,
remark varchar2(100));

--21-整车保养条款
create table tmpdata.VehicleMainteTermQZJ
(WarrantyPolicycode varchar2(50),
WarrantyPolicyname varchar2(100),
code varchar2(50),
name varchar2(100),
MainteType varchar2(10),
BillingMethod varchar2(20),
WorkingHours number(9),
Capacity number(9),
MaxMileage number(9),
minmileage number(9),
DaysUpperLimit number(9),
 DayslowerLimit number(9),
 LaborCost number(9,4),
 MaterialCost number(9,4) ,
 OtherCost number(9,4) ,
 remark varchar2(200));
 
 --22-整车品牌产品线与服务品牌产品线关系
 create table tmpdata.ServiceProdLineProductQZJ
 (mainbrandcode varchar2(50),
 mainbrandname varchar2(100),
 subbrandcode varchar2(50),
 subbrandname varchar2(100),
 TerraceCode varchar2(50),
 TerraceName varchar2(100),
 productlinecode varchar2(50),
 productlinename varchar2(100),
 serviceproductlinecode varchar2(50),
 serviceproductlinename varchar2(100));
 

 --23-保修政策
 create table tmpdata.WarrantyPolicyQZJ
 (
BranchCode varchar2(50),
BranchName varchar2(100),
code varchar2(50),
name varchar2(100),
Category varchar(10),
RepairTermInvolved varchar2(2),
MainteTermInvolved varchar2(2),
PartsWarrantyTermInvolved varchar2(2),
ReleaseDate date);

--24-保修政策与服务产品线关系
create table tmpdata.WarrantyPolicyNServProdLineQZJ(
WarrantyPolicycode varchar2(50),
WarrantyPolicyname varchar2(100),
productlinecode varchar2(50), 
productlinename varchar2(100),
productlinetype varchar2(100),
salesstarttime date ,
saleendtime date,
remark varchar2（200));

--25-服务站分品牌管理信息  
create table tmpdata.DealerServiceInfoQZJ
(
服务站编号 varchar2(100),
服务站名称 varchar2(100),
/*业务编码 varchar2(100),
业务名称 varchar2(100),*/
品牌 varchar2(100),
市场部 varchar2(100),
业务能力 varchar2(100),
服务站类别 varchar2(100),
区域 varchar2(100),
区域类别 varchar2(100),
维修权限 varchar2(100),
授权时间 date,
是否职守 varchar2(100),
小时热线 varchar2(100),
固定电话 varchar2(100),
传真 varchar2(100),
订货默认仓库  varchar2(100),
--服务站 varchar2(100),
服务站维修权限 varchar2(100),
配件管理费率 varchar2(100),
外出服务费等级 varchar2(100),
星级 varchar2(100),

外出服务半径 varchar2(100),
旧件仓库名称  varchar2(100),
配件储存金额 varchar2(100),
材料费开票类型 varchar2(100),
工时费开票类型 varchar2(100),
工时费开票系数 int,
材料费开票系数 int,
销售与服务场地布局关系 varchar2(100)
   );
   
/*--30-配件保修条款
create table tmpdata.PartsWarrantyTermqzj (
WarrantyPolicyCode varchar2(100),
WarrantyPolicyName varchar2(100),
PartsWarrantyCategoryCode varchar2(100),
PartsWarrantyCategoryName varchar2(100),
WarrantyPeriod int,--工作时长天
Capacity int, --方量
WarrantyMileage int, --保修里程
WorkingHours int,
remark  varchar2(100)); *\

--26-PMS企业主数据模板-服务站-专卖店

--27-二级站
create table tmpdata.SubDealer (
subdealername varchar2(100),
subdealercode varchar2(50),
dealercode varchar2(50),
dealername varchar2(100),
dealertype varchar2(100),
address varchar2(200),
DealerManager varchar2(200),
manager varchar2(50),
phone varchar2(50),
mobile varchar2(50),
email varchar2(100),
remark varchar2(200));

--28-强制保养条款车型清单数据
create table tmpdata.VehicleMainteProductCategory (
ProductCategoryCode varchar2(100),
productline varchar2(100),
materialfee number(9,4));

--29-供应商信息*/


--车辆信息
create table tmpdata.VEHICLEINFORMATIONqzj(
vin varchar2(100),
整车编号 varchar2(100),
底盘编号 varchar2(100),
发动机型号代码 varchar2(100),
发动机序编号 varchar2(100),
出厂日期 date,
整车产品新名称 varchar2(100),
底盘品牌 varchar2(100),
品牌  varchar2(100),
销售日期 date,
--保修截止日期 date,
车桥类型 varchar2(100),
是否公路车 varchar2(100),
是否售出 varchar2(100),
方量 int,
工作小时 int,
行驶里程 int,
车牌号 varchar2(100),
大客户车辆	varchar2(100),
车系 varchar2(100),
经销商名称 varchar2(100),	
车型平台 varchar2(100),
车辆功能	varchar2(100),
车身颜色 varchar2(100),
上装编码 varchar2(100),
改装类型 varchar2(100),
改装单位名称 varchar2(100),
驾驶室型号 varchar2(100),
驱动方式代码	varchar2(100),
前桥号	varchar2(100),
前二桥号	varchar2(100),
中桥号	varchar2(100),
后桥号 varchar2(100),
后桥型号及吨级 varchar2(100),	
平衡轴号 varchar2(100),	
转向机 varchar2(100),
销售车型编号	varchar2(100),
销售车型名称	varchar2(100),
发动机厂家	varchar2(100),
公告号	varchar2(100),
变速箱型号	varchar2(100),
变速箱序编号	varchar2(100),
首次故障里程 int);

--基础客户
create table tmpdata.customerqzj(
vin varchar2(100),
姓名 varchar2(100),
性别 varchar2(100),
客户类型 varchar2(100),
省 varchar2(100),
市 varchar2(100),
县 varchar2(100),
详细地址 varchar2(100),
邮政编码 varchar2(100),
手机号码 varchar2(100),
家庭电话 varchar2(100),
出生日期 date,
电子邮件 varchar2(100))






 
 
 


