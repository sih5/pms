-- Create table
create table CODETEMPLATE
(
  ID              NUMBER(9) not null,
  NAME            VARCHAR2(100) not null,
  ENTITYNAME      VARCHAR2(100) not null,
  ISACTIVED       NUMBER(1) not null,
  TEMPLATE        VARCHAR2(200) not null,
  RESETTYPE       NUMBER(9) not null,
  REMARK          VARCHAR2(200),
  CORPORATIONID   NUMBER(9),
  CORPORATIONNAME VARCHAR2(100),
  CREATORID       NUMBER(9),
  CREATORNAME     VARCHAR2(100),
  CREATETIME      DATE,
  MODIFIERID      NUMBER(9),
  MODIFIERNAME    VARCHAR2(100),
  MODIFYTIME      DATE,
  ROWVERSION      TIMESTAMP(6)
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table CODETEMPLATE
  add constraint PK_CODETEMPLATE primary key (ID)
 ;
-- Create/Recreate indexes 
create index IDX_CODETEMPLATE_CORPORATIONID on CODETEMPLATE (CORPORATIONID);
create index IDX_CODETEMPLATE_NAME on CODETEMPLATE (NAME);
TRUNCATE TABLE CodeTemplate;

--配件销售订单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsSalesOrder', 'PartsSalesOrder', 1, 'PSO{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--配件销售退货单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsSalesReturnBill', 'PartsSalesReturnBill', 1, 'PSR{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--旧件借用单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'UsedPartsLoanBill', 'UsedPartsLoanBill', 1, 'UPL{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--旧件入库单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'UsedPartsInboundOrder', 'UsedPartsInboundOrder', 1, 'UPI{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);
--旧件出库单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'UsedPartsOutboundOrder', 'UsedPartsOutboundOrder', 1, 'UPO{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);
--旧件加工单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'UsedPartsRefitBill', 'UsedPartsRefitBill', 1, 'UPR{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--旧件处理单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'UsedPartsDisposalBill', 'UsedPartsDisposalBill', 1, 'UPD{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--旧件移库单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'UsedPartsShiftOrder', 'UsedPartsShiftOrder', 1, 'UPM{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--旧件调拨单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'UsedPartsTransferOrder', 'UsedPartsTransferOrder', 1, 'UPT{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);
--配件采购订单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsPurchaseOrder', 'PartsPurchaseOrder', 1, 'PPO{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--配件采购退货单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsPurReturnOrder', 'PartsPurReturnOrder', 1, 'PPR{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--供应商发运单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'SupplierShippingOrder', 'SupplierShippingOrder', 1, 'PSS{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--旧件发运单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'UsedPartsShippingOrder', 'UsedPartsShippingOrder', 1, 'UPS{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--服务站旧件处理单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'SsUsedPartsDisposalBill', 'SsUsedPartsDisposalBill', 1, 'UPSD{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--内部领入单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'InternalAcquisitionBill', 'InternalAcquisitionBill', 1, 'IAL{CORPCODE}{DATE:yyyyMMdd}{SERIAL:00}', 1, NULL);
--内部领出单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'InternalAllocationBill', 'InternalAllocationBill', 1, 'IAC{CORPCODE}{DATE:yyyyMMdd}{SERIAL:00}', 1, NULL);
--外出服务索赔单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'ServiceTripClaimBill', 'ServiceTripClaimBill', 1, 'STC{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--外出索赔申请单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'ServiceTripClaimApplication', 'ServiceTripClaimApplication', 1, 'STCA{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--维修保养索赔单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'RepairClaimBill', 'RepairClaimBill', 1, 'RC{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--维修保养索赔单维修材料清单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'RepairClaimMaterialDetail', 'RepairClaimMaterialDetail', 1, 'RCM{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);
--维修保养索赔申请单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'RepairClaimApplication', 'RepairClaimApplication', 1, 'RCA{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--配件索赔单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsClaimOrder', 'PartsClaimOrder', 1, 'PC{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--配件索赔单维修材料清单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsClaimMaterialDetail', 'PartsClaimMaterialDetail', 1, 'PCM{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);
--配件索赔申请单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsClaimApplication', 'PartsClaimApplication', 1, 'PCA{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--扣补款单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'ExpenseAdjustmentBill', 'ExpenseAdjustmentBill', 1, 'EA{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--工时单价标准
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'LaborHourUnitPriceRate', 'LaborHourUnitPriceRate', 1, 'WHP{SERIAL:000000}', 1, NULL);
--外出服务费标准
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'ServiceTripPriceRate', 'ServiceTripPriceRate', 1, 'OTP{SERIAL:000000}', 1, NULL);
--配件入库计划
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsInboundPlan', 'PartsInboundPlan', 1, 'PIP{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);
--配件入库检验单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsInboundCheckBill', 'PartsInboundCheckBill', 1, 'RK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);
--配件包装单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsPacking', 'PartsPacking', 1, 'PP{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);
--配件出库计划
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsOutboundPlan', 'PartsOutboundPlan', 1, 'POP{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);
--配件出库单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsOutboundBill', 'PartsOutboundBill', 1, 'CK{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);
--配件盘点单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsInventoryBill', 'PartsInventoryBill', 1, 'PI{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);
--配件移库单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsShiftOrder', 'PartsShiftOrder', 1, 'PM{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);
--配件调拨单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsTransferOrder', 'PartsTransferOrder', 1, 'PT{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);
--积压件申请单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'OverstockPartsApp', 'OverstockPartsApp', 1, 'OPA{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);
--积压件调剂单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'OverstockPartsAdjustBill', 'OverstockPartsAdjustBill', 1, 'OPAB{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);
--配件发运单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsShippingOrder', 'PartsShippingOrder', 1, 'FY{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);
--配件采购价格变更申请单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsPurchasePricingChange', 'PartsPurchasePricingChange', 1, 'PPPC{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--保修政策
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'WarrantyPolicy', 'WarrantyPolicy', 1, 'WP{CORPCODE}{DATE:yyyy}{SERIAL:0000}', 1, NULL);
--整车保养条款
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleMainteTerm', 'VehicleMainteTerm', 1, 'VMT{CORPCODE}{DATE:yyyy}{SERIAL:0000}', 1, NULL);
--整车保修条款
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleWarrantyTerm', 'VehicleWarrantyTerm', 1, 'VWT{CORPCODE}{DATE:yyyy}{SERIAL:0000}', 1, NULL);
--计划价申请单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PlannedPriceApp', 'PlannedPriceApp', 1, 'PPA{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--仓库成本变更单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'WarehouseCostChangeBill', 'WarehouseCostChangeBill', 1, 'WCC{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--配件批次记录
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsBatchRecord', 'PartsBatchRecord', 1, 'P{CORPCODE}{SERIAL:000000}', 1, NULL);
--配件零售订单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsRetailOrder', 'PartsRetailOrder', 1, 'PRO{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000}', 1, NULL);
--配件零售退货单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsRetailReturnBill', 'PartsRetailReturnBill', 1, 'PRR{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000}', 1, NULL);
--配件采购结算单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsPurchaseSettleBill', 'PartsPurchaseSettleBill', 1, 'PPS{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--配件采购退货结算单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsPurchaseRtnSettleBill', 'PartsPurchaseRtnSettleBill', 1, 'PPRS{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--服务站索赔结算单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'SsClaimSettlementBill', 'SsClaimSettlementBill', 1, 'SCS{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--配件销售订单处理
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsSalesOrderProcess', 'PartsSalesOrderProcess', 1, 'PSOP{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--客户转账单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'CustomerTransferBill', 'CustomerTransferBill', 1, 'PCTB{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--来款单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PaymentBill', 'PaymentBill', 1, 'PPB{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--来款分割单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PaymentBeneficiaryList', 'PaymentBeneficiaryList', 1, 'PPBL{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车总部月目标（项目）
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'OEMVehicleMonthQuota', 'OEMVehicleMonthQuota', 1, 'HT{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车经销商月目标（项目）
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'DealerVehicleMonthQuota', 'DealerVehicleMonthQuota', 1, 'DT{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车订单日历
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleOrderSchedule', 'VehicleOrderSchedule', 1, 'OD{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--VIN调整履历
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VINChangeRecord', 'VINChangeRecord', 1, 'VA{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车订单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleOrder', 'VehicleOrder', 1, '{CORPCODE}OR{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车订单清单
--整车发运单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleShippingOrder', 'VehicleShippingOrder', 1, '{CORPCODE}FY{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车订货计划
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleOrderPlan', 'VehicleOrderPlan', 1, '{CORPCODE}SP{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车退货单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleReturnOrder', 'VehicleReturnOrder', 1, '{CORPCODE}RB{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车审批单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleShipplanApproval', 'VehicleShipplanApproval', 1, '{CORPCODE}OP{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车订单变更单

--整车订单变更策略

--整车批发价格变更申请单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehPifaPriceChangeApp', 'VehPifaPriceChangeApp', 1, 'PPCA{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车批发价格折扣等级
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehPifaPriceDiscLevel', 'VehPifaPriceDiscLevel', 1, 'PPDL{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车零售价格变更申请单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehRetailPriceChangeApp', 'VehRetailPriceChangeApp', 1, 'RPCA{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车销售类型
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleSalesType', 'VehicleSalesType', 1, 'SO{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车销售组织
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleSalesOrganization', 'VehicleSalesOrganization', 1, 'ST{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--配件销售结算单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsSalesSettlement', 'PartsSalesSettlement', 1, 'PIS{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--配件销售退货单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsSalesRtnSettlement', 'PartsSalesRtnSettlement', 1, 'PIRS{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--发票信息
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'InvoiceInformation', 'InvoiceInformation', 1, 'PII{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--经销商预测单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'DealerForecast', 'DealerForecast', 1, '{CORPCODE}DF{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车批发价格
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleWholesalePrice', 'VehicleWholesalePrice', 1, 'VWP{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车批发协议价
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleWholesalePactPrice', 'VehicleWholesalePactPrice', 1, 'VWPP{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--信用申请单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'CredenceApplication', 'CredenceApplication', 1, 'CA{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--账户组
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'AccountGroup', 'AccountGroup', 1, 'AG{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000}', 1, NULL);
--整车来款单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehiclePaymentBill', 'VehiclePaymentBill', 1, '{CORPCODE}VPB{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车来款方式
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehiclePaymentMethod', 'VehiclePaymentMethod', 1, 'VPM{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车经销商信用额度申请单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleDealerCreditLimitApp', 'VehicleDealerCreditLimitApp', 1, '{CORPCODE}VDC{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车经销商建店保证金
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleDLRStartSecurityDeposit', 'VehicleDLRStartSecurityDeposit', 1, '{CORPCODE}VDS{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车经销商账户冻结单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleDLRAccountFreezeBill', 'VehicleDLRAccountFreezeBill', 1, '{CORPCODE}VDA{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--整车资金类型
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleFundsType', 'VehicleFundsType', 1, 'VFT{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--旧件清退单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'UsedPartsReturnOrder', 'UsedPartsReturnOrder', 1, 'UPRO{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000}', 1, NULL);
--配件领用结算单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsRequisitionSettleBill', 'PartsRequisitionSettleBill', 1, 'PRSB{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000}', 1, NULL);
--客户开户申请单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'CustomerOpenAccountApp', 'CustomerOpenAccountApp', 1, 'COA{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--仓库
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'Warehouse', 'Warehouse', 1, 'VWH{CORPCODE}{SERIAL:000}', 1, NULL);
--批售审批单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'WholesaleApproval', 'WholesaleApproval', 1, '{CORPCODE}WA{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--多级审核岗位设置单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'AuditHierarchySetting', 'AuditHierarchySetting', 1, 'AHS{SERIAL:0000}', 1, NULL);
--多级审核配置编号
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'MultiLevelAuditConfig', 'MultiLevelAuditConfig', 1, 'MLAC{SERIAL:0000}', 1, NULL);
--批售奖励申请单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'WholesaleRewardApp', 'WholesaleRewardApp', 1, '{CORPCODE}WRA{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--大客户信息
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'KeyAccount', 'KeyAccount', 1, 'CAM{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--配件销售价格变更申请
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsSalesPriceChange', 'PartsSalesPriceChange', 1, 'PSPC{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--财务快照设置
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'FinancialSnapshotSet', 'FinancialSnapshotSet', 1, 'FSS{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--维修单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'RepairOrder', 'RepairOrder', 1, 'REP{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--企业间配件调拨单 
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'CompanyTransferOrder', 'CompanyTransferOrder', 1, '{CORPCODE}CTO{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--配件返利申请单   
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsRebateApplication', 'PartsRebateApplication', 1, 'FAL{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--经销商配件盘点单   
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'DealerPartsInventoryBill', 'DealerPartsInventoryBill', 1, 'DPI{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--维修工单       
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'RepairWorkOrder', 'RepairWorkOrder', 1, 'REPW{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--配件外采申请单   
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsOuterPurchaseChange', 'PartsOuterPurchaseChange', 1, 'POPC{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--单车保修卡   
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleWarrantyCard', 'VehicleWarrantyCard', 1, 'VWC{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--供应商开户申请单
insert into CodeTemplate (ID, NAME, ENTITYNAME, ISACTIVED, TEMPLATE, RESETTYPE, REMARK) values (S_CodeTemplate.Nextval, 'SupplierOpenAccountApp', 'SupplierOpenAccountApp',1,'SOA{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}',1,NULL);
--售前检查单
insert into CodeTemplate (ID, NAME, ENTITYNAME, ISACTIVED, TEMPLATE, RESETTYPE, REMARK) values (S_CodeTemplate.Nextval, 'PreSaleCheckOrder', 'PreSaleCheckOrder',1,'PSC{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}',1,NULL);
--付款单
insert into CodeTemplate (ID, NAME, ENTITYNAME, ISACTIVED, TEMPLATE, RESETTYPE, REMARK) values (S_CodeTemplate.Nextval, 'PayOutBill', 'PayOutBill', 1, 'POB{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1,NULL);
--维修模板
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'RepairTemplet', 'RepairTemplet', 1, 'TR{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
--二级站需求计划
insert into CodeTemplate (ID, NAME, ENTITYNAME, ISACTIVED, TEMPLATE, RESETTYPE, REMARK) values (S_CodeTemplate.Nextval, 'SecondClassStationPlan', 'SecondClassStationPlan', 1, 'SCSP{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1,  NULL);
--服务站零售订单编号
insert into CodeTemplate (ID, NAME, ENTITYNAME, ISACTIVED, TEMPLATE, RESETTYPE, REMARK) values (S_CodeTemplate.Nextval, 'DealerPartsRetailOrder', 'DealerPartsRetailOrder',1,'DRO{CORPCODE}{DATE:yyyyMMdd}{SERIAL:00000}',1,NULL);
--服务活动
insert into CodeTemplate (ID, NAME, ENTITYNAME, ISACTIVED, TEMPLATE, RESETTYPE, REMARK) values (S_CodeTemplate.Nextval, 'ServiceActivity', 'ServiceActivity',1,'SA{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}',1,NULL);
--市场AB质量信息快报
insert into CodeTemplate (ID, NAME, ENTITYNAME, ISACTIVED, TEMPLATE, RESETTYPE, REMARK) values (S_CodeTemplate.Nextval,'MarketABQualityInformation', 'MarketABQualityInformation',1,'KB{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000}',1,NULL);
--公告
insert into CodeTemplate (ID, NAME, ENTITYNAME, ISACTIVED, TEMPLATE, RESETTYPE, REMARK) values (S_CodeTemplate.Nextval,'Notification', 'Notification',1,'GG{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000}',1,NULL);
--供应商扣补款单
insert into CodeTemplate (ID, NAME, ENTITYNAME, ISACTIVED, TEMPLATE, RESETTYPE, REMARK) values (S_CodeTemplate.Nextval,'SupplierExpenseAdjustBill', 'SupplierExpenseAdjustBill',1,'SEA{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}',1,NULL);
--供应商索赔结算单
insert into CodeTemplate (ID, NAME, ENTITYNAME, ISACTIVED, TEMPLATE, RESETTYPE, REMARK) values (S_CodeTemplate.Nextval,'SupplierClaimSettlementBill', 'SupplierClaimSettlementBill',1,'SCSB{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000000}',1,NULL);
--客户信息  
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'Customer', 'Customer', 1, 'CSM{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);
--车辆联系人  
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'VehicleLinkman', 'VehicleLinkman', 1, 'VL{DATE:yyyyMMdd}{SERIAL:000000}', 1, NULL);

--标签打印编码规则  
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'WMSLabelPrintingSerial', 'WMSLabelPrintingSerial', 1, '{SERIAL:000000}', 1, NULL);