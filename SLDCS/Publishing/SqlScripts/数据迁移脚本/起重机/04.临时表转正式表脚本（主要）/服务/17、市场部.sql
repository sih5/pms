

   --营销分公司市场部
   
   insert into marketingdepartment
     (id,
      branchid,
      partssalescategoryid,
      businesstype,
      code,
      name,
      address,
      phone,
      fax,
      postcode,
      status,
      remark,
      creatorid,
      creatorname,
      createtime)
     select s_marketingdepartment.nextval,
            (select id from branch where code='2470'）,
            (select id from partssalescategory where name = v.brandcode),
            decode(v.personneltype, '销售', 1, '服务', 2),
            v.marketdepcode,
            v.marketdepname,
            '',
            v.phone,
            v.fax,
            v.postcode,
            1,
            '',
            1,
            'Admin',
            sysdate
       from (select distinct t.brandcode,
                    t.marketdepcode,
                    t.marketdepname,
                    t.fax,
                    t.postcode,
                    t.phone,
                    t.personneltype 
               from tmpdata.MarketDepRegionqzj t) v;
   
    