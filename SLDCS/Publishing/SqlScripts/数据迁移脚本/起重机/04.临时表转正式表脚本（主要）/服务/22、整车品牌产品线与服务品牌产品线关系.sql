
--整车品牌产品线与服务品牌产品线关系
create table tmpdata.serviceprodlineproductqzj_01 as 
select t.id,
       (select v.serviceproductlinecode
          from tmpdata.ServiceProdLineProductqzj v
         where t.vehiclebrandcode = v.mainbrandcode
           and t.vehiclebrandname = v.mainbrandname
           and t.subbrandcode = v.subbrandcode
           and t.subbrandname = v.subbrandname
           and t.terracecode=v.terracecode
           and t.terracename =v.terracename
           and t.vehicleproductline = v.productlinecode
           and t.vehicleproductname = v.productlinename ) serviceproductlinecode,
       0 as serviceproductlineid,
       '整车品牌产品线与服务品牌产品线关系' as serviceproductlinename,
       0 as brandid,
       0 as PartsSalesCategoryId,
       '整车品牌产品线与服务品牌产品线关系' as SalesCategoryName,
       '整车品牌产品线与服务品牌产品线关系' as SalesCategoryCode
  from serviceprodlineproduct t
 where exists (select 1
          from tmpdata.ServiceProdLineProductqzj v
         where t.vehiclebrandcode = v.mainbrandcode
           and t.vehiclebrandname = v.mainbrandname
           and t.subbrandcode = v.subbrandcode
           and t.subbrandname = v.subbrandname
           and t.terracecode=v.terracecode
           and t.terracename =v.terracename 
           and t.vehicleproductline = v.productlinecode
           and t.vehicleproductname = v.productlinename);
           

           
           update tmpdata.serviceprodlineproductqzj_01 t
              set t.serviceproductlineid  =
                  (select productlineid
                     from serviceproductlineview v
                    where v.ProductLineCode = t.serviceproductlinecode);
                              
           update tmpdata.serviceprodlineproductqzj_01 t
              set 
                  t.serviceproductlinename =
                  (select ProductLineName
                     from serviceproductlineview v
                    where v.ProductLineCode = t.serviceproductlinecode);
                    
           update tmpdata.serviceprodlineproductqzj_01 t
              set 
                    t.PartsSalesCategoryId=(select v.Partssalescategoryid from serviceproductlineview v where v.ProductLineCode=t.serviceproductlinecode);
                    
            update tmpdata.serviceprodlineproductqzj_01 t
              set 
                    t.brandid=(select brandid from serviceproductlineview v where v.ProductLineCode=t.serviceproductlinecode);
                    
                  
                    update tmpdata.serviceprodlineproductqzj_01 t
                       set t.SalesCategoryName =
                           (select v.name
                              from partssalescategory v
                             where v.id = t.PartsSalesCategoryId),
                           t.SalesCategoryCode =
                           (select v.code
                              from partssalescategory v
                             where v.id = t.PartsSalesCategoryId);


update serviceprodlineproduct t
   set t.serviceproductlineid  =
       (select v.serviceproductlineid
          from tmpdata.serviceprodlineproductqzj_01 v
         where v.id = t.id),
       t.serviceproductlinecode =
       (select v.serviceproductlinecode
          from tmpdata.serviceprodlineproductqzj_01 v
         where v.id = t.id),
       t.serviceproductlinename =
       (select v.serviceproductlinename
          from tmpdata.serviceprodlineproductqzj_01 v
         where v.id = t.id),
       t.partssalescategoryid  =
       (select v.PartsSalesCategoryId
          from tmpdata.serviceprodlineproductqzj_01 v
         where v.id = t.id),
       t.salescategorycode     =
       (select v.SalesCategoryCode
          from tmpdata.serviceprodlineproductqzj_01 v
         where v.id = t.id),
       t.salescategoryname     =
       (select v.SalesCategoryName
          from tmpdata.serviceprodlineproductqzj_01 v
         where v.id = t.id)
 where exists (select 1
          from tmpdata.serviceprodlineproductqzj_01 v
         where v.id = t.id);
 
