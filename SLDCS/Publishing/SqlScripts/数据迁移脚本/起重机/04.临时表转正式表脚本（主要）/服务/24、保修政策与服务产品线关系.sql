
insert into warrantypolicynservprodline   
  (id,
   warrantypolicyid,
   serviceproductlineid,
   validationdate,
   expiredate,
   productlinetype,
   remark,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_warrantypolicynservprodline.nextval,
         (select id from warrantypolicy where code = t.warrantypolicycode),
         (select b.ProductLineId
            from serviceproductlineview b
           where b.ProductLineCode = t.productlinecode),
         t.salesstarttime,
         t.saleendtime,
         (select b.ProductLineType
            from serviceproductlineview b
           where b.ProductLineCode = t.productlinecode),
         t.remark,
         1,
         1,
         'Admin',
         sysdate
    from tmpdata.WarrantyPolicyNServProdLineQZJ t where t.productlinecode in (select productlinecode from serviceproductlineview);
    
   
    


insert into warrpolnservprodlinehistory
  (id,
   warrantypolicynservprodlineid,
   warrantypolicyid,
   serviceproductlineid,
   validationdate,
   expiredate,
   productlinetype,
   remark,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_warrpolnservprodlinehistory.nextval,
         c.id,
         b.id as warrantypolicyid,
         c.serviceproductlineid,
         c.validationdate,
         c.expiredate,
         c.productlinetype,
         c.remark,
         c.status,
         c.creatorid,
         c.creatorname,
         c.createtime
    from tmpdata.WarrantyPolicyNServProdLineQZJ a inner join WarrantyPolicy b on a.warrantypolicycode=b.code
    inner join ServiceProductLine on ServiceProductLine.code=a.productlinecode
  inner join   warrantypolicynservprodline c on b.id=c.warrantypolicyid and c.serviceproductlineid=ServiceProductLine.id

