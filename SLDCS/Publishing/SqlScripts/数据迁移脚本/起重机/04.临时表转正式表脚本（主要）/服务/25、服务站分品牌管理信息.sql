


insert into DealerServiceInfo
  (Id,
   BranchId,
   PartsSalesCategoryId,
   DealerId,
   MarketingDepartmentId,
   UsedPartsWarehouseId,
   ChannelCapabilityId,
   PartsManagingFeeGradeId,
   GradeCoefficientId,
   OutServiceradii,
   BusinessDivision,
   AccreditTime,
   ServicePermission,
   ServiceStationType,
   OutFeeGradeId,
   RepairAuthorityGrade,
   Status,
   CreateTime,
   CreatorName,
   isonduty,
   remark,
 businesscode,
 businessname,
   PartReserveAmount)
  select s_dealerserviceinfo.nextval,
         (select id from branch where code='2470'),
         c.id partssalescategoryid,
         b.id dealerid,
         nvl(d.id, -1) marketingdepartmentid,
         e.id usedpartswarehouseid,
         f.id channelcapabilityid,
         g.id partsmanagementcostgradeid,
         nvl(h.id,
         -1) gradecoefficientid,
         nvl(a.外出服务半径, 1),
         c.name,
         dsc.BuildTime,
         a.维修权限,
         decode(a.服务站类别,
                '模拟站',
                1,
                '星级服务中心',
                2,
                '快修店',
                4,
                '服务工程师',
                5,
                '标杆服务站',
                6,
                '自保站',
                7,
                3),
         i.id,
         decode(a.服务站维修权限, '大修', 1, '小修',2,1),
         1,
         sysdate,
         'Admin',
         decode(a.是否职守, '是', 1, 0),
         '起重机数据初始化',
       a.服务站编号,
       a.服务站名称,
         a.配件储存金额
    from tmpdata.DealerServiceInfoQZJ a
   inner join dealer b
      on a.服务站编号 = b.code
    left join dealerserviceext dsc
      on dsc.id = b.id
   inner join partssalescategory c
      on c.name = a.品牌
   inner join company
      on company.id = b.id
    left join marketingdepartment d
      on d.partssalescategoryid = c.id
     and d.name = a.市场部
    left join usedpartswarehouse e
      on e.name = a.旧件仓库名称
    left join partsmanagementcostgrade g
      on g.name = a.配件管理费率
    left join gradecoefficient h 
      on h.grade = a.星级 and h.partssalescategoryname='起重机'
     and h.partssalescategoryid = c.id
    left join channelcapability f
      on f.name = a.业务能力
    left join ServiceTripPriceGrade i
      on i.code = a.外出服务费等级
   where not exists (select 1
            from DealerServiceInfo tmp
           where tmp.partssalescategoryid = c.id
             and tmp.dealerid = company.id) and a.服务站编号 is not null
             
             
             
             
             
             
