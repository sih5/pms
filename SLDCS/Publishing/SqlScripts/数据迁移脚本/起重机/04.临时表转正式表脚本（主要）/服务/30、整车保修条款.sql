


--整车保修条款
insert into VehicleWarrantyTerm
（Id,
Code,
Name,
WarrantyPolicyId,
MaxMileage,
MinMileage,
DaysUpperLimit,
DaysLowerLimit,
Capacity,
WorkingHours,
Remark,
status,
CreatorId,
CreatorName,
CreateTime)
select s_PartsWarrantyTerm.Nextval,
PartsWarrantyCategoryCode,
PartsWarrantyCategoryName,
b.id as WarrantyPolicyId,
MaxMileage,
MinMileage,
DaysUpperLimit,
DaysLowerLimit,
Capacity,
WorkingHours,
a.Remark,1,1,'Admin',sysdate
from tmpdata.VehicleWarrantyTermqzj a inner join WarrantyPolicy b on a.warrantypolicycode=b.code;




--整车保修条款履历
insert into VehicleWarrantyTermHistory
（Id,VehicleWarrantyTermId,
Code,
Name,
WarrantyPolicyId,
MaxMileage,
MinMileage,
DaysUpperLimit,
DaysLowerLimit,
Capacity,
WorkingHours,
Remark,
CreatorId,
CreatorName,
CreateTime)
select s_PartsWarrantyTermHistory.Nextval,b.id,
b.code,
b.name,
b.WarrantyPolicyId,
b.MaxMileage,
b.MinMileage,
b.DaysUpperLimit,
b.DaysLowerLimit,
b.Capacity,
b.WorkingHours,
b.Status,
b.CreatorId,
b.CreatorName,
b.CreateTime
from VehicleWarrantyTerm b 
 inner join tmpdata.VehicleWarrantyTermqzj c on c.partswarrantycategorycode=b.code;


