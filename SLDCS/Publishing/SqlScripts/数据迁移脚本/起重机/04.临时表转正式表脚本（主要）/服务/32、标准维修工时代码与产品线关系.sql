

insert into RepairItemAffiServProdLine 
  (id,
   branchid,
   repairitemid,
   serviceproductlineid,
   productlinetype,
   remark,
   status,
   creatorid,
   creatorname,
   createtime,
   defaultlaborhour,
   PartsSalesCategoryId)
  select s_RepairItemAffiServProdLine.Nextval,
         (select id from branch where  code='2470'),
         (select id from repairitem v where v.code = t.code),
         (select productlineid
            from serviceproductlineview a
           where a.ProductLineCode = t.productlinecode),
         (select productlinetype
            from serviceproductlineview a
           where a.ProductLineCode = t.productlinecode),
         '起重机期初数据导入',
         1,
         1,
         'Admin',
         sysdate,
         t.defaultlaborhour,
         (select a.Partssalescategoryid
            from serviceproductlineview a
           where a.ProductLineCode = t.productlinecode)
    from tmpdata.RepairItemAffiServProdLineQZJ t;
    
   
 

 insert into RepairItemBrandRelation
   (id,
    branchid,
    repairitemid,
    repairitemcode,
    repairitemname,
    partssalescategoryid,
    repairqualificationgrade,
    creatorid,
    creatorname,
    createtime,status)
   select s_RepairItemBrandRelation.Nextval,
               (select id from branch where  code='2470'),
          (select id from repairitem v where v.code = t.code),
          t.code,
          t.name,
          (select a.Partssalescategoryid
            from serviceproductlineview a
           where a.ProductLineCode = t.productlinecode), 1, 1, 'Admin', sysdate,1 from tmpdata.RepairItemAffiServProdLineQZJ t;
         
     