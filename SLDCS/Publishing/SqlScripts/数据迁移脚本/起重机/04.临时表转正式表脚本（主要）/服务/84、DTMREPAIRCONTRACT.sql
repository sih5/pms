-- Create table
create table DTMREPAIRCONTRACT
(
  DTMOBJID                     VARCHAR2(40) not null,
  DCSREPAIRORDERID             NUMBER(9),
  CODE                         VARCHAR2(50),
  SERIALCODE                   VARCHAR2(40),
  CUSTOMERNAME                 VARCHAR2(100),
  CELLNUMBER                   VARCHAR2(50),
  ADDRESS                      VARCHAR2(200),
  ZIPCODE                      VARCHAR2(6),
  VIN                          VARCHAR2(50) not null,
  ENGINECODE                   VARCHAR2(50),
  PLATENUMBER                  VARCHAR2(20),
  VEHICLEMODELCODE             VARCHAR2(50),
  VEHICLEMODELNAME             VARCHAR2(100),
  MILEAGE                      NUMBER(9),
  REPAIRTYPE                   NUMBER(9),
  REPAIRTIME                   DATE,
  EXPECTEDFINISHTIME           DATE,
  COMPLETIONTIME               DATE,
  VEHICLEOBJECTS               VARCHAR2(200),
  FUELAMOUNT                   NUMBER(15,6),
  DESCRIPTION                  VARCHAR2(200),
  CUSTOMERCOMPLAINT            VARCHAR2(200),
  WORKINGFEE                   NUMBER(19,4),
  MATERIALFEE                  NUMBER(19,4),
  OTHERFEE                     NUMBER(19,4),
  OTHERFEEDESCRIPTION          VARCHAR2(200),
  ISOUTSERVICE                 NUMBER(1),
  ISOUTCLAIM                   NUMBER(1),
  TECHNICALSERVICEACTIVITYCODE VARCHAR2(50),
  CLAIMSTATUS                  NUMBER(9),
  MAINTENANCESTATUS            NUMBER(9),
  STATUS                       NUMBER(9),
  REMARK                       VARCHAR2(200),
  OUTLETNAME                   VARCHAR2(100),
  VEHICLEREPAIRNAME            VARCHAR2(100),
  VEHICLEREPAIRADDRESS         VARCHAR2(100),
  VEHICLEREPAIRCELLPHONE       VARCHAR2(20),
  BELONGBRAND                  VARCHAR2(50),
  BRANDCODE                    VARCHAR2(50),
  BRANDNAME                    VARCHAR2(100),
  DCSCORPORATIONID             NUMBER(9),
  CORPORATIONID                NUMBER(9),
  CORPORATIONNAME              VARCHAR2(100),
  CREATORID                    NUMBER(9),
  CREATORNAME                  VARCHAR2(100),
  CREATETIME                   DATE,
  MODIFIERID                   NUMBER(9),
  MODIFIERNAME                 VARCHAR2(100),
  MODIFYTIME                   DATE,
  SPTYPE                       NUMBER(9),
  REPAIROBJECTNAME             VARCHAR2(100),
  CAUSEANALYSE                 VARCHAR2(200),
  REGIONCODE                   VARCHAR2(50),
  TRAVELDESTINATION            VARCHAR2(200),
  RANGE                        NUMBER(9),
  TRAVELDISTANCE               NUMBER(9),
  STARTTIME                    DATE,
  ENDTIME                      DATE,
  OUTPERSONCODE                VARCHAR2(100),
  OUTVEHICLEKIND               NUMBER(1),
  OUTDAYS                      NUMBER(9),
  OUTNUMBER                    NUMBER(9),
  HIGHWAYFREIGHT               NUMBER(19,4),
  OUTFAREPRICE                 NUMBER(19,4),
  SUBSIDYPRICE                 NUMBER(19,4),
  TOTALEXPENSE                 NUMBER(19,4)
)
tablespace DATA_DCS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128
    next 1
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table DTMREPAIRCONTRACT
  add constraint PK_DTMREPAIRCONTRACT primary key (DTMOBJID)
  using index 
  tablespace INDEX_DCS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
