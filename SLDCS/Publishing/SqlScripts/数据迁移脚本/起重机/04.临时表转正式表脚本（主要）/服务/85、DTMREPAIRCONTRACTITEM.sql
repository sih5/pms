-- Create table
create table DTMREPAIRCONTRACTITEM
(
  DTMOBJID                   VARCHAR2(40) not null,
  DTMREPAIRCONTRACTITEMOBJID VARCHAR2(40) not null,
  REPAIRITEMCODE             VARCHAR2(50),
  REPAIRITEMNAME             VARCHAR2(200),
  ACCOUNTINGPROPERTY         NUMBER(9),
  HOURS                      NUMBER(15,6),
  BWORKHOUR                  NUMBER(9,2),
  PRICE                      NUMBER(19,4),
  WORKINGFEE                 NUMBER(19,4),
  WORKINGFEEDISCOUNT         NUMBER(19,4),
  REPAIRMAN                  VARCHAR2(100),
  REPAIRREASON               VARCHAR2(200),
  CLAIMSTATUS                NUMBER(9),
  REMARK                     VARCHAR2(200)
)
tablespace  DATA_DCS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table DTMREPAIRCONTRACTITEM
  add constraint PK_DTMREPAIRCONTRACTITEM primary key (DTMREPAIRCONTRACTITEMOBJID)
  using index 
  tablespace INDEX_DCS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index FK_REPCON_REPCONITEM_FK on DTMREPAIRCONTRACTITEM (DTMOBJID)
  tablespace INDEX_DCS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
