-- Create table
create table DTMREPAIRCONTRACTMATERIAL
(
  DTMREPAIRCONTRACTMATERIALOBJID VARCHAR2(40) not null,
  DTMREPAIRCONTRACTITEMOBJID     VARCHAR2(40) not null,
  DTMREPAIRCONTRACTOBJID         VARCHAR2(40) not null,
  SPAREPARTCODE                  VARCHAR2(50),
  SPAREPARTNAME                  VARCHAR2(100),
  ACCOUNTINGPROPERTY             NUMBER(9),
  MEASUREUNITNAME                VARCHAR2(100),
  AMOUNT                         NUMBER(9),
  PRICE                          NUMBER(19,4),
  GUIDEPRICE                     NUMBER(19,4),
  MATERIALFEE                    NUMBER(19,4),
  MATERIALFEEDISCOUNT            NUMBER(19,4),
  CLAIMSTATUS                    NUMBER(9),
  REMARK                         VARCHAR2(200),
  OLDSPAREPARTCODE               VARCHAR2(50),
  OLDSPAREPARTNAME               VARCHAR2(100),
  OLDSPAREPARTSUPPLIERCODE       VARCHAR2(50),
  OLDSPAREPARTSUPPLIERNAME       VARCHAR2(100),
  USEDPARTSBARCODE               VARCHAR2(50)
)
tablespace  DATA_DCS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 192
    next 1
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table DTMREPAIRCONTRACTMATERIAL
  add constraint PK_DTMREPAIRCONTRACTMATERIAL primary key (DTMREPAIRCONTRACTMATERIALOBJID)
  using index 
  tablespace INDEX_DCS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index FKT_REPCITEM_REPCMATERIAL_FK on DTMREPAIRCONTRACTMATERIAL (DTMREPAIRCONTRACTITEMOBJID)
  tablespace INDEX_DCS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
