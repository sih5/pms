-- Create table
create table DTMREPAIRCONTRACTORDERLIST
(
  ID                     NUMBER(9) not null,
  DTMREPAIRCONTRACTOBJID VARCHAR2(40) not null,
  DCSREPAIRORDERID       NUMBER(9) not null,
  CREATORID              NUMBER(9),
  CREATORNAME            VARCHAR2(100),
  CREATETIME             DATE,
  WARRANTYSTATUS         NUMBER(9)
)
tablespace DATA_DCS 
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table DTMREPAIRCONTRACTORDERLIST
  add constraint PK_DTMREPAIRCONTRACTORDERLIST primary key (ID)
  using index 
  tablespace INDEX_DCS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DTMREPAIRCONTRACTORDERLIST
  add constraint FK_DTMREPAI_FK_REPCON_DTMREPAI foreign key (DTMREPAIRCONTRACTOBJID)
  references DTMREPAIRCONTRACT (DTMOBJID);
-- Create/Recreate indexes 
create index FK_REPCON_REPCONORDERLIST_FK on DTMREPAIRCONTRACTORDERLIST (DTMREPAIRCONTRACTOBJID)
  tablespace INDEX_DCS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
