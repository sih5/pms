

--结转有效的临时数据
select * into tmpdata.RetainedCustomerForCode from 
(select distinct r.* from tmpdata.customerqzj r 
inner join  vehicleinformation v on v.VIN=r.VIN
where 姓名 is not null) t




--插入正式表(基础客户)


declare
            @Name varchar(100),@Gender int,@CustomerType int,@RegionId int,@RegionName  varchar(100)
           ,@ProvinceName varchar(100),@CityName  varchar(100),@Fax varchar(100),
            @IfComplain  bit ,@CountyName varchar(100),
            @Address varchar(100),@PostCode varchar(100),@CellPhoneNumber varchar(100),
            @HomePhoneNumber varchar(100),@OfficePhoneNumber varchar(100),
            @Birthdate varchar(100),@Email varchar(100),@Status int,
            @CreatorId int ,@CreatorName varchar(100) ,@CreateTime datetime, @Remark varchar(100),
begin
--创建游标
DECLARE Customer_Code CURSOR FAST_FORWARD FOR

    select distinct 姓名,
    case 性别 when '男' then 1 when '女' then 2 else 3 end ,
    case 客户类型 when '企业' then 2  else 1 end ,
    '','',省,市,'',0,县,详细地址,邮政编码,手机号码,家庭电话,'',
    出生日期,电子邮件,1,0,'admin',getdate(),'起重机期初数据导入'
    from tmpdata.RetainedCustomerForCode 
    
-- 打开游标.
   OPEN Customer_Code;   
    
    FETCH NEXT FROM Customer_Code INTO  
            @Name,@Gender, @CustomerType, @RegionId, @RegionName ,
            @ProvinceName ,@CityName  ,@Fax ,
            @IfComplain  ,@CountyName ,
            @Address,@PostCode ,@CellPhoneNumber ,
            @HomePhoneNumber ,@OfficePhoneNumber ,
            @Birthdate ,@Email,@Status ,
            @CreatorId ,@CreatorName  ,@CreateTime, @Remark;
    WHILE @@fetch_status = 0
     begin
      INSERT INTO [DCS].[dbo].[Customer]
           ([Code],[Name],[Gender],[CustomerType],[RegionId],[RegionName],
            [ProvinceName],[CityName] ,[Fax],[IfComplain],[CountyName],
            [Address],[PostCode],[CellPhoneNumber],[HomePhoneNumber],[OfficePhoneNumber],
            [Birthdate],[Email],[Status],[CreatorId] ,[CreatorName] ,[CreateTime],[Remark])
       select dbo.CreateCustomerCode(),
             @Name,@Gender, @CustomerType, @RegionId, @RegionName ,
            @ProvinceName ,@CityName  ,@Fax ,
            @IfComplain  ,@CountyName ,
            @Address,@PostCode ,@CellPhoneNumber ,
            @HomePhoneNumber ,@OfficePhoneNumber ,
            @Birthdate ,@Email,@Status ,
            @CreatorId ,@CreatorName  ,@CreateTime , @Remark;
        exec dcs.dbo.UpdateCustomerCode 
        
        
        FETCH NEXT FROM Customer_Code INTO  
            @Name,@Gender, @CustomerType, @RegionId, @RegionName ,
            @ProvinceName ,@CityName  ,@Fax ,
            @IfComplain  ,@CountyName ,
            @Address,@PostCode ,@CellPhoneNumber ,
            @HomePhoneNumber ,@OfficePhoneNumber ,
            @Birthdate ,@Email,@Status ,
            @CreatorId ,@CreatorName  ,@CreateTime , @Remark;   
            
      end
      -- 关闭游标
   CLOSE Customer_Code;
   --释放游标.
   DEALLOCATE Customer_Code;
end






    
    
    
    



