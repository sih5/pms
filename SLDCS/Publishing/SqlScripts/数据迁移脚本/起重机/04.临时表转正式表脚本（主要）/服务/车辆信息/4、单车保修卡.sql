insert into Vehiclewarrantycard
  (ID,
   CODE,
   VEHICLEID,
   VIN,
   VEHICLECATEGORYID,
   VEHICLECATEGORYNAME,
   SERVICEPRODUCTLINEID,
   SERVICEPRODUCTLINENAME,
   PRODUCTID,
   PRODUCTCODE,
   SALESDATE,
   WARRANTYSTARTDATE,
   RETAINEDCUSTOMERID,
   CUSTOMERNAME,
   CUSTOMERGENDER,
   IDDOCUMENTTYPE,
   IDDOCUMENTNUMBER,
   WARRANTYPOLICYCATEGORY,
   WARRANTYPOLICYID,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME)
  select --s_VehicleWarrantyCard.Nextval,
         'VWC20141104' || decode(length(rownum),
                                 1,
                                 '20000' || rownum,
                                 2,
                                 '2000' || rownum,
                                 3,
                                 '200' || rownum,
                                 4,
                                 '20' || rownum,
                                 5,
                                 '2' || rownum,rownum+200000) CODE,
         t.id,
         t.vin,
         1 VEHICLECATEGORYID,
         2 VEHICLECATEGORYNAME,
         (select a.Serviceproductlineid from ServiceProdLineProduct a
             inner join product b on b.brandid = a.brandid where
             b.id = t.productid)SERVICEPRODUCTLINEID,
         (select a.Serviceproductlinename from ServiceProdLineProduct a
             inner join product b on b.brandid = a.brandid where
             b.id = t.productid） SERVICEPRODUCTLINENAME,
         (select id from product a where a.id = t.productid) PRODUCTID,
         (select a.code from product a where a.id = t.productid) PRODUCTCODE,
         t.salesdate,
         nvl(t.salesdate,to_date('1899-1-1','yyyy-mm-dd')) WARRANTYSTARTDATE,
       a.id,
        b.name,
         b.gender,
           b.iddocumenttype,
 b.iddocumentnumber,
      nvl((select distinct d.Category
            from WarrantyPolicy d
           inner join WarrantyPolicyNServProdLine a
              on a. WarrantyPolicyId = d.id
           inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1          
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate and rownum=1),0) WARRANTYPOLICYCATEGORY,
       nvl((select a.WarrantyPolicyId
            from WarrantyPolicyNServProdLine a
          inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1    
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate and rownum=1),0) WARRANTYPOLICYID,
         1 STATUS,
         1 CREATORID,
         'Admin' CREATORNAME,
         sysdate CREATETIME
    from    vehicleinformation t 
    inner join retainedcustomervehiclelist v on t.id=v.vehicleid
 inner join retainedcustomer a on a.id=v.retainedcustomerid
 inner join customer b on b.id=a.customerid where t.remark='起重机期初数据导入' and t.salesdate>=to_date('2015-01-01','yyyy-mm-dd')
 
and exists(select distinct d.Category
            from WarrantyPolicy d
           inner join WarrantyPolicyNServProdLine a
              on a. WarrantyPolicyId = d.id
           inner join ServProdLineAffiProduct b
                on b.Serviceproductlineid = a.ServiceProductLineId and b.ProductLinetype=1          
           where b.ProductId = t.productid and t.Salesdate>=a.ValidationDate and t.salesdate<=a.ExpireDate and rownum=1)
         