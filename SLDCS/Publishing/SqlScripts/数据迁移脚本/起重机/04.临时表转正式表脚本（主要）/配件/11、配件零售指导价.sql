--配件零售指导价
insert into PartsRetailGuidePrice
  (id,
   branchid,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   sparepartid,
   sparepartcode,
   sparepartname,
   retailguideprice,
   creatorid,
   creatorname,
   createtime,status)
  select s_partsretailguideprice.Nextval,
         (select id from branch where code='2470') as branchid,
        m.id ,m.code,m.name,
         v.id,
         v.code,
         v.name,
         nvl(t.RetailGuidePrice, 0),
         1,
         'Admin',
         sysdate,1
    from tmpdata.PartsRetailGuidePricetmpqzj t 
   inner join sparepart v
      on t.SpareCode = v.code
   inner join partssalescategory m on m.name=t.brandname;
  
--配件零售指导价变更履历
  insert into PartsRetailGuidePriceHistory
  (id,
   branchid,
   partssalescategoryid,
   partssalescategorycode,
   partssalescategoryname,
   sparepartid,
   sparepartcode,
   sparepartname,
   retailguideprice,
  -- validationtime,
  -- expiretime,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_PartsRetailGuidePriceHistory.Nextval,
         (select id from branch where code='2470') as branchid,
         m.id as partssalescategoryid,
         m.code as partssalescategorycode,
         m.name as partssalescategoryname,
         v.id as sparepartid,
         v.code as sparepartcode,
         v.name as sparepartname,
         t.RetailGuidePrice,
        -- validationtime,
        -- expiretime,
         1,
         1,
         'Admin',
         sysdate
    from tmpdata.PartsRetailGuidePricetmpqzj t 
   inner join sparepart v
      on t.SpareCode = v.code
   inner join partssalescategory m on m.name=t.brandname;