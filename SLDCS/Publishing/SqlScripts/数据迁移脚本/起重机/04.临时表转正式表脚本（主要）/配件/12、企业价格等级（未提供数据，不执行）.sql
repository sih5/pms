--配件销售订单类型
insert into partssalesordertype
  (id,
   branchid,
   code,
   name,
   partssalescategoryid,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_partssalesordertype.nextval,
         (select id from branch where code='2470'),
         t.salesordertypecode,
         t.salesordertypename,
         (select id from partssalescategory where name = t.brandname),
         1,
         1 ,'Admin',
         sysdate
    from (select distinct t.brandname,
                          t.salesordertypename,
                          t.salesordertypecode
            from tmpdata.CustomerOrderPriceGradetmpqzj t) t;
            
            
    

--客户企业订单价格等级
insert into CustomerOrderPriceGrade
  (id,
   branchid,
   partssalescategoryid,
   customercompanyid,
   customercompanycode,
   customercompanyname,
   partssalesordertypeid,
   partssalesordertypecode,
   partssalesordertypename,
   coefficient,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_CustomerOrderPriceGrade.Nextval,
         (select id from branch where code='2470'),
         v.partssalescategoryid,
         t.id,
         t.code,
         t.name,
         v.id,
         v.code,
         v.name,
         1,
         1,
         1,
         'Admin',
         sysdate
    from dealer t inner join tmpdata.CustomerOrderPriceGradetmpqzj b on t.code =b.customercode
    inner join partssalesordertype v on b.salesordertypecode=v.code and v.branchid in (select id from branch where code='2470');

    
