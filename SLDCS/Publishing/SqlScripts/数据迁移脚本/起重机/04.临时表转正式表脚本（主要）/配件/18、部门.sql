

insert into departmentinformation
  (id,
   branchid,
   branchname,
   code,
   name,
   status,
   creatorid,
   creatorname,
   createtime)
  select s_DepartmentInformation.Nextval,
         (select id from branch where code='2470'),
         (select name from branch where code='2470'),
         t.code,
         t.name,
         1,
         1,
         'Admin',
         sysdate
    from tmpdata.departmentinformationtmpQZJ t;
