--企业
insert into  company
  (ID,
   TYPE,
   CODE,
   NAME,
   REGIONID,
   PROVINCENAME,
   CITYNAME,
   COUNTYNAME,
   CONTACTADDRESS,
   CONTACTPOSTCODE,
   CONTACTMAIL,
   ContactPerson,
   ContactPhone,
   RegisteredAddress,
   BusinessScope,
   RegisterDate,
   ShortName,
   CityLevel,
   LegalRepresentative,
   LegalRepresentTel,
   RegisterCapital,
   BusinessAddress,
   STATUS,
   CREATORID,
   CREATORNAME,
   CREATETIME,CustomerCode,SupplierCode,remark)
  select S_Company.Nextval,
         4,
         "企业编号",
         "企业名称",
         (select Id
            from TiledRegion
           where provincename = "省"
             and cityname = "市"
             and countyname = "县"),
         "省",
         "市",
         "县",
         "联系地址",
         "邮编",
         "电子邮件",
         t.联系人,
         t.联系电话,
         t.税务登记地址,
         t.经营范围,
         t.注册时间,
         t.简称,
         decode(t.城市级别, '省级', 1, '地级', 2, '县级', 3),
         t.法定代表人,
         t.法人代表电话,
         0,
         t.营业地址, 1,
         1,
         'Admin',
         sysdate,MDM客户编码,MDM供应商编码,'起重机初始数据处理'
  from tmpdata.LogisticCompanytmpQZJ t
  where t.企业编号 not in (select code from company);
  
  
 

--物流公司
insert into LogisticCompany
( ID,
  CODE,
  NAME,
  CREATETIME,
  CREATORID,
  CREATORNAME,
  status
)
  select  b.id,
         b.code,
         b.name,
         sysdate,1,
         'Admin',1
    from tmpdata.LogisticCompanytmpQZJ t inner join company b on t.企业编号=b.code
    where b.remark='起重机初始数据处理';
    
--企业开票信息
insert into companyinvoiceinfo
  (id,
   Invoicecompanyid,
   Companyid,
   Companycode,
   Companyname,
   Isvehiclesalesinvoice,
   Ispartssalesinvoice,
   TaxRegisteredNumber,
   InvoiceTitle,
   InvoiceType,
   TaxRegisteredAddress,
   TaxRegisteredPhone,
   BankName,
   BankAccount,
   status)
  select s_CompanyInvoiceInfo.Nextval,
         (select id from branch where code = '2470'),
         (select id
            from company
           where code ='2470'and type=1),
         t.企业编号,
         t.企业名称,
         0,
         1,
         t.税务登记号,
         t.开票名称,
         decode(t.开票类型,'增值税发票',1,2),
         t.税务登记地址,
         t.税务登记电话,
         t.开户行名称,
         t.开户行账号,
         1
    from tmpdata.LogisticCompanytmpQZJ t inner join company b on t.企业编号=b.code
        where b.remark='起重机初始数据处理';
    
    