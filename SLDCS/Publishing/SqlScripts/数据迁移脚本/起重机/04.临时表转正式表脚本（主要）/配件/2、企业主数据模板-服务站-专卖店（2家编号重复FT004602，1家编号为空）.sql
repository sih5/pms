
 

insert into company
  (ID,
   TYPE,
   CODE,
   NAME,
   CustomerCode,
   SupplierCode,
   REGIONID,
   PROVINCENAME,
   CITYNAME,
   COUNTYNAME,
   CityLevel,
   FoundDate,
   ContactPerson,
   ContactPhone,
   ContactMobile,
   Fax,
   CONTACTADDRESS,
   CONTACTPOSTCODE,
   CONTACTMAIL,
   RegisterCode,
   RegisterName,
   CorporateNature,
   LegalRepresentative,
   LegalRepresentTel,
   IdDocumentType,
   IdDocumentNumber,
   RegisterCapital,
   RegisterDate,
   BusinessScope,
   BusinessAddress,
   RegisteredAddress,
   STATUS,
   CREATORNAME,
   CREATETIME)
  select s_company.nextval,
         2,
         企业编号,
         substr(企业名称, 1, 25),
         MDM客户编码,
         MDM供应商编码,
         (select Id
            from TiledRegion
           where provincename = 省
             and cityname = 市
             and countyname = 县),
         省,
         市,
         县,
         decode(城市级别, '省级', 1, '地级', 2, '县级', 3),
         to_date(成立日期, 'yyyy-mm-dd'),
         联系人,
         固定电话,
         联系人手机,
         传真,
         联系地址,
         邮政编码,
         substrb(电子邮箱, 1, 50),
         '',
         '',
         '', ---企业性质先不导
         法定代表人,
         法人代表电话,
         '',
         '',
         注册资本,
         to_date(注册日期,'yyyy-mm-dd'),
         经营范围,
         营业地址,
         税务登记地址,
         1,
         'Admin',
         sysdate
    from tmpdata.dealertmpqzj where 县 is not null and 企业编号 is not null 
    and 企业编号  <>'FT004602';
    
    


    --企业
insert into company
  (ID,
   TYPE,
   CODE,
   NAME,
   CustomerCode,
   SupplierCode,
   REGIONID,
   PROVINCENAME,
   CITYNAME,
   COUNTYNAME,
   CityLevel,
   FoundDate,
   ContactPerson,
   ContactPhone,
   ContactMobile,
   Fax,
   CONTACTADDRESS,
   CONTACTPOSTCODE,
   CONTACTMAIL,
   RegisterCode,
   RegisterName,
   CorporateNature,
   LegalRepresentative,
   LegalRepresentTel,
   IdDocumentType,
   IdDocumentNumber,
   RegisterCapital,
   RegisterDate,
   BusinessScope,
   BusinessAddress,
   RegisteredAddress,
   STATUS,
   CREATORNAME,
   CREATETIME)
  select s_company.nextval,
         2,
         企业编号,
         substr(企业名称, 1, 25),
         MDM客户编码,
         MDM供应商编码,
         (select Id
            from TiledRegion
           where provincename = 省
             and cityname = 市 and countyname is null),
         省,
         市,
         县,
         decode(城市级别, '省级', 1, '地级', 2, '县级', 3),
         to_date(成立日期, 'yyyy-mm-dd'),
         联系人,
         固定电话,
         联系人手机,
         传真,
         联系地址,
         邮政编码,
         substrb(电子邮箱, 1, 50),
         '',
         '',
         '', ---企业性质先不导
         法定代表人,
         法人代表电话,
         '',
         '',
         注册资本,
         to_date(注册日期,'yyyy-mm-dd'),
         经营范围,
         营业地址,
         税务登记地址,
         1,
         'Admin',
         sysdate
    from tmpdata.dealertmpqzj where 县 is  null  and 企业编号 is not null
        and 企业编号  <>'FT004602';
    

    



--企业开票信息
insert into CompanyInvoiceInfo
  (id,Invoicecompanyid,Companyid,Companycode,Companyname,Isvehiclesalesinvoice,Ispartssalesinvoice,TaxRegisteredNumber,InvoiceTitle,
InvoiceType,TaxRegisteredAddress, BankName, BankAccount,status)
select s_CompanyInvoiceInfo.Nextval,
(select id from branch where code = '2470'),b.id,b.code,b.name,1,1,税务登记号,开票名称,
decode(nvl(a.开票类型,'空'),'增值税发票',1,'空',4,2),税务登记地址,银行名称,银行账号,1
from tmpdata.dealertmpqzj a 
inner join company b on a.企业编号 = b.code
where  a.企业编号  <>'FT004602';




--经销商基本信息
insert into dealer
(ID,CODE,NAME,ISVIRTUALDEALER,CREATETIME,CREATORNAME,status
)
select b.id,b.code,b.name,0,sysdate,'Admin',1
from tmpdata.dealertmpqzj a 
inner join company b on a.企业编号 = b.code
where     a.企业编号 <>'FT004602';


--经销商服务扩展信息
insert into DealerServiceExt
  (Id,
   RepairQualification,
   HasBranch,
   DangerousRepairQualification,
   BrandScope,
   CompetitiveBrandScope,
   ParkingArea,
   RepairingArea,
   EmployeeNumber,
   PartWarehouseArea,
   GeographicPosition,
   OwnerCompany,
   MainBusinessAreas,
   AndBusinessAreas,
   BuildTime,
   TrafficRestrictionsdescribe,
   ManagerPhoneNumber,
   ManagerMobile,
   ManagerMail,
   ReceptionRoomArea,
   Status,
   CreateTime,
   CreatorName)
  select b.id,
         decode(维修资质等级, '一类', '1', '二类', 2, '三类', 3, 4),
         decode(有二级服务站, '有', 1, 2),
         decode(危险品运输车辆维修资质, '有', 1, 2),
         substrb(福田授权品牌,1,50),
         substr(非福田授权品牌, 0, 25),
         停车场面积,
         维修车面积,
         decode(服务站人数, '10人以下', 10, '30-50人', 50, '50人以上', 51),
         配件库面积,
         null,
         隶属单位,
         主营范围,
         兼营范围,
         to_date(建厂时间,'yyyy-mm-dd'),
         交通限行描述,
         null,
         null,
         null,
         接待室面积,
         1,
         sysdate,
         'Admin'
    from tmpdata.dealertmpqzj a
   inner join dealer b
      on a.企业编号 = b.code
      where     a.企业编号 <>'FT004602';

