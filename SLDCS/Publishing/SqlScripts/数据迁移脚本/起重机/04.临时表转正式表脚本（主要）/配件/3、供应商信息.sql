--企业
insert into company
  (ID,
   TYPE,
   CODE,
   NAME,
  -- CustomerCode,
  -- SupplierCode,
   REGIONID,
   PROVINCENAME,
   CITYNAME,
   COUNTYNAME,
  -- CityLevel,
   --FoundDate,
   ContactPerson,
   ContactPhone,
   --ContactMobile,
   Fax,
   --CONTACTADDRESS,
   CONTACTPOSTCODE,
   CONTACTMAIL,
   RegisterCode,
   RegisterName,
   CorporateNature,
   --LegalRepresentative,
   --LegalRepresentTel,
   IdDocumentType,
   IdDocumentNumber,
   --RegisterCapital,
   RegisterDate,
   BusinessScope,
   BusinessAddress,
   --RegisteredAddress,
   Remark,
   STATUS,
   CREATORNAME,
   CREATETIME)
  select s_company.nextval,
         6,
         业务编码,--供应商编号,
         substr(业务名称, 1, 25), -- 供应商名称
        -- MDM客户编码,
        -- MDM供应商编码,
         (select Id
            from TiledRegion
           where provincename = 省
             and cityname = 市
             and countyname = 县),
         省,
         市,
         县,
         --decode(城市级别, '省级', 1, '地级', 2, '县级', 3),
       --  to_date(成立日期, 'yyyy-mm-dd'),
         联系人,
         t.联系电话,
         --电子邮件,
         t.传真,
         --联系地址,
         t.邮编,
         t.电子邮件,
         '',
         '',
         '', ---企业性质先不导
        -- 法定代表人,
         --法人代表电话,
         '',
         '',
         --t.,
         case 注册日期
           when null then
            to_date('1885-1-1', 'yyyy-mm-dd')
           when '' then
            to_date('1885-1-1', 'yyyy-mm-dd')
           else
            to_date(注册日期, 'yyyy-mm-dd')
         end as RegisterDate,
         t.经营范围,
         t.联系地址,
         --税务登记地址,
         '起重机初始数据导入',
         1,
         'Admin',
         sysdate
    from tmpdata.PartsSuppliertmpqzj t
   where 业务编码 not in (select code from company);
  
   
   
  --配件供应商基本信息
insert into PartsSupplier
(Id,Code,Name,ShortName,SupplierType,IsCanClaim,Status,CreatorId,CreatorName,CreateTime)
select b.id,b.code,b.name,'',1,1,1,1,'Admin',sysdate
from  tmpdata.PartsSuppliertmpqzj t inner join company b on t.业务编码=b.code
where b.remark='起重机初始数据导入';
   
   
--分公司与供应商关系
insert into BranchSupplierRelation
  (Id,
   BranchId,
   PartsSalesCategoryId,
   SupplierId,
   BusinessCode,
   BusinessName,
   PurchasingCycle,
   PartsManagementCostGradeId,
   PartsClaimCoefficient,
   ClaimPriceCategory,
   LaborUnitPrice,
   IfSPByLabor,
   LaborCoefficient,
   IfHaveOutFee,
   OldPartTransCoefficient,
   OutServiceCarUnitPrice,
   OutSubsidyPrice,
   Status,
   CreatorName,
   CreateTime
   )
  select s_BranchSupplierRelation.Nextval,
        (select id from branch where code ='2470') as BranchId,
         b.id,
         c.id,
         a.供应商编号,
         a.供应商名称,
         null,
         d.id,
         null,1,
         null,
         DECODE(A.是否按工时单价索赔,'是',1,'否',0,0),
         工时索赔系数,
         decode(a.是否存在外出费用,'是',1,'否',0,0),
         round(旧件运费系数, 2),
         a.外出里程单价,
         a.外出人员单价,
         1,       
         'Admin',
         sysdate
    from tmpdata.PartsSuppliertmpqzj a 
   left join partssalescategory b on 1=1 
   left join company c
      on c.code = a.业务编码
    LEFT JOIN partsmanagementcostgrade d
      on d.NAME like '%供应商管理费等级%' AND D.PARTSSALESCATEGORYID=B.ID
      where c.remark='起重机初始数据导入' and b.code='QZJ';
      
      
      
   
insert into supplieraccount
( id,BuyerCompanyId,PartsSalesCategoryId,PartsSalesCategoryName,SupplierCompanyId,DueAmount,EstimatedDueAmount,PaymentLimit,Status,CreatorId,CreatorName,CreateTime
)
select s_supplieraccount.nextval,(select id from branch where code ='2470'),c.id,c.name,b.id,0,0,0,1,null,'Admin',sysdate
from tmpdata.PartsSuppliertmpqzj a 
inner join partssupplier b on a.业务编码 = b.code
inner join partssalescategory c on c.name = a.品牌
inner join company d on a.供应商编号=d.code
      where d.remark='起重机初始数据导入';


