--配件采购价
insert into PartsPurchasePricing
  (id,
   branchid,
   partid,
   partssupplierid,
   partssalescategoryid,
   partssalescategoryname,
   validfrom,
   validto,
   pricetype,
   purchaseprice,
   status)
  select s_PartsPurchasePricing.Nextval,
         (select id from branch where code='2470') as branchid,
         v.id,
         f.id,
         m.id ,
         m.name,
         t.ValidFrom,
         t.validto,
         case when pricetype ='临时价格' then 2 else 1 end  ,
         nvl(t.PurchasePrice, 0),
         2
    from tmpdata.PartsPurchasePricingtmpqzj t 
   inner join sparepart v
      on t.SpareCode = v.code
    inner join BranchSupplierRelation c on t.SupplierCode=c.businesscode
    inner join company f on f.id =c.supplierid
   left join partssalescategory m on m.name=t.brandname;
   
   
   
   
   --配件采购价格变更申请单
   insert into PartsPurchasePricingChange
  (id,
   code,
   branchid,
   partssalescategoryid,
   partssalescategoryname,
   creatorid,
   creatorname,
   createtime,status)
   select s_PartsPurchasePricingChange.Nextval,'PPPC201412310001',(select id from branch where code='2470') as branchid,
   b.id as partssalescategoryid ,b.name as partssalescategoryname, 1 as creatorid, 'Admin'as creatorname, sysdate as createtime,2 as status
   from (select distinct brandname from  tmpdata.PartsPurchasePricingtmpqzj) a
   inner join partssalescategory b on b.name=a.brandname;
   
 --配件采购价格变更申请清单    
insert into PartsPurchasePricingDetail
  (id,
   parentid,
   partid,
   partcode,
   partname,
   supplierid,
   suppliercode,
   suppliername,
   pricetype,
   price,VALIDFROM,validto)
  select s_PartsPurchasePricingDetail.Nextval,
         d.id,
         b.id,
         b.code,
         b.name,
         f.id,
         f.code,
         f.name,
         case when a.pricetype ='临时价格' then 2 else 1 end  ,
         nvl(a.PurchasePrice, 0), a.VALIDFROM,a.validto
    from tmpdata.PartsPurchasePricingtmpqzj a inner join sparepart b on a.SpareCode=b.code
    inner join BranchSupplierRelation c on a.SupplierCode=c.businesscode
    inner join company f on f.id =c.supplierid
    inner join PartsPurchasePricingChange d on 1=1 and d.code='PPPC201412310001';
    
    

    
    
    
    
   
   
      