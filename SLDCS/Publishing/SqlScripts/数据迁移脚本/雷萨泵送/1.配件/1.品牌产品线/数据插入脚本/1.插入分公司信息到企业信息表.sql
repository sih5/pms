--把分公司信息插入到企业信息表 company
insert into dcs.Company
  (Id,
   Type,
   Code,
   Name,
   ProvinceName,
   CityName,
   CountyName,
   
   ContactPerson,
   ContactPhone,
   --ContactMobile,
   Fax,
   ContactAddress,
   ContactPostCode,
   ContactMail,
   --CorporateNature, --企业性质
   RegisterDate,
   BusinessScope,
   RegisteredAddress,
   Status,
   CreatorName,
   CreateTime)
values
  (
   dcs.s_company.nextval,
   1, --企业类型 - 分公司
   'SHSB',
   '商混设备销售分公司',
   '北京市',
   '北京市',
   '',
   
   '蒋浩',
   '1',
   '',
   '北京市怀柔区大中富乐村北红螺东路21号',
   '',
   '',
   to_date('2009-02-19 15:18:36','yyyy-mm-dd hh24:mi:ss'),
   '',
   '北京市怀柔区大中富乐村北红螺东路21号',
   1,
   'admin',
   sysdate
   );



---------------

/*insert into dcs.Company
  (Id,
   Type,
   Code,
   Name,
   ProvinceName,
   CityName,
   CountyName,
   
   ContactPerson,
   ContactPhone,
   --ContactMobile,
   Fax,
   ContactAddress,
   ContactPostCode,
   ContactMail,
   --CorporateNature, --企业性质
   RegisterDate,
   BusinessScope,
   RegisteredAddress,
   Status,
   CreatorName,
   CreateTime)
select
    dcs.s_company.nextval,
    1, ---企业类型-分公司
    FilialeCode,FilialeName,Province,city,county,Linker,Tel,Fax,Address,zip,E_Mail,estabishdate,business,QYaddress,1,'admin',sysdate
   
  from tFilialesTemp;
*/
