--插入分公司   （商混设备）

insert into dcs.Branch
  (Id,
   Code,
   Name,
   MainBrand, --主营品牌  
   Status,
   CreatorName,
   CreateTime)
values
  ((select id
            from dcs.company
           where company.code ='SHSB'), 'SHSB', '商混设备销售分公司', '商混设备/欧曼', 1, 'admin', sysdate);




/*--批量插入分公司
insert into dcs.Branch
  (Id, Code, Name, MainBrand, Status, CreatorName, CreateTime)
  select dcs.s_company.nextval,
         FilialeCode,
         FilialeName,
         BrandName,
         1,
         'admin',
         sysdate
    from tFilialesTemp;
*/
