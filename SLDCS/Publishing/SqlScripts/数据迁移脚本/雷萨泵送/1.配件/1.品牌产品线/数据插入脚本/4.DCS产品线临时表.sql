create table TPRODUCTLINES_TEMP_OM
(
  FilialeCode          VARCHAR2(40) ,
  FilialeName          VARCHAR2(50) ,  
  BrandName            VARCHAR2(40) ,
  ProductLineCode      VARCHAR2(40) ,
  ProductLineName      VARCHAR2(40)   
)
