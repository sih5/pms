Insert Into ServiceProductLine
  (Id,
   BranchId,
   PartsSalesCategoryId,
   Code,
   Name,
   Status,
   CreatorName,
   CreateTime)
  select dcs.s_Serviceproductline.nextval as Id,
         (select id from Branch where Branch.code = temp.FilialeCode) as BranchId,
         (select id
            from PartsSalesCategory
           where PartsSalesCategory.name = temp.BrandName) as PartsSalesCategoryId,
         ProductLineCode as code,
         ProductLinename as name,
         1 as Status,
         'admin',
         sysdate
    from TPRODUCTLINES_TEMP_OM temp;
