select code,
       name,
       '' 主营品牌,
       province,
       city,
       '' county, --县
       zip,
       E_Mail,
       Fax,
       Linker,
       Tel,
       Address,
       (select ADDRESS
          from ft."tEnterprise"@fpurview t2
         where t1.objid = t2.objid) QYaddress,
       (select establishdate
          from ft."tEnterprise"@fpurview t2
         where t1.objid = t2.objid) establishdate,
       (select BUSINESS
          from ft."tEnterprise"@fpurview t2
         where t1.objid = t2.objid) BUSINESS
  from ft.tfiliales t1
 where isvalid > 0
   and t1.brandid =
       (select objid from ft.tbrands where tbrands.name = '商混设备');
