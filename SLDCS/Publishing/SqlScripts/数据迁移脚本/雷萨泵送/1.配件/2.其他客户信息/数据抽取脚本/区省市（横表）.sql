select t0.code t0code,-- 行政区域编码 ,
       t0.name t0name , -- 行政区域名称,
       t1.code t1code , -- 大区编码,
       t1.name t1name , -- 大区名称,
       t2.code t2code , -- 省编码,
       t2.name t2name , -- 省名称,
       t3.code t3code , -- 市编码,
       t3.name t3name , -- 市名称,
       t4.code t4code , -- 县编码,
       t4.name t4name   -- 县名称
  from dcs.region t0
  left join (select id, parentid, type, code, name
               from dcs.region t
              where t.type = 1) t1
    on t0.id = t1.parentid
  left join (select id, parentid, type, code, name
               from dcs.region t
              where t.type = 2) t2
    on t1.id = t2.parentid
  left join (select id, parentid, type, code, name
               from dcs.region t
              where t.type = 3) t3
    on t2.id = t3.parentid
  left join (select id, parentid, type, code, name
               from dcs.region t
              where t.type = 4) t4
    on t3.id = t4.parentid
 where t0.type = 0
