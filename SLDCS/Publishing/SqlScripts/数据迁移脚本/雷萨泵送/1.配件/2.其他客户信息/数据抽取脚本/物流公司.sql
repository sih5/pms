select code,
       name,
       province,
       city,
       '' county, --县
       zip,
       E_Mail,
       Fax,
       Linker,
       Tel,
       Address,
       (select ADDRESS
          from ft."tEnterprise"@fpurview t2
         where t1.objid = t2.objid) QYaddress,
       (select establishdate
          from ft."tEnterprise"@fpurview t2
         where t1.objid = t2.objid) establishdate,
       (select BUSINESS
          from ft."tEnterprise"@fpurview t2
         where t1.objid = t2.objid) BUSINESS,
       '' 物流公司业务领域,
       '' 隶属分公司编号,
       '' 隶属分公司名称
  from ft.ttrafficcompanys t1
 where isvalid > 0;
