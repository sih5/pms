--创建代理库临时表 tAgentTemp

--临时表  条数据
create table tAgentTemp
(
       AgentCode varchar2(50),
       AgentName varchar2(100),
       Province varchar2(50),
       City varchar2(50),
       County varchar2(50),
       zip varchar2(50),
       E_Mail varchar2(50),
       Fax varchar2(50),
       Linker varchar2(50),
       Tel varchar2(50),
       Address varchar2(200),    --联系地址
       QYaddress varchar2(200),   --注册地址
       establishdate date,       --注册日期
       business varchar2(50),    --经营范围
       FilialeCode varchar2(50),   --隶属分公司编号
       FilialeName varchar2(100)   --隶属分公司名称
);





--创建分公司临时表   数据较少，不用建临时表
/*create table tFilialesTemp
(
       FilialeCode varchar2(50),
       FilialeName varchar2(100),
       BrandName varchar2(50),   --主营品牌
       Province varchar2(50),
       city varchar2(50),
       county varchar2(50),
       zip varchar2(50),
       E_Mail varchar2(50),
       Fax varchar2(50),
       Linker varchar2(50),
       Tel varchar2(50),
       Address varchar2(200),
       QYaddress varchar2(200),  --注册地址
       estabishdate date,       --注册日期
       business  varchar2(50)                 --经营范围
)*/



--创建配件供应商临时表
--临时表  条数据
create table tSupplierTemp
(
       SupplierCode varchar2(50),
       SupplierName varchar2(100),
       IsFormalSup  varchar2(50),     --供应商类型（正式供应商，临时供应商）
       Province varchar2(50),
       city varchar2(50),
       county varchar2(50),
       zip varchar2(50),
       E_Mail varchar2(50),
       Fax varchar2(50),
       Linker varchar2(50),
       tel varchar2(50),
       Address varchar2(200),   --联系地址
       QYaddress varchar2(200),    --注册地址
       Business varchar2(50),    --经营范围
       establishdate date,        --注册日期
       
       FilialeCode varchar2(50),   --隶属分公司编号
       FilialeName varchar2(100),  --隶属分公司名称
       BrandName varchar2(50)      --品牌
       
);



--创建物流公司临时表
-- 临时表  条数据
create table tTraficCompanyTemp
(
       TraficCode varchar2(50),
       TraficName varchar2(100),
       Province varchar2(50),
       City varchar2(50),
       County varchar2(50),
       zip varchar2(50),
       E_Mail varchar2(50),
       Fax varchar2(50),
       Linker varchar2(50),
       Tel varchar2(50),
       Address varchar2(200),
       QYaddress varchar2(200),       -- 注册地址
       establishdate date,      --注册日期
       Business varchar2(50),        --经营范围
       TraficBusiness varchar2(50),   --物流公司业务领域
       FilialeCode varchar2(50),   --隶属分公司编号
       FilialeName varchar2(100)     --隶属分公司名称
       
);

