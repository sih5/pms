--插入代理库到企业信息表
insert into dcs.company
  (Id,
   Type,
   Code,
   Name,
   ProvinceName,
   CityName,
   CountyName,
   ContactPerson,
   ContactPhone,
   --ContactMobile,
   Fax,
   ContactAddress,
   ContactPostCode,
   ContactMail,
   
   RegisterDate,
   BusinessScope,
   RegisteredAddress,
   Status,
   CreatorName,
   CreateTime)
  select
      dcs.s_company.nextval,
       3, --企业类型 - 代理库
       AgentCode,
       AgentName,
       Province,
       City,
       County,
       Linker,
       Tel,
       Fax,
       Address,
       zip,
       E_Mail,
       establishdate,
       business,
       QYaddress,
       1,
       'admin',
       sysdate
        from tAgentTemp;
