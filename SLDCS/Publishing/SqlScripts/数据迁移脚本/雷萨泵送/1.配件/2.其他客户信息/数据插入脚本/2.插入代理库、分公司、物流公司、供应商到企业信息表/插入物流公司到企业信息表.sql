--插入物流公司信息到企业表

insert into dcs.company
  (Id,
   Type,
   Code,
   Name,
   ProvinceName,
   CityName,
   CountyName,
   ContactPerson,
   ContactPhone,
   --ContactMobile,
   Fax,
   ContactAddress,
   ContactPostCode,
   ContactMail,
   
   RegisterDate,
   BusinessScope,
   RegisteredAddress,
   Status,
   CreatorName,
   CreateTime)

  select dcs.s_company.nextval,
         4, --企业类型 - 物流公司
         TraficCode,
         TraficName,
         Province,
         City,
         County,
         Linker,
         Tel,
         Fax,
         Address,
         zip,
         E_Mail,
         establishdate,
         Business,
         QYaddress,
         1,
         'admin',
         sysdate
    from tTraficCompanyTemp;
