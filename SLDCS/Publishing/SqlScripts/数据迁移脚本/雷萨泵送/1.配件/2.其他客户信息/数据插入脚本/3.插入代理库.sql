--插入代理库数据

insert into dcs.Agency
  (Id, Code, Name, Status, CreatorName, CreateTime)
  select (select id
            from dcs.company
           where company.type = 3
             and company.code = tAgentTemp.Agentcode),
         AgentCode,
         AgentName,
         1,
         'Admin',
         sysdate
    from tAgentTemp;
