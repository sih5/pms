insert into dcs.BranchSupplierRelation
  (Id,
   BranchId, --营销分公司Id
   PartsSalesCategoryId, --配件销售类型Id
   SupplierId, --供应商Id
   Status,
   CreatorName,
   CreateTime)
select
   dcs.s_branchsupplierrelation.nextval,
   (select id from dcs.branch where branch.code = tsuppliertemp.filialecode),
   (select id from dcs.Partssalescategory where Partssalescategory.Name = tsuppliertemp.brandname),
   (select id from dcs.Partssupplier where Partssupplier.Code = tsuppliertemp.suppliercode),
   1,
   'admin',
   sysdate
  from dcs.tsuppliertemp
  
  
  
