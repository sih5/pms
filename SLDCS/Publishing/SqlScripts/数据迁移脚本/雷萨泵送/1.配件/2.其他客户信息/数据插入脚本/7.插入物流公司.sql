--插入物流公司

insert into dcs.Logisticcompany
  (Id, Code, Name, Status, CreatorName, CreateTime)
  select (select id
            from dcs.company
           where company.type = 4
             and company.code = tTraficCompanyTemp.Traficcode),
         TraficCode,
         TraficName,
         1,
         'admin',
         sysdate
    from tTraficCompanyTemp;
