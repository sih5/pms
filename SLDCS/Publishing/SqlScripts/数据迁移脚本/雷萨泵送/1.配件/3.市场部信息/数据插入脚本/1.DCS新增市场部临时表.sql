create table tmarkets_temp_om
(
 marketcode varchar2(40),
 marketname varchar2(100),
 filialecode varchar2(40),
 filialename varchar2(100),
 tel varchar2(50),
 address varchar2(200),
 businesstype varchar2(10)
)
