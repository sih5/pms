insert into MarketingDepartment
  (Id,
   BranchId,
   PartsSalesCategoryId,
   BusinessType,
   Code,
   Name,
   Address,
   Phone,
   Status,
   CreatorName,
   CreateTime)
  select dcs.s_Marketingdepartment.nextval as Id,
         (select id from dcs.branch where branch.name = temp.filialename) as BranchId,
         (select id
            from dcs.PartsSalesCategory
           where PartsSalesCategory.Name = '商混设备') as PartsSalesCategoryId,
         decode(temp.businesstype, '销售', 1, '服务', 2,2) as BusinessType,
         temp.marketcode as Code,
         temp.marketname as Name,
         Address,
         tel as Phone,
         1 as Status,
         'admin' as CreatorName,
         sysdate as CreateTime
    from tmarkets_temp_om temp;
