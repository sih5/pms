select 
          --开票企业Id(分公司ID)
 t1.objid,
 t2.Code, --企业编号
 t1.name,   --企业名称
          --是否整车销售开票
          --是否配件销售开票
 t1.taxcode, --税号
 t1.name InvoiceTitle,     --开票名称
 1 InvoiceType,--开票类型(增值税发票)
 1 TaxpayerQualification,--纳税人资格
 t2.inviocequota, --发票限额
 t1.addressphone,--地址电话
 t2.bank, --银行
 t1.bankaccount,--银行账号
 0.17 InvoiceTax,--开票税率
 t2.linker, --财务联系人
 t2.tel,    --财务联系电话
 t2.fax   --财务传真
 from ft.ttax_info t1
left join ft.tcustomers t2 on t2.ObjID = t1.objid
left join ft.tstations@fservice t3 on t2.objid=t3.objid
left join ft.tstationlists@fservice t4 on t3.objid=t4.stationid 
where t4.brandid = (select objid from ft.tbrands where tbrands.name = '商混设备');