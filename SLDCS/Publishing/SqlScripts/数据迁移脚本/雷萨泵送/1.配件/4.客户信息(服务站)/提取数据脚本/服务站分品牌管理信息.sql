select tstations.code,
       tstations.name,
       (select code
          from ft.tfiliales
         where tfiliales.objid = tstationlists.filialeid) as filialecode,
       (select name
          from ft.tfiliales
         where tfiliales.objid = tstationlists.filialeid) as filialename,
       (select name
          from ft.tbrands
         where tbrands.objid = tstationlists.brandid) as brandname,
       (select code
          from ft.tmarkets
         where tmarkets.objid = tstationlists.marketid) as marketcode,
       (select name
          from ft.tmarkets
         where tmarkets.objid = tstationlists.marketid) as marketname,
       '' as ChannelCapabilityId, --业务能力,
       tstationlists.PHONE, --固定电话
       tstationlists.DutyPhone, --24小时热线
       tstationlists.Fax, --传真
       decode(tstations.REPAIRPURVIEW, 0, '一类', 1, '二类') as REPAIRPURVIEW, --维修资质等级
       '' as PartsManagingFeeGradeId, --配件管理费率,
       '' as OutFeeGradeId, --外出服务费等级,
       (select tparams.paramvalue
          from ft.tparams
         where tparams.paramcode = tstationlevels.levels
           and tparams.paramtype = '星级') as GradeCoefficientId,--星级,
        '' OutServiceradii, --	外出服务半径
        '' UsedPartsWarehouseId, --	旧件仓库Id
	--tstationlists.MatFeeType,	--材料费开票类型 营销有 欧曼无
	--tstationlists.WorkFeeType,	--工时费开票类型 营销有 欧曼无
	--tstationlists.MatFeeRate,	--工时费开票系数 营销有 欧曼无
	--tstationlists.WorkFeeRate,	--材料费开票系数 营销有 欧曼无
	tstationlists.Stationtype,      --服务站类别
	tstationlists.Isduty,         --是否职守
	tstationlists.SalingTime,      --授权时间
	tstationlists.CutTime,         --解约时间
	--tstationlists.Area,             --区域         营销有 欧曼无
	--tstationlists.AreaType,          --地区类别    营销有 欧曼无
	tstationlists.ServicePurview    --维修权限

  from ft.tstations
 inner join ft.tstationlists
    on tstationlists.stationid = tstations.objid
 inner join ft.tstationlevels
    on tstationlevels.objid = tstationlists.levelid
 inner join ft.tareaunitpricedetails de
    on de.parentid = tstationlists.objid
 where tstations.isvalid > 0
   and tstationlists.isvalid > 0
   and tstationlists.brandid =
       (select objid from ft.tbrands where tbrands.name = '欧曼');



