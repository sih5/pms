select tstations.code,
       tstations.name,
       (select code
          from ft.tfiliales
         where tfiliales.objid = tstationlists.filialeid) as filialecode,
       (select name
          from ft.tfiliales
         where tfiliales.objid = tstationlists.filialeid) as filialename,
       (select name
          from ft.tproductlines
         where tproductlines.objid = de.productlineid) as productname,
       de.workprice,
       '' as 工时单价等级
  from ft.tstations
 inner join ft.tstationlists
    on tstationlists.stationid = tstations.objid
 inner join ft.tareaunitpricedetails de
    on de.parentid = tstationlists.objid
 where tstations.isvalid > 0
   and tstationlists.isvalid > 0
   and tstationlists.brandid =
       (select objid from ft.tbrands where tbrands.name = '商混设备');
