insert into DealerMarketDptRelation
( 
    Id, --Id
    MarketId, --市场部Id
    DealerId, --经销商Id
    BranchId, --营销分公司Id
    Status, --状态
    CreatorId, --创建人Id
    CreatorName, --创建人
    CreateTime --创建时间
--    ModifierId, --修改人Id
--    ModifierName, --修改人
--    ModifyTime, --修改时间
--    AbandonerId, --作废人Id
--    AbandonerName, --作废人
--    AbandonTime, --作废时间
)
(
    select 
    dcs.s_dealermarketdptrelation.nextval,
    (select id from MarketingDepartment t6 where t1.marketcode=t6.code), --	市场部Id
    (select id from Enterprise t2 where t1.stationcode=t2.code),
    (select id from dcs.branch t3 where t1.filialecode=t3.code), --分公司ID
    1,
    1,
    '系统管理员',
    sysdate
     from DealerMarketDptRelationtemp t1
)



--select * from DealerMarketDptRelation

--select * from DealerMarketDptRelationtemp