insert into CompanyInvoiceInfo
( Id, --Id
  InvoiceCompanyId, --开票企业Id
  CompanyId, --企业Id
  CompanyCode, --企业编号
  CompanyName, --企业名称
  IsVehicleSalesInvoice, --是否整车销售开票
  IsPartsSalesInvoice, --是否配件销售开票
  TaxRegisteredNumber, --税务登记号
  InvoiceTitle, --开票名称
  InvoiceType, --开票类型
  TaxpayerQualification, --纳税人资格
  InvoiceAmountQuota, --发票限额
  TaxRegisteredAddress, --税务登记地址
--TaxRegisteredPhone, --税务登记电话
  BankName, --开户行名称
  BankAccount, --开户行账号
  InvoiceTax, --开票税率
  Linkman, --财务联系人
  ContactNumber, --财务联系电话
  Fax, --财务传真
  Remark, --备注
  CreateTime, --创建时间
  CreatorId, --创建人Id
  CreatorName, --创建人
--  ModifyTime, --修改时间
--  ModifierId, --修改人Id
--  ModifierName, --修改人
  Status --状态
)
(
  select 
  dcs.s_CompanyInvoiceInfo.nextval, --id
  (select id from dcs.branch t3 where t3.code='SHSB'), --开票企业（分公司ID）
  (select id from dcs.company t2 where t1.code=t2.code) ID, --企业ID
  (select code from dcs.company t2 where t1.code=t2.code),--企业编号
  (select name from dcs.company t2 where t1.code=t2.code),--企业名称
   0, --是否整车销售开票
   1, --是否配件销售开票
   taxcode, --税务登记号
   InvoiceTitle, --开票名称
   InvoiceType, --开票类型
   TaxpayerQualification, --纳税人资格
   inviocequota, --发票限额
   addressphone, --税务登记地址
-- addressphone, --税务登记电话
   Bank, --开户行名称
   BankAccount, --开户行账号
   InvoiceTax, --开票税率
   Linker, --财务联系人
   tel, --财务联系电话
   Fax, --财务传真
   '', --备注
   sysdate, --创建时间
   1, --创建人Id
   '系统管理员', --创建人
--  ModifyTime, --修改时间
--  ModifierId, --修改人Id
--  ModifierName, --修改人
    1 --状态
from CompanyInvoiceInfotemp t1 where exists(select * from company where company.code=t1.code)
)
