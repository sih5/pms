select tsuppliers.code,
       tsuppliers.name,
       'SHSB' as 购货方企业编号,
       '商混设备销售分公司' as 购货方企业名称,
       (select name
          from ft.tbrands
         where tbrands.objid = tcustaccounts.brandid) as brandname,
       tcustaccounts.balance
  from ft.tcustaccounts
 inner join ft.tsuppliers
    on tsuppliers.objid = tcustaccounts.customerid
 where tsuppliers.isvalid > 0
   and tcustaccounts.isvalid > 0
   and tcustaccounts.brandid =
       (select objid from ft.tbrands where tbrands.name = '商混设备');
