select tstations.code,
       tstations.name,
       (select name from ft.tbrands where tbrands.objid = t.brandid) as brandname,
       t.balance,
       t.credit,
       (select ReturnFee
          from tReturns@fspare where tReturns.Customerid = tstations.objid and tReturns.Brandid = tstationlists.brandid) as returnfee,
       '' as 账户组编号,
       '' as 账户组名称
  from ft.tcustaccounts@fspare t
 inner join ft.tstations 
    on tstations.objid = t.customerid
 inner join ft.tstationlists
    on tstationlists.stationid = tstations.objid and t.brandid=tstationlists.brandid
 where tstations.isvalid > 0
   and tstationlists.isvalid > 0
   and t.isvalid > 0
   and tstationlists.brandid =
       (select objid from ft.tbrands where tbrands.name = '商混设备')
   and t.brandid =
       (select objid from ft.tbrands where tbrands.name = '商混设备');
