--创建客户账户临时表
create table tCutAccountTemp
(

       StaCode varchar2(50),
       StaName varchar2(100),
       BrandName varchar2(50),
       Banlance number(19,4),   --账户余额
       Credit number(19,4),   --信用额度
       returnfee number(19,4),   --返利金额
       AccCode varchar2(50),   --账户组编号
       AccName varchar2(100)   --账户组名称
);



--创建供应商客户临时表
create table tSupAccountTemp
(
       SupCode varchar2(50),
       supName varchar2(100),
       Entcode varchar2(50),    --购货方企业编号
       EntName varchar2(100),    --购货方企业名称
       BrandName varchar2(50),    --品牌
       balance number(19,4)     --应付款金额
)
