--插入供应商客户信息
insert into SupplierAccount
  (Id,
   BuyerCompanyId,
   SupplierCompanyId,
   DueAmount, --应付款金额
   EstimatedDueAmount,
   PaymentLimit,
   Status,
   CreatorName,
   CreateTime)
  select dcs.s_supplieraccount.nextval,
         (select id
            from dcs.company
           where company.code = tSupAccountTemp.Entcode),    --购货方企业id
         (select id
            from dcs.PartsSupplier
           where Partssupplier.Code = tSupAccountTemp.Supcode),   --供应商企业Id
         balance,
         0,
         0,
         1,
         'admin',
         sysdate
    from tSupAccountTemp;
