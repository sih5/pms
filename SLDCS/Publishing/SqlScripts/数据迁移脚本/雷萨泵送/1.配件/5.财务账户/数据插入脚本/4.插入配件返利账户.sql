--插入配件返利账户信息

insert into dcs.Partsrebateaccount
  (Id,
   AccountGroupId, --账户组id
   CustomerCompanyId,
   CustomerCompanyCode,
   CustomerCompanyName,
   AccountBalanceAmount, --结存金额
   Status,
   CreatorName,
   CreateTime)
  select
     dcs.s_Partsrebateaccount.nextval,
     (select id from dcs.accountgroup where AccountGroup.Code = tCutAccountTemp.Acccode),
     (select Id from dcs.company where company.code = tCutAccountTemp.StaCode),
     StaCode,
     StaName,
     returnfee, --返利金额
     1,
     'admin',
     sysdate
  from tCutAccountTemp;
