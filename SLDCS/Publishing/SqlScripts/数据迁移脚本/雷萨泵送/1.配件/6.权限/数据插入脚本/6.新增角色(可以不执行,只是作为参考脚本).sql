insert into role
  (id, enterpriseid, name, isadmin, status, creatorname, createtime)
  select security.s_role.nextval as id,
         enterprise.id as enterpriseid,
         (select name
            from security.RoleTemplate
           where name = '服务站业务管理角色') as name,
         0 as isadmin,
         1 as status,
         'admin' as creatorname,
         sysdate as createtime
    from security.enterprise
   where enterprisecategoryid = 2;


insert into rolepersonnel
  (id, roleid, personnelid)   
select s_rolepersonnel.nextval as id, M.Roleid, MM.Personnelid
  from (select role.id as roleid, enterprise.id as enterpriseid
          from role
         inner join enterprise
            on enterprise.id = role.enterpriseid
         where enterprisecategoryid = 2
           and role.name = '服务站业务管理角色') M
 inner join (select personnel.id  as personnelid,
                    enterprise.id as enterpriseid
               from personnel
              inner join enterprise
                 on enterprise.id = personnel.enterpriseid
              where enterprisecategoryid = 2) MM
    on M.Enterpriseid = MM.Enterpriseid;

insert into rule
  (id, roleid, nodeid)
  select s_rule.nextval as id, M.roleid, MM.Nodeid
    from (select role.id as roleid
            from role
           inner join enterprise
              on enterprise.id = role.enterpriseid
           where enterprisecategoryid = 2
             and role.name = '服务站业务管理角色') M,         
         (select RoleTemplateRule.Nodeid
            from security.RoleTemplate
           inner join RoleTemplateRule
              on RoleTemplateRule.Roletemplateid = RoleTemplate.id
           where name = '服务站业务管理角色') MM


