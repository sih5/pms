--雷萨欧曼
select tmaterials.code, --配件图号
       tmaterials.name, --配件名称
       tmaterials.oldcode, --配件参图号
       tparams.paramvalue, --损耗类型
       tmaterials.brandname, --品牌
       decode(tmaterials.ispurchased, 1, '是', 0, '否') as IsOrderable,--是否可采购,
       decode(tmaterials.issaled, 1, '是', 0, '否') as IsSalable,--是否销售,
       tmaterials.minqty, --最小销售数量
       '' isdirectsupply,--是否可直供,
       '' isservice,--是否售后服务件,
       '' partsreturnpolicy,--旧件返回政策,
       '' warrantysupplystatus,--保内保外供货属性,
       '' purchaseroute,--采购路线,
       '' partswarhousemanagegranularity,--配件仓储管理粒度,
       '' partswarrantycategorycode,--配件保修分类编号,
       '' partswarrantycategoryname,--配件保修分类名称,
       tsuppliers.code as suppliercode,--供应商编号,
       tsuppliers.name as suppliername --供应商名称
  from ft.tmaterials
inner join ft.tsuppliers on tsuppliers.objid =tmaterials.supplierid 
  left join ft.tParams
    on tmaterials.UllageTypeCode = tparams.paramcode
   and tparams.paramtype = '配件损耗类型'
 where brandid =
       (select objid from ft.tbrands where tbrands.name = '商混设备')
       and tmaterials.isvalid>0;

--营销时代
select  tmaterials.code, --配件图号
       tmaterials.name, --配件名称
       tmaterials.oldcode, --配件参图号
       tparams.paramvalue, --损耗类型
       tmaterials.brandname, --品牌
       decode(tmaterials.ispurchased, 1, '是', 0, '否') as 是否可采购,
       decode(tmaterials.issaled, 1, '是', 0, '否') as 是否销售,
       tmaterials.minqty, --最小销售数量
       '' 是否可直供,
       '' 是否售后服务件,
       '' 旧件返回政策,
       '' 保内保外供货属性,
       '' 采购路线,
       '' 配件仓储管理粒度,
       '' 配件保修分类编号,
       '' 配件保修分类名称,
       tsuppliers.code as 供应商编号,
       tsuppliers.name as 供应商名称
  from ft.tmaterials
 inner join ft.tsuppliers on tsuppliers.objid =tmaterials.supplierid 
 left join ft.tParams
    on tmaterials.UllageTypeCode = tparams.paramcode
   and tparams.paramtype = '配件损耗类型'  
 inner join ft.tmaterialbrand
    on tmaterialbrand.materialid = tmaterials.objid
 where tmaterialbrand.brandid =
       (select objid from ft.tbrands where tbrands.name = '时代')
       and tmaterials.isvalid>0
       and tmaterialbrand.isvalid>0;
