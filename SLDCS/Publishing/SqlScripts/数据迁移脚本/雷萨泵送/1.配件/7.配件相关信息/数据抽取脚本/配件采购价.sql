--雷萨欧曼
select tmaterials.code,
       tmaterials.name,
       tmaterials.brandname,
       (select code
          from ft.tsuppliers
         where tsuppliers.objid = tmaterials.supplierid) as suppliercode,
       (select name
          from ft.tsuppliers
         where tsuppliers.objid = tmaterials.supplierid) as suppliername,
       tmaterialprices.compactprice,
       '' as validfrom,--生效时间,
       '' as validto --失效时间
  from ft.tmaterials
 inner join ft.tmaterialprices
    on tmaterialprices.materialid = tmaterials.objid
 where tmaterials.brandid =
       (select objid from ft.tbrands where name = '商混设备');

--营销时代
select tmaterials.code,
       tmaterials.name,
       (select code
          from ft.tsuppliers
         where tsuppliers.objid = tmaterials.supplierid) as suppliercode,
       (select name
          from ft.tsuppliers
         where tsuppliers.objid = tmaterials.supplierid) as suppliername,
       tmaterialprices.compactprice,
       '' as 生效时间,
       '' as 失效时间
  from ft.tmaterials
 inner join ft.tmaterialbrand
    on tmaterialbrand.materialid = tmaterials.objid
 inner join ft.tmaterialprices
    on tmaterialprices.materialid = tmaterials.objid
 where tmaterialbrand.brandid =
       (select objid from ft.tbrands where name = '时代');
