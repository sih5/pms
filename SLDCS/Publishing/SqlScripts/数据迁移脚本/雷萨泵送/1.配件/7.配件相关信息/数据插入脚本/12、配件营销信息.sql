--配件营销信息
insert into PartsBranch
(
   Id,
   PartId,
   PartCode,
   PartName,
   ReferenceCode,
-- StockMaximum,
-- StockMinimum,
   BranchId,
   BranchName,
   PartsSalesCategoryId,
   PartsSalesCategoryName,
-- ABCStrategyId,
-- ProductLifeCycle,
   LossType,
   IsOrderable,
-- PurchaseCycle,
   IsService,
   IsSalable,
   PartsReturnPolicy,
   IsDirectSupply,
   WarrantySupplyStatus,
   MinSaleQuantity,
-- PartABC,
   PurchaseRoute,
-- RepairMatMinUnit,
   PartsWarrantyCategoryId,
   PartsWarrantyCategoryCode,
   PartsWarrantyCategoryName,
   PartsWarhouseManageGranularity,
   Status,
   CreatorId,
   CreatorName,
   CreateTime,
-- ModifierId,
-- ModifierName,
-- ModifyTime,
-- AbandonerId,
-- AbandonerName,
-- AbandonTime,
   Remark
)
( select
   dcs.s_PartsBranch.nextval,
   (select id from SparePart t2 where t1.code=t2.code),
   Code,
   Name,
   oldCode,
-- StockMaximum,
-- StockMinimum,
   (select ID from dcs.branch t5 where t5.code='OMYXKHB'),  --营销分公司Id
   'SHSB', 
   (select id from PartsSalesCategory t3 where t1.brandname=t3.name),
   brandname,
-- ABCStrategyId,
-- ProductLifeCycle,
   decode(paramvalue,'非常用件',1,'常用件',2,'易耗件',3,'大总成',4),--1非常用件 2常用件 3易耗件 4大总成
   decode(IsOrderable,'是',1,'否',0),
-- PurchaseCycle,
   decode(IsService,'是',1,'否',0),
   decode(IsSalable,'是',1,'否',0),
   decode(PartsReturnPolicy,'返回本部',1,'供应商自行回收',2,'监督处理',3,'自行处理',4), --1	返回本部2	供应商自行回收3	监督处理4	自行处理
   decode(IsDirectSupply,'是',1,'否',0),
   WarrantySupplyStatus,
   minqty,
-- PartABC,
   PurchaseRoute,
-- RepairMatMinUnit,
   (select id from PartsWarrantyCategory t4 where t1.partswarrantycategorycode=t4.code),
   PartsWarrantyCategoryCode,
   PartsWarrantyCategoryName,
   3,--PartsWarhouseManageGranularity 仓库管理粒度
   1,
   1,
   '系统管理员',
   sysdate,
-- ModifierId,
-- ModifierName,
-- ModifyTime,
-- AbandonerId,
-- AbandonerName,
-- AbandonTime,
   ''
  from PartsBranchwktemp t1
  where exists (select id from SparePart t7 where t1.code=t7.code)
)


/*
update PartsBranchwktemp
set PartsReturnPolicy='返回本部'*/



/*
select * from PartsBranchwktemp t1
where not exists (select id from SparePart t2 where t1.code=t2.code)
*/


--select * from PartsWarrantyCategory


--select * from PartsReturnPolicy