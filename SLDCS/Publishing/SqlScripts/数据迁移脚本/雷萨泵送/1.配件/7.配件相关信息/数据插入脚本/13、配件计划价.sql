insert into PartsPlannedPrice
(
  Id, --Id
  OwnerCompanyId, --隶属企业Id
  OwnerCompanyType, --隶属企业类型
  PartsSalesCategoryId, --配件销售类型Id
  PartsSalesCategoryName, --配件销售类型名称
  SparePartId, --配件Id
  PlannedPrice, --计划价
  CreatorId, --创建人Id
  CreatorName, --创建人
  CreateTime --创建时间
/*ModifierId, --修改人Id
ModifierName, --修改人
ModifyTime, --修改时间*/
)
(
 select 
  dcs.s_PartsPlannedPrice.nextval,
  (select ID from dcs.branch t4 where t4.code='SHSB'),  --营销分公司Id 隶属企业
  1, --隶属企业类型
  (select id from PartsSalesCategory t3 where t1.brandname=t3.name), --配件销售类型Id
  brandname, --配件销售类型名称
  (select id from SparePart t2 where t1.code=t2.code), --配件Id
  stdprice, --计划价
  1, --创建人Id
  '系统管理员', --创建人
  sysdate --创建时间
  from PartsPlannedPricetemp t1
)













--select * from PartsPlannedPricetemp