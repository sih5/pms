

insert into PartsPurchasePricing
(
  Id, --Id
  BranchId, --营销分公司Id
  PartId, --配件Id
  PartsSupplierId, --配件供应商Id
  PartsSalesCategoryId, --配件销售类型Id
  PartsSalesCategoryName, --配件销售类型名称
  ValidFrom, --生效时间
  ValidTo, --失效时间
  PriceType, --价格类型
  PurchasePrice, --采购价格
  Status, --状态
  CreatorId, --创建人Id
  CreatorName, --创建人
  CreateTime --创建时间
/*  ModifierId, --修改人Id
  ModifierName, --修改人
  ModifyTime, --修改时间
AbandonerId, --作废人Id
AbandonerName, --作废人
AbandonTime, --作废时间*/
)
(
  select 
  dcs.s_PartsPurchasePricing.nextval, --Id
  (select ID from dcs.branch t4 where t4.code='SHSB'),  --营销分公司Id
  (select id from SparePart t2 where t1.code=t2.code), --配件Id
  (select id from PartsSupplier t3 where t1.suppliercode=t3.code), --配件供应商Id
  (select id from PartsSalesCategory t5 where t1.brandname=t5.name), --配件销售类型Id
  brandname, --配件销售类型名称
  sysdate, --生效时间
  sysdate+365 as ValidTo, --失效时间
  1, --价格类型 1基准销售价 2全国统一价
  COMPACTPrice, --采购价格
  2, --状态
  1, --创建人Id
  '系统管理员', --创建人
  sysdate --创建时间
  from PartsPurchasePricingtemp t1
  where t1.suppliercode is not null
)








--select * from PartsPurchasePricingtemp


/*
select * from PartsPurchasePricingtemp t1
where not exists(select id from PartsSupplier t3 where t1.suppliercode=t3.code)


*/