insert into PartsRetailGuidePrice
(
  Id, --Id
  BranchId, --营销分公司Id
  PartsSalesCategoryId, --配件销售类型Id
  PartsSalesCategoryCode, --销售类型编号
  PartsSalesCategoryName, --销售类型名称
  SparePartId, --配件Id
  SparePartCode, --配件编号
  SparePartName, --配件名称
  RetailGuidePrice, --零售指导价
  ValidationTime, --生效时间
  ExpireTime, --失效时间
  Status, --状态
  Remark, --备注
  CreatorId, --创建人Id
  CreatorName, --创建人
  CreateTime --创建时间
 /* ModifierId, --修改人Id
  ModifierName, --修改人
  ModifyTime, --修改时间
  AbandonerId, --作废人Id
  AbandonerName, --作废人
  AbandonTime, --作废时间*/
)
select
  dcs.s_PartsRetailGuidePrice.nextval, --Id
  (select ID from dcs.branch t4 where t4.code='SHSB'),  --营销分公司Id
  (select id from PartsSalesCategory t5 where t1.brandname=t5.name), --配件销售类型Id
  (select code from PartsSalesCategory t3 where t1.brandname=t3.name), --配件销售类型Id
  brandname, --销售类型名称
  (select id from SparePart t2 where t1.code=t2.code), --配件Id
  Code, --配件编号
  Name, --配件名称
  RetailPrice, --零售指导价
  sysdate, --生效时间
  sysdate+365, --失效时间*/
  1, --状态
  '', --备注
  1, --创建人Id
  '系统管理员', --创建人
  sysdate --创建时间
/*  ModifierId, --修改人Id
  ModifierName, --修改人
  ModifyTime, --修改时间
  AbandonerId, --作废人Id
  AbandonerName, --作废人
  AbandonTime, --作废时间*/
from PartsRetailGuidePricetemp t1



--select * from PartsRetailGuidePricetemp