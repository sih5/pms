insert into PartsSupplierRelation
(
  Id,	--Id
  BranchId,	--营销分公司Id
  PartsSalesCategoryId,	--配件销售类型Id
  PartsSalesCategoryName,	--销售类型名称
  PartId,	--配件Id
  SupplierId,	--供应商Id
  SupplierPartCode,	--供应商图号
  SupplierPartName,	--供应商配件名称
  --IsPrimary,	--是否首选供应商
  --PurchasePercentage,	--采购占比
  --EconomicalBatch,	--经济采购批量
  MinBatch,	--最小采购批量
  Status,	--状态
  Remark	--备注
)
(
   select dcs.s_partssupplierrelation.nextval, --ID
    (select ID from dcs.branch t4 where t4.code='SHSB'),--欧曼营销分公司Id
    (select id from PartsSalesCategory t5 where t1.brandname=t5.name), --配件销售类型Id
    brandname, --配件销售类型名称
    (select id from SparePart t2 where t1.code=t2.code), --配件Id
    (select id from PartsSupplier t3 where t1.suppliercode=t3.code), --配件供应商Id
    Code,	--供应商图号
    Name,	--供应商配件名称
--  IsPrimary,	--是否首选供应商
--  PurchasePercentage,	--采购占比
--  EconomicalBatch,	--经济采购批量
    minqty,	--最小采购批量
    1,	--状态
    ''	--备注
from PartsBranchwktemp t1
where exists (select id from SparePart t7 where t1.code=t7.code)
)