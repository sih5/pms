insert into SparePart
(   Id,                 
    Code,               
    Name,                
    LastSubstitute,      
    NextSubstitute,     
    ShelfLife,           
    EnglishName,
--  PinyinCode,
--  ReferenceCode,
--  ReferenceName,
    CADCode,
    CADName,
    PartType,                
    Specification,
    Feature,
    Status,                    
--    Length,
--    Width,
--    Height,
--  Volume,
--  Weight,
--  Material,
    PackingAmount,
    PackingSpecification,
--   PartsOutPackingCode,
--   PartsInPackingCode,
    MeasureUnit,                 
    CreatorId,
    CreatorName,
    CreateTime
--    ModifierId,
--    ModifierName,
--    ModifyTime,
--    AbandonerId,
--    AbandonerName,
--    AbandonTime
   )
(
   select 
     dcs.s_sparepart.nextval,
     code,
     name,
     upparts,
     nextparts,
     ShelfLife, 
     Ename,
--   PinyinCode,
--   ReferenceCode,
--   ReferenceName,
     CADCode,
     CADName,
     2, --提取数据时为空，无法将NULL值插入    1总成件 2配件 3精品件 4辅料????????????????
     Spec,
     memosp,
     1,                    
--   Length,
--   Width,
--   Height,
--   Volume,
--   Weight,
--   Material,
    PackingAmount,
    PackingSpecification,
--  PartsOutPackingCode,
--  PartsInPackingCode,
    unitname,   --为空的配件计量单位 更新为件              
    1,
    '系统管理员',
    sysdate
   from SparePartwktemp t1
)






update SparePartwktemp
set unitname='件'
where unitname is null





--select * from SparePartwktemp
--where unitname is null




