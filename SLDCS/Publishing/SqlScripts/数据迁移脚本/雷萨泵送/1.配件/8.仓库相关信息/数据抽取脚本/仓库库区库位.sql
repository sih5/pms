--分公司
select code,
       name,
       仓储企业编号,
       仓储企业名称,
       仓储企业类型,
       库区编号,
       库位编号,
       decode(库位编号, '', '库区', '库位') 库区库位类型,
       库区用途
  from (select twarehouses.code,
               twarehouses.name,
               '' 仓储企业编号,
               '' 仓储企业名称,
               '分公司' 仓储企业类型,
               tareas.code || tareafields.code as 库区编号,
               tlocations.code as 库位编号,
               '' 库区库位类型,
               '' 库区用途
          from ft.twarehouses
         inner join ft.tareas
            on tareas.warehouseid = twarehouses.objid
         inner join ft.tareafields
            on tareafields.areaid = tareas.objid
         inner join ft.tlocations
            on tlocations.areafieldid = tareafields.objid
         where brandid =
               (select objid from ft.tbrands where tbrands.name = '商混设备')
               and twarehouses.isvalid>0
               and tareas.isvalid>0
               and tareafields.isvalid>0
               and tlocations.isvalid>0)

union all
select code,
       name,
       '' 仓储企业编号,
       '' 仓储企业名称,
       '分公司' 仓储企业类型,
       code 库区编号,
       '' 库位编号,
       '仓库' 库区库位类型,
       '' 库区用途
  from ft.twarehouses
 where brandid =
       (select objid from ft.tbrands where tbrands.name = '商混设备')
       and twarehouses.isvalid>0;

--代理库
select code,
       name,
       仓储企业编号,
       仓储企业名称,
       仓储企业类型,
       库区编号,
       库位编号,
       decode(库位编号, '', '库区', '库位') 库区库位类型,
       库区用途
  from (select twarehouses.code,
               twarehouses.name,
               tagents.code 仓储企业编号,
               tagents.name 仓储企业名称,
               '代理库' 仓储企业类型,
               tareas.code || tareafields.code as 库区编号,
               tlocations.code as 库位编号,
               '' 库区库位类型,
               '' 库区用途
          from ft.twarehouses
         inner join ft.tagents
            on tagents.objid = twarehouses.entercode
         inner join ft.tareas
            on tareas.warehouseid = twarehouses.objid
         inner join ft.tareafields
            on tareafields.areaid = tareas.objid
         inner join ft.tlocations
            on tlocations.areafieldid = tareafields.objid
         where tagents.brandid =
               (select objid from ft.tbrands where tbrands.name = '商混设备')
           and twarehouses.isvalid > 0
           and tagents.isvalid > 0
            and tareas.isvalid>0
               and tareafields.isvalid>0
               and tlocations.isvalid>0)

union all
select twarehouses.code,
       twarehouses.name,
       tagents.code 仓储企业编号,
       tagents.name 仓储企业名称,
       '代理库' 仓储企业类型,
       twarehouses.code 库区编号,
       '' 库位编号,
       '仓库' 库区库位类型,
       '' 库区用途
  from ft.twarehouses
 inner join ft.tagents
    on tagents.objid = twarehouses.entercode
 where tagents.brandid =
       (select objid from ft.tbrands where tbrands.name = '商混设备')
   and twarehouses.isvalid > 0
   and tagents.isvalid > 0;


--服务站
select code,
       name,
       仓储企业编号,
       仓储企业名称,
       仓储企业类型,
       库区编号,
       库位编号,
       decode(库位编号, '', '库区', '库位') 库区库位类型,
       库区用途
  from (select twarehouses.code,
               twarehouses.name,
               tstations.code 仓储企业编号,
               tstations.name 仓储企业名称,
               '服务站' 仓储企业类型,
               tareas.code || tareafields.code as 库区编号,
               tlocations.code as 库位编号,
               '' 库区库位类型,
               '' 库区用途
          from ft.twarehouses
         inner join ft.tareas
            on tareas.warehouseid = twarehouses.objid
         inner join ft.tareafields
            on tareafields.areaid = tareas.objid
         inner join ft.tlocations
            on tlocations.areafieldid = tareafields.objid
         inner join ft.tstations
            on tstations.objid = twarehouses.entercode
         inner join ft.tstationlists
            on tstationlists.stationid = tstations.objid
         where twarehouses.isvalid > 0
           and tstations.isvalid > 0
           and tstationlists.isvalid>0
            and tareas.isvalid>0
               and tareafields.isvalid>0
               and tlocations.isvalid>0
           and tstationlists.brandid =
               (select objid from ft.tbrands where tbrands.name = '商混设备'))

union all
select twarehouses.code,
       twarehouses.name,
       tstations.code 仓储企业编号,
       tstations.name 仓储企业名称,
       '服务站' 仓储企业类型,
       twarehouses.code 库区编号,
       '' 库位编号,
       '仓库' 库区库位类型,
       '' 库区用途
  from ft.twarehouses
 inner join ft.tstations
    on tstations.objid = twarehouses.entercode
 inner join ft.tstationlists
    on tstationlists.stationid = tstations.objid
 where twarehouses.isvalid > 0
   and tstations.isvalid > 0
   and tstationlists.isvalid>0
   and tstationlists.brandid =
       (select objid from ft.tbrands where tbrands.name = '商混设备');
