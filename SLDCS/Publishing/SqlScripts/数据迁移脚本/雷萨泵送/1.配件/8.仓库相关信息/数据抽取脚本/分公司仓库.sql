select code,
       name,
       decode(WarehouseType, 1, '总库', 2, '分库') as 仓库类型,
       '' as 分公司编号,
       '' as 分公司名称,
       E_mail,
       Fax,
       Linker,
       Tel,
       Address,
       '定位存储' as 存储策略,
       '' as 储运中心,
       '' as 是否与WMS接口对接
  from ft.twarehouses
 where brandid =
       (select objid from ft.tbrands where tbrands.name = '商混设备') and twarehouses.isvalid>0;
