select twarehouses.code,
       twarehouses.name,
       '' 仓储企业编号,
       '' 仓储企业名称,
       '分公司' 仓储企业类型,
       tareas.code || tareafields.code as 库区编号,
       (select code
          from ft."tPerson"@fpurview p
         where p.objid = tareafieldmans.empid) 库段负责人编号,
       (select name
          from ft."tPerson"@fpurview p
         where p.objid = tareafieldmans.empid) 库段负责人名称
  from ft.twarehouses
 inner join ft.tareas
    on tareas.warehouseid = twarehouses.objid
 inner join ft.tareafields
    on tareafields.areaid = tareas.objid
 inner join ft.tareafieldmans
    on tareafieldmans.areafieldid = tareafields.objid
 where brandid =
       (select objid from ft.tbrands where tbrands.name = '商混设备')
   and twarehouses.isvalid > 0
   and tareas.isvalid>0
   and tareafields.isvalid>0
   and tareafieldmans.isvalid>0   
