select twarehouses.code,
       twarehouses.name,
       '分库' as 仓库类型,
       '' as 分公司编号,
       '' as 分公司名称,
       tstations.code as 仓储企业编号,
       tstations.name as 仓储企业名称,
       twarehouses.E_mail,
       twarehouses.Fax,
       twarehouses.Linker,
       twarehouses.Tel,
       twarehouses.Address,
       '定位存储' as 存储策略,
       '' as 储运中心,
       '' as 是否与WMS接口对接
  from ft.twarehouses
 inner join ft.tstations
    on tstations.objid = twarehouses.entercode
 inner join ft.tstationlists
    on tstationlists.stationid = tstations.objid
 where twarehouses.isvalid > 0
   and tstations.isvalid > 0
   and tstationlists.isvalid>0
   and tstationlists.brandid =
       (select objid from ft.tbrands where tbrands.name = '商混设备');
