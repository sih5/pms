--分公司
select twarehouses.code,
       twarehouses.name,
       '' 仓储企业编号,
       '' 仓储企业名称,
       '分公司' 仓储企业类型,
       tlocations.code as 库位编号,
       tareas.code || tareafields.code as 库区编号,
       '' 库区用途,
       (select code
          from ft.tmaterials
         where tmaterials.objid = tstores.materialid) as 配件编号,
       (select name
          from ft.tmaterials
         where tmaterials.objid = tstores.materialid) as 配件名称,
       sum(tstores.usableqty) as 数量
  from ft.tstores
 inner join ft.tmaterials t1
    on t1.objid = tstores.materialid
 inner join ft.twarehouses
    on twarehouses.objid = tstores.warehouseid
 inner join ft.tlocations
    on tlocations.objid = tstores.locationid
 inner join ft.tareafields
    on tlocations.areafieldid = tareafields.objid
 inner join ft.tareas
    on tareafields.areaid = tareas.objid
 where twarehouses.brandid =
       (select objid from ft.tbrands where tbrands.name = '商混设备')
   and twarehouses.isvalid > 0
   and tstores.isvalid > 0
   and tlocations.isvalid > 0
   and tareafields.isvalid > 0
   and tareas.isvalid > 0
   and t1.brandid =
       (select objid from ft.tbrands where tbrands.name = '商混设备')
   and t1.isvalid > 0
 group by tstores.warehouseid,
          tstores.locationid,
          tstores.materialid,
          twarehouses.code,
          twarehouses.name,
          tlocations.code,
          tareas.code,
          tareafields.code

--代理库
select twarehouses.code,
       twarehouses.name,
       tagents.code 仓储企业编号,
       tagents.name 仓储企业名称,
       '代理库' 仓储企业类型,
       tlocations.code as 库位编号,
       tareas.code || tareafields.code as 库区编号,
       '' 库区用途,
       (select code
          from ft.tmaterials
         where tmaterials.objid = tstores.materialid) as 配件编号,
       (select name
          from ft.tmaterials
         where tmaterials.objid = tstores.materialid) as 配件名称,
       sum(tstores.usableqty) as 数量
  from ft.tstores
   inner join ft.tmaterials t1
    on t1.objid = tstores.materialid
 inner join ft.twarehouses
    on twarehouses.objid = tstores.warehouseid
 inner join ft.tagents
    on tagents.objid = twarehouses.entercode
 inner join ft.tlocations
    on tlocations.objid = tstores.locationid
 inner join ft.tareafields
    on tlocations.areafieldid = tareafields.objid
 inner join ft.tareas
    on tareafields.areaid = tareas.objid
 where tagents.brandid =
       (select objid from ft.tbrands where tbrands.name = '商混设备')
   and twarehouses.isvalid > 0
   and tstores.isvalid > 0
   and tagents.isvalid>0
   and tlocations.isvalid>0
   and tareafields.isvalid>0
   and tareas.isvalid>0
    and t1.brandid =
       (select objid from ft.tbrands where tbrands.name = '商混设备')
   and t1.isvalid > 0
 group by tstores.warehouseid,
          tstores.locationid,
          tstores.materialid,
          twarehouses.code,
          twarehouses.name,
          tlocations.code,
          tareas.code,
          tareafields.code,
          tagents.code,
          tagents.name
