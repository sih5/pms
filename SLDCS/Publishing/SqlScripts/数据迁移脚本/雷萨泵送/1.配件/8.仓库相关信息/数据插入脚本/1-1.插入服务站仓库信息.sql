--插入服务站仓库信息
-- 服务站仓库插入 828 条数据
insert into dcs.Warehouse
  (Id,
   code,
   Name,
   Type,
   Status,
   
   Address,PhoneNumber,Contact,Fax,Email,
   
   StorageStrategy,
   BranchId, --营销分公司Id
   StorageCompanyId,
   StorageCompanyType, --仓储企业类型
   
   WmsInterface,
   CreatorName,
   CreateTime)
  select dcs.s_warehouse.nextval,
         WareCode,
         WareName,
         decode(tStaWareTemp.Waretype, '分库', 2, '总库', 1, '虚拟库', 99), --仓库类型
         1, --状态
         
         WareAddress,WareTel,WareLinker,WareFax,WareE_mail,
         
         decode(tStaWareTemp.Storagestrategy, '定位存储', 1, '随机存储', 2), --存储策略
         (select Id from dcs.branch where branch.code = 'SHSB'), --商混设备销售分公司
         (select Id from dcs.Dealer where Dealer.Code = tStaWareTemp.Stacode), --仓储企业ID
         2, --仓储企业类型（服务站）
         
         decode(tStaWareTemp.IsWMS, '', 0, 1),
         'admin',
         sysdate
    from tStaWareTemp;



