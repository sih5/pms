--插入代理库仓库信息

insert into dcs.Warehouse
  (Id,
   Code,
   Name,
   Type,
   Status,
   
   Address,
   PhoneNumber,
   Contact,
   Fax,
   Email,
   
   StorageStrategy, --存储策略
   BranchId, --营销分公司ID
   StorageCompanyId,
   StorageCompanyType,
   WmsInterface,
   CreatorName,
   CreateTime)
  select dcs.s_warehouse.nextval,
         WareCode,
         WareName,
         decode(tAgentWareTemp.Waretype, '分库', 2, '总库', 1, '虚拟库', 99), --仓库类型
         1,
         
         WareAddress,
         WareTel,
         WareLinker,
         WareFax,
         WareE_Mail,
         
         decode(tAgentWareTemp.Storagestrategy,
                '定位存储',
                1,
                '随机存储',
                2), --存储策略
         
         (select Id from dcs.branch where branch.code = 'SHSB'), --商混设备销售分公司
         (select Id
            from dcs.Agency
           where Agency.Code = tAgentWareTemp.Agentcode), --仓储企业ID
         3, --仓储企业类型（代理库）
         
         decode(tAgentWareTemp.Iswms, '', 0, 1),
         'Admin',
         sysdate
    from tAgentWareTemp;
