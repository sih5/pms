--插入分公司仓库信息
--插入 2 条数据
insert into dcs.warehouse
  (Id,
   Code,
   Name,
   Type,        --仓库类型
   Status,
   
   Address,PhoneNumber,Contact,Fax,Email,
   
   StorageStrategy,    --存储策略
   BranchId,               --营销分公司id
   StorageCompanyId,     --仓储企业id
   StorageCompanyType,    --仓储企业类型
   WmsInterface,
   CreatorName,CreateTime)
select
   dcs.s_warehouse.nextval,
   WareCode,
   WareName,
   decode(tWareTemp.WareType,'分库',2,'总库',1,'虚拟库',99),     --仓库类型
   1,
   
   WareAddress,WareTel,WareLinker,WareFax,WareE_mail,
   --StorageStrategy,
   decode(tWareTemp.Storagestrategy,'定位存储',1,'随机存储',2),   --存储策略
   (select Id from dcs.Branch where Branch.Code = tWareTemp.Filialecode),   --商混设备销售分公司 
   (select Id from dcs.Branch where Branch.Code = tWareTemp.Filialecode),  --仓储企业ID (分公司)
   1,       --仓储企业类型(分公司)
   --IsWMS
   decode(tWareTemp.IsWMS,'',0,1),
   'admin',
   sysdate
 from tWareTemp;
