--创建仓库信息临时表

--创建服务站仓库信息临时表 tStaWareTemp
--临时表  条数据
create table tStaWareTemp
(   WareCode varchar2(50) not null, --仓库编号
    WareName varchar2(100) not null, --仓库名称
    WareType varchar2(40),     --仓库类型
    FilialeCode varchar2(50), --分公司编号
    FilialeName varchar2(100), --分公司名称
    StaCode varchar2(50) not null,     ---仓储企业编号
    StaName varchar2(100) not null,   --仓储企业名称 (服务站)
    WareE_mail varchar2(50),  
    WareFax varchar2(50),
    WareLinker varchar2(50),
    WareTel varchar2(50),
    WareAddress varchar2(200),
    StorageStrategy varchar2(50), --存储策略
    StorageCenter varchar2(50),  --储运中心
    IsWMS varchar2(50)            --是否与WMS接口
  );  
  
  
  
 
--创建代理库仓库信息临时表 tAgentWareTemp
--临时表插入 30 条数据
create table tAgentWareTemp(
    WareCode varchar2(50) not null,  --仓库编号
    WareName varchar2(100) not null,   --仓库名称
    WareType varchar2(40),       --仓库类型
    FilialeCode varchar2(50),   --分公司编号
    FilialeName varchar2(100),  --分公司名称
    AgentCode varchar2(50),     --仓储企业编号
    AgentName varchar2(100) not null,    --仓储企业名称（代理库）
    WareE_Mail varchar2(50),
    WareFax varchar2(50), 
    WareLinker varchar2(50),
    WareTel varchar2(50),
    WareAddress varchar2(200),
    StorageStrategy varchar2(50), --存储策略
    StorageCenter varchar2(50), --储运中心
    IsWMS varchar2(50)           --是否与WMS 接口    
    );   
    
    
--创建分公司仓库临时表 tWareTemp    

--临时表插入 2 条数据 
create table tWareTemp(
    WareCode varchar2(50),          --仓库编号
    WareName varchar2(100),         --仓库名称
    WareType varchar2(40),          --仓库类型
    FilialeCode varchar2(50),       --分公司编号
    FilialeName varchar2(100),      --分公司名称
    --FilialeWareName varchar2(100),   --仓储企业名称(分公司)
    WareE_mail varchar2(50),
    WareFax varchar2(50),
    WareLinker varchar2(50),
    WareTel varchar2(50),
    WareAddress varchar2(200),
    StorageStrategy varchar2(50),     --存储策略
    StorageCenter varchar2(50),       --储运中心
    IsWMS varchar2(50)                   --是否与WMS 接口
);
