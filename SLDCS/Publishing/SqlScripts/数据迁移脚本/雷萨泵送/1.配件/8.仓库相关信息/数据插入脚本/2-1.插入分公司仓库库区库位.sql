--����ֹ�˾�ֿ������λ
--1.�ֿ�
insert into dcs.Warehousearea
  (Id,
   WarehouseId,
   ParentId, --���ڵ�Id
   Code, --������λ���
   TopLevelWarehouseAreaId, --�������id
   AreaCategoryId, --������;id
   Status,
   AreaKind, --������λ����
   CreatorName,
   CreateTime)
  select dcs.s_warehousearea.nextval,
         warehouseid,
         parentid,
         AreaCode,
         TopWareLevelid,
         AreaCategoryid,
         Status,
         decode(kindtype,'�ֿ�',1,'����',2,'��λ',3),
         'admin',
         sysdate
    from (select (select Id
                    from dcs.warehouse
                   where warehouse.code = tFilialeAreaLocationTemp.Warecode) as warehouseid, --�ֿ�id
                 '' as parentid,
                 AreaCode, --������λ���
                 '' as TopWareLevelid,
                 null as AreaCategoryid,
                 1 as Status,
                 '�ֿ�' as kindtype
            from tFilialeAreaLocationTemp
           where AreaLocationType = '�ֿ�');

----------------------------------------------------------------------------
--2.����
insert into dcs.Warehousearea
  (Id,
   WarehouseId,
   ParentId, --���ڵ�Id
   Code, --������λ���
   TopLevelWarehouseAreaId, --�������id
   AreaCategoryId, --������;id
   Status,
   AreaKind, --������λ����
   CreatorName,
   CreateTime)

  select dcs.s_warehousearea.nextval,
         warehouseid,
         parentid,
         AreaCode,
         TopWareLevelid,
         AreaCategoryid,
         Status,
         decode(kindtype,'�ֿ�',1,'����',2,'��λ',3),
         'admin',
         sysdate
    from (select distinct (select Id
                              from dcs.warehouse
                             where warehouse.code =
                                   tFilialeAreaLocationTemp.Warecode) as warehouseid, --�ֿ�id
                           (select Id
                              from dcs.Warehousearea
                             where Warehousearea.code =
                                   tFilialeAreaLocationTemp.Warecode) as parentid,
                           AreaCode, --������λ���
                          null as TopWareLevelid,
                           (select id
                              from dcs.WarehouseAreaCategory
                             where WarehouseAreaCategory.Category = 1) as AreaCategoryid,
                           1 as Status,
                           '����' as kindtype
             from tFilialeAreaLocationTemp
            where AreaLocationType = '��λ');
            
        
----------------------------------------------------------------------------------------------
--3.��λ
insert into dcs.Warehousearea
  (Id,
   WarehouseId,
   ParentId, --���ڵ�Id
   Code, --������λ���
   TopLevelWarehouseAreaId, --�������id
   AreaCategoryId, --������;id
   Status,
   AreaKind, --������λ����
   CreatorName,
   CreateTime)

  select dcs.s_warehousearea.nextval,
         warehouseid,
         parentid,
         LocationCode,
         TopWareLevelid,
         AreaCategoryid,
         Status,
         decode(kindtype, '�ֿ�', 1, '����', 2, '��λ', 3),
         'admin',
         sysdate
    from (select (select Id
                    from dcs.warehouse
                   where warehouse.code = tFilialeAreaLocationTemp.Warecode) as warehouseid, --�ֿ�id
                 (select Id
                    from dcs.Warehousearea
                   where Warehousearea.code =
                         tFilialeAreaLocationTemp.AreaCode
                     and tFilialeAreaLocationTemp.Arealocationtype = '��λ') as parentid,
                 LocationCode, --������λ���
                 (select Id
                    from dcs.Warehousearea
                   where Warehousearea.code =
                         tFilialeAreaLocationTemp.Areacode
                     and tFilialeAreaLocationTemp.Arealocationtype = '��λ') as TopWareLevelid,
                 (select id
                    from dcs.WarehouseAreaCategory
                   where WarehouseAreaCategory.Category = 1) as AreaCategoryid,
                 1 as Status,
                 '��λ' as kindtype
            from tFilialeAreaLocationTemp
           where AreaLocationType = '��λ');

