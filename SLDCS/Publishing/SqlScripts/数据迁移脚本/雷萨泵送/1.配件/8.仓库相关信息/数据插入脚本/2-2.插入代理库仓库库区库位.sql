--���������ֿ������λ

--1.�ֿ�
insert into dcs.Warehousearea
  (Id,
   WarehouseId,
   ParentId,
   Code, --������λ���
   TopLevelWarehouseAreaId, --�������id
   AreaCategoryId, --������;Id
   Status,
   AreaKind, --������λ����
   CreatorName,
   CreateTime)
  select dcs.s_warehousearea.nextval,
         warehouseid,
         parentid,
         AreaCode,
         TopWareLevelid,
         AreaCategoryid,
         Status,
         decode(kindtype,'�ֿ�',1,'����',2,'��λ',3),
         'admin',
         sysdate
    from (select (select id
                    from dcs.warehouse
                   where warehouse.code = tAgentAreaLocationTemp.Warecode
                     and warehouse.storagecompanyid =
                         (select id
                            from dcs.agency
                           where agency.code =
                                 tAgentAreaLocationTemp.Agentcode)) as warehouseid,
                 '' as parentid,
                 AreaCode, --������λ���
                 '' as TopWareLevelid, --�������id
                null as AreaCategoryid,
                 1 as Status,
                 '�ֿ�' as kindtype
          
            from tAgentAreaLocationTemp
           where tAgentAreaLocationTemp.Arealocationtype = '�ֿ�');
--------------------------------------------------------------------------

--2.����
insert into dcs.Warehousearea
  (Id,
   WarehouseId,
   ParentId,
   Code, --������λ���
   TopLevelWarehouseAreaId, --�������id
   AreaCategoryId, --������;Id
   Status,
   AreaKind, --������λ����
   CreatorName,
   CreateTime)
  select dcs.s_warehousearea.nextval,
       warehouseid,
         parentid,
         AreaCode,
         TopWareLevelid,
         AreaCategoryid,
         Status,
         decode(kindtype, '�ֿ�', 1, '����', 2, '��λ', 3),
         'admin',
         sysdate
    from (select distinct (select Id
                             from dcs.Warehouse
                            where Warehouse.Code =
                                  tAgentAreaLocationTemp.Warecode
                              and Warehouse.Storagecompanyid =
                                  (select id from dcs.Agency where Agency.Code = tAgentAreaLocationTemp.Agentcode)) as warehouseid,
                          (select Id
                             from dcs.Warehousearea
                            where Warehousearea.Warehouseid =
                                  (select Id
                                     from dcs.warehouse
                                    where Warehouse.Code =
                                          tAgentAreaLocationTemp.Warecode
                                      and warehouse.storagecompanyid =
                                          (select id
                                             from dcs.agency
                                            where agency.code =
                                                  tAgentAreaLocationTemp.Agentcode))) as parentid,
                          AreaCode, --������λ���
                          null as TopWareLevelid,
                          (select Id
                             from dcs.WarehouseAreaCategory
                            where WarehouseAreaCategory.Category = 1) as AreaCategoryid,
                          1 as Status,
                          '����' as kindtype
            from tAgentAreaLocationTemp
           where tAgentAreaLocationTemp.Arealocationtype = '��λ');
---------------------------------------------------------------------------
--3.��λ
insert into dcs.Warehousearea
  (Id,
   WarehouseId,
   ParentId,
   Code, --������λ���
   TopLevelWarehouseAreaId, --�������id
   AreaCategoryId, --������;Id
   Status,
   AreaKind, --������λ����
   CreatorName,
   CreateTime)
select dcs.s_Warehousearea.nextval,
       warehouseid,
       parentid,
       Locationcode,
       TopWareLevelid,
       AreaCategoryid,
       status,
       decode(kindtype, '�ֿ�', 1, '����', 2, '��λ', 3),
       'admin',
       sysdate
  from (select (select Id
                  from dcs.Warehouse
                 where Warehouse.Code = tAgentAreaLocationTemp.Warecode
                   and Warehouse.Storagecompanyid =
                       (select Id
                          from dcs.Agency
                         where Agency.Code = tAgentAreaLocationTemp.Agentcode)) as warehouseid,
               (select Id
                  from dcs.Warehousearea
                 where Warehousearea.Code = tAgentAreaLocationTemp.Areacode
                   and Warehousearea.Warehouseid =
                       (select Id
                          from dcs.warehouse
                         where Warehouse.Code =
                               tAgentAreaLocationTemp.Warecode
                           and warehouse.storagecompanyid =
                               (select id
                                  from dcs.agency
                                 where agency.code =
                                       tAgentAreaLocationTemp.Agentcode))) as parentid,
               tAgentAreaLocationTemp.Locationcode,   --��λ���
               (select Id
                  from dcs.Warehousearea
                 where Warehousearea.Code = tAgentAreaLocationTemp.Areacode
                   and Warehousearea.Warehouseid =
                       (select Id
                          from dcs.warehouse
                         where Warehouse.Code =
                               tAgentAreaLocationTemp.Warecode
                           and warehouse.storagecompanyid =
                               (select id
                                  from dcs.agency
                                 where agency.code =
                                       tAgentAreaLocationTemp.Agentcode))) as TopWareLevelid,
               
               (select Id
                  from dcs.WarehouseAreaCategory
                 where WarehouseAreaCategory.Category = 1) as AreaCategoryid,
               1 as status,
               '��λ' as kindtype
          from tAgentAreaLocationTemp
         where tAgentAreaLocationTemp.Arealocationtype = '��λ');
