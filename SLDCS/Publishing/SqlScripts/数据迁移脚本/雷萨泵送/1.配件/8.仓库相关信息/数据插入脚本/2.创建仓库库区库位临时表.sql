--仓库库区库位临时表

--创建分公司仓库库区库位临时表
create table tFilialeAreaLocationTemp
(
       WareCode varchar2(50),
       WareName varchar2(100),
       FilialeCode varchar2(50),    --仓储企业编号
       FilialeName varchar2(100),    --仓储企业名称
       typeName varchar2(50),       --仓储企业类型-分公司
       AreaCode varchar2(50),     --库区编号
       LocationCode varchar2(50),  --库位编号
       AreaLocationType varchar2(50),    --库区库位类型
       AreaCategory varchar2(50) --库区用途
        
);



--创建代理库仓库库区库位临时表
create table tAgentAreaLocationTemp
(
       WareCode varchar2(50),
       WareName varchar2(100),
       AgentCode varchar2(50),   --仓储企业编号
       AgentName varchar2(100),  --仓储企业名称
       typeName varchar2(50),    --仓储企业类型 - 代理库
       AreaCode varchar2(50),   --库区编号
       LocationCode varchar2(50),  --库位编号
       AreaLocationType  varchar2(50),  --库区库位类型
       AreaCategory varchar2(50)   --库区用途
);
