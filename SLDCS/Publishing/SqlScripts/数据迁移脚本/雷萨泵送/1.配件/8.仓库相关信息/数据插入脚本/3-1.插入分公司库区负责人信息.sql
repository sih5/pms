--插入分公司库区负责人信息
insert into dcs.Warehouseareamanager
  (Id, WarehouseAreaId, ManagerId, CreatorName, CreateTime)

  select dcs.s_warehouseareamanager.nextval,
         (select id
            from dcs.Warehousearea
           where Warehousearea.Code = tAreaPersonTemp.Warecode
             and Warehousearea.Areakind = 1),
         (select id
            from security.personnel
           where security.personnel.loginid = tAreaPersonTemp.Areapersoncode
           and security.personnel.enterpriseid=enterprise.id),
         'admin',
         sysdate
    from tAreaPersonTemp
    left join security.enterprise on enterprise.code=tAreaPersonTemp.Entcode
