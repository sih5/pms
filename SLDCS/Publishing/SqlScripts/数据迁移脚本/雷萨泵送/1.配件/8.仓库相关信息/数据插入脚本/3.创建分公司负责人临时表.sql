--创建分公司库区负责人

create table tAreaPersonTemp
(
      Warecode varchar2(50),
      wareName varchar2(100),
      Entcode varchar2(50),     --仓储企业编号
      EntName varchar2(100),    --仓储企业名称
      EntType varchar2(50),   --仓储企业类型
      AreaCode  varchar2(50),   --库区编号
      AreaPersonCode varchar2(50),    --库段负责人编号
      AreaPersonName varchar2(100)   --库段负责人名称
)
