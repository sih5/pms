
--注：插入人员信息前，人员必须存在 人员表中
--插入 仓库人员 信息

insert into dcs.Warehouseoperator
  (Id, WarehouseId, OperatorId, CreatorName, CreateTime)
  select dcs.s_warehouseoperator.nextval,
         (select Id
            from dcs.warehouse
           where Warehouse.Code = Warehouse_linkerTemp.WareCode),    --仓库id
         (select Id from security.personnel
           where personnel.loginid = Warehouse_linkerTemp.WareOperCode
             and personnel.EnterpriseId =enterprise.id
                  ),   --仓库人员Id
         'admin',
         sysdate
 from Warehouse_linkerTemp
 left join security.enterprise on enterprise.code=Warehouse_linkerTemp.Entcode
