--创建分公司仓库人员信息 临时表Warehouse_linkerTemp

create table Warehouse_linkerTemp
(
       WareCode varchar2(50),
       WareName varchar2(100),
       EntCode varchar2(50),    --仓储企业编号
       EntName varchar2(100),    --仓储企业名称
       EntType varchar2(50),    --仓储企业类型 -分公司
       WareOperCode varchar2(50),    --仓库人员编号
       WareOperName varchar2(100)  --仓库人员名称  
       
);
