--插入（分公司）配件库存信息
insert into dcs.PartsStock
  (Id,
   WarehouseId,
   StorageCompanyId,
   StorageCompanyType,
   BranchId,
   WarehouseAreaId,    ---库位Id
   WarehouseAreaCategoryId,  --库区用途Id
   PartId,
   Quantity,
   CreatorName,
   CreateTime)
select
    dcs.s_partsstock.nextval,
    (select Id from dcs.warehouse where Warehouse.Code = tFilialeStoreTemp.Warecode),   --仓库id
    --(select Id from dcs.branch where Branch.Code = tFilialeStoreTemp.Filialecode),  --仓储企业id
    (select Id from dcs.branch where Branch.Code = 'SHSB'),
    decode(tFilialeStoreTemp.Typename,'分公司',1,'服务站',2,'代理库',3),
    --(select Id from dcs.branch where branch.code = tFilialeStoreTemp.Filialecode),  --营销分公司id
    (select Id from dcs.branch where Branch.Code = 'SHSB'),
    (select Id from dcs.warehousearea where Warehousearea.Code = tFilialeStoreTemp.Locationcode
      and Warehousearea.Warehouseid = (select id from dcs.warehouse where warehouse.code = tFilialeStoreTemp.Warecode)
      and warehousearea.areakind = 3), 
      
    (select AreaCategoryId from dcs.warehousearea where Warehousearea.Code = tFilialeStoreTemp.Locationcode
      and Warehousearea.Warehouseid = (select id from dcs.warehouse where warehouse.code = tFilialeStoreTemp.Warecode)
      and warehousearea.areakind = 3),   --库区用途Id 
      
    (select Id from dcs.SparePart where SparePart.Code = tFilialeStoreTemp.Materialcode),   --配件id
    Qty,     --配件数量
    'admin',
    sysdate
  from tFilialeStoreTemp
  
/*  where exists(select * from dcs.SparePart where SparePart.Code = tFilialeStoreTemp.Materialcode)
  and exists(select * from dcs.warehousearea where warehousearea.code = tFilialeStoreTemp.Locationcode)*/;
