-- 插入（代理库）配件库存信息

insert into dcs.PartsStock
  (Id,
   WarehouseId,
   StorageCompanyId,
   StorageCompanyType,
   BranchId,
   WarehouseAreaId, ---库位Id
   WarehouseAreaCategoryId,  --库区用途Id
   PartId,
   Quantity,
   CreatorName,
   CreateTime)
   
select 
        dcs.s_partsstock.nextval,
        warehouseid,
        storcomid,
        typename,
        branchid,
        locationid,
        AreaCategoryId,  --库区用途id
        materialid,
        qty,
        'admin',
        sysdate
 from (
     select
         (select id
            from dcs.warehouse
           where Warehouse.Code = tAgentStoreTemp.Warecode
             and Warehouse.Storagecompanyid =
                 (select id
                    from dcs.Agency
                   where Agency.Code = tAgentStoreTemp.Agentcode)) as warehouseid,
         
         (select id from dcs.agency where Agency.Code = tAgentStoreTemp.AgentCode) as storcomid, --仓储企业id
         
         decode(tAgentStoreTemp.Typename, '分公司', 1, '服务站', 2, '代理库', 3) as typename, --仓储企业类型
         
         (select id from dcs.Branch where Branch.Code = 'SHSB') as branchid, --营销分公司id
         
         (select Id
            from dcs.warehousearea
           where warehousearea.parentid =
                 (select Id
                    from warehousearea
                   where warehouseid =
                         (select id
                            from dcs.Warehouse
                           where Warehouse.Code = tAgentStoreTemp.Warecode
                             and warehouse.storagecompanyid =
                                 (select id
                                    from dcs.agency
                                   where Agency.Code = tAgentStoreTemp.Agentcode))
                     and code = tAgentStoreTemp.Areafieldcode
                     and areakind = 2)
             and warehousearea.code = tAgentStoreTemp.Locationcode
             and rownum = 1) as locationid, --库位id
             
             
             
         (select AreaCategoryId
            from dcs.warehousearea
           where warehousearea.parentid =
                 (select Id
                    from warehousearea
                   where warehouseid =
                         (select id
                            from dcs.Warehouse
                           where Warehouse.Code = tAgentStoreTemp.Warecode
                             and warehouse.storagecompanyid =
                                 (select id
                                    from dcs.agency
                                   where Agency.Code = tAgentStoreTemp.Agentcode))
                     and code = tAgentStoreTemp.Areafieldcode
                     and areakind = 2)
             and warehousearea.code = tAgentStoreTemp.Locationcode
             and rownum = 1) as AreaCategoryId,     --库区用途id    
             
         
         (select id
            from dcs.Sparepart
           where Sparepart.Code = tAgentStoreTemp.Materialcode) as materialid, --配件id
         
         tAgentStoreTemp.Qty as qty -- 数量

          from tAgentStoreTemp

         where exists (select *
                  from SparePart
                 where SparePart.Code = tAgentStoreTemp.Materialcode)
           and exists
         (select *
                  from dcs.warehousearea
                 where warehousearea.code = tAgentStoreTemp.Locationcode)
      ) where locationid is not null;
