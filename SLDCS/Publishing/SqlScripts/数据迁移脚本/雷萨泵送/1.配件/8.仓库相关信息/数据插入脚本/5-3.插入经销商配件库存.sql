insert into dcs.DealerPartsStock
  (Id,
   DealerId, --经销商Id
   DealerCode, --经销商编号
   DealerName, --经销商名称
   BranchId, --营销分公司Id
   SalesCategoryId, --销售类型Id
   SalesCategoryName,
   SparePartId, --配件Id
   SparePartCode,
   SparePartName,
   Quantity,
   CreatorName,
   CreateTime)

  select dcs.s_Dealerpartsstock.nextval,
    (select Id
            from dcs.Dealer
           where Dealer.Code = tStaStoreTemp.Stacode),
         (select Code
            from dcs.Dealer
           where Dealer.Code = tStaStoreTemp.Stacode),
         (select Name
            from dcs.Dealer
           where Dealer.Code = tStaStoreTemp.Stacode),
         
         (select id from dcs.Branch where Branch.Code = 'SHSB') as branchid, --营销分公司id
         (select Id
            from dcs.Partssalescategory
           where Partssalescategory.Name = tStaStoreTemp.Brandname), --销售类型Id
         (select Name
            from dcs.Partssalescategory
           where Partssalescategory.Name = tStaStoreTemp.Brandname), --销售类型名称
         
         (select Id
            from dcs.SparePart
           where SparePart.Code = tStaStoreTemp.Materialcode), --配件Id
         (select Code
            from dcs.SparePart
           where SparePart.Code = tStaStoreTemp.Materialcode), --配件编号
         (select Name
            from dcs.SparePart
           where SparePart.Code = tStaStoreTemp.Materialcode), --配件名称
         tStaStoreTemp.Qty,
         'admin',
         sysdate
  
    from dcs.tStaStoreTemp
   where exists (select *
            from SparePart
           where SparePart.Code = tStaStoreTemp.Materialcode)
　　　and exists (select * from dcs.Dealer where Dealer.Code = tStaStoreTemp.Stacode)


