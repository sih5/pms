--创建经销商配件库存临时表 （服务站）

/*create table tStaStoreTemp
(
   StaCode varchar2(50),       --服务站编号
   StaName varchar2(100),      --服务站名称
   BrandName varchar2(50),     --品牌
   MaterialCode varchar2(50),  --配件编号
   MaterialName varchar2(100),  --配件名称
   Qty number(9)                 --数量
);
*/




--创建配件库存临时表
  
   --创建配件库存临时表 （分公司）

create table tFilialeStoreTemp
(
       WareCode varchar2(50),
       WareName varchar2(100),
       FilialeCode varchar2(50),    --仓储企业编号
       FilialeName varchar2(100),   ---仓储企业名称
       TypeName varchar2(50),      --仓储企业类型 
       locationCode varchar2(50),   --库位编号
       AreaFieldCode varchar2(50), --库区编号
       AreaCategory varchar2(50),  --库区用途
       MaterialCode varchar2(50),
       MaterialName varchar2(100),
       Qty number(9)
);


  --创建配件库存临时表（代理库）
 create table tAgentStoreTemp
 (
      WareCode varchar2(50),
      WareName varchar2(100),
      AgentCode varchar2(50),         --仓储企业编号
      AgentName varchar2(100),          ---仓储企业名称
      TypeName varchar2(50),           --仓储企业类型
      LocationCode varchar2(50),        --库位编号
      AreaFieldCode varchar2(50),      --库区编号
      AreaCategory varchar2(50),
      MaterialCode varchar2(50),
      MaterialName varchar2(100),
      Qty number(9)
 );
