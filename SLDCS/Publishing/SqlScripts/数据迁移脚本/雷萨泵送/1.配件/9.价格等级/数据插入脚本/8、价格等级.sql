
insert into CustomerOrderPriceGrade
(
    Id, --Id
    BranchId, --营销分公司Id
    PartsSalesCategoryId, --配件销售类型Id
    CustomerCompanyId, --客户企业Id
    CustomerCompanyCode, --客户企业编号
    CustomerCompanyName, --客户企业名称
    PartsSalesOrderTypeId, --配件销售订单类型Id
    PartsSalesOrderTypeCode, --配件销售订单类型编号
    PartsSalesOrderTypeName, --配件销售订单类型名称
    Coefficient, --系数
    Status, --状态
    Remark, --备注
    CreatorId, --创建人Id
    CreatorName, --创建人
    CreateTime --创建时间
--    ModifierId, --修改人Id
--    ModifierName, --修改人
--    ModifyTime, --修改时间
--    AbandonerId, --作废人Id
--    AbandonerName, --作废人
--    AbandonTime, --作废时间
)
(
select 
    dcs.s_customerorderpricegrade.nextval, --Id
    (select ID from dcs.branch t2 where t2.code='SHSB'),  --营销分公司Id
    (select id from PartsSalesCategory t3 where t1.brandname=t3.name), --配件销售类型Id
    (select id from Enterprise t4 where t1.customercode=t4.code), --客户企业Id
    CustomerCode, --客户企业编号
    CustomerName, --客户企业名称
    (select id from PartsSalesOrderType t5 where t1.saletypecode=t5.code and t5.partssalescategoryid=1), --配件销售订单类型Id
    saletypecode, --配件销售订单类型编号
    SaleTypeName, --配件销售订单类型名称
    crate, --系数
    1, --状态
    '', --备注
    1, --创建人Id
    '系统管理员', --创建人
     sysdate --创建时间
--    ModifierId, --修改人Id
--    ModifierName, --修改人
--    ModifyTime, --修改时间
--    AbandonerId, --作废人Id
--    AbandonerName, --作废人
--    AbandonTime --作废时间
    from CustomerOrderPriceGradewktemp t1

)






--select * from PartsSalesOrderType




--select * from CustomerOrderPriceGrade


--select * from CustomerOrderPriceGradewktemp