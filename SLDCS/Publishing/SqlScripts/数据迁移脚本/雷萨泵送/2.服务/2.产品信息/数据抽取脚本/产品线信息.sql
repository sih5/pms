select 'SHSB' as 分公司编号,
       '商混设备销售分公司' as 分公司名称,
       tbrands.name as 品牌,
       tproductlines.code as 产品线编号,
       tproductlines.name as 产品线名称
  from ft.tproductlines
 inner join ft.tbrands
    on tbrands.objid = tproductlines.brandid
 where tbrands.name = '商混设备'
    or tbrands.name = '欧曼';
