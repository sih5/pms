select tfiliales.code         as filialecode,
       tfiliales.name         as filialename,
       tbrands.name           as brandname,
       tproductlines.code     as productlinecode,
       tproductlines.name     as productlinename,
       tbrandin.mainbrandid,
       tbrandin.mainbrandcode,
       tbrandin.mainbrandname,
       tbrandin.subbrandcode,
       tbrandin.subbrandname,
       tbrandin.platformcode,
       tbrandin.platformname,
       tbrandin.linecode,
       tbrandin.linename
  from ft.tbrandin
 inner join ft.tbrands
    on tbrands.objid = tbrandin.ssbrandid
 inner join ft.tproductlines
    on tproductlines.objid = tbrandin.ssproductlineid
 inner join ft.tfiliales
    on tfiliales.brandid = tbrands.objid
 where (tbrands.name = 'ŷ��'
    or tbrands.name = '�̻��豸')
    and tfiliales.isvalid>0
   
