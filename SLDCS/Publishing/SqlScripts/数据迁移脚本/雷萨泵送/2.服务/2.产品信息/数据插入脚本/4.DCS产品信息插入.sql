insert into Product
  (Id,
   Code,
   ProductCategoryCode,
   brandid,
   BrandCode,
   BrandName,
   SubBrandCode,
   SubBrandName,
   TerraceCode,
   TerraceName,
   ProductLine,
   ProductName,
   OldInternalCode,
   AnnoucementNumber,
   TonnageCode,
   EngineTypeCode,
   EngineManufacturerCode,
   GearSerialTypeCode,
   GearSerialManufacturerCode,
   RearAxleTypeCode,
   RearAxleManufacturerCode,
   TireTypeCode,
   TireFormCode,
   BrakeModeCode,
   DriveModeCode,
   FrameConnetCode,
   VehicleSize,
   AxleDistanceCode,
   EmissionStandardCode,
   ProductFunctionCode,
   ProductLevelCode,
   VehicleTypeCode,
   ColorDescribe,
   SeatingCapacity,
   Weight,
   Status,
   CreatorName,
   CreateTime)
  select dcs.s_Product.nextval as Id,
         ProductCode,
         ProductName,
         MainBrandCode||SubBrandCode||PlatFormCode||PKind,
         MainBrandCode,
         MainBrandname,
         SubBrandCode,
         SubBrandName,
         PlatFormCode,
         PlatFormName,
         PKind,
         LineName,
         InnerCode,
         BulletinCode,
         Load,
         EngineType,
         EngineFactoryCode,
         GearType,
         GearFactoryCode,
         BrideType,
         BrideFactoryCode,
         TyreType,
         TyreFactoryCode,
         brakeType,
         DriveType,
         FrameType,
         FSize,
         WHEELBASE,
         emissionType,
         PFunctionType,
         Pstand,
         Vtype,
         Color,
         SEATCOUNTER,
         WEIGHT,
         1 as Status,
         'Admin' as CreatorName,
         sysdate as CreateTime
    from TSTDPRODUCTIN_Temp_OM t1;


--服务产品线与产品明细关系

Insert Into ServProdLineProductDetail
  (Id, ServiceProductLineId, ProductId)
  select dcs.s_Servprodlineproductdetail.nextval as Id,
         (select id
            from ServiceProductLine
           where ServiceProductLine.name =
                 TSTDPRODUCTIN_Temp_OM.SPRODUCTLINENAME) as ServiceProductLineId,
         (select id
            from Product
           where Product.Code = TSTDPRODUCTIN_Temp_OM.Productcode) as ProductId
    from TSTDPRODUCTIN_Temp_OM
    where exists (select id
            from Product
           where Product.Code = TSTDPRODUCTIN_Temp_OM.Productcode);



