create table tbrandin_temp_om
(filialecode varchar2(100),
 filialename varchar2(100),
 brandname varchar2(100),
 productlinecode varchar2(100),
 productlinename varchar2(100),
 mainbrandid   varchar2(100) ,
 mainbrandcode   varchar2(30) ,
 mainbrandname   varchar2(100) ,
 subbrandcode    varchar2(30) ,
 subbrandname    varchar2(100) ,
 platformcode    varchar2(30) ,
 platformname    varchar2(100) ,
 linecode        varchar2(50) ,
 linename        varchar2(100) )
 
