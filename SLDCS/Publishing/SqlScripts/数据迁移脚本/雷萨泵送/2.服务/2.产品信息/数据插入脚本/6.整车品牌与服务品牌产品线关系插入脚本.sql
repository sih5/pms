--服务产品线与产品关系
Insert into ServiceProdLineProduct
  (Id,
   BrandId,
   VehicleBrandCode,  --1
   VehicleBrandName,
   SubBrandCode,      --2
   SubBrandName,
   TerraceCode,       --3
   TerraceName,
   VehicleProductLine,--4
   VehicleProductName,
   ServiceProductLineId,
   ServiceProductLineCode,
   ServiceProductLineName,
   PartsSalesCategoryId,
   SalesCategoryCode,
   SalesCategoryName)
  select dcs.S_ServiceProdLineProduct.nextval as Id,
         Vehiclebrandcode || subbrandcode || terracecode || Vehicleproductline,
         VehicleBrandCode,  --1
         VehicleBrandName,
         SubBrandCode, --2
         SubBrandName,
         TerraceCode,  --3
         TerraceName,
         VehicleProductLine, --4
         VehicleProductName,
         ServiceProductLineId,
         ServiceProductLineCode,
         ServiceProductLineName,
         PartsSalesCategoryId,
         SalesCategoryCode,
         SalesCategoryName
    from (select distinct mainbrandid as BrandId,
                          mainbrandcode as VehicleBrandCode,   --1
                          mainbrandname as VehicleBrandName,
                          SubBrandCode,  --2
                          SubBrandName,
                          platformcode as TerraceCode,  --3
                          platformname as TerraceName,
                          linecode as VehicleProductLine,  --4
                          linename as VehicleProductName,
                          (select id
                             from ServiceProductLine
                            where ServiceProductLine.Code =
                                  temp.productlinecode
                              and ServiceProductLine.name =
                                  temp.productlinename) as ServiceProductLineId,
                          productlinecode as ServiceProductLineCode,
                          productlinename as ServiceProductLineName,
                          (select id
                             from dcs.partssalescategory
                            where partssalescategory.name = temp.brandname) as PartsSalesCategoryId,
                          (select code
                             from dcs.partssalescategory
                            where partssalescategory.name = temp.brandname) as SalesCategoryCode,
                          brandname as SalesCategoryName
            from tbrandin_temp_om temp) M;

--分公司与整车品牌关系
Insert Into BranchVehicleBrand
  (Id, BranchId, BrandCode, BrandName)
  select dcs.s_BranchVehicleBrand.nextval as Id,
         BranchId,
         BrandCode,
         BrandName
    from (select distinct (select id
                             from dcs.branch
                            where branch.code = temp.filialecode
                              and branch.name = temp.filialename) as BranchId,
                          temp.mainbrandcode as BrandCode,
                          temp.mainbrandname as BrandName
            from dcs.tbrandin_temp_om temp);
