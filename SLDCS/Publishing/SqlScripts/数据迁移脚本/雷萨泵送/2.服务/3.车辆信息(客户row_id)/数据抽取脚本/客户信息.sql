select 客户姓名,
       性别,
       case
         when namelength <= 6 then
          '个人客户'
         else
          '单位客户'
       end as 客户类型,
       年龄,
       证件类型,
       证件号码,
       区域,
       省份,
       城市,
       县,
       邮政编码,
       家庭电话,
       单位电话,
       手机号码,
       电子邮件,
       职务,
       文化程度,
       客户爱好,
       详细地址,
       客户id
  from (select tconsumers.customer_name as 客户姓名,
               lengthb(tconsumers.customer_name) as namelength,
               decode(tconsumers.Sex, 0, '女', 1, '男', '未填') as 性别,
               '' as 客户类型,
               tconsumers.age as 年龄,
               '居民身份证' as 证件类型,
               tconsumers.ID_card as 证件号码,
               (select areaname
                  from ft.thzone
                 where thzone.numcode = tconsumers.province) as 区域,
               (select provname
                  from ft.thzone
                 where thzone.numcode = tconsumers.province) as 省份,
               (select cityname
                  from ft.thzone
                 where thzone.numcode = tconsumers.city) as 城市,
               (select townname
                  from ft.thzone
                 where thzone.numcode = tconsumers.county) as 县,
               tconsumers.post as 邮政编码,
               tconsumers.home_phone as 家庭电话,
               tconsumers.office_phone as 单位电话,
               tconsumers.mobile_phone as 手机号码,
               tconsumers.email as 电子邮件,
               tconsumers.duty as 职务,
               (select paramvalue
                  from ft.tparams
                 where paramtype = '文化程度'
                   and paramcode = tconsumers.education) as 文化程度,
               tconsumers.liking as 客户爱好,
               tconsumers.address as 详细地址,
               tconsumers.row_id as 客户id
          from ft.tconsumers
         inner join ft.tpurchaseinfos
            on tpurchaseinfos.customer_id = tconsumers.row_id
         inner join ft.tstdproductin
            on tstdproductin.productcode = tpurchaseinfos.product_whole_code
         inner join ft.tbrandin
            on tbrandin.mainbrandid = tstdproductin.mainbrandid
         inner join ft.tbrands
            on tbrands.objid = tbrandin.ssbrandid
         where (tbrands.name = '商混设备' or tbrands.name = '欧曼')
           and tpurchaseinfos.isvalid > 0
           and tconsumers.isvalid > 0);

--大数据量数据抽取脚本
create table TCONSUMERS_Temp_OMM
as
  select distinct CUSTOMER_NAME,
                  SEX,
                  case
                    when namelength <= 6 then
                     '个人客户'
                    else
                     '单位客户'
                  end as Type,
                  AGE,
                  CardType,
                  ID_CARD,
                  areaname,
                  provname,
                  cityname,
                  townname,
                  post,
                  home_phone,
                  office_phone,
                  mobile_phone,
                  email,
                  duty,
                  education,
                  liking,
                  address,
                  row_id
    from (select tconsumers.customer_name as CUSTOMER_NAME,
                 decode(tconsumers.Sex, 0, '女', 1, '男', '未填') as SEX,
                 lengthb(tconsumers.customer_name) as namelength,
                 tconsumers.age as AGE,
                 '居民身份证' as CardType,
                 tconsumers.ID_card as ID_CARD,
                 (select areaname
                    from ft.thzone
                   where thzone.numcode = tconsumers.province) as areaname,
                 (select provname
                    from ft.thzone
                   where thzone.numcode = tconsumers.province) as provname,
                 (select cityname
                    from ft.thzone
                   where thzone.numcode = tconsumers.city) as cityname,
                 (select townname
                    from ft.thzone
                   where thzone.numcode = tconsumers.county) as townname,
                 tconsumers.post,
                 tconsumers.home_phone,
                 tconsumers.office_phone,
                 tconsumers.mobile_phone,
                 tconsumers.email,
                 tconsumers.duty,
                 (select paramvalue
                    from ft.tparams
                   where paramtype = '文化程度'
                     and paramcode = tconsumers.education) as education,
                 tconsumers.liking as liking,
                 tconsumers.address as address,
                 tconsumers.row_id
            from ft.tconsumers
           inner join ft.tpurchaseinfos
              on tpurchaseinfos.customer_id = tconsumers.row_id
           inner join ft.tstdproductin
              on tstdproductin.productcode =
                 tpurchaseinfos.product_whole_code
           inner join ft.tbrandin
              on tbrandin.mainbrandid = tstdproductin.mainbrandid
           inner join ft.tbrands
              on tbrands.objid = tbrandin.ssbrandid
           where (tbrands.name = '商混设备' or tbrands.name = '欧曼')
             and tpurchaseinfos.isvalid > 0
             and tconsumers.isvalid > 0);
------------
create index TCONSUMERS_TEMP_OMM_ROWID on TCONSUMERS_TEMP_OMM (ROW_ID);

delete TCONSUMERS_Temp_OMM a
    where rowid != (select max(rowid)
                      from TCONSUMERS_Temp_OMM b
                     where a.row_id = b.row_id)             
