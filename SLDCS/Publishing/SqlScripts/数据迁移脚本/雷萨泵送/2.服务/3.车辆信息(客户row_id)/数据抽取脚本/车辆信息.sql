select tpurchaseinfos.VINCODE as VIN码,
       tpurchaseinfos.Leave_Factory_Code as 出厂编号,
       tpurchaseinfos.Auto_No as 车牌号,
       tpurchaseinfos.Sales_Flag as 是否售出,
       tpurchaseinfos.Product_Type_Name as 销售车型,
       tpurchaseinfos.Product_Out_Type_Name as 公告号,
       tpurchaseinfos.Engine_Model as 发动机型号,
       tpurchaseinfos.Engine_Code as 发动机序列号,
       tpurchaseinfos.Leave_Factory_Date as 出厂日期,
       tpurchaseinfos.Sales_Date as 销售日期,
       tpurchaseinfos.invoices as 销售发票号,
       tpurchaseinfos.sales_price as 销售价格,
       '' as 车辆种类,
       '' as 车辆类型,
       tpurchaseinfos.CarSeries as 车系,
       tpurchaseinfos.Product_Line_Name as 产品线名称,
       tpurchaseinfos.Cab_Model as 驾驶室型号,
       tpurchaseinfos.FrontAxleNo as 前桥号,
       tpurchaseinfos.IntermediateAxleNo as 中桥号,
       tpurchaseinfos.RearAxleNo as 后桥号,
       tpurchaseinfos.BalanceShaftNo as 平衡轴号,
       tpurchaseinfos.Transmission as 变速箱序列号,       
       tpurchaseinfos.Product_Whole_Code as 产品编号,
       tpurchaseinfos.Customer_Name as 客户名称,    
       (select tconsumers.id_card from ft.tconsumers where tconsumers.row_id=tpurchaseinfos.customer_id ) as 客户身份证号, 
       (select tconsumers.mobile_phone from ft.tconsumers where tconsumers.row_id=tpurchaseinfos.customer_id ) as 客户手机号,
       tpurchaseinfos.Customer_ID as 客户Row_id,
       tpurchaseinfos.Franchiser_Name
  from ft.tpurchaseinfos
 inner join ft.tstdproductin
    on tstdproductin.productcode = tpurchaseinfos.product_whole_code
 inner join ft.tbrandin
    on tbrandin.mainbrandid = tstdproductin.mainbrandid
 inner join ft.tbrands
    on tbrands.objid = tbrandin.ssbrandid
 inner join ft.tproductlines
    on tproductlines.objid = tbrandin.ssproductlineid
 where (tbrands.name = '商混设备' or tbrands.name = '欧曼')
   and tpurchaseinfos.isvalid > 0
   and tproductlines.isvalid > 0;


Create table TPURCHASEINFOS_temp_OM
as
select tpurchaseinfos.VINCODE,
       tpurchaseinfos.Leave_Factory_Code,
       tpurchaseinfos.Auto_No,
       tpurchaseinfos.Sales_Flag,
       tpurchaseinfos.Product_Type_Name,
       tpurchaseinfos.Product_Out_Type_Name,
       tpurchaseinfos.Engine_Model,
       tpurchaseinfos.Engine_Code,
       tpurchaseinfos.Leave_Factory_Date,
       tpurchaseinfos.Sales_Date,
       tpurchaseinfos.invoices,
       tpurchaseinfos.sales_price,
       tpurchaseinfos.Product_Whole_Code,
       tpurchaseinfos.Customer_Name,
       tpurchaseinfos.Customer_ID,
       'null' as VehicleCategoryName,
       'null' as VehicleTypeName,
       tpurchaseinfos.CarSeries,
       tpurchaseinfos.Product_Line_Name,
       tpurchaseinfos.Cab_Model,
       tpurchaseinfos.FrontAxleNo,
       tpurchaseinfos.IntermediateAxleNo,
       tpurchaseinfos.RearAxleNo,
       tpurchaseinfos.BalanceShaftNo,
       tpurchaseinfos.Transmission,
       tpurchaseinfos.Franchiser_Name
  from ft.tpurchaseinfos
 inner join ft.tstdproductin
    on tstdproductin.productcode = tpurchaseinfos.product_whole_code
 inner join ft.tbrandin
    on tbrandin.mainbrandid = tstdproductin.mainbrandid
 inner join ft.tbrands
    on tbrands.objid = tbrandin.ssbrandid
 inner join ft.tproductlines
    on tproductlines.objid = tbrandin.ssproductlineid
 where (tbrands.name = '商混设备' or tbrands.name = '欧曼')
   and tpurchaseinfos.isvalid > 0
   and tproductlines.isvalid > 0;
