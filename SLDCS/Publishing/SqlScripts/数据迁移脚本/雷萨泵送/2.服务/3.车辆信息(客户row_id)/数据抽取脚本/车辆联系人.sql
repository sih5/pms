
Create table tpurchaselinkers_Temp_OM
as
select tpurchaseinfos.VINCODE,
       tpurchaseinfos.Auto_No,
       tpurchaselinkers.name,
       (select paramvalue
          from ft.tparams
         where paramtype = '联系人类型'
           and paramcode = tpurchaselinkers.type) as type,
       tpurchaselinkers.custmobile,
       tpurchaselinkers.address,
       tpurchaseinfos.Customer_ID
  from ft.tpurchaselinkers
 inner join ft.tpurchaseinfos
    on tpurchaseinfos.objid = tpurchaselinkers.parentid
 inner join ft.tstdproductin
    on tstdproductin.productcode = tpurchaseinfos.product_whole_code
 inner join ft.tbrandin
    on tbrandin.mainbrandid = tstdproductin.mainbrandid
 inner join ft.tbrands
    on tbrands.objid = tbrandin.ssbrandid
 inner join ft.tproductlines
    on tproductlines.objid = tbrandin.ssproductlineid
 where (tbrands.name = '商混设备' or tbrands.name = '欧曼')
   and tpurchaseinfos.isvalid > 0;
