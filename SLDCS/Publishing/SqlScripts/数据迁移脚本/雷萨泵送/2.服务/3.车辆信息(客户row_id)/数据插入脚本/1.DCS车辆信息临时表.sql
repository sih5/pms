
create table TPURCHASEINFOS_temp_OM
(
    VINCODE                   VARCHAR2(40),
    LEAVE_FACTORY_CODE        VARCHAR2(16),
    AUTO_NO                   VARCHAR2(32),
    SALES_FLAG                VARCHAR2(32),
    PRODUCT_TYPE_NAME         VARCHAR2(100),
    PRODUCT_OUT_TYPE_NAME     VARCHAR2(100),
    ENGINE_MODEL              VARCHAR2(100),
    ENGINE_CODE               VARCHAR2(32),
    LEAVE_FACTORY_DATE        DATE,
    SALES_DATE                DATE,
    INVOICES                  VARCHAR2(32),
    SALES_PRICE               NUMBER(20,6),  
    PRODUCT_WHOLE_CODE        VARCHAR2(50),
    CUSTOMER_NAME             VARCHAR2(100),
    CUSTOMER_ID               VARCHAR2(40),
    VehicleCategoryName       VARCHAR2(40),
    VehicleTypeName           VARCHAR2(40),
    CARSERIES                 VARCHAR2(40),
    PRODUCT_LINE_NAME         VARCHAR2(100),
    CAB_MODEL                 VARCHAR2(32),
    FRONTAXLENO               VARCHAR2(40),
    INTERMEDIATEAXLENO        VARCHAR2(40),
    REARAXLENO                VARCHAR2(40),
    BALANCESHAFTNO            VARCHAR2(40),
    TRANSMISSION              VARCHAR2(40),
    Franchiser_Name           VARCHAR2(200)
)

