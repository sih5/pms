create table TCONSUMERS_Temp_OMM
(
  CUSTOMER_NAME        VARCHAR2(100),
  SEX                  VARCHAR2(32),
  AGE                  NUMBER(4),
  Type                 VARCHAR2(32),
  CardType             VARCHAR2(32),
  ID_CARD              VARCHAR2(38),  
  areaname             VARCHAR2(32),
  provname             VARCHAR2(32),
  cityname             VARCHAR2(32),
  townname             VARCHAR2(32),
  post                 VARCHAR2(8), 
  home_phone           VARCHAR2(64),
  office_phone         VARCHAR2(64),
  mobile_phone         VARCHAR2(64),
  email                VARCHAR2(128),
  duty                 VARCHAR2(32),
  education            VARCHAR2(256),
  liking               VARCHAR2(256),   
  address              VARCHAR2(300),
  row_id               VARCHAR2(38)   
)
