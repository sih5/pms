--基础客户
Insert Into Customer
(
Id,
Name,
Gender,
CustomerType,
Age,
RegionId,
RegionName,
ProvinceName,
CityName,
IfComplain,
CountyName,
Address,
PostCode,
CellPhoneNumber,
HomePhoneNumber,
OfficePhoneNumber,
IdDocumentType,
IdDocumentNumber,
Email,
JobPosition,
EducationLevel,
Status,
CreatorName,
CreateTime,
Remark
)
  select dcs.s_Customer.nextval as Id,
         CUSTOMER_NAME as Name,
         decode(SEX, '男', 1, '女', 2, '未填', 0) as Gender,
         decode(Type, '个人客户', 1, '单位客户', 2) as CustomerType,
         Age,
         (select id from Region where Region.Name = temp.areaname) as RegionId,
         temp.areaname RegionName,
         temp.provname as ProvinceName,
         temp.cityname as CityName,
         0 as IfComplain,
         temp.townname CountyName,
         Address,
         substrb(temp.post,1,6) as PostCode,
         temp.mobile_phone as CellPhoneNumber,
         substrb(temp.home_phone,1,50) as HomePhoneNumber,
         substrb(temp.office_phone,1,50) as OfficePhoneNumber,
         1 as IdDocumentType,
         temp.id_card as IdDocumentNumber,
         Email,
         temp.duty as JobPosition,
         decode(temp.education,
                '其他',
                9,
                '小学',
                8,
                '中学',
                7,
                '本科',
                3,
                '硕士',
                2,
                '博士',
                1) as EducationLevel,
         1 as Status,
         'admin' as CreatorName,
         sysdate as CreateTime,
         temp.row_id as Remark
    from TCONSUMERS_Temp_OMM temp;

--新增备注临时索引
create index CUSTOMER_REMARK on CUSTOMER (remark);
--create index TCONSUMERS_TEMP_OMM_rowid on TCONSUMERS_TEMP_OMM (row_id);

--保有客户
Insert Into RetainedCustomer
  (Id,
   CustomerId,
   Hobby,
   AcquisitorName,
   AcquisitorPhone,
   CreatorName,
   CreateTime,
   Status,
   Remark)
  select dcs.s_Retainedcustomer.nextval as Id,
         --(select id from Customer where Customer.Remark = temp.row_id and rownum=1) as CustomerId,
         1 as CustomerId,
         temp.liking as Hobby,
         temp.customer_name as AcquisitorName,
         temp.mobile_phone as AcquisitorPhone,
         'admin' as CreatorName,
         sysdate as CreateTime,
         1 as Status,
         temp.row_id as Remark
    from TCONSUMERS_Temp_OMM temp;

update RetainedCustomer set customerid =   (select id from Customer where Customer.Remark = RetainedCustomer.Remark ) ; 

create index RetainedCustomer_REMARK on RetainedCustomer (remark);
create index TPURCHASEINFOS_TEMP_OM_CUSTID on TPURCHASEINFOS_TEMP_OM (customer_id);
create index TPURCHASEINFOS_TEMP_OM_vincode on TPURCHASEINFOS_TEMP_OM (vincode);
create index VEHICLEINFORMATION_VIN on VEHICLEINFORMATION (vin);
alter table RETAINEDCUSTOMERVEHICLELIST add Remark varchar2(40);
--保有客户车辆清单

Insert Into RetainedCustomerVehicleList
  (Id, RetainedCustomerId, VehicleId, Status,Remark)
  select dcs.s_Retainedcustomervehiclelist.nextval as Id,
         RetainedCustomer.id as RetainedCustomerId,
         VehicleInformation.id as VehicleId,
         1 as Status,
         temp.customer_id as Remark
    from TPURCHASEINFOS_temp_OM temp
    inner join RetainedCustomer on RetainedCustomer.Remark = temp.customer_id
    inner join VehicleInformation on VehicleInformation.Vin = temp.vincode
 

    
