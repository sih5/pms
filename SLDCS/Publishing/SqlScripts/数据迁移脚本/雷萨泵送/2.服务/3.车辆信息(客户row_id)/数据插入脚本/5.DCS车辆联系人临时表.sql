create table TPURCHASELINKERS_TEMP_OM
(
  VINCODE     VARCHAR2(40),
  AUTO_NO     VARCHAR2(32),
  NAME        VARCHAR2(50) not null,
  TYPE        VARCHAR2(40),
  CUSTMOBILE  VARCHAR2(64),
  ADDRESS     VARCHAR2(100),
  CUSTOMER_ID VARCHAR2(40)
)
