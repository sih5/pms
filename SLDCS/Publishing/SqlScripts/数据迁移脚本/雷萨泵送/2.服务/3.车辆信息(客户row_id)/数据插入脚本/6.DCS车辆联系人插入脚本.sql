create index TPURCHASELINKERS_TEMP_OM_vcode on TPURCHASELINKERS_TEMP_OM (vincode);
create index TPURCHASELINKERS_TEMP_OM_CTID on TPURCHASELINKERS_TEMP_OM (customer_id);

Insert Into VehicleLinkman
(Id,
RetainedCustomerId,
VehicleInformationId,
LinkmanType,
Name,
Gender,
CellPhoneNumber,
Address,
Status,
CreatorName,
CreateTime
)
select dcs.s_Vehiclelinkman.nextval as Id,
       RetainedCustomer.id as RetainedCustomerId,
       VehicleInformation.id as VehicleInformationId,
       decode(temp.type, '车主', 1, '一般联系人', 3, '经销商（商品车）', 4) as LinkmanType,
       temp.name as Name,
       3 as Gender,
       temp.custmobile as CellPhoneNumber,
       temp.Address,
       1 as Status,
       'admin' as CreatorName,
       sysdate as CreateTime
  from TPURCHASELINKERS_TEMP_OM temp
 inner join RetainedCustomer
    on RetainedCustomer.Remark = temp.customer_id
 inner join VehicleInformation
    on VehicleInformation.vin = temp.vincode
