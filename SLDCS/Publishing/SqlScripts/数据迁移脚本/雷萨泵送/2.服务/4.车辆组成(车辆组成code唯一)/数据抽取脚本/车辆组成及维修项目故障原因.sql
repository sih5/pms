--导数据脚本

--维修项目车辆组成
select rownum,      
       (select code from ft.TPARTCOMPOSINGS t2 where t2.objid=t1.parentid) UPcode,
       (select name from ft.TPARTCOMPOSINGS t2 where t2.objid=t1.parentid) UPname,
       (select decode(sparetype,1,'总成',2,'零部件') from ft.TPARTCOMPOSINGS t2 where t2.objid=t1.parentid) UPsparetype,
       (select compcode from ft.TPARTCOMPOSINGS t2 where t2.objid=t1.parentid) UPcompcode,
       code,
       name,
       decode(sparetype,1,'总成',2,'零部件') sparetype,
       compcode
        from ft.TPARTCOMPOSINGS t1
where t1.isvalid=1
and t1.showindex=0
;

--故障原因车辆组成
select rownum,
       (select code from ft.TPARTCOMPOSINGS t2 where t2.objid=t1.parentid) UPcode,
       (select name from ft.TPARTCOMPOSINGS t2 where t2.objid=t1.parentid) UPname,
       (select decode(sparetype,1,'总成',2,'零部件') from ft.TPARTCOMPOSINGS t2 where t2.objid=t1.parentid) UPsparetype,
       (select compcode from ft.TPARTCOMPOSINGS t2 where t2.objid=t1.parentid) UPcompcode,
       code,
       name,
       decode(sparetype,1,'总成',2,'零部件') sparetype,
       compcode
        from ft.TPARTCOMPOSINGS t1
where t1.isvalid=1
and t1.showindex=1;



--故障原因
select rownum,t3.* from 
(select  distinct
        t1.code ccode,
        t1.name cname,
        t2.Code,
        t2.name,
        t2.compcode,
        t2.quickcode
       from ft.tfaultreasons t2
left join ft.TPARTCOMPOSINGS t1 on t1.COMPCODE=t2.compcode  
where t2.isvalid=1
and t1.isvalid=1
and t1.sparetype=2) t3   

--维修项目
select rownum, t3.*
  from (select distinct t1.code     ccode,
                        t1.name     cname,
                        t2.compcode,
                        t2.CODE,
                        t2.NAME,
                        
                        '' a1,
                        '' a2,
                        --decode(t2.repairpurview,0,'一类',1,'二类',2,'三类',3,'四类',4,'五类',5,'六类',6,'七类',7,'八类',8,'九类',9),
                        
                        decode(t2.maintenancediff,
                               1,
                               'A',
                               2,
                               'B',
                               3,
                               'C',
                               4,
                               'D',
                               5,
                               'R'),
                        t2.QUICKCODE,
                        (select name
                           from ft.tproductlines
                          where tproductlines.objid = t2.productlineid) as productlinename,
                        t2.WORKHOUR
          from ft.tworkitemlinks t2
         inner join ft.TPARTCOMPOSINGS t1
            on t1.COMPCODE = t2.compcode
         where t1.isvalid = 1
           and t2.isvalid = 1
           and t1.sparetype = 2) t3


