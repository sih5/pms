alter table MALFUNCTIONCATEGORY add compcode VARCHAR2(50);
alter table REPAIRITEMCATEGORY add compcode VARCHAR2(50);
--新增根节点
insert into MalfunctionCategory
  (ID,
   PARENTID,
   ROOTGROUPID,
   CODE,
   NAME,
   LAYERNODETYPEID,
   STATUS,
   CREATORNAME,
   CREATETIME)
values
  (s_MalfunctionCategory.Nextval,
   null,
   s_MalfunctionCategory.Nextval,
   'GJD',
   '根节点',
   4,
   1,
   'admin',
   sysdate);

--新增总成类型的节点
--第一层
Insert Into MalfunctionCategory
  (Id,
   ParentId,
   RootGroupId,
   Code,
   Name,
   LayerNodeTypeId,
   Status,
   CreatorName,
   CreateTime,
   CompCode)
  select dcs.s_Malfunctioncategory.nextval as Id,
         (select id
            from MalfunctionCategory
           where MalfunctionCategory.name = '根节点') as ParentId,
         (select id
            from MalfunctionCategory
           where MalfunctionCategory.name = '根节点') as RootGroupId,
         nvl(Code,dcs.s_Malfunctioncategory.nextval) as code,
         Name,
         5 as LayerNodeTypeId,
         1 as Status,
         'admin' as CreatorName,
         sysdate as CreateTime,
         CompCode
    from TPARTCOMPOSINGS_Temp_OM temp
   where temp.upname is null and temp.sparetype='总成';

--第二层
Insert Into MalfunctionCategory
  (Id,
   ParentId,
   RootGroupId,
   Code,
   Name,
   LayerNodeTypeId,
   Status,
   CreatorName,
   CreateTime,
   compcode)
  select dcs.s_Malfunctioncategory.nextval as Id,
         (select id
            from MalfunctionCategory
           where MalfunctionCategory.name = temp.upname and MalfunctionCategory.code = temp.upcode and MalfunctionCategory.Compcode=temp.upcompcode and MalfunctionCategory.Layernodetypeid=2 ) as ParentId,
         (select id
            from MalfunctionCategory
           where MalfunctionCategory.name = '根节点') as RootGroupId,
        nvl(code,dcs.s_Malfunctioncategory.nextval ) Code,
         Name,
         5 as LayerNodeTypeId,
         1 as Status,
         'admin' as CreatorName,
         sysdate as CreateTime,
         compcode
    from TPARTCOMPOSINGS_Temp_OM temp
   where temp.upname is not null and temp.sparetype='总成';
   
--第三层
Insert Into MalfunctionCategory
  (Id,
   ParentId,
   RootGroupId,
   Code,
   Name,
   LayerNodeTypeId,
   Status,
   CreatorName,
   CreateTime,
   compcode)
  select dcs.s_Malfunctioncategory.nextval as Id,
         (select id
            from MalfunctionCategory
           where MalfunctionCategory.name = temp.upname and MalfunctionCategory.code = temp.upcode and MalfunctionCategory.Compcode=temp.upcompcode and MalfunctionCategory.Layernodetypeid=2) as ParentId,
         (select id
            from MalfunctionCategory
           where MalfunctionCategory.name = '根节点') as RootGroupId,
         nvl(code,dcs.s_Malfunctioncategory.nextval ) as Code,
         Name,
         6 as LayerNodeTypeId,
         1 as Status,
         'admin' as CreatorName,
         sysdate as CreateTime,
         compcode
    from TPARTCOMPOSINGS_Temp_OM temp
   where temp.upname is not null and temp.sparetype='零部件';

 
--注意:不同品牌树对应的编号/名称/车辆组成可能相同.
--需修改触发器,同步compcode  

--故障现象分类与车辆种类关系
insert into MalfCatAffiVehiCat
(
Id,
VehicleCategoryId,
MalfunctionCategoryId
)
select dcs.s_MalfCatAffiVehiCat.nextval as Id,
       (select id
          from VehicleCategory
         where VehicleCategory.Name = '搅拌车'
           and VehicleCategory.Branchid =
               (select id from dcs.branch where branch.name = '商混设备销售分公司')) as VehicleCategoryId,
       id as MalfunctionCategoryId
  from MalfunctionCategory where MalfunctionCategory.Layernodetypeid=4;

--维修项目分类与车辆种类关系
insert into RepairItemCatAffiVehiCat
  (Id, VehicleCategoryId, RepairItemCategoryId)
  select dcs.s_RepairItemCatAffiVehiCat.nextval as Id,
         (select id
            from VehicleCategory
           where VehicleCategory.Name = '搅拌车'
             and VehicleCategory.Branchid =
                 (select id from dcs.branch where branch.name = '商混设备销售分公司')) as VehicleCategoryId,
         RepairItemCategory.id as RepairItemCategoryId
    from RepairItemCategory where RepairItemCategory.Layernodetypeid=1;
