alter table MALFUNCTION add compcode VARCHAR2(50);
--故障现象
insert into Malfunction
  (Id,
   Code,
   Description,
   MalfunctionCategoryId,
   QuickCode,
   Status,
   CreatorName,
   CreateTime,
   compcode)
  select dcs.s_Malfunction.nextval as Id,
         nvl(Code,'nul') as code,
         nvl(name,'null') as Description,
         nvl((select id
            from dcs.MalfunctionCategory M
           where M.Code = temp.upcode
             and M.Name = temp.upname
             and M.compcode = temp.compcode),9999999) as MalfunctionCategoryId,
         QuickCode,
         1 as Status,
         'admin' as CreatorName,
         sysdate as CreateTime,
         compcode
    from tfaultreasons_temp_om temp;

--故障现象与品牌关系
insert into MalfunctionBrandRelation
  (Id,
   BranchId,
   MalfunctionId,
   MalfunctionCode,
   MalfunctionName,
   PartsSalesCategoryId,
   --SalesCategoryName,
   IsApplication,
   CreatorName,
   CreateTime,
   Status)
  select dcs.s_Malfunctionbrandrelation.nextval as Id,
         (select id from dcs.branch where branch.name='商混设备销售分公司') as BranchId,
         (select id from dcs.Malfunction where Malfunction.Code=temp.code and Malfunction.Description=temp.name and Malfunction.Compcode=temp.compcode and rownum=1) as MalfunctionId,
         temp.code as MalfunctionCode,
         substrb(temp.name,1,200) as MalfunctionName,
         (select id from dcs.partssalescategory where partssalescategory.name='商混设备') as PartsSalesCategoryId,
        -- '商混设备' as SalesCategoryName,
         0 as  IsApplication,
         'admin' CreatorName,
         sysdate CreateTime,
         1 as Statu
    from tfaultreasons_temp_om temp;
 
