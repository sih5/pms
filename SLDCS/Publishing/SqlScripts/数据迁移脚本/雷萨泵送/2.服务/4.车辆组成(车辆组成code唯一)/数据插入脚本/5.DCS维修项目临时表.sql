create table tworkitemlinks_temp_om
(
  upcode varchar2(40),
  upname varchar2(400),
  compcode varchar2(40),
  code varchar2(40),
  name varchar2(400),
  quickcode varchar2(40),
  jobtype  varchar2(40),
  repairqualificationgrade  varchar2(40),
  maintenancediff varchar2(40),
  productlinename varchar2(40),
  workhour varchar2(40)
)
