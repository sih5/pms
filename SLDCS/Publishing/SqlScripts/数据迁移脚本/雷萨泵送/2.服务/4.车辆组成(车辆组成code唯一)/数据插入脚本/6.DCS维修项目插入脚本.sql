alter table REPAIRITEM add compcode VARCHAR2(50);
--维修项目
insert into RepairItem
  (Id,
   Code,
   Name,
   RepairItemCategoryId,
   QuickCode,
   JobType,
   RepairQualificationGrade,
   Status,
   CreatorName,
   CreateTime,
   compcode)
  select dcs.s_repairitem.nextval as Id,
         Code,
         substrb(Name,1,100) as name ,
         nvl((select id
            from dcs.RepairItemCategory M
           where M.Code = temp.upcode
             and M.Name = temp.upname
             --and M.compcode = temp.compcode
             and rownum=1
             ),9999999) as RepairItemCategoryId,
         QuickCode,
         decode(jobtype, '机修', 1, '钣金', 2, '喷漆', 3, '其他', 4,1) as JobType,
         decode(RepairQualificationGrade,'一级',1, '二级', 2, '三级', 3,1) as RepairQualificationGrade,
         1 as Status,
         'admin' as CreatorName,
         sysdate as CreateTim,
         temp.compcode 
    from tworkitemlinks_temp_om temp;

--维修项目与品牌的关系
insert into RepairItemBrandRelation
  (Id,
   BranchId,
   RepairItemId,
   RepairItemCode,
   RepairItemName,
   PartsSalesCategoryId,
   --SalesCategoryName,
   CreatorName,
   CreateTime,
   Status)
  select dcs.s_Repairitembrandrelation.nextval as Id,
         (select id from dcs.branch where branch.name = '商混设备销售分公司') as BranchId,
         nvl((select id
               from dcs.RepairItem
              where RepairItem.Code = temp.code
                and RepairItem.Name = substrb(temp.name,1,100)
                and RepairItem.Compcode = temp.compcode
                and rownum = 1),
             9999999) as RepairItemId,
         code as RepairItemCode,
         substrb(name, 1, 100) as RepairItemName,
         (select id
            from dcs.partssalescategory
           where partssalescategory.name = '商混设备') as PartsSalesCategoryId,
         --'商混设备' as SalesCategoryName,
         'admin' as CreatorName,
         sysdate as CreateTime,
         1 as Status
    from tworkitemlinks_temp_om temp;

--维修项目与服务产品线关系
insert into RepairItemAffiServProdLine
  (Id,
   BranchId,
   RepairItemId,
   ServiceProductLineId,
   DefaultLaborHour,
   Status,
   CreatorName,
   CreateTime,
   ProductLineType)
  select dcs.s_Repairitemaffiservprodline.nextval as Id,
         (select id from dcs.branch where branch.name = '商混设备销售分公司') as BranchId,
         nvl((select id
            from dcs.RepairItem
           where RepairItem.Code = temp.code
             and RepairItem.Name = substrb(temp.name,1,100)
             and RepairItem.Compcode = temp.compcode and rownum=1),999999) as RepairItemId,
         nvl((select id
            from dcs.ServiceProductLine
           where ServiceProductLine.Name = temp.productlinename),99999999) as ServiceProductLineId,
         temp.workhour as DefaultLaborHour,
         1 as Status,
         'admin' as CreatorName,
         sysdate as CreateTime,
         1 as ProductLineType
    from tworkitemlinks_temp_om temp;
 
