select *
  from (select twarehouses.code,
               twarehouses.name,
               'SHSB' 分公司编号,
               '商混设备销售分公司' 分公司名称,
               (select code
                  from ft."tPerson"@fpurview p
                 where p.objid = links.operatorid) as 仓库人员编号,
               (select name
                  from ft."tPerson"@fpurview p
                 where p.objid = links.operatorid) as 仓库人员名称
          from ft.twarehouses
         inner join ft.twarehouseoperator_Links links
            on links.warehouseid = twarehouses.objid
         where twarehouses.entercode =
               '{3CFD3185-C122-4383-A142-2A8AF6E1E03E}'
           and twarehouses.isvalid > 0)
 where 仓库人员编号 is not null;


