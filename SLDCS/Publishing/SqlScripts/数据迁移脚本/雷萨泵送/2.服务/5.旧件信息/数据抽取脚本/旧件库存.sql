--分公司

select twarehouses.code,
       twarehouses.name,
       'SHSB' 分公司编号,
       '商混设备销售分公司' 分公司名称,
       '分公司' 仓储企业类型,
       tlocations.code as 库位编号,
       tareas.code || tareafields.code as 库区编号,
       (select code
          from ft.tmaterials
         where tmaterials.objid = tstores.materialid) as 旧件配件编号,
       (select name
          from ft.tmaterials
         where tmaterials.objid = tstores.materialid) as 旧件配件名称,
       tstores.batchcode as 旧件条码,
       tstores.usableqty as 数量,
       (select paramvalue
          from ft.tparams
         where paramtype = '索赔单类型'
           and tparams.paramcode = tbyservicecards.servicecardtype) as 索赔单类型,
       nvl((select count(CauseMID)
             from ft.tbyservicecards
            where tbyservicecards.CauseMID = tstores.materialid
              and tstores.batchcode = toldparts.barcode),
           0) as 是否祸首件,
       toldparts.supplycode as 旧件供应商编号,
       toldparts.supplyname as 旧件供应商名称,
       nvl((select DutyUnitID
             from ft.tbyservicecards
            where tbyservicecards.CauseMID = tstores.materialid
              and tstores.batchcode = toldparts.barcode),
           0) as 祸首件供应商编号,
       nvl((select DutyUnitID
             from ft.tbyservicecards
            where tbyservicecards.CauseMID = tstores.materialid
              and tstores.batchcode = toldparts.barcode),
           0) as 祸首件供应商名称,
       nvl((select DutyUnitID
             from ft.tbyservicecards
            where tbyservicecards.CauseMID = tstores.materialid
              and tstores.batchcode = toldparts.barcode),
           0) as 责任单位编号,
       nvl((select DutyUnitID
             from ft.tbyservicecards
            where tbyservicecards.CauseMID = tstores.materialid
              and tstores.batchcode = toldparts.barcode),
           0) as 责任单位名称,
       toldparts.spareprice as 结算价格,
       toldparts.spareprice as 成本价格
  from ft.tstores
 inner join ft.toldparts
    on toldparts.barcode = tstores.batchcode
 inner join ft.tsprepairitems
    on tsprepairitems.objid = toldparts.parentid
 inner join ft.tbyservicecards
    on tbyservicecards.objid = tsprepairitems.parentid
 inner join ft.twarehouses
    on twarehouses.objid = tstores.warehouseid
 inner join ft.tlocations
    on tlocations.objid = tstores.locationid
 inner join ft.tareafields
    on tlocations.areafieldid = tareafields.objid
 inner join ft.tareas
    on tareafields.areaid = tareas.objid
 where twarehouses.isvalid > 0
   and tstores.isvalid > 0
   and tbyservicecards.isvalid > 0
   and tlocations.isvalid > 0
   and tareafields.isvalid > 0
   and tareas.isvalid > 0
   and tbyservicecards.brandid =
       (select brandid from ft.tbrands where tbrands.name = '商混设备')
