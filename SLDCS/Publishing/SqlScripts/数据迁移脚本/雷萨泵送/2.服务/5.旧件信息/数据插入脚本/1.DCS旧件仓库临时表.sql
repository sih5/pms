Create table toldpartwarehouses_temp_om
(
  CODE          VARCHAR2(20), 
  NAME          VARCHAR2(40),
  FilialeCode   VARCHAR2(20),
  FilialeName   VARCHAR2(100),
  BrandName     VARCHAR2(20),
  E_MAIL        VARCHAR2(50),
  FAX           VARCHAR2(20),
  LINKER        VARCHAR2(20),
  TEL           VARCHAR2(20),
  ADDRESS       VARCHAR2(200),
  StoragePolicy VARCHAR2(20)
)
