Insert Into UsedPartsWarehouse
  (Id,
   Code,
   Name,
   BranchId,
   PartsSalesCategoryId,
   Status,
   Address,
   ContactPhone,
   Contact,
   Fax,
   Email,
   StoragePolicy,
   CreatorName,
   CreateTime)
  select dcs.s_Usedpartswarehouse.nextval as id,
         CODE,
         NAME,
         (select id from dcs.branch where branch.name = temp.filialename) as BranchId,
         (select id
            from dcs.PartsSalesCategory
           where PartsSalesCategory.name = temp.brandname) as PartsSalesCategoryId,
         1 as Status,
         ADDRESS,
         TEL as ContactPhone,
         LINKER as Contact,
         FAX,
         E_MAIL as Email,         
         1 as StoragePolicy,
         'admin' as CreatorName,
         sysdate as CreateTime
    from toldpartwarehouses_temp_om temp;
