create table twarehouseoperators_temp_om
(
  warehousecode varchar2(20),
  warehousename varchar2(50),
  enterprisecode varchar2(40),
  enterprisename varchar2(100),
  personcode     varchar2(20),    
  personname     varchar2(20)
)
