Insert Into UsedPartsWarehouseStaff
  (Id, UsedPartsWarehouseId, PersonnelId, CreatorName, CreateTime)
  select dcs.s_Usedpartswarehousestaff.nextval as id,
         Usedpartswarehouse.id as UsedPartsWarehouseId,
         p.id as PersonnelId,
         'admin' as CreatorName,
         sysdate as CreateTime
    from twarehouseoperators_temp_om temp
   inner join Usedpartswarehouse
      on Usedpartswarehouse.name = temp.warehousename
   inner join Branch
      on Usedpartswarehouse.BranchId = Branch.id
     and Branch.Name = temp.enterprisename
   inner join personnel p
      on p.loginid = temp.personcode
     and p.name = temp.personname
     and Branch.Id = p.corporationid
