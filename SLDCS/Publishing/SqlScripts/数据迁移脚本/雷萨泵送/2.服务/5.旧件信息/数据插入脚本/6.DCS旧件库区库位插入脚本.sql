--�ֿ����͵Ŀ���
insert into UsedPartsWarehouseArea
  (Id,
   UsedPartsWarehouseId,
   ParentId,
   TopLevelUsedPartsWhseAreaId,
   Code,
   IfDefaultStoragePosition,
   StorageCategory,
   Status,
   StorageAreaType,
   CreatorName,
   CreateTime)
  select dcs.s_Usedpartswarehousearea.nextval as id,
         Usedpartswarehouse.id as UsedPartsWarehouseId,
         null as ParentId,
         null as TopLevelUsedPartsWhseAreaId,
         temp.areacode as Code,
         0 as IfDefaultStoragePosition,
         null as StorageCategory,
         1 as status,
         1 as StorageAreaType,
         'admin' as CreatorName,
         sysdate as CreateTime
    from toldpartwarehousearea_temp_om temp
   inner join dcs.Usedpartswarehouse
      on UsedPartsWarehouse.Name = temp.warehousename
   inner join dcs.branch
      on branch.id = UsedPartsWarehouse.Branchid
     and temp.filialename = branch.name
   where temp.type = '�ֿ�';

--Ĭ�Ͽ���
insert into UsedPartsWarehouseArea
  (Id,
   UsedPartsWarehouseId,
   ParentId,
   TopLevelUsedPartsWhseAreaId,
   Code,
   IfDefaultStoragePosition,
   StorageCategory,
   Status,
   StorageAreaType,
   CreatorName,
   CreateTime)
  select dcs.s_Usedpartswarehousearea.nextval as id,
         Usedpartswarehouse.id as UsedPartsWarehouseId,
         (select id
            from UsedPartsWarehouseArea
           where UsedPartsWarehouseArea.Usedpartswarehouseid =
                 Usedpartswarehouse.id
             and UsedPartsWarehouseArea.Code = temp.areacode) as ParentId,
         null as TopLevelUsedPartsWhseAreaId,
         temp.areacode || 'Ĭ�Ͽ���' as Code,
         0 as IfDefaultStoragePosition,
         1 as StorageCategory,
         1 as status,
         2 as StorageAreaType,
         'admin' as CreatorName,
         sysdate as CreateTime
    from toldpartwarehousearea_temp_om temp
   inner join dcs.Usedpartswarehouse
      on UsedPartsWarehouse.Name = temp.warehousename
   inner join dcs.branch
      on branch.id = UsedPartsWarehouse.Branchid
     and temp.filialename = branch.name
   where temp.type = '�ֿ�';

--Ĭ�Ͽ�λ
insert into UsedPartsWarehouseArea
  (Id,
   UsedPartsWarehouseId,
   ParentId,
   TopLevelUsedPartsWhseAreaId,
   Code,
   IfDefaultStoragePosition,
   StorageCategory,
   Status,
   StorageAreaType,
   CreatorName,
   CreateTime)
  select dcs.s_Usedpartswarehousearea.nextval as id,
         Usedpartswarehouse.id as UsedPartsWarehouseId,
         (select id
            from UsedPartsWarehouseArea
           where UsedPartsWarehouseArea.Usedpartswarehouseid =
                 Usedpartswarehouse.id
             and UsedPartsWarehouseArea.Code = (temp.areacode || 'Ĭ�Ͽ���')) as ParentId,
         null as TopLevelUsedPartsWhseAreaId,
         temp.areacode || 'Ĭ�Ͽ�λ' as Code,
         1 as IfDefaultStoragePosition,
         1 as StorageCategory,
         1 as status,
         3 as StorageAreaType,
         'admin' as CreatorName,
         sysdate as CreateTime
    from toldpartwarehousearea_temp_om temp
   inner join dcs.Usedpartswarehouse
      on UsedPartsWarehouse.Name = temp.warehousename
   inner join dcs.branch
      on branch.id = UsedPartsWarehouse.Branchid
     and temp.filialename = branch.name
   where temp.type = '�ֿ�';

--�ɼ��ֿ����
insert into UsedPartsWarehouseArea
  (Id,
   UsedPartsWarehouseId,
   ParentId,
   TopLevelUsedPartsWhseAreaId,
   Code,
   IfDefaultStoragePosition,
   StorageCategory,
   Status,
   StorageAreaType,
   CreatorName,
   CreateTime)
  select dcs.s_Usedpartswarehousearea.nextval as id,
         UsedPartsWarehouseId,
         ParentId,
         TopLevelUsedPartsWhseAreaId,
         Code,
         IfDefaultStoragePosition,
         StorageCategory,
         Status,
         StorageAreaType,
         CreatorName,
         CreateTime
    from (select distinct Usedpartswarehouse.id as UsedPartsWarehouseId,
                          (select id
                             from UsedPartsWarehouseArea
                            where UsedPartsWarehouseArea.Usedpartswarehouseid =
                                  Usedpartswarehouse.id
                              and UsedPartsWarehouseArea.Code =
                                  temp.warehousecode) as ParentId,
                          null as TopLevelUsedPartsWhseAreaId,
                          temp.areacode as Code,
                          0 as IfDefaultStoragePosition,
                          1 as StorageCategory,
                          1 as status,
                          2 as StorageAreaType,
                          'admin' as CreatorName,
                          sysdate as CreateTime
            from toldpartwarehousearea_temp_om temp
           inner join dcs.Usedpartswarehouse
              on UsedPartsWarehouse.Name = temp.warehousename
           inner join dcs.branch
              on branch.id = UsedPartsWarehouse.Branchid
             and temp.filialename = branch.name
           where temp.type = '��λ');



--�ɼ��ֿ��λ
insert into UsedPartsWarehouseArea
  (Id,
   UsedPartsWarehouseId,
   ParentId,
   TopLevelUsedPartsWhseAreaId,
   Code,
   IfDefaultStoragePosition,
   StorageCategory,
   Status,
   StorageAreaType,
   CreatorName,
   CreateTime)
  select dcs.s_Usedpartswarehousearea.nextval as id,
         Usedpartswarehouse.id as UsedPartsWarehouseId,
         (select id
            from UsedPartsWarehouseArea
           where UsedPartsWarehouseArea.Usedpartswarehouseid =
                 Usedpartswarehouse.id
             and UsedPartsWarehouseArea.Code = temp.areacode) as ParentId,
          (select id
            from UsedPartsWarehouseArea
           where UsedPartsWarehouseArea.Usedpartswarehouseid =
                 Usedpartswarehouse.id
             and UsedPartsWarehouseArea.Code = temp.areacode) as TopLevelUsedPartsWhseAreaId,
         temp.locationcode as Code,
         0 as IfDefaultStoragePosition,
         1 as StorageCategory,
         1 as status,
         3 as StorageAreaType,
         'admin' as CreatorName,
         sysdate as CreateTime
    from toldpartwarehousearea_temp_om temp
   inner join dcs.Usedpartswarehouse
      on UsedPartsWarehouse.Name = temp.warehousename
   inner join dcs.branch
      on branch.id = UsedPartsWarehouse.Branchid
     and temp.filialename = branch.name
   where temp.type = '��λ';


