
--配件保修分类标准
Insert Into PartsWarrantyStandard
  (Id,
   Code,
   Name,
   PartsWarrantyType,
   WarrantyPeriod,
   WarrantyMileage,
   Capacity,
   WorkingHours,
   Status,
   CreatorName,
   CreateTime)
values
  (s_PartsWarrantyStandard.Nextval,
   'SHSB-PJBXBZ',
   '商混设备-配件保修分类标准',
   1,
   180,
   9999999,
   0,
   9999999,
   1,
   'admin',
   sysdate);
   
--保修政策
Insert Into WarrantyPolicy
  (Id,
   BranchId,
   BranchCode,
   BranchName,
   Code,
   Name,
   Category,
   ApplicationScope,
   RepairTermInvolved,
   MainteTermInvolved,
   PartsWarrantyTermInvolved,
   ReleaseDate,
   ExecutionDate,
   Status,
   CreatorName,
   CreateTime)
values
  (s_WarrantyPolicy.Nextval,
   (select id from dcs.branch where name = '商混设备销售分公司'),
   'SHSB',
   '商混设备销售分公司',
   'SHSB-001',
   '商混设备保修政策',
   1,
   2,
   1,
   1,
   1,
   to_date('2013-01-01', 'YYYY-MM-DD'),
   to_date('2014-01-01', 'YYYY-MM-DD'),
   1,
   'admin',
   sysdate);


--整车保修条款
Insert Into VehicleWarrantyTerm
  (Id,
   Code,
   Name,
   WarrantyPolicyId,
   MaxMileage,
   MinMileage,
   DaysUpperLimit,
   DaysLowerLimit,
   Capacity,
   WorkingHours,
   Status,
   CreatorName,
   CreateTime)
values
  (s_VehicleWarrantyTerm.Nextval,
   'SHSB-ZCBX',
   '商混设备-整车保修条款',
   (select id from WarrantyPolicy where code = 'SHSB-001'),
   10000,
   0,
   360,
   0,
   0,
   600,
   1,
   'admin',
   sysdate);

--整车保养条款
Insert Into VehicleMainteTerm
  (Id,
   Code,
   Name,
   WarrantyPolicyId,
   MainteType,
   WorkingHours,
   Capacity,
   MaxMileage,
   MinMileage,
   DaysUpperLimit,
   DaysLowerLimit,
   BillingMethod,
   LaborCost,
   MaterialCost,
   OtherCost,
   DisplayOrder,
   Status,
   CreatorName,
   CreateTime)
values
  (s_VehicleMainteTerm.Nextval,
   'SHSB-ZCBY',
   '商混设备-整车保养条款',
   (select id from WarrantyPolicy where code = 'SHSB-001'),
   1,
   60,
   0,
   4000,
   0,
   60,
   0,
   2,
   0,
   0,
   0,
   1,
   1,
   'admin',
   sysdate);

--配件保修条款
Insert Into PartsWarrantyTerm
  (Id,
   WarrantyPolicyId,
   PartsWarrantyType,
   PartsWarrantyCategoryId,
   WarrantyPeriod,
   WarrantyMileage,
   Capacity,
   WorkingHours,
   Status,
   CreatorName,
   CreateTime)
values
  (s_PartsWarrantyTerm.Nextval,
   (select id from WarrantyPolicy where code = 'SHSB-001'),
   1,
   (select id from PartsWarrantyStandard where code ='SHSB-PJBXBZ'),
   180,
   9999999,
   0,
   9999999,
   1,
   'admin',
   sysdate);


--保修政策与服务产品线关系
Insert Into WarrantyPolicyNServProdLine
  (Id,
   WarrantyPolicyId,
   ServiceProductLineId,
   ValidationDate,
   ExpireDate,
   Status,
   CreatorName,
   CreateTime,
   ProductLineType)
select s_WarrantyPolicyNServProdLine.Nextval as Id,
       (select id from WarrantyPolicy where code = 'SHSB-001') as WarrantyPolicyId,
       serviceproductline.id as ServiceProductLineId,
       to_date('2013-01-01', 'YYYY-MM-DD') as ValidationDate,
       to_date('2014-01-01', 'YYYY-MM-DD') as ExpireDate,
       1 as Status,
       'admin' as CreatorName,
       sysdate as CreateTime,
       1 as ProductLineType
  from dcs.serviceproductline
 inner join dcs.PartsSalesCategory
    on PartsSalesCategory.Id = serviceproductline.partssalescategoryid
 where PartsSalesCategory.Name = '欧曼'
    or PartsSalesCategory.Name = '商混设备';

--单车保修卡
Insert Into VehicleWarrantyCard
  (Id,
   Code,
   VehicleId,
   VIN,
   VehicleCategoryId,
   VehicleCategoryName,
   ServiceProductLineId,
   ServiceProductLineName,
   ProductId,
   ProductCode,
   SalesDate,
   WarrantyStartDate,
   RetainedCustomerId,
   CustomerName,
   CustomerGender,
   IdDocumentType,
   IdDocumentNumber,
   WarrantyPolicyCategory,
   WarrantyPolicyId,
   Status,
   CreatorName,
   CreateTime)
  select s_VehicleWarrantyCard.Nextval as Id,
         to_char(sysdate, 'YYYYMMDD') || rownum as Code,
         vehicleinformation.id as VehicleId,
         vehicleinformation.VIN,
         vehicleinformation.VehicleCategoryId,
         vehicleinformation.VehicleCategoryName,
         serviceproductline.id as ServiceProductLineId,
         serviceproductline.name as ServiceProductLineName,
         product.id as ProductId,
         product.code as ProductCode,
         vehicleinformation.salesdate as SalesDate,
         nvl(vehicleinformation.salesdate,sysdate) as WarrantyStartDate,
         Retainedcustomer.Id as RetainedCustomerId,
         Customer.Name as CustomerName,
         Customer.Gender as CustomerGender,
         Customer.IdDocumentType,
         Customer.IdDocumentNumber,
         1 as WarrantyPolicyCategory,
         (select id
            from WarrantyPolicy
           where WarrantyPolicy.Code = 'SHSB-001') as WarrantyPolicyId,
         1 as Status,
         'admin' as CreatorName,
         sysdate as CreateTime
    from dcs.vehicleinformation
   inner join dcs.product
      on product.id = vehicleinformation.productid
   inner join dcs.ServProdLineProductDetail de
      on de.productid = product.id
   inner join dcs.serviceproductline
      on serviceproductline.id = de.serviceproductlineid
   inner join dcs.PartsSalesCategory
      on PartsSalesCategory.Id = serviceproductline.partssalescategoryid
   inner join dcs.Retainedcustomervehiclelist list
      on list.vehicleid = vehicleinformation.id
   inner join dcs.Retainedcustomer
      on RetainedCustomer.Id = list.retainedcustomerid
   inner join dcs.Customer
      on Customer.Id = Retainedcustomer.Customerid
   where PartsSalesCategory.Name = '欧曼'
      or PartsSalesCategory.Name = '商混设备';

--单车保养条款
Insert Into VehicleMaintenancePoilcy
  (Id,
   VehicleWarrantyCardId,
   VehicleMainteTermId,
   VehicleMainteTermCode,
   VehicleMainteTermName,
   MainteType,
   WorkingHours,
   Capacity,
   MaxMileage,
   MinMileage,
   DaysUpperLimit,
   DaysLowerLimit,
   IfEmployed,
   DisplayOrder)
  select s_VehicleMaintenancePoilcy.Nextval as Id,
         Vehiclewarrantycard.id             as VehicleWarrantyCardId,
         VehicleMainteTerm.id               as VehicleMainteTermId,
         VehicleMainteTerm.code             as VehicleMainteTermCode,
         VehicleMainteTerm.name             as VehicleMainteTermName,
         VehicleMainteTerm.MainteType,
         VehicleMainteTerm.WorkingHours,
         VehicleMainteTerm.Capacity,
         VehicleMainteTerm.MaxMileage,
         VehicleMainteTerm.MinMileage,
         VehicleMainteTerm.DaysUpperLimit,
         VehicleMainteTerm.DaysLowerLimit,
         0                                  as IfEmployed,
         1                                  as DisplayOrder
    from dcs.vehicleinformation
   inner join dcs.product
      on product.id = vehicleinformation.productid
   inner join dcs.ServProdLineProductDetail de
      on de.productid = product.id
   inner join dcs.serviceproductline
      on serviceproductline.id = de.serviceproductlineid
   inner join dcs.PartsSalesCategory
      on PartsSalesCategory.Id = serviceproductline.partssalescategoryid
   inner join dcs.Vehiclewarrantycard
      on VehicleWarrantyCard.Vehicleid = vehicleinformation.id
   inner join dcs.WarrantyPolicy
      on WarrantyPolicy.id = VehicleWarrantyCard.Warrantypolicyid
   inner join dcs.VehicleMainteTerm
      on VehicleMainteTerm.WarrantyPolicyId = WarrantyPolicy.id
   where PartsSalesCategory.Name = '欧曼'
      or PartsSalesCategory.Name = '商混设备';
