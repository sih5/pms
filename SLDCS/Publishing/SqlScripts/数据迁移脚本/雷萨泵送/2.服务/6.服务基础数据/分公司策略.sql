insert into Branchstrategy
  (Id,
   BranchId,
   PartsPurchasePricingStrategy,
   EmergencySalesStrategy,
   UsedPartsReturnStrategy,
   PartsShipping,
   IsGPS,
   DirectSupplyStrategy,
   PurchasePricingStrategy,
   ClaimAutoStopStrategy,
   ClaimRejectTimeStrategy,
   StopClaimStartStrategy,
   StopClaimTimeStrategy,
   ScanCodeStartTime,
   IsAllServiceTripApp,
   IsFirstMainteManage,
   IsOuterPurchaseManage,
   IsDebtManage,
   Outforce,
   OutNumberPeople,
   OutNumberDays,
   Status,
   CreatorName,
   CreateTime)
values
  (dcs.s_Branchstrategy.nextval,
   13 ,
   2,
   1,
   1,
   1,
   0,
   1,
   0,
   1,
   1,
   2,
   24,
   to_date('2013-01-01','YYYY-MM-DD'),
   0,
   0,
   0,
   0,
   0,
   10,
   10,
   1,
   'admin',
   sysdate);
