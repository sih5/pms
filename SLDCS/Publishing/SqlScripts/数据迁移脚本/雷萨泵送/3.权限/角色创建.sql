--创建临时表
create table roles(
lv1 VARCHAR2(200),
lv2 VARCHAR2(200),
lv3 VARCHAR2(200),
action VARCHAR2(200),
j1 VARCHAR2(200),
j2 VARCHAR2(200),
j3 VARCHAR2(200),
j4 VARCHAR2(200),
j5 VARCHAR2(200),
j6 VARCHAR2(200),
j7 VARCHAR2(200),
j8 VARCHAR2(200),
j9 VARCHAR2(200),
j10 VARCHAR2(200),
j11 VARCHAR2(200),
j12 VARCHAR2(200),
j13 VARCHAR2(200),
j14 VARCHAR2(200),
j15 VARCHAR2(200),
j16 VARCHAR2(200),
j17 VARCHAR2(200),
j18 VARCHAR2(200),
j19 VARCHAR2(200),
j20 VARCHAR2(200),
j21 VARCHAR2(200),
j22 VARCHAR2(200),
j23 VARCHAR2(200),
j24 VARCHAR2(200),
j25 VARCHAR2(200),
j26 VARCHAR2(200),
j27 VARCHAR2(200),
j28 VARCHAR2(200),
j29 VARCHAR2(200)
);
create table rolenames(
Jname varchar(200),
name varchar(200)
);
-----------------------------
--这里请把Excel导入到数据库中
-----------------------------

--创建角色，改模板针对雷萨分公司，由于现在不知道雷萨分公司ID是多少，暂时赋值为1，后续如果更改，再进行修改
insert into role select S_role.Nextval,1,Name,0,1,null,1,'系统管理员',sysdate,null,null,null,null from rolenames;

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j1')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j1 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j2')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j2 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j3')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j3 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j4')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j4 is not null) x);


insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j5')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j5 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j6')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j6 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j7')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j7 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j8')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j8 is not null) x);


insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j9')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j9 is not null) x);
                   
insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j10')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j10 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j11')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j11 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j12')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j12 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j13')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j13 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j14')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j14 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j15')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j15 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j16')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j16 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j17')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j17 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j18')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j18 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j19')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j19 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j20')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j20 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j21')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j21 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j22')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j22 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j23')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j23 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j24')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j24 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j25')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j25 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j26')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j26 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j27')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j27 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j28')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j28 is not null) x);

insert into rule select S_rule.Nextval,roleid, nodeid
    from (select distinct (x.id) as nodeid,
                          (select id
                             from role
                            where name = (select name
                                            from rolenames
                                           where jname = 'j29')) as roleid
            from (select node.id, roles.action, roles.lv3
                    from roles
                   inner join page
                      on page.name = roles.lv3
                   inner join action
                      on action.name = roles.action
                     and action.pageid = page.id
                   inner join node
                      on (node.categorytype = 0 and node.categoryid = page.id)
                      or (node.categoryType = 1 and
                         node.categoryid = action.id)
                   where j29 is not null) x);


