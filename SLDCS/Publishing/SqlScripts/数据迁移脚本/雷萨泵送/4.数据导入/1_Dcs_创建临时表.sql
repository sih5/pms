-- Create table
create table TSTATIONSTEMP
(
  CODE          VARCHAR2(20),
  NAME          VARCHAR2(100),
  FACTORYTIME   VARCHAR2(20),
  PROVINCENAME  VARCHAR2(20),
  REGIONNAME    VARCHAR2(50),
  COUNTYNAME    VARCHAR2(50),
  EMAIL         VARCHAR2(50),
  POSTCODE      NUMBER(9),
  MASTERCODE    VARCHAR2(50),
  PHONE         VARCHAR2(40),
  ADDRESS       VARCHAR2(100),
  FAX           VARCHAR2(20),
  REGISTERCODE  VARCHAR2(50),
  TSTATIONNAME  VARCHAR2(100),
  ESTABLISHDATE VARCHAR2(20),
  REGISTERFUND  NUMBER(19,4),
  ENTERKIND     VARCHAR2(50),
  CORPORATION   VARCHAR2(50),
  PRIBUSINESS   VARCHAR2(100),
  QYADDRESS     VARCHAR2(100),
  REPQUA        VARCHAR2(50),
  LICENSE       VARCHAR2(50),
  ACCREDITBRAND VARCHAR2(100),
  PARKAREA      NUMBER(9),
  REPPLANTAREA  NUMBER(9),
  ANTEROOMAREA  NUMBER(9),
  WAREHOUSEAREA NUMBER(9)
)
tablespace SYSTEM
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );
  
-- Create table
create table COMPANYADDRESSTEMP
(
  CODE               VARCHAR2(20),
  NAME               VARCHAR2(100),
  PROVINCENAME       VARCHAR2(20),
  REGIONNAME         VARCHAR2(50),
  COUNTYNAME         VARCHAR2(50),
  SPARELINKERMAN     VARCHAR2(100),
  SPARELINKERTEL     VARCHAR2(100),
  SPARELINKERADDRESS VARCHAR2(100)
)
tablespace SYSTEM
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );
  
  
-- Create table
create table DEALERSERVICEINFOTEMP
(
  CODE                    VARCHAR2(20),
  NAME                    VARCHAR2(100),
  FILIALECODE             VARCHAR2(20),
  FILIALENAME             VARCHAR2(40),
  BRANDNAME               VARCHAR2(40),
  MARKETCODE              VARCHAR2(20),
  MARKETNAME              VARCHAR2(40),
  CHANNELCAPABILITYID     VARCHAR2(40),
  PHONE                   VARCHAR2(40),
  DUTYPHONE               VARCHAR2(40),
  FAX                     VARCHAR2(20),
  REPAIRPURVIEW           VARCHAR2(4),
  PARTSMANAGINGFEEGRADEID VARCHAR2(40),
  OUTFEEGRADEID           VARCHAR2(40),
  GRADECOEFFICIENTID      VARCHAR2(40),
  OUTSERVICERADII         VARCHAR2(40),
  USEDPARTSWAREHOUSEID    VARCHAR2(40),
  STATIONTYPE             NUMBER(3),
  ISDUTY                  NUMBER(3),
  SALINGTIME              VARCHAR2(40),
  CUTTIME                 VARCHAR2(40),
  SERVICEPURVIEW          VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );
    
-- Create table
create table DEALERBUSINESSPERMITTEMP
(
  CODE              VARCHAR2(20),
  NAME              VARCHAR2(100),
  FILIALECODE       VARCHAR2(20),
  FILIALENAME       VARCHAR2(40),
  PRODUCTNAME       VARCHAR2(40),
  WORKPRICE         NUMBER(19,4),
  SERVICEFEEGRADEID VARCHAR2(20)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 1
    minextents 1
    maxextents unlimited
  );
  
  -- Create table
create table TEMP_PARTSSUPPLIER
(
  MDM编号   VARCHAR2(90),
  供应商编号   VARCHAR2(90),
  供应商名称   VARCHAR2(90),
  分类      VARCHAR2(90),
  供应商类型   VARCHAR2(90),
  省       VARCHAR2(90),
  市       VARCHAR2(90),
  县       VARCHAR2(90),
  邮编      VARCHAR2(90),
  电子邮件    VARCHAR2(90),
  传真      VARCHAR2(90),
  联系人     VARCHAR2(90),
  联系电话    VARCHAR2(90),
  联系地址    VARCHAR2(90),
  注册地址    VARCHAR2(90),
  经营范围    VARCHAR2(90),
  注册日期    VARCHAR2(90),
  隶属分公司编号 VARCHAR2(90),
  隶属分公司名称 VARCHAR2(90),
  品牌      VARCHAR2(90)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64
    next 8
    minextents 1
    maxextents unlimited
  );  
create table Temp_ServiceTripPriceGrade (
ServiceTripPriceGradeName varchar2(500),
RegionDescription varchar2(500),
ServiceProductLineCode varchar2(500),
ServiceProductLineName varchar2(500),
ProductLineType varchar2(500),
ServiceTripType varchar2(500),
MaxMileage varchar2(500),
MinMileage varchar2(500),
FixedAmount varchar2(500),
IsSelfCar varchar2(500),
SubsidyPrice varchar2(500),
Price varchar2(500),
Remark varchar2(500)
);
create table Temp_Warehouse(
Code varchar2(500),
Name varchar2(500),
Type varchar2(500),
BranchCode varchar2(500),
BranchName varchar2(500),
StorageCompanyCode varchar2(500),
StorageCompanyName varchar2(500),
Email varchar2(500),
Fax varchar2(500),
Contact varchar2(500),
PhoneNumber varchar2(500),
Address varchar2(500),
StorageStrategy varchar2(500)
);

create table Temp_CustomerOrderPriceGrade(
CustomerCompanyCode varchar2(500),
CustomerCompanyName varchar2(500),
PartsSalesOrderTypeCode varchar2(500),
PartsSalesOrderTypeName varchar2(500),
PartsSalesCategoryId varchar2(500),
Coefficient varchar2(500)
);
create table Temp_PartsPurchasePricing(
PartCode varchar2(500),
PartName varchar2(500),
PartsSupplierCode varchar2(500),
PartsSupplierName varchar2(500),
PurchasePrice varchar2(500),
ValidFrom varchar2(500),
ValidTo varchar2(500)
);
create table Temp_Prices (
SparePartCode varchar2(500),
SparePartName varchar2(500),
PartsSalesCategoryName varchar2(500),
SalePrice varchar2(500),
IfClaim varchar2(500),
RetaiSparePartCode varchar2(500),
RetailGuidePrice varchar2(500)
);