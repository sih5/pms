insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '内部组织', '企业收货地址管理', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '内部组织', '企业收货地址管理', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '内部组织', '企业收货地址管理', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件营销信息查询', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件销售基础数据', '配件客户信息', '导入');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件销售基础数据', '配件客户信息', '停用');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件销售基础数据', '配件客户信息', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件销售基础数据', '配件客户信息', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件销售基础数据', '配件客户信息', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件销售基础数据', '配件客户信息', '恢复');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件销售基础数据', '配件零售指导价管理', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件替换件管理', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件替换件管理', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件替换件管理', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件替换件管理', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件替换件管理', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件互换件管理', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件互换件管理', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件互换件管理', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件互换件管理', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件互换件管理', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '直供发运单确认管理', '确认');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '直供发运单确认管理', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '直供发运单确认管理', '打印');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件信息查询', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件申请管理', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件申请管理', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件申请管理', '提交');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件申请管理', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件申请管理', '打印');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件调剂管理', '确认');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件调剂管理', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件调剂管理', '调剂看板');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件调剂管理', '完成');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件调剂管理', '打印');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售结算管理', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售结算管理', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售结算管理', '审批');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售结算管理', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售结算管理', '登记发票');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售结算管理', '反结算');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售结算管理', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售结算管理', '打印');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售退货结算管理', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售退货结算管理', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售退货结算管理', '打印');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售退货结算管理', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售退货结算管理', '反结算');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售退货结算管理', '审批');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售退货结算管理', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售退货结算管理', '登记发票');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售结算发票查询', '打印');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售结算发票查询', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售退货结算查询', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售结算单查询', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单管理', '下载');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货提报', '合并导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单管理', '合并导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单管理', '审批');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单管理', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单管理', '终止');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单管理', '新增(代做)');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单管理', '撤单(代做)');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单管理', '折让审批');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '提交');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '合并导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '撤单');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货管理', '合并导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货管理', '审批');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货管理', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货管理', '新增(代做)');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货提报', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货提报', '提交');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货提报', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货提报', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '配件零售订单管理', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '配件零售订单管理', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '配件零售订单管理', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '配件零售订单管理', '审批');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '配件零售订单管理', '开票');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '配件零售退货管理', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '配件零售退货管理', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '配件零售退货管理', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '配件零售退货管理', '审批');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '配件零售退货管理', '退款');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件仓储', '仓库管理', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件仓储', '仓库管理', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件仓储', '仓库管理', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件仓储', '仓库管理', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件仓储', '库区库位管理', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件仓储', '库区库位管理', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件仓储', '库区库位管理', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件仓储', '配件库位分配', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件仓储', '配件库位变更履历查询', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件入库业务', '配件检验入库', '查看明细');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件入库业务', '配件检验入库', '检验入库');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件入库业务', '配件检验入库', '终止');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件入库业务', '配件上架', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件入库业务', '配件入库计划查询', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件入库业务', '配件入库检验单查询', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件入库业务', '配件入库检验单查询', '查看包装清单');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件入库业务', '配件入库检验单查询', '打印');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件出库业务', '配件出库', '出库');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件出库业务', '配件出库', '终止');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件出库业务', '配件出库', '查看明细');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件出库业务', '配件出库计划查询', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件出库业务', '配件出库单查询', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件出库业务', '配件出库退回', '退库');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件物流', '配件发运管理', '回执确认');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件物流', '配件发运管理', '路损处理');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件物流', '配件发运管理', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件物流', '配件发运管理', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件物流', '配件发运管理', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件物流', '配件收货管理', '收货确认');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库内业务', '配件移库管理', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库内业务', '配件移库管理', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库存', '配件库存明细查询', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件出库业务', '配件出库单查询', '打印');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库存', '配件库存明细查询', '下载');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件出库业务', '配件出库计划查询', '打印');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件出库业务', '配件出库计划查询', '合并导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件入库业务', '配件入库计划查询', '打印');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件仓储', '库区库位管理', '导入库区');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件仓储', '库区库位管理', '导入库位');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件物流', '配件发运管理', '打印');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库内业务', '配件移库管理', '打印');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件物流', '配件发运管理', '合并导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库存', '配件仓库库存查询', '导出');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件入库业务', '配件检验入库', '标签打印');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库内业务', '代理库配件盘点单提报', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库内业务', '代理库配件盘点单提报', '查看清单');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库内业务', '代理库配件盘点单提报', '审批');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库内业务', '代理库配件盘点单提报', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库内业务', '代理库配件盘点单提报', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件调拨业务', '配件调拨管理', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件调拨业务', '配件调拨管理', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件调拨业务', '配件调拨管理', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件调拨业务', '配件调拨管理', '审批');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件调拨业务', '配件调拨管理', '调拨看板');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '客户开户申请管理', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '客户开户申请管理', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '客户开户申请管理', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '客户开户申请管理', '审核');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '来款单管理', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '来款单管理', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '来款单管理', '审批');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '来款单管理', '来款分割');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '来款单管理', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '来款分割单查询', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '客户账户查询', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '客户账户明细查询', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '客户转账管理', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '客户转账管理', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '客户转账管理', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '客户转账管理', '审批');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '账户查询', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '信用管理', '新增');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '信用管理', '修改');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '信用管理', '审批');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '信用管理', '作废');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('公告管理', '公告管理', '公告查询', '查看明细');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('报表', '财务', '配件入库暂估明细表', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('报表', '配件', '历史入库记录查询', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('报表', '财务', '配件暂估入库汇总表', 'N/A');
insert into TEMP_ROLE_A (LV1, LV2, LV3, ACTION)
values ('报表', '配件', '历史出库记录查询', 'N/A');

insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '服务站分公司信息管理-服务站', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '服务站分公司信息管理-服务站', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '服务站地址维护-服务站', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '服务站地址维护-服务站', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '服务站地址维护-服务站', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '二级站管理', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '二级站管理', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '二级站管理', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '二级站管理', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '二级站人员管理', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '二级站人员管理', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '二级站人员管理', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '二级站人员管理', '删除');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '企业', '企业开票信息管理', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '企业', '企业开票信息管理', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '企业', '企业开票信息管理', '删除');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '企业', '企业开票信息管理', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件营销信息查询', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件信息管理', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件信息管理', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件信息管理', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件信息管理', '恢复');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件信息管理', '停用');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件信息管理', '导入');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件信息管理', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件替换件管理', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件替换件管理', '导入');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件替换件管理', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件替换件管理', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件替换件管理', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件互换件管理', '导入');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件互换件管理', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件互换件管理', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件互换件管理', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件互换件管理', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '直供发运单确认管理', '确认');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '直供发运单确认管理', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '直供发运单确认管理', '打印');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '服务站配件零售管理', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '服务站配件零售管理', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '服务站配件零售管理', '审批');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '服务站配件零售管理', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '服务站配件零售审核', '审批');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '服务站配件零售审核', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '服务站配件零售审核', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件信息查询', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件申请管理', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件申请管理', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件申请管理', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件申请管理', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件申请管理', '打印');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售结算发票查询', '打印');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售结算发票查询', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售退货结算查询', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售结算单查询', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '二级站配件零售订单审核', '审批');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '二级站配件零售订单审核', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '二级站配件零售订单审核', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '二级站配件零售管理', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '二级站配件零售管理', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '二级站配件零售管理', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '二级站配件零售管理', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '二级站配件零售管理', '审批');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单管理', '下载');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货提报', '合并导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '合并导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '撤单');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货提报', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货提报', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货提报', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货提报', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '二级站配件需求管理', '审核');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '二级站配件需求提报', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '二级站配件需求提报', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '二级站配件需求提报', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '二级站配件需求提报', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件外采业务', '配件外采提报', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件外采业务', '配件外采提报', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件外采业务', '配件外采提报', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件外采业务', '配件外采提报', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件外采业务', '配件外采提报', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '服务站配件零售管理', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库存', '服务站配件库存管理', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库内业务', '服务站配件盘点提报', '导入');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库内业务', '服务站配件盘点提报', '审核');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库内业务', '服务站配件盘点提报', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库内业务', '服务站配件盘点提报', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件调拨业务', '企业间配件调拨管理', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件调拨业务', '企业间配件调拨管理', '审批');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件调拨业务', '企业间配件调拨管理', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件调拨业务', '企业间配件调拨管理', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('配件财务管理', '应收管理', '账户查询', 'N/A');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('服务基础数据管理', '车辆及客户档案', '客户信息管理', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('服务基础数据管理', '车辆及客户档案', '车辆信息管理', '服务站修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('服务基础数据管理', '车辆及客户档案', '车辆信息管理', '历史车主');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('服务基础数据管理', '车辆及客户档案', '车辆信息管理', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '服务活动索赔管理-服务站', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '配件索赔管理-服务站', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '配件索赔管理-服务站', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '配件索赔管理-服务站', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '配件索赔管理-服务站', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '配件索赔管理-服务站', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '服务活动管理', '服务活动管理', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '服务站索赔结算', '服务站索赔结算查询-服务站', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '服务站索赔结算', '服务站发票管理', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '服务站索赔结算', '服务站发票管理', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '服务站索赔结算', '服务站发票管理', '查看详细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '来客登记');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '来客并派工');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '维修单登记');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '维修单修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '指定领料完工');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '打印维修单');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '打印结算单');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理', '维修工单查询', 'N/A');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '保养索赔申请管理-服务站', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '保养索赔申请管理-服务站', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '保养索赔申请管理-服务站', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '保养索赔申请管理-服务站', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '保养索赔申请管理-服务站', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '外出索赔申请管理-服务站', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '外出索赔申请管理-服务站', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '外出索赔申请管理-服务站', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '外出索赔申请管理-服务站', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '外出索赔申请管理-服务站', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '服务活动索赔申请管理-服务站', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '服务活动索赔申请管理-服务站', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '服务活动索赔申请管理-服务站', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '服务活动索赔申请管理-服务站', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '服务活动索赔申请管理-服务站', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '配件索赔申请管理-服务站', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '配件索赔申请管理-服务站', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '配件索赔申请管理-服务站', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '配件索赔申请管理-服务站', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '配件索赔申请管理-服务站', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '外出服务索赔管理-服务站', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '外出服务索赔管理-服务站', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '外出服务索赔管理-服务站', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '外出服务索赔管理-服务站', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '维修索赔管理-服务站', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '保养索赔管理-服务站', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '保养索赔管理-服务站', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '保养索赔管理-服务站', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '保养索赔管理-服务站', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '保养索赔管理-服务站', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '服务活动索赔管理-服务站', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '服务活动索赔管理-服务站', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '服务活动索赔管理-服务站', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '服务活动索赔管理-服务站', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理', '保内维修单管理', '查看详细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理', '保外维修单管理', '查看详细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '打印维修单');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '打印结算单');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '打印维修单');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '打印结算单');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '售前检查管理-服务站', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '售前检查管理-服务站', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '售前检查管理-服务站', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '售前检查管理-服务站', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '售前检查管理-服务站', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '售前检查管理-服务站', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '维修工单管理-服务站', '生成维修单');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '维修工单管理-服务站', '关闭');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '生成紧急销售订单');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '生成外采申请');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '维修完工');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '驳回修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '生成索赔');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '生成外出索赔');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '生成紧急销售订单');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '生成外采申请');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '生成索赔');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '维修完工');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '二级站维修单管理', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '二级站维修单管理', '提交');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '二级站维修单管理', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '二级站维修单管理', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '二级站维修单管理', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '二级站维修单管理', '审批');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '旧件发运管理', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '旧件发运管理', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '旧件发运管理', '打印');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '旧件发运管理', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '旧件发运管理', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '服务站旧件库存查询', '导出');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '服务站旧件处理管理', '修改');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '服务站旧件处理管理', '作废');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '服务站旧件处理管理', '审批');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '服务站旧件处理管理', '打印');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '服务站旧件处理管理', '新增');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('待办事项', '待办事项', '今日公告', 'N/A');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公告管理', '公告管理', '公告查询', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公告管理', '公告管理', '公告查询', '查看明细');
insert into TEMP_ROLE_D (LV1, LV2, LV3, ACTION)
values ('公告管理', '公告管理', '公告查询', '查看明细');

insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '企业', '企业开票信息管理', '新增');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '企业', '企业开票信息管理', '修改');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '企业', '企业开票信息管理', '删除');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '企业', '企业开票信息管理', '查看明细');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购结算业务', '配件采购结算管理-供应商', '导出');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购结算业务', '配件采购结算管理-供应商', '新增');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购结算业务', '配件采购结算管理-供应商', '修改');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购结算业务', '配件采购结算管理-供应商', '审批');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购结算业务', '配件采购结算管理-供应商', '打印');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购结算业务', '配件采购退货结算查询-供应商', '导出');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购结算业务', '配件采购退货结算查询-供应商', '打印');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '配件采购订单管理-供应商', '打印');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '配件采购订单管理-供应商', '合并导出');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '配件采购订单管理-供应商', '审批');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '配件采购订单管理-供应商', '终止');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '配件采购退货管理-供应商 ', '打印');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '配件采购退货管理-供应商 ', '审批');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '供应商发运管理-供应商', '作废');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '供应商发运管理-供应商', '修改');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '供应商发运管理-供应商', '打印');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '供应商发运管理-供应商', '新增');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '供应商发运管理-供应商', '导出');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理', '维修索赔管理', '供应商确认');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('待办事项', '待办事项', '今日公告', 'N/A');
insert into TEMP_ROLE_S (LV1, LV2, LV3, ACTION)
values ('公告管理', '公告管理', '公告查询', '查看明细');