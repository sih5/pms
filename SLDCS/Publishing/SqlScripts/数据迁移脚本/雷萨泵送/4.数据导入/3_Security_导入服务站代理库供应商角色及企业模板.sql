--导入到代理库企业授权模板
declare
  S_Int number(9);
begin
  select S_EntNodeTemplate.Nextval into S_Int from dual;
  insert into EntNodeTemplate
    select S_Int,
           'AgencyTemplate',
           '代理库授权模板',
           2,
           null,
           1,
           1,
           '系统管理员',
           sysdate,
           null,
           null,
           null,
           null
      from dual;
  insert into EntNodeTemplateDetail
    select S_EntNodeTemplateDetail.Nextval, S_Int, nodeid
      from (select distinct (x.id) as nodeid
              from (select node.id, Temp_Role_A.action, Temp_Role_A.lv3
                      from Temp_Role_A
                     inner join page
                        on page.name = Temp_Role_A.lv3
                     left join action
                        on action.name = Temp_Role_A.action
                       and action.pageid = page.id
                     inner join node
                        on (node.categorytype = 0 and
                           node.categoryid = page.id)
                        or (node.categoryType = 1 and
                           node.categoryid = action.id)) x) xnode;
end;
--导入到服务站企业授权模板
declare
  S_Int number(9);
begin
  select S_EntNodeTemplate.Nextval into S_Int from dual;
  insert into EntNodeTemplate
    select S_Int,
           'DealerTemplate',
           '服务站授权模板',
           2,
           null,
           1,
           1,
           '系统管理员',
           sysdate,
           null,
           null,
           null,
           null
      from dual;
  insert into EntNodeTemplateDetail
    select S_EntNodeTemplateDetail.Nextval, S_Int, nodeid
      from (select distinct (x.id) as nodeid
              from (select node.id, Temp_Role_D.action, Temp_Role_D.lv3
                      from Temp_Role_D
                     inner join page
                        on page.name = Temp_Role_D.lv3
                     left join action
                        on action.name = Temp_Role_D.action
                       and action.pageid = page.id
                     inner join node
                        on (node.categorytype = 0 and
                           node.categoryid = page.id)
                        or (node.categoryType = 1 and
                           node.categoryid = action.id)) x) xnode;
end;
--导入到供应商企业授权模板
declare
  S_Int number(9);
begin
  select S_EntNodeTemplate.Nextval into S_Int from dual;
  insert into EntNodeTemplate
    select S_Int,
           'PartsSupplierTemplate',
           '供应商授权模板',
           2,
           null,
           1,
           1,
           '系统管理员',
           sysdate,
           null,
           null,
           null,
           null
      from dual;
  insert into EntNodeTemplateDetail
    select S_EntNodeTemplateDetail.Nextval, S_Int, nodeid
      from (select distinct (x.id) as nodeid
              from (select node.id, Temp_Role_S.action, Temp_Role_S.lv3
                      from Temp_Role_S
                     inner join page
                        on page.name = Temp_Role_S.lv3
                     left join action
                        on action.name = Temp_Role_S.action
                       and action.pageid = page.id
                     inner join node
                        on (node.categorytype = 0 and
                           node.categoryid = page.id)
                        or (node.categoryType = 1 and
                           node.categoryid = action.id)) x) xnode;
end;
--给已存在的代理库创建角色，并创建测试人员
declare
  cursor Cursor_Agency is
    select *
      from enterprise
     where EnterpriseCategoryId =
           (select id from EnterpriseCategory where name = '代理库');
  S_RoleId      number(9);
  S_PersonnelId number(9);
begin
  for C_Agency in Cursor_Agency loop
    select S_Role.Nextval into S_RoleId from dual;
    insert into Role
      select S_RoleId,
             C_Agency.id,
             '代理库业务角色',
             0,
             1,
             null,
             1,
             '系统管理员',
             sysdate,
             null,
             null,
             null,
             null
        from dual;
    insert into rule
      select S_rule.Nextval, S_RoleId, EntNodeTemplateDetail.Nodeid
        from EntNodeTemplateDetail
       where EntNodeTemplateDetail.Entnodetemplateid =
             (select id from EntNodeTemplate where code = 'AgencyTemplate');
    select S_personnel.Nextval into S_PersonnelId from dual;
    insert into personnel
      select S_PersonnelId,
             C_Agency.id,
             C_Agency.Code || 'test',
             '7C4A8D09CA3762AF61E59520943DC26494F8941B',
             C_Agency.Code || '测试',
             1,
             null,
             null,
             sysdate,
             null,
             null,
             1,
             '系统管理员',
             sysdate,
             null,
             null,
             null,
             null
        from dual;
    insert into rolepersonnel
      select S_rolepersonnel.Nextval, S_RoleId, S_PersonnelId from dual;
      end loop;
  end;
  --给已存在的服务站创建角色，并创建测试人员
  declare
    cursor Cursor_Agency is
      select *
        from enterprise
       where EnterpriseCategoryId =
             (select id from EnterpriseCategory where name = '服务站');
    S_RoleId      number(9);
    S_PersonnelId number(9);
  begin
    for C_Agency in Cursor_Agency loop
      select S_Role.Nextval into S_RoleId from dual;
      insert into Role
        select S_RoleId,
               C_Agency.id,
               '服务站业务角色',
               0,
               1,
               null,
               1,
               '系统管理员',
               sysdate,
               null,
               null,
               null,
               null
          from dual;
      insert into rule
        select S_rule.Nextval, S_RoleId, EntNodeTemplateDetail.Nodeid
          from EntNodeTemplateDetail
         where EntNodeTemplateDetail.Entnodetemplateid =
               (select id from EntNodeTemplate where code = 'DealerTemplate');
      select S_personnel.Nextval into S_PersonnelId from dual;
      insert into personnel
        select S_PersonnelId,
               C_Agency.id,
               C_Agency.Code || 'test',
               '7C4A8D09CA3762AF61E59520943DC26494F8941B',
               C_Agency.Code || '测试',
               1,
               null,
               null,
               sysdate,
               null,
               null,
               1,
               '系统管理员',
               sysdate,
               null,
               null,
               null,
               null
          from dual;
      insert into rolepersonnel
        select S_rolepersonnel.Nextval, S_RoleId, S_PersonnelId from dual;
        end loop;
    end;
    --给已存在的供应商创建角色，并创建测试人员
    declare
      cursor Cursor_Agency is
        select *
          from enterprise
         where EnterpriseCategoryId =
               (select id from EnterpriseCategory where name = '配件供应商');
      S_RoleId      number(9);
      S_PersonnelId number(9);
    begin
      for C_Agency in Cursor_Agency loop
        select S_Role.Nextval into S_RoleId from dual;
        insert into Role
          select S_RoleId,
                 C_Agency.id,
                 '配件供应商业务角色',
                 0,
                 1,
                 null,
                 1,
                 '系统管理员',
                 sysdate,
                 null,
                 null,
                 null,
                 null
            from dual;
        insert into rule
          select S_rule.Nextval, S_RoleId, EntNodeTemplateDetail.Nodeid
            from EntNodeTemplateDetail
           where EntNodeTemplateDetail.Entnodetemplateid =
                 (select id
                    from EntNodeTemplate
                   where code = 'PartsSupplierTemplate');
        select S_personnel.Nextval into S_PersonnelId from dual;
        insert into personnel
          select S_PersonnelId,
                 C_Agency.id,
                 C_Agency.Code || 'test',
                 '7C4A8D09CA3762AF61E59520943DC26494F8941B',
                 C_Agency.Code || '测试',
                 1,
                 null,
                 null,
                 sysdate,
                 null,
                 null,
                 1,
                 '系统管理员',
                 sysdate,
                 null,
                 null,
                 null,
                 null
            from dual;
        insert into rolepersonnel
          select S_rolepersonnel.Nextval, S_RoleId, S_PersonnelId
            from dual;
            end loop;
      end;
