insert into CompanyInvoiceInfo
  select S_CompanyInvoiceInfo.Nextval,
         (select id
            from company
           where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
         x.customercompanyid,
         (select code from company where id = x.customercompanyid),
         (select Name from company where id = x.customercompanyid),
         0,
         1,
         null,
         null,
         1, --增值税发票
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         sysdate,
         1,
         '系统管理员',
         null,
         null,
         null,
         1,
         null
    from customerinformation x
    where not exists (select * from CompanyInvoiceInfo a where a.companyid=x.customercompanyid);
