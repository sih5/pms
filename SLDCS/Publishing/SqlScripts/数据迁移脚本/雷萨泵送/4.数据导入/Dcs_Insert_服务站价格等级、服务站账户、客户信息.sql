insert into CustomerInformation
  select S_CustomerInformation.Nextval,
         (select Id
            from branch
           where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
         id,
         null,
         null,
         1,
         1,
         '系统管理员',
         sysdate,
         null,
         null,
         null,
         null
    from dealer
   where not exists
   (select *
            from CustomerInformation
           where customerinformation.customercompanyid = dealer.id);


insert into CustomerOpenAccountApp
  select S_CustomerOpenAccountApp.Nextval,
         regexp_replace(regexp_replace(regexp_replace((select template
                                                        from codetemplate
                                                       where name =
                                                             'CustomerOpenAccountApp'),
                                                      '{CORPCODE}',
                                                      '247000'),
                                       '{DATE:yyyyMMdd}',
                                       to_char(sysdate, 'yyyymmdd')),
                        '{SERIAL:0000}',
                        lpad(to_char(GetSerial(1,
                                               trunc(sysdate, 'DD'),
                                               '247000')),
                             4,
                             '0')),
         (select id from AccountGroup where code = 'AG247000001' and salescompanyid =CustomerInformation.Salescompanyid),
         Customercompanyid,
         10000000,
         null,
         2,
         null,
         1,
         '系统管理员',
         sysdate,
         null,
         null,
         null,
         null,
         null,
         null,
         null
    from CustomerInformation
   where not exists (select *
            from CustomerOpenAccountApp
           where customerinformation.customercompanyid =
                 CustomerOpenAccountApp.Customercompanyid)
                 and exists (select * from AccountGroup where  code = 'AG247000001' and AccountGroup.Salescompanyid=CustomerInformation.Salescompanyid);

insert into CustomerAccount
  select S_CustomerAccount.Nextval,
         (select id from AccountGroup where code = 'AG247000001' and salescompanyid =CustomerInformation.Salescompanyid),
         Customercompanyid,
         10000000,
         0,
         0,
         0,
         1,
         1,
         '系统管理员',
         sysdate,
         null,
         null,
         null,
         null
    from CustomerInformation
   where not exists (select *
            from CustomerAccount
           where customerinformation.customercompanyid =
                 CustomerAccount.Customercompanyid)
                  and exists (select * from AccountGroup where  code = 'AG247000001' and AccountGroup.Salescompanyid=CustomerInformation.Salescompanyid);
                 
                 
                 
insert into CustomerAccountHisDetail
  select S_CustomerAccountHisDetail.Nextval,
         customercompanyid,
         10000000,
         sysdate,
         1, --应付帐业务类型  开户 1
         id,
         code,
         null
    from CustomerOpenAccountApp;

insert into CustomerOrderPriceGrade
  select S_CustomerOrderPriceGrade.Nextval,
         (select Id
            from branch
           where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
         (select id from partssalescategory where name = '雷萨泵送'),
         CustomerInformation.Customercompanyid,
         (select code
            from company
           where id = CustomerInformation.Customercompanyid),
         (select Name
            from company
           where id = CustomerInformation.Customercompanyid),
         PartsSalesOrderType.Id,
         PartsSalesOrderType.Code,
         PartsSalesOrderType.Name,
         1,
         1,
         null,
         1,
         '系统管理员',
         sysdate,
         null,
         null,
         null,
         null,
         null,
         null,
         null
    from CustomerInformation
   inner join PartsSalesOrderType
      on CustomerInformation.Salescompanyid = PartsSalesOrderType.Branchid
   where not exists (select *
            from CustomerOrderPriceGrade
           where CustomerOrderPriceGrade.Customercompanyid =
                 CustomerInformation.Customercompanyid);         

insert into CustomerOrderPriceGradeHistory
  select Id,
         BranchId,
         PartsSalesCategoryId,
         CustomerCompanyId,
         CustomerCompanyCode,
         CustomerCompanyName,
         PartsSalesOrderTypeId,
         PartsSalesOrderTypeCode,
         PartsSalesOrderTypeName,
         Coefficient,
         Status,
         Remark,
         CreatorId,
         CreatorName,
         CreateTime,
         null
    from CustomerOrderPriceGrade
   where not exists (select *
            from CustomerOrderPriceGradeHistory
           where CustomerOrderPriceGradeHistory.id =
                 CustomerOrderPriceGrade.Id);

