create table Temp_MalfunctionCategory
(
LV1 varchar2(500),
LV1Name varchar2(500),
LV2 varchar2(500),
LV2Name varchar2(500),
LV3 varchar2(500),
LV3Name varchar2(500),
LV4 varchar2(500),
LV4Name varchar2(500)
);

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BJ', '泵车技术整改', '10100', '泵车技术整改', '0', '泵车技术整改', 'BJ10100000', '泵车技术整改泵车技术整改');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '10000', '臂架总成', '0', '臂架总成', 'BB10000000', '臂架总成臂架总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '12000', '连杆总成', '0', '连杆总成', 'BB12000000', '连杆总成连杆总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '12000', '连杆总成', '10', '连杆一', 'BB12000010', '连杆总成连杆一');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '12000', '连杆总成', '20', '连杆二', 'BB12000020', '连杆总成连杆二');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '12000', '连杆总成', '30', '连杆三', 'BB12000030', '连杆总成连杆三');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '12000', '连杆总成', '40', '连杆四', 'BB12000040', '连杆总成连杆四');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '12000', '连杆总成', '50', '连杆五', 'BB12000050', '连杆总成连杆五');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '12000', '连杆总成', '60', '连杆六', 'BB12000060', '连杆总成连杆六');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '12000', '连杆总成', '70', '连杆七', 'BB12000070', '连杆总成连杆七');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '12000', '连杆总成', '80', '连杆八', 'BB12000080', '连杆总成连杆八');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '12000', '连杆总成', '90', '连杆九', 'BB12000090', '连杆总成连杆九');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '12000', '连杆总成', '100', '连杆十', 'BB12000100', '连杆总成连杆十');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '12000', '连杆总成', '110', '连杆十一', 'BB12000110', '连杆总成连杆十一');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '12000', '连杆总成', '120', '连杆十二', 'BB12000120', '连杆总成连杆十二');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '13000', '一节臂', '0', '一节臂', 'BB13000000', '一节臂一节臂');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '13000', '一节臂', '10', '臂架销轴', 'BB13000010', '一节臂臂架销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '13000', '一节臂', '20', '臂架销轴衬套', 'BB13000020', '一节臂臂架销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '13000', '一节臂', '30', '油缸销轴', 'BB13000030', '一节臂油缸销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '13000', '一节臂', '40', '油缸销轴衬套', 'BB13000040', '一节臂油缸销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '13000', '一节臂', '50', '连杆销轴', 'BB13000050', '一节臂连杆销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '13000', '一节臂', '60', '连杆销轴衬套', 'BB13000060', '一节臂连杆销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '13000', '一节臂', '70', '轴端挡板', 'BB13000070', '一节臂轴端挡板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '13000', '一节臂', '80', '泵管支撑', 'BB13000080', '一节臂泵管支撑');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '13000', '一节臂', '90', '臂架支撑', 'BB13000090', '一节臂臂架支撑');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '13000', '一节臂', '100', '臂架垫板', 'BB13000100', '一节臂臂架垫板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '14000', '二节臂', '0', '二节臂', 'BB14000000', '二节臂二节臂');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '14000', '二节臂', '10', '臂架销轴', 'BB14000010', '二节臂臂架销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '14000', '二节臂', '20', '臂架销轴衬套', 'BB14000020', '二节臂臂架销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '14000', '二节臂', '30', '油缸销轴', 'BB14000030', '二节臂油缸销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '14000', '二节臂', '40', '油缸销轴衬套', 'BB14000040', '二节臂油缸销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '14000', '二节臂', '50', '连杆销轴', 'BB14000050', '二节臂连杆销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '14000', '二节臂', '60', '连杆销轴衬套', 'BB14000060', '二节臂连杆销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '14000', '二节臂', '70', '轴端挡板', 'BB14000070', '二节臂轴端挡板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '14000', '二节臂', '80', '泵管支撑', 'BB14000080', '二节臂泵管支撑');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '14000', '二节臂', '90', '臂架支撑', 'BB14000090', '二节臂臂架支撑');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '14000', '二节臂', '100', '臂架垫板', 'BB14000100', '二节臂臂架垫板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '15000', '三节臂', '0', '三节臂', 'BB15000000', '三节臂三节臂');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '15000', '三节臂', '10', '臂架销轴', 'BB15000010', '三节臂臂架销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '15000', '三节臂', '20', '臂架销轴衬套', 'BB15000020', '三节臂臂架销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '15000', '三节臂', '30', '油缸销轴', 'BB15000030', '三节臂油缸销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '15000', '三节臂', '40', '油缸销轴衬套', 'BB15000040', '三节臂油缸销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '15000', '三节臂', '50', '连杆销轴', 'BB15000050', '三节臂连杆销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '15000', '三节臂', '60', '连杆销轴衬套', 'BB15000060', '三节臂连杆销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '15000', '三节臂', '70', '轴端挡板', 'BB15000070', '三节臂轴端挡板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '15000', '三节臂', '80', '泵管支撑', 'BB15000080', '三节臂泵管支撑');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '15000', '三节臂', '90', '臂架支撑', 'BB15000090', '三节臂臂架支撑');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '15000', '三节臂', '100', '臂架垫板', 'BB15000100', '三节臂臂架垫板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '16000', '四节臂', '0', '四节臂', 'BB16000000', '四节臂四节臂');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '16000', '四节臂', '10', '臂架销轴', 'BB16000010', '四节臂臂架销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '16000', '四节臂', '20', '臂架销轴衬套', 'BB16000020', '四节臂臂架销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '16000', '四节臂', '30', '油缸销轴', 'BB16000030', '四节臂油缸销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '16000', '四节臂', '40', '油缸销轴衬套', 'BB16000040', '四节臂油缸销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '16000', '四节臂', '50', '连杆销轴', 'BB16000050', '四节臂连杆销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '16000', '四节臂', '60', '连杆销轴衬套', 'BB16000060', '四节臂连杆销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '16000', '四节臂', '70', '轴端挡板', 'BB16000070', '四节臂轴端挡板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '16000', '四节臂', '80', '泵管支撑', 'BB16000080', '四节臂泵管支撑');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '16000', '四节臂', '90', '臂架支撑', 'BB16000090', '四节臂臂架支撑');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '16000', '四节臂', '100', '臂架垫板', 'BB16000100', '四节臂臂架垫板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '17000', '五节臂', '0', '五节臂', 'BB17000000', '五节臂五节臂');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '17000', '五节臂', '10', '臂架销轴', 'BB17000010', '五节臂臂架销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '17000', '五节臂', '20', '臂架销轴衬套', 'BB17000020', '五节臂臂架销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '17000', '五节臂', '30', '油缸销轴', 'BB17000030', '五节臂油缸销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '17000', '五节臂', '40', '油缸销轴衬套', 'BB17000040', '五节臂油缸销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '17000', '五节臂', '50', '连杆销轴', 'BB17000050', '五节臂连杆销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '17000', '五节臂', '60', '连杆销轴衬套', 'BB17000060', '五节臂连杆销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '17000', '五节臂', '70', '轴端挡板', 'BB17000070', '五节臂轴端挡板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '17000', '五节臂', '80', '泵管支撑', 'BB17000080', '五节臂泵管支撑');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '17000', '五节臂', '90', '臂架支撑', 'BB17000090', '五节臂臂架支撑');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '17000', '五节臂', '100', '臂架垫板', 'BB17000100', '五节臂臂架垫板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '18000', '六节臂', '0', '六节臂', 'BB18000000', '六节臂六节臂');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '18000', '六节臂', '10', '臂架销轴', 'BB18000010', '六节臂臂架销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '18000', '六节臂', '20', '臂架销轴衬套', 'BB18000020', '六节臂臂架销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '18000', '六节臂', '30', '油缸销轴', 'BB18000030', '六节臂油缸销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '18000', '六节臂', '40', '油缸销轴衬套', 'BB18000040', '六节臂油缸销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '18000', '六节臂', '50', '连杆销轴', 'BB18000050', '六节臂连杆销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '18000', '六节臂', '60', '连杆销轴衬套', 'BB18000060', '六节臂连杆销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '18000', '六节臂', '70', '轴端挡板', 'BB18000070', '六节臂轴端挡板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '18000', '六节臂', '80', '泵管支撑', 'BB18000080', '六节臂泵管支撑');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '18000', '六节臂', '90', '臂架支撑', 'BB18000090', '六节臂臂架支撑');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '18000', '六节臂', '100', '臂架垫板', 'BB18000100', '六节臂臂架垫板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '19000', '七节臂', '0', '七节臂', 'BB19000000', '七节臂七节臂');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '19000', '七节臂', '10', '臂架销轴', 'BB19000010', '七节臂臂架销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '19000', '七节臂', '20', '臂架销轴衬套', 'BB19000020', '七节臂臂架销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '19000', '七节臂', '30', '油缸销轴', 'BB19000030', '七节臂油缸销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '19000', '七节臂', '40', '油缸销轴衬套', 'BB19000040', '七节臂油缸销轴衬套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '19000', '七节臂', '50', '轴端挡板', 'BB19000050', '七节臂轴端挡板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '19000', '七节臂', '60', '泵管支撑', 'BB19000060', '七节臂泵管支撑');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BB', '泵车臂架系统', '19000', '七节臂', '70', '臂架垫板', 'BB19000070', '七节臂臂架垫板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '20000', '中心泵总成', '0', '中心泵总成', 'BS20000000', '中心泵总成中心泵总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '20000', '中心泵总成', '10', '左主油缸', 'BS20000010', '中心泵总成左主油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '20000', '中心泵总成', '20', '右主油缸', 'BS20000020', '中心泵总成右主油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '20000', '中心泵总成', '30', '连通块', 'BS20000030', '中心泵总成连通块');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '20000', '中心泵总成', '40', '限位油缸', 'BS20000040', '中心泵总成限位油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '20000', '中心泵总成', '50', '输送缸', 'BS20000050', '中心泵总成输送缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '20000', '中心泵总成', '60', '水槽', 'BS20000060', '中心泵总成水槽');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '20000', '中心泵总成', '70', '水槽盖板', 'BS20000070', '中心泵总成水槽盖板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '20000', '中心泵总成', '80', '感应套', 'BS20000080', '中心泵总成感应套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '20000', '中心泵总成', '90', '卡箍', 'BS20000090', '中心泵总成卡箍');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '20000', '中心泵总成', '100', '输送缸拉杆', 'BS20000100', '中心泵总成输送缸拉杆');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '20000', '中心泵总成', '110', '保护套', 'BS20000110', '中心泵总成保护套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '20000', '中心泵总成', '120', '推送头总成', 'BS20000120', '中心泵总成推送头总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '20000', '中心泵总成', '130', '砼活塞', 'BS20000130', '中心泵总成砼活塞');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '20000', '中心泵总成', '140', '压板', 'BS20000140', '中心泵总成压板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '20000', '中心泵总成', '150', '连接杆', 'BS20000150', '中心泵总成连接杆');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '20000', '中心泵总成', '160', '支撑环', 'BS20000160', '中心泵总成支撑环');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '0', 'S摆阀总成', 'BS21000000', 'S摆阀总成S摆阀总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '10', 'S管焊接总成', 'BS21000010', 'S摆阀总成S管焊接总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '20', '眼镜板', 'BS21000020', 'S摆阀总成眼镜板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '30', '切割环', 'BS21000030', 'S摆阀总成切割环');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '40', '橡胶弹簧', 'BS21000040', 'S摆阀总成橡胶弹簧');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '50', '出料口', 'BS21000050', 'S摆阀总成出料口');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '60', '耐磨套', 'BS21000060', 'S摆阀总成耐磨套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '70', '尼龙轴承', 'BS21000070', 'S摆阀总成尼龙轴承');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '80', '大端防尘圈', 'BS21000080', 'S摆阀总成大端防尘圈');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '90', '大轴承座', 'BS21000090', 'S摆阀总成大轴承座');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '100', '大端密封圈', 'BS21000100', 'S摆阀总成大端密封圈');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '110', '大端橡胶垫', 'BS21000110', 'S摆阀总成大端橡胶垫');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '120', '大端压盘', 'BS21000120', 'S摆阀总成大端压盘');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '130', '砼缸接头', 'BS21000130', 'S摆阀总成砼缸接头');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '140', '砼缸密封', 'BS21000140', 'S摆阀总成砼缸密封');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '150', '小端轴套', 'BS21000150', 'S摆阀总成小端轴套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '160', '小端套管', 'BS21000160', 'S摆阀总成小端套管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '170', '小端铜轴承', 'BS21000170', 'S摆阀总成小端铜轴承');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '180', '小端密封圈', 'BS21000180', 'S摆阀总成小端密封圈');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '21000', 'S摆阀总成', '190', '小端封盖', 'BS21000190', 'S摆阀总成小端封盖');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '0', '搅拌总成', 'BS22000000', '搅拌总成搅拌总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '10', '搅拌轴', 'BS22000010', '搅拌总成搅拌轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '20', '左搅拌叶片', 'BS22000020', '搅拌总成左搅拌叶片');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '30', '右搅拌叶片', 'BS22000030', '搅拌总成右搅拌叶片');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '40', '搅拌马达', 'BS22000040', '搅拌总成搅拌马达');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '50', '马达座', 'BS22000050', '搅拌总成马达座');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '60', '花键套', 'BS22000060', '搅拌总成花键套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '70', '左端盖', 'BS22000070', '搅拌总成左端盖');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '80', '轴承座', 'BS22000080', '搅拌总成轴承座');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '90', '左轴承', 'BS22000090', '搅拌总成左轴承');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '100', '搅拌轴密封', 'BS22000100', '搅拌总成搅拌轴密封');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '110', '连接座', 'BS22000110', '搅拌总成连接座');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '120', '密封挡圈', 'BS22000120', '搅拌总成密封挡圈');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '130', '挡圈压环', 'BS22000130', '搅拌总成挡圈压环');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '140', '轴套', 'BS22000140', '搅拌总成轴套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '150', '右轴承', 'BS22000150', '搅拌总成右轴承');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '160', '轴承压板', 'BS22000160', '搅拌总成轴承压板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '22000', '搅拌总成', '170', '右端盖', 'BS22000170', '搅拌总成右端盖');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '23000', '摆柄总成', '0', '摆柄总成', 'BS23000000', '摆柄总成摆柄总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '23000', '摆柄总成', '10', '左摆阀油缸', 'BS23000010', '摆柄总成左摆阀油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '23000', '摆柄总成', '20', '右摆阀油缸', 'BS23000020', '摆柄总成右摆阀油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '23000', '摆柄总成', '30', '摆臂', 'BS23000030', '摆柄总成摆臂');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '23000', '摆柄总成', '40', '摇臂球面轴承', 'BS23000040', '摆柄总成摇臂球面轴承');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '23000', '摆柄总成', '50', '座处球面轴承', 'BS23000050', '摆柄总成座处球面轴承');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '23000', '摆柄总成', '60', '球头挡板', 'BS23000060', '摆柄总成球头挡板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '23000', '摆柄总成', '70', '限位挡板', 'BS23000070', '摆柄总成限位挡板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '23000', '摆柄总成', '80', '承力板', 'BS23000080', '摆柄总成承力板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '23000', '摆柄总成', '90', '油杯', 'BS23000090', '摆柄总成油杯');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '24000', '料斗总成', '0', '料斗总成', 'BS24000000', '料斗总成料斗总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '24000', '料斗总成', '10', '料斗焊接总成', 'BS24000010', '料斗总成料斗焊接总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '24000', '料斗总成', '20', '筛网', 'BS24000020', '料斗总成筛网');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '24000', '料斗总成', '30', '料斗盖', 'BS24000030', '料斗总成料斗盖');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '24000', '料斗总成', '40', '摆缸护罩', 'BS24000040', '料斗总成摆缸护罩');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '24000', '料斗总成', '50', '灯架总成', 'BS24000050', '料斗总成灯架总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '24000', '料斗总成', '60', '料斗踏板', 'BS24000060', '料斗总成料斗踏板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '24000', '料斗总成', '70', '卸料门', 'BS24000070', '料斗总成卸料门');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '25000', '出料口', '0', '出料口', 'BS25000000', '出料口出料口');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '25000', '出料口', '10', '铰链弯管', 'BS25000010', '出料口铰链弯管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '25000', '出料口', '20', '销轴', 'BS25000020', '出料口销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '25000', '出料口', '30', '螺栓铰座', 'BS25000030', '出料口螺栓铰座');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '25000', '出料口', '40', '拉杆', 'BS25000040', '出料口拉杆');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '25000', '出料口', '50', '卡板', 'BS25000050', '出料口卡板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '25000', '出料口', '60', '密封圈', 'BS25000060', '出料口密封圈');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '26000', '润滑系统', '0', '润滑系统', 'BS26000000', '润滑系统润滑系统');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '26000', '润滑系统', '10', '同步润滑泵', 'BS26000010', '润滑系统同步润滑泵');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '26000', '润滑系统', '20', '邦迪管', 'BS26000020', '润滑系统邦迪管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '26000', '润滑系统', '30', '滤油器', 'BS26000030', '润滑系统滤油器');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '26000', '润滑系统', '40', '片式分油器', 'BS26000040', '润滑系统片式分油器');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '27000', '泵送系统连接', '0', '泵送系统连接', 'BS27000000', '泵送系统连接泵送系统连接');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '27000', '泵送系统连接', '10', '料斗安装板', 'BS27000010', '泵送系统连接料斗安装板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '27000', '泵送系统连接', '20', '铰链销轴', 'BS27000020', '泵送系统连接铰链销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '27000', '泵送系统连接', '30', '主油缸吊挂', 'BS27000030', '泵送系统连接主油缸吊挂');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '27000', '泵送系统连接', '40', 'U形管箍', 'BS27000040', '泵送系统连接U形管箍');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '27000', '泵送系统连接', '50', '吊板总成', 'BS27000050', '泵送系统连接吊板总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '27000', '泵送系统连接', '60', '连接筒焊接', 'BS27000060', '泵送系统连接连接筒焊接');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '27000', '泵送系统连接', '70', '吊挂连接环', 'BS27000070', '泵送系统连接吊挂连接环');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '27000', '泵送系统连接', '80', '挂板总成', 'BS27000080', '泵送系统连接挂板总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '27000', '泵送系统连接', '90', '销轴', 'BS27000090', '泵送系统连接销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BS', '泵车泵送系统', '27000', '泵送系统连接', '100', '铰接总成', 'BS27000100', '泵送系统连接铰接总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30003', '泵送机构布管', '0', '泵送机构布管', 'BY30003000', '泵送机构布管泵送机构布管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30003', '泵送机构布管', '10', '怠速动臂架阀', 'BY30003010', '泵送机构布管怠速动臂架阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30003', '泵送机构布管', '20', '防溜缸阀', 'BY30003020', '泵送机构布管防溜缸阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30003', '泵送机构布管', '30', '对开法兰', 'BY30003030', '泵送机构布管对开法兰');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30003', '泵送机构布管', '40', '钢管', 'BY30003040', '泵送机构布管钢管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30003', '泵送机构布管', '50', '油缸', 'BY30003050', '泵送机构布管油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '36000', '组合阀总成', '70', '测压接头', 'BY36000070', '组合阀总成测压接头');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '36000', '组合阀总成', '80', '溢流阀', 'BY36000080', '组合阀总成溢流阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '36000', '组合阀总成', '90', '电磁阀', 'BY36000090', '组合阀总成电磁阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '36000', '组合阀总成', '100', '主阀块油管', 'BY36000100', '组合阀总成主阀块油管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '36000', '组合阀总成', '110', '胶管', 'BY36000110', '组合阀总成胶管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '36000', '组合阀总成', '120', '钢管', 'BY36000120', '组合阀总成钢管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '36000', '组合阀总成', '130', '管接头', 'BY36000130', '组合阀总成管接头');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '37000', '支撑台液压系统', '0', '支撑台液压布管', 'BY37000000', '支撑台液压系统支撑台液压布管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '37000', '支撑台液压系统', '10', '旋转缓冲阀', 'BY37000010', '支撑台液压系统旋转缓冲阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '37000', '支撑台液压系统', '20', '胶管', 'BY37000020', '支撑台液压系统胶管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '37000', '支撑台液压系统', '30', '钢管', 'BY37000030', '支撑台液压系统钢管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '38000', '冷却液压系统', '0', '冷却液压系统', 'BY38000000', '冷却液压系统冷却液压系统');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '38000', '冷却液压系统', '10', '风冷器总成', 'BY38000010', '冷却液压系统风冷器总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '38000', '冷却液压系统', '20', '风冷器马达', 'BY38000020', '冷却液压系统风冷器马达');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '38000', '冷却液压系统', '30', '集油阀总成', 'BY38000030', '冷却液压系统集油阀总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '38000', '冷却液压系统', '40', '回油过滤器', 'BY38000040', '冷却液压系统回油过滤器');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '38000', '冷却液压系统', '50', '冷却器开启阀', 'BY38000050', '冷却液压系统冷却器开启阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '38000', '冷却液压系统', '60', '溢流阀', 'BY38000060', '冷却液压系统溢流阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '38000', '冷却液压系统', '70', '胶管', 'BY38000070', '冷却液压系统胶管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '38000', '冷却液压系统', '80', '钢管', 'BY38000080', '冷却液压系统钢管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '38000', '冷却液压系统', '90', '管接头', 'BY38000090', '冷却液压系统管接头');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '39000', '液压油箱总成', '0', '液压油箱总成', 'BY39000000', '液压油箱总成液压油箱总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '39000', '液压油箱总成', '10', '油箱焊接体', 'BY39000010', '液压油箱总成油箱焊接体');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '39000', '液压油箱总成', '20', '安装座', 'BY39000020', '液压油箱总成安装座');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '39000', '液压油箱总成', '30', '带锁空气滤清器', 'BY39000030', '液压油箱总成带锁空气滤清器');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '39000', '液压油箱总成', '40', '法兰盖板', 'BY39000040', '液压油箱总成法兰盖板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '39000', '液压油箱总成', '50', '油箱踏脚板', 'BY39000050', '液压油箱总成油箱踏脚板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '39000', '液压油箱总成', '60', '吊环', 'BY39000060', '液压油箱总成吊环');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '39000', '液压油箱总成', '70', '液位液温计', 'BY39000070', '液压油箱总成液位液温计');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '39000', '液压油箱总成', '80', '放油球阀', 'BY39000080', '液压油箱总成放油球阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '39000', '液压油箱总成', '90', '油管', 'BY39000090', '液压油箱总成油管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '39000', '液压油箱总成', '100', '钢管', 'BY39000100', '液压油箱总成钢管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '39000', '液压油箱总成', '110', '管接头', 'BY39000110', '液压油箱总成管接头');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '70000', '水路系统', '0', '水路系统', 'BY70000000', '水路系统水路系统');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '70000', '水路系统', '10', '水泵', 'BY70000010', '水路系统水泵');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '70000', '水路系统', '20', '水泵电机', 'BY70000020', '水路系统水泵电机');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '70000', '水路系统', '30', '水泵马达', 'BY70000030', '水路系统水泵马达');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '70000', '水路系统', '40', '水管', 'BY70000040', '水路系统水管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '70000', '水路系统', '50', '水枪', 'BY70000050', '水路系统水枪');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '70000', '水路系统', '60', '油管', 'BY70000060', '水路系统油管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '70000', '水路系统', '70', '接头', 'BY70000070', '水路系统接头');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '70000', '水路系统', '80', '球阀', 'BY70000080', '水路系统球阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '70000', '水路系统', '90', '水泵支架', 'BY70000090', '水路系统水泵支架');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '70000', '水路系统', '100', '过滤器', 'BY70000100', '水路系统过滤器');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '40000', '转台总成', '0', '转台总成', 'BZ40000000', '转台总成转台总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '40000', '转台总成', '10', '转台焊接体', 'BZ40000010', '转台总成转台焊接体');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '40000', '转台总成', '20', '小盖板', 'BZ40000020', '转台总成小盖板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '40000', '转台总成', '30', '油管固定板', 'BZ40000030', '转台总成油管固定板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '41000', '支撑台总成', '0', '支撑台总成', 'BZ41000000', '支撑台总成支撑台总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '41000', '支撑台总成', '10', '支持台焊接体', 'BZ41000010', '支撑台总成支持台焊接体');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '41000', '支撑台总成', '20', '旋转减速机安装台', 'BZ41000020', '支撑台总成旋转减速机安装台');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '41000', '支撑台总成', '30', '泵管安装架', 'BZ41000030', '支撑台总成泵管安装架');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '41000', '支撑台总成', '40', '电控箱门', 'BZ41000040', '支撑台总成电控箱门');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '41000', '支撑台总成', '50', '臂架多路阀门', 'BZ41000050', '支撑台总成臂架多路阀门');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '41000', '支撑台总成', '60', '气弹簧', 'BZ41000060', '支撑台总成气弹簧');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '41000', '支撑台总成', '70', '前支腿滚轮', 'BZ41000070', '支撑台总成前支腿滚轮');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '41000', '支撑台总成', '80', '支腿展开油缸座', 'BZ41000080', '支撑台总成支腿展开油缸座');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '42000', '前支腿总成', '0', '前支腿总成', 'BZ42000000', '前支腿总成前支腿总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '42000', '前支腿总成', '10', '左前一级支腿', 'BZ42000010', '前支腿总成左前一级支腿');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '42000', '前支腿总成', '20', '左前二级支腿', 'BZ42000020', '前支腿总成左前二级支腿');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '42000', '前支腿总成', '30', '右前一级支腿', 'BZ42000030', '前支腿总成右前一级支腿');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '42000', '前支腿总成', '40', '右前二级支腿', 'BZ42000040', '前支腿总成右前二级支腿');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '42000', '前支腿总成', '50', '支腿销轴', 'BZ42000050', '前支腿总成支腿销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '42000', '前支腿总成', '60', '销轴挡板', 'BZ42000060', '前支腿总成销轴挡板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '42000', '前支腿总成', '70', '伸缩油缸销轴', 'BZ42000070', '前支腿总成伸缩油缸销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '42000', '前支腿总成', '80', '展开油缸销轴', 'BZ42000080', '前支腿总成展开油缸销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '42000', '前支腿总成', '90', '圆盖板', 'BZ42000090', '前支腿总成圆盖板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '42000', '前支腿总成', '100', '滚轮', 'BZ42000100', '前支腿总成滚轮');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '44000', '后支腿总成', '0', '后支腿总成', 'BZ44000000', '后支腿总成后支腿总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '44000', '后支腿总成', '10', '支腿销轴', 'BZ44000010', '后支腿总成支腿销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '44000', '后支腿总成', '20', '展开油缸销轴', 'BZ44000020', '后支腿总成展开油缸销轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '44000', '后支腿总成', '30', '销轴挡板', 'BZ44000030', '后支腿总成销轴挡板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '44000', '后支腿总成', '40', '支腿锁', 'BZ44000040', '后支腿总成支腿锁');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '44000', '后支腿总成', '50', '圆盖板', 'BZ44000050', '后支腿总成圆盖板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '45000', '回转机构总成 ', '0', '回转机构', 'BZ45000000', '回转机构总成 回转机构');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '45000', '回转机构总成 ', '10', '旋转减速机', 'BZ45000010', '回转机构总成 旋转减速机');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '45000', '回转机构总成 ', '20', '回转支撑（大齿轮）', 'BZ45000020', '回转机构总成 回转支撑（大齿轮）');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '45000', '回转机构总成 ', '30', '高强度大六角螺栓', 'BZ45000030', '回转机构总成 高强度大六角螺栓');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '45000', '回转机构总成 ', '40', '高强度垫圈', 'BZ45000040', '回转机构总成 高强度垫圈');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '45000', '回转机构总成 ', '50', '高强度大六角螺母', 'BZ45000050', '回转机构总成 高强度大六角螺母');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '45000', '回转机构总成 ', '60', '小齿轮', 'BZ45000060', '回转机构总成 小齿轮');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '45000', '回转机构总成 ', '70', '惰轮', 'BZ45000070', '回转机构总成 惰轮');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '45000', '回转机构总成 ', '80', '惰轮轴', 'BZ45000080', '回转机构总成 惰轮轴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '45000', '回转机构总成 ', '90', '惰轮铜套', 'BZ45000090', '回转机构总成 惰轮铜套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '45000', '回转机构总成 ', '100', '惰轮轴挡板', 'BZ45000100', '回转机构总成 惰轮轴挡板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '45000', '回转机构总成 ', '110', '盖板', 'BZ45000110', '回转机构总成 盖板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '45000', '回转机构总成 ', '120', '轮滑油杯', 'BZ45000120', '回转机构总成 轮滑油杯');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '46000', '回转支承罩总成', '0', '回转支撑罩总成', 'BZ46000000', '回转支承罩总成回转支撑罩总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '46000', '回转支承罩总成', '10', '齿轮护罩', 'BZ46000010', '回转支承罩总成齿轮护罩');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '46000', '回转支承罩总成', '20', '护罩安装板', 'BZ46000020', '回转支承罩总成护罩安装板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '46000', '回转支承罩总成', '30', '侧板', 'BZ46000030', '回转支承罩总成侧板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '46000', '回转支承罩总成', '40', '盖板', 'BZ46000040', '回转支承罩总成盖板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '48000', '回转润滑', '0', '回转润滑', 'BZ48000000', '回转润滑回转润滑');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '48000', '回转润滑', '10', '黄油嘴', 'BZ48000010', '回转润滑黄油嘴');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '48000', '回转润滑', '20', '铜管', 'BZ48000020', '回转润滑铜管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BZ', '泵车转台及支腿', '48000', '回转润滑', '30', '接头', 'BZ48000030', '回转润滑接头');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '50000', '下车输送管', '0', '下车输送管', 'BS50000000', '下车输送管下车输送管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '50000', '下车输送管', '10', '180大弯管', 'BS50000010', '下车输送管180大弯管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '50000', '下车输送管', '20', '变径管', 'BS50000020', '下车输送管变径管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '50000', '下车输送管', '30', '直管', 'BS50000030', '下车输送管直管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '50000', '下车输送管', '40', '弯管', 'BS50000040', '下车输送管弯管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '50000', '下车输送管', '50', '管卡', 'BS50000050', '下车输送管管卡');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '50000', '下车输送管', '60', '管卡密封圈', 'BS50000060', '下车输送管管卡密封圈');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '50000', '下车输送管', '70', 'U型管卡', 'BS50000070', '下车输送管U型管卡');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '51000', '上车输送管', '0', '上车输送管', 'BS51000000', '上车输送管上车输送管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '51000', '上车输送管', '10', '弯管', 'BS51000010', '上车输送管弯管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '51000', '上车输送管', '20', '直管', 'BS51000020', '上车输送管直管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '51000', '上车输送管', '30', '管卡', 'BS51000030', '上车输送管管卡');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '51000', '上车输送管', '40', '管卡密封圈', 'BS51000040', '上车输送管管卡密封圈');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '51000', '上车输送管', '50', 'U型管卡', 'BS51000050', '上车输送管U型管卡');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '51000', '上车输送管', '60', '软管安全绳', 'BS51000060', '上车输送管软管安全绳');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '52000', '下车输送管支架', '0', '下车输送管支架', 'BS52000000', '下车输送管支架下车输送管支架');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '52000', '下车输送管支架', '10', '固定架', 'BS52000010', '下车输送管支架固定架');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '52000', '下车输送管支架', '20', '支架', 'BS52000020', '下车输送管支架支架');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '52000', '下车输送管支架', '30', '固定板', 'BS52000030', '下车输送管支架固定板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BG', '泵车输送管系统', '52000', '下车输送管支架', '40', '垫板', 'BS52000040', '下车输送管支架垫板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '53000', '副车架总成', '0', '副车架总成', 'BF53000000', '副车架总成副车架总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '53000', '副车架总成', '10', '挑梁', 'BF53000010', '副车架总成挑梁');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '53000', '副车架总成', '20', '斜拉梁', 'BF53000020', '副车架总成斜拉梁');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '53000', '副车架总成', '30', '支腿限位锁', 'BF53000030', '副车架总成支腿限位锁');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '53000', '副车架总成', '40', '摆缸阀罩', 'BF53000040', '副车架总成摆缸阀罩');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '54000', '布料杆支架总成', '0', '布料杆支架总成', 'BF54000000', '布料杆支架总成布料杆支架总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '54000', '布料杆支架总成', '10', '龙门支架焊接总成', 'BF54000010', '布料杆支架总成龙门支架焊接总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '54000', '布料杆支架总成', '20', '尼龙垫', 'BF54000020', '布料杆支架总成尼龙垫');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '54000', '布料杆支架总成', '30', '辅助支撑焊接总成', 'BF54000030', '布料杆支架总成辅助支撑焊接总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '55000', '梯子', '0', '梯子', 'BF55000000', '梯子梯子');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '55000', '梯子', '10', '踏板', 'BF55000010', '梯子踏板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '55000', '梯子', '20', '踏板支架', 'BF55000020', '梯子踏板支架');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '56000', '下车护栏', '0', '下车护栏', 'BF56000000', '下车护栏下车护栏');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '56000', '下车护栏', '10', '护栏支架', 'BF56000010', '下车护栏护栏支架');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '56000', '下车护栏', '20', '护栏杆', 'BF56000020', '下车护栏护栏杆');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '57000', '工具箱', '0', '工具箱', 'BF57000000', '工具箱工具箱');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '58000', '泵车附件', '0', '泵车附件', 'BF58000000', '泵车附件泵车附件');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '58000', '泵车附件', '10', '后桥挡泥板', 'BF58000010', '泵车附件后桥挡泥板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '58000', '泵车附件', '20', '水平仪', 'BF58000020', '泵车附件水平仪');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '58000', '泵车附件', '30', '支腿垫板支架', 'BF58000030', '泵车附件支腿垫板支架');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '58000', '泵车附件', '40', '挡车器', 'BF58000040', '泵车附件挡车器');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '58000', '泵车附件', '50', '挡车器挂板', 'BF58000050', '泵车附件挡车器挂板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '59000', '走台总成', '0', '走台总成', 'BF59000000', '走台总成走台总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '59000', '走台总成', '10', '左走台', 'BF59000010', '走台总成左走台');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '59000', '走台总成', '20', '右走台', 'BF59000020', '走台总成右走台');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '59000', '走台总成', '30', '扶手', 'BF59000030', '走台总成扶手');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '80000', '底盘改制', '0', '底盘改制', 'BF80000000', '底盘改制底盘改制');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '80000', '底盘改制', '10', '连接板', 'BF80000010', '底盘改制连接板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '80000', '底盘改制', '20', '主梁焊板', 'BF80000020', '底盘改制主梁焊板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '80000', '底盘改制', '30', '安装板', 'BF80000030', '底盘改制安装板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '82000', '分动箱总成', '0', '分动箱总成', 'BF82000000', '分动箱总成分动箱总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '82000', '分动箱总成', '10', '安装板', 'BF82000010', '分动箱总成安装板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '82000', '分动箱总成', '20', '连接板', 'BF82000020', '分动箱总成连接板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '82000', '分动箱总成', '30', '电磁阀', 'BF82000030', '分动箱总成电磁阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '82000', '分动箱总成', '40', '气缸', 'BF82000040', '分动箱总成气缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '82000', '分动箱总成', '50', '气动二联件', 'BF82000050', '分动箱总成气动二联件');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '82000', '分动箱总成', '60', '气阀', 'BF82000060', '分动箱总成气阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '82000', '分动箱总成', '70', '油泵吊挂', 'BF82000070', '分动箱总成油泵吊挂');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '82000', '分动箱总成', '80', '通气器', 'BF82000080', '分动箱总成通气器');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '82000', '分动箱总成', '90', '加油杯', 'BF82000090', '分动箱总成加油杯');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '93000', '随机工具', '0', '随车工具', 'BF93000000', '随机工具随车工具');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BF', '泵车副车架及附件', '94000', '随车附件', '0', '随车附件', 'BF94000000', '随车附件随车附件');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '61000', '电气系统总布置', '0', '电气系统总布置', 'BD61000000', '电气系统总布置电气系统总布置');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '61000', '电气系统总布置', '10', '电控箱底座', 'BD61000010', '电气系统总布置电控箱底座');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '61000', '电气系统总布置', '20', '臂架限位开关', 'BD61000020', '电气系统总布置臂架限位开关');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '61000', '电气系统总布置', '30', '旋转编码器', 'BD61000030', '电气系统总布置旋转编码器');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '61000', '电气系统总布置', '40', '旋转限位开关', 'BD61000040', '电气系统总布置旋转限位开关');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '61000', '电气系统总布置', '50', '支腿限位开关', 'BD61000050', '电气系统总布置支腿限位开关');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '61000', '电气系统总布置', '60', '水槽接近感应器', 'BD61000060', '电气系统总布置水槽接近感应器');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '62000', '遥控器系统', '0', '遥控器系统', 'BD62000000', '遥控器系统遥控器系统');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '62000', '遥控器系统', '10', '遥控器发射器', 'BD62000010', '遥控器系统遥控器发射器');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '62000', '遥控器系统', '20', '遥控器接收机', 'BD62000020', '遥控器系统遥控器接收机');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '62000', '遥控器系统', '30', '电池', 'BD62000030', '遥控器系统电池');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '62000', '遥控器系统', '40', '充电器', 'BD62000040', '遥控器系统充电器');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '62000', '遥控器系统', '50', '天线', 'BD62000050', '遥控器系统天线');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '62000', '遥控器系统', '60', '总线线束', 'BD62000060', '遥控器系统总线线束');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '62000', '遥控器系统', '70', '电源线', 'BD62000070', '遥控器系统电源线');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30003', '泵送机构布管', '60', '管接头', 'BY30003060', '泵送机构布管管接头');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30003', '泵送机构布管', '70', '主油缸连通管', 'BY30003070', '泵送机构布管主油缸连通管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30001', '吸回油布管', '0', '吸回油布管', 'BY30001000', '吸回油布管吸回油布管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30001', '吸回油布管', '10', '吸油过滤器', 'BY30001010', '吸回油布管吸油过滤器');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30001', '吸回油布管', '20', '吸油管', 'BY30001020', '吸回油布管吸油管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30001', '吸回油布管', '30', '真空表', 'BY30001030', '吸回油布管真空表');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30001', '吸回油布管', '40', '测压管', 'BY30001040', '吸回油布管测压管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30001', '吸回油布管', '50', '测压接头', 'BY30001050', '吸回油布管测压接头');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30001', '吸回油布管', '60', '管接头', 'BY30001060', '吸回油布管管接头');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30001', '吸回油布管', '70', '对开法兰', 'BY30001070', '吸回油布管对开法兰');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30001', '吸回油布管', '80', '球阀', 'BY30001080', '吸回油布管球阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30001', '吸回油布管', '90', '胶管', 'BY30001090', '吸回油布管胶管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30001', '吸回油布管', '100', '钢管', 'BY30001100', '吸回油布管钢管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30004', '控制油路布管', '0', '控制油路布管', 'BY30004000', '控制油路布管控制油路布管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30004', '控制油路布管', '10', '压力表', 'BY30004010', '控制油路布管压力表');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30004', '控制油路布管', '20', '测压软管', 'BY30004020', '控制油路布管测压软管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30004', '控制油路布管', '30', '软管', 'BY30004030', '控制油路布管软管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '30004', '控制油路布管', '40', '胶管', 'BY30004040', '控制油路布管胶管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '31000', 'S管及分配阀液压系统', '0', 'S管及分配阀液压系统', 'BY31000000', 'S管及分配阀液压系统S管及分配阀液压系统');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '31000', 'S管及分配阀液压系统', '10', '蓄能器', 'BY31000010', 'S管及分配阀液压系统蓄能器');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '31000', 'S管及分配阀液压系统', '20', '电磁换向阀', 'BY31000020', 'S管及分配阀液压系统电磁换向阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '31000', 'S管及分配阀液压系统', '30', '溢流阀', 'BY31000030', 'S管及分配阀液压系统溢流阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '31000', 'S管及分配阀液压系统', '40', '液动阀', 'BY31000040', 'S管及分配阀液压系统液动阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '31000', 'S管及分配阀液压系统', '50', '分配阀总成', 'BY31000050', 'S管及分配阀液压系统分配阀总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '31000', 'S管及分配阀液压系统', '60', '插装阀', 'BY31000060', 'S管及分配阀液压系统插装阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '31000', 'S管及分配阀液压系统', '70', '泄压阀', 'BY31000070', 'S管及分配阀液压系统泄压阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '31000', 'S管及分配阀液压系统', '80', '油压表', 'BY31000080', 'S管及分配阀液压系统油压表');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '32000', '搅拌及水泵液压系统', '0', '搅拌及水泵液压系统', 'BY32000000', '搅拌及水泵液压系统搅拌及水泵液压系统');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '32000', '搅拌及水泵液压系统', '10', '搅拌阀', 'BY32000010', '搅拌及水泵液压系统搅拌阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '32000', '搅拌及水泵液压系统', '20', '搅拌阀支架', 'BY32000020', '搅拌及水泵液压系统搅拌阀支架');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '32000', '搅拌及水泵液压系统', '30', '塑料护套', 'BY32000030', '搅拌及水泵液压系统塑料护套');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '32000', '搅拌及水泵液压系统', '40', '胶管', 'BY32000040', '搅拌及水泵液压系统胶管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '32000', '搅拌及水泵液压系统', '50', '钢管管接头', 'BY32000050', '搅拌及水泵液压系统钢管管接头');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '33000', '臂架液压系统', '0', '臂架液压系统', 'BY33000000', '臂架液压系统臂架液压系统');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '33000', '臂架液压系统', '10', '多路阀', 'BY33000010', '臂架液压系统多路阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '33000', '臂架液压系统', '20', '高压滤清器', 'BY33000020', '臂架液压系统高压滤清器');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '33000', '臂架液压系统', '30', '单向阀', 'BY33000030', '臂架液压系统单向阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '33000', '臂架液压系统', '40', '油压表', 'BY33000040', '臂架液压系统油压表');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '33000', '臂架液压系统', '50', '臂架平衡阀', 'BY33000050', '臂架液压系统臂架平衡阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '33000', '臂架液压系统', '60', '一臂油缸', 'BY33000060', '臂架液压系统一臂油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '33000', '臂架液压系统', '70', '二臂油缸', 'BY33000070', '臂架液压系统二臂油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '33000', '臂架液压系统', '80', '三臂油缸', 'BY33000080', '臂架液压系统三臂油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '33000', '臂架液压系统', '90', '四臂油缸', 'BY33000090', '臂架液压系统四臂油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '33000', '臂架液压系统', '100', '五臂油缸', 'BY33000100', '臂架液压系统五臂油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '33000', '臂架液压系统', '110', '六臂油缸', 'BY33000110', '臂架液压系统六臂油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '33000', '臂架液压系统', '120', '七臂油缸', 'BY33000120', '臂架液压系统七臂油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '33000', '臂架液压系统', '130', '胶管', 'BY33000130', '臂架液压系统胶管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '33000', '臂架液压系统', '140', '钢管', 'BY33000140', '臂架液压系统钢管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '33000', '臂架液压系统', '150', '管接头', 'BY33000150', '臂架液压系统管接头');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '34000', '全套液压泵', '0', '全套液压泵', 'BY34000000', '全套液压泵全套液压泵');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '34000', '全套液压泵', '10', '前主油泵', 'BY34000010', '全套液压泵前主油泵');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '34000', '全套液压泵', '20', '前主油泵油管', 'BY34000020', '全套液压泵前主油泵油管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '34000', '全套液压泵', '30', '前主油泵油管法兰', 'BY34000030', '全套液压泵前主油泵油管法兰');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '34000', '全套液压泵', '40', '后主油泵', 'BY34000040', '全套液压泵后主油泵');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '34000', '全套液压泵', '50', '前主油泵油管', 'BY34000050', '全套液压泵前主油泵油管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '34000', '全套液压泵', '60', '前主油泵油管法兰', 'BY34000060', '全套液压泵前主油泵油管法兰');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '34000', '全套液压泵', '70', '恒压泵', 'BY34000070', '全套液压泵恒压泵');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '34000', '全套液压泵', '80', '恒压泵油管', 'BY34000080', '全套液压泵恒压泵油管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '34000', '全套液压泵', '90', '恒压泵油管法兰', 'BY34000090', '全套液压泵恒压泵油管法兰');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '34000', '全套液压泵', '100', '齿轮泵', 'BY34000100', '全套液压泵齿轮泵');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '34000', '全套液压泵', '110', '齿轮泵油管', 'BY34000110', '全套液压泵齿轮泵油管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '34000', '全套液压泵', '120', '齿轮泵油管法兰', 'BY34000120', '全套液压泵齿轮泵油管法兰');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '34000', '全套液压泵', '130', '臂架泵', 'BY34000130', '全套液压泵臂架泵');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '34000', '全套液压泵', '140', '臂架泵油管', 'BY34000140', '全套液压泵臂架泵油管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '34000', '全套液压泵', '150', '臂架泵油管法兰', 'BY34000150', '全套液压泵臂架泵油管法兰');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '35000', '支腿液压系统', '0', '支腿液压系统', 'BY35000000', '支腿液压系统支腿液压系统');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '35000', '支腿液压系统', '10', '前支腿展开油缸', 'BY35000010', '支腿液压系统前支腿展开油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '35000', '支腿液压系统', '20', '前支腿垂直油缸', 'BY35000020', '支腿液压系统前支腿垂直油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '35000', '支腿液压系统', '30', '前支腿伸缩油缸', 'BY35000030', '支腿液压系统前支腿伸缩油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '35000', '支腿液压系统', '40', '后支腿展开油缸', 'BY35000040', '支腿液压系统后支腿展开油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '35000', '支腿液压系统', '50', '后支腿垂直油缸', 'BY35000050', '支腿液压系统后支腿垂直油缸');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '35000', '支腿液压系统', '60', '支腿油缸垫板', 'BY35000060', '支腿液压系统支腿油缸垫板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '35000', '支腿液压系统', '70', '胶管', 'BY35000070', '支腿液压系统胶管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '35000', '支腿液压系统', '80', '钢管', 'BY35000080', '支腿液压系统钢管');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '35000', '支腿液压系统', '90', '管接头', 'BY35000090', '支腿液压系统管接头');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '35000', '支腿液压系统', '100', '支腿平衡阀', 'BY35000100', '支腿液压系统支腿平衡阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '35000', '支腿液压系统', '110', '支腿多路阀', 'BY35000110', '支腿液压系统支腿多路阀');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '36000', '组合阀总成', '0', '组合阀总成', 'BY36000000', '组合阀总成组合阀总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '36000', '组合阀总成', '10', '主阀快安装', 'BY36000010', '组合阀总成主阀快安装');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '36000', '组合阀总成', '20', '主阀快', 'BY36000020', '组合阀总成主阀快');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '36000', '组合阀总成', '30', '主阀块堵头', 'BY36000030', '组合阀总成主阀块堵头');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '36000', '组合阀总成', '40', '主阀块盖板', 'BY36000040', '组合阀总成主阀块盖板');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '36000', '组合阀总成', '50', '主阀块街头', 'BY36000050', '组合阀总成主阀块街头');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BY', '泵车液压系统', '36000', '组合阀总成', '60', '比例阀总成', 'BY36000060', '组合阀总成比例阀总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '63000', '电气控制系统总成', '0', '电气控制系统总成', 'BD63000000', '电气控制系统总成电气控制系统总成');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '63000', '电气控制系统总成', '10', '主阀块线束', 'BD63000010', '电气控制系统总成主阀块线束');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '63000', '电气控制系统总成', '20', '多路阀线束', 'BD63000020', '电气控制系统总成多路阀线束');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '63000', '电气控制系统总成', '30', '调试盒线束', 'BD63000030', '电气控制系统总成调试盒线束');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '63000', '电气控制系统总成', '40', '近控操作盒线束', 'BD63000040', '电气控制系统总成近控操作盒线束');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '63000', '电气控制系统总成', '50', '支腿操作盒线束', 'BD63000050', '电气控制系统总成支腿操作盒线束');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '63000', '电气控制系统总成', '60', '电控柜线束', 'BD63000060', '电气控制系统总成电控柜线束');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '63000', '电气控制系统总成', '70', 'PLC模块', 'BD63000070', '电气控制系统总成PLC模块');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '63000', '电气控制系统总成', '80', '主控制器', 'BD63000080', '电气控制系统总成主控制器');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '63000', '电气控制系统总成', '90', '显示器', 'BD63000090', '电气控制系统总成显示器');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '64000', 'GPS系统', '0', 'GPS系统', 'BD64000000', 'GPS系统GPS系统');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '64000', 'GPS系统', '10', 'GPS', 'BD64000010', 'GPS系统GPS');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '64000', 'GPS系统', '20', '线束', 'BD64000020', 'GPS系统线束');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '64000', 'GPS系统', '30', 'GSM天线', 'BD64000030', 'GPS系统GSM天线');

insert into Temp_MalfunctionCategory (LV1, LV1NAME, LV2, LV2NAME, LV3, LV3NAME, LV4, LV4NAME)
values ('BD', '泵车电气系统', '64000', 'GPS系统', '40', 'GPS天线', 'BD64000040', 'GPS系统GPS天线');


--插入总成
insert into MalfunctionCategory
  select S_MalfunctionCategory.Nextval,
         (select id from MalfunctionCategory a where a.layernodetypeid = 4) parentid, --父分类
         (select id from MalfunctionCategory a where a.layernodetypeid = 4) RootGroupId, --根分类Id
         LV1,
         LV1Name,
         5, --总成
         1,
         null,
         1,
         '系统管理员',
         sysdate,
         null,
         null,
         null,
         null
    from (select distinct LV1,LV1Name  from Temp_MalfunctionCategory);

--插入子组
insert into MalfunctionCategory
  select S_MalfunctionCategory.Nextval,
         (select id from MalfunctionCategory a where Code=LV1) parentid, --父分类
         (select id from MalfunctionCategory a where a.layernodetypeid = 4) RootGroupId, --根分类Id
         LV2,
         LV2Name,
         5, --总成
         1,
         null,
         1,
         '系统管理员',
         sysdate,
         null,
         null,
         null,
         null
    from (select distinct LV1,LV1Name,LV2,LV2Name  from Temp_MalfunctionCategory);

--插入零件
insert into MalfunctionCategory
  select S_MalfunctionCategory.Nextval,
         (select id from MalfunctionCategory a where Code=LV2) parentid, --父分类
         (select id from MalfunctionCategory a where a.layernodetypeid = 4) RootGroupId, --根分类Id
         LV3,
         LV3Name,
         5, --总成
         1,
         null,
         1,
         '系统管理员',
         sysdate,
         null,
         null,
         null,
         null
    from (select distinct LV1,LV1Name,LV2,LV2Name,LV3,LV3Name  from Temp_MalfunctionCategory);

--插入车辆组成
insert into MalfunctionCategory
  select S_MalfunctionCategory.Nextval,
         (select id from MalfunctionCategory a where Code=LV3 and parentid=(select id from MalfunctionCategory b where b.code =LV2)) parentid, --父分类
         (select id from MalfunctionCategory a where a.layernodetypeid = 4) RootGroupId, --根分类Id
         LV4,
         LV4Name,
         6, --零部件
         1,
         null,
         1,
         '系统管理员',
         sysdate,
         null,
         null,
         null,
         null
    from (select distinct LV1,LV1Name,LV2,LV2Name,LV3,LV3Name,LV4,LV4Name  from Temp_MalfunctionCategory);
    
