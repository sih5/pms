-- Add/modify columns 
alter table SERVICEPRODLINEPRODUCT modify VEHICLEBRANDCODE null;
alter table SERVICEPRODLINEPRODUCT modify VEHICLEBRANDNAME null;
alter table SERVICEPRODLINEPRODUCT modify SUBBRANDCODE null;
alter table SERVICEPRODLINEPRODUCT modify SUBBRANDNAME null;
alter table SERVICEPRODLINEPRODUCT modify TERRACECODE null;
alter table SERVICEPRODLINEPRODUCT modify TERRACENAME null;
alter table SERVICEPRODLINEPRODUCT modify VEHICLEPRODUCTLINE null;
alter table SERVICEPRODLINEPRODUCT modify VEHICLEPRODUCTNAME null;
insert into SERVICEPRODLINEPRODUCT
select S_SERVICEPRODLINEPRODUCT.Nextval, x.*
  from (select distinct brandid,
                        BrandCode,
                        BrandName,
                        SubBrandCode,
                        SubBrandName,
                        TerraceCode,
                        TerraceName,
                        ProductLine,
                        ProductName,
                        null a,
                        null b,
                        null c,
                        null d,
                        null e,
                        null f
          from product) x;
