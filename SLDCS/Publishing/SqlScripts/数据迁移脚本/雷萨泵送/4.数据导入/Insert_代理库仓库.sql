insert into Warehouse
  select S_Warehouse.Nextval,
         code,
         name,
         (select key
            from keyvalueitem x
           where x.caption = '配件仓库类型'
             and value = type),
         1,
         Address,
         null,
         PhoneNumber,
         Contact,
         Fax,
         Email,
         (select key
            from keyvalueitem x
           where x.caption = '存储策略'
             and value = StorageStrategy),
         null,
         null,
         (select id
            from branch
           where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
         (select id
            from company
           where name = StorageCompanyName
             and code = StorageCompanyCode),
         (select key
            from keyvalueitem x
           where x.caption = '企业类型'
             and value = '代理库'),
         null,
         0,
         1,
         (select Name from personnel where id = 1),
         sysdate,
         null,
         null,
         null,
         null
    from Temp_Warehouse;

insert into WarehouseArea
  select S_WarehouseArea.Nextval,
         id,
         null,
         code,
         null,
         null,
         1,
         null,
         1,
         1,
         '系统管理员',
         sysdate,
         null,
         null,
         null,
         null
    from Warehouse
   where storagecompanytype = (select key
                                 from keyvalueitem x
                                where x.caption = '企业类型'
                                  and value = '代理库')
     and type = (select key
                   from keyvalueitem x
                  where x.caption = '配件仓库类型'
                    and value = '分库');