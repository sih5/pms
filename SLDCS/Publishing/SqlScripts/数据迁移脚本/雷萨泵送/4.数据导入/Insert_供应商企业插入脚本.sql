  insert into Company 
    select S_Company.Nextval,
          6,
           "供应商编号",
           "供应商名称",
           null,
           null,
           null,
           (select Id
              from TiledRegion
             where provincename = "省"
               and cityname = "市"
               and countyname = "县"),
           "省",
           "市",
           "县",
           null,
           null,
           "联系人",
           "联系电话",
           null,
           "传真",
           "联系地址",
           "邮编",
           "电子邮件",
           null,
           null,
           null,
           null,
           null,
           null,
           null,
           to_date("注册日期", 'yyyy-mm-dd hh24:mi:ss'),
           "经营范围",
           null,
           "注册地址",
           null,
           1,
           1,
           (select name from personnel where id = 1),
           sysdate,
           null,
           null,
           null,
           null
      from Temp_PartsSupplier;
insert into PartsSupplier 
    select (select id from company where company.code="供应商编号"),
           "供应商编号",
           "供应商名称",
           null,
           1,
           1,
           null,
           1,
           (select name from personnel where id = 1),
           sysdate,
           null,
           null,
           null,
           null,
           null,
           null,
           null
      from Temp_PartsSupplier;
      
insert into BranchSupplierRelation
  select S_BranchSupplierRelation.Nextval,
         (select id from branch where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
         (select id
            from PartsSalesCategory
           where name = '雷萨泵送'),
         (select id
            from PartsSupplier
           where PartsSupplier.Code = Temp_PartsSupplier."供应商编号"),
         null,
         null,
         null,
         null,
         1,
         null,
         1,
         (select name from personnel where id = 1),
         sysdate,
         null,
         null,
         null,
         null,
         null,
         null,
         null
    from Temp_PartsSupplier;
