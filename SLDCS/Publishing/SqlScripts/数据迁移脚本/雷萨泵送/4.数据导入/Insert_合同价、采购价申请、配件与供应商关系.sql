declare
  S_int number(9);
begin
  select S_PartsPurchasePricingChange.Nextval into S_int from dual;
  insert into PartsPurchasePricingChange
    select S_int,
           regexp_replace(regexp_replace(regexp_replace((select template
                                                          from codetemplate
                                                         where name =
                                                               'PartsPurchasePricingChange'),
                                                        '{CORPCODE}',
                                                        '247000'),
                                         '{DATE:yyyyMMdd}',
                                         to_char(sysdate, 'yyyymmdd')),
                          '{SERIAL:0000}',
                          lpad(to_char(GetSerial(1,
                                                 trunc(sysdate, 'DD'),
                                                 '247000')),
                               4,
                               '0')),
           (select id
              from branch
             where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
           (select id from PartsSalesCategory where name = '雷萨泵送'),
           '雷萨泵送',
           1,
           (select Name from personnel where id = 1),
           sysdate,
           1,
           (select Name from personnel where id = 1),
           sysdate,
           1,
           (select Name from personnel where id = 1),
           sysdate,
           null,
           null,
           null,
           (select key
              from keyvalueitem
             where caption = '采购价格变更申请单状态'
               and value = '生效'),
           1,
           null,
           null
      from dual;

  insert into PartsPurchasePricingDetail
    select S_PartsPurchasePricingDetail.Nextval,
           S_int,
           (select id
              from sparepart
             where code = x.partcode
               and name = x.partname),
           partcode,
           partname,
           (select id
              from partssupplier
             where code = x.partssuppliercode
               and name = x.partssuppliername),
           partssuppliercode,
           partssupplierName,
           (select key
              from keyvalueitem
             where caption = '采购价格类型'
               and value = '正式价格'),
           purchaseprice,
           0,
           to_date(ValidFrom, 'yyyy-MM-dd'),
           to_date(ValidTo, 'yyyy-MM-dd'),
           null
      from Temp_PartsPurchasePricing x;

  insert into PartsSupplierRelation
    select S_PartsSupplierRelation.Nextval,
           (select id
              from branch
             where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
           (select id from PartsSalesCategory where name = '雷萨泵送'),
           '雷萨泵送',
           (select id
              from sparepart
             where code = x.partcode
               and name = x.partname),
           (select id
              from company
             where code = x.partssuppliercode
               and name = x.partssuppliername),
           null,
           null,
           null,
           null,
           null,
           null,
           1,
           null,
           null
      from Temp_PartsPurchasePricing x;

  insert into PartsPurchasePricing
    select S_PartsPurchasePricing.Nextval,
           (select id
              from branch
             where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
           (select id
              from sparepart
             where code = x.partcode
               and name = x.partname),
           (select id
              from partssupplier
             where code = x.partssuppliercode
               and name = x.partssuppliername),
           (select id from PartsSalesCategory where name = '雷萨泵送'),
           '雷萨泵送',
           to_date(ValidFrom, 'yyyy-MM-dd'),
           to_date(ValidTo, 'yyyy-MM-dd'),
           (select key
              from keyvalueitem
             where caption = '采购价格类型'
               and value = '正式价格'),
           purchaseprice,
           (select key
              from keyvalueitem
             where caption = '采购价格状态'
               and value = '生效'),
           1,
           (select Name from personnel where id = 1),
           sysdate,
           null,
           null,
           null,
           null,
           null,
           null,
           null
      from Temp_PartsPurchasePricing x;
end;
