  insert into ServiceTripPriceGrade
    select S_ServiceTripPriceGrade.Nextval, x.*
      from (select (select id
                      from branch
                     where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
                   Temp_ServiceTripPriceGrade.ServiceTripPriceGradeName i,
                   Temp_ServiceTripPriceGrade.ServiceTripPriceGradeName,
                   Temp_ServiceTripPriceGrade.RegionDescription,
                   1 a,
                   null b,
                   1 c,
                   (select Name from personnel where id = 1),
                   sysdate d,
                   null e,
                   null f,
                   null g,
                   null h
              from Temp_ServiceTripPriceGrade
             group by Temp_ServiceTripPriceGrade.ServiceTripPriceGradeName,
                      Temp_ServiceTripPriceGrade.RegionDescription) x;
                      
insert into ServiceTripPriceRate
    select S_ServiceTripPriceRate.Nextval,
    (select Id from ServiceTripPriceGrade where Temp_ServiceTripPriceGrade.ServiceTripPriceGradeName=ServiceTripPriceGrade.Code),
    (select Id from ServiceProductLine where ServiceProductLine.code = Temp_ServiceTripPriceGrade.ServiceProductLineCode and ServiceProductLine.Name=Temp_ServiceTripPriceGrade.Serviceproductlinename),
    (select key from keyvalueitem x where x.caption='产品线类型' and value = Temp_ServiceTripPriceGrade.Productlinetype),
    S_ServiceTripPriceRate.Nextval,
    (select key from keyvalueitem x where x.caption='外出类型' and value = Temp_ServiceTripPriceGrade.ServiceTripType),
    to_number(Temp_ServiceTripPriceGrade.MaxMileage),
    to_number(Temp_ServiceTripPriceGrade.MinMileage),
    to_number(Temp_ServiceTripPriceGrade.FixedAmount),
    case Temp_ServiceTripPriceGrade.Isselfcar when  '是'
         then 1
           else
             0
             end ,
           to_number(Temp_ServiceTripPriceGrade.SubsidyPrice),
    case  when Temp_ServiceTripPriceGrade.Price is not null then 
         to_number(Temp_ServiceTripPriceGrade.Price)
         else
           0
           end,
         1,
         Temp_ServiceTripPriceGrade.remark  ,
         1,
         (select Name from personnel where id=1),
         sysdate,
         null,
         null,
         null,
         null
from Temp_ServiceTripPriceGrade;         
         
