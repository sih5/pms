insert into CustomerOrderPriceGrade
  select S_CustomerOrderPriceGrade.Nextval,
         (select id
            from branch
           where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
         (select id
            from PartsSalesCategory
           where name = x.PartsSalesCategoryId),
         (select id
            from company
           where code = CustomerCompanyCode
             and name = CustomerCompanyName),
         CustomerCompanyCode,
         CustomerCompanyName,
         (select id
            from PartsSalesOrderType a
           where a.partssalescategoryid =
                 (select id
                    from PartsSalesCategory
                   where name = x.PartsSalesCategoryId and a.code=x.partssalesordertypecode and a.name=x.partssalesordertypename)),
                   PartsSalesOrderTypeCode,
                   PartsSalesOrderTypeName,
         to_number(Coefficient),
         1,
         null,
         1,
         (select Name from personnel where id = 1),
         sysdate,
         null,
         null,
         null,
         null,
         null,
         null,
         null
    from Temp_CustomerOrderPriceGrade x;
 
insert into CustomerOrderPriceGradeHistory
  select s_Customerorderpricegradehis.Nextval,
         BranchId,
         PartsSalesCategoryId,
         CustomerCompanyId,
         CustomerCompanyCode,
         CustomerCompanyName,
         PartsSalesOrderTypeId,
         PartsSalesOrderTypeCode,
         PartsSalesOrderTypeName,
         Coefficient,
         Status,
         Remark,
         1,
         (select Name from personnel where id = 1),
         sysdate,
         null
    from CustomerOrderPriceGrade;
