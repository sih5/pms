insert into Company
  (Id, -- 企业Id
   Type, -- 企业类型
   Code, -- 企业编号
   Name, -- 企业名称
   --CustomerCode,-- 客户编码
   --SupplierCode,-- 供应商编码
   --ShortName,--  简称
   RegionId, -- 区省市Id
   ProvinceName, -- 省
   CityName, -- 市
   CountyName, -- 县
   FoundDate, --  成立日期
   ContactPerson, --  联系人
   ContactPhone, -- 联系电话
   ContactMobile, --  联系人手机
   Fax, --  传真
   ContactAddress, -- 联系地址
   ContactPostCode, --  邮政编码
   ContactMail, --  电子邮箱
   RegisterCode, -- 注册证号
   RegisterName, -- 注册名称
   CorporateNature, --  企业性质
   LegalRepresentative, --  法定代表人
   --IdDocumentType,-- 身份证件类型
   --IdDocumentNumber,-- 身份证件号码
   RegisterCapital, --  注册资本
   RegisterDate, -- 注册日期
   BusinessScope, --  经营范围
   --BusinessAddress,--  营业地址
   RegisteredAddress, --  注册地址
   Remark, -- 备注
   Status, -- 状态
   CreatorId, --  创建人Id
   CreatorName, --  创建人
   CreateTime --  创建时间
   --ModifierId  修改人Id
   --ModifierName  修改人
   --ModifyTime  修改时间
   )
  (select dcs.s_company.nextval, -- ID
          2, --企业类型
          Code, --企业编号
          Name, --企业名称
          --CustomerCode, --客户编码
          --SupplierCode, --供应商编码
          --ShortName, --简称
          (select t2.id
             from dcs.tiledregion t2
            where t1.provincename = t2.provincename
              and t1.regionname = t2.cityname
              and t1.countyname = t2.countyname) RegionId, --区省市Id
          ProvinceName, --省
          regionname, --市
          CountyName, --县
          to_date(factorytime, 'YYYY-MM-DD hh24:mi:ss'), --成立日期
          mastercode, --联系人
          Phone, --联系电话
          phone, --联系人手机
          Fax, --传真
          Address, --联系地址
          PostCode, --邮政编码
          EMail, --电子邮箱
          RegisterCode, --注册证号
          TstationName, --注册名称
          decode(enterkind, '国有', 1, '国营', 1, 2), --企业性质
          corporation, --法定代表人
          --IdDocumentType, --身份证件类型
          --IdDocumentNumber, --身份证件号码
          registerfund, --注册资本
          to_date(establishdate, 'YYYY-MM-DD hh24:mi:ss'), --注册日期
          pribusiness, --经营范围
          --BusinessAddress, --营业地址
          qyaddress, --注册地址
          '', --备注
          1, --状态
          1, --创建人Id
          '系统管理员', --创建人
          sysdate --创建时间
   --ModifierId, --修改人Id
   --ModifierName, --修改人
   --ModifyTime, --修改时间
     from tstationstemp t1
    where not exists (select * from Company t2 where t1.code = t2.code));

--新增经销商基本信息
insert into Dealer
  (ID,
   CODE,
   NAME,
   ISVIRTUALDEALER,
   CREATETIME,
   CREATORID,
   CREATORNAME,
   status,
   manager)
  (select id, code, name, 0, sysdate, 1, '系统管理员', 1, ContactPerson
     from company t1
    where not exists (select * from Dealer t2 where t1.code = t2.code)
      and exists
    (select * from tstationstemp t3 where t1.code = t3.code))

--select * from tstationstemp order by id
