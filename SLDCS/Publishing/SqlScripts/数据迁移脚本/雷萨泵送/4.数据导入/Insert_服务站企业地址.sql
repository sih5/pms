
--4、服务站企业地址
insert into CompanyAddress(
Id, --Id
CompanyId, --	企业Id
RegionId, --	区省市Id
Usage, --	地址用途
ContactPerson, --	联系人
ContactPhone, --	联系电话
DetailAddress, --	详细地址
CreateTime, --	创建时间
CreatorId, --	创建人Id
CreatorName, --	创建人
--ModifyTime, --	修改时间
--ModifierId, --	修改人Id
--ModifierName, --	修改人
Status --	状态
--Remark, --	备注
)
(
select 
   dcs.S_COMPANYADDRESS.nextval,
   (select id from Dealer t5 where t1.code=t5.code) ID,
   (
      select t2.id from dcs.tiledregion t2 
      where t1.regionname=t2.REGIONNAME 
      and t1.provincename=t2.provincename
      and t1.countyname=t2.countyname
    ) RegionId,
   2,--地址用途2接受配件
   sparelinkerman,
   sparelinkertel,
   sparelinkeraddress,
   sysdate,
   1,
   '系统管理员',
   1
from COMPANYADDRESSTEMP t1)





--select * from CompanyAddress 


--select * from COMPANYADDRESSTEMP