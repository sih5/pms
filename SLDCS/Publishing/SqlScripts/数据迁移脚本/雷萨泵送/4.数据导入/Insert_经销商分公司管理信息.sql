--需要有经销商信息，分公司，市场部，星级系数信息


insert into DealerServiceInfo
(
    Id, --Id
    BranchId, --营销分公司Id
    PartsSalesCategoryId, --	配件销售类型Id
    DealerId, --	经销商Id
    MarketingDepartmentId, --	市场部Id
    UsedPartsWarehouseId, --	旧件仓库Id
    ChannelCapabilityId, --	网络业务能力Id
    PartsManagingFeeGradeId, --	配件管理费率等级Id
    GradeCoefficientId, --	星级系数Id
    OutServiceradii, --	外出服务半径
--   LaborHourFloatCoefficient, --	工时上浮系数
--   FloatValidFrom, --	上浮生效时间
--   FloatValidTo, --	上浮失效时间
    OutFeeGradeId, --	外出服务费等级Id
    RepairAuthorityGrade, --	维修资质等级
    HotLine, --	24小时热线
    Fix, --	固定电话
    Fax, --	传真
    Remark, --	备注
    Status, --	状态
    CreateTime, --	创建时间
    CreatorId, --	创建人Id
    CreatorName, --	创建人
 --   ModifierId, --	修改人Id
 --   ModifyTime, --	修改时间
 --  ModifierName, --	修改人

    ServicePermission,   --维修权限
    ServiceStationType, --服务站类别
    AccreditTime,    --授权时间
    CancellingDate,     --解约时间
    IsOnDuty    --是否职守





)
select 
      dcs.s_dealerserviceinfo.nextval, --id
      (select id from dcs.branch t2 where t1.filialecode=t2.code), --分公司ID
      (select id from PartsSalesCategory t3 where t1.brandname=t3.name), --销售类型（品牌ID）
      (select id from Dealer t4 where t1.code=t4.code),--经销商ID
      1,
      --(select id from MarketingDepartment t6 where t1.marketcode=t6.code), --	市场部Id
      nvl((select id from dcs.warehouse t7 where t1.UsedPartsWarehouseId=t7.id),0)UsedPartsWarehouseId, --	旧件仓库Id
      1,
      -- (select id from ChannelCapability where ChannelCapability.name=t1.ChannelCapabilityId) ChannelCapabilityId, --	网络业务能力Id,后期需要修改 
       (select id from dcs.partsmanagementcostgrade where t1.PartsManagingFeeGradeId=partsmanagementcostgrade.name) PartsManagingFeeGradeId, --	配件管理费率等级Id
       --PartsManagingFeeGradeId, --	配件管理费率等级Id
       1,
      --(select id from GradeCoefficient t5 where t1.GradeCoefficientId=t5.grade), --	星级系数Id
       0 OutServiceradii, --	外出服务半径
--     '', --	工时上浮系数
--     '', --	上浮生效时间
--     '', --	上浮失效时间
       1,
      -- OutFeeGradeId, --	外出服务费等级Id
       decode(repairpurview,'一类',1,'二类',2,'三类',3), --	维修资质等级
       dutyphone, --	24小时热线
       phone, --	固定电话
       Fax, --	传真
       '', --	备注
       1, --	状态
       sysdate, --	创建时间
       1, --	创建人Id
       '系统管理员', --	创建人
--    ModifierId, --	修改人Id
--    ModifyTime, --	修改时间
--    ModifierName, --	修改人

      ServicePurview,
      1,
      --Stationtype, --服务站类别
      SalingTime,
      CutTime,
      1
      --Isduty  --是否职守

 from Dealerserviceinfotemp t1 where exists (select * from Dealer where Dealer.code=t1.code)

--select * from DealerServiceInfo

--select * from Dealerserviceinfotemp

--select * from MarketingDepartment


--select * from GradeCoefficient

--delete from Dealerserviceinfotemp
--where code='XIZ00003'


--select * from ChannelCapability 
--select id from dcs.partsmanagementcostgrade


