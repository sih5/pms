declare
  S_int number(9);
begin
  select S_PlannedPriceApp.Nextval into S_int from dual;
  insert into PlannedPriceApp
    select S_int,
           (select id
              from branch
             where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
           (select code
              from branch
             where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
           '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司',
           (select id from PartsSalesCategory where name = '雷萨泵送'),
           '雷萨泵送',
           regexp_replace(regexp_replace(regexp_replace((select template
                                                          from codetemplate
                                                         where name =
                                                               'PlannedPriceApp'),
                                                        '{CORPCODE}',
                                                        '247000'),
                                         '{DATE:yyyyMMdd}',
                                         to_char(sysdate, 'yyyymmdd')),
                          '{SERIAL:0000}',
                          lpad(to_char(GetSerial(1,
                                                 trunc(sysdate, 'DD'),
                                                 '247000')),
                               4,
                               '0')),
           0,
           0,
           0,
           3, --已执行
           1, --不需要计账
           null,
           1,
           (select Name from personnel where id = 1),
           sysdate,
           null,
           null,
           null,
           sysdate,
           sysdate,
           null
      from dual;

  insert into PlannedPriceAppDetail
    select S_PlannedPriceAppDetail.Nextval,
           S_int,
           (select id from sparepart where code = x.partscode),
           x.partscode,
           (select name from sparepart where code = x.partscode),
           x.price,
           0,
           0,
           x.price
      from Temp_PlanPrice x;

  insert into PartsPlannedPrice
    select S_PartsPlannedPrice.Nextval,
           (select id
              from branch
             where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
           1, --分公司
           (select id from PartsSalesCategory where name = '雷萨泵送'),
           '雷萨泵送',
           (select id from sparepart where code = x.partscode),
           x.price,
           1,
           (select Name from personnel where id = 1),
           sysdate,
           null,
           null,
           null,
           null
      from Temp_PlanPrice x;
end;
