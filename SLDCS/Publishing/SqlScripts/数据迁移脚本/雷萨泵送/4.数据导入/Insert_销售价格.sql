--配件销售价不能通用，由于这批数据不区分 是否用于索赔，为了不使脚本过于复杂，写死处理
declare
  S_int number(9);
begin
  select S_PartsSalesPriceChange.Nextval into S_int from dual;
  insert into PartsSalesPriceChange
    select S_int,
           (select id
              from branch
             where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
           regexp_replace(regexp_replace(regexp_replace((select template
                                                          from codetemplate
                                                         where name =
                                                               'PartsSalesPriceChange'),
                                                        '{CORPCODE}',
                                                        'SAX00034'),
                                         '{DATE:yyyyMMdd}',
                                         to_char(sysdate, 'yyyymmdd')),
                          '{SERIAL:0000}',
                          lpad(to_char(GetSerial(1,
                                                 trunc(sysdate, 'DD'),
                                                 'SAX00034')),
                               4,
                               '0')),
           (select id from PartsSalesCategory where name = '雷萨泵送'),
           (select code from PartsSalesCategory where name = '雷萨泵送'),
           '雷萨泵送',
           1,
           1,
           (select Name from personnel where id = 1),
           sysdate,
           1,
           (select Name from personnel where id = 1),
           sysdate,
           1,
           (select Name from personnel where id = 1),
           sysdate,
           1,
           (select Name from personnel where id = 1),
           sysdate,
           null,
           null,
           null,
           null,
           null,
           null,
           (select key
              from keyvalueitem
             where caption = '配件销售价格变更申请状态'
               and value = '终审通过'),
           null,
           null
      from dual;

  insert into PartsSalesPriceChangeDetail
    select S_PartsSalesPriceChangeDetail.Nextval,
           S_int,
           (select id
              from sparepart
             where code = x.sparepartcode),
           sparepartcode,
           sparepartname,
           x.saleprice,
           x.retailguideprice,
           (select key
              from keyvalueitem
             where caption = '价格类型'
               and value = '基准销售价'),
           null,
           null,
           null,
           null
      from Temp_Prices x;

  insert into PartsSalesPrice
    select S_PartsSalesPrice.Nextval,
           (select id
              from branch
             where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
           (select id from PartsSalesCategory where name = '雷萨泵送'),
           (select code from PartsSalesCategory where name = '雷萨泵送'),
           '雷萨泵送',
           (select id
              from sparepart
             where code = x.sparepartcode),
           sparepartcode,
           sparepartname,
           1,
           x.saleprice,
           (select key
              from keyvalueitem
             where caption = '价格类型'
               and value = '基准销售价'),
           1,
           null,
           null,
           null,
           1,
           (select Name from personnel where id = 1),
           sysdate,
           1,
           (select Name from personnel where id = 1),
           sysdate,
           1,
           (select Name from personnel where id = 1),
           sysdate,
           null,
           null,
           null,
           null
      from Temp_Prices x;

  insert into PartsSalesPriceHistory
    select S_PartsSalesPriceHistory.Nextval,
           (select id
              from branch
             where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
           (select id from PartsSalesCategory where name = '雷萨泵送'),
           (select code from PartsSalesCategory where name = '雷萨泵送'),
           '雷萨泵送',
           1,
           (select id
              from sparepart
             where code = x.sparepartcode),
           sparepartcode,
           sparepartname,
           x.saleprice,
           (select key
              from keyvalueitem
             where caption = '价格类型'
               and value = '基准销售价'),
           null,
           null,
           1,
           null,
           1,
           (select Name from personnel where id = 1),
           sysdate,
           null
      from Temp_Prices x;

  insert into PartsRetailGuidePrice
    select S_PartsRetailGuidePrice.Nextval,
           (select id
              from branch
             where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
           (select id from PartsSalesCategory where name = '雷萨泵送'),
           (select code from PartsSalesCategory where name = '雷萨泵送'),
           '雷萨泵送',
           (select id
              from sparepart
             where code = x.sparepartcode),
           sparepartcode,
           sparepartname,
           x.retailguideprice,
           null,
           null,
           1,
           null,
           1,
           (select Name from personnel where id = 1),
           sysdate,
           null,
           null,
           null,
           null,
           null,
           null,
           null
      from Temp_Prices x;

  insert into PartsRetailGuidePriceHistory
    select S_PartsRetailGuidePriceHistory.Nextval,
           (select id
              from branch
             where name = '北汽福田汽车股份有限公司北京福田雷萨泵送机械分公司'),
           (select id from PartsSalesCategory where name = '雷萨泵送'),
           (select code from PartsSalesCategory where name = '雷萨泵送'),
           '雷萨泵送',
           (select id
              from sparepart
             where code = x.sparepartcode),
           sparepartcode,
           sparepartname,
           x.retailguideprice,
           null,
           null,
           1,
           null,
           1,
           (select Name from personnel where id = 1),
           sysdate,
           null
      from Temp_Prices x;
end;
