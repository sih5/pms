delete temp_role_d;
insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '生成紧急销售订单');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '生成外采申请');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '维修完工');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '驳回修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '生成索赔');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '查看明细');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '生成外出索赔');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '生成紧急销售订单');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '生成外采申请');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '生成索赔');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '维修完工');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '查看明细');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '二级站维修单管理', '查看明细');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '二级站维修单管理', '提交');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '二级站维修单管理', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '二级站维修单管理', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '二级站维修单管理', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '二级站维修单管理', '审批');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '旧件发运管理', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '旧件发运管理', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '旧件发运管理', '打印');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '旧件发运管理', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '旧件发运管理', '导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '服务站旧件库存查询', '导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '服务站旧件处理管理', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '服务站旧件处理管理', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '服务站旧件处理管理', '审批');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '服务站旧件处理管理', '打印');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('旧件业务管理', '市场旧件处理-服务站', '服务站旧件处理管理', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('公告管理', '公告管理', '公告查询', '');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件互换件管理', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件互换件管理', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件互换件管理', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '直供发运单确认管理', '确认');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '直供发运单确认管理', '导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件采购管理', '配件采购业务', '直供发运单确认管理', '打印');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件申请管理', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件申请管理', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件申请管理', '提交');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件申请管理', '导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '积压件平台', '积压件申请管理', '打印');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售退货结算查询', '导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售结算业务', '配件销售结算单查询', '导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货提报', '合并导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '提交');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '合并导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '撤单');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售订单提报', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货提报', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货提报', '提交');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货提报', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '配件销售退货提报', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '二级站配件需求管理', '审核');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '二级站配件需求提报', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '二级站配件需求提报', '提交');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '二级站配件需求提报', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件销售业务', '二级站配件需求提报', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件外采业务', '配件外采提报', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件外采业务', '配件外采提报', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件外采业务', '配件外采提报', '提交');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件外采业务', '配件外采提报', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件外采业务', '配件外采提报', '审批');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件外采业务', '配件外采提报', '导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件销售管理', '配件零售业务', '服务站配件零售管理', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库存', '服务站配件库存管理', '导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件物流', '配件收货管理', '收货确认');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库内业务', '服务站配件盘点提报', '导入');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库内业务', '服务站配件盘点提报', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件库内业务', '服务站配件盘点提报', '导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件调拨业务', '企业间配件调拨管理', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件调拨业务', '企业间配件调拨管理', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件仓储物流管理', '配件调拨业务', '企业间配件调拨管理', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('服务基础数据管理', '车辆及客户档案', '客户信息管理', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('服务基础数据管理', '车辆及客户档案', '客户信息管理', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('服务基础数据管理', '车辆及客户档案', '客户信息管理', '查看明细');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('服务基础数据管理', '车辆及客户档案', '车辆信息管理', '服务站修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('服务基础数据管理', '车辆及客户档案', '车辆信息管理', '变更车主');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('服务基础数据管理', '车辆及客户档案', '车辆信息管理', '历史车主');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('服务基础数据管理', '车辆及客户档案', '车辆信息管理', '查看明细');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('服务基础数据管理', '质保标准管理', '保修政策管理', '详细信息');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '服务站索赔结算', '服务站索赔结算查询-服务站', '查看明细');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '服务站索赔结算', '服务站发票管理', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '服务站索赔结算', '服务站发票管理', '提交');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '服务站索赔结算', '服务站发票管理', '查看详细');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '来客登记');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '来客并派工');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '维修单登记');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '维修单修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '打印维修单');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保内维修单管理-服务站', '打印结算单');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '外出索赔申请管理-服务站', '查看明细');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '外出索赔申请管理-服务站', '提交');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '外出索赔申请管理-服务站', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '外出索赔申请管理-服务站', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '外出索赔申请管理-服务站', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '外出服务索赔管理-服务站', '提交');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '外出服务索赔管理-服务站', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '外出服务索赔管理-服务站', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '外出服务索赔管理-服务站', '查看明细');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '索赔单管理-服务站', '维修索赔管理-服务站', '查看明细');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '打印维修单');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '打印结算单');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '打印维修单');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '保外维修单管理-服务站', '打印结算单');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '售前检查管理-服务站', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '售前检查管理-服务站', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '售前检查管理-服务站', '提交');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '售前检查管理-服务站', '查看明细');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '售前检查管理-服务站', '导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '售前检查管理-服务站', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '维修工单管理-服务站', '生成维修单');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '维修单管理-服务站', '维修工单管理-服务站', '关闭');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '提交');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '服务站分公司信息管理-服务站', '查看明细');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '服务站分公司信息管理-服务站', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '服务站地址维护-服务站', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '服务站地址维护-服务站', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '服务站地址维护-服务站', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '二级站管理', '导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '二级站管理', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '二级站管理', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '二级站管理', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '二级站人员管理', '导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '二级站人员管理', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '二级站人员管理', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('公共数据管理', '服务站信息管理', '二级站人员管理', '删除');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件营销信息查询', '导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件信息管理', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件信息管理', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件信息管理', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件信息管理', '恢复');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件信息管理', '停用');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件信息管理', '导入');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件信息管理', '导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件替换件管理', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件替换件管理', '导入');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件替换件管理', '作废');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件替换件管理', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件替换件管理', '导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件互换件管理', '导入');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('配件基础数据管理', '配件信息', '配件互换件管理', '导出');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '新增');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '修改');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '查看明细');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '提交');

insert into temp_role_d (LV1, LV2, LV3, ACTION)
values ('维修索赔业务管理', '申请单管理-服务站', '维修索赔申请管理-服务站', '查看明细');

delete EntNodeTemplateDetail where entnodetemplateid = (select id from EntNodeTemplate where code='DealerTemplate');
insert into EntNodeTemplateDetail
select S_EntNodeTemplateDetail.Nextval, (select id from EntNodeTemplate where code='DealerTemplate'), nodeid
      from (select distinct (x.id) as nodeid
              from (select node.id, Temp_Role_D.action, Temp_Role_D.lv3
                      from Temp_Role_D
                     inner join page
                        on page.name = Temp_Role_D.lv3
                     left join action
                        on action.name = Temp_Role_D.action
                       and action.pageid = page.id
                     inner join node
                        on (node.categorytype = 0 and
                           node.categoryid = page.id)
                        or (node.categoryType = 1 and
                           node.categoryid = action.id)) x) xnode;
delete rule where roleid in (select id from role where enterpriseid in (select id from enterprise where EnterpriseCategoryid=2) and isadmin =0);
declare
  cursor Cursor_role is(
    select id
      from role
     where enterpriseid in
           (select id from enterprise where EnterpriseCategoryid = 2)
       and isadmin = 0);
begin
  for S_role in Cursor_role loop
    insert into rule
      select S_rule.Nextval, S_role.id, EntNodeTemplateDetail.Nodeid
        from EntNodeTemplateDetail
       where EntNodeTemplateDetail.Entnodetemplateid =
             (select id from EntNodeTemplate where code = 'DealerTemplate');
end loop;
end;
