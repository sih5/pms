--1【SAP采购计划】
select '主单' || a.id,
       a.syscode,
       a.abroadcode,
       a.plantype as 计划类型,
       a.handlestatus as 状态,
       a.modifytime as 修改时间,
       a.message,
       '清单' || b.id,
       b.purchaseplanid as 采购计划单Id,
       b.abroadpartcode,
       b.sparepartcode as 配件编号,
       b.planamount as 计划量,
       b.measureunit,
       b.promiseddeliverytime as 要求到货时间,
       b.remark as 备注,
       b.businesscode,
       b.warehousename as 仓库名称,
       b.originalorder,
       b.overseassupliercode
  from yxdcs.PartsPurchasePlanSap a
 inner join yxdcs.PartsPurchasePlanDetailSap b
    on a.Id = b.PURCHASEPLANID
 where a.modifytime >=
       to_date(to_char(trunc(sysdate), 'yyyy-mm-dd') || '11:30:00',
               'yyyy-mm-dd hh24:mi:ss');