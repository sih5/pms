--4【出库包装计划】
select '主单' || a.id,
       a.objid,
       a.thedate,
       a.interface_record_id,
       a.interface_condition,
       a.interface_action_code,
       a.company,
       a.shipment_id,
       a.warehouse,
       a.user_stamp,
       a.date_time_stamp,
       a.scheduled_ship_date,
       a.customer,
       a.order_type,
       a.ship_to,
       a.ship_to_address1,
       a.ship_to_address2,
       a.ship_to_name,
       a.ship_to_attention_to,
       a.carrier,
       a.allocate_complete,
       a.erp_order,
       a.carrier_service,
       a.ship_to_city,
       a.ship_to_state,
       a.ship_to_country,
       a.ship_to_postal_code,
       a.read_date_time_stamp,
       a.user_def1,
       a.user_def2,
       a.user_def3,
       a.user_def4,
       a.user_def5,
       a.user_def6,
       a.user_def7,
       a.user_def8,
       a.user_def9,
       a.user_def10,
       a.user_def11,
       a.user_def12,
       a.user_def13,
       a.p_fumigating,
       a.p_auditing,
       a.p_packing_instructions,
       a.p_mark,
       a.p_inspection,
       a.p_user_def1,
       a.p_user_def2,
       a.handlestatus,
       a.modifytime as 修改时间,
       a.message as 信息,
       '清单' || b.id,
       b.objid,
       b.parentid as 主单ID,
       b.interface_condition,
       b.interface_action_code,
       b.interface_record_id,
       b.company,
       b.interface_link_id,
       b.shipment_id,
       b.warehouse,
       b.item,
       b.quantity_um,
       b.total_qty,
       b.lot_controlled,
       b.user_stamp,
       b.date_time_stamp,
       b.erp_order_line_num,
       b.read_date_time_stamp,
       b.user_def1,
       b.user_def2,
       b.user_def3,
       b.user_def4,
       b.user_def5,
       b.user_def6,
       b.user_def7,
       b.user_def8,
       b.erp_order,
       b.posnr,
       b.ebeln as 销售订单编号,
       b.ebelp
  from yxdcs.PartsOutboundPlanASAP a
 inner join yxdcs.PartsOutboundPlanDASAP b
    on a.Id = b.parentid
 where a.modifytime >=
       to_date(to_char(trunc(sysdate), 'yyyy-mm-dd') || '11:30:00',
               'yyyy-mm-dd hh24:mi:ss');
