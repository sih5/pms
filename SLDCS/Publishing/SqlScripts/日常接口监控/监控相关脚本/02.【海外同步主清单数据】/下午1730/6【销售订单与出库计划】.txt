--销售订单错误信息
--影响行数：851 
select '主单' || a.id, a.*
  from yxdcs.PartsSalesOrderASAP a
 where a.handlestatus = 3
 order by a.id;

--出库计划错误信息
--影响行数：892
select '主单' || a.id, a.*
  from yxdcs.PartsOutboundPlanASAP a
 where a.handlestatus = 3
 order by a.id;

