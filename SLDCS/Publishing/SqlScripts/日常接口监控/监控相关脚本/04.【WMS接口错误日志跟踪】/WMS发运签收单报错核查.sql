select *
  from yxdcs.Syncwmsinoutloginfo t
 where t.syncdate >= trunc(sysdate);

SELECT t_Shippingorderhead.*
  FROM yxdcs.t_Shippingorderhead
 WHERE User_Def1 = 'NPMS'
   AND isfinished = 0
 ORDER BY Save_Date_Time_Stamp, Interface_Record_Id;

select *
  from yxdcs.t_Shippingorderhead
 where shipment_id = 'POP110120170613000118';

select pop.Id,
       pop.Branchid,
       pop.StorageCompanyId,
       Pop.PartsSalesCategoryId,
       NVL(Pop.CustomerAccountId, 0),
       Pop.OutboundType,
       Pop.StorageCompanyCode,
       pop.Warehouseid
  from yxdcs.Partsoutboundplan pop
 where code = 'POP110120170613000118';

select branchstrategy.WMSOutStrategy
  from yxdcs.branchstrategy
 where branchid = (select branchid
                     from yxdcs.Partsoutboundplan
                    where code = 'POP110120170613000118');

select etl.logisticscompanyid,
       etl.logisticscompanycode,
       etl.logisticscompanyname
  from yxdcs.expresstologistics etl
 where etl.focufingcorecode =
       (select warehouse
          from yxdcs.t_Shippingorderhead
         where shipment_id = 'POP110120170613000118')
   and etl.expresscode =
       (select Carrier
          from yxdcs.t_Shippingorderhead
         where shipment_id = 'POP110120170613000118');

select id, code, name
  from yxdcs.logisticcompany
 where code = (select Carrier
                 from yxdcs.t_Shippingorderhead
                where shipment_id = 'POP110120170613000118');

select t.shipment_id, l.item
  from yxdcs.t_Shippingorderhead t
 inner join yxdcs.t_Shippingorderdetails l
    on t.interface_record_id = l.Interface_Link_Id
 where t.shipment_id = 'POP110120170613000118';

--【ORA-01422: exact fetch returns more than requested number of rows】 出库计划单清单有重复配件
--比较与上面查询的配件数据量【ORA-01403: no data found】
--出库计划单清单的计划量< 出库计划单清单的出库完成量 + 发运签收单清单的TOTAL_QTY【完成量大于计划量，请进行核对！】
SELECT NVL(pd.OutboundFulfillment, 0) Finishqty,
       NVL(Pd.Plannedamount, 0) Planqty,
       pd.sparepartid AS FSparePartId
  FROM yxdcs.Partsoutboundplandetail Pd
 INNER JOIN yxdcs.Sparepart
    ON Sparepart.Id = Pd.Sparepartid
 WHERE Pd.Partsoutboundplanid =
       (select id
          from yxdcs.Partsoutboundplan
         where code = 'POP110120170613000118')
   AND Sparepart.Code in
       (select l.item from yxdcs.t_Shippingorderhead t inner join
        yxdcs.t_Shippingorderdetails l on
        t.interface_record_id = l.Interface_Link_Id where
        t.shipment_id = 'POP110120170613000118');


SELECT ppp.PlannedPrice
  FROM yxdcs.PartsPlannedPrice ppp
 WHERE ppp.OwnerCompanyId =
       (select StorageCompanyId
          from yxdcs.Partsoutboundplan
         where code = 'POP110120170613000118')
   AND ppp.PartsSalesCategoryId =
       (select PartsSalesCategoryId
          from yxdcs.Partsoutboundplan
         where code = 'POP110120170613000118')
   AND ppp.sparepartid in
       (select s.id
          from yxdcs.t_Shippingorderhead t
         inner join yxdcs.t_Shippingorderdetails l
            on t.interface_record_id = l.Interface_Link_Id
         inner join yxdcs.sparepart s
            on s.code = l.item
         where t.shipment_id = 'POP110120170613000118');

--查询出多条数据 【ORA-01422: exact fetch returns more than requested number of rows】
SELECT WarehouseArea.Id, WarehouseArea.Code
  FROM yxdcs.WarehouseArea
 INNER JOIN yxdcs.WarehouseAreaCategory
    ON WarehouseArea.AreaCategoryId = WarehouseAreaCategory.Id
 WHERE WarehouseArea.WarehouseId =
       (select Warehouseid
          from yxdcs.Partsoutboundplan
         where code = 'POP110120170613000118')
   AND WarehouseArea.Areakind = 3
   AND WarehouseAreaCategory.Category = 1;


--【找不到对应的配件，请核对！S1003040-04S1A6360】 -- 配件的状态是2--停用
SELECT Sparepart.*
  FROM sddcs.Sparepart
 WHERE 1 = 1
   AND Sparepart.Code in
       (select l.item
          from sddcs.t_Shippingorderhead t
         inner join sddcs.t_Shippingorderdetails l
            on t.interface_record_id = l.Interface_Link_Id
         where t.shipment_id = 'POP110120170613000118')
   and Sparepart.Status = 1

--【存在多条锁定库存,仓库Id14配件Id263438】 锁定库存数据存在多条
--【锁定库存不足F1104916200039A0434】 锁定库存不足
select v.*, l.total_qty ,v.Lockedquantity- l.total_qty as dif
  from (SELECT PartsLockedStock.partid,
               (select code
                  from yxdcs.sparepart
                 where id = PartsLockedStock.Partid) as Partcode,
               PartsLockedStock.Lockedquantity
        
          FROM yxdcs.PartsLockedStock
         WHERE PartsLockedStock.WarehouseId =
               (select Warehouseid
                  from yxdcs.Partsoutboundplan
                 where code = 'POP110120170619000928')
           AND PartsLockedStock.Partid in
               (select s.id
                  from yxdcs.t_Shippingorderhead t
                 inner join yxdcs.t_Shippingorderdetails l
                    on t.interface_record_id = l.Interface_Link_Id
                 inner join yxdcs.sparepart s
                    on s.code = l.item
                 where t.shipment_id = 'POP110120170619000928')) v
 inner join yxdcs.t_Shippingorderdetails l
    on l.item = v.Partcode
 inner join yxdcs.t_Shippingorderhead t
    on t.interface_record_id = l.Interface_Link_Id
 where t.shipment_id = 'POP110120170619000928';



--【库存不存在S3701321-X198A1530】 --查不到数据，有99作废的库位信息
--【存在多条库存,仓库Id1262配件Id215835】 --存在作废库位的库存数据
SELECT PartsStock.partid,
       (select code from yxdcs.sparepart where id = PartsStock.Partid) as Partcode,
       PartsStock.quantity,
       PartsStock.warehouseareaid,
       (select code
          from yxdcs.WarehouseArea
         where id = PartsStock.warehouseareaid) as WarehouseAreacode,
       (select Status
          from yxdcs.WarehouseArea
         where id = PartsStock.warehouseareaid) as WarehouseAreaStatus
  FROM yxdcs.PartsStock
 INNER JOIN yxdcs.WarehouseAreaCategory
    ON PartsStock.WarehouseAreaCategoryId = WarehouseAreaCategory.ID
 inner join yxdcs.warehousearea on warehousearea.id=PartsStock.Warehouseareaid 
 WHERE WarehouseAreaCategory.Category = 1
   AND PartsStock.WarehouseId =
       (select Warehouseid
          from yxdcs.Partsoutboundplan
         where code = 'POP110120170613000118')
   AND PartsStock.Partid in
       (select s.id
          from yxdcs.t_Shippingorderhead t
         inner join yxdcs.t_Shippingorderdetails l
            on t.interface_record_id = l.Interface_Link_Id
         inner join yxdcs.sparepart s
            on s.code = l.item
         where t.shipment_id = 'POP110120170613000118')
  and warehousearea.status=1;


--【库存不足F1104313100141A1764】  看dif
select v.*, l.total_qty, v.Quantity - l.total_qty as dif
  from (SELECT PartsStock.partid,
               (select code from sddcs.sparepart where id = PartsStock.Partid) as Partcode,
               PartsStock.quantity,
               (select code from sddcs.warehouse where id = PartsStock.warehouseid) as Warehouseid
          FROM sddcs.PartsStock
         INNER JOIN sddcs.WarehouseAreaCategory
            ON PartsStock.WarehouseAreaCategoryId = WarehouseAreaCategory.ID
         inner join sddcs.warehousearea
            on warehousearea.id = PartsStock.Warehouseareaid
         WHERE WarehouseAreaCategory.Category = 1
           AND PartsStock.WarehouseId =
               (select Warehouseid
                  from sddcs.Partsoutboundplan
                 where code = 'POP229020170629000003')
           AND PartsStock.Partid in
               (select s.id
                  from sddcs.t_Shippingorderhead t
                 inner join sddcs.t_Shippingorderdetails l
                    on t.interface_record_id = l.Interface_Link_Id
                 inner join sddcs.sparepart s
                    on s.code = l.item
                 where t.shipment_id = 'POP229020170629000003')
           and warehousearea.status = 1) v
 inner join sddcs.t_Shippingorderdetails l
    on l.item = v.Partcode
 inner join sddcs.t_Shippingorderhead t
    on t.interface_record_id = l.Interface_Link_Id
 where t.shipment_id = 'POP229020170629000003';
