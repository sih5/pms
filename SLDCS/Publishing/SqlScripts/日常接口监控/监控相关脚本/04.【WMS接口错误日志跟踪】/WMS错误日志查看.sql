select t.code, synctype,count(1)
  from yxdcs.Syncwmsinoutloginfo t
 where t.syncdate >= trunc(sysdate)
 group by t.code, synctype;

select t.code, synctype,count(1)
  from sddcs.Syncwmsinoutloginfo t
 where t.syncdate >= trunc(sysdate)
 group by t.code, synctype;

select t.code, synctype,count(1)
  from dcs.Syncwmsinoutloginfo t
 where t.syncdate >= trunc(sysdate)
 group by t.code, synctype;

select distinct t.inoutcode,t.code, synctype
  from yxdcs.Syncwmsinoutloginfo t
where t.syncdate >= trunc(sysdate);

--报文核查
select *
  from yxdcs.EsbLogsap t
 where t.message like '%PIP110120170615000056%'
 and thedate >=  trunc(sysdate-5)

select count(*)
  from yxdcs.partsoutboundbill t
 inner join yxdcs.ESBLogSAP te
    on te.interfacename = 'Asap_PartsOutboundBill_SYNC'
   and te.thedate > to_date('2017-06-06 00:00:00', 'yyyy-mm-dd hh24:mi:ss')
 where t.createtime >
       to_date('2017-06-06 00:00:00', 'yyyy-mm-dd hh24:mi:ss')
   and t.PartsSalesCategoryId = 8
   and instr(te.message, t.code) > 0;


--入库单
select *
  from yxdcs.Syncwmsinoutloginfo t
 where t.syncdate >= trunc(sysdate)
   and t.synctype = 1;

--发运签收单
select *
  from yxdcs.Syncwmsinoutloginfo t
 where t.syncdate >= trunc(sysdate)
   and t.synctype = 2;

--库存调整单
select *
  from yxdcs.Syncwmsinoutloginfo t
 where t.syncdate >= trunc(sysdate)
   and t.synctype = 3;

--WMS仓库表信息
select *
  from yxdcs.Syncwmsinoutloginfo t
 where t.syncdate >= trunc(sysdate)
   and t.synctype = 4;

--上架单
select *
  from yxdcs.Syncwmsinoutloginfo t
 where t.syncdate >= trunc(sysdate)
   and t.synctype = 5;

--【入库单中间表】   
select * from yxdcs.t_in where sourcecode = 'PIP110120170517000122';

select *
  from yxdcs.Partsinboundplan
 where code = (select sourcecode
                 from yxdcs.t_in
                where sourcecode = 'PIP110120170517000122');

--【发运单中间表】
/*看isfinished是否是1--完成，2--错误*/
select *
  from yxdcs.t_Shippingorderhead
 where shipment_id = 'POP110120170613000183';
 
--查看出库单
select *
  from yxdcs.PartsOutboundBill
 where PartsOutboundPlanId =
       (select id
          from yxdcs.PartsOutboundPlan
         where code = 'POP110120170613000183');
 

select t.shipment_id,l.item
  from yxdcs.t_Shippingorderhead t
 inner join yxdcs.t_Shippingorderdetails l
    on t.interface_record_id = l.Interface_Link_Id
 where t.shipment_id = 'POP110120170613000118';

--查看库存
select *
  from yxdcs.PartsStock t
 where t.partid =
       (select id from yxdcs.sparepart where code = 'R3104210HF15019AA1552')
   and t.warehouseid =
       (select warehouseid
          from yxdcs.PartsOutboundPlan
         where code = 'POP110120170613000118');

--查看销售订单         
select *
  from yxdcs.PartsSalesOrder t
 inner join yxdcs.PartsSalesOrderDetail l
    on t.id = l.PartsSalesOrderId
 where t.code = 'PSO1101201706130018'         

