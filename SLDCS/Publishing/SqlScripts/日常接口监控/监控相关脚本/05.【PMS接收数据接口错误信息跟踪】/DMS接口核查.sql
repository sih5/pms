/*
DMS接口 错误信息，先处理 客户与产品信息，这2者不分先后，车辆信息必须是最后处理
*/


select b.code           as DMS客户编号,
       c.customercode   as PMS配件编号,
       b.IdDocumentType as DMS证件类型,
       handlemessage    as DMS错误信息
  from (SELECT code, handlemessage, IdDocumentType
          FROM dcs.SEP_DTP_CUSTOMER t
         WHERE HandleStatus = 3
           and t.handletime >= trunc(sysdate - 1)) b
  left join dcs.customer c
    on b.code = c.customercode;