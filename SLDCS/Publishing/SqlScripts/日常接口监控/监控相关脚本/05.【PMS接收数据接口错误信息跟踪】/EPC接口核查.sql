/*若PMS配件编号为空，则通知黄慧明，告知上游接口*/

select b.code as EPC配件编号, s.code as PMS配件编号
  from (SELECT distinct code
          FROM yxdcs.SEP_SPAREPARTINFO t
         WHERE ProcessStatus = 3
           and t.LastProcessTime >= trunc(sysdate - 1)) b
  left join yxdcs.sparepart s
    on b.code = s.code
