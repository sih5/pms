declare
  iCount   number(9);
  SqlStr01 varchar2(4000);
  SqlStr02 varchar2(4000);
begin

  /*AbroadSAP 海外  销售订单*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.PartsSalesOrderASAP t
           WHERE HandleStatus = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.PartsSalesOrderASAP t
           WHERE HandleStatus = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.PartsSalesOrderASAP t
           WHERE HandleStatus = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.PartsSalesOrderASAP t
           WHERE HandleStatus = 3
             and t.modifytime >= trunc(sysdate - 1));
  SqlStr01 := SqlStr01 || 'select ''AbroadSAP 海外  销售订单'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*AbroadSAP 海外  配件销售退货*/
  SELECT count(1)
    into iCount
    FROM yxdcs.PartsSalesReturnBillSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  SqlStr01 := SqlStr01 ||
              ' union all select ''AbroadSAP 海外 配件销售退货'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*SELECT t.message, t.*
  FROM sddcs.PartsSalesReturnBillSap t
  WHERE HandleStatus = 3
  and t.modifytime >= trunc(sysdate - 1);
    
  SELECT t.message, t.*
  FROM dcs.PartsSalesReturnBillSap t
  WHERE HandleStatus = 3
  and t.modifytime >= trunc(sysdate - 1);
    
  SELECT t.message, t.*
  FROM gcdcs.PartsSalesReturnBillSap t
  WHERE HandleStatus = 3
  and t.modifytime >= trunc(sysdate - 1); */

  /*AbroadSAP 海外  配件采购计划信息*/
  SELECT count(1)
    into iCount
    FROM yxdcs.PartsPurchasePlanSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  SqlStr01 := SqlStr01 ||
              ' union all select ''AbroadSAP 海外  配件采购计划信息'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*SELECT t.message, t.*
  FROM sddcs.PartsPurchasePlanSap t
  WHERE HandleStatus = 3
  and t.modifytime >= trunc(sysdate - 1);
    
  SELECT t.message, t.*
  FROM dcs.PartsPurchasePlanSap t
  WHERE HandleStatus = 3
  and t.modifytime >= trunc(sysdate - 1);
    
  SELECT t.message, t.*
  FROM gcdcs.PartsPurchasePlanSap t
  WHERE HandleStatus = 3
  and t.modifytime >= trunc(sysdate - 1);*/

  /*AbroadSAP 海外  出库包装计划*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.PartsOutboundPlanASAP t
           WHERE HandleStatus = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.PartsOutboundPlanASAP t
           WHERE HandleStatus = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.PartsOutboundPlanASAP t
           WHERE HandleStatus = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.PartsOutboundPlanASAP t
           WHERE HandleStatus = 3
             and t.modifytime >= trunc(sysdate - 1));
  SqlStr01 := SqlStr01 ||
              ' union all select ''AbroadSAP 海外  出库包装计划'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*AbroadSAP 海外  配件分品牌信息*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.PartsBranchXml t
           WHERE HandleStatus = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          /*SELECT t.message, t.*
          FROM sddcs.PartsBranchXml t
          WHERE HandleStatus = 3
          and t.modifytime >= trunc(sysdate - 1);*/
          
          SELECT count(1) as num
            FROM dcs.PartsBranchXml t
           WHERE HandleStatus = 3
             and t.modifytime >= trunc(sysdate - 1));

  SqlStr01 := SqlStr01 ||
              ' union all select ''AbroadSAP 海外  配件分品牌信息'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*SELECT t.message, t.*
  FROM gcdcs.PartsBranchXml t
  WHERE HandleStatus = 3
  and t.modifytime >= trunc(sysdate - 1);*/

  /*AbroadSAP 海外  配件采购退货*/
  SELECT count(1)
    into iCount
    FROM yxdcs.PartsPurReturnOrderSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  SqlStr01 := SqlStr01 ||
              ' union all select ''AbroadSAP 海外  配件采购退货'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*SELECT t.message, t.*
  FROM sddcs.PartsPurReturnOrderSap t
  WHERE HandleStatus = 3
  and t.modifytime >= trunc(sysdate - 1);
    
  SELECT t.message, t.*
  FROM dcs.PartsPurReturnOrderSap t
  WHERE HandleStatus = 3
  and t.modifytime >= trunc(sysdate - 1);
    
  SELECT t.message, t.*
  FROM gcdcs.PartsPurReturnOrderSap t
  WHERE HandleStatus = 3
  and t.modifytime >= trunc(sysdate - 1);*/

  /*APP  维修工单全媒体(车联网APP)*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.AppRepairWorkOrderMd t
           WHERE HandleStatus = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.AppRepairWorkOrderMd t
           WHERE HandleStatus = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          
          SELECT count(1) as num
            FROM dcs.AppRepairWorkOrderMd t
           WHERE HandleStatus = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          
          SELECT count(1) as num
            FROM gcdcs.AppRepairWorkOrderMd t
           WHERE HandleStatus = 3
             and t.modifytime >= trunc(sysdate - 1));
  SqlStr01 := SqlStr01 ||
              ' union all select ''APP  维修工单全媒体(车联网APP)'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*BPM 采购计划终止审批结果*/
  SELECT count(1)
    into iCount
    FROM yxdcs.PartsPurchasePlanHWBPM t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  SqlStr01 := SqlStr01 ||
              ' union all select ''BPM 采购计划终止审批结果'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*SELECT t.message, t.*
  FROM sddcs.PartsPurchasePlanHWBPM t
  WHERE HandleStatus = 3
  and t.modifytime >= trunc(sysdate - 1);
    
  SELECT t.message, t.*
  FROM dcs.PartsPurchasePlanHWBPM t
  WHERE HandleStatus = 3
  and t.modifytime >= trunc(sysdate - 1);
    
  SELECT t.message, t.*
  FROM gcdcs.PartsPurchasePlanHWBPM t
  WHERE HandleStatus = 3
  and t.modifytime >= trunc(sysdate - 1);*/

  /*Cummins 康明斯*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.sep_cummins_settlefilelog t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          
          SELECT count(1) as num
            FROM sddcs.sep_cummins_settlefilelog t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.sep_cummins_settlefilelog t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.sep_cummins_settlefilelog t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1));
  SqlStr01 := SqlStr01 ||
              ' union all select ''Cummins 康明斯'' as 接口名,to_char(' || iCount ||
              ') as 数量 from dual';

  /*DMS 车辆信息*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.SEP_DTP_VEHICLEINFORMATION t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          
          SELECT count(1) as num
            FROM sddcs.SEP_DTP_VEHICLEINFORMATION t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          
          SELECT count(1) as num
            FROM dcs.SEP_DTP_VEHICLEINFORMATION t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          
          SELECT count(1) as num
            FROM gcdcs.SEP_DTP_VEHICLEINFORMATION t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1));
  SqlStr01 := SqlStr01 || ' union all select ''DMS 车辆信息'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*DMS 车辆联系人信息*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.SEP_DTP_VEHICLELINKMAN t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.SEP_DTP_VEHICLELINKMAN t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.SEP_DTP_VEHICLELINKMAN t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.SEP_DTP_VEHICLELINKMAN t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1));
  SqlStr01 := SqlStr01 ||
              ' union all select ''DMS 车辆联系人信息'' as 接口名,to_char(' || iCount ||
              ') as 数量 from dual';

  /*DMS 客户信息*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.SEP_DTP_CUSTOMER t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.SEP_DTP_CUSTOMER t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          
          SELECT count(1) as num
            FROM dcs.SEP_DTP_CUSTOMER t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.SEP_DTP_CUSTOMER t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1));
  SqlStr01 := SqlStr01 || ' union all select ''DMS 客户信息'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*DMS 产品信息*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.SEP_DTP_PRODUCT t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.SEP_DTP_PRODUCT t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.SEP_DTP_PRODUCT t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.SEP_DTP_PRODUCT t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1));
  SqlStr01 := SqlStr01 || ' union all select ''DMS 产品信息'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*EPC 配件信息*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.SEP_SPAREPARTINFO t
           WHERE ProcessStatus = 3
             and t.LastProcessTime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.SEP_SPAREPARTINFO t
           WHERE ProcessStatus = 3
             and t.LastProcessTime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.SEP_SPAREPARTINFO t
           WHERE ProcessStatus = 3
             and t.LastProcessTime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.SEP_SPAREPARTINFO t
           WHERE ProcessStatus = 3
             and t.LastProcessTime >= trunc(sysdate - 1)
          union all
          /*SELECT  t.*
          FROM yxdcs.SEP_SPAREPARTINFO_SD t
          WHERE ProcessStatus = 3
          and t.LastProcessTime >= trunc(sysdate - 1);*/
          /* SELECT  t.*
          FROM dcs.SEP_SPAREPARTINFO_SD t
          WHERE ProcessStatus = 3
          and t.LastProcessTime >= trunc(sysdate - 1);
                    
          SELECT  t.*
          FROM gcdcs.SEP_SPAREPARTINFO_SD t
          WHERE ProcessStatus = 3
          and t.LastProcessTime >= trunc(sysdate - 1); */
          SELECT count(1) as num
            FROM sddcs.SEP_SPAREPARTINFO_SD t
           WHERE ProcessStatus = 3
             and t.LastProcessTime >= trunc(sysdate - 1));
  SqlStr01 := SqlStr01 || ' union all select ''EPC 配件信息'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*ERP 电商信息*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.ERPDeliverydetail t
           WHERE syncstatus = 3
             and t.synctime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.ERPDeliverydetail t
           WHERE syncstatus = 3
             and t.synctime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.ERPDeliverydetail t
           WHERE syncstatus = 3
             and t.synctime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.ERPDeliverydetail t
           WHERE syncstatus = 3
             and t.synctime >= trunc(sysdate - 1));
  SqlStr01 := SqlStr01 || ' union all select ''ERP 电商信息'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*ESB-YX 收付款信息写入*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.SAP_YX_PAYMENTINFO t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.SAP_YX_PAYMENTINFO t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.SAP_YX_PAYMENTINFO t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.SAP_YX_PAYMENTINFO t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1));
  SqlStr01 := SqlStr01 ||
              ' union all select ''ESB-YX 收付款信息写入'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*ESB-YX 采购发票信息回传*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.SAP_YX_PURINVOICEPASSBACK t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.SAP_YX_PURINVOICEPASSBACK t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.SAP_YX_PURINVOICEPASSBACK t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.SAP_YX_PURINVOICEPASSBACK t
           WHERE HandleStatus = 3
             and t.handletime >= trunc(sysdate - 1));
  SqlStr01 := SqlStr01 ||
              ' union all select ''ESB-YX 采购发票信息回传'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*ESB-YX 采购退货开红票*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.SAP_YX_INVOICEVERACCOUNTINFO t
           WHERE Status = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.SAP_YX_INVOICEVERACCOUNTINFO t
           WHERE Status = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.SAP_YX_INVOICEVERACCOUNTINFO t
           WHERE Status = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.SAP_YX_INVOICEVERACCOUNTINFO t
           WHERE Status = 3
             and t.modifytime >= trunc(sysdate - 1));
  SqlStr01 := SqlStr01 ||
              ' union all select ''ESB-YX 采购退货开红票'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*ESB-YX 销售结算（1对1），销售退货结算，销售退货开红票*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.SAP_YX_SALESBILLINFO t
           WHERE Status = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.SAP_YX_SALESBILLINFO t
           WHERE Status = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.SAP_YX_SALESBILLINFO t
           WHERE Status = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.SAP_YX_SALESBILLINFO t
           WHERE Status = 3
             and t.modifytime >= trunc(sysdate - 1));
  SqlStr01 := SqlStr01 ||
              ' union all select ''ESB-YX 销售退货开红票'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*ESB-YX 电商 销售结算（1对1），销售退货结算，销售退货开红票*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.SAP_YX_RetailerSALESBILLINFO t
           WHERE Status = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          /*SELECT count(1)
          into iCount
          FROM sddcs.SAP_YX_RetailerSALESBILLINFO t
          WHERE Status = 3
          and t.modifytime >= trunc(sysdate - 1);
          if iCount <> 0 then
          SqlStr01 := SqlStr01 || '[sddcs.SAP_YX_RetailerSALESBILLINFO]:' || to_char(iCount) || ',' ||
          chr(13);
          end if;*/
          /*SELECT count(1)
          into iCount
          FROM gcdcs.SAP_YX_RetailerSALESBILLINFO t
          WHERE Status = 3
          and t.modifytime >= trunc(sysdate - 1);
          if iCount <> 0 then
          SqlStr01 := SqlStr01 || '[gcdcs.SAP_YX_RetailerSALESBILLINFO]:' || to_char(iCount) || ',' ||
          chr(13);
          end if;*/
          SELECT count(1) as num
            FROM dcs.SAP_YX_RetailerSALESBILLINFO t
           WHERE Status = 3
             and t.modifytime >= trunc(sysdate - 1));
  SqlStr01 := SqlStr01 ||
              ' union all select ''ESB-YX 电商销售退货开红票'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*IMS 发运单接口*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.IMSShippingLoginInfo t
           WHERE syncstatus = 3
             and t.synctime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.IMSShippingLoginInfo t
           WHERE syncstatus = 3
             and t.synctime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.IMSShippingLoginInfo t
           WHERE syncstatus = 3
             and t.synctime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.IMSShippingLoginInfo t
           WHERE syncstatus = 3
             and t.synctime >= trunc(sysdate - 1));
  SqlStr02 := SqlStr02 || ' union all select ''IMS 发运单接口'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*Qtsinfo sep_qtp_QTSINFO*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.sep_qtp_QTSINFO t
           WHERE handleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.sep_qtp_QTSINFO t
           WHERE handleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.sep_qtp_QTSINFO t
           WHERE handleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.sep_qtp_QTSINFO t
           WHERE handleStatus = 3
             and t.handletime >= trunc(sysdate - 1));
  SqlStr02 := SqlStr02 ||
              ' union all select ''Qtsinfo sep_qtp_QTSINFO'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*Qtsinfo qtp_QTSINFO*/
  select sum(num)
    into iCount
    from (
          /*SELECT count(1) as num
            FROM yxdcs.qtp_QTSINFO t
           WHERE handleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.qtp_QTSINFO t
           WHERE handleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all*/
          SELECT count(1) as num
            FROM dcs.qtp_QTSINFO t
           WHERE handleStatus = 3
             and t.handletime >= trunc(sysdate - 1)
          /*union all
          SELECT count(1) as num
          FROM gcdcs.qtp_QTSINFO t
          WHERE handleStatus = 3
          and t.handletime >= trunc(sysdate - 1)*/
          );
  SqlStr02 := SqlStr02 ||
              ' union all select ''Qtsinfo qtp_QTSINFO'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*RETAILER 电商 零售商-交付*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.Retailer_DeliveryDetail t
           WHERE syncstatus = 3
             and t.synctime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.Retailer_DeliveryDetail t
           WHERE syncstatus = 3
             and t.synctime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.Retailer_DeliveryDetail t
           WHERE syncstatus = 3
             and t.synctime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.Retailer_DeliveryDetail t
           WHERE syncstatus = 3
             and t.synctime >= trunc(sysdate - 1));
  SqlStr02 := SqlStr02 ||
              ' union all select ''RETAILER 电商 零售商-交付'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*SAP-FD 采购结算发票*/
  select sum(num)
    into iCount
    from (
          /*SELECT count(1) as num
            FROM yxdcs.sap_fd_invoiceveraccountinfo t
           WHERE status = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.sap_fd_invoiceveraccountinfo t
           WHERE status = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all*/
          SELECT count(1) as num
            FROM dcs.sap_fd_invoiceveraccountinfo t
           WHERE status = 3
             and t.modifytime >= trunc(sysdate - 1)
          /*union all
          SELECT count(1) as num
          FROM gcdcs.sap_fd_invoiceveraccountinfo t
          WHERE status = 3
          and t.modifytime >= trunc(sysdate - 1)*/
          );
  SqlStr02 := SqlStr02 ||
              ' union all select ''SAP-FD 采购结算发票'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*SAP-FD 采购退货开蓝票*/
  select sum(num)
    into iCount
    from (
          /*SELECT count(1) as num
            FROM yxdcs.SAP_FD_PurchaseRtnInvoice t
           WHERE status = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.SAP_FD_PurchaseRtnInvoice t
           WHERE status = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all*/
          SELECT count(1) as num
            FROM dcs.SAP_FD_PurchaseRtnInvoice t
           WHERE status = 3
             and t.modifytime >= trunc(sysdate - 1)
          /*union all
          SELECT count(1) as num
          FROM gcdcs.SAP_FD_PurchaseRtnInvoice t
          WHERE status = 3
          and t.modifytime >= trunc(sysdate - 1)*/
          );
  SqlStr02 := SqlStr02 ||
              ' union all select ''SAP-FD 采购退货开蓝票'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*SAP-FD 销售发票及销售退货发票*/
  select sum(num)
    into iCount
    from (
          /*SELECT count(1) as num
            FROM yxdcs.sap_fd_salesbillinfo t
           WHERE status = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.sap_fd_salesbillinfo t
           WHERE status = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all*/
          SELECT count(1) as num
            FROM dcs.SAP_FD_PurchaseRtnInvoice t
           WHERE status = 3
             and t.modifytime >= trunc(sysdate - 1)
          /*union all
          SELECT count(1) as num
          FROM gcdcs.sap_fd_salesbillinfo t
          WHERE status = 3
          and t.modifytime >= trunc(sysdate - 1)*/
          );
  SqlStr02 := SqlStr02 ||
              ' union all select ''SAP-FD 销售发票及销售退货发票'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*Siebel 呼叫中心 回访问卷*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.NccQuestsessionId t
           WHERE handlestatus = 3
             and t.sendtime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.NccQuestsessionId t
           WHERE handlestatus = 3
             and t.sendtime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.NccQuestsessionId t
           WHERE handlestatus = 3
             and t.sendtime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.NccQuestsessionId t
           WHERE handlestatus = 3
             and t.sendtime >= trunc(sysdate - 1));
  SqlStr02 := SqlStr02 ||
              ' union all select ''Siebel 呼叫中心 回访问卷'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*Siebel 呼叫中心 维修工单全媒体*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.NccRepairWorkOrderMd t
           WHERE handlestatus = 3
             and t.modifytime >= trunc(sysdate - 1)
          /*union all
          SELECT count(1) as num
          FROM sddcs.NccRepairWorkOrderMd t
          WHERE handlestatus = 3
          and t.modifytime >= trunc(sysdate - 1)*/
          union all
          SELECT count(1) as num
            FROM dcs.NccRepairWorkOrderMd t
           WHERE handlestatus = 3
             and t.modifytime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.NccRepairWorkOrderMd t
           WHERE handlestatus = 3
             and t.modifytime >= trunc(sysdate - 1));
  SqlStr02 := SqlStr02 ||
              ' union all select ''Siebel 呼叫中心 维修工单全媒体'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*Stibo MDM Cs_Stibo*/
  select sum(num)
    into iCount
    from (
          /*SELECT count(1) as num
          FROM yxdcs.Cs_Stibo t
          WHERE syncstatus = 3
          and t.synctime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
          FROM sddcs.Cs_Stibo t
          WHERE syncstatus = 3
          and t.synctime >= trunc(sysdate - 1)
          union all*/
          SELECT count(1) as num
            FROM dcs.Cs_Stibo t
           WHERE syncstatus = 3
             and t.synctime >= trunc(sysdate - 1)
          /*union all
          SELECT count(1) as num
          FROM gcdcs.Cs_Stibo t
          WHERE syncstatus = 3
          and t.synctime >= trunc(sysdate - 1)*/
          );
  SqlStr02 := SqlStr02 ||
              ' union all select ''Stibo MDM Cs_Stibo'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*STMS SEP_STMS_MALCATEGORY*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.SEP_STMS_MALCATEGORY t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.SEP_STMS_MALCATEGORY t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.SEP_STMS_MALCATEGORY t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.SEP_STMS_MALCATEGORY t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1));
  SqlStr02 := SqlStr02 ||
              ' union all select ''STMS SEP_STMS_MALCATEGORY'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*STMS SEP_STMS_MALFUNCTION*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.SEP_STMS_MALFUNCTION t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.SEP_STMS_MALFUNCTION t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.SEP_STMS_MALFUNCTION t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.SEP_STMS_MALFUNCTION t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1));
  SqlStr02 := SqlStr02 ||
              ' union all select ''STMS SEP_STMS_MALFUNCTION'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*STMS SEP_STMS_REPITEM*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.SEP_STMS_REPITEM t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.SEP_STMS_REPITEM t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.SEP_STMS_REPITEM t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.SEP_STMS_REPITEM t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1));
  SqlStr02 := SqlStr02 ||
              ' union all select ''STMS SEP_STMS_REPITEM'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*STMS SEP_STMS_MALBRANDREL*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.SEP_STMS_MALBRANDREL t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.SEP_STMS_MALBRANDREL t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.SEP_STMS_MALBRANDREL t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.SEP_STMS_MALBRANDREL t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1));
  SqlStr02 := SqlStr02 ||
              ' union all select ''STMS SEP_STMS_MALBRANDREL'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*STMS SEP_STMS_CARREPITEMPRODLINE*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.SEP_STMS_CARREPITEMPRODLINE t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.SEP_STMS_CARREPITEMPRODLINE t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.SEP_STMS_CARREPITEMPRODLINE t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.SEP_STMS_CARREPITEMPRODLINE t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1));
  SqlStr02 := SqlStr02 ||
              ' union all select ''STMS SEP_STMS_CARREPITEMPRODLINE'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*STMS SEP_STMS_ENGINEREPITEMPRODLINE*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.SEP_STMS_ENGINEREPITEMPRODLINE t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.SEP_STMS_ENGINEREPITEMPRODLINE t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.SEP_STMS_ENGINEREPITEMPRODLINE t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM gcdcs.SEP_STMS_ENGINEREPITEMPRODLINE t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1));
  SqlStr02 := SqlStr02 ||
              ' union all select ''STMS SEP_STMS_ENGINEREPITEMPRODLINE'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*VIP 红包信息*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.Retailer_LuckyMoney t
           WHERE syncstatus = 3
             and t.synctime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.Retailer_LuckyMoney t
           WHERE syncstatus = 3
             and t.synctime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM dcs.Retailer_LuckyMoney t
           WHERE syncstatus = 3
             and t.synctime >= trunc(sysdate - 1)
          /*union all
          SELECT count(1) as num
            FROM gcdcs.Retailer_LuckyMoney t
           WHERE syncstatus = 3
             and t.synctime >= trunc(sysdate - 1)*/
          );
  SqlStr02 := SqlStr02 || ' union all select ''VIP 红包信息'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*VPC 维修单完毕信息*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.RepairWorkOrderFinishInfo t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.RepairWorkOrderFinishInfo t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          /*union all
          SELECT count(1) as num
            FROM dcs.RepairWorkOrderFinishInfo t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)*/
          union all
          SELECT count(1) as num
            FROM gcdcs.RepairWorkOrderFinishInfo t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1));
  SqlStr02 := SqlStr02 ||
              ' union all select ''VPC 维修单完毕信息'' as 接口名,to_char(' || iCount ||
              ') as 数量 from dual';

  /*VPC 维修确定信息*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.RepairWorkOrderConfirmInfo t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          union all
          SELECT count(1) as num
            FROM sddcs.RepairWorkOrderConfirmInfo t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)
          /*union all
          SELECT count(1) as num
            FROM dcs.RepairWorkOrderConfirmInfo t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1)*/
          union all
          SELECT count(1) as num
            FROM gcdcs.RepairWorkOrderConfirmInfo t
           WHERE handlestatus = 3
             and t.handletime >= trunc(sysdate - 1));
  SqlStr02 := SqlStr02 ||
              ' union all select ''VPC 维修确定信息'' as 接口名,to_char(' || iCount ||
              ') as 数量 from dual';

  /*WMS 入库单*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.Syncwmsinoutloginfo t
           WHERE t.syncdate >= trunc(sysdate - 1)
             and t.synctype = 1
             and t.status = 0
          union all
          SELECT count(1) as num
            FROM sddcs.Syncwmsinoutloginfo t
           WHERE t.syncdate >= trunc(sysdate - 1)
             and t.synctype = 1
             and t.status = 0
          union all
          SELECT count(1) as num
            FROM dcs.Syncwmsinoutloginfo t
           WHERE t.syncdate >= trunc(sysdate - 1)
             and t.synctype = 1
             and t.status = 0);
  SqlStr02 := SqlStr02 || ' union all select ''WMS 入库单'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*WMS 发运签收单*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.Syncwmsinoutloginfo t
           WHERE t.syncdate >= trunc(sysdate - 1)
             and t.synctype = 2
             and t.status = 0
          union all
          SELECT count(1) as num
            FROM sddcs.Syncwmsinoutloginfo t
           WHERE t.syncdate >= trunc(sysdate - 1)
             and t.synctype = 2
             and t.status = 0
          union all
          SELECT count(1) as num
            FROM dcs.Syncwmsinoutloginfo t
           WHERE t.syncdate >= trunc(sysdate - 1)
             and t.synctype = 2
             and t.status = 0);
  SqlStr02 := SqlStr02 || ' union all select ''WMS 发运签收单'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*WMS 库存调整单*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.Syncwmsinoutloginfo t
           WHERE t.syncdate >= trunc(sysdate - 1)
             and t.synctype = 3
             and t.status = 0
          union all
          SELECT count(1) as num
            FROM sddcs.Syncwmsinoutloginfo t
           WHERE t.syncdate >= trunc(sysdate - 1)
             and t.synctype = 3
             and t.status = 0
          union all
          SELECT count(1) as num
            FROM dcs.Syncwmsinoutloginfo t
           WHERE t.syncdate >= trunc(sysdate - 1)
             and t.synctype = 3
             and t.status = 0);
  SqlStr02 := SqlStr02 || ' union all select ''WMS 库存调整单'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*WMS 仓库表信息*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.Syncwmsinoutloginfo t
           WHERE t.syncdate >= trunc(sysdate - 1)
             and t.synctype = 4
             and t.status = 0
          union all
          SELECT count(1) as num
            FROM sddcs.Syncwmsinoutloginfo t
           WHERE t.syncdate >= trunc(sysdate - 1)
             and t.synctype = 4
             and t.status = 0
          union all
          SELECT count(1) as num
            FROM dcs.Syncwmsinoutloginfo t
           WHERE t.syncdate >= trunc(sysdate - 1)
             and t.synctype = 4
             and t.status = 0);
  SqlStr02 := SqlStr02 || ' union all select ''WMS 仓库表信息'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*WMS 上架单*/
  select sum(num)
    into iCount
    from (SELECT count(1) as num
            FROM yxdcs.Syncwmsinoutloginfo t
           WHERE t.syncdate >= trunc(sysdate - 1)
             and t.synctype = 5
             and t.status = 0
          union all
          SELECT count(1) as num
            FROM sddcs.Syncwmsinoutloginfo t
           WHERE t.syncdate >= trunc(sysdate - 1)
             and t.synctype = 5
             and t.status = 0
          union all
          SELECT count(1) as num
            FROM dcs.Syncwmsinoutloginfo t
           WHERE t.syncdate >= trunc(sysdate - 1)
             and t.synctype = 5
             and t.status = 0);
  SqlStr02 := SqlStr02 || ' union all select ''WMS 仓库表信息'' as 接口名,to_char(' ||
              iCount || ') as 数量 from dual';

  /*输出异常总数*/
  DBMS_OUTPUT.PUT_LINE(SqlStr01);
  DBMS_OUTPUT.PUT_LINE(SqlStr02);
end;
