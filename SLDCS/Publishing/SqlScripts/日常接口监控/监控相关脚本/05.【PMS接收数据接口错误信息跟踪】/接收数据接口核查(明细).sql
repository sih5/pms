declare
  iCount   number(9);
  SqlStr01 varchar2(4000);
  SqlStr02 varchar2(4000);
  SqlStr03 varchar2(4000);
  SqlStr04 varchar2(4000);
  SqlStr05 varchar2(4000);
  SqlStr06 varchar2(4000);
  SqlStr07 varchar2(4000);
  SqlStr08 varchar2(4000);
  SqlStr09 varchar2(4000);
  SqlStr10 varchar2(4000);
  SqlStr11 varchar2(4000);
  SqlStr12 varchar2(4000);
  SqlStr13 varchar2(4000);
  SqlStr14 varchar2(4000);
  SqlStr15 varchar2(4000);
  SqlStr16 varchar2(4000);
  SqlStr17 varchar2(4000);
  SqlStr18 varchar2(4000);
  SqlStr19 varchar2(4000);
  SqlStr20 varchar2(4000);
begin

  SqlStr01 := 'select '''' as 接口, '''' as 表名 ,'''' as 报错信息,sysdate as 出错时间,1 as 唯一标识ID from dual';

  /*AbroadSAP 海外  销售订单*/
  SELECT count(1)
    into iCount
    FROM yxdcs.PartsSalesOrderASAP t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr01 := SqlStr01 || ' union all SELECT ''AbroadSAP 海外  销售订单'' as 接口,''yxdcs.PartsSalesOrderASAP'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.AppRepairWorkOrderMd t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.PartsSalesOrderASAP t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr01 := SqlStr01 || ' union all SELECT ''AbroadSAP 海外  销售订单'' as 接口,''sddcs.PartsSalesOrderASAP'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.AppRepairWorkOrderMd t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.PartsSalesOrderASAP t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr01 := SqlStr01 || ' union all SELECT ''AbroadSAP 海外  销售订单'' as 接口,''dcs.PartsSalesOrderASAP'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM dcs.AppRepairWorkOrderMd t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.PartsSalesOrderASAP t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr01 := SqlStr01 || ' union all SELECT ''AbroadSAP 海外  销售订单'' as 接口,''gcdcs.PartsSalesOrderASAP'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.AppRepairWorkOrderMd t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*AbroadSAP 海外  配件销售退货*/
  SELECT count(1)
    into iCount
    FROM yxdcs.PartsSalesReturnBillSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr01 := SqlStr01 || ' union all SELECT ''AbroadSAP 海外  配件销售退货'' as 接口,''yxdcs.PartsSalesReturnBillSap'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.PartsSalesReturnBillSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*SELECT t.message, t.*
    FROM sddcs.PartsSalesReturnBillSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  
  SELECT t.message, t.*
    FROM dcs.PartsSalesReturnBillSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  
  SELECT t.message, t.*
    FROM gcdcs.PartsSalesReturnBillSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1); */

  /*AbroadSAP 海外  配件采购计划信息*/
  SELECT count(1)
    into iCount
    FROM yxdcs.PartsPurchasePlanSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr01 := SqlStr01 || ' union all SELECT ''AbroadSAP 海外  配件采购计划信息'' as 接口,''yxdcs.PartsPurchasePlanSap'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.PartsPurchasePlanSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*SELECT t.message, t.*
    FROM sddcs.PartsPurchasePlanSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  
  SELECT t.message, t.*
    FROM dcs.PartsPurchasePlanSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  
  SELECT t.message, t.*
    FROM gcdcs.PartsPurchasePlanSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);*/

  /*AbroadSAP 海外  出库包装计划*/
  SELECT count(1)
    into iCount
    FROM yxdcs.PartsOutboundPlanASAP t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr01 := SqlStr01 || ' union all SELECT ''AbroadSAP 海外  出库包装计划'' as 接口,''yxdcs.PartsOutboundPlanASAP'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.PartsOutboundPlanASAP t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.PartsOutboundPlanASAP t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr02 := SqlStr02 || ' union all SELECT ''AbroadSAP 海外  出库包装计划'' as 接口,''sddcs.PartsOutboundPlanASAP'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.PartsOutboundPlanASAP t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.PartsOutboundPlanASAP t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr02 := SqlStr02 || ' union all SELECT ''AbroadSAP 海外  出库包装计划'' as 接口,''dcs.PartsOutboundPlanASAP'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM dcs.PartsOutboundPlanASAP t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.PartsOutboundPlanASAP t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr02 := SqlStr02 || ' union all SELECT ''AbroadSAP 海外  出库包装计划'' as 接口,''gcdcs.PartsOutboundPlanASAP'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.PartsOutboundPlanASAP t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*AbroadSAP 海外  配件分品牌信息*/
  SELECT count(1)
    into iCount
    FROM yxdcs.PartsBranchXml t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr02 := SqlStr02 || ' union all SELECT ''AbroadSAP 海外  配件分品牌信息'' as 接口,''yxdcs.PartsBranchXml'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.PartsBranchXml t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*SELECT t.message, t.*
   FROM sddcs.PartsBranchXml t
  WHERE HandleStatus = 3
    and t.modifytime >= trunc(sysdate - 1);*/

  SELECT count(1)
    into iCount
    FROM dcs.PartsBranchXml t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr02 := SqlStr02 || ' union all SELECT ''AbroadSAP 海外  配件分品牌信息'' as 接口,''dcs.PartsBranchXml'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM dcs.PartsBranchXml t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*SELECT t.message, t.*
   FROM gcdcs.PartsBranchXml t
  WHERE HandleStatus = 3
    and t.modifytime >= trunc(sysdate - 1);*/

  /*AbroadSAP 海外  配件采购退货*/
  SELECT count(1)
    into iCount
    FROM yxdcs.PartsPurReturnOrderSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr02 := SqlStr02 || ' union all SELECT ''AbroadSAP 海外  配件采购退货'' as 接口,''yxdcs.PartsPurReturnOrderSap'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.PartsPurReturnOrderSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*SELECT t.message, t.*
    FROM sddcs.PartsPurReturnOrderSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  
  SELECT t.message, t.*
    FROM dcs.PartsPurReturnOrderSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  
  SELECT t.message, t.*
    FROM gcdcs.PartsPurReturnOrderSap t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);*/

  /*APP  维修工单全媒体(车联网APP)*/
  SELECT count(1)
    into iCount
    FROM yxdcs.AppRepairWorkOrderMd t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr02 := SqlStr02 || ' union all SELECT ''APP  维修工单全媒体(车联网APP)'' as 接口,''yxdcs.AppRepairWorkOrderMd'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.AppRepairWorkOrderMd t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.AppRepairWorkOrderMd t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr03 := SqlStr03 || ' union all SELECT ''APP  维修工单全媒体(车联网APP)'' as 接口,''sddcs.AppRepairWorkOrderMd'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.AppRepairWorkOrderMd t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.AppRepairWorkOrderMd t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr03 := SqlStr03 || ' union all SELECT ''APP  维修工单全媒体(车联网APP)'' as 接口,''dcs.AppRepairWorkOrderMd'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM dcs.AppRepairWorkOrderMd t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.AppRepairWorkOrderMd t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr03 := SqlStr03 || ' union all SELECT ''APP  维修工单全媒体(车联网APP)'' as 接口,''gcdcs.AppRepairWorkOrderMd'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.AppRepairWorkOrderMd t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*BPM 采购计划终止审批结果*/
  SELECT count(1)
    into iCount
    FROM yxdcs.PartsPurchasePlanHWBPM t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr03 := SqlStr03 || ' union all SELECT ''BPM 采购计划终止审批结果'' as 接口,''yxdcs.PartsPurchasePlanHWBPM'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.PartsPurchasePlanHWBPM t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*SELECT t.message, t.*
    FROM sddcs.PartsPurchasePlanHWBPM t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  
  SELECT t.message, t.*
    FROM dcs.PartsPurchasePlanHWBPM t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  
  SELECT t.message, t.*
    FROM gcdcs.PartsPurchasePlanHWBPM t
   WHERE HandleStatus = 3
     and t.modifytime >= trunc(sysdate - 1);*/

  /*Cummins 康明斯*/
  SELECT count(1)
    into iCount
    FROM yxdcs.sep_cummins_settlefilelog t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr03 := SqlStr03 || ' union all SELECT ''Cummins 康明斯'' as 接口,''yxdcs.sep_cummins_settlefilelog'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.sep_cummins_settlefilelog t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.sep_cummins_settlefilelog t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr03 := SqlStr03 || ' union all SELECT ''Cummins 康明斯'' as 接口,''sddcs.sep_cummins_settlefilelog'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.sep_cummins_settlefilelog t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.sep_cummins_settlefilelog t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr03 := SqlStr03 || ' union all SELECT ''Cummins 康明斯'' as 接口,''dcs.sep_cummins_settlefilelog'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM dcs.sep_cummins_settlefilelog t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.sep_cummins_settlefilelog t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr04 := SqlStr04 || ' union all SELECT ''Cummins 康明斯'' as 接口,''gcdcs.sep_cummins_settlefilelog'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.sep_cummins_settlefilelog t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*DMS 车辆信息*/
  SELECT count(1)
    into iCount
    FROM yxdcs.SEP_DTP_VEHICLEINFORMATION t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr04 := SqlStr04 || ' union all SELECT ''DMS 车辆信息'' as 接口,''yxdcs.SEP_DTP_VEHICLEINFORMATION'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.SEP_DTP_VEHICLEINFORMATION t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.SEP_DTP_VEHICLEINFORMATION t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr04 := SqlStr04 || ' union all SELECT ''DMS 车辆信息'' as 接口,''sddcs.SEP_DTP_VEHICLEINFORMATION'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.SEP_DTP_VEHICLEINFORMATION t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.SEP_DTP_VEHICLEINFORMATION t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr04 := SqlStr04 || ' union all SELECT ''DMS 车辆信息'' as 接口,''dcs.SEP_DTP_VEHICLEINFORMATION'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM dcs.SEP_DTP_VEHICLEINFORMATION t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.SEP_DTP_VEHICLEINFORMATION t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr04 := SqlStr04 || ' union all SELECT ''DMS 车辆信息'' as 接口,''gcdcs.SEP_DTP_VEHICLEINFORMATION'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.SEP_DTP_VEHICLEINFORMATION t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*DMS 车辆联系人信息*/
  SELECT count(1)
    into iCount
    FROM yxdcs.SEP_DTP_VEHICLELINKMAN t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr04 := SqlStr04 || ' union all SELECT ''DMS 车辆联系人信息'' as 接口,''yxdcs.SEP_DTP_VEHICLELINKMAN'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.SEP_DTP_VEHICLELINKMAN t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.SEP_DTP_VEHICLELINKMAN t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr04 := SqlStr04 || ' union all SELECT ''DMS 车辆联系人信息'' as 接口,''sddcs.SEP_DTP_VEHICLELINKMAN'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.SEP_DTP_VEHICLELINKMAN t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.SEP_DTP_VEHICLELINKMAN t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr05 := SqlStr05 || ' union all SELECT ''DMS 车辆联系人信息'' as 接口,''dcs.SEP_DTP_VEHICLELINKMAN'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM dcs.SEP_DTP_VEHICLELINKMAN t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.SEP_DTP_VEHICLELINKMAN t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr05 := SqlStr05 || ' union all SELECT ''DMS 车辆联系人信息'' as 接口,''gcdcs.SEP_DTP_VEHICLELINKMAN'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.SEP_DTP_VEHICLELINKMAN t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*DMS 客户信息*/
  SELECT count(1)
    into iCount
    FROM yxdcs.SEP_DTP_CUSTOMER t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr05 := SqlStr05 || ' union all SELECT ''DMS 客户信息'' as 接口,''yxdcs.SEP_DTP_CUSTOMER'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.SEP_DTP_CUSTOMER t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.SEP_DTP_CUSTOMER t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr05 := SqlStr05 || ' union all SELECT ''DMS 客户信息'' as 接口,''sddcs.SEP_DTP_CUSTOMER'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.SEP_DTP_CUSTOMER t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.SEP_DTP_CUSTOMER t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr05 := SqlStr05 || ' union all SELECT ''DMS 客户信息'' as 接口,''dcs.SEP_DTP_CUSTOMER'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM dcs.SEP_DTP_CUSTOMER t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.SEP_DTP_CUSTOMER t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr05 := SqlStr05 || ' union all SELECT ''DMS 客户信息'' as 接口,''gcdcs.SEP_DTP_CUSTOMER'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.SEP_DTP_CUSTOMER t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*DMS 产品信息*/
  SELECT count(1)
    into iCount
    FROM yxdcs.SEP_DTP_PRODUCT t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr05 := SqlStr05 || ' union all SELECT ''DMS 产品信息'' as 接口,''yxdcs.SEP_DTP_PRODUCT'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.SEP_DTP_PRODUCT t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.SEP_DTP_PRODUCT t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr06 := SqlStr06 || ' union all SELECT ''DMS 产品信息'' as 接口,''sddcs.SEP_DTP_PRODUCT'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.SEP_DTP_PRODUCT t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.SEP_DTP_PRODUCT t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr06 := SqlStr06 || ' union all SELECT ''DMS 产品信息'' as 接口,''dcs.SEP_DTP_PRODUCT'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM dcs.SEP_DTP_PRODUCT t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.SEP_DTP_PRODUCT t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr06 := SqlStr06 || ' union all SELECT ''DMS 产品信息'' as 接口,''gcdcs.SEP_DTP_PRODUCT'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.SEP_DTP_PRODUCT t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*EPC 配件信息*/
  SELECT count(1)
    into iCount
    FROM yxdcs.SEP_SPAREPARTINFO t
   WHERE ProcessStatus = 3
     and t.LastProcessTime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr06 := SqlStr06 ||
                ' union all SELECT ''EPC 配件信息'' as 接口,''yxdcs.SEP_SPAREPARTINFO'' as 表名,
    to_char(t.errormessage) as 报错信息,t.LastProcessTime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.SEP_SPAREPARTINFO t
   WHERE ProcessStatus = 3
     and t.LastProcessTime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.SEP_SPAREPARTINFO t
   WHERE ProcessStatus = 3
     and t.LastProcessTime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr06 := SqlStr06 ||
                ' union all SELECT ''EPC 配件信息'' as 接口,''sddcs.SEP_SPAREPARTINFO'' as 表名,
    to_char(t.errormessage) as 报错信息,t.LastProcessTime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.SEP_SPAREPARTINFO t
   WHERE ProcessStatus = 3
     and t.LastProcessTime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.SEP_SPAREPARTINFO t
   WHERE ProcessStatus = 3
     and t.LastProcessTime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr06 := SqlStr06 ||
                ' union all SELECT ''EPC 配件信息'' as 接口,''dcs.SEP_SPAREPARTINFO'' as 表名,
    to_char(t.errormessage) as 报错信息,t.LastProcessTime as 出错时间,t.id as 唯一标识ID
    FROM dcs.SEP_SPAREPARTINFO t
   WHERE ProcessStatus = 3
     and t.LastProcessTime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.SEP_SPAREPARTINFO t
   WHERE ProcessStatus = 3
     and t.LastProcessTime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr06 := SqlStr06 ||
                ' union all SELECT ''EPC 配件信息'' as 接口,''gcdcs.SEP_SPAREPARTINFO'' as 表名,
    to_char(t.errormessage) as 报错信息,t.LastProcessTime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.SEP_SPAREPARTINFO t
   WHERE ProcessStatus = 3
     and t.LastProcessTime >= trunc(sysdate - 1)';
  end if;

  /*SELECT  t.*
   FROM yxdcs.SEP_SPAREPARTINFO_SD t
  WHERE ProcessStatus = 3
    and t.LastProcessTime >= trunc(sysdate - 1);*/

  SELECT count(1)
    into iCount
    FROM sddcs.SEP_SPAREPARTINFO_SD t
   WHERE ProcessStatus = 3
     and t.LastProcessTime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr07 := SqlStr07 ||
                ' union all SELECT ''EPC 配件信息'' as 接口,''sddcs.SEP_SPAREPARTINFO_SD'' as 表名,
    to_char(t.errormessage) as 报错信息,t.LastProcessTime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.SEP_SPAREPARTINFO_SD t
   WHERE ProcessStatus = 3
     and t.LastProcessTime >= trunc(sysdate - 1)';
  end if;

  /* SELECT  t.*
    FROM dcs.SEP_SPAREPARTINFO_SD t
   WHERE ProcessStatus = 3
     and t.LastProcessTime >= trunc(sysdate - 1);
  
  SELECT  t.*
    FROM gcdcs.SEP_SPAREPARTINFO_SD t
   WHERE ProcessStatus = 3
     and t.LastProcessTime >= trunc(sysdate - 1); */

  /*ERP 电商信息*/
  SELECT count(1)
    into iCount
    FROM yxdcs.ERPDeliverydetail t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr07 := SqlStr07 || ' union all SELECT ''ERP 电商信息'' as 接口,''yxdcs.ERPDeliverydetail'' as 表名,
    to_char(t.errormessage) as 报错信息,t.synctime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.ERPDeliverydetail t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.ERPDeliverydetail t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr07 := SqlStr07 || ' union all SELECT ''ERP 电商信息'' as 接口,''sddcs.ERPDeliverydetail'' as 表名,
    to_char(t.errormessage) as 报错信息,t.synctime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.ERPDeliverydetail t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.ERPDeliverydetail t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr07 := SqlStr07 || ' union all SELECT ''ERP 电商信息'' as 接口,''dcs.ERPDeliverydetail'' as 表名,
    to_char(t.errormessage) as 报错信息,t.synctime as 出错时间,t.id as 唯一标识ID
    FROM dcs.ERPDeliverydetail t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.ERPDeliverydetail t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr07 := SqlStr07 || ' union all SELECT ''ERP 电商信息'' as 接口,''gcdcs.ERPDeliverydetail'' as 表名,
    to_char(t.errormessage) as 报错信息,t.synctime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.ERPDeliverydetail t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1)';
  end if;

  /*ESB-YX 收付款信息写入*/
  SELECT count(1)
    into iCount
    FROM yxdcs.SAP_YX_PAYMENTINFO t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr07 := SqlStr07 || ' union all SELECT ''ESB-YX 收付款信息写入'' as 接口,''yxdcs.SAP_YX_PAYMENTINFO'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.SAP_YX_PAYMENTINFO t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.SAP_YX_PAYMENTINFO t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr07 := SqlStr07 || ' union all SELECT ''ESB-YX 收付款信息写入'' as 接口,''sddcs.SAP_YX_PAYMENTINFO'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.SAP_YX_PAYMENTINFO t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.SAP_YX_PAYMENTINFO t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr08 := SqlStr08 || ' union all SELECT ''ESB-YX 收付款信息写入'' as 接口,''dcs.SAP_YX_PAYMENTINFO'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM dcs.SAP_YX_PAYMENTINFO t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.SAP_YX_PAYMENTINFO t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr08 := SqlStr08 || ' union all SELECT ''ESB-YX 收付款信息写入'' as 接口,''gcdcs.SAP_YX_PAYMENTINFO'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.SAP_YX_PAYMENTINFO t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*ESB-YX 采购发票信息回传*/
  SELECT count(1)
    into iCount
    FROM yxdcs.SAP_YX_PURINVOICEPASSBACK t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr08 := SqlStr08 || ' union all SELECT ''ESB-YX 采购发票信息回传'' as 接口,''yxdcs.SAP_YX_PURINVOICEPASSBACK'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.SAP_YX_PURINVOICEPASSBACK t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.SAP_YX_PURINVOICEPASSBACK t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr08 := SqlStr08 || ' union all SELECT ''ESB-YX 采购发票信息回传'' as 接口,''sddcs.SAP_YX_PURINVOICEPASSBACK'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.SAP_YX_PURINVOICEPASSBACK t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.SAP_YX_PURINVOICEPASSBACK t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr08 := SqlStr08 || ' union all SELECT ''ESB-YX 采购发票信息回传'' as 接口,''dcs.SAP_YX_PURINVOICEPASSBACK'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM dcs.SAP_YX_PURINVOICEPASSBACK t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.SAP_YX_PURINVOICEPASSBACK t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr08 := SqlStr08 || ' union all SELECT ''ESB-YX 采购发票信息回传'' as 接口,''gcdcs.SAP_YX_PURINVOICEPASSBACK'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.SAP_YX_PURINVOICEPASSBACK t
   WHERE HandleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*ESB-YX 采购退货，采购退货开红票*/
  SELECT count(1)
    into iCount
    FROM yxdcs.SAP_YX_INVOICEVERACCOUNTINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr08 := SqlStr08 || ' union all SELECT ''ESB-YX 采购退货开红票'' as 接口,''yxdcs.SAP_YX_INVOICEVERACCOUNTINFO'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.SAP_YX_INVOICEVERACCOUNTINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.SAP_YX_INVOICEVERACCOUNTINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr09 := SqlStr09 || ' union all SELECT ''ESB-YX 采购退货开红票'' as 接口,''sddcs.SAP_YX_INVOICEVERACCOUNTINFO'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.SAP_YX_INVOICEVERACCOUNTINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.SAP_YX_INVOICEVERACCOUNTINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr09 := SqlStr09 || ' union all SELECT ''ESB-YX 采购退货开红票'' as 接口,''dcs.SAP_YX_INVOICEVERACCOUNTINFO'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM dcs.SAP_YX_INVOICEVERACCOUNTINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.SAP_YX_INVOICEVERACCOUNTINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr09 := SqlStr09 || ' union all SELECT ''ESB-YX 采购退货开红票'' as 接口,''gcdcs.SAP_YX_INVOICEVERACCOUNTINFO'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.SAP_YX_INVOICEVERACCOUNTINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*ESB-YX 销售结算（1对1），销售退货结算，销售退货开红票*/
  SELECT count(1)
    into iCount
    FROM yxdcs.SAP_YX_SALESBILLINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr09 := SqlStr09 || ' union all SELECT ''ESB-YX 销售退货开红票'' as 接口,''yxdcs.SAP_YX_SALESBILLINFO'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.SAP_YX_SALESBILLINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.SAP_YX_SALESBILLINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr09 := SqlStr09 || ' union all SELECT ''ESB-YX 销售退货开红票'' as 接口,''sddcs.SAP_YX_SALESBILLINFO'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.SAP_YX_SALESBILLINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.SAP_YX_SALESBILLINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr09 := SqlStr09 || ' union all SELECT ''ESB-YX 销售退货开红票'' as 接口,''dcs.SAP_YX_SALESBILLINFO'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM dcs.SAP_YX_SALESBILLINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.SAP_YX_SALESBILLINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr09 := SqlStr09 || ' union all SELECT ''ESB-YX 销售退货开红票'' as 接口,''gcdcs.SAP_YX_SALESBILLINFO'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.SAP_YX_SALESBILLINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*ESB-YX 电商 销售结算（1对1），销售退货结算，销售退货开红票*/
  SELECT count(1)
    into iCount
    FROM yxdcs.SAP_YX_RetailerSALESBILLINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr10 := SqlStr10 || ' union all SELECT ''ESB-YX 电商销售退货开红票'' as 接口,''yxdcs.SAP_YX_RetailerSALESBILLINFO'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.SAP_YX_RetailerSALESBILLINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*SELECT count(1)
    into iCount
    FROM sddcs.SAP_YX_RetailerSALESBILLINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1);
  end if;*/

  SELECT count(1)
    into iCount
    FROM dcs.SAP_YX_RetailerSALESBILLINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr10 := SqlStr10 || ' union all SELECT ''ESB-YX 电商销售退货开红票'' as 接口,''dcs.SAP_YX_RetailerSALESBILLINFO'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM dcs.SAP_YX_RetailerSALESBILLINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*SELECT count(1)
    into iCount
    FROM gcdcs.SAP_YX_RetailerSALESBILLINFO t
   WHERE Status = 3
     and t.modifytime >= trunc(sysdate - 1);
  end if;*/

  /*ESB-YX 库存调整单   WMS的接口 对应 WMS库存调整单*/
  /*SELECT count(1)
    into iCount
    FROM yxdcs.T_Inventory_Adjustment t
   WHERE isfinished = 2;
  
  SELECT count(1)
    into iCount
    FROM sddcs.T_Inventory_Adjustment t
   WHERE isfinished = 2;
  
  
  SELECT count(1)
    into iCount
    FROM dcs.T_Inventory_Adjustment t
   WHERE isfinished = 2;
  
  SELECT count(1)
    into iCount
    FROM gcdcs.T_Inventory_Adjustment t
   WHERE isfinished = 2;*/

  /*IMS 发运单接口*/
  SELECT count(1)
    into iCount
    FROM yxdcs.IMSShippingLoginInfo t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr10 := SqlStr10 || ' union all SELECT ''IMS 发运单接口'' as 接口,''yxdcs.IMSShippingLoginInfo'' as 表名,
    to_char(t.errormessage) as 报错信息,t.synctime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.IMSShippingLoginInfo t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.IMSShippingLoginInfo t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr10 := SqlStr10 || ' union all SELECT ''IMS 发运单接口'' as 接口,''sddcs.IMSShippingLoginInfo'' as 表名,
    to_char(t.errormessage) as 报错信息,t.synctime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.IMSShippingLoginInfo t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.IMSShippingLoginInfo t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr10 := SqlStr10 || ' union all SELECT ''IMS 发运单接口'' as 接口,''dcs.IMSShippingLoginInfo'' as 表名,
    to_char(t.errormessage) as 报错信息,t.synctime as 出错时间,t.id as 唯一标识ID
    FROM dcs.IMSShippingLoginInfo t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.IMSShippingLoginInfo t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr10 := SqlStr10 || ' union all SELECT ''IMS 发运单接口'' as 接口,''gcdcs.IMSShippingLoginInfo'' as 表名,
    to_char(t.errormessage) as 报错信息,t.synctime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.IMSShippingLoginInfo t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1)';
  end if;

  /*Qtsinfo sep_qtp_QTSINFO*/
  SELECT count(1)
    into iCount
    FROM yxdcs.sep_qtp_QTSINFO t
   WHERE handleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr10 := SqlStr10 || ' union all SELECT ''Qtsinfo sep_qtp_QTSINFO'' as 接口,''yxdcs.sep_qtp_QTSINFO'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.sep_qtp_QTSINFO t
   WHERE handleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.sep_qtp_QTSINFO t
   WHERE handleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr11 := SqlStr11 || ' union all SELECT ''Qtsinfo sep_qtp_QTSINFO'' as 接口,''sddcs.sep_qtp_QTSINFO'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.sep_qtp_QTSINFO t
   WHERE handleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.sep_qtp_QTSINFO t
   WHERE handleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr11 := SqlStr11 || ' union all SELECT ''Qtsinfo sep_qtp_QTSINFO'' as 接口,''dcs.sep_qtp_QTSINFO'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM dcs.sep_qtp_QTSINFO t
   WHERE handleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.sep_qtp_QTSINFO t
   WHERE handleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr11 := SqlStr11 || ' union all SELECT ''Qtsinfo sep_qtp_QTSINFO'' as 接口,''gcdcs.sep_qtp_QTSINFO'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.sep_qtp_QTSINFO t
   WHERE handleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*Qtsinfo qtp_QTSINFO*/

  /*SELECT count(1)
   into iCount
   FROM yxdcs.qtp_QTSINFO t
  WHERE handleStatus = 3
    and t.handletime >= trunc(sysdate - 1);*/

  /*SELECT count(1)
   into iCount
   FROM sddcs.qtp_QTSINFO t
  WHERE handleStatus = 3
    and t.handletime >= trunc(sysdate - 1);*/

  SELECT count(1)
    into iCount
    FROM dcs.qtp_QTSINFO t
   WHERE handleStatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr11 := SqlStr11 || ' union all SELECT ''Qtsinfo qtp_QTSINFO'' as 接口,''dcs.qtp_QTSINFO'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM dcs.qtp_QTSINFO t
   WHERE handleStatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*SELECT count(1)
   into iCount
   FROM gcdcs.qtp_QTSINFO t
  WHERE handleStatus = 3
    and t.handletime >= trunc(sysdate - 1);*/

  /*RETAILER 电商 零售商-交付*/
  SELECT count(1)
    into iCount
    FROM yxdcs.Retailer_DeliveryDetail t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr11 := SqlStr11 || ' union all SELECT ''RETAILER 电商 零售商-交付'' as 接口,''yxdcs.Retailer_DeliveryDetail'' as 表名,
    to_char(t.message) as 报错信息,t.synctime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.Retailer_DeliveryDetail t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.Retailer_DeliveryDetail t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr11 := SqlStr11 || ' union all SELECT ''RETAILER 电商 零售商-交付'' as 接口,''sddcs.Retailer_DeliveryDetail'' as 表名,
    to_char(t.message) as 报错信息,t.synctime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.Retailer_DeliveryDetail t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.Retailer_DeliveryDetail t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr11 := SqlStr11 || ' union all SELECT ''RETAILER 电商 零售商-交付'' as 接口,''dcs.Retailer_DeliveryDetail'' as 表名,
    to_char(t.message) as 报错信息,t.synctime as 出错时间,t.id as 唯一标识ID
    FROM dcs.Retailer_DeliveryDetail t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.Retailer_DeliveryDetail t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr12 := SqlStr12 || ' union all SELECT ''RETAILER 电商 零售商-交付'' as 接口,''gcdcs.Retailer_DeliveryDetail'' as 表名,
    to_char(t.message) as 报错信息,t.synctime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.Retailer_DeliveryDetail t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1)';
  end if;

  /*SAP-FD 采购结算发票*/

  /*SELECT count(1)
    into iCount
    FROM yxdcs.sap_fd_invoiceveraccountinfo t
   WHERE status = 3
     and t.modifytime >= trunc(sysdate - 1);
     
  SELECT count(1)
    into iCount
    FROM sddcs.sap_fd_invoiceveraccountinfo t
   WHERE status = 3
     and t.modifytime >= trunc(sysdate - 1); */

  SELECT count(1)
    into iCount
    FROM dcs.sap_fd_invoiceveraccountinfo t
   WHERE status = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr12 := SqlStr12 || ' union all SELECT ''SAP-FD 采购结算发票'' as 接口,''dcs.sap_fd_invoiceveraccountinfo'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM dcs.sap_fd_invoiceveraccountinfo t
   WHERE status = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*SELECT count(1)
   into iCount
   FROM gcdcs.sap_fd_invoiceveraccountinfo t
  WHERE status = 3
    and t.modifytime >= trunc(sysdate - 1);*/

  /*SAP-FD 采购退货开蓝票*/

  /*SELECT count(1)
    into iCount
    FROM yxdcs.SAP_FD_PurchaseRtnInvoice t
   WHERE status = 3
     and t.modifytime >= trunc(sysdate - 1);
     
  SELECT count(1)
    into iCount
    FROM sddcs.SAP_FD_PurchaseRtnInvoice t
   WHERE status = 3
     and t.modifytime >= trunc(sysdate - 1); */

  SELECT count(1)
    into iCount
    FROM dcs.SAP_FD_PurchaseRtnInvoice t
   WHERE status = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr12 := SqlStr12 || ' union all SELECT ''SAP-FD 采购退货开蓝票'' as 接口,''dcs.SAP_FD_PurchaseRtnInvoice'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM dcs.SAP_FD_PurchaseRtnInvoice t
   WHERE status = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*SELECT count(1)
   into iCount
   FROM gcdcs.SAP_FD_PurchaseRtnInvoice t
  WHERE status = 3
    and t.modifytime >= trunc(sysdate - 1);*/

  /*SAP-FD 销售发票及销售退货发票*/

  /*SELECT count(1)
    into iCount
    FROM yxdcs.sap_fd_salesbillinfo t
   WHERE status = 3
     and t.modifytime >= trunc(sysdate - 1);
     
  SELECT count(1)
    into iCount
    FROM sddcs.sap_fd_salesbillinfo t
   WHERE status = 3
     and t.modifytime >= trunc(sysdate - 1); */

  SELECT count(1)
    into iCount
    FROM dcs.sap_fd_salesbillinfo t
   WHERE status = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr12 := SqlStr12 || ' union all SELECT ''SAP-FD 销售发票及销售退货发票'' as 接口,''dcs.sap_fd_salesbillinfo'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM dcs.sap_fd_salesbillinfo t
   WHERE status = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*SELECT count(1)
   into iCount
   FROM gcdcs.sap_fd_salesbillinfo t
  WHERE status = 3
    and t.modifytime >= trunc(sysdate - 1);*/

  /*Siebel 呼叫中心 回访问卷*/
  SELECT count(1)
    into iCount
    FROM yxdcs.NccQuestsessionId t
   WHERE handlestatus = 3
     and t.sendtime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr12 := SqlStr12 || ' union all SELECT ''Siebel 呼叫中心 回访问卷'' as 接口,''yxdcs.NccQuestsessionId'' as 表名,
    to_char(t.message) as 报错信息,t.sendtime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.NccQuestsessionId t
   WHERE handlestatus = 3
     and t.sendtime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.NccQuestsessionId t
   WHERE handlestatus = 3
     and t.sendtime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr12 := SqlStr12 || ' union all SELECT ''Siebel 呼叫中心 回访问卷'' as 接口,''sddcs.NccQuestsessionId'' as 表名,
    to_char(t.message) as 报错信息,t.sendtime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.NccQuestsessionId t
   WHERE handlestatus = 3
     and t.sendtime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.NccQuestsessionId t
   WHERE handlestatus = 3
     and t.sendtime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr12 := SqlStr12 || ' union all SELECT ''Siebel 呼叫中心 回访问卷'' as 接口,''dcs.NccQuestsessionId'' as 表名,
    to_char(t.message) as 报错信息,t.sendtime as 出错时间,t.id as 唯一标识ID
    FROM dcs.NccQuestsessionId t
   WHERE handlestatus = 3
     and t.sendtime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.NccQuestsessionId t
   WHERE handlestatus = 3
     and t.sendtime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr13 := SqlStr13 || ' union all SELECT ''Siebel 呼叫中心 回访问卷'' as 接口,''gcdcs.NccQuestsessionId'' as 表名,
    to_char(t.message) as 报错信息,t.sendtime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.NccQuestsessionId t
   WHERE handlestatus = 3
     and t.sendtime >= trunc(sysdate - 1)';
  end if;

  /*Siebel 呼叫中心 维修工单全媒体*/
  SELECT count(1)
    into iCount
    FROM yxdcs.NccRepairWorkOrderMd t
   WHERE handlestatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr13 := SqlStr13 || ' union all SELECT ''Siebel 呼叫中心 维修工单全媒体'' as 接口,''yxdcs.NccRepairWorkOrderMd'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.NccRepairWorkOrderMd t
   WHERE handlestatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*SELECT count(1)
   into iCount
   FROM sddcs.NccRepairWorkOrderMd t
  WHERE handlestatus = 3
    and t.modifytime >= trunc(sysdate - 1);*/

  SELECT count(1)
    into iCount
    FROM dcs.NccRepairWorkOrderMd t
   WHERE handlestatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr13 := SqlStr13 || ' union all SELECT ''Siebel 呼叫中心 维修工单全媒体'' as 接口,''dcs.NccRepairWorkOrderMd'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM dcs.NccRepairWorkOrderMd t
   WHERE handlestatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.NccRepairWorkOrderMd t
   WHERE handlestatus = 3
     and t.modifytime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr13 := SqlStr13 || ' union all SELECT ''Siebel 呼叫中心 维修工单全媒体'' as 接口,''gcdcs.NccRepairWorkOrderMd'' as 表名,
    to_char(t.message) as 报错信息,t.modifytime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.NccRepairWorkOrderMd t
   WHERE handlestatus = 3
     and t.modifytime >= trunc(sysdate - 1)';
  end if;

  /*Stibo MDM Cs_Stibo*/

  /*SELECT count(1)
    into iCount
    FROM yxdcs.Cs_Stibo t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);
  
  SELECT count(1)
    into iCount
    FROM sddcs.Cs_Stibo t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);*/

  SELECT count(1)
    into iCount
    FROM dcs.Cs_Stibo t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr13 := SqlStr13 || ' union all SELECT ''Stibo MDM Cs_Stibo'' as 接口,''dcs.Cs_Stibo'' as 表名,
    to_char(t.message) as 报错信息,t.synctime as 出错时间,t.id as 唯一标识ID
    FROM dcs.Cs_Stibo t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1)';
  end if;

  /*SELECT count(1)
   into iCount
   FROM gcdcs.Cs_Stibo t
  WHERE syncstatus = 3
    and t.synctime >= trunc(sysdate - 1);*/

  /*STMS SEP_STMS_MALCATEGORY*/
  SELECT count(1)
    into iCount
    FROM yxdcs.SEP_STMS_MALCATEGORY t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr13 := SqlStr13 || ' union all SELECT ''STMS SEP_STMS_MALCATEGORY'' as 接口,''yxdcs.SEP_STMS_MALCATEGORY'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.SEP_STMS_MALCATEGORY t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.SEP_STMS_MALCATEGORY t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr13 := SqlStr13 || ' union all SELECT ''STMS SEP_STMS_MALCATEGORY'' as 接口,''sddcs.SEP_STMS_MALCATEGORY'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.SEP_STMS_MALCATEGORY t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.SEP_STMS_MALCATEGORY t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr14 := SqlStr14 || ' union all SELECT ''STMS SEP_STMS_MALCATEGORY'' as 接口,''dcs.SEP_STMS_MALCATEGORY'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM dcs.SEP_STMS_MALCATEGORY t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.SEP_STMS_MALCATEGORY t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr14 := SqlStr14 || ' union all SELECT ''STMS SEP_STMS_MALCATEGORY'' as 接口,''gcdcs.SEP_STMS_MALCATEGORY'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.SEP_STMS_MALCATEGORY t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*STMS SEP_STMS_MALFUNCTION*/
  SELECT count(1)
    into iCount
    FROM yxdcs.SEP_STMS_MALFUNCTION t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr14 := SqlStr14 || ' union all SELECT ''STMS SEP_STMS_MALFUNCTION'' as 接口,''yxdcs.SEP_STMS_MALFUNCTION'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.SEP_STMS_MALFUNCTION t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.SEP_STMS_MALFUNCTION t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr14 := SqlStr14 || ' union all SELECT ''STMS SEP_STMS_MALFUNCTION'' as 接口,''sddcs.SEP_STMS_MALFUNCTION'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.SEP_STMS_MALFUNCTION t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.SEP_STMS_MALFUNCTION t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr14 := SqlStr14 || ' union all SELECT ''STMS SEP_STMS_MALFUNCTION'' as 接口,''dcs.SEP_STMS_MALFUNCTION'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM dcs.SEP_STMS_MALFUNCTION t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.SEP_STMS_MALFUNCTION t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr14 := SqlStr14 || ' union all SELECT ''STMS SEP_STMS_MALFUNCTION'' as 接口,''gcdcs.SEP_STMS_MALFUNCTION'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.SEP_STMS_MALFUNCTION t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*STMS SEP_STMS_REPITEM*/
  SELECT count(1)
    into iCount
    FROM yxdcs.SEP_STMS_REPITEM t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr14 := SqlStr14 || ' union all SELECT ''STMS SEP_STMS_REPITEM'' as 接口,''yxdcs.SEP_STMS_REPITEM'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.SEP_STMS_REPITEM t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.SEP_STMS_REPITEM t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr15 := SqlStr15 || ' union all SELECT ''STMS SEP_STMS_REPITEM'' as 接口,''sddcs.SEP_STMS_REPITEM'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.SEP_STMS_REPITEM t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.SEP_STMS_REPITEM t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr15 := SqlStr15 || ' union all SELECT ''STMS SEP_STMS_REPITEM'' as 接口,''dcs.SEP_STMS_REPITEM'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM dcs.SEP_STMS_REPITEM t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.SEP_STMS_REPITEM t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr15 := SqlStr15 || ' union all SELECT ''STMS SEP_STMS_REPITEM'' as 接口,''gcdcs.SEP_STMS_REPITEM'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.SEP_STMS_REPITEM t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*STMS SEP_STMS_MALBRANDREL*/
  SELECT count(1)
    into iCount
    FROM yxdcs.SEP_STMS_MALBRANDREL t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr15 := SqlStr15 || ' union all SELECT ''STMS SEP_STMS_MALBRANDREL'' as 接口,''yxdcs.SEP_STMS_MALBRANDREL'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.SEP_STMS_MALBRANDREL t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.SEP_STMS_MALBRANDREL t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr15 := SqlStr15 || ' union all SELECT ''STMS SEP_STMS_MALBRANDREL'' as 接口,''sddcs.SEP_STMS_MALBRANDREL'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.SEP_STMS_MALBRANDREL t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.SEP_STMS_MALBRANDREL t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr15 := SqlStr15 || ' union all SELECT ''STMS SEP_STMS_MALBRANDREL'' as 接口,''dcs.SEP_STMS_MALBRANDREL'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM dcs.SEP_STMS_MALBRANDREL t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.SEP_STMS_MALBRANDREL t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr15 := SqlStr15 || ' union all SELECT ''STMS SEP_STMS_MALBRANDREL'' as 接口,''gcdcs.SEP_STMS_MALBRANDREL'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.SEP_STMS_MALBRANDREL t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*STMS SEP_STMS_CARREPITEMPRODLINE*/
  SELECT count(1)
    into iCount
    FROM yxdcs.SEP_STMS_CARREPITEMPRODLINE t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr16 := SqlStr16 || ' union all SELECT ''STMS SEP_STMS_CARREPITEMPRODLINE'' as 接口,''yxdcs.SEP_STMS_CARREPITEMPRODLINE'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.SEP_STMS_CARREPITEMPRODLINE t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.SEP_STMS_CARREPITEMPRODLINE t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr16 := SqlStr16 || ' union all SELECT ''STMS SEP_STMS_CARREPITEMPRODLINE'' as 接口,''sddcs.SEP_STMS_CARREPITEMPRODLINE'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.SEP_STMS_CARREPITEMPRODLINE t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.SEP_STMS_CARREPITEMPRODLINE t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr16 := SqlStr16 || ' union all SELECT ''STMS SEP_STMS_CARREPITEMPRODLINE'' as 接口,''dcs.SEP_STMS_CARREPITEMPRODLINE'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM dcs.SEP_STMS_CARREPITEMPRODLINE t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.SEP_STMS_CARREPITEMPRODLINE t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr16 := SqlStr16 || ' union all SELECT ''STMS SEP_STMS_CARREPITEMPRODLINE'' as 接口,''gcdcs.SEP_STMS_CARREPITEMPRODLINE'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.SEP_STMS_CARREPITEMPRODLINE t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*STMS SEP_STMS_ENGINEREPITEMPRODLINE*/
  SELECT count(1)
    into iCount
    FROM yxdcs.SEP_STMS_ENGINEREPITEMPRODLINE t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr16 := SqlStr16 || ' union all SELECT ''STMS SEP_STMS_ENGINEREPITEMPRODLINE'' as 接口,''yxdcs.SEP_STMS_ENGINEREPITEMPRODLINE'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.SEP_STMS_ENGINEREPITEMPRODLINE t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.SEP_STMS_ENGINEREPITEMPRODLINE t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr16 := SqlStr16 || ' union all SELECT ''STMS SEP_STMS_ENGINEREPITEMPRODLINE'' as 接口,''sddcs.SEP_STMS_ENGINEREPITEMPRODLINE'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.SEP_STMS_ENGINEREPITEMPRODLINE t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.SEP_STMS_ENGINEREPITEMPRODLINE t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr16 := SqlStr16 || ' union all SELECT ''STMS SEP_STMS_ENGINEREPITEMPRODLINE'' as 接口,''dcs.SEP_STMS_ENGINEREPITEMPRODLINE'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM dcs.SEP_STMS_ENGINEREPITEMPRODLINE t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM gcdcs.SEP_STMS_ENGINEREPITEMPRODLINE t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr17 := SqlStr17 || ' union all SELECT ''STMS SEP_STMS_ENGINEREPITEMPRODLINE'' as 接口,''gcdcs.SEP_STMS_ENGINEREPITEMPRODLINE'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.SEP_STMS_ENGINEREPITEMPRODLINE t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*VIP 红包信息*/
  SELECT count(1)
    into iCount
    FROM yxdcs.Retailer_LuckyMoney t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr17 := SqlStr17 || ' union all SELECT ''VIP 红包信息'' as 接口,''yxdcs.Retailer_LuckyMoney'' as 表名,
    to_char(t.message) as 报错信息,t.synctime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.Retailer_LuckyMoney t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.Retailer_LuckyMoney t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr17 := SqlStr17 || ' union all SELECT ''VIP 红包信息'' as 接口,''sddcs.Retailer_LuckyMoney'' as 表名,
    to_char(t.message) as 报错信息,t.synctime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.Retailer_LuckyMoney t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM dcs.Retailer_LuckyMoney t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr17 := SqlStr17 || ' union all SELECT ''VIP 红包信息'' as 接口,''dcs.Retailer_LuckyMoney'' as 表名,
    to_char(t.message) as 报错信息,t.synctime as 出错时间,t.id as 唯一标识ID
    FROM dcs.Retailer_LuckyMoney t
   WHERE syncstatus = 3
     and t.synctime >= trunc(sysdate - 1)';
  end if;

  /*SELECT count(1)
   into iCount
   FROM gcdcs.Retailer_LuckyMoney t
  WHERE syncstatus = 3
    and t.synctime >= trunc(sysdate - 1);*/

  /*VPC 维修单完毕信息*/
  SELECT count(1)
    into iCount
    FROM yxdcs.RepairWorkOrderFinishInfo t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr17 := SqlStr17 || ' union all SELECT ''VPC 维修单完毕信息'' as 接口,''yxdcs.RepairWorkOrderFinishInfo'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.RepairWorkOrderFinishInfo t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.RepairWorkOrderFinishInfo t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr17 := SqlStr17 || ' union all SELECT ''VPC 维修单完毕信息'' as 接口,''sddcs.RepairWorkOrderFinishInfo'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.RepairWorkOrderFinishInfo t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*SELECT count(1)
   into iCount
   FROM dcs.RepairWorkOrderFinishInfo t
  WHERE handlestatus = 3
    and t.handletime >= trunc(sysdate - 1);*/

  SELECT count(1)
    into iCount
    FROM gcdcs.RepairWorkOrderFinishInfo t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr17 := SqlStr17 || ' union all SELECT ''VPC 维修单完毕信息'' as 接口,''gcdcs.RepairWorkOrderFinishInfo'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.RepairWorkOrderFinishInfo t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*VPC 维修确定信息*/
  SELECT count(1)
    into iCount
    FROM yxdcs.RepairWorkOrderConfirmInfo t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr18 := SqlStr18 || ' union all SELECT ''VPC 维修确定信息'' as 接口,''yxdcs.RepairWorkOrderConfirmInfo'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM yxdcs.RepairWorkOrderConfirmInfo t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  SELECT count(1)
    into iCount
    FROM sddcs.RepairWorkOrderConfirmInfo t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr18 := SqlStr18 || ' union all SELECT ''VPC 维修确定信息'' as 接口,''sddcs.RepairWorkOrderConfirmInfo'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM sddcs.RepairWorkOrderConfirmInfo t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*SELECT count(1)
   into iCount
   FROM dcs.RepairWorkOrderConfirmInfo t
  WHERE handlestatus = 3
    and t.handletime >= trunc(sysdate - 1);*/

  SELECT count(1)
    into iCount
    FROM gcdcs.RepairWorkOrderConfirmInfo t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1);
  if iCount <> 0 then
    SqlStr18 := SqlStr18 || ' union all SELECT ''VPC 维修确定信息'' as 接口,''gcdcs.RepairWorkOrderConfirmInfo'' as 表名,
    to_char(t.handlemessage) as 报错信息,t.handletime as 出错时间,t.id as 唯一标识ID
    FROM gcdcs.RepairWorkOrderConfirmInfo t
   WHERE handlestatus = 3
     and t.handletime >= trunc(sysdate - 1)';
  end if;

  /*WMS 入库单*/
  SELECT count(1)
    into iCount
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
     and t.synctype = 1
     and t.status = 0;
  if iCount <> 0 then
    SqlStr18 := SqlStr18 || ' union all SELECT ''WMS 入库单'' as 接口,''yxdcs.Syncwmsinoutloginfo'' as 表名,
    to_char(t.code) as 报错信息,t.syncdate as 出错时间,t.objid as 唯一标识ID
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
   and t.synctype = 1
   and t.status = 0';
  end if;

  SELECT count(1)
    into iCount
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
     and t.synctype = 1
     and t.status = 0;
  if iCount <> 0 then
    SqlStr18 := SqlStr18 || ' union all SELECT ''WMS 入库单'' as 接口,''sddcs.Syncwmsinoutloginfo'' as 表名,
    to_char(t.code) as 报错信息,t.syncdate as 出错时间,t.objid as 唯一标识ID
    FROM sddcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
   and t.synctype = 1
   and t.status = 0';
  end if;

  SELECT count(1)
    into iCount
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
     and t.synctype = 1
     and t.status = 0;
  if iCount <> 0 then
    SqlStr18 := SqlStr18 || ' union all SELECT ''WMS 入库单'' as 接口,''dcs.Syncwmsinoutloginfo'' as 表名,
    to_char(t.code) as 报错信息,t.syncdate as 出错时间,t.objid as 唯一标识ID
    FROM dcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
   and t.synctype = 1
   and t.status = 0';
  end if;

  /*WMS 发运签收单*/
  SELECT count(1)
    into iCount
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
     and t.synctype = 2
     and t.status = 0;
  if iCount <> 0 then
    SqlStr18 := SqlStr18 || ' union all SELECT ''WMS 发运签收单'' as 接口,''yxdcs.Syncwmsinoutloginfo'' as 表名,
    to_char(t.code) as 报错信息,t.syncdate as 出错时间,t.objid as 唯一标识ID
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
   and t.synctype = 2
   and t.status = 0';
  end if;

  SELECT count(1)
    into iCount
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
     and t.synctype = 2
     and t.status = 0;
  if iCount <> 0 then
    SqlStr19 := SqlStr19 || ' union all SELECT ''WMS 发运签收单'' as 接口,''sddcs.Syncwmsinoutloginfo'' as 表名,
    to_char(t.code) as 报错信息,t.syncdate as 出错时间,t.objid as 唯一标识ID
    FROM sddcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
   and t.synctype = 2
   and t.status = 0';
  end if;

  SELECT count(1)
    into iCount
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
     and t.synctype = 2
     and t.status = 0;
  if iCount <> 0 then
    SqlStr19 := SqlStr19 || ' union all SELECT ''WMS 发运签收单'' as 接口,''dcs.Syncwmsinoutloginfo'' as 表名,
    to_char(t.code) as 报错信息,t.syncdate as 出错时间,t.objid as 唯一标识ID
    FROM dcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
   and t.synctype = 2
   and t.status = 0';
  end if;

  /*WMS 库存调整单*/
  SELECT count(1)
    into iCount
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
     and t.synctype = 3
     and t.status = 0;
  if iCount <> 0 then
    SqlStr19 := SqlStr19 || ' union all SELECT ''WMS 库存调整单'' as 接口,''yxdcs.Syncwmsinoutloginfo'' as 表名,
    to_char(t.code) as 报错信息,t.syncdate as 出错时间,t.objid as 唯一标识ID
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
   and t.synctype = 3
   and t.status = 0';
  end if;

  SELECT count(1)
    into iCount
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
     and t.synctype = 3
     and t.status = 0;
  if iCount <> 0 then
    SqlStr19 := SqlStr19 || ' union all SELECT ''WMS 库存调整单'' as 接口,''sddcs.Syncwmsinoutloginfo'' as 表名,
    to_char(t.code) as 报错信息,t.syncdate as 出错时间,t.objid as 唯一标识ID
    FROM sddcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
   and t.synctype = 3
   and t.status = 0';
  end if;

  SELECT count(1)
    into iCount
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
     and t.synctype = 3
     and t.status = 0;
  if iCount <> 0 then
    SqlStr19 := SqlStr19 || ' union all SELECT ''WMS 库存调整单'' as 接口,''dcs.Syncwmsinoutloginfo'' as 表名,
    to_char(t.code) as 报错信息,t.syncdate as 出错时间,t.objid as 唯一标识ID
    FROM dcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
   and t.synctype = 3
   and t.status = 0';
  end if;

  /*WMS 仓库表信息*/
  SELECT count(1)
    into iCount
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
     and t.synctype = 4
     and t.status = 0;
  if iCount <> 0 then
    SqlStr19 := SqlStr19 || ' union all SELECT ''WMS 仓库表信息'' as 接口,''yxdcs.Syncwmsinoutloginfo'' as 表名,
    to_char(t.code) as 报错信息,t.syncdate as 出错时间,t.objid as 唯一标识ID
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
   and t.synctype = 4
   and t.status = 0';
  end if;

  SELECT count(1)
    into iCount
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
     and t.synctype = 4
     and t.status = 0;
  if iCount <> 0 then
    SqlStr19 := SqlStr19 || ' union all SELECT ''WMS 仓库表信息'' as 接口,''sddcs.Syncwmsinoutloginfo'' as 表名,
    to_char(t.code) as 报错信息,t.syncdate as 出错时间,t.objid as 唯一标识ID
    FROM sddcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
   and t.synctype = 4
   and t.status = 0';
  end if;

  SELECT count(1)
    into iCount
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
     and t.synctype = 4
     and t.status = 0;
  if iCount <> 0 then
    SqlStr20 := SqlStr20 || ' union all SELECT ''WMS 仓库表信息'' as 接口,''dcs.Syncwmsinoutloginfo'' as 表名,
    to_char(t.code) as 报错信息,t.syncdate as 出错时间,t.objid as 唯一标识ID
    FROM dcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
   and t.synctype = 4
   and t.status = 0';
  end if;

  /*WMS 上架单*/
  SELECT count(1)
    into iCount
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
     and t.synctype = 5
     and t.status = 0;
  if iCount <> 0 then
    SqlStr20 := SqlStr20 || ' union all SELECT ''WMS 上架单'' as 接口,''yxdcs.Syncwmsinoutloginfo'' as 表名,
    to_char(t.code) as 报错信息,t.syncdate as 出错时间,t.objid as 唯一标识ID
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
   and t.synctype = 5
   and t.status = 0';
  end if;

  SELECT count(1)
    into iCount
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
     and t.synctype = 5
     and t.status = 0;
  if iCount <> 0 then
    SqlStr20 := SqlStr20 || ' union all SELECT ''WMS 上架单'' as 接口,''sddcs.Syncwmsinoutloginfo'' as 表名,
    to_char(t.code) as 报错信息,t.syncdate as 出错时间,t.objid as 唯一标识ID
    FROM sddcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
   and t.synctype = 5
   and t.status = 0';
  end if;

  SELECT count(1)
    into iCount
    FROM yxdcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
     and t.synctype = 5
     and t.status = 0;
  if iCount <> 0 then
    SqlStr20 := SqlStr20 || ' union all SELECT ''WMS 上架单'' as 接口,''dcs.Syncwmsinoutloginfo'' as 表名,
    to_char(t.code) as 报错信息,t.syncdate as 出错时间,t.objid as 唯一标识ID
    FROM dcs.Syncwmsinoutloginfo t
   WHERE t.syncdate >= trunc(sysdate - 1)
   and t.synctype = 5
   and t.status = 0';
  end if;

  /*输出异常总数*/
  DBMS_OUTPUT.PUT_LINE(SqlStr01);
  DBMS_OUTPUT.PUT_LINE(SqlStr02);
  DBMS_OUTPUT.PUT_LINE(SqlStr03);
  DBMS_OUTPUT.PUT_LINE(SqlStr04);
  DBMS_OUTPUT.PUT_LINE(SqlStr05);
  DBMS_OUTPUT.PUT_LINE(SqlStr06);
  DBMS_OUTPUT.PUT_LINE(SqlStr07);
  DBMS_OUTPUT.PUT_LINE(SqlStr08);
  DBMS_OUTPUT.PUT_LINE(SqlStr09);
  DBMS_OUTPUT.PUT_LINE(SqlStr10);
  DBMS_OUTPUT.PUT_LINE(SqlStr11);
  DBMS_OUTPUT.PUT_LINE(SqlStr12);
  DBMS_OUTPUT.PUT_LINE(SqlStr13);
  DBMS_OUTPUT.PUT_LINE(SqlStr14);
  DBMS_OUTPUT.PUT_LINE(SqlStr15);
  DBMS_OUTPUT.PUT_LINE(SqlStr16);
  DBMS_OUTPUT.PUT_LINE(SqlStr17);
  DBMS_OUTPUT.PUT_LINE(SqlStr18);
  DBMS_OUTPUT.PUT_LINE(SqlStr19);
  DBMS_OUTPUT.PUT_LINE(SqlStr20);
end;
