--���ÿ��=�ֿ��� - �ֿ��������-�ֿⶳ����-�ֿⲻ���ÿ��
create or replace view AllValidStock as
       select StorageCompanyId as BranchId,PartsSalesCategoryId,PartId ,sum(nums) as nums from 
          (select PartsStock.StorageCompanyId,
                 SalesUnit.Partssalescategoryid,
                 PartsStock.PartId,
                 sum(PartsStock.Quantity) as nums
            from PartsStock
           inner join SalesUnitAffiWarehouse --������֯�ֿ��ϵ
              on PartsStock.Warehouseid = SalesUnitAffiWarehouse.Warehouseid
           inner join SalesUnit
              on SalesUnit.Id = SalesUnitAffiWarehouse.Salesunitid and SalesUnit.Branchid=PartsStock.Branchid
           inner join WarehouseAreaCategory --������;
              on WarehouseAreaCategory.id = PartsStock.WarehouseAreaCategoryId
           where WarehouseAreaCategory.Category in (1, 3) --��������������
           group by PartsStock.StorageCompanyId,
                    PartsStock.PartId,
                    SalesUnit.Partssalescategoryid
          union all
          select PartsLockedStock.StorageCompanyId,
                 SalesUnit.PartsSalesCategoryid,
                 PartsLockedStock.PartId,
                 -nvl(PartsLockedStock.LockedQuantity, 0) as nums
            from PartsLockedStock --����������
           inner join SalesUnitAffiWarehouse
              on PartsLockedStock.Warehouseid =
                 SalesUnitAffiWarehouse.Warehouseid
           inner join SalesUnit
              on SalesUnit.Id = SalesUnitAffiWarehouse.Salesunitid 
              and  SalesUnit.Branchid=PartsLockedStock.Branchid
          union all
          select StorageCompanyId,
                 PartsSalesCategoryId,
                 SparePartId as PartId,
                 -nvl(CongelationStockQty, 0) as nums
            from WmsCongelationStockView --WMS�ֿ���������棨��ͼ��
          union all
          select StorageCompanyId,
                 PartsSalesCategoryId,
                 SparePartId as ,
                 -nvl(DisabledStock, 0) as nums
            from WmsCongelationStockView
       ) group by StorageCompanyId,Partssalescategoryid,PartId;


