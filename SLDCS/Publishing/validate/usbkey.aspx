﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="usbkey.aspx.cs" Inherits="Sunlight.Silverlight.Web.usbkey" %>

<%
    String path = System.Web.HttpContext.Current.Request.MapPath("/");
%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script>
    function downTool() {
        var elemIF = document.createElement("iframe");
        elemIF.src = "Client/三未信安USBKey管理工具v3.1.exe.zip";
        elemIF.style.display = "none";
        document.body.appendChild(elemIF);
}
function downCert() {
    var elemIF = document.createElement("iframe");
    elemIF.src = "Client/Cert.rar";
    elemIF.style.display = "none";
    document.body.appendChild(elemIF);
}
    </script>
    <style type="text/css">
</style>
</head>
<body background='./images/mainback11.jpg'>
    <form id="form1" runat="server">
        <div align='center'>
            <h1>福田U盾使用说明</h1>
        </div>
        <table width='1100' align='center' border="0px;">
            <tr>
                <td>
                    <span style="line-height: 35px;">一、浏览器使用设置</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;当前数字证书插件仅支持IE浏览器，请使用IE浏览器访问。</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浏览器设置方法如下：</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1、打开IE浏览器，点击“工具”-->“Internet选项(I)”--->“安全”，</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2、选中“可信站点”区域，点击“站点(S)”按钮，在弹出的“可信站点”页面，将网址"http://*.foton.com.cn","https://*.foton.com.cn"</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;添加到“将该网站添加到区域(D)”下的输入框，点击添加按钮。点击“关闭(C)”。</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3、在“Internet属性”页面，点击“自定义级别(C)”按钮，弹出的“安全设置-受信任的站点区域”页面。在“重置为(R)”下拉列表选择“低”，</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;点击“重置(E)”按钮。点击“确定”，关闭当前页面。</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4、在“Internet属性”页面，点击“确定”，退出。</span><br />
                    <span style="line-height: 35px;">二、首次使用U盾时，请修改默认密码，方法如下：</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1、下载并安装“<span style="line-height: 30px;"><a href='#' onclick="downTool()">三未信安USBKey管理工具v3.1</a></a>”</span>。</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2、插入U盾并打开USBKey管理工具，点击程序中“修改口令”按钮</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3、在弹出的对话框中分别输入“原口令”、“新口令”和“确认新口令”，点击确定。U盾默认口令“66666666”，新口令和确认新口令长度为8-16，</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;要求字母加数字。<font color='red'>请牢记新设口令</font>，否则将无法进入系统。</span><br />
                    <span style="line-height: 35px;">三、数字证书选择</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在登录应用系统前，如果浏览器证书容器中存放多张证书，会自动弹出选择证书界面，需要用户选择要使用的证书。用户在选择时，要选择由</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;“福田CA”签发的数字证书。证书名称为“福田U盾证书”或“福田安全证书”。当证书容器中仅有一张证书时，不会弹出该选择界面。</span><br />
                    <span style="line-height: 35px;">四、如果浏览器地址栏出现红色并显示“证书错误”，请下载并安装“<a href='#' onclick="downCert()">福田Cert.der证书</a>”!</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;安装方法：</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1、下载后双击打开该证书文件，点击“安装证书”按钮，然后点击“下一步（N）”。</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2、在证书存储页面选择“将所有的证书放入下列存储（P）”，然后点击“浏览（R)”按钮，在弹出的对话框中，选择<font color='red'>“受信任的根证书颁发机构”</font>，</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;点击“下一步（N）”。</span><br />
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3、点击“完成”，在弹出的“安全性警告页面”，点击“是（Y)”按钮。程序弹出导入成功提示，点击确定完成安装。</span><br />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>