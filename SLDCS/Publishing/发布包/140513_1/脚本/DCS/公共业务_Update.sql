
--增加 Tables: 公告操作日志
--增加Sequences:S_公告操作日志
    
    
create sequence S_NotificationOpLog
/

/*==============================================================*/
/* Table: NotificationOpLog                                     */
/*==============================================================*/
create table NotificationOpLog  (
   Id                   NUMBER(9)                       not null,
   NotificationID       NUMBER(9)                       not null,
   OperationType        NUMBER(9)                       not null,
   CustomerCompanyId    NUMBER(9)                       not null,
   CustomerCompanyCode  VARCHAR2(50)                    not null,
   CustomerCompanyName  VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_NOTIFICATIONOPLOG primary key (Id)
)
/
