-- 供应商索赔结算单,供应商索赔结算单清单添加字段 旧件运费

Alter Table SupplierClaimSettleBill Add   OldPartTranseCost    NUMBER(19,4);

Alter Table SupplierClaimSettleDetail Add   OldPartTranseCost    NUMBER(19,4);
