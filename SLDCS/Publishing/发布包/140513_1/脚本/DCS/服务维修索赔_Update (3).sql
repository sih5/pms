--售前检查单添加字段
     /* Columns:
        [+-]<Column> 审核意见
        [+-]<Column> 驳回意见
        [+-]<Column> 驳回人Id
        [+-]<Column> 驳回人
        [+-]<Column> 驳回时间*/

Alter Table PreSaleCheckOrder Add  ApprovertComment     VARCHAR2(200);
Alter Table PreSaleCheckOrder Add     RejectComment        VARCHAR2(200);
Alter Table PreSaleCheckOrder Add       RejectId             NUMBER(9);
Alter Table PreSaleCheckOrder Add     RejectName           VARCHAR2(100);
Alter Table PreSaleCheckOrder Add     RejectTime           Date;
