--<Table> 维修单供应商索赔清单
--<Sequence> S_维修单供应商索赔清单
    
create sequence S_RepairOrderSupplierDetail
/

/*==============================================================*/
/* Table: RepairOrderSupplierDetail                             */
/*==============================================================*/
create table RepairOrderSupplierDetail  (
   Id                   NUMBER(9)                       not null,
   RepairOrderFaultReasonId NUMBER(9)                       not null,
   SupplierId           NUMBER(9)                       not null,
   TotalAmount          NUMBER(19,4),
   LaborCost            NUMBER(19,4),
   MaterialCost         NUMBER(19,4),
   PartsManagementCost  NUMBER(19,4),
   OtherCost            NUMBER(19,4),
   constraint PK_REPAIRORDERSUPPLIERDETAIL primary key (Id)
)
/
