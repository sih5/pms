
--配件供应商基本信息/ 配件与供应商关系添加字段
    /*  Columns:
        [+-]<Column> 是否可索赔
    */


Alter Table PartsSupplier Add  IsCanClaim           NUMBER(1);
Alter Table PartsSupplierRelation  Add  IsCanClaim           NUMBER(1);
