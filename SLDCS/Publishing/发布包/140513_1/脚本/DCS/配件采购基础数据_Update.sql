--分公司与供应商关系增加字段工时单价、是否按工时单价索赔、工时系数、外出里程单价、外出人员单价、是否存在外出费用、旧件运费系数
                
Alter Table BranchSupplierRelation Add   LaborUnitPrice       NUMBER(19,4);
 
Alter Table BranchSupplierRelation Add   IfSPByLabor          NUMBER(1);
Update BranchSupplierRelation Set  IfSPByLabor=0 Where IfSPByLabor Is Null;
Alter  Table BranchSupplierRelation Modify    IfSPByLabor       not Null;
alter table BranchSupplierRelation modify IfSPByLabor       default 0;

Alter Table BranchSupplierRelation Add   LaborCoefficient     NUMBER(15,6);
alter table BranchSupplierRelation modify LaborCoefficient     default 1;
Alter Table BranchSupplierRelation Add   OutServiceCarUnitPrice NUMBER(19,4);
Alter Table BranchSupplierRelation Add   OutSubsidyPrice      NUMBER(19,4);

Alter Table BranchSupplierRelation Add   IfHaveOutFee         NUMBER(1);
Update BranchSupplierRelation Set  IfHaveOutFee=0 Where IfHaveOutFee Is Null;
Alter Table   BranchSupplierRelation Modify    IfHaveOutFee       not Null;
alter table BranchSupplierRelation modify IfHaveOutFee       default 0;

Alter Table BranchSupplierRelation Add   OldPartTransCoefficient NUMBER(15,6);
alter table BranchSupplierRelation modify OldPartTransCoefficient default 1;