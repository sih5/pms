 /*  [!]<Table> EPCƷ��ӳ��
      Columns:
        [#]<Column> Id
          <Sequence>  S_EPCƷ��ӳ�� : <None>*/

create sequence S_PMSEPCBRANDMAPPING
/


/*==============================================================*/
/* Table: PMSEPCBRANDMAPPING                                    */
/*==============================================================*/
create table PMSEPCBRANDMAPPING  (
   Id                   NUMBER(9)                       not null,
   PMSBrandId           NUMBER(9)                       not null,
   PMSBrandCode         VARCHAR2(50)                    not null,
   PMSBrandName         VARCHAR2(50)                    not null,
   EPCBrandId           VARCHAR2(50)                    not null,
   EPCBrandCode         VARCHAR2(50)                    not null,
   EPCBrandName         VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9)                       not null,
   CreatorName          VARCHAR2(50)                    not null,
   Createtime           DATE                            not null,
   constraint PK_PMSEPCBRANDMAPPING primary key (Id)
)
/
