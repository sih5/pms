  update page set name ='查询电子目录图册',Description='查询电子目录图册'  where id= 4108 and name ='配件目录选择';
  
create  table tmpdata.rule140516 as select * from rule where rownum<0;


declare
  cursor Cursor_role is(
    select *
      from role
     where enterpriseid in
           (select id from enterprise where EnterpriseCategoryid =2)
       );
begin
  for S_role in Cursor_role loop
    insert into tmpdata.rule140516 
      select S_rule.Nextval, S_role.id, t.id
        from (select distinct c.id
                from page a
               left join action b 
               on b.pageid=a.id
               inner join node c
                  on （ a.id= c.categoryid ) and c.categorytype=0
                 where a.name = '查询电子目录图册') t
       where not exists (select 1
                from rule v
               where v.nodeid = t.id
                 and v.roleid = S_role.id);
  end loop;
end;

insert into security.rule select * from tmpdata.rule140516_2 ;


create  table tmpdata.rule140516_2 as select * from rule where rownum<0;


declare
  cursor Cursor_role is(
    select *
      from role
     where enterpriseid in
           (select id from enterprise where EnterpriseCategoryid =6)
       );
begin
  for S_role in Cursor_role loop
    insert into tmpdata.rule140516_2 
      select S_rule.Nextval, S_role.id, t.id
        from (select distinct c.id
                from page a
               inner join action b 
               on b.pageid=a.id
               inner join node c
                  on( (  a.id= c.categoryid ) and （c.categorytype=0）)or( (  b.id= c.categoryid ) and （c.categorytype=1）)
                 where a.name = '供应商索赔结算管理-供应商') t
       where not exists (select 1
                from rule v
               where v.nodeid = t.id
                 and v.roleid = S_role.id);
  end loop;
end;

insert into security.rule select * from tmpdata.rule140516_2 ;
delete from  tmpdata.rule140516_2
delete from rule where exists(select 1 from tmpdata.rule140516_2 where rule.id=rule140516_2.id)
