--服务
--select * from page where parentid=13200;
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13203,13200, 2, 'PartsFailureRateDetails', 'Released', NULL, '配件故障率(配件明细)','统计某一时间段内某一供应商维修旧件明细的故障率', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13204,13200, 2, 'PartsFailureRate', 'Released', NULL, '配件故障率', '统计某一时间段内某一供应商维修旧件的故障率', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13205,13200, 2, 'RepairDetailsQuery', 'Released', NULL, '维修单综合查询报表(明细)', '统计某一时间段内维修内容明细含索赔单号', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 11, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13206,13200, 2, 'RepairIntegratedQuery', 'Released', NULL, '维修单综合查询报表', '统计某一时间段内维修单主单信息含索赔单号', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 12, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13207,13200, 2, 'WarrantyDetailsQuery', 'Released', NULL, '保修明细表', '统计某一时间段内服务站保修明细内容', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 13, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13208,13200, 2, 'SupplierFailureItemSta', 'Released', NULL, '各供应商故障项次统计', '统计某一时间段内某一供应商发生的故障项次、台次', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 14, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13209,13200, 2, 'FailureModeFailureItemSta', 'Released', NULL, '按故障模式统计故障项次统计', '统计某一时间段内某一故障原因的故障项次、台次', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 15, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13210,13200, 2, 'SeriesFailureItemSta', 'Released', NULL, '各系列故障分布统计', '统计某一时间段内各系列故障项次、台次，可以按车型平台、产品类别、驱动形式、子品牌、整车编号分类统计', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 16, 2);

--配件
--select * from page where parentid=13100;
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13137,13100, 2, 'PeriodSupplyAmount', 'Released', NULL, '期间供货额', '统计某一时间段内某个仓库销售出库、采购入库的配件明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 14, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13138,13100, 2, 'PurchasePriceFailureAMonth', 'Released', NULL, '采购价格失效前一个月统计', '统计截至目前之后30天将失效的采购价格配件明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 15, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13139,13100, 2, 'AgentsCreditQuotaQuery', 'Released', NULL, '代理库授信额度统计报表', '统计某一时间段内某一代理库给予服务站的授信金额明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 16, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13140,13100, 2, 'AgentsHistoryInQuery', 'Released', NULL, '代理库入库记录查询', '统计某一时间段内代理库入库明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 17, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13141,13100, 2, 'AgentsHistoryOutQuery', 'Released', NULL, '代理库出库记录查询', '统计某一时间段内代理库出库明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 18, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13142,13100, 2, 'PurchasePlaseRate', 'Released', NULL, '采购到位率', '统计某一时间段内某一仓库采购某一供应商的采购到位率', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 19, 2);

--财务
--select  * from page where parentid=13300;
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13303,13300, 2, 'PurchasingCostDerate', 'Released', NULL, '采购成本降低额', '统计某一时间段内供应商出入库配件数量及相比去年年底采购价格出入库金额价差', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 5, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
