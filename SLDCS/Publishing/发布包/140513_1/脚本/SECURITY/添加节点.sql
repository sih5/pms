--Select * From page Where parentid=8800;
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8806, 8800, 2, 'SupplierClaimSettleBill', 'Released', NULL, '供应商索赔结算管理', '生成、修改、审批供应商索赔结算单', 'Client/DCS/Images/Menu/Service/SupplierClaimSettle.png', 6, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8807,8800, 2, 'SupplierClaimSettleBillQuery', 'Released', NULL, '供应商索赔结算管理-供应商', '供应商查询供应商索赔结算单', 'Client/DCS/Images/Menu/Service/SupplierClaimSettleForSupplier.png', 7, 2);
-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8806, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8806, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8806, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8806, 'Common|BatchApproval', '批量审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8806, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8806, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8806, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8806, 'Common|ExportDetail', '导出清单', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8807, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8807, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8807, 'Common|ExportDetail', '导出清单', 2);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
                  
                  --select * from action where pageid=8806;
