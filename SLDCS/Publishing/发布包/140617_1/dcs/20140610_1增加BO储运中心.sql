--增加BO储运中心
create sequence S_StorageCenter
/

/*==============================================================*/
/* Table: StorageCenter                                         */
/*==============================================================*/
create table StorageCenter  (
   Id                   NUMBER(9)                       not null,
   WMSWarehouseCode     VARCHAR2(50),
   WMSWarehouseName     VARCHAR2(50),
   constraint PK_STORAGECENTER primary key (Id)
)
/
