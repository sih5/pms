--储运中心
create or replace trigger StorageCenter_trig
  after insert or update ON dcs.StorageCenter
  FOR EACH ROW
DECLARE
BEGIN
  DECLARE
  Begin
		Declare KeyId numner(9,0);
		KeyId := Null;
		Begin
		  Select Id Into KeyId From dcs.KeyValueItem Where Name = 'Storage_Center' And Key=:new.id;
		Exception
		  When No_Data_Found Then
			      KeyId := Null;
		End
		If KeyId Is Null Then 
			Insert Into dcs.KeyValueItem(Category,Name,caption,Key,Value,isbuiltin,status)
			Values('DCS','Storage_Center','储运中心',:new.id,:New.Wmswarehousename,0,1);
		Else
			Update dcs.KeyValueItem
			Set Value=:new.Wmswarehousename
			Where Id=KeyId;
		End If
  END;
END;
