-- 供应商发运单增加字段收货单位编号

Alter Table SupplierShippingOrder Add    ReceivingCompanyCode VARCHAR2(100);

--更新供应商发运单收货单位编号字段

Update SupplierShippingOrder a Set a.ReceivingCompanyCode=(Select Code From dcs.company b Where b.id=a.receivingcompanyid);
