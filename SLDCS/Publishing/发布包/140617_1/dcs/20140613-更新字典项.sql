--更新维修资质等级字典项

Update dcs.KeyValueItem Set Value='一类' Where caption='维修资质等级' And Key=1;
Update dcs.KeyValueItem Set Value='二类' Where caption='维修资质等级' And Key=2;
Update dcs.KeyValueItem Set Value='三类' Where caption='维修资质等级' And Key=3;

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'Repair_Qualification_Grade', '维修资质等级', 4, '无', 0, 1);

--更新维修申请类型字典项
 Update KeyValueItem Set Value='油品补加' Where caption='维修资质等级' And Key=11;

--更新销售与服务场地布局关系字典项

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SaleServiceLayout', '销售与服务场地布局关系', 1, '前后', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SaleServiceLayout', '销售与服务场地布局关系', 2, '左右', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SaleServiceLayout', '销售与服务场地布局关系', 3, '分离', 1, 1);

--订单处理方式增加统购分销
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSalesOrderProcessDetail_ProcessMethod', '订单处理方式', 10, '统购分销', 1, 1);
