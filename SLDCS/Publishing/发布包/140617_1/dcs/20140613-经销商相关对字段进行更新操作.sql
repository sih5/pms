-- 经销商分公司管理信息、 经销商分公司管理信息履历增加字段配件储备金额、销售与服务场地布局关系

Alter Table DealerServiceInfo Add  PartReserveAmount    VARCHAR2(100);
Alter Table DealerServiceInfo Add   SaleandServicesiteLayout NUMBER(9);

Alter Table DealerServiceInfoHistory Add  PartReserveAmount    VARCHAR2(100);
Alter Table DealerServiceInfoHistory Add   SaleandServicesiteLayout NUMBER(9);

--经销商分公司管理信息更新配件储备金额字段
Update dcs.dealerserviceinfo a Set a.PartReserveAmount=(Select PartReserveAmount From DealerServiceExt b Where b.id=a.dealerid);

--备份经销商服务扩展信息。经销商服务扩展信息履历数据
Create Table tmpdata.DealerServiceExt20140613 As Select Id,PartReserveAmount From dcs.DealerServiceExt;
Create Table tmpdata.DealerServiceExtHis140613 As Select Id,PartReserveAmount From dcs.DealerServiceExtHistory;

--经销商服务扩展信息/经销商服务扩展信息履历删除字段配件储备金额

Alter Table DealerServiceExt Drop Column PartReserveAmount;
Alter Table DealerServiceExtHistory Drop Column PartReserveAmount;
