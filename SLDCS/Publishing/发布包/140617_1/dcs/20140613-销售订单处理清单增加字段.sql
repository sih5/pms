--配件销售订单处理清单增加字段统购出库仓库Id、 统购出库仓库编号、统购出库仓库名称

Alter Table PartsSalesOrderProcessDetail Add  PurchaseWarehouseId  NUMBER(9);
Alter Table PartsSalesOrderProcessDetail Add    PurchaseWarehouseCode VARCHAR2(50);
Alter Table PartsSalesOrderProcessDetail Add    PurchaseWarehouseName VARCHAR2(100);
