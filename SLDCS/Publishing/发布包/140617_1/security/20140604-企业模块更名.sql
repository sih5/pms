
/*
--1 、脚本执行时间: 2014-06-4
--2 、脚本编制人:  袁霞
--3 、脚本审核人:  
--4 、脚本执行服务器:  FT所在正式服务器
--5 、脚本执行数据库名: Security
--6 、脚本执行原因: 模块更名
--7 、请注明每条SQL语句的作用
--8 、需分步操作的请注明方法：
*/

--备份更新的数据
create table tmpdata.Page0604 as
Select a.* from Security.Page a where id in (1100,1104);


update Security.Page set Name = '企业信息管理' where name='企业' and id=1100;

update Security.Page set Name = '生产工厂管理',Description='维护生产工厂信息' where name='责任单位管理' and id=1104;



