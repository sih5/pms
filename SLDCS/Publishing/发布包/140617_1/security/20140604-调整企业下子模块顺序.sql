
/*
--1 、脚本执行时间: 2014-06-4
--2 、脚本编制人:  袁霞
--3 、脚本审核人:  
--4 、脚本执行服务器:  FT所在正式服务器
--5 、脚本执行数据库名: Security
--6 、脚本执行原因: 模块更名
--7 、请注明每条SQL语句的作用
--8 、需分步操作的请注明方法：
*/

--备份更新的数据
create table tmpdata.Page0604_1 as
Select a.* from Security.Page a where id in (1101,1102,1103,1104,1105);


update Security.Page set Sequence = 0 where id=1104;
update Security.Page set Sequence = 1 where id=1105;
update Security.Page set Sequence = 2 where id=1101;
update Security.Page set Sequence = 3 where id=1102;
update Security.Page set Sequence = 4 where id=1103;
--update Security.Page set Sequence = 5 where id=1106;
--update Security.Page set Sequence = 6 where id=1107;