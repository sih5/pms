
/*
--1 、脚本执行时间: 2014-06-4
--2 、脚本编制人:  袁霞
--3 、脚本审核人:  
--4 、脚本执行服务器:  FT所在正式服务器
--5 、脚本执行数据库名: Security
--6 、脚本执行原因: 模块更名
--7 、请注明每条SQL语句的作用
--8 、需分步操作的请注明方法：
*/

--备份更新的数据
create table tmpdata.Page0605 as
Select a.* from Security.Page a where id in (1200,1206,1207，1208);


update Security.Page set Name = '营销公司信息管理',Description='维护营销公司信息' where name='内部组织' and id=1200;

update Security.Page set Name = '大区管理',Description='维护大区信息' where name='销售区域管理' and id=1206;

update Security.Page set Name = '大区与人员关系',Description='分公司维护大区与人员的关系' where name='销售区域与人员关系' and id=1207;

update Security.Page set Name = '大区与市场部关系',Description='分公司维护大区与市场部的关系' where name='销售区域与市场部关系' and id=1208;
