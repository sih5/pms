
/*
--1 、脚本执行时间: 2014-06-4
--2 、脚本编制人:  袁霞
--3 、脚本审核人:  
--4 、脚本执行服务器:  FT所在正式服务器
--5 、脚本执行数据库名: Security
--6 、脚本执行原因: 模块更名
--7 、请注明每条SQL语句的作用
--8 、需分步操作的请注明方法：
*/

--备份更新的数据
create table tmpdata.Page0605_1 as
Select a.* from Security.Page a where id in (1201,1202,1203,1204,1205,1206,1207,1208,8307,8308);


update Security.Page set Sequence = 0 where id=1202;
update Security.Page set Sequence = 1 where id=1205;
update Security.Page set Sequence = 2 where id=1206;
update Security.Page set Sequence = 3 where id=1207;
update Security.Page set Sequence = 4 where id=1203;
update Security.Page set Sequence = 5 where id=1204;
update Security.Page set Sequence = 6 where id=1208;
update Security.Page set Sequence = 7 where id=1201;

--将“供应商扣补款管理”调整到“供应商索赔结算管理”模块下；
update Security.Page set Sequence = 4,ParentId =8900 ,icon='Client\Dcs\Images\Menu\Service\SupplierBuckleFillingMoney.png' where id=8307;

--扣补款单查询节点，移到索赔单管理-服务站下；
update Security.Page set Sequence = 6,ParentId =8600  where id=8308;
