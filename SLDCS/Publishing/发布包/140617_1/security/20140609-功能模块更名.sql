
/*
--1 、脚本执行时间: 2014-06-09
--2 、脚本编制人:  袁霞
--3 、脚本审核人:  
--4 、脚本执行服务器:  FT所在正式服务器
--5 、脚本执行数据库名: Security
--6 、脚本执行原因: 模块更名
--7 、请注明每条SQL语句的作用
--8 、需分步操作的请注明方法：
*/

--备份更新的数据
create table tmpdata.Page0609 as
Select a.* from Security.Page a where id in (7302,7304,1301,1302);


update Security.Page set Name = '故障代码与产品线对应关系',Description='查询和维护故障代码与产品线关系' where id=7302;

update Security.Page set Name = '标准维修工时代码与产品线对应关系',Description='查询和维护标准维修工时代码与产品线关系' where id=7304;

update Security.Page set Name = '服务站/专卖店企业信息管理' where id=1301;
update Security.Page set Name = '服务站/专卖店分品牌信息管理',Description='查询和维护服务站/专卖店分品牌信息' where id=1302;