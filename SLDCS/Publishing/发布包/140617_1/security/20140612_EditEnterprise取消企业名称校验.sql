create or replace procedure security.EditEnterprise(PId number, PName varchar2, PType number, PModifierId number, PModifierName varchar2, PModifyTime date) as
  EnterpriseId integer;
begin
  /*begin
    select Id into EnterpriseId from Enterprise
     where Status != 0 and Name = PName and Id != PId and rownum = 1;
    raise_application_error(-20003,'已存在相同名称的企业');
  exception
    when NO_DATA_FOUND then
      null;
    when others then
      raise;
  end;*/

  begin
    select Id into EnterpriseId from Enterprise
     where Status = 0 and Id = PId;
    raise_application_error(-20004,'只允许更新处于“新建”、“冻结”、“生效”状态下的企业');
  exception
    when NO_DATA_FOUND then
      null;
    when others then
      raise;
  end;

  update Enterprise set Name = PName, EnterpriseCategoryId = PType, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime
   where Enterprise.Id = PId;

  update Organization set Name = PName, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime
   where Organization.EnterpriseId = PId and Organization.Type = 0;
end;
/
