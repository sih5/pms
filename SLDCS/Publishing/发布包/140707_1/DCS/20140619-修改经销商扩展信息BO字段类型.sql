Select HasBranch,DangerousRepairQualification From DealerServiceExtHistory;
--修改经销商扩展信息表字段

Update dcs.DealerServiceExt Set DangerousRepairQualification=Null;
Alter Table dcs.DealerServiceExt Modify  (HasBranch NUMBER(9));
alter table dcs.DealerServiceExt Modify (DangerousRepairQualification NUMBER(9));

--修改经销商扩展信息履历表字段
Update dcs.DealerServiceExtHistory Set DangerousRepairQualification=Null;
Alter Table dcs.DealerServiceExtHistory  Modify (HasBranch NUMBER(9));
alter table dcs.DealerServiceExtHistory Modify (DangerousRepairQualification NUMBER(9));
