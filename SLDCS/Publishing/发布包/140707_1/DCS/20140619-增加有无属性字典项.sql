--更新字典项
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'YesOrNo', '有无属性', 1, '有', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'YesOrNo', '有无属性', 0, '无', 1, 1);
--应收账业务类型增加客户转账项
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'AccountPayment_BusinessType', '应收账业务类型', 16, '客户转账', 1, 1);