create or replace trigger StorageCenter_trig
  after insert or update ON dcs.StorageCenter
  FOR EACH ROW
DECLARE
  KeyId number;
BEGIN
  Begin
    Select Id
      Into KeyId
      From dcs.KeyValueItem
     Where Name = 'Storage_Center'
       And Key = :new.id;
  Exception
    When No_Data_Found Then
      KeyId := Null;
  end;

  If KeyId Is Null Then
    begin
      Insert Into dcs.KeyValueItem
        (Category, Name, caption, Key, Value, isbuiltin, status)
      Values
        ('DCS',
         'Storage_Center',
         '��������',
         :new.id,
         :New.Wmswarehousename,
         0,
         1);
    end;
  Else
    Update dcs.KeyValueItem
       Set Value = :new.Wmswarehousename
     Where Id = KeyId;
  End If;

end;
