create or replace view servprodlineaffiproduct as
select ProductId,ServiceProductLineId,ProductLinetype from
(
select Product.id as ProductId,
       case
         when ServProdLineProductDetail.Serviceproductlineid is null then
           ServiceProdLineProduct.Serviceproductlineid
         else
           ServProdLineProductDetail.Serviceproductlineid
       end as ServiceProductLineId,
       1 as ProductLinetype
  from Product
 left join ServiceProdLineProduct
    on Product.Brandid = ServiceProdLineProduct.Brandid
  left join ServProdLineProductDetail
    on Product.Id = ServProdLineProductDetail.Productid
union
select Product.id                                 as ProductId,
       EngineModelProductLine.Engineproductlineid as ServiceProductLineId,
       2                                          as ProductLinetype
  from EngineModelProductLine
 inner join EngineModel
    on EngineModelProductLine.Enginemodeid = EngineModel.Id
 inner join Product
    on Product.EngineTypeCode = EngineModel.Enginemodel)
    where ProductId is not null and  ServiceProductLineId is not null;
    