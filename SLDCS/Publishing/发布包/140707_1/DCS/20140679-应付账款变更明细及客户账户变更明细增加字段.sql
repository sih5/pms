/* [+-]<Column> 借方
        [+-]<Column> 贷方
        [+-]<Column> 发生前金额
        [+-]<Column> 发生后金额*/
        
--应付账款变更明细增加字段
Alter Table AccountPayableHistoryDetail Add debit                NUMBER(19,4);
Alter Table AccountPayableHistoryDetail Add   credit               NUMBER(19,4);
Alter Table AccountPayableHistoryDetail Add  BeforeChangeAmount   NUMBER(19,4);
Alter Table AccountPayableHistoryDetail Add  AfterChangeAmount    NUMBER(19,4);

--客户账户变更明细增加字段
Alter Table CustomerAccountHisDetail Add debit                NUMBER(19,4);
Alter Table CustomerAccountHisDetail Add   credit               NUMBER(19,4);
Alter Table CustomerAccountHisDetail Add  BeforeChangeAmount   NUMBER(19,4);
Alter Table CustomerAccountHisDetail Add  AfterChangeAmount    NUMBER(19,4);
