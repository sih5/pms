create or replace procedure ExecutePlannedPriceApp(priceAppId in number) as
  /*变更前价格合计*/
  sumMoney_Before NUMBER(19, 4);
  /*变更后价格合计*/
  sumMoney_After NUMBER(19, 4);
  /*变更后价格差异合计*/
  sumMoney_Diff NUMBER(19, 4);
  /*仓库成本变更单自增长Id*/
  n integer;
  /*计划价申请单*/
  FOwnerCompanyType integer;
  FPlannedPriceApp PlannedPriceApp%rowtype;
  /*自动获取单据编码相关*/
  FDateFormat   date;
  FDateStr      varchar2(8);
  FSerialLength integer;
  FNewCode      varchar2(50);
  FComplateId   integer;
  cursor warehouses is
    select Id, Code, Name,Type from Warehouse
     where StorageCompanyId = FPlannedPriceApp.OwnerCompanyId and Status = 1 and Type in(1,2);
  warehouse warehouses%rowtype;
begin
  /*编码规则变更，需同步调整*/
  FDateFormat:=trunc(sysdate,'YEAR');
  --FDateFormat:=trunc(sysdate,'MONTH');
  --FDateFormat   := trunc(sysdate, 'DD');
  FDateStr      := to_char(sysdate, 'yyyy');
  FSerialLength := 4;
  FNewCode      := 'WCC{CORPCODE}' || FDateStr || '{SERIAL}';

  begin
    select * into FPlannedPriceApp from PlannedPriceApp where Id = priceAppId and Status = 2;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20001, '只允许执行处于“已审核”状态的计划价申请单。');
    when others then
      raise;
  end;

  -- 获取隶属企业类型
  begin
    select Type into FOwnerCompanyType from Company where Id = FPlannedPriceApp.OwnerCompanyId;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20002, '计划价申请单隶属企业不存在。');
    when others then
      raise;
  end;

  /*获取编号生成模板Id*/
  begin
    select Id into FComplateId from CodeTemplate where name = 'WarehouseCostChangeBill' and IsActived = 1;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20003, '未找到名称为"WarehouseCostChangeBill"的有效编码规则。');
    when others then
      raise;
  end;

  /*更新计划价申请单，清单赋值变更前计划价*/
  update PlannedPriceAppDetail detail set (PriceBeforeChange, Quantity, AmountDifference) =
         (select nvl(PlannedPrice,0), 0, 0 from PartsPlannedPrice price
           where OwnerCompanyId = FPlannedPriceApp.OwnerCompanyId
             and price.partssalescategoryid=FPlannedPriceApp.partssalescategoryid
             and price.SparePartId = detail.SparePartId)
   where detail.PlannedPriceAppId = FPlannedPriceApp.Id
     and exists (select 1 from PartsPlannedPrice price
                  where price.OwnerCompanyId = FPlannedPriceApp.OwnerCompanyId
                    and price.partssalescategoryid=FPlannedPriceApp.partssalescategoryid
                    and price.SparePartId = detail.SparePartId);

    update PlannedPriceAppDetail detail
    set PriceBeforeChange=0, Quantity=0, AmountDifference=0
    WHERE detail.PlannedPriceAppId = FPlannedPriceApp.Id
     AND NOT exists (select 1 from PartsPlannedPrice price
                  where price.OwnerCompanyId = FPlannedPriceApp.OwnerCompanyId
                    and price.partssalescategoryid=FPlannedPriceApp.partssalescategoryid
                    and price.SparePartId = detail.SparePartId);

  /*更新计划价申请单，清单赋值数量、差异金额(数量包括在库中的数量和在途的数量，在途数量从企业调拨在途配件成本中获取，在库中的数量从配件库存中获取）*/
 update PlannedPriceAppDetail detail
   set (quantity, Amountdifference) =
       (select nvl(cost.Quantity,0),
               (detail.RequestedPrice - nvl(detail.PriceBeforeChange, 0)) *
               cost.Quantity
          from (select tmp.sparepartid, sum(tmp.tempquantity) as quantity
                  from (select a.partid as sparepartId,
                               nvl(a.quantity, 0) as tempquantity
                          from partsstock a,SalesUnitAffiWarehouse,salesunit
                          WHERE a.warehouseid=SalesUnitAffiWarehouse.Warehouseid
                          AND SalesUnitAffiWarehouse.Salesunitid=salesunit.id
                          AND salesunit.partssalescategoryid=FPlannedPriceApp.Partssalescategoryid
                        union
                        select b.sparepartId,
                               nvl(b.quantity, 0) as tempquantity
                          from PartsCostTransferInTransit b
                         where b.ownercompanyid =
                               FPlannedPriceApp.OwnerCompanyId
                           and b.partssalescategoryid =
                               FPlannedPriceApp.partssalescategoryid) tmp
                 group by tmp.sparepartid) cost
         where cost.sparepartid = detail.sparepartid)
 where detail.PlannedPriceAppId = priceAppId
   and exists
 (select *
          from (select a.partid as sparepartId
                  from partsstock a,SalesUnitAffiWarehouse,salesunit
                  WHERE a.warehouseid=SalesUnitAffiWarehouse.Warehouseid
                  AND SalesUnitAffiWarehouse.Salesunitid=salesunit.id
                  AND salesunit.partssalescategoryid=FPlannedPriceApp.Partssalescategoryid
                union
                select b.sparepartId
                  from PartsCostTransferInTransit b
                 where b.ownercompanyid = FPlannedPriceApp.OwnerCompanyId
                   and b.partssalescategoryid =
                       FPlannedPriceApp.partssalescategoryid) c
         where c.sparepartId = detail.sparepartid);

  /*更新配件计划价，如果不存在则新增*/
  merge into PartsPlannedPrice price
  using (select * from PlannedPriceAppDetail where PlannedPriceAppId = FPlannedPriceApp.Id) detail
  on (price.OwnerCompanyId = FPlannedPriceApp.OwnerCompanyId and price.SparePartId = detail.SparePartId and price.partssalescategoryid=FPlannedPriceApp.partssalescategoryid)
  when matched then
    update set price.plannedprice = detail.RequestedPrice,price.modifierid=FPlannedPriceApp.Modifierid,price.modifiername=FPlannedPriceApp.Modifiername,price.modifytime=FPlannedPriceApp.Modifytime
  when not matched then
    insert
      (Id,OwnerCompanyId, OwnerCompanyType, SparePartId, PlannedPrice, CreatorId, CreatorName, CreateTime,PartsSalesCategoryId,PartsSalesCategoryName)
    values
      (S_PartsPlannedPrice.Nextval,FPlannedPriceApp.OwnerCompanyId, FOwnerCompanyType, detail.SparePartId, detail.RequestedPrice, FPlannedPriceApp.CreatorId, FPlannedPriceApp.CreatorName, sysdate,FPlannedPriceApp.PartsSalesCategoryId,FPlannedPriceApp.PartsSalesCategoryName);

  /*更新企业调拨在途配件成本*/
  update PartsCostTransferInTransit cost set (CostPrice, CostAmount) =
         (select detail.RequestedPrice, detail.RequestedPrice * cost.Quantity
            from PlannedPriceAppDetail detail
           where detail.PlannedPriceAppId = FPlannedPriceApp.Id
             and detail.SparePartId = cost.SparePartId)
   where cost.OwnerCompanyId = FPlannedPriceApp.OwnerCompanyId
   and cost.partssalescategoryid=FPlannedPriceApp.partssalescategoryid
     and exists (select 1 from PlannedPriceAppDetail detail
                  where detail.PlannedPriceAppId = FPlannedPriceApp.Id
                    and detail.SparePartId = cost.SparePartId);

  /*更新计划价申请单*/
  update PlannedPriceApp set ActualExecutionTime = sysdate, Status = 3, (AmountBeforeChange, AmountAfterChange, AmountDifference) =
         (SELECT sum(PriceBeforeChange * Quantity), sum(RequestedPrice * Quantity), sum(AmountDifference)
            from plannedPriceAppDetail where plannedPriceAppDetail.PlannedPriceAppId = FPlannedPriceApp.Id)
   where Id = FPlannedPriceApp.Id;


  /*针对每一个仓库，生成仓库成本变更单和清单*/
  for warehouse in warehouses loop
    /*获取自增长Id，插入库存成本变更单*/
    select S_WAREHOUSECOSTCHANGEBILL.NEXTVAL into n from dual;
    /*插入库存成本变更清单*/
    insert into WarehouseCostChangeDetail
      (Id, WarehouseCostChangeBillId, WarehouseId,
       SparePartId, SparePartCode, SparePartName,
       Pricebeforechange, Quantity, PriceAfterChange,
       AmountDifference)
    select S_WAREHOUSECOSTCHANGEDETAIL.NEXTVAL, n, warehouse.Id,
           SparePartId, SparePartCode, SparePartName,
           PriceBeforeChange, stock.TotalQuantity, RequestedPrice,
           (RequestedPrice - nvl(PriceBeforeChange,0)) * stock.TotalQuantity
      from (select PartId,sum(Quantity) as TotalQuantity
            from (select PartId,Quantity
                  from PartsStock
                 where WarehouseId = warehouse.Id
                   and exists (select 1 from SalesUnitAffiWarehouse where SalesUnitAffiWarehouse.Warehouseid=warehouse.Id and SalesUnitAffiWarehouse.Salesunitid in (select id from Salesunit where Salesunit.Partssalescategoryid=FPlannedPriceApp.Partssalescategoryid))
                   and exists (select 1 from PlannedPriceAppDetail
                                where PlannedPriceAppDetail.PlannedPriceAppId = FPlannedPriceApp.Id
                                  and PlannedPriceAppDetail.SparePartId = PartsStock.PartId)
                 union 
                 select plbid.SparePartId as PartId, (OutboundAmount - InboundAmount) as Quantity
                   from PartsLogisticBatchItemDetail plbid
                  inner join PartsLogisticBatchBillDetail plbbd
                     on plbid.PartsLogisticBatchId = plbbd.PartsLogisticBatchId
                        and plbbd.BillType = 1
                  inner join PartsOutboundBill pob
                     on plbbd.BillId = pob.Id
                        and pob.OutboundType = 4
                        and pob.ORIGINALREQUIREMENTBILLTYPE=6
                  where plbid.OutboundAmount > plbid.InboundAmount
                        and pob.ReceivingWarehouseId = warehouse.Id
                        and exists (select 1 from PlannedPriceAppDetail
                                where PlannedPriceAppDetail.PlannedPriceAppId = FPlannedPriceApp.Id
                                  and PlannedPriceAppDetail.SparePartId = plbid.SparePartId)) t
              group by t.PartId) stock
     inner join PlannedPriceAppDetail on PlannedPriceAppDetail.SparePartId = stock.PartId
     where PlannedPriceAppDetail.PlannedPriceAppId = FPlannedPriceApp.Id;

  /*不存在变更清单，仍然需要生成主单*/
  /*计算该仓库下的合计*/
  select nvl(sum(PriceBeforechange * Quantity),0),
         nvl(sum(PriceAfterChange * Quantity),0),
         nvl(sum(Amountdifference),0)
    into sumMoney_Before, sumMoney_After, sumMoney_Diff
    from WarehouseCostChangeDetail
   where WarehouseCostChangeBillId = n;
  /*插入主单*/
  insert into WarehouseCostChangeBill
    (Id, Code, PlannedPriceAppId,
     OwnerCompanyId, OwnerCompanyCode, OwnerCompanyName,
     WarehouseId, WarehouseCode, WarehouseName,
     AmountBeforeChange, AmountAfterChange, AmountDifference, RecordStatus,
     CreatorId, CreatorName, CreateTime)
  values
    (n,
     regexp_replace(
       regexp_replace(FNewCode, '{CORPCODE}', FPlannedPriceApp.OwnerCompanyCode),
         '{SERIAL}',
         lpad(to_char(GetSerial(FComplateId, FDateFormat, FPlannedPriceApp.OwnerCompanyCode)),FSerialLength,'0')),
     FPlannedPriceApp.Id,
     FPlannedPriceApp.OwnerCompanyId, FPlannedPriceApp.OwnerCompanyCode, FPlannedPriceApp.OwnerCompanyName,
     warehouse.Id, warehouse.Code, warehouse.Name,
     sumMoney_Before, sumMoney_After, sumMoney_Diff, 2,
     FPlannedPriceApp.CreatorId, FPlannedPriceApp.CreatorName, sysdate);
  end loop;

select S_WAREHOUSECOSTCHANGEBILL.NEXTVAL into n from dual;
  /*针对调拨在途仓库，生成仓库成本变更单和清单*/
  insert into WarehouseCostChangeDetail
    (Id, WarehouseCostChangeBillId, WarehouseId,
     SparePartId, SparePartCode, SparePartName,
     Pricebeforechange, Quantity, PriceAfterChange,
     AmountDifference)
  select S_WAREHOUSECOSTCHANGEDETAIL.NEXTVAL, n, 0,
         SparePart.Id, SparePart.Code, SparePart.Name,
         PriceBeforeChange, PartsCostTransferInTransit.Quantity, RequestedPrice,
         (RequestedPrice - nvl(PriceBeforeChange,0)) * PartsCostTransferInTransit.Quantity
    from PartsCostTransferInTransit
   inner join SparePart on PartsCostTransferInTransit.SparePartId = SparePart.Id
   inner join PlannedPriceAppDetail on PlannedPriceAppDetail.SparePartId = PartsCostTransferInTransit.SparePartId
   where PartsCostTransferInTransit.OwnerCompanyId = FPlannedPriceApp.OwnerCompanyId
     and PlannedPriceAppDetail.PlannedPriceAppId = FPlannedPriceApp.Id;
  /*计算在途仓库下的合计*/
  select nvl(sum(PriceBeforechange * Quantity),0),
         nvl(sum(PriceAfterChange * Quantity),0),
         nvl(sum(Amountdifference),0)
    into sumMoney_Before, sumMoney_After, sumMoney_Diff
    from WarehouseCostChangeDetail
   where WarehouseCostChangeBillId = n;
  /*插入库存成本变更单*/
  insert into WarehouseCostChangeBill
    (Id, Code, PlannedPriceAppId,
     OwnerCompanyId, OwnerCompanyCode, OwnerCompanyName,
     WarehouseId, WarehouseCode, WarehouseName,
     AmountBeforeChange, AmountAfterChange, AmountDifference, RecordStatus,
     CreatorId, CreatorName, CreateTime)
  values
    (n,
     regexp_replace(
       regexp_replace(FNewCode, '{CORPCODE}', FPlannedPriceApp.OwnerCompanyCode),
         '{SERIAL}',
         lpad(to_char(GetSerial(FComplateId, FDateFormat, FPlannedPriceApp.OwnerCompanyCode)),FSerialLength,'0')),
     FPlannedPriceApp.Id,
     FPlannedPriceApp.OwnerCompanyId, FPlannedPriceApp.OwnerCompanyCode, FPlannedPriceApp.OwnerCompanyName,
     0, '调拨在途', '调拨在途',
     sumMoney_Before, sumMoney_After, sumMoney_Diff, 2,
     FPlannedPriceApp.CreatorId, FPlannedPriceApp.CreatorName, sysdate);
End;
/