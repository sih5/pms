INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (3109,3100, 2, 'SupplierPartsTagPrintting', 'Released', NULL, '供应商配件标签打印', '供应商打印配件条码', 'Client/DCS/Images/Menu/PartsPurchasing/SupplierPartsTagPrintting.png', 8, 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3105, 'Common|PrintLabel', '标签打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3106, 'Common|PrintLabel', '标签打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3109, 'Common|PrintLabel', '标签打印', 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
