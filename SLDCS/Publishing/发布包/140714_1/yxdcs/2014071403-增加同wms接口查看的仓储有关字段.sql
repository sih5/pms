--配件出库单增加字段 装载单编号
--配件入库检验单增加字段 入库质检单号
--配件发运单增加字段装载单编号

Alter Table PartsInboundCheckBill Add  Objid                VARCHAR2(25);
Alter Table PartsShippingOrder Add  InterfaceRecordId    VARCHAR2(25);
Alter Table PartsOutboundBill Add  InterfaceRecordId    VARCHAR2(25);
