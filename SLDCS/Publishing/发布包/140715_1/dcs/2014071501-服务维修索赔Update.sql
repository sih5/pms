
--PS:脚本需分段执行
--更新字段长度

alter table RepairClaimBill modify   MalfunctionDescription VARCHAR2(600);
alter table RepairClaimItemDetail modify  RepairItemName       VARCHAR2(600);
alter table RepairOrderFaultReason modify  MalfunctionDescription VARCHAR2(600);
alter table RepairOrderItemDetail modify  RepairItemName       VARCHAR2(600);
alter table RepairTempletItemDetail modify  RepairItemName       VARCHAR2(600);

--创建sequence(如果报错对象名已被使用的话则此条sql语句不必执行)
create sequence S_RepairWorkOrder
/
