--分公司策略增加字段销售结算返利比例和最大开票金额,设置默认值为1000万

Alter Table dcs.Branchstrategy Add  MaxInvoiceAmount   NUMBER(19,4) default 10000000;



Alter Table dcs.Branchstrategy Add   ReturnFeeRate        NUMBER(15,6);
--更新雷萨分公司策略销售结算返利比例为0
Update Branchstrategy Set ReturnFeeRate=0   Where branchid=2;
