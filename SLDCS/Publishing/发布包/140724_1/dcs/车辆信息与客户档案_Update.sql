 /*  [!]<Table> 车辆联系人
      Columns:
        [+-]<Column> 区省市Id
        [+-]<Column> 区域
        [+-]<Column> 省份
        [+-]<Column> 城市
        [+-]<Column> 区县*/
  
  
Alter Table VehicleLinkman Add   RegionId             NUMBER(9);
Alter Table VehicleLinkman Add     RegionName           VARCHAR2(100);
Alter Table VehicleLinkman Add     ProvinceName         VARCHAR2(100);
Alter Table VehicleLinkman Add     CityName             VARCHAR2(100);
Alter Table VehicleLinkman Add    CountyName           VARCHAR2(100);
