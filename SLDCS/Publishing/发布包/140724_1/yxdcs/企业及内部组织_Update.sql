--分公司策略增加字段销售结算返利比例和最大开票金额,并设置默认值为11万

Alter Table yxdcs.Branchstrategy Add   MaxInvoiceAmount     NUMBER(19,4) default 110000;


Alter Table yxdcs.Branchstrategy Add   ReturnFeeRate        NUMBER(15,6);

--更新营销分公司策略销售结算返利比例为0
Update Branchstrategy Set ReturnFeeRate=0.2   Where branchid=2;
