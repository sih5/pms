--����DTM���BO

/*==============================================================*/
/* Table: DTMRepairContract                                     */
/*==============================================================*/
create table DTMRepairContract  (
   DTMObjid             VARCHAR2(40)                    not null,
   DCSRepairOrderId     NUMBER(9),
   Code                 VARCHAR2(50),
   SerialCode           VARCHAR2(40),
   CustomerName         VARCHAR2(100),
   CellNumber           VARCHAR2(50),
   Address              VARCHAR2(200),
   ZipCode              VARCHAR2(6),
   Vin                  VARCHAR2(50)                    not null,
   EngineCode           VARCHAR2(50),
   PlateNumber          VARCHAR2(20),
   VehicleModelCode     VARCHAR2(50),
   VehicleModelName     VARCHAR2(100),
   Mileage              NUMBER(9),
   RepairType           NUMBER(9),
   RepairTime           DATE,
   ExpectedFinishTime   DATE,
   CompletionTime       DATE,
   VehicleObjects       VARCHAR2(200),
   FuelAmount           NUMBER(15,6),
   Description          VARCHAR2(200),
   CustomerComplaint    VARCHAR2(200),
   WorkingFee           NUMBER(19,4),
   MaterialFee          NUMBER(19,4),
   OtherFee             NUMBER(19,4),
   OtherFeeDescription  VARCHAR2(200),
   IsOutService         NUMBER(1),
   IsOutClaim           NUMBER(1),
   TechnicalServiceActivityCode VARCHAR2(50),
   ClaimStatus          NUMBER(9),
   MaintenanceStatus    NUMBER(9),
   Status               NUMBER(9),
   Remark               VARCHAR2(200),
   OutletName           VARCHAR2(100),
   VehicleRepairName    VARCHAR2(100),
   VehicleRepairAddress VARCHAR2(100),
   VehicleRepairCellPhone VARCHAR2(20),
   BelongBrand          VARCHAR2(50),
   BrandCode            VARCHAR2(50),
   BrandName            VARCHAR2(100),
   DCSCorporationId     NUMBER(9),
   CorporationId        NUMBER(9),
   CorporationName      VARCHAR2(100),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   SPType               NUMBER(9),
   RepairObjectName     VARCHAR2(100),
   CauseAnalyse         VARCHAR2(200),
   RegionCode           VARCHAR2(50),
   TravelDestination    VARCHAR2(200),
   Range                NUMBER(9),
   TravelDistance       NUMBER(9),
   StartTime            DATE,
   EndTime              DATE,
   OutPersonCode        VARCHAR2(100),
   OutVehicleKind       NUMBER(1),
   OutDays              NUMBER(9),
   OutNumber            NUMBER(9),
   HighwayFreight       NUMBER(19,4),
   OutFarePrice         NUMBER(19,4),
   SubsidyPrice         NUMBER(19,4),
   TotalExpense         NUMBER(19,4),
   constraint PK_DTMREPAIRCONTRACT primary key (DTMObjid)
)
/


/*==============================================================*/
/* Table: DTMRepairContractItem                                 */
/*==============================================================*/
create table DTMRepairContractItem  (
   DTMObjid             VARCHAR2(40)                    not null,
   DTMRepairContractItemObjid VARCHAR2(40)                    not null,
   RepairItemCode       VARCHAR2(50),
   RepairItemName       VARCHAR2(200),
   AccountingProperty   NUMBER(9),
   Hours                NUMBER(15,6),
   BWorkHour            NUMBER(9,2),
   Price                NUMBER(19,4),
   WorkingFee           NUMBER(19,4),
   WorkingFeeDiscount   NUMBER(19,4),
   Repairman            VARCHAR2(100),
   RepairReason         VARCHAR2(200),
   ClaimStatus          NUMBER(9),
   Remark               VARCHAR2(200),
   constraint PK_DTMREPAIRCONTRACTITEM primary key (DTMRepairContractItemObjid)
)
/

/*==============================================================*/
/* Index: FK_RepCon_RepConItem_FK                               */
/*==============================================================*/
create index FK_RepCon_RepConItem_FK on DTMRepairContractItem (
   DTMObjid ASC
)
/

/*==============================================================*/
/* Table: DTMRepairContractMaterial                             */
/*==============================================================*/
create table DTMRepairContractMaterial  (
   DTMRepairContractMaterialObjid VARCHAR2(40)                    not null,
   DTMRepairContractItemObjid VARCHAR2(40)                    not null,
   DTMRepairContractObjid VARCHAR2(40)                    not null,
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(100),
   AccountingProperty   NUMBER(9),
   MeasureUnitName      VARCHAR2(100),
   Amount               NUMBER(9),
   Price                NUMBER(19,4),
   GuidePrice           NUMBER(19,4),
   MaterialFee          NUMBER(19,4),
   MaterialFeeDiscount  NUMBER(19,4),
   ClaimStatus          NUMBER(9),
   Remark               VARCHAR2(200),
   OldSparePartCode     VARCHAR2(50),
   OldSparePartName     VARCHAR2(100),
   OldSparePartSupplierCode VARCHAR2(50),
   OldSparePartSupplierName VARCHAR2(100),
   UsedPartsBarCode     VARCHAR2(50),
   constraint PK_DTMREPAIRCONTRACTMATERIAL primary key (DTMRepairContractMaterialObjid)
)
/

/*==============================================================*/
/* Index: FKt_RepCItem_RepCMaterial_FK                          */
/*==============================================================*/
create index FKt_RepCItem_RepCMaterial_FK on DTMRepairContractMaterial (
   DTMRepairContractItemObjid ASC
)
/
