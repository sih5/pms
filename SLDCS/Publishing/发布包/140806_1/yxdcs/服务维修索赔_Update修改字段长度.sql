--维修单，维修单故障原因，维修单项目清单修改字段长度
Alter Table   RepairOrder Modify   CarActuality  VARCHAR2(600);
Alter Table   RepairOrder Modify   DealMethod  VARCHAR2(600);
Alter Table   RepairOrder Modify   CustomOpinion  VARCHAR2(600);
Alter Table   RepairOrder Modify   VehicleUse     VARCHAR2(600);
Alter Table   RepairOrder Modify     Remark VARCHAR2(600);
   
Alter Table   RepairOrderFaultReason  Modify   Amendments     VARCHAR2(600);
Alter Table   RepairOrderFaultReason  Modify     MalfunctionReason VARCHAR2(600);
Alter Table   RepairOrderFaultReason  Modify      Remark     VARCHAR2(600);
Alter Table   RepairOrderItemDetail  Modify Remark               VARCHAR2(600);
