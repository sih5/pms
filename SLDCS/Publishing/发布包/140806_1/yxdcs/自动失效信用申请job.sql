begin
  sys.dbms_scheduler.create_job(job_name            => 'DCS.自动失效信用申请',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'autoexpirecredit',
                                start_date          => to_date('07-08-2014 23:50:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;

