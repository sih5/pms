INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13147,13100, 2, 'AgentShippingDetailSta','Released', NULL, '代理库发运单明细', '查询代理库向服务站发运配件的到位及时性', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 24, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13148,13100, 2, 'AgentShippingSta','Released', NULL, '代理库发运清单', '查询代理库向服务站发运配件的到位及时性', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 25, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);



