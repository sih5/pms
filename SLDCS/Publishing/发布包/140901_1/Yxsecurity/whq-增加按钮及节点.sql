Insert INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6212,6200, 2, 'SAPYXPaymentInfo', 'Released', NULL, 'SAP收付款接口查询', '查询SAP接口收付款单', 'Client/DCS/Images/Menu/Financial/SAPYXPaymentInfo.png', 12, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6212, 'SAPYXPaymentInfo|SetStatus', '手工设置状态', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7405, 'DTPVehicleinformation|Export', '导出', 2);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
