--市场部与人员关系增加 类型字段
Alter Table MarketDptPersonnelRelation Add   Type   NUMBER(9);

-- 配件调拨单增加字段原始订单ID、原始订单编号
Alter Table PartsTransferOrder Add  OriginalBillId       NUMBER(9);
Alter Table PartsTransferOrder Add   OriginalBillCode     VARCHAR2(50);
  
