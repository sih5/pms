create or replace view SAPINVOICEINFO 
AS
SELECT  a.id,
        BUKRS         as CompanyCode,
        2             as BusinessType,
        code,
        InvoiceNumber,
        InvoiceAmount,
        a.invoicetax,
        CreateTime,
        Status,
        Message
   from SAP_YX_SALESBILLINFO a
 union
 SELECT 
        b.id,
        BUKRS         as CompanyCode,
        1             as BusinessType,
        code,
        InvoiceNumber,
        InvoiceAmount,
        b.invoicetax,
        CreateTime,
        Status,
        Message
   from SAP_YX_INVOICEVERACCOUNTINFO b
