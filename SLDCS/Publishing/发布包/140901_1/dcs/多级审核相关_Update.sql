--ɾ���༶������BO
declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AuditHierarchySetting');
  if num>0 then
    execute immediate 'drop table AuditHierarchySetting cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AuditRoleAffiPerconnel');
  if num>0 then
    execute immediate 'drop table AuditRoleAffiPerconnel cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('BusinessObject');
  if num>0 then
    execute immediate 'drop table BusinessObject cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ClaimTemplate');
  if num>0 then
    execute immediate 'drop table ClaimTemplate cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ClaimTemplateItemDetail');
  if num>0 then
    execute immediate 'drop table ClaimTemplateItemDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ClaimTemplateMaterialDetail');
  if num>0 then
    execute immediate 'drop table ClaimTemplateMaterialDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('MultiLevelAuditConfig');
  if num>0 then
    execute immediate 'drop table MultiLevelAuditConfig cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('MultiLevelAuditConfigDetail');
  if num>0 then
    execute immediate 'drop table MultiLevelAuditConfigDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PostAuditStatus');
  if num>0 then
    execute immediate 'drop table PostAuditStatus cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RegionConfuguration');
  if num>0 then
    execute immediate 'drop table RegionConfuguration cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RegionDealerList');
  if num>0 then
    execute immediate 'drop table RegionDealerList cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RegionalRoleConfig');
  if num>0 then
    execute immediate 'drop table RegionalRoleConfig cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AuditHierarchySetting');
  if num>0 then
    execute immediate 'drop sequence S_AuditHierarchySetting';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AuditRoleAffiPerconnel');
  if num>0 then
    execute immediate 'drop sequence S_AuditRoleAffiPerconnel';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_BusinessObject');
  if num>0 then
    execute immediate 'drop sequence S_BusinessObject';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ClaimTemplate');
  if num>0 then
    execute immediate 'drop sequence S_ClaimTemplate';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ClaimTemplateItemDetail');
  if num>0 then
    execute immediate 'drop sequence S_ClaimTemplateItemDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ClaimTemplateMaterialDetail');
  if num>0 then
    execute immediate 'drop sequence S_ClaimTemplateMaterialDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_MultiLevelAuditConfig');
  if num>0 then
    execute immediate 'drop sequence S_MultiLevelAuditConfig';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_MultiLevelAuditConfigDetail');
  if num>0 then
    execute immediate 'drop sequence S_MultiLevelAuditConfigDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PostAuditStatus');
  if num>0 then
    execute immediate 'drop sequence S_PostAuditStatus';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RegionDealerList');
  if num>0 then
    execute immediate 'drop sequence S_RegionDealerList';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RegionalRoleConfig');
  if num>0 then
    execute immediate 'drop sequence S_RegionalRoleConfig';
  end if;
end;
/
