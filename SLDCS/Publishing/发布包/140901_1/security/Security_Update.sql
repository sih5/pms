--新增登陆日志和授权履历BO
create sequence S_LoginLog
/

create sequence S_RuleRecord
/

/*==============================================================*/
/* Table: LoginLog                                              */
/*==============================================================*/
create table LoginLog  (
   Id                   NUMBER(9)                       not null,
   PersonnelId          NUMBER(9)                       not null,
   IP                   VARCHAR2(50),
   Mac                  VARCHAR2(50),
   LoginTime            DATE                            not null,
   LogoutTime           DATE,
   constraint PK_LOGINLOG primary key (Id)
)
/
/*==============================================================*/
/* Table: RuleRecord                                            */
/*==============================================================*/
create table RuleRecord  (
   Id                   NUMBER(9)                       not null,
   RoleId               NUMBER(9)                       not null,
   NodeId               NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   constraint PK_RULERECORD primary key (Id)
)
/
