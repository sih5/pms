--更新字典项

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMRepairContract_RepairType', 'DTM维修类型', 0, '普通维修', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMRepairContract_RepairType', 'DTM维修类型', 1, '事故维修', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMRepairContract_RepairType', 'DTM维修类型', 2, '车辆召回', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMRepairContract_RepairType', 'DTM维修类型', 3, '服务活动', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMRepairContract_RepairType', 'DTM维修类型', 4, '商品车维修', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMRepairContract_RepairType', 'DTM维修类型', 5, '物流损失维修', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMRepairContract_RepairType', 'DTM维修类型', 6, '海运损失维修', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMRepairContract_RepairType', 'DTM维修类型', 7, '精品件加装', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMRepairContract_RepairType', 'DTM维修类型', 8, '店外商品车维修', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMService_ClaimStatus', 'DTM维修索赔状态', 0, '无索赔', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMService_ClaimStatus', 'DTM维修索赔状态', 1, '产生索赔', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMService_ClaimStatus', 'DTM维修索赔状态', 2, '部分生成索赔单', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMService_ClaimStatus', 'DTM维修索赔状态', 3, '全部生成索赔单', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMRepairContract_MaintenanceStatus', 'DTM免费保养状态', 0, '无免费保养', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMRepairContract_MaintenanceStatus', 'DTM免费保养状态', 1, '产生免费保养', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMRepairContract_MaintenanceStatus', 'DTM免费保养状态', 2, '已生成索赔单', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMService_AccountingProperty', 'DTM维修结算属性', 0, '索赔', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMService_AccountingProperty', 'DTM维修结算属性', 1, '免费保养', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMService_AccountingProperty', 'DTM维修结算属性', 2, '免费维修', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMService_AccountingProperty', 'DTM维修结算属性', 3, '自费保养', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMService_AccountingProperty', 'DTM维修结算属性', 4, '自费维修', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMService_AccountingProperty', 'DTM维修结算属性', 5, '返修', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMService_AccountingProperty', 'DTM维修结算属性', 6, '技术服务', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DTMService_AccountingProperty', 'DTM维修结算属性', 7, '保险理赔', 1, 1);
