--新增自动编码规则

Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'WMSLabelPrintingSerial', 'WMSLabelPrintingSerial', 1, '{SERIAL:000000}', 1, NULL);

--更新仓库成本变更单编码规则

Update codetemplate a Set a.template='WCC{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}'  Where entityname='WarehouseCostChangeBill';
