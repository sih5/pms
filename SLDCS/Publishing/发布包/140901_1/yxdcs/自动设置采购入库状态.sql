create or replace procedure autosetpurchaseorderinstatus as
IsAllNew number(9);
IsAllFinish  number(9);
begin

  declare
    cursor AutoSet is(
      select id
        from partspurchaseorder where status in(3,4,5,6,7)and InStatus in(5,10)
      ) ;

  begin
    for S_AutoSet in AutoSet
    loop
      begin
        select count(*) into isallnew from suppliershippingorder a
        inner join partsinboundplan b
        on a.code=b.sourcecode
        inner join partsinboundplandetail c on b.id =c.partsinboundplanid
        where a.PartsPurchaseOrderId=S_AutoSet.id and nvl(c.inspectedquantity,0)>0;
        if isallnew=0 then
          update partspurchaseorder set InStatus=5 where id=S_AutoSet.id;
          end if;
        select count(*) into IsAllFinish from suppliershippingorder a
        inner join partsinboundplan b
        on a.code=b.sourcecode
        inner join partsinboundplandetail c on b.id =c.partsinboundplanid
        where a.PartsPurchaseOrderId=S_AutoSet.id and nvl(c.inspectedquantity,0)<>nvl(c.plannedamount,0);
        if IsAllFinish=0 then
          update partspurchaseorder set InStatus=15 where id=S_AutoSet.id;
          end if;
        if  (isallnew>0)and  (IsAllFinish>0) then
          update partspurchaseorder set InStatus=10 where id=S_AutoSet.id;
          end if;
      end;
    end loop;
  end;
  end autosetpurchaseorderinstatus;
