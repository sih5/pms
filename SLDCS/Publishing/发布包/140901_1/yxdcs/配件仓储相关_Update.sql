--����boWMS��ǩ��ӡ
create sequence S_WMSLabelPrintingSerial
/
/*==============================================================*/
/* Table: WMSLabelPrintingSerial                                */
/*==============================================================*/
create table WMSLabelPrintingSerial  (
   Id                   NUMBER(9)                       not null,
   PartId               NUMBER(9),
   PartCode             VARCHAR2(50),
   PartName             VARCHAR2(100),
   Serial               VARCHAR2(50),
   constraint PK_WMSLABELPRINTINGSERIAL primary key (Id)
)
/
