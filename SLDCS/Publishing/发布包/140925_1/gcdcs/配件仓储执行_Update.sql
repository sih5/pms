
--配件出库单增加字段 出库包装计划号、 合同号、GPMS采购计划单号
--配件出库计划/ 配件入库计划/ 配件入库检验单/配件调拨单增加字段GPMS采购计划单号
  
Alter Table  PartsInboundCheckBill  Add    GPMSPurOrderCode     VARCHAR2(50);

Alter Table  PartsInboundPlan  Add    GPMSPurOrderCode     VARCHAR2(50);

Alter Table  PartsOutboundBill  Add    GPMSPurOrderCode     VARCHAR2(50);
Alter Table  PartsOutboundBill  Add    ContractCode     VARCHAR2(50);
Alter Table  PartsOutboundBill  Add    OutboundPackPlanCode     VARCHAR2(50);

Alter Table  PartsOutboundPlan  Add    GPMSPurOrderCode     VARCHAR2(50);

Alter Table  PartsTransferOrder  Add    GPMSPurOrderCode     VARCHAR2(50);
