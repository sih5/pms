--创建车型清单bo
create sequence S_VehicleMainteProductCategory
/

/*==============================================================*/
/* Table: VehicleMainteProductCategory                          */
/*==============================================================*/
create table VehicleMainteProductCategory  (
   id                   NUMBER(9)                       not null,
   VehicleMainteTermId  NUMBER(9)                       not null,
   ProductCategoryName  VARCHAR2(200),
   MaterialCost         NUMBER(19,4),
   constraint PK_VEHICLEMAINTEPRODUCTCATEGOR primary key (id)
)
/
