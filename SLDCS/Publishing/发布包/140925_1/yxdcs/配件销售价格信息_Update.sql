--创建BO  DMS品牌映射/配件销售加价率
    
create sequence S_PMSDMSBRANDMAPPING
/

create sequence S_PartsSalePriceIncreaseRate
/

/*==============================================================*/
/* Table: PMSDMSBRANDMAPPING                                    */
/*==============================================================*/
create table PMSDMSBRANDMAPPING  (
   Id                   NUMBER(9)                       not null,
   PMSBrandId           NUMBER(9),
   PMSBrandCode         VARCHAR2(50),
   PMSBrandName         VARCHAR2(50),
   DMSBrandId           VARCHAR2(50)                    not null,
   DMSBrandCode         VARCHAR2(50)                    not null,
   DMSBrandName         VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(50),
   Createtime           DATE,
   ModifyId             NUMBER(9),
   ModifyName           VARCHAR2(50),
   ModifyTime           DATE,
   Syscode              NUMBER(9)                       not null,
   constraint PK_PMSDMSBRANDMAPPING primary key (Id)
)
/
/*==============================================================*/
/* Table: PartsSalePriceIncreaseRate                            */
/*==============================================================*/
create table PartsSalePriceIncreaseRate  (
   Id                   NUMBER(9)                       not null,
   GroupCode            VARCHAR2(50),
   GroupName            VARCHAR2(100),
   IncreaseRate         NUMBER(15,6),
   IsDefault            NUMBER(1),
   status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   CreateTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   constraint PK_PARTSSALEPRICEINCREASERATE primary key (Id)
)
/

