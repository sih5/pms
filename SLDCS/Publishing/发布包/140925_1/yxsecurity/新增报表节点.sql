--新增报表节点
--Select * From page Where parentId=13200;

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13212,13200, 2, 'UsedPartsInboundSta', 'Released', NULL, '旧件接收入库查询', '汇总旧件仓库某一时间段接收入库的旧件明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 18, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13213,13200, 2, 'UsedPartsReturnSta', 'Released', NULL, '旧件清退出库查询', '汇总某一个时间段旧件仓库清退出库旧件明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 19, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13214,13200, 2, 'UsedPartsRecoverSta', 'Released', NULL, '旧件回收统计表', '汇总各个分公司各市场部所属服务站旧件返回统计表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 20, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13215,13200, 2, 'UsedPartsDealerSta', 'Released', NULL, '旧件服务站回收情况统计表', '汇总各个品牌各个服务站某个时间段旧件回收情况统计表', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 21, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
