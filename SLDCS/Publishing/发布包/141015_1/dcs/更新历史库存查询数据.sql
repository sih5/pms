--更新历史库存查询数据 整表更新
update PartsHistoryStock t1
   set t1.partssalescategoryid = (select t2.partssalescategoryid
                                    from SalesUnit t2
                                   where exists (select 1
                                            from SalesUnitAffiWarehouse t3
                                           where t2.id = t3.salesunitid
                                             and t3.warehouseid =
                                                 t1.warehouseid))