--服务站索赔结算指令添加字段结算方式
Alter Table SsClaimSettleInstruction Add    SettleMethods        NUMBER(9);

--服务站索赔结算单添加字段结算类型，结算字段，关联单据ID
Alter Table SsClaimSettlementBill Add  SettleType           NUMBER(9);
Alter Table SsClaimSettlementBill Add   SettleMethods        NUMBER(9);
Alter Table SsClaimSettlementBill Add     RelatedId           NUMBER(9);
