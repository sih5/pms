--增加字典项配件采购结算单状态

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsPurchaseSettle_Status', '配件采购结算单状态', 5, '发票驳回', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsPurchaseSettle_Status', '配件采购结算单状态', 6, '发票已审核', 1, 1);

--增加字典项服务站索赔结算方式
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DealerSettleMethods', '服务站索赔结算方式', 1, '合并结算', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DealerSettleMethods', '服务站索赔结算方式', 2, '拆分结算', 1, 1);

--增加字典项服务站索赔结算类型
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DealerSettleType', '服务站索赔结算类型', 1, '材料费', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DealerSettleType', '服务站索赔结算类型', 2, '工时费', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'DealerSettleType', '服务站索赔结算类型', 3, '合并费用', 1, 1);
