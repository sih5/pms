create or replace function GenerateSsClaimSettlementBill(FBranchId in integer, FDealerId in integer, FStartTime in date, FEndTime in date,
  FCreatorId in integer, FCreatorName in varchar2,FPartsSalesCategoryId in integer,FServiceProductLineId in integer,FProductLineType in integer, FSettleMethods in integer) return integer
as
  GeneratedBillQuantity integer;
  /*营销分公司编号*/
  FBranchCode VARCHAR2(50);
  /*营销分公司名称*/
  FBranchName VARCHAR2(100);
  /*编号模板Id*/
  FComplateId integer;
  /*编码规则相关设置*/
  FDateFormat date;
  FDateStr varchar2(8);
  FSerialLength integer;
  FNewCode varchar2(50);
  FSsClaimSettlementBillId integer;
  FBILLFORCLAIMSETTLEMENTiD integer;
begin
  /*编码规则变更，需同步调整*/
  --FDateFormat:=trunc(sysdate,'YEAR');
  --FDateFormat:=trunc(sysdate,'MONTH');
  FDateFormat:=trunc(sysdate,'DD');
  FDateStr:=to_char(sysdate,'yyyymmdd');
  FSerialLength:=6;
  FNewCode:='SCS{CORPCODE}'||FDateStr||'{SERIAL}';

  /*获取编号生成模板Id*/
  begin
    select Id
      into FComplateId
      from codetemplate
     where name = 'SsClaimSettlementBill'
       and IsActived = 1;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20001, '未找到名称为"SsClaimSettlementBill"的有效编码规则。');
    when others then
      raise;
  end;

  select Code,Name into FBranchCode, FBranchName from Branch where id = FBranchId;
  GeneratedBillQuantity := 0;

  /*维修保养索赔单*/
  insert into BillForClaimSettlement
    (SourceId,
     SourceCode,
     SourceType,
     DealerId,
     BranchId,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     Partssalescategoryid,
     ServiceProductLineId,
     ProductLineType)
    select Id,
           ClaimBillCode,
           decode(ClaimType, 1, 1, 2, 2, 3, 3,10,1, 0) SourceType, --维修保养索赔单索赔类型、服务站索赔结算源单据类型，可能存在差异
           DealerId,
           BranchId,
           LaborCost,
           MaterialCost,
           NVL(PartsManagementCost, 0) PartsManagementCost,
           0 FieldServiceExpense,
           0 DebitAmount,
           0 ComplementAmount,
           NVL(OtherCost, 0) OtherCost,
           0 PreSaleAmount,
           Partssalescategoryid,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  ServiceProductLineId) ServiceProductLineId,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From dcs.serviceproductlineview v
                    Where v.ProductLineId =
                          RepairClaimBill.ServiceProductLineId)) ProductLineType
      from RepairClaimBill
     where CreateTime <= FEndTime
       and (FStartTime is null or CreateTime >= FStartTime)
       and SettlementStatus = 2
       and BranchId = FBranchId
       and (FDealerId is null or DealerId = FDealerId)
       and status = 4
       and (FPartsSalesCategoryId = 0 or
           PartsSalesCategoryId = FPartsSalesCategoryId)
            /* and (FProductLineType is null or ProductLineType=FProductLineType)*/
       and (FServiceProductLineId is null or FServiceProductLineId = 0 or
           ServiceProductLineId = FServiceProductLineId);
/*
   update RepairClaimBill
      set SettlementStatus = 3
    where RepairClaimBill.Id in
          (select SourceId
             from BillForClaimSettlement
            where SourceType in (1, 2, 3));*/

   /*配件索赔单*/
  insert into BillForClaimSettlement
    (SourceId,
     SourceCode,
     SourceType,
     DealerId,
     BranchId,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     Partssalescategoryid,
     ServiceProductLineId,
     ProductLineType)
    select Id SourceId,
           Code SourceCode,
           4 SourceType,
           DealerId,
           BranchId,
           LaborCost,
           MaterialCost,
           NVL(PartsManagementCost, 0),
           0 FieldServiceExpense,
           0 DebitAmount,
           0 ComplementAmount,
           NVL(OtherCost, 0) OtherCost,
           0 PreSaleAmount,
           Partssalescategoryid,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  ServiceProductLineId) ServiceProductLineId,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From dcs.serviceproductlineview v
                    Where v.ProductLineId =
                          PartsClaimOrder.ServiceProductLineId)) ProductLineType
      from PartsClaimOrder
     where CreateTime <= FEndTime
       and (FStartTime is null or CreateTime >= FStartTime)
       and SettlementStatus = 2
       and BranchId = FBranchId
       and (FDealerId is null or DealerId = FDealerId)
       and status = 3
       and (FPartsSalesCategoryId = 0 or
           PartsSalesCategoryId = FPartsSalesCategoryId)
           /*and (FProductLineType is null or ProductLineType=FProductLineType)*/
       and (FServiceProductLineId is null or FServiceProductLineId = 0 or
           ServiceProductLineId = FServiceProductLineId) ;
/*
   update PartsClaimOrder
      set SettlementStatus = 3
    where PartsClaimOrder.Id in
          (select SourceId from BillForClaimSettlement where SourceType = 4);*/

  /*外出服务索赔单*/
  insert into BillForClaimSettlement
    (SourceId,
     SourceCode,
     SourceType,
     DealerId,
     BranchId,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     Partssalescategoryid,
     ServiceProductLineId,
     ProductLineType)
    select Id SourceId,
           Code SourceCode,
           5 SourceType,
           DealerId,
           BranchId,
           0 LaborCost,
           0 MaterialCost,
           0 PartsManagementCost,
           NVL(TowCharge, 0) + NVL(FieldServiceCharge, 0) FieldServiceExpense,
           0 DebitAmount,
           0 ComplementAmount,
           NVL(OtherCost, 0) OtherCost,
           0 PreSaleAmount,
           Partssalescategoryid,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  ServiceProductLineId) ServiceProductLineId,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From dcs.serviceproductlineview v
                    Where v.ProductLineId =
                          ServiceTripClaimBill.ServiceProductLineId)) ProductLineType
      from ServiceTripClaimBill
     where CreateTime <= FEndTime
       and (FStartTime is null or CreateTime >= FStartTime)
       and SettlementStatus = 2
       and BranchId = FBranchId
       and (FDealerId is null or DealerId = FDealerId)
       and status = 6
       and (FPartsSalesCategoryId = 0 or
           PartsSalesCategoryId = FPartsSalesCategoryId)
          /*and (FProductLineType is null or ProductLineType=FProductLineType)*/
       and (FServiceProductLineId is null or FServiceProductLineId = 0 or
           ServiceProductLineId = FServiceProductLineId);
/*
   update ServiceTripClaimBill
      set SettlementStatus = 3
    where ServiceTripClaimBill.Id in
          (select SourceId from BillForClaimSettlement where SourceType = 5);*/

  /*扣补款单*/
  insert into BillForClaimSettlement
    (SourceId,
     SourceCode,
     SourceType,
     DealerId,
     BranchId,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     Partssalescategoryid,
     ServiceProductLineId,
     ProductLineType,DEBITTYPE )
    select Id SourceId,
           Code SourceCode,
           6 SourceType,
           DealerId,
           BranchId,
           0 LaborCost,
           0 MaterialCost,
           0 PartsManagementCost,
           0 FieldServiceExpense,
           decode(DebitOrReplenish, 1, -TransactionAmount, 0) DebitAmount,
           decode(DebitOrReplenish, 2, TransactionAmount, 0) ComplementAmount,
           0 OtherCost,
           0 PreSaleAmount,
           Partssalescategoryid,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  ServiceProductLineId) ServiceProductLineId,
           decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From dcs.serviceproductlineview v
                    Where v.ProductLineId =
                          ExpenseAdjustmentBill.ServiceProductLineId)) ProductLineType,ExpenseAdjustmentBill.TransactionCategory
      from ExpenseAdjustmentBill
     where CreateTime <= FEndTime
       and (FStartTime is null or CreateTime >= FStartTime)
       and SettlementStatus = 2
       and BranchId = FBranchId
       and (FDealerId is null or DealerId = FDealerId)
       and status = 2
       and (FPartsSalesCategoryId = 0 or
           PartsSalesCategoryId = FPartsSalesCategoryId)
           /*and (FProductLineType is null or ProductLineType=FProductLineType)*/
       and (FServiceProductLineId is null or FServiceProductLineId = 0 or
           ServiceProductLineId = FServiceProductLineId);
/*
   update ExpenseAdjustmentBill
      set SettlementStatus = 3
    where ExpenseAdjustmentBill.Id in
          (select SourceId from BillForClaimSettlement where SourceType = 6);*/

 /*售前检查单*/
 insert into BillForClaimSettlement
   (SourceId,
    SourceCode,
    SourceType,
    DealerId,
    BranchId,
    LaborCost,
    MaterialCost,
    PartsManagementCost,
    FieldServiceExpense,
    DebitAmount,
    ComplementAmount,
    OtherCost,
    PreSaleAmount,
    Partssalescategoryid,
    ServiceProductLineId,
    ProductLineType)
   select Id SourceId,
          Code SourceCode,
          7 SourceType,
          DealerId,
          BranchId,
          0 LaborCost, --工时费
          0 MaterialCost, --材料费
          0 PartsManagementCost, --配件管理费
          0 FieldServiceExpense, --外出费用
          0 DebitAmount, --扣款金额
          0 ComplementAmount, --补款金额
          0 OtherCost, --其他费用
          nvl(Cost, 0) PreSaleAmount, --售前检查费用
          Partssalescategoryid, --品牌Id
          decode(FServiceProductLineId,
                 null,
                 null,
                 0,
                 null,
                 ServiceProductLineId),
         decode(FServiceProductLineId,
                  null,
                  null,
                  0,
                  null,
                  (Select v.ProductLineType
                     From dcs.serviceproductlineview v
                    Where v.ProductLineId =
                          PreSaleCheckOrder.ServiceProductLineId)) ProductLineType
     from PreSaleCheckOrder
    where CreateTime <= FEndTime
      and (FStartTime is null or CreateTime >= FStartTime)
      and SettleStatus = 2
      and BranchId = FBranchId
      and (FDealerId is null or DealerId = FDealerId)
      and status = 3
      and (FPartsSalesCategoryId = 0 or
          PartsSalesCategoryId = FPartsSalesCategoryId)
      and (FServiceProductLineId is null or FServiceProductLineId = 0 or
          ServiceProductLineId = FServiceProductLineId);
/*
 update PreSaleCheckOrder
    set SettleStatus = 3
  where PreSaleCheckOrder.Id in
        (select SourceId from BillForClaimSettlement where SourceType = 7);
*/
FBillForClaimSettlementId:=s_BillForClaimSettlement.Nextval; 
 if FSettleMethods=1 then begin
declare
  cursor Cursor_role is(

     select
          0 IfInvoiced,
          1 Status,
          regexp_replace(
            regexp_replace(FNewCode, '{CORPCODE}', Dealer.Code),
            '{SERIAL}',
            lpad(to_char(GetSerial(FComplateId, FDateFormat, Dealer.Code)),FSerialLength,'0')
          ) Code,
          DealerId,
          Dealer.Code DealerCode,
          Dealer.Name DealerName,
          FBranchId BranchId,
          FBranchCode BranchCode,
          FBranchName BranchName,
          FStartTime SettlementStartTime,
          FEndTime SettlementEndTime,
          LaborCost,
          MaterialCost,
          PartsManagementCost,
          FieldServiceExpense,
          DebitAmount,
          ComplementAmount,
          OtherCost,
          PreSaleAmount,
          LaborCost + MaterialCost + PartsManagementCost + FieldServiceExpense + DebitAmount + ComplementAmount +OtherCost+PreSaleAmount TotalAmount,
          FCreatorId CreatorId,
          FCreatorName CreatorName,
          sysdate CreateTime,
          PARTSSALESCATEGORYID,
          ServiceProductLineId ,
          ProductLineType
     from (select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  sum(MaterialCost) MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(PreSaleAmount) PreSaleAmount
             from BillForClaimSettlement
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join Dealer on Dealer.Id = t_date.DealerId
       );
begin
  for S_role in Cursor_role loop
   
    FSsClaimSettlementBillId:=S_SsClaimSettlementBill.Nextval;
    /*主单生成*/
  if s_role.TotalAmount>=0 then
    begin
    update RepairClaimBill
       set SettlementStatus = 3
     where RepairClaimBill.Id in
           (select SourceId
              from BillForClaimSettlement
             where SourceType in (1, 2, 3)
               and BillForClaimSettlement.Dealerid = s_role.DealerId
               and BranchId = s_role.BranchId
               and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                   PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
               and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                   ServiceProductLineId = s_role.ServiceProductLineId));
   
   update ServiceTripClaimBill
      set SettlementStatus = 3
    where ServiceTripClaimBill.Id in
          (select SourceId
             from BillForClaimSettlement
            where SourceType = 5
              and BillForClaimSettlement.Dealerid = s_role.DealerId
              and BranchId = s_role.BranchId
              and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                  PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
              and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                  ServiceProductLineId = s_role.ServiceProductLineId));


 update PreSaleCheckOrder
    set SettleStatus = 3
  where PreSaleCheckOrder.Id in
        (select SourceId
           from BillForClaimSettlement
          where SourceType = 7
            and BillForClaimSettlement.Dealerid = s_role.DealerId
            and BranchId = s_role.BranchId
            and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
            and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                ServiceProductLineId = s_role.ServiceProductLineId));
                
   update ExpenseAdjustmentBill
      set SettlementStatus = 3
    where ExpenseAdjustmentBill.Id in
          (select SourceId
             from BillForClaimSettlement
            where SourceType = 6
              and BillForClaimSettlement.Dealerid = s_role.DealerId
              and BranchId = s_role.BranchId
              and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                  PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
              and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                  ServiceProductLineId = s_role.ServiceProductLineId));

  insert into SsClaimSettlementBill
    (Id,
     IfInvoiced,
     Status,
     Code,
     DealerId,
     DealerCode,
     DealerName,
     BranchId,
     BranchCode,
     BranchName,
     SettlementStartTime,
     SettlementEndTime,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     CreatorId,
     CreatorName,
     CreateTime,
     PartsSalesCategoryId,
     ServiceProductLineId,
     ProductLineType,SettleType,RELATEDID,SettleMethods)
     values(FSsClaimSettlementBillId,
     s_role.IfInvoiced,
     s_role.Status,
     s_role.Code,
     s_role.DealerId,
     s_role.DealerCode,
     s_role.DealerName,
     s_role.BranchId,
     s_role.BranchCode,
     s_role.BranchName,
     s_role.SettlementStartTime,
     s_role.SettlementEndTime,
     s_role.LaborCost,
     s_role.MaterialCost,
     s_role.PartsManagementCost,
     s_role.FieldServiceExpense,
     s_role.DebitAmount,
     s_role.ComplementAmount,
     s_role.OtherCost,
     s_role.PreSaleAmount,
     s_role.TotalAmount,
     s_role.CreatorId,
     s_role.CreatorName,
     s_role.CreateTime,
     s_role.PartsSalesCategoryId,
     s_role.ServiceProductLineId,
     s_role.ProductLineType,3,FBillForClaimSettlementId,1);

   /*清单生成*/
      insert into SsClaimSettlementDetail
    (Id,
     SsClaimSettlementBillId,
     SourceId,
     SourceCode,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     SourceType)
  select S_SsClaimSettlementDetail.NEXTVAL,
         FSsClaimSettlementBillId,
         BillForClaimSettlement.SourceId,
         BillForClaimSettlement.SourceCode,
         BillForClaimSettlement.LaborCost,
         BillForClaimSettlement.MaterialCost,
         BillForClaimSettlement.PartsManagementCost,
         BillForClaimSettlement.FieldServiceExpense,
         BillForClaimSettlement.DebitAmount,
         BillForClaimSettlement.ComplementAmount,
         BillForClaimSettlement.OtherCost,
         BillForClaimSettlement.PreSaleAmount,
         BillForClaimSettlement.LaborCost + BillForClaimSettlement.MaterialCost + BillForClaimSettlement.PartsManagementCost
         + BillForClaimSettlement.FieldServiceExpense + BillForClaimSettlement.DebitAmount + BillForClaimSettlement.ComplementAmount
         + BillForClaimSettlement.OtherCost+BillForClaimSettlement.PreSaleAmount,
         SourceType
    from BillForClaimSettlement
   inner join SsClaimSettlementBill on BillForClaimSettlement.DealerId = SsClaimSettlementBill.DealerId
                                   and BillForClaimSettlement.BranchId = SsClaimSettlementBill.BranchId
                                   And BillForClaimSettlement.PARTSSALESCATEGORYID = SsClaimSettlementBill.PARTSSALESCATEGORYID
                                   And (BillForClaimSettlement.ServiceProductLineId = SsClaimSettlementBill.ServiceProductLineId or BillForClaimSettlement.ServiceProductLineId  =0 or
                                   BillForClaimSettlement.ServiceProductLineId is null)
                                   And (BillForClaimSettlement.ProductLineType = SsClaimSettlementBill.ProductLineType or BillForClaimSettlement.ProductLineType is null
                                   or BillForClaimSettlement.ProductLineType=0)
   where not exists (select * from SsClaimSettlementDetail where SsClaimSettlementDetail.Ssclaimsettlementbillid = SsClaimSettlementBill.Id) AND SsClaimSettlementBill.ID=FSsClaimSettlementBillId;



      end;
  /* raise_application_error(-20001, '存在结算汇总单总金额为负数的单据'); */
   end if;

  end loop;
end;

end;
 else begin

declare
  cursor Cursor_role is(
     --材料费
     select
          0 IfInvoiced,
          1 Status,
          regexp_replace(
            regexp_replace(FNewCode, '{CORPCODE}', Dealer.Code),
            '{SERIAL}',
            lpad(to_char(GetSerial(FComplateId, FDateFormat, Dealer.Code)),FSerialLength,'0')
          ) Code,
          DealerId,
          Dealer.Code DealerCode,
          Dealer.Name DealerName,
          FBranchId BranchId,
          FBranchCode BranchCode,
          FBranchName BranchName,
          FStartTime SettlementStartTime,
          FEndTime SettlementEndTime,
          LaborCost,
          MaterialCost,
          PartsManagementCost,
          FieldServiceExpense,
          DebitAmount,
          ComplementAmount,
          OtherCost,
          PreSaleAmount,
          LaborCost + MaterialCost + PartsManagementCost + FieldServiceExpense + DebitAmount + ComplementAmount +OtherCost+PreSaleAmount TotalAmount,
          FCreatorId CreatorId,
          FCreatorName CreatorName,
          sysdate CreateTime,
          PARTSSALESCATEGORYID,
          ServiceProductLineId ,
          ProductLineType
     from (select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  0 LaborCost,
                  sum(MaterialCost) MaterialCost,
                  0 PartsManagementCost,
                  0 FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  0 ComplementAmount,
                  0 OtherCost,
                  0 PreSaleAmount
             from BillForClaimSettlement where( BillForClaimSettlement.Sourcetype in (1,2,3)) or (BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype in(2,16))
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join Dealer on Dealer.Id = t_date.DealerId
    where  not exists(select 1 from (
               select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  0 MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(PreSaleAmount) PreSaleAmount
             from BillForClaimSettlement where (BillForClaimSettlement.Sourcetype in (1,2,3,4,5,7)) or(BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype not in(2,16))
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t
            where  t_date.DealerId = t.DealerId
       and t_date.BranchId = t.BranchId
       And t_date.PARTSSALESCATEGORYID =
           t.PARTSSALESCATEGORYID
       And (t_date.ServiceProductLineId =
           t.ServiceProductLineId or
           t_date.ServiceProductLineId = 0 or
           t_date.ServiceProductLineId is null and
           t.LaborCost  + t.PartsManagementCost + t.FieldServiceExpense +t. DebitAmount +t. ComplementAmount +t.OtherCost+PreSaleAmount<0)
    )
       );
begin
  for S_role in Cursor_role loop
   
    FSsClaimSettlementBillId:=S_SsClaimSettlementBill.Nextval;
    /*主单生成*/
  if s_role.TotalAmount>=0 then
    begin
/*    update RepairClaimBill
       set SettlementStatus = 3
     where RepairClaimBill.Id in
           (select SourceId
              from BillForClaimSettlement
             where SourceType in (1, 2, 3)
               and BillForClaimSettlement.Dealerid = s_role.DealerId
               and BranchId = s_role.BranchId
               and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                   PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
               and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                   ServiceProductLineId = s_role.ServiceProductLineId));*/
   
   update ExpenseAdjustmentBill
      set SettlementStatus = 3
    where ExpenseAdjustmentBill.Id in
          (select SourceId
             from BillForClaimSettlement
            where SourceType = 6 and Debittype  in(2,16)
              and BillForClaimSettlement.Dealerid = s_role.DealerId
              and BranchId = s_role.BranchId
              and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                  PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
              and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                  ServiceProductLineId = s_role.ServiceProductLineId));

  insert into SsClaimSettlementBill
    (Id,
     IfInvoiced,
     Status,
     Code,
     DealerId,
     DealerCode,
     DealerName,
     BranchId,
     BranchCode,
     BranchName,
     SettlementStartTime,
     SettlementEndTime,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     CreatorId,
     CreatorName,
     CreateTime,
     PartsSalesCategoryId,
     ServiceProductLineId,
     ProductLineType,SettleType,RelatedId,SettleMethods)
     values(FSsClaimSettlementBillId,
     s_role.IfInvoiced,
     s_role.Status,
     s_role.Code,
     s_role.DealerId,
     s_role.DealerCode,
     s_role.DealerName,
     s_role.BranchId,
     s_role.BranchCode,
     s_role.BranchName,
     s_role.SettlementStartTime,
     s_role.SettlementEndTime,
     s_role.LaborCost,
     s_role.MaterialCost,
     s_role.PartsManagementCost,
     s_role.FieldServiceExpense,
     s_role.DebitAmount,
     s_role.ComplementAmount,
     s_role.OtherCost,
     s_role.PreSaleAmount,
     s_role.TotalAmount,
     s_role.CreatorId,
     s_role.CreatorName,
     s_role.CreateTime,
     s_role.PartsSalesCategoryId,
     s_role.ServiceProductLineId,
     s_role.ProductLineType,1,FBillForClaimSettlementId,2);

   /*清单生成*/
  insert into SsClaimSettlementDetail
    (Id,
     SsClaimSettlementBillId,
     SourceId,
     SourceCode,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     SourceType)
    select S_SsClaimSettlementDetail.NEXTVAL,
           FSsClaimSettlementBillId,
           BillForClaimSettlement.SourceId,
           BillForClaimSettlement.SourceCode,
           0,
           BillForClaimSettlement.MaterialCost,
           0,
           0,
           BillForClaimSettlement.DebitAmount,
           0,
           0,
           0,
           0 +
           BillForClaimSettlement.MaterialCost +
           0 +
           0 +
           BillForClaimSettlement.DebitAmount +
           0 +
           0 +
           0,
           SourceType
      from BillForClaimSettlement
     inner join SsClaimSettlementBill
        on BillForClaimSettlement.DealerId = SsClaimSettlementBill.DealerId
       and BillForClaimSettlement.BranchId = SsClaimSettlementBill.BranchId
       And BillForClaimSettlement.PARTSSALESCATEGORYID =
           SsClaimSettlementBill.PARTSSALESCATEGORYID
       And (BillForClaimSettlement.ServiceProductLineId =
           SsClaimSettlementBill.ServiceProductLineId or
           BillForClaimSettlement.ServiceProductLineId = 0 or
           BillForClaimSettlement.ServiceProductLineId is null)
       And (BillForClaimSettlement.ProductLineType =
           SsClaimSettlementBill.ProductLineType or
           BillForClaimSettlement.ProductLineType is null or
           BillForClaimSettlement.ProductLineType = 0)
     where not exists
     (select *
              from SsClaimSettlementDetail
             where SsClaimSettlementDetail.Ssclaimsettlementbillid =
                   SsClaimSettlementBill.Id)
       AND SsClaimSettlementBill.ID = FSsClaimSettlementBillId
      and ( ( BillForClaimSettlement.Sourcetype in (1,2,3)) or (BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype in(2,16)));


      end;
  /* raise_application_error(-20001, '存在结算汇总单总金额为负数的单据'); */
   end if;

  end loop;
end;   
 
declare
  cursor Cursor_role is(

     select
          0 IfInvoiced,
          1 Status,
          regexp_replace(
            regexp_replace(FNewCode, '{CORPCODE}', Dealer.Code),
            '{SERIAL}',
            lpad(to_char(GetSerial(FComplateId, FDateFormat, Dealer.Code)),FSerialLength,'0')
          ) Code,
          DealerId,
          Dealer.Code DealerCode,
          Dealer.Name DealerName,
          FBranchId BranchId,
          FBranchCode BranchCode,
          FBranchName BranchName,
          FStartTime SettlementStartTime,
          FEndTime SettlementEndTime,
          LaborCost,
          MaterialCost,
          PartsManagementCost,
          FieldServiceExpense,
          DebitAmount,
          ComplementAmount,
          OtherCost,
          PreSaleAmount,
          LaborCost + MaterialCost + PartsManagementCost + FieldServiceExpense + DebitAmount + ComplementAmount +OtherCost+PreSaleAmount TotalAmount,
          FCreatorId CreatorId,
          FCreatorName CreatorName,
          sysdate CreateTime,
          PARTSSALESCATEGORYID,
          ServiceProductLineId ,
          ProductLineType
    from (select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  0 MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(PreSaleAmount) PreSaleAmount
             from BillForClaimSettlement where (BillForClaimSettlement.Sourcetype in (1,2,3,4,5,7)) or(BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype not in(2,16))
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join Dealer on Dealer.Id = t_date.DealerId
        where  not exists(select 1 from (
              select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  0 LaborCost,
                  sum(MaterialCost) MaterialCost,
                  0 PartsManagementCost,
                  0 FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  0 ComplementAmount,
                  0 OtherCost,
                  0 PreSaleAmount
             from BillForClaimSettlement where( BillForClaimSettlement.Sourcetype in (1,2,3)) or (BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype in(2,16))
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t
            where  t_date.DealerId = t.DealerId
       and t_date.BranchId = t.BranchId
       And t_date.PARTSSALESCATEGORYID =
           t.PARTSSALESCATEGORYID
       And (t_date.ServiceProductLineId =
           t.ServiceProductLineId or
           t_date.ServiceProductLineId = 0 or
           t_date.ServiceProductLineId is null and
           t.MaterialCost  + t.DebitAmount <0)
    )
       );
begin
  for S_role in Cursor_role loop
   
    FSsClaimSettlementBillId:=S_SsClaimSettlementBill.Nextval;
    /*主单生成*/
  if s_role.TotalAmount>=0 then
    begin
    update RepairClaimBill
       set SettlementStatus = 3
     where RepairClaimBill.Id in
           (select SourceId
              from BillForClaimSettlement
             where SourceType in (1, 2, 3)
               and BillForClaimSettlement.Dealerid = s_role.DealerId
               and BranchId = s_role.BranchId
               and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                   PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
               and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                   ServiceProductLineId = s_role.ServiceProductLineId));
   
   update ServiceTripClaimBill
      set SettlementStatus = 3
    where ServiceTripClaimBill.Id in
          (select SourceId
             from BillForClaimSettlement
            where SourceType = 5
              and BillForClaimSettlement.Dealerid = s_role.DealerId
              and BranchId = s_role.BranchId
              and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                  PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
              and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                  ServiceProductLineId = s_role.ServiceProductLineId));


 update PreSaleCheckOrder
    set SettleStatus = 3
  where PreSaleCheckOrder.Id in
        (select SourceId
           from BillForClaimSettlement
          where SourceType = 7
            and BillForClaimSettlement.Dealerid = s_role.DealerId
            and BranchId = s_role.BranchId
            and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
            and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                ServiceProductLineId = s_role.ServiceProductLineId));
                
   update ExpenseAdjustmentBill
      set SettlementStatus = 3
    where ExpenseAdjustmentBill.Id in
          (select SourceId
             from BillForClaimSettlement
            where SourceType = 6 and Debittype not in(2,16)
              and BillForClaimSettlement.Dealerid = s_role.DealerId
              and BranchId = s_role.BranchId
              and (PARTSSALESCATEGORYID IS NULL OR PARTSSALESCATEGORYID = 0 OR
                  PARTSSALESCATEGORYID = s_role.PARTSSALESCATEGORYID)
              and (ServiceProductLineId is null or ServiceProductLineId = 0 or
                  ServiceProductLineId = s_role.ServiceProductLineId));

  insert into SsClaimSettlementBill
    (Id,
     IfInvoiced,
     Status,
     Code,
     DealerId,
     DealerCode,
     DealerName,
     BranchId,
     BranchCode,
     BranchName,
     SettlementStartTime,
     SettlementEndTime,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     CreatorId,
     CreatorName,
     CreateTime,
     PartsSalesCategoryId,
     ServiceProductLineId,
     ProductLineType,SettleType,RELATEDID,SettleMethods)
     values(FSsClaimSettlementBillId,
     s_role.IfInvoiced,
     s_role.Status,
     s_role.Code,
     s_role.DealerId,
     s_role.DealerCode,
     s_role.DealerName,
     s_role.BranchId,
     s_role.BranchCode,
     s_role.BranchName,
     s_role.SettlementStartTime,
     s_role.SettlementEndTime,
     s_role.LaborCost,
     s_role.MaterialCost,
     s_role.PartsManagementCost,
     s_role.FieldServiceExpense,
     s_role.DebitAmount,
     s_role.ComplementAmount,
     s_role.OtherCost,
     s_role.PreSaleAmount,
     s_role.TotalAmount,
     s_role.CreatorId,
     s_role.CreatorName,
     s_role.CreateTime,
     s_role.PartsSalesCategoryId,
     s_role.ServiceProductLineId,
     s_role.ProductLineType,2,FBillForClaimSettlementId,2);

   /*清单生成*/
  insert into SsClaimSettlementDetail
    (Id,
     SsClaimSettlementBillId,
     SourceId,
     SourceCode,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     SourceType)
    select S_SsClaimSettlementDetail.NEXTVAL,
           FSsClaimSettlementBillId,
           BillForClaimSettlement.SourceId,
           BillForClaimSettlement.SourceCode,
           BillForClaimSettlement.LaborCost,
           0,
           BillForClaimSettlement.PartsManagementCost,
           BillForClaimSettlement.FieldServiceExpense,
           BillForClaimSettlement.DebitAmount,
           BillForClaimSettlement.ComplementAmount,
           BillForClaimSettlement.OtherCost,
           BillForClaimSettlement.PreSaleAmount,
           BillForClaimSettlement.LaborCost +
           0 +
           BillForClaimSettlement.PartsManagementCost +
           BillForClaimSettlement.FieldServiceExpense +
           BillForClaimSettlement.DebitAmount +
           BillForClaimSettlement.ComplementAmount +
           BillForClaimSettlement.OtherCost +
           BillForClaimSettlement.PreSaleAmount,
           SourceType
      from BillForClaimSettlement
     inner join SsClaimSettlementBill
        on BillForClaimSettlement.DealerId = SsClaimSettlementBill.DealerId
       and BillForClaimSettlement.BranchId = SsClaimSettlementBill.BranchId
       And BillForClaimSettlement.PARTSSALESCATEGORYID =
           SsClaimSettlementBill.PARTSSALESCATEGORYID
       And (BillForClaimSettlement.ServiceProductLineId =
           SsClaimSettlementBill.ServiceProductLineId or
           BillForClaimSettlement.ServiceProductLineId = 0 or
           BillForClaimSettlement.ServiceProductLineId is null)
       And (BillForClaimSettlement.ProductLineType =
           SsClaimSettlementBill.ProductLineType or
           BillForClaimSettlement.ProductLineType is null or
           BillForClaimSettlement.ProductLineType = 0)
     where not exists
     (select *
              from SsClaimSettlementDetail
             where SsClaimSettlementDetail.Ssclaimsettlementbillid =
                   SsClaimSettlementBill.Id)
       AND SsClaimSettlementBill.ID = FSsClaimSettlementBillId
      and ( ( BillForClaimSettlement.Sourcetype in (1,2,3,4,5,7)) or (BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype not in(2,16)));


      end;
  /* raise_application_error(-20001, '存在结算汇总单总金额为负数的单据'); */
   end if;

  end loop;
end; 
                 
  end;
  end if;
/*
  \*主单生成*\
   FSsClaimSettlementBillId:=S_SsClaimSettlementBill.Nextval;
  insert into SsClaimSettlementBill
    (Id,
     IfInvoiced,
     Status,
     Code,
     DealerId,
     DealerCode,
     DealerName,
     BranchId,
     BranchCode,
     BranchName,
     SettlementStartTime,
     SettlementEndTime,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     CreatorId,
     CreatorName,
     CreateTime,
     PartsSalesCategoryId,
     ServiceProductLineId,
     ProductLineType)
   select FSsClaimSettlementBillId,
          0,
          1,
          regexp_replace(
            regexp_replace(FNewCode, '{CORPCODE}', Dealer.Code),
            '{SERIAL}',
            lpad(to_char(GetSerial(FComplateId, FDateFormat, Dealer.Code)),FSerialLength,'0')
          ) Code,
          DealerId,
          Dealer.Code,
          Dealer.Name,
          FBranchId,
          FBranchCode,
          FBranchName,
          FStartTime,
          FEndTime,
          LaborCost,
          MaterialCost,
          PartsManagementCost,
          FieldServiceExpense,
          DebitAmount,
          ComplementAmount,
          OtherCost,
          PreSaleAmount,
          LaborCost + MaterialCost + PartsManagementCost + FieldServiceExpense + DebitAmount + ComplementAmount +OtherCost+PreSaleAmount,
          FCreatorId,
          FCreatorName,
          sysdate,
          PARTSSALESCATEGORYID,
          ServiceProductLineId,
          ProductLineType
     from (select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  sum(MaterialCost) MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(PreSaleAmount) PreSaleAmount
             from BillForClaimSettlement
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join Dealer on Dealer.Id = t_date.DealerId;*/
IF FSettleMethods=1 THEN 
 select count(*) into GeneratedBillQuantity from (select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  sum(MaterialCost) MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(PreSaleAmount) PreSaleAmount
             from BillForClaimSettlement
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join Dealer on Dealer.Id = t_date.DealerId where LaborCost + MaterialCost + PartsManagementCost + FieldServiceExpense + DebitAmount + ComplementAmount +OtherCost+PreSaleAmount>=0
    ;
    ELSE
    select count(*) into GeneratedBillQuantity from 
   ( select * from(select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  0 LaborCost,
                  sum(MaterialCost) MaterialCost,
                  0 PartsManagementCost,
                  0 FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  0 ComplementAmount,
                  0 OtherCost,
                  0 PreSaleAmount
             from BillForClaimSettlement where( BillForClaimSettlement.Sourcetype in (1,2,3)) or (BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype in(2,16))
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join Dealer on Dealer.Id = t_date.DealerId where  MaterialCost + DebitAmount>=0
    and  not exists(select 1 from (
               select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  0 MaterialCost,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(PreSaleAmount) PreSaleAmount
             from BillForClaimSettlement where (BillForClaimSettlement.Sourcetype in (1,2,3,4,5,7)) or(BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype not in(2,16))
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t
            where  t_date.DealerId = t.DealerId
       and t_date.BranchId = t.BranchId
       And t_date.PARTSSALESCATEGORYID =
           t.PARTSSALESCATEGORYID
       And (t_date.ServiceProductLineId =
           t.ServiceProductLineId or
           t_date.ServiceProductLineId = 0 or
           t_date.ServiceProductLineId is null and
           t.LaborCost  + t.PartsManagementCost + t.FieldServiceExpense +t. DebitAmount +t. ComplementAmount +t.OtherCost+PreSaleAmount<0)
    )
    union 
    select * from(select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  sum(LaborCost) LaborCost,
                  0,
                  sum(PartsManagementCost) PartsManagementCost,
                  sum(FieldServiceExpense) FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  sum(ComplementAmount) ComplementAmount,
                  sum(OtherCost) OtherCost,
                  sum(PreSaleAmount) PreSaleAmount
            from BillForClaimSettlement where (BillForClaimSettlement.Sourcetype in (1,2,3,4,5,7)) or(BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype not in(2,16))            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t_date
    inner join Dealer on Dealer.Id = t_date.DealerId where LaborCost+ PartsManagementCost+FieldServiceExpense + ComplementAmount+OtherCost+PreSaleAmount>=0
     and  not exists(select 1 from (
              select DealerId,
                  BranchId,
                  PARTSSALESCATEGORYID,
                  ServiceProductLineId,
                  ProductLineType,
                  0 LaborCost,
                  sum(MaterialCost) MaterialCost,
                  0 PartsManagementCost,
                  0 FieldServiceExpense,
                  sum(DebitAmount) DebitAmount,
                  0 ComplementAmount,
                  0 OtherCost,
                  0 PreSaleAmount
             from BillForClaimSettlement where( BillForClaimSettlement.Sourcetype in (1,2,3)) or (BillForClaimSettlement.Sourcetype =6 and BillForClaimSettlement.Debittype in(2,16))
            group by DealerId, BranchId,PARTSSALESCATEGORYID,ServiceProductLineId,ProductLineType) t
            where  t_date.DealerId = t.DealerId
       and t_date.BranchId = t.BranchId
       And t_date.PARTSSALESCATEGORYID =
           t.PARTSSALESCATEGORYID
       And (t_date.ServiceProductLineId =
           t.ServiceProductLineId or
           t_date.ServiceProductLineId = 0 or
           t_date.ServiceProductLineId is null and
           t.MaterialCost  + t.DebitAmount <0)
    )
    );

      END IF;
  --GeneratedBillQuantity:=SQL%ROWCOUNT;

  /*清单生成*/
 /* insert into SsClaimSettlementDetail
    (Id,
     SsClaimSettlementBillId,
     SourceId,
     SourceCode,
     LaborCost,
     MaterialCost,
     PartsManagementCost,
     FieldServiceExpense,
     DebitAmount,
     ComplementAmount,
     OtherCost,
     PreSaleAmount,
     TotalAmount,
     SourceType)
  select S_SsClaimSettlementDetail.NEXTVAL,
         FSsClaimSettlementBillId,
         BillForClaimSettlement.SourceId,
         BillForClaimSettlement.SourceCode,
         BillForClaimSettlement.LaborCost,
         BillForClaimSettlement.MaterialCost,
         BillForClaimSettlement.PartsManagementCost,
         BillForClaimSettlement.FieldServiceExpense,
         BillForClaimSettlement.DebitAmount,
         BillForClaimSettlement.ComplementAmount,
         BillForClaimSettlement.OtherCost,
         BillForClaimSettlement.PreSaleAmount,
         BillForClaimSettlement.LaborCost + BillForClaimSettlement.MaterialCost + BillForClaimSettlement.PartsManagementCost
         + BillForClaimSettlement.FieldServiceExpense + BillForClaimSettlement.DebitAmount + BillForClaimSettlement.ComplementAmount
         + BillForClaimSettlement.OtherCost+BillForClaimSettlement.PreSaleAmount,
         SourceType
    from BillForClaimSettlement
   inner join SsClaimSettlementBill on BillForClaimSettlement.DealerId = SsClaimSettlementBill.DealerId
                                   and BillForClaimSettlement.BranchId = SsClaimSettlementBill.BranchId
                                   And BillForClaimSettlement.PARTSSALESCATEGORYID = SsClaimSettlementBill.PARTSSALESCATEGORYID
                                   And (BillForClaimSettlement.ServiceProductLineId = SsClaimSettlementBill.ServiceProductLineId or BillForClaimSettlement.ServiceProductLineId  =0 or
                                   BillForClaimSettlement.ServiceProductLineId is null)
                                   And (BillForClaimSettlement.ProductLineType = SsClaimSettlementBill.ProductLineType or BillForClaimSettlement.ProductLineType is null
                                   or BillForClaimSettlement.ProductLineType=0)
   where not exists (select * from SsClaimSettlementDetail where SsClaimSettlementDetail.Ssclaimsettlementbillid = SsClaimSettlementBill.Id) AND SsClaimSettlementBill.ID=FSsClaimSettlementBillId;
*/
 -- delete BillForClaimSettlement;
  return(GeneratedBillQuantity);
end;
