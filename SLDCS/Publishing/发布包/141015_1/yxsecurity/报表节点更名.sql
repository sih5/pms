--Select * From page Where Id In (13212,13213);

--报表节点更名

Update page Set Name='旧件接收入库统计' Where Id =13212;
Update page Set Name='旧件清退出库统计' Where Id =13213;


--新增订单审核缺口统计报表报表
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13149,13100, 2, 'PartsOrderConfirmGapSta',  'Released', NULL, '订单审核缺口统计报表', '统计各品牌配件销售订单审核缺口情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 26, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
