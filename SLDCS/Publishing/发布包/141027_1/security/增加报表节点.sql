INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13151,13100, 2, 'PartsPurchaseSalesSta', 'Released', NULL, '配件采购及销售情况汇总', '监控配件采购入库情况，为储备管理提供依据', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 15, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13152,13100, 2, 'AllBrandStockDetailSta', 'Released', NULL, '各品牌超X月库存明细表', '分析CDC库存情况，为储备管理提供依据', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 16, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
   
   
