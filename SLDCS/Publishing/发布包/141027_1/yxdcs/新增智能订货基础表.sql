Create Sequence S_IntelligentOrderWeeklyBase;
/

Create Sequence S_IntelligentOrderMonthlyBase;
/



/*==============================================================*/
/* Table: IntelligentOrderWeeklyBase   智能订货周度基础表        */
/*==============================================================*/
create table IntelligentOrderWeeklyBase (
   Id                   NUMBER(9)                       not null,
   BranchId                  NUMBER(9)                    not null,
   BranchCode        VARCHAR2(50)                    not null,
   BranchName               VARCHAR2(100)                    not null,
   OrderTypeId    NUMBER(9),
   OrderTypeName  VARCHAR2(100),
   IfDirectProvision    NUMBER(1),
   SalesCategoryId   NUMBER(9)                   not null,
   SalesCategoryCode  VARCHAR2(50)            Not Null,
   SalesCategoryName            VARCHAR2(100)                       not null,
   PartId        NUMBER(9)              Not Null,
   PartCode           VARCHAR2(50)      Not Null,
   PartName      VARCHAR2(100)          Not Null,
   SupplierId     NUMBER(9)  ,
   SupplierCode      VARCHAR2(50),
   SupplierName         VARCHAR2(100),
   PartABC             NUMBER(9),
   ProductLifeCycle            NUMBER(9),
   PartsBranchCreateTime     Date  Not Null,
   PlannedPrice          NUMBER(19,4),
   PlannedPriceType       NUMBER(9),
   StockQuantity           NUMBER(9) Not Null,
   StockAmount          NUMBER(19,4) Not Null,                    
   BusinessType        NUMBER(9),
   TotalQuantity      NUMBER(9),
   TotalWeeklyFrequency        NUMBER(9),
   YearD      NUMBER(9),
   WeeklyD  NUMBER(9),
   constraint PK_IntelligentOrderWeeklyBase primary key (Id)
)
/

/*==============================================================*/
/* Table: IntelligentOrderMonthlyBase      智能订货月度基础表    */
/*==============================================================*/
create table IntelligentOrderMonthlyBase  (
  Id                   NUMBER(9)                       not null,
   BranchId                  NUMBER(9)                    not null,
   BranchCode        VARCHAR2(50)                    not null,
   BranchName               VARCHAR2(100)                    not null,
   OrderTypeId    NUMBER(9),
   OrderTypeName  VARCHAR2(100),
   IfDirectProvision    NUMBER(1),
   SalesCategoryId   NUMBER(9)                   not null,
   SalesCategoryCode  VARCHAR2(50)            Not Null,
   SalesCategoryName            VARCHAR2(100)                       not null,
   PartId        NUMBER(9)              Not Null,
   PartCode           VARCHAR2(50)      Not Null,
   PartName      VARCHAR2(100)          Not Null,
   SupplierId     NUMBER(9)  ,
   SupplierCode      VARCHAR2(50),
   SupplierName         VARCHAR2(100),
   PartABC             NUMBER(9),
   ProductLifeCycle            NUMBER(9),
   PartsBranchCreateTime     Date  Not Null,
   PlannedPrice          NUMBER(19,4),
   PlannedPriceType       NUMBER(9),
   StockQuantity           NUMBER(9) Not Null,
   StockAmount          NUMBER(19,4) Not Null,                    
   BusinessType        NUMBER(9),
   TotalQuantity      NUMBER(9),
   TotalMonthlyFrequency        NUMBER(9),
   OccurMonth  VARCHAR2(50),
   constraint PK_IntelligentOrderMonthlyBase primary key (Id)
)
/
