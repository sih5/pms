
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13150,13100, 2, 'OverseaPartsOutboundSta', 'Released', NULL, '海外品牌配件出库统计表', '海外乘用车品牌、海外轻卡品牌、海外中重卡品牌配件出库单出库单明细查询', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 27, 2);
-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
