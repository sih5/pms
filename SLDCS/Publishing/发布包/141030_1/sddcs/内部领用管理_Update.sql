--增加BO:部门与品牌关系
create sequence S_DepartmentBrandRelation
/


/*==============================================================*/
/* Table: DepartmentBrandRelation                               */
/*==============================================================*/
create table DepartmentBrandRelation  (
   Id                   NUMBER(9)                       not null,
   DepartmentId         NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   Branchid             NUMBER(9)                       not null,
   constraint PK_DEPARTMENTBRANDRELATION primary key (Id)
)
/
