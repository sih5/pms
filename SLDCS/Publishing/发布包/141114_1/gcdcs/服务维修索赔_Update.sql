-- 外出索赔申请单/维修保养索赔申请单增加字段提交人Id、提交人、提交时间

Alter Table RepairClaimApplication Add   SubmitterId          NUMBER(9);
Alter Table RepairClaimApplication Add     SubmitterName        VARCHAR2(100);
Alter Table RepairClaimApplication Add     SubmitTime           Date;



Alter Table ServiceTripClaimApplication Add   SubmitterId          NUMBER(9);
Alter Table ServiceTripClaimApplication Add     SubmitterName        VARCHAR2(100);
Alter Table ServiceTripClaimApplication Add     SubmitTime           Date;
