--select * from page where ParentId=13200 ;
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13216,13200, 2, 'DealerRepairDetailSta', 'Released', NULL, '服务站维修明细', '查询服务站维修明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 22, 2);
-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
