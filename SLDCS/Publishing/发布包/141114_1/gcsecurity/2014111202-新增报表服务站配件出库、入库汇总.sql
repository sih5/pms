INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13156,13100, 2, 'DealerSparePartInWHSta', 'Released', NULL, '服务站配件入库汇总', '查询服务站配件入库情况汇总', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 31, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13157,13100, 2, 'DealerSparePartOutWHSta', 'Released', NULL, '服务站配件出库汇总', '查询服务站配件出库情况汇总', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 32, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
