--配件与供应商关系/配件与供应商关系履历增加字段月度采购到货周期、补货采购到货周期、紧急采购到货周期

Alter Table PartsSupplierRelation Add  MonthlyArrivalPeriod NUMBER(9);
Alter Table PartsSupplierRelation Add    ArrivalReplenishmentCycle NUMBER(9);
Alter Table PartsSupplierRelation Add    EmergencyArrivalPeriod NUMBER(9);


Alter Table PartsSupplierRelationHistory Add  MonthlyArrivalPeriod NUMBER(9);
Alter Table PartsSupplierRelationHistory Add    ArrivalReplenishmentCycle NUMBER(9);
Alter Table PartsSupplierRelationHistory Add    EmergencyArrivalPeriod NUMBER(9);
