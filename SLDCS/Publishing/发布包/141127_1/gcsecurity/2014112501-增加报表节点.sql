--增加报表节点

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13217,13200, 2, 'DealerClaimSalesRateSta', 'Released', NULL, '服务站保外保内销量比', '反映各个服务站的配件保内保外比情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 23, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
