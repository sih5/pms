--[#]<Physical Data Model> 服务基础信息_Oracle（PDM）
--增加bo整车保养条款车型清单履历 整车保养条款维修项目清单履历
create sequence S_VMainteProductCategoryHist
/
create sequence S_VehicleMainteRepairItemHist
/
/*==============================================================*/
/* Table: VMainteProductCategoryHist                             */
/*==============================================================*/
create table VMainteProductCategoryHist  (
   id                   NUMBER(9)                       not null,
   VehicleMainteProductCategoryId NUMBER(9),
   VehicleMainteTermId  NUMBER(9)                       not null,
   ProductCategoryName  VARCHAR2(200),
   ProductCategoryCode  VARCHAR2(200),
   MaterialCost         NUMBER(19,4),
   Type1                NUMBER(9),
   Quantity             NUMBER(15,6),
   Price                NUMBER(19,4),
   Type1Amount          NUMBER(19,4),
   Type2                NUMBER(9),
   Quantity2            NUMBER(15,6),
   Price2               NUMBER(19,4),
   Type2Amount          NUMBER(19,4),
   constraint PK_VMAINTEPRODUCTCATEGORYHIST primary key (id)
)
/
/*==============================================================*/
/* Table: VehicleMainteRepairItemHist                            */
/*==============================================================*/
create table VehicleMainteRepairItemHist  (
   Id                   NUMBER(9)                       not null,
   VehicleMainteRepairItemId NUMBER(9),
   VehicleMainteTermId  NUMBER(9)                       not null,
   RepairItemId         NUMBER(9)                       not null,
   DefaultLaborHour     NUMBER(15,6)                    not null,
   IfObliged            NUMBER(1)                       not null,
   constraint PK_VEHICLEMAINTEREPAIRITEMHIST primary key (Id)
)
/

--整车保养条款车型清单表增加字段
Alter Table VehicleMainteProductCategory Add   Type1                NUMBER(9);
Alter Table VehicleMainteProductCategory Add   Quantity             NUMBER(15,6);
Alter Table VehicleMainteProductCategory Add   Price                NUMBER(19,4);
Alter Table VehicleMainteProductCategory Add   Type1Amount          NUMBER(19,4);
Alter Table VehicleMainteProductCategory Add   Type2                NUMBER(9);
Alter Table VehicleMainteProductCategory Add   Quantity2            NUMBER(15,6);
Alter Table VehicleMainteProductCategory Add   Price2               NUMBER(19,4);
Alter Table VehicleMainteProductCategory Add   Type2Amount          NUMBER(19,4);
