INSERT INTO Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (13159,
   13100,
   2,
   'PartsSalesMonthlySta',
   'Released',
   NULL,
   '配件销售月报表（明细表）',
   '汇总总部发给服务商配件销量明细',
   'Client/DCS/Images/Menu/Channels/NotificationQuery.png',
   34,
   2);
   
   -- 确保所有窗体节点，在Node中已存在
INSERT INTO Node
  (Id, CategoryType, CategoryId, Status)
  SELECT S_Node.Nextval, 0, Id, 2
    FROM Page
   WHERE Type = 2
     AND Status = 2
     and not exists (select *
            from node
           where node.categoryid = page.Id
             and CategoryType = 0);
             