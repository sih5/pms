--服务维修索赔增加BO   DTM维修合同关联清单

create sequence S_DTMRepairContractOrderlist
/

/*==============================================================*/
/* Table: DTMRepairContractOrderlist                            */
/*==============================================================*/
create table DTMRepairContractOrderlist  (
   Id                   NUMBER(9)                       not null,
   DTMRepairContractObjid VARCHAR2(40)                    not null,
   DCSRepairOrderId     NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   constraint PK_DTMREPAIRCONTRACTORDERLIST primary key (Id)
)
/

/*==============================================================*/
/* Index: FK_RepCon_RepConOrderlist_FK                          */
/*==============================================================*/
create index FK_RepCon_RepConOrderlist_FK on DTMRepairContractOrderlist (
   DTMRepairContractObjid ASC
)
/


alter table DTMRepairContractOrderlist
   add constraint FK_DTMREPAI_FK_REPCON_DTMREPAI foreign key (DTMRepairContractObjid)
      references DTMRepairContract (DTMObjid)
/
