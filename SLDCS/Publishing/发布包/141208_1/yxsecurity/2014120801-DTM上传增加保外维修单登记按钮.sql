--更新按钮名称
Update action Set Name='保内维修单登记' Where operationid='DTMRepairContract|RepairOrderRegistration';

--添加按钮
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8407, 'DTMRepairContract|RepairOrderWarranty', '保外维修单登记', 2);
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
                  
