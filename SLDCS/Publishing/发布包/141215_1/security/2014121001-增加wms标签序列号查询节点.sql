--增加wms标签序列号查询节点

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5106,5100, 2, 'WMSLabelPrintingSerialQuery', 'Released', NULL, 'WMS标签序列号查询', '查询WMS标签序列号', 'Client/DCS/Images/Menu/PartsStocking/WMSLabelPrintingSerial.png', 6, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
