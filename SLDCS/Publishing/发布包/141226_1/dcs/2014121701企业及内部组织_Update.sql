--分公司策略增加字段内部领用取价策略、验收状态策略、是否可登记多发票策略

Alter Table Branchstrategy Add    InternalUsePriceStrategy NUMBER(9);
Alter Table Branchstrategy Add     ReceptionStatus      NUMBER(1);
Alter Table Branchstrategy Add     MutInvoiceStrategy   NUMBER(1);
