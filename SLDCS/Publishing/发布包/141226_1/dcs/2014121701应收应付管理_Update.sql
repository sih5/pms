--应收应付管理Update脚本

--建序列

create sequence S_SAP_18_SYNC
/

create sequence S_SAP_19_SYNC
/
--建表

/*==============================================================*/
/* Table: SAP_18_SYNC                                           */
/*==============================================================*/
create table SAP_18_SYNC  (
   SYNCNumber           NUMBER(9)                       not null,
   BussinessId          NUMBER(9),
   BussinessCode        VARCHAR2(100),
   BussinessTableName   VARCHAR2(100),
   constraint PK_SAP_18_SYNC primary key (SYNCNumber)
)
/

/*==============================================================*/
/* Table: SAP_19_SYNC                                           */
/*==============================================================*/
create table SAP_19_SYNC  (
   SYNCNumber           NUMBER(9)                       not null,
   BussinessId          NUMBER(9),
   BussinessCode        VARCHAR2(100),
   BussinessTableName   VARCHAR2(100),
   constraint PK_SAP_19_SYNC primary key (SYNCNumber)
)
/
--发票增加字段凭证号
Alter Table InvoiceInformation Add  VoucherNumber        VARCHAR2(30);
