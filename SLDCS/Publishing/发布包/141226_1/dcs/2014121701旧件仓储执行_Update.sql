--旧件入库单增加字段 结算状态

Alter Table UsedPartsInboundOrder Add     SettlementStatus     NUMBER(9) default 2 not null;

-- 旧件入库单清单增加字段验收状态、验收备注

Alter Table UsedPartsInboundDetail Add    ReceptionStatus      NUMBER(9);
Alter Table UsedPartsInboundDetail Add    ReceptionRemark      VARCHAR2(200);
