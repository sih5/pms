--更新字典项

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'UsedPartsShippingDetail_ReceptionStatus', '旧件验收状态', 5, '不合格', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'ExpenseAdjustmentBill_SourceType', '扣补款源单据类型', 6, '售前检查', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InternalUsePrice', '内部领用取价', 1, '计划价', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InternalUsePrice', '内部领用取价', 2, '批发价', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InvoiceFlag', '开票标记', 1, '一结算单多发票', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InvoiceFlag', '开票标记', 2, '多结算单一发票', 1, 1);
