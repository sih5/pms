--增加表采购结算发票关联单


create sequence S_PurchaseSettleInvoiceRel
/


/*==============================================================*/
/* Table: PurchaseSettleInvoiceRel                              */
/*==============================================================*/
create table PurchaseSettleInvoiceRel  (
   Id                   NUMBER(9)                       not null,
   PartsPurchaseSettleBillId NUMBER(9)                       not null,
   InvoiceId            NUMBER(9)                       not null,
   constraint PK_PURCHASESETTLEINVOICEREL primary key (Id)
)
/

--增加字段
Alter Table PartsPurchaseSettleBill Add   InvoiceFlag          NUMBER(9);
