-- 市场AB质量信息快报增加字段故障模式、排放


ALTER TABLE  MarketABQualityInformation ADD FailureMode          VARCHAR2(40);
UPDATE MarketABQualityInformation SET FailureMode ='N/A';
ALTER TABLE  MarketABQualityInformation MODIFY FailureMode  not NULL;

Alter Table MarketABQualityInformation Add    Emission             VARCHAR2(50);
