--增加激励金额字段

Alter Table SsClaimSettlementBill Add    UsedPartsEncourageAmount NUMBER(19,4);
--服务站索赔结算单清单增加字段 旧件激励金额

Alter Table SsClaimSettlementDetail Add   UsedPartsEncourageAmount NUMBER(19,4);

Alter Table SupplierClaimSettleBill Add    UsedPartsEncourageAmount NUMBER(19,4);
