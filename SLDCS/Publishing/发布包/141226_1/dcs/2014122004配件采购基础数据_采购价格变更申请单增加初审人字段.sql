  /* [!]<Table> 配件采购价格变更申请单
      Columns:
        [+-]<Column> 初审人Id
        [+-]<Column> 初审人
        [+-]<Column> 初审时间*/

Alter Table PartsPurchasePricingChange Add   InitialApproverId    NUMBER(9);
Alter Table PartsPurchasePricingChange Add    InitialApproverName  VARCHAR2(100);
Alter Table PartsPurchasePricingChange Add    InitialApproveTime   Date;
