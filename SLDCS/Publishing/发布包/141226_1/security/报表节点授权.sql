
INSERT INTO yxsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14100, 13000, 1, 'Purchase', NULL, NULL, '配件-采购管理', NULL, NULL, 11, 2);



INSERT INTO yxsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14200, 13000, 1, 'Sale', NULL, NULL, '配件-销售管理', NULL, NULL,14, 2);



INSERT INTO yxsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14300, 13000, 1, 'RDC', NULL, NULL, '配件-RDC管理', NULL, NULL,15, 2);



INSERT INTO yxsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14400, 13000, 1, 'Dealer', NULL, NULL, '配件-服务站管理', NULL, NULL, 16, 2);



INSERT INTO yxsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14500, 13000, 1, 'Partsstock', NULL, NULL, '配件-库存管理', NULL, NULL, 13, 2);



INSERT INTO yxsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14600, 13000, 1, 'target', NULL, NULL, '配件-指标管理', NULL, NULL, 17, 2);



INSERT INTO yxsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14700, 13000, 1, 'Purchase', NULL, NULL, '配件-储备管理', NULL, NULL,12, 2);
  
  create table tmpdata.pagechange141218 (no  number(9),lv2 varchar2(100),lv3 varchar2(100),oldlv2 varchar2(100))
update  tmpdata.pagechange141218  set oldlv2=(select parentid from yxsecurity.page where name=pagechange141218.lv3 and parentid in(13100,13200,13300))
select name from page group by name having count (name)>1
create table tmpdata.page141218upd as 
select *
  from yxsecurity.page
 where page.name in (select lv3 from tmpdata.pagechange141218)
   and page.parentid in (13100, 13200, 13300);

update yxsecurity.page
   set page.parentid =
       (select parentpage.id
          from security.page parentpage
         inner join tmpdata.pagechange141218 a
            on a.lv2 = parentpage.name
         where page.name = a.lv3),
       page.sequence =
       (select no
          from tmpdata.pagechange141218 a        
         where page.name = a.lv3)
 where exists
 (select 1 from tmpdata.pagechange141218 b where b.lv3 = page.name)
   and page.parentid in (13100, 13200, 13300);
select * from security.page where parentid = 13100 for update;
update yxsecurity.page set sequence =22 where id=13200 ;

update yxsecurity.page set sequence =23 where id=13300 ;


INSERT INTO security.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14100, 13000, 1, 'Purchase', NULL, NULL, '配件-采购管理', NULL, NULL, 11, 2);



INSERT INTO security.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14200, 13000, 1, 'Sale', NULL, NULL, '配件-销售管理', NULL, NULL,14, 2);



INSERT INTO security.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14300, 13000, 1, 'RDC', NULL, NULL, '配件-RDC管理', NULL, NULL,15, 2);



INSERT INTO security.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14400, 13000, 1, 'Dealer', NULL, NULL, '配件-服务站管理', NULL, NULL, 16, 2);



INSERT INTO security.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14500, 13000, 1, 'Partsstock', NULL, NULL, '配件-库存管理', NULL, NULL, 13, 2);



INSERT INTO security.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14600, 13000, 1, 'target', NULL, NULL, '配件-指标管理', NULL, NULL, 17, 2);



INSERT INTO security.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14700, 13000, 1, 'Purchase', NULL, NULL, '配件-储备管理', NULL, NULL,12, 2);


create table tmpdata.page141218updls as 
select *
  from security.page
 where page.name in (select lv3 from tmpdata.pagechange141218)
   and page.parentid in (13100, 13200, 13300);

update security.page
   set page.parentid =
       (select parentpage.id
          from security.page parentpage
         inner join tmpdata.pagechange141218 a
            on a.lv2 = parentpage.name
         where page.name = a.lv3),
       page.sequence =
       (select no
          from tmpdata.pagechange141218 a        
         where page.name = a.lv3)
 where exists
 (select 1 from tmpdata.pagechange141218 b where b.lv3 = page.name)
   and page.parentid in (13100, 13200, 13300);
select * from page where parentid = 13100;
update security.page set sequence =22 where id=13200 ;

update security.page set sequence =23 where id=13300 ;



INSERT INTO gcsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14100, 13000, 1, 'Purchase', NULL, NULL, '配件-采购管理', NULL, NULL, 11, 2);



INSERT INTO gcsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14200, 13000, 1, 'Sale', NULL, NULL, '配件-销售管理', NULL, NULL,14, 2);



INSERT INTO gcsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14300, 13000, 1, 'RDC', NULL, NULL, '配件-RDC管理', NULL, NULL,15, 2);



INSERT INTO gcsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14400, 13000, 1, 'Dealer', NULL, NULL, '配件-服务站管理', NULL, NULL, 16, 2);



INSERT INTO gcsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14500, 13000, 1, 'Partsstock', NULL, NULL, '配件-库存管理', NULL, NULL, 13, 2);



INSERT INTO gcsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14600, 13000, 1, 'target', NULL, NULL, '配件-指标管理', NULL, NULL, 17, 2);



INSERT INTO gcsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14700, 13000, 1, 'Purchase', NULL, NULL, '配件-储备管理', NULL, NULL,12, 2);


create table tmpdata.page141218updgc as 
select *
  from gcsecurity.page
 where page.name in (select lv3 from tmpdata.pagechange141218)
   and page.parentid in (13100, 13200, 13300);

update gcsecurity.page
   set page.parentid =
       (select parentpage.id
          from gcsecurity.page parentpage
         inner join tmpdata.pagechange141218 a
            on a.lv2 = parentpage.name
         where page.name = a.lv3),
       page.sequence =
       (select no
          from tmpdata.pagechange141218 a        
         where page.name = a.lv3)
 where exists
 (select 1 from tmpdata.pagechange141218 b where b.lv3 = page.name)
   and page.parentid in (13100, 13200, 13300);
select * from page where parentid = 13100;
update gcsecurity.page set sequence =22 where id=13200 ;

update gcsecurity.page set sequence =23 where id=13300 ;




INSERT INTO sdsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14100, 13000, 1, 'Purchase', NULL, NULL, '配件-采购管理', NULL, NULL, 11, 2);



INSERT INTO sdsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14200, 13000, 1, 'Sale', NULL, NULL, '配件-销售管理', NULL, NULL,14, 2);



INSERT INTO sdsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14300, 13000, 1, 'RDC', NULL, NULL, '配件-RDC管理', NULL, NULL,15, 2);



INSERT INTO sdsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14400, 13000, 1, 'Dealer', NULL, NULL, '配件-服务站管理', NULL, NULL, 16, 2);



INSERT INTO sdsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14500, 13000, 1, 'Partsstock', NULL, NULL, '配件-库存管理', NULL, NULL, 13, 2);



INSERT INTO sdsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14600, 13000, 1, 'target', NULL, NULL, '配件-指标管理', NULL, NULL, 17, 2);



INSERT INTO sdsecurity.Page
  (Id,
   ParentId,
   Type,
   PageId,
   PageType,
   Parameter,
   Name,
   Description,
   Icon,
   Sequence,
   Status)
VALUES
  (14700, 13000, 1, 'Purchase', NULL, NULL, '配件-储备管理', NULL, NULL,12, 2);


create table tmpdata.page141218updsd as 
select *
  from sdsecurity.page
 where page.name in (select lv3 from tmpdata.pagechange141218)
   and page.parentid in (13100, 13200, 13300);

update sdsecurity.page
   set page.parentid =
       (select parentpage.id
          from sdsecurity.page parentpage
         inner join tmpdata.pagechange141218 a
            on a.lv2 = parentpage.name
         where page.name = a.lv3),
       page.sequence =
       (select no
          from tmpdata.pagechange141218 a        
         where page.name = a.lv3)
 where exists
 (select 1 from tmpdata.pagechange141218 b where b.lv3 = page.name)
   and page.parentid in (13100, 13200, 13300);
select * from page where parentid = 13100;
update sdsecurity.page set sequence =22 where id=13200 ;

update sdsecurity.page set sequence =23 where id=13300 ;
