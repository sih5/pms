--增加表和对应的序列
   /*Tables:
    [+-]<Table> Sap同步队列18福戴
    [+-]<Table> Sap同步队列19福戴
    [+-]<Table> 错误信息同步队列
 Sequences:
    [+-]<Sequence> S_Sap同步队列18福戴
    [+-]<Sequence> S_Sap同步队列19福戴
    [+-]<Sequence> S_错误信息同步队列*/
    
create sequence S_ERRORTABLE_SAP_FD
/

create sequence S_SAP_18_SYNC_FD
/

create sequence S_SAP_19_SYNC_FD
/


/*==============================================================*/
/* Table: ERRORTABLE_SAP_FD                                     */
/*==============================================================*/
create table ERRORTABLE_SAP_FD  (
   Id                   NUMBER(9)                       not null,
   BussinessId          NUMBER(9),
   BussinessCode        VARCHAR2(100),
   BussinessTableName   VARCHAR2(100),
   SYNCTypeName         NUMBER(9),
   Message              VARCHAR2(100),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_ERRORTABLE_SAP_FD primary key (Id)
)
/

*==============================================================*/
/* Table: SAP_18_SYNC_FD                                        */
/*==============================================================*/
create table SAP_18_SYNC_FD  (
   SYNCNumber           NUMBER(9)                       not null,
   BussinessId          NUMBER(9),
   BussinessCode        VARCHAR2(100),
   BussinessTableName   VARCHAR2(100),
   constraint PK_SAP_18_SYNC_FD primary key (SYNCNumber)
)
/

/*==============================================================*/
/* Table: SAP_19_SYNC_FD                                        */
/*==============================================================*/
create table SAP_19_SYNC_FD  (
   SYNCNumber           NUMBER(9)                       not null,
   BussinessId          NUMBER(9),
   BussinessCode        VARCHAR2(100),
   BussinessTableName   VARCHAR2(100),
   constraint PK_SAP_19_SYNC_FD primary key (SYNCNumber)
)
/
