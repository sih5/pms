
   /* [!]<Table> 发票信息
      Columns:
        [+-]<Column> 审批人Id
        [+-]<Column> 审批时间
        [+-]<Column> 审批人*/
        
        
Alter Table InvoiceInformation Add    ApproverId           NUMBER(9);
Alter Table InvoiceInformation Add     ApproveTime          Date;
Alter Table InvoiceInformation Add     ApproverName         VARCHAR2(100);
