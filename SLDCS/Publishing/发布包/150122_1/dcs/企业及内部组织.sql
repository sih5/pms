/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2015/1/13 10:07:57                           */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Agency');
  if num>0 then
    execute immediate 'drop table Agency cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AgencyAffiBranch');
  if num>0 then
    execute immediate 'drop table AgencyAffiBranch cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AppointDealerDtl');
  if num>0 then
    execute immediate 'drop table AppointDealerDtl cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AppointFaultReasonDtl');
  if num>0 then
    execute immediate 'drop table AppointFaultReasonDtl cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AppointRepairTypeDtl');
  if num>0 then
    execute immediate 'drop table AppointRepairTypeDtl cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AppointServiceProductLineDtl');
  if num>0 then
    execute immediate 'drop table AppointServiceProductLineDtl cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AppointSupplierDtl');
  if num>0 then
    execute immediate 'drop table AppointSupplierDtl cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AutoApproveStrategy');
  if num>0 then
    execute immediate 'drop table AutoApproveStrategy cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AutoApproveStrategyHistory');
  if num>0 then
    execute immediate 'drop table AutoApproveStrategyHistory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Branch');
  if num>0 then
    execute immediate 'drop table Branch cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Branchstrategy');
  if num>0 then
    execute immediate 'drop table Branchstrategy cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Company');
  if num>0 then
    execute immediate 'drop table Company cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CompanyAddress');
  if num>0 then
    execute immediate 'drop table CompanyAddress cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CompanyInvoiceInfo');
  if num>0 then
    execute immediate 'drop table CompanyInvoiceInfo cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('CompanyLoginPicture');
  if num>0 then
    execute immediate 'drop table CompanyLoginPicture cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DealerMarketDptRelation');
  if num>0 then
    execute immediate 'drop table DealerMarketDptRelation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('LogisticCompany');
  if num>0 then
    execute immediate 'drop table LogisticCompany cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('LogisticCompanyServiceRange');
  if num>0 then
    execute immediate 'drop table LogisticCompanyServiceRange cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('MDMCustomer');
  if num>0 then
    execute immediate 'drop table MDMCustomer cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('MDMSupplier');
  if num>0 then
    execute immediate 'drop table MDMSupplier cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('MarketDptPersonnelRelation');
  if num>0 then
    execute immediate 'drop table MarketDptPersonnelRelation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('MarketingDepartment');
  if num>0 then
    execute immediate 'drop table MarketingDepartment cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RegionMarketDptRelation');
  if num>0 then
    execute immediate 'drop table RegionMarketDptRelation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RegionPersonnelRelation');
  if num>0 then
    execute immediate 'drop table RegionPersonnelRelation cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ResponsibleUnit');
  if num>0 then
    execute immediate 'drop table ResponsibleUnit cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ResponsibleUnitBranch');
  if num>0 then
    execute immediate 'drop table ResponsibleUnitBranch cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('ResponsibleUnitProductDetail');
  if num>0 then
    execute immediate 'drop table ResponsibleUnitProductDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SalesCenterstrategy');
  if num>0 then
    execute immediate 'drop table SalesCenterstrategy cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('SalesRegion');
  if num>0 then
    execute immediate 'drop table SalesRegion cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AgencyAffiBranch');
  if num>0 then
    execute immediate 'drop sequence S_AgencyAffiBranch';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AppointDealerDtl');
  if num>0 then
    execute immediate 'drop sequence S_AppointDealerDtl';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AppointFaultReasonDtl');
  if num>0 then
    execute immediate 'drop sequence S_AppointFaultReasonDtl';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AppointRepairTypeDtl');
  if num>0 then
    execute immediate 'drop sequence S_AppointRepairTypeDtl';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AppointServiceProductLineDtl');
  if num>0 then
    execute immediate 'drop sequence S_AppointServiceProductLineDtl';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AppointSupplierDtl');
  if num>0 then
    execute immediate 'drop sequence S_AppointSupplierDtl';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AutoApproveStrategy');
  if num>0 then
    execute immediate 'drop sequence S_AutoApproveStrategy';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AutoApproveStrategyHistory');
  if num>0 then
    execute immediate 'drop sequence S_AutoApproveStrategyHistory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Branchstrategy');
  if num>0 then
    execute immediate 'drop sequence S_Branchstrategy';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Company');
  if num>0 then
    execute immediate 'drop sequence S_Company';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CompanyAddress');
  if num>0 then
    execute immediate 'drop sequence S_CompanyAddress';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CompanyInvoiceInfo');
  if num>0 then
    execute immediate 'drop sequence S_CompanyInvoiceInfo';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_CompanyLoginPicture');
  if num>0 then
    execute immediate 'drop sequence S_CompanyLoginPicture';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DealerMarketDptRelation');
  if num>0 then
    execute immediate 'drop sequence S_DealerMarketDptRelation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_LogisticCompanyServiceRange');
  if num>0 then
    execute immediate 'drop sequence S_LogisticCompanyServiceRange';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_MDMCustomer');
  if num>0 then
    execute immediate 'drop sequence S_MDMCustomer';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_MDMSupplier');
  if num>0 then
    execute immediate 'drop sequence S_MDMSupplier';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_MarketDptPersonnelRelation');
  if num>0 then
    execute immediate 'drop sequence S_MarketDptPersonnelRelation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_MarketingDepartment');
  if num>0 then
    execute immediate 'drop sequence S_MarketingDepartment';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RegionMarketDptRelation');
  if num>0 then
    execute immediate 'drop sequence S_RegionMarketDptRelation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RegionPersonnelRelation');
  if num>0 then
    execute immediate 'drop sequence S_RegionPersonnelRelation';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ResponsibleUnitBranch');
  if num>0 then
    execute immediate 'drop sequence S_ResponsibleUnitBranch';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_ResponsibleUnitProductDetail');
  if num>0 then
    execute immediate 'drop sequence S_ResponsibleUnitProductDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SalesCenterstrategy');
  if num>0 then
    execute immediate 'drop sequence S_SalesCenterstrategy';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_SalesRegion');
  if num>0 then
    execute immediate 'drop sequence S_SalesRegion';
  end if;
end;
/

create sequence S_AgencyAffiBranch
/

create sequence S_AppointDealerDtl
/

create sequence S_AppointFaultReasonDtl
/

create sequence S_AppointRepairTypeDtl
/

create sequence S_AppointServiceProductLineDtl
/

create sequence S_AppointSupplierDtl
/

create sequence S_AutoApproveStrategy
/

create sequence S_AutoApproveStrategyHistory
/

create sequence S_Branchstrategy
/

create sequence S_Company
/

create sequence S_CompanyAddress
/

create sequence S_CompanyInvoiceInfo
/

create sequence S_CompanyLoginPicture
/

create sequence S_DealerMarketDptRelation
/

create sequence S_LogisticCompanyServiceRange
/

create sequence S_MDMCustomer
/

create sequence S_MDMSupplier
/

create sequence S_MarketDptPersonnelRelation
/

create sequence S_MarketingDepartment
/

create sequence S_RegionMarketDptRelation
/

create sequence S_RegionPersonnelRelation
/

create sequence S_ResponsibleUnitBranch
/

create sequence S_ResponsibleUnitProductDetail
/

create sequence S_SalesCenterstrategy
/

create sequence S_SalesRegion
/

/*==============================================================*/
/* Table: Agency                                                */
/*==============================================================*/
create table Agency  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   ShortName            VARCHAR2(50),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_AGENCY primary key (Id)
)
/

/*==============================================================*/
/* Table: AgencyAffiBranch                                      */
/*==============================================================*/
create table AgencyAffiBranch  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   AgencyId             NUMBER(9)                       not null,
   constraint PK_AGENCYAFFIBRANCH primary key (Id)
)
/

/*==============================================================*/
/* Table: AppointDealerDtl                                      */
/*==============================================================*/
create table AppointDealerDtl  (
   Id                   NUMBER(9)                       not null,
   DealerId             NUMBER(9),
   AutoApproveStrategyId NUMBER(9)                       not null,
   constraint PK_APPOINTDEALERDTL primary key (Id)
)
/

/*==============================================================*/
/* Table: AppointFaultReasonDtl                                 */
/*==============================================================*/
create table AppointFaultReasonDtl  (
   Id                   NUMBER(9)                       not null,
   AutoApproveStrategyId NUMBER(9)                       not null,
   FaultReasonId        NUMBER(9)                       not null,
   FaultReasonName      VARCHAR2(100),
   FaultReasonCode      VARCHAR2(50),
   constraint PK_APPOINTFAULTREASONDTL primary key (Id)
)
/

/*==============================================================*/
/* Table: AppointRepairTypeDtl                                  */
/*==============================================================*/
create table AppointRepairTypeDtl  (
   Id                   NUMBER(9)                       not null,
   RepairType           NUMBER(9),
   AutoApproveStrategyId NUMBER(9)                       not null,
   constraint PK_APPOINTREPAIRTYPEDTL primary key (Id)
)
/

/*==============================================================*/
/* Table: AppointServiceProductLineDtl                          */
/*==============================================================*/
create table AppointServiceProductLineDtl  (
   Id                   NUMBER(9)                       not null,
   AutoApproveStrategyId NUMBER(9)                       not null,
   ServiceProductlineId NUMBER(9),
   constraint PK_APPOINTSERVICEPRODUCTLINEDT primary key (Id)
)
/

/*==============================================================*/
/* Table: AppointSupplierDtl                                    */
/*==============================================================*/
create table AppointSupplierDtl  (
   Id                   NUMBER(9)                       not null,
   AutoApproveStrategyId NUMBER(9)                       not null,
   SupplierId           NUMBER(9),
   constraint PK_APPOINTSUPPLIERDTL primary key (Id)
)
/

/*==============================================================*/
/* Table: AutoApproveStrategy                                   */
/*==============================================================*/
create table AutoApproveStrategy  (
   Id                   NUMBER(9)                       not null,
   IsAssociatedApplication NUMBER(1),
   ServiceFee           NUMBER(19,4)                    not null,
   IsNoMaterialFee      NUMBER(1)                       not null,
   RepeatedRepairTime   NUMBER(9)                       not null,
   ClaimTimeOutLong     NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   constraint PK_AUTOAPPROVESTRATEGY primary key (Id)
)
/

/*==============================================================*/
/* Table: AutoApproveStrategyHistory                            */
/*==============================================================*/
create table AutoApproveStrategyHistory  (
   Id                   NUMBER(9)                       not null,
   AutoApproveStrategyId NUMBER(9),
   IsAssociatedApplication NUMBER(1),
   ServiceFee           NUMBER(19,4)                    not null,
   IsNoMaterialFee      NUMBER(1)                       not null,
   RepeatedRepairTime   NUMBER(9)                       not null,
   ClaimTimeOutLong     NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   constraint PK_AUTOAPPROVESTRATEGYHISTORY primary key (Id)
)
/

/*==============================================================*/
/* Table: Branch                                                */
/*==============================================================*/
create table Branch  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   Abbreviation         VARCHAR2(50),
   MainBrand            VARCHAR2(100),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_BRANCH primary key (Id)
)
/

/*==============================================================*/
/* Table: Branchstrategy                                        */
/*==============================================================*/
create table Branchstrategy  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsPurchasePricingStrategy NUMBER(9)                       not null,
   EmergencySalesStrategy NUMBER(9),
   UsedPartsReturnStrategy NUMBER(9),
   PartsShipping        NUMBER(9),
   IsGPS                NUMBER(1),
   ReturnFeeRate        NUMBER(15,6),
   MaxInvoiceAmount     NUMBER(19,4),
   PurchaseIsNeedStd    NUMBER(1),
   PreSaleAddItemStrategy NUMBER(1),
   PreSaleSaleTimeStrategy NUMBER(9),
   IsResetCreditStrategy NUMBER(9),
   SalesSettleStrategy  NUMBER(1),
   DirectSupplyStrategy NUMBER(9),
   PurchasePricingStrategy NUMBER(1),
   ClaimAutoStopStrategy NUMBER(9),
   ClaimRejectTimeStrategy NUMBER(9),
   StopClaimStartStrategy NUMBER(9),
   StopClaimTimeStrategy NUMBER(9)                      default 24,
   ScanCodeStartTime    DATE,
   IsAllServiceTripApp  NUMBER(1),
   IsFirstMainteManage  NUMBER(1),
   IsOuterPurchaseManage NUMBER(1),
   IsDebtManage         NUMBER(1),
   Outforce             NUMBER(1),
   InternalUsePriceStrategy NUMBER(9),
   ReceptionStatus      NUMBER(1),
   MutInvoiceStrategy   NUMBER(1),
   OutNumberPeople      NUMBER(9),
   OutNumberDays        NUMBER(9),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_BRANCHSTRATEGY primary key (Id)
)
/

/*==============================================================*/
/* Table: Company                                               */
/*==============================================================*/
create table Company  (
   Id                   NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   CustomerCode         VARCHAR2(50),
   SupplierCode         VARCHAR2(50),
   ShortName            VARCHAR2(50),
   RegionId             NUMBER(9),
   ProvinceName         VARCHAR2(50),
   CityName             VARCHAR2(50),
   CountyName           VARCHAR2(50),
   CityLevel            NUMBER(9),
   FoundDate            DATE,
   ContactPerson        VARCHAR2(100),
   ContactPhone         VARCHAR2(50),
   ContactMobile        VARCHAR2(50),
   Fax                  VARCHAR2(50),
   ContactAddress       VARCHAR2(100),
   ContactPostCode      VARCHAR2(100),
   ContactMail          VARCHAR2(50),
   RegisterCode         VARCHAR2(50),
   RegisterName         VARCHAR2(100),
   CorporateNature      NUMBER(9),
   LegalRepresentative  VARCHAR2(100),
   LegalRepresentTel    VARCHAR2(100),
   IdDocumentType       NUMBER(9),
   IdDocumentNumber     VARCHAR2(50),
   RegisterCapital      NUMBER(19,4),
   FixedAsset           NUMBER(19,4),
   RegisterDate         DATE,
   BusinessScope        VARCHAR2(200),
   BusinessAddress      VARCHAR2(200),
   RegisteredAddress    VARCHAR2(200),
   Remark               VARCHAR2(200),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_COMPANY primary key (Id)
)
/

/*==============================================================*/
/* Table: CompanyAddress                                        */
/*==============================================================*/
create table CompanyAddress  (
   Id                   NUMBER(9)                       not null,
   CompanyId            NUMBER(9)                       not null,
   RegionId             NUMBER(9),
   ProvinceName         VARCHAR2(50),
   CityName             VARCHAR2(50),
   CountyName           VARCHAR2(50),
   Usage                NUMBER(9),
   ContactPerson        VARCHAR2(50),
   ContactPhone         VARCHAR2(50),
   DetailAddress        VARCHAR2(200),
   CreateTime           DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   ModifyTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   RowVersion           TIMESTAMP,
   constraint PK_COMPANYADDRESS primary key (Id)
)
/

/*==============================================================*/
/* Table: CompanyInvoiceInfo                                    */
/*==============================================================*/
create table CompanyInvoiceInfo  (
   Id                   NUMBER(9)                       not null,
   InvoiceCompanyId     NUMBER(9)                       not null,
   CompanyId            NUMBER(9)                       not null,
   CompanyCode          VARCHAR2(50)                    not null,
   CompanyName          VARCHAR2(100)                   not null,
   IsVehicleSalesInvoice NUMBER(1)                       not null,
   IsPartsSalesInvoice  NUMBER(1)                       not null,
   TaxRegisteredNumber  VARCHAR2(50),
   InvoiceTitle         VARCHAR2(100),
   InvoiceType          NUMBER(9)                       not null,
   TaxpayerQualification NUMBER(9),
   InvoiceAmountQuota   NUMBER(19,4),
   TaxRegisteredAddress VARCHAR2(200),
   TaxRegisteredPhone   VARCHAR2(50),
   BankName             VARCHAR2(200),
   BankAccount          VARCHAR2(200),
   InvoiceTax           NUMBER(15,6),
   Linkman              VARCHAR2(50),
   ContactNumber        VARCHAR2(50),
   Fax                  VARCHAR2(50),
   Remark               VARCHAR2(200),
   CreateTime           DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   ModifyTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   Status               NUMBER(9)                       not null,
   RowVersion           TIMESTAMP,
   constraint PK_COMPANYINVOICEINFO primary key (Id)
)
/

/*==============================================================*/
/* Table: CompanyLoginPicture                                   */
/*==============================================================*/
create table CompanyLoginPicture  (
   Id                   NUMBER(9)                       not null,
   CompanyId            NUMBER(9)                       not null,
   Path                 VARCHAR2(2000),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_COMPANYLOGINPICTURE primary key (Id)
)
/

/*==============================================================*/
/* Table: DealerMarketDptRelation                               */
/*==============================================================*/
create table DealerMarketDptRelation  (
   Id                   NUMBER(9)                       not null,
   MarketId             NUMBER(9)                       not null,
   DealerId             NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_DEALERMARKETDPTRELATION primary key (Id)
)
/

/*==============================================================*/
/* Table: LogisticCompany                                       */
/*==============================================================*/
create table LogisticCompany  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   Abbreviation         VARCHAR2(50),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_LOGISTICCOMPANY primary key (Id)
)
/

/*==============================================================*/
/* Table: LogisticCompanyServiceRange                           */
/*==============================================================*/
create table LogisticCompanyServiceRange  (
   Id                   NUMBER(9)                       not null,
   LogisticCompanyId    NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BusinessDomain       NUMBER(9)                       not null,
   constraint PK_LOGISTICCOMPANYSERVICERANGE primary key (Id)
)
/

/*==============================================================*/
/* Table: MDMCustomer                                           */
/*==============================================================*/
create table MDMCustomer  (
   Id                   NUMBER(9)                       not null,
   BAHNS                VARCHAR2(12),
   LZONE                NUMBER(9),
   NAME4                VARCHAR2(70),
   NAME3                VARCHAR2(120),
   ORT01                VARCHAR2(70),
   PSTLZ                VARCHAR2(20),
   KUNNR                VARCHAR2(20),
   NAME1                VARCHAR2(160),
   KTOKD                NUMBER(9),
   ANRED                NUMBER(9),
   KOINH                VARCHAR2(120),
   BANKN                VARCHAR2(120),
   BANKA                VARCHAR2(120),
   BANKS                NUMBER(9),
   STREET               VARCHAR2(120),
   SORT1                VARCHAR2(40),
   BRNCH                VARCHAR2(80),
   STRAS                VARCHAR2(120),
   PROVZ                NUMBER(9),
   Bustr                VARCHAR2(100),
   Maror                VARCHAR2(100),
   Back                 NUMBER(9),
   Visua                NUMBER(9),
   Disch                NUMBER(9),
   ORT02                VARCHAR2(70),
   STCEG                VARCHAR2(40),
   VBUND                NUMBER(9),
   TELFX                VARCHAR2(62),
   TELF1                VARCHAR2(32),
   REGIO                NUMBER(9),
   ���Ҵ���                 CHAR(10),
   constraint PK_MDMCUSTOMER primary key (Id)
)
/

/*==============================================================*/
/* Table: MDMSupplier                                           */
/*==============================================================*/
create table MDMSupplier  (
   Id                   NUMBER(9)                       not null,
   LegalPerson          VARCHAR2(60),
   BNKLZ                VARCHAR2(8),
   BRNCH                VARCHAR2(10),
   CNAPS�к�              VARCHAR2(24),
   BANKS                NUMBER(9),
   BANKA                VARCHAR2(120),
   BANKN                VARCHAR2(120),
   KOINH                VARCHAR2(120),
   REGIO                NUMBER(9),
   LAND1                NUMBER(9),
   PSTLZ                VARCHAR2(12),
   STRAS                VARCHAR2(70),
   LIFNR                VARCHAR2(200),
   SORTL                VARCHAR2(30),
   NAME1                VARCHAR2(120),
   STCEG                VARCHAR2(40),
   KTOKK                NUMBER(9),
   ANRED                NUMBER(9),
   VBUND                NUMBER(9),
   TELFX                VARCHAR2(60),
   TELF1                VARCHAR2(60),
   SPRAS                NUMBER(9),
   SupplyStyle          NUMBER(9),
   str                  NUMBER(9),
   sgpp                 VARCHAR2(300),
   LinkTelephone        VARCHAR2(60),
   LinkName             VARCHAR2(40),
   Position             VARCHAR2(40),
   Department           VARCHAR2(40),
   LinkmanCata          NUMBER(9),
   ProBus               VARCHAR2(300),
   LinkPhone            VARCHAR2(60),
   RegiAd               VARCHAR2(120),
   LinkeMail            VARCHAR2(100),
   KUNNR                VARCHAR2(100),
   validity             VARCHAR2(20),
   KVERM                VARCHAR2(60),
   KVERM1               VARCHAR2(60),
   Products             VARCHAR2(200),
   Trans                VARCHAR2(100),
   ComWeb               VARCHAR2(100),
   Email                VARCHAR2(100),
   Relation             NUMBER(9),
   OwnerStr             NUMBER(9),
   Remark               VARCHAR2(500),
   STATE                NUMBER(9),
   constraint PK_MDMSUPPLIER primary key (Id)
)
/

/*==============================================================*/
/* Table: MarketDptPersonnelRelation                            */
/*==============================================================*/
create table MarketDptPersonnelRelation  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   MarketDepartmentId   NUMBER(9)                       not null,
   PersonnelId          NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   Type                 NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_MARKETDPTPERSONNELRELATION primary key (Id)
)
/

/*==============================================================*/
/* Table: MarketingDepartment                                   */
/*==============================================================*/
create table MarketingDepartment  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   BusinessType         NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   RegionId             NUMBER(9),
   ProvinceName         VARCHAR2(50),
   CityName             VARCHAR2(50),
   CountyName           VARCHAR2(50),
   Address              VARCHAR2(100),
   Phone                VARCHAR2(20),
   Fax                  VARCHAR2(20),
   PostCode             VARCHAR2(10),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_MARKETINGDEPARTMENT primary key (Id)
)
/

/*==============================================================*/
/* Table: RegionMarketDptRelation                               */
/*==============================================================*/
create table RegionMarketDptRelation  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   SalesRegionId        NUMBER(9)                       not null,
   MarketDepartmentId   NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_REGIONMARKETDPTRELATION primary key (Id)
)
/

/*==============================================================*/
/* Table: RegionPersonnelRelation                               */
/*==============================================================*/
create table RegionPersonnelRelation  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   SalesRegionId        NUMBER(9)                       not null,
   PersonnelId          NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_REGIONPERSONNELRELATION primary key (Id)
)
/

/*==============================================================*/
/* Table: ResponsibleUnit                                       */
/*==============================================================*/
create table ResponsibleUnit  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   Abbreviation         VARCHAR2(50),
   ClaimRange           VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_RESPONSIBLEUNIT primary key (Id)
)
/

/*==============================================================*/
/* Table: ResponsibleUnitBranch                                 */
/*==============================================================*/
create table ResponsibleUnitBranch  (
   Id                   NUMBER(9)                       not null,
   ResponsibleUnitId    NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   constraint PK_RESPONSIBLEUNITBRANCH primary key (Id)
)
/

/*==============================================================*/
/* Table: ResponsibleUnitProductDetail                          */
/*==============================================================*/
create table ResponsibleUnitProductDetail  (
   Id                   NUMBER(9)                       not null,
   ResponsibleUnitId    NUMBER(9)                       not null,
   ProductId            NUMBER(9)                       not null,
   constraint PK_RESPONSIBLEUNITPRODUCTDETAI primary key (Id)
)
/

/*==============================================================*/
/* Table: SalesCenterstrategy                                   */
/*==============================================================*/
create table SalesCenterstrategy  (
   Id                   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9),
   PartsBranchDefault   NUMBER(1),
   AllowCustomerCredit  NUMBER(1),
   IsOutDateClaim       NUMBER(1),
   RetailOrderStrategy  NUMBER(1),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_SALESCENTERSTRATEGY primary key (Id)
)
/

/*==============================================================*/
/* Table: SalesRegion                                           */
/*==============================================================*/
create table SalesRegion  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   RegionCode           VARCHAR2(50)                    not null,
   RegionName           VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreateTime           DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   ModifyTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_SALESREGION primary key (Id)
)
/

