--Select * From keyvalueitem Where  caption ='企业性质';

--更新字典项
Update keyvalueitem Set Value='集体企业' Where caption ='企业性质' And Key=2;

Delete keyvalueitem Where  caption ='企业性质' And Key Not In (1,2,3);

--清空企业表的企业性质字段(请先关闭Company_Trg触发器)
Update company Set CorporateNature='';
