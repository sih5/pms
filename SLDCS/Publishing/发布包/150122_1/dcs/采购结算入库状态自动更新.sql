create or replace procedure autosetpurchaseorderinstatus as
IsAllNew number(9);
IsAllFinish  number(9);
hasforcefinish number(9);
isforcefinish number(9);
ISCANNCEL NUMBER(9);
begin

  declare
    cursor AutoSet is(
      select id
        from partspurchaseorder where status in(3,4,5,6,7,99)and InStatus in(5,10) 
      ) ;

  begin
    for S_AutoSet in AutoSet
    loop
      begin
 select count(*)
   into isallnew
   from suppliershippingorder a
  inner join partsinboundplan b
     on a.code = b.sourcecode
  inner join partsinboundplandetail c
     on b.id = c.partsinboundplanid
  where a.PartsPurchaseOrderId = S_AutoSet.id
    and nvl(c.inspectedquantity, 0) > 0;
 if isallnew = 0 then
   update partspurchaseorder set InStatus = 5 where id = S_AutoSet.id;
 end if;
 select count(*)
   into IsAllFinish
   from partspurchaseorderdetail d
   left join (select a.PartsPurchaseOrderId,
                     c.sparepartid,
                     sum(c.inspectedquantity) inspectedquantity,
                     sum(c.plannedamount) plannedamount
                from suppliershippingorder a
               inner join partsinboundplan b
                  on a.code = b.sourcecode
               inner join partsinboundplandetail c
                  on b.id = c.partsinboundplanid
               inner join partspurchaseorderdetail d
                  on d.partspurchaseorderid = a.partspurchaseorderid
                 and d.sparepartid = c.sparepartid
               where a.PartsPurchaseOrderId = S_AutoSet.id
               group by c.sparepartid, a.PartsPurchaseOrderId) t
     on d.partspurchaseorderid = t.PartsPurchaseOrderId
    and d.sparepartid = t.sparepartid
  where ((t.inspectedquantity <> t.plannedamount) or
        (nvl(d.confirmedamount, 0) <> nvl(t.inspectedquantity, 0)) or
        (d.confirmedamount is null or d.confirmedamount = 0))
    and d.PartsPurchaseOrderId = S_AutoSet.id;
    
    SELECT count(*)into ISCANNCEL from partspurchaseorder where id= S_AutoSet.id and status=99;
 select count(*)
   into hasforcefinish
   from partspurchaseorder a
  inner join partspurchaseorderdetail b
     on a.id = b.partspurchaseorderid
   left join suppliershippingorder c
     on c.partspurchaseordercode = a.code
   left join suppliershippingdetail d
     on d.suppliershippingorderid = c.id
    and b.sparepartid = d.sparepartid
   left join partsinboundplan e
     on e.sourcecode = c.code
   left join partsinboundplandetail f
     on f.partsinboundplanid = e.id
    and b.sparepartid = f.sparepartid
  where (a.status = 7 or c.status = 4 or e.status = 3)
    and a.id = S_AutoSet.id;
   select count(*)
           into isforcefinish
          from (select * from (select a.id aid,
                  c.id cid,
                  e.id eid,
                  b.sparepartid,
                  -- d.sparepartid,
                  sum(NVL(b.orderamount, 0) -
                      NVL(b.confirmedamount, 0)) partspurchaseorderamount,
                       sum(nvl(b.confirmedamount,0)-nvl(b.shippingamount,0))shippingamount,
                  sum(NVL(d.quantity, 0) - NVL(d.confirmedamount, 0)) suppliershippingorderamount,
                  sum(NVL(f.plannedamount, 0) -
                      NVL(f.inspectedquantity, 0)) partsinboundplanamount,
                  sum(NVL(f.inspectedquantity, 0)) inspectedquantity,
                  sum(nvl(b.orderamount, 0)) orderamount
             from partspurchaseorder a
            inner join partspurchaseorderdetail b
               on a.id = b.partspurchaseorderid
             left join suppliershippingorder c
               on c.partspurchaseordercode = a.code
             left join suppliershippingdetail d
               on d.suppliershippingorderid = c.id
              and b.sparepartid = d.sparepartid
             left join partsinboundplan e
               on e.sourcecode = c.code
             left join partsinboundplandetail f
               on f.partsinboundplanid = e.id
              and b.sparepartid = f.sparepartid
            where (a.status = 7 or c.status = 4 or e.status = 3) 
              and a.id = S_AutoSet.id
            group by a.id, c.id, e.id, b.sparepartid --, d.sparepartid
           )
          where inspectedquantity + partsinboundplanamount +
                suppliershippingorderamount + partspurchaseorderamount+shippingamount <>
                orderamount
                 union 
                 select *
          
           from (select a.id aid,
                  c.id cid,
                  e.id eid,
                  b.sparepartid,
                  -- d.sparepartid,
                  sum(NVL(b.orderamount, 0) -
                      NVL(b.confirmedamount, 0)) partspurchaseorderamount,
                       sum(nvl(b.confirmedamount,0)-nvl(b.shippingamount,0))shippingamount,
                  sum(NVL(d.quantity, 0) - NVL(d.confirmedamount, 0)) suppliershippingorderamount,
                  sum(NVL(f.plannedamount, 0) -
                      NVL(f.inspectedquantity, 0)) partsinboundplanamount,
                  sum(NVL(f.inspectedquantity, 0)) inspectedquantity,
                  sum(nvl(b.orderamount, 0)) orderamount
             from partspurchaseorder a
            inner join partspurchaseorderdetail b
               on a.id = b.partspurchaseorderid
             left join suppliershippingorder c
               on c.partspurchaseordercode = a.code
             left join suppliershippingdetail d
               on d.suppliershippingorderid = c.id
              and b.sparepartid = d.sparepartid
             left join partsinboundplan e
               on e.sourcecode = c.code
             left join partsinboundplandetail f
               on f.partsinboundplanid = e.id
              and b.sparepartid = f.sparepartid
            where (a.status = 7 or c.status = 4 or e.status = 3)and (e.status=4 or c.status=1 or c.status=3 or e.status=1)
              and a.id =   S_AutoSet.id
            group by a.id, c.id, e.id, b.sparepartid --, d.sparepartid
           ))
           ;
        if (IsAllFinish=0) and (isallnew<>0) then
          update partspurchaseorder set InStatus=15 where id=S_AutoSet.id;
          end if;
        if  (isallnew>0)and  (IsAllFinish>0) then
          update partspurchaseorder set InStatus=10 where id=S_AutoSet.id;
          end if;
         if (hasforcefinish>0) and  (isforcefinish=0)/* and ( IsAllFinish<>0)*/ then
            update partspurchaseorder set InStatus=11 where id=S_AutoSet.id;
          end if;
                 if ISCANNCEL>0 then
            update partspurchaseorder set InStatus=99 where id=S_AutoSet.id;
          end if;
      end;
    end loop;
  end;
  end autosetpurchaseorderinstatus;
