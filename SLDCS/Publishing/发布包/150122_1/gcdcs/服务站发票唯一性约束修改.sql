alter table DEALERINVOICEINFORMATION
  drop constraint UK_DEALERINVOICEINFORMATION cascade;
alter table DEALERINVOICEINFORMATION
  add constraint UK_DEALERINVOICEINFORMATION unique (INVOICENUMBER, INVOICECODE, STATUS);
