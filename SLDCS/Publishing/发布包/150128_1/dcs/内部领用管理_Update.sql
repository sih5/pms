-- 内部领出清单、 内部领入清单增加字段 批发价

Alter Table InternalAcquisitionDetail Add    SalesPrice           NUMBER(19,4);

Alter Table InternalAllocationDetail   Add    SalesPrice           NUMBER(19,4);
 
