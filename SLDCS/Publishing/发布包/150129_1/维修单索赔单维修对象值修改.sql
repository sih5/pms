create table tmpdata.repairclaimbill150129 as  select id ,REPAIROBJECTID from REPAIRCLAIMBILL T;
UPDATE REPAIRCLAIMBILL T
   SET T.REPAIROBJECTID = DECODE((SELECT V.REPAIRTARGET
                                   FROM REPAIROBJECT V
                                  WHERE V.ID = T.REPAIROBJECTID),
                                 '整车',
                                 (SELECT ID FROM REPAIROBJECT A
                                   WHERE A.REPAIRTARGET = '整车'
                                     AND A.PARTSSALESCATEGORYID = 0),
                                 '底盘',
                                 (SELECT ID FROM REPAIROBJECT A
                                   WHERE A.REPAIRTARGET = '底盘'
                                     AND A.PARTSSALESCATEGORYID = 0),
                                 '专项发动机',
                                 (SELECT ID FROM REPAIROBJECT A
                                   WHERE A.REPAIRTARGET = '专项发动机'
                                     AND A.PARTSSALESCATEGORYID = 0));
                                     
create table tmpdata.REPAIRORDER150129 as  select id ,REPAIROBJECTID from REPAIRORDER T;
UPDATE REPAIRORDER T
   SET T.REPAIROBJECTID = DECODE((SELECT V.REPAIRTARGET
                                   FROM REPAIROBJECT V
                                  WHERE V.ID = T.REPAIROBJECTID),
                                 '整车',
                                 (SELECT ID FROM REPAIROBJECT A
                                   WHERE A.REPAIRTARGET = '整车'
                                     AND A.PARTSSALESCATEGORYID = 0),
                                 '底盘',
                                 (SELECT ID FROM REPAIROBJECT A
                                   WHERE A.REPAIRTARGET = '底盘'
                                     AND A.PARTSSALESCATEGORYID = 0),
                                 '专项发动机',
                                 (SELECT ID FROM REPAIROBJECT A
                                   WHERE A.REPAIRTARGET = '专项发动机'
                                     AND A.PARTSSALESCATEGORYID = 0));
