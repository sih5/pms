--增加BO销售退货结算发票关联单

create sequence S_SalesRtnSettleInvoiceRel
/

/*==============================================================*/
/* Table: SalesRtnSettleInvoiceRel                              */
/*==============================================================*/
create table SalesRtnSettleInvoiceRel  (
   Id                   NUMBER(9)                       not null,
   PartsSalesRtnSettlementId NUMBER(9)                       not null,
   InvoiceId            NUMBER(9)                       not null,
   constraint PK_SALESRTNSETTLEINVOICEREL primary key (Id)
)
/
