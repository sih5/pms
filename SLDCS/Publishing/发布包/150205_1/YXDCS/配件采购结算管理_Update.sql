--增加BO采购退货结算发票关联单


create sequence S_PurchaseRtnSettleInvoiceRel
/

/*==============================================================*/
/* Table: PurchaseRtnSettleInvoiceRel                           */
/*==============================================================*/
create table PurchaseRtnSettleInvoiceRel  (
   Id                   NUMBER(9)                       not null,
   PartsPurchaseRtnSettleBillId NUMBER(9)                       not null,
   InvoiceId            NUMBER(9)                       not null,
   constraint PK_PURCHASERTNSETTLEINVOICEREL primary key (Id)
)
/
