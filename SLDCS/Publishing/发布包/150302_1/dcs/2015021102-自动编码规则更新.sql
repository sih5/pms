--货款预批单编号 
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'SupplierPreApprovedLoan', 'SupplierPreApprovedLoan', 1, 'SGP{CORPCODE}{DATE:yyyyMMdd}{SERIAL:000}', 1, NULL);

--配件互换号生成规则 
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'PartsExchange', 'PartsExchange', 1, 'SWP{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
