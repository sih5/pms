

create view salessettleinvoiceresource as
select a.id salessettleid,
       a.code salessettlecode,
       '配件销售结算单' type,
       b.id invoiceid
  from partssalessettlement a
 inner join invoiceinformation b
    on a.id = b.sourceid and b.sourcetype=1
union
select c.id, c.code, '配件销售退货结算单' type, d.invoiceid
  from PartsSalesRtnSettlement c
 inner join  SalesRtnSettleInvoiceRel d
    on c.id = d.partssalesrtnsettlementid;