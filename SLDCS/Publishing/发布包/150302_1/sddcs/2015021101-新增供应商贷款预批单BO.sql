 /* [+-]<Table> 供应商货款预批单
    [+-]<Table> 供应商货款预批单清单
    [+-]<Table> 供应商计划欠款*/
 

create sequence S_SupplierPlanArrear
/

create sequence S_SupplierPreApprovedLoan
/

create sequence S_SupplierPreAppLoanDetail
/

/*==============================================================*/
/* Table: SupplierPlanArrear                                    */
/*==============================================================*/
create table SupplierPlanArrear  (
   Id                   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   SupplierCode         VARCHAR2(50)                    not null,
   SupplierId           NUMBER(9)                       not null,
   SupplierCompanyCode  VARCHAR2(50)                    not null,
   SupplierCompanyName  VARCHAR2(100)                   not null,
   PlannedAmountOwed    NUMBER(19,4)                    not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime            DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_SUPPLIERPLANARREAR primary key (Id)
)
/

/*==============================================================*/
/* Table: SupplierPreApprovedLoan                               */
/*==============================================================*/
create table SupplierPreApprovedLoan  (
   Id                   NUMBER(9)                       not null,
   PreApprovedCode      VARCHAR2(50)                    not null,
   BranchId             NUMBER(9),
   BranchCode           VARCHAR2(50),
   PartsSalesCategoryId NUMBER(9)                       not null,
   ActualDebt           NUMBER(19,4)                    not null,
   PlanArrear           NUMBER(19,4)                    not null,
   ExceedAmount         NUMBER(19,4)                    not null,
   ApprovalAmount       NUMBER(19,4)                    not null,
   ActualPaidAmount     NUMBER(19,4),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_SUPPLIERPREAPPROVEDLOAN primary key (Id)
)
/

/*==============================================================*/
/* Table: SupplierPreAppLoanDetail                              */
/*==============================================================*/
create table SupplierPreAppLoanDetail  (
   Id                   NUMBER(9)                       not null,
   SupplierPreApprovedLoanId NUMBER(9)                       not null,
   SupplierCode         VARCHAR2(50)                    not null,
   SupplierId           NUMBER(9)                       not null,
   SupplierCompanyCode  VARCHAR2(50)                    not null,
   SupplierCompanyName  VARCHAR2(100)                   not null,
   BankName             VARCHAR2(50),
   BankCode             VARCHAR2(20),
   PlannedAmountOwed    NUMBER(19,4)                    not null,
   ActualDebt           NUMBER(19,4)                    not null,
   ExceedAmount         NUMBER(19,4)                    not null,
   ApprovalAmount       NUMBER(19,4),
   ActualPaidAmount     NUMBER(19,4),
   IsOverstock          NUMBER(1)                       not null,
   Remark               VARCHAR2(500),
   constraint PK_SUPPLIERPREAPPLOANDETAIL primary key (Id)
)
/

