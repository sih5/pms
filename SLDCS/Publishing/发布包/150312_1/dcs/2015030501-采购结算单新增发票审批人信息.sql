   /*[!]<Table> 配件采购结算单
      Columns:
        [+-]<Column> 发票审批人Id
        [+-]<Column> 发票审批人
        [+-]<Column> 发票审批时间
*/

Alter Table PartsPurchaseSettleBill Add    InvoiceApproverId    NUMBER(9);
Alter Table PartsPurchaseSettleBill Add    InvoiceApproverName  VARCHAR2(100);
Alter Table PartsPurchaseSettleBill Add    InvoiceApproveTime   Date;
