create table SupClaimSettleInstruction
(
  ID                   NUMBER(9) not null,
  BRANCHID             NUMBER(9) not null,
  BRANCHCODE           VARCHAR2(50) not null,
  BRANCHNAME           VARCHAR2(200) not null,
  PARTSSALESCATEGORYID NUMBER(9),
  SERVICEPRODUCTLINEID NUMBER(9),
  PRODUCTLINETYPE      NUMBER(9),
  STATUS               NUMBER(9) not null,
  SETTLEMENTSTARTTIME  DATE,
  SETTLEMENTENDTIME    DATE not null,
  REMARK               VARCHAR2(200),
  CREATORID            NUMBER(9),
  CREATORNAME          VARCHAR2(100),
  CREATETIME           DATE,
  EXECUTIONTIME        DATE,
  ABANDONERID          NUMBER(9),
  ABANDONERNAME        VARCHAR2(100),
  ABANDONTIME          DATE,
  EXECUTIONRESULT      VARCHAR2(200)
);
/
create or replace procedure ExecSupClaimSettleInstruction as
  /*定义接收参数*/
  GeneratedBillQuantity integer;
  ErrorMsg varchar2(200);
  /*定义游标*/
  cursor Instructions Is
    select Id,
           BranchId,
           SettlementStartTime,
           SettlementEndTime,
           CreatorId,
           CreatorName,
           nvl(PartsSalesCategoryId,0) PartsSalesCategoryId,
           nvl(ServiceProductLineId,0) ServiceProductLineId,
           ProductLineType
      from SupClaimSettleInstruction

     where SettlementEndTime < trunc(sysdate,'DD')
       and Status = 1
     order by SettlementEndTime;
   Instruction Instructions%rowtype;
begin
  for Instruction in Instructions loop
    ErrorMsg:='';
    GeneratedBillQuantity:=0;
    begin
      GeneratedBillQuantity:=GenerateSupClaimSettlementBill(Instruction.branchid,null,Instruction.settlementstarttime,Instruction.settlementendtime,Instruction.creatorid,Instruction.creatorname,Instruction.PartsSalesCategoryId,Instruction.ServiceProductLineId,Instruction.ProductLineType);
      Update SsClaimSettleInstruction set SsClaimSettleInstruction.Status = 2, SsClaimSettleInstruction.ExecutionTime = sysdate
       where Id = Instruction.Id;
    exception
      when others then
        rollback;
        ErrorMsg:=Sqlerrm;
        Update SupClaimSettleInstruction set SupClaimSettleInstruction.Status = 3, SupClaimSettleInstruction.ExecutionResult = ErrorMsg, SupClaimSettleInstruction.ExecutionTime = sysdate
         where Id = Instruction.Id;
        commit;
        exit;
    end;
    commit;
  end loop;
end;
/

begin
  sys.dbms_scheduler.create_job(job_name            => '供应商索赔结算指令',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'ExecSupClaimSettleInstruction',
                                start_date          => to_date('09-03-2015 00:30:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => false,
                                auto_drop           => false,
                                comments            => '');
end;
/