create or replace view LocketUnitQuery as
select a.code,
       a.status,
       a.outboundtype,
       (select code from sparepart where sparepart.id = b.sparepartid) SparePartCode,
       (select name from sparepart where sparepart.id = b.sparepartid) SparePartName,
       b.plannedamount - nvl(b.OutboundFulfillment, 0) LocketQty,
       a.creatorname,
       a.createtime,
       (select code from warehouse where warehouse.id = a.warehouseid) WarehouseCode,
       (select name from warehouse where warehouse.id = a.warehouseid) WarehouseName,
       (select code from company where company.id = a.counterpartcompanyid) CounterpartCompanyCode,
       (select name from company where company.id = a.counterpartcompanyid) CounterpartCompanyName
  from partsoutboundplan a
 inner join partsoutboundplandetail b
    on a.id = b.partsoutboundplanid
 where a.status in (1, 2)
   and b.PlannedAmount - nvl(b.OutboundFulfillment, 0) > 0;
