--新增报表节点

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES(13218,13200, 2, 'DealerRepairOutInfoSta', 'Released', NULL, '服务站保外维修信息统计报表', '对各品牌服务站提报保内、保外维修信息数量进行统计，并计算出保外信息占保内信息比', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 24, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13219,13200, 2, 'RepairCheckTime', 'Released', NULL, '维修审批时长统计', '监控各事业本部对于服务站提报申请单据的审批时长', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 25, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13220,13200, 2, 'RegisteredCapitalSta', 'Released', NULL, '注册资金统计报表', '根据服务网络基本信息中注册资金档案信息及分品牌信息中业务能力对特约服务站注册资金分区段进行统计', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 26, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
