--新增报表节点

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13221,13200, 2, 'QualifyInefficientNetworkAnalysisReport', 'Released', NULL, '保外低效网络分析报表', '根据网络数量及维修量，查询各品牌低效网络数量（零维修量、月均不足10台）', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 27, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13222,13200, 2, 'InefficientNetworkAnalysisReport', 'Released', NULL, '低效网络分析报表', '根据网络数量及维修量，查询各品牌低效网络数量（零维修量、月均不足10台', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 28, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13223,13200, 2, 'DealerCustomerChurnRateSta', 'Released', NULL, '服务站客户流失率分析表', '本报表是对服务站客户流失率进行统计', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 29, 2);


INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13224,13200, 2, 'StarStatistics', 'Released', NULL, '星级统计报表', '查询区域内符合某项指标的服务商的数量和分布', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 30, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13225,13200, 2, 'StatisticalReportsMainQuali', 'Released', NULL, '维修资质统计报表', '查询区域内符合某项指标的服务商的数量和分布', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 31, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13226,13200, 2, 'StatisticalReportsOnTheServiceType', 'Released', NULL, '服务站类型统计报表', '查询区域内符合某项指标的服务商的数量和分布', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 32, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13227,13200, 2, 'BusinessAbilitySta', 'Released', NULL, '业务能力统计报表', '查询区域内符合某项指标的服务商的数量和分布', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 33, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13228,13200, 2, 'StarOutSerQualitySta', 'Released', NULL, '星级站保外服务量分析报表', '根据各品牌星级站数量及维修量分析某个时间段内星级站的服务集中度', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 34, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13229,13200, 2, 'StarInSerQualitySta', 'Released', NULL, '星级站保内服务量分析报表', '根据各品牌星级站数量及维修量分析某个时间段内星级站的服务集中度', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 35, 2);



-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
