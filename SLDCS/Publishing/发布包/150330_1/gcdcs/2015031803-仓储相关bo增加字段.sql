/*  [!]<Table> WMS标签序列号
      Columns:
        [+-]<Column> 创建企业ID
        [+-]<Column> 创建企业编号
        [+-]<Column> 创建企业名称
        [+-]<Column> 创建人Id
        [+-]<Column> 创建人
        [+-]<Column> 创建时间*/
        
--仓库增加字段是否质量件仓库
 
Alter Table WMSLabelPrintingSerial Add  CreateCompanyId      NUMBER(9);
Alter Table WMSLabelPrintingSerial Add    CreateCompanyCode    VARCHAR2(50);
Alter Table WMSLabelPrintingSerial Add    CreateCompanyName    VARCHAR2(100);
Alter Table WMSLabelPrintingSerial Add    CreatorId            NUMBER(9);
Alter Table WMSLabelPrintingSerial Add    CreatorName          VARCHAR2(100);
Alter Table WMSLabelPrintingSerial Add    CreateTime           Date;


Alter Table Warehouse Add    IsQualityWarehouse   NUMBER(1);
