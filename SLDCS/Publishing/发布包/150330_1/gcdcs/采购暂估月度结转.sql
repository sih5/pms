-- Create table
create table PURCHASENOSETTLETable
( id   NUMBER(9) not null,
  BRANCHCODE           VARCHAR2(50),
  PARTSSALESCATEGORYID NUMBER(9),
  CATEGORYNAME         VARCHAR2(100),
  COMPANYID            NUMBER(9),
  CODE                 VARCHAR2(50),
  NAME                 VARCHAR2(100),
  ORDERCODE            VARCHAR2(50),
  PARTCODE             VARCHAR2(50),
  PARTNAME             VARCHAR2(100),
  JH                   NUMBER(19,4),
  HT                   NUMBER(19,4),
  QTY                  NUMBER(9),
  JHCB                 NUMBER,
  HTJE                 NUMBER,
  CREATETIME           DATE,
  INBOUNDTYPE          NUMBER(9),
  VALUE                VARCHAR2(200),
  WAREHOUSECODE        VARCHAR2(50),
  WAREHOUSENAME        VARCHAR2(100),
  SYNCDATE             VARCHAR2(100)
);
alter table PURCHASENOSETTLETable
  add constraint PK_PURCHASENOSETTLETable primary key (ID);
/
create sequence S_PURCHASENOSETTLETable
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;
/

create or replace procedure autoaddpurchasenosettle as
begin
  insert into PURCHASENOSETTLETable
  select S_PURCHASENOSETTLETable.nextval,t.*from (
    select c.code as BranchCode,
           a.partssalescategoryid,
           psc.name as CategoryName,
           d.id CompanyId,
           d.code,
           d.name,
           a.code as OrderCode,
           sp.code as PartCode,
           sp.name as PartName,
           b.costprice as Jh,
           b.settlementprice as HT,
           b.inspectedquantity as Qty,
           case
             when a.createtime < to_date('2014-07-01', 'yyyy-mm-dd') then
              round(b.costprice * b.inspectedquantity / 1.17, 2)
             else
              round(b.costprice * b.inspectedquantity, 2)
           end as JHCB,
           round(b.settlementprice * b.inspectedquantity, 2) as HTJE,
           a.createtime,
           a.inboundtype,
           kv.value,
           warehouse.code as warehousecode,
           warehouse.name as warehousename,
           to_char(sysdate - 1, 'yyyy-mm') syncdate
      from PartsInboundCheckBill a
     inner join warehouse
        on a.warehouseid = warehouse.id
     inner join Partsinboundcheckbilldetail b
        on a.id = b.partsinboundcheckbillid
     inner join branch c
        on c.id = a.storagecompanyid
     inner join company d
        on d.id = a.counterpartcompanyid
     inner join sparepart sp
        on sp.id = b.sparepartid
     inner join partssalescategory psc
        on psc.id = a.partssalescategoryid
      left join keyvalueitem kv
        on kv.key = a.inboundtype
       and kv.name = 'Parts_InboundType'
     inner join branch
        on a.storagecompanyid = branch.id
       and branch.status <> 99
     where a.Settlementstatus <> 3
    union all
    select c.code as BranchCode,
           a.partssalescategoryid,
           psc.name as CategoryName,
           d.id CompanyId,
           d.code,
           d.name,
           a.code as OrderCode,
           sp.code as PartCode,
           sp.name as PartName,
           b.costprice as Jh,
           b.settlementprice as HT,
           b.inspectedquantity as Qty,
           case
             when a.createtime < to_date('2014-07-01', 'yyyy-mm-dd') then
              round(b.costprice * b.inspectedquantity / 1.17, 2)
             else
              round(b.costprice * b.inspectedquantity, 2)
           end as JHCB,
           round(b.settlementprice * b.inspectedquantity, 2) as HTJE,
           a.createtime,
           a.inboundtype,
           kv.value,
           warehouse.code as warehousecode,
           warehouse.name as warehousename,
           to_char(sysdate - 1, 'yyyy-mm') syncdate
      from PartsInboundCheckBill a
     inner join warehouse
        on a.warehouseid = warehouse.id
     inner join Partsinboundcheckbilldetail b
        on a.id = b.partsinboundcheckbillid
     inner join branch c
        on c.id = a.storagecompanyid
     inner join company d
        on d.id = a.counterpartcompanyid
     inner join PartsPurchaseSettleRef psr
        on psr.sourceid = a.id
       and psr.SourceType = 5
     inner join partspurchasesettlebill psb
        on psb.id = psr.partspurchasesettlebillid
     inner join sparepart sp
        on sp.id = b.sparepartid
     inner join partssalescategory psc
        on psc.id = a.partssalescategoryid
      left join keyvalueitem kv
        on kv.key = a.inboundtype
       and kv.name = 'Parts_InboundType'
     inner join branch
        on a.storagecompanyid = branch.id
       and branch.status <> 99
     where a.Settlementstatus = 3
       and psb.status in (1, 2, 3, 5)
    union all
    select c.code as BranchCode,
           a.partssalescategoryid,
           psc.name as CategoryName,
           d.id CompanyId,
           d.code,
           d.name,
           a.code as OrderCode,
           sp.code as PartCode,
           sp.name as PartName,
           b.costprice as Jh,
           b.settlementprice as HT,
           b.inspectedquantity as Qty,
           case
             when a.createtime < to_date('2014-07-01', 'yyyy-mm-dd') then
              round(b.costprice * b.inspectedquantity / 1.17, 2)
             else
              round(b.costprice * b.inspectedquantity, 2)
           end as JHCB,
           round(b.settlementprice * b.inspectedquantity, 2) as HTJE,
           a.createtime,
           a.inboundtype,
           kv.value,
           warehouse.code as warehousecode,
           warehouse.name as warehousename,
           to_char(sysdate - 1, 'yyyy-mm') syncdate
      from PartsInboundCheckBill a
     inner join warehouse
        on a.warehouseid = warehouse.id
     inner join Partsinboundcheckbilldetail b
        on a.id = b.partsinboundcheckbillid
     inner join branch c
        on c.id = a.storagecompanyid
     inner join company d
        on d.id = a.counterpartcompanyid
     inner join partssalesrtnsettlementref psr
        on psr.sourceid = a.id
     inner join partssalesrtnsettlement psb
        on psb.id = psr.partssalesrtnsettlementId
     inner join sparepart sp
        on sp.id = b.sparepartid
     inner join partssalescategory psc
        on psc.id = a.partssalescategoryid
      left join keyvalueitem kv
        on kv.key = a.inboundtype
       and kv.name = 'Parts_InboundType'
     inner join branch
        on a.storagecompanyid = branch.id
       and branch.status <> 99
     where a.Settlementstatus = 3
       and psb.status in (1, 2)
    ----销售退货入库包含销售合并结算
    union all
    select c.code as BranchCode,
           a.partssalescategoryid,
           psc.name as CategoryName,
           d.id CompanyId,
           d.code,
           d.name,
           a.code as OrderCode,
           sp.code as PartCode,
           sp.name as PartName,
           b.costprice as Jh,
           b.settlementprice as HT,
           b.inspectedquantity as Qty,
           case
             when a.createtime < to_date('2014-07-01', 'yyyy-mm-dd') then
              round(b.costprice * b.inspectedquantity / 1.17, 2)
             else
              round(b.costprice * b.inspectedquantity, 2)
           end as JHCB,
           round(b.settlementprice * b.inspectedquantity, 2) as HTJE,
           a.createtime,
           a.inboundtype,
           kv.value,
           warehouse.code as warehousecode,
           warehouse.name as warehousename,
           to_char(sysdate - 1, 'yyyy-mm') syncdate
      from PartsInboundCheckBill a
     inner join warehouse
        on a.warehouseid = warehouse.id
     inner join Partsinboundcheckbilldetail b
        on a.id = b.partsinboundcheckbillid
     inner join branch c
        on c.id = a.storagecompanyid
     inner join company d
        on d.id = a.counterpartcompanyid
     inner join PartsSalesSettlementRef psr
        on psr.sourceid = a.id
       and psr.SourceType = 5
     inner join partssalessettlement psb
        on psb.id = psr.partssalessettlementid
     inner join sparepart sp
        on sp.id = b.sparepartid
     inner join partssalescategory psc
        on psc.id = a.partssalescategoryid
      left join keyvalueitem kv
        on kv.key = a.inboundtype
       and kv.name = 'Parts_InboundType'
     inner join branch
        on a.storagecompanyid = branch.id
       and branch.status <> 99
     where a.Settlementstatus = 3
       and psb.status in (1, 2)
    union all
    select c.code as BranchCode,
           a.partssalescategoryid,
           psc.name as CategoryName,
           d.id CompanyId,
           d.code,
           d.name,
           a.code as OrderCode,
           sp.code as PartCode,
           sp.name as PartName,
           b.costprice as Jh,
           b.settlementprice as HT,
           b.inspectedquantity as Qty,
           case
             when a.createtime < to_date('2014-07-01', 'yyyy-mm-dd') then
              round(b.costprice * b.inspectedquantity / 1.17, 2)
             else
              round(b.costprice * b.inspectedquantity, 2)
           end as JHCB,
           round(b.settlementprice * b.inspectedquantity, 2) as HTJE,
           a.createtime,
           a.inboundtype,
           kv.value,
           warehouse.code as warehousecode,
           warehouse.name as warehousename,
           to_char(sysdate - 1, 'yyyy-mm') syncdate
      from PartsInboundCheckBill a
     inner join warehouse
        on a.warehouseid = warehouse.id
     inner join Partsinboundcheckbilldetail b
        on a.id = b.partsinboundcheckbillid
     inner join branch c
        on c.id = a.storagecompanyid
     inner join company d
        on d.id = a.counterpartcompanyid
     inner join PartsRequisitionSettleRef psr
        on psr.sourceid = a.id
       and psr.SourceType = 5
     inner join PartsRequisitionSettleBill psb
        on psb.id = psr.PartsRequisitionSettleBillID
     inner join sparepart sp
        on sp.id = b.sparepartid
     inner join partssalescategory psc
        on psc.id = a.partssalescategoryid
      left join keyvalueitem kv
        on kv.key = a.inboundtype
       and kv.name = 'Parts_InboundType'
     inner join branch
        on a.storagecompanyid = branch.id
       and branch.status <> 99
     where a.Settlementstatus = 3
       and psb.status = 1)t;
end autoaddpurchasenosettle;
/
begin
  sys.dbms_scheduler.create_job(job_name            => '采购暂估月度结转',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'autoaddpurchasenosettle',
                                start_date          => to_date('01-04-2015 00:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Monthly;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
