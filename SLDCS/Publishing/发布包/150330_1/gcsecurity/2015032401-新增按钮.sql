--新增节点

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6214,6200, 2, 'FDSAPInvoiceInfoQuery', 'Released', NULL, '福戴SAP发票接口查询', '查询福戴SAP接口发票', 'Client/DCS/Images/Menu/Financial/SAPInvoiceInfoQuery.png', 14, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6215,6200, 2, 'SAPFDErrorTable', 'Released', NULL,'福戴SAP业务接口查询', '查询福戴SAP接口错误信息', 'Client/DCS/Images/Menu/Financial/SAPYXErrorTable.png', 15, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);




--分公司与供应商关系管理增加导入修改按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2201, 'Common|ImportUpdate', '导入修改', 2);

--保内维修单管理-服务站增加维修工单打印按钮
--INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|RepairOrderPrintNew', '打印维修单(新)', 2);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);

