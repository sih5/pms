--新增维修索赔供应商及外出服务索赔管理供应商节点
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8307,8300, 2, 'ServiceTripClaimBillForSupplier', 'Released', NULL, '外出服务索赔管理-供应商', '供应商复核，确认外出索赔单', 'Client/DCS/Images/Menu/Service/ServiceTripClaimBill.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8311,8300, 2, 'RepairClaimBillForSupplier', 'Released', NULL, '维修索赔管理-供应商', '供应商复核，确认维修索赔单', 'Client/DCS/Images/Menu/Service/RepairClaimBill.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7308, 7300, 2, 'RepairItemQuery', 'Released', NULL, ' 标准维修工时代码查询', '查询维修项目分类与维修项目','Client/DCS/Images/Menu/ServiceBasicData/RepairItemQuery.png' ,86, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (7309, 7300, 2, 'MalfunctionQuery', 'Released', NULL, '故障代码查询', '查询故障现象分类及故障现象', 'Client/DCS/Images/Menu/ServiceBasicData/MalfunctionQuery.png', 9, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8307, 'ServiceTripClaimBillForSupplier|SupplierConfirm', '供应商确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8307, 'ServiceTripClaimBillForSupplier|SupplierCheck', '供应商复核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8307, 'ServiceTripClaimBillForSupplier|Detail', '查看详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8307, 'ServiceTripClaimBillForSupplier|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8311, 'RepairClaimBillForSupplier|SupplierConfirm', '供应商确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8311, 'RepairClaimBillForSupplier|SupplierCheck', '供应商复核', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8311, 'RepairClaimBillForSupplier|Detail', '查看详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8311, 'RepairClaimBillForSupplier|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8311, 'RepairClaimBillForSupplier|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7308, 'Common|Detail', '查看详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7309, 'Common|Detail', '查看详细', 2);


-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);

