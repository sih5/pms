--新增报表节点

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13304,13300, 2, 'DealerInAgencyAccountSta', 'Released', NULL, '服务站在代理库账面余额统计', '统计各服务站在代理库账面余额', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 6, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
