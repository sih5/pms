--新增报表节点
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13230,13200, 2, 'ClaimSupplierDetailsSplit', 'Released', NULL, '供应商索赔结算统计(拆分结算)', '统计期间内供应商索赔单信息', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 36, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13231,13200, 2, 'ClaimSettlementBillSplit', 'Released', NULL, '索赔结算报表(拆分结算)', '查询维修索赔单、索赔金额、外出金额等信息', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 37, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
