--供应商索赔结算指令

create sequence S_SupClaimSettleInstruction
/

/*==============================================================*/
/* Table: SupClaimSettleInstruction                             */
/*==============================================================*/
create table SupClaimSettleInstruction  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(200)                   not null,
   PartsSalesCategoryId NUMBER(9),
   ServiceProductLineId NUMBER(9),
   ProductLineType      NUMBER(9),
   Status               NUMBER(9)                       not null,
   SettlementStartTime  DATE,
   SettlementEndTime    DATE                            not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ExecutionTime        DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   ExecutionResult      VARCHAR2(200)
   constraint PK_SupClaimSettleInstruction primary key (Id)
)
/
