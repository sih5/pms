-- Create table
create table SALENOSETTLETABLE
(
  ID                     NUMBER(9) not null,
  STORAGECOMPANYCODE     VARCHAR2(50),
  PARTSSALESCATEGORYID   NUMBER(9),
  CODE                   VARCHAR2(50),
  WAREHOUSEID            NUMBER(9),
  WAREHOUSENAME          VARCHAR2(100),
  COUNTERPARTCOMPANYNAME VARCHAR2(100),
  OUTBOUNDTYPE           NUMBER(9),
  OUTBOUNDNAME           CHAR(8),
  ISSETTLED              CHAR(6),
  TOTAL                  NUMBER,
  TOTALJH                NUMBER,
  TOTALNOTAX             NUMBER,
  SOURCECODE             VARCHAR2(50),
  CREATETIME             DATE,
  SYNCDATE               VARCHAR2(100)
);
alter table SALENOSETTLETABLE
  add constraint PK_SALENOSETTLETABLE primary key (ID);
/
create sequence S_SALENOSETTLETABLE
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

create or replace procedure autoaddSALENOSETTLETABLE as
begin
  insert into SALENOSETTLETABLE
    select s_SALENOSETTLETABLE.Nextval, t.*
      from (select a.storagecompanycode,
                   a.partssalescategoryid,
                   a.code,
                   a.warehouseid,
                   a.warehousename,
                   a.Counterpartcompanyname,
                   a.outboundtype,
                   case a.outboundtype
                     when 1 then
                      '配件销售'
                     when 2 then
                      '采购退货'
                     when 3 then
                      '内部领出'
                     when 4 then
                      '配件调拨'
                   end as outboundname,
                   case a.settlementstatus
                     when 1 then
                      '不结算'
                     when 2 then
                      '可结算'
                   end as IsSettled,
                   round(sum(b.SettlementPrice * b.OutboundAmount), 2) as Total,
                   case
                     when a.createtime < to_date('2014-07-01', 'yyyy-mm-dd') then
                      round(sum(b.costprice * b.OutboundAmount) / 1.17, 2)
                     else
                      round(sum(b.costprice * b.OutboundAmount), 2)
                   end as TotalJH,
                   round(sum(b.costprice * b.OutboundAmount) / 1.17, 2) as TotalNoTax,
                   c.sourcecode,
                   a.createtime,
                   to_char(sysdate - 1, 'yyyy-mm') syncdate
              from partsoutboundbill a
             inner join partsoutboundbilldetail b
                on a.id = b.partsoutboundbillid
             inner join PartsOutboundPlan c
                on c.id = a.partsoutboundplanid
             inner join branch
                on a.storagecompanyid = branch.id
               and branch.status <> 99
             where a.SettlementStatus in (1, 2)
            -- and a.Storagecompanyid in(2,2203)
            --and a.Storagecompanyid in(2,2203)
             group by a.storagecompanycode,
                      a.partssalescategoryid,
                      a.code,
                      a.warehouseid,
                      a.warehousename,
                      a.Counterpartcompanyname,
                      a.outboundtype,
                      a.outboundtype,
                      a.settlementstatus,
                      c.sourcecode,
                      a.createtime
            union all
            select a.storagecompanycode,
                   a.partssalescategoryid,
                   a.code,
                   a.warehouseid,
                   a.warehousename,
                   a.Counterpartcompanyname,
                   a.outboundtype,
                   case a.outboundtype
                     when 1 then
                      '配件销售'
                     when 2 then
                      '采购退货'
                     when 3 then
                      '内部领出'
                     when 4 then
                      '配件调拨'
                   end as outboundname,
                   case a.settlementstatus
                     when 1 then
                      '不结算'
                     else
                      '可结算'
                   end as IsSettled,
                   round(sum(b.SettlementPrice * b.OutboundAmount), 2) as Total,
                   case
                     when a.createtime < to_date('2014-07-01', 'yyyy-mm-dd') then
                      round(sum(b.costprice * b.OutboundAmount) / 1.17, 2)
                     else
                      round(sum(b.costprice * b.OutboundAmount), 2)
                   end as TotalJH,
                   round(sum(b.costprice * b.OutboundAmount) / 1.17, 2) as TotalNoTax,
                   e.sourcecode,
                   a.createtime,
                   to_char(sysdate - 1, 'yyyy-mm') syncdate
              from partsoutboundbill a
             inner join partsoutboundbilldetail b
                on b.partsoutboundbillid = a.id
             inner join PartsSalesSettlementRef c
                on a.id = c.sourceid
               and c.SourceType = 1
             inner join partssalessettlement d
                on d.id = c.partssalessettlementid
             inner join partsoutboundplan e
                on e.id = a.partsoutboundplanid
             inner join branch
                on a.storagecompanyid = branch.id
               and branch.status <> 99
             where d.status in (1, 2)
            -- and a.StorageCompanyId in(2,2203)
            --and a.Storagecompanyid in(2,2203)
             group by a.storagecompanycode,
                      a.partssalescategoryid,
                      a.code,
                      a.warehouseid,
                      a.warehousename,
                      a.Counterpartcompanyname,
                      a.outboundtype,
                      a.outboundtype,
                      a.settlementstatus,
                      e.sourcecode,
                      a.createtime
            union all
            select a.storagecompanycode,
                   a.partssalescategoryid,
                   a.code,
                   a.warehouseid,
                   a.warehousename,
                   a.Counterpartcompanyname,
                   a.outboundtype,
                   case a.outboundtype
                     when 1 then
                      '配件销售'
                     when 2 then
                      '采购退货'
                     when 3 then
                      '内部领出'
                     when 4 then
                      '配件调拨'
                   end as outboundname,
                   case a.settlementstatus
                     when 1 then
                      '不结算'
                     else
                      '可结算'
                   end as IsSettled,
                   round(sum(b.SettlementPrice * b.OutboundAmount), 2) as Total,
                   case
                     when a.createtime < to_date('2014-07-01', 'yyyy-mm-dd') then
                      round(sum(b.costprice * b.OutboundAmount) / 1.17, 2)
                     else
                      round(sum(b.costprice * b.OutboundAmount), 2)
                   end as TotalJH,
                   round(sum(b.costprice * b.OutboundAmount) / 1.17, 2) as TotalNoTax,
                   e.sourcecode,
                   a.createtime,
                   to_char(sysdate - 1, 'yyyy-mm') syncdate
              from partsoutboundbill a
             inner join partsoutboundbilldetail b
                on b.partsoutboundbillid = a.id
             inner join partspurchasertnsettleref c
                on a.id = c.sourceid
             inner join PartsPurchaseRtnSettleBill d
                on d.id = c.PartsPurchaseRtnSettleBillId
             inner join partsoutboundplan e
                on e.id = a.partsoutboundplanid
             inner join branch
                on a.storagecompanyid = branch.id
               and branch.status <> 99
             where d.status in (1, 2)
            -- and a.StorageCompanyId in(2,2203)
            --and a.Storagecompanyid in(2,2203)
             group by a.storagecompanycode,
                      a.partssalescategoryid,
                      a.code,
                      a.warehouseid,
                      a.warehousename,
                      a.Counterpartcompanyname,
                      a.outboundtype,
                      a.outboundtype,
                      a.settlementstatus,
                      e.sourcecode,
                      a.createtime
            ----加上采购结算合并的采购退货出库
            union all
            select a.storagecompanycode,
                   a.partssalescategoryid,
                   a.code,
                   a.warehouseid,
                   a.warehousename,
                   a.Counterpartcompanyname,
                   a.outboundtype,
                   case a.outboundtype
                     when 1 then
                      '配件销售'
                     when 2 then
                      '采购退货'
                     when 3 then
                      '内部领出'
                     when 4 then
                      '配件调拨'
                   end as outboundname,
                   case a.settlementstatus
                     when 1 then
                      '不结算'
                     else
                      '可结算'
                   end as IsSettled,
                   round(sum(b.SettlementPrice * b.OutboundAmount), 2) as Total,
                   case
                     when a.createtime < to_date('2014-07-01', 'yyyy-mm-dd') then
                      round(sum(b.costprice * b.OutboundAmount) / 1.17, 2)
                     else
                      round(sum(b.costprice * b.OutboundAmount), 2)
                   end as TotalJH,
                   round(sum(b.costprice * b.OutboundAmount) / 1.17, 2) as TotalNoTax,
                   e.sourcecode,
                   a.createtime,
                   to_char(sysdate - 1, 'yyyy-mm') syncdate
              from partsoutboundbill a
             inner join partsoutboundbilldetail b
                on b.partsoutboundbillid = a.id
             inner join PartsPurchaseSettleRef c
                on a.id = c.sourceid
               and c.SourceType = 1
             inner join PartsPurchaseSettleBill d
                on d.id = c.PartsPurchaseSettleBillId
             inner join partsoutboundplan e
                on e.id = a.partsoutboundplanid
             inner join branch
                on a.storagecompanyid = branch.id
               and branch.status <> 99
             where d.status in (1, 2)
            -- and a.StorageCompanyId in(2,2203)
            -- and a.Storagecompanyid in(2,2203)
             group by a.storagecompanycode,
                      a.partssalescategoryid,
                      a.code,
                      a.warehouseid,
                      a.warehousename,
                      a.Counterpartcompanyname,
                      a.outboundtype,
                      a.outboundtype,
                      a.settlementstatus,
                      e.sourcecode,
                      a.createtime
            union all
            select a.storagecompanycode,
                   a.partssalescategoryid,
                   a.code,
                   a.warehouseid,
                   a.warehousename,
                   a.Counterpartcompanyname,
                   a.outboundtype,
                   case a.outboundtype
                     when 1 then
                      '配件销售'
                     when 2 then
                      '采购退货'
                     when 3 then
                      '内部领出'
                     when 4 then
                      '配件调拨'
                   end as outboundname,
                   case a.settlementstatus
                     when 1 then
                      '不结算'
                     else
                      '可结算'
                   end as IsSettled,
                   round(sum(b.SettlementPrice * b.OutboundAmount), 2) as Total,
                   case
                     when a.createtime < to_date('2014-07-01', 'yyyy-mm-dd') then
                      round(sum(b.costprice * b.OutboundAmount) / 1.17, 2)
                     else
                      round(sum(b.costprice * b.OutboundAmount), 2)
                   end as TotalJH,
                   round(sum(b.costprice * b.OutboundAmount) / 1.17, 2) as TotalNoTax,
                   e.sourcecode,
                   a.createtime,
                   to_char(sysdate - 1, 'yyyy-mm') syncdate
              from partsoutboundbill a
             inner join partsoutboundbilldetail b
                on b.partsoutboundbillid = a.id
             inner join PartsRequisitionSettleRef c
                on a.id = c.sourceid
               and c.SourceType = 1
             inner join PartsRequisitionSettleBill d
                on d.id = c.PartsRequisitionSettleBillId
             inner join partsoutboundplan e
                on e.id = a.partsoutboundplanid
             inner join branch
                on a.storagecompanyid = branch.id
               and branch.status <> 99
             where d.status = 1
            -- and a.StorageCompanyId in(2,2203)
            --  and a.Storagecompanyid in(2,2203)
             group by a.storagecompanycode,
                      a.partssalescategoryid,
                      a.code,
                      a.warehouseid,
                      a.warehousename,
                      a.Counterpartcompanyname,
                      a.outboundtype,
                      a.outboundtype,
                      a.settlementstatus,
                      e.sourcecode,
                      a.createtime) t;

end autoaddSALENOSETTLETABLE;
/

begin
  sys.dbms_scheduler.create_job(job_name            => '销售未结算月度结转',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'autoaddSALENOSETTLETABLE',
                                start_date          => to_date('01-04-2015 00:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Monthly;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
