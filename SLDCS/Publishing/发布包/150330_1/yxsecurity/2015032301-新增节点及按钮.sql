--新增供应商索赔结算指令管理节点
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8310, 8900, 2 ,'SupClaimSettleInstruction','Released', NULL, '供应商索赔结算指令管理','用于分公司维护、查看供应商索赔结算指令', 'Client/DCS/Images/Menu/Service/SupClaimSettleInstruction.png', 5, 2);
--新增配件返利变更明细查询-服务站节点
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6405,6400, 2, 'PartsRebateChangeDetailForDealerQuery', 'Released', NULL, '配件返利变更明细查询-服务站', '查询配件返利账户变更明细', 'Client/DCS/Images/Menu/Financial/PartsRebateChangeDetailQuery.png', 5, 2);
--新增节点服务站/专卖店企业信息管理-服务站
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1315,1300, 2, 'DealerForDealer', 'Released', NULL, '服务站/专卖店企业信息管理-服务站', '', 'Client/DCS/Images/Menu/Channels/Dealer.png', 14, 2);
 

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);



--供应商索赔结算指令管理节点按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8310, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8310, 'Common|Abandon', '作废', 2);

--计划价申请管理新增导出按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6101, 'Common|Export', '导出', 2);

--内部领入增加合并导出按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3301, 'Common|MergeExport', '合并导出', 2);
--配件返利变更明细查询-服务站增加导出按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6405, 'Common|Export', '导出', 2);
--收货管理-分公司增加导出按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1210, 'Common|Export', '导出', 2);

--服务站/专卖店企业信息管理-服务站节点按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1315, 'Common|EditTogether', '修改(集中)', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1315, 'Common|DetailTogether', '详细信息(集中)', 2);

--增加打印按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9701, 'Common|PartsTransferOrderPrint', '调拨单打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 9701, 'Common|CodePrint', '条形码打印', 2);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);

