--bo增加字段
    /*[!]<Table> 维修保养索赔单
      Columns:
        [+-]<Column> 供应商结算状态
    [!]<Table> 维修工单
      Columns:
        [+-]<Column> 下次跟踪时间
        [+-]<Column> 派工内容
        [+-]<Column> 是否最后一次回访
        [+-]<Column> 是否满意
        [+-]<Column> 不满意原因*/

--Alter Table RepairClaimBill Add  SupplierSettleStatus NUMBER(9);

Alter Table RepairWorkOrder Add    NextTime             DATE;
Alter Table RepairWorkOrder Add     TaskProject          VARCHAR2(500);
Alter Table RepairWorkOrder Add     IsLastVisitOut       NUMBER(1);
Alter Table RepairWorkOrder Add     IsSatisfy            NUMBER(9);
Alter Table RepairWorkOrder Add     NoSatisfyReason      VARCHAR2(200);
