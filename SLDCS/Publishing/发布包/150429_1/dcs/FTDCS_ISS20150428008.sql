create or replace view repairclaimbillwithsupplier as
select
     repairClaimBill.Id as ID,
     repairClaimBill.BranchId as BranchId,
     repairClaimBill.ProductLineType as ProductLineType,
     repairClaimBill.IfClaimToSupplier as IfClaimToSupplier,
     repairClaimBill.ClaimBillCode as ClaimBillCode,
     repairClaimBill.RepairContractCode as RepairContractCode,
     dsi.BusinessCode as CustomerCode,
     repairClaimBill.RepairWorkOrder as RepairWorkOrder,
     repairClaimBill.QualityInformationCode as QualityInformationCode,
     repairClaimBill.InitialApprovertComment as InitialApprovertComment,
     repairClaimBill.OtherCost as OtherCost,
     repairClaimBill.InitialApproverName as InitialApproverName,
     repairClaimBill.RepairType as RepairType,
     repairClaimBill.Status as Status,
     repairClaimBill.AutoApproveStatus as AutoApproveStatus,
     repairClaimBill.RepairClaimApplicationCode as RepairClaimApplicationCode,
     repairClaimBill.SupplierCheckStatus as SupplierCheckStatus,
     repairClaimBill.SupplierConfirmStatus as SupplierConfirmStatus,
     repairClaimBill.SettlementStatus as SettlementStatus,
     repairClaimBill.SupplierSettleStatus as SupplierSettleStatus,
     repairClaimBill.UsedPartsDisposalStatus as UsedPartsDisposalStatus,
     repairClaimBill.RejectQty as RejectQty,
     repairClaimBill.DealerId as DealerId,
     repairClaimBill.DealerCode as  DealerCode,
     repairClaimBill.DealerName as DealerName,
     ve.SerialNumber as SerialNumber,
     ve.SalesDate as SalesDate,
     repairClaimBill.Mileage as Mileage,
     repairClaimBill.WorkingHours as WorkingHours,
     repairClaimBill.Capacity as Capacity,
     sp.ProductLineId as ServiceProductLineId,
     sp.ProductLineCode as ServiceProductLineCode,
     sp.ProductLineName as ServiceProductLineName,
     ve.ProductCategoryName as VehicleType,
     repairClaimBill.ApplicationType as ApplicationType,
     at.ApplicationTypeName as ApplicationTypeName,
     repairClaimBill.MalfunctionId as MalfunctionId,
     repairClaimBill.MalfunctionCode as MalfunctionCode,
     repairClaimBill.MalfunctionDescription as MalfunctionDescription,
     repairClaimBill.MalfunctionReason as MalfunctionReason,
     repairClaimBill.FaultyPartsId as FaultyPartsId,
     repairClaimBill.FaultyPartsCode as FaultyPartsCode,
     repairClaimBill.FaultyPartsName as FaultyPartsName,
     repairClaimBill.ClaimSupplierId as ClaimSupplierId,
     repairClaimBill.ClaimSupplierCode as ClaimSupplierCode,
     repairClaimBill.ClaimSupplierName as ClaimSupplierName,
     repairClaimBill.ResponsibleUnitId as ResponsibleUnitId,
     ru.Code as responsibleUnitCode,
     ru.Name as responsibleUnitName,
     --工时费
     decode(k.IfSPByLabor,1,(select sum(DefaultLaborHour) from RepairClaimItemDetail where repairClaimBill.id=RepairClaimItemDetail.Repairclaimbillid) * nvl(k.LaborUnitPrice,0),
     (select sum(DefaultLaborHour*LaborUnitPrice) from RepairClaimItemDetail where repairClaimBill.id=RepairClaimItemDetail.Repairclaimbillid) * nvl(k.LaborCoefficient,0)) as LaborCost,

     --材料费
    ((select sum(t2.MaterialCost) from RepairClaimItemDetail t1
     inner join RepairClaimMaterialDetail t2 on t1.id=t2.repairclaimitemdetailid
     where repairClaimBill.id=t1.Repairclaimbillid) * nvl(k.PartsClaimCoefficient,0)) as MaterialCost,

     --配件管理费
     ((select sum(t2.MaterialCost) from RepairClaimItemDetail t1
     inner join RepairClaimMaterialDetail t2 on t1.id=t2.repairclaimitemdetailid
     where repairClaimBill.id=t1.Repairclaimbillid) * nvl(k.PartsClaimCoefficient,0) * l.rate+ (select sum(t2.MaterialCost) from RepairClaimItemDetail t1
     inner join RepairClaimMaterialDetail t2 on t1.id=t2.repairclaimitemdetailid
     where repairClaimBill.id=t1.Repairclaimbillid) * nvl(k.PartsClaimCoefficient,0) * nvl(k.oldparttranscoefficient,0)) as PartsManagementCost,

     --费用合计
     decode(k.IfSPByLabor,1,(((select sum(DefaultLaborHour) from RepairClaimItemDetail where repairClaimBill.id=RepairClaimItemDetail.Repairclaimbillid) * nvl(k.LaborUnitPrice,0))

     +((select sum(t2.MaterialCost) from RepairClaimItemDetail t1 inner join RepairClaimMaterialDetail t2 on t1.id=t2.repairclaimitemdetailid where repairClaimBill.id=t1.Repairclaimbillid)
     * nvl(k.PartsClaimCoefficient,0))

     +((select sum(t2.MaterialCost) from RepairClaimItemDetail t1
     inner join RepairClaimMaterialDetail t2 on t1.id=t2.repairclaimitemdetailid
     where repairClaimBill.id=t1.Repairclaimbillid) * nvl(k.PartsClaimCoefficient,0)  * l.rate+ (select sum(t2.MaterialCost) from RepairClaimItemDetail t1
     inner join RepairClaimMaterialDetail t2 on t1.id=t2.repairclaimitemdetailid
     where repairClaimBill.id=t1.Repairclaimbillid) * nvl(k.PartsClaimCoefficient,0) * nvl(k.oldparttranscoefficient,0))+nvl(repairClaimBill.Othercost,0)),

     ((select sum(DefaultLaborHour*LaborUnitPrice) from RepairClaimItemDetail where repairClaimBill.id=RepairClaimItemDetail.Repairclaimbillid) * nvl(k.LaborCoefficient,0))+
     ((select sum(t2.MaterialCost) from RepairClaimItemDetail t1
     inner join RepairClaimMaterialDetail t2 on t1.id=t2.repairclaimitemdetailid
     where repairClaimBill.id=t1.Repairclaimbillid) * nvl(k.PartsClaimCoefficient,0)) +  ((select sum(t2.MaterialCost) from RepairClaimItemDetail t1
     inner join RepairClaimMaterialDetail t2 on t1.id=t2.repairclaimitemdetailid
     where repairClaimBill.id=t1.Repairclaimbillid) * nvl(k.PartsClaimCoefficient,0) * l.rate+ (select sum(t2.MaterialCost) from RepairClaimItemDetail t1
     inner join RepairClaimMaterialDetail t2 on t1.id=t2.repairclaimitemdetailid
     where repairClaimBill.id=t1.Repairclaimbillid) * nvl(k.PartsClaimCoefficient,0) * nvl(k.oldparttranscoefficient,0))
     +nvl(repairClaimBill.Othercost,0)) as TotalAmount,


     repairClaimBill.Remark as Remark,
     repairClaimBill.MarketingDepartmentId as MarketingDepartmentId,
     md.Code as marketingDepartmentCode,
     md.Name as marketingDepartmentName,
     ve.BridgeType as BridgeType,
     repairClaimBill.PartsSalesCategoryId as PartsSalesCategoryId,
     pc.Name as PartsSalesCategoryName,
     repairClaimBill.EngineModel as EngineModel,
     repairClaimBill.EngineSerialNumber as EngineCode,
     repairClaimBill.CreatorId as CreatorId,
     repairClaimBill.CreatorName as CreatorName,
     repairClaimBill.CreateTime as CreateTime,
     repairClaimBill.ApproverId as ApproverId,
     repairClaimBill.ApproverName as ApproverName,
     repairClaimBill.ApproveTime as ApproveTime,
     repairClaimBill.FinalApproverId as FinalApproverId,
     repairClaimBill.FinalApproverName as FinalApproverName,
     repairClaimBill.FinalApproverTime as FinalApproveTime,
     repairClaimBill.SupplierConfirmComment as SupplierConfirmComment,
     repairClaimBill.EngineSerialNumber as EngineSerialNumber,
     repairClaimBill.ServiceDepartmentComment as ServiceDepartmentComment,
     repairClaimBill.RejectReason as RejectReason,
     repairClaimBill.VideoMobileNumber as VideoMobileNumber,
     repairClaimBill.MVSOutRange as MVSOutRange,
     repairClaimBill.MVSOutTime as MVSOutTime,
     repairClaimBill.MVSOutDistance as MVSOutDistance,
     repairClaimBill.MVSOutCoordinate as MVSOutCoordinate,
     rca.CheckComment as CheckComment,
     repairClaimBill.OutOfFactoryDate as OutOfFactoryDate,
     repairClaimBill.VehicleContactPerson as VehicleContactPerson,
     repairClaimBill.ContactPhone as ContactPhone,
     repairClaimBill.RepairRequestTime as RepairRequestTime
    
 from repairClaimBill 
left join  BranchSupplierRelation k  on  repairClaimBill.ClaimSupplierId= k.SupplierId 
and repairClaimBill.BranchId= k.BranchId and  repairClaimBill.PartsSalesCategoryId= k.PartsSalesCategoryId and k.status=1
left join PartsManagementCostRate l on k.PartsManagementCostGradeId = l.PartsManagementCostGradeId and l.status=1
left join ServiceProductLineView sp on repairClaimBill.ServiceProductLineId = sp.ProductLineId
left join VehicleInformation ve on repairClaimBill.VehicleId = ve.Id
left join SalesRegion sr on repairClaimBill.SalesRegionId = sr.Id and sr.status=1
left join MarketingDepartment md  on repairClaimBill.MarketingDepartmentId = md.Id and md.status=1
left join PartsSalesCategory pc on repairClaimBill.PartsSalesCategoryId = pc.Id and pc.status=1
left join ResponsibleUnit ru on repairClaimBill.ResponsibleUnitId = ru.Id and ru.status=1
left join ApplicationType at on repairClaimBill.BranchId = at.BranchId
left join RepairClaimApplication rca on repairClaimBill.RepairClaimApplicationId = rca.Id and rca.status<99
left join DealerServiceInfo dsi on repairClaimBill.DealerId= dsi.DealerId and repairClaimBill.PartsSalesCategoryId=dsi.PartsSalesCategoryId and  dsi.status=1


