--请分步执行

--1、备份待处理数据
create table tempdata.PartsRebateAccount_150429 as
select *
  from (select id,
               BranchId,
               AccountGroupId,
               CustomerCompanyId,
               CustomerCompanyCode,
               CustomerCompanyName,
               status,
               t.accountbalanceamount,
               round(t.accountbalanceamount, 2) rodAmount,
               CreatorId,
               CreatorName,
               CreateTime,
               ModifierId,
               ModifierName,
               ModifyTime
          from PartsRebateAccount t)
 where accountbalanceamount <> rodAmount;
 
 --2、将返利台账的后两位小数四舍五入清零
update PartsRebateAccount
   set accountbalanceamount = round(accountbalanceamount, 2)
 where id in (select id
                from (select id,
                             BranchId,
                             AccountGroupId,
                             CustomerCompanyCode,
                             CustomerCompanyName,
                             status,
                             t.accountbalanceamount,
                             round(t.accountbalanceamount, 2) rodAmount
                        from PartsRebateAccount t)
               where accountbalanceamount <> rodAmount);