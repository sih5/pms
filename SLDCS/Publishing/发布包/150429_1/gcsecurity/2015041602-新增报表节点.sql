--服务
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13232,13200, 2, 'CoverageSta', 'Released', NULL, '覆盖率统计报表', '根据服务网络所在省、市、区（县）及城市级别划分，统计各级城市服务网络覆盖率', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 38, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13233,13200, 2, 'CustomerOutServeLoyalSta', 'Released', NULL, '保外客户售后服务忠诚度分析表', '本报表是对各品牌提报的保外信息进行统计，按月度分析服务网络忠诚客户情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 39, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13234,13200, 2, 'CustomerChurnRateSta', 'Released', NULL, '客户流失率分析表', '本报表是对服务站客户流失率进行统计', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 40, 2);

--指标管理
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14605, 14600, 2, 'AllBrandPartSupCollectSta', 'Released', '', '各品牌现货供应情况月度汇总表', '', '', '各品牌现货供应情况月度汇总表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 51, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14606, 14600, 2, 'AllBrandPartTOCollectSta', 'Released', '', '各品牌配件周转情况汇总表', '', '', '各品牌配件周转情况汇总表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 52, 2);
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14607, 14600, 2, 'AllBrandPartTODetailSta', 'Released', '', '各品牌配件周转情况明细表', '', '', '各品牌配件周转情况明细表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 53, 2);

--储备管理
insert into Page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14701, 14700, 2, 'MonthDemandWaveCoeffSta', 'Released', '', '月度需求波动系数表（品牌）', '', '', '统计配件需求月度波动规律', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 13, 2);

--采购管理
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14101, 14100, 2, 'PartsInfoSta', 'Released', '', '配件基本属性统计表', '', '', '汇总每个配件的基本属性', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 12, 2);

--RDC管理
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14301, 14300, 2, 'AcrossBrandPartsTransferSta', 'Released', '', '跨品牌配件调拨报表', '', '', '统计代理库或服务站跨品牌配件调拨的清单', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 34, 2);

--服务站管理
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values (14401, 14400, 2, 'DealerROAcrossBrandSta', 'Released', '', '服务站维修单跨品牌配件使用报表', '', '', '统计在生成保内或保外维修单时跨品牌配件调拨的清单', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 39, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);