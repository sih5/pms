--节点更名

--Select * From page Where Id=1313;

Update Page
   Set Name        = '服务站/专卖店分品牌信息履历查询',
       Description = '查询服务站/专卖店分公司信息变更履历'
 Where Id = 1313;


--新增节点服务站基本信息履历查询
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1316,1300, 2, 'DealerHistoryQuery', 'Released', NULL, '服务站基本信息履历查询', '查询服务站基础信息变更履历', 'Client/DCS/Images/Menu/Channels/DealerServiceInfoForBranchHistory.png', 15, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);

