--外出索赔申请单 增加字段

alter table ServiceTripClaimApplication add    FaultyPartsId        NUMBER(9);
alter table ServiceTripClaimApplication add  FaultyPartsCode      VARCHAR2(50);
alter table ServiceTripClaimApplication add  FaultyPartsName      VARCHAR2(100);
alter table ServiceTripClaimApplication add FaultyPartsSupplierId NUMBER(9);
alter table ServiceTripClaimApplication add FaultyPartsSupplierCode VARCHAR2(50);
alter table ServiceTripClaimApplication add FaultyPartsSupplierName VARCHAR2(100);
