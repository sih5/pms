
alter table INTERNALACQUISITIONBILL add InStatus NUMBER(9) default 5 not null;
/
CREATE OR REPLACE Procedure AutoSetICBinstatus As
  Isallnew      Number(9);
  Isallfinish   Number(9);
  isPartfinish  Number(9);
  Isforcefinish Number(9);
  Iscanncel     Number(9);
Begin
  Declare
    Cursor Autoset Is(
      Select Id,code
        From INTERNALACQUISITIONBILL
       Where Status in( 2, 99)
         And Instatus In (5, 10));

  Begin
    For s_Autoset In Autoset Loop
      Begin
        select count(*)
          into Isallnew
          from partsinboundplan a
         where a.sourcecode = s_Autoset.code
           and a.status = 1;
        select count(*)
          into Isallfinish
          from partsinboundplan a
         where a.sourcecode = s_Autoset.code
           and a.status = 2;
        select count(*)
          into Isforcefinish
          from partsinboundplan a
         where a.sourcecode = s_Autoset.code
           and a.status = 3;
        select count(*)
          into isPartfinish
          from partsinboundplan a
         where a.sourcecode = s_Autoset.code
           and a.status = 4;
        select count(*)
          into Iscanncel
          from INTERNALACQUISITIONBILL a
         where a.code = s_Autoset.code
         and a.status = 99;

        if Isallnew > 0 then
          update INTERNALACQUISITIONBILL
             set InStatus = 5
           where INTERNALACQUISITIONBILL.ID = s_Autoset.id;
        end if;
        if Isallfinish > 0 then
          update INTERNALACQUISITIONBILL
             set InStatus = 15
           where INTERNALACQUISITIONBILL.ID = s_Autoset.id;
        end if;
        if Isforcefinish > 0 then
          update INTERNALACQUISITIONBILL
             set InStatus = 11
           where INTERNALACQUISITIONBILL.ID = s_Autoset.id;
        end if;
        if isPartfinish > 0 then
          update INTERNALACQUISITIONBILL
             set InStatus = 10
           where INTERNALACQUISITIONBILL.ID = s_Autoset.id;
        end if;
        if Iscanncel > 0 then
          update INTERNALACQUISITIONBILL
             set InStatus = 99
           where INTERNALACQUISITIONBILL.ID = s_Autoset.id;
        end if;
      end;
    end loop;
  end;
end;
/
begin
  sys.dbms_scheduler.create_job(job_name            => '内部领入自动设置入库状态',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoSetICBinstatus',
                                start_date          => to_date('07-04-2015 20:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
begin
  sys.dbms_scheduler.create_job(job_name            => '内部领入自动设置入库状态12点',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoSetICBinstatus',
                                start_date          => to_date('07-04-2015 12:20:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
