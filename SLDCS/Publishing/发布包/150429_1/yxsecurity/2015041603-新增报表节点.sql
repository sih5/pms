insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14601, 14600, 2, 'AllBrandStockCollectSta', 'Released', '', '各品牌超X月库存汇总表', '', '', '各品牌超X月库存汇总表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 47, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14602, 14600, 2, 'AllBrandPartSupplySta', 'Released', '', '各品牌配件现货供应情况月度明细表', '', '', '各品牌配件现货供应情况月度明细表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 48, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14603, 14600, 2, 'AllBrandPartSupplyYearSta', 'Released', '', '各品牌配件现货供应情况年度分析表', '', '', '各品牌配件现货供应情况年度分析表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 49, 2);


insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14604, 14600, 2, 'AllBrandPartSupplyCurrSta', 'Released', '', '各配件现货供应能力实时查询表', '', '', '各配件现货供应能力实时查询表', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 50, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
