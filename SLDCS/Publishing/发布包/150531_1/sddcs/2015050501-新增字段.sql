 --经销商关键岗位人员
--新增字段: 培训类型 ,培训时间,认证类型,认证时间
alter table DealerKeyEmployee add TrainingType         NUMBER(9);
alter table DealerKeyEmployee add TrainingTime         DATE;
alter table DealerKeyEmployee add AuthenticationType   NUMBER(9);
alter table DealerKeyEmployee add AuthenticationTime   DATE;    




-- 经销商关键岗位人员变更履历
--新增字段: 培训类型, 培训时间,认证类型, 认证时间
alter table DealerKeyEmployeeHistory add TrainingType         NUMBER(9);
alter table DealerKeyEmployeeHistory add  TrainingTime         DATE;
alter table DealerKeyEmployeeHistory add  AuthenticationType   NUMBER(9);
alter table DealerKeyEmployeeHistory add  AuthenticationTime   DATE;
