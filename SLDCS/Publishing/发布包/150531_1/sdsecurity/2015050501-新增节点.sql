--新增节点

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1317,1300, 2, 'AuthenticationType', 'Released', NULL, '认证类型管理', '维护认证类型', 'Client/DCS/Images/Menu/Common/CompanyInvoiceInfo.png', 16, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1318,1300, 2, 'TrainingType', 'Released', NULL, '培训类型管理', '维护培训类型', 'Client/DCS/Images/Menu/Common/Branch.png', 17, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1319,1300, 2, 'DealerPerTrainAut', 'Released', NULL, '服务站人员培训及认证维护（本部 事业本部）', '分公司查询及维护服务站人员培训及认证', 'Client/DCS/Images/Menu/Channels/DealerServiceInfoForBranchHistory.png', 18, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (1320,1300, 2, 'DealerPerTrainAutForDealer', 'Released', NULL, '服务站人员培训及认证查询（服务站）', '查询服务站人员培训及认证', 'Client/DCS/Images/Menu/Channels/DealerServiceInfoForBranch.png', 19, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1317, 'Common|Add', '新增', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1318, 'Common|Add', '新增', 2);
   
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1319, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1319, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1319, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1319, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1319, 'Common|Import', '导入', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2202, 'Common|ImportUpdate', '修改导入', 2);



-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
