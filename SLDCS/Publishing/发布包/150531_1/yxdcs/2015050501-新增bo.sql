--新增bo:
--服务站人员培训及认证
--认证类型
--培训类型


create sequence S_AuthenticationType
/
create sequence S_DealerPerTrainAut
/
create sequence S_TrainingType
/

/*==============================================================*/
/* Table:  AuthenticationType                                   */
/*==============================================================*/
create table  AuthenticationType   (
    Id                  NUMBER(9)                       not null,
    AuthenticationTypeName  VARCHAR2(50)                    not null,
    BranchId            NUMBER(9)                       not null,
    CreatorName         VARCHAR2(100),
    CreatorId           NUMBER(9),
    CreateTime          DATE,
   constraint PK_AUTHENTICATIONTYPE primary key ( Id )
)
/

/*==============================================================*/
/* Table:  DealerPerTrainAut                                    */
/*==============================================================*/
create table  DealerPerTrainAut   (
    Id                  NUMBER(9)                       not null,
    BranchId            NUMBER(9)                       not null,
    PartsSalesCategoryId  NUMBER(9)                       not null,
    MarketingDepartmentId  NUMBER(9)                       not null,
    DealerId            NUMBER(9)                       not null,
    DealerCode          VARCHAR2(50),
    DealerName          VARCHAR2(100),
    Name                VARCHAR2(100)                   not null,
    PositionId          NUMBER(9)                       not null,
    TrainingType        NUMBER(9),
    TrainingTime        DATE,
    AuthenticationType  NUMBER(9),
    AuthenticationTime  DATE,
    Status              NUMBER(9)                       not null,
    CreateTime          DATE,
    CreatorId           NUMBER(9),
    CreatorName         VARCHAR2(100),
    ModifierId          NUMBER(9),
    ModifyTime          DATE,
    ModifierName        VARCHAR2(100),
   constraint PK_DEALERPERTRAINAUT primary key ( Id )
)
/

/*==============================================================*/
/* Table:  TrainingType                                         */
/*==============================================================*/
create table  TrainingType   (
    Id                  NUMBER(9)                       not null,
    TrainingTypeName    VARCHAR2(50)                    not null,
    BranchId            NUMBER(9)                       not null,
    CreatorName         VARCHAR2(100),
    CreatorId           NUMBER(9),
    CreateTime          DATE,
   constraint PK_TRAININGTYPE primary key ( Id )
)
/
