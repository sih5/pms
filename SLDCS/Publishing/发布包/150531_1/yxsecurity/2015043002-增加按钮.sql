--增加按钮

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3302, 'Common|MergeExport', '合并导出', 2);

INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|RepairOrderPrintNew', '打印维修单(新)', 2);


-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
