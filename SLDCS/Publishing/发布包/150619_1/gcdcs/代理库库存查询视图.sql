      
--配件图号、配件名称、代理库编号、代理库名称、提报数量、代理库可用库存、库存满足数量、库存不满足数量      
      
create view AgencyStockQryFrmOrder 
as
select PartsSalesOrderId,PartsSalesOrderCode,SparePartCode,SparePartName,AgencyCode,AgencyName, OrderedQuantity,Usablequantity,
       case when Usablequantity>OrderedQuantity then OrderedQuantity else Usablequantity end as Fulfilquantity,
       case when Usablequantity>OrderedQuantity then 0 else OrderedQuantity-Usablequantity end as NoFulfilquantity
  from (  
  select ps.id as PartsSalesOrderId,ps.code as PartsSalesOrderCode,psd.sparepartid,psd.SparePartCode,psd.SparePartName,ar.AgencyCode,ar.AgencyName,
  sum(nvl(psd.OrderedQuantity,0)) as OrderedQuantity,sum(nvl(s.quantity,0)-nvl(pls.lockedquantity,0)-nvl(wv.CongelationStockQty,0)-nvl(wv.DisabledStock,0)) as Usablequantity
  from PartsSalesOrder ps
  inner join PartsSalesOrderDetail psd on ps.id = psd.partssalesorderid
  inner join AgencyDealerRelation ar on ps.SubmitCompanyId = ar.DealerId and ps.salescategoryid =ar.partssalesordertypeid
  left join PartsStock s on psd.sparepartid = s.partid and ps.WarehouseId = s.warehouseid and s.branchid = ps.branchid
  left join WarehouseAreaCategory kw on kw.id =s.WarehouseAreaCategoryId and  kw.category in (1,3)
  left join PartsLockedStock pls on pls.WarehouseId = s.warehouseid and s.partid =pls.partid
  left join WmsCongelationStockView wv on wv.WarehouseId = s.warehouseid and wv.SparePartId = s.partid
  where ps.status =2
  group by ps.id,ps.code,psd.sparepartid,psd.SparePartCode,psd.SparePartName,ar.AgencyCode,ar.AgencyName
  ) tmp
  where 1=1 


