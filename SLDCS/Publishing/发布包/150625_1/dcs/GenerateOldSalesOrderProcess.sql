create or replace function GenerateOldSalesOrderProcess(FOldDateBase      in varchar2,
                                                        FNewDateBase      in varchar2,
                                                        FBillSourceBillId in integer,
                                                        FProcessBillId    in integer,
                                                        FEnterpriseCode   in varchar2)
  return integer as
  SalesOrderBillQuantity integer;
  /*源配件销售订单Id*/
  FPartsSalesOrderId integer;
  /*原始配件销售订单Id*/
  FOldPartsSalesOrderId integer;
  /*客户账户ID*/
  FCustomerAccountId integer;
  /*编号模板Id*/
  FComplateId integer;
  /*源订单订货仓库id*/
  FWarehouseId   integer;
  FWarehouseCode varchar2(200);
  FWarehouseName varchar2(200);
  /*编码规则相关设置*/
  FDateFormat               date;
  FDateStr                  varchar2(8);
  FSerialLength             integer;
  FNewCode                  varchar2(50);
  FPartsSalesOrderProcessId integer;
  /*判断是否有任意一个配件不存在源库下*/
  FQuantity integer;
begin
  SalesOrderBillQuantity := 0;
  FDateFormat            := trunc(sysdate, 'DD');
  FDateStr               := to_char(sysdate, 'yyyymmdd');
  FSerialLength          := 6;
  FNewCode               := 'PSOP{CORPCODE}' || FDateStr || '{SERIAL}';
  begin
    IF FOldDateBase != 'YXDCS' AND FOldDateBase != 'SDDCS' AND
       FOldDateBase != 'GCDCS' AND FOldDateBase != 'DCS' THEN
      raise_application_error(-20002, '未找到对应的数据库。');
    END IF;
  
    IF FNewDateBase != 'YXDCS' AND FNewDateBase != 'SDDCS' AND
       FNewDateBase != 'GCDCS' AND FNewDateBase != 'DCS' THEN
      raise_application_error(-20002,
                              '未找到对应的分公司与数据库关联关系。');
    END IF;
  
    if FOldDateBase = 'YXDCS' AND FNewDateBase = 'SDDCS' then
      begin
        /*获取编号生成模板Id*/
        begin
          select Id
            into FComplateId
            from SDDCS.codetemplate
           where name = 'PartsSalesOrderProcess'
             and IsActived = 1;
        exception
          when NO_DATA_FOUND then
            raise_application_error(-20001,
                                    '未找到名称为"PartsSalesOrderProcess"的有效编码规则。');
          when others then
            raise;
        end;
        /*源订单订货仓库id*/
        begin
          select WarehouseId, CustomerAccountId, Id
            into FWarehouseId, FCustomerAccountId, FPartsSalesOrderId
            from SDDCS.partssalesorder
           where Id = FBillSourceBillId;
        exception
          when NO_DATA_FOUND then
            raise_application_error(-20001,
                                    '未找到名称为有效的源订单仓库ID。');
          when others then
            raise;
        end;
        /*获取原始配件销售订单ID*/
        begin
          select Id
            into FOldPartsSalesOrderId
            from yxdcs.partssalesorder
           where sourcebillId = FBillSourceBillId;
        exception
          when NO_DATA_FOUND then
            raise_application_error(-20001,
                                    '未找到名称为有效的原始销售订单。');
          when others then
            raise;
        end;
      
        begin
          select Code, Name
            into FWarehouseCode, FWarehouseName
            from SDDCS.warehouse a
           where id = FWarehouseId;
        exception
          when NO_DATA_FOUND then
            raise_application_error(-20001, '未找到源订单仓库信息。');
          when others then
            raise;
        end;
        declare
          cursor Cursor_role is(
            select Id,
                   regexp_replace(regexp_replace(FNewCode,
                                                 '{CORPCODE}',
                                                 FEnterpriseCode),
                                  '{SERIAL}',
                                  lpad(to_char(GetSerialForDataBase(FComplateId,
                                                                    FDateFormat,
                                                                    FEnterpriseCode,
                                                                    FNewDateBase)),
                                       FSerialLength,
                                       '0')) Code,
                   OriginalSalesOrderId,
                   BillStatusBeforeProcess,
                   BillStatusAfterProcess,
                   RequestedShippingTime,
                   RequestedDeliveryTime,
                   CurrentFulfilledAmount,
                   ApprovalComment,
                   ShippingMethod,
                   ShippingInformationRemark,
                   Remark
              from yxdcs.partssalesorderprocess
             where id = FProcessBillId);
        begin
          for s_role in Cursor_role loop
          
            FPartsSalesOrderProcessId := SDDCS.S_PARTSSALESORDERPROCESS.Nextval;
            if S_role.Id > 0 then
              begin
                begin
                  select count(*)
                    into FQuantity
                    from yxdcs.partssalesorderprocessdetail a
                   where a.partssalesorderprocessid = s_role.Id
                     and not exists (select 1
                            from sddcs.sparepart b
                           where a.sparepartcode = b.code
                             and b.status = 1);
                
                  if FQuantity > 0 then
                    -- begin
                    raise_application_error(-20001,
                                            '存在配件编号不存在源库中。');
                    --end;
                  end if;
                end;
                /*主单生成*/
                insert into SDDCS.Partssalesorderprocess
                  (Id,
                   Code,
                   OriginalSalesOrderId,
                   BillStatusBeforeProcess,
                   BillStatusAfterProcess,
                   RequestedShippingTime,
                   RequestedDeliveryTime,
                   CurrentFulfilledAmount,
                   ApprovalComment,
                   ShippingMethod,
                   ShippingInformationRemark,
                   Remark)
                values
                  (FPartsSalesOrderProcessId,
                   s_role.Code,
                   FBillSourceBillId,
                   s_role.BillStatusBeforeProcess,
                   s_role.BillStatusAfterProcess,
                   s_role.RequestedShippingTime,
                   s_role.RequestedDeliveryTime,
                   --s_role.CurrentFulfilledAmount,
                   (SELECT SUM(A.CURRENTFULFILLEDQUANTITY *
                               (SELECT C.ORDERPRICE
                                  FROM SDDCS.PARTSSALESORDERDETAIL C
                                 WHERE C.PARTSSALESORDERID =
                                       FPartsSalesOrderId
                                   AND C.SPAREPARTCODE = A.SPAREPARTCODE))
                      FROM yxdcs.partssalesorderprocessdetail A
                     WHERE A.PARTSSALESORDERPROCESSID = s_role.Id),
                   s_role.ApprovalComment,
                   s_role.ShippingMethod,
                   s_role.ShippingInformationRemark,
                   s_role.Remark);
                /*清单生成*/
              
                insert into SDDCS.PARTSSALESORDERPROCESSDETAIL
                  (Id,
                   PartsSalesOrderProcessId,
                   WarehouseId,
                   WarehouseCode,
                   WarehouseName,
                   SparePartId,
                   SparePartCode,
                   SparePartName,
                   NewPartId,
                   NewPartCode,
                   NewPartName,
                   IfDirectSupply,
                   MeasureUnit,
                   OrderedQuantity,
                   CurrentFulfilledQuantity,
                   OrderPrice,
                   OrderProcessStatus,
                   OrderProcessMethod)
                  (select SDDCS.S_PARTSSALESORDERPROCESSDETAIL.NEXTVAL,
                          FPartsSalesOrderProcessId,
                          FWarehouseId,
                          FWarehouseCode,
                          FWarehouseName,
                          (select a.Id
                             from sddcs.sparepart a
                            where a.code = SparePartCode
                              and a.status = 1),
                          SparePartCode,
                          SparePartName,
                          NewPartId,
                          NewPartCode,
                          NewPartName,
                          IfDirectSupply,
                          MeasureUnit,
                          OrderedQuantity,
                          CurrentFulfilledQuantity,
                          (select ORDERPRICE
                             from SDDCS.PARTSSALESORDERDETAIL C
                            WHERE C.PARTSSALESORDERID = FPartsSalesOrderId
                              AND C.SPAREPARTCODE = t.SparePartCode),
                          OrderProcessStatus,
                          OrderProcessMethod
                     From yxdcs.partssalesorderprocessdetail t
                    where t.partssalesorderprocessid = s_role.Id);
                /*同步更新配件销售订单清单数量和主单状态*/
                update SDDCS.PARTSSALESORDER b
                   SET b.STATUS =
                       (SELECT A.STATUS
                          FROM YXDCS.PARTSSALESORDER A
                         WHERE A.ID = FOldPartsSalesOrderId
                           AND ROWNUM = 1)
                 where b.id = FPartsSalesOrderId;
                UPDATE SDDCS.PARTSSALESORDERDETAIL B
                   SET B.ApproveQuantity =
                       (SELECT A.ApproveQuantity
                          FROM YXDCS.PARTSSALESORDERDETAIL A
                         WHERE A.PARTSSALESORDERID = FOldPartsSalesOrderId
                           AND B.PARTSSALESORDERID = FPartsSalesOrderId
                           AND A.Sparepartcode = B.Sparepartcode)
                 where B.partssalesorderid = FPartsSalesOrderId;
                UPDATE SDDCS.Customeraccount A
                   SET A.PendingAmount = A.PENDINGAMOUNT +
                                         (Select SUM(B.OrderPrice *
                                                     B.CURRENTFULFILLEDQUANTITY)
                                            from SDDCS.PARTSSALESORDERPROCESSDETAIL B
                                           WHERE B.PARTSSALESORDERPROCESSID =
                                                 FPartsSalesOrderProcessId
                                             and B.Orderprocessmethod = 2)
                 where A.ID = FCustomerAccountId;
              end;
            end if;
          
          end loop;
        end;
      
      end;
    end if;
    IF FOldDateBase = 'YXDCS' AND FNewDateBase = 'GCDCS' then
      begin
        /*获取编号生成模板Id*/
        begin
          select Id
            into FComplateId
            from GCDCS.codetemplate
           where name = 'PartsSalesOrderProcess'
             and IsActived = 1;
        exception
          when NO_DATA_FOUND then
            raise_application_error(-20001,
                                    '未找到名称为"PartsSalesOrderProcess"的有效编码规则。');
          when others then
            raise;
        end;
        /*源订单订货仓库id*/
        begin
          select WarehouseId, CustomerAccountId, Id
            into FWarehouseId, FCustomerAccountId, FPartsSalesOrderId
            from GCDCS.partssalesorder
           where id = FBillSourceBillId;
        exception
          when NO_DATA_FOUND then
            raise_application_error(-20001,
                                    '未找到名称为有效的源订单仓库ID。');
          when others then
            raise;
        end;
      
        /*获取原始配件销售订单ID*/
        begin
          select Id
            into FOldPartsSalesOrderId
            from yxdcs.partssalesorder
           where sourcebillid = FBillSourceBillId;
        exception
          when NO_DATA_FOUND then
            raise_application_error(-20001,
                                    '未找到名称为有效的原始销售订单。');
          when others then
            raise;
        end;
      
        begin
          select Code, Name
            into FWarehouseCode, FWarehouseName
            from GCDCS.warehouse a
           where id = FWarehouseId;
        exception
          when NO_DATA_FOUND then
            raise_application_error(-20001, '未找到源订单仓库信息。');
          when others then
            raise;
        end;
        declare
          cursor Cursor_role is(
            select Id,
                   regexp_replace(regexp_replace(FNewCode,
                                                 '{CORPCODE}',
                                                 FEnterpriseCode),
                                  '{SERIAL}',
                                  lpad(to_char(GetSerialForDataBase(FComplateId,
                                                                    FDateFormat,
                                                                    FEnterpriseCode,
                                                                    FNewDateBase)),
                                       FSerialLength,
                                       '0')) Code,
                   OriginalSalesOrderId,
                   BillStatusBeforeProcess,
                   BillStatusAfterProcess,
                   RequestedShippingTime,
                   RequestedDeliveryTime,
                   CurrentFulfilledAmount,
                   ApprovalComment,
                   ShippingMethod,
                   ShippingInformationRemark,
                   Remark
              from yxdcs.partssalesorderprocess
             where id = FProcessBillId);
        begin
          for s_role in Cursor_role loop
          
            FPartsSalesOrderProcessId := GCDCS.S_PARTSSALESORDERPROCESS.Nextval;
            if S_role.Id > 0 then
              begin
                 begin
                  select count(*)
                    into FQuantity
                    from yxdcs.partssalesorderprocessdetail a
                   where a.partssalesorderprocessid = s_role.Id
                     and not exists (select 1
                            from sddcs.sparepart b
                           where a.sparepartcode = b.code
                             and b.status = 1);
                
                  if FQuantity > 0 then
                    -- begin
                    raise_application_error(-20001,
                                            '存在配件编号不存在源库中。');
                    --end;
                  end if;
                end;
                /*主单生成*/
                insert into GCDCS.Partssalesorderprocess
                  (Id,
                   Code,
                   OriginalSalesOrderId,
                   BillStatusBeforeProcess,
                   BillStatusAfterProcess,
                   RequestedShippingTime,
                   RequestedDeliveryTime,
                   CurrentFulfilledAmount,
                   ApprovalComment,
                   ShippingMethod,
                   ShippingInformationRemark,
                   Remark)
                values
                  (FPartsSalesOrderProcessId,
                   s_role.Code,
                   FBillSourceBillId,
                   s_role.BillStatusBeforeProcess,
                   s_role.BillStatusAfterProcess,
                   s_role.RequestedShippingTime,
                   s_role.RequestedDeliveryTime,
                   --s_role.CurrentFulfilledAmount,
                   (SELECT SUM(A.CURRENTFULFILLEDQUANTITY *
                               (SELECT C.ORDERPRICE
                                  FROM GCDCS.PARTSSALESORDERDETAIL C
                                 WHERE C.PARTSSALESORDERID =
                                       FPartsSalesOrderId
                                   AND C.SPAREPARTCODE = A.SPAREPARTCODE))
                      FROM yxdcs.partssalesorderprocessdetail A
                     WHERE A.PARTSSALESORDERPROCESSID = s_role.Id),
                   s_role.ApprovalComment,
                   s_role.ShippingMethod,
                   s_role.ShippingInformationRemark,
                   s_role.Remark);
                /*清单生成*/
                insert into GCDCS.PARTSSALESORDERPROCESSDETAIL
                  (Id,
                   PartsSalesOrderProcessId,
                   WarehouseId,
                   WarehouseCode,
                   WarehouseName,
                   SparePartId,
                   SparePartCode,
                   SparePartName,
                   NewPartId,
                   NewPartCode,
                   NewPartName,
                   IfDirectSupply,
                   MeasureUnit,
                   OrderedQuantity,
                   CurrentFulfilledQuantity,
                   OrderPrice,
                   OrderProcessStatus,
                   OrderProcessMethod)
                  (select GCDCS.S_PARTSSALESORDERPROCESSDETAIL.NEXTVAL,
                          FPartsSalesOrderProcessId,
                          FWarehouseId,
                          FWarehouseCode,
                          FWarehouseName,
                          --SparePartId,
                          (select a.Id
                             from sddcs.sparepart a
                            where a.code = SparePartCode
                              and a.status = 1),
                          SparePartCode,
                          SparePartName,
                          NewPartId,
                          NewPartCode,
                          NewPartName,
                          IfDirectSupply,
                          MeasureUnit,
                          OrderedQuantity,
                          CurrentFulfilledQuantity,
                          --OrderPrice,
                          (select ORDERPRICE
                             from GCDCS.PARTSSALESORDERDETAIL C
                            WHERE C.PARTSSALESORDERID = FPartsSalesOrderId
                              AND C.SPAREPARTCODE = t.SparePartCode),
                          OrderProcessStatus,
                          OrderProcessMethod
                     From yxdcs.partssalesorderprocessdetail t
                    where t.partssalesorderprocessid = s_role.Id);
                /*同步更新配件销售订单清单数量和主单状态*/
                update GCDCS.PARTSSALESORDER b
                   SET b.STATUS =
                       (SELECT A.STATUS
                          FROM YXDCS.PARTSSALESORDER A
                         WHERE A.ID = FOldPartsSalesOrderId
                           AND ROWNUM = 1)
                 where b.id = FPartsSalesOrderId;
                UPDATE GCDCS.PARTSSALESORDERDETAIL B
                   SET B.ApproveQuantity =
                       (SELECT A.ApproveQuantity
                          FROM YXDCS.PARTSSALESORDERDETAIL A
                         WHERE A.PARTSSALESORDERID = FOldPartsSalesOrderId
                           AND B.PARTSSALESORDERID = FPartsSalesOrderId
                           AND A.Sparepartcode = B.Sparepartcode)
                 where B.partssalesorderid = FPartsSalesOrderId;
                UPDATE GCDCS.Customeraccount A
                   SET A.PendingAmount = A.PENDINGAMOUNT +
                                         (Select SUM(B.OrderPrice *
                                                     B.CURRENTFULFILLEDQUANTITY)
                                            from YXDCS.PARTSSALESORDERPROCESSDETAIL B
                                           WHERE B.PARTSSALESORDERPROCESSID =
                                                 FPartsSalesOrderProcessId
                                             and B.Orderprocessmethod = 2)
                 where A.ID = FCustomerAccountId;
              end;
            end if;
          
          end loop;
        end;
      
      end;
    
    END IF;
    if FOldDateBase = 'YXDCS' AND FNewDateBase = 'DCS' then
      begin
        /*获取编号生成模板Id*/
        begin
          select Id
            into FComplateId
            from DCS.codetemplate
           where name = 'PartsSalesOrderProcess'
             and IsActived = 1;
        exception
          when NO_DATA_FOUND then
            raise_application_error(-20001,
                                    '未找到名称为"PartsSalesOrderProcess"的有效编码规则。');
          when others then
            raise;
        end;
        /*源订单订货仓库id*/
        begin
          select WarehouseId, CustomerAccountId, Id
            into FWarehouseId, FCustomerAccountId, FPartsSalesOrderId
            from DCS.partssalesorder
           where id = FBillSourceBillId;
        exception
          when NO_DATA_FOUND then
            raise_application_error(-20001,
                                    '未找到名称为有效的源订单仓库ID。');
          when others then
            raise;
        end;
      
        /*获取原始配件销售订单ID*/
        begin
          select Id
            into FOldPartsSalesOrderId
            from yxdcs.partssalesorder
           where sourcebillid = FBillSourceBillId;
        exception
          when NO_DATA_FOUND then
            raise_application_error(-20001,
                                    '未找到名称为有效的原始销售订单。');
          when others then
            raise;
        end;
      
        begin
          select Code, Name
            into FWarehouseCode, FWarehouseName
            from DCS.warehouse a
           where id = FWarehouseId;
        exception
          when NO_DATA_FOUND then
            raise_application_error(-20001, '未找到源订单仓库信息。');
          when others then
            raise;
        end;
        declare
          cursor Cursor_role is(
            select Id,
                   regexp_replace(regexp_replace(FNewCode,
                                                 '{CORPCODE}',
                                                 FEnterpriseCode),
                                  '{SERIAL}',
                                  lpad(to_char(GetSerialForDataBase(FComplateId,
                                                                    FDateFormat,
                                                                    FEnterpriseCode,
                                                                    FNewDateBase)),
                                       FSerialLength,
                                       '0')) Code,
                   OriginalSalesOrderId,
                   BillStatusBeforeProcess,
                   BillStatusAfterProcess,
                   RequestedShippingTime,
                   RequestedDeliveryTime,
                   CurrentFulfilledAmount,
                   ApprovalComment,
                   ShippingMethod,
                   ShippingInformationRemark,
                   Remark
              from yxdcs.partssalesorderprocess
             where id = FProcessBillId);
        begin
          for s_role in Cursor_role loop
          
            FPartsSalesOrderProcessId := DCS.S_PARTSSALESORDERPROCESS.Nextval;
            if S_role.Id > 0 then
              begin
               begin
                  select count(*)
                    into FQuantity
                    from yxdcs.partssalesorderprocessdetail a
                   where a.partssalesorderprocessid = s_role.Id
                     and not exists (select 1
                            from sddcs.sparepart b
                           where a.sparepartcode = b.code
                             and b.status = 1);
                
                  if FQuantity > 0 then
                    -- begin
                    raise_application_error(-20001,
                                            '存在配件编号不存在源库中。');
                    --end;
                  end if;
                end;
                /*主单生成*/
                insert into DCS.Partssalesorderprocess
                  (Id,
                   Code,
                   OriginalSalesOrderId,
                   BillStatusBeforeProcess,
                   BillStatusAfterProcess,
                   RequestedShippingTime,
                   RequestedDeliveryTime,
                   CurrentFulfilledAmount,
                   ApprovalComment,
                   ShippingMethod,
                   ShippingInformationRemark,
                   Remark)
                values
                  (FPartsSalesOrderProcessId,
                   s_role.Code,
                   FBillSourceBillId,
                   s_role.BillStatusBeforeProcess,
                   s_role.BillStatusAfterProcess,
                   s_role.RequestedShippingTime,
                   s_role.RequestedDeliveryTime,
                   --s_role.CurrentFulfilledAmount,
                   (SELECT SUM(A.CURRENTFULFILLEDQUANTITY *
                               (SELECT C.ORDERPRICE
                                  FROM DCS.PARTSSALESORDERDETAIL C
                                 WHERE C.PARTSSALESORDERID =
                                       FPartsSalesOrderId
                                   AND C.SPAREPARTCODE = A.SPAREPARTCODE))
                      FROM yxdcs.partssalesorderprocessdetail A
                     WHERE A.PARTSSALESORDERPROCESSID = s_role.Id),
                   s_role.ApprovalComment,
                   s_role.ShippingMethod,
                   s_role.ShippingInformationRemark,
                   s_role.Remark);
                /*清单生成*/
                insert into DCS.PARTSSALESORDERPROCESSDETAIL
                  (Id,
                   PartsSalesOrderProcessId,
                   WarehouseId,
                   WarehouseCode,
                   WarehouseName,
                   SparePartId,
                   SparePartCode,
                   SparePartName,
                   NewPartId,
                   NewPartCode,
                   NewPartName,
                   IfDirectSupply,
                   MeasureUnit,
                   OrderedQuantity,
                   CurrentFulfilledQuantity,
                   OrderPrice,
                   OrderProcessStatus,
                   OrderProcessMethod)
                  (select DCS.S_PARTSSALESORDERPROCESSDETAIL.NEXTVAL,
                          FPartsSalesOrderProcessId,
                          FWarehouseId,
                          FWarehouseCode,
                          FWarehouseName,
                          --SparePartId,
                          (select a.Id
                             from sddcs.sparepart a
                            where a.code = SparePartCode
                              and a.status = 1),
                          SparePartCode,
                          SparePartName,
                          NewPartId,
                          NewPartCode,
                          NewPartName,
                          IfDirectSupply,
                          MeasureUnit,
                          OrderedQuantity,
                          CurrentFulfilledQuantity,
                          --OrderPrice,
                          (select ORDERPRICE
                             from DCS.PARTSSALESORDERDETAIL C
                            WHERE C.PARTSSALESORDERID = FPartsSalesOrderId
                              AND C.SPAREPARTCODE = t.SparePartCode),
                          OrderProcessStatus,
                          OrderProcessMethod
                     From yxdcs.partssalesorderprocessdetail t
                    where t.partssalesorderprocessid = s_role.Id);
                /*同步更新配件销售订单清单数量和主单状态*/
                update DCS.PARTSSALESORDER b
                   SET b.STATUS =
                       (SELECT A.STATUS
                          FROM YXDCS.PARTSSALESORDER A
                         WHERE A.ID = FOldPartsSalesOrderId
                           AND ROWNUM = 1)
                 where b.id = FPartsSalesOrderId;
                UPDATE DCS.PARTSSALESORDERDETAIL B
                   SET B.ApproveQuantity =
                       (SELECT A.ApproveQuantity
                          FROM YXDCS.PARTSSALESORDERDETAIL A
                         WHERE A.PARTSSALESORDERID = FOldPartsSalesOrderId
                           AND B.PARTSSALESORDERID = FPartsSalesOrderId
                           AND A.Sparepartcode = B.Sparepartcode)
                 where B.partssalesorderid = FPartsSalesOrderId;
                UPDATE DCS.Customeraccount A
                   SET A.PendingAmount = A.PENDINGAMOUNT +
                                         (Select SUM(B.OrderPrice *
                                                     B.CURRENTFULFILLEDQUANTITY)
                                            from YXDCS.PARTSSALESORDERPROCESSDETAIL B
                                           WHERE B.PARTSSALESORDERPROCESSID =
                                                 FPartsSalesOrderProcessId
                                             and B.Orderprocessmethod = 2)
                 where A.ID = FCustomerAccountId;
              end;
            end if;
          
          end loop;
        end;
      
      end;
    
    end if;
  end;
  return(SalesOrderBillQuantity);
end;
