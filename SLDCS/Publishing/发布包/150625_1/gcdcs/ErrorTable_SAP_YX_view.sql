
create or replace View view_errortable_sap_yx as
select errortable_sap_yx.*,
  partssalescategory.name as brandname,--品牌名称
  partsinboundcheckbill.CounterpartCompanyName as customername,--对方单位名称
  partsinboundcheckbill.CreatorName as billCreatename--创建人
  from errortable_sap_yx
 inner join partsinboundcheckbill
    on errortable_sap_yx.bussinesscode = partsinboundcheckbill.code
   and errortable_sap_yx.synctypename in (6,9)--(销售退货入库，采购入库)
 inner join partssalescategory
    on partsinboundcheckbill.partssalescategoryid = partssalescategory.id
    
  union all
   select errortable_sap_yx.*,
      partssalescategory.name as brandname,--品牌名称
      partspurchasesettlebill.PartsSupplierName as customername, --对方单位名称
      partspurchasesettlebill.CreatorName as billCreatename--创建人  
  from errortable_sap_yx
 inner join partspurchasesettlebill
    on errortable_sap_yx.bussinesscode = partspurchasesettlebill.code
   and errortable_sap_yx.synctypename in (8,17,18)--（采购结算，采购结算供应商发票，一采购结算单多发票）
 inner join partssalescategory
    on partspurchasesettlebill.partssalescategoryid = partssalescategory.id
   
   union all
   select errortable_sap_yx.*,
   partssalescategory.name as brandname ,--品牌名称
   partssalessettlement.CustomerCompanyName as customername,---客户名称
   partssalessettlement.Creatorname as billCreatename--创建人名称
   from errortable_sap_yx 
   inner join partssalessettlement on errortable_sap_yx.bussinesscode=partssalessettlement.code
   and errortable_sap_yx.synctypename in (2,14)--（销售结算单成本结转，销售结算）
   inner join partssalescategory on partssalessettlement.partssalescategoryid = partssalescategory.id 
   
   union all 
   select errortable_sap_yx.*,
   partssalescategory.name as brandname,--品牌名称
   partsoutboundbill.CounterpartCompanyName as customername,--客户名称
   partsoutboundbill.CreatorName as billCreatename--创建人名称
   from errortable_sap_yx
   inner join partsoutboundbill on errortable_sap_yx.bussinesscode=partsoutboundbill.code
   and errortable_sap_yx.synctypename in(5,10)---（采购退货出库，销售出库）
   inner join partssalescategory on partsoutboundbill.partssalescategoryid=partssalescategory.id
  
   union all 
   select errortable_sap_yx.*,
   partssalescategory.name as brandname,--品牌名称
   partssalesrtnsettlement.CustomerCompanyName as customername,--客户名称
   partssalesrtnsettlement.CreatorName as billCreatename--创建人名称
   from errortable_sap_yx
   inner join partssalesrtnsettlement on errortable_sap_yx.bussinesscode=partssalesrtnsettlement.code
   and errortable_sap_yx.synctypename in(1,4,15)-----（销售退货结算单成本结转，销售退货结算，销售退货结算开红票）
   inner join partssalescategory on partssalesrtnsettlement.partssalescategoryid=partssalescategory.id
   
   union all
   select errortable_sap_yx.*,
   partssalescategory.name as brandname,--品牌名称
   partspurchasertnsettlebill.Partssuppliername as customername,--客户名称
   partspurchasertnsettlebill.CreatorName as billCreatename---创建人名称
   from errortable_sap_yx
   inner join partspurchasertnsettlebill on errortable_sap_yx.bussinesscode=partspurchasertnsettlebill.code
   and errortable_sap_yx.synctypename in (3,16)-----（采购退货结算，采购退货结算开红票）
   inner join partssalescategory on partspurchasertnsettlebill.partssalescategoryid=partssalescategory.id
   
      
   union all
   select errortable_sap_yx.*,
   partssalescategory.name as brandname,--品牌名称
   partsrequisitionsettlebill.DepartmentName as customername,--客户名称
   partsrequisitionsettlebill.CreatorName as billCreatename---创建人名称
   from errortable_sap_yx
   inner join partsrequisitionsettlebill on errortable_sap_yx.bussinesscode=partsrequisitionsettlebill.code
   and errortable_sap_yx.synctypename=7---（领用结算）
   inner join partssalescategory on partsrequisitionsettlebill.partssalescategoryid=partssalescategory.id
   
   union all
   select errortable_sap_yx.*,
   partssalescategory.name as brandname,--品牌名称
   partsinventorybill.WarehouseName as customername,--客户名称
   partsinventorybill.CreatorName as billCreatename---创建人名称
   from errortable_sap_yx
   inner join partsinventorybill on errortable_sap_yx.bussinesscode=partsinventorybill.code
   and errortable_sap_yx.synctypename=12----盘点
   inner join SalesUnitAffiWarehouse on PartsInventoryBill.WarehouseId=SalesUnitAffiWarehouse.WarehouseId
   inner join SalesUnit on SalesUnitAffiWarehouse.SalesUnitId=SalesUnit.id
   inner join partssalescategory on SalesUnit.PartsSalesCategoryId=partssalescategory.id
   
   union all
   select errortable_sap_yx.*,
   partssalescategory.name as brandname,--品牌名称
   plannedpriceapp.OwnerCompanyName as customername,--客户名称
   plannedpriceapp.CreatorName as billCreatename---创建人名称
   from errortable_sap_yx
   inner join plannedpriceapp on errortable_sap_yx.bussinesscode=plannedpriceapp.code
   and errortable_sap_yx.synctypename=13---成本变更
   inner join partssalescategory on plannedpriceapp.partssalescategoryid=partssalescategory.id
   
   union all
   select errortable_sap_yx.*,
   partssalescategory.name as brandname,--品牌名称
   partstransferorder.OriginalWarehouseName as customername,--客户名称
   partstransferorder.CreatorName as billCreatename---创建人名称
   from errortable_sap_yx
   inner join partstransferorder on errortable_sap_yx.bussinesscode=partstransferorder.code
   and errortable_sap_yx.synctypename=11----调拨
   inner join SalesUnitAffiWarehouse on PartsTransferOrder.OriginalWarehouseId=SalesUnitAffiWarehouse.Warehouseid
   inner join  SalesUnit on SalesUnitAffiWarehouse.SalesUnitId=SalesUnit.id
   inner join partssalescategory on  SalesUnit.PartsSalesCategoryId=partssalescategory.id
   
   union all
   select errortable_sap_yx.*,
   partssalescategory.name as brandname,---品牌名称
   InvoiceInformation.InvoiceCompanyName as customername,--开票企业名称
   InvoiceInformation.CreatorName as billCreatename--创建人名称
   from errortable_sap_yx
   inner join InvoiceInformation on errortable_sap_yx.bussinesscode=InvoiceInformation.code
   and errortable_sap_yx.synctypename=19----一发票多采购结算单
   inner join partssalescategory on InvoiceInformation.Partssalescategoryid=partssalescategory.id;
