--������ PurDistributionDealerLog


create table PurDistributionDealerLog  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(1000),
   SyncDate             DATE,
   OutBoundCode         VARCHAR2(100),
   Status               NUMBER(9),
   DealDate             DATE,
   constraint PK_PURDISTRIBUTIONDEALERLOG primary key (Id)
);


create sequence S_PurDistributionDealerLog;
