--������ BranchDateBaseRel

create table BranchDateBaseRel  (
   Id                   NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50),
   DateBase             VARCHAR2(50),
   constraint PK_BRANCHDATEBASEREL primary key (Id)
);


create sequence S_BranchDateBaseRel;
