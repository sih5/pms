--经销商基本信息履历bo增加企业基本信息字段

--正式环境已正式 2015-6-16 by gox

alter table DealerHistory add CustomerCode         VARCHAR2(50);
alter table DealerHistory add SupplierCode         VARCHAR2(50);
alter table DealerHistory add FoundDate            DATE;
alter table DealerHistory add RegionId             NUMBER(9);
alter table DealerHistory add ProvinceName         VARCHAR2(50);
alter table DealerHistory add CityName             VARCHAR2(50);
alter table DealerHistory add CountyName           VARCHAR2(50);
alter table DealerHistory add CityLevel            NUMBER(9);

alter table DealerHistory add ContactMobile        VARCHAR2(50);
alter table DealerHistory add ContactPhone         VARCHAR2(50);
alter table DealerHistory add ContactMail          VARCHAR2(50);
alter table DealerHistory add CompanyFax           VARCHAR2(50);
alter table DealerHistory add ContactPostCode      VARCHAR2(100);
alter table DealerHistory add BusinessAddress      VARCHAR2(200);
alter table DealerHistory add RegisterCode         VARCHAR2(50);
alter table DealerHistory add RegisterName         VARCHAR2(100);
alter table DealerHistory add RegisterCapital      NUMBER(19,4);
alter table DealerHistory add FixedAsset           NUMBER(19,4);
alter table DealerHistory add RegisterDate         DATE;
alter table DealerHistory add CorporateNature      NUMBER(9);
alter table DealerHistory add LegalRepresentative  VARCHAR2(100);
alter table DealerHistory add LegalRepresentTel    VARCHAR2(100);
alter table DealerHistory add IdDocumentType       NUMBER(9);
alter table DealerHistory add IdDocumentNumber     VARCHAR2(50);
alter table DealerHistory add RegisteredAddress    VARCHAR2(200);
alter table DealerHistory add BusinessScope        VARCHAR2(200);
alter table DealerHistory add Remark               VARCHAR2(200);


alter table DealerHistory add ContactPerson        VARCHAR2(100);
alter table DealerHistory add ACustomerCode        VARCHAR2(50);
alter table DealerHistory add ASupplierCode        VARCHAR2(50);
alter table DealerHistory add AFoundDate           DATE;
alter table DealerHistory add ARegionId            NUMBER(9);
alter table DealerHistory add AProvinceName        VARCHAR2(50);
alter table DealerHistory add ACityName            VARCHAR2(50);
alter table DealerHistory add ACountyName          VARCHAR2(50);
alter table DealerHistory add ACityLevel           NUMBER(9);
alter table DealerHistory add AContactPerson       VARCHAR2(100);
alter table DealerHistory add AContactMobile       VARCHAR2(50);
alter table DealerHistory add AContactPhone        VARCHAR2(50);
alter table DealerHistory add AContactMail         VARCHAR2(50);
alter table DealerHistory add ACompanyFax          VARCHAR2(50);
alter table DealerHistory add AContactPostCode     VARCHAR2(100);

alter table DealerHistory add ABusinessAddress     VARCHAR2(200);
alter table DealerHistory add ARegisterCode        VARCHAR2(50);
alter table DealerHistory add ARegisterName        VARCHAR2(100);
alter table DealerHistory add ARegisterCapital     NUMBER(19,4);
alter table DealerHistory add AFixedAsset          NUMBER(19,4);
alter table DealerHistory add ARegisterDate        DATE;
alter table DealerHistory add ACorporateNature     NUMBER(9);
alter table DealerHistory add ALegalRepresentative VARCHAR2(100);
alter table DealerHistory add ALegalRepresentTel   VARCHAR2(100);
alter table DealerHistory add AIdDocumentType      NUMBER(9);
alter table DealerHistory add AIdDocumentNumber    VARCHAR2(50);
alter table DealerHistory add ARegisteredAddress   VARCHAR2(200);
alter table DealerHistory add ABusinessScope       VARCHAR2(200);
