create or replace function UpdateSalesOrderForDataBase(FSourceBillCode in string,
                                                       FDateBase       in string,
                                                       FMoney          in decimal)
  return integer as
  SalesOrderBillQuantity integer;
  /*配件销售订单Id*/
  FPartsSalesOrderId integer;
  /*客户账户ID*/
  FCustomerAccountId    integer;
  FCustomerAccountIdNew integer;
  --更新对应 分公司与数据库关联关系.数据库 数据库对应的配件销售订单（源配件销售订单.编号=配件销售订单.原始销售订单编号） 状态=终止，
  --释放对应配件销售订单 客户账户的审批待发金额（客户账户.id=原配件销售订单.客户账户id) 
  --（如 分公司与数据库关联关系.数据库=dcs 则更新的是DCS库下的的配件销售订单 和 客户账户）释放金额为销售订单清单（订货量-审批量）*订货价格
begin
  SalesOrderBillQuantity := 0;
  IF FDateBase != 'YXDCS' AND FDateBase != 'SDDCS' AND FDateBase != 'GCDCS' AND
     FDateBase != 'DCS' THEN
    raise_application_error(-20002, '未找到对应的分公司与数据库关联关系。');
  END IF;
  if FDateBase = 'YXDCS' THEN
    begin
      select id, CustomerAccountId
        into FPartsSalesOrderId, FCustomerAccountId
        from YXDCS.PartsSalesOrder
       where Code = FSourceBillCode; /*配件销售订单.原始销售订单编号*/
    exception
      when NO_DATA_FOUND then
        raise_application_error(-20001, '未找到对应的配件销售订单。');
      when others then
        raise;
    end;
    begin
      select id
        into FCustomerAccountIdNew
        from YXDCS.CustomerAccount
       where id = FCustomerAccountId
         and status = 1;
    exception
      when NO_DATA_FOUND then
        raise_application_error(-20001, '未找到对应的客户账户。');
      when others then
        raise;
    end;
    update YXDCS.PartsSalesOrder
       set Status = 6
     where id = FPartsSalesOrderId;
    update YXDCS.CustomerAccount
       SET PendingAmount = PendingAmount - FMoney
     where id = FCustomerAccountIdNew;
  end if;

  if FDateBase = 'SDDCS' THEN
    begin
      select id, CustomerAccountId
        into FPartsSalesOrderId, FCustomerAccountId
        from SDDCS.PartsSalesOrder
       where Code = FSourceBillCode; /*配件销售订单.原始销售订单编号*/
    exception
      when NO_DATA_FOUND then
        raise_application_error(-20001, '未找到对应的配件销售订单。');
      when others then
        raise;
    end;
    begin
      select id
        into FCustomerAccountIdNew
        from SDDCS.CustomerAccount
       where id = FCustomerAccountId
         and status = 1;
    exception
      when NO_DATA_FOUND then
        raise_application_error(-20001, '未找到对应的客户账户。');
      when others then
        raise;
    end;
    update SDDCS.PartsSalesOrder
       set Status = 6
     where id = FPartsSalesOrderId;
    update SDDCS.CustomerAccount
       SET PendingAmount = PendingAmount - FMoney
     where id = FCustomerAccountIdNew;
  END IF;

  IF FDateBase = 'GCDCS' THEN
    begin
      select id, CustomerAccountId
        into FPartsSalesOrderId, FCustomerAccountId
        from GCDCS.PartsSalesOrder
       where Code = FSourceBillCode; /*配件销售订单.原始销售订单编号*/
    exception
      when NO_DATA_FOUND then
        raise_application_error(-20001, '未找到对应的配件销售订单。');
      when others then
        raise;
    end;
    begin
      select id
        into FCustomerAccountIdNew
        from GCDCS.CustomerAccount
       where id = FCustomerAccountId
         and status = 1;
    exception
      when NO_DATA_FOUND then
        raise_application_error(-20001, '未找到对应的客户账户。');
      when others then
        raise;
    end;
    update GCDCS.PartsSalesOrder
       set Status = 6
     where id = FPartsSalesOrderId;
    update GCDCS.CustomerAccount
       SET PendingAmount = PendingAmount - FMoney
     where id = FCustomerAccountIdNew;
  END IF;

  IF FDateBase = 'DCS' THEN
    begin
      select id, CustomerAccountId
        into FPartsSalesOrderId, FCustomerAccountId
        from DCS.PartsSalesOrder
       where Code = FSourceBillCode; /*配件销售订单.原始销售订单编号*/
    exception
      when NO_DATA_FOUND then
        raise_application_error(-20001, '未找到对应的配件销售订单。');
      when others then
        raise;
    end;
    begin
      select id
        into FCustomerAccountIdNew
        from DCS.CustomerAccount
       where id = FCustomerAccountId
         and status = 1;
    exception
      when NO_DATA_FOUND then
        raise_application_error(-20001, '未找到对应的客户账户。');
      when others then
        raise;
    end;
    update DCS.PartsSalesOrder
       set Status = 6
     where id = FPartsSalesOrderId;
    update DCS.CustomerAccount
       SET PendingAmount = PendingAmount - FMoney
     where id = FCustomerAccountIdNew;
  END IF;
  return(SalesOrderBillQuantity);
end;
/
