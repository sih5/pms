
--更新产品生命周期字典项
update KeyValueItem
set KeyValueItem.Value = '新件'
where name = 'PartsBranch_ProductLifeCycle'
and Caption = '产品生命周期'
and value = '新产品'
and key = 1;


update KeyValueItem
set KeyValueItem.Value = '低频件'
where name = 'PartsBranch_ProductLifeCycle'
and Caption = '产品生命周期'
and value = '正常产品'
and key =2;


update KeyValueItem
set KeyValueItem.Value = '正常件'
where name = 'PartsBranch_ProductLifeCycle'
and Caption = '产品生命周期'
and value = '淘汰产品'
and key = 3;

update KeyValueItem
set KeyValueItem.Value = '老件'
where name = 'PartsBranch_ProductLifeCycle'
and Caption = '产品生命周期'
and value = '整改产品'
and key = 4;
