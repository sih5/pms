begin
  sys.dbms_scheduler.create_job(job_name            => '采购退货自动更新出库状态12点',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoSetICBoutstatus',
                                start_date          => to_date('23-07-2015 12:20:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
