
--1.校验数据 校验是否有旧件编号字段为空的数据。

select a.*,'DCS' as syscode from dcs.repairordermaterialdetail a where a.usedpartsid is not null and a.usedpartscode is null
union all
select a.*,'YXDCS' as syscode from yxdcs.repairordermaterialdetail a where a.usedpartsid is not null and a.usedpartscode is null
union all
select a.*,'SDDCS' as syscode from sddcs.repairordermaterialdetail a where a.usedpartsid is not null and a.usedpartscode is null
union all
select a.*,'GCDCS' as syscode from gcdcs.repairordermaterialdetail a where a.usedpartsid is not null and a.usedpartscode is null;
---------------------------
--注:如果上面的脚本查到数据，需要执行下面步骤2、3的脚本，否则不需要执行。





--2.备份临时表

create table tmpdata.repairordermaterialdetail0811
as
select a.*,'DCS' as syscode from dcs.repairordermaterialdetail a where a.usedpartsid is not null and a.usedpartscode is null
union all
select a.*,'YXDCS' as syscode from yxdcs.repairordermaterialdetail a where a.usedpartsid is not null and a.usedpartscode is null
union all
select a.*,'SDDCS' as syscode from sddcs.repairordermaterialdetail a where a.usedpartsid is not null and a.usedpartscode is null
union all
select a.*,'GCDCS' as syscode from gcdcs.repairordermaterialdetail a where a.usedpartsid is not null and a.usedpartscode is null;


--3.更新旧件编号为空的数据

--更新DCS库
update dcs.repairordermaterialdetail a set a.usedpartscode=(select b.code from dcs.sparepart b where b.id=a.usedpartsid),a.usedpartsname=(select c.name from dcs.sparepart c where c.id=a.usedpartsid)
where a.id in(select id from tmpdata.repairordermaterialdetail0811 where syscode='DCS');

--更新YXDCS库
update yxdcs.repairordermaterialdetail a set a.usedpartscode=(select b.code from yxdcs.sparepart b where b.id=a.usedpartsid),a.usedpartsname=(select c.name from yxdcs.sparepart c where c.id=a.usedpartsid)
where a.id in(select id from tmpdata.repairordermaterialdetail0811 where syscode='YXDCS');


--更新SDDCS库
update sddcs.repairordermaterialdetail a set a.usedpartscode=(select b.code from sddcs.sparepart b where b.id=a.usedpartsid),a.usedpartsname=(select c.name from sddcs.sparepart c where c.id=a.usedpartsid)
where a.id in(select id from tmpdata.repairordermaterialdetail0811 where syscode='SDDCS');

--更新GCDCS库
update gcdcs.repairordermaterialdetail a set a.usedpartscode=(select b.code from gcdcs.sparepart b where b.id=a.usedpartsid),a.usedpartsname=(select c.name from gcdcs.sparepart c where c.id=a.usedpartsid)
where a.id in(select id from tmpdata.repairordermaterialdetail0811 where syscode='GCDCS');
