--删除以前存在报表顺序
--40
delete from Page where PageId = 'StarStatistics' and Name = '星级统计报表';

delete from Page where PageId = 'StatisticalReportsOnTheServiceType' and Name = '服务站类型统计报表';
delete from Page where PageId = 'RegisteredCapitalSta' and Name = '注册资金统计报表';

delete from Page where PageId = 'StatisticalReportsMainQuali' and Name = '维修资质统计报表';
delete from Page where PageId = 'DealerInformation' and Name = '服务站基本信息查询';
delete from Page where PageId = 'DealerBranch' and Name = '服务站分品牌信息查询';
delete from Page where PageId = 'BusinessAbilitySta' and Name = '业务能力统计报表';
delete from Page where PageId = 'StarOutSerQualitySta' and Name = '星级站保外服务量分析报表';
delete from Page where PageId = 'StarInSerQualitySta' and Name = '星级站保内服务量分析报表';
delete from Page where PageId = 'DealerRepairOutInfoSta' and Name = '服务站保外维修信息统计报表';
delete from Page where PageId = 'QualifyInefficientNetworkAnalysisReport' and Name = '保外低效网络分析报表';
delete from Page where PageId = 'InefficientNetworkAnalysisReport' and Name = '低效网络分析报表';
delete from Page where PageId = 'DealerCustomerChurnRateSta' and Name = '服务站客户流失率分析表';
delete from Page where PageId = 'DealerClaimSalesRateSta' and Name = '服务站保外保内销量比';
delete from Page where PageId = 'RepairAmountStat' and Name = '维修量统计报表';
delete from Page where PageId = 'CoverageSta' and Name = '覆盖率统计报表';
delete from Page where PageId = 'CustomerOutServeLoyalSta' and Name = '保外客户售后服务忠诚度分析表';
delete from Page where PageId = 'CustomerChurnRateSta' and Name = '客户流失率分析表';


--
delete from Page where PageId = 'PartsFailureRateDetails' and Name = '配件故障率(配件明细)';
delete from Page where PageId = 'PartsFailureRate' and Name = '配件故障率';
delete from Page where PageId = 'SupplierFailureItemSta' and Name = '各供应商故障项次统计';
delete from Page where PageId = 'FailureModeFailureItemSta' and Name = '按故障模式统计故障项次统计';
delete from Page where PageId = 'SeriesFailureItemSta' and Name = '各系列故障分布统计';



--
delete from Page where PageId = 'UsedPartsRecoverSta' and Name = '旧件回收统计表';
delete from Page where PageId = 'UsedPartsDealerSta' and Name = '旧件服务站回收情况统计表';
delete from Page where PageId = 'UsedPartsTransferOrderStop' and Name = '旧件调拨终止查询';
delete from Page where PageId = 'UsedPartsInboundSta' and Name = '旧件接收入库统计';
delete from Page where PageId = 'UsedPartsReturnSta' and Name = '旧件清退出库统计';


--
delete from Page where PageId = 'RepairCheckTime' and Name = '维修审批时长统计';
delete from Page where PageId = 'RepairCarFeeStat' and Name = '保修台车费用统计';
delete from Page where PageId = 'ClaimBillAccurate' and Name = '索赔信息提报准确率';
delete from Page where PageId = 'SupplierCarFee' and Name = '供应商单台费用';
delete from Page where PageId = 'ClaimDepartment' and Name = '索赔单事业部综合查询';
delete from Page where PageId = 'ClaimCacuStat' and Name = '索赔结算报表';
delete from Page where PageId = 'RepairClaimAppStat' and Name = '维修索赔申请报表';
delete from Page where PageId = 'RepairDetailsQuery' and Name = '维修单综合查询报表(明细)';
delete from Page where PageId = 'RepairIntegratedQuery' and Name = '维修单综合查询报表';
delete from Page where PageId = 'WarrantyDetailsQuery' and Name = '保修明细表';
delete from Page where PageId = 'ClaimSupplierDetails' and Name = '供应商索赔结算统计';
delete from Page where PageId = 'ClaimSettlementBillSplit' and Name = '索赔结算报表(拆分结算)';

--
