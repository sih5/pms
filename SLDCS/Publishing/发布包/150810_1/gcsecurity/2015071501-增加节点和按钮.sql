
--增加节点

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(7600, 7000, 1, 'AfterSaleTecFileManagementSys', 'Released', NULL, '售后技术文件管理系统', NULL, NULL, 6, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(7601, 7600, 2, 'AfterSaleTecFileManaSysQuery', 'Released', NULL, '售后技术文件管理系统查询', '查询售后技术文件管理系统', 'Client/Dcs/Images/Menu/ServiceBasicData/VehProductStrucQuery.png', 1, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);


--增加按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5203, 'DealerPartsStock|InAndOutRecordQuery', '出入库记录查询', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6210, 'Common|Export', '导出', 2);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
