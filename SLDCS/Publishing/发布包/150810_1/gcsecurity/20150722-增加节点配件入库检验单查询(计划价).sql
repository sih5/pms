--增加节点配件入库检验单查询(计划价)

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(5307,5300, 2, 'PartsInboundCheckBillWithPlanPriceQuery', 'Released', NULL, '配件入库检验单查询(计划价)', '仓库人员查询辖内仓库入库检验单', 'Client/DCS/Images/Menu/PartsStocking/PartsInboundCheckBillQuery.png', 7, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
   
   

--增加按钮销售订单管理增加清单修改按钮
--入库检验单查询增加可结算和不可结算按钮
--配件入库检验单查询(计划价)加导出、合并导出、工程车打印按钮

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4101, 'PartsSalesOrder|EditDetail', '清单修改', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5305, 'PartsInboundCheckBillQuery|NotSettle', '不可结算', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5305, 'PartsInboundCheckBillQuery|Settle', '可结算', 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5307, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5307, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5307, 'Common|PrintForGC', '工程车打印', 2);


-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
