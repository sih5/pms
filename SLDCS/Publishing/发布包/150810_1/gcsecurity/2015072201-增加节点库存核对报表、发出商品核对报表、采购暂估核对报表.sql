--增加节点库存核对报表、发出商品核对报表、采购暂估核对报表

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(13305,13300, 2, 'PartsStockCheckSta', 'Released', NULL, '库存核对报表', '统计期间内入库结算的成本', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 0, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(13306,13300, 2, 'PartsSendGoodsSta', 'Released', NULL, '发出商品核对报表', '统计期间内入库结算的成本', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 1, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(13307,13300, 2, 'PartsPurchaseAssessmentSta', 'Released', NULL, '采购暂估核对报表', '统计期间内入库结算的成本', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 2, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
