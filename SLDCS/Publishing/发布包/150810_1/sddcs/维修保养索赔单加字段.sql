
--维修保养索赔单 加字段
alter table RepairClaimBill add FaultyPartsAssemblyId NUMBER(9);
alter table RepairClaimBill add FaultyPartsAssemblyCode VARCHAR2(50);
alter table RepairClaimBill add FaultyPartsAssemblyName VARCHAR2(100);


alter table RepairClaimBill add RetainedCustomer     VARCHAR2(100);
alter table RepairClaimBill add RetainedCustomerPhone VARCHAR2(100);
alter table RepairClaimBill add RetainedCustomerId   NUMBER(9);



--维修单故障原因清单 加字段
alter table RepairOrderFaultReason add  FaultyPartsAssemblyId NUMBER(9);
alter table RepairOrderFaultReason add FaultyPartsAssemblyCode VARCHAR2(50);
alter table RepairOrderFaultReason add FaultyPartsAssemblyName VARCHAR2(100);
