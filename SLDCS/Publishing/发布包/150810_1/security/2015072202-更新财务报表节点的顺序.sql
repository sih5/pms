--更新财务报表节点的顺序

update Page
set Page.Sequence = 3
where Page.Pageid = 'InboundEstimateCollect'
and Page.Name = '配件暂估入库汇总表'
and Page.Sequence = 0;


update Page
set Page.Sequence = 4
where PageId= 'InboundEstimateDetails'
and Name = '配件入库暂估明细表'
and Sequence = 1;


update Page
set Page.Sequence = 5
where PageId= 'PartsStockSumStat'
and Name = '配件库存汇总'
and Sequence = 2 ;


update Page
set Page.Sequence = 6
where PageId= 'PartsCostSettleDetail'
and Name = '配件成本统计表结算清单'
and Sequence = 3;


update Page
set Page.Sequence = 7
where PageId= 'PartsOutNoSettleStat'
and Name = '配件销售未结算明细'
and Sequence = 4;


update Page
set Page.Sequence = 8
where PageId= 'PartsInCostSettleDetail'
and Name = '入库成本结算清单'
and Sequence = 5;


--------

update Page
set Page.Sequence =  9
where PageId= 'PurchasingCostDerate'
and Name = '采购成本降低额'
and Sequence = 5;

update Page
set Page.Sequence = 10
where PageId= 'DealerInAgencyAccountSta'
and Name = '服务站在代理库账面余额统计'
and Sequence =6;
