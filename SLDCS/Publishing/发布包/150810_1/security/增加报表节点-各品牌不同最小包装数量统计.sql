--增加报表节点

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values 
(14102, 14100, 2, 'AllBrandDiffMInPackNumSta', 'Released', '', '各品牌不同最小包装数量统计', '', '', '用于pms系统最小包装数量统一监控管理，与wms数据不一致，导致CDC仓库亏损', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 13, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
