--维修单维修材料清单  RepairOrderMaterialDetail 的：
--旧件配件编号  UsedPartsCode  、旧件配件名称  UsedPartsName
--两个字段，调整为非空字段

alter table RepairOrderMaterialDetail modify UsedPartsCode     VARCHAR2(50)       not null;
alter table RepairOrderMaterialDetail modify UsedPartsName        VARCHAR2(100)   not null;
