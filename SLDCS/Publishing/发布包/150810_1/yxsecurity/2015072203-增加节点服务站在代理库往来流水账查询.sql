
--增加节点服务站在代理库往来流水账查询
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(6216,6200, 2, 'AgencyCustomerAccount', 'Released', NULL,'服务站在代理库往来流水账查询', '服务站在代理库账户明细查询', 'Client/DCS/Images/Menu/Financial/AgencyCustomerAccount.png', 16, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);


--增加按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,6216, 'Common|Export', '导出', 2);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
