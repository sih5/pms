
--配件入库计划新增字段：要求到货时间、配件采购订单类型id

alter table PartsInboundPlan add PartsPurchaseOrderTypeId NUMBER(9);

alter table PartsInboundPlan add RequestedDeliveryTime DATE;
