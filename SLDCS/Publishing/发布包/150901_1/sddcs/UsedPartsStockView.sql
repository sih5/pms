create or replace view UsedPartsStockView as
select
a.Id,u.Id as WarehouseId,u.Code,u.Name,UsedPartsBarCode,UsedPartsCode,UsedPartsName,s.Code as AreaCode,StorageQuantity,LockedQuantity,SettlementPrice,ClaimBillCode,
IfFaultyParts,FaultyPartsCode,UsedPartsSupplierCode,FaultyPartsSupplierCode,ResponsibleUnitCode,a.CreatorName,a.CreateTime,
UsedPartsId,
UsedPartsBatchNumber,
UsedPartsSerialNumber,
ClaimBillType,
ClaimBillId,
FaultyPartsId,
FaultyPartsName,
UsedPartsSupplierId,
UsedPartsSupplierName,
FaultyPartsSupplierId,
FaultyPartsSupplierName,
ResponsibleUnitId,
ResponsibleUnitName,
a.BranchId,
CAST(case when (select upt.Status from UsedPartsTransferDetail uptd
inner join UsedPartsTransferOrder upt on uptd.UsedPartsTransferOrderId = upt.Id
where upt.Status <> 99 and uptd.UsedPartsBarCode = a.UsedPartsBarCode and rownum = 1) is not null then 1 else 0 end AS NUMBER(1))  as IsTransfers
from UsedPartsStock a
inner join UsedPartsWarehouse  u on u.Id=a.UsedPartsWarehouseId
inner join UsedPartsWarehouseArea s on s.Id=a.UsedPartsWarehouseAreaId;