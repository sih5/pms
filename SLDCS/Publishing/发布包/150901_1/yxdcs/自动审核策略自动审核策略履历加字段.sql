
--自动审核策略/自动审核策略履历 加字段

alter table AutoApproveStrategy add ForcedMainteTime     NUMBER(9);

alter table AutoApproveStrategyHistory add ForcedMainteTime     NUMBER(9);
