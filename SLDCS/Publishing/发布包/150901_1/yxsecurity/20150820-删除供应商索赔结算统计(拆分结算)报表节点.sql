--备份node 表中供应商索赔结算统计(拆分结算) 报表节点
-- 1条
create table node150820
as
select * from node
where exists(select * from page where name = '供应商索赔结算统计(拆分结算)'
and page.id = node.categoryid);


--删除node表中的 供应商索赔结算统计(拆分结算) 报表节点
--1条
delete from node
where exists(select * from page where name = '供应商索赔结算统计(拆分结算)'
and page.id = node.categoryid);


---------------------------------

--备份page  表中供应商索赔结算统计(拆分结算) 报表节点
--1条
create table Page150820
as
select * from Page
where name = '供应商索赔结算统计(拆分结算)';


--删除Page表中供应商索赔结算统计(拆分结算) 报表节点
--1条
delete from Page
where name = '供应商索赔结算统计(拆分结算)';
