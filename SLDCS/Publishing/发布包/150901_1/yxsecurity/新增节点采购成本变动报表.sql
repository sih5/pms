--新增节点采购成本变动报表

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(13308,13300, 2, 'PurchaseCostChangeSta', 'Released', NULL, '采购成本变动报表', '查询采购价格调整前后价格变动过程', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 11, 2);



-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
   

--新增按钮 
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2105, 'Common|ExportByPrice', '价格不一致导出', 2);


-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
