--历史数据根据第一次处理时间进行更新处理
--数据处理，取值销售订单对应的最早的一条处理单的创建时间

update PartsSalesOrder
   set PartsSalesOrder.Firstapprovetime =
       (select createtime
          from (select t.originalsalesorderid,
                       t.createtime,
                       (row_number() over(partition by t.originalsalesorderid
                                          order by t.createtime)) as rnum
                  from PartsSalesOrderProcess t) temp
         where temp.rnum = 1 and temp.originalsalesorderid = PartsSalesOrder.Id)
 where PartsSalesOrder.Status in('4','5','6')
 and exists(select * from PartsSalesOrderProcess where PartsSalesOrder.Id = PartsSalesOrderProcess.Originalsalesorderid)
