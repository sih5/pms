
--增加报表 外购配件占比

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(13236,13800, 2, 'OutPurchasePartRateSta', 'Released', NULL, '外购配件占比', '外购配件占比', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 18, 2);



-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
