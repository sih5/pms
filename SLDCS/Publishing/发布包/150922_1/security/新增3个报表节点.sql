--新增3个报表节点

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(13801,13800, 2, 'DealerCoincidenceSta', 'Released', NULL, '重合率统计报表', '根据各品牌服务网络名称或服务站编号统计各品牌服务网络重合建设情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 19, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(13802,13800, 2, 'DealerCoincidenceDetailSta', 'Released', NULL, '重合率统计明细报表', '根据各品牌服务网络名称或服务站编号统计各品牌服务网络重合建设情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 19, 2);


insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)values 
(14103, 14100, 2, 'OverseasStockCostControlSta', 'Released', '', '海外库存资金监控报表', '', '', '用于海外采购成本监控情况统计', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 14, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
