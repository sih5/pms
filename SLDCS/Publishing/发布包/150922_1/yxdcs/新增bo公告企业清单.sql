--新增bo公告企业清单

create table CompanyDetail  (
   Id                   NUMBER(9)                       not null,
   NoticeId             NUMBER(9)                       not null,
   CompanyId            NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   constraint PK_COMPANYDETAIL primary key (Id)
);


create sequence S_CompanyDetail;
