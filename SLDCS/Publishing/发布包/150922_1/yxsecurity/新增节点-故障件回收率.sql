
--新增节点故障件回收率

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(13501,13500, 2, 'FaultPartRecycleRateSta', 'Released', NULL, '故障件回收率', '故障件回收率', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 4, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
