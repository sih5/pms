--新增节点服务量分析报表

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(13803,13800, 2, 'RepairQuantityAnalysisSta', 'Released', NULL, '服务量分析报表', '根据各品牌服务网络数量及维修量分析某个时间段内服务网络的服务集中度', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 20, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
