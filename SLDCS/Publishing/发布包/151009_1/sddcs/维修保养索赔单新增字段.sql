
--维修保养索赔单新增字段

alter table RepairClaimBill add DebitAmount          NUMBER(19,4);

alter table RepairClaimBill add ComplementAmount     NUMBER(19,4);
