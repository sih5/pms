--新增节点 服务站在代理库账面余额历史查询

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(6218,6200, 2, 'CustomerAccountHistoryQuery', 'Released', NULL,'服务站在代理库账面余额历史查询', '服务站在代理库账面余额历史查询', 'Client/DCS/Images/Menu/Financial/CustomerAccountHistoryQuery.png', 18, 2);



-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);


--新增按钮

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval,6218, 'Common|Export', '导出', 2);


-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);

