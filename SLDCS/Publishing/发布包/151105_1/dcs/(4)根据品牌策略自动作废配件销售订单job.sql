begin
                        
      sys.dbms_scheduler.create_job( 
                                job_name => 'PartsSalesOrderCennel_JOB',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'PartsSalesOrderCennel',
                                start_date          => sysdate,
                                repeat_interval     => 'FREQ=Daily;BYHOUR=3;BYMINUTE=0;BYSECOND=0',
                                enabled             => true,
                                auto_drop           => false,
                               comments            => '根据品牌策略自动作废配件销售订单');
end;