create or replace view PartNotConPurchasePlan as
select * from (
  select PartsBranch.BranchId,
         PartsBranch.Branchname,
         PartsBranch.PartsSalesCategoryId,
         PartsBranch.PartsSalesCategoryName,
         PartsBranch.Partid,
         PartsBranch.Partcode,
         PartsBranch.Partname,
         PartsSupplierRelation.DefaultOrderWarehouseId,
         PartsSupplierRelation.DefaultOrderWarehouseName,
         PartsSupplierRelation.SupplierId,
         PartsSupplierRelation.SupplierPartCode,
         PartsSupplierRelation.SupplierPartName,   
         PartsSupplierRelation.OrderCycle,--订货周期
         PartsSupplierRelation.EmergencyArrivalPeriod,--,--紧急到货周期
         PartsSupplierRelation.MonthlyArrivalPeriod,--常规到货周期
         PartsSupplierRelation.ArrivalReplenishmentCycle,--,--补充计划到货周期
         PartsBranch.PartABC,
         PartsBranch.ProductLifeCycle,
         PartsBranch.PlannedPriceCategory,
         PartSafeStock.OrderQuantityFor12M, --近12月出库频次        
         PartsPlannedPrice.PlannedPrice, --计划价  
         IntelligentOrderMonthly.Frequency1, --上月出库频次  Frequency1
         IntelligentOrderMonthly.DayAvgFor1m, --近1月日均销量
         IntelligentOrderMonthly.OrderQuantity1,--T-1月销量
         IntelligentOrderMonthly.OrderQuantity2,
         IntelligentOrderMonthly.OrderQuantity3,--T-3月销量   
         PartSafeStock.SafeStock,--安全库存
         allValidStock.nums as allValidStocknums, --有效库存
         PurchaseOL.nums as PurchaseOLnums, --采购在途
         PC.nums as PCnums,  --采购待确认量
         TransferOL.nums as TransferOLnums, --调拨在途
         Replacement.nums as Replacementnums,  --替换件库存
         ReplacePurchaseOL.nums as ReplacePurchaseOLnums, --替换件采购在途
         PartsExchange.nums as PartsExchangenums, --互换件库存
         PartsExchangeOL.nums as PartsExchangeOLnums, --互换件采购在途         
         SLNC.nums as SLNCnums, --销售待审量 
         PurchaseOLDT.DeliveryTime,--在途预计到货时间：采购在途部分对应的采购订单中，预计到货时间最小的一个
         (nvl(PartsSupplierRelation.OrderCycle,0)*nvl(IntelligentOrderMonthly.DAYAVGFOR1M,0)) as RsAdvicePurchase ,--补货建议采购量:订货周期*近1个月日均
          --紧急建议采购量：订货周期*近1个月日均-有效库存-采购在途-采购待确认量+销售待审量
         (nvl(PartsSupplierRelation.OrderCycle,0)*nvl(IntelligentOrderMonthly.DAYAVGFOR1M,0)-nvl(allValidStock.nums,0)-nvl(PurchaseOL.nums,0)-nvl(PC.nums,0)+nvl(SLNC.nums,0)) as EgAdvicePurchase,
         --是否满足补充采购计划：有效库存+采购在途量+采购待确认量-常规到货周期*近1个月日均销量-销售待审≤0 的部分填充为是，其他的填充为否
         case when (nvl(allValidStock.nums,0)+nvl(PurchaseOL.nums,0)+nvl(PC.nums,0)-nvl(PartsSupplierRelation.MonthlyArrivalPeriod,0)*nvl(IntelligentOrderMonthly.DAYAVGFOR1M,0)-nvl(SLNC.nums,0))<=0 then 1
           else 0 end as IsMeetSupplementPlan,
         --是否满足紧急采购计划：有效库存-销售待审≤0 的部分填充为是，其他的填充为空
         case when nvl(allValidStock.nums,0)-nvl(SLNC.nums,0)<=0 then 1 else 0 end as IsMeetEmergencyPlan
    from PartsBranch
    
    left join PartsSupplierRelation
      on PartsSupplierRelation.BranchId = PartsBranch.BranchId and PartsSupplierRelation.IsPrimary=1
     and PartsSupplierRelation.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and PartsBranch.Partid = PartsSupplierRelation.Partid
     
    left join PartSafeStock  --安全库存
     on PartSafeStock.BranchId = PartsBranch.BranchId
     and PartSafeStock.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and PartSafeStock.Partid = PartsBranch.Partid
     
    left join AllValidStock --品牌可用库存  （视图）
     on allValidStock.BranchId = PartsBranch.BranchId
     and allValidStock.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and allValidStock.PartId = PartsBranch.PartId      

    left join PartsPlannedPrice --计划价
      on PartsPlannedPrice.OwnerCompanyId = PartsBranch.BranchId
     and PartsPlannedPrice.PartsSalesCategoryId =
         PartsBranch.PartsSalesCategoryId
     and PartsPlannedPrice.SparePartId = PartsBranch.PartId
     
    left join IntelligentOrderMonthly--智能订货月度累计表
      on IntelligentOrderMonthly.BRANCHID = PartsBranch.BranchId
     and IntelligentOrderMonthly.SALESCATEGORYID =
         PartsBranch.PartsSalesCategoryId
     and IntelligentOrderMonthly.PARTID = PartsBranch.PartId
  
  --采购在途 供应商已确认但是未生成发运单+供应商已生成发运单但是未入库的
    left join PartsPurchaseOnLine  PurchaseOL
      on PurchaseOL.BranchId = PartsBranch.BranchId
     and PurchaseOL.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and PurchaseOL.PartId = PartsBranch.PartId
     
   --采购待确认量：采购订单中提交但是供应商未确认的部分  
   left join 
       (select orders.BranchId,orders.PartsSalesCategoryId,detail.sparepartid as PartId, sum(nvl(detail.Orderamount,0)-nvl(detail.ConfirmedAmount,0)) as nums 
         from PartsPurchaseOrderDetail detail inner join PartsPurchaseOrder orders 
         on detail.PartsPurchaseOrderId=orders.id
         where orders.status in (2,3) --提交、部分确认
         group by orders.BranchId,orders.PartsSalesCategoryId,detail.sparepartid
       )PC
       on PC.BranchId = PartsBranch.BranchId
     and PC.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and PC.PartId = PartsBranch.PartId
     
     
  --调拨在途：对方已出库未发运的+对方已发运未收货确认的+已生成入库计划未入库的
    left join (
               --对方已出库未发运的
               select BranchId,
                       PartsSalesCategoryId,
                       PartId,
                       sum(nums) as nums
                 from (select plans.StorageCompanyId as BranchId,
                               plans.PartsSalesCategoryId,
                               detail.SparePartId as PartId,
                               sum(detail.OutboundFulfillment) nums
                          from PartsOutboundPlanDetail detail
                         inner join PartsOutboundPlan plans
                            on detail.PartsOutboundPlanId = plans.id
                         inner join PartsLogisticBatch batchs
                            on batchs.SourceId = plans.id
                           and batchs.SourceType = 1
                           and batchs.ShippingStatus = 1
                         where plans.OutboundType = 4
                         group by plans.StorageCompanyId,
                                  plans.PartsSalesCategoryId,
                                  detail.SparePartId
                        union all --对方已发运未收货确认的
                        select orders.BranchId,
                               orders.PartsSalesCategoryId,
                               detail.sparepartid,
                               sum(nvl(detail.Shippingamount, 0) -
                                   nvl(detail.ConfirmedAmount, 0)) nums
                          from PartsShippingOrderDetail detail
                         inner join PartsShippingOrder orders
                            on detail.PartsShippingOrderId = orders.id
                         inner join PartsShippingOrderRef refs
                            on orders.id = refs.PartsShippingOrderId
                         inner join PartsOutboundBill bill
                            on bill.id = refs.partsoutboundbillid
                         where bill.OutboundType = 4
                           and orders.status = 1 --发运单状态为新建
                         group by orders.BranchId,
                                  orders.PartsSalesCategoryId,
                                  detail.sparepartid
                        union all --已生成入库计划未入库
                        select plans.StorageCompanyId as BranchId,
                               plans.PartsSalesCategoryId,
                               detail.SparePartId,
                               sum(nvl(PlannedAmount, 0) -
                                   nvl(InspectedQuantity, 0)) nums   ---计划量  - 检验量
                          from Partsinboundplandetail detail
                         inner join PartsInboundPlan plans
                            on detail.PartsInboundPlanId = plans.id
                         where plans.InboundType = 3
                           and plans.status in (1, 4) --新建、部分检验
                         group by plans.StorageCompanyId,
                                  plans.PartsSalesCategoryId,
                                  detail.SparePartId)
                group by BranchId, PartsSalesCategoryId, PartId) TransferOL
      on TransferOL.BranchId = PartsBranch.BranchId
     and TransferOL.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and TransferOL.PartId = PartsBranch.PartId
     
     --替换件库存：统计配件可采购可销售的替换件品牌仓库的所有可用库存
     --替换件库存：统计配件可采购可销售的替换件品牌仓库的所有可用库存
     left join 
     ( select BranchId,PartsSalesCategoryId,OldPartId as PartId,sum(nums) as nums from 
          (select AllValidStock.BranchId,AllValidStock.PartsSalesCategoryId,PartsReplacement.OldPartId,PartsReplacement.NewPartId,AllValidStock.nums
         from PartsReplacement 
         inner join AllValidStock on AllValidStock.PartId=NewPartId
         inner join PartsBranch on PartsReplacement.Newpartid=PartsBranch.Partid and AllValidStock.BranchId = PartsBranch.Branchid 
                and AllValidStock.PartsSalesCategoryId=PartsBranch.Partssalescategoryid 
         where   PartsReplacement.Status=1 and PartsBranch.IsOrderable=1 and PartsBranch.IsSalable=1
         ) group by BranchId,PartsSalesCategoryId,OldPartId
     )Replacement 
     on Replacement.BranchId = PartsBranch.BranchId
     and Replacement.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and Replacement.PartId = PartsBranch.PartId
     
     --替换件采购在途:统计配件可采购可销售的替换件供应商已确认但是未生成发运单+供应商已生成发运单但是未入库的
     left join (select BranchId,PartsSalesCategoryId,Oldpartid as PartId,sum(nums) as nums from
                    (select PartsPurchaseOnLine.BranchId,
                      PartsPurchaseOnLine.PartsSalesCategoryId,
                      PartsReplacement.Oldpartid,
                      PartsReplacement.Newpartid,
                      PartsPurchaseOnLine.nums
                 from PartsReplacement 
                 inner join PartsPurchaseOnLine on PartsReplacement.Newpartid=PartsPurchaseOnLine.PartId
                 inner join PartsBranch on PartsReplacement.Newpartid=PartsBranch.Partid 
                    and PartsPurchaseOnLine.BranchId=PartsBranch.Branchid
                    and PartsPurchaseOnLine.PartsSalesCategoryId=PartsBranch.Partssalescategoryid
                 where PartsReplacement.Status=1 and PartsBranch.Isorderable=1 and PartsBranch.Issalable=1
                   ) group by BranchId,PartsSalesCategoryId,Oldpartid
                 ) ReplacePurchaseOL 
      on ReplacePurchaseOL.BranchId = PartsBranch.BranchId
     and ReplacePurchaseOL.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and ReplacePurchaseOL.PartId = PartsBranch.PartId

   --互换件库存：统计配件可采购可销售的所有互换件品牌仓库的所有可用库存。
    left join 
     ( 
        select A.BranchId,A.PartsSalesCategoryId,A.Partid,nvl(B.nums,0)-nvl(A.nums,0) as nums from 
        (select AllValidStock.BranchId,AllValidStock.PartsSalesCategoryId,PartsExchange.Exchangecode,PartsExchange.Partid,sum(AllValidStock.nums) as nums from PartsExchange
         inner join AllValidStock on AllValidStock.PartId=PartsExchange.PartId 
         inner join PartsBranch on PartsExchange.PartId=PartsBranch.Partid and AllValidStock.BranchId = PartsBranch.Branchid 
                and AllValidStock.PartsSalesCategoryId=PartsBranch.Partssalescategoryid
          where PartsExchange.status=1  and PartsBranch.IsOrderable=1 and PartsBranch.IsSalable=1
         group by AllValidStock.BranchId,AllValidStock.PartsSalesCategoryId,PartsExchange.Exchangecode,PartsExchange.Partid
        ) A left join 
        (select AllValidStock.BranchId,AllValidStock.PartsSalesCategoryId,ExchangeCode,sum(AllValidStock.nums) as nums from PartsExchange
         inner join AllValidStock on AllValidStock.PartId=PartsExchange.PartId 
         inner join PartsBranch on PartsExchange.PartId=PartsBranch.Partid and AllValidStock.BranchId = PartsBranch.Branchid 
                and AllValidStock.PartsSalesCategoryId=PartsBranch.Partssalescategoryid
         where  PartsExchange.status=1  and PartsBranch.IsOrderable=1 and PartsBranch.IsSalable=1
         group by AllValidStock.BranchId,AllValidStock.PartsSalesCategoryId,ExchangeCode) B
        on A.BranchId=B.BranchId and A.PartsSalesCategoryId=B.PartsSalesCategoryId and A.Exchangecode=B.Exchangecode
     )PartsExchange 
     on PartsExchange.BranchId = PartsBranch.BranchId
     and PartsExchange.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and PartsExchange.PartId = PartsBranch.PartId
     
--互换件采购在途：统计配件可采购可销售的互换件供应商已确认但是未生成发运单+供应商已生成发运单但是未入库的     
     left join 
     ( 
        select A.BranchId,A.PartsSalesCategoryId,A.Partid,nvl(B.nums,0)-nvl(A.nums,0) as nums from 
        (select PartsPurchaseOnLine.BranchId,PartsPurchaseOnLine.PartsSalesCategoryId,PartsExchange.ExchangeCode,PartsExchange.Partid,PartsPurchaseOnLine.nums  
         from PartsExchange
         inner join PartsPurchaseOnLine on PartsExchange.Partid=PartsPurchaseOnLine.PartId
         inner join PartsBranch on PartsExchange.PartId=PartsBranch.Partid and PartsPurchaseOnLine.BranchId = PartsBranch.Branchid 
                and PartsPurchaseOnLine.PartsSalesCategoryId=PartsBranch.Partssalescategoryid
         where PartsExchange.status=1  and PartsBranch.IsOrderable=1 and PartsBranch.IsSalable=1
        ) A left join 
        (select PartsPurchaseOnLine.BranchId,PartsPurchaseOnLine.PartsSalesCategoryId,PartsExchange.ExchangeCode,sum(PartsPurchaseOnLine.nums) as nums  
         from PartsExchange
         inner join PartsPurchaseOnLine on PartsExchange.Partid=PartsPurchaseOnLine.PartId
         inner join PartsBranch on PartsExchange.PartId=PartsBranch.Partid and PartsPurchaseOnLine.BranchId = PartsBranch.Branchid 
                and PartsPurchaseOnLine.PartsSalesCategoryId=PartsBranch.Partssalescategoryid
         where PartsExchange.status=1  and PartsBranch.IsOrderable=1 and PartsBranch.IsSalable=1
         group by PartsPurchaseOnLine.BranchId,PartsPurchaseOnLine.PartsSalesCategoryId,PartsExchange.ExchangeCode) B
        on A.BranchId=B.BranchId and A.PartsSalesCategoryId=B.PartsSalesCategoryId and A.Exchangecode=B.Exchangecode
     )PartsExchangeOL
     on PartsExchangeOL.BranchId = PartsBranch.BranchId
     and PartsExchangeOL.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and PartsExchangeOL.PartId = PartsBranch.PartId  
   

   --销售待审量: 配件品牌对应的所有提交或者部分审批中未审核部分。
   --统购品牌需要再合计上与统购品牌属于同一分公司代码下的其他品牌的该配件的所有提交或者部分审批中未审核的配件数量。
    left join
      ( select branchid,SalesCategoryId,sparepartid as PartId,sum(nums) as nums from 
        (
          select orders.branchid,orders.SalesCategoryId,detail.sparepartid, sum(nvl(detail.OrderedQuantity,0)-nvl(detail.ApproveQuantity,0)) as nums --- 订货数 、审批数
            from  PartsSalesOrderdetail detail inner join PartsSalesOrder orders on detail.PartsSalesOrderId=orders.id
            where orders.status in (2,4) and orders.SalesCategoryName<>'统购' -- 提交 、部分审批
            group by orders.branchid,orders.SalesCategoryId,detail.sparepartid
         union all
            select orders.branchid,(select id from PartsSalesCategory where name='统购' and status=1) as SalesCategoryId,detail.sparepartid, sum(nvl(detail.OrderedQuantity,0)-nvl(detail.ApproveQuantity,0)) as nums 
            from  PartsSalesOrderdetail detail 
            inner join PartsSalesOrder orders on detail.PartsSalesOrderId=orders.id
            inner join (select PartsSalesOrder.branchid,PartsSalesOrderdetail.Sparepartid from PartsSalesOrder 
                          inner join PartsSalesOrderdetail on PartsSalesOrder.id=PartsSalesOrderdetail.Partssalesorderid 
                         where PartsSalesOrder.status in (2,4) and PartsSalesOrder.SalesCategoryName='统购' 
                         group by PartsSalesOrder.branchid,PartsSalesOrderdetail.Sparepartid 
                       ) TG on TG.branchid=orders.branchid and TG.Sparepartid=detail.sparepartid
            where orders.status in (2,4)  and orders.branchid=(select branchid from PartsSalesCategory where Name='统购' and status=1)
            group by orders.branchid,detail.sparepartid) group by branchid,SalesCategoryId,sparepartid
      )SLNC
     on SLNC.BranchId = PartsBranch.BranchId
     and SLNC.SalesCategoryId = PartsBranch.PartsSalesCategoryId
     and SLNC.PartId = PartsBranch.PartId 
      
     --在途预计到货时间：采购在途部分对应的采购订单中，预计到货时间最小的一个。  
     left join (select orders.BranchId,
                      orders.PartsSalesCategoryId,
                      detail.SparePartId,
                      min(orders.RequestedDeliveryTime) as DeliveryTime
                 from PartsPurchaseOrderDetail detail
                inner join PartsPurchaseOrder orders    
                   on orders.id = detail.partspurchaseorderid
                where orders.status in (3, 4, 5, 6)
                  and orders.InStatus = 5  --未入库
                group by orders.BranchId,
                         orders.PartsSalesCategoryId,
                         detail.SparePartId) PurchaseOLDT
      on PurchaseOLDT.BranchId = PartsBranch.BranchId
     and PurchaseOLDT.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and PurchaseOLDT.SparePartId = PartsBranch.PartId
   
)  where IsMeetSupplementPlan=1 or IsMeetEmergencyPlan=1 ; 




