--每个月22号凌晨0点，自动结转。保存一年的数据
create or replace procedure Prc_GetPartConPurchasePlan as 
begin
  delete  PartConPurchasePlan where to_char(DateT,'yyyymm')=to_char(ADD_MONTHS(sysdate, -12),'yyyymm');

  insert into PartConPurchasePlan
  ( Id,
    BranchId,
    BranchName,
    PartsSalesCategoryId,
    PartsSalesCategoryName,
    PartId,
    PartCode,
    PartName,
    PrimarySupplierId,
    PrimarySupplierCode,
    PrimarySupplierName,
    DFOrderWarehouse,
    PartABC,
    PruductLifeCycle,
    PlannedPriceType,
    Near12MOutFrequency,
    PlannedPrice,
    OrderQuantity1,
    OrderQuantity2,
    OrderQuantity3,
    OrderQuantityFor3M,
    OrderQuantityFor6M,
    OrderQuantityFor12M,
    DayAvgFor1M,
    DayAvgFor3M,
    DayAvgFor6M,
    DayAvgFor12M,
    Average,
    ValidStock,
    PurchaseInLine,
    TransferInLine,
    ReStock,
    RePurchaseInLine,
    ExStock,
    ExPurchaseInLine,
    SalesPending,
    PurchaseToBeConfirm,
    ConArrivalCycle,
    OrderCycle,
    EconomicalBatch,
    MinPurchaseQuantity,
    TheoryMaxStock,
    PurchaseMethod1,
    PurchaseMethod2,
    PurchaseMethod3,
    PurchaseMethod4,
    AdvicePurchase,
    ActualSupplierId,
    ActualSupplierCode,
    ActualOrderWarehouseId,
    ActualOrderWarehouse,
    ActualPurchase,
    DateT
    )
  select S_PartConPurchasePlan.Nextval,
         PartsBranch.BranchId,
         PartsBranch.Branchname,
         PartsBranch.PartsSalesCategoryId,
         PartsBranch.PartsSalesCategoryName,
         PartsBranch.Partid,
         PartsBranch.Partcode,
         PartsBranch.Partname,
         PartsSupplierRelation.SupplierId, 
         PartsSupplierRelation.SupplierPartCode,
         PartsSupplierRelation.SupplierPartName,   
         PartsSupplierRelation.DefaultOrderWarehouseName,
         PartsBranch.PartABC,
         PartsBranch.ProductLifeCycle,
         PartsBranch.PlannedPriceCategory,
         PartSafeStock.OrderQuantityFor12M, --近12月出库频次
         PartsPlannedPrice.PlannedPrice, --计划价
         IntelligentOrderMonthly.ORDERQUANTITY1, --T-1月销量
         IntelligentOrderMonthly.ORDERQUANTITY2,
         IntelligentOrderMonthly.ORDERQUANTITY3,
         IntelligentOrderMonthly.ORDERQUANTITYFOR3M, --近3个月销量
         IntelligentOrderMonthly.ORDERQUANTITYFOR6M,
         IntelligentOrderMonthly.ORDERQUANTITYFOR12M,
         IntelligentOrderMonthly.DAYAVGFOR1M, --上月日均
         IntelligentOrderMonthly.DAYAVGFOR3M, --近3月日均
         IntelligentOrderMonthly.DAYAVGFOR6M,
         IntelligentOrderMonthly.DAYAVGFOR12M,
         IntelligentOrderWeekly.AVG, --近52周均 
         allValidStock.nums, --有效库存
         PurchaseOL.nums, --采购在途
         TransferOL.nums, --调拨在途
         Replacement.nums,  --替换件库存
         ReplacePurchaseOL.nums, --替换件采购在途
         PartsExchange.nums, --互换件库存
         PartsExchangeOL.nums, --互换件采购在途
         SLNC.nums, --销售待审量
         PC.nums,  --采购待确认量
         PartsSupplierRelation.MonthlyArrivalPeriod,--常规到货周期    
         PartsSupplierRelation.OrderCycle,--订货周期
         PartsSupplierRelation.EconomicalBatch,-- 经济采购批量	
         PartsSupplierRelation.MinBatch as MinPurchaseQuantity,  --最小采购量
         --理论最大库存=近52周周均/7*（订货周期+常规到货周期）+安全库存 
         (nvl(IntelligentOrderWeekly.AVG,0)/7*(nvl(PartsSupplierRelation.OrderCycle,0)+nvl(PartsSupplierRelation.MonthlyArrivalPeriod,0))+nvl(PartSafeStock.SafeStock,0)) as TheoryMaxStock,
         
         --方法一采购量= （上月日均*60%+近三月日均*40%）* 常规到货周期*3-有效库存-采购在途-采购待确认量+销售待审量
         ((nvl(IntelligentOrderMonthly.DAYAVGFOR1M,0)*0.6 + nvl(IntelligentOrderMonthly.DAYAVGFOR3M,0)*0.4 )*nvl(PartsSupplierRelation.MonthlyArrivalPeriod,0)*3 
         -nvl(allValidStock.nums,0)-nvl(PurchaseOL.nums,0)-nvl(PC.nums,0)+nvl(SLNC.nums,0))  as PurchaseMethod1,
         
         --方法二采购量=理论最大库存-有效库存-采购在途-采购待确认+销售待审量
         (nvl(IntelligentOrderWeekly.AVG,0)/7*(nvl(PartsSupplierRelation.OrderCycle,0)+nvl(PartsSupplierRelation.MonthlyArrivalPeriod,0))+nvl(PartSafeStock.SafeStock,0)
           -nvl(allValidStock.nums,0)-nvl(PurchaseOL.nums,0)-nvl(PC.nums,0)+nvl(SLNC.nums,0)) as PurchaseMethod2,
           
         --方法三采购量=近3个月销量-有效库存-采购在途-采购待确认+销售待审量
         (nvl(IntelligentOrderMonthly.ORDERQUANTITYFOR3M,0)-nvl(allValidStock.nums,0)-nvl(PurchaseOL.nums,0)-nvl(PC.nums,0)+nvl(SLNC.nums,0)) PurchaseMethod3, 
         
         --方法四：采购量=[(近6个月销量 – 近3个月销量)/3*20%+(T-2月销量+T-3月销量)/2*30%+T-1月销量*50%]/30*（订货周期+常规到货周期）-有效库存-采购在途-采购待确认量+销售待审量
            (((nvl(IntelligentOrderMonthly.ORDERQUANTITYFOR6M,0)-nvl(IntelligentOrderMonthly.ORDERQUANTITYFOR3M,0))/3*0.2
              +(nvl(IntelligentOrderMonthly.ORDERQUANTITY2,0)+nvl(IntelligentOrderMonthly.ORDERQUANTITY3,0))/2*0.3
              + nvl(IntelligentOrderMonthly.ORDERQUANTITY1,0)*0.5)/30*nvl(PartsSupplierRelation.OrderCycle,0)+nvl(PartsSupplierRelation.MonthlyArrivalPeriod,0)
              -nvl(allValidStock.nums,0)-nvl(PurchaseOL.nums,0)-nvl(PC.nums,0)+nvl(SLNC.nums,0)) as PurchaseMethod4,
          0,
          PartsSupplierRelation.SupplierId,
          PartsSupplierRelation.SupplierPartCode,
          PartsSupplierRelation.DefaultOrderWarehouseId,
          PartsSupplierRelation.DefaultOrderWarehouseName,
          0,
          to_char(sysdate, 'yyyymm' )
    from PartsBranch
    
    left join PartsSupplierRelation
      on PartsSupplierRelation.BranchId = PartsBranch.BranchId and PartsSupplierRelation.IsPrimary=1
     and PartsSupplierRelation.PartsSalesCategoryId =
         PartsBranch.PartsSalesCategoryId
     and PartsBranch.Partid = PartsSupplierRelation.Partid
     
    left join IntelligentOrderWeekly --智能订货周度累计表
      on IntelligentOrderWeekly.BRANCHID = PartsBranch.BranchId
     and IntelligentOrderWeekly.SALESCATEGORYID =
         PartsBranch.PartsSalesCategoryId
     and IntelligentOrderWeekly.PARTID = PartsBranch.PartId
     
    left join PartSafeStock --配件安全库存
      on PartSafeStock.BRANCHID = PartsBranch.BranchId
     and PartSafeStock.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and PartSafeStock.PARTID = PartsBranch.PartId   

     
    left join PartsPlannedPrice --配件计划价
      on PartsPlannedPrice.OwnerCompanyId = PartsBranch.BranchId
     and PartsPlannedPrice.PartsSalesCategoryId =
         PartsBranch.PartsSalesCategoryId
     and PartsPlannedPrice.SparePartId = PartsBranch.PartId
     
    left join IntelligentOrderMonthly --智能订货月度累计表
      on IntelligentOrderMonthly.BRANCHID = PartsBranch.BranchId
     and IntelligentOrderMonthly.SALESCATEGORYID =
         PartsBranch.PartsSalesCategoryId
     and IntelligentOrderMonthly.PARTID = PartsBranch.PartId
     
    left join AllValidStock --可用库存，视图
     on allValidStock.BranchId = PartsBranch.BranchId
     and allValidStock.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and allValidStock.PartId = PartsBranch.PartId 
  
  --采购在途 供应商已确认但是未生成发运单+供应商已生成发运单但是未入库的
    left join PartsPurchaseOnLine  PurchaseOL  --视图
      on PurchaseOL.BranchId = PartsBranch.BranchId
     and PurchaseOL.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and PurchaseOL.PartId = PartsBranch.PartId
     
  --调拨在途：对方已出库未发运的+对方已发运未收货确认的+已生成入库计划未入库的
    left join (
               --对方已出库未发运的
               select BranchId,
                       PartsSalesCategoryId,
                       PartId,
                       sum(nums) as nums
                 from (select plans.StorageCompanyId as BranchId,
                               plans.PartsSalesCategoryId,
                               detail.SparePartId as PartId,
                               sum(detail.OutboundFulfillment) nums
                          from PartsOutboundPlanDetail detail
                         inner join PartsOutboundPlan plans
                            on detail.PartsOutboundPlanId = plans.id
                         inner join PartsLogisticBatch batchs
                            on batchs.SourceId = plans.id
                           and batchs.SourceType = 1 --配件出库计划
                           and batchs.ShippingStatus = 1 --待运
                         where plans.OutboundType = 4 --配件调拨
                         group by plans.StorageCompanyId,
                                  plans.PartsSalesCategoryId,
                                  detail.SparePartId
                        union all --对方已发运未收货确认的
                        select orders.BranchId,
                               orders.PartsSalesCategoryId,
                               detail.sparepartid,
                               sum(nvl(detail.Shippingamount, 0) -
                                   nvl(detail.ConfirmedAmount, 0)) nums
                          from PartsShippingOrderDetail detail
                         inner join PartsShippingOrder orders
                            on detail.PartsShippingOrderId = orders.id
                         inner join PartsShippingOrderRef refs
                            on orders.id = refs.PartsShippingOrderId
                         inner join PartsOutboundBill bill
                            on bill.id = refs.partsoutboundbillid
                         where bill.OutboundType = 4
                           and orders.status = 1 --发运单状态为新建
                         group by orders.BranchId,
                                  orders.PartsSalesCategoryId,
                                  detail.sparepartid
                        union all --已生成入库计划未入库
                        select plans.StorageCompanyId as BranchId,
                               plans.PartsSalesCategoryId,
                               detail.SparePartId,
                               sum(nvl(PlannedAmount, 0) -  --计划量
                                   nvl(InspectedQuantity, 0)) nums --检验量
                          from Partsinboundplandetail detail
                         inner join PartsInboundPlan plans
                            on detail.PartsInboundPlanId = plans.id
                         where plans.InboundType = 3 --配件调拨
                           and plans.status in (1, 4) --新建、部分检验
                         group by plans.StorageCompanyId,
                                  plans.PartsSalesCategoryId,
                                  detail.SparePartId)
                group by BranchId, PartsSalesCategoryId, PartId) TransferOL
      on TransferOL.BranchId = PartsBranch.BranchId
     and TransferOL.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and TransferOL.PartId = PartsBranch.PartId
     
     --替换件库存：统计配件可采购可销售的替换件品牌仓库的所有可用库存
     left join 
     ( select BranchId,PartsSalesCategoryId,OldPartId as PartId,sum(nums) as nums from 
          (select AllValidStock.BranchId,AllValidStock.PartsSalesCategoryId,PartsReplacement.OldPartId,PartsReplacement.NewPartId,AllValidStock.nums
         from PartsReplacement 
         inner join AllValidStock on AllValidStock.PartId=NewPartId
         inner join PartsBranch on PartsReplacement.Newpartid=PartsBranch.Partid and AllValidStock.BranchId = PartsBranch.Branchid 
                and AllValidStock.PartsSalesCategoryId=PartsBranch.Partssalescategoryid 
         where   PartsReplacement.Status=1 and PartsBranch.IsOrderable=1 and PartsBranch.IsSalable=1
         ) group by BranchId,PartsSalesCategoryId,OldPartId
     )Replacement 
     on Replacement.BranchId = PartsBranch.BranchId
     and Replacement.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and Replacement.PartId = PartsBranch.PartId
     
     --替换件采购在途:统计配件可采购可销售的替换件供应商已确认但是未生成发运单+供应商已生成发运单但是未入库的
     left join (select BranchId,PartsSalesCategoryId,Oldpartid as PartId,sum(nums) as nums from
                    (select PartsPurchaseOnLine.BranchId,
                      PartsPurchaseOnLine.PartsSalesCategoryId,
                      PartsReplacement.Oldpartid,
                      PartsReplacement.Newpartid,
                      PartsPurchaseOnLine.nums
                 from PartsReplacement 
                 inner join PartsPurchaseOnLine on PartsReplacement.Newpartid=PartsPurchaseOnLine.PartId
                 inner join PartsBranch on PartsReplacement.Newpartid=PartsBranch.Partid 
                    and PartsPurchaseOnLine.BranchId=PartsBranch.Branchid
                    and PartsPurchaseOnLine.PartsSalesCategoryId=PartsBranch.Partssalescategoryid
                 where PartsReplacement.Status=1 and PartsBranch.Isorderable=1 and PartsBranch.Issalable=1
                   ) group by BranchId,PartsSalesCategoryId,Oldpartid
                 ) ReplacePurchaseOL 
      on ReplacePurchaseOL.BranchId = PartsBranch.BranchId
     and ReplacePurchaseOL.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and ReplacePurchaseOL.PartId = PartsBranch.PartId

   --互换件库存：统计配件可采购可销售的所有互换件品牌仓库的所有可用库存。
     left join 
     ( 
        select A.BranchId,A.PartsSalesCategoryId,A.Partid,nvl(B.nums,0)-nvl(A.nums,0) as nums from 
        (select AllValidStock.BranchId,AllValidStock.PartsSalesCategoryId,PartsExchange.Exchangecode,PartsExchange.Partid,sum(AllValidStock.nums) as nums from PartsExchange
         inner join AllValidStock on AllValidStock.PartId=PartsExchange.PartId 
         inner join PartsBranch 
                 on PartsExchange.PartId=PartsBranch.Partid 
                 and AllValidStock.BranchId = PartsBranch.Branchid 
                 and AllValidStock.PartsSalesCategoryId=PartsBranch.Partssalescategoryid
         where PartsExchange.status=1  and PartsBranch.IsOrderable=1 and PartsBranch.IsSalable=1
         group by AllValidStock.BranchId,AllValidStock.PartsSalesCategoryId,PartsExchange.Exchangecode,PartsExchange.Partid
        ) A left join 
        (select AllValidStock.BranchId,AllValidStock.PartsSalesCategoryId,ExchangeCode,sum(AllValidStock.nums) as nums from PartsExchange
         inner join AllValidStock on AllValidStock.PartId=PartsExchange.PartId 
         inner join PartsBranch 
                on PartsExchange.PartId=PartsBranch.Partid 
                and AllValidStock.BranchId = PartsBranch.Branchid 
                and AllValidStock.PartsSalesCategoryId=PartsBranch.Partssalescategoryid
         where PartsExchange.status=1  and PartsBranch.IsOrderable=1 and PartsBranch.IsSalable=1
         group by AllValidStock.BranchId,AllValidStock.PartsSalesCategoryId,ExchangeCode
        ) B
        on A.BranchId=B.BranchId and A.PartsSalesCategoryId=B.PartsSalesCategoryId and A.Exchangecode=B.Exchangecode
     )PartsExchange 
     on PartsExchange.BranchId = PartsBranch.BranchId
     and PartsExchange.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and PartsExchange.PartId = PartsBranch.PartId
   
  --互换件采购在途：统计配件可采购可销售的互换件供应商已确认但是未生成发运单+供应商已生成发运单但是未入库的
     left join 
     ( 
        select A.BranchId,A.PartsSalesCategoryId,A.Partid,nvl(B.nums,0)-nvl(A.nums,0) as nums from 
        (select PartsPurchaseOnLine.BranchId,PartsPurchaseOnLine.PartsSalesCategoryId,PartsExchange.ExchangeCode,PartsExchange.Partid,PartsPurchaseOnLine.nums  
         from PartsExchange
         inner join PartsPurchaseOnLine on PartsExchange.Partid=PartsPurchaseOnLine.PartId
         inner join PartsBranch on PartsExchange.PartId=PartsBranch.Partid and PartsPurchaseOnLine.BranchId = PartsBranch.Branchid 
                and PartsPurchaseOnLine.PartsSalesCategoryId=PartsBranch.Partssalescategoryid
         where PartsExchange.status=1  and PartsBranch.IsOrderable=1 and PartsBranch.IsSalable=1
        ) A left join 
        (select PartsPurchaseOnLine.BranchId,PartsPurchaseOnLine.PartsSalesCategoryId,PartsExchange.ExchangeCode,sum(PartsPurchaseOnLine.nums) as nums  
         from PartsExchange
         inner join PartsPurchaseOnLine on PartsExchange.Partid=PartsPurchaseOnLine.PartId
         inner join PartsBranch on PartsExchange.PartId=PartsBranch.Partid and PartsPurchaseOnLine.BranchId = PartsBranch.Branchid 
                and PartsPurchaseOnLine.PartsSalesCategoryId=PartsBranch.Partssalescategoryid
         where PartsExchange.status=1  and PartsBranch.IsOrderable=1 and PartsBranch.IsSalable=1
         group by PartsPurchaseOnLine.BranchId,PartsPurchaseOnLine.PartsSalesCategoryId,PartsExchange.ExchangeCode
         ) B
        on A.BranchId=B.BranchId and A.PartsSalesCategoryId=B.PartsSalesCategoryId and A.Exchangecode=B.Exchangecode
     )PartsExchangeOL
     on PartsExchangeOL.BranchId = PartsBranch.BranchId
     and PartsExchangeOL.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and PartsExchangeOL.PartId = PartsBranch.PartId  
  
  
   
   --销售待审量: 配件品牌对应的所有提交或者部分审批中未审核部分。
   --统购品牌需要再合计上与统购品牌属于同一分公司代码下的其他品牌的该配件的所有提交或者部分审批中未审核的配件数量。
    left join
      ( select branchid,SalesCategoryId,sparepartid as PartId,sum(nums) as nums from 
        (
          select orders.branchid,orders.SalesCategoryId,detail.sparepartid, sum(nvl(detail.OrderedQuantity,0)-nvl(detail.ApproveQuantity,0)) as nums --- 订货数 、审批数
            from  PartsSalesOrderdetail detail inner join PartsSalesOrder orders on detail.PartsSalesOrderId=orders.id
            where orders.status in (2,4) and orders.SalesCategoryName<>'统购' -- 提交 、部分审批
            group by orders.branchid,orders.SalesCategoryId,detail.sparepartid
         union all
            select orders.branchid,(select id from PartsSalesCategory where name='统购' and status=1) as SalesCategoryId,detail.sparepartid, sum(nvl(detail.OrderedQuantity,0)-nvl(detail.ApproveQuantity,0)) as nums 
            from  PartsSalesOrderdetail detail 
            inner join PartsSalesOrder orders on detail.PartsSalesOrderId=orders.id
            inner join (select PartsSalesOrder.branchid,PartsSalesOrderdetail.Sparepartid from PartsSalesOrder 
                          inner join PartsSalesOrderdetail on PartsSalesOrder.id=PartsSalesOrderdetail.Partssalesorderid 
                         where PartsSalesOrder.status in (2,4) and PartsSalesOrder.SalesCategoryName='统购' 
                         group by PartsSalesOrder.branchid,PartsSalesOrderdetail.Sparepartid 
                       ) TG on TG.branchid=orders.branchid and TG.Sparepartid=detail.sparepartid
            where orders.status in (2,4)  and orders.branchid=(select branchid from PartsSalesCategory where Name='统购' and status=1)
            group by orders.branchid,detail.sparepartid 
          ) group by branchid,SalesCategoryId,sparepartid
      )SLNC
     on SLNC.BranchId = PartsBranch.BranchId
     and SLNC.SalesCategoryId = PartsBranch.PartsSalesCategoryId
     and SLNC.PartId = PartsBranch.PartId 
        
   --采购待确认量：采购订单中提交但是供应商未确认的部分    订货量 - 确认量
   left join 
       (select orders.BranchId,orders.PartsSalesCategoryId,detail.sparepartid as PartId, sum(nvl(detail.Orderamount,0)-nvl(detail.ConfirmedAmount,0)) as nums 
         from PartsPurchaseOrderDetail detail inner join PartsPurchaseOrder orders 
         on detail.PartsPurchaseOrderId=orders.id
         where orders.status in (2,3) --提交、部分确认
         group by orders.BranchId,orders.PartsSalesCategoryId,detail.sparepartid
       )PC
       on PC.BranchId = PartsBranch.BranchId
     and PC.PartsSalesCategoryId = PartsBranch.PartsSalesCategoryId
     and PC.PartId = PartsBranch.PartId
     
   where PartsBranch.IsOrderable=1 and PartsBranch.IsSalable=1 and PartsBranch.ProductLifeCycle<>5;
   
  end;


/*   已出库未发运的 , sum（出库清单.出库完成量）
     配件出库计划.出库类型=配件调拨 
     配件出库计划.Id=配件物流批次.源单据Id and
     配件物流批次.源单据类型=配件出库计划 and
     配件物流批次.发运状态=待运

*/


