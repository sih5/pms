
create or replace procedure Prc_InsertIntOrderMonthly is
begin
  insert into intelligentordermonthly
    (id,
    branchid,
    branchcode,
    branchname,
    ordertypename,
    salescategoryid,
    salescategorycode,
    salescategoryname,
    partid,
    partcode,
    partname,
    supplierid,
    suppliercode,
    suppliername,
    partabc,
    productlifecycle,
    plannedpricetype,
    ORDERQUANTITY1,
    ORDERQUANTITY2,    
    ORDERQUANTITY3,      
    ORDERQUANTITY4,
    ORDERQUANTITY5,    
    ORDERQUANTITY6,      
    ORDERQUANTITY7,
    ORDERQUANTITY8,    
    ORDERQUANTITY9, 
    ORDERQUANTITY10,
    ORDERQUANTITY11,    
    ORDERQUANTITY12,
    FREQUENCY1,
    FREQUENCY2,
    FREQUENCY3,
    FREQUENCY4,
    FREQUENCY5,
    FREQUENCY6,
    FREQUENCY7,
    FREQUENCY8,
    FREQUENCY9,
    FREQUENCY10,
    FREQUENCY11,
    FREQUENCY12,
    FREQUENCYFOR12M,
    ORDERQUANTITYFOR3M,
    ORDERQUANTITYFOR6M,
    ORDERQUANTITYFOR12M,
    MONTHAVGFOR3M,
    MONTHAVGFOR6M,
    MONTHAVGFOR12M,
    DAYAVGFOR1M,
    DAYAVGFOR3M,
    DAYAVGFOR6M,
    DAYAVGFOR12M  
     )

    select S_intelligentordermonthly.Nextval,t1.BRANCHID,
    t1.BRANCHCODE,t1.BRANCHNAME,t1.ORDERTYPENAME,
    t1.SALESCATEGORYID,t1.SALESCATEGORYCODE,t1.SALESCATEGORYNAME,t1.PARTID,t1.PARTCODE,t1.PARTNAME,
    t1.SUPPLIERID,t1.SUPPLIERCODE,t1.SUPPLIERNAME,
    t1.PARTABC,t1.PRODUCTLIFECYCLE,t1.PLANNEDPRICETYPE,
    nvl(t1.qty,0) as qty1 ,nvl(t2.qty,0) as qty2 ,nvl(t3.qty,0) as qty3,
    nvl(t4.qty,0) as qty4 ,nvl(t5.qty,0) as qty5 ,nvl(t6.qty,0) as qty6 ,
    nvl(t7.qty,0) as qty7 ,nvl(t8.qty,0) as qty8 ,nvl(t9.qty,0) as qty9 ,
    nvl(t10.qty,0) as qty10 ,nvl(t11.qty,0) as qty11 ,nvl(t12.qty,0) as qty12,
    nvl(t1.fcy,0) as fcy1 ,nvl(t2.fcy,0) as fcy2 ,nvl(t3.fcy,0) as fcy3,
    nvl(t4.fcy,0) as fcy4 ,nvl(t5.fcy,0) as fcy5 ,nvl(t6.fcy,0) as fcy6 ,
    nvl(t7.fcy,0) as fcy7 ,nvl(t8.fcy,0) as fcy8 ,nvl(t9.fcy,0) as fcy9 ,
    nvl(t10.fcy,0) as fcy10 ,nvl(t11.fcy,0) as fcy11 ,nvl(t12.fcy,0) as fcy12,
    nvl(t1.fcy,0)+nvl(t2.fcy,0)+nvl(t3.fcy,0)+nvl(t4.fcy,0)
    + nvl(t5.fcy,0)+nvl(t6.fcy,0)+nvl(t7.fcy,0)+nvl(t8.fcy,0)
    + nvl(t9.fcy,0)+nvl(t10.fcy,0)+nvl(t11.fcy,0)+nvl(t12.fcy,0) as FREQUENCYFOR12M,
    nvl(M3.qty,0) as qtyM3,nvl(M6.qty,0) as qtyM6,nvl(M12.qty,0) as qtyM12,
    nvl(M3.fcy,0) as fcyM3,nvl(M6.fcy,0) as fcyM6,nvl(M12.fcy,0) as fcyM12,
    nvl(t1.dfcy,0) as dfcyM1,nvl(M3.dfcy,0) as dfcyM3,nvl(M6.dfcy,0) as dfcyM6,nvl(M12.dfcy,0) as dfcyM12
     FROM 
       --T-1销量、频次
    (select a.BRANCHID,
    a.BRANCHCODE,a.BRANCHNAME,a.SALESCATEGORYID,a.SALESCATEGORYCODE,a.SALESCATEGORYNAME,a.PARTID,a.PARTCODE,a.PARTNAME,
    a.PARTABC,a.PRODUCTLIFECYCLE,a.PLANNEDPRICETYPE,b.SupplierId,c.code as SUPPLIERCODE,c.name as SUPPLIERNAME,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
        sum(TOTALQUANTITY) as qty,
      sum(TOTALMONTHLYFREQUENCY) fcy ,
        sum(TOTALQUANTITY)/30 as dfcy 
     from  intelligentordermonthlybase a  left join PartsSupplierRelation b
      left join PartsSupplier c on b.supplierid=c.id
     on a.branchid=b.Branchid and b.Partssalescategoryid=a.salescategoryid
     and a.partid=b.Partid and b.IsPrimary=1
    where trunc(thedate,'mm')=trunc(sysdate,'mm') and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by 
    a.BRANCHID,
    a.BRANCHCODE,a.BRANCHNAME,a.SALESCATEGORYID,a.SALESCATEGORYCODE,a.SALESCATEGORYNAME,a.PARTID,a.PARTCODE,a.PARTNAME,
    a.PARTABC,a.PRODUCTLIFECYCLE,a.PLANNEDPRICETYPE,b.SupplierId,c.code,c.name,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end
      ) t1 
    --T-2销量、频次
      left join 
      (select BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
      sum(TOTALQUANTITY) as qty,
      sum(TOTALMONTHLYFREQUENCY) fcy 
     from  intelligentordermonthlybase
    where trunc(thedate,'mm')=trunc(add_months(sysdate,-1),'mm') and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by 
    BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end 
      ) t2 on t1.BRANCHID=t2.BRANCHID
    and t1.SALESCATEGORYID=t2.SALESCATEGORYID
    and t1.PARTID=t2.PARTID and t1.ORDERTYPENAME=t2.ORDERTYPENAME
    --T-3销量、频次
      left join 
      (select BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
      sum(TOTALQUANTITY) as qty,
      sum(TOTALMONTHLYFREQUENCY) fcy 
     from  intelligentordermonthlybase
    where  trunc(thedate,'mm')=trunc(add_months(sysdate,-2),'mm') and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by 
    BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end 
      ) t3 on t1.BRANCHID=t3.BRANCHID
    and t1.SALESCATEGORYID=t3.SALESCATEGORYID
    and t1.PARTID=t3.PARTID and t1.ORDERTYPENAME=t3.ORDERTYPENAME
    --T-4销量、频次
      left join 
      (select BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
      sum(TOTALQUANTITY) as qty,
      sum(TOTALMONTHLYFREQUENCY) fcy 
     from  intelligentordermonthlybase
    where trunc(thedate,'mm')=trunc(add_months(sysdate,-3),'mm') and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by 
    BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end 
      ) t4 on t1.BRANCHID=t4.BRANCHID
    and t1.SALESCATEGORYID=t4.SALESCATEGORYID
    and t1.PARTID=t4.PARTID and t1.ORDERTYPENAME=t4.ORDERTYPENAME
    --T-5销量、频次
      left join 
      (select BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
      sum(TOTALQUANTITY) as qty,
      sum(TOTALMONTHLYFREQUENCY) fcy 
     from  intelligentordermonthlybase
    where trunc(thedate,'mm')=trunc(add_months(sysdate,-4),'mm') and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by 
    BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end 
      ) t5 on t1.BRANCHID=t5.BRANCHID
    and t1.SALESCATEGORYID=t5.SALESCATEGORYID
    and t1.PARTID=t5.PARTID and t1.ORDERTYPENAME=t5.ORDERTYPENAME
    --T-6销量、频次
      left join 
      (select BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
      sum(TOTALQUANTITY) as qty,
      sum(TOTALMONTHLYFREQUENCY) fcy 
     from  intelligentordermonthlybase
    where trunc(thedate,'mm')=trunc(add_months(sysdate,-5),'mm') and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by 
    BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end 
      ) t6 on t1.BRANCHID=t6.BRANCHID
    and t1.SALESCATEGORYID=t6.SALESCATEGORYID
    and t1.PARTID=t6.PARTID and t1.ORDERTYPENAME=t6.ORDERTYPENAME
    --T-7销量、频次
      left join 
      (select BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
      sum(TOTALQUANTITY) as qty,
      sum(TOTALMONTHLYFREQUENCY) fcy 
     from  intelligentordermonthlybase
    where trunc(thedate,'mm')=trunc(add_months(sysdate,-6),'mm') and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by 
    BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end 
      ) t7 on t1.BRANCHID=t7.BRANCHID
    and t1.SALESCATEGORYID=t7.SALESCATEGORYID
    and t1.PARTID=t7.PARTID and t1.ORDERTYPENAME=t7.ORDERTYPENAME
    --T-8销量、频次
      left join 
      (select BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
      sum(TOTALQUANTITY) as qty,
      sum(TOTALMONTHLYFREQUENCY) fcy 
     from  intelligentordermonthlybase
    where trunc(thedate,'mm')=trunc(add_months(sysdate,-7),'mm') and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by 
    BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end 
      ) t8 on t1.BRANCHID=t8.BRANCHID
    and t1.SALESCATEGORYID=t8.SALESCATEGORYID
    and t1.PARTID=t8.PARTID and t1.ORDERTYPENAME=t8.ORDERTYPENAME
    --T-9销量、频次
      left join 
      (select BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
      sum(TOTALQUANTITY) as qty,
      sum(TOTALMONTHLYFREQUENCY) fcy 
     from  intelligentordermonthlybase
    where trunc(thedate,'mm')=trunc(add_months(sysdate,-8),'mm') and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by 
    BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end 
      ) t9 on t1.BRANCHID=t9.BRANCHID
    and t1.SALESCATEGORYID=t9.SALESCATEGORYID
    and t1.PARTID=t9.PARTID and t1.ORDERTYPENAME=t9.ORDERTYPENAME
    --T-10销量、频次
      left join 
      (select BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
      sum(TOTALQUANTITY) as qty,
      sum(TOTALMONTHLYFREQUENCY) fcy 
     from  intelligentordermonthlybase
    where trunc(thedate,'mm')=trunc(add_months(sysdate,-9),'mm') and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by 
    BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end 
      ) t10 on t1.BRANCHID=t10.BRANCHID
    and t1.SALESCATEGORYID=t10.SALESCATEGORYID
    and t1.PARTID=t10.PARTID and t1.ORDERTYPENAME=t10.ORDERTYPENAME
    --T-11销量、频次
      left join 
      (select BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
      sum(TOTALQUANTITY) as qty,
      sum(TOTALMONTHLYFREQUENCY) fcy 
     from  intelligentordermonthlybase
    where trunc(thedate,'mm')=trunc(add_months(sysdate,-10),'mm') and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by 
    BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end 
      ) t11 on t1.BRANCHID=t11.BRANCHID
    and t1.SALESCATEGORYID=t11.SALESCATEGORYID
    and t1.PARTID=t11.PARTID and t1.ORDERTYPENAME=t11.ORDERTYPENAME
    --T-12销量、频次
      left join 
      (select BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
      sum(TOTALQUANTITY) as qty,
      sum(TOTALMONTHLYFREQUENCY) fcy 
     from  intelligentordermonthlybase
    where trunc(thedate,'mm')=trunc(add_months(sysdate,-11),'mm')  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by 
    BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end 
      ) t12 on t1.BRANCHID=t12.BRANCHID
    and t1.SALESCATEGORYID=t12.SALESCATEGORYID
    and t1.PARTID=t12.PARTID and t1.ORDERTYPENAME=t12.ORDERTYPENAME
    --近3个月销量
      left join 
      (select BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
      sum(TOTALQUANTITY) as qty,
        sum(TOTALQUANTITY)/3 as fcy,
        sum(TOTALQUANTITY)/( trunc(sysdate,'mm')- trunc(add_months(sysdate,-2),'mm') ) as dfcy
     from  intelligentordermonthlybase
    where trunc(thedate,'mm')>=trunc(add_months(sysdate,-2),'mm')  AND trunc(thedate,'mm')<=trunc(sysdate,'mm')  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by 
    BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end 
      ) M3 on t1.BRANCHID=M3.BRANCHID
    and t1.SALESCATEGORYID=M3.SALESCATEGORYID
    and t1.PARTID=M3.PARTID and t1.ORDERTYPENAME=M3.ORDERTYPENAME
    --近6个月销量
      left join 
      (select BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
      sum(TOTALQUANTITY) as qty,
        sum(TOTALQUANTITY)/6 as fcy,
            sum(TOTALQUANTITY)/( trunc(sysdate,'mm')- trunc(add_months(sysdate,-5),'mm') ) as dfcy
     from  intelligentordermonthlybase
    where  trunc(thedate,'mm')>=trunc(add_months(sysdate,-5),'mm')  AND trunc(thedate,'mm')<=trunc(sysdate,'mm') and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by 
    BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end 
      ) M6 on t1.BRANCHID=M6.BRANCHID
    and t1.SALESCATEGORYID=M6.SALESCATEGORYID
    and t1.PARTID=M6.PARTID and t1.ORDERTYPENAME=M6.ORDERTYPENAME  
    --近12个月销量
      left join 
      (select BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end as ORDERTYPENAME,
      sum(TOTALQUANTITY) as qty,
        sum(TOTALQUANTITY)/12 as fcy,
           sum(TOTALQUANTITY)/365 as dfcy
     from  intelligentordermonthlybase
    where trunc(thedate,'mm')>=trunc(add_months(sysdate,-11),'mm')  AND trunc(thedate,'mm')<=trunc(sysdate,'mm')  and BUSINESSTYPE=2 and ORDERTYPENAME is not null
    group by 
    BRANCHID,SALESCATEGORYID,PARTID,
    case ORDERTYPENAME when '新产品订单' then '新产品' when '专项订单' then  '专项' when '直供订单' then  '直供' else '其他' end 
      ) M12 on t1.BRANCHID=M12.BRANCHID
    and t1.SALESCATEGORYID=M12.SALESCATEGORYID
    and t1.PARTID=M12.PARTID and t1.ORDERTYPENAME=M12.ORDERTYPENAME  ;

end Prc_InsertIntOrderMonthly;
