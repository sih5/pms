create or replace procedure addPlanPriceCategory is --更新计划价分类
begin
  update PartsBranch
     set ProductLifeCycle = 1
   where exists (select ppp.PlannedPrice
            from PartsBranch p --配件营销信息
           inner join PartsPlannedPrice ppp --配件计划价
              on p.PartsSalesCategoryId = ppp.PartsSalesCategoryId
           where ppp.PlannedPrice = 0);
  update PartsBranch
     set ProductLifeCycle =
         (select ppc.PriceCategory
            from PlanPriceCategory ppc --计划价格分类
           inner join PartsBranch p --配件营销信息
              on ppc.BranchId = p.BranchId
           inner join PartsPlannedPrice ppp --配件计划价
              on p.PartsSalesCategoryId = ppp.PartsSalesCategoryId
           where ppp.PlannedPrice between ppc.PriceMin and ppc.pricemax)
   where exists
   (select *
            from (select ppc.PriceCategory
                    from PlanPriceCategory ppc --计划价格分类
                   inner join PartsBranch p --配件营销信息
                      on ppc.BranchId = p.BranchId
                   inner join PartsPlannedPrice ppp --配件计划价
                      on p.PartsSalesCategoryId = ppp.PartsSalesCategoryId
                   where ppp.PlannedPrice between ppc.PriceMin and
                         ppc.pricemax));
end addPlanPriceCategory;
