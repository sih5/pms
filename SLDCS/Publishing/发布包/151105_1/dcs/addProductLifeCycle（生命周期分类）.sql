create or replace procedure addProductLifeCycle is
begin
  update PartsBranch set ProductLifeCycle = 5 where IsOrderable = 0; --��̭��
  update PartsBranch a --����̭��
     set a.ProductLifeCycle =
         (select pabc.CycleCategory
            from PartsBranch p
            inner join ABCStrategy a
              on p.branchid = a.branchid
           where p.IsOrderable = 1
             and a.SamplingDuration = round(MONTHS_BETWEEN(sysdate,p.CreateTime)))
   where exists
   (select *
            from (select pabc.CycleCategory
                    from PartsBranch p
                    inner join ABCStrategy a
                      on p.branchid = a.branchid
                   where p.IsOrderable = 1
                     and a.SamplingDuration = round(MONTHS_BETWEEN(sysdate,p.CreateTime)));
end addProductLifeCycle;
/
