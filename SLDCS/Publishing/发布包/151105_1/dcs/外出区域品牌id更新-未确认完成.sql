
update dcs.ServiceTripPriceGrade t
   set t.Partssalescategoryid =
       (select distinct c.Partssalescategoryid
          from dcs.servicetrippricerate v
         inner join dcs.serviceproductlineview c
            on c.ProductLineId = v.serviceproductlineid
         where v.servicetrippricegradeid = t.id)
 where t.status = 1
 and name not in('服务工程师外出等级','西北地区','西南地区','中东部地区','特殊地区');--dcs
 
update yxdcs.ServiceTripPriceGrade t
   set t.Partssalescategoryid =
       nvl((select distinct c.Partssalescategoryid
          from yxdcs.servicetrippricerate v
         inner join yxdcs.serviceproductlineview c
            on c.ProductLineId = v.serviceproductlineid
         where v.servicetrippricegradeid = t.id),0)
 where t.status = 1
 ;--yxdcs
 
update sddcs.ServiceTripPriceGrade t
   set t.Partssalescategoryid =
       (select distinct c.Partssalescategoryid
          from sddcs.servicetrippricerate v
         inner join sddcs.serviceproductlineview c
            on c.ProductLineId = v.serviceproductlineid
         where v.servicetrippricegradeid = t.id)
 where t.status = 1
 ;--sddcs
 update sddcs.ServiceTripPriceGrade t
   set t.Partssalescategoryid =
       (select distinct c.Partssalescategoryid
          from sddcs.servicetrippricerate v
         inner join sddcs.serviceproductlineview c
            on c.ProductLineId = v.serviceproductlineid
         where v.servicetrippricegradeid = t.id)
 where t.status = 1  and name<>'工程车西部区域外出服务费等级';
 
select distinct a.code,a.name,
       (select name
          from dcs.partssalescategory d
         where d.id = c.Partssalescategoryid) brand
  from dcs.ServiceTripPriceGrade a
 inner join dcs.servicetrippricerate b
    on b.servicetrippricegradeid = a.id
 inner join dcs.serviceproductlineview c
    on c.ProductLineId = b.serviceproductlineid
 --group by a.code
 
 --dcs
update dcs.ServiceTripPriceGrade t
   set t.Partssalescategoryid =
       (select id from dcs.partssalescategory where name = '雷萨泵送'),
       t.code                 = '雷萨泵送' || t.code,
       t.name                 = '雷萨泵送' || t.name
 where t.name = '服务工程师外出等级';
insert into dcs.ServiceTripPriceGrade
  (branchid,
   id,
   code,
   name,
   regiondescription,
   status,
   createtime,
   creatorid,
   creatorname,
   Partssalescategoryid)
  select t.branchid,
         s_ServiceTripPriceGrade.Nextval,,
         '搅拌车服务工程师外出等级',
         '搅拌车服务工程师外出等级',
         t.regiondescription,
         t.status,
         sysdate,
         1,
         'Admin',
         (select id from dcs.partssalescategory where name = '搅拌车')
    from dcs.ServiceTripPriceGrade t
   where t.name = '雷萨泵送服务工程师外出等级';
update dcs.servicetrippricerate b
   set b.servicetrippricegradeid =
       (select id
          from dcs.servicetrippricegrade
         where name = '搅拌车服务工程师外出等级')
 where exists (select 1
          from dcs.ServiceTripPriceGrade a
         where a.id = b.servicetrippricegradeid
           and a.name = '雷萨泵送服务工程师外出等级')
   and exists (select 1
          from dcs.serviceproductlineview c
         inner join dcs.partssalescategory d
            on d.id = c.Partssalescategoryid
         where b.serviceproductlineid = c.ProductLineId
           and d.name = '搅拌车');
update dcs.dealerserviceinfo a
   set a.outfeegradeid =
       (select id
          from dcs.ServiceTripPriceGrade b
         where name = '搅拌车服务工程师外出等级')
 where exists (select 1
          from dcs.ServiceTripPriceGrade b
         where a.outfeegradeid = b.id
           and b.name = '雷萨泵送服务工程师外出等级')
   and exists (select 1
          from dcs.partssalescategory c
         where c.id = a.partssalescategoryid
           and c.name = '搅拌车');




update dcs.ServiceTripPriceGrade t
   set t.Partssalescategoryid =
       (select id from dcs.partssalescategory where name = '雷萨泵送'),
       t.code                 = '雷萨泵送' || t.code,
       t.name                 = '雷萨泵送' || t.name
 where t.name = '特殊地区';
insert into dcs.ServiceTripPriceGrade
  (branchid,
   id,
   code,
   name,
   regiondescription,
   status,
   createtime,
   creatorid,
   creatorname,
   Partssalescategoryid)
  select t.branchid,
         s_ServiceTripPriceGrade.Nextval,,
         '搅拌车特殊地区',
         '搅拌车特殊地区',
         t.regiondescription,
         t.status,
         sysdate,
         1,
         'Admin',
         (select id from dcs.partssalescategory where name = '搅拌车')
    from dcs.ServiceTripPriceGrade t
   where t.name = '雷萨泵送特殊地区';
update dcs.servicetrippricerate b
   set b.servicetrippricegradeid =
       (select id
          from dcs.servicetrippricegrade
         where name = '搅拌车特殊地区')
 where exists (select 1
          from dcs.ServiceTripPriceGrade a
         where a.id = b.servicetrippricegradeid
           and a.name = '雷萨泵送特殊地区')
   and exists (select 1
          from dcs.serviceproductlineview c
         inner join dcs.partssalescategory d
            on d.id = c.Partssalescategoryid
         where b.serviceproductlineid = c.ProductLineId
           and d.name = '搅拌车');
update dcs.dealerserviceinfo a
   set a.outfeegradeid =
       (select id
          from dcs.ServiceTripPriceGrade b
         where name = '搅拌车特殊地区')
 where exists (select 1
          from dcs.ServiceTripPriceGrade b
         where a.outfeegradeid = b.id
           and b.name = '雷萨泵送特殊地区')
   and exists (select 1
          from dcs.partssalescategory c
         where c.id = a.partssalescategoryid
           and c.name = '搅拌车');



update dcs.ServiceTripPriceGrade t
   set t.Partssalescategoryid =
       (select id from dcs.partssalescategory where name = '雷萨泵送'),
       t.code                 = '雷萨泵送' || t.code,
       t.name                 = '雷萨泵送' || t.name
 where t.name = '西北地区';
insert into dcs.ServiceTripPriceGrade
  (branchid,
   id,
   code,
   name,
   regiondescription,
   status,
   createtime,
   creatorid,
   creatorname,
   Partssalescategoryid)
  select t.branchid,
         s_ServiceTripPriceGrade.Nextval,,
         '搅拌车西北地区',
         '搅拌车西北地区',
         t.regiondescription,
         t.status,
         sysdate,
         1,
         'Admin',
         (select id from dcs.partssalescategory where name = '搅拌车')
    from dcs.ServiceTripPriceGrade t
   where t.name = '雷萨泵送西北地区';
update dcs.servicetrippricerate b
   set b.servicetrippricegradeid =
       (select id
          from dcs.servicetrippricegrade
         where name = '搅拌车西北地区')
 where exists (select 1
          from dcs.ServiceTripPriceGrade a
         where a.id = b.servicetrippricegradeid
           and a.name = '雷萨泵送西北地区')
   and exists (select 1
          from dcs.serviceproductlineview c
         inner join dcs.partssalescategory d
            on d.id = c.Partssalescategoryid
         where b.serviceproductlineid = c.ProductLineId
           and d.name = '搅拌车');
update dcs.dealerserviceinfo a
   set a.outfeegradeid =
       (select id
          from dcs.ServiceTripPriceGrade b
         where name = '搅拌车西北地区')
 where exists (select 1
          from dcs.ServiceTripPriceGrade b
         where a.outfeegradeid = b.id
           and b.name = '雷萨泵送西北地区')
   and exists (select 1
          from dcs.partssalescategory c
         where c.id = a.partssalescategoryid
           and c.name = '搅拌车');           



update dcs.ServiceTripPriceGrade t
   set t.Partssalescategoryid =
       (select id from partssalescategory where name = '雷萨泵送'),
       t.code                 = '雷萨泵送' || t.code,
       t.name                 = '雷萨泵送' || t.name
 where t.name = '中东部地区';
insert into dcs.ServiceTripPriceGrade
  (branchid,
   id,
   code,
   name,
   regiondescription,
   status,
   createtime,
   creatorid,
   creatorname,
   Partssalescategoryid)
  select t.branchid,
         s_ServiceTripPriceGrade.Nextval,,
         '搅拌车中东部地区',
         '搅拌车中东部地区',
         t.regiondescription,
         t.status,
         sysdate,
         1,
         'Admin',
         (select id from dcs.partssalescategory where name = '搅拌车')
    from dcs.ServiceTripPriceGrade t
   where t.name = '雷萨泵送中东部地区';
update dcs.servicetrippricerate b
   set b.servicetrippricegradeid =
       (select id
          from dcs.servicetrippricegrade
         where name = '搅拌车中东部地区')
 where exists (select 1
          from dcs.ServiceTripPriceGrade a
         where a.id = b.servicetrippricegradeid
           and a.name = '雷萨泵送中东部地区')
   and exists (select 1
          from dcs.serviceproductlineview c
         inner join dcs.partssalescategory d
            on d.id = c.Partssalescategoryid
         where b.serviceproductlineid = c.ProductLineId
           and d.name = '搅拌车');
update dcs.dealerserviceinfo a
   set a.outfeegradeid =
       (select id
          from dcs.ServiceTripPriceGrade b
         where name = '搅拌车中东部地区')
 where exists (select 1
          from dcs.ServiceTripPriceGrade b
         where a.outfeegradeid = b.id
           and b.name = '雷萨泵送中东部地区')
   and exists (select 1
          from dcs.partssalescategory c
         where c.id = a.partssalescategoryid
           and c.name = '搅拌车');      
           

update dcs.ServiceTripPriceGrade t
   set t.Partssalescategoryid =
       (select id from dcs.partssalescategory where name = '雷萨泵送'),
       t.code                 = '雷萨泵送' || t.code,
       t.name                 = '雷萨泵送' || t.name
 where t.name = '西南地区';
insert into dcs.ServiceTripPriceGrade
  (branchid,
   id,
   code,
   name,
   regiondescription,
   status,
   createtime,
   creatorid,
   creatorname,
   Partssalescategoryid)
  select t.branchid,
         s_ServiceTripPriceGrade.Nextval,,
         '搅拌车西南地区',
         '搅拌车西南地区',
         t.regiondescription,
         t.status,
         sysdate,
         1,
         'Admin',
         (select id from dcs.partssalescategory where name = '搅拌车')
    from dcs.ServiceTripPriceGrade t
   where t.name = '雷萨泵送西南地区';
update dcs.servicetrippricerate b
   set b.servicetrippricegradeid =
       (select id
          from dcs.servicetrippricegrade
         where name = '搅拌车西南地区')
 where exists (select 1
          from dcs.ServiceTripPriceGrade a
         where a.id = b.servicetrippricegradeid
           and a.name = '雷萨泵送西南地区')
   and exists (select 1
          from dcs.serviceproductlineview c
         inner join dcs.partssalescategory d
            on d.id = c.Partssalescategoryid
         where b.serviceproductlineid = c.ProductLineId
           and d.name = '搅拌车');
update dcs.dealerserviceinfo a
   set a.outfeegradeid =
       (select id
          from dcs.ServiceTripPriceGrade b
         where name = '搅拌车西南地区')
 where exists (select 1
          from dcs.ServiceTripPriceGrade b
         where a.outfeegradeid = b.id
           and b.name = '雷萨泵送西南地区')
   and exists (select 1
          from dcs.partssalescategory c
         where c.id = a.partssalescategoryid
           and c.name = '搅拌车');      
           
--gcdcs


update gcdcs.ServiceTripPriceGrade t
   set t.Partssalescategoryid =
       (select id from gcdcs.partssalescategory where name = '工程车(北区)'),
       t.code                =
       '工程车(北区)'  || t.code,
       t.name                 = '工程车(北区)' || t.name
 where t.name = '工程车西部区域外出服务费等级';
insert into gcdcs.ServiceTripPriceGrade
  (branchid,
   id,
   code,
   name,
   regiondescription,
   status,
   createtime,
   creatorid,
   creatorname,
   Partssalescategoryid)
  select t.branchid,
         s_ServiceTripPriceGrade.Nextval,,
         '工程车(南区)工程车西部区域外出服务费等级',
         '工程车(南区)工程车西部区域外出服务费等级',
         t.regiondescription,
         t.status,
         sysdate,
         1,
         'Admin',
         (select id from gcdcs.partssalescategory where name = '搅拌车')
    from gcdcs.ServiceTripPriceGrade t
   where t.name = '工程车(北区)工程车西部区域外出服务费等级';
update gcdcs.servicetrippricerate b
   set b.servicetrippricegradeid =
       (select id
          from gcdcs.servicetrippricegrade
         where name = '工程车(南区)工程车西部区域外出服务费等级')
 where exists (select 1
          from gcdcs.ServiceTripPriceGrade a
         where a.id = b.servicetrippricegradeid
           and a.name = '工程车(北区)工程车西部区域外出服务费等级')
   and exists (select 1
          from gcdcs.serviceproductlineview c
         inner join gcdcs.partssalescategory d
            on d.id = c.Partssalescategoryid
         where b.serviceproductlineid = c.ProductLineId
           and d.name = '工程车(南区)');
update gcdcs.dealerserviceinfo a
   set a.outfeegradeid =
       (select id
          from gcdcs.ServiceTripPriceGrade b
         where name = '工程车(南区)工程车西部区域外出服务费等级')
 where exists (select 1
          from gcdcs.ServiceTripPriceGrade b
         where a.outfeegradeid = b.id
           and b.name = '工程车(北区)工程车西部区域外出服务费等级')
   and exists (select 1
          from gcdcs.partssalescategory c
         where c.id = a.partssalescategoryid
           and c.name = '工程车(南区)');      
          