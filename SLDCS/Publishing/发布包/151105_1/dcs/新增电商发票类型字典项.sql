--新增电商发票类型字典项

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values 
(S_KeyValueItem.Nextval, 'DCS', 'ERPInvoiceInformation_Type', '电商发票类型', 0, '增票', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values 
(S_KeyValueItem.Nextval, 'DCS', 'ERPInvoiceInformation_Type', '电商发票类型', 1, '普票', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values 
(S_KeyValueItem.Nextval, 'DCS', 'ERPInvoiceInformation_Type', '电商发票类型', 2, '电子发票', 1, 1);
