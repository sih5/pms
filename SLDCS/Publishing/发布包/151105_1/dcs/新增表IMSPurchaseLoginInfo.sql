--������IMSPurchaseLoginInfo
create table IMSPurchaseLoginInfo  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50),
   FileName             VARCHAR2(200),
   Status               NUMBER(9),
   SyncTime             DATE,
   ErrorMsg             VARCHAR2(2000),
   constraint PK_IMSPURCHASELOGININFO primary key (Id)
);

create sequence S_IMSPurchaseLoginInfo;
