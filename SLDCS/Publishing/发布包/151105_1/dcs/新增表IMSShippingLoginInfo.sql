--������IMSShippingLoginInfo

create table IMSShippingLoginInfo  (
   id                   NUMBER(9)                       not null,
   DealerCode           VARCHAR2(2000),
   OrderNo              VARCHAR2(160),
   OrderLineNo          VARCHAR2(160),
   DelNote              VARCHAR2(160),
   DelLineNo            VARCHAR2(160),
   ReqPart              VARCHAR2(160),
   vendor               VARCHAR2(160),
   ReqQty               VARCHAR2(160),
   DelPart              VARCHAR2(160),
   DelQty               VARCHAR2(160),
   DelDate              DATE,
   ReplCode             VARCHAR2(160),
   InfoCode             VARCHAR2(160),
   Reference            VARCHAR2(160),
   Backorder            VARCHAR2(160),
   Notes                VARCHAR2(160),
   SpecialPrice         VARCHAR2(160),
   FreightCharges       VARCHAR2(160),
   FILENAME             VARCHAR2(160),
   SYNCSTATUS           NUMBER(9),
   SYNCTIME             DATE,
   ERRORMESSAGE         VARCHAR2(160),
   constraint PK_IMSSHIPPINGLOGININFO primary key (id)
);

create sequence S_IMSShippingLoginInfo;
