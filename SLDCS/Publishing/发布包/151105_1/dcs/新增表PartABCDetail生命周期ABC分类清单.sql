--新增表PartABCDetail生命周期ABC分类清单

create table PartABCDetail  (
   Id                   NUMBER(9)                       not null,
   ProductLifeCycleCategoryId NUMBER(9)                       not null,
   PartABC              NUMBER(9)                       not null,
   constraint PK_PARTABCDETAIL primary key (Id)
);

create sequence S_PartABCDetail;
