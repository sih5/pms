--新增表PartSafeStock配件安全库存

create table PartSafeStock  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(100)                   not null,
   PartId               NUMBER(9)                       not null,
   PartCode             VARCHAR2(50)                    not null,
   PartName             VARCHAR2(100)                   not null,
   PrimarySupplierId    NUMBER(9),
   PrimarySupplierCode  VARCHAR2(50),
   PrimarySupplierName  VARCHAR2(100),
   OrderQuantityFor12M  NUMBER(9),
   CreatDuration        NUMBER(9),
   PartABC              NUMBER(9),
   PruductLifeCycle     NUMBER(9),
   PlannedPriceType     NUMBER(9),
   ServiceLevel         NUMBER(15,6),
   SafeCoefficient      NUMBER(15,6),
   NearFTWStandardDiff  NUMBER(15,6),
   NearFTWAverage       NUMBER(15,6),
   SafeStock            NUMBER(9),
   constraint PK_PARTSAFESTOCK primary key (Id)
);

create sequence S_PartSafeStock;
