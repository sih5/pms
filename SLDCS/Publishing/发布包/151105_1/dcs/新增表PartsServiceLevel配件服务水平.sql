--新增表PartsServiceLevel配件服务水平

create table PartsServiceLevel  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(50)                    not null,
   PartABC              NUMBER(9),
   ServiceLevel         NUMBER(15,6),
   PlannedPriceType     NUMBER(9),
   StartDuration        NUMBER(9),
   EndDuration          NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PARTSSERVICELEVEL primary key (Id)
);

create sequence S_PartsServiceLevel;
