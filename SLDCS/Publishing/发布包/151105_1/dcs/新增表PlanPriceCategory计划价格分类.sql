--新增表PlanPriceCategory计划价格分类

create table PlanPriceCategory  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   PriceCategory        NUMBER(9)                       not null,
   PriceMax             NUMBER(15,6)                    not null,
   PriceMin             NUMBER(15,6)                    not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PLANPRICECATEGORY primary key (Id)
);

create sequence S_PlanPriceCategory;
