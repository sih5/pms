--新增表ProductLifeCycleCategory 生命周期分类

create table ProductLifeCycleCategory  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(100)                   not null,
   CycleCategory        NUMBER(9),
   CreatorStartTime     NUMBER(9),
   CreatorEndTime       NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PRODUCTLIFECYCLECATEGORY primary key (Id)
);

create sequence S_ProductLifeCycleCategory;
