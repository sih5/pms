--新增表ServiceLevelBySafeCf配件服务水平对应安全系数

create table ServiceLevelBySafeCf  (
   Id                   NUMBER(9)                       not null,
   SafeCoefficient      NUMBER(15,6)                    not null,
   ServiceLevel         NUMBER(15,6)                    not null,
   constraint PK_SERVICELEVELBYSAFECF primary key (Id)
);

create sequence S_ServiceLevelBySafeCf;
