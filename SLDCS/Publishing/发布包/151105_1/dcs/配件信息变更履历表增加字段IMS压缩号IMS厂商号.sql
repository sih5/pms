--配件信息变更履历表增加字段： IMS压缩号、IMS厂商号

alter table SparePartHistory add IMSCompressionNumber VARCHAR2(50);

alter table SparePartHistory add IMSManufacturerNumber VARCHAR2(50);
