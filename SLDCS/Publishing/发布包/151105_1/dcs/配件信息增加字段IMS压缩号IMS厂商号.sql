--配件信息增加字段：IMS压缩号、IMS厂商号

alter table SparePart add IMSCompressionNumber VARCHAR2(50) ;

alter table SparePart add IMSManufacturerNumber VARCHAR2(50);
