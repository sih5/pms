
---配件信息中配件名称有变化时同步更新配件销售价和配件零售指导价

create or replace trigger SparePart_Trg1
   after  update on SparePart
   for each row
begin

  if  (:new.Name<>:old.Name) then    
     update  PartsSalesPrice b            set SparePartName=:new.name  where b.SparePartId=:new.id;
     update  PartsRetailGuidePrice b  set SparePartName=:new.name  where b.SparePartId=:new.id;
  end if;
  
end SparePart_Trg1;