--配件销售订单表增加8个字段

alter table PartsSalesOrder add InvoiceCustomerName  VARCHAR2(100);

alter table PartsSalesOrder add BankAccount          VARCHAR2(50);

alter table PartsSalesOrder add Taxpayeridentification VARCHAR2(50);

alter table PartsSalesOrder add InvoiceAdress        VARCHAR2(100);

alter table PartsSalesOrder add InvoicePhone         VARCHAR2(50);

alter table PartsSalesOrder add InvoiceType          NUMBER(9);

alter table PartsSalesOrder add ERPOrderCode         VARCHAR2(50);

alter table PartsSalesOrder add ERPSourceOrderCode   VARCHAR2(50);
