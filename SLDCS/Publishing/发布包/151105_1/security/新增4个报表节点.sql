--新增4个报表节点

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14203, 14200, 2, 'PartsOrderConfirmGapSta_fd', 'Released', '', '订单审核缺口统计报表-福戴', '', '', '统计各品牌配件销售订单审核缺口情况', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 27, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14204, 14200, 2, 'PartsOrderConfirmGapSta_yx', 'Released', '', '订单审核缺口统计报表-营销', '', '', '统计各品牌配件销售订单审核缺口情况', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 28, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14205, 14200, 2, 'PartsOrderConfirmGapSta_gc', 'Released', '', '订单审核缺口统计报表-工程车', '', '', '统计各品牌配件销售订单审核缺口情况', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 29, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14206, 14200, 2, 'PartsOrderConfirmGapSta_sd', 'Released', '', '订单审核缺口统计报表-时代', '', '', '统计各品牌配件销售订单审核缺口情况', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 30, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
