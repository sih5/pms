--新增节点

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(3505,3500, 2, 'IntelligentOrderMonthly', 'Released', NULL, '月度累计数据查询',NULL, 'Client/DCS/Images/Menu/PartsPurchasing/IntelligentOrderMonthly.png',4, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(3506,3500, 2, 'IntelligentOrderWeeklyBase', 'Released', NULL, '周度基础数据查询',NULL, 'Client/DCS/Images/Menu/PartsPurchasing/IntelligentOrderWeeklyBase.png',5, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(3507,3500, 2, 'IntelligentOrderWeekly', 'Released', NULL, '周度累计数据查询',NULL, 'Client/DCS/Images/Menu/PartsPurchasing/IntelligentOrderWeekly.png',6, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
