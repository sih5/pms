--新增节点

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(2208,2200, 2, 'IMSPurchaseLoginInfoQuery', 'Released', NULL, 'IMS接口日志查询-采购订单', '查询IMS接口日志采购订单信息', 'Client/DCS/Images/Menu/Common/IMSPurchaseLoginInfo.png', 7, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(2209,2200, 2, 'IMSShippingLoginInfoQuery', 'Released', NULL, 'IMS接口日志查询-发运单', '查询IMS接口日志发运单信息', 'Client/DCS/Images/Menu/Common/IMSPurchaseLoginInfo.png', 8, 2);


INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(4110,4100, 2, 'PartsSalesOrderTraceQuery','Released', NULL, '销售订单跟踪查询', '查询销售订单信息', 'Client/DCS/Images/Menu/PartsSales/PartsSalesOrder.png.png', 9, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
   
--新增按钮 

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1302, 'DealerServiceInfoForBranch|ImportMainInfo', '批量导入', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2208, 'IMSPurchaseLoginInfo|Retransfer', '重新下传', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2208, 'IMSPurchaseLoginInfo|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2209, 'IMSShippingLoginInfo|Setup', '手动设置', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2209, 'IMSShippingLoginInfo|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4110, 'Common|Export', '导出', 2);


-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
