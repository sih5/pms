--新增节点（智能订货）月度基础数据查询

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(3504,3500, 2, 'IntelligentOrderMonthlyBase', 'Released', NULL, '月度基础数据查询',NULL, 'Client/DCS/Images/Menu/PartsPurchasing/IntelligentOrderMonthlyBase.png',3, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
