--新增客户服务评价CustomerServiceEvaluate

create table CustomerServiceEvaluate  (
   Id                   NUMBER(9)                       not null,
   RepairClaimBillId    NUMBER(9)                       not null,
   RepairClaimBillCode  VARCHAR2(100)                   not null,
   EvaluateSource       VARCHAR2(500)                   not null,
   EvaluateGrade        NUMBER(9)                       not null,
   DisContentFactor     NUMBER(9),
   EvaluateTime         DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   constraint PK_CUSTOMERSERVICEEVALUATE primary key (Id)
);


create sequence S_CustomerServiceEvaluate;
