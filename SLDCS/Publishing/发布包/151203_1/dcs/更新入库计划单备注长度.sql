--更新入库计划单备注长度

alter table PartsInboundPlan modify Remark  VARCHAR2(400);

--更新配件入库检验单 备注长度

alter table PartsInboundCheckBill modify Remark  VARCHAR2(400);
