--备份entnodetemplatedetail

create table EntNodeTemplateDetail20151119 
as
select *
  from EntNodeTemplateDetail
 where EntNodeTemplateDetail.NodeId =
       (select id
          from node
         where node.categoryid =
               (select id
                  from action
                 where PAGEID = '3107'
                   and OPERATIONID = 'Common|Print') and CategoryType=1);
                   
 
 --delete  EntNodeTemplateDetail
            
delete from EntNodeTemplateDetail
 where EntNodeTemplateDetail.NodeId =
       (select id
          from node
         where node.categoryid =
               (select id
                  from action
                 where PAGEID = '3107'
                   and OPERATIONID = 'Common|Print') and CategoryType=1);
                     
-----------------------------------------------------------

--备份rule
create table rule20151119
as
select * from rule where rule.nodeid = (select id
          from node
         where node.categoryid =
               (select id
                  from action
                 where PAGEID = '3107'
                   and OPERATIONID = 'Common|Print') and CategoryType=1);
                   
                   
--delete rule

delete from rule where rule.nodeid = (select id
          from node
         where node.categoryid =
               (select id
                  from action
                 where PAGEID = '3107'
                   and OPERATIONID = 'Common|Print') and CategoryType=1);
    
--------------------------------------------------------------

--备份node
create table node20151119
as
 select * from node where node.categoryid = (select id from action
where PAGEID = '3107' and OPERATIONID = 'Common|Print');


--delete node

 delete from node where node.categoryid = (select id from action
where PAGEID = '3107' and OPERATIONID = 'Common|Print');


-------------------------------------------------

--备份数据action
create table action20151119
as
select * from action
where PAGEID = '3107' and OPERATIONID = 'Common|Print';

--删除action
delete from action
where PAGEID = '3107' and OPERATIONID = 'Common|Print';
