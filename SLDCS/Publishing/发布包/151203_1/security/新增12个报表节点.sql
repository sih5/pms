
--新增12个报表节点

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(13160,13300, 2, 'PartsStockSumStat-fd', 'Released', NULL, '配件库存汇总-福戴', '商混设备配件库库存金额汇总', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 5, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(13161,13300, 2, 'PartsStockSumStat-yx', 'Released', NULL, '配件库存汇总-营销', '商混设备配件库库存金额汇总', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 5, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(13162,13300, 2, 'PartsStockSumStat-sd', 'Released', NULL, '配件库存汇总-时代', '商混设备配件库库存金额汇总', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 5, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(13163,13300, 2, 'PartsStockSumStat-gcc', 'Released', NULL, '配件库存汇总-工程车', '商混设备配件库库存金额汇总', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 5, 2);



insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14404, 14400, 2, 'DealerPartsStockStat-fd', 'Released', '', '服务站库存查询-福戴', '', '', '实时查询服务站配件库存明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 36, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14405, 14400, 2, 'DealerPartsStockStat-yx', 'Released', '', '服务站库存查询-营销', '', '', '实时查询服务站配件库存明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 36, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14406, 14400, 2, 'DealerPartsStockStat-sd', 'Released', '', '服务站库存查询-时代', '', '', '实时查询服务站配件库存明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 36, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14407, 14400, 2, 'DealerPartsStockStat-gcc', 'Released', '', '服务站库存查询-工程车', '', '', '实时查询服务站配件库存明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 36, 2);


insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14610, 14600, 2, 'PartsOrderFillRate-fd', 'Released', '', '配件订单满足率（一）-福戴', '', '', '统计PMS系统订单满足率', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 43, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14611, 14600, 2, 'PartsOrderFillRate-yx', 'Released', '', '配件订单满足率（一）-营销', '', '', '统计PMS系统订单满足率', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 43, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14612, 14600, 2, 'PartsOrderFillRate-sd', 'Released', '', '配件订单满足率（一）-时代', '', '', '统计PMS系统订单满足率', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 43, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14613, 14600, 2, 'PartsOrderFillRate-gcc', 'Released', '', '配件订单满足率（一）-工程车', '', '', '统计PMS系统订单满足率', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 43, 2);



-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
