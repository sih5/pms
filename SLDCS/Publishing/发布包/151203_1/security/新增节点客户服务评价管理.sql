--新增节点

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(15000,0, 0, 'Evaluation', NULL, NULL, '客户服务评价管理', NULL, 'Client/Images/ShellView/evaluation-s.png', 10, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(15100,15000, 1, 'EvaluationDetails', NULL, NULL, '客户服务评价管理', NULL, NULL, 1, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(15101,15100, 2, 'CustomerServiceEvaluate', 'Released', NULL, '客户服务评价明细管理', '查询和批量导入客户服务评价明细', 'Client/DCS/Images/Menu/Service/CustomerServiceEvaluate.png', 1, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(15102,15100, 2, 'CustomerServiceEvaluateQuery', 'Released', NULL, '客户服务评价统计', '客户服务评价统计', 'Client/DCS/Images/Menu/Service/CustomerServiceEvaluateQuery.png', 2, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
   
   
--新增按钮
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1302, 'DealerServiceInfoForBranch|ImportForEdito', '批量导入修改', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8606, 'PartsClaimOrderNewForDealer|LabelPrint', '标签打印', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 15101, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 15101, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 15102, 'Common|Export', '导出', 2);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
