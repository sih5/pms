--新增节点调拨在途明细报表

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(13309,13300, 2, 'TransferOnWayReport', 'Released', NULL, '调拨在途明细报表', '查询任何时间段的调拨明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 12, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
