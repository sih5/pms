
--新增表装车明细LoadingDetail

create table LoadingDetail  (
   Id                   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   VehicleId            NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   SerialNumber         VARCHAR2(50)                    not null,
   FaultyPartsAssemblyName VARCHAR2(100)                   not null,
   ResponsibleUnitCode  VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_LOADINGDETAIL primary key (Id)
);


create sequence S_LoadingDetail;
