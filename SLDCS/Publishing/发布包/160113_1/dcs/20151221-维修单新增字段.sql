
--维修单新增字段（配件价格上限、配件价格下限、上限金额、下限金额、配件管理费率）

alter table RepairOrder add PartsPriceUpperLimit NUMBER(19,4);

alter table RepairOrder add PartsPriceLowerLimit NUMBER(19,4);

alter table RepairOrder add UpperLimitAmount     NUMBER(19,4);

alter table RepairOrder add LowerLimitAmount     NUMBER(19,4);

alter table RepairOrder add PartsManagementCostRate NUMBER(15,6);
