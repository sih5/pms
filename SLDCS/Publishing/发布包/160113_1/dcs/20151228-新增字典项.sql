
--新增字典项

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values 
(S_KeyValueItem.Nextval, 'DCS', 'IsSameToLoadingDetail', '装车明细是否一致', 1, '是', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values 
(S_KeyValueItem.Nextval, 'DCS', 'IsSameToLoadingDetail', '装车明细是否一致', 2, '否', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values 
(S_KeyValueItem.Nextval, 'DCS', 'IsSameToLoadingDetail', '装车明细是否一致', 3, '无对应值', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values 
(S_KeyValueItem.Nextval, 'DCS', 'ERPDelivery_SyncStatus', 'ERP同步状态', 1, '同步成功', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values 
(S_KeyValueItem.Nextval, 'DCS', 'ERPDelivery_SyncStatus', 'ERP同步状态', 2, '同步失败', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values 
(S_KeyValueItem.Nextval, 'DCS', 'ERPDelivery_SyncStatus', 'ERP同步状态', 3, '终止', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values 
(S_KeyValueItem.Nextval, 'DCS', 'ERPDelivery_SyncType', 'ERP同步类型', 1, 'ERP订单', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values 
(S_KeyValueItem.Nextval, 'DCS', 'ERPDelivery_Refund', 'ERP退款状态', 0, '未退款', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values 
(S_KeyValueItem.Nextval, 'DCS', 'ERPDelivery_Refund', 'ERP退款状态', 1, '退款', 1, 1);
