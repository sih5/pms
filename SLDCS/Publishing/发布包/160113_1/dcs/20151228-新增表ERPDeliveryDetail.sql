--������-ERPDeliveryDetail

create table ERPDeliveryDetail  (
   Id                   NUMBER(9)                       not null,
   DeliveryId           NUMBER(9)                       not null,
   Item_Code            VARCHAR2(500),
   Item_Name            VARCHAR2(500),
   Sku_Code             VARCHAR2(500),
   Sku_Name             VARCHAR2(500),
   Sku_Note             VARCHAR2(500),
   Price                VARCHAR2(500),
   Qty                  NUMBER(9),
   Amount               NUMBER(19,4),
   Amount_After         NUMBER(19,4),
   Post_Fee             NUMBER(19,4),
   Refund               NUMBER(9),
   SyncStatus           NUMBER(9),
   SyncTime             DATE,
   ErrorMessage         VARCHAR2(500),
   Discount_Fee         NUMBER(19,4),
   constraint PK_ERPDELIVERYDETAIL primary key (Id)
);

create sequence S_ERPDeliveryDetail;
