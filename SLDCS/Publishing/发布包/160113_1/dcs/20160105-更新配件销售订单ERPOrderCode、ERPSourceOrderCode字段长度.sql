--更新配件销售订单ERPOrderCode、ERPSourceOrderCode字段长度

alter table PartsSalesOrder modify ERPOrderCode         VARCHAR2(500);

alter table PartsSalesOrder modify ERPSourceOrderCode   VARCHAR2(500);
