--新增表ERP盘点单-ERPStock 

create table ERPStock  (
   Id                   NUMBER(9)                       not null,
   WAREHOUSECODE        VARCHAR2(500),
   SPAREPARTCODE        VARCHAR2(500),
   QUANTITY             NUMBER(9),
   SYNCSTATUS           NUMBER(9),
   SYNCTIME             DATE,
   ERRORMESSAGE         VARCHAR2(500),
   constraint PK_ERPSTOCK primary key (Id)
);


create sequence S_ERPStock;
