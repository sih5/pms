create or replace procedure AutoUpdateRegionIDyx as
begin
  update company
     set regionid =
         (select a.id
            from tiledregion a
           where a.provincename = company.provincename
             and a.cityname = company.cityname
             and a.countyname = company.countyname)
   where exists (select 1
            from tiledregion a
           where a.provincename = company.provincename
             and a.cityname = company.cityname
             and a.countyname = company.countyname)
     and company.provincename is not null
     and company.cityname is not null
     and company.countyname is not null and company.status<>99 and company.regionid is null
     and not exists(select 1 from yxsecurity.enterprise where enterprise.id=company.id and enterprise.status=0);
   update company
      set regionid =
          (select a.id
             from tiledregion a
            where a.provincename = company.provincename
              and a.cityname = company.cityname
              and a.countyname is null)
    where exists (select 1
             from tiledregion a
            where a.provincename = company.provincename
              and a.cityname = company.cityname
              and a.countyname is null)
      and company.provincename is not null
      and company.cityname is not null
      and company.countyname is null and company.status<>99 and company.regionid is null
       and not exists(select 1 from yxsecurity.enterprise where enterprise.id=company.id and enterprise.status=0);
    update company
       set regionid =
           (select a.id
              from tiledregion a
             where a.provincename = company.provincename
               and a.cityname  is null
               and a.countyname is null)
     where exists (select 1
              from tiledregion a
             where a.provincename = company.provincename
               and a.cityname  is null
               and a.countyname is null)
       and company.provincename is not null
       and company.cityname is null
       and company.countyname is null and company.status<>99 and company.regionid is null
        and not exists(select 1 from yxsecurity.enterprise where enterprise.id=company.id and enterprise.status=0);

  end;
  
  /
  begin
  sys.dbms_scheduler.create_job(job_name            => '自动更新省市县',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoUpdateRegionID',
                                start_date          => to_date('16-12-2015 05:15:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
