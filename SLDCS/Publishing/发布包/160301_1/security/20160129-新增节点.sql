--新增节点配件计划价变更履历查询、配件销售价变更履历查询

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(6104,6100, 2, 'PlannedPriceAppHistory', 'Released', NULL, '配件计划价变更履历查询', '查询本公司配件计划价变更履历查询', 'Client/DCS/Images/Menu/Financial/PartsPlannedPrice.png', 4, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(2320,2300, 2, 'PartsSalesPriceHistory', 'Released', NULL, '配件销售价变更履历查询', '查询配件销售价变更履历查询', 'Client/DCS/Images/Menu/PartsSales/PartsSalePriceIncreaseRate.png', 19, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
