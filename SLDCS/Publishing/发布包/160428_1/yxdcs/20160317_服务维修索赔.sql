
create sequence S_RepairWorkOrderMd
/

create sequence S_ReturnVisitQuest
/

create sequence S_VehicleBrandPMSRealation

create sequence S_ReturnVisitQuestDetail
/
create table RepairWorkOrderMd  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BranchId             NUMBER(9)                       not null,
   RowNo             VARCHAR2(50),
   ChewId               VARCHAR2(50),
   Srnum                VARCHAR2(50),
   Type                 NUMBER(9),
   SubType              NUMBER(9),
   DetailType           VARCHAR2(50),
   Description          VARCHAR2(600),
   UtcdateTime          DATE,
   SubCompany           VARCHAR2(100),
   FaultStmptom         VARCHAR2(200),
   FalutGround          VARCHAR2(200),
   UrgencyLevel         NUMBER(9),
   Expect               VARCHAR2(50),
   ExpectTime           DATE,
   SourceChannel        VARCHAR2(50),
   VinCode              VARCHAR2(50),
   VehicleId	        number(9), 
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryCode VARCHAR2(50),
   ServiceProductLineId NUMBER(9)                       not null,
   SubBrand             VARCHAR2(50),
   DriveMile            VARCHAR2(50),
   AccountName          VARCHAR2(200),
   ContactName          VARCHAR2(50),
   ContactCellPhone     VARCHAR2(50),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9)                       not null,
   CreatorName          VARCHAR2(100)                   not null,
   CreateTime           DATE                            not null,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   ActiviteId           VARCHAR2(50),
   ActiviteNum          VARCHAR2(50),
   SendTime             DATE,
   HandlePerson         VARCHAR2(30),
   HandleDepartment     VARCHAR2(100),
   DealerName           VARCHAR2(100),
   DealerCode           VARCHAR2(30),
   DealerId             NUMBER(9)                       not null,
   MarketDepartmentId   NUMBER(9)                       not null,
   Status               NUMBER(9),
   ReceiveTime          DATE,
   ReceiverId           NUMBER(9),
   ReceiverName         VARCHAR2(100),
   StartRepairTime      DATE,
   StartRepairId        NUMBER(9),
   StartRepairName      VARCHAR2(100),
   ArriveStationTime    DATE,
   ArriveStationerId    NUMBER(9),
   ArriveStationerName  VARCHAR2(100),
   RevokeTime           DATE,
   IsCanFinish          NUMBER(9),
   CannotHandleReason   NUMBER(9),
   RejectTime           DATE,
   RejectorId           NUMBER(9),
   RejectorName         VARCHAR2(100),
   LeavingTime          DATE,
   LeavingId            NUMBER(9),
   LeavingName          VARCHAR2(100),
   ArriveTime           DATE,
   ArriverId            NUMBER(9),
   ArriverName          VARCHAR2(100),
   FimishTime           DATE,
   FimishId             NUMBER(9),
   FimishName           VARCHAR2(100),
   FeedBackTime         DATE,
   RefuseReason         NUMBER(9),
   RepairCategory       NUMBER(9),
   ProcessMode          NUMBER(9),
   ProcessContent       VARCHAR2(500),
   RepairOrderId        NUMBER(9),
   RepairOrderCode      VARCHAR2(50),
   PartsCodeName        VARCHAR2(200),
   PartsSourceChannel   NUMBER(9),
   PartsSalesOrderId    NUMBER(9),
   PartsSalesOrderCode  VARCHAR2(50),
   SalesCategoryName    VARCHAR2(100),
   PartArriveTime       DATE,
   IsUsed               NUMBER(1)                       not null,
   RevokeId             NUMBER(9),
   RevokeName           VARCHAR2(100),
   constraint PK_REPAIRWORKORDERMD primary key (Id)
)
/

/*==============================================================*/
/* Table: ReturnVisitQuest                                      */
/*==============================================================*/
create table ReturnVisitQuest  (
   id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   QuestsessionId       VARCHAR2(50),
   IsConnection         NUMBER(1),
   FailReason           VARCHAR2(500),
   IsTrue               NUMBER(1),
   Description          VARCHAR2(500),
   Type                 NUMBER(9),
   OrderId              VARCHAR2(50),
   Score                VARCHAR2(50),
   Channel              NUMBER(9),
   SurverName           VARCHAR2(50),
   DealerId             NUMBER(9)                       not null,
   DealerCode           VARCHAR2(100),
   DealerName           VARCHAR2(100),
   MarketDepartmentId   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryCode VARCHAR2(50),
   RevisitDays          DATE,
   CreatorName          VARCHAR2(100),
   CreatorId            NUMBER(9),
   CreateTime           DATE,
   Status               NUMBER(9),
   Remark               VARCHAR2(200),
   constraint PK_RETURNVISITQUEST primary key (id)
)
/

/*==============================================================*/
/* Table: ReturnVisitQuestDetail                                */
/*==============================================================*/
create table ReturnVisitQuestDetail  (
   Id                   NUMBER(9)                       not null,
   QuestsessionDetailID              VARCHAR2(30),
   ReturnVisitQuestId   NUMBER(9)	not null,
   QUEST                VARCHAR2(500),
   Choice               VARCHAR2(500),
   ChoiceScore          VARCHAR2(50),
   TextResponse         VARCHAR2(500),
   constraint PK_RETURNVISITQUESTDETAIL primary key (Id)
)
/



create table VehicleBrandPMSRealation  (
   Id                   NUMBER(9)                       not null,
   MainBrandCode        VARCHAR2(100),
   MainBrandName        VARCHAR2(100),
   PartsSalesCategoryCode VARCHAR2(100),
   PartsSalesCategoryName VARCHAR2(100),
   PartsSalesCategoryId NUMBER(9),
   BranchCode           VARCHAR2(100),
   BranchId             NUMBER(9),
   constraint PK_VEHICLEBRANDPMSREALATION primary key (Id)
);