-- Add/modify columns 
alter table COMPANY add BusinessAddressLongitude NUMBER(14,6);
alter table COMPANY add BusinessAddressLatitude NUMBER(14,6);
alter table SalesCenterstrategy add IsEcommerceParts      NUMBER(1);

create table company_log  (
   id                   NUMBER(9)                       not null,
   CODE                 VARCHAR2(50),
   NAME                 VARCHAR2(200),
   REGIONID             NUMBER(9),
   PROVINCENAME         VARCHAR2(50),
   CITYNAME             VARCHAR2(50),
   COUNTYNAME           VARCHAR2(50),
   STATUS               NUMBER(9),
   CREATORID            NUMBER(9),
   CREATORNAME          VARCHAR2(100),
   CREATETIME           date,
   MODIFIERID           NUMBER(9),
   MODIFIERNAME         VARCHAR2(100),
   MODIFYTIME           date,
   SYSTEMDATE           date,
   logtype              NUMBER(9),
   constraint PK_COMPANY_LOG primary key (id)
)
/

create sequence S_company_log;
/