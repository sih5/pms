alter table DealerHistory 
add BusinessAddressLongitude number(14,6);

alter table DealerHistory 
add BusinessAddressLatitude number(14��6);

alter table DealerHistory 
add ABusinessAddressLongitude number(14��6);

alter table DealerHistory 
add ABusinessAddressLatitude number(14��6);
DealerServiceInfo


alter table DealerServiceInfo
add 24HourPhone VARCHAR2(11);


alter table DealerServiceInfo
add TrunkNetworkType NUMBER(9);


alter table DealerServiceInfo
add CenterStack NUMBER(1);




alter table DealerServiceInfoHistory
add TrunkNetworkType     NUMBER(9);

alter table DealerServiceInfoHistory
add 24HourPhone           VARCHAR2(11);
   
alter table DealerServiceInfoHistory
add CenterStack NUMBER(1);

alter table DealerServiceInfoHistory
add ATrunkNetworkType    NUMBER(9);


alter table DealerServiceInfoHistory
add A24HourPhone         VARCHAR2(11);

alter table DealerServiceInfoHistory
add ACenterStack         NUMBER(1);

alter table DEALERKEYEMPLOYEE drop column AUTHENTICATIONTYPE;
alter table DEALERKEYEMPLOYEE drop column AUTHENTICATIONTIME;
alter table DEALERKEYEMPLOYEE add MarketingDepartmentId NUMBER(9);
alter table DEALERKEYEMPLOYEE add IdCardNumber varchar2(50);
alter table DEALERKEYEMPLOYEE add Birthday date;
alter table DEALERKEYEMPLOYEE add School VARCHAR2(50);
alter table DEALERKEYEMPLOYEE add Education NUMBER(9);
alter table DEALERKEYEMPLOYEE add Professional VARCHAR2(100);
alter table DEALERKEYEMPLOYEE add ProfessionalRank NUMBER(9);
alter table DEALERKEYEMPLOYEE add EntryTime date;
alter table DEALERKEYEMPLOYEE add IsOnJob NUMBER(9);
alter table DEALERKEYEMPLOYEE add LeaveTime date;
alter table DEALERKEYEMPLOYEE add WorkType NUMBER(9);
alter table DEALERKEYEMPLOYEE add SkillLevel number(9);
alter table DEALERKEYEMPLOYEE add BTPositionId number(9);
alter table DEALERKEYEMPLOYEE add TransferTime date;