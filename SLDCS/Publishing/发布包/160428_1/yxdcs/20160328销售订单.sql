create sequence S_Retailer_Delivery
/

create sequence S_Retailer_DeliveryDetail
/

create sequence S_Retailer_ServiceApply
/

create sequence S_Retailer_ServiceApplyDetail
/
alter table PartsSalesReturnBill add serviceApplyId number(9);
/
create table Retailer_Delivery  (
   Id                   NUMBER(9)                       not null,
   OrderNumber          VARCHAR2(50)                    not null,
   ReceiveName          VARCHAR2(50),
   Mobile               VARCHAR2(15),
   Province             VARCHAR2(20),
   City                 VARCHAR2(20),
   District             VARCHAR2(20),
   Address              VARCHAR2(500),
   Warehouse_Code       VARCHAR2(500),
   Warehouse_Name       VARCHAR2(500),
   CustomerCode         VARCHAR2(20),
   CustomerName         VARCHAR2(50),
   InvoiceStatus        NUMBER(9),
   InvoiceTitle         VARCHAR2(1024),
   BankAccount          VARCHAR2(50),
   Taxpayeridentification VARCHAR2(20),
   InvoiceAdress        VARCHAR2(100),
   InvoiceTel           VARCHAR2(20),
   BuyerMessage         VARCHAR2(1024),
   Remark               VARCHAR2(1024),
   OrderCreateTime      DATE,
   lastModifyTime       DATE,
   PaymentType          NUMBER(9),
   PaymentTypeName      VARCHAR2(20),
   ActualAmount         NUMBER(14,2),
   ActualFreight        NUMBER(14,2),
   PaymentStatus        NUMBER(1),
   PaymentTime          DATE,
   InterfaceType        NUMBER(9),
   Shop_Code            VARCHAR2(500),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_RETAILER_DELIVERY primary key (Id)
)
/

/*==============================================================*/
/* Table: Retailer_DeliveryDetail                               */
/*==============================================================*/
create table Retailer_DeliveryDetail  (
   Id                   NUMBER(9)                       not null,
   OrderDeliveryId      NUMBER(9)                       not null,
   OrderDetailId        NUMBER(9)                       not null,
   GoodsId              VARCHAR2(50),
   Quantity             NUMBER(9),
   CouponPrice          NUMBER(14,2),
   Weight               NUMBER(14,2),
   TotalAmount          NUMBER(14,2),
   SyncStatus           NUMBER(9),
   SyncTime             DATE,
   Message              VARCHAR2(500),
   constraint PK_RETAILER_DELIVERYDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: Retailer_ServiceApply                                 */
/*==============================================================*/
create table Retailer_ServiceApply  (
   Id                   NUMBER(9)                       not null,
   OrderNumber          VARCHAR2(20),
   ServiceApplyId       NUMBER(9)                       not null,
   Reason               VARCHAR2(20),
   RefundAmount         NUMBER(14,2),
   RefundFreight        NUMBER(14,2),
   InterfaceType        VARCHAR2(20),
   SyncStatus           NUMBER(9),
   SyncTime             DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_RETAILER_SERVICEAPPLY primary key (Id)
)
/

/*==============================================================*/
/* Table: Retailer_ServiceApplyDetail                           */
/*==============================================================*/
create table Retailer_ServiceApplyDetail  (
   Id                   NUMBER(9)                       not null,
   GoodsCode            VARCHAR2(50),
   GoodsName            VARCHAR2(40),
   ReturnQuantity       NUMBER(9),
   ReturnPrice          NUMBER(9),
   ServiceApplyId       NUMBER(9)                       not null,
   SyncTime             DATE,
   SyncStatus           NUMBER(9),
   Message              VARCHAR2(100),
   constraint PK_RETAILER_SERVICEAPPLYDETAIL primary key (Id)
)
/