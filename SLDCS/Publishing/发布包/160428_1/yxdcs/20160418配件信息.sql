alter table SPAREPART add Factury varchar2(100);
alter table SPAREPART add IsOriginal number(9);
alter table SPAREPART add CategoryCode varchar2(100);
alter table Spareparthistory add Factury varchar2(100);
alter table Spareparthistory add IsOriginal number(9);
alter table Spareparthistory add CategoryCode varchar2(100);
create or replace view partsbranchview as
select a.code as PartCode,b.partssalescategoryname as PartsSalesCategoryName  from dcs.sparepart a inner join dcs.partsbranch  b on a.id=b.partid
where a.status=1 and b.status=1
union
select a.code as PartCode,b.partssalescategoryname as PartsSalesCategoryName  from yxdcs.sparepart a inner join yxdcs.partsbranch  b on a.id=b.partid
where a.status=1 and b.status=1
union
select a.code as PartCode,b.partssalescategoryname as PartsSalesCategoryName  from sddcs.sparepart a inner join sddcs.partsbranch  b on a.id=b.partid
where a.status=1 and b.status=1
union
select a.code as PartCode,b.partssalescategoryname as PartsSalesCategoryName  from gcdcs.sparepart a inner join gcdcs.partsbranch  b on a.id=b.partid
where a.status=1 and b.status=1;