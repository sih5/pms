CREATE OR REPLACE PACKAGE body SYNCWMSINOUT
IS
PROCEDURE SYNCIn
AS
type T_indetailsset
IS
  TABLE OF t_indetails%rowtype INDEX BY binary_integer;
  Vtable_indetails T_indetailsset;
  Vdetailno               NUMBER(9);
  Vi                      NUMBER(9);
  FWarehouseAreaId        NUMBER(9);
  FWarehouseAreaCode      VARCHAR2(40);
  FIPWarehouseCode        VARCHAR2(25);
  FAreaCategoryId         NUMBER(9);
  FIPPartsSalesCategoryId NUMBER(9);
  FIPPartId               NUMBER(9);
  FIPDInspectedQuantity   NUMBER(9);
  FIPDPlannedAmount       NUMBER(9);
  FIPWarehouseId          NUMBER(9);
  FIPId                   NUMBER(9);
  FErrormsg               VARCHAR2(1000);
  FPartsStockId           NUMBER(9);
  FIPStorageCompanyId     NUMBER(9);
  FIPStorageCompanyType   VARCHAR2(40);
  FIPStatus               NUMBER(3);
  FIPDPrice               NUMBER(19, 4);
  FIPBranchId             NUMBER(9);
  FPPPrice                NUMBER(19, 4);
  FIsAllFinished          NUMBER(9);
  FParentId NVARCHAR2(25);
  QualityType VARCHAR2(50);
  FWMSCreatorName  VARCHAR2(50);
  FMyException                   EXCEPTION;
  FComplateId                    INTEGER;
  FNewCode                       VARCHAR2(50);
  FNewCodePICB                   VARCHAR2(50);
  FIPStorageCompanyCode          VARCHAR2(50);
  FPartsInboundCheckBillId       NUMBER(9);
  FSYNCWMSINOUTLOGINFOId         NUMBER(9);
  FPartsInboundCheckBillDetailId NUMBER(9);
  FIPInboundType                 NUMBER(9);
  FNewPartsStockId               NUMBER(9);
  FPartsLogisticBatchId          NUMBER(9);
  FExistsErrorTin                NUMBER(9);
  CURSOR getT_ins
  IS
    SELECT * FROM t_in WHERE isfinished = 0 AND syscode = 'NPMS' and sourcecode='PIP110120160128000116' ORDER BY OBJID;
  CURSOR getT_indetails(FParentIds VARCHAR2)
  IS
    SELECT t_indetails.*,
      row_number() over(order by ParentId ASC) rno
    FROM t_indetails
    INNER JOIN t_in
    ON t_indetails.ParentID  = t_in.OBJID
    WHERE t_in.isfinished    = 0
    AND t_in.syscode         = 'NPMS'
    AND t_indetails.ParentID = FParentIds;
  CURSOR getFinshDetails(FIPId VARCHAR2)
  IS
    SELECT * FROM PartsInboundPlanDetail WHERE PartsInboundPlanId = FIPId;
BEGIN
  Vtable_indetails.delete;
  FNewCode := 'RK{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
  BEGIN
    SELECT Id
    INTO FComplateId
    FROM CodeTemplate
    WHERE name    = 'PartsInboundCheckBill'
    AND IsActived = 1;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    FErrormsg := '未找到名称为"PartsInboundCheckBill"的有效编码规则。';
    raise FMyException;
  WHEN OTHERS THEN
    raise;
  END;
  --循环处理主单数据
  FOR C_getT_ins IN getT_ins
  LOOP
    FPartsInboundCheckBillId := S_PartsInboundCheckBill.NEXTVAL;
    FSYNCWMSINOUTLOGINFOId   := S_SYNCWMSINOUTLOGINFO.nextval;
    BEGIN
      FParentId := C_getT_ins.OBJID;
      QualityType:= C_getT_ins.USER_DEF7;
      IF C_getT_ins.USER_DEF2 IS NULL THEN
        BEGIN
           FErrormsg := 'WMS操作人员为空';
              raise FMyException;
          END;
      END IF;

      FWMSCreatorName:='WMS_'||C_getT_ins.USER_DEF2;
      FOR C_getT_indetails IN getT_indetails(FParentId)
      LOOP
        Vdetailno                                        := C_getT_indetails.rno;
        Vtable_indetails(Vdetailno).ParentID             := C_getT_indetails.ParentID;
        Vtable_indetails(Vdetailno).Materialcode         := C_getT_indetails.Materialcode;
        Vtable_indetails(Vdetailno).Materialname         := C_getT_indetails.Materialname;
        Vtable_indetails(Vdetailno).Qty                  := C_getT_indetails.Qty;
        Vtable_indetails(Vdetailno).Save_Date_Time_Stamp := C_getT_indetails.Save_Date_Time_Stamp;
      END LOOP;
      FErrormsg := '';

      --校验并获取入库仓库ID
      BEGIN
        SELECT PartsInboundPlan.Id              AS FIPId,
          PartsInboundPlan.Status               AS FIPStatus,
          PartsInboundPlan.WarehouseId          AS FIPWarehouseId,
          PartsInboundPlan.StorageCompanyId     AS FIPStorageCompanyId,
          PartsInboundPlan.StorageCompanyCode   AS FIPStorageCompanyCode,
          PartsInboundPlan.StorageCompanyType   AS FIPStorageCompanyType,
          PartsInboundPlan.Partssalescategoryid AS FIPPartsSalesCategoryId,
          PartsInboundPlan.Branchid             AS FIPBranchId,
          PartsInboundPlan.InboundType          AS FIPInboundType
        INTO FIPId,
          FIPStatus,
          FIPWarehouseId,
          FIPStorageCompanyId,
          FIPStorageCompanyCode,
          FIPStorageCompanyType,
          FIPPartsSalesCategoryId,
          FIPBranchId,
          FIPInboundType
        FROM PartsInboundPlan
        WHERE Code = C_getT_ins.Sourcecode;
      EXCEPTION
      WHEN no_data_found THEN
        FIPId                 := NULL;
        FIPWarehouseId        := NULL;
        FIPStorageCompanyId   := NULL;
        FIPStorageCompanyType := NULL;
      END;
      --生成编号
      FNewCodePICB := regexp_replace(regexp_replace(FNewCode, '{CORPCODE}', FIPStorageCompanyCode), '{SERIAL}', lpad(TO_CHAR(GetSerial(FComplateId, TRUNC(sysdate, 'DD'), FIPStorageCompanyCode)), 6, '0'));

      --校验并获取入库仓库编号
      BEGIN
        SELECT Warehouse.code
        INTO FIPWarehouseCode
        FROM Warehouse
        WHERE Warehouse.Id = FIPWarehouseId;
      EXCEPTION
      WHEN no_data_found THEN
        FIPWarehouseCode := NULL;
      END;
      --如果入库计划单存在则进行处理否则记录错误日志
      IF FIPId IS NOT NULL THEN
        BEGIN
          --校验入库计划单状态(1:新建 4:部分检验)
          IF FIPStatus NOT IN (1, 4) THEN
            BEGIN
              FErrormsg := '入库异常[入库计划单状态错误]';
              raise FMyException;
            END;
          END IF;
          --校验入库仓库
          IF FIPWarehouseCode = C_getT_ins.WarehouseCode THEN
            BEGIN
              IF (Vtable_indetails.Count > 0) THEN
                BEGIN
                  --如果存在入库清单，则生成PMS入库单入库检验单。
                  INSERT
                  INTO PartsInboundCheckBill
                    (
                      Id,
                      Code,
                      PartsInboundPlanId,
                      WarehouseId,
                      WarehouseCode,
                      WarehouseName,
                      StorageCompanyId,
                      StorageCompanyCode,
                      StorageCompanyName,
                      StorageCompanyType,
                      PartsSalesCategoryId,
                      Customeraccountid,
                      BranchId,
                      BranchCode,
                      BranchName,
                      CounterpartCompanyId,
                      CounterpartCompanyCode,
                      CounterpartCompanyName,
                      InboundType,
                      OriginalRequirementBillId,
                      OriginalRequirementBillCode,
                      OriginalRequirementBillType,
                      Status,
                      SettlementStatus,
                      Remark,
                      CreatorId,
                      CreatorName,
                      CreateTime,
                      Objid,
                      GPMSPURORDERCODE
                    )
                  SELECT FPartsInboundCheckBillId,
                    FNewCodePICB,
                    PartsInboundPlan.Id,
                    PartsInboundPlan.WarehouseId,
                    PartsInboundPlan.WarehouseCode,
                    PartsInboundPlan.WarehouseName,
                    PartsInboundPlan.StorageCompanyId,
                    PartsInboundPlan.StorageCompanyCode,
                    PartsInboundPlan.StorageCompanyName,
                    PartsInboundPlan.StorageCompanyType,
                    PartsInboundPlan.PartsSalesCategoryId,
                    Customeraccountid,
                    PartsInboundPlan.BranchId,
                    PartsInboundPlan.BranchCode,
                    PartsInboundPlan.BranchName,
                    PartsInboundPlan.CounterpartCompanyId,
                    PartsInboundPlan.CounterpartCompanyCode,
                    PartsInboundPlan.CounterpartCompanyName,
                    PartsInboundPlan.InboundType,
                    PartsInboundPlan.OriginalRequirementBillId,
                    PartsInboundPlan.OriginalRequirementBillCode,
                    PartsInboundPlan.OriginalRequirementBillType,
                    1, --新建
                    2, --待结算
                    PartsInboundPlan.Remark,
                    1,
                    FWMSCreatorName,
                    SYSDATE,
                    FParentId,
                    PartsInboundPlan.Gpmspurordercode
                  FROM PartsInboundPlan
                  WHERE PartsInboundPlan.Id = FIPId;
                  FOR Vi IN 1 .. Vtable_indetails.count
                  LOOP
                    FPartsInboundCheckBillDetailId := S_PartsInboundCheckBillDetail.NEXTVAL;
                    --校验配件的有效
                    BEGIN
                      SELECT PartsInboundPlanDetail.SparePartId
                      INTO FIPPartId
                      FROM PartsInboundPlanDetail
                      WHERE PartsInboundPlanDetail.PartsInboundPlanId = FIPId
                      AND PartsInboundPlanDetail.SparePartId          =
                        (SELECT Id FROM SparePart WHERE Code = Vtable_indetails(Vi) .Materialcode
                        );
                    EXCEPTION
                    WHEN NO_Data_Found THEN
                      FIPPartId := NULL;
                    END;
                    --如果配件有效则开始处理清单数据。
                    IF FIPPartId IS NOT NULL THEN
                      BEGIN
                        --获取入库计划单的确认数量
                        SELECT NVL(PartsInboundPlanDetail.InspectedQuantity, 0) AS FIPDInspectedQuantity,
                          NVL(PartsInboundPlanDetail.PlannedAmount, 0)          AS FIPDPlannedAmount,
                          NVL(PRICE, 0)                                         AS PRICE
                        INTO FIPDInspectedQuantity,
                          FIPDPlannedAmount,
                          FIPDPrice
                        FROM PartsInboundPlanDetail
                        WHERE PartsInboundPlanDetail.PartsInboundPlanId = FIPId
                        AND PartsInboundPlanDetail.SparePartId          = FIPPartId;
                        IF FIPDPrice                                    < 0 THEN
                          BEGIN
                            FErrormsg := '配件价格小于0，请进行核对！配件图号：' || Vtable_indetails(Vi) .Materialcode;
                            raise FMyException;
                          END;
                        END IF;
                        BEGIN
                          --校验计划价
                          SELECT t.plannedprice
                          INTO FPPPrice
                          FROM partsplannedprice t
                          WHERE t.partssalescategoryid = FIPPartsSalesCategoryId
                          AND t.SparePartId            = FIPPartId;
                          IF FPPPrice                  < 0 THEN
                            BEGIN
                              FErrormsg := '配件计划价格小于0，请进行核对！配件图号：' || Vtable_indetails(Vi) .Materialcode;
                              raise FMyException;
                            END;
                          END IF;
                        EXCEPTION
                        WHEN no_data_found THEN
                          FErrormsg := '计划价不存在请维护' || Vtable_indetails(Vi) .Materialcode;
                          raise FMyException;
                        END;
                        --清单数量校验
                        IF (FIPDPlannedAmount >= FIPDInspectedQuantity + NVL(Vtable_indetails(Vi).qty, 0)) THEN
                          BEGIN
                            UPDATE PartsInboundPlanDetail
                            SET InspectedQuantity                           = NVL(InspectedQuantity, 0) + NVL(Vtable_indetails(Vi).qty, 0)
                            WHERE PartsInboundPlanDetail.PartsInboundPlanId = FIPId
                            AND PartsInboundPlanDetail.SparePartId          = FIPPartId;
                          END;
                        END IF;
                        IF (FIPDPlannedAmount < FIPDInspectedQuantity + NVL(Vtable_indetails(Vi).qty, 0)) THEN
                          BEGIN
                            FErrormsg := '确认量大于计划量，请进行核对！';
                            raise FMyException;
                          END;
                        END IF;

                        --如果质检类型为正常质检，获取库位ID使用检验区库位ID
                        IF QualityType='2' THEN
                        BEGIN
                         BEGIN
                          SELECT WarehouseArea.Id,
                            WarehouseArea.Code,
                            WarehouseArea.AreaCategoryId
                          INTO FWarehouseAreaId,
                            FWarehouseAreaCode,
                            FAreaCategoryId
                          FROM WarehouseArea
                          INNER JOIN WarehouseAreaCategory
                          ON WarehouseArea.AreaCategoryId    = WarehouseAreaCategory.Id
                          WHERE WarehouseArea.WarehouseId    = FIPWarehouseId
                          AND WarehouseArea.Areakind         = 3
                          AND WarehouseAreaCategory.Category = 3;
                        EXCEPTION
                        WHEN no_data_found THEN
                          FWarehouseAreaId := NULL;
                        WHEN too_many_rows THEN
                        BEGIN
                          FErrormsg := 'wms仓库库位不唯一';
                          raise FMyException;
                        END;
                        END;
                        END;
                        ELSE
                        BEGIN
                        BEGIN
                          SELECT WarehouseArea.Id,
                            WarehouseArea.Code,
                            WarehouseArea.AreaCategoryId
                          INTO FWarehouseAreaId,
                            FWarehouseAreaCode,
                            FAreaCategoryId
                          FROM WarehouseArea
                          INNER JOIN WarehouseAreaCategory
                          ON WarehouseArea.AreaCategoryId    = WarehouseAreaCategory.Id
                          WHERE WarehouseArea.WarehouseId    = FIPWarehouseId
                          AND WarehouseArea.Areakind         = 3
                          AND WarehouseAreaCategory.Category = 1;
                        EXCEPTION
                        WHEN no_data_found THEN
                          FWarehouseAreaId := NULL;
                        WHEN too_many_rows THEN
                        BEGIN
                          FErrormsg := 'wms仓库库位不唯一';
                          raise FMyException;
                        END;
                        END;
                        END;
                        END IF;
                        /*
                        1、更新配件入库计划、入库计划清单
                        检验量=配件入库检验单清单.检验量（根据 配件Id）
                        */
                        IF FWarehouseAreaId IS NOT NULL THEN
                          BEGIN

                            BEGIN
                              SELECT PartsStock.Id
                              INTO FPartsStockId
                              FROM PartsStock
                              WHERE PartsStock.WarehouseId   = FIPWarehouseId
                              AND PartsStock.PartId          = FIPPartId
                              AND PartsStock.WarehouseAreaId = FWarehouseAreaId for update;
                            EXCEPTION
                            WHEN no_data_found THEN
                              FPartsStockId := NULL;
                            END;
                            IF FPartsStockId IS NULL THEN
                              BEGIN
                                FNewPartsStockId := s_PartsStock.Nextval;
                                INSERT
                                INTO PartsStock
                                  (
                                    Id,
                                    WarehouseId,
                                    StorageCompanyId,
                                    StorageCompanyType,
                                    BranchId,
                                    WarehouseAreaId,
                                    PartId,
                                    WarehouseAreaCategoryId,
                                    Quantity,
                                    CreatorId,
                                    CreateTime,
                                    CreatorName
                                  )
                                  VALUES
                                  (
                                    FNewPartsStockId,
                                    FIPWarehouseId,
                                    FIPStorageCompanyId,
                                    FIPStorageCompanyType,
                                    FIPBranchId,
                                    FWarehouseAreaId,
                                    FIPPartId,
                                    FAreaCategoryId,
                                    Vtable_indetails(Vi).qty,
                                    1,
                                    sysdate,
                                    'WMS'
                                  );
                              END;
                            ELSE
                              BEGIN
                                UPDATE PartsStock
                                SET Quantity        = NVL(Quantity, 0) + NVL(Vtable_indetails(Vi).qty, 0)
                                WHERE PartsStock.Id = FPartsStockId;
                              END;
                            END IF;
                            INSERT
                            INTO PartsInboundCheckBillDetail
                              (
                                Id,
                                PartsInboundCheckBillId,
                                SparePartId,
                                SparePartCode,
                                SparePartName,
                                WarehouseAreaId,
                                WarehouseAreaCode,
                                InspectedQuantity,
                                SettlementPrice,
                                CostPrice,
                              Remark)
                              SELECT FPartsInboundCheckBillDetailId,
                                FPartsInboundCheckBillId,
                                SparePartId,
                                SparePartCode,
                                SparePartName,
                                FWarehouseAreaId,
                                FWarehouseAreaCode,
                                Vtable_indetails(Vi).qty,
                                PartsInboundPlanDetail.Price,
                                FPPPrice,
                                PartsInboundPlanDetail.Remark
                              FROM PartsInboundPlanDetail
                              WHERE PartsInboundPlanDetail.PartsInboundPlanId = FIPId
                              AND PartsInboundPlanDetail.SparePartId          = FIPPartId;
                            END;
                          ELSE
                            BEGIN
                              FErrormsg := 'PMS仓库对应唯一库位未维护，请核对！';
                              raise FMyException;
                            END;
                          END IF;
                        END;
                      ELSE
                        BEGIN
                          FErrormsg := '清单内配件与入库计划单内记录不符，请核对！';
                          raise FMyException;
                        END;
                      END IF;
                    END LOOP;
                    --出库类型为调拨和采购则新增物流批次
                    IF FIPInboundType IN (1, 3) THEN
                      BEGIN
                        BEGIN
                          SELECT plb.Id
                          INTO FPartsLogisticBatchId
                          FROM PartsLogisticBatch plb
                          WHERE EXISTS
                            (SELECT 1
                            FROM PartsLogisticBatchBillDetail plbbd
                            WHERE plbbd.BillId             = FIPId
                            AND plbbd.BillType             = 4
                            AND plbbd.PartsLogisticBatchId = plb.Id
                            )
                                  
   and ((plb.sourcetype = 1 and plb.sourceid = (select id
                           from partsoutboundplan
                          where originalrequirementbillid =
                                (select originalrequirementbillid
                                   from partsinboundplan
                                  where id = FIPId
                                    and inboundtype = 3)
                            and outboundtype = 4
                            and originalrequirementbilltype=6
                            )) or
       (plb.sourcetype = 2 and 
       plb.sourceid = (select originalrequirementbillid
                           from partsinboundplan
                          where id = FIPId
                            and inboundtype = 1
                            and originalrequirementbilltype=4
                            )));
                            
                        EXCEPTION
                        WHEN no_data_found THEN
                          FPartsLogisticBatchId := NULL;
                        END;
                        IF FPartsLogisticBatchId IS NOT NULL THEN
                          BEGIN
                            UPDATE PartsLogisticBatchItemDetail plbid
                            SET plbid.InboundAmount =
                              (SELECT picbd.inspectedquantity
                              FROM PartsInboundCheckBillDetail picbd
                              WHERE picbd.SparePartId           = plbid.sparepartid
                              AND picbd.partsinboundcheckbillid = FPartsInboundCheckBillId
                              )
                            WHERE plbid.PartsLogisticBatchId = FPartsLogisticBatchId
                            AND EXISTS
                              (SELECT 1
                              FROM PartsInboundCheckBillDetail picbd1
                              WHERE picbd1.SparePartId           = plbid.sparepartid
                              AND picbd1.partsinboundcheckbillid = FPartsInboundCheckBillId
                              );
                            UPDATE PartsLogisticBatch
                            SET Status = 2
                            WHERE ID   = FPartsLogisticBatchId
                            AND NOT EXISTS
                              (SELECT 1
                              FROM PartsLogisticBatchItemDetail plbid
                              WHERE plbid.InboundAmount    != plbid.OutboundAmount
                              AND plbid.PartsLogisticBatchId=FPartsLogisticBatchId
                              );
                          END;
                        END IF;
                      END;
                    END IF;
                  END;
                END IF;
                ---------------无清单强制完成--------------------
                IF Vtable_indetails.count = 0 THEN
                  BEGIN
                    IF C_getT_ins.USER_DEF4 <> 'Y' THEN
                      BEGIN
                        FErrormsg := '入库清单数量为空，请核对！';
                        raise FMyException;
                      END;
                    END IF;
                  END;
                END IF;
                ---------------无清单强制完成--------------------
              END;
            ELSE
              BEGIN
                FErrormsg := '仓库编号与入库计划单记录不符，请核对！';
                raise FMyException;
              END;
            END IF;
          END;
        ELSE
          BEGIN
            FErrormsg := '入库计划单编号不存在，请核对！';
            raise FMyException;
          END;
        END IF;
        --校验入库计划单是否已经全部完成
        SELECT SUM(PlannedAmount) - SUM(NVL(InspectedQuantity, 0)) AS isfinished
        INTO FIsAllFinished
        FROM PartsInboundPlanDetail
        WHERE PartsInboundPlanId = FIPId
        GROUP BY PartsInboundPlanId;
        IF C_getT_ins.USER_DEF4 = 'Y' THEN
          BEGIN
            BEGIN
              SELECT 1
              INTO FExistsErrorTin
              FROM t_in
              WHERE SOURCECODE=C_getT_ins.Sourcecode
              AND isfinished  =2;
            EXCEPTION
            WHEN no_data_found THEN
              FExistsErrorTin    :=NULL;
            WHEN too_many_rows THEN
              FExistsErrorTin    :=NULL;
              IF FExistsErrorTin IS NOT NULL THEN
                FErrormsg        := '存在未处理并且有问题的单据，必须先处理问题单据后才可强制完成';
                raise FMyException;
              END IF;
            END;
            BEGIN
              UPDATE PartsInboundPlan
              SET PartsInboundPlan.Status     = 3,
                PartsInboundPlan.ModifyTime   = sysdate,
                PartsInboundPlan.ModifierId   = 1,
                PartsInboundPlan.ModifierName = 'WMS',
                Remark                        = Remark ||
                ' [该单据已经由业务人员WMS强制完成]'
              WHERE PartsInboundPlan.Id = FIPId;
            END;
          END;
        ELSE
          BEGIN
            BEGIN
              IF FIsAllFinished = 0 THEN
                BEGIN
                  UPDATE PartsInboundPlan
                  SET PartsInboundPlan.Status     = 2,
                    PartsInboundPlan.ModifyTime   = sysdate,
                    PartsInboundPlan.ModifierId   = 1,
                    PartsInboundPlan.ModifierName = 'WMS'
                  WHERE PartsInboundPlan.Id       = FIPId;
                END;
              ELSE
                BEGIN
                  UPDATE PartsInboundPlan
                  SET PartsInboundPlan.Status     = 4,
                    PartsInboundPlan.ModifyTime   = sysdate,
                    PartsInboundPlan.ModifierId   = 1,
                    PartsInboundPlan.ModifierName = 'WMS'
                  WHERE PartsInboundPlan.Id       = FIPId;
                END;
              END IF;
            END;
          END;
        END IF;
        UPDATE t_in
        SET t_in.Isfinished = 1,
          t_in.TINID        = FPartsInboundCheckBillId
        WHERE t_in.OBJID    = C_getT_ins.OBJID;
      EXCEPTION
      WHEN FMyException THEN
        BEGIN
          ROLLBACK;
          INSERT
          INTO Syncwmsinoutloginfo
            (
              OBJID,
              syncdate,
              code,
              synctype,
              synccode,
              INOUTCODE
            )
            VALUES
            (
              FSYNCWMSINOUTLOGINFOId,
              sysdate,
              FErrormsg,
              1,
              C_getT_ins.OBJID,
              C_getT_ins.SourceCode
            );
          UPDATE t_in SET t_in.Isfinished = 2 WHERE t_in.OBJID = C_getT_ins.OBJID;
        END;
      WHEN OTHERS THEN
        BEGIN
          ROLLBACK;
          FErrormsg := sqlerrm;
          INSERT
          INTO Syncwmsinoutloginfo
            (
              OBJID,
              syncdate,
              code,
              synctype,
              synccode,
              INOUTCODE
            )
            VALUES
            (
              FSYNCWMSINOUTLOGINFOId,
              sysdate,
              FErrormsg,
              1,
              C_getT_ins.OBJID,
              C_getT_ins.SourceCode
            );
          UPDATE t_in SET t_in.Isfinished = 2 WHERE t_in.OBJID = C_getT_ins.OBJID;
        END;
      END;
      Vtable_indetails.delete;
      COMMIT;
    END LOOP;
  END;
PROCEDURE Synctraffic
AS
  Vdetailno             NUMBER(9, 0);
  Vi                    NUMBER(9, 0);
  FWarehouseAreaId      NUMBER(9);
  FWarehouseAreaCode    VARCHAR2(40);
  FNewcodepso           VARCHAR2(40);
  FNewcodepsoF          VARCHAR2(40);
  FNewcodeckF           VARCHAR2(40);
  FNewcodeck            VARCHAR2(40);
  FOutplanid            VARCHAR2(40);
  FStorageCompanyId     NUMBER(9, 0);
  FPartsSalesCategoryId NUMBER(9, 0);
  FOutwarehouseid       VARCHAR2(40);
  FFinishstatus         NUMBER(9, 0);
  FIsAllFinished          NUMBER(9);
  FErrormsg             VARCHAR2(1000);
  FShiptypecodevalue    NUMBER(9);
  FFinishqty            NUMBER(9, 0);
  FPlanqty              NUMBER(9, 0);
  FSparePartId          NUMBER(9, 0);
  FBranchid             NUMBER(9, 0);
  FCostPrice            NUMBER(19, 4);
  FAmount               NUMBER(19, 4);
  FLeftAmount           NUMBER(19, 4);
  FEstimateddueamount   NUMBER(19, 4);
  FCustomerAccountId    NUMBER(9, 0);
  FOutboundType         NUMBER(9, 0);
  FComplateCKId         NUMBER(9, 0);
  FComplatePSOId        NUMBER(9, 0);
  FStorageCompanyCode   VARCHAR2(40);
  FParentId NVARCHAR2(25);
  FPartsOutboundBillId        NUMBER(9);
  FPartsShippingOrderId       NUMBER(9);
  FPartsShippingOrderRefId    NUMBER(9);
  FSYNCWMSINOUTLOGINFOId      NUMBER(9);
  FPartsoutboundbilldetailId  NUMBER(9);
  FPartsShippingOrderDetailId NUMBER(9);
  FPartsLogisticBatchId       NUMBER(9);
  FPartsLBBillDetailIdCK      NUMBER(9);
  FNewPartslogisticbatchId    NUMBER(9);
  FPartsLBBillDetailIdFY      NUMBER(9);
  FPartsLockedStockResult     NUMBER(9);
  FPartsStockResult           NUMBER(9);
  FPartsStockId               NUMBER(9);
  FExistsErrorTship           NUMBER(9);
  FWMSCreatorName  VARCHAR2(50);
  FMyexception                EXCEPTION;
type t_Shippingorderdetailset
IS
  TABLE OF t_Shippingorderdetails%rowtype INDEX BY binary_integer;
  Vtable_Shippingorderdetails t_Shippingorderdetailset;
  CURSOR Gett_Shippingorderhead
  IS
    SELECT t_Shippingorderhead.*
    FROM t_Shippingorderhead
    WHERE User_Def1 = 'NPMS'
    AND isfinished  = 0
    ORDER BY Save_Date_Time_Stamp,Interface_Record_Id;
  CURSOR Gett_Shippingorderdetails(Parentid VARCHAR2)
  IS
    SELECT t_Shippingorderdetails.*,
      Row_Number() Over(order by Parentid ASC) Rno
    FROM t_Shippingorderdetails
    WHERE t_Shippingorderdetails.Interface_Link_Id = Parentid;
BEGIN
  Vtable_Shippingorderdetails.delete;
  FNewCodeCK  := 'CK{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
  FNewCodePSO := 'FY{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
  BEGIN
    SELECT Id
    INTO FComplateCKId
    FROM CodeTemplate
    WHERE name    = 'PartsOutboundBill'
    AND IsActived = 1;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    FErrormsg := '未找到名称为"PartsOutboundBill"的有效编码规则。';
    raise FMyException;
  WHEN OTHERS THEN
    raise;
  END;
  BEGIN
    SELECT Id
    INTO FComplatePSOId
    FROM CodeTemplate
    WHERE name    = 'PartsShippingOrder'
    AND IsActived = 1;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    FErrormsg := '未找到名称为"PartsShippingOrder"的有效编码规则。';
    raise FMyException;
  WHEN OTHERS THEN
    raise;
  END;
  FOR c_Gett_Shippingorderhead IN Gett_Shippingorderhead
  LOOP
    BEGIN
      FPartsOutboundBillId     := s_Partsoutboundbill.Nextval;
      FPartsShippingOrderId    := s_Partsshippingorder.Nextval;
      FPartsShippingOrderRefId := s_Partsshippingorderref.Nextval;
      FSYNCWMSINOUTLOGINFOId   := S_SYNCWMSINOUTLOGINFO.nextval;
      BEGIN
        SELECT pop.Id,
          pop.Branchid,
          pop.StorageCompanyId,
          Pop.PartsSalesCategoryId,
          NVL(Pop.CustomerAccountId, 0),
          Pop.OutboundType,
          Pop.StorageCompanyCode,
          pop.Warehouseid
        INTO FOutplanid,
          FBranchid,
          FStorageCompanyId,
          FPartsSalesCategoryId,
          FCustomerAccountId,
          FOutboundType,
          FStorageCompanyCode,
          FOutwarehouseid
        FROM Partsoutboundplan Pop
        WHERE pop.Code = c_Gett_Shippingorderhead.User_Def3;
      EXCEPTION
      WHEN No_Data_Found THEN
        FErrormsg := '未找到出库计划';
        raise FMyException;
      WHEN OTHERS THEN
        raise;
      END;
      FNewcodeckF   := regexp_replace(regexp_replace(FNewCodeCk, '{CORPCODE}', FStorageCompanyCode), '{SERIAL}', lpad(TO_CHAR(GetSerial(FComplateCKId, TRUNC(sysdate, 'DD'), FStorageCompanyCode)), 6, '0'));
      FNewcodepsoF  := regexp_replace(regexp_replace(FNewCodePSO, '{CORPCODE}', FStorageCompanyCode), '{SERIAL}', lpad(TO_CHAR(GetSerial(FComplatePSOId, TRUNC(sysdate, 'DD'), FStorageCompanyCode)), 4, '0'));
      FFinishstatus := 1; --0:未出库,1:出库完成，>1:部分出库
      FErrormsg     := '';
      FParentId     := c_Gett_Shippingorderhead.Interface_Record_Id;
      IF c_Gett_Shippingorderhead.User_Stamp IS NULL THEN
        BEGIN
           FErrormsg := 'WMS操作人员为空';
              raise FMyException;
          END;
      END IF;
      FWMSCreatorName:='WMS_'||c_Gett_Shippingorderhead.User_Stamp;
      FOR C_getT_Shippingorderdetails IN Gett_Shippingorderdetails(FParentId)
      LOOP
        Vdetailno                                                := C_getT_Shippingorderdetails.rno;
        Vtable_Shippingorderdetails(Vdetailno).Interface_Link_Id := C_getT_Shippingorderdetails.Interface_Link_Id;
        Vtable_Shippingorderdetails(Vdetailno).ITEM              := C_getT_Shippingorderdetails.ITEM;
        Vtable_Shippingorderdetails(Vdetailno).TOTAL_QTY         := C_getT_Shippingorderdetails.TOTAL_QTY;
      END LOOP;
      IF trim(c_Gett_Shippingorderhead.User_Def5)    = 1 THEN
        FShiptypecodevalue                          := 1;
      elsif trim(c_Gett_Shippingorderhead.User_Def5) = 2 THEN
        FShiptypecodevalue                          := 2;
      elsif trim(c_Gett_Shippingorderhead.User_Def5) = 3 THEN
        FShiptypecodevalue                          := 3;
      elsif trim(c_Gett_Shippingorderhead.User_Def5) = 4 THEN
        FShiptypecodevalue                          := 4;
      elsif trim(c_Gett_Shippingorderhead.User_Def5) = 5 THEN
        FShiptypecodevalue                          := 5;
      elsif trim(c_Gett_Shippingorderhead.User_Def5) = 6 THEN
        FShiptypecodevalue                          := 6;
      elsif trim(c_Gett_Shippingorderhead.User_Def5) = 7 THEN
        FShiptypecodevalue                          := 7;
      elsif trim(c_Gett_Shippingorderhead.User_Def5) = 8 THEN
        FShiptypecodevalue                          := 8;
      elsif trim(c_Gett_Shippingorderhead.User_Def5) = 9 THEN
        FShiptypecodevalue                          := 9;
      END IF;
      IF (Vtable_Shippingorderdetails.Count > 0) THEN
        BEGIN
          --配件出库单
          INSERT
          INTO Partsoutboundbill
            (
              Id,
              Code,
              Partsoutboundplanid,
              Warehouseid,
              Warehousecode,
              Warehousename,
              Storagecompanyid,
              Storagecompanycode,
              Storagecompanyname,
              Storagecompanytype,
              Branchid,
              Branchcode,
              Branchname,
              Partssalescategoryid,
              Partssalesordertypeid,
              Partssalesordertypename,
              Counterpartcompanyid,
              Counterpartcompanycode,
              Counterpartcompanyname,
              Receivingcompanyid,
              Receivingcompanycode,
              Receivingcompanyname,
              Receivingwarehouseid,
              Receivingwarehousecode,
              Receivingwarehousename,
              Outboundtype,
              Shippingmethod,
              Customeraccountid,
              Originalrequirementbillid,
              Originalrequirementbilltype,
              Originalrequirementbillcode,
              Settlementstatus,
              Creatorid,
              Creatorname,
              Createtime,
              InterfaceRecordId,
              Gpmspurordercode,
              orderapprovecomment
            )
          SELECT FPartsOutboundBillId,
            FNewcodeckF,
            Pop.Id,
            Pop.Warehouseid,
            Pop.Warehousecode,
            Pop.Warehousename,
            Pop.Storagecompanyid,
            Pop.Storagecompanycode,
            Pop.Storagecompanyname,
            Pop.Storagecompanytype,
            Pop.Branchid,
            Pop.Branchcode,
            Pop.Branchname,
            Pop.Partssalescategoryid,
            pop.Partssalesordertypeid,
            pop.Partssalesordertypename,
            Pop.Counterpartcompanyid,
            Pop.Counterpartcompanycode,
            Pop.Counterpartcompanyname,
            Pop.ReceivingCompanyId,
            Pop.ReceivingCompanyCode,
            Pop.ReceivingCompanyName,
            Pop.Receivingwarehouseid,
            Pop.Receivingwarehousecode,
            Pop.Receivingwarehousename,
            Pop.Outboundtype,
            Pop.Shippingmethod,
            Pop.Customeraccountid,
            Pop.Originalrequirementbillid,
            Pop.Originalrequirementbilltype,
            Pop.Originalrequirementbillcode,
            2,
            1,
            FWMSCreatorName,
            SYSDATE,
            FParentId,
            pop.gpmspurordercode,
            pop.orderapprovecomment
          FROM Partsoutboundplan Pop
          WHERE Pop.Id     = FOutplanid;
          IF FOutboundType = 1 OR FOutboundType = 4 THEN
            BEGIN
              --配件发运单
              INSERT
              INTO Partsshippingorder
                (
                  Id,
                  Code,
                  type,
                  Branchid,
                  PartsSalesCategoryId,
                  Shippingcompanyid,
                  Shippingcompanycode,
                  Shippingcompanyname,
                  Settlementcompanyid,
                  Settlementcompanycode,
                  Settlementcompanyname,
                  Receivingcompanyid,
                  Receivingcompanycode,
                  Invoicereceivesalecateid,
                  Invoicereceivesalecatename,
                  Receivingcompanyname,
                  Warehouseid,
                  Warehousecode,
                  Warehousename,
                  Receivingwarehouseid,
                  Receivingwarehousecode,
                  Receivingwarehousename,
                  Receivingaddress,
                  LogisticCompanyId,
                  LogisticCompanyCode,
                  LogisticCompanyName,
                  Originalrequirementbillid,
                  Originalrequirementbillcode,
                  Originalrequirementbilltype,
                  Shippingmethod,
                  Requestedarrivaldate,
                  Shippingdate,
                  Status,
                  Creatorid,
                  Creatorname,
                  Createtime,
                  InterfaceRecordId,
                  Gpmspurordercode
                )
              SELECT FPartsShippingOrderId,
                FNewcodepsoF,
                FOutboundType,
                Pop.Branchid,
                Pop.PartsSalesCategoryId,
                Pop.Storagecompanyid,
                Pop.Storagecompanycode,
                Pop.Storagecompanyname,
                Pop.Storagecompanyid,
                Pop.Storagecompanycode,
                Pop.Storagecompanyname,
                Pop.Receivingcompanyid,
                Pop.Receivingcompanycode,
                Pop.Partssalescategoryid,
                (SELECT name
                FROM Partssalescategory
                WHERE Partssalescategory.Id = Pop.Partssalescategoryid
                ),
                Pop.Receivingcompanyname,
                Pop.Warehouseid,
                Pop.Warehousecode,
                Pop.Warehousename,
                Pop.Receivingwarehouseid,
                Pop.Receivingwarehousecode,
                Pop.Receivingwarehousename,
                c_Gett_Shippingorderhead.Ship_To_Address1,
                (SELECT Id
                FROM LogisticCompany
                WHERE Code = c_Gett_Shippingorderhead.Carrier
                ),
                c_Gett_Shippingorderhead.Carrier,
                (SELECT Name
                FROM LogisticCompany
                WHERE Code = c_Gett_Shippingorderhead.Carrier
                ),
                Pop.Originalrequirementbillid,
                Pop.Originalrequirementbillcode,
                Pop.Originalrequirementbilltype,
                Pop.Shippingmethod,
                c_Gett_Shippingorderhead.Actual_Departure_Date_Time,
                c_Gett_Shippingorderhead.Scheduled_Ship_Date,
                1,
                1,
                FWMSCreatorName,
                SYSDATE,
                FParentId,
                pop.gpmspurordercode
              FROM Partsoutboundplan Pop
              WHERE Pop.Id = FOutplanid;
              --配件发运单关联单
              INSERT
              INTO Partsshippingorderref
                (
                  Id,
                  Partsshippingorderid,
                  Partsoutboundbillid
                )
                VALUES
                (
                  FPartsshippingorderrefId,
                  FPartsShippingOrderId,
                  FPartsoutboundbillId
                );
            END;
          END IF;
          FOR Vi IN 1 .. Vtable_Shippingorderdetails.count
          LOOP
            FPartsoutboundbilldetailId  := s_Partsoutboundbilldetail.Nextval;
            FPartsShippingOrderDetailId := s_PartsShippingOrderDetail.Nextval;
            SELECT NVL(pd.OutboundFulfillment, 0) Finishqty,
              NVL(Pd.Plannedamount, 0) Planqty,
              pd.sparepartid AS FSparePartId
            INTO FFinishqty,
              FPlanqty,
              FSparePartId
            FROM Partsoutboundplandetail Pd
            INNER JOIN Sparepart
            ON Sparepart.Id              = Pd.Sparepartid
            WHERE Pd.Partsoutboundplanid = FOutplanid
            AND Sparepart.Code           = Vtable_Shippingorderdetails(Vi).Item;
            --查询成本价
            BEGIN
              SELECT ppp.PlannedPrice
              INTO FCostPrice
              FROM PartsPlannedPrice ppp
              WHERE ppp.OwnerCompanyId     = FStorageCompanyId
              AND ppp.PartsSalesCategoryId = FPartsSalesCategoryId
              AND ppp.sparepartid          = FSparePartId;
            EXCEPTION
            WHEN No_Data_Found THEN
              BEGIN
                FErrormsg := '找不到对应的配件计划价，请核对！';
                raise FMyException;
              END;
            END;
            BEGIN
              SELECT WarehouseArea.Id,
                WarehouseArea.Code
              INTO FWarehouseAreaId,
                FWarehouseAreaCode
              FROM WarehouseArea
              INNER JOIN WarehouseAreaCategory
              ON WarehouseArea.AreaCategoryId    = WarehouseAreaCategory.Id
              WHERE WarehouseArea.WarehouseId    = FOutwarehouseid
              AND WarehouseArea.Areakind         = 3
              AND WarehouseAreaCategory.Category = 1;
            EXCEPTION
            WHEN no_data_found THEN
              BEGIN
                FErrormsg := 'PMS仓库对应唯一库位未维护，请核对！';
                raise FMyException;
              END;
            END;
            BEGIN
              SELECT Sparepart.Id
              INTO FSparepartId
              FROM Sparepart
              WHERE Sparepart.Status = 1
              AND Sparepart.Code     = Vtable_Shippingorderdetails(Vi).ITEM;
            EXCEPTION
            WHEN no_data_found THEN
              BEGIN
                FErrormsg := '找不到对应的配件，请核对！' || Vtable_Shippingorderdetails(Vi).ITEM;
                raise FMyException;
              END;
            END;
            --配件出库清单
            INSERT
            INTO Partsoutboundbilldetail
              (
                Id,
                PartsOutboundBillId,
                SparePartId,
                SparePartCode,
                SparePartName,
                OutboundAmount,
                WarehouseAreaId,
                WarehouseAreaCode,
                BatchNumber,
                SettlementPrice,
                CostPrice
              )
            SELECT FPartsOutboundBillDetailID,
              FPartsOutboundBillId,
              pd.SparePartId,
              pd.SparePartCode,
              pd.SparePartName,
              Vtable_Shippingorderdetails(Vi).TOTAL_QTY,
              FWarehouseAreaId,
              FWarehouseAreaCode,
              NULL,
              pd.Price,
              DECODE(pop.StorageCompanyType, 1, FCostPrice, 0)
            FROM PartsOutboundPlanDetail pd
            INNER JOIN PartsOutboundPlan Pop
            ON pop.id                    = pd.PartsOutboundPlanId
            WHERE pd.PartsOutboundPlanId = FOutplanid
            AND pd.SparePartId           = FSparepartId;
            IF FOutboundType             = 1 OR FOutboundType = 4 THEN
              BEGIN
                --配件发运清单
                INSERT
                INTO PartsShippingOrderDetail
                  (
                    Id,
                    PartsShippingOrderId,
                    SparePartId,
                    SparePartCode,
                    SparePartName,
                    ShippingAmount,
                    ConfirmedAmount,
                    DifferenceClassification,
                    TransportLossesDisposeMethod,
                    InTransitDamageLossAmount,
                    SettlementPrice
                  )
                SELECT FPartsShippingOrderDetailId,
                  FPartsshippingorderId,
                  pd.SparePartId,
                  pd.SparePartCode,
                  pd.SparePartName,
                  Vtable_Shippingorderdetails(Vi).TOTAL_QTY,
                  Vtable_Shippingorderdetails(Vi).TOTAL_QTY,
                  NULL,
                  NULL,
                  NULL,
                  pd.Price
                FROM PartsOutboundPlanDetail pd
                WHERE pd.PartsOutboundPlanId = FOutplanid
                AND pd.SparePartId           = FSparepartId;
              END;
            END IF;
            --减少配件锁定库存
            BEGIN
              SELECT NVL(PartsLockedStock.LockedQuantity, 0) - NVL(Vtable_Shippingorderdetails(Vi).Total_Qty, 0)
              INTO FPartsLockedStockResult
              FROM PartsLockedStock
              WHERE PartsLockedStock.WarehouseId = FOutwarehouseid
              AND PartsLockedStock.Partid        = FSparepartId;
            EXCEPTION
            WHEN no_data_found THEN
              BEGIN
                FErrormsg := '锁定库存不存在' || Vtable_Shippingorderdetails(Vi).ITEM;
                raise FMyException;
              END;
            WHEN too_many_rows THEN
              BEGIN
                FErrormsg := '存在多条锁定库存,仓库Id' ||FOutwarehouseid || '配件Id' || FSparepartId;
                raise FMyException;
              END;
            END;
            BEGIN
              IF FPartsLockedStockResult < 0 THEN
                BEGIN
                  FErrormsg := '锁定库存不足' || Vtable_Shippingorderdetails(Vi).ITEM;
                  raise FMyException;
                END;
              END IF;
            END;
            UPDATE PartsLockedStock
            SET PartsLockedStock.LockedQuantity = NVL(PartsLockedStock.LockedQuantity, 0) - NVL(Vtable_Shippingorderdetails(Vi) .Total_Qty, 0)
            WHERE PartsLockedStock.WarehouseId  = FOutwarehouseid
            AND PartsLockedStock.Partid         = FSparepartId;
            --减少配件库存
            BEGIN
              SELECT NVL(PartsStock.Quantity, 0) - NVL(Vtable_Shippingorderdetails(Vi).Total_Qty, 0),
                PartsStock.Id
              INTO FPartsStockResult,
                FPartsStockId
              FROM PartsStock
              INNER JOIN WarehouseAreaCategory
              ON PartsStock.WarehouseAreaCategoryId=WarehouseAreaCategory.ID
              WHERE WarehouseAreaCategory.Category =1
              AND PartsStock.WarehouseId           = FOutwarehouseid
              AND PartsStock.Partid                = FSparepartId for update;
            EXCEPTION
            WHEN no_data_found THEN
              BEGIN
                FErrormsg := '库存不存在' || Vtable_Shippingorderdetails(Vi).ITEM;
                raise FMyException;
              END;
            WHEN too_many_rows THEN
              BEGIN
                FErrormsg := '存在多条库存,仓库Id' ||FOutwarehouseid || '配件Id' || FSparepartId;
                raise FMyException;
              END;
            END;
            BEGIN
              IF FPartsStockResult < 0 THEN
                BEGIN
                  FErrormsg := '库存不足' || Vtable_Shippingorderdetails(Vi).ITEM;
                  raise FMyException;
                END;
              END IF;
            END;
            UPDATE PartsStock
            SET PartsStock.Quantity = NVL(PartsStock.Quantity, 0) - NVL(Vtable_Shippingorderdetails(Vi) .Total_Qty, 0)
            WHERE PartsStock.Id     = FPartsStockId;
            UPDATE Partsoutboundplandetail
            SET Partsoutboundplandetail.Outboundfulfillment   = NVL(FFinishqty, 0) + NVL(Vtable_Shippingorderdetails(Vi) .Total_Qty, 0)
            WHERE Partsoutboundplandetail.Partsoutboundplanid = FOutplanid
            AND Partsoutboundplandetail.Sparepartid           = FSparepartId;
            IF FPlanqty                                       > NVL(FFinishqty, 0) + NVL(Vtable_Shippingorderdetails(Vi).TOTAL_QTY, 0) THEN
              BEGIN
                FFinishstatus := FFinishstatus + 1;
              END;
            END IF;
            IF FPlanqty < NVL(FFinishqty, 0) + NVL(Vtable_Shippingorderdetails(Vi).TOTAL_QTY, 0) THEN
              BEGIN
                FErrormsg := '完成量大于计划量，请进行核对！';
                raise FMyexception;
              END;
            END IF;
          END LOOP;
          IF FOutboundType IN (1, 2, 4, 5) THEN
            BEGIN
              BEGIN
                SELECT plb.Id
                INTO FPartsLogisticBatchId
                FROM PartsLogisticBatch plb
                WHERE plb.SourceId = FOutplanid
                AND plb.SourceType = 1;
              EXCEPTION
              WHEN no_data_found THEN
                FPartsLogisticBatchId := NULL;
              END;
              IF FPartsLogisticBatchId IS NULL THEN
                BEGIN
                  FNewPartslogisticbatchId := s_Partslogisticbatch.Nextval;
                  INSERT
                  INTO PartsLogisticBatch
                    (
                      Id,
                      Status,
                      ShippingStatus,
                      CompanyId,
                      SourceId,
                      SourceType
                    )
                  SELECT FNewPartslogisticbatchId AS Id,
                    1                             AS Status,
                    1                             AS ShippingStatus,
                    pop.storagecompanyid          AS CompanyId,
                    FOutplanid                    AS SourceId,
                    1                             AS SourceType
                  FROM Partsoutboundplan Pop
                  WHERE Pop.Id            = FOutplanid;
                  FPartsLBBillDetailIdCK := s_PartsLogisticBatchBillDetail.Nextval;
                  --新增出库单物流批次单据清单
                  INSERT
                  INTO PartsLogisticBatchBillDetail
                    (
                      Id,
                      PartsLogisticBatchId,
                      BillId,
                      BillType
                    )
                    VALUES
                    (
                      FPartsLBBillDetailIdCK,
                      FNewPartslogisticbatchId,
                      FPartsOutboundBillId,
                      1
                    );
                  FPartsLBBillDetailIdFY := s_PartsLogisticBatchBillDetail.Nextval;
                  --新增发运单物流批次单据清单
                  INSERT
                  INTO PartsLogisticBatchBillDetail
                    (
                      Id,
                      PartsLogisticBatchId,
                      BillId,
                      BillType
                    )
                    VALUES
                    (
                      FPartsLBBillDetailIdFY,
                      FNewPartslogisticbatchId,
                      FPartsShippingOrderId,
                      2
                    );
                  --新增配件物流批次配件清单
                  INSERT
                  INTO PartsLogisticBatchItemDetail
                    (
                      Id,
                      PartsLogisticBatchId,
                      CounterpartCompanyId,
                      ReceivingCompanyId,
                      ShippingCompanyId,
                      SparePartId,
                      SparePartCode,
                      SparePartName,
                      BatchNumber,
                      OutboundAmount,
                      InboundAmount
                    )
                  SELECT s_PartsLogisticBatchItemDetail.Nextval AS Id,
                    FNewPartslogisticbatchId                    AS PartsLogisticBatchId,
                    (SELECT pob.CounterpartCompanyId
                    FROM PartsOutboundBill pob
                    WHERE pob.id = pobdt.partsoutboundbillid
                    ) AS CounterpartCompanyId,
                    (SELECT pob.CounterpartCompanyId
                    FROM PartsOutboundBill pob
                    WHERE pob.id = pobdt.partsoutboundbillid
                    ) AS ReceivingCompanyId,
                    (SELECT pob.storagecompanyid
                    FROM PartsOutboundBill pob
                    WHERE pob.id = pobdt.partsoutboundbillid
                    ) AS ShippingCompanyId,
                    pobdt.SparePartId,
                    (SELECT Code FROM sparepart WHERE id = pobdt.SparePartId
                    ) AS SparePartCode,
                    (SELECT Name FROM sparepart WHERE id = pobdt.SparePartId
                    ) AS SparePartName,
                    pobdt.BatchNumber,
                    pobdt.OutboundAmount AS OutboundAmount,
                    0                    AS InboundAmount
                  FROM
                    (SELECT pobd.partsoutboundbillid,
                      pobd.SparePartId         AS SparePartId,
                      pobd.BatchNumber         AS BatchNumber,
                      SUM(pobd.OutboundAmount) AS OutboundAmount
                    FROM PartsOutboundBillDetail pobd
                    WHERE pobd.partsoutboundbillid = FPartsOutboundBillId
                    GROUP BY pobd.partsoutboundbillid,
                      pobd.sparepartid,
                      pobd.batchnumber
                    ) pobdt;
                END;
              ELSE
                BEGIN
                  UPDATE PartsLogisticBatch
                  SET ShippingStatus      = 2
                  WHERE Id                = FPartsLogisticBatchId
                  AND ShippingStatus      = 1;
                  FPartsLBBillDetailIdCK := s_PartsLogisticBatchBillDetail.Nextval;
                  --新增出库单物流批次单据清单
                  INSERT
                  INTO PartsLogisticBatchBillDetail
                    (
                      Id,
                      PartsLogisticBatchId,
                      BillId,
                      BillType
                    )
                    VALUES
                    (
                      FPartsLBBillDetailIdCK,
                      FPartsLogisticBatchId,
                      FPartsOutboundBillId,
                      1
                    );
                  FPartsLBBillDetailIdFY := s_PartsLogisticBatchBillDetail.Nextval;
                  --新增发运单物流批次单据清单
                  INSERT
                  INTO PartsLogisticBatchBillDetail
                    (
                      Id,
                      PartsLogisticBatchId,
                      BillId,
                      BillType
                    )
                    VALUES
                    (
                      FPartsLBBillDetailIdFY,
                      FPartsLogisticBatchId,
                      FPartsShippingOrderId,
                      2
                    );
                  BEGIN
                    --更新配件物流批次配件清单
                    UPDATE PartsLogisticBatchItemDetail plbid
                    SET OutboundAmount =
                      (SELECT SUM(pobd.OutboundAmount)
                      FROM PartsOutboundBillDetail pobd
                      WHERE pobd.partsoutboundbillid = FPartsOutboundBillId
                      AND pobd.SparePartId           = plbid.SparePartId
                      AND pobd.BatchNumber           = plbid.BatchNumber
                      GROUP BY pobd.partsoutboundbillid,
                        pobd.sparepartid,
                        pobd.batchnumber
                      )
                    WHERE EXISTS
                      (SELECT 1
                      FROM PartsOutboundBillDetail pobd
                      WHERE pobd.partsoutboundbillid = FPartsOutboundBillId
                      AND pobd.SparePartId           = plbid.SparePartId
                      AND pobd.BatchNumber           = plbid.BatchNumber
                      );
                    --新增配件物流批次配件清单
                    INSERT
                    INTO PartsLogisticBatchItemDetail
                      (
                        Id,
                        PartsLogisticBatchId,
                        CounterpartCompanyId,
                        ReceivingCompanyId,
                        ShippingCompanyId,
                        SparePartId,
                        SparePartCode,
                        SparePartName,
                        BatchNumber,
                        OutboundAmount,
                        InboundAmount
                      )
                    SELECT s_PartsLogisticBatchItemDetail.Nextval AS Id,
                      FPartsLogisticBatchId                       AS PartsLogisticBatchId,
                      (SELECT pob.CounterpartCompanyId
                      FROM PartsOutboundBill pob
                      WHERE pob.id = pobdt.partsoutboundbillid
                      ) AS CounterpartCompanyId,
                      (SELECT pob.CounterpartCompanyId
                      FROM PartsOutboundBill pob
                      WHERE pob.id = pobdt.partsoutboundbillid
                      ) AS ReceivingCompanyId,
                      (SELECT pob.storagecompanyid
                      FROM PartsOutboundBill pob
                      WHERE pob.id = pobdt.partsoutboundbillid
                      ) AS ShippingCompanyId,
                      pobdt.SparePartId,
                      (SELECT Code FROM sparepart WHERE id = pobdt.SparePartId
                      ) AS SparePartCode,
                      (SELECT Name FROM sparepart WHERE id = pobdt.SparePartId
                      ) AS SparePartName,
                      pobdt.BatchNumber,
                      pobdt.OutboundAmount AS OutboundAmount,
                      0                    AS InboundAmount
                    FROM
                      (SELECT pobd.partsoutboundbillid,
                        pobd.SparePartId         AS SparePartId,
                        pobd.BatchNumber         AS BatchNumber,
                        SUM(pobd.OutboundAmount) AS OutboundAmount
                      FROM PartsOutboundBillDetail pobd
                      WHERE pobd.partsoutboundbillid = FPartsOutboundBillId
                      AND NOT EXISTS
                        (SELECT 1
                        FROM PartsLogisticBatchItemDetail plbid
                        WHERE plbid.SparePartId        = pobd.SparePartId
                        AND plbid.BatchNumber          = pobd.BatchNumber
                        AND plbid.PartsLogisticBatchId = FPartsLogisticBatchId
                        )
                      GROUP BY pobd.partsoutboundbillid,
                        pobd.sparepartid,
                        pobd.batchnumber
                      ) pobdt;
                  END;
                END;
              END IF;
            END;
          END IF;
          SELECT SUM(NVL(pd.SettlementPrice, 0) * NVL(pd.OutboundAmount, 0))
          INTO FAmount
          FROM Partsoutboundbill Pob
          INNER JOIN Partsoutboundbilldetail pd
          ON pob.Id  = pd.PartsOutboundBillId
          WHERE Pob.Id = FPartsOutboundBillId;
          IF FCustomerAccountId > 0 THEN
            BEGIN
              IF FOutboundType = 1 THEN
                BEGIN
                  UPDATE CustomerAccount
                  SET CustomerAccount.Pendingamount     = NVL(CustomerAccount.Pendingamount, 0)       - NVL(FAmount, 0),
                    CustomerAccount.Shippedproductvalue = NVL(CustomerAccount.Shippedproductvalue, 0) + NVL(FAmount, 0),
                    CustomerAccount.Modifierid          = 1,
                    CustomerAccount.Modifiername        = 'WMSInterface',
                    CustomerAccount.Modifytime          = sysdate
                  WHERE CustomerAccount.id              = FCustomerAccountId
                  AND CustomerAccount.status           <> 99;
                END;
              elsif FOutboundType = 2 THEN
                BEGIN
                  BEGIN
                    SELECT sa.estimateddueamount
                    INTO FEstimateddueamount
                    FROM supplieraccount sa
                    WHERE sa.id   = FCustomeraccountId
                    AND sa.status = 1;
                  EXCEPTION
                  WHEN No_Data_Found THEN
                    FErrormsg := '未找到供应商账户，请核对！';
                    raise FMyexception;
                  END;
                  UPDATE supplieraccount
                  SET supplieraccount.estimateddueamount = NVL(supplieraccount.estimateddueamount, 0) + NVL(FAmount, 0),
                    supplieraccount.Modifierid           = 1,
                    supplieraccount.Modifiername         = 'WMSInterface',
                    supplieraccount.Modifytime           = sysdate
                  WHERE supplieraccount.id               = FcustomeraccountId
                  AND supplieraccount.status             = 1;
                END;
              END IF;
            END;
          END IF;
        END;
      END IF;
      IF (c_Gett_Shippingorderhead.User_Def6 = 'Y') THEN
        BEGIN
          BEGIN
            SELECT 1
            INTO FExistsErrorTship
            FROM t_Shippingorderhead
            WHERE shipment_id=c_Gett_Shippingorderhead.shipment_id
            AND isfinished   =2;
          EXCEPTION
          WHEN no_data_found THEN
            FExistsErrorTship    :=NULL;
          WHEN too_many_rows THEN
            FExistsErrorTship    :=NULL;
            IF FExistsErrorTship IS NOT NULL THEN
              FErrormsg          := '存在未处理并且有问题的单据，必须先处理问题单据后才可强制完成';
              raise FMyException;
            END IF;
          END;
          UPDATE PartsOutboundPlan
          SET PartsOutboundPlan.status     = 4,
            PartsOutboundPlan.Modifytime   = sysdate,
            PartsOutboundPlan.ModifierName = 'WMS',
            PartsOutboundPlan.Stoper       = 'WMS',
            PartsOutboundPlan.StopTime     = sysdate,
            PartsOutboundPlan.Remark       = PartsOutboundPlan.Remark
            || ' [该单据已经由业务人员强制完成]'
          WHERE PartsOutboundPlan.id = FOutplanid;
          --减少配件锁定库存
          UPDATE PartsLockedStock
          SET PartsLockedStock.LockedQuantity = PartsLockedStock.LockedQuantity -
            (SELECT NVL(plannedamount, 0)                                       - NVL(outboundfulfillment, 0)
            FROM Partsoutboundplandetail
            WHERE Partsoutboundplandetail.Partsoutboundplanid = FOutplanid
            AND Partsoutboundplandetail.Sparepartid           = PartsLockedStock.Partid
            )
          WHERE PartsLockedStock.WarehouseId = FOutwarehouseid
          AND EXISTS
            (SELECT Id
            FROM Partsoutboundplandetail
            WHERE Partsoutboundplandetail.Partsoutboundplanid = FOutplanid
            AND Partsoutboundplandetail.Sparepartid           = PartsLockedStock.Partid
            );
          SELECT SUM(NVL(popd.Price, 0) * (NVL(popd.PlannedAmount,0)- NVL(popd.OutboundFulfillment, 0)))
          INTO FLeftAmount
          FROM PartsOutboundPlan pop
          INNER JOIN PartsOutboundPlanDetail popd
          ON pop.Id             = popd.PartsOutboundPlanId
          WHERE pop.ID          = FOutplanid;
          IF FCustomerAccountId > 0 THEN
            BEGIN
              IF FOutboundType = 1 THEN
                BEGIN
                  UPDATE CustomerAccount
                  SET CustomerAccount.Pendingamount = NVL(CustomerAccount.Pendingamount, 0) - NVL(FLeftAmount, 0),
                    CustomerAccount.Modifierid      = 1,
                    CustomerAccount.Modifiername    = 'WMSInterface',
                    CustomerAccount.Modifytime      = sysdate
                  WHERE CustomerAccount.id          = FCustomerAccountId
                  AND CustomerAccount.status       <> 99;
                END;
              END IF;
            END;
          END IF;
        END;
      ELSE

      Select SUM(PlannedAmount) - SUM(NVL(outboundfulfillment, 0)) AS isfinished
      INTO FIsAllFinished
      from partsoutboundplandetail where partsoutboundplanid=FOutplanid
      GROUP BY PartsoutboundPlanId;

      If FIsAllFinished>0 THEN
        BEGIN
          FFinishstatus := 2;
        END;
      END IF;
        --1  新建,2  部分出库,3  出库完成,4  终止
        IF FFinishstatus > 1 THEN
          BEGIN
            UPDATE PartsOutboundPlan
            SET PartsOutboundPlan.status = 2,
            PartsOutboundPlan.Modifierid = 1,
            PartsOutboundPlan.Modifiername =FWMSCreatorName,
            PartsOutboundPlan.Modifytime = sysdate
            WHERE PartsOutboundPlan.id   = FOutplanid;
          END;
        elsif FFinishstatus = 1 THEN
          BEGIN
            UPDATE PartsOutboundPlan
            SET PartsOutboundPlan.status = 3,
            PartsOutboundPlan.Modifierid = 1,
            PartsOutboundPlan.Modifiername =FWMSCreatorName,
            PartsOutboundPlan.Modifytime = sysdate
            WHERE PartsOutboundPlan.id   = FOutplanid;
          END;
        ELSE
          BEGIN
            UPDATE PartsOutboundPlan
            SET PartsOutboundPlan.status = 4,
            PartsOutboundPlan.Modifierid = 1,
            PartsOutboundPlan.Modifiername =FWMSCreatorName,
            PartsOutboundPlan.Modifytime = sysdate
            WHERE PartsOutboundPlan.id   = FOutplanid;
          END;
        END IF;
      END IF;
      UPDATE t_Shippingorderhead
      SET t_Shippingorderhead.Isfinished            = 1,
        t_Shippingorderhead.Ttrafficid              = FPartsShippingOrderId,
        t_Shippingorderhead.TOUTID                  = FPartsOutboundBillId
      WHERE t_Shippingorderhead.Interface_Record_Id = c_Gett_Shippingorderhead.Interface_Record_Id;
    EXCEPTION
    WHEN FMyexception THEN
      BEGIN
        ROLLBACK;
        INSERT
        INTO Syncwmsinoutloginfo
          (
            Objid,
            Syncdate,
            Code,
            Synctype,
            Synccode,
            Inoutcode
          )
          VALUES
          (
            FSYNCWMSINOUTLOGINFOId,
            sysdate,
            FErrormsg,
            2,
            c_Gett_Shippingorderhead.Interface_Record_Id,
            c_Gett_Shippingorderhead.User_Def3
          );
        UPDATE t_Shippingorderhead
        SET t_Shippingorderhead.Isfinished            = 2
        WHERE t_Shippingorderhead.Interface_Record_Id = c_Gett_Shippingorderhead.Interface_Record_Id
        AND t_Shippingorderhead.Isfinished            = 0;
      END;
    WHEN OTHERS THEN
      BEGIN
        ROLLBACK;
        FErrormsg := sqlerrm;
        INSERT
        INTO Syncwmsinoutloginfo
          (
            Objid,
            Syncdate,
            Code,
            Synctype,
            Synccode,
            Inoutcode
          )
          VALUES
          (
            FSYNCWMSINOUTLOGINFOId,
            sysdate,
            FErrormsg,
            2,
            c_Gett_Shippingorderhead.Interface_Record_Id,
            c_Gett_Shippingorderhead.User_Def3
          );
      END;
      UPDATE t_Shippingorderhead
      SET t_Shippingorderhead.Isfinished            = 2
      WHERE t_Shippingorderhead.Interface_Record_Id = c_Gett_Shippingorderhead.Interface_Record_Id
      AND t_Shippingorderhead.Isfinished            = 0;
    END;
    Vtable_Shippingorderdetails.delete;
    COMMIT;
  END LOOP;
END;
PROCEDURE synccheck
AS
  FDateFormat                 DATE;
  FDateStr                    VARCHAR2(8);
  FSerialLength               INTEGER;
  FComplateId                 INTEGER;
  FNewCode                    VARCHAR2(50);
  FNewCodePIB                 VARCHAR2(50);
  FPIBBranchId                NUMBER(9);
  FPIBWarehouseId             NUMBER(9);
  FPIBWarehouseCode           VARCHAR2(50);
  FPIBWarehouseName           VARCHAR2(50);
  FPIBStorageCompanyId        NUMBER(9);
  FPIBStorageCompanyCode      VARCHAR2(50);
  FPIBStorageCompanyName      VARCHAR2(50);
  FPIBStorageCompanyType      NUMBER(9);
  FErrorMsg                   VARCHAR2(1000);
  FPartsInventoryBillId       NUMBER(9);
  FPartsInventoryBillPartId   NUMBER(9);
  FPartsInventoryBillPartCode VARCHAR2(50);
  FPartsInventoryBillPartName VARCHAR2(50);
  FPIBWarehouseAreaId         NUMBER(9);
  FPIBWarehouseAreaCode       VARCHAR2(50);
  FAreaCategoryId             NUMBER(9);
  FPartsStockId               NUMBER(9);
  FNewPartsStockId            NUMBER(9);
  FPartStockQuantity          NUMBER(9);
  FPlannedPrice               NUMBER(19, 4);
  FPIBPARTSSALESCATEGORYID    NUMBER(9);
  FPartsInventoryDetailId     NUMBER(9);
  FSYNCWMSINOUTLOGINFOId      NUMBER(9);
  FMyException                EXCEPTION;
  CURSOR Ft_inventory_adjustment
  IS
    SELECT *
    FROM t_inventory_adjustment
    WHERE isfinished = 0
    AND syscode      = 'NPMS';
BEGIN
  FDateFormat   := TRUNC(sysdate, 'DD');
  FDateStr      := TO_CHAR(sysdate, 'yyyymmdd');
  FSerialLength := 6;
  FNewCode      := 'PI{CORPCODE}' || FDateStr || '{SERIAL}';
  BEGIN
    SELECT Id
    INTO FComplateId
    FROM CodeTemplate
    WHERE name    = 'PartsInventoryBill'
    AND IsActived = 1;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    raise_application_error(-20001, '未找到名称为"PartsInventoryBill"的有效编码规则。');
  WHEN OTHERS THEN
    raise;
  END;
  FOR C_t_inventory_adjustment IN Ft_inventory_adjustment
  LOOP
    BEGIN
      FErrorMsg               := '';
      FPartsInventoryBillId   := s_PartsInventoryBill.NEXTVAL;
      FPartsInventoryDetailId := S_PartsInventoryDetail.NEXTVAL;
      FSYNCWMSINOUTLOGINFOId  := S_SYNCWMSINOUTLOGINFO.nextval;
      BEGIN
        BEGIN
          SELECT c.Id,
            w.Id,
            w.Code,
            w.Name,
            w.StorageCompanyId,
            c.Code,
            c.Name,
            w.StorageCompanyType,
            PSC.Id
          INTO FPIBBranchId,
            FPIBWarehouseId,
            FPIBWarehouseCode,
            FPIBWarehouseName,
            FPIBStorageCompanyId,
            FPIBStorageCompanyCode,
            FPIBStorageCompanyName,
            FPIBStorageCompanyType,
            FPIBPARTSSALESCATEGORYID
          FROM warehouse w
          INNER JOIN salesunitaffiwarehouse suaw
          ON w.id=suaw.warehouseid
          INNER JOIN salesunit su
          ON suaw.salesunitid=su.ID
          INNER JOIN Company c
          ON w.StorageCompanyId = c.ID
          AND c.TYPE            = 1
          INNER JOIN partssalescategory psc
          ON su.partssalescategoryid=psc.id
          AND psc.Status            = 1
          INNER JOIN t_warehouse tw
          ON w.storagecenter  =tw.pmsstoragecenterid
          WHERE w.wmsinterface=1
          AND tw.warehousecode=C_t_inventory_adjustment.Warehousecode
          AND psc.NAME        =C_t_inventory_adjustment.BrandName;
        EXCEPTION
        WHEN no_data_found THEN
          BEGIN
            FErrormsg := '找不到仓库,WMS仓库编号：' ||C_t_inventory_adjustment.Warehousecode || '品牌名称：' || C_t_inventory_adjustment.BrandName;
            raise FMyException;
          END;
        WHEN too_many_rows THEN
          BEGIN
            FErrormsg := '存在多个仓库,WMS仓库编号：' ||C_t_inventory_adjustment.Warehousecode || '品牌名称：' || C_t_inventory_adjustment.BrandName;
            raise FMyException;
          END;
        END;
        FNewCodePIB := regexp_replace(regexp_replace(FNewCode, '{CORPCODE}', FPIBStorageCompanyCode), '{SERIAL}', lpad(TO_CHAR(GetSerial(FComplateId, FDateFormat, FPIBStorageCompanyCode)), FSerialLength, '0'));
        INSERT
        INTO PartsInventoryBill
          (
            Id,
            Code,
            BranchId,
            WarehouseAreaCategory,
            WarehouseId,
            WarehouseCode,
            WarehouseName,
            StorageCompanyId,
            StorageCompanyCode,
            StorageCompanyName,
            StorageCompanyType,
            InventoryReason,
            Remark,
            Status,
            CreatorId,
            CreatorName,
            CreateTime,
            ModifierId,
            ModifierName,
            ModifyTime,
            ApproverId,
            ApproverName,
            ApproveTime,
            ResultInputOperatorId,
            ResultInputOperatorName,
            ResultInputTime,
            InventoryRecordOperatorId,
            InventoryRecordOperatorName,
            InventoryRecordTime
          )
          VALUES
          (
            FPartsInventoryBillId,
            FNewCodePIB,
            FPIBBranchId,
            1,
            FPIBWarehouseId,
            FPIBWarehouseCode,
            FPIBWarehouseName,
            FPIBStorageCompanyId,
            FPIBStorageCompanyCode,
            FPIBStorageCompanyName,
            FPIBStorageCompanyType,
            C_t_inventory_adjustment.Memo,
            C_t_inventory_adjustment.Memo,
            3,
            1,
            'WMSPMSInterface',
            sysdate,
            1,
            'WMSPMSInterface',
            sysdate,
            1,
            'WMSPMSInterface',
            sysdate,
            1,
            'WMSPMSInterface',
            sysdate,
            1,
            'WMSPMSInterface',
            sysdate
          );
        BEGIN
          SELECT SparePart.Id,
            SparePart.Code,
            SparePart.Name
          INTO FPartsInventoryBillPartId,
            FPartsInventoryBillPartCode,
            FPartsInventoryBillPartName
          FROM SparePart
          WHERE SparePart.Code = C_t_inventory_adjustment.MaterialCode;
        EXCEPTION
        WHEN no_data_found THEN
          FPartsInventoryBillPartId := NULL;
        WHEN too_many_rows THEN
          BEGIN
            FErrormsg := '根据配件编号查到多个配件,配件编号：' ||C_t_inventory_adjustment.MaterialCode;
            raise FMyException;
          END;
        END;
        IF FPartsInventoryBillPartId IS NOT NULL THEN
          BEGIN
            --查库位
            BEGIN
              SELECT WarehouseArea.Id,
                WarehouseArea.Code,
                WarehouseArea.Areacategoryid
              INTO FPIBWarehouseAreaId,
                FPIBWarehouseAreaCode,
                FAreaCategoryId
              FROM WarehouseArea
              INNER JOIN WarehouseAreaCategory
              ON WarehouseArea.AreaCategoryId    = WarehouseAreaCategory.Id
              WHERE WarehouseArea.WarehouseId    = FPIBWarehouseId
              AND WarehouseArea.AreaKind         = 3
              AND WarehouseArea.Status           = 1
              AND WarehouseAreaCategory.Category = 1;
            EXCEPTION
            WHEN no_data_found THEN
              FPIBWarehouseAreaId := NULL;
            WHEN too_many_rows THEN
              BEGIN
                FErrormsg := '存在多个库位,仓库Id：' ||FPIBWarehouseId;
                raise FMyException;
              END;
            END;
            IF FPIBWarehouseAreaId IS NOT NULL THEN
              BEGIN
                --查库存
                BEGIN
                  SELECT PartsStock.Id
                  INTO FPartsStockId
                  FROM PartsStock
                  WHERE PartsStock.WarehouseId   = FPIBWarehouseId
                  AND PartsStock.PartId          = FPartsInventoryBillPartId
                  AND PartsStock.WarehouseAreaId = FPIBWarehouseAreaId for update;
                EXCEPTION
                WHEN no_data_found THEN
                  FPartsStockId := NULL;
                END;
                BEGIN
                  IF UPPER(trim(C_t_inventory_adjustment.Direction)) = 'FROM' THEN
                    BEGIN
                      IF FPartsStockId IS NOT NULL THEN
                        BEGIN
                          --查库存量
                          SELECT NVL(PartsStock.Quantity, 0) Quantity
                          INTO FPartStockQuantity
                          FROM PartsStock
                          WHERE PartsStock.Id = FPartsStockId;
                          --校验库存量
                          IF (NVL(FPartStockQuantity,0) >= C_t_inventory_adjustment.Quantity) THEN
                            BEGIN
                              UPDATE PartsStock
                              SET PartsStock.Quantity = NVL(Quantity, 0) - C_t_inventory_adjustment.Quantity
                              WHERE PartsStock.Id     = FPartsStockId;
                            END;
                          ELSE
                            BEGIN
                              FErrormsg := '配件库存不足，请检查！';
                              raise FMyException;
                            END;
                          END IF;
                        END;
                      ELSE
                        BEGIN
                          FNewPartsStockId := s_PartsStock.Nextval;
                          --新增库存
                          INSERT
                          INTO PartsStock
                            (
                              Id,
                              WarehouseId,
                              StorageCompanyId,
                              StorageCompanyType,
                              BranchId,
                              WarehouseAreaId,
                              PartId,
                              WarehouseAreaCategoryId,
                              Quantity,
                              CreatorId,
                              CreateTime,
                              CreatorName
                            )
                            VALUES
                            (
                              FNewPartsStockId,
                              FPIBWarehouseId,
                              FPIBStorageCompanyId,
                              FPIBStorageCompanyType,
                              FPIBBranchId,
                              FPIBWarehouseAreaId,
                              FPartsInventoryBillPartId,
                              FAreaCategoryId,
                              C_t_inventory_adjustment.Quantity,
                              1,
                              sysdate,
                              'WMS'
                            );
                        END;
                      END IF;
                      --查计划价
                      BEGIN
                        SELECT NVL(PartsPlannedPrice.PlannedPrice, 0)
                        INTO FPlannedPrice
                        FROM PartsPlannedPrice
                        WHERE PartsPlannedPrice.SparePartId        = FPartsInventoryBillPartId
                        AND PartsPlannedPrice.PartsSalesCategoryId = FPIBPARTSSALESCATEGORYID;
                      EXCEPTION
                      WHEN NO_DATA_FOUND THEN
                        BEGIN
                          FErrormsg := '配件计划价不存在，请检查！';
                          raise FMyException;
                        END;
                      END;
                      INSERT
                      INTO PartsInventoryDetail
                        (
                          Id,
                          PartsInventoryBillId,
                          SparePartId,
                          SparePartCode,
                          SparePartName,
                          WarehouseAreaId,
                          WarehouseAreaCode,
                          CurrentStorage,
                          StorageAfterInventory,
                          Ifcover,
                          StorageDifference,
                          CostPrice,
                          AmountDifference
                        )
                        VALUES
                        (
                          FPartsInventoryDetailId,
                          FPartsInventoryBillId,
                          FPartsInventoryBillPartId,
                          FPartsInventoryBillPartCode,
                          FPartsInventoryBillPartName,
                          FPIBWarehouseAreaId,
                          FPIBWarehouseAreaCode,
                          NVL(FPartStockQuantity,0),
                          NVL(FPartStockQuantity,0) - NVL(C_t_inventory_adjustment.Quantity, 0),
                          1,
                          -C_t_inventory_adjustment.Quantity,
                          FPlannedPrice,
                          FPlannedPrice * -NVL(C_t_inventory_adjustment.Quantity, 0)
                        );
                      UPDATE PartsInventoryBill
                      SET AmountDifference = FPlannedPrice * -NVL(C_t_inventory_adjustment.Quantity, 0)
                      WHERE Id             = FPartsInventoryBillId;
                      --因为当前PMS系统都是无批次管理所以不生成 配件库存 批次明细
                    END;
                  END IF;
                  IF UPPER(trim(C_t_inventory_adjustment.Direction)) = 'TO' THEN
                    BEGIN
                      IF FPartsStockId IS NOT NULL THEN
                        BEGIN
                         --查库存量
                          SELECT NVL(PartsStock.Quantity, 0) Quantity
                          INTO FPartStockQuantity
                          FROM PartsStock
                          WHERE PartsStock.ID = FPartsStockId;

                          UPDATE PartsStock
                          SET Quantity        = NVL(Quantity, 0) + NVL(C_t_inventory_adjustment.Quantity, 0)
                          WHERE PartsStock.ID = FPartsStockId;
                        END;
                      ELSE
                        BEGIN
                          FNewPartsStockId := s_PartsStock.Nextval;
                          --新增库存
                          INSERT
                          INTO PartsStock
                            (
                              Id,
                              WarehouseId,
                              StorageCompanyId,
                              StorageCompanyType,
                              BranchId,
                              WarehouseAreaId,
                              PartId,
                              WarehouseAreaCategoryId,
                              Quantity,
                              CreatorId,
                              CreateTime,
                              CreatorName
                            )
                            VALUES
                            (
                              FNewPartsStockId,
                              FPIBWarehouseId,
                              FPIBStorageCompanyId,
                              FPIBStorageCompanyType,
                              FPIBBranchId,
                              FPIBWarehouseAreaId,
                              FPartsInventoryBillPartId,
                              FAreaCategoryId,
                              C_t_inventory_adjustment.Quantity,
                              1,
                              sysdate,
                              'WMS'
                            );
                        END;
                      END IF;
                      BEGIN
                        SELECT NVL(PartsPlannedPrice.PlannedPrice, 0)
                        INTO FPlannedPrice
                        FROM PartsPlannedPrice
                        WHERE PartsPlannedPrice.SparePartId        = FPartsInventoryBillPartId
                        AND PartsPlannedPrice.Partssalescategoryid = FPIBPARTSSALESCATEGORYID;
                      EXCEPTION
                      WHEN NO_DATA_FOUND THEN
                        BEGIN
                          FErrormsg := '配件计划价不存在，请检查！';
                          raise FMyException;
                        END;
                      END;
                      INSERT
                      INTO PartsInventoryDetail
                        (
                          Id,
                          PartsInventoryBillId,
                          SparePartId,
                          SparePartCode,
                          SparePartName,
                          WarehouseAreaId,
                          WarehouseAreaCode,
                          CurrentStorage,
                          StorageAfterInventory,
                          Ifcover,
                          StorageDifference,
                          CostPrice,
                          AmountDifference
                        )
                        VALUES
                        (
                          FPartsInventoryDetailId,
                          FPartsInventoryBillId,
                          FPartsInventoryBillPartId,
                          FPartsInventoryBillPartCode,
                          FPartsInventoryBillPartName,
                          FPIBWarehouseAreaId,
                          FPIBWarehouseAreaCode,
                          NVL(FPartStockQuantity,0),
                          NVL(FPartStockQuantity,0) + NVL(C_t_inventory_adjustment.Quantity, 0),
                          1,
                          NVL(C_t_inventory_adjustment.Quantity, 0),
                          FPlannedPrice,
                          FPlannedPrice * NVL(C_t_inventory_adjustment.Quantity, 0)
                        );
                      UPDATE PartsInventoryBill
                      SET AmountDifference = FPlannedPrice * NVL(C_t_inventory_adjustment.Quantity, 0)
                      WHERE Id             = FPartsInventoryBillId;
                      --因为当前PMS系统都是无批次管理所以不生成 配件库存批次明细
                    END;
                  END IF;
                  IF (Upper(trim(C_t_inventory_adjustment.Direction)) <> 'FROM') AND (uPPER(trim(C_t_inventory_adjustment.Direction)) <> 'TO') THEN
                    BEGIN
                      FErrormsg := '库存调整方向非法，请检查！';
                      raise FMyException;
                    END;
                  END IF;
                END;
              END;
            ELSE
              BEGIN
                FErrormsg := '仓库库区库位不存在，请检查！';
                raise FMyException;
              END;
            END IF;
          END;
        ELSE
          BEGIN
            FErrormsg := '盘点单配件不存在，请检查！';
            raise FMyException;
          END;
        END IF;
      END;
      UPDATE t_inventory_adjustment
      SET t_inventory_adjustment.IsFinished = 1,
        t_inventory_adjustment.TCHECKID     = FPartsInventoryBillId
      WHERE t_inventory_adjustment.Code     = C_t_inventory_adjustment.Code;
      COMMIT;
    EXCEPTION
    WHEN FMyException THEN
      BEGIN
        ROLLBACK;
        INSERT
        INTO Syncwmsinoutloginfo
          (
            objid,
            syncdate,
            code,
            synctype,
            synccode
          )
          VALUES
          (
            FSYNCWMSINOUTLOGINFOId,
            sysdate,
            FErrormsg,
            3,
            C_t_inventory_adjustment.Code
          );
        UPDATE t_inventory_adjustment
        SET t_inventory_adjustment.IsFinished = 2
        WHERE t_inventory_adjustment.Code     = C_t_inventory_adjustment.Code;
      END;
    WHEN OTHERS THEN
      BEGIN
        ROLLBACK;
        FErrorMsg := sqlerrm;
        INSERT
        INTO Syncwmsinoutloginfo
          (
            objid,
            syncdate,
            code,
            synctype,
            synccode
          )
          VALUES
          (
            FSYNCWMSINOUTLOGINFOId,
            sysdate,
            FErrormsg,
            3,
            C_t_inventory_adjustment.Code
          );
        UPDATE t_inventory_adjustment
        SET t_inventory_adjustment.IsFinished = 2
        WHERE t_inventory_adjustment.Code     = C_t_inventory_adjustment.Code;
      END;
    END;
  END LOOP;
END;
PROCEDURE Syncwarehouses
AS
  FWmswarehousecode    VARCHAR2(25);
  FWmswarehousename    VARCHAR2(25);
  FExistIds            NUMBER(9);
  FMaxId               NUMBER(9);
  Finishstatus         NUMBER(9);
  Errormsg             VARCHAR2(1000);
  FKeyValueItemId      NUMBER(9);
  FSYNCWMSINOUTLOGINFO NUMBER(9);
  CURSOR t_Warehouse
  IS
    SELECT *
    FROM t_Warehouse
    WHERE Isfinished = 0
    ORDER BY INTERNAL_WAREHOUSE_NUM ASC;
BEGIN
  FOR c_t_Warehouse IN t_Warehouse
  LOOP
    BEGIN
      Finishstatus         := 1;
      FWmswarehousecode    := c_t_Warehouse.Warehousecode;
      FWmswarehousename    := c_t_Warehouse.Warehousename;
      FKeyValueItemId      := s_KeyValueItem.Nextval;
      FSYNCWMSINOUTLOGINFO := S_SYNCWMSINOUTLOGINFO.nextval;
      BEGIN
        SELECT Id
        INTO FExistIds
        FROM KeyValueItem
        WHERE name = 'Storage_Center'
        AND value  = c_t_Warehouse.PMSStorageCenterId;
      EXCEPTION
      WHEN No_Data_Found THEN
        FExistIds := NULL;
      END;
      IF FExistIds IS NULL THEN
        BEGIN
          BEGIN
            SELECT NVL(MAX(KEY), 0)
            INTO FMaxId
            FROM KeyValueItem
            WHERE name = 'Storage_Center';
          EXCEPTION
          WHEN No_Data_Found THEN
            FMaxId := 0;
          END;
          BEGIN
            INSERT
            INTO KeyValueItem
              (
                Id,
                Category,
                Name,
                caption,
                KEY,
                Value,
                isbuiltin,
                status
              )
              VALUES
              (
                FKeyValueItemId,
                'DCS',
                'Storage_Center',
                '储运中心',
                FMaxId + 1,
                FWmswarehousename,
                0,
                1
              );
          END;
          UPDATE t_Warehouse
          SET t_Warehouse.PMSStorageCenterId = FMaxId + 1
          WHERE t_Warehouse.Warehousecode    = c_t_Warehouse.Warehousecode;
        END;
      ELSE
        BEGIN
          UPDATE KeyValueItem
          SET Value  = FWmswarehousename
          WHERE name = 'Storage_Center'
          AND KEY    = FExistIds;
        END;
      END IF;
      UPDATE t_Warehouse
      SET t_Warehouse.Isfinished      = 1
      WHERE t_Warehouse.Warehousecode = c_t_Warehouse.Warehousecode;
      COMMIT;
    EXCEPTION
    WHEN OTHERS THEN
      BEGIN
        Errormsg := sqlerrm;
        INSERT
        INTO Syncwmsinoutloginfo
          (
            Objid,
            Syncdate,
            Code,
            Synctype,
            Synccode
          )
          VALUES
          (
            S_SYNCWMSINOUTLOGINFO.nextval,
            sysdate,
            Errormsg,
            4,
            c_t_Warehouse.Warehousecode
          );
        UPDATE t_Warehouse
        SET t_Warehouse.Isfinished      = 2
        WHERE t_Warehouse.Warehousecode = c_t_Warehouse.Warehousecode;
      END;
    END;
  END LOOP;
END;


PROCEDURE Synctoshelve
AS

type T_toshelvedetailsset
IS
  TABLE OF t_toshelvedetail%rowtype INDEX BY binary_integer;
  Vtable_toshelvedetails T_toshelvedetailsset;
  FNewCode                       VARCHAR2(50);
  FNewCodePSO         VARCHAR2(40);
  Vdetailno                      NUMBER(9);
  Vi                             NUMBER(9);
  FComplateId                    INTEGER;
  FErrormsg                      VARCHAR2(1000);
  FErrorInplanCode               VARCHAR2(50);
  FWarehouseCode                 VARCHAR2(50);
  FParentId                      VARCHAR2(25);
  FIPStorageCompanyId     NUMBER(9);
  FIPStorageCompanyType   VARCHAR2(40);
  FIPId                   NUMBER(9);
  FIPStatus               NUMBER(3);
  FIPWarehouseId          NUMBER(9);
  FIPWarehousecode        VARCHAR2(50);
  FIPWarehousename     VARCHAR2(100);
  FIPStorageCompanyCode          VARCHAR2(50);
  FIPStorageCompanyName  VARCHAR2(100);
  FIPBranchId             NUMBER(9);
  FIPPartsSalesCategoryId NUMBER(9);
  FIPInboundType                 NUMBER(9);
  FJYWarehouseAreaId      NUMBER(9);
  FJYWarehouseAreaCode    VARCHAR2(40);
  FBGWarehouseAreaId      NUMBER(9);
  FBGWarehouseAreaCode    VARCHAR2(40);
  FSparePartId          NUMBER(9, 0);
  FSparePartName         VARCHAR2(100);
  FPARTSSHIFTORDERID    NUMBER(9);
  FPartsStockResult           NUMBER(9);
  FJYPartsStockId            NUMBER(9);
  FBGPartsStockId            NUMBER(9);
  FBGAreaCategoryId         NUMBER(9);
  FJYAreaCategoryId         NUMBER(9);
  FMyException                   EXCEPTION;

  CURSOR getT_toshelve
  IS
    SELECT * FROM t_toshelve WHERE isfinished = 0 order by objid;

  CURSOR getT_toshelvedetails(FParentIds VARCHAR2)
  IS
    SELECT t_toshelvedetail.*,
      row_number() over(order by ParentId ASC) rno
    FROM t_toshelvedetail
    INNER JOIN t_toshelve
    ON t_toshelvedetail.ParentID  = t_toshelve.OBJID
    LEFT JOIN PartsInboundPlan on t_toshelvedetail.inplancode=PartsInboundPlan.Code
    WHERE t_toshelve.isfinished    = 0
    AND t_toshelvedetail.ParentID = FParentIds
    ORDER BY PartsInboundPlan.Warehouseid;

BEGIN
  Vtable_toshelvedetails.delete;
  FNewCode := 'PM{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
  BEGIN
    SELECT Id
    INTO FComplateId
    FROM CodeTemplate
    WHERE name    = 'PartsShiftOrder'
    AND IsActived = 1;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    FErrormsg := '未找到名称为"PartsShiftOrder"的有效编码规则。';
    raise FMyException;
  WHEN OTHERS THEN
    raise;
  END;

  FOR C_getT_toshelve IN getT_toshelve
  LOOP
  BEGIN
      FParentId := C_getT_toshelve.Objid;
      FWarehouseCode:=' ';
      FOR C_getT_toshelvedetails IN getT_toshelvedetails(FParentId)
      LOOP
        Vdetailno                                              := C_getT_toshelvedetails.rno;
        Vtable_toshelvedetails(Vdetailno).interface_record_id  := C_getT_toshelvedetails.interface_record_id;
        Vtable_toshelvedetails(Vdetailno).ParentID             := C_getT_toshelvedetails.ParentID;
        Vtable_toshelvedetails(Vdetailno).Materialcode         := C_getT_toshelvedetails.Materialcode;
        Vtable_toshelvedetails(Vdetailno).Materialname         := C_getT_toshelvedetails.Materialname;
        Vtable_toshelvedetails(Vdetailno).Qty                  := C_getT_toshelvedetails.Qty;
        Vtable_toshelvedetails(Vdetailno).INPLANCODE           := C_getT_toshelvedetails.INPLANCODE;
        Vtable_toshelvedetails(Vdetailno).WAREHOUSECODE        := C_getT_toshelvedetails.WAREHOUSECODE;
        Vtable_toshelvedetails(Vdetailno).PARTSSHIFTORDERID    := C_getT_toshelvedetails.PARTSSHIFTORDERID;
      END LOOP;
      FErrormsg := '';
      FOR Vi IN 1 .. Vtable_toshelvedetails.count
      LOOP
      BEGIN
        SELECT PartsInboundPlan.Id              AS FIPId,
          PartsInboundPlan.Status               AS FIPStatus,
          PartsInboundPlan.WarehouseId          AS FIPWarehouseId,
          partsinboundplan.Warehousecode        AS FIPWarehousecode,
          partsinboundplan.warehousename        AS FIPWarehousename,
          PartsInboundPlan.StorageCompanyId     AS FIPStorageCompanyId,
          PartsInboundPlan.StorageCompanyCode   AS FIPStorageCompanyCode,
          PartsInboundPlan.StorageCompanyName   AS FIPStorageCompanyName,
          PartsInboundPlan.StorageCompanyType   AS FIPStorageCompanyType,
          PartsInboundPlan.Partssalescategoryid AS FIPPartsSalesCategoryId,
          PartsInboundPlan.Branchid             AS FIPBranchId,
          PartsInboundPlan.InboundType          AS FIPInboundType
        INTO FIPId,
          FIPStatus,
          FIPWarehouseId,
          FIPWarehousecode,
          FIPWarehousename,
          FIPStorageCompanyId,
          FIPStorageCompanyCode,
          FIPStorageCompanyName,
          FIPStorageCompanyType,
          FIPPartsSalesCategoryId,
          FIPBranchId,
          FIPInboundType
        FROM PartsInboundPlan
        WHERE Code =  Vtable_toshelvedetails(Vi).INPLANCODE;
      EXCEPTION
      WHEN no_data_found THEN
        FIPId                 := NULL;
        FIPWarehouseId        := NULL;
        FIPWarehousecode      := NULL;
        FIPStorageCompanyId   := NULL;
        FIPStorageCompanyType := NULL;
        FIPBranchId:= NULL;
      END;

     IF FIPID is not null then
      BEGIN
        --入库计划单状态校验
        IF FIPStatus =1 THEN
        BEGIN
          FErrormsg := '入库计划单'||Vtable_toshelvedetails(Vi).INPLANCODE||'状态为新建';
          FErrorInplanCode := Vtable_toshelvedetails(Vi).INPLANCODE;
          raise FMyException;
        END;
        END IF;

        --获取保管区唯一库位
        BEGIN
           SELECT WarehouseArea.Id,
                WarehouseArea.Code,
                WarehouseArea.AreaCategoryId
           INTO FBGWarehouseAreaId,
                FBGWarehouseAreaCode,
                FBGAreaCategoryId
              FROM WarehouseArea
              INNER JOIN WarehouseAreaCategory
              ON WarehouseArea.AreaCategoryId    = WarehouseAreaCategory.Id
              WHERE WarehouseArea.WarehouseId    = FIPWarehouseId
              AND WarehouseArea.Areakind         = 3
              AND WarehouseAreaCategory.Category = 1;
         EXCEPTION
            WHEN no_data_found THEN
              BEGIN
                FErrormsg := 'PMS仓库对应唯一保管区库位未维护，请核对！';
                FErrorInplanCode := Vtable_toshelvedetails(Vi).INPLANCODE;
                raise FMyException;
              END;
            WHEN too_many_rows THEN
               BEGIN
                FErrormsg := 'wms仓库库位不唯一';
                 FErrorInplanCode := Vtable_toshelvedetails(Vi).INPLANCODE;
                raise FMyException;
               END;

         END;

         --获取检验区唯一库位
         BEGIN
           SELECT WarehouseArea.Id,
                WarehouseArea.Code,
                WarehouseArea.AreaCategoryId
           INTO FJYWarehouseAreaId,
                FJYWarehouseAreaCode,
                FJYAreaCategoryId
              FROM WarehouseArea
              INNER JOIN WarehouseAreaCategory
              ON WarehouseArea.AreaCategoryId    = WarehouseAreaCategory.Id
              WHERE WarehouseArea.WarehouseId    = FIPWarehouseId
              AND WarehouseArea.Areakind         = 3
              AND WarehouseAreaCategory.Category = 3;
         EXCEPTION
            WHEN no_data_found THEN
              BEGIN
                FErrormsg := 'PMS仓库对应唯一检验区库位未维护，请核对！';
                FErrorInplanCode := Vtable_toshelvedetails(Vi).INPLANCODE;
                raise FMyException;
              END;
                WHEN too_many_rows THEN
               BEGIN
                FErrormsg := 'wms仓库库位不唯一';
                 FErrorInplanCode := Vtable_toshelvedetails(Vi).INPLANCODE;
                raise FMyException;
               END;

         END;

         --校验配件是否存在
         BEGIN
              SELECT Sparepart.Id,
              Sparepart.Name
              INTO FSparepartId,
              FSparepartName
              FROM Sparepart
              WHERE Sparepart.Status = 1
              AND Sparepart.Code     = Vtable_toshelvedetails(Vi).MaterialCode;
         EXCEPTION
              WHEN no_data_found THEN
              BEGIN
                FErrormsg := '找不到对应的配件，请核对！' || Vtable_toshelvedetails(Vi).MaterialCode;
                FErrorInplanCode := Vtable_toshelvedetails(Vi).INPLANCODE;
                raise FMyException;
              END;
         END;

         --校验库存
         BEGIN
              SELECT NVL(PartsStock.Quantity, 0) - NVL(Vtable_toshelvedetails(Vi).Qty, 0),
                PartsStock.Id
              INTO FPartsStockResult,
                FJYPartsStockId
              FROM PartsStock
              INNER JOIN WarehouseAreaCategory
              ON PartsStock.WarehouseAreaCategoryId=WarehouseAreaCategory.ID
              WHERE WarehouseAreaCategory.Category =3
              AND PartsStock.WarehouseId           = FIPWarehouseId
              AND PartsStock.Partid                = FSparepartId for update;
            EXCEPTION
            WHEN no_data_found THEN
              BEGIN
                FErrormsg := '检验区库存不存在' || Vtable_toshelvedetails(Vi).MATERIALCODE;
                FErrorInplanCode := Vtable_toshelvedetails(Vi).INPLANCODE;
                raise FMyException;
              END;
            WHEN too_many_rows THEN
              BEGIN
                FErrormsg := '检验区存在多条库存,仓库'|| FIPWarehousename || '配件' || Vtable_toshelvedetails(Vi).MATERIALCODE;
                FErrorInplanCode := Vtable_toshelvedetails(Vi).INPLANCODE;
                raise FMyException;
              END;
            END;
            BEGIN
              IF FPartsStockResult < 0 THEN
                BEGIN
                  FErrormsg := '检验区库存不足' ||  Vtable_toshelvedetails(Vi).MATERIALCODE;
                  FErrorInplanCode := Vtable_toshelvedetails(Vi).INPLANCODE;
                  raise FMyException;
                END;
              END IF;
            END;

        IF FIPWarehousecode <> FWarehouseCode THEN
        BEGIN
          --生成编号
          FNewCodePSO := regexp_replace(regexp_replace(FNewCode, '{CORPCODE}', FIPStorageCompanyCode), '{SERIAL}', lpad(TO_CHAR(GetSerial(FComplateId, TRUNC(sysdate, 'DD'), FIPStorageCompanyCode)), 6, '0'));
          FPARTSSHIFTORDERID:= s_PARTSSHIFTORDER.Nextval;
          --插入移库单主单
          Insert into PARTSSHIFTORDER
           ( ID,
            CODE,
            BRANCHID,
            WAREHOUSEID,
            WAREHOUSECODE,
            WAREHOUSENAME,
            STORAGECOMPANYID,
            STORAGECOMPANYCODE,
            STORAGECOMPANYNAME,
            STORAGECOMPANYTYPE,
            STATUS,
            TYPE,
            CREATORID,
            CREATORNAME,
            CREATETIME,
            Remark)
            values
            (
               FPARTSSHIFTORDERID,
               FNewCodePSO,
               FIPBranchId,
               FIPWarehouseId,
               FIPWarehousecode,
               FIPWarehousename,
               FIPStorageCompanyId,
               FIPStorageCompanyCode,
               FIPStorageCompanyName,
               FIPStorageCompanyType,
               1, --默认生效状态
               1, --默认上架类型
               1,
               'WMSPMSInterface',
               SYSDATE,
               Vtable_toshelvedetails(Vi).INPLANCODE
            );
            FWarehouseCode:=FIPWarehousecode;
        END;
        END IF;
        --插入移库单清单
        insert into PARTSSHIFTORDERDETAIL
           (
            ID,
            PARTSSHIFTORDERID,
            SPAREPARTID,
            SPAREPARTCODE,
            SPAREPARTNAME,
            ORIGINALWAREHOUSEAREAID,
            ORIGINALWAREHOUSEAREACODE,
            ORIGINALWAREHOUSEAREACATEGORY,
            DESTWAREHOUSEAREAID,
            DESTWAREHOUSEAREACODE,
            DESTWAREHOUSEAREACATEGORY,
            BATCHNUMBER,
            QUANTITY
            )
            values
            (
              s_PARTSSHIFTORDERDETAIL.Nextval,
              FPARTSSHIFTORDERID,
              FSparepartId,
              Vtable_toshelvedetails(Vi).MATERIALCODE,
              FSparepartName,
              FJYWarehouseAreaId,
              FJYWarehouseAreaCode,
              3,
              FBGWarehouseAreaId,
              FBGWarehouseAreaCode,
              1,
              FParentId,
              Vtable_toshelvedetails(Vi).Qty
            );
         --减少检验区库存
         UPDATE PartsStock
         SET PartsStock.Quantity = NVL(PartsStock.Quantity, 0) - NVL(Vtable_toshelvedetails(Vi).Qty, 0)
         WHERE PartsStock.Id     = FJYPartsStockId;
         --增加保管区库存
          BEGIN
            SELECT PartsStock.Id
            INTO FBGPartsStockId
            FROM PartsStock
            WHERE PartsStock.WarehouseId   = FIPWarehouseId
            AND PartsStock.PartId          = FSparepartId
            AND PartsStock.WarehouseAreaId = FBGWarehouseAreaId for update;
          EXCEPTION
          WHEN no_data_found THEN
            FBGPartsStockId := NULL;
          END;
          IF FBGPartsStockId IS NULL THEN
          BEGIN
          FBGPartsStockId := s_PartsStock.Nextval;
          INSERT
          INTO PartsStock
            (
              Id,
              WarehouseId,
              StorageCompanyId,
              StorageCompanyType,
              BranchId,
              WarehouseAreaId,
              PartId,
              WarehouseAreaCategoryId,
              Quantity,
              CreatorId,
              CreateTime,
              CreatorName
            )
            VALUES
            (
              FBGPartsStockId,
              FIPWarehouseId,
              FIPStorageCompanyId,
              FIPStorageCompanyType,
              FIPBranchId,
              FBGWarehouseAreaId,
              FSparepartId,
              FBGAreaCategoryId,
              Vtable_toshelvedetails(Vi).Qty,
              1,
              sysdate,
              'WMSPMSInterface'
            );
        END;
      ELSE
        BEGIN
          UPDATE PartsStock
          SET Quantity        = NVL(Quantity, 0) + NVL( Vtable_toshelvedetails(Vi).Qty, 0)
          WHERE PartsStock.Id = FBGPartsStockId;
        END;
      END IF;
      --新增配件库位变更履历
      insert into WAREHOUSEAREAHISTORY
       (
        ID,
        WAREHOUSEID,
        STORAGECOMPANYID,
        STORAGECOMPANYTYPE,
        BRANCHID,
        DESTWAREHOUSEAREAID,
        DESTWAREHOUSEAREACATEGORYID,
        WAREHOUSEAREAID,
        WAREHOUSEAREACATEGORYID,
        PARTID,
        QUANTITY,
        CREATORID,
        CREATORNAME,
        CREATETIME
      )values
      (
        s_WAREHOUSEAREAHISTORY.Nextval,
        FIPWarehouseId,
        FIPStorageCompanyId,
        FIPStorageCompanyType,
        FIPBranchId,
        FBGWarehouseAreaId,
        FBGAreaCategoryId,
        FJYWarehouseAreaId,
        FJYAreaCategoryId,
        FSparepartId,
         Vtable_toshelvedetails(Vi).Qty,
        1,
        'WMSPMSInterface',
        sysdate
      );

      update t_toshelvedetail
      set partsshiftorderid=FPARTSSHIFTORDERID
      where interface_record_id=Vtable_toshelvedetails(Vi).interface_record_id;

      END;
        ELSE
          BEGIN
            FErrormsg := '入库计划单编号不存在，请核对！';
            FErrorInplanCode := Vtable_toshelvedetails(Vi).INPLANCODE;
            raise FMyException;
          END;
        END IF;
    END LOOP;
    UPDATE t_toshelve SET t_toshelve.Isfinished = 1 WHERE t_toshelve.OBJID = C_getT_toshelve.OBJID;
  EXCEPTION
      WHEN FMyException THEN
        BEGIN
          ROLLBACK;
          INSERT
          INTO Syncwmsinoutloginfo
            (
              OBJID,
              syncdate,
              code,
              synctype,
              synccode,
              INOUTCODE
            )
            VALUES
            (
              s_Syncwmsinoutloginfo.Nextval,
              sysdate,
              FErrormsg,
              5,
              C_getT_toshelve.OBJID,
              FErrorInplanCode
            );
          UPDATE t_toshelve SET t_toshelve.Isfinished = 2 WHERE t_toshelve.OBJID = C_getT_toshelve.OBJID;
        END;
      WHEN OTHERS THEN
        BEGIN
          ROLLBACK;
          FErrormsg := sqlerrm;
          INSERT
          INTO Syncwmsinoutloginfo
            (
              OBJID,
              syncdate,
              code,
              synctype,
              synccode,
              INOUTCODE
            )
            VALUES
            (
              s_Syncwmsinoutloginfo.Nextval,
              sysdate,
              FErrormsg,
              5,
              C_getT_toshelve.OBJID,
              FErrorInplanCode
            );
          UPDATE t_toshelve SET t_toshelve.Isfinished = 2 WHERE t_toshelve.OBJID = C_getT_toshelve.OBJID;
        END;
      END;
      Vtable_toshelvedetails.delete;
      COMMIT;
  END LOOP;
END;
END SYNCWMSINOUT;
