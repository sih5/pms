--新增按钮

--维修索赔管理
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|APPPicture', '查看APP照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8301, 'RepairClaimBill|APPRoute', '查看APP轨迹', 2);


--保外维修单管理-服务站
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|APPPicture', '查看APP照片', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|APPRoute', '查看APP轨迹', 2);


--二级站维修单管理
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8404, 'RepairOrderForSecond|APPPicture', '查看APP照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8404, 'RepairOrderForSecond|APPRoute', '查看APP轨迹', 2);

--维修索赔管理-服务站
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8602, 'RepairClaimBillForDealer|APPPicture', '查看APP照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8602, 'RepairClaimBillForDealer|APPRoute', '查看APP轨迹', 2);

--保外维修单管理
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8103, 'RepairOrderForWarrantyBranch|APPPicture', '查看APP照片', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8103, 'RepairOrderForWarrantyBranch|APPRoute', '查看APP轨迹', 2);



-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
