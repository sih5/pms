
--新增8个节点

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(2109,2100, 2, 'PartsPriceHistoryQuery', 'Released', NULL, '配件价格报表查询', '查看配件价格', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 8, 2);


INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(3112, 3100, 2, 'PurchaseQuery', 'Released', NULL, '采购需求报表查询', '采购需求查询', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 9, 2);


INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(4113, 4100,2, 'ServiceAgentsHistoryQuery', 'Released', NULL, '服务站向代理库提报订单报表查询', '服务站向代理库提报订单查询', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 12, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(4114, 4100, 2, 'PartsSalesQuery', 'Released', NULL, '配件销售在途报表查询', '配件销售在途查询', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 13, 2);


INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(5207, 5200, 2, 'AgencyStockQuery', 'Released', NULL, '代理库库存报表查询', '代理库库存查询', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 7, 2);


INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(5308, 5300, 2, 'InboundHistoryQuery', 'Released', NULL, '历史入库记录报表查询', '查看历史入库记录', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 8, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(5309, 5300, 2, 'AgentsHistoryIn', 'Released', NULL, '代理库入库记录报表查询', '查询某一时间段内代理库入库明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 9, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES 
(5405, 5400, 2, 'AgentsHistoryOut', 'Released', NULL, '代理库出库记录报表查询', '查询某一时间段内代理库出库明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 10, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
