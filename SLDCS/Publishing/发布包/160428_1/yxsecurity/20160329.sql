
insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14501, 14500, 2, 'WarehouseAgeDetailSta', 'Released', '', '库龄明细统计报表', '', '', '统计各个配件在库时间', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 18, 2);
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);