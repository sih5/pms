INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8107, 8100, 2, 'ReturnVisitQuest', 'Released', NULL, '满意度问卷管理', '满意度问卷管理', 'Client/DCS/Images/Menu/Service/ReturnVisitQuest.png', 1, 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8107, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8107, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5402, 'Common|NotWarehousePrint', '未出库打印', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5402, 'Common|NotPartitionPrint', '未出库分区打印', 2);
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);