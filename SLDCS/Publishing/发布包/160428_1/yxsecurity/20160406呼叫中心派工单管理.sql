INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8108, 8100, 2, 'RepairWorkOrderMd', 'Released', NULL, '呼叫中心派工单管理', '呼叫中心派工单管理', 'Client/DCS/Images/Menu/Service/RepairWorkOrderMd.png', 1, 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5401, 'PartsOutboundBill|NotPartitionPrint', '未出库分区打印', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Accept', '接受', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Refuse', '拒绝', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|SetOut', '出发', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Station', '到站', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Arrive', '到达', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|StartMaintenance', '开始维修', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|ProcessingCompleted', '处理完毕', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|HoursCompleted', '24小时是否可完成', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Revoke', '撤销', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|ViewDetails', '查看详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|VisitorRegistration', '来客登记', 2);

INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
