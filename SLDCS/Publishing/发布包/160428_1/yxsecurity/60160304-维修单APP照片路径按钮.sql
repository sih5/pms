insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8102, 'RepairOrderForBranch|APPPicture', '查看APP照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8102, 'RepairOrderForBranch|APPRoute', '查看APP轨迹', 2);

INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|APPPicture', '查看APP照片', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8402, 'RepairOrderForDealer|APPRoute', '查看APP轨迹', 2);


-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
