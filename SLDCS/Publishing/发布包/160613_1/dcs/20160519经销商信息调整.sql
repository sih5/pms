alter table DealerServiceInfoHistory add Distance NUMBER(9);
alter table DealerServiceInfoHistory add MinServiceCode VARCHAR2(50);
alter table DealerServiceInfoHistory add MinServiceName VARCHAR2(100);
alter table DealerServiceInfoHistory add ADistance NUMBER(9);
alter table DealerServiceInfoHistory add AMinServiceCode VARCHAR2(50);
alter table DealerServiceInfoHistory add AMinServiceName VARCHAR2(100);
alter table DealerHistory add Distance NUMBER(9);
alter table DealerHistory add MinServiceCode VARCHAR2(50);
alter table DealerHistory add MinServiceName VARCHAR2(100);
alter table DealerHistory add ADistance NUMBER(9);
alter table DealerHistory add AMinServiceCode VARCHAR2(50);
alter table DealerHistory add AMinServiceName VARCHAR2(100);

alter table DealerServiceInfo add Distance NUMBER(9);
alter table DealerServiceInfo add MinServiceCode VARCHAR2(50);
alter table DealerServiceInfo add MinServiceName VARCHAR2(100);

alter table DealerKeyEmployee add IsKeyPositions NUMBER(1) default 1 not null;
alter table DealerKeyPosition add IsKeyPositions NUMBER(1) default 1 not null;
