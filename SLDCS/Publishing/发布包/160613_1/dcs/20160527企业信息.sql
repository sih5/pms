create table SignalRealTime  (
   Id                   VARCHAR2(38)                    not null,
   UserId               VARCHAR2(50),
   OverTime             DATE,
   CreateTime           DATE,
   CompanyCode          VARCHAR2(50),
   Status               NUMBER(9),
   constraint PK_SIGNALREALTIME primary key (Id)
)
/