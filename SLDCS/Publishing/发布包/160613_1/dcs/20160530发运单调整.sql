
alter table PartsShippingOrder add ArrivalMode           NUMBER(9);
alter table PartsShippingOrder add ShippingTime           DATE;
alter table PartsShippingOrder add ShippingMsg           VARCHAR2(100);
alter table PartsShippingOrder add ArrivalTime           DATE;
alter table PartsShippingOrder add CargoTerminalMsg           VARCHAR2(100);
alter table PartsShippingOrder add FlowFeedback           VARCHAR2(4000);
alter table PartsShippingOrder add ExpressCompany          VARCHAR2(100);
alter table PartsShippingOrder add QueryURL           VARCHAR2(100);


Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsShippingOrder_Status', '配件发运单状态', 4, '已发货', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsShippingOrder_Status', '配件发运单状态', 5, '待提货', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsShippingOrder_Status', '配件发运单状态', 6, '待收货', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'ArrivalMode', '到货模式', 1, '送货上门', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'ArrivalMode', '到货模式', 2, '货运站', 1, 1);

