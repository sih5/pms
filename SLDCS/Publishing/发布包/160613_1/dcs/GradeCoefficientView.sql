
create or replace view GradeCoefficientView as
select id, PartsSalesCategoryName,a.Grade from dcs.GradeCoefficient a where status=1
union
select id, PartsSalesCategoryName,a.Grade from yxdcs.GradeCoefficient a  where status=1
union
select id, PartsSalesCategoryName,a.Grade from gcdcs.GradeCoefficient a  where status=1
union 
select id, PartsSalesCategoryName,a.Grade from sddcs.GradeCoefficient a  where status=1;
