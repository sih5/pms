delete from keyvalueitem where caption='电商同步状态';
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'EADelivery_SyncStatus', '电商同步状态', 1, '未处理', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'EADelivery_SyncStatus', '电商同步状态', 2, '同步成功', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'EADelivery_SyncStatus', '电商同步状态', 3, '同步失败', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'EADelivery_SyncStatus', '电商同步状态', 4, '终止', 1, 1);