insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1301, 'Dealer|UpdateDistance','更新站间距离', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 1302, 'DealerServiceInfoForBranch|UpdateDistance','更新站间距离', 2);
 INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
