create sequence S_VehiclePartsStockLevel
/

create table VehiclePartsStockLevel  (
   Id                   NUMBER(9)                       not null,
   BrandId              NUMBER(9)                       not null,
   BrandCode            VARCHAR2(50)                    not null,
   BrandName            VARCHAR2(100)                   not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   PartId               NUMBER(9)			not null,
   PartCode             VARCHAR2(50)			not null,
   PartName             VARCHAR2(100)			not null,
   StockMaximum         NUMBER(9),
   StockMinimum         NUMBER(9),
   SafeStock            NUMBER(9),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_VEHICLEPARTSSTOCKLEVEL primary key (Id)
)
/
