create sequence S_Express
/

create sequence S_ExpressToLogistics
/
create table Express  (
   Id                   NUMBER(9)                       not null,
   ExpressCode          VARCHAR2(50)                       not null,
   ExpressName          VARCHAR2(100)                   not null,
   Contacts             VARCHAR2(100),
   ContactsNumber       VARCHAR2(50),
   FixedTelephone       VARCHAR2(50),
   E_Mail             VARCHAR2(100),
   Address            varchar2(200),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   CancelId             NUMBER(9),
   CancelName           VARCHAR2(100),
   CancelTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_EXPRESS primary key (Id)
)
/
create table ExpressToLogistics  (
   Id                   NUMBER(9)                       not null,
   FocufingCoreId       NUMBER(9)                       not null,
   FocufingCoreCode     VARCHAR2(50)                       not null,
   FocufingCoreName     VARCHAR2(100)                   not null,
   LogisticsCompanyId   NUMBER(9)                       not null,
   LogisticsCompanyCode VARCHAR2(50)                       not null,
   LogisticsCompanyName VARCHAR2(100)                   not null,
   ExpressId            NUMBER(9)                       not null,
   ExpressCode          VARCHAR2(50)                     not null,
   ExpressName          VARCHAR2(100)                   not null,
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   CancelId             NUMBER(9),
   CancelName           VARCHAR2(100),
   CancelTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_EXPRESSTOLOGISTICS primary key (Id)
)
/
 alter table PARTSSHIPPINGORDER add ExpressCompanyId NUMBER(9);
alter table PARTSSHIPPINGORDER add ExpressCompanyCode VARCHAR2(100);
