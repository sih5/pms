ALTER TABLE RetainedCustomerVehicleList
ADD  Mileage  NUMBER(9);
ALTER TABLE RetainedCustomerVehicleList
ADD  ChangeTime  DATE;
ALTER TABLE RetainedCustomerVehicleList
ADD  ChangeOperation VARCHAR2(50);
ALTER TABLE RetainedCustomerVehicleList
ADD  ChangeOperationId  NUMBER(9);