ALTER TABLE PartsBranch
ADD  AutoApproveUpLimit   NUMBER(9);

ALTER TABLE PartsBranchHistory
ADD  AutoApproveUpLimit   NUMBER(9);