ALTER TABLE PartsSalesOrder
ADD IsAutoApprove        NUMBER(1);
ALTER TABLE PartsSalesOrder
ADD  AutoApproveComment   VARCHAR2(2000);
ALTER TABLE PartsSalesOrder
ADD AutoApproveTime      DATE;