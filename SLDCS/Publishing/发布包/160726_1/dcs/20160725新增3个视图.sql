--查询工程车配件检验入库单
create or replace view WarehousingInspection as
select pic.Id as PartsInboundCheckBillId,pic.code,pic.warehouseid,pic.warehousecode,pic.WarehouseName,pic.InboundType,(SELECT SUM(picbd.SettlementPrice*picbd.InspectedQuantity) FROM PartsInboundCheckBillDetail picbd WHERE picbd.PartsInboundCheckBillId=pic.id) AS zongjine,pic.OriginalRequirementBillCode,pip.code as PartsInboundPlanCode,
psc.name,pic.BranchName,pic.CounterpartCompanyCode,pic.Counterpartcompanyname,pic.Objid,pic.GPMSPurOrderCode,pic.SettlementStatus,pic.remark,pso.ERPSourceOrderCode,pso.code as PartsSalesOrderCode,
pso.InvoiceType,pic.Creatorname,pic.createtime,pic.ModifierName,pic.modifytime
from gcdcs.PartsInboundCheckBill pic 
inner join gcdcs.PartsInboundPlan pip on pip.id=pic.PartsInboundPlanId
inner join gcdcs.PartsSalesCategory psc on psc.id=pic.PartsSalesCategoryId
inner join gcdcs.PartsSalesOrder pso on pso.id=pic.OriginalRequirementBillId and pic.Originalrequirementbilltype=1;



--查询工程车配件出库单
create or replace view OutboundOrder as
select pb.code,pb.Warehouseid,pb.WarehouseCode,pb.warehousename,pb.OutboundType,( SELECT SUM(pbd.OutboundAmount*pbd.SettlementPrice) FROM gcdcs.PartsOutboundBillDetail pbd WHERE pbd.partsoutboundbillid=pb.id) as zongjine,pb.OriginalRequirementBillCode,pb.PartsSalesOrderTypeName,pp.code as PartsOutboundPlanCode,psc.name,pb.BranchName,pb.CounterpartCompanyCode,
pb.Counterpartcompanyname,cp.type,pb.ReceivingWarehouseCode,pb.ReceivingWarehouseName,cp.ProvinceName,pb.OutboundPackPlanCode,pb.ContractCode,pb.GPMSPurOrderCode,pb.InterfaceRecordId,pb.SettlementStatus,pso.InvoiceType,pso.ERPSourceOrderCode,
pb.Remark,pb.OrderApproveComment,pb.CreatorName,pb.createtime,pb.modifiername,pb.modifytime
from gcdcs.PartsOutboundBill pb 
inner join gcdcs.PartsOutboundPlan pp on pp.id=pb.PartsOutboundPlanId
inner join gcdcs.PartsSalesCategory psc on psc.id=pb.Partssalescategoryid
inner join gcdcs.Company cp on cp.id=pb.counterpartcompanyid
inner join gcdcs.PartsSalesOrder pso on pso.id=pb.originalrequirementbillid and pb.Originalrequirementbilltype=1;




--查询工程车配件发运单
create or replace view SendWaybill as
select psc.Id as PartsSalesCategoryId,pso.WarehouseId,pso.Id as PartsShippingOrderId,psc.name,pso.code,pso.Status,pso.Type,pso.OriginalRequirementBillCode,pso.ShippingCompanyCode,pso.ShippingCompanyName,pso.ReceivingCompanyCode,pso.ReceivingCompanyName,pso.ReceivingWarehouseName,(SELECT SUM(psod.ShippingAmount*psod.settlementprice) FROM gcdcs.PartsShippingOrderDetail psod WHERE psod.PartsShippingOrderId=pso.id) as zongjine,
pso.ReceivingAddress,pso.IsTransportLosses,pso.TransportLossesDisposeStatus,pso.LogisticCompanyCode,pso.LogisticCompanyName,pso.ShippingMethod,pso.InterfaceRecordId,pso.RequestedArrivalDate,pso.ShippingDate,pso.TransportDriverPhone,
pso.DeliveryBillNumber,pso.WarehouseName,pso.TransportMileage,pso.TransportVehiclePlate,pso.TransportDriver,pso.Remark,pso.OrderApproveComment,pso.CreatorName,pso.CreateTime,pso.ModifierName,pso.ModifyTime,pso.ConsigneeName,pso.ConfirmedReceptionTime,
pso.ReceiptConfirmorName,pso.ReceiptConfirmTime
from gcdcs.PartsShippingOrder pso 
inner join gcdcs.PartsSalesCategory psc on psc.id=pso.PartsSalesCategoryId;
