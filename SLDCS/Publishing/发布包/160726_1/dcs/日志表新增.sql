create sequence S_ESBLogNCC
/
create table ESBLogNCC  (
   Id                   NUMBER(9)                       not null,
   InterfaceName        VARCHAR2(50),
   SyncNumberBegin      NUMBER(9),
   SyncNumberEnd        NUMBER(9),
   Message              CLOB,
   TheDate              DATE,
   RNO		        NUMBER(9),
   constraint PK_ESBLOGNCC primary key (Id)
)
/
create sequence S_ESBLogDianShang
/
create table ESBLogDianShang  (
   Id                   NUMBER(9)                       not null,
   InterfaceName        VARCHAR2(50),
   SyncNumberBegin      NUMBER(9),
   SyncNumberEnd        NUMBER(9),
   Message              CLOB,
   TheDate              DATE,
   RNO		        NUMBER(9),
   constraint PK_ESBLOGDIANSHANG primary key (Id)
)
/
create sequence S_ESBLogSAP
/
create table ESBLogSAP  (
   Id                   NUMBER(9)                       not null,
   InterfaceName        VARCHAR2(50),
   SyncNumberBegin      NUMBER(9),
   SyncNumberEnd        NUMBER(9),
   Message              CLOB,
   TheDate              DATE,
   RNO		        NUMBER(9),
   constraint PK_ESBLOGSAP primary key (Id)
)
/
