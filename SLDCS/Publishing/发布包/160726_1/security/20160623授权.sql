insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 3201, 'PartsPurchaseSettleBill|InvoiceSettlement', '发票已审核反结算', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7401, 'RetainedCustomerVehicleList|SetIsGuarantee', '是否保修修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7401, 'RetainedCustomerVehicleList|ExportIsGuarantee', '批导修改是否保修', 2);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
