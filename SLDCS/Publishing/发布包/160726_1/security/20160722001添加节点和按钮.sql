INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5408,5400, 2, 'GCCPartsOutboundBillQuery', 'Released', NULL, '工程车配件出库单查询', '仓库人员查询辖内仓库出库单', 'Client/DCS/Images/Menu/PartsStocking/PartsOutboundBillQuery.png',11, 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5408, 'Common|MergeExport', '合并导出', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5508,5500, 2, 'GCCReceiptQuery', 'Released', NULL, '工程车配件收货单查询', '查询、合并导出工程车配件收货单', 'Client/DCS/Images/Menu/PartsStocking/GCCReceiptQuery.png', 8, 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5508, 'Common|MergeExport', '合并导出', 2);

INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);


INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);