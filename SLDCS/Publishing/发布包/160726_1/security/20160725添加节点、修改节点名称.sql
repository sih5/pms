update page 
set name = '配件仓库高低限管理'，
Icon = 'Client/DCS/Images/Menu/PartsStocking/DealerPartsStock.png'
where name  = '随车行配件仓库高低限管理' and id = 5208;



INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5310, 5300, 2, 'GCCPartsInboundCheckBillQuery', 'Released', NULL, '工程车配件入库检验单查询', '查询工程车配件入库检验单信息', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 10, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
