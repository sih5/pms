CREATE OR REPLACE VIEW OUTBOUNDORDER AS
select pb.StorageCompanyId,pb.PartsOutboundPlanId,pb.Partssalescategoryid as PartsSalesCategoryId,pb.id as PartsOutboundBillId,pb.code,pb.Warehouseid,pb.WarehouseCode,pb.warehousename,pb.OutboundType,( SELECT SUM(pbd.OutboundAmount*pbd.SettlementPrice) FROM gcdcs.PartsOutboundBillDetail pbd WHERE pbd.partsoutboundbillid=pb.id) as zongjine,pb.OriginalRequirementBillCode,pb.PartsSalesOrderTypeName,pp.code as PartsOutboundPlanCode,psc.name,pb.BranchName,pb.CounterpartCompanyCode,
pb.Counterpartcompanyname,cp.type,pb.ReceivingWarehouseCode,pb.ReceivingWarehouseName,cp.ProvinceName,pb.OutboundPackPlanCode,pb.ContractCode,pb.GPMSPurOrderCode,pb.InterfaceRecordId,pb.SettlementStatus,pso.InvoiceType,pso.ERPSourceOrderCode,
pb.Remark,pb.OrderApproveComment,pb.CreatorName,pb.createtime,pb.modifiername,pb.modifytime
from gcdcs.PartsOutboundBill pb
inner join gcdcs.PartsOutboundPlan pp on pp.id=pb.PartsOutboundPlanId
inner join gcdcs.PartsSalesCategory psc on psc.id=pb.Partssalescategoryid
inner join gcdcs.Company cp on cp.id=pb.counterpartcompanyid
inner join gcdcs.PartsSalesOrder pso on pso.id=pb.originalrequirementbillid and pb.Originalrequirementbilltype=1;