
alter table DealerServiceInfo
add SettlementGrade NUMBER(9);

alter table PartsShippingOrder
add ExpectedPlaceDate  DATE DEFAULT SYSDATE not NULL;

alter table VehicleChangeMsg
add CustomerCategory   VARCHAR2(50);

alter table VehicleChangeMsg
add BCustomerCategory  VARCHAR2(50);

alter table VehicleInformation
add CustomerCategory   VARCHAR2(50);


alter table RepairOrder
add BonusPointsStatus  NUMBER(9);

alter table BrandGradeMessage
modify Grade VARCHAR2(50);

alter TABLE DealerServiceInfo
add Grade VARCHAR2(50);

alter table RepairItemAffiServProdLine
add EngineRepairPower  NUMBER(9);

alter table DealerBusinessPermit
add EngineRepairPower  NUMBER(9);

alter table DealerBusinessPermitHistory
add EngineRepairPower  NUMBER(9);

alter table IntegralClaimBill
add RepairOrderId      NUMBER(9);

alter table DealerServiceInfoHistory
add Grade VARCHAR2(50);

alter table DealerServiceInfoHistory
add AGrade  VARCHAR2(50);