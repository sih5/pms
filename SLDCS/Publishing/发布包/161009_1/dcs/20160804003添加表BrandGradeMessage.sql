create sequence S_IntegralClaimBill
/

create sequence S_IntegralClaimBillDetail
/

create sequence S_BrandGradeMessage
/


create table BrandGradeMessage  (
   Id                 NUMBER(9)                       not null,
   BrandId            NUMBER(9)                       not null,
   BrandName          VARCHAR2(50),
   Grade              NUMBER(9)                       not null,
   Status             NUMBER(9)                       not null,
   Remark             VARCHAR2(200),
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   AbandonerId        NUMBER(9),
   AbandonerName      VARCHAR2(100),
   AbandonTime        DATE,
   RowVersion         TIMESTAMP,
   constraint PK_BRANDGRADEMESSAGE primary key (Id)
);
/

/*==============================================================*/
/* Table: IntegralClaimBill                                     */
/*==============================================================*/
create table IntegralClaimBill  (
   Id                 NUMBER(9)                       not null,
   IntegralClaimBillCode VARCHAR2(50)                    not null,
   RepairCode         VARCHAR2(50)                    not null,
   DealerCode         VARCHAR2(50)                    not null,
   DealerName         VARCHAR2(100),
   DealerBusinessCode VARCHAR2(50),
   IntegralClaimBillType NUMBER(9),
   IntegralStatus     NUMBER(9),
   VIP_Code           VARCHAR2(15),
   VIP_Name           VARCHAR2(100),
   CellPhoneNumber    VARCHAR2(30),
   RedPacketsCode     VARCHAR2(50),
   RedPacketsPhone    NUMBER(9),
   BranchId           NUMBER(9),
   BranchCode         VARCHAR2(50),
   BranchName         VARCHAR2(100),
   BrandId            NUMBER(9),
   BrandName          VARCHAR2(50),
   UsedIntegral       NUMBER(9),
   DeductibleLaborCost NUMBER(19,4),
   DeductibleMaterialCost NUMBER(19,4),
   Deductible         NUMBER(19,4),
   OutFeeGrade        NUMBER(19,4),
   SettlementStatus   NUMBER(9),
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   CheckerId          NUMBER(9),
   CheckerName        VARCHAR2(100),
   CheckTime          DATE,
   IDType             VARCHAR2(100),
   IDNumer            VARCHAR2(50),
   Memo               VARCHAR2(100),
   BonusPointsStatus  NUMBER(9),
   RepairOrderId      NUMBER(9),
   BusinessType       VARCHAR2(200),
   constraint PK_INTEGRALCLAIMBILL primary key (Id)
);
/

/*==============================================================*/
/* Table: IntegralClaimBillDetail                               */
/*==============================================================*/
create table IntegralClaimBillDetail  (
   Id                   NUMBER(9)                       not null,
   IntegralClaimItemBillId NUMBER(9)                       not null,
   RightsItem           VARCHAR2(100),
   Times                NUMBER(9),
   IsRights             NUMBER(1),
   constraint PK_INTEGRALCLAIMBILLDETAIL primary key (Id)
);
/