create sequence S_IntegralClaimBillSettle
/

create sequence S_IntegralClaimBillSettleDtl
/

create sequence S_Rights
/

/*==============================================================*/
/* Table: IntegralClaimBillSettle                             */
/*==============================================================*/
create table IntegralClaimBillSettle  (
   Id                 NUMBER(9)                       not null,
   BonusPointsSettleCode VARCHAR2(50)                    not null,
   DealerCode         VARCHAR2(50),
   DealerName         VARCHAR2(100),
   DealerBusinessCode VARCHAR2(50),
   BrandId            NUMBER(9),
   BrandName          VARCHAR2(50),
   ServiceProductLineId NUMBER(9),
   ServiceProductLineName VARCHAR2(50),
   SettleType         NUMBER(9),
   BonusPointsSum     NUMBER(19,4),
   Status             NUMBER(9),
   FreeRescueTimesTotal NUMBER(9),
   DeductibleLaborCost NUMBER(19,4),
   DeductibleMaterialCost NUMBER(19,4),
   Deductible         NUMBER(19,4),
   OutFeeGrade        NUMBER(19,4),
   SettleValidFrom    DATE,
   SettleValidTo      DATE,
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   ApproverId         NUMBER(9),
   ApproverName       VARCHAR2(100),
   ApproveTime        DATE,
   AbandonerId        NUMBER(9),
   AbandonerName      VARCHAR2(100),
   AbandonTime        DATE,
   Memo               VARCHAR2(200),
   RowVersion         TIMESTAMP,
   constraint PK_INTEGRALCLAIMBILLSETTLE primary key (Id)
)
/

/*==============================================================*/
/* Table: IntegralClaimBillSettleDtl                          */
/*==============================================================*/
create table IntegralClaimBillSettleDtl  (
   Id                 NUMBER(9)                       not null,
   IntegralClaimBillSettleId NUMBER(9)                       not null,
   IntegralClaimBillId NUMBER(9),
   IntegralClaimBillCode VARCHAR2(50),
   UseIntegral        NUMBER(9),
   DeductionLaborCost NUMBER(19,4),
   DeductionMaterialCost NUMBER(19,4),
   DeductionFieldSerExp NUMBER(19,4),
   DeductionTotalAmount NUMBER(19,4),
   constraint PK_INTEGRALCLAIMBILLSETTLEDTL primary key (Id)
)
/


/*==============================================================*/
/* Table: Rights                                              */
/*==============================================================*/
create table Rights  (
   Id                 NUMBER(9)                       not null,
   CategoryID         NUMBER(9),
   RightsName         VARCHAR2(100),
   IsUse              NUMBER(1),
   constraint PK_RIGHTS primary key (Id)
)
/