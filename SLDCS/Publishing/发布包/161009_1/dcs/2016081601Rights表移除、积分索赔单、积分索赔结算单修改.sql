DROP SEQUENCE S_Rights;
DROP TABLE Rights;

ALTER TABLE IntegralClaimBill
ADD DealerId NUMBER(9);

ALTER TABLE IntegralClaimBill
ADD MaterialIntegral   NUMBER(9);

ALTER TABLE IntegralClaimBill
ADD LaborHourIntegral  NUMBER(9);

ALTER TABLE IntegralClaimBill
ADD ExpenseAmountIntegral NUMBER(9);

ALTER TABLE IntegralClaimBillSettle
ADD DealerId           NUMBER(9);