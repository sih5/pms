  alter table SEP_SparePartInfo
add(
  CATEGORYCODE          VARCHAR2(100),
  OVERSEASPARTSFIGURE   VARCHAR2(18),
  CATEGORYNAME          VARCHAR2(100),
  CreatorId            NUMBER(9),
  CreatorName          VARCHAR2(100),
  CreateTime           DATE,
  ModifierId           NUMBER(9),
  ModifierName         VARCHAR2(100),
  ModifyTime           DATE,
  AbandonerId          NUMBER(9),
  AbandonerName        VARCHAR2(100),
  AbandonTime          DATE,
  MInPackingAmount     NUMBER(9),
  RowVersion           TIMESTAMP
  );