create sequence S_ManualScheduling
/


/*==============================================================*/
/* Table: ManualScheduling                                    */
/*==============================================================*/
create table ManualScheduling  (
   Id                 NUMBER(9)                       not null,
   InterfaceName      VARCHAR2(100),
   OldSyncStatus      NUMBER(9),
   NewSyncStatus      NUMBER(9),
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   RowVersion         TIMESTAMP,
   constraint PK_MANUALSCHEDULING primary key (Id)
)
/'


ALTER TABLE PartsPurchaseOrder
ADD PurOrderCode       VARCHAR2(50);
ALTER TABLE PartsPurchaseOrderDetail
ADD OverseasPartsFigure VARCHAR2(25);
ALTER TABLE  PartsPurchaseOrderDetail
ADD  OverseasProjectNumber NUMBER(9);
ALTER TABLE PartsPurchasePricing
ADD OverseasPartsFigure  VARCHAR2(25);