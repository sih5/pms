ALTER TABLE PartsSalesOrder
ADD OverseasDemandSheetNo NUMBER(9);
ALTER TABLE PartsSalesOrderDetail
ADD OverseasPartsFigure VARCHAR2(25);
ALTER TABLE PartsSalesOrderDetail
ADD  OverseasProjectNumber NUMBER(9);
ALTER TABLE PartsSalesReturnBill
ADD OldOverseasDemandSheetNo NUMBER(9);
ALTER TABLE PartsSalesReturnBill
ADD  OverseasDemandSheetNo VARCHAR2(25);
ALTER TABLE PartsSalesReturnBillDetail
ADD
(   OverseasPartsFigure VARCHAR2(25),
   OverseasProjectNumber NUMBER(9),
   ReturnContainerNumberLine VARCHAR2(25),
   SettlementPurchaseOrder VARCHAR2(25),
   SettlementPurchaseOrderLine VARCHAR2(25));
ALTER TABLE "RepairOrder"
ADD 
(   "VIP_Code"           VARCHAR2(15),
   "VIP_Name"           VARCHAR2(100),
   "CellPhoneNumber"    VARCHAR2(30),
   "IDType"             VARCHAR2(100),
   "IDNumer"            VARCHAR2(50),
   "MemberRank"         VARCHAR2(100)
);