create sequence S_RedPacketsMsg
/


/*==============================================================*/
/* Table: RedPacketsMsg                                       */
/*==============================================================*/
create table RedPacketsMsg  (
   Id                   NUMBER(9)                       not null,
   Tel                  VARCHAR2(20),
   BrandId              NUMBER(9),
   BrandName            VARCHAR2(50),
   RepairCode           VARCHAR2(50),
   RepairOrderId        NUMBER(9),
   CouponNo             VARCHAR2(100),
   Title                VARCHAR2(255),
   State                NUMBER(9),
   StartTime            DATE,
   EndTime              DATE,
   UsedPrice            NUMBER(19,4),
   LimitPrice           VARCHAR2(100),
   UsedTime             DATE,
   DateTime             DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   constraint PK_REDPACKETSMSG primary key (Id)
)
/

create sequence S_AutoSalesOrderProcess
/

/*==============================================================*/
/* Table: AutoSalesOrderProcess                               */
/*==============================================================*/
create table AutoSalesOrderProcess  (
   Id                 NUMBER(9)                       not null,
   OrderId            NUMBER(9),
   Code               VARCHAR2(50),
   Status             NUMBER(9),
   constraint PK_AUTOSALESORDERPROCESS primary key (Id)
)
/