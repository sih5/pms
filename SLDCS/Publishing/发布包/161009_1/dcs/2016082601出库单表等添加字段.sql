ALTER TABLE PartsOutboundBillDetail
ADD 
(
   ContainerNumber    VARCHAR2(25),
   ContainerType      VARCHAR2(25),
   SonContainer       VARCHAR2(25),
   PackageOrderNumber VARCHAR2(25),
   PackageOrderNumberLine VARCHAR2(25),
   ContainerTotalVolume VARCHAR2(25),
   ContainerTotalWeight VARCHAR2(25),
   ContainerLong      VARCHAR2(25),
   ContainerWide      VARCHAR2(25),
   ContainerHigh      VARCHAR2(25),
   ContainerHeavy     VARCHAR2(25),
   OverseasPartsFigure VARCHAR2(25),
   OverseasProjectNumber NUMBER(9)
);

ALTER TABLE PartsOutboundPlan
ADD 
(
   SPRAS                VARCHAR2(25),
   IsWoodenFumigation VARCHAR2(1),
   IsAdvanceSignature VARCHAR2(1),
   PackingDescription VARCHAR2(25),
   Mark               VARCHAR2(25),
   IsInspectionCamera VARCHAR2(1),
   OtherPackingRequirements VARCHAR2(25),
   IsSpecialInspection VARCHAR2(25)
);

ALTER TABLE PartsOutboundPlanDetail
ADD
(
   OverseasPartsFigure VARCHAR2(25),
   OverseasProjectNumber NUMBER(9)
);

ALTER TABLE IntegralClaimBill
MODIFY RedPacketsPhone    VARCHAR2(30);

ALTER TABLE RepairOrder
ADD
(
   RedPacketsId       NUMBER(9),
   RedPacketsCode     VARCHAR2(50),
   Title              VARCHAR2(255),
   RedPacketsPhone    VARCHAR2(30),
   UsedPrice          NUMBER(19,4)
);