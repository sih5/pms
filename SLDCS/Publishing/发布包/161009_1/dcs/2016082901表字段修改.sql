DROP TABLE IntegralClaimBill;
/
/*==============================================================*/
/* Table: IntegralClaimBill                                   */
/*==============================================================*/
create table IntegralClaimBill  (
   Id                 NUMBER(9)                       not null,
   IntegralClaimBillCode VARCHAR2(50)                    not null,
   RepairOrderId      NUMBER(9)                       not null,
   RepairCode         VARCHAR2(50)                    not null,
   DealerId           NUMBER(9)                       not null,
   DealerCode         VARCHAR2(50)                    not null,
   DealerName         VARCHAR2(100),
   DealerBusinessCode VARCHAR2(50),
   IntegralClaimBillType NUMBER(9),
   IntegralStatus     NUMBER(9),
   SettlementStatus   NUMBER(9),
   BranchId           NUMBER(9),
   BranchCode         VARCHAR2(50),
   BranchName         VARCHAR2(100),
   BrandId            NUMBER(9),
   BrandName          VARCHAR2(50),
   MemberCode         VARCHAR2(15),
   MemberName         VARCHAR2(100),
   MemberPhone        VARCHAR2(30),
   IDType             VARCHAR2(100),
   IDNumer            VARCHAR2(50),
   BusinessType       VARCHAR2(200),
   MemberRank         VARCHAR2(100),
   MemberUsedIntegral NUMBER(9),
   MemberUsedPrice    NUMBER(19,4),
   MemberRightsCost   NUMBER(19,4),
   RedPacketsCode     VARCHAR2(50),
   RedPacketsPhone    VARCHAR2(30),
   RedPacketsUsedPrice NUMBER(19,4),
   DeductionTotalAmount NUMBER(19,4),
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   CheckerId          NUMBER(9),
   CheckerName        VARCHAR2(100),
   CheckTime          DATE,
   Memo               VARCHAR2(100),
   RowVersion         TIMESTAMP,
   constraint PK_INTEGRALCLAIMBILL primary key (Id)
)
/


ALTER TABLE IntegralClaimBillSettle
DROP COLUMN FreeRescueTimesTotal;
 ALTER TABLE IntegralClaimBillSettle
DROP COLUMN  DeductibleLaborCost;
 ALTER TABLE IntegralClaimBillSettle
DROP COLUMN  DeductibleMaterialCost;
ALTER TABLE IntegralClaimBillSettle
DROP  COLUMN  Deductible;
 ALTER TABLE IntegralClaimBillSettle
DROP COLUMN  OutFeeGrade;
/


DROP TABLE IntegralClaimBillSettleDtl;
/
/*==============================================================*/
/* Table: IntegralClaimBillSettleDtl                          */
/*==============================================================*/
create table IntegralClaimBillSettleDtl  (
   Id                 NUMBER(9)                       not null,
   IntegralClaimBillSettleId NUMBER(9)                       not null,
   IntegralClaimBillId NUMBER(9),
   IntegralClaimBillCode VARCHAR2(50),
   UsedPrice          NUMBER(19,4),
   constraint PK_INTEGRALCLAIMBILLSETTLEDTL primary key (Id)
)
/

ALTER TABLE RedPacketsMsg
ADD RowVersion         TIMESTAMP;
/

ALTER TABLE RepairOrder
DROP COLUMN VIP_Code;
 ALTER TABLE RepairOrder
DROP  COLUMN VIP_Name;
ALTER TABLE RepairOrder
DROP COLUMN   CellPhoneNumber ;
 ALTER TABLE RepairOrder
DROP COLUMN  RedPacketsId ;
 ALTER TABLE RepairOrder
DROP COLUMN  RedPacketsCode  ;
 ALTER TABLE RepairOrder
DROP COLUMN  Title ;
ALTER TABLE RepairOrder
DROP COLUMN   RedPacketsPhone;
ALTER TABLE RepairOrder
DROP   COLUMN UsedPrice ;
ALTER TABLE RepairOrder
ADD 
(   MemberCode         VARCHAR2(15),
   MemberName         VARCHAR2(100),
   MemberPhone        VARCHAR2(30));