create sequence S_PartsPurchasePlanDetail_HW
/

create sequence S_PartsPurchasePlan_HW
/


/*==============================================================*/
/* Table: PartsPurchasePlanDetail_HW                          */
/*==============================================================*/
create table PartsPurchasePlanDetail_HW  (
   Id                 NUMBER(9)                       not null,
   PartsPurchasePlanId NUMBER(9),
   PartId             NUMBER(9),
   PartCode           VARCHAR2(50),
   PartName           VARCHAR2(100),
   GPMSPartCode       VARCHAR2(25),
   PartsPurchasePlanQty NUMBER(9),
   UntFulfilledQty    NUMBER(9),
   TransferQty        NUMBER(9),
   PurchaseQty        NUMBER(9),
   PurchaseConfimQty  NUMBER(9),
   UnitPrice          NUMBER(19,4),
   MeasureUnit        VARCHAR2(50),
   GPMSRowNum         VARCHAR2(50),
   RequestedDeliveryTime DATE,
   Remark             VARCHAR2(200),
   constraint PK_PARTSPURCHASEPLANDETAIL_HW primary key (Id)
)
/

/*==============================================================*/
/* Table: PartsPurchasePlan_HW                                */
/*==============================================================*/
create table PartsPurchasePlan_HW  (
   Id                 NUMBER(9)                       not null,
   Code               VARCHAR2(50),
   WarehouseId        NUMBER(9),
   WarehouseCode      VARCHAR2(50),
   WarehouseName      VARCHAR2(100),
   PartsSalesCategoryId NUMBER(9),
   PartsSalesCategoryName VARCHAR2(50),
   PartsSupplierId    NUMBER(9),
   PartsSupplierCode  VARCHAR2(50),
   PartsSupplierName  VARCHAR2(100),
   BussinessCode      VARCHAR2(50),
   PurchasePlanType   NUMBER(9),
   TotalAmount        NUMBER(19,4),
   RequestedDeliveryTime DATE,
   Status             NUMBER(9),
   StopReason         VARCHAR2(200),
   FaultReason        VARCHAR2(500),
   Remark             VARCHAR2(200),
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   StoporId           NUMBER(9),
   StoporName         VARCHAR2(100),
   StopTime           DATE,
   constraint PK_PARTSPURCHASEPLAN_HW primary key (Id)
)
/
