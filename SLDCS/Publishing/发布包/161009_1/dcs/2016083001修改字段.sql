ALTER TABLE PartsOutboundPlanDetail
ADD
(
   Vbeln              VARCHAR2(10),
   Posnr              NUMBER(9),
   Ebeln              VARCHAR2(10),
   Ebelp              NUMBER(9),
   Zpitem             NUMBER(9),
   Pnumber            VARCHAR2(20)
   );
ALTER TABLE PartsOutboundPlan
ADD    ZPNUMBER             VARCHAR2(20);