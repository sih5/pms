ALTER TABLE  PartsSalesOrder
ADD IsBarter NUMBER(1) DEFAULT 0  not NULL;
ALTER TABLE PartsSalesReturnBill
ADD IsBarter NUMBER(1) DEFAULT 0  not NULL;

ALTER TABLE PartsPurchasePlanDetail_HW
ADD    FaultReason        VARCHAR2(500);

ALTER TABLE PartsPurchasePlan_HW
DROP COLUMN FaultReason;