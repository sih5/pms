create sequence S_BonusPointsSummary
/

create sequence S_BonusPointsSummaryList
/

create sequence S_BonusPointsOrder
/

create sequence S_BonusPointsOrderList
/

/*==============================================================*/
/* Table: BonusPointsSummary                                  */
/*==============================================================*/
create table BonusPointsSummary  (
   Id                 NUMBER(9)                       not null,
   BonusPointsSummaryCode VARCHAR2(50)                    not null,
   BrandId            NUMBER(9),
   BrandName          VARCHAR2(50),
   Status             NUMBER(9),
   SummaryBonusPoints NUMBER(9),
   SummaryAmount      NUMBER(9),
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(50),
   CreateTime         DATE,
   ApproverId         NUMBER(9),
   ApproverName       VARCHAR2(50),
   ApproveTime        DATE,
   AbandonerId        NUMBER(9),
   AbandonerName      VARCHAR2(50),
   AbandonTime        DATE,
   constraint PK_BONUSPOINTSSUMMARY primary key (Id)
)
/

/*==============================================================*/
/* Table: BonusPointsSummaryList                              */
/*==============================================================*/
create table BonusPointsSummaryList  (
   Id                 NUMBER(9)                       not null,
   BonusPointsSummaryId NUMBER(9)                       not null,
   BonusPointsId      NUMBER(9),
   PlatForm_Code      VARCHAR2(50),
   ServiceApplyCode   VARCHAR2(50),
   BonusPoints        NUMBER(9),
   BonusPointsAmount  NUMBER(9),
   constraint PK_BONUSPOINTSSUMMARYLIST primary key (Id)
)
/

/*==============================================================*/
/* Table: BonusPointsOrder                                    */
/*==============================================================*/
create table BonusPointsOrder  (
   Id                 NUMBER(9)                       not null,
   BonusPointsOrderCode NUMBER(9)                       not null,
   BrandId            NUMBER(9),
   BrandName          VARCHAR2(50),
   CorporationId      NUMBER(9),
   CorporationCode    VARCHAR2(50),
   CorporationName    VARCHAR2(50),
   BonusPointsAmount  NUMBER(9),
   BonusPoints        NUMBER(9),
   PlatForm_Code      VARCHAR2(50),
   ServiceApplyCode   VARCHAR2(50),
   Type               NUMBER(9),
   SettlementStatus   NUMBER(9),
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(50),
   CreateTime         DATE,
   constraint PK_BONUSPOINTSORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: BonusPointsOrderList                                */
/*==============================================================*/
create table BonusPointsOrderList  (
   Id                 NUMBER(9)                       not null,
   BonusPointsOrderId NUMBER(9)                       not null,
   SparePartId        NUMBER(9),
   SparePartCode      VARCHAR2(50),
   SparePartName      VARCHAR2(50),
   Quantity           NUMBER(9),
   MeasureUnit        VARCHAR2(50),
   Amount             NUMBER(9),
   constraint PK_BONUSPOINTSORDERLIST primary key (Id)
)
/
