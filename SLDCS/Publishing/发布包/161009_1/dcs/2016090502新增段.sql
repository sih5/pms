ALTER TABLE PartsSupplier
ADD OverseasPartsFigure VARCHAR2(25);
ALTER TABLE PartsSupplierRelation
ADD OverseasPartsFigure VARCHAR2(25);
ALTER TABLE PartsSupplierRelationHistory
ADD OverseasPartsFigure VARCHAR2(25);