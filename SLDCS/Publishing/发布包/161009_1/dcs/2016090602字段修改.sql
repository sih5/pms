ALTER TABLE PartsInboundCheckBill
ADD (
PurOrderCode       VARCHAR2(50),
ReturnContainerNumber VARCHAR2(25));

ALTER TABLE PartsInboundCheckBillDetail
ADD (
   OverseasPartsFigure VARCHAR2(25),
   OverseasProjectNumber NUMBER(9));
   
   ALTER TABLE PartsInboundPlan
   ADD ReturnContainerNumber VARCHAR2(25);
   
   ALTER TABLE PartsInboundPlanDetail
   ADD(
      OverseasPartsFigure VARCHAR2(25),
   OverseasProjectNumber NUMBER(9));
   
   ALTER TABLE PartsInventoryDetail
   ADD OverseasPartsFigure VARCHAR2(25);
   
   ALTER TABLE PartsTransferOrder
   ADD PurOrderCode       VARCHAR2(50);
   
   ALTER TABLE PartsTransferOrderDetail
   ADD(
      OverseasPartsFigure VARCHAR2(25),
   OverseasProjectNumber NUMBER(9));
   
   ALTER TABLE IntegralClaimBillDetail
   DROP COLUMN Times;
   
   ALTER TABLE IntegralClaimBillDetail
   ADD(
      BenefiType         VARCHAR2(30),
   BenefiUom          NUMBER(9),
   DiscountNum        NUMBER(19,4));