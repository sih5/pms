ALTER TABLE IntegralClaimBill
ADD(
   DeductionLaborCost NUMBER(19,4),
   CostDiscount       NUMBER(19,4),
   Discount           NUMBER(19,4));

ALTER TABLE BonusPointsOrder
MODIFY BonusPointsOrderCode VARCHAR2(50);

ALTER TABLE BonusPointsSummary
ADD(
   CutTime            DATE,
   TaxRate            NUMBER(15,6),
   Tax                NUMBER(19,4));
   
ALTER TABLE BonusPointsSummaryList
ADD(
BonusPointsOrderCode VARCHAR2(50),
   SouceType          NUMBER(9),
   CreateTime         DATE);