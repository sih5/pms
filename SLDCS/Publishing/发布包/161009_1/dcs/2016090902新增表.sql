create sequence S_IntegralSettleDictate
/


/*==============================================================*/
/* Table: IntegralSettleDictate                               */
/*==============================================================*/
create table IntegralSettleDictate  (
   Id                 NUMBER(9)                       not null,
   BranchId           NUMBER(9),
   BranchCode         VARCHAR2(50),
   BranchName         VARCHAR2(200),
   BrandId            NUMBER(9),
   Brand              VARCHAR2(50),
   Status             NUMBER(9),
   SettlementStartTime DATE,
   SettlementEndTime  DATE,
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ExecutionTime      DATE,
   AbandonerId        NUMBER(9),
   AbandonerName      VARCHAR2(100),
   AbandonerTime      DATE,
   ExecutionResult    VARCHAR2(200),
   constraint PK_INTEGRALSETTLEDICTATE primary key (Id)
)
/