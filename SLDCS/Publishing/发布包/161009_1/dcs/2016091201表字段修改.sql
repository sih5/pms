ALTER TABLE BonusPointsSummaryList
DROP COLUMN CreateTime;

ALTER TABLE BonusPointsSummaryList
ADD BonusPointsTime    DATE;

ALTER TABLE PartsSalesSettlementDetail
ADD(
   CatisClassifyId    NUMBER(9),
   CatisClassifyCode  VARCHAR2(50),
   CatisClassifyName  VARCHAR2(50)
);