ALTER TABLE PartsPurchasePlanDetail_HW
ADD OverseasSuplierCode VARCHAR2(50);

ALTER TABLE PartsBranch
ADD ExchangeIdentification VARCHAR2(50);

ALTER TABLE SparePart
ADD ExchangeIdentification VARCHAR2(50);

ALTER TABLE SparePartHistory
ADD ExchangeIdentification VARCHAR2(50);