ALTER TABLE PartsPurchasePlanDetail_HW
ADD (
   PartsSupplierId    NUMBER(9),
   PartsSupplierCode  VARCHAR2(50),
   PartsSupplierName  VARCHAR2(100)
);

ALTER TABLE BonusPointsSummary
MODIFY SummaryAmount      NUMBER(19,4);

ALTER TABLE BonusPointsSummaryList
MODIFY BonusPointsAmount  NUMBER(19,4);

ALTER TABLE BonusPointsOrder
MODIFY BonusPointsAmount  NUMBER(19,4);

ALTER TABLE BonusPointsOrderList
MODIFY Amount             NUMBER(19,4);

ALTER TABLE IntegralClaimBill
MODIFY MemberUsedIntegral NUMBER(19,4);