create sequence S_RepairIntegral
/

create sequence S_RepairIntegralRightsDetail
/

create table RepairIntegral  (
   Id                 NUMBER(9)                       not null,
   RepairOrderId      NUMBER(9),
   MemberCode         VARCHAR2(15),
   MemberName         VARCHAR2(100),
   IDType             VARCHAR2(100),
   IDNumer            VARCHAR2(50),
   MemberRank         VARCHAR2(100),
   MemberPhone        VARCHAR2(30),
   MemberValue        NUMBER(19,4),
   MaxMemberValue     NUMBER(9),
   MemberUsedIntegral NUMBER(19,4),
   LaborCost          NUMBER(19,4),
   MaterialCost       NUMBER(19,4),
   TrimCost           NUMBER(19,4),
   OtherCost          NUMBER(19,4),
   OutRange           NUMBER(9),
   FieldTotalAmount   NUMBER(9),
   TotalAmount        NUMBER(19,4),
   Discount           NUMBER(19,4),
   CostDiscount       NUMBER(19,4),
   DeductionCost      NUMBER(19,4),
   MemberRightsCost   NUMBER(19,4),
   DeductionTotalAmount NUMBER(19,4),
   constraint PK_REPAIRINTEGRAL primary key (Id)
)
/

create table RepairIntegralRightsDetail  (
   Id                 NUMBER(9)                       not null,
   RepairIntegralId   NUMBER(9)                       not null,
   RightsItem         VARCHAR2(100),
   RightsCode         VARCHAR2(100),
   IsRights           NUMBER(1),
   BenefiType         VARCHAR2(30),
   BenefiUom          NUMBER(9),
   DiscountNum        NUMBER(19,4),
   Time               NUMBER(9),
   constraint PK_REPAIRINTEGRALRIGHTSDETAIL primary key (Id)
)
/