ALTER TABLE RepairIntegral
MODIFY FieldTotalAmount   NUMBER(19,4);

ALTER TABLE RepairOrder
ADD(   MemberHandStatus   NUMBER(9),
   FrozenStatus       NUMBER(9));

ALTER TABLE PartsSalesOrder
MODIFY OverseasDemandSheetNo VARCHAR2(50);
ALTER TABLE PartsSalesReturnBill
MODIFY OldOverseasDemandSheetNo VARCHAR2(50);