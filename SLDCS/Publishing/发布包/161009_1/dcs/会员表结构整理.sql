-- Create table
drop table INTEGRALCLAIMBILL;
create table INTEGRALCLAIMBILL
(
  ID                    NUMBER(9) not null,
  INTEGRALCLAIMBILLCODE VARCHAR2(50) not null,
  REPAIRORDERID         NUMBER(9) not null,
  REPAIRCODE            VARCHAR2(50) not null,
  DEALERID              NUMBER(9) not null,
  DEALERCODE            VARCHAR2(50) not null,
  DEALERNAME            VARCHAR2(100),
  DEALERBUSINESSCODE    VARCHAR2(50),
  INTEGRALCLAIMBILLTYPE NUMBER(9),
  INTEGRALSTATUS        NUMBER(9),
  SETTLEMENTSTATUS      NUMBER(9),
  BRANCHID              NUMBER(9),
  BRANCHCODE            VARCHAR2(50),
  BRANCHNAME            VARCHAR2(100),
  BRANDID               NUMBER(9),
  BRANDNAME             VARCHAR2(50),
  MEMBERCODE            VARCHAR2(15),
  MEMBERNAME            VARCHAR2(100),
  MEMBERPHONE           VARCHAR2(30),
  IDTYPE                VARCHAR2(100),
  IDNUMER               VARCHAR2(50),
  BUSINESSTYPE          VARCHAR2(200),
  MEMBERRANK            VARCHAR2(100),
  MEMBERUSEDINTEGRAL    NUMBER(19,4),
  MEMBERUSEDPRICE       NUMBER(19,4),
  MEMBERRIGHTSCOST      NUMBER(19,4),
  REDPACKETSCODE        VARCHAR2(50),
  REDPACKETSPHONE       VARCHAR2(30),
  REDPACKETSUSEDPRICE   NUMBER(19,4),
  DEDUCTIONTOTALAMOUNT  NUMBER(19,4),
  CREATORID             NUMBER(9),
  CREATORNAME           VARCHAR2(100),
  CREATETIME            DATE,
  MODIFIERID            NUMBER(9),
  MODIFIERNAME          VARCHAR2(100),
  MODIFYTIME            DATE,
  CHECKERID             NUMBER(9),
  CHECKERNAME           VARCHAR2(100),
  CHECKTIME             DATE,
  MEMO                  VARCHAR2(100),
  ROWVERSION            TIMESTAMP(6),
  DEDUCTIONLABORCOST    NUMBER(19,4),
  COSTDISCOUNT          NUMBER(19,4),
  DISCOUNT              NUMBER(19,4),
  EXISTINGINTEGRAL      NUMBER(19,4),
  APPROVALCOMMENT       VARCHAR2(1000)
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table INTEGRALCLAIMBILL
  add constraint PK_INTEGRALCLAIMBILL primary key (ID) ;
  -- Create table
  drop table INTEGRALCLAIMBILLDETAIL;
create table INTEGRALCLAIMBILLDETAIL
(
  ID                      NUMBER(9) not null,
  INTEGRALCLAIMITEMBILLID NUMBER(9) not null,
  RIGHTSITEM              VARCHAR2(100),
  ISRIGHTS                NUMBER(1),
  RIGHTSCODE              VARCHAR2(100),
  BENEFITYPE              VARCHAR2(30),
  BENEFIUOM               NUMBER(9),
  DISCOUNTNUM             NUMBER(19,4),
  TIME                    NUMBER(9)
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table INTEGRALCLAIMBILLDETAIL
  add constraint PK_INTEGRALCLAIMBILLDETAIL primary key (ID);
  
  -- Create table
  drop table INTEGRALCLAIMBILLSETTLE;
create table INTEGRALCLAIMBILLSETTLE
(
  ID                     NUMBER(9) not null,
  BONUSPOINTSSETTLECODE  VARCHAR2(50) not null,
  DEALERCODE             VARCHAR2(50),
  DEALERNAME             VARCHAR2(100),
  DEALERBUSINESSCODE     VARCHAR2(50),
  BRANDID                NUMBER(9),
  BRANDNAME              VARCHAR2(50),
  SERVICEPRODUCTLINEID   NUMBER(9),
  SERVICEPRODUCTLINENAME VARCHAR2(50),
  SETTLETYPE             NUMBER(9),
  BONUSPOINTSSUM         NUMBER(19,4),
  STATUS                 NUMBER(9),
  SETTLEVALIDFROM        DATE,
  SETTLEVALIDTO          DATE,
  CREATORID              NUMBER(9),
  CREATORNAME            VARCHAR2(100),
  CREATETIME             DATE,
  MODIFIERID             NUMBER(9),
  MODIFIERNAME           VARCHAR2(100),
  MODIFYTIME             DATE,
  APPROVERID             NUMBER(9),
  APPROVERNAME           VARCHAR2(100),
  APPROVETIME            DATE,
  ABANDONERID            NUMBER(9),
  ABANDONERNAME          VARCHAR2(100),
  ABANDONTIME            DATE,
  MEMO                   VARCHAR2(200),
  ROWVERSION             TIMESTAMP(6),
  DEALERID               NUMBER(9)
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table INTEGRALCLAIMBILLSETTLE
  add constraint PK_INTEGRALCLAIMBILLSETTLE primary key (ID);
-- Create table
drop table INTEGRALCLAIMBILLSETTLEDTL;
create table INTEGRALCLAIMBILLSETTLEDTL
(
  ID                        NUMBER(9) not null,
  INTEGRALCLAIMBILLSETTLEID NUMBER(9) not null,
  INTEGRALCLAIMBILLID       NUMBER(9),
  INTEGRALCLAIMBILLCODE     VARCHAR2(50),
  USEDPRICE                 NUMBER(19,4)
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table INTEGRALCLAIMBILLSETTLEDTL
  add constraint PK_INTEGRALCLAIMBILLSETTLEDTL primary key (ID);

drop table INTEGRALSETTLEDICTATE;
-- Create table
create table INTEGRALSETTLEDICTATE
(
  ID                  NUMBER(9) not null,
  BRANCHID            NUMBER(9),
  BRANCHCODE          VARCHAR2(50),
  BRANCHNAME          VARCHAR2(200),
  BRANDID             NUMBER(9),
  BRAND               VARCHAR2(50),
  STATUS              NUMBER(9),
  SETTLEMENTSTARTTIME DATE,
  SETTLEMENTENDTIME   DATE,
  CREATORID           NUMBER(9),
  CREATORNAME         VARCHAR2(100),
  CREATETIME          DATE,
  EXECUTIONTIME       DATE,
  ABANDONERID         NUMBER(9),
  ABANDONERNAME       VARCHAR2(100),
  ABANDONERTIME       DATE,
  EXECUTIONRESULT     VARCHAR2(200)
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table INTEGRALSETTLEDICTATE
  add constraint PK_INTEGRALSETTLEDICTATE primary key (ID);



drop table REDPACKETSMSG;
-- Create table
create table REDPACKETSMSG
(
  ID            NUMBER(9) not null,
  TEL           VARCHAR2(20),
  BRANDID       NUMBER(9),
  BRANDNAME     VARCHAR2(50),
  REPAIRCODE    VARCHAR2(50),
  REPAIRORDERID NUMBER(9),
  COUPONNO      VARCHAR2(100),
  TITLE         VARCHAR2(255),
  STATE         NUMBER(9),
  STARTTIME     DATE,
  ENDTIME       DATE,
  USEDPRICE     NUMBER(19,4),
  LIMITPRICE    VARCHAR2(100),
  USEDTIME      DATE,
  DATETIME      DATE,
  CREATORID     NUMBER(9),
  CREATORNAME   VARCHAR2(100),
  CREATETIME    DATE,
  MODIFIERID    NUMBER(9),
  MODIFIERNAME  VARCHAR2(100),
  MODIFYTIME    DATE,
  ROWVERSION    TIMESTAMP(6)
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table REDPACKETSMSG
  add constraint PK_REDPACKETSMSG primary key (ID);
  
  drop table BONUSPOINTSORDER;
  -- Create table
create table BONUSPOINTSORDER
(
  ID                   NUMBER(9) not null,
  BONUSPOINTSORDERCODE VARCHAR2(50) not null,
  BRANDID              NUMBER(9),
  BRANDNAME            VARCHAR2(50),
  CORPORATIONID        NUMBER(9),
  CORPORATIONCODE      VARCHAR2(50),
  CORPORATIONNAME      VARCHAR2(50),
  BONUSPOINTSAMOUNT    NUMBER(19,4),
  BONUSPOINTS          NUMBER(9),
  PLATFORM_CODE        VARCHAR2(50),
  SERVICEAPPLYCODE     VARCHAR2(50),
  TYPE                 NUMBER(9),
  SETTLEMENTSTATUS     NUMBER(9),
  CREATORID            NUMBER(9),
  CREATORNAME          VARCHAR2(50),
  CREATETIME           DATE
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table BONUSPOINTSORDER
  add constraint PK_BONUSPOINTSORDER primary key (ID);
-- Create table
drop table BONUSPOINTSORDERLIST;
create table BONUSPOINTSORDERLIST
(
  ID                 NUMBER(9) not null,
  BONUSPOINTSORDERID NUMBER(9) not null,
  SPAREPARTID        NUMBER(9),
  SPAREPARTCODE      VARCHAR2(50),
  SPAREPARTNAME      VARCHAR2(50),
  QUANTITY           NUMBER(9),
  MEASUREUNIT        VARCHAR2(50),
  AMOUNT             NUMBER(19,4)
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table BONUSPOINTSORDERLIST
  add constraint PK_BONUSPOINTSORDERLIST primary key (ID);

drop table BONUSPOINTSSUMMARY;
create table BONUSPOINTSSUMMARY
(
  ID                     NUMBER(9) not null,
  BONUSPOINTSSUMMARYCODE VARCHAR2(50) not null,
  BRANDID                NUMBER(9),
  BRANDNAME              VARCHAR2(50),
  STATUS                 NUMBER(9),
  SUMMARYBONUSPOINTS     NUMBER(9),
  SUMMARYAMOUNT          NUMBER(19,4),
  CREATORID              NUMBER(9),
  CREATORNAME            VARCHAR2(50),
  CREATETIME             DATE,
  APPROVERID             NUMBER(9),
  APPROVERNAME           VARCHAR2(50),
  APPROVETIME            DATE,
  ABANDONERID            NUMBER(9),
  ABANDONERNAME          VARCHAR2(50),
  ABANDONTIME            DATE,
  CUTTIME                DATE,
  TAXRATE                NUMBER(15,6),
  TAX                    NUMBER(19,4)
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table BONUSPOINTSSUMMARY
  add constraint PK_BONUSPOINTSSUMMARY primary key (ID);
  
  
  drop table BONUSPOINTSSUMMARYLIST ;
  -- Create table
create table BONUSPOINTSSUMMARYLIST
(
  ID                   NUMBER(9) not null,
  BONUSPOINTSSUMMARYID NUMBER(9) not null,
  BONUSPOINTSID        NUMBER(9),
  PLATFORM_CODE        VARCHAR2(50),
  SERVICEAPPLYCODE     VARCHAR2(50),
  BONUSPOINTS          NUMBER(9),
  BONUSPOINTSAMOUNT    NUMBER(19,4),
  BONUSPOINTSORDERCODE VARCHAR2(50),
  SOUCETYPE            NUMBER(9),
  BONUSPOINTSTIME      DATE
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table BONUSPOINTSSUMMARYLIST
  add constraint PK_BONUSPOINTSSUMMARYLIST primary key (ID);
