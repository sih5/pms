begin
  sys.dbms_scheduler.create_job(job_name            => 'DCS.自动审核销售订单',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoSubmitSalesOrder',
                                start_date          => to_date('09-10-2016 00:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Minutely;Interval=5',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
create or replace procedure AutoSubmitSalesOrder as
begin
 declare
    cursor Autoadd is(
     select * from AutoSalesOrderProcess where status=0 );
begin
    for S_Autoadd in Autoadd loop
      begin
          autochecknoecommerceorder(S_Autoadd.orderid);
          update AutoSalesOrderProcess set status=1 where id=S_Autoadd.id;
          commit;
        end;
        end loop;
        end;
end AutoSubmitSalesOrder;
/
-- Create table
create table AUTOSALESORDERPROCESS
(
  ID      NUMBER(9) not null,
  ORDERID NUMBER(9),
  CODE    VARCHAR2(50),
  STATUS  NUMBER(9)
);
/