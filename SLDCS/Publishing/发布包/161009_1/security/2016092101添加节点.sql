
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 8403, 'RepairOrderForWarranty|PrintBillMember', '打印结算单(会员)', 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (14208, 14200, 2, 'BonusPointsOrderReport', 'Released', '', '积分订单明细报表', '', '', '汇总积分订单明细', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 31, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);



-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
