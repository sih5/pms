begin                
      sys.dbms_scheduler.create_job( 
                                job_name => 'SapPaymentInfo_JOB',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'Prc_SapPaymentInfo',
                                start_date          => sysdate,
                                repeat_interval     => 'FREQ=MONTHLY; BYMONTHDAY=1;BYHOUR=0;',
                                enabled             => true,
                                auto_drop           => false,
                               comments            => '每月1号自动清理三月前的SAP收付款接口日志');
end;
