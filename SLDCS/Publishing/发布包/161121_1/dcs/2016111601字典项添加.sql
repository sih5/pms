update keyvalueitem set value='电子普通发票' where name='ERPInvoiceInformation_Type' and key='2';

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'Invoice_Type', '开票类型', 0, '为空', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'ERPInvoiceInformation_Type', '电商发票类型', 3, '不开发票', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'EnterpriseType', '销售订单企业类型', 1, '企业', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'EnterpriseType', '销售订单企业类型', 2, '机关事业单位', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'EnterpriseType', '销售订单企业类型', 3, '个人', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'EnterpriseType', '销售订单企业类型', 4, '其他', 1, 1);

delete KeyValueItem where name='InvoiceInformation_Type';
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InvoiceInformation_Type', '发票类型', 0, '增值税发票', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InvoiceInformation_Type', '发票类型', 1, '普通发票', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InvoiceInformation_Type', '发票类型', 2, '电子普通发票', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InvoiceInformation_Type', '发票类型', 3, '不开发票', 1, 1);
