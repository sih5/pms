alter table Product
modify VehicleSize        VARCHAR2(50);

alter table ServiceProdLineProduct
add(
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE);

alter table PartsSalesOrder
modify BankAccount        VARCHAR2(200);

alter table PartsSalesOrder
add(
   EnterpriseType     NUMBER(9),
   ReceivingMailbox   VARCHAR2(100)
);

alter table InvoiceInformation
add InvoiceDownloadLink VARCHAR2(200);