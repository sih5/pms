INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13804,13800, 2, 'RepairWorkOrderMDReport', 'Released', NULL, '互动中心派工单汇总表', '统计互动中心派工单处理时长情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 21, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13805,13800, 2, 'RepairWorkOrderMDReportDtl', 'Released', NULL, '互动中心派工单明细表', '统计互动中心派工单处理时长情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 22, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);