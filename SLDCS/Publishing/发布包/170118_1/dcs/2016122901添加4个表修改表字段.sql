create sequence S_InteSettleBillInvoiceLink
/
create sequence S_IntegralClaimInvoiceInfo
/
create sequence S_OutofWarrantyPayment
/
create sequence S_TransactionInterfaceLog
/

/*==============================================================*/
/* Table: InteSettleBillInvoiceLink                           */
/*==============================================================*/
create table InteSettleBillInvoiceLink  (
   Id                 NUMBER(9)                       not null,
   IntegralClaimInvoiceId NUMBER(9)                       not null,
   IntegralClaimSettlementBillId NUMBER(9)                       not null,
   constraint PK_INTESETTLEBILLINVOICELINK primary key (Id)
)
/

/*==============================================================*/
/* Table: IntegralClaimInvoiceInfo                            */
/*==============================================================*/
create table IntegralClaimInvoiceInfo  (
   Id                 NUMBER(9)                       not null,
   Code               VARCHAR2(50)                    not null,
   InvoiceCode        VARCHAR2(50)                    not null,
   InvoiceNumber      VARCHAR2(50)                    not null,
   ClaimSettlementBillCode VARCHAR2(500)                   not null,
   BranchId           NUMBER(9)                       not null,
   BranchCode         VARCHAR2(50)                    not null,
   BranchName         VARCHAR2(100)                   not null,
   DealerId           NUMBER(9)                       not null,
   DealerCode         VARCHAR2(50)                    not null,
   DealerName         VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   InvoiceFee         NUMBER(19,4)                    not null,
   TaxRate            NUMBER(15,6)                    not null,
   InvoiceTaxFee      NUMBER(19,4)                    not null,
   Status             NUMBER(9)                       not null,
   InvoiceDate        DATE                            not null,
   Remark             VARCHAR2(200),
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   AbandonerId        NUMBER(9),
   AbandonerName      VARCHAR2(100),
   AbandonTime        DATE,
   InitialApproverId  NUMBER(9),
   InitialApproverName VARCHAR2(100),
   InitialApproverTime DATE,
   InitialApproverComment VARCHAR2(200),
   ApproverId         NUMBER(9),
   ApproverName       VARCHAR2(100),
   ApproveTime        DATE,
   ApproveComment     VARCHAR2(200),
   RejectId           NUMBER(9),
   RejectName         VARCHAR2(100),
   RejectTime         DATE,
   RejectComment      VARCHAR2(200),
   ApproveCommentHistory VARCHAR2(2000),
   RowVersion         TIMESTAMP,
   SettleType         NUMBER(9),
   constraint PK_INTEGRALCLAIMINVOICEINFO primary key (Id)
)
/

/*==============================================================*/
/* Table: OutofWarrantyPayment                                */
/*==============================================================*/
create table OutofWarrantyPayment  (
   Id                 NUMBER(9)                       not null,
   Code               VARCHAR2(50)                    not null,
   Status             NUMBER(9)                       not null,
   SettlementStatus   NUMBER(9)                       not null,
   DebitOrReplenish   NUMBER(9)                       not null,
   TransactionCategory NUMBER(9)                       not null,
   BranchId           NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   ProductLineType    NUMBER(9),
   ServiceProductLineId NUMBER(9),
   DealerId           NUMBER(9)                       not null,
   DealerCode         VARCHAR2(20)                    not null,
   DealerName         VARCHAR2(100)                   not null,
   SourceType         NUMBER(9),
   SourceId           NUMBER(9),
   SourceCode         VARCHAR2(50),
   RepairClaimBillCode VARCHAR2(50),
   DealerContactPerson VARCHAR2(50),
   DealerPhoneNumber  VARCHAR2(50),
   TransactionAmount  NUMBER(19,4),
   TransactionReason  VARCHAR2(200),
   IfClaimToResponsible NUMBER(1)                       not null,
   GradeCoefficientId NUMBER(9),
   ResponsibleUnitId  NUMBER(9),
   Remark             VARCHAR2(200),
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   AbandonerId        NUMBER(9),
   AbandonerName      VARCHAR2(100),
   AbandonTime        DATE,
   ApproverId         NUMBER(9),
   ApproverName       VARCHAR2(100),
   ApproveTime        DATE,
   RowVersion         TIMESTAMP,
   constraint PK_OUTOFWARRANTYPAYMENT primary key (Id)
)
/

/*==============================================================*/
/* Table: TransactionInterfaceLog                             */
/*==============================================================*/
create table TransactionInterfaceLog  (
   Id                 NUMBER(9)                       not null,
   RepairOrderID      NUMBER(9),
   RepairWorkOrderId  VARCHAR2(50),
   MemberChannel      NUMBER(9),
   MemberNumber       VARCHAR2(50),
   CellPhoneNumber    VARCHAR2(50),
   Points             NUMBER(9),
   TIME                 DATE,
   TransactionSubType VARCHAR2(30),
   BenefitsName       VARCHAR2(100),
   VIN                  VARCHAR2(50),
   Code               VARCHAR2(50),
   SubjectCode        VARCHAR2(100),
   SubjuctName        VARCHAR2(100),
   BUSINESSUNIT         VARCHAR2(100),
   RepairWorkOrder    VARCHAR2(50),
   OrderType          NUMBER(9),
   OrderCategory     NUMBER(9),
   Warranty_Status    NUMBER(9),
   BrandName          VARCHAR2(100),
   MaterialCost       NUMBER(19,4),
   LaborCost          NUMBER(19,4),
   FieldServiceCharge NUMBER(19,4),
   TotalAmount        NUMBER(19,4),
   PrMaterialCost     NUMBER(19,4),
   PrLaborCost        NUMBER(19,4),
   PrFieldServiceCharge NUMBER(19,4),
   PrTotalAmount      NUMBER(19,4),
   ActualCost         NUMBER(19,4),
   OtherCost          NUMBER(19,4),
   Accessories        NUMBER(19,4),
   Accrued            NUMBER(19,4),
   ManHour            NUMBER(19,4),
   CostDiscount       NUMBER(19,4),
   SyncStatus         NUMBER(9),
   SyncMessage        VARCHAR2(100),
   SyncTime           DATE,
   constraint PK_TRANSACTIONINTERFACELOG primary key (Id)
)
/

alter table IntegralClaimBillSettle
add (
    BranchCode         VARCHAR2(50),
    PartsSalesCategoryId NUMBER(9),
    IfInvoiced         NUMBER(1),
    TaxRate            NUMBER(15,6),
    InvoiceAmountDifference NUMBER(19,4)
);