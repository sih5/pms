Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'OutofWarrantyPayment_TransactionCategory', '保外扣补款类型', 1, '会员积分费', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'OutofWarrantyPayment_TransactionCategory', '保外扣补款类型', 2, '红包费', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'OutofWarrantyPayment_SourceType', '保外扣补款源单据类型', 1, '会员积分费', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'OutofWarrantyPayment_SourceType', '保外扣补款源单据类型', 2, '红包费', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PurchasePlanStatus', '采购计划状态', 7, '申请终止', 1, 1);



--保外扣补款单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'OutofWarrantyPayment', 'OutofWarrantyPayment', 1, 'OWP{CORPCODE}{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
