alter table IntegralClaimBill
add (
   DebitAmount        NUMBER(19,4),
   ComplementAmount   NUMBER(19,4)
);