create sequence S_VeriCodeEffectTime
/

/*==============================================================*/
/* Table: VeriCodeEffectTime                                    */
/*==============================================================*/
create table VeriCodeEffectTime  (
   Id                   NUMBER(9)                       not null,
   effectTime           NUMBER(9)                       not null,
   retransTime          NUMBER(9),
   creatorid            NUMBER(9),
   creatorname          VARCHAR2(100),
   createtime           DATE,
   constraint PK_VERICODEEFFECTTIME primary key (Id)
)
/
