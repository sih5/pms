
/*

--1、   脚本执行时间: 2017-1-12

--2、   脚本编制人: 孙建军

--3、   脚本审核人:  张晓亮

--4、   脚本执行服务器:  PMSDB_172.24.7.20 

--5、   脚本执行数据库名: gcdcs

--6、   脚本执行原因: FTDCS_ISS20170110004  (yxdcs,dcs,sddcs,gcdcs)客户信息 证件类型=其他  的数据进行清洗

--7、   请注明每条SQL语句的作用

--8、   需分步操作的请注明方法：

*/
--备份 343466 条数据
create table tmpdata.customer170112gc as select *from gcdcs.customer where IdDocumentType=65011005
--添加标识字段

alter table CUSTOMER170112GC drop column ISFINISHED;
alter table tmpdata.customer170112gc add (isfinished NUMBER(9) default 1);
--添加标识字段索引
create index idx_customer170112gcif on tmpdata.customer170112gc(isfinished) ;

create index idx_customer170112gc on tmpdata.customer170112gc(id) ;
--更新 343466 条数据
declare
  Iiddocumentnumber varchar2(50);
  IiddocumentType   number(9);
  sumqty            number(9);
  yu                number(9);
  yuchange          varchar2(50);
  IID               number(9); 
  n                 number(9); 
  CURSOR getiddocumentnumber IS SELECT ID, t.iddocumentnumber,isfinished FROM tmpdata.customer170112gc t where isfinished =1 and rownum <= 10000;
  
begin
  for m in 1..35 loop begin
  FOR C_getiddocumentnumber IN getiddocumentnumber LOOP
  begin
    n := 0;
    Iiddocumentnumber := null;
    IiddocumentType   := null;
    IID               := C_getiddocumentnumber.ID;
    if C_getiddocumentnumber.iddocumentnumber is not null then
        Iiddocumentnumber := regexp_replace(C_getiddocumentnumber.iddocumentnumber,'[^0-9a-zA-Z-]');
    end if;
      
    if (lengthb(Iiddocumentnumber) = 15) then
      IiddocumentType := 65011001;
    end if;
    if lengthb(Iiddocumentnumber) = 18 then
      begin
        if (regexp_replace(substrb(Iiddocumentnumber, 1, 17),'[^0-9]') = substrb(Iiddocumentnumber, 1, 17)) then
      begin
            sumqty := substrb(Iiddocumentnumber, 1, 1) * 7 +
                            substrb(Iiddocumentnumber, 2, 1) * 9 +
                            substrb(Iiddocumentnumber, 3, 1) * 10 +
                            substrb(Iiddocumentnumber, 4, 1) * 5 +
                            substrb(Iiddocumentnumber, 5, 1) * 8 +
                            substrb(Iiddocumentnumber, 6, 1) * 4 +
                            substrb(Iiddocumentnumber, 7, 1) * 2 +
                            substrb(Iiddocumentnumber, 8, 1) * 1 +
                            substrb(Iiddocumentnumber, 9, 1) * 6 +
                            substrb(Iiddocumentnumber, 10, 1) * 3 +
                            substrb(Iiddocumentnumber, 11, 1) * 7 +
                            substrb(Iiddocumentnumber, 12, 1) * 9 +
                            substrb(Iiddocumentnumber, 13, 1) * 10 +
                            substrb(Iiddocumentnumber, 14, 1) * 5 +
                            substrb(Iiddocumentnumber, 15, 1) * 8 +
                            substrb(Iiddocumentnumber, 16, 1) * 4 +
                            substrb(Iiddocumentnumber, 17, 1) * 2;
            yu:= mod(sumqty, 11);
            select decode (yu,0,'1',1,'0',2,'X',3,'9',4,'8',5,'7',6,'6',7,'5',8,'4',9,'3',10,'2')into yuchange from dual;
            if (yuchange = upper(substrb(Iiddocumentnumber, 18, 1))) then                     
              IiddocumentType := 65011001;
              else
                IiddocumentType := null;
            end if;
        end;
    end if;

    if (IiddocumentType is null and regexp_replace(substrb(UPPER(Iiddocumentnumber), 1, 18),'[^0-9A-HJ-NP-RTUWXY]') = substrb(UPPER(Iiddocumentnumber), 1, 18)
                and regexp_replace(substrb(Iiddocumentnumber, 3, 6),'[^0-9]') = substrb(Iiddocumentnumber, 3, 6)) then
          IiddocumentType := 65011006;               
        end if;  
    
    end;
    end if;
    
    if lengthb(Iiddocumentnumber) = 10 and substrb(Iiddocumentnumber, 9, 1)='-' then
      IiddocumentType := 65011002;
    end if;
    
    if ((lengthb(Iiddocumentnumber) = 8 or lengthb(Iiddocumentnumber) = 9) 
      and (regexp_replace(substrb(Iiddocumentnumber, 1, 1),'[^a-zA-Z]')=substrb(Iiddocumentnumber, 1, 1))
      and (regexp_replace(substrb(Iiddocumentnumber, 2, 7),'[^0-9]') = substrb(Iiddocumentnumber, 2, 7) 
           or regexp_replace(substrb(Iiddocumentnumber, 2, 8),'[^0-9]') = substrb(Iiddocumentnumber, 2, 8)))then
      IiddocumentType := 65011003;
    end if;
    
    if(IiddocumentType is null) then
      Iiddocumentnumber := null;
    end if;
  
    update tmpdata.customer170112gc T set T.isfinished=2 where T.ID = IID;
    update gcdcs.customer T set T.IDDOCUMENTNUMBER = Iiddocumentnumber,T.IDDOCUMENTTYPE = IiddocumentType where T.ID = IID;
  
  end;
  end loop;
  commit;
  end;
  end loop;
end;

delete from  gcdcs.keyvalueitem where keyvalueitem.key='65011005';
