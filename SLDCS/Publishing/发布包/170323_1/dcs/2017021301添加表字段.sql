alter table PartsPurchaseOrder
add IfInnerDirectProvision NUMBER(1);

alter table Warehouse
add IfSync NUMBER(1) default 0;

alter table BonusPointsOrder
add Status NUMBER(9);

alter table PartsSalesOrder
add 
(
   IfInnerDirectProvision NUMBER(1),
   InnerSourceId      NUMBER(9),
   InnerSourceCode    VARCHAR2(50)
);
   

alter table PlannedPriceAppDetail
add PriceFluctuationRatio NUMBER(15,6);

alter table PartsPurchasePricingDetail
add 
(
   PriceFluctuationRatio NUMBER(15,6),
   Brand              VARCHAR2(100)
);

alter table PartsSupplierRelation
drop column OverseasPartsFigure;

alter table PartsSupplierRelationHistory
drop column OverseasPartsFigure;

alter table PartsSalesPriceChange
add RejectOpinion      VARCHAR2(200);

alter table PartsSalesPriceChangeDetail
add (
   CategoryCode       VARCHAR2(100),
   CategoryName       VARCHAR2(100),
   SalesPriceFluctuationRatio NUMBER(15,6),
   RetailPriceFluctuationRatio NUMBER(15,6),
   MaxSalesPriceFloating NUMBER(15,6),
   MinSalesPriceFloating NUMBER(15,6),
   MaxRetailOrderPriceFloating NUMBER(15,6),
   MinRetailOrderPriceFloating NUMBER(15,6),
   IsUpsideDown       NUMBER(9)
);

alter table WarehouseSequence
modify 
(
   Id                 NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   CustomerCompanyId  NUMBER(9)                       not null,
   CustomerCompanyCode VARCHAR2(50)                    not null,
   CustomerCompanyName VARCHAR2(100)                   not null
);

/
create sequence S_PartsPriceIncreaseRate
/

/*==============================================================*/
/* Table: PartsPriceIncreaseRate                              */
/*==============================================================*/
create table PartsPriceIncreaseRate  (
   Id                 NUMBER(9)                       not null,
   BrandId            NUMBER(9),
   Brand              VARCHAR2(100),
   CategoryCode       VARCHAR2(100),
   CategoryName       VARCHAR2(100),
   SaleIncreaseRate   NUMBER(15,6),
   retailIncreaseRate NUMBER(15,6),
   MaxSalesPriceFloating NUMBER(15,6),
   MinSalesPriceFloating NUMBER(15,6),
   MaxRetailOrderPriceFloating NUMBER(15,6),
   MinRetailOrderPriceFloating NUMBER(15,6),
   Status             NUMBER(9),
   Remark             VARCHAR2(200),
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   AbandonerId        NUMBER(9),
   AbandonerName      VARCHAR2(100),
   AbandonTime        DATE,
   constraint PK_PARTSPRICEINCREASERATE primary key (Id)
)
/

create sequence S_SpecialPriceChangeList
/

create sequence S_SpecialTreatyPriceChange
/


/*==============================================================*/
/* Table: SpecialPriceChangeList                              */
/*==============================================================*/
create table SpecialPriceChangeList  (
   Id                 NUMBER(9)                       not null,
   SpecialTreatyPriceChangeId NUMBER(9),
   SparePartId        NUMBER(9),
   SparePartCode      VARCHAR2(50),
   SparePartName      VARCHAR2(100),
   SpecialTreatyPrice NUMBER(19,4),
   Remark             VARCHAR2(200),
   constraint PK_SPECIALPRICECHANGELIST primary key (Id)
)
/

/*==============================================================*/
/* Table: SpecialTreatyPriceChange                            */
/*==============================================================*/
create table SpecialTreatyPriceChange  (
   Id                 NUMBER(9)                       not null,
   Code               VARCHAR2(100),
   BrandId            NUMBER(9),
   BrandName          VARCHAR2(100),
   CorporationId      NUMBER(9),
   CorporationCode    VARCHAR2(100),
   CorporationName    VARCHAR2(100),
   Status             NUMBER(9),
   Remark             VARCHAR2(200),
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   CheckerId          NUMBER(9),
   CheckerName        VARCHAR2(100),
   CheckerTime        DATE,
   AbandonerId        NUMBER(9),
   AbandonerName      VARCHAR2(100),
   AbandonerTime      DATE,
   constraint PK_SPECIALTREATYPRICECHANGE primary key (Id)
)
/
