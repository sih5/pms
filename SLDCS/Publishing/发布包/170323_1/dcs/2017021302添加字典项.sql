Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'InternalSupplier', '内部直供供应商', 1, 'FT001078', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsSalesOrderProcessDetail_ProcessMethod', '订单处理方式', 11, '转内部直供', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsPriceAllType', '价格类型2', 1, '采购价', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsPriceAllType', '价格类型2', 2, '计划价', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsPriceAllType', '价格类型2', 3, '销售价', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'PartsPriceAllType', '价格类型2', 4, '零售指导价', 1, 1);



--配件特殊协议价变更申请单
Insert into CodeTemplate (Id, Name, EntityName, IsActived, Template, ResetType, Remark) Values (S_CodeTemplate.Nextval, 'SpecialTreatyPriceChange', 'SpecialTreatyPriceChange', 1, 'STPC{DATE:yyyyMMdd}{SERIAL:0000}', 1, NULL);
