alter table PartsPurchaseOrderDetail
add OriginalPlanNumber NUMBER(9);

alter table PartsPurchasePlanDetail_HW
add (
   POCode             VARCHAR2(50),
   BJCDCStock         NUMBER(9),
   SDCDCStock         NUMBER(9),
   SuplierCode        VARCHAR2(50)
   );
alter table PartsPurchasePlanDetail_HW
add 
(
   WarehouseId        NUMBER(9),
   WarehouseCode      VARCHAR2(50),
   WarehouseName      VARCHAR2(100)
);
   
alter table PartsPurchasePlan_HW
drop column WarehouseId;
   
alter table PartsPurchasePlan_HW
drop column WarehouseCode;
   
alter table PartsPurchasePlan_HW
drop column WarehouseName;
   
alter table PartsPurchasePlan_HW
drop column PartsSupplierId;

alter table PartsPurchasePlan_HW
drop column PartsSupplierCode;
   
alter table PartsPurchasePlan_HW
drop column PartsSupplierName;
   
alter table PartsPurchasePlan_HW
drop column BussinessCode;

alter table PartsInboundPlanDetail
add OriginalPlanNumber NUMBER(9);

alter table PartsTransferOrderDetail
add
(
   OriginalPlanNumber NUMBER(9),
   POCode             VARCHAR2(50)
);