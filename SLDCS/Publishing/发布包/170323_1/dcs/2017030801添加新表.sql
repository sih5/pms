create sequence S_FullStockTask
/


/*==============================================================*/
/* Table: FullStockTask                                       */
/*==============================================================*/
create table FullStockTask  (
   Id                 NUMBER(9)                       not null,
   SyncTime           VARCHAR2(100),
   CreatorId          NUMBER(9),
   CreateTime         DATE,
   CreatorName        VARCHAR2(100),
   constraint PK_FULLSTOCKTASK primary key (Id)
)
/
