update action set operationid='IntegralClaimInvoiceInfo|InitialApprove' where pageid=8810 and name='初审';

update page set id=8808,parentid=8800,name='保外索赔结算管理' where PageId='IntegralClaimBillSettle';
update page set id=8809,parentid=8800,name='服务站保外索赔结算指令管理' where PageId='IntegralSettleDictate';
update page set name='保外发票管理' where PageId='IntegralClaimInvoiceInformation';

update action set pageid=8808 where pageid=8814;
update action set pageid=8809 where pageid=8315;

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);