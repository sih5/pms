insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2102, 'PartsBranch|ExportNoGoldenTaxClassifyMsg', '导出无金税信息的数据', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2112,2100, 2, 'PartsPriceIncreaseRate', 'Released', NULL, '配件加价策略管理', '查看和维护配件加价策略信息', 'Client/DCS/Images/Menu/Common/SparePart.png', 11, 2);

insert into page (ID, PARENTID, TYPE, PAGEID, PAGETYPE, PARAMETER, NAME, NAME_ENUS, NAME_JAJP, DESCRIPTION, DESCRIPTION_ENUS, DESCRIPTION_JAJP, ICON, SEQUENCE, STATUS)
values (13164, 14100, 2, 'PartsPricingALL', 'Released', '', '各品牌配件价格查询', '', '', '按配件图号跨库查询福田品牌的采购价、计划价、销售价、零售指导价信息', '', '', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 9, 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2112, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2112, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2112, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2112, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2112, 'Common|Export', '导出', 2);


INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6105,6100, 2, 'PlannedPriceAppYX', 'Released', NULL, '配件计划价申请管理（营销）', '申请、执行计划价变更', 'Client/DCS/Images/Menu/Financial/PlannedPriceApp.png', 1, 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|BatchApproval', '批量审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|Detail', '查看明细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'PlannedPriceApp|WarehouseDetail', '查看仓库详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|MergeExport', '合并导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6105, 'Common|Abandon', '作废', 2);


INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2210,2200, 2, 'PartsPurchasePricingChangeYX', 'Released', NULL, '配件采购价格变更申请管理（营销）', '查询和维护配件采购价格变更申请单', 'Client/DCS/Images/Menu/Common/PartsPurchasePricingChange.png', 9, 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'Workflow|InitialApprove', '初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'Workflow|FinalApprove', '终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'PartsPurchasePricingChange|BatchInitialApprove', '批量初审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'PartsPurchasePricingChange|BatchFinalApprove', '批量终审', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2210, 'Common|ExportDetail', '导出清单', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 2101, 'SparePart|ExportNoGoldenTaxClassify', '无金税分类导出', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2323, 2300, 2, 'PartsSalesPriceChangeApplication', 'Released', NULL, '配件销售价格变更申请管理（营销）','查询、维护配件价格变更申请', 'Client/DCS/Images/Menu/PartsPurchasing/PartsPurchaseSettleInvoice.png', 4, 2);

INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'Common|Add', '新增', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'Common|Edit', '修改', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'Common|Abandon', '作废', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'Workflow|InitialApprove', '初审', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'Workflow|FinalApprove', '终审', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'Common|Import', '导入', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'Common|Export', '导出', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'PartsSalesPriceChange|BatchInitialApprove', '批量初审', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2323, 'PartsSalesPriceChange|BatchFinalApprove', '批量终审', 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (2324,2300, 2, 'SpecialTreatyPriceChange', 'Released', NULL, '配件特殊协议价申请管理', '查询、维护配件特殊协议价变更申请', 'Client/DCS/Images/Menu/PartsSales/PartsSpecialTreatyPrice.png', 4, 2);


INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2324, 'Common|Add', '新增', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2324, 'Common|Edit', '修改', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2324, 'Common|Approve', '审核', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2324, 'Common|BatchApproval', '批量审核', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2324, 'Common|Abandon', '作废', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2324, 'Common|Export', '导出', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 2324, 'Common|MergeExport', '合并导出', 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 7408, 'MemberInfo|VeriCodeEffectTime', '调整验证码有效时间', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5204, 'Common|PartsStockSync', '全量库存同步', 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
