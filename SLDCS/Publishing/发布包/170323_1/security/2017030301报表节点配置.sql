INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13806,13800, 2, 'RepairQuantityAnalysisSta1', 'Released', NULL, '服务商会员招募信息查询报表', '根据服务网络会员招募情况及会员维修情况统计会员信息', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 23, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13807,13800, 2, 'RepairQuantityAnalysisSta2', 'Released', NULL, '服务商会员交易明细查询报表', '根据服务网络会员维修交易情况统计会员维修频次、消费习惯等', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 24, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
