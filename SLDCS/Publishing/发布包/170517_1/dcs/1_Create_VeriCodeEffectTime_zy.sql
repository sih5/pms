-- Create table
create table VeriCodeEffectTime
(
  id                     NUMBER(9) not null,
  effectTime             NUMBER(9) not null,
  retransTime            NUMBER(9) not null,
  creatorid              NUMBER(9),
  creatorname            VARCHAR2(100),
  createtime             DATE
);

alter index PK_VeriCodeEffectTime nologging;

--create sequence
create sequence se_VeriCodeEffectTime;

