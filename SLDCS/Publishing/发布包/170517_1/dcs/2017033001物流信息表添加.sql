create sequence S_Logistics
/

create sequence S_LogisticsDetail
/


/*==============================================================*/
/* Table: Logistics                                           */
/*==============================================================*/
create table Logistics  (
   Id                 NUMBER(9)                       not null,
   ShippingCode       VARCHAR2(50)                    not null,
   OrderCode          VARCHAR2(50)                    not null,
   ShippingCompanyName VARCHAR2(100),
   ShippingCompanyNumber VARCHAR2(50),
   ApproveTime        DATE,
   WarehouseName      VARCHAR2(100),
   ShippingDate       DATE,
   RequestedArrivalDate DATE,
   ReceivingAddress   VARCHAR2(200),
   LogisticName       VARCHAR2(100),
   TransportDriverPhone VARCHAR2(100),
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   CancelId           NUMBER(9),
   CancelName         VARCHAR2(100),
   CancelTime         DATE,
   RowVersion         TIMESTAMP,
   Status             NUMBER(9)                       not null,
   ShippingMethod     NUMBER(9),
   constraint PK_LOGISTICS primary key (Id)
)
/

/*==============================================================*/
/* Table: LogisticsDetail                                     */
/*==============================================================*/
create table LogisticsDetail  (
   Id                 NUMBER(9)                       not null,
   Logisticsid        NUMBER(9)                       not null,
   PointName          VARCHAR2(100),
   PointTime          DATE,
   ScanTime           DATE,
   ScanName           VARCHAR2(100),
   Remark             VARCHAR2(200),
   Signatory          VARCHAR2(100),
   constraint PK_LOGISTICSDETAIL primary key (Id)
)
/
