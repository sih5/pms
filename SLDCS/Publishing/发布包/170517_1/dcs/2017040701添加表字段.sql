alter table RepairOrder
add (
   GenerateClaimTime  DATE,
   IsContainPhoto     NUMBER(9));
   
alter table RepairWorkOrderMd
add    IsClickTime        DATE;


alter table OverstockPartsApp
add  (  RejecterName         VARCHAR2(50),
   RejecterTime         DATE,
   RejectComment        VARCHAR2(200));

alter table SparePartHistory
add ProductBrand       VARCHAR2(50);