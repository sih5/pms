alter table DealerServiceExt
modify MainBusinessAreas  VARCHAR2(2000);
alter table DealerServiceExtHistory
modify MainBusinessAreas  VARCHAR2(2000);
alter table Logistics
add SignStatus         NUMBER(9);
/
create sequence S_TransactionInterfaceList
/
alter table MarketABQualityInformation
add EngModel           VARCHAR2(100);
/
/*==============================================================*/
/* Table: TransactionInterfaceList                            */
/*==============================================================*/
create table TransactionInterfaceList  (
   Id                 NUMBER(9)                       not null,
   TransactionInterfaceLogId NUMBER(9),
   RightsItem         VARCHAR2(100),
   RightsCode         VARCHAR2(100),
   IsRights           NUMBER(1),
   BenefiType         VARCHAR2(30),
   BenefiUom          NUMBER(9),
   DiscountNum        NUMBER(19,4),
   Time               NUMBER(9),
   constraint PK_TRANSACTIONINTERFACELIST primary key (Id)
)
/
drop table TransactionInterfaceLog;
/

/*==============================================================*/
/* Table: TransactionInterfaceLog                             */
/*==============================================================*/
create table TransactionInterfaceLog  (
   Id                 NUMBER(9)                       not null,
   RepairOrderID      NUMBER(9),
   MemberNumber       VARCHAR2(15),
   MemberName         VARCHAR2(100),
   IDType             VARCHAR2(100),
   IDNumer            VARCHAR2(50),
   MemberRank         VARCHAR2(100),
   MemberPhone        VARCHAR2(30),
   MemberValue        NUMBER(19,4),
   MaxMemberValue     NUMBER(9),
   MemberUsedIntegral NUMBER(19,4),
   LaborCost          NUMBER(19,4),
   MaterialCost       NUMBER(19,4),
   TrimCost           NUMBER(19,4),
   OtherCost          NUMBER(19,4),
   OutRange           NUMBER(9),
   FieldTotalAmount   NUMBER(19,4),
   TotalAmount        NUMBER(19,4),
   Discount           NUMBER(19,4),
   CostDiscount       NUMBER(19,4),
   DeductionCost      NUMBER(19,4),
   MemberRightsCost   NUMBER(19,4),
   DeductionTotalAmount NUMBER(19,4),
   TransactionTime    DATE,
   SyncStatus  NUMBER(9),
   SyncMessage        VARCHAR2(200),
   SyncTime           DATE,
   VIN                  VARCHAR2(100),
   MinServiceCode     VARCHAR2(100),
   FrozenStatus       NUMBER(9),
   constraint PK_TRANSACTIONINTERFACELOG primary key (Id)
)
/

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SynchronousState', '交易接口日志同步状态', 1, '未处理', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SynchronousState', '交易接口日志同步状态', 2, '处理成功', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SynchronousState', '交易接口日志同步状态', 3, '处理失败', 1, 1);

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SignStatus', '签收状态', 1, '在途', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'SignStatus', '签收状态', 2, '已发送', 1, 1);

