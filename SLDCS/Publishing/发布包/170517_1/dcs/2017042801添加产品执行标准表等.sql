create sequence S_ProductStandard
/
/*==============================================================*/
/* Table: ProductStandard                                     */
/*==============================================================*/
create table ProductStandard  (
   Id                 NUMBER(9)                       not null,
   StandardCode       VARCHAR2(100)                   not null,
   StandardName       VARCHAR2(100)                   not null,
   Remark             VARCHAR2(200),
   Status             NUMBER(9)                       not null,
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   AbandonerId        NUMBER(9),
   AbandonerName      VARCHAR2(100),
   AbandonTime        DATE,
   constraint PK_PRODUCTSTANDARD primary key (Id)
)
/

alter table SparePart
add (   StandardCode       VARCHAR2(100),
   StandardName       VARCHAR2(100));
   
alter table SparePartHistory
add (   StandardCode       VARCHAR2(100),
   StandardName       VARCHAR2(100));