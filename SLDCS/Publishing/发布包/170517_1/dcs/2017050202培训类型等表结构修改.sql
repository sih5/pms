alter table AuthenticationType
add (
   AuthenticationTypeCode VARCHAR2(50),
   ValDate            NUMBER(9),
   ModifyName         VARCHAR2(100),
   ModifyId           NUMBER(9),
   ModifyTime         DATE,
   CancelName         VARCHAR2(100),
   CancelId           NUMBER(9),
   CancelTime         DATE,
   Status             NUMBER(9)
);

alter table DealerPerTrainAut
add (
   IdCard             VARCHAR2(50),
   CertificateId      VARCHAR2(100),
   TrainingName       VARCHAR2(100),
   CancelId           NUMBER(9),
   CancelTime         DATE,
   CancelName         VARCHAR2(100)
);

alter table TrainingType
modify TrainingTypeName NUMBER(9);

alter table TrainingType
add (
   TrainingTypeCode   NUMBER(9),
   TrainingCode       VARCHAR2(50),
   TrainingName       VARCHAR2(100),
   ModifyName         VARCHAR2(100),
   ModifyId           NUMBER(9),
   ModifyTime         DATE,
   CancelName         VARCHAR2(100),
   CancelId           NUMBER(9),
   CancelTime         DATE,
   Status             NUMBER(9)
);