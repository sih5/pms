create sequence S_PartsExchangeGroup
/

/*==============================================================*/
/* Table: PartsExchangeGroup                                  */
/*==============================================================*/
create table PartsExchangeGroup  (
   Id                 NUMBER(9)                       not null,
   ExGroupCode        VARCHAR2(100)                   not null,
   ExchangeCode       VARCHAR2(100),
   Status             NUMBER(9)                       not null,
   Remark             VARCHAR2(200),
   CreatorId          NUMBER(9),
   CreatorName        VARCHAR2(100),
   CreateTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifyTime         DATE,
   AbandonerId        NUMBER(9),
   AbandonerName      VARCHAR2(100),
   AbandonTime        DATE,
   constraint PK_PARTSEXCHANGEGROUP primary key (Id)
)
/
