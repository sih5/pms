CREATE OR REPLACE PROCEDURE PRO_MoveInvalidPartsExGroup AS
--将互换组下互换号和配件信息数量小于等于一条的互换组信息作废
update PartsExchangeGroup 
set status=99
where status<>99 and id in(
select id from(
select eg.id, eg.exgroupcode, count(e.exchangecode)
  from PartsExchangeGroup eg
 left outer join PartsExchange e
    on eg.exchangecode = e.exchangecode
    and e.status <> 99
 group by eg.id,eg.exgroupcode
 having count(e.exchangecode) <=1
 ))
/

-----------------每天定时删除符合条件的互换组信息
begin                
      sys.dbms_scheduler.create_job( 
                                job_name => 'MoveInvalidPartsExGroup_JOB',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'PRO_MoveInvalidPartsExGroup',
                                start_date          => to_date(to_char(sysdate,'yyyy-mm-dd')||' 22:00:00', 'yyyy-mm-dd hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                enabled             => false,
                                auto_drop           => false,
                               comments            => '每天22点定时删除符合条件的互换组信息');
end;
