CREATE OR REPLACE PROCEDURE PFullStockTask is
categoryname number(9);
BEGIN
  begin
   select count(*) into categoryname   from FullStockTask where SyncTime= to_char(sysdate,'YYYY-MM-DD');
   EXCEPTION
   when no_data_found then
   categoryname:=0;
     end ;
    if categoryname >0 then
     INSERT INTO Retailer_PartsStockYX_SYNC
                select S_Retailer_PartsStockYX_SYNC.NEXTVAL,s.Id,'PartsStock',sysdate  from PartsStock  s
                                                inner join Warehouse w on s.warehouseid=w.Id
                                                inner join SalesUnitAffiWarehouse u on u.warehouseid=w.Id
                                                inner join SalesUnit b on u.SalesUnitId=b.Id
                                                inner join PartsSalesCategory p on p.Id=b.partssalescategoryid where ( p.Code='999999' Or p.Code='999991') and w.IsQualityWarehouse !=1 and w.Code !='SCXYFCK' and w.Code !='XNSCXCK'  and w.Code !='XNPJGSCK';
     end if;
END PFullStockTask;