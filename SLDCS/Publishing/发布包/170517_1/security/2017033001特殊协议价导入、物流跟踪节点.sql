INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5509,5500, 2, 'LogisticsTracking', 'Released', NULL, '物流跟踪管理', '录入、查询、合并导出物流跟踪单', 'Client/DCS/Images/Menu/PartsStocking/PartsLogisticLossBill.png', 9, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8609, 8600, 2, 'IntegralClaimBillForDealer', 'Released', NULL, '保外索赔单查询', '查询保外索赔单', 'Client/DCS/Images/Menu/Common/IntegralClaimBill.png', 8, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8314, 8600, 2, 'OutofWarrantyPaymentForSearch', 'Released', NULL, '保外扣补款查询', '查询保外扣补款', 'Client/DCS/Images/Menu/Service/ExpenseAdjustmentBill.png', 6, 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5509, 'LogisticsTracking|Input', '录入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5509, 'LogisticsTracking|InputByDeputy', '代录入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5509, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5509, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8609, 'IntegralClaimBillForDealer|Detail', '查看详细', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8609, 'IntegralClaimBillForDealer|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8609, 'IntegralClaimBillForDealer|APPPicture', '查看APP照片', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8609, 'IntegralClaimBillForDealer|APPRoute', '查看APP轨迹', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8108, 'RepairWorkOrderMd|OursFinished', '24小时是否处理完毕', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8314, 'Common|Export', '导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4404, 'Common|Reject', '驳回', 2);

update page set Name='新电商退货接口日志查询',Description='新电商退货接口日志查询' where id=4308;
update page set name='保内扣补款查询',description='查询保内扣补款' where id=8308;

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);