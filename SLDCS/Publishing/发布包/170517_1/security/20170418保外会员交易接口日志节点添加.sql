INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8109, 8100, 2, 'TransactionInterfaceLog', 'Released', NULL, '保外会员交易接口日志查询', '保外会员交易接口处理', 'Client/DCS/Images/Menu/Common/MemberInfo.png', 9, 2);

INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5607,5600, 2, 'WMSPartsInventoryBill', 'Released', NULL, 'WMS配件盘点管理', '分公司生成、审核配件盘点单', 'Client/DCS/Images/Menu/PartsStocking/PartsInventoryBill.png', 2, 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8109, 'TransactionInterfaceLog|ManualProcessing', '手动处理', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8109, 'TransactionInterfaceLog|Export', '导出', 2);


insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4403, 'Common|Abandon', '作废', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4101, 'Logistics|LogisticsQuery', '物流信息查询', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5607, 'Common|Approve', '审批', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5607, 'Common|Detail', '查看清单', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5607, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5607, 'PartsInventoryBill|InventoryCoverage', '库存覆盖', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5607, 'Common|MergeExport', '合并导出', 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);
