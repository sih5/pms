INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13808,13800, 2, 'AuthenticationRepair', 'Released', NULL, '服务站人员培训认证与维修关联报表', '通过维修索赔单，统计出维修工的维修项目信息及认证培训信息', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 25, 2);


-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
