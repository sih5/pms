create table PartsSalesOrderASAP
(
  Id                      NUMBER(9) not null primary key,
  SysCode                 VARCHAR2(50),
  OverseasDemandSheetNo varchar2(50),
  BranchCode varchar2(50),
  SubmitCompanyName varchar2(50),
  ShippingMethod varchar2(50),
  RequestedDeliveryTime varchar2(50),
    PartsSalesOrderTypeName varchar2(50),
    CreatorName varchar2(50),
  HandleStatus varchar2(50),
  ModifyTime date,
  Message                  nvarchar2(200)
)
/
create table PartsSalesOrderDetailASAP
(
  Id number(9) not null primary key,
  PartsSalesOrderId number(9),
  OverseasPartsFigure varchar2(50),
  MeasureUnit varchar2(50),
  OrderedQuantity number(9),
  SupplierCode varchar2(50),
  OrderPrice varchar2(50),
  Remark varchar2(50)
)
/
create table PartsOutboundPlanASAP
(
  Id number(9) not null primary key,
ObjId varchar2(38),                 
TheDate date,
Interface_Record_Id varchar2(25),                 
Interface_Condition varchar2(25),                 
Interface_Action_Code varchar2(25),                 
Company varchar2(25),                 
Shipment_Id nvarchar2(25),                  
Warehouse nvarchar2(25),                  
User_Stamp varchar2(30),                  
Date_Time_Stamp date,
Scheduled_Ship_Date nvarchar2(25),                  
Customer varchar2(25),                  
Order_Type varchar2(25),                  
Ship_To varchar2(25),                 
Ship_To_Address1 nvarchar2(50),                 
Ship_To_Address2 varchar2(50),                  
Ship_To_Name varchar2(50),                  
Ship_To_Attention_To nvarchar2(50),                 
Carrier varchar2(50),                 
Allocate_Complete varchar2(1),                  
Erp_Order varchar2(25),                 
Carrier_Service varchar2(50),                 
Ship_To_City varchar2(30),                  
Ship_To_State varchar2(30),                 
Ship_To_Country varchar2(25),                 
Ship_To_Postal_Code varchar2(25),                 
Read_Date_Time_Stamp date,
User_Def1 varchar2(25),                 
User_Def2 varchar2(25),                 
User_Def3 varchar2(25),                 
User_Def4 varchar2(25),                 
User_Def5 varchar2(25),                 
User_Def6 varchar2(25),                 
User_Def7 varchar2(25),               
User_Def8 varchar2(25),               
User_Def9 varchar2(25),                 
User_Def10 varchar2(25),                  
User_Def11 varchar2(25),                  
User_Def12 varchar2(25),                  
User_Def13 varchar2(25),                  
P_Fumigating varchar2(1),                 
P_Auditing varchar2(1),                 
P_Packing_Instructions varchar2(25),                  
P_Mark varchar2(25),                  
P_Inspection varchar2(1),                 
P_User_Def1 varchar2(25),                 
P_User_Def2 varchar2(25)        ,
  HandleStatus varchar2(50),
  ModifyTime date,
  Message               nvarchar2(200)
)
/
create table PartsOutboundPlandASAP
(
  Id number(9) not null primary key,
ObjId varchar2(38),                 
ParentId varchar2(38),                  
Interface_Condition varchar2(25),                 
Interface_Action_Code varchar2(25),                 
Interface_Record_Id varchar2(25),                 
Company varchar2(25),                 
Interface_Link_Id varchar2(25),                 
Shipment_Id varchar2(25),                 
Warehouse varchar2(25),                 
Item varchar2(25),                  
Quantity_Um varchar2(25),                 
Total_Qty numeric(19,5),                  
Lot_Controlled varchar2(1),                 
User_Stamp varchar2(30),                  
Date_Time_Stamp date,
Erp_Order_Line_Num  numeric(19,5),                  
Read_Date_Time_Stamp date,
User_Def1 varchar2(25),                 
User_Def2 varchar2(25),                 
User_Def3 varchar2(25),                 
User_Def4 varchar2(25),                 
User_Def5 varchar2(25),                 
User_Def6 varchar2(25),                 
User_Def7 varchar2(25),               
User_Def8 varchar2(25),                 
Erp_Order varchar2(25),
POSNR numeric(19),  
EBELN varchar2(25), 
EBELP numeric(19)
)
/
create sequence S_CAReconciliation
/

create sequence S_CAReconciliationList
/


/*==============================================================*/
/* Table: CAReconciliation                                    */
/*==============================================================*/
create table CAReconciliation  (
   Id                 NUMBER(9)                       not null,
   StatementCode      VARCHAR2(50)                    not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(100)                   not null,
   CorporationId      NUMBER(9)                       not null,
   CorporationCode    VARCHAR2(100)                   not null,
   CorporationName    VARCHAR2(100)                   not null,
   YeBalance          NUMBER(19,4)                    not null,
   ReconciliationTime DATE                            not null,
   ReconciliationId   NUMBER(9)                       not null,
   ReconciliationName VARCHAR2(100)                   not null,
   Status             NUMBER(9)                       not null,
   Remark             VARCHAR2(500),
   CreatorId          NUMBER(9)                       not null,
   CreatorName        VARCHAR2(100)                   not null,
   CreateTime         DATE                            not null,
   SubmitterId        NUMBER(9),
   SubmitterName      VARCHAR2(100),
   SubmitTime         DATE,
   ModifierId         NUMBER(9),
   ModifierName       VARCHAR2(100),
   ModifierTime       DATE,
   ConfirmorId        NUMBER(9),
   ConfirmorName      VARCHAR2(100),
   ConfirmorTime      DATE,
   AbandonerId        NUMBER(9),
   AbandonerName      VARCHAR2(100),
   AbandonerTime      DATE,
   constraint PK_CARECONCILIATION primary key (Id)
)
/

/*==============================================================*/
/* Table: CAReconciliationList                                */
/*==============================================================*/
create table CAReconciliationList  (
   Id                 NUMBER(9)                       not null,
   CAReconciliationId NUMBER(9)                       not null,
   ReconciliationDirection NUMBER(9)                       not null,
   Time               DATE                            not null,
   Abstract           VARCHAR2(500)                   not null,
   Money              NUMBER(19,4)                    not null,
   Remark             VARCHAR2(500),
   constraint PK_CARECONCILIATIONLIST primary key (Id)
)
/