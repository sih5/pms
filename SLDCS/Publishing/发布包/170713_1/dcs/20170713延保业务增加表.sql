
create sequence S_DeferredDiscountType
/

create sequence S_ExtendedWarrantyOrder
/

create sequence S_ExtendedWarrantyOrderList
/

create sequence S_ExtendedWarrantyProduct
/

/*==============================================================*/
/* Table: DeferredDiscountType                                  */
/*==============================================================*/
create table DeferredDiscountType  (
   Id                   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategory   VARCHAR2(100)                   not null,
   ExtendedWarrantyProductId NUMBER(9)                       not null,
   ExtendedWarrantyProductName VARCHAR2(100)                   not null,
   ExtendedWarrantyAgioType VARCHAR2(100)                   not null,
   ExtendedWarrantyAgio NUMBER(15,6)                    not null,
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreatorTime          DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifierTime         DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonerTime        DATE,
   constraint PK_DEFERREDDISCOUNTTYPE primary key (Id)
)
/

/*==============================================================*/
/* Table: ExtendedWarrantyOrder                                 */
/*==============================================================*/
create table ExtendedWarrantyOrder  (
   Id                   NUMBER(9)                       not null,
   ExtendedWarrantyOrderCode VARCHAR2(50)                    not null,
   CustomerName         VARCHAR2(100)                   not null,
   CellNumber           VARCHAR2(50)                    not null,
   MemberRank           VARCHAR2(50),
   Email                VARCHAR2(50),
   IDType               VARCHAR2(50),
   IDNumer              VARCHAR2(50),
   Province             VARCHAR2(50),
   City                 VARCHAR2(50),
   Region               VARCHAR2(50),
   DetailAddress        VARCHAR2(50),
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(100)                   not null,
   ServiceProductLineId NUMBER(9)                       not null,
   ServiceProductLine   VARCHAR2(100)                   not null,
   VIN                  VARCHAR2(50)                    not null,
   CarSalesDate         DATE                            not null,
   Mileage              NUMBER(9)                       not null,
   EngineCylinder       NUMBER(15,6),
   PurchaseAmount       NUMBER(19,4)                    not null,
   EngineModel          VARCHAR2(100)                   not null,
   VehicleLicensePlate  VARCHAR2(50),
   SalesDate            DATE                            not null,
   TotalAmount          NUMBER(19,4)                    not null,
   DealerTotalAmount    NUMBER(19,4)                    not null,
   SellPersonnel        VARCHAR2(100),
   Remark               VARCHAR2(100),
   Status               NUMBER(9)                       not null,
   CheckerResult        NUMBER(9)                       not null,
   DetailedReasons      VARCHAR2(100),
   ProtocolPhotos       VARCHAR2(100),
   IDPhoto              VARCHAR2(100)                   not null,
   CarInvoicePhotos     VARCHAR2(100)                   not null,
   DealerCode           VARCHAR2(100)                   not null,
   DealerName           VARCHAR2(100)                   not null,
   TravelPermitPhoto    VARCHAR2(100),
   MileagePhotos        VARCHAR2(100),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreatorTime          DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitterTime        DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifierTime         DATE,
   ConfirmorId          NUMBER(9),
   ConfirmorName        VARCHAR2(100),
   ConfirmorTime        DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonerTime        DATE,
   constraint PK_EXTENDEDWARRANTYORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: ExtendedWarrantyOrderList                             */
/*==============================================================*/
create table ExtendedWarrantyOrderList  (
   Id                   NUMBER(9)                       not null,
   ExtendedWarrantyOrderId NUMBER(9)                       not null,
   ExtendedWarrantyProductId NUMBER(9)                       not null,
   CustomerRetailPrice  NUMBER(19,4)                    not null,
   SalesPrice           NUMBER(19,4)                    not null,
   AgioType             NUMBER(9),
   DiscountRate         NUMBER(15,6)                    not null,
   DiscountedPrice      NUMBER(19,4)                    not null,
   TotalAmount          NUMBER(19,4)                    not null,
   ValidFrom            DATE,
   EndDate              DATE,
   StartMileage         NUMBER(9),
   EndMileage           NUMBER(9),
   ExtensionCoverage    VARCHAR2(200),
   constraint PK_EXTENDEDWARRANTYORDERLIST primary key (Id)
)
/

/*==============================================================*/
/* Table: ExtendedWarrantyProduct                               */
/*==============================================================*/
create table ExtendedWarrantyProduct  (
   Id                   NUMBER(9)                       not null,
   ExtendedWarrantyProductCode VARCHAR2(50)                    not null,
   ExtendedWarrantyProductName VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategory   VARCHAR2(100)                   not null,
   ServiceProductLineId NUMBER(9)                       not null,
   ServiceProductLine   VARCHAR2(100)                   not null,
   ExtendedWarrantyTerm NUMBER(15,6),
   ExtendedWarrantyMileage NUMBER(9),
   ExtendedWarrantyTermRat NUMBER(9),
   ExtendedWarrantyMileageRat NUMBER(9),
   TradePrice           NUMBER(19,4)                    not null,
   CustomerRetailPrice  NUMBER(19,4)                    not null,
   PayStartTimeCondition DATE                            not null,
   PayEndTimeCondition  DATE                            not null,
   PayStartTimeMileage  NUMBER(9),
   PayEndTimeMileage    NUMBER(9),
   AutomaticAuditPeriod NUMBER(9),
   Status               NUMBER(9)                       not null,
   ExtensionCoverage    VARCHAR2(500),
   OutExtensionCoverage VARCHAR2(500),
   AttachmentUpload     VARCHAR2(500),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreatorTime          DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifierTime         DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonerTime        DATE,
   constraint PK_EXTENDEDWARRANTYPRODUCT primary key (Id)
)
/