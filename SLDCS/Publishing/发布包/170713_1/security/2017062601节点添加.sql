INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (6219,6200, 2, 'CAReconciliation', 'Released', NULL,'企业往来账对账函管理', '企业往来账对账函管理', 'Client/DCS/Images/Menu/Financial/CAReconciliation.png', 19, 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6219, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6219, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6219, 'Common|Submit', '提交', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6219, 'Common|Confirm', '确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6219, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6219, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6219, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 6219, 'Common|Detail', '详细信息', 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);

-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);


