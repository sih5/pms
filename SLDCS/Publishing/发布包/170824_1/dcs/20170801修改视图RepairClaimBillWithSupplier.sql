create or replace view dcs.repairclaimbillwithsupplier as
select RepairClaimBill.Id                         as ID,
       RepairClaimBill.BranchId                   as BranchId,
       RepairClaimBill.ProductLineType            as ProductLineType,
       RepairClaimBill.IfClaimToSupplier          as IfClaimToSupplier,
       RepairClaimBill.ClaimBillCode              as ClaimBillCode,
       RepairClaimBill.RepairContractCode         as RepairContractCode,
       dsi.BusinessCode                           as CustomerCode,
       RepairClaimBill.RepairWorkOrder            as RepairWorkOrder,
       RepairClaimBill.QualityInformationCode     as QualityInformationCode,
       RepairClaimBill.InitialApprovertComment    as InitialApprovertComment,
       RepairClaimBill.OtherCost                  as OtherCost,
       RepairClaimBill.InitialApproverName        as InitialApproverName,
       RepairClaimBill.RepairType                 as RepairType,
       RepairClaimBill.Status                     as Status,
       RepairClaimBill.AutoApproveStatus          as AutoApproveStatus,
       RepairClaimBill.RepairClaimApplicationCode as RepairClaimApplicationCode,
       RepairClaimBill.SupplierCheckStatus        as SupplierCheckStatus,
       RepairClaimBill.SupplierConfirmStatus      as SupplierConfirmStatus,
       RepairClaimBill.SettlementStatus           as SettlementStatus,
       RepairClaimBill.SupplierSettleStatus       as SupplierSettleStatus,
       RepairClaimBill.UsedPartsDisposalStatus    as UsedPartsDisposalStatus,
       RepairClaimBill.RejectQty                  as RejectQty,
       RepairClaimBill.DealerId                   as DealerId,
       RepairClaimBill.DealerCode                 as DealerCode,
       RepairClaimBill.DealerName                 as DealerName,
       ve.SerialNumber                            as SerialNumber,
       ve.SalesDate                               as SalesDate,
       RepairClaimBill.Mileage                    as Mileage,
       RepairClaimBill.WorkingHours               as WorkingHours,
       RepairClaimBill.Capacity                   as Capacity,
       sp.ProductLineId                           as ServiceProductLineId,
       sp.ProductLineCode                         as ServiceProductLineCode,
       sp.ProductLineName                         as ServiceProductLineName,
       ve.ProductCategoryName                     as VehicleType,
       RepairClaimBill.ApplicationType            as ApplicationType,
       at.ApplicationTypeName                     as ApplicationTypeName,
       RepairClaimBill.MalfunctionId              as MalfunctionId,
       RepairClaimBill.MalfunctionCode            as MalfunctionCode,
       RepairClaimBill.MalfunctionDescription     as MalfunctionDescription,
       RepairClaimBill.MalfunctionReason          as MalfunctionReason,
       RepairClaimBill.FaultyPartsId              as FaultyPartsId,
       RepairClaimBill.FaultyPartsCode            as FaultyPartsCode,
       RepairClaimBill.FaultyPartsName            as FaultyPartsName,
       RepairClaimBill.ClaimSupplierId            as ClaimSupplierId,
       RepairClaimBill.ClaimSupplierCode          as ClaimSupplierCode,
       RepairClaimBill.ClaimSupplierName          as ClaimSupplierName,
       RepairClaimBill.ResponsibleUnitId          as ResponsibleUnitId,
       ru.Code                                    as responsibleUnitCode,
       ru.Name                                    as responsibleUnitName,
       --工时费
       decode(k.IfSPByLabor,
              1,
              (select sum(DefaultLaborHour)
                 from RepairClaimItemDetail
                where RepairClaimBill.id =
                      RepairClaimItemDetail.RepairClaimBillid) *
              nvl(k.LaborUnitPrice, 0),
              (select sum(DefaultLaborHour * LaborUnitPrice)
                 from RepairClaimItemDetail
                where RepairClaimBill.id =
                      RepairClaimItemDetail.RepairClaimBillid) *
              nvl(k.LaborCoefficient, 0)) as LaborCost,
       --材料费
       ((select sum(nvl(t2.MaterialCost, 0))
           from RepairClaimItemDetail t1
           left join RepairClaimMaterialDetail t2
             on t1.id = t2.RepairClaimitemDetailId
          where RepairClaimBill.id = t1.RepairClaimBillid) *
       nvl(k.PartsClaimCoefficient, 0)) as MaterialCost,
       --配件管理费
       ((select sum(nvl(t2.MaterialCost, 0))
           from RepairClaimItemDetail t1
           left join RepairClaimMaterialDetail t2
             on t1.id = t2.RepairClaimitemDetailId
          where RepairClaimBill.id = t1.RepairClaimBillid) *
       nvl(k.PartsClaimCoefficient, 0) * l.rate +
       (select sum(nvl(t2.MaterialCost, 0))
           from RepairClaimItemDetail t1
           left join RepairClaimMaterialDetail t2
             on t1.id = t2.RepairClaimitemDetailId
          where RepairClaimBill.id = t1.RepairClaimBillid) *
       nvl(k.PartsClaimCoefficient, 0) * nvl(k.OldPartTranscoeffiCient, 0)) as PartsManagementCost,
       --费用合计
       decode(k.IfSPByLabor,
              1,
              (((select sum(nvl(DefaultLaborHour, 0))
                   from RepairClaimItemDetail
                  where RepairClaimBill.id =
                        RepairClaimItemDetail.RepairClaimBillid) *
              nvl(k.LaborUnitPrice, 0))

              + ((select sum(nvl(t2.MaterialCost, 0))
                     from RepairClaimItemDetail t1
                     left join RepairClaimMaterialDetail t2
                       on t1.id = t2.RepairClaimitemDetailId
                    where RepairClaimBill.id = t1.RepairClaimBillid) *
              nvl(k.PartsClaimCoefficient, 0))

              + ((select sum(nvl(t2.MaterialCost, 0))
                     from RepairClaimItemDetail t1
                     left join RepairClaimMaterialDetail t2
                       on t1.id = t2.RepairClaimitemDetailId
                    where RepairClaimBill.id = t1.RepairClaimBillid) *
              nvl(k.PartsClaimCoefficient, 0) * l.rate +
              (select sum(nvl(t2.MaterialCost, 0))
                     from RepairClaimItemDetail t1
                     left join RepairClaimMaterialDetail t2
                       on t1.id = t2.RepairClaimitemDetailId
                    where RepairClaimBill.id = t1.RepairClaimBillid) *
              nvl(k.PartsClaimCoefficient, 0) *
              nvl(k.OldPartTranscoeffiCient, 0)) +
              nvl(RepairClaimBill.Othercost, 0)),

              ((select sum(nvl(DefaultLaborHour, 0) * nvl(LaborUnitPrice, 0))
                  from RepairClaimItemDetail
                 where RepairClaimBill.id =
                       RepairClaimItemDetail.RepairClaimBillid) *
              nvl(k.LaborCoefficient, 0)) +
              ((select sum(nvl(t2.MaterialCost, 0))
                  from RepairClaimItemDetail t1
                  left join RepairClaimMaterialDetail t2
                    on t1.id = t2.RepairClaimitemDetailId
                 where RepairClaimBill.id = t1.RepairClaimBillid) *
              nvl(k.PartsClaimCoefficient, 0)) +
              ((select sum(nvl(t2.MaterialCost, 0))
                  from RepairClaimItemDetail t1
                  left join RepairClaimMaterialDetail t2
                    on t1.id = t2.RepairClaimitemDetailId
                 where RepairClaimBill.id = t1.RepairClaimBillid) *
              nvl(k.PartsClaimCoefficient, 0) * l.rate +
              (select sum(nvl(t2.MaterialCost, 0))
                  from RepairClaimItemDetail t1
                  left join RepairClaimMaterialDetail t2
                    on t1.id = t2.RepairClaimitemDetailId
                 where RepairClaimBill.id = t1.RepairClaimBillid) *
              nvl(k.PartsClaimCoefficient, 0) *
              nvl(k.OldPartTranscoeffiCient, 0)) +
              nvl(RepairClaimBill.Othercost, 0)) as TotalAmount,

       RepairClaimBill.Remark                   as Remark,
       RepairClaimBill.MarketingDepartmentId    as MarketingDepartmentId,
       md.Code                                  as marketingDepartmentCode,
       md.Name                                  as marketingDepartmentName,
       ve.BridgeType                            as BridgeType,
       RepairClaimBill.PartsSalesCategoryId     as PartsSalesCategoryId,
       pc.Name                                  as PartsSalesCategoryName,
       RepairClaimBill.EngineModel              as EngineModel,
       RepairClaimBill.EngineSerialNumber       as EngineCode,
       RepairClaimBill.CreatorId                as CreatorId,
       RepairClaimBill.CreatorName              as CreatorName,
       RepairClaimBill.CreateTime               as CreateTime,
       RepairClaimBill.ApproverId               as ApproverId,
       RepairClaimBill.ApproverName             as ApproverName,
       RepairClaimBill.ApproveTime              as ApproveTime,
       RepairClaimBill.FinalApproverId          as FinalApproverId,
       RepairClaimBill.FinalApproverName        as FinalApproverName,
       RepairClaimBill.FinalApproverTime        as FinalApproveTime,
       RepairClaimBill.SupplierConfirmComment   as SupplierConfirmComment,
       RepairClaimBill.EngineSerialNumber       as EngineSerialNumber,
       RepairClaimBill.ServiceDepartmentComment as ServiceDepartmentComment,
       RepairClaimBill.RejectReason             as RejectReason,
       RepairClaimBill.VideoMobileNumber        as VideoMobileNumber,
       RepairClaimBill.MVSOutRange              as MVSOutRange,
       RepairClaimBill.MVSOutTime               as MVSOutTime,
       RepairClaimBill.MVSOutDistance           as MVSOutDistance,
       RepairClaimBill.MVSOutCoordinate         as MVSOutCoordinate,
       rca.CheckComment                         as CheckComment,
       RepairClaimBill.OutOfFactoryDate         as OutOfFactoryDate,
       RepairClaimBill.VehicleContactPerson     as VehicleContactPerson,
       RepairClaimBill.ContactPhone             as ContactPhone,
       RepairClaimBill.RepairRequestTime        as RepairRequestTime,
       RepairClaimBill.SupplierConfirmResult    as SupplierConfirmResult,  -- 供应商确认结果
       RepairClaimBill.SupplierConfirmNotP      as SupplierConfirmNotP,    --供应商确认不通过原因
       RepairClaimBill.SupplierCheckResult      as SupplierCheckResult,    --供应商复核结果
       RepairClaimBill.SupplierCheckNotP        as SupplierCheckNotP,      --供应商复核不通过原因
       RepairClaimBill.SupplierCheckComment     as SupplierCheckComment,   --供应商复核意见
       (select code from branch where branch.id=RepairClaimBill.branchid)as  SysCode
  from RepairClaimBill
  left join BranchSupplierRelation k
    on RepairClaimBill.ClaimSupplierId = k.SupplierId
   and RepairClaimBill.BranchId = k.BranchId
   and RepairClaimBill.PartsSalesCategoryId = k.PartsSalesCategoryId
   and k.Status = 1
  left join PartsManagementCostRate l
    on k.PartsManagementCostGradeId = l.PartsManagementCostGradeId
   and l.Status = 1
  left join ServiceProductLineView sp
    on RepairClaimBill.ServiceProductLineId = sp.ProductLineId
  left join VehicleInformation ve
    on RepairClaimBill.VehicleId = ve.Id
  left join SalesRegion sr
    on RepairClaimBill.SalesRegionId = sr.Id
   and sr.Status = 1
  left join MarketingDepartment md
    on RepairClaimBill.MarketingDepartmentId = md.Id
   and md.Status = 1
  left join PartsSalesCategory pc
    on RepairClaimBill.PartsSalesCategoryId = pc.Id
   and pc.Status = 1
  left join ResponsibleUnit ru
    on RepairClaimBill.ResponsibleUnitId = ru.Id
   and ru.Status = 1
  left join ApplicationType at
    on RepairClaimBill.BranchId = at.BranchId
  left join RepairClaimApplication rca
    on RepairClaimBill.RepairClaimApplicationId = rca.Id
   and rca.Status < 99
  left join DealerServiceInfo dsi
    on RepairClaimBill.DealerId = dsi.DealerId
   and RepairClaimBill.PartsSalesCategoryId = dsi.PartsSalesCategoryId
   and dsi.Status = 1
;
