alter table INTEGRALCLAIMBILL add redpacketstype NUMBER(9);
alter table REDPACKETSMSG add redpacketstype NUMBER(9);
alter table INTEGRALCLAIMBILL add redpacketsrange NUMBER(9);
alter table REDPACKETSMSG add redpacketsrange NUMBER(9);

create table MaintainRedProductMap  (
   Id                   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9),
   PartsSalesCategoryName VARCHAR2(100),
   PartsSalesCategoryCode VARCHAR2(50),
   ServiceProductLineId NUMBER(9),
   ServiceProductLineCode VARCHAR2(50),
   ServiceProductLineName VARCHAR2(100),
   FixedAmount          NUMBER(19,4),
   Status               NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   constraint PK_MAINTAINREDPRODUCTMAP primary key (Id)
);