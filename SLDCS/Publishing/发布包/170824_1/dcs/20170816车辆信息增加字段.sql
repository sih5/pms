alter table VEHICLEINFORMATION add device_type VARCHAR2(32);
alter table VEHICLEINFORMATION add is_finance VARCHAR2(6);
alter table VEHICLEINFORMATION add did VARCHAR2(32);
alter table VEHICLEINFORMATION add comm_code VARCHAR2(16);
alter table VEHICLEINFORMATION add std_simcode VARCHAR2(32);
alter table VEHICLEINFORMATION add ext_simcode VARCHAR2(32);
alter table VEHICLEINFORMATION add bound_time DATE;
alter table VEHICLEINFORMATION add sn VARCHAR2(32);