alter table PARTSPURCHASEORDER add bpmaudited NUMBER(9);
alter table PARTSPURCHASEPLANDETAIL_HW add gdcdcstock NUMBER(9);
alter table PARTSPURCHASEPLANDETAIL_HW add endamount NUMBER(9);
alter table PARTSINBOUNDPLAN add bpmaudited NUMBER(9);
alter table PARTSTRANSFERORDER add bpmaudited NUMBER(9);