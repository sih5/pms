Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'RepairClaimBill_SupplierResult', '索赔供应商确认结果', 0, '不通过', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'RepairClaimBill_SupplierResult', '索赔供应商确认结果', 1, '通过', 1, 1);


Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'RepairClaimBill_SupplierNotP', '供应商确认不通过原因', 1, '责任划分有误', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'RepairClaimBill_SupplierNotP', '供应商确认不通过原因', 2, '服务站违规操作', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'RepairClaimBill_SupplierNotP', '供应商确认不通过原因', 3, '非保修范围内', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'RepairClaimBill_SupplierNotP', '供应商确认不通过原因', 4, '涉嫌虚假索赔', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'RepairClaimBill_SupplierNotP', '供应商确认不通过原因', 5, '信息有差异', 1, 1);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'DCS', 'RepairClaimBill_SupplierNotP', '供应商确认不通过原因', 99, '其他', 1, 1);
