alter table PARTSSALESORDER add vehiclepartshandleordercode VARCHAR2(50);
alter table PARTSSALESORDER add vehiclepartshandleorderid NUMBER(9);
alter table PARTSSALESORDER add isagencyship NUMBER(1);