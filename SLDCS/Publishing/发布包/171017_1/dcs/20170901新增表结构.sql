/*==============================================================*/
/* Table: AgencyRetailerList                                    */
/*==============================================================*/
create table AgencyRetailerList  (
   Id                   NUMBER(9)                       not null,
   AgencyRetailerOrderId NUMBER(9)                       not null,
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(50),
   OrderedQuantity      NUMBER(9),
   ConfirmedAmount      NUMBER(9),
   UnitPrice            NUMBER(19,4),
   OrderSum             NUMBER(19,4),
   Remark               VARCHAR2(200),
   constraint PK_AGENCYRETAILERLIST primary key (Id)
)
/

/*==============================================================*/
/* Table: AgencyRetailerOrder                                   */
/*==============================================================*/
create table AgencyRetailerOrder  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(50)                    not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(50),
   ShippingCompanyId    NUMBER(9),
   ShippingCompanyCode  VARCHAR2(50),
   ShippingCompanyName  VARCHAR2(50),
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(50),
   CounterpartCompanyId NUMBER(9),
   CounterpartCompanyCode VARCHAR2(50),
   CounterpartCompanyName VARCHAR2(100),
   VehiclePartsHandleOrderId NUMBER(9),
   VehiclePartsHandleOrderCode VARCHAR2(50),
   Status               NUMBER(9),
   ShippingStatus       NUMBER(9),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(50),
   CreateTime           DATE,
   ConfirmorId          NUMBER(9),
   ConfirmorName        VARCHAR2(50),
   ConfirmorTime        DATE,
   CloserId             NUMBER(9),
   CloserName           VARCHAR2(50),
   CloserTime           DATE,
   RowVersion           TIMESTAMP,
   PartsSalesOrderId    NUMBER(9),
   PartsSalesOrderCode  VARCHAR2(50),
   ERPSourceOrderCode   VARCHAR2(500),
   constraint PK_AGENCYRETAILERORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: VehiclePartsHandleList                                */
/*==============================================================*/
create table VehiclePartsHandleList  (
   Id                   NUMBER(9)                       not null,
   VehiclePartsHandleOrderId NUMBER(9)                       not null,
   SparePartId          NUMBER(9),
   SparePartCode        VARCHAR2(50),
   SparePartName        VARCHAR2(50),
   OrderedQuantity      NUMBER(9),
   UnitPrice            NUMBER(19,4),
   OrderSum             NUMBER(19,4),
   Remark               VARCHAR2(200),
   constraint PK_VEHICLEPARTSHANDLELIST primary key (Id)
)
/

/*==============================================================*/
/* Table: VehiclePartsHandleOrder                               */
/*==============================================================*/
create table VehiclePartsHandleOrder  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BranchId             NUMBER(9)                       not null,
   BranchName           VARCHAR2(50)                    not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(50),
   ShippingCompanyId    NUMBER(9),
   ShippingCompanyCode  VARCHAR2(50),
   ShippingCompanyName  VARCHAR2(50),
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(50),
   CounterpartCompanyId NUMBER(9),
   CounterpartCompanyCode VARCHAR2(50),
   CounterpartCompanyName VARCHAR2(100),
   OrderNumber          VARCHAR2(50),
   IfDirectProvision    NUMBER(9),
   IsBarter             NUMBER(9),
   Status               NUMBER(9),
   Remark               VARCHAR2(200),
   TotalAmount          NUMBER(19,4),
   PaymentBonusPoints   NUMBER(9),
   RequestedDeliveryTime DATE,
   SubmitTime           DATE,
   ReceiverName         VARCHAR2(50),
   ContactPhone         VARCHAR2(15),
   ReceivingAddress     VARCHAR2(200),
   PaymentType          NUMBER(9),
   InvoiceType          NUMBER(9),
   InvoiceTitle         VARCHAR2(500),
   CreateTime           DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   HandleId             NUMBER(9),
   HandleName           VARCHAR2(50),
   HandleTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   AbandonComment       VARCHAR2(500),
   RowVersion           TIMESTAMP,
   ActualFreight        NUMBER(19,4),
   Province             VARCHAR2(100),
   City                 VARCHAR2(100),
   BankAccount          VARCHAR2(200),
   Taxpayeridentification VARCHAR2(50),
   InvoiceAdress        VARCHAR2(100),
   InvoicePhone         VARCHAR2(50),
   EnterpriseType       NUMBER(9),
   ReceivingMailbox     VARCHAR2(100),
   constraint PK_VEHICLEPARTSHANDLEORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: AgencyLogisticCompany                                 */
/*==============================================================*/
create table AgencyLogisticCompany  (
   Id                   NUMBER(9)                       not null,
   AgencyId             NUMBER(9)                       not null,
   Code                 VARCHAR2(50),
   Name                 VARCHAR2(100),
   Status               NUMBER(9),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonerTime        DATE,
   RowVersion           TIMESTAMP,
   constraint PK_AGENCYLOGISTICCOMPANY primary key (Id)
)
/

/*==============================================================*/
/* Table: AgencyPartsShippingOrder                              */
/*==============================================================*/
create table AgencyPartsShippingOrder  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Type                 NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9),
   ShippingCompanyId    NUMBER(9)                       not null,
   ShippingCompanyCode  VARCHAR2(50)                    not null,
   ShippingCompanyName  VARCHAR2(100)                   not null,
   SettlementCompanyId  NUMBER(9)                       not null,
   SettlementCompanyCode VARCHAR2(50)                    not null,
   SettlementCompanyName VARCHAR2(100)                   not null,
   ReceivingCompanyId   NUMBER(9)                       not null,
   ReceivingCompanyCode VARCHAR2(50)                    not null,
   InvoiceReceiveSaleCateId NUMBER(9),
   InvoiceReceiveSaleCateName VARCHAR2(50),
   ReceivingCompanyName VARCHAR2(100)                   not null,
   WarehouseId          NUMBER(9),
   WarehouseCode        VARCHAR2(50),
   WarehouseName        VARCHAR2(100),
   ReceivingWarehouseId NUMBER(9),
   ReceivingWarehouseCode VARCHAR2(50),
   ReceivingWarehouseName VARCHAR2(100),
   ReceivingAddress     VARCHAR2(200),
   LogisticCompanyId    NUMBER(9),
   LogisticCompanyCode  VARCHAR2(50),
   LogisticCompanyName  VARCHAR2(100),
   OriginalRequirementBillId NUMBER(9),
   OriginalRequirementBillCode VARCHAR2(50),
   OriginalRequirementBillType NUMBER(9),
   IsTransportLosses    NUMBER(1),
   TransportLossesDisposeStatus NUMBER(9),
   ShippingMethod       NUMBER(9),
   LogisticArrivalDate  DATE,
   RequestedArrivalDate DATE,
   ShippingDate         DATE,
   Weight               NUMBER(15,6),
   Volume               NUMBER(15,6),
   BillingMethod        NUMBER(9),
   TransportCostRate    NUMBER(9),
   TransportCost        NUMBER(19,4),
   TransportCostSettleStatus NUMBER(1),
   InsuranceRate        NUMBER(9),
   InsuranceFee         NUMBER(19,4),
   TransportMileage     NUMBER(9),
   TransportVehiclePlate VARCHAR2(50),
   TransportDriver      VARCHAR2(100),
   TransportDriverPhone VARCHAR2(100),
   DeliveryBillNumber   VARCHAR2(50),
   InterfaceRecordId    VARCHAR2(25),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   OrderApproveComment  VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   ConsigneeId          NUMBER(9),
   ConsigneeName        VARCHAR2(100),
   ConfirmedReceptionTime DATE,
   ReceiptConfirmorId   NUMBER(9),
   ReceiptConfirmorName VARCHAR2(100),
   ReceiptConfirmTime   DATE,
   RowVersion           TIMESTAMP,
   GPMSPurOrderCode     VARCHAR2(50),
   ArrivalMode          NUMBER(9),
   ShippingTime         DATE,
   ShippingMsg          VARCHAR2(100),
   ArrivalTime          DATE,
   CargoTerminalMsg     VARCHAR2(100),
   FlowFeedback         VARCHAR2(4000),
   ExpressCompanyId     NUMBER(9),
   ExpressCompanyCode   VARCHAR2(100),
   ExpressCompany       VARCHAR2(100),
   QueryURL             VARCHAR2(100),
   GPSCode              VARCHAR2(100),
   ExpectedPlaceDate    DATE,
   Logistic             VARCHAR2(100),
   SAPPurchasePlanCode  VARCHAR2(50),
   constraint PK_AGENCYPARTSSHIPPINGORDER primary key (Id)
)
/

/*==============================================================*/
/* Table: APartsShippingOrderDetail                        */
/*==============================================================*/
create table APartsShippingOrderDetail  (
   Id                   NUMBER(9)                       not null,
   PartsShippingOrderId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   ShippingAmount       NUMBER(9)                       not null,
   ConfirmedAmount      NUMBER(9),
   DifferenceClassification NUMBER(9),
   TransportLossesDisposeMethod NUMBER(9),
   InTransitDamageLossAmount NUMBER(9),
   SettlementPrice      NUMBER(19,4)                    not null,
   Remark               VARCHAR2(200),
   constraint PK_AGENCYPARTSSHIPPINGORDERDET primary key (Id)
)
/

/*==============================================================*/
/* Table: AgencyPartsShippingOrderRef                           */
/*==============================================================*/
create table AgencyPartsShippingOrderRef  (
   Id                   NUMBER(9)                       not null,
   PartsShippingOrderId NUMBER(9)                       not null,
   PartsOutboundBillId  NUMBER(9)                       not null,
   constraint PK_AGENCYPARTSSHIPPINGORDERREF primary key (Id)
)
/

/*==============================================================*/
/* Table: AgencyPartsOutboundBill                               */
/*==============================================================*/
create table AgencyPartsOutboundBill  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   PartsOutboundPlanId  NUMBER(9)                       not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   StorageCompanyId     NUMBER(9)                       not null,
   StorageCompanyCode   VARCHAR2(50)                    not null,
   StorageCompanyName   VARCHAR2(100)                   not null,
   StorageCompanyType   NUMBER(9)                       not null,
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesOrderTypeId NUMBER(9),
   PartsSalesOrderTypeName VARCHAR2(100),
   CounterpartCompanyId NUMBER(9)                       not null,
   CounterpartCompanyCode VARCHAR2(50)                    not null,
   CounterpartCompanyName VARCHAR2(100)                   not null,
   ReceivingCompanyId   NUMBER(9)                       not null,
   ReceivingCompanyCode VARCHAR2(50)                    not null,
   ReceivingCompanyName VARCHAR2(100)                   not null,
   ReceivingWarehouseId NUMBER(9),
   ReceivingWarehouseCode VARCHAR2(50),
   ReceivingWarehouseName VARCHAR2(100),
   OutboundType         NUMBER(9)                       not null,
   ShippingMethod       NUMBER(9),
   CustomerAccountId    NUMBER(9),
   OriginalRequirementBillId NUMBER(9)                       not null,
   OriginalRequirementBillType NUMBER(9)                       not null,
   OriginalRequirementBillCode VARCHAR2(50)                    not null,
   SettlementStatus     NUMBER(9)                       not null,
   OrderApproveComment  VARCHAR2(2000),
   InterfaceRecordId    VARCHAR2(25),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   ERPSourceOrderCode   VARCHAR2(500),
   constraint PK_AGENCYPARTSOUTBOUNDBILL primary key (Id)
)
/

/*==============================================================*/
/* Table: APartsOutboundBillDetail                         */
/*==============================================================*/
create table APartsOutboundBillDetail  (
   Id                   NUMBER(9)                       not null,
   PartsOutboundBillId  NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   OutboundAmount       NUMBER(9)                       not null,
   WarehouseAreaId      NUMBER(9),
   WarehouseAreaCode    VARCHAR2(50),
   BatchNumber          VARCHAR2(100),
   SettlementPrice      NUMBER(19,4)                    not null,
   CostPrice            NUMBER(19,4)                    not null,
   Remark               VARCHAR2(200),
   constraint PK_AGENCYPARTSOUTBOUNDBILLDETA primary key (Id)
)
/

/*==============================================================*/
/* Table: AgencyPartsOutboundPlan                               */
/*==============================================================*/
create table AgencyPartsOutboundPlan  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   WarehouseId          NUMBER(9)                       not null,
   WarehouseCode        VARCHAR2(50)                    not null,
   WarehouseName        VARCHAR2(100)                   not null,
   StorageCompanyId     NUMBER(9)                       not null,
   StorageCompanyCode   VARCHAR2(50)                    not null,
   StorageCompanyName   VARCHAR2(100)                   not null,
   StorageCompanyType   NUMBER(9)                       not null,
   CompanyAddressId     NUMBER(9),
   BranchId             NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   BranchName           VARCHAR2(100)                   not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesOrderTypeId NUMBER(9),
   PartsSalesOrderTypeName VARCHAR2(100),
   CounterpartCompanyId NUMBER(9)                       not null,
   CounterpartCompanyCode VARCHAR2(50)                    not null,
   CounterpartCompanyName VARCHAR2(100)                   not null,
   ReceivingCompanyId   NUMBER(9)                       not null,
   ReceivingCompanyCode VARCHAR2(50)                    not null,
   ReceivingCompanyName VARCHAR2(100)                   not null,
   ReceivingWarehouseId NUMBER(9),
   ReceivingWarehouseCode VARCHAR2(50),
   ReceivingWarehouseName VARCHAR2(100),
   SourceId             NUMBER(9)                       not null,
   SourceCode           VARCHAR2(50)                    not null,
   OutboundType         NUMBER(9)                       not null,
   ShippingMethod       NUMBER(9),
   CustomerAccountId    NUMBER(9),
   OriginalRequirementBillId NUMBER(9)                       not null,
   OriginalRequirementBillType NUMBER(9)                       not null,
   OriginalRequirementBillCode VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   StopComment          VARCHAR2(200),
   Remark               VARCHAR2(500),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   StoperId             NUMBER(9),
   Stoper               VARCHAR2(100),
   StopTime             DATE,
   RowVersion           TIMESTAMP,
   ERPSourceOrderCode   VARCHAR2(500),
   constraint PK_AGENCYPARTSOUTBOUNDPLAN primary key (Id)
)
/

/*==============================================================*/
/* Table: APartsOutboundPlanDetail                         */
/*==============================================================*/
create table APartsOutboundPlanDetail  (
   Id                   NUMBER(9)                       not null,
   PartsOutboundPlanId  NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   PlannedAmount        NUMBER(9)                       not null,
   OutboundFulfillment  NUMBER(9),
   Price                NUMBER(19,4)                    not null,
   Remark               VARCHAR2(200),
   constraint PK_AGENCYPARTSOUTBOUNDPLANDETA primary key (Id)
)
/