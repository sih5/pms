create sequence S_NotificationReply
/

create sequence S_DealerPartsTransOrderDetail
/

create sequence S_DealerPartsTransferOrder
/

/*==============================================================*/
/* Table: NotificationReply                                     */
/*==============================================================*/
create table NotificationReply  (
   id                   NUMBER(9)                       not null,
   NotificationId       NUMBER(9),
   NotificationCode     VARCHAR2(50),
   CompanyId            NUMBER(9),
   CompanyCode          VARCHAR2(100),
   CompanyName          VARCHAR2(200),
   ReplyChannel         NUMBER(9),
   ReplyContent         VARCHAR2(2000),
   ReplyFilePath        VARCHAR2(4000),
   ReplyDate            DATE,
   constraint PK_NOTIFICATIONREPLY primary key (id)
)
/

/*==============================================================*/
/* Table: DealerPartsTransOrderDetail                           */
/*==============================================================*/
create table DealerPartsTransOrderDetail  (
   Id                   NUMBER(9)                       not null,
   DealerPartsTransferOrderId NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   Amount               NUMBER(9)                       not null,
   WarrantyPrice        NUMBER(19,4),
   NoWarrantyPrice      NUMBER(19,4),
   DiffPrice            NUMBER(19,4),
   Remark               VARCHAR2(200),
   constraint PK_DEALERPARTSTRANSORDERDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: DealerPartsTransferOrder                              */
/*==============================================================*/
create table DealerPartsTransferOrder  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   BranchId             NUMBER(9)                       not null,
   BranchIdCode         VARCHAR2(50)                    not null,
   BranchIdName         VARCHAR2(100)                   not null,
   WarrantyBrandId      NUMBER(9)                       not null,
   WarrantyBrandCode    VARCHAR2(50)                    not null,
   WarrantyBrandName    VARCHAR2(100)                   not null,
   NoWarrantyBrandId    NUMBER(9)                       not null,
   NoWarrantyBrandCode  VARCHAR2(50)                    not null,
   NoWarrantyBrandName  VARCHAR2(100)                   not null,
   DealerId             NUMBER(9)                       not null,
   DealerCode           VARCHAR2(50)                    not null,
   DealerName           VARCHAR2(100)                   not null,
   SourceId             NUMBER(9),
   SourceCode           VARCHAR2(50),
   SumWarrantyPrice     NUMBER(19,4),
   SumNoWarrantyPrice   NUMBER(19,4),
   DiffPrice            NUMBER(19,4),
   Remark               VARCHAR2(500),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   SubmitterId          NUMBER(9),
   SubmitterName        VARCHAR2(100),
   SubmitTime           DATE,
   CheckerId            NUMBER(9),
   CheckerName          VARCHAR2(100),
   CheckTime            DATE,
   CheckOpinion         VARCHAR2(200),
   RejecterId           NUMBER(9),
   RejecterName         VARCHAR2(100),
   RejectTime           DATE,
   RejectOpinion        VARCHAR2(200),
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   Rowversion           TIMESTAMP,
   constraint PK_DEALERPARTSTRANSFERORDER primary key (Id)
)
/