alter table REPAIRCLAIMAPPLICATION add isapppicture NUMBER(9) default 0;

alter table REPAIRORDER add isagreement NUMBER(9);
alter table REPAIRORDER add arrivaltime date;
alter table REPAIRORDER add departuretime date;
alter table REPAIRORDER add atstationtime NUMBER(15,6);
