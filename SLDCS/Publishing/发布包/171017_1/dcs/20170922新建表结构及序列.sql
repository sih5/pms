create sequence S_DataBaseLink
/

create sequence S_DataBaseLinkBranch
/

create sequence S_ReportDownloadMsg
/

/*==============================================================*/
/* Table: DataBaseLink                                          */
/*==============================================================*/
create table DataBaseLink  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(100)                   not null,
   Link                 VARCHAR2(500)                   not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_DATABASELINK primary key (Id),
   CONSTRAINT UNQ_CODE UNIQUE (Code)
)
/

/*==============================================================*/
/* Table: DataBaseLinkBranch                                    */
/*==============================================================*/
create table DataBaseLinkBranch  (
   Id                   NUMBER(9)                       not null,
   BranchCode           VARCHAR2(50)                    not null,
   DataBaseLinkCode     VARCHAR2(100)                   not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_DATABASELINKBRANCH primary key (Id)
)
/

/*==============================================================*/
/* Table: ReportDownloadMsg                                     */
/*==============================================================*/
create table ReportDownloadMsg  (
   Id                   NUMBER(9)                       not null,
   CompanyId            NUMBER(9)                       not null,
   CompanyCode          VARCHAR2(50)                    not null,
   CompanyName          VARCHAR2(100)                   not null,
   NodeName             VARCHAR2(100)                   not null,
   NodeId               NUMBER(9)                       not null,
   QueryCriteria        VARCHAR2(1000),
   FilePath             VARCHAR2(2000),
   Status               NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_REPORTDOWNLOADMSG primary key (Id)
)
/