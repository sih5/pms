create sequence S_RepairMemberRightsDetail
/

/*==============================================================*/
/* Table: RepairMemberRightsDetail                              */
/*==============================================================*/
create table RepairMemberRightsDetail  (
   Id                   NUMBER(9)                       not null,
   RepairOrderId        NUMBER(9)                       not null,
   RightsCode           VARCHAR2(100),
   RightsItem           VARCHAR2(100),
   IsRights             NUMBER(1),
   BenefiType           VARCHAR2(30),
   BenefiUom            NUMBER(9),
   DiscountNum          NUMBER(19,4),
   Time                 NUMBER(9),
   constraint PK_REPAIRMEMBERRIGHTSDETAIL primary key (Id)
)
/


alter table DEFERREDDISCOUNTTYPE add EffectiveTime Date;
alter table DEFERREDDISCOUNTTYPE add EndTime Date;
alter table ExtendedWarrantyOrder add OtherAccessories VARCHAR2(2000);