create or replace view dealerpartsstockoutinrecord as
select PartRequisitionReturnCode as code,'维修领料单' as OutInBandtype,
NewPartsId as PartsId,NewPartsCode as PartsCode,NewPartsName as PartsName,
Quantity,CreateTime,dealerid
from PartRequisitionReturnBill
where RequisitionReturnStatus =1
union all
select pr.Code,'销售退货单' as OutInBandtype,
prlist.sparepartid as PartsId,prlist.sparepartcode as PartsCode,prlist.sparepartname as PartsName,
prlist.ApproveQuantity as Quantity，pr.CreateTime,pr.SubmitCompanyId as dealerid
from PartsSalesReturnBill pr
inner join PartsSalesReturnBillDetail prlist on pr.id =prlist.partssalesreturnbillid
where pr.Status = 3   --审批完成
union all
select po.Code,'零售订单' as OutInBandtype,
polist.PartsId,polist.PartsCode,polist.PartsName,
polist.Quantity,po.CreateTime,po.DealerId
from DealerPartsRetailOrder po
inner join DealerRetailOrderDetail polist on po.id =polist.dealerpartsretailorderid
where po.Status =2   --已审核
union all
select pc.Code,'外采单' as OutInBandtype,
pclist.partsid,pclist.PartsCode,pclist.partsname,
pclist.Quantity,pc.CreateTime,pc.Customercompanyid as dealerid
from PartsOuterPurchaseChange pc
inner join PartsOuterPurchaselist pclist on pc.id =pclist.PartsOuterPurchaseChangeId
where pc.Status =3   --生效
union all
select DI.Code,'盘亏' as OutInBandtype,
DIlist.SparePartId as partsid,DIlist.SparePartCode as PartsCode,DIlist.SparePartName as partsname,
(DIlist.CurrentStorage -DIlist.StorageAfterInventory ) as Quantity,DI.CreateTime,di.StorageCompanyId as dealerid
from DealerPartsInventoryBill DI
inner join DealerPartsInventoryDetail DIlist on DI.id =DIlist.DealerPartsInventoryId
where DI.Status =2   --已审核
and DIlist.StorageAfterInventory < DIlist.CurrentStorage
union all
--入库
select PartRequisitionReturnCode as code,'退料单' as OutInBandtype,
NewPartsId as PartId,NewPartsCode as PartsCode,NewPartsName as PartsName,Quantity,CreateTime,dealerid
from PartRequisitionReturnBill
where RequisitionReturnStatus =2
union all
select ps.Code,'发运单' as OutInBandtype,
pslist.SparePartId as partsid,pslist.SparePartCode as PartsCode,pslist.SparePartName as partsname,
pslist.ConfirmedAmount as Quantity,ps.CreateTime,ps.ReceivingCompanyId as dealerid
from PartsShippingOrder ps
inner join PartsShippingOrderDetail pslist on ps.id =pslist.PartsShippingOrderId
where ps.Status in (2,3)   --2收货确认,3回执确认
union all
select dpr.Code,'零售退货' as OutInBandtype,
dprlist.PartsId,dprlist.PartsCode,dprlist.PartsName,
dprlist.Quantity，dpr.CreateTime,dpr.dealerid
from DealerPartsSalesReturnBill dpr
inner join DealerRetailReturnBillDetail dprlist on dpr.id =dprlist.DealerPartsSalesReturnBillId
where dpr.Status =2   --已审核
union all
select DI.Code,'盘盈' as OutInBandtype，
DIlist.SparePartId as partsid,DIlist.SparePartCode as PartsCode,DIlist.SparePartName as partsname,
(DIlist.StorageAfterInventory -DIlist.CurrentStorage) as Quantity，DI.CreateTime,di.StorageCompanyId as dealerid
from DealerPartsInventoryBill DI
inner join DealerPartsInventoryDetail DIlist on DI.id =DIlist.DealerPartsInventoryId
where DI.Status =2   --已审核
and DIlist.StorageAfterInventory >DIlist.CurrentStorage
union all
select ps.Code,'电商订单' as OutInBandtype,
pslist.SparePartId as partsid,pslist.SparePartCode as PartsCode,pslist.SparePartName as partsname,
pslist.ConfirmedAmount as Quantity,ps.CreateTime,ps.ReceivingCompanyId as dealerid
from AgencyPartsShippingOrder ps
inner join APartsShippingOrderDetail pslist on ps.id =pslist.PartsShippingOrderId
where ps.Status in (2,3,4)--已发货、待收货、收货确认
