INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4117,4100, 2, 'AgencyRetailerOrder', 'Released', NULL, '代理库电商订单管理', '代理库电商订单管理', 'Client/DCS/Images/Menu/PartsSales/AgencyRetailerOrder.png', 16, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (8317,8300, 2, 'FDWarrantyMessageMng', 'Released', NULL, '福戴三包信息管理', '查询、维护福戴三包信息', 'Client/DCS/Images/Menu/PartsSales/FDWarrantyMessageMng.png', 16, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4118,4100, 2, 'AgentsPartsSalesOrder', 'Released', NULL, '随车行代理库配件销售订单管理', '用于分公司或代理库对本企业的订单处理', 'Client/DCS/Images/Menu/PartsSales/PartsSalesOrder.png', 17, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5409,5400, 2, 'AgentsPartsOutboundPlanQuery', 'Released', NULL, '代理库配件出库计划查询', '仓库人员查询辖内仓库出库计划单', 'Client/DCS/Images/Menu/PartsStocking/PartsOutboundPlanQuery.png', 12, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5410,5400, 2, 'AgentsPartsOutboundBillQuery', 'Released', NULL, '代理库配件出库单查询', '仓库人员查询辖内仓库出库单', 'Client/DCS/Images/Menu/PartsStocking/PartsOutboundBillQuery.png', 13, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5510,5500, 2, 'AgentsPartsShippingOrder', 'Released', NULL, '代理库配件发运单查询', '查询、合并导出工程车配件收货单', 'Client/DCS/Images/Menu/PartsStocking/AgentsPartsShippingOrder.png', 10, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (5511,5500, 2, 'AgentsLogisticCompany', 'Released', NULL, '代理库物流公司管理', '维护物流公司信息', 'Client/DCS/Images/Menu/Common/LogisticCompany.png', 11, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (4119,4100, 2, 'FollowingTrainPartsSalesOrder', 'Released', NULL, '随车行订单管理', '用于分发、导出订单处理', 'Client/DCS/Images/Menu/PartsSales/FollowingTrainPartsSalesOrder.png', 0, 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4117, 'AgencyRetailerOrder|Confirm', '确认', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4117, 'AgencyRetailerOrder|Terminate', '终止', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4117, 'AgencyRetailerOrder|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4117, 'AgencyRetailerOrder|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8317, 'Common|Import', '导入', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8317, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8317, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8317, 'Common|Abandon', '作废', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 8317, 'Common|Detail', '查看明细', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4118, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 4118, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5409, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5409, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5410, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5410, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5510, 'Common|Export', '导出', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5510, 'Common|MergeExport', '合并导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5511, 'Common|Add', '新增', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5511, 'Common|Edit', '修改', 2);
insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5511, 'Common|Abandon', '作废', 2);

INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 4119, 'Common|Distribute', '分发', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 4119, 'Common|Abandon', '作废', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 4119, 'Common|Export', '导出', 2);
INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, 4119, 'Common|DistributeExport', '分发导出', 2);

insert into action (ID, PAGEID, OPERATIONID, NAME, STATUS)values (S_Action.Nextval, 5511, 'Common|Detail', '查看详细', 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);
   
-- 确保所有按钮，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 1, Id, 2 
  FROM Action 
 WHERE Status = 2
   and not exists (select * from node where node.categoryid = action.Id and CategoryType=1)
   and exists (select * from page
                WHERE Type = 2 AND Status = 2 
                  and page.id= action.Pageid);