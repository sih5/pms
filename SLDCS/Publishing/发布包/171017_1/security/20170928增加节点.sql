INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13809,13800, 2, 'NotificationReport', 'Released', NULL, '公告查看信息（汇总表）', '查看公告情况汇总', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 26, 2);
INSERT INTO Page (Id, ParentId, Type, PageId, PageType, Parameter, Name, Description, Icon, Sequence, Status) VALUES (13810,13800, 2, 'NotificationReportDetail', 'Released', NULL, '公告查看信息（明细表）', '查看公告记录明细', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png', 27, 2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);