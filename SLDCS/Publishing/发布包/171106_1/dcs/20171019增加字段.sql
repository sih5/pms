alter table PartsShippingOrder add LogisticsEvaluation  NUMBER(9);
alter table PartsShippingOrder add EvaluationContent    VARCHAR2(500);

alter table ExtendedWarrantyOrder add MemberCode VARCHAR2(50);
