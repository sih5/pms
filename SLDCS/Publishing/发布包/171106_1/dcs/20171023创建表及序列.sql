/*==============================================================*/
/* Table: ExpressVehicleList                                    */
/*==============================================================*/
create table ExpressVehicleList  (
   Id                   NUMBER(9)                       not null,
   MarketABQualityInfoId NUMBER(9)                       not null,
   VIN                  VARCHAR2(50)                    not null,
   VehicleId            NUMBER(9)                       not null,
   VehicleCode          VARCHAR2(50)                    not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(50)                    not null,
   OutOfFactoryDate     DATE,
   SalesDate            DATE,
   FaultyMileage        NUMBER(15,6),
   FaultTime            DATE,
   BatchNumber          VARCHAR2(50),
   constraint PK_EXPRESSVEHICLELIST primary key (Id)
)
/

create sequence S_ExpressVehicleList
/
