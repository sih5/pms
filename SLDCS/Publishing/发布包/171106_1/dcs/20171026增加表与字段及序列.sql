alter table BonusPointsOrder add PartsOutboundBillId  NUMBER(9);
alter table BonusPointsOrder add PartsOutboundBillCode VARCHAR2(50);
alter table BonusPointsOrder add PartsInboundCheckBillId NUMBER(9);
alter table BonusPointsOrder add PartsInboundCheckBillCode VARCHAR2(50);
alter table BonusPointsOrder add PartsPurchaseSettleBillId NUMBER(9);
alter table BonusPointsOrder add PartsPurchaseSettleBillCode VARCHAR2(50);
alter table BonusPointsOrder add PurchaseSettlementStatus NUMBER(9);
alter table BonusPointsOrder add PartsPurchaseRtnSettleId NUMBER(9);
alter table BonusPointsOrder add PartsPurchaseRtnSettleCode VARCHAR2(50);
alter table BonusPointsOrder add PartsPurchaseRtnSettleStatus NUMBER(9);

alter table PartsSalesOrder add EcommerceMoney NUMBER(19,4);


alter table PartsPurReturnOrder add ServiceApplyCode VARCHAR2(50);
alter table PartsPurReturnOrder add ERPSourceOrderCode VARCHAR2(500);

alter table PartsPurchaseOrder add ERPSourceOrderCode VARCHAR2(500);
alter table PartsPurchaseOrder add VehiclePartsHandleOrderCode VARCHAR2(50);

alter table SupplierShippingOrder add ERPSourceOrderCode VARCHAR2(500);

alter table PartsSalesReturnBill add ERPSourceOrderCode VARCHAR2(500);

alter table PartsInboundCheckBill add ERPSourceOrderCode VARCHAR2(500);

alter table PartsInboundPlan add ERPSourceOrderCode VARCHAR2(500);

alter table PartsOutboundBill add ERPSourceOrderCode VARCHAR2(500);

alter table PartsOutboundPlan add ERPSourceOrderCode VARCHAR2(500);

alter table PartsTransferOrder add ERPSourceOrderCode VARCHAR2(500);

alter table AgencyPartsShippingOrder add ERPSourceOrderCode VARCHAR2(500);

alter table EcommerceFreightDisposal add ERPSourceOrderCode VARCHAR2(500);
alter table EcommerceFreightDisposal add VehiclePartsHandleOrderCode VARCHAR2(50);

alter table PartsShippingOrder add AppraiserName VARCHAR2(100);
alter table PartsShippingOrder add ERPSourceOrderCode VARCHAR2(500);

alter table ReturnFreightDisposal add ERPSourceOrderCode VARCHAR2(500);



alter table PartsBranch add OMSparePartMark NUMBER(9);

alter table SparePart add OMSparePartMark NUMBER(9);

alter table PartsPurchaseOrder add OMVehiclePartsHandleCode VARCHAR2(50);

alter table AgencyRetailerOrder add Remark VARCHAR2(200);

alter table PartsSalesOrder add OMVehiclePartsHandleCode VARCHAR2(50);

alter table PartsInboundCheckBill add EcommerceMoney NUMBER(19,4);

alter table PartsInboundPlan add EcommerceMoney NUMBER(19,4);

alter table PartsOutboundBill add EcommerceMoney NUMBER(19,4);

alter table PartsOutboundPlan add EcommerceMoney NUMBER(19,4);

alter table PartsTransferOrder add EcommerceMoney NUMBER(19,4);

alter table PartsShippingOrder add AppraiserPhone VARCHAR2(50);
alter table PartsShippingOrder add PackageEvaluation NUMBER(9);
alter table PartsShippingOrder add AuditEvaluation NUMBER(9);
alter table PartsShippingOrder add ServiceEvaluateStar NUMBER(9);
alter table PartsShippingOrder add images VARCHAR2(4000);
alter table PartsShippingOrder add EcommerceMoney NUMBER(19,4);




create sequence S_EcommerceFreightDisposal
/

create sequence S_ReturnFreightDisposal
/

/*==============================================================*/
/* Table: EcommerceFreightDisposal                              */
/*==============================================================*/
create table EcommerceFreightDisposal  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   VehiclePartsHandleOrderId NUMBER(9)                       not null,
   PartsOutboundBillId  NUMBER(9),
   PartsOutboundBillCode VARCHAR2(50),
   PartsInboundCheckBillId NUMBER(9),
   PartsInboundCheckBillCode VARCHAR2(50),
   PartsPurchaseSettleBillId NUMBER(9),
   PartsPurchaseSettleBillCode VARCHAR2(50),
   PurchaseSettlementStatus NUMBER(9),
   PartsSalesSettlementId NUMBER(9),
   PartsSalesSettlementCode VARCHAR2(50),
   SalesSettlementStatus NUMBER(9),
   FreightCharges       NUMBER(19,4),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ECOMMERCEFREIGHTDISPOSAL primary key (Id)
)
/

/*==============================================================*/
/* Table: ReturnFreightDisposal                                 */
/*==============================================================*/
create table ReturnFreightDisposal  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   ServiceApplyCode     VARCHAR2(50)                    not null,
   PartsOutboundBillId  NUMBER(9),
   PartsOutboundBillCode VARCHAR2(50),
   PartsInboundCheckBillId NUMBER(9),
   PartsInboundCheckBillCode VARCHAR2(50),
   PartsPurchaseSettleBillId NUMBER(9),
   PartsPurchaseSettleBillCode VARCHAR2(50),
   PurchaseSettlementStatus NUMBER(9),
   PartsPurchaseRtnSettleId NUMBER(9),
   PartsPurchaseRtnSettleCode VARCHAR2(50),
   PartsSalesSettlementId NUMBER(9),
   PartsSalesSettlementCode VARCHAR2(50),
   SalesSettlementStatus NUMBER(9),
   PartsSalesRtnSettlementId NUMBER(9),
   PartsSalesRtnSettlementCode VARCHAR2(50),
   FreightCharges       NUMBER(19,4),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_RETURNFREIGHTDISPOSAL primary key (Id)
)
/