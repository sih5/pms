CREATE OR REPLACE PROCEDURE UpdatePartsExchangeIsVisible is
  begin
    execute immediate 'alter table PartsExchange disable all triggers';

--更新是否可见=空的数据为可见
update partsexchange e set IsVisible = 1 where IsVisible is null;

--更新单条配件互换的数据为不可见
update partsexchange e set IsVisible = 0 where e.status <> 99 and IsVisible = 1 and e.exchangecode in (
select exchangecode from partsexchange e where e.status <> 99 group by e.exchangecode having count(*) = 1 )
and e.exchangecode not in (
select e1.exchangecode from (
select e.exchangecode,e.partid,g.exgroupcode from partsexchange e
inner join partsexchangegroup g on g.exchangecode = e.exchangecode
where e.status <> 99 and g.status <> 99 ) e1
inner join partsexchangegroup g1 on g1.exgroupcode = e1.exgroupcode and e1.exchangecode <> g1.exchangecode
where g1.status <> 99);

--更新非单条配件互换的数据为可见--一个识别号下有2个或以上配件
update partsexchange e set IsVisible = 1 where e.status <> 99 and IsVisible = 0 and (e.exchangecode not in (
select exchangecode from partsexchange e where e.status <> 99 group by e.exchangecode having count(*) = 1 ));

--更新非单条配件互换的数据为可见--一个识别号下只有一个配件但是和对应分组下的其他识别号互换的
update partsexchange e set IsVisible = 1 where e.status <> 99 and IsVisible = 0 and (e.exchangecode in (
select exchangecode from partsexchange e where e.status <> 99 group by e.exchangecode having count(*) = 1)
and e.exchangecode in (
select e1.exchangecode from (
select e.exchangecode,e.partid,g.exgroupcode from partsexchange e
inner join partsexchangegroup g on g.exchangecode = e.exchangecode
where e.status <> 99 and g.status <> 99 ) e1
inner join partsexchangegroup g1 on g1.exgroupcode = e1.exgroupcode and e1.exchangecode <> g1.exchangecode
where g1.status <> 99));

    commit;
    execute immediate 'alter table PartsExchange enable all triggers';
END UpdatePartsExchangeIsVisible;