
BEGIN
  dbms_scheduler.create_job('"自动更新互换号是否可见"',
                            job_type               => 'STORED_PROCEDURE',
                            job_action             => 'UpdatePartsExchangeIsVisible',
                            number_of_arguments    => 0,
                            start_date             => systimestamp at time zone '+8:00',
                            repeat_interval        => 'Freq=Minutely;Interval=30;ByHour=08, 09, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,23',
                            end_date               => NULL,
                            job_class              => '"DEFAULT_JOB_CLASS"',
                            enabled                => FALSE,
                            auto_drop              => FALSE,
                            comments               => NULL);
  dbms_scheduler.enable('"自动更新互换号是否可见"');
  COMMIT;
END;
