alter table PartsExchange add IsVisible NUMBER(9) default 1;

alter table BonusPointsOrder add OMVehiclePartsHandleCode VARCHAR2(50);

alter table IntegralClaimBill add RejectComment VARCHAR2(200);
alter table IntegralClaimBill add ApproveCommentHistory VARCHAR2(4000);

alter table RepairOrder add ExtendedWarrantyProductId NUMBER(9);
alter table RepairOrder add ExtendedWarrantyProductCode VARCHAR2(50);
alter table RepairOrder add ExtendedWarrantyProductName VARCHAR2(100);
alter table RepairOrder add ExtendedWarrantyMileage NUMBER(9);
alter table RepairOrder add ExtendedWarrantyTerm NUMBER(15,6);
alter table RepairOrder add ExtensionCoverage VARCHAR2(500);
alter table RepairOrder add TestResults VARCHAR2(4000);

alter table Warehouse add IsEcommerceWarehouse NUMBER(9);

create sequence S_ExtendedSparePartList
/

/*==============================================================*/
/* Table: ExtendedSparePartList                                 */
/*==============================================================*/
create table ExtendedSparePartList  (
   Id                   NUMBER(9)                       not null,
   SparePartId          NUMBER(9)                       not null,
   SparePartCode        VARCHAR2(50)                    not null,
   SparePartName        VARCHAR2(100)                   not null,
   ReqPart              VARCHAR2(160),
   constraint PK_EXTENDEDSPAREPARTLIST primary key (Id)
)
/