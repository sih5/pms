--3.4.5  代理库配件入库结果反馈（PWMS?PMS）主清单  （执行）

create table partsinboundcheckbill_inf  
(
  Id                          NUMBER(9) not null,
  Code                        VARCHAR2(50) not null,	
  PwmsCode                    VARCHAR2(50) not null,
  PostBackCode                VARCHAR2(50) not null, 
  WarehouseCode               VARCHAR2(50) not null,
  WarehouseName               VARCHAR2(100) not null,
  StorageCompanyCode          VARCHAR2(50) not null,
  StorageCompanyName          VARCHAR2(100) not null,
  BranchCode                  VARCHAR2(50) not null,
  BranchName                  VARCHAR2(100) not null,
  PartsSalesCategoryName      VARCHAR2(100) not null,
  PartsSalesCategoryCode      VARCHAR2(50) not null,
  CounterpartCompanyCode      VARCHAR2(50) not null,
  CounterpartCompanyName      VARCHAR2(100) not null,
  InboundType                 NUMBER(9) not null,
  CreatorName                 VARCHAR2(100),
  CreateTime                  DATE,
  RequestedDeliveryTime       DATe,
  FinishFlag                   NUMBER(9) ,
  OrderNum                    NUMBER(9),
  InfReadDateTime             DATE,
  InfCreateDateTime           DATE,
  InfProcessStatus            NUMBER(9) 
);

ALTER TABLE partsinboundcheckbill_inf
ADD CONSTRAINT INBOUNDPWMSCODEPBCODE UNIQUE (PwmsCode,PostBackCode);
--PwmsCode,PostBackCode 增加唯一性约束
--FinishFlag  1 完成, 其他取值  未完成
--OrderNum    本次之前的回传次数
--对应 parts_inbound_bill


--（执行）
create table partsinboundcheckbilld_inf
(
  Id                          NUMBER(9) not null,
  PostBackCode                VARCHAR2(50) not null, 	
  PwmsCode                    VARCHAR2(50) not null,
  Code                        VARCHAR2(50) not null,
  SparePartCode               VARCHAR2(50),
  SparePartName               VARCHAR2(100),
  PlannedAmount               NUMBER(9),
  ReceivedAmount              NUMBER(9)
);
--对应 parts_inbound_bill_detail 


--3.4.7  代理库配件出库结果反馈(PWMS?PMS)（主清单）（执行）

CREATE TABLE partsoutboundbill_inf
( 
  Id                        NUMBER(9) not null,
  Code                      VARCHAR2(50) not null,
  PwmsCode                  varchar2(50) not null,
  PostBackCode              varchar2(50) not null,
  WarehouseCode             VARCHAR2(50) not null,
  WarehouseName             VARCHAR2(100) not null,
  StorageCompanyCode        VARCHAR2(50) not null,
  StorageCompanyName        VARCHAR2(100) not null,
  BranchCode                VARCHAR2(50) not null,
  BranchName                VARCHAR2(100) not null,
  PartsSalesCategorycode    VARCHAR2(50) not null,
  PartsSalesCategoryName    VARCHAR2(100) not null,
  CounterpartCompanyCode    VARCHAR2(50) not null,
  CounterpartCompanyName    VARCHAR2(100) not null,
  ReceivingWarehouseCode    VARCHAR2(50) ,
  ReceivingWarehouseName    VARCHAR2(100) ,
  OutboundType              number(9),
  CreatorName               VARCHAR2(50) not null,
  CreateTime                DATE,
  RequestedDeliveryTime     DATe,
  FinishFlag                NUMBER(9) ,
  OrderNum                  NUMBER(9),
  InfReadDateTime             DATE,
  InfCreateDateTime           DATE,
  InfProcessStatus            NUMBER(9) 
);

ALTER TABLE partsoutboundbill_inf
ADD CONSTRAINT OUTBOUNDPWMSCODEPBCODE UNIQUE (PwmsCode,PostBackCode);
--对应 parts_outbound_bill

--PwmsCode,PostBackCode 增加唯一性约束


--（执行）
create table partsoutboundbilld_inf 
(
  Id                        NUMBER(9) not null,
  PostBackCode              varchar2(50) not null,
  PwmsCode                  VARCHAR2(50) not null,
  Code                      VARCHAR2(50) not null,
  SparePartCode             VARCHAR2(50),
  SparePartName             VARCHAR2(100),
  PlannedAmount             NUMBER(9),
  OutboundAmount            NUMBER(9)
);
--对应 parts_outbound_bill_detail 

--3.4.8  代理库库存调整申请 （PWMS?PMS）（主清单）（执行）
create table partsinventorybill_inf
(
  Id                        NUMBER(9) not null,
  
  PwmsCode                  varchar2(50) not null,
  Code                      varchar2(50),
  Branch                    varchar2(50),
  WarehouseCode             varchar2(50) not null,
  WarehouseName             varchar2(100) not null,
  StorageCompanyCode        varchar2(50)not null,
  StorageCompanyName        varchar2(100)not null,
  InventoryReason           varchar2(200),
  CreatorName               varchar2(50),
  CreateTime                date,
  WarehouseAreaCategory     varchar2(20),
  InfReadDateTime             DATE,
  InfCreateDateTime           DATE,
  InfProcessStatus            NUMBER(9) 
);
ALTER TABLE partsinventorybill_inf
ADD CONSTRAINT INVENTORYPWMSCODE UNIQUE (PwmsCode);

--对应 parts_inventorybill 

--WarehouseAreaCategory  取值范围--[BG, WT]  
--                                  BG -保管区  WT -问题区
--PwmsCode 增加唯一性约束


--（执行）
create table partsinventorybilld_inf
(
  Id                          NUMBER(9) not null,
  
  PwmsCode                    varchar2(50) not null,
  Code                        varchar2(50),
  SparePartCode               varchar2(50),
  SparePartName               varchar2(100),
  StorageDifference           number(9),
  Remark                      varchar2(200)
);
--对应 parts_inventorybill_detail



--3.4.10  代理库移库接口说明（执行）
create table partsshiftorder_inf 
(
  Id                                 NUMBER(9) not null,
  PwmsCode                           varchar2(50) not null , 
  Code                               varchar2(50) ,
  WarehouseCode                      varchar2(50) not null,
  WarehouseName                      varchar2(100) not null,
  StorageCompanyCode                 varchar2(50) not null,
  StorageCompanyName                 varchar2(100) not null,
  CreatorName                        varchar2(50),
  CreateTime                         date,
  SparePartCode                      varchar2(50),
  SparePartName                      varchar2(100),
  OriginalWarehouseAreaCategory      varchar2(20),
  DestWarehouseAreaCategory          varchar2(20),
  Quantity                           number(9),
  InfReadDateTime             DATE,
  InfCreateDateTime           DATE,
  InfProcessStatus            NUMBER(9) 
);

ALTER TABLE partsshiftorder_inf
ADD CONSTRAINT SHIFTORDERPWMSCODE UNIQUE (PwmsCode);
--对应  parts_shiftorder 

--OriginalWarehouseAreaCategory, DestWarehouseAreaCategory 取值范围--[BG, WT] 

--PwmsCode 增加唯一性约束

--日志接口（执行）
create table sync_wms_inout_errlog 
(
  Id                                NUMBER(9) not null,
  CreatedAt                         DATE not null,
  Message                           VARCHAR2(1000) not null,
  PwmsCode                          VARCHAR2(50) ,
  POSTBACKCODE                      VARCHAR2(50) ,
  PmsCode                           VARCHAR2(50) ,  
  TableName                         VARCHAR2(50) ,  
  TableRowId                        int, 
  DetailTableRowId                  int default 0
);
--ID增加序列
--DetailTableRowId  默认为0

create sequence s_sync_wms_inout_errlog
/