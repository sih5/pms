alter table Warehouse add PwmsInterface NUMBER(1);
alter table PartsInboundCheckBill add PWMSStorageCode VARCHAR2(50);
alter table PartsOutboundBill add PWMSOutboundCode VARCHAR2(50);

alter table ExtendedSparePartList add ExtendedWarrantyProductId NUMBER(9);