create sequence S_LinkAllocation
/

/*==============================================================*/
/* Table: LinkAllocation                                        */
/*==============================================================*/
create table LinkAllocation  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(100)                   not null,
   Link                 VARCHAR2(2000)                  not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_LINKALLOCATION primary key (Id)
)
/