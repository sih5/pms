alter table PartsSalesOrder add IsVehicleOutage NUMBER(1);

alter table PartsOutboundBillDetail add OriginalPrice NUMBER(19,4);
alter table PartsOutboundPlanDetail add OriginalPrice NUMBER(19,4);

alter table CAReconciliation add NoArrivalAdd NUMBER(19,4);
alter table CAReconciliation add NoArrivalReduce NUMBER(19,4);
alter table CAReconciliation add AfterAdjustmentAmount NUMBER(19,4);
alter table CAReconciliation add CustomerAccountAmount NUMBER(19,4);

alter table Branchstrategy add MInPackingAmountStrategy NUMBER(1);
alter table Company add IsSynchronization NUMBER(1);
alter table Dealer add IsSynchronization NUMBER(1);
alter table DealerServiceExt add IsSynchronization NUMBER(1);