--与王宏权确认，url是正式的，在测试库中执行没有问题

--查看照片
insert into LinkAllocation(id,Code,Link,CreatorId,CreatorName,CreateTime) values
(1,'MVSPicture','http://bfda-pics.ifoton.com.cn/pms/images.jsp?repaircode=',1,'admin',sysdate);
--查看视频
insert into LinkAllocation(id,Code,Link,CreatorId,CreatorName,CreateTime) values
(2,'MVSVideo','http://mvs.foton.com.cn:8089/ViewDetail.htm?flag=2'||'&'||'syscode= NEWPMS'||'&'||'code=',1,'admin',sysdate);
--查看外出轨迹
insert into LinkAllocation(id,Code,Link,CreatorId,CreatorName,CreateTime) values
(3,'MVSRoute','http://bfda.ifoton.com.cn/pms/track.jsp?repaircode=',1,'admin',sysdate);
--查看APP照片
insert into LinkAllocation(id,Code,Link,CreatorId,CreatorName,CreateTime) values
(4,'APPPicture','http://bfda-app.ifoton.com.cn/serviceProvider/getRepairBillImageByRepairId.action?id=',1,'admin',sysdate);
--查看APP轨迹
insert into LinkAllocation(id,Code,Link,CreatorId,CreatorName,CreateTime) values
(5,'APPRoute','http://bfda-app.ifoton.com.cn/serviceProvider/getRepairBillTrackByRepairId.action?id=',1,'admin',sysdate);
--车辆行驶轨迹查询
insert into LinkAllocation(id,Code,Link,CreatorId,CreatorName,CreateTime) values
(6,'VehicleRoute','http://saas.ifoton.com.cn/pages/nosecurity/pms/service_validation_map.jsp?token=1289250c1be2fc0f695cacebd03ff04',1,'admin',sysdate);
--故障代码查询
insert into LinkAllocation(id,Code,Link,CreatorId,CreatorName,CreateTime) values
(7,'FaultQuery','http://ov.ifoton.com.cn/pages/nosecurity/pms/fault_query.jsp?token=1289250c1be2fc0f695cacebd03ff04'||'&'||'vin=',1,'admin',sysdate);
