CREATE OR REPLACE PROCEDURE SYNCShiftOrder_dlk
as
       FNewCode                     varchar2(50);
       FComplateId                  NUMBER(9);
       FErrormsg                    VARCHAR2(200);
       FMyException                 EXCEPTION;
       FNewCodePSO                   varchar2(50);
       FPSOStorageCompanyCode       varchar2(50);
       FOraWarehouseAreaId          NUMBER(9);
       FOraWarehouseAreaCode        varchar2(50);
       FOraWarehouseAreaCategory    NUMBER(9);
       FDestWarehouseAreaId         NUMBER(9);
       FDestWarehouseAreaCode       varchar2(50);
       FDestWarehouseAreaCategory   NUMBER(9);
       FIPWarehouseId               NUMBER(9);
       FSparePartId                 NUMBER(9);
       FPartsStockId                NUMBER(9);
       FPartStockQuantity           NUMBER(9);
       FPARTSSHIFTORDERID           NUMBER(9);
       FDestPartsStockId            NUMBER(9);
       FDestWarehouseAreaCategoryId NUMBER(9);
       CURSOR Fpartsshiftorder_inf
  IS
    SELECT   *
    FROM partsshiftorder_inf
    WHERE partsshiftorder_inf.infprocessstatus  =0      ;
begin
      FNewCode := 'PM{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
      BEGIN
        SELECT Id
        INTO FComplateId
        FROM CodeTemplate
        WHERE name    = 'PartsShiftOrder'
        AND IsActived = 1;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        FErrormsg := '未找到名称为"PartsShiftOrder"的有效编码规则。';
        raise FMyException;
      WHEN OTHERS THEN
        raise;
      END;
      FOR C_Fpartsshiftorder_inf IN Fpartsshiftorder_inf LOOP
        begin
        --生成编号
         FPSOStorageCompanyCode:=C_Fpartsshiftorder_inf.Storagecompanycode;
         FNewCodePSO := regexp_replace(regexp_replace(FNewCode, '{CORPCODE}', FPSOStorageCompanyCode), '{SERIAL}', lpad(TO_CHAR(GetSerial(FComplateId, TRUNC(sysdate, 'DD'), FPSOStorageCompanyCode)), 6, '0'));
       --获取仓库Id
         begin
            select WAREHOUSE.id
              into FIPWarehouseId
              from WAREHOUSE
             inner join company
                on company.id = WAREHOUSE.storagecompanyid
             where warehouse.code = C_Fpartsshiftorder_inf.Warehousecode
               and company.code = C_Fpartsshiftorder_inf.Storagecompanycode;
          exception
            WHEN no_data_found THEN
              begin
                FErrormsg := 'pwms仓库不存在';
                raise FMyException;
              end;
            WHEN too_many_rows THEN
              BEGIN
                FErrormsg := 'pwms仓库不唯一';
                raise FMyException;
              END;
          end;
          --获取源仓库库位ID
          BEGIN
            SELECT WarehouseArea.Id,WarehouseArea.Code,WarehouseAreaCategory.Category
              INTO FOraWarehouseAreaId,FOraWarehouseAreaCode,FOraWarehouseAreaCategory
              FROM WarehouseArea
             INNER JOIN WarehouseAreaCategory
                ON WarehouseArea.AreaCategoryId = WarehouseAreaCategory.Id
             WHERE WarehouseArea.WarehouseId = FIPWarehouseId
               AND WarehouseArea.Areakind =  3
               AND WarehouseAreaCategory.Category = decode(C_Fpartsshiftorder_inf.Originalwarehouseareacategory,'BG',1,'WT',2)
               and WarehouseArea.status = 1;
          EXCEPTION
            WHEN no_data_found THEN
             begin
                FErrormsg := 'pwms仓库源库区的库位不存在';
                raise FMyException;
                end;
            WHEN too_many_rows THEN
              BEGIN
                FErrormsg := 'pwms仓库源库区的库位不唯一';
                raise FMyException;
              END;
          end;
          --获取目标仓库库位ID
          BEGIN
            SELECT WarehouseArea.Id,WarehouseArea.Code,WarehouseAreaCategory.Category,WarehouseArea.Areacategoryid
              INTO FDestWarehouseAreaId,FDestWarehouseAreaCode,FDestWarehouseAreaCategory,FDestWarehouseAreaCategoryId
              FROM WarehouseArea
             INNER JOIN WarehouseAreaCategory
                ON WarehouseArea.AreaCategoryId = WarehouseAreaCategory.Id
             WHERE WarehouseArea.WarehouseId = FIPWarehouseId
               AND WarehouseArea.Areakind =  3
               AND WarehouseAreaCategory.Category = decode(C_Fpartsshiftorder_inf.Destwarehouseareacategory,'BG',1,'WT',2)
               and WarehouseArea.status = 1;
          EXCEPTION
            WHEN no_data_found THEN
             begin
                FErrormsg := 'pwms仓库目标库区的库位不存在';
                raise FMyException;
                end;
            WHEN too_many_rows THEN
              BEGIN
                FErrormsg := 'pwms仓库目标库区的库位不唯一';
                raise FMyException;
              END;
           end;
           --获取配件信息
           begin
             select id
               into FSparePartId
               from sparepart
              where sparepart.code = C_Fpartsshiftorder_inf.Sparepartcode;
           exception
             when no_data_found then
               begin
                 FErrormsg := '配件信息不存在';
                 raise FMyException;
               end;
           end;
           --获取目标库区库存信息
           BEGIN
             SELECT PartsStock.Id
               INTO FPartsStockId
               FROM PartsStock
              WHERE PartsStock.WarehouseId = FIPWarehouseId
                AND PartsStock.PartId = FSparePartId
                AND PartsStock.WarehouseAreaId = FOraWarehouseAreaId;
           EXCEPTION
             WHEN no_data_found THEN
               begin
                 FErrormsg := 'pwms仓库源库区的库存不存在';
                 raise FMyException;
               end;
               WHEN too_many_rows THEN
               begin
                 FErrormsg := 'pwms仓库源库区的库存不唯一';
                 raise FMyException;
               end;
           END;
           BEGIN
             --查库存量
             SELECT NVL(PartsStock.Quantity, 0) Quantity
               INTO FPartStockQuantity
               FROM PartsStock
              WHERE PartsStock.Id = FPartsStockId;
             --校验库存量
             IF (NVL(FPartStockQuantity, 0) <
                C_Fpartsshiftorder_inf.Quantity) THEN
               BEGIN

                 /*FErrormsg := '配件库存不足，请检查！';--modify by mk 2017.07.19*/
                 FErrormsg := '配件源库区库存不足，请检查！配件图号：【' ||
                              C_Fpartsshiftorder_inf.Sparepartcode || '】';
                 raise FMyException;
               END;

             END IF;
           END;
           --减源仓库库存
           update PartsStock set Quantity=Quantity-C_Fpartsshiftorder_inf.Quantity,PartsStock.Modifierid=1
                ,PartsStock.Modifiername='PWMSPMSInterface',PartsStock.Modifytime=sysdate
            WHERE PartsStock.Id = FPartsStockId;
           --获取目标库区库存信息
           BEGIN
             SELECT PartsStock.Id
               INTO FDestPartsStockId
               FROM PartsStock
              WHERE PartsStock.WarehouseId = FIPWarehouseId
                AND PartsStock.PartId = FSparePartId
                AND PartsStock.WarehouseAreaId = FDestWarehouseAreaId;
           EXCEPTION
               WHEN too_many_rows THEN
               begin
                 FErrormsg := 'pwms仓库目标库区的库存不唯一';
                 raise FMyException;
               end;
           END;
           if FDestPartsStockId is not null then
             begin
                update PartsStock set Quantity=Quantity+C_Fpartsshiftorder_inf.Quantity,PartsStock.Modifierid=1
                ,PartsStock.Modifiername='PWMSPMSInterface',PartsStock.Modifytime=sysdate
                WHERE PartsStock.Id = FDestPartsStockId;
             end;
               else
             begin
                INSERT INTO PartsStock
                  (Id,
                   WarehouseId,
                   StorageCompanyId,
                   StorageCompanyType,
                   BranchId,
                   WarehouseAreaId,
                   PartId,
                   WarehouseAreaCategoryId,
                   Quantity,
                   CreatorId,
                   CreateTime,
                   CreatorName)
                VALUES
                  (s_partsstock.nextval,
                   FIPWarehouseId,
                   (select id
                      from company
                     where code = C_Fpartsshiftorder_inf.Storagecompanycode),
                   (select type
                      from company
                     where code = C_Fpartsshiftorder_inf.Storagecompanycode),
                   (select branchid
                      from warehouse
                     where warehouse.id = FIPWarehouseId),
                   FDestWarehouseAreaId,
                   FSparepartId,
                   FDestWarehouseAreaCategoryId,
                   C_Fpartsshiftorder_inf.Quantity,
                   1,
                   sysdate,
                   'PWMSPMSInterface');
               end;
             end if;
           FPARTSSHIFTORDERID:=s_partsshiftorder.nextval;
           insert into PARTSSHIFTORDER
             (ID,
              CODE,
              BRANCHID,
              WAREHOUSEID,
              WAREHOUSECODE,
              WAREHOUSENAME,
              STORAGECOMPANYID,
              STORAGECOMPANYCODE,
              STORAGECOMPANYNAME,
              STORAGECOMPANYTYPE,
              STATUS,
              TYPE,
              CREATORID,
              CREATORNAME,
              CREATETIME,
              Remark)
           values
             (FPARTSSHIFTORDERID,
              FNewCodePSO,
              (select branchid
                 from warehouse
                where warehouse.id = FIPWarehouseId),
              FIPWarehouseId,
              C_Fpartsshiftorder_inf.Warehousecode,
              C_Fpartsshiftorder_inf.Warehousename,
              (select id
                 from company
                where code = C_Fpartsshiftorder_inf.Storagecompanycode),
              C_Fpartsshiftorder_inf.Storagecompanycode,
              C_Fpartsshiftorder_inf.Storagecompanyname,
              (select type
                 from company
                where code = C_Fpartsshiftorder_inf.Storagecompanycode),
              1,
              2,
              1,
              'PWMSPMSInterface',
              SYSDATE,
              '');
           insert into PARTSSHIFTORDERDETAIL
           (
            ID,
            PARTSSHIFTORDERID,
            SPAREPARTID,
            SPAREPARTCODE,
            SPAREPARTNAME,
            ORIGINALWAREHOUSEAREAID,
            ORIGINALWAREHOUSEAREACODE,
            ORIGINALWAREHOUSEAREACATEGORY,
            DESTWAREHOUSEAREAID,
            DESTWAREHOUSEAREACODE,
            DESTWAREHOUSEAREACATEGORY,
            BATCHNUMBER,
            QUANTITY
            )
            values
            (
              s_PARTSSHIFTORDERDETAIL.Nextval,
              FPARTSSHIFTORDERID,
              FSparePartId,
              C_Fpartsshiftorder_inf.Sparepartcode,
              C_Fpartsshiftorder_inf.Sparepartname,
              FOraWarehouseAreaId,
              FOraWarehouseAreaCode,
              FOraWarehouseAreaCategory,
              FDestWarehouseAreaId,
              FDestWarehouseAreaCode,
              FDestWarehouseAreaCategory,
              '',
             C_Fpartsshiftorder_inf.Quantity
            );
          update partsshiftorder_inf set partsshiftorder_inf.infprocessstatus=1,
          partsshiftorder_inf.code=FNewCodePSO
          where   partsshiftorder_inf.pwmscode=C_Fpartsshiftorder_inf.pwmscode;
         exception
           when FMyException then
            begin
              insert INTO sync_wms_inout_errlog
                (ID,
                 CREATEDAT,
                 Message,
                 PwmsCode,
                 PmsCode,
                 TableName,
                 TABLEROWID,
                 DETAILTABLEROWID)
              VALUES
                (s_sync_wms_inout_errlog.NEXTVAL,
                 sysdate,
                 FErrormsg,
                 C_Fpartsshiftorder_inf.Pwmscode,
                 C_Fpartsshiftorder_inf.Code,
                 'parts_shiftorder',
                 C_Fpartsshiftorder_inf.id,
                 0);
              update partsshiftorder_inf
                 set partsshiftorder_inf.infprocessstatus = 2
               where partsshiftorder_inf.pwmscode = C_Fpartsshiftorder_inf.pwmscode;
            end;
           when others then
           begin
             FErrormsg := sqlerrm;
             insert INTO sync_wms_inout_errlog
               (ID,
                CREATEDAT,
                Message,
                PwmsCode,
                PmsCode,
                TableName,
                TABLEROWID,
                DETAILTABLEROWID)
             VALUES
               (s_sync_wms_inout_errlog.NEXTVAL,
                sysdate,
                FErrormsg,
                C_Fpartsshiftorder_inf.Pwmscode,
                C_Fpartsshiftorder_inf.Code,
                'parts_shiftorder',
                C_Fpartsshiftorder_inf.id,
                0);
             update partsshiftorder_inf
                set partsshiftorder_inf.infprocessstatus = 2
              where partsshiftorder_inf.pwmscode = C_Fpartsshiftorder_inf.pwmscode;
           end;
           end;
        END LOOP;

end;
