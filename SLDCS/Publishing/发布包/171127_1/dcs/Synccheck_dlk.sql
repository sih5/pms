CREATE OR REPLACE PROCEDURE Synccheck_dlk
AS
  FDateFormat                 DATE;
  fuser_def2                  VARCHAR2(50);
  FDateStr                    VARCHAR2(8);
  FSerialLength               INTEGER;
  FComplateId                 INTEGER;
  FNewCode                    VARCHAR2(50);
  FNewCodePIB                 VARCHAR2(50);
  FPIBBranchId                NUMBER(9);
  FPIBWarehouseId             NUMBER(9);
  FPIBWarehouseCode           VARCHAR2(50);
  FPIBWarehouseName           VARCHAR2(50);
  FPIBStorageCompanyId        NUMBER(9);
  FPIBStorageCompanyCode      VARCHAR2(50);
  FPIBStorageCompanyName      VARCHAR2(50);
  FPIBStorageCompanyType      NUMBER(9);
  FErrorMsg                   VARCHAR2(1000);
  FPartsInventoryBillId       NUMBER(9);
  FPartsInventoryBillPartId   NUMBER(9);
  FPartsInventoryBillPartCode VARCHAR2(50);
  FPartsInventoryBillPartName VARCHAR2(50);
  FPIBWarehouseAreaId         NUMBER(9);
  FPIBWarehouseAreaCode       VARCHAR2(50);
  FAreaCategoryId             NUMBER(9);
  FPartsStockId               NUMBER(9);
  FNewPartsStockId            NUMBER(9);
  FPartStockQuantity          NUMBER(9);
  FPlannedPrice               NUMBER(19, 4);
  FPIBPARTSSALESCATEGORYID    NUMBER(9);
  FPartsInventoryDetailId     NUMBER(9);
  FSYNCWMSINOUTLOGINFOId      NUMBER(9);
  FMyException                EXCEPTION;
  FDirection                  VARCHAR2(50);
  FBRAND                      VARCHAR2(50);
  FWAREHOSUE                  VARCHAR2(50);
  FMEMO                       VARCHAR2(50);
  FCREATENAME                 VARCHAR2(50);
  FDETAILID                   NUMBER(9);

  CURSOR Ft_inventory_adjustment
  IS
    SELECT *
    FROM partsinventorybill_inf
    where InfProcessStatus = 0 ORDER BY code,pwmscode;

  CURSOR Ft_inventory_adjustmentdt(FPARENTID VARCHAR2)
  IS
    SELECT partsinventorybilld_inf.*
    FROM partsinventorybilld_inf
    WHERE PwmsCode=FPARENTID;
BEGIN
  FDateFormat   := TRUNC(sysdate, 'DD');
  FDateStr      := TO_CHAR(sysdate, 'yyyymmdd');
  FSerialLength := 6;
  FNewCode      := 'PI{CORPCODE}' || FDateStr || '{SERIAL}';
  BEGIN
    SELECT Id
    INTO FComplateId
    FROM CodeTemplate
    WHERE name    = 'PartsInventoryBill'
    AND IsActived = 1;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    raise_application_error(-20001, '未找到名称为"PartsInventoryBill"的有效编码规则。');
  WHEN OTHERS THEN
    raise;
  END;
  FOR C_t_inventory_adjustment IN Ft_inventory_adjustment
  LOOP
    BEGIN
      fuser_def2:=C_t_inventory_adjustment.pwmscode;
      begin
      select distinct WarehouseAreaCategory ,Warehousecode,InventoryReason,CreatorName
        into FDirection,FWAREHOSUE,Fmemo,Fcreatename from partsinventorybill_inf
       where partsinventorybill_inf.pwmscode =
             C_t_inventory_adjustment.pwmscode and InfProcessStatus=0;
           EXCEPTION
        WHEN no_data_found THEN
          begin
             FErrormsg:='盘点单号未找到,单号:' ||C_t_inventory_adjustment.pwmscode;
             raise FMyException;
             end;
             when too_many_rows then
               FErrorMsg:='盘点单号对应的仓库、品牌和盘点方向,备注不唯一,单号:' ||C_t_inventory_adjustment.pwmscode;
             raise FMyException;
             end;

      FPartsInventoryBillId   := s_PartsInventoryBill.NEXTVAL;

      BEGIN
          SELECT w.branchid,
            w.Id,
            w.Code,
            w.Name,
            w.StorageCompanyId,
            c.Code,
            c.Name,
            w.StorageCompanyType,
            PSC.Id
          INTO FPIBBranchId,
            FPIBWarehouseId,
            FPIBWarehouseCode,
            FPIBWarehouseName,
            FPIBStorageCompanyId,
            FPIBStorageCompanyCode,
            FPIBStorageCompanyName,
            FPIBStorageCompanyType,
            FPIBPARTSSALESCATEGORYID
          FROM warehouse w
          INNER JOIN salesunitaffiwarehouse suaw
          ON w.id=suaw.warehouseid
          INNER JOIN salesunit su
          ON suaw.salesunitid=su.ID
          INNER JOIN Company c
          ON w.StorageCompanyId = c.ID
          /*AND c.TYPE            = 1*/
          INNER JOIN partssalescategory psc
          ON su.partssalescategoryid=psc.id
          AND psc.Status            = 1
          /*INNER JOIN t_warehouse tw
          ON w.storagecenter  =tw.pmsstoragecenterid*/
          WHERE w.pwmsinterface=1
          AND w.code=FWAREHOSUE;

        EXCEPTION
        WHEN no_data_found THEN
          BEGIN
            FErrormsg := '找不到仓库,PWMS仓库编号：' ||FWAREHOSUE || '品牌名称：' || FBRAND;
            raise FMyException;
          END;
        WHEN too_many_rows THEN
          BEGIN
            FErrormsg := '存在多个仓库,PWMS仓库编号：' ||FWAREHOSUE || '品牌名称：' ||FBRAND;
            raise FMyException;
          END;
        END;
        FNewCodePIB := regexp_replace(regexp_replace(FNewCode, '{CORPCODE}', FPIBStorageCompanyCode), '{SERIAL}', lpad(TO_CHAR(GetSerial(FComplateId, FDateFormat, FPIBStorageCompanyCode)), FSerialLength, '0'));
        INSERT INTO PartsInventoryBill
          (Id,
           Code,
           BranchId,
           WarehouseAreaCategory,
           WarehouseId,
           WarehouseCode,
           WarehouseName,
           StorageCompanyId,
           StorageCompanyCode,
           StorageCompanyName,
           StorageCompanyType,
           InventoryReason,
           Remark,
           Status,
           CreatorId,
           CreatorName,
           CreateTime,
           ModifierId,
           ModifierName,
           ModifyTime,
           ResultInputOperatorId,
           ResultInputOperatorName,
           ResultInputTime)
        VALUES
          (FPartsInventoryBillId,
           FNewCodePIB,
           FPIBBranchId,
           decode(FDirection, 'WT', 2, 'BG', 1),
           FPIBWarehouseId,
           FPIBWarehouseCode,
           FPIBWarehouseName,
           FPIBStorageCompanyId,
           FPIBStorageCompanyCode,
           FPIBStorageCompanyName,
           FPIBStorageCompanyType,
           FMEMO,
           FMEMO,
           2,
           1,
           FCREATENAME,
           sysdate,
           1,
           FCREATENAME,
           sysdate,
           1,
           FCREATENAME,
           sysdate);


     for C_t_inventory_adjustmentdt IN Ft_inventory_adjustmentdt(C_t_inventory_adjustment.pwmscode)
    loop begin


      FErrorMsg               := '';
      FPartsStockId:=null;
      FPartStockQuantity:=null;
      FPartsInventoryDetailId := S_PartsInventoryDetail.NEXTVAL;

      BEGIN

        BEGIN
          SELECT SparePart.Id,
            SparePart.Code,
            SparePart.Name
          INTO FPartsInventoryBillPartId,
            FPartsInventoryBillPartCode,
            FPartsInventoryBillPartName
          FROM SparePart
          WHERE SparePart.Code = C_t_inventory_adjustmentdt.SparePartCode;
        EXCEPTION
        WHEN no_data_found THEN
          FPartsInventoryBillPartId := NULL;
        WHEN too_many_rows THEN
          BEGIN
            FErrormsg := '根据配件编号查到多个配件,配件编号：' ||C_t_inventory_adjustmentdt.SparePartCode;
            FDETAILID :=  C_t_inventory_adjustmentdt.id;
            raise FMyException;
          END;
        END;

        if nvl(C_t_inventory_adjustmentdt.StorageDifference,0) = 0 then
          begin
            FErrormsg := 'PWMS传递的盘点数量为0！配件编号：【'||C_t_inventory_adjustmentdt.SparePartCode||'】';
            FDETAILID :=  C_t_inventory_adjustmentdt.id;
            raise FMyException;
          end;
        end if;

        IF FPartsInventoryBillPartId IS NOT NULL THEN
          BEGIN
            --查库位

          /*  IF FPIBWarehouseAreaId IS NOT NULL THEN
              BEGIN*/
                --查库存

                BEGIN
                  IF UPPER(trim(C_t_inventory_adjustment.WarehouseAreaCategory)) = 'WT' THEN
                    BEGIN
                      if C_t_inventory_adjustmentdt.StorageDifference > 0 then
                        begin
                          FErrormsg := '问题区应盘亏，数量应小于0！ 配件编号：【'||C_t_inventory_adjustmentdt.SparePartCode||'】';
                          FDETAILID :=  C_t_inventory_adjustmentdt.id;
                          raise FMyException;
                        end;
                      end if;

                      BEGIN
                        SELECT WarehouseArea.Id,
                               WarehouseArea.Code,
                               WarehouseArea.Areacategoryid
                          INTO FPIBWarehouseAreaId,
                               FPIBWarehouseAreaCode,
                               FAreaCategoryId
                          FROM WarehouseArea
                         INNER JOIN WarehouseAreaCategory
                            ON WarehouseArea.AreaCategoryId =
                               WarehouseAreaCategory.Id
                         WHERE WarehouseArea.WarehouseId = FPIBWarehouseId
                           AND WarehouseArea.AreaKind = 3
                           AND WarehouseArea.Status = 1
                           AND WarehouseAreaCategory.Category = 2;
                      EXCEPTION
                        WHEN no_data_found THEN
                           FErrormsg := '仓库库区库位不存在，请检查！';
                           FDETAILID :=  C_t_inventory_adjustmentdt.id;
                           raise FMyException;
                        WHEN too_many_rows THEN
                          BEGIN
                            FErrormsg := '存在多个库位,仓库Id：' || FPIBWarehouseId;
                            FDETAILID :=  C_t_inventory_adjustmentdt.id;
                            raise FMyException;
                          END;
                      END;

                      BEGIN
                        SELECT PartsStock.Id
                          INTO FPartsStockId
                          FROM PartsStock
                         WHERE PartsStock.WarehouseId = FPIBWarehouseId
                           AND PartsStock.PartId = FPartsInventoryBillPartId
                           AND PartsStock.WarehouseAreaId = FPIBWarehouseAreaId;
                      EXCEPTION
                        WHEN no_data_found THEN
                          FPartsStockId := NULL;
                      END;
                      IF FPartsStockId IS NOT NULL THEN
                        BEGIN
                          --查库存量
                          SELECT NVL(PartsStock.Quantity, 0) StorageDifference
                          INTO FPartStockQuantity
                          FROM PartsStock
                          WHERE PartsStock.Id = FPartsStockId;
                          --校验库存量
                         /* IF (NVL(FPartStockQuantity,0) < C_t_inventory_adjustmentdt.StorageDifference) THEN
                            BEGIN

                               \*FErrormsg := '配件库存不足，请检查！';--modify by mk 2017.07.19*\
                              FErrormsg := '配件库存不足，请检查！配件图号：【'||C_t_inventory_adjustmentdt.SparePartCode||'】';
                              raise FMyException;
                            END;

                          END IF;*/
                        END;
                      /*ELSE

                        BEGIN
                              \*FErrormsg := '配件库存不足，请检查！';--modify by mk 2017.07.19*\
                             FErrormsg := '配件库存不存在，请检查！配件图号：【'||C_t_inventory_adjustmentdt.SparePartCode||'】';
                              raise FMyException;
                            END;*/
                      END IF;
                      --查计划价
                      BEGIN
                        SELECT NVL(PartsPlannedPrice.PlannedPrice, 0)
                        INTO FPlannedPrice
                        FROM PartsPlannedPrice
                        WHERE PartsPlannedPrice.SparePartId        = FPartsInventoryBillPartId
                        AND PartsPlannedPrice.PartsSalesCategoryId = FPIBPARTSSALESCATEGORYID;
                      EXCEPTION
                      WHEN NO_DATA_FOUND THEN
                        BEGIN
                          /*FErrormsg := '配件计划价不存在，请检查！';--modify by mk 2017.07.19*/
                          FErrormsg := '配件计划价不存在，请检查！配件图号：【'||C_t_inventory_adjustmentdt.SparePartCode||'】';
                          FDETAILID :=  C_t_inventory_adjustmentdt.id;
                          raise FMyException;
                        END;
                      END;
                      INSERT
                      INTO PartsInventoryDetail
                        (
                          Id,
                          PartsInventoryBillId,
                          SparePartId,
                          SparePartCode,
                          SparePartName,
                          WarehouseAreaId,
                          WarehouseAreaCode,
                          CurrentStorage,
                          StorageAfterInventory,
                          Ifcover,
                          StorageDifference,
                          CostPrice,
                          AmountDifference
                        )
                        VALUES
                        (
                          FPartsInventoryDetailId,
                          FPartsInventoryBillId,
                          FPartsInventoryBillPartId,
                          FPartsInventoryBillPartCode,
                          FPartsInventoryBillPartName,
                          FPIBWarehouseAreaId,
                          FPIBWarehouseAreaCode,
                          NVL(FPartStockQuantity,0),
                          NVL(FPartStockQuantity,0) - NVL(C_t_inventory_adjustmentdt.StorageDifference, 0),
                          1,
                          -C_t_inventory_adjustmentdt.StorageDifference,
                          FPlannedPrice,
                          FPlannedPrice * -NVL(C_t_inventory_adjustmentdt.StorageDifference, 0)
                        );
                      UPDATE PartsInventoryBill
                      SET AmountDifference = FPlannedPrice * -NVL(C_t_inventory_adjustmentdt.StorageDifference, 0)
                      WHERE Id             = FPartsInventoryBillId;
                      --因为当前PMS系统都是无批次管理所以不生成 配件库存 批次明细
                    END;
                  END IF;
                  IF UPPER(trim(C_t_inventory_adjustment.WarehouseAreaCategory)) = 'BG' THEN
                    BEGIN
                      if C_t_inventory_adjustmentdt.StorageDifference < 0 then
                        begin
                          FErrormsg := '保管区应盘盈，数量应大于0！ 配件编号：【'||C_t_inventory_adjustmentdt.SparePartCode||'】';
                          FDETAILID :=  C_t_inventory_adjustmentdt.id;
                          raise FMyException;
                        end;
                      end if;

                      BEGIN
                        SELECT WarehouseArea.Id,
                               WarehouseArea.Code,
                               WarehouseArea.Areacategoryid
                          INTO FPIBWarehouseAreaId,
                               FPIBWarehouseAreaCode,
                               FAreaCategoryId
                          FROM WarehouseArea
                         INNER JOIN WarehouseAreaCategory
                            ON WarehouseArea.AreaCategoryId =
                               WarehouseAreaCategory.Id
                         WHERE WarehouseArea.WarehouseId = FPIBWarehouseId
                           AND WarehouseArea.AreaKind = 3
                           AND WarehouseArea.Status = 1
                           AND WarehouseAreaCategory.Category = 1;
                      EXCEPTION
                        WHEN no_data_found THEN
                           FErrormsg := '仓库库区库位不存在，请检查！';
                           FDETAILID :=  C_t_inventory_adjustmentdt.id;
                           raise FMyException;
                        WHEN too_many_rows THEN
                          BEGIN
                            FErrormsg := '存在多个库位,仓库Id：' || FPIBWarehouseId;
                            FDETAILID :=  C_t_inventory_adjustmentdt.id;
                            raise FMyException;
                          END;
                      END;

                      BEGIN
                        SELECT PartsStock.Id
                          INTO FPartsStockId
                          FROM PartsStock
                         WHERE PartsStock.WarehouseId = FPIBWarehouseId
                           AND PartsStock.PartId = FPartsInventoryBillPartId
                           AND PartsStock.WarehouseAreaId = FPIBWarehouseAreaId;
                      EXCEPTION
                        WHEN no_data_found THEN
                          FPartsStockId := NULL;
                      END;
                      IF FPartsStockId IS NOT NULL THEN
                        BEGIN
                         --查库存量
                          SELECT NVL(PartsStock.Quantity, 0) StorageDifference
                          INTO FPartStockQuantity
                          FROM PartsStock
                          WHERE PartsStock.ID = FPartsStockId;


                        END;

                      END IF;
                      BEGIN
                        SELECT NVL(PartsPlannedPrice.PlannedPrice, 0)
                        INTO FPlannedPrice
                        FROM PartsPlannedPrice
                        WHERE PartsPlannedPrice.SparePartId        = FPartsInventoryBillPartId
                        AND PartsPlannedPrice.Partssalescategoryid = FPIBPARTSSALESCATEGORYID;
                      EXCEPTION
                      WHEN NO_DATA_FOUND THEN
                        BEGIN
                          /*FErrormsg := '配件计划价不存在，请检查！';--modify by mk 2017.07.19*/
                          FErrormsg := '配件计划价不存在，请检查！配件图号：【'||C_t_inventory_adjustmentdt.SparePartCode||'】';
                          FDETAILID :=  C_t_inventory_adjustmentdt.id;
                          raise FMyException;
                        END;
                      END;
                      INSERT
                      INTO PartsInventoryDetail
                        (
                          Id,
                          PartsInventoryBillId,
                          SparePartId,
                          SparePartCode,
                          SparePartName,
                          WarehouseAreaId,
                          WarehouseAreaCode,
                          CurrentStorage,
                          StorageAfterInventory,
                          Ifcover,
                          StorageDifference,
                          CostPrice,
                          AmountDifference
                        )
                        VALUES
                        (
                          FPartsInventoryDetailId,
                          FPartsInventoryBillId,
                          FPartsInventoryBillPartId,
                          FPartsInventoryBillPartCode,
                          FPartsInventoryBillPartName,
                          FPIBWarehouseAreaId,
                          FPIBWarehouseAreaCode,
                          NVL(FPartStockQuantity,0),
                          NVL(FPartStockQuantity,0) + NVL(C_t_inventory_adjustmentdt.StorageDifference, 0),
                          1,
                          NVL(C_t_inventory_adjustmentdt.StorageDifference, 0),
                          FPlannedPrice,
                          FPlannedPrice * NVL(C_t_inventory_adjustmentdt.StorageDifference, 0)
                        );
                      UPDATE PartsInventoryBill
                      SET AmountDifference = FPlannedPrice * NVL(C_t_inventory_adjustmentdt.StorageDifference, 0)
                      WHERE Id             = FPartsInventoryBillId;
                      --因为当前PMS系统都是无批次管理所以不生成 配件库存批次明细
                    END;
                  END IF;
                  IF (Upper(trim(C_t_inventory_adjustment.WarehouseAreaCategory)) <> 'WT') AND (uPPER(trim(C_t_inventory_adjustment.WarehouseAreaCategory)) <> 'BG') THEN
                    BEGIN
                      FErrormsg := '库存调整方向非法，请检查！';
                      FDETAILID :=  C_t_inventory_adjustmentdt.id;
                      raise FMyException;
                    END;
                  END IF;
                END;
             /* END;
            ELSE
              BEGIN
                FErrormsg := '仓库库区库位不存在，请检查！';
                raise FMyException;
              END;
            END IF;*/
          END;
        ELSE
          BEGIN
            /*FErrormsg := '盘点单配件不存在，请检查！';--modify by mk 2017.07.19*/
            FErrormsg := '盘点单配件不存在，请检查！配件图号：【'||C_t_inventory_adjustmentdt.SparePartCode||'】';
            FDETAILID :=  C_t_inventory_adjustmentdt.id;
            raise FMyException;
          END;
        END IF;
      END;
       end ;
      end loop;

      UPDATE partsinventorybill_inf
         SET partsinventorybill_inf.InfProcessStatus = 1,
             partsinventorybill_inf.code             = FNewCodePIB
       WHERE partsinventorybill_inf.pwmscode =
             C_t_inventory_adjustment.pwmscode
         and InfProcessStatus = 0;

      COMMIT;

      EXCEPTION
    WHEN FMyException THEN
      BEGIN
        ROLLBACK;
        INSERT INTO sync_wms_inout_errlog
          (ID,
           CreatedAt,
           Message,
           PwmsCode,
           PmsCode,
           TableName,
           TABLEROWID,
           DETAILTABLEROWID)
        values
          (s_sync_wms_inout_errlog.NEXTVAL,
           sysdate,
           FErrormsg,
           C_t_inventory_adjustment.Pwmscode,
           C_t_inventory_adjustment.CODE,
           'parts_inventorybill',
           C_t_inventory_adjustment.Id,
           nvl(FDETAILID, 0));

        UPDATE partsinventorybill_inf
        SET partsinventorybill_inf.InfProcessStatus = 2
        WHERE partsinventorybill_inf.pwmscode     = C_t_inventory_adjustment.pwmscode  and InfProcessStatus=0;

      END;
    WHEN OTHERS THEN
      BEGIN
        ROLLBACK;
        FErrorMsg := sqlerrm;
        INSERT INTO sync_wms_inout_errlog
          (ID,
           CreatedAt,
           Message,
           PwmsCode,
           PmsCode,
           TableName,
           TABLEROWID,
           DETAILTABLEROWID)
        values
          (s_sync_wms_inout_errlog.NEXTVAL,
           sysdate,
           FErrormsg,
           C_t_inventory_adjustment.Pwmscode,
           C_t_inventory_adjustment.CODE,
           'parts_inventorybill',
           C_t_inventory_adjustment.Id,
           nvl(FDETAILID, 0));

        UPDATE partsinventorybill_inf
        SET partsinventorybill_inf.InfProcessStatus = 2
        WHERE partsinventorybill_inf.pwmscode     = C_t_inventory_adjustment.pwmscode and InfProcessStatus=0;
      END;
    END;
  END LOOP;
END Synccheck_dlk;
