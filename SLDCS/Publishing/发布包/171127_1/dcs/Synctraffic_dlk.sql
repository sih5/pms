CREATE OR REPLACE PROCEDURE Synctraffic_dlk
AS
  Vdetailno             NUMBER(9, 0);
  Vdetailidx             NUMBER(9, 0);
  Vi                    NUMBER(9, 0);
  FWarehouseAreaId      NUMBER(9);
  FWarehouseAreaCode    VARCHAR2(40);
  FNewcodepso           VARCHAR2(40);
  FNewcodepsoF          VARCHAR2(40);
  FNewcodeckF           VARCHAR2(40);
  FNewcodeck            VARCHAR2(40);
  FOutplanid            VARCHAR2(40);
  FStorageCompanyId     NUMBER(9, 0);
  FPartsSalesCategoryId NUMBER(9, 0);
  FOutwarehouseid       VARCHAR2(40);
  FFinishstatus         NUMBER(9, 0);
  FIsAllFinished          NUMBER(9);
  FErrormsg             VARCHAR2(1000);
  FShiptypecodevalue    NUMBER(9);
  FFinishqty            NUMBER(9, 0);
  FPlanqty              NUMBER(9, 0);
  FSparePartId          NUMBER(9, 0);
  FBranchid             NUMBER(9, 0);
  FCostPrice            NUMBER(19, 4);
  FAmount               NUMBER(19, 4);
  FLeftAmount           NUMBER(19, 4);
  FEstimateddueamount   NUMBER(19, 4);
  FCustomerAccountId    NUMBER(9, 0);
  FOutboundType         NUMBER(9, 0);
  FComplateCKId         NUMBER(9, 0);
  FComplatePSOId        NUMBER(9, 0);
  FStorageCompanyCode   VARCHAR2(40);
  FParentId NVARCHAR2(25);
  FPartsOutboundBillId        NUMBER(9);
  FPartsShippingOrderId       NUMBER(9);
  FPartsShippingOrderRefId    NUMBER(9);
  FSYNCWMSINOUTLOGINFOId      NUMBER(9);
  FPartsoutboundbilldetailId  NUMBER(9);
  FPartsShippingOrderDetailId NUMBER(9);
  FPartsLogisticBatchId       NUMBER(9);
  FPartsLBBillDetailIdCK      NUMBER(9);
  FNewPartslogisticbatchId    NUMBER(9);
  FPartsLBBillDetailIdFY      NUMBER(9);
  FPartsLockedStockResult     NUMBER(9);
  FPartsStockResult           NUMBER(9);
  FPartsStockId               NUMBER(9);
  FExistsErrorTship           NUMBER(9);
  flogisticscompanyid         NUMBER(9);
  fWMSOutStrategy             NUMBER(9);
  flogisticscompanyCode       varchar2(50);
  flogisticscompanyName       varchar2(100);
  FWMSCreatorName  VARCHAR2(50);
  FMyexception                EXCEPTION;
  FSUMRN                      NUMBER(9);
  FOPStatus                   NUMBER(9);
  Fpostbackcode               VARCHAR2(50);
  FDETAILID                   NUMBER(9);
type t_PartsOutboundBillDset
IS
  TABLE OF partsoutboundbilld_inf%rowtype INDEX BY binary_integer;
  Vtable_Outdetails t_PartsOutboundBillDset;
  CURSOR Get_partsoutboundbill_inf
  IS
    SELECT * FROM partsoutboundbill_inf WHERE InfProcessStatus = 0  ORDER BY code,pwmscode,PostBackCode;
  CURSOR Get_partsoutboundbilld_inf(Parentid VARCHAR2,fpostbackcodes VARCHAR2)
  IS
     SELECT partsoutboundbilld_inf.*,
      Row_Number() Over(order by Parentid ASC) Rno
    FROM partsoutboundbilld_inf
    WHERE partsoutboundbilld_inf.PwmsCode = Parentid
      and partsoutboundbilld_inf.postbackcode = fpostbackcodes;
BEGIN
  Vtable_Outdetails.delete;
  FNewCodeCK  := 'CK{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
  FNewCodePSO := 'FY{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
  BEGIN
    SELECT Id
    INTO FComplateCKId
    FROM CodeTemplate
    WHERE name    = 'PartsOutboundBill'
    AND IsActived = 1;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    FErrormsg := '未找到名称为"PartsOutboundBill"的有效编码规则。';
    raise FMyException;
  WHEN OTHERS THEN
    raise;
  END;
  BEGIN
    SELECT Id
    INTO FComplatePSOId
    FROM CodeTemplate
    WHERE name    = 'PartsShippingOrder'
    AND IsActived = 1;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    FErrormsg := '未找到名称为"PartsShippingOrder"的有效编码规则。';
    raise FMyException;
  WHEN OTHERS THEN
    raise;
  END;
  FOR c_Get_partsoutboundbill_inf IN Get_partsoutboundbill_inf
  LOOP

        begin
    select COUNT(DISTINCT PostBackCode)  into FSUMRN from partsoutboundbill_inf where partsoutboundbill_inf.code=c_Get_partsoutboundbill_inf.code and InfProcessStatus=1 and OrderNum IS NULL;

    EXCEPTION
  WHEN NO_DATA_FOUND THEN
    FSUMRN:=0;
    end;
     IF (NVL(c_Get_partsoutboundbill_inf.OrderNum,0)= FSUMRN )OR (c_Get_partsoutboundbill_inf.FinishFlag IS NULL) THEN
    BEGIN
      FPartsOutboundBillId     := s_Partsoutboundbill.Nextval;
      FPartsShippingOrderId    := s_Partsshippingorder.Nextval;
      FPartsShippingOrderRefId := s_Partsshippingorderref.Nextval;
      FSYNCWMSINOUTLOGINFOId   := S_SYNCWMSINOUTLOGINFO.nextval;

      BEGIN
        SELECT pop.Id,
          pop.Branchid,
          pop.StorageCompanyId,
          Pop.PartsSalesCategoryId,
          NVL(Pop.CustomerAccountId, 0),
          Pop.OutboundType,
          Pop.StorageCompanyCode,
          pop.Warehouseid,pop.status
        INTO FOutplanid,
          FBranchid,
          FStorageCompanyId,
          FPartsSalesCategoryId,
          FCustomerAccountId,
          FOutboundType,
          FStorageCompanyCode,
          FOutwarehouseid,FOPStatus
        FROM Partsoutboundplan Pop
        WHERE pop.Code = c_Get_partsoutboundbill_inf.code;
      EXCEPTION
      WHEN No_Data_Found THEN
        FErrormsg := '未找到出库计划';
        raise FMyException;
      WHEN OTHERS THEN
        raise;
      END;
      begin select branchstrategy.WMSOutStrategy into fWMSOutStrategy from branchstrategy where branchstrategy.branchid=FBranchid ;
       EXCEPTION
      WHEN No_Data_Found THEN
        FErrormsg := '不存在分公司策略';
        raise FMyException;
      WHEN OTHERS THEN
        raise;
      END;
      IF FOPStatus NOT IN (1, 2) THEN
            BEGIN
              FErrormsg := '出库异常[出库计划单状态错误]';
              raise FMyException;
            END;
          END IF;
      FNewcodeckF   := regexp_replace(regexp_replace(FNewCodeCk, '{CORPCODE}', FStorageCompanyCode), '{SERIAL}', lpad(TO_CHAR(GetSerial(FComplateCKId, TRUNC(sysdate, 'DD'), FStorageCompanyCode)), 6, '0'));
      FNewcodepsoF  := regexp_replace(regexp_replace(FNewCodePSO, '{CORPCODE}', FStorageCompanyCode), '{SERIAL}', lpad(TO_CHAR(GetSerial(FComplatePSOId, TRUNC(sysdate, 'DD'), FStorageCompanyCode)), 4, '0'));
      FFinishstatus := 1; --0:未出库,1:出库完成，>1:部分出库
      FErrormsg     := '';
      FParentId     := c_Get_partsoutboundbill_inf.PwmsCode;
      Fpostbackcode := c_Get_partsoutboundbill_inf.PostBackCode;
      IF c_Get_partsoutboundbill_inf.CreatorName IS NULL THEN
        BEGIN
           FErrormsg := 'PWMS操作人员为空';
              raise FMyException;
          END;
      END IF;
      FWMSCreatorName:='PWMS_'||c_Get_partsoutboundbill_inf.CreatorName;
      FOR C_get_partsoutboundbilld_inf IN Get_partsoutboundbilld_inf(FParentId,Fpostbackcode)
      LOOP
        Vdetailno                                      := C_get_partsoutboundbilld_inf.rno;
        Vtable_Outdetails(Vdetailno).pwmscode          := C_get_partsoutboundbilld_inf.pwmscode;
        Vtable_Outdetails(Vdetailno).SparePartCode     := C_get_partsoutboundbilld_inf.SparePartCode;
        Vtable_Outdetails(Vdetailno).OutboundAmount    := C_get_partsoutboundbilld_inf.OutboundAmount;
        Vtable_Outdetails(Vdetailno).Id                := C_get_partsoutboundbilld_inf.Id;
      END LOOP;

      /*
      IF trim(c_Get_partsoutboundbill_inf.User_Def5)    = 1 THEN
        FShiptypecodevalue                          := 1;
      elsif trim(c_Get_partsoutboundbill_inf.User_Def5) = 2 THEN
        FShiptypecodevalue                          := 2;
      elsif trim(c_Get_partsoutboundbill_inf.User_Def5) = 3 THEN
        FShiptypecodevalue                          := 3;
      elsif trim(c_Get_partsoutboundbill_inf.User_Def5) = 4 THEN
        FShiptypecodevalue                          := 4;
      elsif trim(c_Get_partsoutboundbill_inf.User_Def5) = 5 THEN
        FShiptypecodevalue                          := 5;
      elsif trim(c_Get_partsoutboundbill_inf.User_Def5) = 6 THEN
        FShiptypecodevalue                          := 6;
      elsif trim(c_Get_partsoutboundbill_inf.User_Def5) = 7 THEN
        FShiptypecodevalue                          := 7;
      elsif trim(c_Get_partsoutboundbill_inf.User_Def5) = 8 THEN
        FShiptypecodevalue                          := 8;
      elsif trim(c_Get_partsoutboundbill_inf.User_Def5) = 9 THEN
        FShiptypecodevalue                          := 9;
      END IF;
      */
      IF (Vtable_Outdetails.Count > 0) THEN
        BEGIN
          --配件出库单
          INSERT
          INTO Partsoutboundbill
            (
              Id,
              Code,
              Partsoutboundplanid,
              Warehouseid,
              Warehousecode,
              Warehousename,
              Storagecompanyid,
              Storagecompanycode,
              Storagecompanyname,
              Storagecompanytype,
              Branchid,
              Branchcode,
              Branchname,
              Partssalescategoryid,
              Partssalesordertypeid,
              Partssalesordertypename,
              Counterpartcompanyid,
              Counterpartcompanycode,
              Counterpartcompanyname,
              Receivingcompanyid,
              Receivingcompanycode,
              Receivingcompanyname,
              Receivingwarehouseid,
              Receivingwarehousecode,
              Receivingwarehousename,
              Outboundtype,
              Shippingmethod,
              Customeraccountid,
              Originalrequirementbillid,
              Originalrequirementbilltype,
              Originalrequirementbillcode,
              Settlementstatus,
              Creatorid,
              Creatorname,
              Createtime,
              InterfaceRecordId,
              Gpmspurordercode,
              orderapprovecomment,
              remark,SAPPurchasePlanCode
            )
          SELECT FPartsOutboundBillId,
            FNewcodeckF,
            Pop.Id,
            Pop.Warehouseid,
            Pop.Warehousecode,
            Pop.Warehousename,
            Pop.Storagecompanyid,
            Pop.Storagecompanycode,
            Pop.Storagecompanyname,
            Pop.Storagecompanytype,
            Pop.Branchid,
            Pop.Branchcode,
            Pop.Branchname,
            Pop.Partssalescategoryid,
            pop.Partssalesordertypeid,
            pop.Partssalesordertypename,
            Pop.Counterpartcompanyid,
            Pop.Counterpartcompanycode,
            Pop.Counterpartcompanyname,
            Pop.ReceivingCompanyId,
            Pop.ReceivingCompanyCode,
            Pop.ReceivingCompanyName,
            Pop.Receivingwarehouseid,
            Pop.Receivingwarehousecode,
            Pop.Receivingwarehousename,
            Pop.Outboundtype,
            Pop.Shippingmethod,
            Pop.Customeraccountid,
            Pop.Originalrequirementbillid,
            Pop.Originalrequirementbilltype,
            Pop.Originalrequirementbillcode,
            2,
            1,
            FWMSCreatorName,
            SYSDATE,
            FParentId,
            pop.gpmspurordercode,
            pop.orderapprovecomment,
            pop.remark,SAPPurchasePlanCode
          FROM Partsoutboundplan Pop
          WHERE Pop.Id     = FOutplanid;
          /*IF FOutboundType = 1 OR FOutboundType = 4 THEN
            BEGIN
              --配件发运单
           \* begin
              select etl.logisticscompanyid,
                     etl.logisticscompanycode,
                     etl.logisticscompanyname
                into flogisticscompanyid,flogisticscompanycode,flogisticscompanyname from expresstologistics etl
               where etl.focufingcorecode =
                     c_Get_partsoutboundbill_inf.WarehouseCode
                 and etl.expresscode = c_Get_partsoutboundbill_inf.Carrier;
              exception
                when no_data_found then

                 FErrormsg := '快递物流对照不存在,或物流公司不存在';
                  raise FMyException;
                   WHEN OTHERS THEN
                     raise;
                END;*\
              if fWMSOutStrategy=1 then begin

                  \*begin
              select etl.logisticscompanyid,
                     etl.logisticscompanycode,
                     etl.logisticscompanyname
                into flogisticscompanyid,flogisticscompanycode,flogisticscompanyname from expresstologistics etl
               where etl.focufingcorecode =
                     c_Get_partsoutboundbill_inf.WarehouseCode
                 and etl.expresscode = c_Get_partsoutboundbill_inf.Carrier;
              exception
                when no_data_found then
                  begin
                    select id,code,name  into flogisticscompanyid,flogisticscompanycode,flogisticscompanyname
                     from logisticcompany where code=c_Get_partsoutboundbill_inf.Carrier;
                      exception
                when no_data_found then

                 FErrormsg := '快递物流对照不存在,或物流公司不存在';
                  raise FMyException;
                   WHEN OTHERS THEN
                     raise;
                     end;
                END;*\


              INSERT
              INTO Partsshippingorder
                (
                  Id,
                  Code,
                  type,
                  Branchid,
                  PartsSalesCategoryId,
                  Shippingcompanyid,
                  Shippingcompanycode,
                  Shippingcompanyname,
                  Settlementcompanyid,
                  Settlementcompanycode,
                  Settlementcompanyname,
                  Receivingcompanyid,
                  Receivingcompanycode,
                  Invoicereceivesalecateid,
                  Invoicereceivesalecatename,
                  Receivingcompanyname,
                  Warehouseid,
                  Warehousecode,
                  Warehousename,
                  Receivingwarehouseid,
                  Receivingwarehousecode,
                  Receivingwarehousename,
                  Receivingaddress,
                  LogisticCompanyId,
                  LogisticCompanyCode,
                  LogisticCompanyName,
                  Originalrequirementbillid,
                  Originalrequirementbillcode,
                  Originalrequirementbilltype,
                  Shippingmethod,
                  Requestedarrivaldate,
                  Shippingdate,
                  Status,
                  Creatorid,
                  Creatorname,
                  Createtime,
                  InterfaceRecordId,
                  Gpmspurordercode,ExpressCompanyId,ExpressCompanyCode,ExpressCompany
                )
              SELECT FPartsShippingOrderId,
                FNewcodepsoF,
                FOutboundType,
                Pop.Branchid,
                Pop.PartsSalesCategoryId,
                Pop.Storagecompanyid,
                Pop.Storagecompanycode,
                Pop.Storagecompanyname,
                Pop.Storagecompanyid,
                Pop.Storagecompanycode,
                Pop.Storagecompanyname,
                Pop.Receivingcompanyid,
                Pop.Receivingcompanycode,
                Pop.Partssalescategoryid,
                (SELECT name
                FROM Partssalescategory
                WHERE Partssalescategory.Id = Pop.Partssalescategoryid
                ),
                Pop.Receivingcompanyname,
                Pop.Warehouseid,
                Pop.Warehousecode,
                Pop.Warehousename,
                Pop.Receivingwarehouseid,
                Pop.Receivingwarehousecode,
                Pop.Receivingwarehousename,
                ''\*c_Get_partsoutboundbill_inf.Ship_To_Address1*\,
               \* (SELECT Id
                FROM LogisticCompany
                WHERE Code = c_Get_partsoutboundbill_inf.Carrier
                ),
                c_Get_partsoutboundbill_inf.Carrier,
                (SELECT Name
                FROM LogisticCompany
                WHERE Code = c_Get_partsoutboundbill_inf.Carrier
                ),*\
                flogisticscompanyid,flogisticscompanycode,flogisticscompanyname,
                Pop.Originalrequirementbillid,
                Pop.Originalrequirementbillcode,
                Pop.Originalrequirementbilltype,
                Pop.Shippingmethod,
                ''\*c_Get_partsoutboundbill_inf.Actual_Departure_Date_Time*\,
                ''\*c_Get_partsoutboundbill_inf.Scheduled_Ship_Date*\,
                1,
                1,
                FWMSCreatorName,
                SYSDATE,
                FParentId,
                pop.gpmspurordercode,\*(SELECT express.id
                FROM express
                WHERE express.expresscode = c_Get_partsoutboundbill_inf.Carrier
                )*\'',
                \*(SELECT express.expresscode
                FROM express
                WHERE express.expresscode = c_Get_partsoutboundbill_inf.Carrier
                )*\'',
                \*(SELECT express.expressname
                FROM express
                WHERE express.expresscode = c_Get_partsoutboundbill_inf.Carrier
                )*\''
              FROM Partsoutboundplan Pop
              WHERE Pop.Id = FOutplanid;
              --配件发运单关联单
              INSERT
              INTO Partsshippingorderref
                (
                  Id,
                  Partsshippingorderid,
                  Partsoutboundbillid
                )
                VALUES
                (
                  FPartsshippingorderrefId,
                  FPartsShippingOrderId,
                  FPartsoutboundbillId
                );
                end ;
                end if;
            END;
          END IF;*/
          FOR Vi IN 1 .. Vtable_Outdetails.count
          LOOP
            FPartsoutboundbilldetailId  := s_Partsoutboundbilldetail.Nextval;
            FPartsShippingOrderDetailId := s_PartsShippingOrderDetail.Nextval;
            SELECT NVL(pd.OutboundFulfillment, 0) Finishqty,
              NVL(Pd.Plannedamount, 0) Planqty,
              pd.sparepartid AS FSparePartId
            INTO FFinishqty,
              FPlanqty,
              FSparePartId
            FROM Partsoutboundplandetail Pd
            INNER JOIN Sparepart
            ON Sparepart.Id              = Pd.Sparepartid
            WHERE Pd.Partsoutboundplanid = FOutplanid
            AND Sparepart.Code           = Vtable_Outdetails(Vi).SparePartCode;
            --查询成本价
            BEGIN
              SELECT ppp.PlannedPrice
              INTO FCostPrice
              FROM PartsPlannedPrice ppp
              WHERE /*ppp.OwnerCompanyId     = FStorageCompanyId
              AND */ppp.PartsSalesCategoryId = FPartsSalesCategoryId
              AND ppp.sparepartid          = FSparePartId;
            EXCEPTION
            WHEN No_Data_Found THEN
              BEGIN
                FErrormsg := '找不到对应的配件计划价，请核对！';
                FDETAILID :=  Vtable_Outdetails(Vi).id;
                raise FMyException;
              END;
            END;
            BEGIN
              SELECT WarehouseArea.Id,
                WarehouseArea.Code
              INTO FWarehouseAreaId,
                FWarehouseAreaCode
              FROM WarehouseArea
              INNER JOIN WarehouseAreaCategory
              ON WarehouseArea.AreaCategoryId    = WarehouseAreaCategory.Id
              WHERE WarehouseArea.WarehouseId    = FOutwarehouseid
              AND WarehouseArea.Areakind         = 3
              AND WarehouseAreaCategory.Category = 1 and WarehouseArea.status=1;
            EXCEPTION
            WHEN no_data_found THEN
              BEGIN
                FErrormsg := 'PMS仓库对应唯一库位未维护，请核对！';
                FDETAILID :=  Vtable_Outdetails(Vi).id;
                raise FMyException;
              END;
            END;
            BEGIN
              SELECT Sparepart.Id
              INTO FSparepartId
              FROM Sparepart
              WHERE Sparepart.Status = 1
              AND Sparepart.Code     = Vtable_Outdetails(Vi).SparePartCode;
            EXCEPTION
            WHEN no_data_found THEN
              BEGIN
                FErrormsg := '找不到对应的配件，请核对！' || Vtable_Outdetails(Vi).SparePartCode;
                FDETAILID :=  Vtable_Outdetails(Vi).id;
                raise FMyException;
              END;
            END;

            if nvl(Vtable_Outdetails(Vi).OutboundAmount,0) = 0 then
              begin
                FErrormsg := 'PWMS传递的出库数量为0，配件图号：【'||Vtable_Outdetails(Vi).SparePartCode||'】';
                FDETAILID :=  Vtable_Outdetails(Vi).id;
                raise FMyException;
              end;
            end if;

            --配件出库清单
            INSERT
            INTO Partsoutboundbilldetail
              (
                Id,
                PartsOutboundBillId,
                SparePartId,
                SparePartCode,
                SparePartName,
                OutboundAmount,
                WarehouseAreaId,
                WarehouseAreaCode,
                BatchNumber,
                SettlementPrice,
                CostPrice
              )
            SELECT FPartsOutboundBillDetailID,
              FPartsOutboundBillId,
              pd.SparePartId,
              pd.SparePartCode,
              pd.SparePartName,
              Vtable_Outdetails(Vi).OutboundAmount,
              FWarehouseAreaId,
              FWarehouseAreaCode,
              NULL,
              pd.Price,
              DECODE(pop.StorageCompanyType, 1, FCostPrice, 0)
            FROM PartsOutboundPlanDetail pd
            INNER JOIN PartsOutboundPlan Pop
            ON pop.id                    = pd.PartsOutboundPlanId
            WHERE pd.PartsOutboundPlanId = FOutplanid
            AND pd.SparePartId           = FSparepartId;
            /*IF ((FOutboundType             = 1 )OR (FOutboundType = 4 ))and (fWMSOutStrategy=1)  THEN
              BEGIN
                --配件发运清单
                INSERT
                INTO PartsShippingOrderDetail
                  (
                    Id,
                    PartsShippingOrderId,
                    SparePartId,
                    SparePartCode,
                    SparePartName,
                    ShippingAmount,
                    ConfirmedAmount,
                    DifferenceClassification,
                    TransportLossesDisposeMethod,
                    InTransitDamageLossAmount,
                    SettlementPrice
                  )
                SELECT FPartsShippingOrderDetailId,
                  FPartsshippingorderId,
                  pd.SparePartId,
                  pd.SparePartCode,
                  pd.SparePartName,
                  Vtable_Outdetails(Vi).OutboundAmount,
                  Vtable_Outdetails(Vi).OutboundAmount,
                  NULL,
                  NULL,
                  NULL,
                  pd.Price
                FROM PartsOutboundPlanDetail pd
                WHERE pd.PartsOutboundPlanId = FOutplanid
                AND pd.SparePartId           = FSparepartId;
              END;
            END IF;*/
            --减少配件锁定库存
            BEGIN
              SELECT NVL(PartsLockedStock.LockedQuantity, 0) - NVL(Vtable_Outdetails(Vi).OutboundAmount, 0)
              INTO FPartsLockedStockResult
              FROM PartsLockedStock
              WHERE PartsLockedStock.WarehouseId = FOutwarehouseid
              AND PartsLockedStock.Partid        = FSparepartId for update;
            EXCEPTION
            WHEN no_data_found THEN
              BEGIN
                FErrormsg := '锁定库存不存在' || Vtable_Outdetails(Vi).SparePartCode;
                FDETAILID :=  Vtable_Outdetails(Vi).id;
                raise FMyException;
              END;
            WHEN too_many_rows THEN
              BEGIN
                FErrormsg := '存在多条锁定库存,仓库Id' ||FOutwarehouseid || '配件Id' || FSparepartId;
                FDETAILID :=  Vtable_Outdetails(Vi).id;
                raise FMyException;
              END;
            END;
            BEGIN
              IF FPartsLockedStockResult < 0 THEN
                BEGIN
                  FErrormsg := '锁定库存不足' || Vtable_Outdetails(Vi).SparePartCode;
                  FDETAILID :=  Vtable_Outdetails(Vi).id;
                  raise FMyException;
                END;
              END IF;
            END;
            UPDATE PartsLockedStock
            SET PartsLockedStock.LockedQuantity = NVL(PartsLockedStock.LockedQuantity, 0) - NVL(Vtable_Outdetails(Vi) .OutboundAmount, 0)
            WHERE PartsLockedStock.WarehouseId  = FOutwarehouseid
            AND PartsLockedStock.Partid         = FSparepartId;
            --减少配件库存
            BEGIN
              SELECT NVL(PartsStock.Quantity, 0) - NVL(Vtable_Outdetails(Vi).OutboundAmount, 0),
                PartsStock.Id
              INTO FPartsStockResult,
                FPartsStockId
              FROM PartsStock
              INNER JOIN WarehouseAreaCategory
              ON PartsStock.WarehouseAreaCategoryId=WarehouseAreaCategory.ID
              inner join warehousearea on warehousearea.id=PartsStock.Warehouseareaid
              WHERE WarehouseAreaCategory.Category =1
              AND PartsStock.WarehouseId           = FOutwarehouseid
              AND PartsStock.Partid                = FSparepartId and warehousearea.status=1 for update;
            EXCEPTION
            WHEN no_data_found THEN
              BEGIN
                FErrormsg := '库存不存在' || Vtable_Outdetails(Vi).SparePartCode;
                FDETAILID :=  Vtable_Outdetails(Vi).id;
                raise FMyException;
              END;
            WHEN too_many_rows THEN
              BEGIN
                FErrormsg := '存在多条库存,仓库Id' ||FOutwarehouseid || '配件Id' || FSparepartId;
                FDETAILID :=  Vtable_Outdetails(Vi).id;
                raise FMyException;
              END;
            END;
            BEGIN
              IF FPartsStockResult < 0 THEN
                BEGIN
                  FErrormsg := '库存不足' || Vtable_Outdetails(Vi).SparePartCode;
                  FDETAILID :=  Vtable_Outdetails(Vi).id;
                  raise FMyException;
                END;
              END IF;
            END;
            UPDATE PartsStock
            SET PartsStock.Quantity = NVL(PartsStock.Quantity, 0) - NVL(Vtable_Outdetails(Vi) .OutboundAmount, 0)
            WHERE PartsStock.Id     = FPartsStockId;
            UPDATE Partsoutboundplandetail
            SET Partsoutboundplandetail.Outboundfulfillment   = NVL(FFinishqty, 0) + NVL(Vtable_Outdetails(Vi) .OutboundAmount, 0)
            WHERE Partsoutboundplandetail.Partsoutboundplanid = FOutplanid
            AND Partsoutboundplandetail.Sparepartid           = FSparepartId;
            IF FPlanqty                                       > NVL(FFinishqty, 0) + NVL(Vtable_Outdetails(Vi).OutboundAmount, 0) THEN
              BEGIN
                FFinishstatus := FFinishstatus + 1;
              END;
            END IF;
            IF FPlanqty < NVL(FFinishqty, 0) + NVL(Vtable_Outdetails(Vi).OutboundAmount, 0) THEN
              BEGIN
                FErrormsg := '完成量大于计划量，请进行核对！';
                FDETAILID :=  Vtable_Outdetails(Vi).id;
                raise FMyexception;
              END;
            END IF;
          END LOOP;
          IF FOutboundType IN (1, 2, 4, 5) THEN
            BEGIN
              BEGIN
                SELECT plb.Id
                INTO FPartsLogisticBatchId
                FROM PartsLogisticBatch plb
                WHERE plb.SourceId = FOutplanid
                AND plb.SourceType = 1;
              EXCEPTION
              WHEN no_data_found THEN
                FPartsLogisticBatchId := NULL;
              END;
              IF FPartsLogisticBatchId IS NULL THEN
                BEGIN
                  FNewPartslogisticbatchId := s_Partslogisticbatch.Nextval;
                  INSERT
                  INTO PartsLogisticBatch
                    (
                      Id,
                      Status,
                      ShippingStatus,
                      CompanyId,
                      SourceId,
                      SourceType
                    )
                  SELECT FNewPartslogisticbatchId AS Id,
                    1                             AS Status,
                    1                             AS ShippingStatus,
                    pop.storagecompanyid          AS CompanyId,
                    FOutplanid                    AS SourceId,
                    1                             AS SourceType
                  FROM Partsoutboundplan Pop
                  WHERE Pop.Id            = FOutplanid;
                  FPartsLBBillDetailIdCK := s_PartsLogisticBatchBillDetail.Nextval;
                  --新增出库单物流批次单据清单
                  INSERT
                  INTO PartsLogisticBatchBillDetail
                    (
                      Id,
                      PartsLogisticBatchId,
                      BillId,
                      BillType
                    )
                    VALUES
                    (
                      FPartsLBBillDetailIdCK,
                      FNewPartslogisticbatchId,
                      FPartsOutboundBillId,
                      1
                    );
                  FPartsLBBillDetailIdFY := s_PartsLogisticBatchBillDetail.Nextval;
                  --新增发运单物流批次单据清单
                  INSERT
                  INTO PartsLogisticBatchBillDetail
                    (
                      Id,
                      PartsLogisticBatchId,
                      BillId,
                      BillType
                    )
                    VALUES
                    (
                      FPartsLBBillDetailIdFY,
                      FNewPartslogisticbatchId,
                      FPartsShippingOrderId,
                      2
                    );
                  --新增配件物流批次配件清单
                  INSERT
                  INTO PartsLogisticBatchItemDetail
                    (
                      Id,
                      PartsLogisticBatchId,
                      CounterpartCompanyId,
                      ReceivingCompanyId,
                      ShippingCompanyId,
                      SparePartId,
                      SparePartCode,
                      SparePartName,
                      BatchNumber,
                      OutboundAmount,
                      InboundAmount
                    )
                  SELECT s_PartsLogisticBatchItemDetail.Nextval AS Id,
                    FNewPartslogisticbatchId                    AS PartsLogisticBatchId,
                    (SELECT pob.CounterpartCompanyId
                    FROM PartsOutboundBill pob
                    WHERE pob.id = pobdt.partsoutboundbillid
                    ) AS CounterpartCompanyId,
                    (SELECT pob.CounterpartCompanyId
                    FROM PartsOutboundBill pob
                    WHERE pob.id = pobdt.partsoutboundbillid
                    ) AS ReceivingCompanyId,
                    (SELECT pob.storagecompanyid
                    FROM PartsOutboundBill pob
                    WHERE pob.id = pobdt.partsoutboundbillid
                    ) AS ShippingCompanyId,
                    pobdt.SparePartId,
                    (SELECT Code FROM sparepart WHERE id = pobdt.SparePartId
                    ) AS SparePartCode,
                    (SELECT Name FROM sparepart WHERE id = pobdt.SparePartId
                    ) AS SparePartName,
                    pobdt.BatchNumber,
                    pobdt.OutboundAmount AS OutboundAmount,
                    0                    AS InboundAmount
                  FROM
                    (SELECT pobd.partsoutboundbillid,
                      pobd.SparePartId         AS SparePartId,
                      pobd.BatchNumber         AS BatchNumber,
                      SUM(pobd.OutboundAmount) AS OutboundAmount
                    FROM PartsOutboundBillDetail pobd
                    WHERE pobd.partsoutboundbillid = FPartsOutboundBillId
                    GROUP BY pobd.partsoutboundbillid,
                      pobd.sparepartid,
                      pobd.batchnumber
                    ) pobdt;
                END;
              ELSE
                BEGIN
                  /*UPDATE PartsLogisticBatch
                  SET ShippingStatus      = 2
                  WHERE Id                = FPartsLogisticBatchId
                  AND ShippingStatus      = 1;*/
                  FPartsLBBillDetailIdCK := s_PartsLogisticBatchBillDetail.Nextval;
                  --新增出库单物流批次单据清单
                  INSERT
                  INTO PartsLogisticBatchBillDetail
                    (
                      Id,
                      PartsLogisticBatchId,
                      BillId,
                      BillType
                    )
                    VALUES
                    (
                      FPartsLBBillDetailIdCK,
                      FPartsLogisticBatchId,
                      FPartsOutboundBillId,
                      1
                    );
                  FPartsLBBillDetailIdFY := s_PartsLogisticBatchBillDetail.Nextval;
                  --新增发运单物流批次单据清单
                  INSERT
                  INTO PartsLogisticBatchBillDetail
                    (
                      Id,
                      PartsLogisticBatchId,
                      BillId,
                      BillType
                    )
                    VALUES
                    (
                      FPartsLBBillDetailIdFY,
                      FPartsLogisticBatchId,
                      FPartsShippingOrderId,
                      2
                    );
                  BEGIN
                    --更新配件物流批次配件清单
                    UPDATE PartsLogisticBatchItemDetail plbid
                    SET OutboundAmount =
                      (SELECT SUM(pobd.OutboundAmount)
                      FROM PartsOutboundBillDetail pobd
                      WHERE pobd.partsoutboundbillid = FPartsOutboundBillId
                      AND pobd.SparePartId           = plbid.SparePartId
                      AND pobd.BatchNumber           = plbid.BatchNumber
                      GROUP BY pobd.partsoutboundbillid,
                        pobd.sparepartid,
                        pobd.batchnumber
                      )
                    WHERE EXISTS
                      (SELECT 1
                      FROM PartsOutboundBillDetail pobd
                      WHERE pobd.partsoutboundbillid = FPartsOutboundBillId
                      AND pobd.SparePartId           = plbid.SparePartId
                      AND pobd.BatchNumber           = plbid.BatchNumber
                      );
                    --新增配件物流批次配件清单
                    INSERT
                    INTO PartsLogisticBatchItemDetail
                      (
                        Id,
                        PartsLogisticBatchId,
                        CounterpartCompanyId,
                        ReceivingCompanyId,
                        ShippingCompanyId,
                        SparePartId,
                        SparePartCode,
                        SparePartName,
                        BatchNumber,
                        OutboundAmount,
                        InboundAmount
                      )
                    SELECT s_PartsLogisticBatchItemDetail.Nextval AS Id,
                      FPartsLogisticBatchId                       AS PartsLogisticBatchId,
                      (SELECT pob.CounterpartCompanyId
                      FROM PartsOutboundBill pob
                      WHERE pob.id = pobdt.partsoutboundbillid
                      ) AS CounterpartCompanyId,
                      (SELECT pob.CounterpartCompanyId
                      FROM PartsOutboundBill pob
                      WHERE pob.id = pobdt.partsoutboundbillid
                      ) AS ReceivingCompanyId,
                      (SELECT pob.storagecompanyid
                      FROM PartsOutboundBill pob
                      WHERE pob.id = pobdt.partsoutboundbillid
                      ) AS ShippingCompanyId,
                      pobdt.SparePartId,
                      (SELECT Code FROM sparepart WHERE id = pobdt.SparePartId
                      ) AS SparePartCode,
                      (SELECT Name FROM sparepart WHERE id = pobdt.SparePartId
                      ) AS SparePartName,
                      pobdt.BatchNumber,
                      pobdt.OutboundAmount AS OutboundAmount,
                      0                    AS InboundAmount
                    FROM
                      (SELECT pobd.partsoutboundbillid,
                        pobd.SparePartId         AS SparePartId,
                        pobd.BatchNumber         AS BatchNumber,
                        SUM(pobd.OutboundAmount) AS OutboundAmount
                      FROM PartsOutboundBillDetail pobd
                      WHERE pobd.partsoutboundbillid = FPartsOutboundBillId
                      AND NOT EXISTS
                        (SELECT 1
                        FROM PartsLogisticBatchItemDetail plbid
                        WHERE plbid.SparePartId        = pobd.SparePartId
                        AND plbid.BatchNumber          = pobd.BatchNumber
                        AND plbid.PartsLogisticBatchId = FPartsLogisticBatchId
                        )
                      GROUP BY pobd.partsoutboundbillid,
                        pobd.sparepartid,
                        pobd.batchnumber
                      ) pobdt;
                  END;
                END;
              END IF;
            END;
          END IF;
          SELECT SUM(NVL(pd.SettlementPrice, 0) * NVL(pd.OutboundAmount, 0))
          INTO FAmount
          FROM Partsoutboundbill Pob
          INNER JOIN Partsoutboundbilldetail pd
          ON pob.Id  = pd.PartsOutboundBillId
          WHERE Pob.Id = FPartsOutboundBillId;
          IF FCustomerAccountId > 0 THEN
            BEGIN
              IF FOutboundType = 1 THEN
                BEGIN
                  UPDATE CustomerAccount
                  SET CustomerAccount.Pendingamount     = NVL(CustomerAccount.Pendingamount, 0)       - NVL(FAmount, 0),
                    CustomerAccount.Shippedproductvalue = NVL(CustomerAccount.Shippedproductvalue, 0) + NVL(FAmount, 0),
                    CustomerAccount.Modifierid          = 1,
                    CustomerAccount.Modifiername        = 'PWMSInterface',
                    CustomerAccount.Modifytime          = sysdate
                  WHERE CustomerAccount.id              = FCustomerAccountId
                  AND CustomerAccount.status           <> 99;
                END;
              /*代理库的出库计划 = 采购退货，实则为向总部退货，而非供应商
              elsif FOutboundType = 2 THEN
                BEGIN
                  BEGIN
                    SELECT sa.estimateddueamount
                    INTO FEstimateddueamount
                    FROM supplieraccount sa
                    WHERE sa.id   = FCustomeraccountId
                    AND sa.status = 1;
                  EXCEPTION
                  WHEN No_Data_Found THEN
                    FErrormsg := '未找到供应商账户，请核对！';
                    raise FMyexception;
                  END;
                  UPDATE supplieraccount
                  SET supplieraccount.estimateddueamount = NVL(supplieraccount.estimateddueamount, 0) + NVL(FAmount, 0),
                    supplieraccount.Modifierid           = 1,
                    supplieraccount.Modifiername         = 'PWMSInterface',
                    supplieraccount.Modifytime           = sysdate
                  WHERE supplieraccount.id               = FcustomeraccountId
                  AND supplieraccount.status             = 1;
                END;*/
              END IF;
            END;
          END IF;
        END;
      END IF;

      Select SUM(PlannedAmount) - SUM(NVL(outboundfulfillment, 0)) AS isfinished
      INTO FIsAllFinished
      from partsoutboundplandetail where partsoutboundplanid=FOutplanid
      GROUP BY PartsoutboundPlanId;

      IF (c_Get_partsoutboundbill_inf.FinishFlag = '1' )and (FIsAllFinished > 0) THEN
        BEGIN
          BEGIN
            SELECT 1
            INTO FExistsErrorTship
            FROM partsoutboundbill_inf
            WHERE code=c_Get_partsoutboundbill_inf.code
            AND InfProcessStatus   =2;
          EXCEPTION
          WHEN no_data_found THEN
            FExistsErrorTship    :=NULL;
          WHEN too_many_rows THEN
            FExistsErrorTship    :=NULL;
            IF FExistsErrorTship IS NOT NULL THEN
              FErrormsg          := '存在未处理并且有问题的单据，必须先处理问题单据后才可强制完成';
              raise FMyException;
            END IF;
          END;
          UPDATE PartsOutboundPlan
          SET PartsOutboundPlan.status     = 4,
            PartsOutboundPlan.Modifytime   = sysdate,
            PartsOutboundPlan.ModifierName = 'PWMS',
            PartsOutboundPlan.Stoper       = 'PWMS',
            PartsOutboundPlan.StopTime     = sysdate,
            PartsOutboundPlan.Remark       = PartsOutboundPlan.Remark
            || ' [该单据已经由业务人员强制完成]'
          WHERE PartsOutboundPlan.id = FOutplanid;
          --减少配件锁定库存
          UPDATE PartsLockedStock
          SET PartsLockedStock.LockedQuantity = PartsLockedStock.LockedQuantity -
            (SELECT NVL(plannedamount, 0)                                       - NVL(outboundfulfillment, 0)
            FROM Partsoutboundplandetail
            WHERE Partsoutboundplandetail.Partsoutboundplanid = FOutplanid
            AND Partsoutboundplandetail.Sparepartid           = PartsLockedStock.Partid
            )
          WHERE PartsLockedStock.WarehouseId = FOutwarehouseid
          AND EXISTS
            (SELECT Id
            FROM Partsoutboundplandetail
            WHERE Partsoutboundplandetail.Partsoutboundplanid = FOutplanid
            AND Partsoutboundplandetail.Sparepartid           = PartsLockedStock.Partid
            );
          SELECT SUM(NVL(popd.Price, 0) * (NVL(popd.PlannedAmount,0)- NVL(popd.OutboundFulfillment, 0)))
          INTO FLeftAmount
          FROM PartsOutboundPlan pop
          INNER JOIN PartsOutboundPlanDetail popd
          ON pop.Id             = popd.PartsOutboundPlanId
          WHERE pop.ID          = FOutplanid;
          IF FCustomerAccountId > 0 THEN
            BEGIN
              IF FOutboundType = 1 THEN
                BEGIN
                  UPDATE CustomerAccount
                  SET CustomerAccount.Pendingamount = NVL(CustomerAccount.Pendingamount, 0) - NVL(FLeftAmount, 0),
                    CustomerAccount.Modifierid      = 1,
                    CustomerAccount.Modifiername    = 'PWMSInterface',
                    CustomerAccount.Modifytime      = sysdate
                  WHERE CustomerAccount.id          = FCustomerAccountId
                  AND CustomerAccount.status       <> 99;
                END;
              END IF;
            END;
          END IF;
        END;
      END IF;

        --出库计划状态  1  新建,2  部分出库,3  出库完成,4  终止
      IF (FIsAllFinished > 0) and (c_Get_partsoutboundbill_inf.FinishFlag is null) THEN
        BEGIN
          UPDATE PartsOutboundPlan
          SET PartsOutboundPlan.status = 2,
          PartsOutboundPlan.Modifierid = 1,
          PartsOutboundPlan.Modifiername =FWMSCreatorName,
          PartsOutboundPlan.Modifytime = sysdate
          WHERE PartsOutboundPlan.id   = FOutplanid;
        END;
      END IF;

      IF (FIsAllFinished = 0) and (c_Get_partsoutboundbill_inf.FinishFlag = '1') THEN
        BEGIN
          UPDATE PartsOutboundPlan
          SET PartsOutboundPlan.status = 3,
          PartsOutboundPlan.Modifierid = 1,
          PartsOutboundPlan.Modifiername =FWMSCreatorName,
          PartsOutboundPlan.Modifytime = sysdate
          WHERE PartsOutboundPlan.id   = FOutplanid;
        END;
      END IF;

      UPDATE partsoutboundbill_inf
         SET partsoutboundbill_inf.InfProcessStatus = 1
       WHERE partsoutboundbill_inf.pwmscode =
             c_Get_partsoutboundbill_inf.pwmscode
         and partsoutboundbill_inf.postbackcode =
             c_Get_partsoutboundbill_inf.postbackcode;

       UPDATE PartsOutboundBill
          SET PartsOutboundBill.pwmsoutboundcode = c_Get_partsoutboundbill_inf.Pwmscode || c_Get_partsoutboundbill_inf.postbackcode
        WHERE PartsOutboundBill.code = FNewcodeckF;

    EXCEPTION
    WHEN FMyexception THEN
      BEGIN
        ROLLBACK;
        INSERT INTO sync_wms_inout_errlog
          (ID,
           CreatedAt,
           Message,
           PwmsCode,
           PostBackCode,
           PmsCode,
           TableName,
           TABLEROWID,
           DETAILTABLEROWID)
        VALUES
          (s_sync_wms_inout_errlog.NEXTVAL,
           sysdate,
           FErrormsg,
           c_Get_partsoutboundbill_inf.Pwmscode,
           c_Get_partsoutboundbill_inf.postbackcode,
           c_Get_partsoutboundbill_inf.CODE,
           'parts_outbound_bill',
           c_Get_partsoutboundbill_inf.Id,
           nvl(FDETAILID, 0));

        UPDATE partsoutboundbill_inf
        SET partsoutboundbill_inf.InfProcessStatus            = 2
        WHERE partsoutboundbill_inf.PwmsCode = c_Get_partsoutboundbill_inf.PwmsCode
        and partsoutboundbill_inf.postbackcode = c_Get_partsoutboundbill_inf.postbackcode
        AND partsoutboundbill_inf.InfProcessStatus            = 0;

      END;
    WHEN OTHERS THEN
      BEGIN
        ROLLBACK;
        FErrormsg := sqlerrm;
        INSERT INTO sync_wms_inout_errlog
          (ID,
           CreatedAt,
           Message,
           PwmsCode,
           PostBackCode,
           PmsCode,
           TableName,
           TABLEROWID,
           DETAILTABLEROWID)
        VALUES
          (s_sync_wms_inout_errlog.NEXTVAL,
           sysdate,
           FErrormsg,
           c_Get_partsoutboundbill_inf.Pwmscode,
           c_Get_partsoutboundbill_inf.postbackcode,
           c_Get_partsoutboundbill_inf.CODE,
           'parts_outbound_bill',
           c_Get_partsoutboundbill_inf.Id,
           nvl(FDETAILID, 0));


      END;
      UPDATE partsoutboundbill_inf
      SET partsoutboundbill_inf.InfProcessStatus            = 2
      WHERE partsoutboundbill_inf.pwmscode = c_Get_partsoutboundbill_inf.pwmscode
      and partsoutboundbill_inf.postbackcode = c_Get_partsoutboundbill_inf.postbackcode
      AND partsoutboundbill_inf.InfProcessStatus            = 0;
    END;
   end if;
    Vtable_Outdetails.delete;
    COMMIT;
  END LOOP;
END;
