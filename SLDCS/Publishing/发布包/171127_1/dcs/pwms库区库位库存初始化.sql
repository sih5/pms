update warehouse set pwmsinterface=1 where code in('OM457');
create table tmpdata.warehosuearea171125 as select * from warehousearea where rownum<1;
insert into tmpdata.warehosuearea171125
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   areakind,
   remark,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
         warehouse.id,
         (select a.id
            from warehousearea a
           inner join warehouseareacategory b
              on a.areacategoryid = b.id
             and b.category = 1
             and a.areakind = 2
             where a.warehouseid = warehouse.id and rownum=1),
         'XNBGKW',
         (select a.id
            from warehousearea a
           inner join warehouseareacategory b
              on a.areacategoryid = b.id
             and b.category = 1
             and a.areakind = 2
             where a.warehouseid = warehouse.id and rownum=1),
         89,
         1,
         3,
         '库位初始化',
         1,
         'Admin',
         sysdate
    from warehouse
   where warehouse.pwmsinterface = 1;

insert into tmpdata.warehosuearea171125
  (id,
   warehouseid,
   parentid,
   code,
   toplevelwarehouseareaid,
   areacategoryid,
   status,
   areakind,
   remark,
   creatorid,
   creatorname,
   createtime)
  select s_warehousearea.nextval,
         warehouse.id,
         (select a.id
            from warehousearea a
           inner join warehouseareacategory b
              on a.areacategoryid = b.id
             and b.category = 2
             and a.areakind = 2
             where a.warehouseid = warehouse.id and rownum=1),
         'XNWTKW',
         (select a.id
            from warehousearea a
           inner join warehouseareacategory b
              on a.areacategoryid = b.id
             and b.category = 2
             and a.areakind = 2
             where a.warehouseid = warehouse.id and rownum=1),
         87,
         1,
         3,
         'pwms库位初始化',
         1,
         'Admin',
         sysdate
    from warehouse
   where warehouse.pwmsinterface = 1;
   
   UPDATE warehousearea a
      set status = 99
    WHERE EXISTS (SELECT 1
             FROM warehouse b
            where a.warehouseid = b.id
              and b.pwmsinterface = 1)
      and exists (select 1
             from warehouseareacategory c
            where c.id = a.areacategoryid
              and c.category in (1, 2))
      and areakind = 3;
insert into warehousearea select * from  tmpdata.warehosuearea171125;

create table tmpdata.partsstock171125del as select * from partsstock where rownum<1;
insert into tmpdata.partsstock171125del
select *
  from partsstock
 where warehouseid in (select id from warehouse where pwmsinterface = 1)
 and warehouseareaid in(select a.id from warehousearea a inner join warehouseareacategory b on b.id=a.areacategoryid where b.category in(1,2));
delete from  partsstock
 where warehouseid in (select id from warehouse where pwmsinterface = 1)
 and warehouseareaid in(select a.id from warehousearea a inner join warehouseareacategory b on b.id=a.areacategoryid where b.category in(1,2));
 
 
create table tmpdata.partsstock171125 as select * from partsstock where rownum<1;
insert Into tmpdata.partsstock171125
  (id,
   warehouseid,
   storagecompanyid,
   storagecompanytype,
   branchid,
   warehouseareaid,
   warehouseareacategoryid,
   partid,
   quantity,
   remark,
   creatorid,
   creatorname,
   createtime)
  select s_partsstock.nextval,
         a.warehouseid,
         a.storagecompanyid,
         a.storagecompanytype,
         a.branchid,
         (select b.id
            from tmpdata.warehosuearea171125 b
           inner join warehouseareacategory c
              on c.id = b.areacategoryid
           where c.category = a.category),
         (select b.areacategoryid
            from tmpdata.warehosuearea171125 b
           inner join warehouseareacategory c
              on c.id = b.areacategoryid
           where c.category = a.category),
         a.partid,
         a.quantity,
         'PWMS库存初始化',
         1,
         'Admin',
         sysdate
    from (select a.warehouseid,
                 a.storagecompanyid,
                 a. storagecompanytype,
                 a. branchid,
                 a. partid,
                 sum(a.quantity) quantity,
                 warehouseareacategory.category
            from tmpdata.partsstock171125del a
           inner join warehousearea
              on warehousearea.id = a.warehouseareaid
           inner join warehouseareacategory
              on warehouseareacategory.id = warehousearea.areacategoryid
           group by a.warehouseid,
                    a.storagecompanyid,
                    a.                             storagecompanytype,
                    a.branchid,
                    a.                             partid,
                    warehouseareacategory.category) a;
insert into partsstock select * from  tmpdata.partsstock171125;
