begin
  sys.dbms_scheduler.create_job(job_name            => 'PWMS接口盘点数据同步',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'Synccheck_dlk',
                                start_date          => to_date('23-11-2017 00:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Minutely;Interval=5',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '');
end;
/
