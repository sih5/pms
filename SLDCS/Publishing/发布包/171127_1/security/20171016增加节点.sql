INSERT INTO Page(Id,ParentId,Type,PageId,PageType,Parameter,Name,Description,Icon,Sequence,Status)VALUES(14104,14100,2,'UnmetOrderCollect','Released',NULL, '未满足订单汇总统计表','以系统内采购订单和配件图号为依据，统计到目前为止未满足情况', 'Client/DCS/Images/Menu/Channels/NotificationQuery.png',15,2);

-- 确保所有窗体节点，在Node中已存在
INSERT INTO Node (Id, CategoryType, CategoryId, Status)
SELECT S_Node.Nextval, 0, Id, 2 
  FROM Page 
 WHERE Type = 2 AND Status = 2
   and not exists (select * from node where node.categoryid = page.Id and CategoryType=0);