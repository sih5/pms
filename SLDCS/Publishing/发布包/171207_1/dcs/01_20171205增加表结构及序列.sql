create sequence S_MDMPartsSalesCategoryMap
/

/*==============================================================*/
/* Table: MDMPartsSalesCategoryMap                              */
/*==============================================================*/
create table MDMPartsSalesCategoryMap  (
   Id                   NUMBER(9)                       not null,
   MDMPartsSalesCategory VARCHAR2(100)                   not null,
   PMSPartsSalesCategoryCode VARCHAR2(100)                   not null,
   PMSPartsSalesCategoryName VARCHAR2(100)                   not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_MDMPARTSSALESCATEGORYMAP primary key (Id)
)
/