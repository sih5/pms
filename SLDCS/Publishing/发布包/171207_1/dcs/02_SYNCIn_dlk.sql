CREATE OR REPLACE PROCEDURE SYNCIn_dlk
AS
type T_indetailsset
IS
  TABLE OF partsinboundcheckbilld_inf%rowtype INDEX BY binary_integer;
  Vtable_indetails T_indetailsset;
  Vdetailno               NUMBER(9);
  Vi                      NUMBER(9);
  FWarehouseAreaId        NUMBER(9);
  FWarehouseAreaCode      VARCHAR2(40);
  FIPWarehouseCode        VARCHAR2(25);
  FAreaCategoryId         NUMBER(9);
  FIPPartsSalesCategoryId NUMBER(9);
  FIPPartId               NUMBER(9);
  FIPDInspectedQuantity   NUMBER(9);
  FIPDPlannedAmount       NUMBER(9);
  FIPWarehouseId          NUMBER(9);
  FIPId                   NUMBER(9);
  FErrormsg               VARCHAR2(1000);
  FPartsStockId           NUMBER(9);
  FIPStorageCompanyId     NUMBER(9);
  FIPStorageCompanyType   VARCHAR2(40);
  FIPStatus               NUMBER(3);
  FIPDPrice               NUMBER(19, 4);
  FIPBranchId             NUMBER(9);
  FPPPrice                NUMBER(19, 4);
  FIsAllFinished          NUMBER(9);
  FDETAILID               NUMBER(9);
  FParentId NVARCHAR2(25);
  QualityType VARCHAR2(50);
  FWMSCreatorName  VARCHAR2(50);
  FMyException                   EXCEPTION;
  FComplateId                    INTEGER;
  FNewCode                       VARCHAR2(50);
  FNewCodePICB                   VARCHAR2(50);
  FIPStorageCompanyCode          VARCHAR2(50);
  FPartsInboundCheckBillId       NUMBER(9);
  FSYNCWMSINOUTLOGINFOId         NUMBER(9);
  FPartsInboundCheckBillDetailId NUMBER(9);
  FIPInboundType                 NUMBER(9);
  FNewPartsStockId               NUMBER(9);
  FPartsLogisticBatchId          NUMBER(9);
  FExistsErrorTin                NUMBER(9);
  FSUMRN                         NUMBER(9);
  Fpostbackcode                  VARCHAR2(50);
  CURSOR getparts_inbound_checkbills
  IS
    SELECT * FROM partsinboundcheckbill_inf WHERE infprocessstatus = 0 ORDER BY code,pwmscode;
  CURSOR getparts_inbound_checkbilldt(FParentIds VARCHAR2,fpostbackcodes VARCHAR2)
  IS
    SELECT partsinboundcheckbilld_inf.*,
      row_number() over(order by partsinboundcheckbill_inf.PWMSCODE ASC) rno
    FROM partsinboundcheckbill_inf
    INNER JOIN partsinboundcheckbilld_inf
    ON partsinboundcheckbill_inf.PwmsCode  = partsinboundcheckbilld_inf.PwmsCode
    and  partsinboundcheckbill_inf.postbackcode=partsinboundcheckbilld_inf.postbackcode
    WHERE partsinboundcheckbill_inf.infprocessstatus    = 0
   -- AND parts_inbound_checkbill.syscode         = 'NPMS'
    AND partsinboundcheckbilld_inf.PwmsCode = FParentIds
    and partsinboundcheckbilld_inf.postbackcode=fpostbackcodes;
  CURSOR getFinshDetails(FIPId VARCHAR2)
  IS
    SELECT * FROM PartsInboundPlanDetail WHERE PartsInboundPlanId = FIPId;
BEGIN
  Vtable_indetails.delete;
  FNewCode := 'RK{CORPCODE}' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}';
  BEGIN
    SELECT Id
    INTO FComplateId
    FROM CodeTemplate
    WHERE name    = 'PartsInboundCheckBill'
    AND IsActived = 1;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    FErrormsg := '未找到名称为"PartsInboundCheckBill"的有效编码规则。';
    raise FMyException;
  WHEN OTHERS THEN
    raise;
  END;
  --循环处理主单数据
  FOR C_getparts_inbound_checkbills IN getparts_inbound_checkbills
  LOOP
    FPartsInboundCheckBillId := S_PartsInboundCheckBill.NEXTVAL;
    FSYNCWMSINOUTLOGINFOId   := S_SYNCWMSINOUTLOGINFO.nextval;
    begin
    select COUNT(DISTINCT  POSTBACKCODE)  into FSUMRN from partsinboundcheckbill_inf
     where partsinboundcheckbill_inf.code=C_getparts_inbound_checkbills.code
      and infprocessstatus=1 
      and (partsinboundcheckbill_inf.finishflag<>'1' )or (partsinboundcheckbill_inf.finishflag is null);

    EXCEPTION
  WHEN NO_DATA_FOUND THEN
    FSUMRN:=0;
    end;
     IF (NVL(C_getparts_inbound_checkbills.OrderNum,0)= FSUMRN )OR (C_getparts_inbound_checkbills.Finishflag IS NULL) THEN
    BEGIN
      FParentId := C_getparts_inbound_checkbills.pwmscode;
      fpostbackcode:=C_getparts_inbound_checkbills.Postbackcode;
     -- QualityType:= C_getT_ins.USER_DEF7;
      IF C_getparts_inbound_checkbills.CREATORNAME IS NULL THEN
        BEGIN
           FErrormsg := 'WMS操作人员为空';
              raise FMyException;
          END;
      END IF;

      FWMSCreatorName:='PWMS_'||C_getparts_inbound_checkbills.CREATORNAME;
      FOR C_getparts_inbound_checkbilldt IN getparts_inbound_checkbilldt(FParentId,Fpostbackcode)
      LOOP
        if C_getparts_inbound_checkbilldt.Receivedamount =0 then
           BEGIN
             FErrormsg:='入库数量为0';
             FDETAILID:=C_getparts_inbound_checkbilldt.Id;
             RAISE FMyException;
           END;
        END IF;
        Vdetailno                                        := C_getparts_inbound_checkbilldt.rno;
        Vtable_indetails(Vdetailno).pwmscode             := C_getparts_inbound_checkbilldt.pwmscode;
        Vtable_indetails(Vdetailno).SparePartCode        := C_getparts_inbound_checkbilldt.SparePartCode;
        Vtable_indetails(Vdetailno).SparePartName        := C_getparts_inbound_checkbilldt.SparePartName;
        Vtable_indetails(Vdetailno).Receivedamount    := C_getparts_inbound_checkbilldt.Receivedamount;
         Vtable_indetails(Vdetailno).id               := C_getparts_inbound_checkbilldt.Id;
      END LOOP;
      FErrormsg := '';

      --校验并获取入库仓库ID
      BEGIN
        SELECT PartsInboundPlan.Id              AS FIPId,
          PartsInboundPlan.Status               AS FIPStatus,
          PartsInboundPlan.WarehouseId          AS FIPWarehouseId,
          PartsInboundPlan.StorageCompanyId     AS FIPStorageCompanyId,
          PartsInboundPlan.StorageCompanyCode   AS FIPStorageCompanyCode,
          PartsInboundPlan.StorageCompanyType   AS FIPStorageCompanyType,
          PartsInboundPlan.Partssalescategoryid AS FIPPartsSalesCategoryId,
          PartsInboundPlan.Branchid             AS FIPBranchId,
          PartsInboundPlan.InboundType          AS FIPInboundType
        INTO FIPId,
          FIPStatus,
          FIPWarehouseId,
          FIPStorageCompanyId,
          FIPStorageCompanyCode,
          FIPStorageCompanyType,
          FIPPartsSalesCategoryId,
          FIPBranchId,
          FIPInboundType
        FROM PartsInboundPlan
        WHERE Code = C_getparts_inbound_checkbills.code;
      EXCEPTION
      WHEN no_data_found THEN
        FIPId                 := NULL;
        FIPWarehouseId        := NULL;
        FIPStorageCompanyId   := NULL;
        FIPStorageCompanyType := NULL;
      END;
      --生成编号
      FNewCodePICB := regexp_replace(regexp_replace(FNewCode, '{CORPCODE}', FIPStorageCompanyCode), '{SERIAL}', lpad(TO_CHAR(GetSerial(FComplateId, TRUNC(sysdate, 'DD'), FIPStorageCompanyCode)), 6, '0'));

      --校验并获取入库仓库编号
      BEGIN
        SELECT Warehouse.code
        INTO FIPWarehouseCode
        FROM Warehouse
        WHERE Warehouse.Id = FIPWarehouseId;
      EXCEPTION
      WHEN no_data_found THEN
        FIPWarehouseCode := NULL;
      END;
      --如果入库计划单存在则进行处理否则记录错误日志
      IF FIPId IS NOT NULL THEN
        BEGIN
          --校验入库计划单状态(1:新建 4:部分检验)
          IF FIPStatus NOT IN (1, 4) THEN
            BEGIN
              FErrormsg := '入库异常[入库计划单状态错误]';
              raise FMyException;
            END;
          END IF;
          --校验入库仓库
          IF FIPWarehouseCode = C_getparts_inbound_checkbills.WarehouseCode THEN
            BEGIN
              IF (Vtable_indetails.Count > 0) THEN
                BEGIN
                  --如果存在入库清单，则生成PMS入库单入库检验单。
                  INSERT
                  INTO PartsInboundCheckBill
                    (
                      Id,
                      Code,
                      PartsInboundPlanId,
                      WarehouseId,
                      WarehouseCode,
                      WarehouseName,
                      StorageCompanyId,
                      StorageCompanyCode,
                      StorageCompanyName,
                      StorageCompanyType,
                      PartsSalesCategoryId,
                      Customeraccountid,
                      BranchId,
                      BranchCode,
                      BranchName,
                      CounterpartCompanyId,
                      CounterpartCompanyCode,
                      CounterpartCompanyName,
                      InboundType,
                      OriginalRequirementBillId,
                      OriginalRequirementBillCode,
                      OriginalRequirementBillType,
                      Status,
                      SettlementStatus,
                      Remark,
                      CreatorId,
                      CreatorName,
                      CreateTime,
                      Objid,
                      GPMSPURORDERCODE,PURORDERCODE,SAPPurchasePlanCode
                    )
                  SELECT FPartsInboundCheckBillId,
                    FNewCodePICB,
                    PartsInboundPlan.Id,
                    PartsInboundPlan.WarehouseId,
                    PartsInboundPlan.WarehouseCode,
                    PartsInboundPlan.WarehouseName,
                    PartsInboundPlan.StorageCompanyId,
                    PartsInboundPlan.StorageCompanyCode,
                    PartsInboundPlan.StorageCompanyName,
                    PartsInboundPlan.StorageCompanyType,
                    PartsInboundPlan.PartsSalesCategoryId,
                    Customeraccountid,
                    PartsInboundPlan.BranchId,
                    PartsInboundPlan.BranchCode,
                    PartsInboundPlan.BranchName,
                    PartsInboundPlan.CounterpartCompanyId,
                    PartsInboundPlan.CounterpartCompanyCode,
                    PartsInboundPlan.CounterpartCompanyName,
                    PartsInboundPlan.InboundType,
                    PartsInboundPlan.OriginalRequirementBillId,
                    PartsInboundPlan.OriginalRequirementBillCode,
                    PartsInboundPlan.OriginalRequirementBillType,
                    1, --新建
                    2, --待结算
                    PartsInboundPlan.Remark,
                    1,
                    FWMSCreatorName,
                    SYSDATE,
                    FParentId,
                    PartsInboundPlan.Gpmspurordercode,
                         nvl((select partspurchaseorder.purordercode
                            from partspurchaseorder
                           where partspurchaseorder.code =
                                 PartsInboundPlan.Originalrequirementbillcode),(select partstransferorder.purordercode
                            from partstransferorder
                           where partstransferorder.code =
                                 PartsInboundPlan.Originalrequirementbillcode)
                                 ),SAPPurchasePlanCode
                  FROM PartsInboundPlan
                  WHERE PartsInboundPlan.Id = FIPId;
                  FOR Vi IN 1 .. Vtable_indetails.count
                  LOOP
                    FPartsInboundCheckBillDetailId := S_PartsInboundCheckBillDetail.NEXTVAL;
                    --校验配件的有效
                    BEGIN
                      SELECT PartsInboundPlanDetail.SparePartId
                      INTO FIPPartId
                      FROM PartsInboundPlanDetail
                      WHERE PartsInboundPlanDetail.PartsInboundPlanId = FIPId
                      AND PartsInboundPlanDetail.SparePartId          =
                        (SELECT Id FROM SparePart WHERE Code = Vtable_indetails(Vi) .SparePartCode
                        );
                    EXCEPTION
                    WHEN NO_Data_Found THEN
                      FIPPartId := NULL;
                    END;
                    --如果配件有效则开始处理清单数据。
                    IF FIPPartId IS NOT NULL THEN
                      BEGIN
                        --获取入库计划单的确认数量
                        SELECT NVL(PartsInboundPlanDetail.InspectedQuantity, 0) AS FIPDInspectedQuantity,
                          NVL(PartsInboundPlanDetail.PlannedAmount, 0)          AS FIPDPlannedAmount,
                          NVL(PRICE, 0)                                         AS PRICE
                        INTO FIPDInspectedQuantity,
                          FIPDPlannedAmount,
                          FIPDPrice
                        FROM PartsInboundPlanDetail
                        WHERE PartsInboundPlanDetail.PartsInboundPlanId = FIPId
                        AND PartsInboundPlanDetail.SparePartId          = FIPPartId;
                        IF FIPDPrice                                    < 0 THEN
                          BEGIN
                            FErrormsg := '配件价格小于0，请进行核对！配件图号：' || Vtable_indetails(Vi) .SparePartCode;
                            FDETAILID:=Vtable_indetails(Vi) .id;
                            raise FMyException;
                          END;
                        END IF;
                        BEGIN
                          --校验计划价
                          SELECT t.plannedprice
                          INTO FPPPrice
                          FROM partsplannedprice t
                          WHERE t.partssalescategoryid = FIPPartsSalesCategoryId
                          AND t.SparePartId            = FIPPartId;
                          IF FPPPrice                  < 0 THEN
                            BEGIN
                              FErrormsg := '配件计划价格小于0，请进行核对！配件图号：' || Vtable_indetails(Vi) .SparePartCode;
                              FDETAILID:=Vtable_indetails(Vi) .id;
                              raise FMyException;
                            END;
                          END IF;
                        EXCEPTION
                        WHEN no_data_found THEN
                          FErrormsg := '计划价不存在请维护' || Vtable_indetails(Vi) .SparePartCode;
                          FDETAILID:=Vtable_indetails(Vi) .id;
                          raise FMyException;
                        END;
                        --清单数量校验
                        IF (FIPDPlannedAmount >= FIPDInspectedQuantity + NVL(Vtable_indetails(Vi).Receivedamount, 0)) THEN
                          BEGIN
                            UPDATE PartsInboundPlanDetail
                            SET InspectedQuantity                           = NVL(InspectedQuantity, 0) + NVL(Vtable_indetails(Vi).Receivedamount, 0)
                            WHERE PartsInboundPlanDetail.PartsInboundPlanId = FIPId
                            AND PartsInboundPlanDetail.SparePartId          = FIPPartId;
                          END;
                        END IF;
                        IF (FIPDPlannedAmount < FIPDInspectedQuantity + NVL(Vtable_indetails(Vi).Receivedamount, 0)) THEN
                          BEGIN
                            FErrormsg := '确认量大于计划量，请进行核对！';
                            FDETAILID:=Vtable_indetails(Vi) .id;
                            raise FMyException;
                          END;
                        END IF;

                        --如果质检类型为正常质检，获取库位ID使用检验区库位ID
                       /* IF QualityType='2' THEN
                        BEGIN
                         BEGIN
                          SELECT WarehouseArea.Id,
                            WarehouseArea.Code,
                            WarehouseArea.AreaCategoryId
                          INTO FWarehouseAreaId,
                            FWarehouseAreaCode,
                            FAreaCategoryId
                          FROM WarehouseArea
                          INNER JOIN WarehouseAreaCategory
                          ON WarehouseArea.AreaCategoryId    = WarehouseAreaCategory.Id
                          WHERE WarehouseArea.WarehouseId    = FIPWarehouseId
                          AND WarehouseArea.Areakind         = 3
                          AND WarehouseAreaCategory.Category = 3 and WarehouseArea.status=1;
                        EXCEPTION
                        WHEN no_data_found THEN
                          FWarehouseAreaId := NULL;
                        WHEN too_many_rows THEN
                        BEGIN
                          FErrormsg := 'wms仓库库位不唯一';
                          raise FMyException;
                        END;
                        END;
                        END;
                        ELSE
                        BEGIN

                        END;
                        END;
                        END IF;*/
                        BEGIN
                          SELECT WarehouseArea.Id,
                            WarehouseArea.Code,
                            WarehouseArea.AreaCategoryId
                          INTO FWarehouseAreaId,
                            FWarehouseAreaCode,
                            FAreaCategoryId
                          FROM WarehouseArea
                          INNER JOIN WarehouseAreaCategory
                          ON WarehouseArea.AreaCategoryId    = WarehouseAreaCategory.Id
                          WHERE WarehouseArea.WarehouseId    = FIPWarehouseId
                          AND WarehouseArea.Areakind         = 3
                          AND WarehouseAreaCategory.Category = 1 and WarehouseArea.status=1;
                        EXCEPTION
                        WHEN no_data_found THEN
                          FWarehouseAreaId := NULL;
                        WHEN too_many_rows THEN
                        BEGIN
                          FErrormsg := 'wms仓库库位不唯一';
                          FDETAILID:=Vtable_indetails(Vi) .id;
                          raise FMyException;
                        END;
                        end;
                        /*
                        1、更新配件入库计划、入库计划清单
                        检验量=配件入库检验单清单.检验量（根据 配件Id）
                        */
                        IF FWarehouseAreaId IS NOT NULL THEN
                          BEGIN

                            BEGIN
                              SELECT PartsStock.Id
                              INTO FPartsStockId
                              FROM PartsStock
                              WHERE PartsStock.WarehouseId   = FIPWarehouseId
                              AND PartsStock.PartId          = FIPPartId
                              AND PartsStock.WarehouseAreaId = FWarehouseAreaId for update;
                            EXCEPTION
                            WHEN no_data_found THEN
                              FPartsStockId := NULL;
                            END;
                            IF FPartsStockId IS NULL THEN
                              BEGIN
                                FNewPartsStockId := s_PartsStock.Nextval;
                                INSERT
                                INTO PartsStock
                                  (
                                    Id,
                                    WarehouseId,
                                    StorageCompanyId,
                                    StorageCompanyType,
                                    BranchId,
                                    WarehouseAreaId,
                                    PartId,
                                    WarehouseAreaCategoryId,
                                    Quantity,
                                    CreatorId,
                                    CreateTime,
                                    CreatorName
                                  )
                                  VALUES
                                  (
                                    FNewPartsStockId,
                                    FIPWarehouseId,
                                    FIPStorageCompanyId,
                                    FIPStorageCompanyType,
                                    FIPBranchId,
                                    FWarehouseAreaId,
                                    FIPPartId,
                                    FAreaCategoryId,
                                    Vtable_indetails(Vi).Receivedamount,
                                    1,
                                    sysdate,
                                    'WMS'
                                  );
                              END;
                            ELSE
                              BEGIN
                                UPDATE PartsStock
                                SET Quantity        = NVL(Quantity, 0) + NVL(Vtable_indetails(Vi).Receivedamount, 0)
                                WHERE PartsStock.Id = FPartsStockId;
                              END;
                            END IF;
                            INSERT
                            INTO PartsInboundCheckBillDetail
                              (
                                Id,
                                PartsInboundCheckBillId,
                                SparePartId,
                                SparePartCode,
                                SparePartName,
                                WarehouseAreaId,
                                WarehouseAreaCode,
                                InspectedQuantity,
                                SettlementPrice,
                                CostPrice,
                              Remark,pocode)
                              SELECT FPartsInboundCheckBillDetailId,
                                FPartsInboundCheckBillId,
                                SparePartId,
                                SparePartCode,
                                SparePartName,
                                FWarehouseAreaId,
                                FWarehouseAreaCode,
                                Vtable_indetails(Vi).Receivedamount,
                                PartsInboundPlanDetail.Price,
                                FPPPrice,
                                PartsInboundPlanDetail.Remark,pocode
                              FROM PartsInboundPlanDetail
                              WHERE PartsInboundPlanDetail.PartsInboundPlanId = FIPId
                              AND PartsInboundPlanDetail.SparePartId          = FIPPartId;
                            END;
                          ELSE
                            BEGIN
                              FErrormsg := 'PMS仓库对应唯一库位未维护，请核对！';
                              FDETAILID:=Vtable_indetails(Vi) .id;
                              raise FMyException;
                            END;
                          END IF;
                        END;
                      ELSE
                        BEGIN
                          FErrormsg := '清单内配件与入库计划单内记录不符，请核对！';
                          FDETAILID:=Vtable_indetails(Vi) .id;
                          raise FMyException;
                        END;
                      END IF;
                    END LOOP;
                    --出库类型为调拨和采购则新增物流批次
                    IF FIPInboundType IN (1, 3) THEN
                      BEGIN
                        BEGIN
                          SELECT plb.Id
                          INTO FPartsLogisticBatchId
                          FROM PartsLogisticBatch plb
                          WHERE EXISTS
                            (SELECT 1
                            FROM PartsLogisticBatchBillDetail plbbd
                            WHERE plbbd.BillId             = FIPId
                            AND plbbd.BillType             = 4
                            AND plbbd.PartsLogisticBatchId = plb.Id
                            )

   and ((plb.sourcetype = 1 and plb.sourceid = (select id
                           from partsoutboundplan
                          where originalrequirementbillid =
                                (select originalrequirementbillid
                                   from partsinboundplan
                                  where id = FIPId
                                    and inboundtype = 3)
                            and outboundtype = 4
                            and originalrequirementbilltype=6
                            )) or
       (plb.sourcetype = 2 and
       plb.sourceid = (select originalrequirementbillid
                           from partsinboundplan
                          where id = FIPId
                            and inboundtype = 1
                            and originalrequirementbilltype=4
                            )));

                        EXCEPTION
                        WHEN no_data_found THEN
                          FPartsLogisticBatchId := NULL;
                        END;
                        IF FPartsLogisticBatchId IS NOT NULL THEN
                          BEGIN
                            UPDATE PartsLogisticBatchItemDetail plbid
                            SET plbid.InboundAmount =
                              (SELECT picbd.inspectedquantity
                              FROM PartsInboundCheckBillDetail picbd
                              WHERE picbd.SparePartId           = plbid.sparepartid
                              AND picbd.partsinboundcheckbillid = FPartsInboundCheckBillId
                              )
                            WHERE plbid.PartsLogisticBatchId = FPartsLogisticBatchId
                            AND EXISTS
                              (SELECT 1
                              FROM PartsInboundCheckBillDetail picbd1
                              WHERE picbd1.SparePartId           = plbid.sparepartid
                              AND picbd1.partsinboundcheckbillid = FPartsInboundCheckBillId
                              );
                            UPDATE PartsLogisticBatch
                            SET Status = 2
                            WHERE ID   = FPartsLogisticBatchId
                            AND NOT EXISTS
                              (SELECT 1
                              FROM PartsLogisticBatchItemDetail plbid
                              WHERE plbid.InboundAmount    != plbid.OutboundAmount
                              AND plbid.PartsLogisticBatchId=FPartsLogisticBatchId
                              );
                          END;
                        END IF;
                      END;
                    END IF;
                  END;
                END IF;
                ---------------无清单强制完成--------------------
                IF Vtable_indetails.count = 0 THEN
                  BEGIN
                    IF C_getparts_inbound_checkbills.Finishflag <> '1' THEN
                      BEGIN
                        FErrormsg := '入库清单数量为空，请核对！';
                        raise FMyException;
                      END;
                    END IF;
                  END;
                END IF;
                ---------------无清单强制完成--------------------
              END;
            ELSE
              BEGIN
                FErrormsg := '仓库编号与入库计划单记录不符，请核对！';
                raise FMyException;
              END;
            END IF;
          END;
        ELSE
          BEGIN
            FErrormsg := '入库计划单编号不存在，请核对！';
            raise FMyException;
          END;
        END IF;
        --校验入库计划单是否已经全部完成
        SELECT SUM(PlannedAmount) - SUM(NVL(InspectedQuantity, 0)) AS isfinished
        INTO FIsAllFinished
        FROM PartsInboundPlanDetail
        WHERE PartsInboundPlanId = FIPId
        GROUP BY PartsInboundPlanId;
        IF (C_getparts_inbound_checkbills.Finishflag = '1' )and (FIsAllFinished>0) THEN
          BEGIN
            BEGIN
              SELECT 1
              INTO FExistsErrorTin
              FROM partsinboundcheckbill_inf
              WHERE partsinboundcheckbill_inf.code=C_getparts_inbound_checkbills.code
              AND partsinboundcheckbill_inf.infprocessstatus  =2;
            EXCEPTION
            WHEN no_data_found THEN
              FExistsErrorTin    :=NULL;
            WHEN too_many_rows THEN
              FExistsErrorTin    :=NULL;
              IF FExistsErrorTin IS NOT NULL THEN
                FErrormsg        := '存在未处理并且有问题的单据，必须先处理问题单据后才可强制完成';
                raise FMyException;
              END IF;
            END;
            BEGIN
              UPDATE PartsInboundPlan
              SET PartsInboundPlan.Status     = 3,
                PartsInboundPlan.ModifyTime   = sysdate,
                PartsInboundPlan.ModifierId   = 1,
                PartsInboundPlan.ModifierName = FWMSCreatorName,
                Remark                        = substrb(Remark ||
                ' [该单据已经由业务人员PWMS强制完成]，终止原因:' || C_getparts_inbound_checkbills.StopComment,1,400)
              WHERE PartsInboundPlan.Id = FIPId;
               /* update partspurchaseplandetail_hw a
              set a.untfulfilledqty=a.untfulfilledqty+(select nvl(c.plannedamount,0)-nvl(c.inspectedquantity,0) from
              partsinboundplan b inner join
              partsinboundplandetail c on b.id=c.partsinboundplanid
              inner join partspurchaseorder d on d.code=b.originalrequirementbillcode
              inner join partspurchaseplan_hw e on e.code=d.hwpurordercode
              where b.id=FIPId and e.id=a.partspurchaseplanid and a.partcode=c.sparepartcode),a.rowversion=SYSTIMESTAMP,
              a.endamount=nvl(a.endamount,0)+(select nvl(c.plannedamount,0)-nvl(c.inspectedquantity,0) from
              partsinboundplan b inner join
              partsinboundplandetail c on b.id=c.partsinboundplanid
              inner join partspurchaseorder d on d.code=b.originalrequirementbillcode
              inner join partspurchaseplan_hw e on e.code=d.hwpurordercode
              where b.id=FIPId and e.id=a.partspurchaseplanid and a.partcode=c.sparepartcode)
              where exists
              (select 1 from
              partsinboundplan b inner join
              partsinboundplandetail c on b.id=c.partsinboundplanid
              inner join partspurchaseorder d on d.code=b.originalrequirementbillcode
              inner join partspurchaseplan_hw e on e.code=d.hwpurordercode
              where b.id=FIPId and e.id=a.partspurchaseplanid and a.partcode=c.sparepartcode);

               update partspurchaseplandetail_hw a
              set a.untfulfilledqty=a.untfulfilledqty+(select nvl(c.plannedamount,0)-nvl(c.inspectedquantity,0) from
              partsinboundplan b inner join
              partsinboundplandetail c on b.id=c.partsinboundplanid
              inner join partstransferorder d on d.code=b.originalrequirementbillcode
              inner join partspurchaseplan_hw e on e.code=d.originalbillcode
              where b.id=FIPId and e.id=a.partspurchaseplanid and a.partcode=c.sparepartcode),a.rowversion=SYSTIMESTAMP,
              a.endamount=nvl(a.endamount,0)+(select nvl(c.plannedamount,0)-nvl(c.inspectedquantity,0) from
              partsinboundplan b inner join
              partsinboundplandetail c on b.id=c.partsinboundplanid
              inner join partspurchaseorder d on d.code=b.originalrequirementbillcode
              inner join partspurchaseplan_hw e on e.code=d.hwpurordercode
              where b.id=FIPId and e.id=a.partspurchaseplanid and a.partcode=c.sparepartcode)
              where exists
              (select 1 from
              partsinboundplan b inner join
              partsinboundplandetail c on b.id=c.partsinboundplanid
               inner join partstransferorder d on d.code=b.originalrequirementbillcode
              inner join partspurchaseplan_hw e on e.code=d.originalbillcode
              where b.id=FIPId and e.id=a.partspurchaseplanid and a.partcode=c.sparepartcode);*/
            END;
          END;
       /* ELSE
          BEGIN
            BEGIN
              IF FIsAllFinished = 0 THEN
                BEGIN
                  UPDATE PartsInboundPlan
                  SET PartsInboundPlan.Status     = 2,
                    PartsInboundPlan.ModifyTime   = sysdate,
                    PartsInboundPlan.ModifierId   = 1,
                    PartsInboundPlan.ModifierName =FWMSCreatorName
                  WHERE PartsInboundPlan.Id       = FIPId;
                END;
              ELSE
                BEGIN
                  UPDATE PartsInboundPlan
                  SET PartsInboundPlan.Status     = 4,
                    PartsInboundPlan.ModifyTime   = sysdate,
                    PartsInboundPlan.ModifierId   = 1,
                    PartsInboundPlan.ModifierName =FWMSCreatorName
                  WHERE PartsInboundPlan.Id       = FIPId;
                END;
              END IF;
            END;
          END;*/
        END IF;
        IF (FIsAllFinished = 0)and (C_getparts_inbound_checkbills.Finishflag ='1'  ) THEN
                BEGIN
                  UPDATE PartsInboundPlan
                  SET PartsInboundPlan.Status     = 2,
                    PartsInboundPlan.ModifyTime   = sysdate,
                    PartsInboundPlan.ModifierId   = 1,
                    PartsInboundPlan.ModifierName =FWMSCreatorName
                  WHERE PartsInboundPlan.Id       = FIPId;
                END;
        end if;
        IF (FIsAllFinished >0)and ((C_getparts_inbound_checkbills.Finishflag is null) or (C_getparts_inbound_checkbills.Finishflag<>'1') ) THEN
        BEGIN
          UPDATE PartsInboundPlan
          SET PartsInboundPlan.Status     = 4,
            PartsInboundPlan.ModifyTime   = sysdate,
            PartsInboundPlan.ModifierId   = 1,
            PartsInboundPlan.ModifierName =FWMSCreatorName
          WHERE PartsInboundPlan.Id       = FIPId;
        END;
        end if;
        UPDATE partsinboundcheckbill_inf
        SET partsinboundcheckbill_inf.infprocessstatus = 1
        WHERE partsinboundcheckbill_inf.pwmscode    = C_getparts_inbound_checkbills.Pwmscode
        and partsinboundcheckbill_inf.postbackcode=C_getparts_inbound_checkbills.Postbackcode;
        UPDATE PARTSINBOUNDCHECKBILL SET  PARTSINBOUNDCHECKBILL.PWMSSTORAGECODE=C_getparts_inbound_checkbills.Pwmscode
        ||C_getparts_inbound_checkbills.Postbackcode
        WHERE PARTSINBOUNDCHECKBILL.code=FNewCodePICB;
      EXCEPTION
      WHEN FMyException THEN
        BEGIN
          ROLLBACK;
          INSERT
          INTO sync_wms_inout_errlog 
            (
              ID,
              CREATEDAT,
              Message                           ,
              PwmsCode                          ,
              PmsCode                           ,
              TableName                         ,
              TABLEROWID,
              DETAILTABLEROWID,postbackcode
            )
            VALUES
            (
              s_sync_wms_inout_errlog.NEXTVAL,
              sysdate,
              FErrormsg,
              C_getparts_inbound_checkbills.Pwmscode,
              C_getparts_inbound_checkbills.CODE,
              'parts_inbound_bill',
              C_getparts_inbound_checkbills.Id,
              nvl(FDETAILID,0),C_getparts_inbound_checkbills.Postbackcode
            );
         UPDATE partsinboundcheckbill_inf
        SET partsinboundcheckbill_inf.infprocessstatus = 2
        WHERE partsinboundcheckbill_inf.pwmscode    = C_getparts_inbound_checkbills.Pwmscode
        and partsinboundcheckbill_inf.postbackcode=C_getparts_inbound_checkbills.Postbackcode;
          END;
      WHEN OTHERS THEN
        BEGIN
          ROLLBACK;
          FErrormsg := sqlerrm;
          INSERT
          INTO sync_wms_inout_errlog 
            (
              ID,
              CREATEDAT,
              Message                           ,
              PwmsCode                          ,
              PmsCode                           ,
              TableName                         ,
              TABLEROWID,
              DETAILTABLEROWID,postbackcode
            )
            VALUES
            (
              s_sync_wms_inout_errlog.NEXTVAL,
              sysdate,
              FErrormsg,
              C_getparts_inbound_checkbills.Pwmscode,
              C_getparts_inbound_checkbills.CODE,
              'parts_inbound_bill',
              C_getparts_inbound_checkbills.Id,
              nvl(FDETAILID,0),C_getparts_inbound_checkbills.Postbackcode
            );
         UPDATE partsinboundcheckbill_inf
        SET partsinboundcheckbill_inf.infprocessstatus = 2
        WHERE partsinboundcheckbill_inf.pwmscode    = C_getparts_inbound_checkbills.Pwmscode
        and partsinboundcheckbill_inf.postbackcode=C_getparts_inbound_checkbills.Postbackcode ;
        END;
      END;
      end if;
      Vtable_indetails.delete;
      COMMIT;
    END LOOP;
  END;
