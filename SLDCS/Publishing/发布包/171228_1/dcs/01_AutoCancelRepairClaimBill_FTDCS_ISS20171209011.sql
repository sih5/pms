create or replace procedure AutoCancelRepairClaimBill is

  cursor RepairClaimBills is(
    select id, RepairOrderId, ClaimBillCode
      from RepairClaimBill t
     where t.repairtype = 3 /*强制保养*/
       and add_months(sysdate, -3) >= t.createtime
       and t.status = 1
       and exists (select id
              from Branchstrategy b
             where b.isdealclaimbill = 1
               and b.branchid = t.branchid));
begin
  for s_RepairClaimBills in RepairClaimBills loop
    begin
      update RepairClaimBill t
         set t.status        = 99,
             t.abandonerid   = 1,
             t.abandonername = 'Admin',
             t.abandontime   = sysdate,
             t.CancelReason  = '索赔单' || s_RepairClaimBills.ClaimBillCode ||
                               ' 3月未提交'
       where t.id = s_RepairClaimBills.id;
    
      update RepairOrder t
         set t.status = 7 /*生成索赔*/
       where t.id = s_RepairClaimBills.RepairOrderId
         and t.status = 6; /*维修完工*/
    end;
  end loop;

end AutoCancelRepairClaimBill;
