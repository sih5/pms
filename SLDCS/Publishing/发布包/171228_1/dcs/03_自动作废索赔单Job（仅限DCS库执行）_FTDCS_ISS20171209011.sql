begin
  sys.dbms_scheduler.create_job(job_name            => '自动作废索赔单',
                                job_type            => 'STORED_PROCEDURE',
                                job_action          => 'AutoCancelRepairClaimBill',
                                start_date          => to_date('11-12-2017 05:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;Interval=1',
                                end_date            => to_date(null),
                                job_class           => 'DEFAULT_JOB_CLASS',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => '自动作废超过3个月未提交的强制保养索赔单');
end;


