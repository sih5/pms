alter table RedPacketsMsg add VIN VARCHAR2(50);

create sequence S_FundRedPostVehicleMsg
/

/*==============================================================*/
/* Table: FundRedPostVehicleMsg                                 */
/*==============================================================*/
create table FundRedPostVehicleMsg  (
   Id                   NUMBER(9)                       not null,
   RedPostCode          VARCHAR2(50)                    not null,
   VIN                  VARCHAR2(50)                    not null,
   BrandName            VARCHAR2(100),
   RedPostName          VARCHAR2(100)                   not null,
   RedPostDescribe      VARCHAR2(200)                   not null,
   CustomerName         VARCHAR2(100)                   not null,
   CellPhoneNumber      VARCHAR2(100)                   not null,
   RedPostAmount        NUMBER(19,4)                    not null,
   LimitPrice           NUMBER(19,4)                    not null,
   ValidationDate       DATE                            not null,
   ExpireDate           DATE                            not null,
   LssuingUnit          NUMBER(9),
   Status               NUMBER(9)                       not null,
   RepairContractCode   VARCHAR2(150),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_FUNDREDPOSTVEHICLEMSG primary key (Id)
)
/