alter table VehicleInformation add RealSalesType NUMBER(9);

alter table Customer add CustomerIdentity NUMBER(9);
alter table Customer add CustomerNature NUMBER(9);
alter table Customer add ProductQuality NUMBER(9);
alter table Customer add CompetitiveQuality NUMBER(9);
alter table Customer add RegionalIndustryRankings VARCHAR2(30);
alter table Customer add EngageIndustry VARCHAR2(30);
alter table Customer add IsAnchored NUMBER(1);
alter table Customer add AnchoredBranch VARCHAR2(100);
alter table Customer add CustomerStatus VARCHAR2(30);
alter table Customer add IsDecisionMaker NUMBER(1);