alter table DealerHistory add GeographicPosition_new NUMBER(9);
update DealerHistory
  set GeographicPosition_new = to_number(GeographicPosition)
 where GeographicPosition is not null;   
alter table DealerHistory drop column GeographicPosition; 
alter table DealerHistory rename column GeographicPosition_new to GeographicPosition;

alter table DealerServiceExt add GeographicPosition_new NUMBER(9);
update DealerServiceExt
  set GeographicPosition_new = to_number(GeographicPosition)
 where GeographicPosition is not null;   
alter table DealerServiceExt drop column GeographicPosition;  
alter table DealerServiceExt rename column GeographicPosition_new to GeographicPosition;

alter table DealerServiceExtHistory add GeographicPosition_new NUMBER(9);
update DealerServiceExtHistory
  set GeographicPosition_new = to_number(GeographicPosition)
 where GeographicPosition is not null;  
alter table DealerServiceExtHistory drop column GeographicPosition;  
alter table DealerServiceExtHistory rename column GeographicPosition_new to GeographicPosition; 

alter table DealerHistory add AGeographicPosition_new NUMBER(9);
update DealerHistory
   set AGeographicPosition_new = to_number(AGeographicPosition)
 where AGeographicPosition is not null;
alter table DealerHistory drop column AGeographicPosition;
alter table DealerHistory rename column AGeographicPosition_new to AGeographicPosition;



