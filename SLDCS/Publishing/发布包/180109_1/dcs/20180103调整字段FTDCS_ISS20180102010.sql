alter table PartsPurchaseSettleBill add IsUnifiedSettle NUMBER(1);

alter table PartsPurchaseOrder add CPPartsInboundCheckCode VARCHAR2(50);

alter table PartsSalesOrder add CPPartsInboundCheckCode VARCHAR2(50);

alter table PartsSalesSettlement add IsUnifiedSettle NUMBER(1);

alter table PartsInboundCheckBill add CPPartsPurchaseOrderCode VARCHAR2(50);
alter table PartsInboundCheckBill add CPPartsInboundCheckCode VARCHAR2(50);

alter table PartsOutboundBill add CPPartsPurchaseOrderCode VARCHAR2(50);
alter table PartsOutboundBill add CPPartsInboundCheckCode VARCHAR2(50);
alter table PartsOutboundBill add PartsPurchaseSettleCode VARCHAR2(50);

create index ids_ppsbill_ISUNIFIEDSETTLE on PARTSPURCHASESETTLEBILL (isunifiedsettle);

create index idx_pssettle_ISUNIFIEDSETTLE on PARTSSALESSETTLEMENT (ISUNIFIEDSETTLE);