create sequence S_DealerGradeInfo
/

/*==============================================================*/
/* Table: DealerGradeInfo                                       */
/*==============================================================*/
create table DealerGradeInfo  (
   Id                   NUMBER(9)                       not null,
   PartsSalesCategoryId NUMBER(9)                       not null,
   PartsSalesCategoryName VARCHAR2(100)                   not null,
   GradeName            VARCHAR2(100)                   not null,
   Remark               VARCHAR2(200),
   Status               NUMBER(9)                       not null,
   CreateTime           DATE,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   ModifierId           NUMBER(9),
   ModifyTime           DATE,
   ModifierName         VARCHAR2(100),
   AbandonerId          NUMBER(9),
   AbandonerName        VARCHAR2(100),
   AbandonTime          DATE,
   RowVersion           TIMESTAMP,
   constraint PK_DEALERGRADEINFO primary key (Id)
)
/

alter table DealerServiceInfo add DealerGradeId NUMBER(9);
alter table DealerServiceInfo add DealerGradeName VARCHAR2(100);