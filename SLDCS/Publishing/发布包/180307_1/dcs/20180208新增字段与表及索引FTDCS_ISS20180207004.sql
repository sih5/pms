alter table RepairWorkOrderMd add MemberType VARCHAR2(100);

create sequence S_RepairWorkOrderMdFaultReason
/

/*==============================================================*/
/* Table: RepairWorkOrderMdFaultReason                          */
/*==============================================================*/
create table RepairWorkOrderMdFaultReason  (
   Id                   NUMBER(9)                       not null,
   RepairWorkOrderMdId  NUMBER(9)                       not null,
   MalfunctionCode      VARCHAR2(100),
   MalfunctionDescription VARCHAR2(1000),
   RepairComment        VARCHAR2(1000),
   constraint PK_REPAIRWORKORDERMDFAULTREASO primary key (Id)
)
/