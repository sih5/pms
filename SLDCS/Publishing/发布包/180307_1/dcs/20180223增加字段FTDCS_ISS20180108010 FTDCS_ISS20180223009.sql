alter table SubDealer add BusinessAddressLongitude NUMBER(14,6);
alter table SubDealer add BusinessAddressLatitude NUMBER(14,6);

alter table RetainedCustomer add Path VARCHAR2(2000);