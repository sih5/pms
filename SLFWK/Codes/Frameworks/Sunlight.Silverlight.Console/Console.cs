﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;
using Microsoft.Scripting.Silverlight;

namespace Sunlight.Silverlight.Core {
    public class Console : UserControl {
        public sealed class StringWrittenEventArgs : EventArgs {
            public string Value {
                get;
                internal set;
            }
        }

        public sealed class ConsoleStreamWriter : StreamWriter {
            public event EventHandler<StringWrittenEventArgs> StringWritten;
            private readonly StringBuilder buffer;

            private void OnStringWritten(string value) {
                var handler = this.StringWritten;
                if(handler != null)
                    handler(this, new StringWrittenEventArgs {
                        Value = value
                    });
            }

            internal ConsoleStreamWriter(Stream stream) : base(stream) {
                this.buffer = new StringBuilder();
            }

            public override void Write(string value) {
                base.Write(value);
                if(value == Environment.NewLine) {
                    var output = this.buffer.ToString();
                    this.buffer.Clear();
                    this.OnStringWritten(output);
                } else
                    this.buffer.Append(value);
            }
        }

        private static Popup popup;

        public static void Toggle() {
            if(popup == null) {
                popup = new Popup {
                    Child = new Console()
                };
                popup.Opened += (sender, e) => {
                    if(popup.Tag != null)
                        return;
                    popup.Tag = true;
                    var control = (FrameworkElement)((UserControl)popup.Child).Content;
                    popup.HorizontalOffset = (Application.Current.Host.Content.ActualWidth - control.Width) / 2;
                    popup.VerticalOffset = (Application.Current.Host.Content.ActualHeight - control.Height) / 2;
                };
            }
            popup.IsOpen = !popup.IsOpen;
        }

        private readonly ScrollViewer scroller;
        private readonly StackPanel panel;
        private readonly TextBox inputBox;
        private readonly ScriptEngine engine;
        private readonly ScriptScope scope;
        private readonly List<string> histories;
        private bool isMouseCaptured;
        private Point mousePosition;
        private int historyIndex;

        private Grid CreatePromptText(string text) {
            var result = new Grid();
            result.Children.Add(new TextBlock {
                Style = (Style)this.Resources["Prompt"]
            });
            result.Children.Add(new TextBox {
                Style = (Style)this.Resources["ResultText"],
                Text = text,
                Margin = new Thickness(24, 0, 0, 0)
            });
            return result;
        }

        private void ScrollDown() {
            this.Dispatcher.BeginInvoke(() => this.scroller.ScrollToVerticalOffset(this.scroller.ExtentHeight - this.scroller.ViewportHeight));
        }

        private void AppendInput(string input) {
            this.panel.Children.Add(this.CreatePromptText(input));
            this.ScrollDown();
        }

        private void AppendResult(string result) {
            var text = new TextBox {
                Style = (Style)this.Resources["ResultText"],
                Text = result
            };
            this.panel.Children.Add(text);
            this.ScrollDown();
        }

        private void ExecuteCode(string code) {
            try {
                var result = this.engine.Execute(code, this.scope);
                if(result != null)
                    this.AppendResult(this.engine.Operations.Format(result));
            } catch(Exception ex) {
                this.AppendResult(string.Format("{0}: {1}", ex.GetType().Name, ex.Message));
            }
        }

        private void inputBox_KeyDown(object sender, KeyEventArgs e) {
            switch(e.Key) {
                case Key.Enter:
                    var input = this.inputBox.Text;
                    this.inputBox.Text = string.Empty;
                    if(string.IsNullOrWhiteSpace(input))
                        return;

                    this.histories.Remove(input);
                    if(this.histories.Count == this.histories.Capacity)
                        this.histories.RemoveAt(0);
                    this.histories.Add(input);
                    this.historyIndex = this.histories.Count - 1;

                    this.AppendInput(input);
                    this.ExecuteCode(input);
                    break;
                case Key.Up:
                    if(this.historyIndex == -1)
                        return;
                    this.Dispatcher.BeginInvoke(() => {
                        this.inputBox.Text = this.histories[this.historyIndex];
                        this.inputBox.SelectAll();
                    });
                    if(this.historyIndex <= 0)
                        this.historyIndex = this.histories.Count - 1;
                    else
                        this.historyIndex--;
                    break;
                case Key.Down:
                    if(this.historyIndex == -1)
                        return;
                    this.historyIndex++;
                    if(this.historyIndex >= this.histories.Count)
                        this.historyIndex = 0;
                    this.Dispatcher.BeginInvoke(() => {
                        this.inputBox.Text = this.histories[this.historyIndex];
                        this.inputBox.SelectAll();
                    });
                    break;
            }
        }

        private Console() {
            var stderrStream = new MemoryStream();
            var stdoutStream = new MemoryStream();
            var stderrWriter = new ConsoleStreamWriter(stderrStream);
            stderrWriter.StringWritten += (sender, e) => this.AppendResult(e.Value);
            var stdoutWriter = new ConsoleStreamWriter(stdoutStream);
            stdoutWriter.StringWritten += (sender, e) => this.AppendResult(e.Value);

            this.engine = Scripting.CreatePythonEngine();
            this.engine.Runtime.IO.SetErrorOutput(stderrStream, stderrWriter);
            this.engine.Runtime.IO.SetOutput(stdoutStream, stdoutWriter);
            this.scope = this.engine.CreateScope();

            var client = new WebClient();
            client.DownloadStringCompleted += (sender, e) => {
                if(e.Error == null)
                    this.ExecuteCode(e.Result);
            };
            client.DownloadStringAsync(new Uri(Application.Current.Host.Source, "../Scripting/GetInitScript.aspx"));

            this.Resources.MergedDictionaries.Add(new ResourceDictionary {
                Source = new Uri("/Sunlight.Silverlight.Console;component/ConsoleResources.xaml", UriKind.Relative)
            });

            Border border;
            this.Content = border = new Border {
                Width = Application.Current.Host.Content.ActualWidth / 1.5,
                Height = Application.Current.Host.Content.ActualHeight / 1.5,
                BorderThickness = new Thickness(2),
                CornerRadius = new CornerRadius(3),
                BorderBrush = new SolidColorBrush(Color.FromArgb(255, 108, 226, 108)) {
                    Opacity = 0.8
                },
            };

            Grid grid;
            border.Child = grid = new Grid {
                Background = new SolidColorBrush(Colors.Black) {
                    Opacity = 0.8,
                },
            };
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            grid.MouseLeftButtonDown += (sender, e) => {
                this.mousePosition = e.GetPosition(null);
                this.panel.CaptureMouse();
                this.isMouseCaptured = true;
            };
            grid.MouseLeftButtonUp += (sender, e) => {
                this.isMouseCaptured = false;
                this.panel.ReleaseMouseCapture();
            };
            grid.MouseMove += (sender, e) => {
                if(!this.isMouseCaptured)
                    return;
                var currentMousePosition = e.GetPosition(null);
                popup.HorizontalOffset += currentMousePosition.X - this.mousePosition.X;
                popup.VerticalOffset += currentMousePosition.Y - this.mousePosition.Y;
                this.mousePosition = currentMousePosition;
            };
            grid.MouseWheel += (sender, e) => {
                var max = this.scroller.ExtentHeight - this.scroller.ViewportHeight;
                var offset = this.scroller.VerticalOffset - e.Delta;
                if(offset < 0)
                    offset = 0;
                else if(offset > max)
                    offset = max;
                this.Dispatcher.BeginInvoke(() => this.scroller.ScrollToVerticalOffset(offset));
            };

            grid.Children.Add(this.scroller = new ScrollViewer {
                //Style = Application.Current.Resources["System.Windows.Controls.ScrollViewer"] as Style,
                BorderThickness = new Thickness(0),
                Padding = new Thickness(0),
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled,
                VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
                Content = this.panel = new StackPanel(),
            });

            var prompt = new TextBlock {
                Style = (Style)this.Resources["Prompt"]
            };
            prompt.SetValue(Grid.RowProperty, 1);
            grid.Children.Add(prompt);

            grid.Children.Add(this.inputBox = new TextBox {
                Style = (Style)this.Resources["InputBox"]
            });
            this.inputBox.SetValue(Grid.RowProperty, 1);
            this.inputBox.KeyDown += this.inputBox_KeyDown;

            this.histories = new List<string>(20);
            this.historyIndex = -1;

            this.AppendResult(string.Format("Sunlight Silverlight Debugging Console on IronPython {0}\nCopyright (c) Sunlight Data System. All rights reserved.", this.engine.LanguageVersion));
        }
    }

    public static class Scripting {
        public static ScriptEngine CreatePythonEngine() {
            return Python.GetEngine(new ScriptRuntime(DynamicEngine.CreateRuntimeSetup()));
        }
    }
}
