﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Controls.Maps {
    public class MapMarker : Panel, INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private readonly Ellipse outerCircle, innerCircle;
        private readonly TextBlock textBlock;
        private string text;

        public string Text {
            get {
                return this.text;
            }
            set {
                this.text = value;
                this.OnPropertyChanged("Text");
                this.InvalidateMeasure();
            }
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected override Size MeasureOverride(Size availableSize) {
            if(availableSize.Width <= 0 || availableSize.Height <= 0)
                return new Size(0, 0);

            double finalWidth, finalHeight;
            if(availableSize.Width / availableSize.Height < 1) {
                finalWidth = availableSize.Width;
                finalHeight = finalWidth;
            } else {
                finalHeight = availableSize.Height;
                finalWidth = finalHeight;
            }

            this.outerCircle.Measure(new Size(finalWidth, finalHeight));
            this.innerCircle.Measure(new Size(finalWidth * 0.8, finalHeight * 0.8));

            if(this.textBlock.ActualWidth > 0 && this.textBlock.ActualHeight > 0) {
                var maxTextSize = finalWidth * 0.7;
                var textSize = new Size(this.textBlock.ActualWidth, this.textBlock.ActualHeight);
                if(textSize.Height > textSize.Width) {
                    while(textSize.Height < maxTextSize) {
                        this.textBlock.FontSize++;
                        textSize = new Size(this.textBlock.ActualWidth, this.textBlock.ActualHeight);
                        if(textSize.Height <= maxTextSize)
                            continue;
                        this.textBlock.FontSize--;
                        goto done;
                    }
                    while(textSize.Height > maxTextSize) {
                        this.textBlock.FontSize--;
                        if(this.textBlock.FontSize <= 0)
                            goto done;
                        textSize = new Size(this.textBlock.ActualWidth, this.textBlock.ActualHeight);
                    }
                } else {
                    while(textSize.Width < maxTextSize) {
                        this.textBlock.FontSize++;
                        textSize = new Size(this.textBlock.ActualWidth, this.textBlock.ActualHeight);
                        if(textSize.Width <= maxTextSize)
                            continue;
                        this.textBlock.FontSize--;
                        goto done;
                    }
                    while(textSize.Width > maxTextSize) {
                        this.textBlock.FontSize--;
                        if(this.textBlock.FontSize <= 0)
                            goto done;
                        textSize = new Size(this.textBlock.ActualWidth, this.textBlock.ActualHeight);
                    }
                }
            }
            done:
            this.textBlock.Measure(new Size(this.textBlock.ActualWidth, this.textBlock.ActualHeight));

            return new Size(finalWidth, finalHeight);
        }

        protected override Size ArrangeOverride(Size finalSize) {
            if(finalSize.Width <= 0 || finalSize.Height <= 0)
                return new Size(0, 0);

            double outerWidth, outerHeight;
            if(finalSize.Width / finalSize.Height < 1) {
                outerWidth = finalSize.Width;
                outerHeight = outerWidth;
            } else {
                outerHeight = finalSize.Height;
                outerWidth = outerHeight;
            }

            var innerWidth = outerWidth * 0.8;
            var innerHeight = outerHeight * 0.8;

            this.outerCircle.Arrange(new Rect(0, 0, outerWidth, outerHeight));
            this.innerCircle.Arrange(new Rect((outerWidth - innerWidth) / 2, (outerHeight - innerHeight) / 2, innerWidth, innerHeight));
            this.textBlock.Arrange(new Rect((outerWidth - this.textBlock.ActualWidth) / 2, (outerHeight - this.textBlock.ActualHeight) / 2, this.textBlock.ActualWidth, this.textBlock.ActualHeight));

            return new Size(this.DesiredSize.Width, this.DesiredSize.Height);
        }

        public MapMarker() {
            this.UseLayoutRounding = false;
            this.Children.Add(this.outerCircle = new Ellipse {
                StrokeThickness = 0,
                Fill = new SolidColorBrush(Colors.White),
            });
            this.Children.Add(this.innerCircle = new Ellipse {
                StrokeThickness = 0,
                Fill = new LinearGradientBrush {
                    StartPoint = new Point(0.517, 1),
                    EndPoint = new Point(0.483, 0),
                    GradientStops = new GradientStopCollection {
                        new GradientStop {
                            Offset = 0, Color = Color.FromArgb(0xFF, 0xA3, 0x16, 0x14)
                        },
                        new GradientStop {
                            Offset = 1, Color = Color.FromArgb(0xFF, 0xE8, 0x54, 0x33)
                        }
                    },
                },
            });
            this.Children.Add(this.textBlock = new TextBlock {
                TextAlignment = TextAlignment.Center,
                Foreground = new SolidColorBrush(Colors.White),
                FontFamily = new FontFamily("Microsoft YaHei"),
            });
            this.textBlock.SetBinding(TextBlock.TextProperty, new Binding("Text") {
                Source = this,
                Mode = BindingMode.OneWay,
            });
        }
    }
}
