﻿using System.Windows;

namespace Sunlight.Silverlight.Controls {
    public class AppExtension : Core.Model.ModelBase {
        private readonly Application application = Application.Current;

        public double ActualWidth {
            get {
                return this.application.Host.Content.ActualWidth;
            }
        }

        public double ActualHeight {
            get {
                return this.application.Host.Content.ActualHeight;
            }
        }

        public AppExtension() {
            this.application.Host.Content.Resized += (sender, args) => {
                this.NotifyOfPropertyChange("ActualWidth");
                this.NotifyOfPropertyChange("ActualHeight");
            };
        }
    }
}
