﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;
using Sunlight.Silverlight.Core.Model;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Controls.Behaviors {
    /// <summary>
    /// 针对 <see cref="RadMaskedTextInput"/> 控件，提供在用户输入时自动转换所输入的字母为大写或小写的功能。
    /// </summary>
    public class MaskedCharacterCasingBehavior : Behavior<RadMaskedTextInput> {
        public static readonly DependencyProperty CharacterCasingProperty = DependencyProperty.Register("CharacterCasing", typeof(CharacterCasing), typeof(MaskedCharacterCasingBehavior), new PropertyMetadata(default(CharacterCasing)));

        /// <summary>
        /// 输入的字母应转换为大写/小写
        /// </summary>
        public CharacterCasing CharacterCasing {
            get {
                return (CharacterCasing)this.GetValue(CharacterCasingProperty);
            }
            set {
                this.SetValue(CharacterCasingProperty, value);
            }
        }

        private void AssociatedObject_KeyDown(object sender, KeyEventArgs e) {
            if(this.CharacterCasing == CharacterCasing.Normal)
                return;

            var textBox = (RadMaskedTextInput)sender;
            if(((KeyboardModifiers.Modifiers | ModifierKeys.Shift) == ModifierKeys.Shift) && e.PlatformKeyCode >= 65 && e.PlatformKeyCode <= 90) {
                var newStr = (string)textBox.Value ?? string.Empty;
                if(textBox.SelectionLength > 0)
                    newStr = newStr.Remove(textBox.SelectionStart, textBox.SelectionLength);
                var s = new string(new[] {
                        (char)(this.CharacterCasing == CharacterCasing.Upper ? e.PlatformKeyCode : e.PlatformKeyCode + 32)
                    });
                var i = textBox.SelectionStart;
                newStr = newStr.Insert(i, s);
                textBox.Value = newStr;
                textBox.SelectionStart = i + 1;
                e.Handled = true;
            }
        }

        protected override void OnAttached() {
            base.OnAttached();
            this.AssociatedObject.KeyDown += this.AssociatedObject_KeyDown;
        }

        protected override void OnDetaching() {
            this.AssociatedObject.KeyDown -= this.AssociatedObject_KeyDown;
            base.OnDetaching();
        }
    }
}
