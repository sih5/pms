﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Calendar;

namespace Sunlight.Silverlight.Controls {
    public partial class DateTimeRangeBox {
        public static readonly DependencyProperty SelectedValueFromProperty = DependencyProperty.Register("SelectedValueFrom", typeof(DateTime?), typeof(DateTimeRangeBox), null);
        public static readonly DependencyProperty SelectedValueToProperty = DependencyProperty.Register("SelectedValueTo", typeof(DateTime?), typeof(DateTimeRangeBox), null);
        public static readonly DependencyProperty InputModeProperty = DependencyProperty.Register("InputMode", typeof(InputMode), typeof(DateTimeRangeBox), new PropertyMetadata(OnInputModeChanged));
        // Using a DependencyProperty as the backing store for DateSelectionMode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DateSelectionModeProperty =
            DependencyProperty.Register("DateSelectionMode", typeof(DateSelectionMode), typeof(DateTimeRangeBox), new PropertyMetadata(OnDateSelectionModeChanged));

        private static void OnInputModeChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args) {
            var dateTimeRangeBox = (DateTimeRangeBox)obj;
            dateTimeRangeBox.dtpFrom.InputMode = (InputMode)args.NewValue;
            dateTimeRangeBox.dtpTo.InputMode = (InputMode)args.NewValue;
        }

        public DateTime? SelectedValueFrom {
            get {
                return (DateTime?)this.GetValue(SelectedValueFromProperty);
            }
            set {
                this.SetValue(SelectedValueFromProperty, value);
            }
        }

        public DateTime? SelectedValueTo {
            get {
                return (DateTime?)this.GetValue(SelectedValueToProperty);
            }
            set {
                this.SetValue(SelectedValueToProperty, value);
            }
        }

        public InputMode InputMode {
            get {
                return (InputMode)this.GetValue(InputModeProperty);
            }
            set {
                this.SetValue(InputModeProperty, value);
            }
        }

        public DateSelectionMode DateSelectionMode {
            get {
                return (DateSelectionMode)GetValue(DateSelectionModeProperty);
            }
            set {
                SetValue(DateSelectionModeProperty, value);
            }
        }

        private static void OnDateSelectionModeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var dateTimeRangeBox = (DateTimeRangeBox)d;
            dateTimeRangeBox.dtpFrom.DateSelectionMode = (DateSelectionMode)e.NewValue;
            dateTimeRangeBox.dtpTo.DateSelectionMode = (DateSelectionMode)e.NewValue;
        }

        public DateTimeRangeBox() {
            this.InitializeComponent();
            this.dtpFrom.SetBinding(RadDateTimePicker.SelectedValueProperty, new Binding("SelectedValueFrom") {
                Source = this,
                Mode = BindingMode.TwoWay,
            });
            this.dtpTo.SetBinding(RadDateTimePicker.SelectedValueProperty, new Binding("SelectedValueTo") {
                Source = this,
                Mode = BindingMode.TwoWay,
            });
        }
    }
}
