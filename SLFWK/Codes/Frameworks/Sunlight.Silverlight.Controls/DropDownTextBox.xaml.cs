﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Controls {
    public partial class DropDownTextBox {
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(DropDownTextBox), null);
        public static readonly DependencyProperty UseBlurEffectProperty = DependencyProperty.Register("UseBlurEffect", typeof(bool), typeof(DropDownTextBox), null);
        public static readonly DependencyProperty DropDownContentProperty = DependencyProperty.Register("DropDownContent", typeof(UIElement), typeof(DropDownTextBox), null);
        public static readonly DependencyProperty IsOpenProperty = DependencyProperty.Register("IsOpen", typeof(bool), typeof(DropDownTextBox), new PropertyMetadata(IsOpenPropertyChanged));
        public static readonly DependencyProperty IsReadOnlyProperty = DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(DropDownTextBox), null);

        private static void IsOpenPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args) {
            var dropDownTextBox = (DropDownTextBox)obj;
            if((bool)args.NewValue) {
                dropDownTextBox.oldEffect = Application.Current.RootVisual.Effect;
                if(!dropDownTextBox.UseBlurEffect)
                    return;
                //((IMainUIView)Application.Current.RootVisual).IsDimmed = true;
                Application.Current.RootVisual.Effect = new BlurEffect {
                    Radius = 3,
                };
            } else
                Application.Current.RootVisual.Effect = dropDownTextBox.oldEffect;
                //((IMainUIView)Application.Current.RootVisual).IsDimmed = false;
        }

        private Effect oldEffect;

        public string Text {
            get {
                return (string)this.GetValue(TextProperty);
            }
            set {
                this.SetValue(TextProperty, value);
            }
        }

        public bool UseBlurEffect {
            get {
                return (bool)this.GetValue(UseBlurEffectProperty);
            }
            set {
                this.SetValue(UseBlurEffectProperty, value);
            }
        }

        public UIElement DropDownContent {
            get {
                return (UIElement)this.GetValue(DropDownContentProperty);
            }
            set {
                this.SetValue(DropDownContentProperty, value);
            }
        }

        public bool IsOpen {
            get {
                return (bool)this.GetValue(IsOpenProperty);
            }
            set {
                this.SetValue(IsOpenProperty, value);
            }
        }

        public bool IsReadOnly {
            get {
                return (bool)this.GetValue(IsReadOnlyProperty);
            }
            set {
                this.SetValue(IsReadOnlyProperty, value);
            }
        }

        protected override void OnMouseEnter(MouseEventArgs e) {
            base.OnMouseEnter(e);
            ((Storyboard)this.Resources["MouseOverStoryBoard"]).Begin();
        }

        protected override void OnMouseLeave(MouseEventArgs e) {
            base.OnMouseLeave(e);
            ((Storyboard)this.Resources["NormalStoryBoard"]).Begin();
        }

        protected override void OnGotFocus(RoutedEventArgs e) {
            base.OnGotFocus(e);
            this.FocusVisual.Visibility = Visibility.Visible;
            this.MainButtonChrome.BorderBrush = (Brush)Application.Current.Resources["ControlOuterBorder_Focused"];
        }

        protected override void OnLostFocus(RoutedEventArgs e) {
            base.OnLostFocus(e);
            this.FocusVisual.Visibility = Visibility.Collapsed;
            this.MainButtonChrome.SetBinding(BorderBrushProperty, new Binding("BorderBrush") {
                RelativeSource = new RelativeSource(RelativeSourceMode.TemplatedParent)
            });
        }

        public DropDownTextBox() {
            this.InitializeComponent();
            this.MainTextInput.SetBinding(TextBox.TextProperty, new Binding("Text") {
                Source = this,
                Mode = BindingMode.TwoWay,
            });
            this.MainTextInput.SetBinding(TextBox.IsReadOnlyProperty, new Binding("IsReadOnly") {
                Source = this,
                Mode = BindingMode.TwoWay,
            });
            this.MainDropDownButton.SetBinding(RadDropDownButton.DropDownContentProperty, new Binding("DropDownContent") {
                Source = this,
                Mode = BindingMode.TwoWay,
            });
            this.MainDropDownButton.SetBinding(RadDropDownButton.IsOpenProperty, new Binding("IsOpen") {
                Source = this,
                Mode = BindingMode.TwoWay,
            });
        }
    }
}
