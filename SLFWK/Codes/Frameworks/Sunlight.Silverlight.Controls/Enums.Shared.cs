﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Controls {
    /// <summary>
    /// 边框类型
    /// </summary>
    public enum StartupLocationType {
        左上,
        居中,
        控件下方
    }
}
