﻿
namespace Sunlight.Silverlight.Controls {
    /// <summary>
    /// 边框类型
    /// </summary>
    public enum StartupLocationType {
        左上,
        居中,
        控件下方
    }
}
