﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Sunlight.Silverlight.Controls.Resources;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Telerik.Windows.Controls;
using Clipboard = Telerik.Windows.Controls.Clipboard;
using ItemsControl = System.Windows.Controls.ItemsControl;

namespace Sunlight.Silverlight.Controls {
    public class FilterPanel : UserControl, IFilterPanel {
        public static readonly DependencyProperty RowSizeProperty = DependencyProperty.Register("RowSize", typeof(int), typeof(FilterPanel), new PropertyMetadata(2));

        private static object CheckStringValue(object value) {
            var s = value as string;
            if(s == null)
                return value;
            return string.IsNullOrWhiteSpace(s) ? null : value;
        }


        private static object GetValue(QueryItem queryItem, FrameworkElement control) {
            if(queryItem is KeyValuesQueryItem || queryItem is ComboQueryItem)
                if(control is MultiSelectComboBoxControl && queryItem is KeyValuesQueryItem)
                    return ((MultiSelectComboBoxControl)control).SelectedValues;
            if(control is MultiSelectComboBoxControl)
                return ((MultiSelectComboBoxControl)control).SelectedValues;
            if(control is RadComboBox)
                return ((RadComboBox)control).SelectedValue;
            var dropDownQueryItem = queryItem as DropDownQueryItem;
            if(dropDownQueryItem != null)
                return dropDownQueryItem.GetQueryValue == null ? CheckStringValue(((DropDownTextBox)control).Text) : CheckStringValue(dropDownQueryItem.GetQueryValue());

            if(queryItem is DateTimeRangeQueryItem)
                return new[] {
                    ((DateTimeRangeBox)control).SelectedValueFrom, ((DateTimeRangeBox)control).SelectedValueTo
                };

            if(queryItem is CustomControlQueryItem) {
                // ReSharper disable once SuspiciousTypeConversion.Global
                var filterPanelControl = control as IFilterPanelControl;
                return filterPanelControl == null ? null : filterPanelControl.GetFilterValue();
            }

            if(queryItem is NumericRangeQueryItem)
                return new[] {
                    ((NumericRangeBox)control).SelectedValueFrom, ((NumericRangeBox)control).SelectedValueTo
                };


            var popupQueryItem = queryItem as PopupQueryItem;
            if(popupQueryItem != null)
                return popupQueryItem.GetQueryValue == null ? CheckStringValue(((PopupTextBox)control).Text) : CheckStringValue(popupQueryItem.GetQueryValue());

            var radMaskedTextBox = control as RadWatermarkTextBox;
            if(radMaskedTextBox != null)
                return CheckStringValue(radMaskedTextBox.Text);

            var radDateTimePicker = control as RadDateTimePicker;
            if(radDateTimePicker != null)
                return radDateTimePicker.SelectedValue;

            var popupTextBox = control as PopupTextBox;
            if(popupTextBox != null)
                return popupTextBox.Text;

            var marskedInputBox = control as RadMaskedInputBase;
            if(marskedInputBox != null)
                return string.IsNullOrEmpty(marskedInputBox.Text) ? null : marskedInputBox.Text;

            var checkBox = control as CheckBox;
            return checkBox != null ? checkBox.IsChecked : null;
        }

        private Type entityType;
        private Grid root;
        private List<HiddenQueryItem> hiddenQueryItems;
        private Dictionary<QueryItem, FrameworkElement> currentControls;
        private Dictionary<KeyValuesQueryItem, object> kvQueryItemValues;
        public event EventHandler FiltersAccepted;

        public int RowSize {
            get {
                return (int)this.GetValue(RowSizeProperty);
            }
            set {
                this.SetValue(RowSizeProperty, value);
            }
        }

        private void OnFiltersAccepted() {
            var handler = this.FiltersAccepted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        private void OnTextBoxKeyDown(object sender, KeyEventArgs e) {
            if(e.Key != Key.Enter)
                return;
            e.Handled = true;
            this.OnFiltersAccepted();
        }

        private Control GetFilterControl(Type type, QueryItem queryItem) {
            //字典项查询控件
            var keyValuesQueryItem = queryItem as KeyValuesQueryItem;
            if(keyValuesQueryItem != null) {
                if(!keyValuesQueryItem.AllowMultiSelect) {
                    var comboBox = new RadComboBox();
                    comboBox.CanKeyboardNavigationSelectItems = true;
                    comboBox.ClearSelectionButtonVisibility = keyValuesQueryItem.ClearSelectionButtonVisibility;
                    comboBox.ClearSelectionButtonContent = ControlUIStrings.FilterPanel_ComboBox_Clear;
                    comboBox.IsEditable = false;
                    comboBox.IsMouseWheelEnabled = true;
                    comboBox.ItemsSource = keyValuesQueryItem.KeyValueItems;
                    comboBox.SelectedValuePath = "Key";
                    comboBox.SelectedValue = keyValuesQueryItem.DefaultValue;
                    comboBox.ItemTemplate = keyValuesQueryItem.DataTemplate;
                    if(comboBox.ItemTemplate != null)
                        comboBox.SelectionBoxTemplate = keyValuesQueryItem.SelectDataTemplate;
                    else
                        comboBox.DisplayMemberPath = "Value";
                    keyValuesQueryItem.KeyValueItems.CollectionChanged += (sender, e) => {
                        if(e.Action != NotifyCollectionChangedAction.Add)
                            return;
                        comboBox.SelectedValue = this.kvQueryItemValues != null && this.kvQueryItemValues.ContainsKey(keyValuesQueryItem) ? this.kvQueryItemValues[keyValuesQueryItem] : keyValuesQueryItem.DefaultValue;
                    };
                    return comboBox;
                } else {
                    var multiSelectComboBoxControl = new MultiSelectComboBoxControl {
                        DefaultValues = queryItem.DefaultValue as List<int>,
                        DisplayMemberPath = "Value",
                        SelectedValuePath = "Key",
                    };
                    multiSelectComboBoxControl.RadListBox.SetBinding(ItemsControl.ItemsSourceProperty, new Binding("KeyValueItems") {
                        Mode = BindingMode.OneWay,
                        Source = keyValuesQueryItem,
                    });
                    return multiSelectComboBoxControl;
                }
            }

            //下拉文本框式查询控件
            var dropDownQueryItem = queryItem as DropDownQueryItem;
            if(dropDownQueryItem != null) {
                var dropDownTextBox = new DropDownTextBox {
                    UseBlurEffect = dropDownQueryItem.UseBlurEffect,
                    DropDownContent = dropDownQueryItem.DropDownContent,
                };
                dropDownQueryItem.SetDropDownOpen = isOpen => this.Dispatcher.BeginInvoke(() => dropDownTextBox.IsOpen = isOpen);
                dropDownQueryItem.SetText = text => this.Dispatcher.BeginInvoke(() => dropDownTextBox.Text = text);
                return dropDownTextBox;
            }

            //日期控件
            var dateTimeQueryItem = queryItem as DateTimeQueryItem;
            if(dateTimeQueryItem != null) {
                var dateTimePicker = new RadDateTimePicker {
                    InputMode = dateTimeQueryItem.InputMode,
                    DateTimeWatermarkContent = string.Empty,
                };
                if(dateTimeQueryItem.DefaultValue is DateTime)
                    dateTimePicker.SelectedValue = (DateTime)dateTimeQueryItem.DefaultValue;
                return dateTimePicker;
            }

            //日期范围控件
            var dateTimeRangeQueryItem = queryItem as DateTimeRangeQueryItem;
            if(dateTimeRangeQueryItem != null) {
                var rangeBox = new DateTimeRangeBox {
                    InputMode = dateTimeRangeQueryItem.InputMode
                };
                if(queryItem.DefaultValue is DateTime[]) {
                    var range = (DateTime[])queryItem.DefaultValue;
                    if(range.Length > 0)
                        rangeBox.SelectedValueFrom = range[0];
                    if(range.Length > 1)
                        rangeBox.SelectedValueTo = range[1];
                } else if(queryItem.DefaultValue is DateTime?[]) {
                    var range = (DateTime?[])queryItem.DefaultValue;
                    if(range.Length > 0)
                        rangeBox.SelectedValueFrom = range[0];
                    if(range.Length > 1)
                        rangeBox.SelectedValueTo = range[1];
                }
                return rangeBox;
            }

            //数值范围控件
            var numericRangeQueryItem = queryItem as NumericRangeQueryItem;
            if(numericRangeQueryItem != null) {
                var rangeBox = new NumericRangeBox {
                    ValueFormat = numericRangeQueryItem.ValueFormat,
                    DecimalDigits = numericRangeQueryItem.DecimalDigits,
                };
                if(queryItem.DefaultValue is Double[]) {
                    var range = (Double[])queryItem.DefaultValue;
                    if(range.Length > 0)
                        rangeBox.SelectedValueFrom = range[0];
                    if(range.Length > 1)
                        rangeBox.SelectedValueTo = range[1];
                } else if(queryItem.DefaultValue is Double?[]) {
                    var range = (Double?[])queryItem.DefaultValue;
                    if(range.Length > 0)
                        rangeBox.SelectedValueFrom = range[0];
                    if(range.Length > 1)
                        rangeBox.SelectedValueTo = range[1];
                } if(queryItem.DefaultValue is Int32[]) {
                    var range = (Int32[])queryItem.DefaultValue;
                    if(range.Length > 0)
                        rangeBox.SelectedValueFrom = range[0];
                    if(range.Length > 1)
                        rangeBox.SelectedValueTo = range[1];
                } else if(queryItem.DefaultValue is Int32?[]) {
                    var range = (Int32?[])queryItem.DefaultValue;
                    if(range.Length > 0)
                        rangeBox.SelectedValueFrom = range[0];
                    if(range.Length > 1)
                        rangeBox.SelectedValueTo = range[1];
                }
                return rangeBox;
            }
            //ComboBox 控件
            var comboQueryItem = queryItem as ComboQueryItem;
            if(comboQueryItem != null) {
                var comboBox = new RadComboBox {
                    CanKeyboardNavigationSelectItems = true,
                    ClearSelectionButtonVisibility = Visibility.Visible,
                    ClearSelectionButtonContent = ControlUIStrings.FilterPanel_ComboBox_Clear,
                    IsEditable = false,
                    IsMouseWheelEnabled = true,
                    DisplayMemberPath = comboQueryItem.DisplayMemberPath,
                    SelectedValuePath = comboQueryItem.SelectedValuePath,
                    SelectedValue = comboQueryItem.DefaultValue,
                };
                comboBox.SetBinding(ItemsControl.ItemsSourceProperty, new Binding("ItemsSource") {
                    Mode = BindingMode.OneWay,
                    Source = comboQueryItem,
                });
                if(comboQueryItem.SelectedItemBinding != null)
                    comboBox.SetBinding(Selector.SelectedItemProperty, comboQueryItem.SelectedItemBinding);
                return comboBox;
            }

            //PopupTextBox 控件
            var popupQueryItem = queryItem as PopupQueryItem;
            if(popupQueryItem != null) {
                var popupTextBox = new PopupTextBox {
                    PopupContent = popupQueryItem.PopupContent,
                    Text = popupQueryItem.Text,
                    QueryExecute = popupQueryItem.QueryExecute,
                    IsUpdateCase = popupQueryItem.IsDefaultToUpper.GetValueOrDefault(),
                };
                var contextMenu = this.AddRadContextMenu(popupTextBox);
                RadContextMenu.SetContextMenu(popupTextBox, contextMenu);
                popupQueryItem.SetText = text => this.Dispatcher.BeginInvoke(() => popupTextBox.Text = text);
                popupTextBox.KeyDown += this.OnTextBoxKeyDown;
                return popupTextBox;
            }

            var customControlQueryItem = queryItem as CustomControlQueryItem;
            if(customControlQueryItem != null)
                return customControlQueryItem.CreateControl == null ? null : customControlQueryItem.CreateControl(type);

            Control result = null;

            string typeFullName;
            if(queryItem is CustomQueryItem) {
                var dataType = ((CustomQueryItem)queryItem).DataType;
                if(dataType == null)
                    return null;
                typeFullName = dataType.FullName;
            } else {
                var propertyInfo = type.GetProperty(queryItem.ColumnName);
                if(propertyInfo == null)
                    return null;
                var isNullable = propertyInfo.PropertyType.IsGenericType && propertyInfo.PropertyType.GetGenericTypeDefinition().FullName == "System.Nullable`1";
                typeFullName = isNullable ? propertyInfo.PropertyType.GetGenericArguments()[0].FullName : propertyInfo.PropertyType.FullName;
            }

            switch(typeFullName) {
                case "System.Boolean":
                    result = new CheckBox {
                        IsChecked = queryItem.DefaultValue != null ? Convert.ToBoolean(queryItem.DefaultValue) : (bool?)null,
                        IsThreeState = queryItem.IsThreeState,
                    };
                    break;
                case "System.String":
                    var textBox = new RadWatermarkTextBox {
                        SelectionOnFocus = SelectionOnFocus.SelectAll,
                        Text = (string)queryItem.DefaultValue ?? "",
                    };
                    result = textBox;
                    result.KeyDown += this.OnTextBoxKeyDown;
                    textBox.TextChanged += textBox_TextChanged;
                    var contextMenu = this.AddRadContextMenu(textBox);
                    RadContextMenu.SetContextMenu(textBox, contextMenu);
                    break;
                case "System.DateTime":
                    result = new RadDateTimePicker {
                        InputMode = InputMode.DatePicker,
                        DateTimeWatermarkContent = string.Empty,
                    };
                    if(queryItem.DefaultValue is DateTime)
                        (result as RadDateTimePicker).SelectedValue = (DateTime)queryItem.DefaultValue;
                    break;
                case "System.Int16":
                case "System.Int32":
                case "System.Int64":
                case "System.UInt16":
                case "System.UInt32":
                case "System.UInt64":
                case "System.Double":
                case "System.Decimal":
                    result = new RadMaskedNumericInput {
                        Mask = "",
                        //Mask = typeFullName == "System.Double" ? "F" : typeFullName == "System.Decimal" ? "N" : "D",
                        UpdateValueEvent = UpdateValueEvent.PropertyChanged,
                        Value = queryItem.DefaultValue == null ? (double?)null : Convert.ToDouble(queryItem.DefaultValue),
                    };
                    result.KeyDown += this.OnTextBoxKeyDown;
                    break;
            }
            return result;
        }
        void textBox_TextChanged(object sender, TextChangedEventArgs e) {
            var textBox = (TextBox)sender;
            if(textBox == null)
                return;
            var queryItem = textBox.Tag as QueryItem;
            if(queryItem != null)
                if(queryItem.IsDefaultToUpper.GetValueOrDefault() || (queryItem.ColumnName.IndexOf("Code", StringComparison.OrdinalIgnoreCase) >= 0 && queryItem.IsDefaultToUpper == null)) {
                    var selectionStart = textBox.SelectionStart;
                    var selectionLength = textBox.SelectionLength;
                    textBox.Text = textBox.Text.ToUpper();
                    textBox.SelectionStart = selectionStart;
                    textBox.SelectionLength = selectionLength;
                }
        }

        private RadContextMenu AddRadContextMenu(Control popupTextBox) {
            RadContextMenu contextMenu = new RadContextMenu();
            RadMenuItem Copy = new RadMenuItem();
            Copy.Header = ControlUIStrings.TextBox_Copy;
            Copy.Command = this.CopyCommand;
            Copy.CommandParameter = popupTextBox;
            contextMenu.Items.Add(Copy);

            RadMenuItem Paste = new RadMenuItem();
            Paste.Header = ControlUIStrings.TextBox_Paste;
            Paste.Command = this.PasteCommand;
            Paste.CommandParameter = popupTextBox;
            contextMenu.Items.Add(Paste);

            RadMenuItem Cut = new RadMenuItem();
            Cut.Header = ControlUIStrings.TextBox_Cut;
            Cut.Command = this.CutCommand;
            Cut.CommandParameter = popupTextBox;
            contextMenu.Items.Add(Cut);
            return contextMenu;
        }

        private Type GetValueType(QueryItem queryItem) {
            if(queryItem is KeyValuesQueryItem || queryItem is ComboQueryItem)
                return typeof(int);

            var dropDownQueryItem = queryItem as DropDownQueryItem;
            if(dropDownQueryItem != null)
                return dropDownQueryItem.GetQueryValue == null ? typeof(string) : typeof(object);

            if(queryItem is DateTimeQueryItem)
                return typeof(DateTime);

            if(queryItem is DateTimeRangeQueryItem)
                return typeof(DateTime?[]);

            if(queryItem is NumericRangeQueryItem)
                return typeof(Double?[]);


            var customQueryItem = queryItem as CustomQueryItem;
            if(customQueryItem != null)
                return customQueryItem.DataType;

            var popupQueryItem = queryItem as PopupQueryItem;
            if(popupQueryItem != null)
                return popupQueryItem.DataType ?? (this.entityType != null ? this.entityType.GetProperty(queryItem.ColumnName).PropertyType : null);

            return this.entityType != null ? this.entityType.GetProperty(queryItem.ColumnName).PropertyType : null;
        }

        private FilterOperator GetOperator(QueryItem queryItem) {
            Func<FilterOperator> getStringOperator = () => queryItem.IsExact ? FilterOperator.IsEqualTo : FilterOperator.Contains;

            if(queryItem is KeyValuesQueryItem || queryItem is ComboQueryItem)
                return FilterOperator.IsEqualTo;

            var dropDownQueryItem = queryItem as DropDownQueryItem;
            if(dropDownQueryItem != null)
                return (dropDownQueryItem).GetQueryValue == null ? getStringOperator() : FilterOperator.IsEqualTo;

            var popupQueryItem = queryItem as PopupQueryItem;
            if(popupQueryItem != null)
                return (popupQueryItem).GetQueryValue == null ? getStringOperator() : FilterOperator.IsEqualTo;


            var customQueryItem = queryItem as CustomQueryItem;
            if(customQueryItem != null) {
                var type = (customQueryItem).DataType;
                return typeof(string) == type ? getStringOperator() : FilterOperator.IsEqualTo;
            }

            if(this.entityType != null) {
                var type = this.entityType.GetProperty(queryItem.ColumnName).PropertyType;
                return typeof(string) == type ? getStringOperator() : FilterOperator.IsEqualTo;
            }
            return FilterOperator.IsEqualTo;
        }

        public FilterPanel() {
            this.FontSize = 13;
            this.Margin = new Thickness(0, 0, 5, 0);
            PasteCommand = new DelegateCommand(ExcutePasteCommand, CanExcutePasteCommand);
            CopyCommand = new DelegateCommand(ExcuteCopyCommand, CanExcuteCopyCommand);
            CutCommand = new DelegateCommand(ExcuteCutCommand, CanExcuteCutCommand);
        }

        public IDictionary<string, object> GetValues() {
            return this.currentControls == null ? null : this.currentControls.ToDictionary(kv => kv.Key.ColumnName, kv => GetValue(kv.Key, kv.Value) ?? kv.Key.NullValue);
        }

        public void SetValues(IDictionary<string, object> values) {
            if(values == null)
                throw new ArgumentNullException("values");
            if(this.currentControls == null)
                return;

            foreach(var kv in this.currentControls) {
                var queryItem = kv.Key;
                var control = kv.Value;

                if(!values.ContainsKey(queryItem.ColumnName))
                    continue;

                if(queryItem is CustomControlQueryItem) {
                    var filterPanelControl = this.currentControls[queryItem] as IFilterPanelControl;
                    if(filterPanelControl != null)
                        filterPanelControl.SetFilterValue(values[queryItem.ColumnName]);
                    return;
                }

                if(queryItem is KeyValuesQueryItem) {
                    if(this.kvQueryItemValues == null)
                        this.kvQueryItemValues = new Dictionary<KeyValuesQueryItem, object>();

                    var value = values[queryItem.ColumnName];
                    if(value == null || value is string && string.IsNullOrEmpty((string)value)) {
                        ((RadComboBox)control).SelectedValue = null;
                        if(this.kvQueryItemValues.ContainsKey((KeyValuesQueryItem)queryItem))
                            this.kvQueryItemValues.Remove((KeyValuesQueryItem)queryItem);
                    } else {
                        ((RadComboBox)control).SelectedValue = Convert.ToInt32(value);
                        this.kvQueryItemValues[(KeyValuesQueryItem)queryItem] = Convert.ToInt32(value);
                    }
                }

                if(queryItem is PopupQueryItem) {
                    var popupQueryItem = (PopupQueryItem)queryItem;
                    if(popupQueryItem.GetText == null)
                        ((PopupTextBox)control).Text = values[queryItem.ColumnName] == null ? string.Empty : values[queryItem.ColumnName].ToString();
                    else
                        ((PopupTextBox)control).Text = popupQueryItem.GetText(values[queryItem.ColumnName]);
                }
                if(queryItem is DropDownQueryItem) {
                    var dropDownQueryItem = (DropDownQueryItem)queryItem;
                    if(dropDownQueryItem.GetText == null)
                        ((DropDownTextBox)control).Text = values[queryItem.ColumnName] == null ? string.Empty : values[queryItem.ColumnName].ToString();
                    else
                        ((DropDownTextBox)control).Text = dropDownQueryItem.GetText(values[queryItem.ColumnName]);
                } else if(queryItem is DateTimeRangeQueryItem) {
                    var rangeBox = (DateTimeRangeBox)control;
                    var array = (DateTime?[])values[queryItem.ColumnName];
                    rangeBox.SelectedValueFrom = array[0];
                    rangeBox.SelectedValueTo = array[1];
                } else if(queryItem is NumericRangeQueryItem) {
                    var rangeBox = (NumericRangeBox)control;
                    var array = (Double?[])values[queryItem.ColumnName];
                    rangeBox.SelectedValueFrom = array[0];
                    rangeBox.SelectedValueTo = array[1];
                } else if(control is RadMaskedInputBase)
                    ((RadMaskedInputBase)control).Text = (string)values[queryItem.ColumnName];
                else if(control is RadWatermarkTextBox)
                    ((RadWatermarkTextBox)control).Text = (string)values[queryItem.ColumnName] ?? "";
                else if(control is RadDateTimePicker)
                    if(values[queryItem.ColumnName] == null)
                        ((RadDateTimePicker)control).SelectedValue = null;
                    else
                        ((RadDateTimePicker)control).SelectedValue = Convert.ToDateTime(values[queryItem.ColumnName]);
                else if(control is CheckBox)
                    ((CheckBox)control).IsChecked = values[queryItem.ColumnName] == null ? (bool?)null : Convert.ToBoolean(values[queryItem.ColumnName]);
            }
        }

        public void SetValues(IDictionary<string, string> values) {
            if(values == null)
                throw new ArgumentNullException("values");
            if(this.currentControls == null)
                return;

            var itemValues = new Dictionary<string, object>(values.Count);
            foreach(var queryItem in this.currentControls.Select(kv => kv.Key).Where(queryItem => queryItem != null && values.ContainsKey(queryItem.ColumnName)))
                if(queryItem is DateTimeRangeQueryItem) {
                    var value = values[queryItem.ColumnName].Split(',');
                    if(value.Count() == 2)
                        itemValues[queryItem.ColumnName] = value.Select<string, DateTime?>(str => {
                            DateTime date;
                            if(DateTime.TryParse(str, out date))
                                return date;
                            return null;
                        }).ToArray();
                } else if(queryItem is NumericRangeQueryItem) {
                    var value = values[queryItem.ColumnName].Split(',');
                    if(value.Count() == 2)
                        itemValues[queryItem.ColumnName] = value.Select<string, Double?>(str => {
                            Double date;
                            if(Double.TryParse(str, out date))
                                return date;
                            return null;
                        }).ToArray();
                } else
                    itemValues[queryItem.ColumnName] = values[queryItem.ColumnName];
            this.SetValues(itemValues);
        }

        public void SetValueByUniqueId(string uniqueId, object value) {
            if(this.currentControls == null)
                return;

            var key = this.currentControls.Keys.FirstOrDefault(queryItem => queryItem.UniqueId == uniqueId);
            if(key != null)


                this.SetValues(new Dictionary<string, object> {
                    {
                        key.ColumnName, value
                    }
                });
        }

        public void SetEnabledByUniqueId(string uniqueId, bool value) {
            if(this.currentControls == null)
                return;

            var key = this.currentControls.Keys.FirstOrDefault(queryItem => queryItem.UniqueId == uniqueId);
            if(key != null)
                this.currentControls[key].SetValue(IsEnabledProperty, value);
        }

        public void CreateFilterControls(Type type, IEnumerable<QueryItem> queryItems) {
            if(type == null)
                throw new ArgumentNullException("type");
            if(queryItems == null)
                throw new ArgumentNullException("queryItems");

            this.entityType = type;

            this.hiddenQueryItems = new List<HiddenQueryItem>();
            var currentQueryItems = queryItems.ToArray();

            if(this.currentControls == null)
                this.currentControls = new Dictionary<QueryItem, FrameworkElement>();
            else {
                foreach(var queryItem in this.currentControls.Keys)
                    queryItem.FilterPanel = null;
                this.currentControls.Clear();
            }

            if(this.root == null)
                this.Content = this.root = new Grid();
            else {
                this.root.Children.Clear();
                this.root.ColumnDefinitions.Clear();
                this.root.RowDefinitions.Clear();
            }

            for(var i = 0; i < this.RowSize; i++)
                this.root.RowDefinitions.Add(new RowDefinition {
                    Height = GridLength.Auto
                });

            var index = 0;
            foreach(var queryItem in currentQueryItems) {
                if(queryItem is HiddenQueryItem) {
                    this.hiddenQueryItems.Add((HiddenQueryItem)queryItem);
                    continue;
                }

                var control = this.GetFilterControl(type, queryItem);
                if(control == null)
                    continue;

                queryItem.FilterPanel = this;

                var rowIndex = index % this.RowSize;
                var columnIndex = index / this.RowSize * 2;

                if(rowIndex == 0)
                    for(var i = 0; i < 2; i++)
                        this.root.ColumnDefinitions.Add(new ColumnDefinition {
                            Width = GridLength.Auto
                        });

                var textBlock = new TextBlock {
                    Margin = new Thickness(8, 5, 4, 5),
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Right,
                    Text = string.IsNullOrEmpty(queryItem.Title) ? Utils.GetEntityLocalizedName(type, queryItem.ColumnName) : queryItem.Title,
                };
                textBlock.SetValue(Grid.RowProperty, rowIndex);
                textBlock.SetValue(Grid.ColumnProperty, columnIndex);
                this.root.Children.Add(textBlock);

                if(control is CheckBox)
                    control.Margin = new Thickness(2, 6, 0, 5);
                else {
                    control.Height = 23;
                    if(control is DateTimeRangeBox || control is NumericRangeBox)
                        control.Width = 205;
                    else
                        control.Width = 120;
                    control.Margin = new Thickness(2, 2, 0, 2);
                }
                control.IsEnabled = queryItem.IsEnabled;
                control.HorizontalAlignment = HorizontalAlignment.Left;
                control.VerticalAlignment = VerticalAlignment.Center;
                control.SetValue(Grid.RowProperty, rowIndex);
                control.SetValue(Grid.ColumnProperty, columnIndex + 1);
                this.root.Children.Add(control);
                this.currentControls.Add(queryItem, control);

                index++;
            }

            // ReSharper disable AccessToModifiedClosure, PossibleMultipleEnumeration
            for(var i = 1; i < this.root.ColumnDefinitions.Count; i += 2) {
                var controls = this.currentControls.Values.Where(c => (int)c.GetValue(Grid.ColumnProperty) == i);
                if(!controls.Any())
                    continue;
                var maxWidth = controls.Max(c => c.Width);
                foreach(var control in controls)
                    control.Width = maxWidth;
            }
            // ReSharper restore AccessToModifiedClosure, PossibleMultipleEnumeration
        }

        public IDictionary<string, object> GetQueryParameters() {
            var result = this.GetValues();
            var isFilterNotEmpty = result.Values.Any(value => value != null && Convert.ToString(value) != string.Empty);

            foreach(var hiddenQueryItem in this.hiddenQueryItems) {
                var value = hiddenQueryItem.GetQueryValue == null ? hiddenQueryItem.NullValue : hiddenQueryItem.GetQueryValue();
                result.Add(hiddenQueryItem.ColumnName, value);
                if(!isFilterNotEmpty && value != null && Convert.ToString(value) != string.Empty)
                    isFilterNotEmpty = true;
            }

            return isFilterNotEmpty ? result : null;
        }

        public FilterItem GetFilters() {
            if(this.currentControls == null)
                return null;

            var values = this.currentControls.ToDictionary(kv => kv.Key, kv => GetValue(kv.Key, kv.Value) ?? CheckStringValue(kv.Key.NullValue)).Union(from hiddenQueryItem in this.hiddenQueryItems
                                                                                                                                                       let value = hiddenQueryItem.GetQueryValue == null ? CheckStringValue(hiddenQueryItem.NullValue) : CheckStringValue(hiddenQueryItem.GetQueryValue())
                                                                                                                                                       select new KeyValuePair<QueryItem, object>(hiddenQueryItem, value)).ToArray();

            if(values.Length == 0)
                return null;

            Func<QueryItem, Type, FilterOperator, object, FilterItem> createFilterItemInstance = (queryItem, memberType, filterOp, value) => new FilterItem {
                MemberName = queryItem.ColumnName,
                ParameterName = queryItem.ParameterName,
                MemberType = memberType,
                Operator = filterOp,
                IsCaseSensitive = queryItem.IsCaseSensitive,
                IsExact = queryItem.IsExact,
                NullValue = queryItem.NullValue,
                Value = value,
            };

            Func<QueryItem, object, FilterItem> createFilterItem = (queryItem, value) => {
                var dateTimeRangeQueryItem = queryItem as DateTimeRangeQueryItem;
                if(dateTimeRangeQueryItem != null) {
                    var timeRange = value as DateTime[];
                    if(timeRange == null) {
                        var timeRangeNullable = value as DateTime?[];
                        if(timeRangeNullable != null && timeRangeNullable.Length == 2) {
                            // ReSharper disable PossibleInvalidOperationException
                            timeRange = new DateTime[2];
                            timeRange[0] = timeRangeNullable[0] == null ? DateTime.MinValue : timeRangeNullable[0].Value;
                            timeRange[1] = timeRangeNullable[1] == null ? DateTime.MaxValue : (dateTimeRangeQueryItem.InputMode == InputMode.DatePicker ? timeRangeNullable[1].Value.AddDays(1).AddMilliseconds(-1) : timeRangeNullable[1].Value);
                            // ReSharper restore PossibleInvalidOperationException
                        }
                    }
                    if(timeRange == null || timeRange.Length != 2 || (timeRange[0] == DateTime.MinValue && timeRange[1] == DateTime.MaxValue))
                        return null;

                    var filterItem = new CompositeFilterItem {
                        LogicalOperator = LogicalOperator.And,
                    };
                    for(var i = 0; i < timeRange.Length; i++) {
                        FilterOperator filterOp;
                        if(i == 0)
                            filterOp = dateTimeRangeQueryItem.IsGreaterThanOrEqualToMinValue ? FilterOperator.IsGreaterThanOrEqualTo : FilterOperator.IsGreaterThan;
                        else
                            filterOp = dateTimeRangeQueryItem.IsLessThanOrEqualToMaxValue ? FilterOperator.IsLessThanOrEqualTo : FilterOperator.IsLessThan;
                        filterItem.Filters.Add(createFilterItemInstance(dateTimeRangeQueryItem, typeof(DateTime), filterOp, timeRange[i]));
                    }
                    return filterItem;
                }

                var numericRangeQueryItem = queryItem as NumericRangeQueryItem;
                if(numericRangeQueryItem != null) {
                    const double TOLERANCE = 0.001;
                    var numRange = value as Double[];
                    if(numRange == null) {
                        var timeRangeNullable = value as Double?[];
                        if(timeRangeNullable != null && timeRangeNullable.Length == 2) {
                            // ReSharper disable PossibleInvalidOperationException
                            numRange = new Double[2];
                            numRange[0] = timeRangeNullable[0] == null ? Double.MinValue : timeRangeNullable[0].Value;
                            numRange[1] = timeRangeNullable[1] == null ? Double.MaxValue : timeRangeNullable[1].Value;
                            // ReSharper restore PossibleInvalidOperationException
                        }
                    }
                    if(numRange == null || numRange.Length != 2 || (Math.Abs(numRange[0] - Double.MinValue) < TOLERANCE && Math.Abs(numRange[1] - Double.MaxValue) < TOLERANCE))
                        return null;

                    var filterItem = new CompositeFilterItem {
                        LogicalOperator = LogicalOperator.And,
                    };
                    for(var i = 0; i < numRange.Length; i++) {
                        FilterOperator filterOp;
                        if(i == 0)
                            filterOp = numericRangeQueryItem.IsGreaterThanOrEqualToMinValue ? FilterOperator.IsGreaterThanOrEqualTo : FilterOperator.IsGreaterThan;
                        else
                            filterOp = numericRangeQueryItem.IsLessThanOrEqualToMaxValue ? FilterOperator.IsLessThanOrEqualTo : FilterOperator.IsLessThan;
                        if(Math.Abs(numRange[i] - Double.MaxValue) > TOLERANCE && Math.Abs(numRange[i] - Double.MinValue) > TOLERANCE)
                            filterItem.Filters.Add(createFilterItemInstance(numericRangeQueryItem, typeof(Double), filterOp, numRange[i]));
                    }
                    return filterItem;
                }
                var comboQueryItem = queryItem as ComboQueryItem;
                if(comboQueryItem != null) {
                    if(comboQueryItem.AllowMultiSelect) {
                        int selectedValue = 0;
                        var selecttedValues = value as List<object>;
                        if(selecttedValues == null)
                            return null;
                        var filterItem = new CompositeFilterItem {
                            LogicalOperator = LogicalOperator.Or,
                            Operator = FilterOperator.Contains,
                        };
                        foreach(var item in selecttedValues) {
                            FilterOperator filterOp;
                            filterOp = FilterOperator.IsEqualTo;
                            int.TryParse(item + "", out selectedValue);
                            filterItem.Filters.Add(createFilterItemInstance(comboQueryItem, typeof(int), filterOp, selectedValue));
                        }
                        return filterItem;
                    }
                }
                var keyValuesColumnItem = queryItem as KeyValuesQueryItem;
                if(keyValuesColumnItem != null) {
                    if(keyValuesColumnItem.AllowMultiSelect) {
                        int selectedValue = 0;
                        var selecttedValues = value as List<object>;
                        if(selecttedValues == null)
                            return null;
                        var filterItem = new CompositeFilterItem {
                            LogicalOperator = LogicalOperator.Or,
                            Operator = FilterOperator.Contains,
                        };
                        foreach(var item in selecttedValues) {
                            FilterOperator filterOp;
                            filterOp = FilterOperator.IsEqualTo;
                            int.TryParse(item + "", out selectedValue);
                            filterItem.Filters.Add(createFilterItemInstance(keyValuesColumnItem, typeof(int), filterOp, selectedValue));
                        }
                        return filterItem;
                    }
                }

                var customControlQueryItem = queryItem as CustomControlQueryItem;
                if(customControlQueryItem != null) {
                    // ReSharper disable once SuspiciousTypeConversion.Global
                    var control = this.currentControls[customControlQueryItem] as IFilterPanelControl;
                    if(control != null)
                        return control.GetFilter();
                }

                return createFilterItemInstance(queryItem, this.GetValueType(queryItem), this.GetOperator(queryItem), value);
            };

            if(values.Length == 1)
                return createFilterItem(values[0].Key, values[0].Value);

            var filterItems = values.Select(value => createFilterItem(value.Key, value.Value)).Where(filterItem => filterItem != null).ToArray();
            if(filterItems.Length == 1)
                return filterItems[0];

            var result = new CompositeFilterItem {
                LogicalOperator = LogicalOperator.And,
            };
            foreach(var filterItem in filterItems)
                result.Filters.Add(filterItem);
            return result;
        }
        #region 粘贴

        public DelegateCommand PasteCommand {
            get;
            private set;
        }

        private static bool CanExcutePasteCommand(object param) {
            return CanExcuteComman(param);
        }

        private static bool CanExcuteComman(object param) {
            if(param is RadWatermarkTextBox)
                return !((RadWatermarkTextBox)param).IsReadOnly;
            if(param is PopupTextBox)
                return !((PopupTextBox)param).IsReadOnly;
            return false;
        }

        private static void ExcutePasteCommand(object param) {
            if(!CanExcutePasteCommand(param))
                return;
            string pasteText = string.Empty;
            pasteText = System.Windows.Clipboard.GetText();
            if(param is RadWatermarkTextBox)
                ((RadWatermarkTextBox)param).Text = pasteText;
            if(param is PopupTextBox)
                ((PopupTextBox)param).Text = pasteText;
        }
        #endregion

        #region 复制

        public DelegateCommand CopyCommand {
            get;
            private set;
        }

        private static bool CanExcuteCopyCommand(object param) {
            return param != null;
        }

        private static void ExcuteCopyCommand(object param) {
            if(!CanExcutePasteCommand(param))
                return;
            var copyText = string.Empty;
            if(param is RadWatermarkTextBox)
                copyText = ((RadWatermarkTextBox)param).SelectedText;
            if(param is PopupTextBox)
                copyText = ((PopupTextBox)param).SelectedText;
            Clipboard.SetText(copyText);
        }
        #endregion

        #region 剪切

        public DelegateCommand CutCommand {
            get;
            private set;
        }

        private static bool CanExcuteCutCommand(object param) {
            return CanExcuteComman(param);
        }

        private static void ExcuteCutCommand(object param) {
            if(!CanExcutePasteCommand(param))
                return;
            string cutText = string.Empty;
            if(param is RadWatermarkTextBox) {
                var radWatermarkTextBox = ((RadWatermarkTextBox)param);
                cutText = ((RadWatermarkTextBox)param).SelectedText;
                Clipboard.SetText(cutText);
                radWatermarkTextBox.Text = radWatermarkTextBox.Text.Replace(cutText, "");
            }
            if(param is PopupTextBox) {
                var popupTextBox = ((PopupTextBox)param);
                cutText = ((PopupTextBox)param).SelectedText;
                Clipboard.SetText(cutText);
                popupTextBox.Text = popupTextBox.Text.Replace(cutText, "");
            }
        }
    }
        #endregion

}
