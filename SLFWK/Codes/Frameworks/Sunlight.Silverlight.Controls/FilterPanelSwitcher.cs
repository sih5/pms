﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.TransitionEffects;

namespace Sunlight.Silverlight.Controls {
    public class FilterPanelSwitcher : UserControl {
        private readonly RadTransitionControl transitionControl;
        private readonly RadSlider slider;
        private List<string> keys;
        private Dictionary<string, FilterPanel> panels;
        public event EventHandler FilterPanelSwitched;

        public string CurrentKey {
            get {
                if(this.keys == null)
                    return null;
                var index = (int)this.slider.Value;
                return index >= this.keys.Count ? null : this.keys[index];
            }
        }

        public FilterPanel CurrentFilterPanel {
            get {
                if(this.panels == null)
                    return null;
                return this.panels.ContainsKey(this.CurrentKey) ? this.panels[this.CurrentKey] : null;
            }
        }

        private void RaiseFilterPanelSwitched() {
            var handler = this.FilterPanelSwitched;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        private void SetContent(int index, bool isIncreasing) {
            if(this.keys == null || this.panels == null || index >= this.keys.Count)
                return;

            var key = this.keys[index];
            if(!this.panels.ContainsKey(key))
                return;

            if(this.transitionControl.Transition is PerspectiveRotationTransition)
                ((PerspectiveRotationTransition)this.transitionControl.Transition).Direction = isIncreasing ? RotationDirection.Top : RotationDirection.Bottom;
            else if(this.transitionControl.Transition is RollTransition)
                ((RollTransition)this.transitionControl.Transition).IsRollOut = isIncreasing;

            this.transitionControl.Content = this.panels[key];
            this.RaiseFilterPanelSwitched();
        }

        private void slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
            this.SetContent((int)e.NewValue, e.NewValue > e.OldValue);
        }

        public FilterPanelSwitcher() {
            var stackPanel = new StackPanel {
                Orientation = Orientation.Horizontal
            };

            this.slider = new RadSlider {
                IsSelectionRangeEnabled = false,
                Orientation = Orientation.Vertical,
                IsDirectionReversed = true,
                HandlesVisibility = Visibility.Visible,
                Visibility = Visibility.Collapsed,
                TickFrequency = 1,
                TickPlacement = TickPlacement.None,
                IsSnapToTickEnabled = true,
                SmallChange = 1,
                LargeChange = 1,
                Minimum = 0,
                Maximum = 0,
                Value = 0,
            };
            this.slider.ValueChanged += this.slider_ValueChanged;
            stackPanel.Children.Add(this.slider);

            this.transitionControl = new RadTransitionControl {
                HorizontalContentAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Center,
                Duration = TimeSpan.FromMilliseconds(800),
                Transition = new PerspectiveRotationTransition {
                    Direction = RotationDirection.Top,
                    NewPlaneCenterOfRotationZ = 0.4,
                    OldPlaneCenterOfRotationZ = 0.4,
                    RotationLength = 90,
                    NewPlaneEasing = new CubicEase {
                        EasingMode = EasingMode.EaseInOut
                    },
                    OldPlaneEasing = new CubicEase {
                        EasingMode = EasingMode.EaseInOut
                    },
                }
            };
            stackPanel.Children.Add(this.transitionControl);

            this.Margin = new Thickness(0, 1, 0, 1);
            this.Content = stackPanel;
        }

        public void Register(string key, FilterPanel panel) {
            if(this.panels == null)
                this.panels = new Dictionary<string, FilterPanel>();

            if(this.panels.ContainsKey(key)) {
                this.panels[key] = panel;
                return;
            }

            this.panels.Add(key, panel);
            if(this.keys == null)
                this.keys = new List<string>();
            this.keys.Add(key);

            this.slider.Maximum = this.panels.Count - 1;
            this.slider.Visibility = this.panels.Count > 1 ? Visibility.Visible : Visibility.Collapsed;

            if(this.transitionControl.Content == null && this.panels.Count > 0)
                this.SetContent(0, true);
        }
    }
}
