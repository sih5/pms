﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using Sunlight.Silverlight.Core.Model;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Controls {
    public class GridView : UserControl {
        private const double DOUBLE_CLICK_POSITION_DEVIATION = 4.0;

        // ReSharper disable StaticFieldInitializersReferesToFieldBelow
        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(GridView), null);
        public static readonly DependencyProperty IsBusyProperty = DependencyProperty.Register("IsBusy", typeof(bool), typeof(GridView), null);
        public static readonly DependencyProperty IsReadOnlyProperty = DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(GridView), new PropertyMetadata(true));
        public static readonly DependencyProperty IsGroupPanelVisibleProperty = DependencyProperty.Register("IsGroupPanelVisible", typeof(bool), typeof(GridView), new PropertyMetadata(true));
        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register("SelectedItem", typeof(object), typeof(GridView), null);
        public static readonly DependencyProperty PageSizeProperty = DependencyProperty.Register("PageSize", typeof(int), typeof(GridView), new PropertyMetadata(25, (obj, args) => ((GridView)obj).mainDataPager.PageSize = (int)args.NewValue));
        public static readonly DependencyProperty HideSelectColumnProperty = DependencyProperty.Register("HideSelectColumn", typeof(bool), typeof(GridView), null);
        // ReSharper restore StaticFieldInitializersReferesToFieldBelow
        private static readonly TimeSpan DoubleClickThreshold = TimeSpan.FromMilliseconds(450);

        private readonly RadGridView mainGridView;
        private readonly RadDataPager mainDataPager;
        private bool hasDetailPanels, appliedGroups;
        private DateTime lastClickTime;
        private Point lastClickPosition;
        private IEnumerable<ColumnItem> columnItems;
        public event EventHandler<GridViewRequestingDetailPanelContentEventArgs> RequestingDetailPanelContent;
        public event EventHandler<SelectionChangingEventArgs> SelectionChanging;
        public event EventHandler<SelectionChangeEventArgs> SelectionChanged;
        public event EventHandler<GridViewRowDoubleClickEventArgs> RowDoubleClick;
        public event EventHandler<GridViewRowKeyDownEventArgs> RowKeyDown;

        public IEnumerable ItemsSource {
            get {
                return (IEnumerable)this.GetValue(ItemsSourceProperty);
            }
            set {
                this.SetValue(ItemsSourceProperty, value);
            }
        }

        public bool IsBusy {
            get {
                return (bool)this.GetValue(IsBusyProperty);
            }
            set {
                this.SetValue(IsBusyProperty, value);
            }
        }

        public bool IsReadOnly {
            get {
                return (bool)this.GetValue(IsReadOnlyProperty);
            }
            set {
                this.SetValue(IsReadOnlyProperty, value);
            }
        }

        public bool IsGroupPanelVisible {
            get {
                return (bool)this.GetValue(IsGroupPanelVisibleProperty);
            }
            set {
                this.SetValue(IsGroupPanelVisibleProperty, value);
            }
        }

        public object SelectedItem {
            get {
                return this.GetValue(SelectedItemProperty);
            }
            set {
                this.SetValue(SelectedItemProperty, value);
            }
        }

        public int PageSize {
            get {
                return (int)this.GetValue(PageSizeProperty);
            }
            set {
                this.SetValue(PageSizeProperty, value);
            }
        }

        public bool HideSelectColumn {
            get {
                return (bool)this.GetValue(HideSelectColumnProperty);
            }
            set {
                this.SetValue(HideSelectColumnProperty, value);
            }
        }

        public ObservableCollection<object> SelectedItems {
            get {
                return this.mainGridView == null ? null : this.mainGridView.SelectedItems;
            }
        }

        private void RaiseRequestingDetailPanelContent(GridViewRequestingDetailPanelContentEventArgs args) {
            var handler = this.RequestingDetailPanelContent;
            if(handler != null)
                handler(this, args);
        }

        private void RaiseSelectionChanging(SelectionChangingEventArgs args) {
            var handler = this.SelectionChanging;
            if(handler != null)
                handler(this, args);
        }

        private void RaiseSelectionChanged(SelectionChangeEventArgs args) {
            var handler = this.SelectionChanged;
            if(handler != null)
                handler(this, args);
        }

        private void RaiseRowDoubleClick(GridViewRowDoubleClickEventArgs args) {
            var handler = this.RowDoubleClick;
            if(handler != null)
                handler(this, args);
        }

        private void RaiseRowKeyDown(GridViewRowKeyDownEventArgs args) {
            var handler = this.RowKeyDown;
            if(handler != null)
                handler(this, args);
        }

        private void EnsureDetailToggleColumn() {
            var alreadyHasToggleColumn = this.mainGridView.Columns.OfType<GridViewToggleRowDetailsColumn>().Any();
            if(this.hasDetailPanels && !alreadyHasToggleColumn)
                this.mainGridView.Columns.Insert(0, new GridViewToggleRowDetailsColumn());
            else if(!this.hasDetailPanels && alreadyHasToggleColumn)
                this.mainGridView.Columns.Remove(this.mainGridView.Columns.OfType<GridViewToggleRowDetailsColumn>().Single());
        }

        private void GridRow_KeyDown(object sender, KeyEventArgs e) {
            var row = sender as GridViewRow;
            if(row == null)
                return;
            var args = new GridViewRowKeyDownEventArgs {
                Row = row,
                Key = e.Key,
                Handled = e.Handled,
            };
            this.RaiseRowKeyDown(args);
            e.Handled = args.Handled;
        }

        private void mainGridView_LoadingRowDetails(object sender, GridViewRowDetailsEventArgs e) {
            var tabControl = e.DetailsElement as RadTabControl;
            if(tabControl == null)
                return;

            foreach(var tabItem in tabControl.Items.OfType<RadTabItem>()) {
                var args = new GridViewRequestingDetailPanelContentEventArgs {
                    UniqueId = tabItem.Header as string,
                    DataContext = e.Row.DataContext
                };
                this.RaiseRequestingDetailPanelContent(args);
                tabItem.Header = args.Header;
                tabItem.Content = new Border {
                    Margin = new Thickness(2),
                    Child = args.Content,
                };
            }
        }

        private void mainGridView_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(this.appliedGroups || e.PropertyName != "CurrentItem")
                return;
            this.appliedGroups = true;
            if(this.mainGridView.GroupDescriptors.Count != 0)
                return;
            foreach(var columnItem in this.columnItems.Where(c => c.IsDefaultGroup && c.SubColumns == null).OrderBy(c => c.DefaultGroupPosition))
                this.mainGridView.GroupDescriptors.Add(new ColumnGroupDescriptor {
                    Column = this.mainGridView.Columns.OfType<GridViewBoundColumnBase>().Single(c => c.DataMemberBinding.Path.Path == columnItem.Name),
                    SortDirection = columnItem.IsGroupSortDescending == null ? (ListSortDirection?)null : (columnItem.IsGroupSortDescending.Value ? ListSortDirection.Descending : ListSortDirection.Ascending),
                });
        }

        private void mainGridView_SelectionChanging(object sender, SelectionChangingEventArgs e) {
            this.RaiseSelectionChanging(e);
        }

        private void mainGridView_SelectionChanged(object sender, SelectionChangeEventArgs e) {
            this.RaiseSelectionChanged(e);
        }

        private void mainGridView_RowLoaded(object sender, RowLoadedEventArgs e) {
            e.Row.KeyDown += this.GridRow_KeyDown;
        }

        private void mainGridView_RowUnloaded(object sender, RowUnloadedEventArgs e) {
            e.Row.KeyDown -= this.GridRow_KeyDown;
        }

        private void GridViewRow_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
            var element = sender as UIElement;
            if(element == null)
                return;
            var position = e.GetPosition(element);
            if(DateTime.Now - this.lastClickTime <= DoubleClickThreshold && Math.Abs(this.lastClickPosition.X - position.X) < DOUBLE_CLICK_POSITION_DEVIATION &&
               Math.Abs(this.lastClickPosition.Y - position.Y) < DOUBLE_CLICK_POSITION_DEVIATION) {
                this.lastClickTime = DateTime.MinValue;
                this.lastClickPosition = new Point();
                var elements = VisualTreeHelper.FindElementsInHostCoordinates(e.GetPosition(null), element).ToArray();
                if(elements.OfType<DetailsPresenter>().Any())
                    return;
                var row = elements.OfType<GridViewRow>().FirstOrDefault();
                if(row != null) {
                    var args = new GridViewRowDoubleClickEventArgs {
                        Row = row,
                        Handled = e.Handled,
                    };
                    this.RaiseRowDoubleClick(args);
                    e.Handled = args.Handled;
                }
            } else {
                this.lastClickTime = DateTime.Now;
                this.lastClickPosition = position;
            }
        }

        public GridView() {
            var grid = new Grid();
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });

            this.mainDataPager = new RadDataPager {
                FontSize = 10,
                AutoEllipsisMode = AutoEllipsisModes.Both,
                DisplayMode = PagerDisplayModes.All,
                NumericButtonCount = 10,
                IsTotalItemCountFixed = true,
                PageSize = this.PageSize,
            };
            this.mainDataPager.SetBinding(RadDataPager.SourceProperty, new Binding("ItemsSource") {
                Source = this,
                Mode = BindingMode.OneWay,
                TargetNullValue = Enumerable.Empty<object>(),
            });
            this.mainDataPager.SetValue(Grid.RowProperty, 1);
            grid.Children.Add(this.mainDataPager);

            this.mainGridView = new RadGridView {
                FontSize = 12,
                ClipboardCopyMode = GridViewClipboardCopyMode.Cells,
                ClipboardPasteMode = GridViewClipboardPasteMode.Cells | GridViewClipboardPasteMode.OverwriteWithEmptyValues | GridViewClipboardPasteMode.SkipHiddenColumns,
                SelectionMode = System.Windows.Controls.SelectionMode.Extended,
                SelectionUnit = GridViewSelectionUnit.Cell,
                AutoGenerateColumns = false,
                AutoExpandGroups = true,
                CanUserFreezeColumns = false,
                CanUserReorderColumns = true,
                CanUserResizeColumns = true,
                CanUserSortColumns = true,
                ActionOnLostFocus = ActionOnLostFocus.None,
                RowDetailsVisibilityMode = GridViewRowDetailsVisibilityMode.Collapsed,
            };
            this.mainGridView.SetBinding(DataControl.ItemsSourceProperty, new Binding("PagedSource") {
                Source = this.mainDataPager,
                TargetNullValue = Enumerable.Empty<object>(),
            });
            this.mainGridView.SetBinding(GridViewDataControl.IsBusyProperty, new Binding("IsBusy") {
                Source = this,
                Mode = BindingMode.TwoWay,
            });
            this.mainGridView.SetBinding(GridViewDataControl.IsReadOnlyProperty, new Binding("IsReadOnly") {
                Source = this,
                Mode = BindingMode.TwoWay,
            });
            this.mainGridView.SetBinding(GridViewDataControl.ShowGroupPanelProperty, new Binding("IsGroupPanelVisible") {
                Source = this,
                Mode = BindingMode.TwoWay,
            });
            this.mainGridView.SetBinding(DataControl.SelectedItemProperty, new Binding("SelectedItem") {
                Source = this,
                Mode = BindingMode.TwoWay,
            });
            this.mainGridView.LoadingRowDetails += this.mainGridView_LoadingRowDetails;
            this.mainGridView.SelectionChanging += this.mainGridView_SelectionChanging;
            this.mainGridView.SelectionChanged += this.mainGridView_SelectionChanged;
            this.mainGridView.RowLoaded += this.mainGridView_RowLoaded;
            this.mainGridView.RowUnloaded += this.mainGridView_RowUnloaded;
            this.mainGridView.PropertyChanged += this.mainGridView_PropertyChanged;
            grid.Children.Add(this.mainGridView);

            this.Content = grid;
            // This event handler MUST be registered in this way, otherwise it won't work.
            this.AddHandler(MouseLeftButtonUpEvent, new MouseButtonEventHandler(this.GridViewRow_MouseLeftButtonUp), true);
        }

        public void SetColumns(Type entityType, IEnumerable<ColumnItem> columnItems) {
            if(entityType == null)
                throw new ArgumentNullException("entityType");
            if(columnItems == null)
                throw new ArgumentNullException("columnItems");

            this.mainGridView.SetColumns(entityType, this.columnItems = columnItems, this.HideSelectColumn, true);
            this.EnsureDetailToggleColumn();
        }

        public void SetDetailPanels(IEnumerable<PanelItem> panelItems) {
            if(panelItems == null)
                throw new ArgumentNullException("panelItems");

            this.hasDetailPanels = true;
            this.mainGridView.RowDetailsTemplate = GridViewHelper.CreateDetailPanelsTemplate(panelItems);
            this.EnsureDetailToggleColumn();
        }

        public void SetAsCommandTarget(UIElement element, DependencyProperty commandTargetProperty) {
            if(element == null)
                throw new ArgumentNullException("element");
            if(commandTargetProperty == null)
                throw new ArgumentNullException("commandTargetProperty");
            element.SetValue(commandTargetProperty, this.mainGridView);
        }

        public void Export(Stream stream, GridViewExportOptions options) {
            this.mainGridView.Export(stream, options);
        }
    }
}
