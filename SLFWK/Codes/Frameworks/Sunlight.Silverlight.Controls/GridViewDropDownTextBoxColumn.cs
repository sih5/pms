﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using Sunlight.Silverlight.Controls;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dms.Controls {
    public class GridViewDropDownTextBoxColumn : GridViewBoundColumnBase {
        public static readonly DependencyProperty DropDownContentProperty = DependencyProperty.Register("DropDownContent", typeof(UIElement), typeof(GridViewDropDownTextBoxColumn), new PropertyMetadata(default(UIElement)));
        public static readonly DependencyProperty IsEditableProperty = DependencyProperty.Register("IsEditable", typeof(bool), typeof(GridViewDropDownTextBoxColumn), new PropertyMetadata(true));

        private DropDownTextBox dropDownTextBox;

        public UIElement DropDownContent {
            get {
                return (UIElement)this.GetValue(DropDownContentProperty);
            }
            set {
                this.SetValue(DropDownContentProperty, value);
            }
        }

        public bool IsEditable {
            get {
                return (bool)GetValue(IsEditableProperty);
            }
            set {
                SetValue(IsEditableProperty, value);
            }
        }

        public override FrameworkElement CreateCellEditElement(GridViewCell cell, object dataItem) {
            this.BindingTarget = DropDownTextBox.TextProperty;

            if(this.dropDownTextBox == null) {
                this.dropDownTextBox = new DropDownTextBox {
                    UseBlurEffect = true,
                    DropDownContent = new Border {
                        BorderBrush = new SolidColorBrush(Color.FromArgb(0x99, 0x0, 0x0, 0x0)),
                        BorderThickness = new Thickness(1),
                        Child = this.DropDownContent,
                    },
                    IsReadOnly = !this.IsEditable
                };
                this.dropDownTextBox.SetBinding(this.BindingTarget, new Binding {
                    Mode = BindingMode.TwoWay,
                    NotifyOnValidationError = true,
                    ValidatesOnExceptions = true,
                    ValidatesOnDataErrors = true,
                    ValidatesOnNotifyDataErrors = true,
                    UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                    Path = new PropertyPath(this.DataMemberBinding.Path.Path)
                });
            }
            return this.dropDownTextBox;
        }

        public override IList<string> UpdateSourceWithEditorValue(GridViewCell gridViewCell) {
            var errors = new List<ValidationError>();
            var editor = (DropDownTextBox)gridViewCell.GetEditingElement();
            var bindingExpression = editor.ReadLocalValue(DropDownTextBox.TextProperty) as BindingExpression;
            if(bindingExpression != null) {
                bindingExpression.UpdateSource();
                errors.AddRange(Validation.GetErrors(editor));
            }
            return errors.Select(e => e.ErrorContent as string).ToList();
        }

        public override object GetNewValueFromEditor(object editor) {
            var dropDownTextBox = editor as DropDownTextBox;
            return dropDownTextBox != null ? dropDownTextBox.Text : null;
        }
    }
}
