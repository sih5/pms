﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Input;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Controls {
    public sealed class GridViewRequestingDetailPanelContentEventArgs : EventArgs {
        public string UniqueId {
            get;
            internal set;
        }

        public object DataContext {
            get;
            internal set;
        }

        public object Header {
            get;
            set;
        }

        public UIElement Content {
            get;
            set;
        }
    }

    public sealed class GridViewRowDoubleClickEventArgs : EventArgs {
        public GridViewRow Row {
            get;
            internal set;
        }

        public bool Handled {
            get;
            set;
        }
    }

    public sealed class GridViewRowKeyDownEventArgs : EventArgs {
        public GridViewRow Row {
            get;
            internal set;
        }

        public Key Key {
            get;
            internal set;
        }

        public bool Handled {
            get;
            set;
        }
    }
}
