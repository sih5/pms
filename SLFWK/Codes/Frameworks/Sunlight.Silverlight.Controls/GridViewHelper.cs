﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using Sunlight.Silverlight.Controls.Resources;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dms.Controls;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Telerik.Windows.Controls.MaskType;

namespace Sunlight.Silverlight.Controls {
    public static class GridViewHelper {
        public static void SetColumns(this GridViewDataControl dataGrid, Type entityType, IEnumerable<ColumnItem> columnItems, bool hideSelectColumn = false, bool useDomainDataSource = false) {
            if(dataGrid == null)
                throw new ArgumentNullException("dataGrid");
            if(entityType == null)
                throw new ArgumentNullException("entityType");
            if(columnItems == null)
                throw new ArgumentNullException("columnItems");

            // ReSharper disable PossibleMultipleEnumeration
            dataGrid.ShowColumnFooters = columnItems.OfType<AggregateColumnItem>().Any();
            dataGrid.Columns.Clear();
            if(!hideSelectColumn && dataGrid.SelectionMode != System.Windows.Controls.SelectionMode.Single)
                dataGrid.Columns.Add(new GridViewSelectColumn());
            foreach(var columnItem in columnItems.Where(c => c.SubColumns == null)) {
                PropertyInfo propertyInfo = null;
                var type = entityType;
                if(columnItem.Name.Contains('.')) {
                    var properties = new Queue<string>(columnItem.Name.Split('.'));
                    while(properties.Count > 0) {
                        propertyInfo = type.GetProperty(properties.Dequeue());
                        if(propertyInfo == null)
                            break;
                        type = propertyInfo.PropertyType;
                    }
                } else
                    propertyInfo = entityType.GetProperty(columnItem.Name);
                if(propertyInfo == null)
                    throw new Exception(string.Format(ControlUIStrings.GridView_PropertyNotExist, entityType.FullName, columnItem.Name));

                GridViewBoundColumnBase column;
                if(columnItem is KeyValuesColumnItem)
                    column = new GridViewComboBoxColumn {
                        IsComboBoxEditable = false,
                        SelectedValueMemberPath = "Key",
                        DisplayMemberPath = "Value",
                        ItemsSource = ((KeyValuesColumnItem)columnItem).KeyValueItems,
                        FilteringControl = new KeyValueItemsFilterControl(),
                    };
                else if(columnItem is DropDownTextBoxColumnItem)
                    column = new GridViewDropDownTextBoxColumn {
                        DropDownContent = ((DropDownTextBoxColumnItem)columnItem).DropDownContent,
                        IsEditable = ((DropDownTextBoxColumnItem)columnItem).IsEditable,
                    };
                else if(columnItem is PopupTextBoxColumnItem)
                    column = new GridViewPopupTextBoxColumn {
                        DropDownContent = ((PopupTextBoxColumnItem)columnItem).DropDownContent,
                        IsEditable = ((PopupTextBoxColumnItem)columnItem).IsEditable,
                        PopupTitle = ((PopupTextBoxColumnItem)columnItem).PopupTitle,
                    };
                else if(columnItem is CustomColumnItem)
                    column = ((CustomColumnItem)columnItem).ColumnFactory();
                else if(propertyInfo.PropertyType == typeof(bool))
                    column = new GridViewCheckBoxColumn {
                        AutoSelectOnEdit = true,
                        EditTriggers = GridViewEditTriggers.Default,
                    };
                else {
                    if(string.IsNullOrEmpty(columnItem.FormatString))
                        column = new GridViewDataColumn();
                    else {
                        column = new GridViewMaskedInputColumn {
                            MaskType = (MaskType)(int)columnItem.MaskType,
                            //Mask = columnItem.MaskText,
                            DataFormatString = columnItem.FormatString
                        };
                    }
                    //if(columnItem.MaskType == Core.Model.MaskType.DateTime && string.IsNullOrEmpty(columnItem.FormatString))
                    //    column.DataFormatString = "yyyy-MM-dd HH:mm:ss";
                    if((propertyInfo.PropertyType == typeof(DateTime) || propertyInfo.PropertyType == typeof(DateTime?))
                        && string.IsNullOrEmpty(columnItem.FormatString))
                        column.DataFormatString = "yyyy-MM-dd HH:mm:ss";

                    if(!string.IsNullOrEmpty(columnItem.IconPropertyName))
                        column.CellTemplate = (DataTemplate)XamlReader.Load(string.Format("<DataTemplate xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\"><StackPanel Orientation=\"Horizontal\"><Image Stretch=\"None\" Source=\"{{Binding {0}}}\" Margin=\"0,0,3,0\" /><TextBlock Text=\"{{Binding {1}}}\" VerticalAlignment=\"Center\" /></StackPanel></DataTemplate>", columnItem.IconPropertyName, columnItem.Name));
                    else if(propertyInfo.PropertyType == typeof(string))
                        column.CellTemplate = (DataTemplate)XamlReader.Load(string.Format("<DataTemplate xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\" xmlns:local=\"clr-namespace:Sunlight.Silverlight.Controls;assembly=Sunlight.Silverlight.Controls\"><Border><Border.Resources><local:GridViewLongTextConverter x:Key=\"LongText\" /></Border.Resources><TextBlock Text=\"{{Binding {0}, Mode=OneWay, Converter={{StaticResource LongText}}}}\" VerticalAlignment=\"Center\" /></Border></DataTemplate>", columnItem.Name));
                }
                column.Header = string.IsNullOrWhiteSpace(columnItem.Title) ? Utils.GetEntityLocalizedName(propertyInfo.DeclaringType, propertyInfo.Name) : columnItem.Title;
                column.TextAlignment = columnItem.TextAlignment;
                column.IsReadOnly = columnItem.IsReadOnly;
                column.IsReadOnlyBinding = columnItem.IsReadOnlyBinding;
                column.IsSortable = columnItem.IsSortable;
                column.IsGroupable = columnItem.IsGroupable;
                column.ShowColumnWhenGrouped = column.ShowColumnWhenGrouped;
                column.ShowDistinctFilters = columnItem.ShowDistinctFilters;
                column.DataMemberBinding = new Binding(columnItem.Name) {
                    Mode = BindingMode.TwoWay,
                    NotifyOnValidationError = true,
                    ValidatesOnDataErrors = true,
                    ValidatesOnExceptions = true,
                    ValidatesOnNotifyDataErrors = true,
                    UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                };
                var aggregateColumnItem = columnItem as AggregateColumnItem;
                if(aggregateColumnItem != null) {
                    column.FooterTextAlignment = columnItem.TextAlignment;
                    string format = null;
                    var attributes = propertyInfo.GetCustomAttributes(typeof(DisplayFormatAttribute), false);
                    if(attributes.Length > 0)
                        format = string.Concat("{0:", ((DisplayFormatAttribute)attributes[0]).DataFormatString, "}");
                    column.AggregateFunctions.Clear();
                    switch(aggregateColumnItem.AggregateType) {
                        case AggregateType.Count:
                            column.AggregateFunctions.Add(new CountFunction {
                                Caption = aggregateColumnItem.AggregateCaption ?? ControlUIStrings.GridView_AggregateCount,
                            });
                            break;
                        case AggregateType.Sum:
                            column.AggregateFunctions.Add(new SumFunction {
                                Caption = aggregateColumnItem.AggregateCaption ?? ControlUIStrings.GridView_AggregateSum,
                                ResultFormatString = format,
                            });
                            break;
                        case AggregateType.Min:
                            column.AggregateFunctions.Add(new MinFunction {
                                Caption = aggregateColumnItem.AggregateCaption ?? ControlUIStrings.GridView_AggregateMin,
                                ResultFormatString = format,
                            });
                            break;
                        case AggregateType.Max:
                            column.AggregateFunctions.Add(new MaxFunction {
                                Caption = aggregateColumnItem.AggregateCaption ?? ControlUIStrings.GridView_AggregateMax,
                                ResultFormatString = format,
                            });
                            break;
                        case AggregateType.Average:
                            column.AggregateFunctions.Add(new AverageFunction {
                                Caption = aggregateColumnItem.AggregateCaption ?? ControlUIStrings.GridView_AggregateAverage,
                                ResultFormatString = format,
                            });
                            break;
                    }
                }
                dataGrid.Columns.Add(column);
            }

            if(useDomainDataSource)
                return;

            foreach(var columnItem in columnItems.Where(c => c.IsSortable && c.IsSortDescending != null && c.SubColumns == null))
                dataGrid.SortDescriptors.Add(new ColumnSortDescriptor {
                    Column = dataGrid.Columns.OfType<GridViewBoundColumnBase>().Single(c => c.DataMemberBinding.Path.Path == columnItem.Name),
                    SortDirection = (columnItem.IsSortDescending != null && columnItem.IsSortDescending.Value) ? ListSortDirection.Descending : ListSortDirection.Ascending,
                });

            foreach(var columnItem in columnItems.Where(c => c.IsGroupable && c.IsDefaultGroup && c.SubColumns == null).OrderBy(c => c.DefaultGroupPosition))
                dataGrid.GroupDescriptors.Add(new ColumnGroupDescriptor {
                    Column = dataGrid.Columns.OfType<GridViewBoundColumnBase>().Single(c => c.DataMemberBinding.Path.Path == columnItem.Name),
                    SortDirection = columnItem.IsGroupSortDescending == null ? (ListSortDirection?)null : (columnItem.IsGroupSortDescending.Value ? ListSortDirection.Descending : ListSortDirection.Ascending),
                });

            // ReSharper restore PossibleMultipleEnumeration
        }

        public static DataTemplate CreateDetailPanelsTemplate(IEnumerable<PanelItem> panelItems) {
            if(panelItems == null)
                throw new ArgumentNullException("panelItems");

            var sbTemplate = new StringBuilder();
            sbTemplate.Append("<DataTemplate xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\" xmlns:telerik=\"http://schemas.telerik.com/2008/xaml/presentation\" xmlns:sds=\"clr-namespace:Sunlight.Silverlight.Controls;assembly=Sunlight.Silverlight.Controls\">");
            sbTemplate.Append("<telerik:RadTabControl BackgroundVisibility=\"Collapsed\" Margin=\"24,0,4,4\" Background=\"Transparent\">");
            foreach(var panelItem in panelItems)
                sbTemplate.AppendFormat("<telerik:RadTabItem Header=\"{0}\" Margin=\"2,0,0,0\" Height=\"24\" />", panelItem.UniqueId);
            sbTemplate.Append("</telerik:RadTabControl>");
            sbTemplate.Append("</DataTemplate>");
            return (DataTemplate)XamlReader.Load(sbTemplate.ToString());
        }
    }
}
