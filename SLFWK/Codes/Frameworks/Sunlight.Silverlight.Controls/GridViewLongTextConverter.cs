﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Sunlight.Silverlight.Controls {
    public class GridViewLongTextConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if(value == null || value.GetType() != typeof(string))
                return value;
            var str = (string)value;
            str = str.Replace("\r\n", " ");
            str = str.Replace("\n", " ");
            str = str.Replace("\r", " ");
            return str;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
