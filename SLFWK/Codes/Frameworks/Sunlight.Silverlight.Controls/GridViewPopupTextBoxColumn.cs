﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using Sunlight.Silverlight.Controls;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dms.Controls {
    public class GridViewPopupTextBoxColumn : GridViewBoundColumnBase {
        public static readonly DependencyProperty DropDownContentProperty = DependencyProperty.Register("DropDownContent", typeof(UIElement), typeof(GridViewPopupTextBoxColumn), new PropertyMetadata(default(UIElement)));
        public static readonly DependencyProperty IsEditableProperty = DependencyProperty.Register("IsEditable", typeof(bool), typeof(GridViewPopupTextBoxColumn), new PropertyMetadata(true));
        public static readonly DependencyProperty PopupTitleProperty = DependencyProperty.Register("PopupTitle", typeof(string), typeof(GridViewPopupTextBoxColumn), new PropertyMetadata("查询"));

        private PopupTextBox popupTextBox;

        public UIElement DropDownContent {
            get {
                return (UIElement)this.GetValue(DropDownContentProperty);
            }
            set {
                this.SetValue(DropDownContentProperty, value);
            }
        }

        public bool IsEditable {
            get {
                return (bool)GetValue(IsEditableProperty);
            }
            set {
                SetValue(IsEditableProperty, value);
            }
        }

        public string PopupTitle {
            get {
                return (string)GetValue(PopupTitleProperty);
            }
            set {
                SetValue(PopupTitleProperty, value);
            }
        }

        public static string Title;

        public override FrameworkElement CreateCellEditElement(GridViewCell cell, object dataItem) {
            this.BindingTarget = PopupTextBox.TextProperty;

            if(this.popupTextBox == null) {
                this.popupTextBox = new PopupTextBox {
                    UseBlurEffect = true,
                    Title = PopupTitle,
                    PopupContent = new Border {
                        BorderBrush = new SolidColorBrush(Color.FromArgb(0x99, 0x0, 0x0, 0x0)),
                        BorderThickness = new Thickness(1),
                        Child = this.DropDownContent,
                    },
                    IsReadOnly = !this.IsEditable
                };
                popupTextBox.PopupDataContext = cell.DataContext;
                this.popupTextBox.SetBinding(this.BindingTarget, new Binding {
                    Mode = BindingMode.TwoWay,
                    NotifyOnValidationError = true,
                    ValidatesOnExceptions = true,
                    ValidatesOnDataErrors = true,
                    ValidatesOnNotifyDataErrors = true,
                    UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                    Path = new PropertyPath(this.DataMemberBinding.Path.Path)
                });
            }
            return this.popupTextBox;
        }

        protected override object PrepareCellForEdit(FrameworkElement editingElement, RoutedEventArgs editingEventArgs) {
            if(popupTextBox == null || editingElement == null)
                return null;

            popupTextBox.PopupDataContext = editingElement.DataContext;
            return null;
        }

        public override IList<string> UpdateSourceWithEditorValue(GridViewCell gridViewCell) {
            var errors = new List<ValidationError>();
            var editor = (PopupTextBox)gridViewCell.GetEditingElement();
            var bindingExpression = editor.ReadLocalValue(PopupTextBox.TextProperty) as BindingExpression;
            if(bindingExpression != null) {
                bindingExpression.UpdateSource();
                errors.AddRange(Validation.GetErrors(editor));
            }
            return errors.Select(e => e.ErrorContent as string).ToList();
        }

        public override object GetNewValueFromEditor(object editor) {
            var popupTextBox = editor as PopupTextBox;
            return popupTextBox != null ? popupTextBox.Text : null;
        }
    }
}
