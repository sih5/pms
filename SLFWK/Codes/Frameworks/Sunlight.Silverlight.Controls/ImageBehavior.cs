﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Controls {
    public class ImageBehavior : Behavior<Image> {
        private double _From = 1;
        private double _To = 0.6;
        //装载DoubleAnimation动画的故事板
        Storyboard sboard = new Storyboard();
        DoubleAnimation danima = new DoubleAnimation();
        Image img;

        /// <summary>
        /// 透明度从多少开始
        /// </summary>
        public double From {
            get {
                return _From;
            }
            set {
                _From = value;
            }
        }

        /// <summary>
        /// 透明度到多少结尾
        /// </summary>
        public double To {
            get {
                return _To;
            }
            set {
                _To = value;
            }
        }

        /// <summary>
        /// 在为某个对象添加Behavior行为时附加事件
        /// </summary>
        protected override void OnAttached() {
            base.OnAttached();

            //清除故事版和资源
            img = this.AssociatedObject as Image;
            sboard.Children.Clear();
            img.Resources.Clear();
            //设置img控件的透明度的Double类型数字变化
            danima.SetValue(Storyboard.TargetNameProperty, img.Name);
            danima.SetValue(Storyboard.TargetPropertyProperty, new PropertyPath("UIElement.Opacity"));
            danima.Duration = new Duration(new TimeSpan(0, 0, 1));
            sboard.Children.Add(danima);
            img.Resources.Add("Storyboard", sboard);

            //绑定鼠标事件
            img.MouseEnter += new MouseEventHandler(img_MouseEnter);
            img.MouseLeave += new MouseEventHandler(img_MouseLeave);
        }

        /// <summary>
        /// 在为某个对象移除Behavior行为时注销事件
        /// </summary>
        protected override void OnDetaching() {
            base.OnDetaching();
            img.MouseEnter -= new MouseEventHandler(img_MouseEnter);
            img.MouseLeave -= new MouseEventHandler(img_MouseLeave);
        }

        void img_MouseEnter(object sender, MouseEventArgs e) {
            danima.From = this.From;
            danima.To = this.To;
            sboard.Begin();
        }

        void img_MouseLeave(object sender, MouseEventArgs e) {
            danima.From = this.To;
            danima.To = this.From;
            sboard.Begin();
        }
    }

}