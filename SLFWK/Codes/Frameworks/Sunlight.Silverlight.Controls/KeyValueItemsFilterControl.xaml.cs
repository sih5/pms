﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Controls {
    public partial class KeyValueItemsFilterControl : IFilteringControl {
        public sealed class BindingItem : ModelBase {
            private bool isChecked;
            private FilterDescriptor filter;

            public bool IsChecked {
                get {
                    return this.isChecked;
                }
                set {
                    if(this.isChecked != value) {
                        this.isChecked = value;
                        this.NotifyOfPropertyChange(() => this.IsChecked);
                    }
                }
            }

            public string Member {
                get;
                set;
            }

            public string Content {
                get;
                set;
            }

            public object Value {
                get;
                set;
            }

            public FilterDescriptor Filter {
                get {
                    return this.filter ?? (this.filter = new FilterDescriptor(this.Member, Telerik.Windows.Data.FilterOperator.IsEqualTo, this.Value));
                }
            }
        }

        public static readonly DependencyProperty IsActiveProperty = DependencyProperty.Register("IsActive", typeof(bool), typeof(KeyValueItemsFilterControl), new PropertyMetadata(false));
        private readonly List<BindingItem> bindingItems = new List<BindingItem>();
        private readonly CompositeFilterDescriptor compositeFilter = new CompositeFilterDescriptor {
            LogicalOperator = FilterCompositionLogicalOperator.Or,
        };
        private bool isSuspending, hasPrepared;
        private int checkedCount;
        private GridViewComboBoxColumn column;

        public bool IsActive {
            get {
                return (bool)this.GetValue(IsActiveProperty);
            }
            set {
                this.SetValue(IsActiveProperty, value);
            }
        }

        private void SetFilter() {
            if(this.isSuspending)
                return;
            if(!this.column.DataControl.FilterDescriptors.Contains(this.compositeFilter))
                this.column.DataControl.FilterDescriptors.Add(this.compositeFilter);
            this.IsActive = true;
        }

        private void ClearFilter() {
            if(this.column.DataControl.FilterDescriptors.Contains(this.compositeFilter))
                this.column.DataControl.FilterDescriptors.Remove(this.compositeFilter);
            this.IsActive = false;
        }

        private void SetAllCheckBox(bool isChecked) {
            this.isSuspending = true;
            try {
                foreach(var item in this.bindingItems)
                    item.IsChecked = isChecked;
            } finally {
                this.isSuspending = false;
                this.SetFilter();
            }
        }

        private void SetAllCheckBoxState() {
            if(this.checkedCount == 0)
                this.cbSelectAll.IsChecked = false;
            else if(this.checkedCount == this.bindingItems.Count)
                this.cbSelectAll.IsChecked = true;
            else
                this.cbSelectAll.IsChecked = null;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e) {
            var fd = this.ParentOfType<FilteringDropDown>();
            if(fd != null)
                fd.IsDropDownOpen = false;
        }

        private void btnClear_Click(object sender, RoutedEventArgs e) {
            this.cbSelectAll.IsChecked = false;
            this.ClearFilter();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e) {
            this.checkedCount++;
            var filter = ((FrameworkElement)sender).Tag as FilterDescriptor;
            if(filter != null) {
                if(!this.compositeFilter.FilterDescriptors.Contains(filter))
                    this.compositeFilter.FilterDescriptors.Add(filter);
                this.SetFilter();
            }
            this.SetAllCheckBoxState();
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e) {
            this.checkedCount--;
            var filter = ((FrameworkElement)sender).Tag as FilterDescriptor;
            if(filter != null) {
                if(this.compositeFilter.FilterDescriptors.Contains(filter))
                    this.compositeFilter.FilterDescriptors.Remove(filter);
                this.SetFilter();
            }
            this.SetAllCheckBoxState();
        }

        private void cbSelectAll_Checked(object sender, RoutedEventArgs e) {
            this.SetAllCheckBox(true);
        }

        private void cbSelectAll_Unchecked(object sender, RoutedEventArgs e) {
            this.SetAllCheckBox(false);
            this.IsActive = false;
        }

        public void Prepare(GridViewColumn column) {
            if(this.hasPrepared)
                return;
            if(column == null || column.DataControl == null || !(column is GridViewComboBoxColumn))
                return;
            this.column = (GridViewComboBoxColumn)column;
            Type itemType = null;
            foreach(var item in this.column.ItemsSource) {
                if(itemType == null)
                    itemType = item.GetType();
                var contentProperty = itemType.GetProperty(this.column.DisplayMemberPath);
                var valueProperty = itemType.GetProperty(this.column.SelectedValueMemberPath);
                this.bindingItems.Add(new BindingItem {
                    Member = this.column.DataMemberBinding.Path.Path,
                    Content = contentProperty.GetValue(item, null) as string,
                    Value = valueProperty.GetValue(item, null),
                });
            }
            this.lbDistinctValues.ItemsSource = this.bindingItems;
            this.hasPrepared = true;
        }

        public KeyValueItemsFilterControl() {
            this.InitializeComponent();
        }
    }
}
