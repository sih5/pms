﻿using System.Collections.Generic;

namespace Sunlight.Silverlight.Logos {
    public partial class Doosan : LogoBase {
        private List<double[]> pathAspectData = new List<double[]> {
            //W H L T
            new double[] { 0.4419, 0.9263, -0.0008, -0.0008 },
            new double[] { 0.3701, 0.7726, 0.3085, 0.2292 },
            new double[] { 0.3882, 0.8117, 0.6127, 0.1553 },
            new double[] { 0.9163, 0.2751, 0.0455, 0.3817 },
        };

        protected override List<double[]> PathAspectData {
            get {
                return this.pathAspectData;
            }
        }

        public override double AspectRatio {
            get {
                return 0.484;
            }
        }

        public Doosan() {
            InitializeComponent();
        }
    }
}
