﻿using System.Collections.Generic;

namespace Sunlight.Silverlight.Logos {
    public partial class Foton : LogoBase {
        private List<double[]> pathAspectData = new List<double[]> {
            //W H L T
            new double[] { 0.9541, 0.9565, 0.0250, 0.0232 },

            new double[] { 0.2688, 0.0203, 0.0356, 0.0034 },
            new double[] { 0.2432, 0.0196, 0.3840, 0.0042 },
            new double[] { 0.2615, 0.0206, 0.7138, 0.0032 },

            new double[] { 0.4871, 0.9519, 0.5142, 0.0500 },
            new double[] { 0.3113, 0.6082, 0.3438, 0.0496 },
            new double[] { 0.1476, 0.2741, 0.1840, 0.0422 },

            new double[] { 0.1375, 0.2577, 0.0022, 0.0566 },
            new double[] { 0.1347, 0.2524, 0.1750, 0.4042 },
            new double[] { 0.1458, 0.2659, 0.3408, 0.7360 },

            new double[] { 0.0399, 0.0553, 0.0024, 0.0028 },
            new double[] { 0.2158, 0.4020, 0.1748, 0.0034 },
            new double[] { 0.3795, 0.7336, 0.3412, 0.0036 },

            new double[] { 0.0365, 0.0490, 0.9650, 0.0030 },
            new double[] { 0.0384, 0.0490, 0.6168, 0.0034 },
            new double[] { 0.0387, 0.0422, 0.2936, 0.0032 },

            new double[] { 0.0453, 0.0229, 0.4788, 0.9770 },
            new double[] { 0.0491, 0.0331, 0.3012, 0.6242 },
            new double[] { 0.0569, 0.0318, 0.1324, 0.2838 },
        };

        protected override List<double[]> PathAspectData {
            get {
                return this.pathAspectData;
            }
        }

        public override double AspectRatio {
            get {
                return 0.8534;
            }
        }

        public Foton() {
            InitializeComponent();
        }
    }
}
