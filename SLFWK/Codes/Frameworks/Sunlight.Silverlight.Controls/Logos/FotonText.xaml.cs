﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Logos {
    public enum FotonTextType {
        Foton,
        福田汽车,
    }

    public partial class FotonText : UserControl {
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(FotonTextType), typeof(FotonText), null);

        private double[] aspectRatio = new double[] { 0.13, 0.17 };

        public FotonText() {
            InitializeComponent();
        }

        public FotonTextType Text {
            get {
                return (FotonTextType)GetValue(TextProperty);
            }
            set {
                SetValue(TextProperty, value);
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e) {
            string name = string.Concat("pth", ((int)Text).ToString());
            var obj = FindName(name);
            if(obj is Path) {
                Path path = obj as Path;
                path.Fill = Foreground;
                path.Visibility = Visibility.Visible;
            }
        }

        public double GetAspectRatio(FotonTextType text) {
            return this.aspectRatio[(int)text];
        }
    }
}
