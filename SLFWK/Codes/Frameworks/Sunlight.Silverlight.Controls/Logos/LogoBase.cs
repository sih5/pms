﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Logos {
    public class LogoBase : UserControl {
        protected virtual List<double[]> PathAspectData {
            get {
                return new List<double[]>(0);
            }
        }

        public virtual double AspectRatio {
            get {
                return 0;
            }
        }

        private void LogoBase_LayoutUpdated(object sender, EventArgs e) {
            Grid LayoutRoot = Content as Grid;
            if(LayoutRoot != null) {
                double height = LayoutRoot.ActualHeight;
                double width = LayoutRoot.ActualWidth;

                for(int i = 0; i < LayoutRoot.Children.Count; i++) {
                    var path = (Path)LayoutRoot.Children[i];
                    if(i >= PathAspectData.Count)
                        path.Visibility = Visibility.Collapsed;
                    else {
                        path.Visibility = Visibility.Visible;
                        path.UseLayoutRounding = false;
                        path.HorizontalAlignment = HorizontalAlignment.Left;
                        path.VerticalAlignment = VerticalAlignment.Top;
                        path.Stretch = Stretch.Fill;
                        double[] ratio = PathAspectData[i];
                        path.Width = width * ratio[0];
                        path.Height = height * ratio[1];
                        path.Margin = new Thickness(width * ratio[2], height * ratio[3], 0, 0);
                    }
                }

                LayoutRoot.Height = width * AspectRatio;
            }
        }

        public LogoBase() {
            this.LayoutUpdated += new EventHandler(LogoBase_LayoutUpdated);
        }
    }
}
