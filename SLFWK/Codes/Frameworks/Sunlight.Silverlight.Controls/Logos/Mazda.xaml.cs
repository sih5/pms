﻿using System.Collections.Generic;

namespace Sunlight.Silverlight.Logos {
    public partial class Mazda : LogoBase {
        private List<double[]> pathAspectData = new List<double[]> {
            //W H L T
            new double[] { 0.9975, 0.9878, 0.0006, 0.0006 },
            new double[] { 0.0858, 0.4243, 0.0036, 0.1645 },
            new double[] { 0.1180, 0.2212, 0.3854, 0.5045 },
            new double[] { 0.4192, 0.4534, 0.4970, 0.27255 },
            new double[] { 0.2550, 0.2175, 0.49824, 0.31575 },
            new double[] { 0.35648, 0.18465, 0.10008, 0.00075 },
            new double[] { 0.74636, 0.65095, 0.04910, 0.281675 },
            new double[] { 0.07256, 0.40565, 0.87002, 0.267275 },
            new double[] { 0.36482, 0.2531, 0.56472, 0.00000 },
            new double[] { 0.44464, 0.09435, 0.26502, 0.02585 },
            new double[] { 0.99886, 0.79545, 0.00000, 0.193525 },
            new double[] { 0.255, 0.2175, 0.24324, 0.31575 },
            new double[] { 0.72822, 0.22505, 0.13298, 0.0225 },
            new double[] { 0.28772, 0.204225, 0.12228, 0.029825 },
            new double[] { 0.33108, 0.213175, 0.55, 0.026325 },
        };

        protected override List<double[]> PathAspectData {
            get {
                return this.pathAspectData;
            }
        }

        public override double AspectRatio {
            get {
                return 0.8;
            }
        }

        public Mazda() {
            InitializeComponent();
        }
    }
}
