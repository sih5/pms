﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Logos {
    public enum MazdaTextType {
        Mazda,
        Mazda6,
        一汽轿车,
        魅力科技,
        ZoomZoom,
    }

    public partial class MazdaText : UserControl {
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(MazdaTextType), typeof(MazdaText), null);

        private double[] aspectRatio = new double[] { 0.17, 0.18, 0.23, 0.13, 0.42 };

        public MazdaText() {
            InitializeComponent();
        }

        public MazdaTextType Text {
            get {
                return (MazdaTextType)GetValue(TextProperty);
            }
            set {
                SetValue(TextProperty, value);
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e) {
            string name = string.Concat("pth", ((int)Text).ToString());
            var obj = FindName(name);
            if(obj is Path) {
                Path path = obj as Path;
                path.Fill = Foreground;
                path.Visibility = Visibility.Visible;
            }
        }

        public double GetAspectRatio(MazdaTextType text) {
            return this.aspectRatio[(int)text];
        }
    }
}
