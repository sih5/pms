﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Controls {
    public class MultiRadListBox : RadListBox {
        public static readonly DependencyProperty DefaultValuesProperty = DependencyProperty.Register("DefaultValues", typeof(List<int>), typeof(MultiRadListBox), null);
        public static readonly DependencyProperty DefaultExValuesProperty = DependencyProperty.Register("DefaultExValues", typeof(List<string>), typeof(MultiRadListBox), null);
        public List<int> DefaultValues {
            get {
                return (List<int>)this.GetValue(DefaultValuesProperty);
            }
            set {
                this.SetValue(DefaultValuesProperty, value);
            }
        }

        public List<string> DefaultExValues {
            get {
                return (List<string>)this.GetValue(DefaultExValuesProperty);
            }
            set {
                this.SetValue(DefaultExValuesProperty, value);
            }
        }
        protected override void OnItemsChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e) {
            base.OnItemsChanged(e);
            if(DefaultValues == null)
                return;
            foreach(var item in Items) {
                var value = DataControlHelper.GetPropertyInfo(item.GetType(), SelectedValuePath).GetValue(item, null);
                if(DefaultValues.Contains((int)value) && !this.SelectedItems.Contains(item))
                    this.SelectedItems.Add(item);
            }
        }
    }
    public partial class MultiSelectComboBoxControl : UserControl, INotifyPropertyChanged {
        public MultiSelectComboBoxControl() {
            InitializeComponent();
            this.RadListBox.SetBinding(MultiRadListBox.DefaultValuesProperty, new Binding("DefaultValues") {
                Source = this
            });
            this.RadListBox.SetBinding(MultiRadListBox.DefaultExValuesProperty, new Binding("DefaultExValues") {
                Source = this
            });
        }

        private string selectedValuePath;
        public string SelectedValuePath {
            get
            {
                return selectedValuePath;
            }
            set
            {
                selectedValuePath = value;
                this.RadListBox.SelectedValuePath = selectedValuePath;
            }
        }
        public string DisplayMemberPath {
            get;
            set;
        }
      
        public static readonly DependencyProperty SelectedTextsProperty = DependencyProperty.Register("SelectedTexts", typeof(string), typeof(MultiSelectComboBoxControl), null);
        public string SelectedTexts {
            get {
                return (string)this.GetValue(SelectedTextsProperty);
            }
            set {
                this.SetValue(SelectedTextsProperty, value);
            }
        }

        private IList _selectedValues = new List<object>();
        /// <summary>
        /// Gets or sets the selected values
        /// </summary>
        public IList SelectedValues {
            get {
                return _selectedValues;
            }
            set {
                if(_selectedValues != value) {
                    _selectedValues = value;
                }
            }
        }

    

        public static readonly DependencyProperty DefaultValuesProperty = DependencyProperty.Register("DefaultValues", typeof(List<int>), typeof(MultiSelectComboBoxControl), null);
        public List<int> DefaultValues {
            get {
                return (List<int>)this.GetValue(DefaultValuesProperty);
            }
            set {
                this.SetValue(DefaultValuesProperty, value);
            }
        }

        public static readonly DependencyProperty DefaultExValuesProperty = DependencyProperty.Register("DefaultExValues", typeof(List<string>), typeof(MultiSelectComboBoxControl), null);
        public List<string> DefaultExValues {
            get {
                return (List<string>)this.GetValue(DefaultExValuesProperty);
            }
            set {
                this.SetValue(DefaultValuesProperty, value);
            }
        }


       
        private void ButtonModelRangeClear_OnClick(object sender, RoutedEventArgs e) {
            this.RadListBox.SelectedItems.Clear();
        }
       
        private void RadListBox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            SelectedTexts = string.Empty;
            _selectedValues.Clear();
            foreach(var item in this.RadListBox.SelectedItems) {
                var key = DataControlHelper.GetPropertyInfo(item.GetType(), DisplayMemberPath).GetValue(item, null);
                SelectedTexts += key+",";
                var value = DataControlHelper.GetPropertyInfo(item.GetType(), SelectedValuePath).GetValue(item, null);
                if(!_selectedValues.Contains(value))
                    _selectedValues.Add(value);
            }
            if(!string.IsNullOrEmpty(SelectedTexts))
               SelectedTexts= SelectedTexts.Remove(SelectedTexts.Length - 1);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ButtonModelRangeSelectAll_OnClick(object sender, RoutedEventArgs e) {
            foreach(var item in this.RadListBox.Items) {
                if(!this.RadListBox.SelectedItems.Contains(item))
                    this.RadListBox.SelectedItems.Add(item);
            }
        }
    }
    public static class DataControlHelper {
        public static PropertyInfo GetPropertyInfo(Type objectType, string bindingPath) {
            if(bindingPath == null) {
                return null;
            }

            PropertyInfo propertyInfo = null;
            foreach(string path in bindingPath.Split('.')) {
                propertyInfo = objectType.GetProperty(path);
                if(propertyInfo == null) {
                    break;
                }
                objectType = propertyInfo.PropertyType;
            }
            return propertyInfo;
        }
    }
}
