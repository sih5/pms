﻿using System.Collections.Generic;
using System.Windows;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Controls {
    partial class MultipleTextBox {
        public static readonly DependencyProperty IsReadOnlyProperty = DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(MultipleTextBox), new PropertyMetadata(default(bool)));

        /// <summary>
        ///     只读状态
        /// </summary>
        public bool IsReadOnly {
            get {
                return (bool)GetValue(IsReadOnlyProperty);
            }
            set {
                SetValue(IsReadOnlyProperty, value);
            }
        }

        public static readonly DependencyProperty ItemsProperty = DependencyProperty.Register("Items", typeof(IList<string>), typeof(MultipleTextBox), new PropertyMetadata(null));

        /// <summary>
        ///     单值/多值的存储
        /// </summary>
        public IList<string> Items {
            get {
                return (IList<string>)GetValue(ItemsProperty);
            }
            private set {
                SetValue(ItemsProperty, value);
            }
        }

        public static readonly DependencyProperty ClearCommandProperty = DependencyProperty.Register("ClearCommand", typeof(DelegateCommand), typeof(MultipleTextBox), new PropertyMetadata(null));

        /// <summary>
        ///     清除输入内容的命令
        /// </summary>
        public DelegateCommand ClearCommand {
            get {
                return (DelegateCommand)GetValue(ClearCommandProperty);
            }
            set {
                SetValue(ClearCommandProperty, value);
            }
        }

        public static readonly DependencyProperty PopupCommandProperty = DependencyProperty.Register("PopupCommand", typeof(DelegateCommand), typeof(MultipleTextBox), new PropertyMetadata(default(DelegateCommand)));

        /// <summary>
        ///     弹出多值输入界面的命令
        /// </summary>
        public DelegateCommand PopupCommand {
            get {
                return (DelegateCommand)GetValue(PopupCommandProperty);
            }
            set {
                SetValue(PopupCommandProperty, value);
            }
        }
    }
}