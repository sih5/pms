﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Telerik.Windows.Controls;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Controls {
    /// <summary>
    ///     多值输入控件
    /// </summary>
    public partial class MultipleTextBox : Control {
        /// <summary>
        ///     输入模式，单值或多值
        /// </summary>
        private enum InputMode {
            Single = 1,
            Multiple = 2,
        }

        private const string PART_TEXT_INPUT = "PART_TextInput";

        /// <summary>
        ///     主输入控件，用于输入单值
        /// </summary>
        private RadWatermarkTextBox MainTextBox {
            get;
            set;
        }

        /// <summary>
        ///     多值输入界面的输入控件
        /// </summary>
        private TextBox PopupTextBox {
            get;
            set;
        }

        /// <summary>
        ///     显示多值输入界面的弹出窗口
        /// </summary>
        private RadWindow Window {
            get;
            set;
        }

        /// <summary>
        ///     当前输入模式；在多值模式下，主输入控件不可用
        /// </summary>
        private InputMode CurrentMode {
            get;
            set;
        }

        /// <summary>
        ///     鼠标移入
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseEnter(MouseEventArgs e) {
            base.OnMouseEnter(e);
            VisualStateManager.GoToState(this, State.MouseOver.ToString(), false);
        }

        /// <summary>
        ///     鼠标移出
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseLeave(MouseEventArgs e) {
            base.OnMouseLeave(e);
            VisualStateManager.GoToState(this, State.Normal.ToString(), false);
        }

        public MultipleTextBox() {
            this.DefaultStyleKey = typeof(MultipleTextBox);

            this.ClearCommand = new DelegateCommand(o => this.HandleClear(), x => this.IsEnabled && !this.IsReadOnly);
            this.PopupCommand = new DelegateCommand(o => this.HandlePopup(), x => this.IsEnabled && !this.IsReadOnly);

            this.IsEnabledChanged += (sender, args) => {
                this.ClearCommand.InvalidateCanExecute();
                this.PopupCommand.InvalidateCanExecute();
            };
            this.CurrentMode = InputMode.Single;
            this.Items = new List<string>(10);
        }

        /// <summary>
        ///     弹出窗口，显示多值输入界面
        /// </summary>
        private void HandlePopup() {
            if(this.Window == null) {
                this.Window = new RadWindow {
                    WindowStartupLocation = WindowStartupLocation.CenterScreen,
                    SizeToContent = true,
                    MinHeight = 450,
                    MaxHeight = 600,
                    MinWidth = 300,
                    Header = "请输入多个数据",
                    FontFamily = new FontFamily("Microsoft YaHei")
                };
                // TextBox
                this.PopupTextBox = new RadWatermarkTextBox {
                    AcceptsReturn = true,
                    VerticalContentAlignment = VerticalAlignment.Top,
                    Margin = new Thickness(5),
                    WatermarkContent = "最多可填入1000个数据，请以回车键隔开",
                    WatermarkBehavior = WatermarkBehavior.HideOnTextEntered,
                    VerticalScrollBarVisibility = ScrollBarVisibility.Auto
                };
                Grid.SetRow(this.PopupTextBox, 0);

                // Buttons
                var okButton = new RadButton {
                    Content = "确定",
                    Width = 45,
                    Height = 23
                };
                okButton.Click += (sender, args) => {
                    Items = this.PopupTextBox.Text.Split(new[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    this.UpdateMainTextBoxByItems();
                    this.Window.DialogResult = true;
                    this.Window.Close();
                };

                var cancelButton = new RadButton {
                    Content = "取消",
                    Width = 45,
                    Height = 23,
                    Margin = new Thickness(5, 0, 5, 0)
                };
                cancelButton.Click += (sender, args) => {
                    this.Window.DialogResult = false;
                    this.Window.Close();
                };

                // layout
                var stackPanel = new StackPanel {
                    Orientation = Orientation.Horizontal,
                    HorizontalAlignment = HorizontalAlignment.Right,
                    Margin = new Thickness(0, 5, 0, 5)
                };
                Grid.SetRow(stackPanel, 1);
                stackPanel.Children.Add(okButton);
                stackPanel.Children.Add(cancelButton);

                var grid = new Grid();
                grid.RowDefinitions.Add(new RowDefinition());
                grid.RowDefinitions.Add(new RowDefinition {
                    Height = GridLength.Auto
                });
                grid.Children.Add(this.PopupTextBox);
                grid.Children.Add(stackPanel);
                this.Window.Content = grid;
            }
            // 再次打开输入界面时，应带入目前输入的内容
            if(this.PopupTextBox != null && Items != null) {
                this.PopupTextBox.Text = string.Join(Environment.NewLine, Items);
                this.PopupTextBox.SelectionStart = this.PopupTextBox.Text.Length;
            }

            this.Window.ShowDialog();
        }

        /// <summary>
        ///     清除输入内容
        /// </summary>
        private void HandleClear() {
            if(this.MainTextBox != null) {
                this.MainTextBox.Text = string.Empty;
                this.MainTextBox.IsReadOnly = false;
            }
            if(this.Items != null)
                this.Items.Clear();
            this.CurrentMode = InputMode.Single;
        }

        /// <summary>
        ///     根据输入框里的内容更新 <see cref="Items"/> 属性
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void UpdateItemsByMainTextBox(object sender, TextChangedEventArgs args) {
            if(this.CurrentMode == InputMode.Single && this.Items != null) {
                this.Items.Clear();
                if(!string.IsNullOrEmpty(this.MainTextBox.Text)) {
                    this.Items.Add(this.MainTextBox.Text);
                }
            }
        }

        /// <summary>
        ///     根据 <see cref="Items"/> 属性值更新输入框的内容和状态
        /// </summary>
        private void UpdateMainTextBoxByItems() {
            if(this.Items == null)
                return;

            this.MainTextBox.Text = string.Join(", ", Items);
            if(Items.Any()) {
                this.MainTextBox.IsReadOnly = true;
                this.CurrentMode = InputMode.Multiple;
            } else {
                this.MainTextBox.IsReadOnly = false;
                this.CurrentMode = InputMode.Single;
            }
        }

        /// <summary>
        ///     挂载内部控件的事件处理程序
        /// </summary>
        public override void OnApplyTemplate() {
            base.OnApplyTemplate();
            this.MainTextBox = this.GetTemplateChild(PART_TEXT_INPUT) as RadWatermarkTextBox;
            if(this.MainTextBox != null) {
                // 在文本修改后更新到 Items 中
                this.MainTextBox.TextChanged -= this.UpdateItemsByMainTextBox;
                this.MainTextBox.TextChanged += this.UpdateItemsByMainTextBox;
            }
        }

        /// <summary>
        ///     设置控件的默认值
        /// </summary>
        /// <param name="value">设置的值，支持单个或多个字符串。</param>
        public void SetValue(object value) {
            if(this.Items == null) {
                this.Items = new List<string>(10);
            }

            this.Items.Clear();
            var items = value as IList<string>;
            if(items == null) {
                if(value as string != null)
                    this.Items.Add(value as string);
            } else {
                foreach(var item in items) {
                    this.Items.Add(item);
                }
            }
            this.UpdateMainTextBoxByItems();
        }
    }
}
