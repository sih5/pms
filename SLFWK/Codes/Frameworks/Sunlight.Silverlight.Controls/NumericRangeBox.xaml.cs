﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Controls {
    public partial class NumericRangeBox {
        #region 依赖属性
        public static readonly DependencyProperty SelectedValueFromProperty = DependencyProperty.Register("SelectedValueFrom", typeof(Double?), typeof(NumericRangeBox), null);
        public static readonly DependencyProperty SelectedValueToProperty = DependencyProperty.Register("SelectedValueTo", typeof(Double?), typeof(NumericRangeBox), null);
        public static readonly DependencyProperty ValueFormatProperty = DependencyProperty.Register("ValueFormat", typeof(ValueFormat), typeof(NumericRangeBox), new PropertyMetadata(OnValueFormatChanged));
        public static readonly DependencyProperty DecimalDigitsProperty = DependencyProperty.Register("DecimalDigits", typeof(int), typeof(NumericRangeBox), new PropertyMetadata(OnDecimalDigitsChanged));

        public int DecimalDigits {
            get {
                return (int)GetValue(DecimalDigitsProperty);
            }
            set {
                SetValue(DecimalDigitsProperty, value);
            }
        }

        private static void OnDecimalDigitsChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args) {
            var numericRangeBox = (NumericRangeBox)obj;
            numericRangeBox.DtpFrom.NumberDecimalDigits = numericRangeBox.DtpTo.NumberDecimalDigits = (int)args.NewValue;
        }

        private static void OnValueFormatChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args) {
            var numericRangeBox = (NumericRangeBox)obj;
            numericRangeBox.DtpFrom.ValueFormat = (ValueFormat)args.NewValue;
            numericRangeBox.DtpTo.ValueFormat = (ValueFormat)args.NewValue;
        }

        public ValueFormat ValueFormat {
            get {
                return (ValueFormat)GetValue(ValueFormatProperty);
            }
            set {
                SetValue(ValueFormatProperty, value);
            }
        }

        public Double? SelectedValueFrom {
            get {
                return (Double?)this.GetValue(SelectedValueFromProperty);
            }
            set {
                this.SetValue(SelectedValueFromProperty, value);
            }
        }

        public Double? SelectedValueTo {
            get {
                return (Double?)this.GetValue(SelectedValueToProperty);
            }
            set {
                this.SetValue(SelectedValueToProperty, value);
            }
        }
        #endregion

        public NumericRangeBox() {
            this.InitializeComponent();
            this.DtpFrom.SetBinding(RadRangeBase.ValueProperty, new Binding("SelectedValueFrom") {
                Source = this,
                Mode = BindingMode.TwoWay,
            });
            this.DtpTo.SetBinding(RadRangeBase.ValueProperty, new Binding("SelectedValueTo") {
                Source = this,
                Mode = BindingMode.TwoWay,
            });
            //默认2位小数，通过控件的DecimalDigits属性触发OnDecimalDigitsChanged事件
            this.DecimalDigits = 2;
            //捕获鼠标滚轮事件，不让其继续向上传递
            this.AddHandler(MouseWheelEvent, new MouseWheelEventHandler(MouseWheel_Move), false);
        }

        private void MouseWheel_Move(object obj, MouseWheelEventArgs e) {
            e.Handled = true;
        }
    }
}
