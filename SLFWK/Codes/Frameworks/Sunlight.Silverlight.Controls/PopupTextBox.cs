﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Threading;
using Sunlight.Silverlight.Core.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Controls {
    [TemplateVisualState(Name = "UnTextExist", GroupName = "ValidationStates")]
    [TemplateVisualState(Name = "TextExist", GroupName = "ValidationStates")]
    [TemplateVisualState(Name = "TextNomal", GroupName = "ValidationStates")]
    public class PopupTextBox : Control {
        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof(string), typeof(PopupTextBox), null);
        public static readonly DependencyProperty PopupContentProperty = DependencyProperty.Register("PopupContent", typeof(UIElement), typeof(PopupTextBox), null);
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(PopupTextBox), null);
        public static readonly DependencyProperty UseBlurEffectProperty = DependencyProperty.Register("UseBlurEffect", typeof(bool), typeof(PopupTextBox), null);
        public static readonly DependencyProperty IsOpenProperty = DependencyProperty.Register("IsOpen", typeof(bool), typeof(PopupTextBox), null);
        public static readonly DependencyProperty IsReadOnlyProperty = DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(PopupTextBox), null);
        public static readonly DependencyProperty TextAlignmentProperty = DependencyProperty.Register("TextAlignment", typeof(TextAlignment), typeof(PopupTextBox), new PropertyMetadata(TextAlignment.Left));
        public static readonly DependencyProperty DelayProperty = DependencyProperty.Register("Delay", typeof(int), typeof(PopupTextBox), new PropertyMetadata(1000));
        public static readonly DependencyProperty IsUpdateCaseProperty = DependencyProperty.Register("IsUpdateCase", typeof(Boolean), typeof(PopupTextBox), new PropertyMetadata(false));
        public event EventHandler RaiseClosedEvent;
        /// <summary>
        ///(int)StartupLocationType.控件下方,(int)StartupLocationType.左上,(int)StartupLocationType.居中:
        /// </summary>
        public StartupLocationType WindowStartupLocation = StartupLocationType.左上;
        private readonly RadWindow window;
        private RadButton button;
        private RadWatermarkTextBox textBox;
        private DispatcherTimer timer;
        /// <summary>
        /// DataGridView PopupTextBoxColumnItem DataContext
        /// </summary>
        public object PopupDataContext;

        public string Title {
            get {
                return (string)this.GetValue(TitleProperty);
            }
            set {
                this.SetValue(TitleProperty, value);
            }
        }
        public Boolean IsUpdateCase {
            get {
                return (Boolean)GetValue(IsUpdateCaseProperty);
            }
            set {
                SetValue(IsUpdateCaseProperty, value);
            }
        }
        public UIElement PopupContent {
            get {
                return (UIElement)this.GetValue(PopupContentProperty);
            }
            set {
                this.SetValue(PopupContentProperty, value);
            }
        }

        public string Text {
            get {
                return (string)this.GetValue(TextProperty);
            }
            set {
                this.SetValue(TextProperty, value);
            }
        }

        public string SelectedText {
            get;
            set;
        }

        public bool UseBlurEffect {
            get {
                return (bool)this.GetValue(UseBlurEffectProperty);
            }
            set {
                this.SetValue(UseBlurEffectProperty, value);
            }
        }

        public bool IsOpen {
            get {
                return (bool)this.GetValue(IsOpenProperty);
            }
            set {
                this.SetValue(IsOpenProperty, value);
            }
        }

        public int Delay {
            get {
                return (int)this.GetValue(DelayProperty);
            }
            set {
                this.SetValue(DelayProperty, value);
            }
        }

        public bool IsReadOnly {
            get {
                return (bool)this.GetValue(IsReadOnlyProperty);
            }
            set {
                this.SetValue(IsReadOnlyProperty, value);
            }
        }

        public TextAlignment TextAlignment {
            get {
                return (TextAlignment)this.GetValue(TextAlignmentProperty);
            }
            set {
                this.SetValue(TextAlignmentProperty, value);
            }
        }

        public Func<UIElement> PopupContentFactory {
            get;
            set;
        }

        private void Button_OnClick(object sender, RoutedEventArgs e) {
            if(this.PopupContent == null && this.PopupContentFactory == null)
                return;
            var popupContent = this.PopupContent;
            if(this.PopupContent == null) {
                popupContent = this.PopupContentFactory();
            }
            if(this.Title == null) {
                var generalView = popupContent as IGeneralView;
                this.window.Header = generalView != null ? generalView.PageTitle : null;
            }
            this.window.Content = popupContent;
            this.window.DataContext = PopupDataContext;
            var transform = (MatrixTransform)this.TransformToVisual(Application.Current.RootVisual);
            switch(this.WindowStartupLocation) {
                case StartupLocationType.控件下方:
                    if(this.window.Left <= 0)
                        this.window.Left = transform.Matrix.OffsetX;
                    if(this.window.Top <= 0)
                        this.window.Top = transform.Matrix.OffsetY + this.ActualHeight;
                    break;
                case StartupLocationType.左上:
                    this.window.Left = 0;
                    this.window.Top = 0;
                    break;
                case StartupLocationType.居中:
                    this.window.Left = Application.Current.RootVisual.RenderSize.Width / 2 - 500;
                    this.window.Top = Application.Current.RootVisual.RenderSize.Height / 2 - 300;
                    //this.window.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                    //this.window.Left = this.window.Left-this.window.Width/2;
                    //this.window.Top = window.Top-this.window.Height/2;
                    break;
            }
            this.window.ShowDialog();
            //if(QueryAutoExecute != null)
            //    QueryAutoExecute(SetValidateState);
        }

        /// <summary>
        /// 设置延迟后 系统自动调用
        /// </summary>
        public Action<string, Action<bool>> QueryExecute;
        public override void OnApplyTemplate() {
            base.OnApplyTemplate();

            this.button = (RadButton)this.GetTemplateChild("PART_DropDownButton");
            this.button.Click += this.Button_OnClick;

            this.textBox = (RadWatermarkTextBox)this.GetTemplateChild("PART_DateTimeInput");
            this.textBox.SetBinding(TextBox.TextProperty, new Binding("Text") {
                Source = this,
                Mode = BindingMode.TwoWay,
            });

            this.textBox.SetBinding(TextBox.IsReadOnlyProperty, new Binding("IsReadOnly") {
                Source = this,
                Mode = BindingMode.TwoWay,
            });

            this.textBox.TextChanged += textBox_TextChanged;
            textBox.MouseRightButtonUp += textBox_MouseRightButtonUp;
        }

        void textBox_MouseRightButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e) {
            this.SelectedText = textBox.SelectedText;
        }

        void textBox_TextChanged(object sender, TextChangedEventArgs e) {
            if(this.timer != null)
                this.timer.Stop();
            if(IsUpdateCase) {
                var selectionStart = textBox.SelectionStart;
                var selectionLength = textBox.SelectionLength;
                textBox.Text = textBox.Text.ToUpper();
                textBox.SelectionStart = selectionStart;
                textBox.SelectionLength = selectionLength;
                SelectedText = textBox.SelectedText;
            }
            if(this.Delay < 0)
                return;

            if(this.timer == null) {
                this.timer = new DispatcherTimer();
                this.timer.Tick += (_, __) => {
                    ((DispatcherTimer)_).Stop();
                    if(QueryExecute != null)
                        QueryExecute(this.textBox.Text, SetValidateState);
                };
            }

            this.timer.Interval = TimeSpan.FromMilliseconds(this.Delay);
            this.timer.Start();
        }

        public void SetValidateState(bool result) {
            if(result)
                VisualStateManager.GoToState(this, "TextExist", true);
            else {
                VisualStateManager.GoToState(this, "UnTextExist", true);
            }
        }

        public PopupTextBox() {
            this.DefaultStyleKey = typeof(PopupTextBox);

            this.window = new RadWindow {
                SizeToContent = true,
                ResizeMode = ResizeMode.CanResize,
                WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.Manual,
            };
            this.window.SetBinding(WindowBase.IsOpenProperty, new Binding("IsOpen") {
                Source = this,
                Mode = BindingMode.TwoWay,
            });
            this.window.SetBinding(HeaderedContentControl.HeaderProperty, new Binding("Title") {
                Source = this,
                Mode = BindingMode.OneWay,
            });
            this.window.Closed += (sender, args) => {
                if(this.PopupContent == null && this.PopupContentFactory != null) {
                    this.window.Content = null;
                }
                this.RaiseClosed();
            };
        }

        private void RaiseClosed() {
            var handler = this.RaiseClosedEvent;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        public void Close() {
            VisualStateManager.GoToState(this, "TextNomal", true);
            this.window.Close();
        }
    }
}
