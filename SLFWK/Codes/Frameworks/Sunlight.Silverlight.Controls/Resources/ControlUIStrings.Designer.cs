﻿//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.18444
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sunlight.Silverlight.Controls.Resources {
    using System;
    
    
    /// <summary>
    ///   一个强类型的资源类，用于查找本地化的字符串等。
    /// </summary>
    // 此类是由 StronglyTypedResourceBuilder
    // 类通过类似于 ResGen 或 Visual Studio 的工具自动生成的。
    // 若要添加或移除成员，请编辑 .ResX 文件，然后重新运行 ResGen
    // (以 /str 作为命令选项)，或重新生成 VS 项目。
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ControlUIStrings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ControlUIStrings() {
        }
        
        /// <summary>
        ///   返回此类使用的缓存的 ResourceManager 实例。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Sunlight.Silverlight.Controls.Resources.ControlUIStrings", typeof(ControlUIStrings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   使用此强类型资源类，为所有资源查找
        ///   重写当前线程的 CurrentUICulture 属性。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   查找类似 至 的本地化字符串。
        /// </summary>
        internal static string DateTimeRangeBox_To {
            get {
                return ResourceManager.GetString("DateTimeRangeBox_To", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 清除 的本地化字符串。
        /// </summary>
        internal static string FilterPanel_ComboBox_Clear {
            get {
                return ResourceManager.GetString("FilterPanel_ComboBox_Clear", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 平均： 的本地化字符串。
        /// </summary>
        internal static string GridView_AggregateAverage {
            get {
                return ResourceManager.GetString("GridView_AggregateAverage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 共计： 的本地化字符串。
        /// </summary>
        internal static string GridView_AggregateCount {
            get {
                return ResourceManager.GetString("GridView_AggregateCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 最大： 的本地化字符串。
        /// </summary>
        internal static string GridView_AggregateMax {
            get {
                return ResourceManager.GetString("GridView_AggregateMax", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 最小： 的本地化字符串。
        /// </summary>
        internal static string GridView_AggregateMin {
            get {
                return ResourceManager.GetString("GridView_AggregateMin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 合计： 的本地化字符串。
        /// </summary>
        internal static string GridView_AggregateSum {
            get {
                return ResourceManager.GetString("GridView_AggregateSum", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 内部错误：实体类型“{0}”不包含名为“{1}”的属性，请联系技术支持人员解决此问题。 的本地化字符串。
        /// </summary>
        internal static string GridView_PropertyNotExist {
            get {
                return ResourceManager.GetString("GridView_PropertyNotExist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 共 {0} 条记录，每页显示 {1} 条 的本地化字符串。
        /// </summary>
        internal static string GridView_TotalRecords {
            get {
                return ResourceManager.GetString("GridView_TotalRecords", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 请键入搜索内容 的本地化字符串。
        /// </summary>
        internal static string SearchTextBox_DefaultText {
            get {
                return ResourceManager.GetString("SearchTextBox_DefaultText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 复制 的本地化字符串。
        /// </summary>
        internal static string TextBox_Copy {
            get {
                return ResourceManager.GetString("TextBox_Copy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 剪切 的本地化字符串。
        /// </summary>
        internal static string TextBox_Cut {
            get {
                return ResourceManager.GetString("TextBox_Cut", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 粘贴 的本地化字符串。
        /// </summary>
        internal static string TextBox_Paste {
            get {
                return ResourceManager.GetString("TextBox_Paste", resourceCulture);
            }
        }
    }
}
