﻿using System.Text.RegularExpressions;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Controls {
    public class SdtMaskedCurrencyInput : RadMaskedCurrencyInput {
        /// <summary>
        /// 由于RadMaskedCurrencyInput控件存在删除时','删除操作不方便问题，所以重写基类删除方法。
        /// </summary>
        protected override void HandleDeleteKeyNoMask() {
            if(string.IsNullOrEmpty(this.Text))
                return;
            var startIndex = this.SelectionStart;
            char currentTag = this.Text[startIndex];
            var text = this.Text;
            base.HandleDeleteKeyNoMask();
            char numbersGroupSeparator = ',';
            char decimalsGroupSeparator = '.';
            char frountGroupSeparator = '0';
            if(numbersGroupSeparator == currentTag) {
                startIndex += 1;
                text = text.Remove(startIndex, 1);
            } if(decimalsGroupSeparator == currentTag) {
                startIndex += 1;
                text = text.Replace((char)text[startIndex], '0');
                this.SelectionStart = startIndex + 1;
            } if(frountGroupSeparator == currentTag && startIndex + 1 < this.Text.Length && text[startIndex + 1] == decimalsGroupSeparator) {
                text = text.Remove(startIndex, 1);
                if(this.Value < 1)
                    this.SelectionStart = startIndex + 1;
            }
            if(startIndex > 0 && text[startIndex - 1] == decimalsGroupSeparator) {
                text = text.Replace((char)text[startIndex], '0');
                this.SelectionStart = startIndex + 1;
            }

            this.Value = decimal.Parse(this.ExractValueFromText(text));
        }

        private string ExractValueFromText(string text) {
            if(string.IsNullOrEmpty(this.Text))
                return "";
            Regex regexp = new Regex(@"(\d|\.|-).*");
            var content = regexp.Matches(text);
            return content == null ? "" : content[0].ToString();
        }
    }
}