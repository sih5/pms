﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Sunlight.Silverlight.Controls.Resources;

namespace Sunlight.Silverlight.Controls {
    public partial class SearchTextBox {
        private class ClearButtonVisibilityConverter : IValueConverter {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
                return string.IsNullOrEmpty(value as string) ? Visibility.Collapsed : Visibility.Visible;
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
                throw new NotSupportedException();
            }
        }

        public static readonly DependencyProperty SearchTextProperty = DependencyProperty.Register("SearchText", typeof(string), typeof(SearchTextBox), new PropertyMetadata(default(string), (obj, args) => {
            var searchTextBox = (SearchTextBox)obj;
            searchTextBox.MainTextBox.Text = (string)args.NewValue;
            searchTextBox.RaiseSearchTextChanged();
        }));
        public static readonly DependencyProperty WatermarkTextProperty = DependencyProperty.Register("WatermarkText", typeof(string), typeof(SearchTextBox), new PropertyMetadata(ControlUIStrings.SearchTextBox_DefaultText));
        public static readonly DependencyProperty DelayProperty = DependencyProperty.Register("Delay", typeof(int), typeof(SearchTextBox), new PropertyMetadata(-1));

        private bool isImmediate;
        private DispatcherTimer timer;
        public event EventHandler SearchTextChanged;

        public string SearchText {
            get {
                return (string)this.GetValue(SearchTextProperty);
            }
            set {
                this.SetValue(SearchTextProperty, value);
            }
        }

        public string WatermarkText {
            get {
                return (string)this.GetValue(WatermarkTextProperty);
            }
            set {
                this.SetValue(WatermarkTextProperty, value);
            }
        }

        public int Delay {
            get {
                return (int)this.GetValue(DelayProperty);
            }
            set {
                this.SetValue(DelayProperty, value);
            }
        }

        private void RaiseSearchTextChanged() {
            var handler = this.SearchTextChanged;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        private void ClearButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            this.isImmediate = true;
            this.SearchText = this.MainTextBox.Text = string.Empty;
        }

        private void MainTextBox_TextChanged(object sender, TextChangedEventArgs e) {
            if(this.timer != null)
                this.timer.Stop();
            if(this.Delay < 0)
                return;
            if(this.isImmediate || this.Delay == 0) {
                this.isImmediate = false;
                this.SearchText = this.MainTextBox.Text;
            } else {
                if(this.timer == null) {
                    this.timer = new DispatcherTimer();
                    this.timer.Tick += (_, __) => {
                        ((DispatcherTimer)_).Stop();
                        this.SearchText = this.MainTextBox.Text;
                    };
                }
                this.timer.Interval = TimeSpan.FromMilliseconds(this.Delay);
                this.timer.Start();
            }
        }

        private void MainTextBox_KeyDown(object sender, KeyEventArgs e) {
            if(e.Key != Key.Enter)
                return;
            if(this.timer != null)
                this.timer.Stop();
            this.SearchText = this.MainTextBox.Text;
        }

        public SearchTextBox() {
            this.InitializeComponent();
            this.Width = 200;
            this.ClearButton.SetBinding(VisibilityProperty, new Binding("Text") {
                Source = this.MainTextBox,
                Mode = BindingMode.OneWay,
                Converter = new ClearButtonVisibilityConverter(),
            });
            this.WatermarkTextBlock.SetBinding(TextBlock.TextProperty, new Binding("WatermarkText") {
                Source = this,
                Mode = BindingMode.OneWay,
            });
        }
    }
}
