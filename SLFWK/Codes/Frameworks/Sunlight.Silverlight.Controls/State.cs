﻿namespace Sunlight.Silverlight.Controls {
    internal enum State {
        /// <summary>
        /// Normal state.
        /// </summary>
        Normal,

        /// <summary>
        /// Disabled State.
        /// </summary>
        Disabled,

        /// <summary>
        /// ReadOnly State.
        /// </summary>
        ReadOnly,

        /// <summary>
        /// Mouse over State.
        /// </summary>
        MouseOver,

        /// <summary>
        /// Focused State.
        /// </summary>
        Focused,

        /// <summary>
        /// Not focused State.
        /// </summary>
        NotFocused,

        /// <summary>
        /// Empty State.
        /// </summary>
        Empty,

        /// <summary>
        /// Not Empty State.
        /// </summary>
        NotEmpty,

        /// <summary>
        /// Valid State.
        /// </summary>
        Valid,

        /// <summary>
        /// Invalid and focused State.
        /// </summary>
        InvalidFocused,

        /// <summary>
        /// Invalid and not focused State.
        /// </summary>
        InvalidUnfocused
    }
}
