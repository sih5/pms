﻿using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Controls {
    public class UIStrings : ModelBase {
        public void Refresh(params string[] propertyNames) {
            if(propertyNames.Length == 0)
                foreach(var propertyInfo in this.GetType().GetProperties())
                    this.NotifyOfPropertyChange(propertyInfo.Name);
            else
                foreach(var propertyName in propertyNames)
                    this.NotifyOfPropertyChange(propertyName);
        }

        public string DateTimeRangeBox_To {
            get {
                return Resources.ControlUIStrings.DateTimeRangeBox_To;
            }
        }

    }
}
