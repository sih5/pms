﻿using System;

namespace Sunlight.Silverlight.Core {
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public sealed class InjectionRelatedAttribute : Attribute {
        //
    }
}
