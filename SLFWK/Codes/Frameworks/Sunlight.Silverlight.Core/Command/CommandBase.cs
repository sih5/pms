﻿using System;
using System.Windows.Input;

namespace Sunlight.Silverlight.Core.Command {
    /// <summary>
    ///     各 ViewModel 中所使用的 Command 的基类。
    /// </summary>
    public abstract class CommandBase : ICommand {
        /// <inheritdoc />
        public event EventHandler CanExecuteChanged;

        void ICommand.Execute(object parameter) {
            this.Execute(parameter);
        }

        bool ICommand.CanExecute(object parameter) {
            return this.CanExecute(parameter);
        }

        /// <summary>
        ///     继承者可以调用此函数以触发 <see cref="CanExecuteChanged" /> 事件。
        /// </summary>
        public void RaiseCanExecuteChanged() {
            var handler = this.CanExecuteChanged;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        /// <inheritdoc />
        protected abstract bool CanExecute(object parameter);

        /// <inheritdoc />
        protected abstract void Execute(object parameter);
    }
}
