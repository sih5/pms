﻿using System;

namespace Sunlight.Silverlight.Core.Command {
    public abstract class DelegateCommandBase : CommandBase {
        private readonly Action<object> executeMethod;
        private readonly Func<object, bool> canExecuteMethod;
        private bool isActive;
        public virtual event EventHandler IsActiveChanged;

        public bool IsActive {
            get {
                return this.isActive;
            }
            set {
                if(this.isActive == value)
                    return;
                this.isActive = value;
                this.OnIsActiveChanged();
            }
        }

        protected DelegateCommandBase(Action<object> executeMethod, Func<object, bool> canExecuteMethod) {
            if(executeMethod == null || canExecuteMethod == null)
                throw new ArgumentNullException("executeMethod");

            this.executeMethod = executeMethod;
            this.canExecuteMethod = canExecuteMethod;
        }

        protected virtual void OnIsActiveChanged() {
            var isActiveChangedHandler = this.IsActiveChanged;
            if(isActiveChangedHandler != null)
                isActiveChangedHandler(this, EventArgs.Empty);
        }

        protected override void Execute(object parameter) {
            this.executeMethod(parameter);
        }

        protected override bool CanExecute(object parameter) {
            return this.canExecuteMethod == null || this.canExecuteMethod(parameter);
        }
    }

    public class DelegateCommand : DelegateCommandBase {
        public DelegateCommand(Action executeMethod) : this(executeMethod, () => true) {
        }

        public DelegateCommand(Action executeMethod, Func<bool> canExecuteMethod) : base(o => executeMethod(), o => canExecuteMethod()) {
            if(executeMethod == null)
                throw new ArgumentNullException("executeMethod");
            if(canExecuteMethod == null)
                throw new ArgumentNullException("canExecuteMethod");
        }

        public void Execute() {
            this.Execute(null);
        }

        public bool CanExecute() {
            return this.CanExecute(null);
        }
    }

    /// <summary>
    ///     通过继承此类，可以实现一个简单的命令对象。
    /// </summary>
    /// <typeparam name="T"> 该命令执行时所需参数的类型。 </typeparam>
    public class DelegateCommand<T> : DelegateCommandBase {
        /// <param name="executeMethod"> 包含命令所执行的内容的 <see cref="Action{T}" /> 。 </param>
        public DelegateCommand(Action<T> executeMethod) : this(executeMethod, o => true) {
        }

        /// <param name="executeMethod"> 包含命令所执行的内容的 <see cref="Action{T}" /> 。 </param>
        /// <param name="canExecuteMethod"> 包含判断命令是否可执行的 <see cref="Func{T1,T2}" /> 。 </param>
        public DelegateCommand(Action<T> executeMethod, Func<T, bool> canExecuteMethod) : base(o => executeMethod((T)o), o => canExecuteMethod((T)o)) {
            if(executeMethod == null)
                throw new ArgumentNullException("executeMethod");
            if(canExecuteMethod == null)
                throw new ArgumentNullException("canExecuteMethod");

            var genericType = typeof(T);

            if(!genericType.IsValueType)
                return;
            if((!genericType.IsGenericType) || (!typeof(Nullable<>).IsAssignableFrom(genericType.GetGenericTypeDefinition())))
                throw new InvalidCastException();
        }

        /// <inheritdoc />
        public bool CanExecute(T parameter) {
            return base.CanExecute(parameter);
        }

        /// <inheritdoc />
        public void Execute(T parameter) {
            base.Execute(parameter);
        }
    }
}
