﻿using System.Collections.Generic;

namespace Sunlight.Silverlight.Core {
    public interface IFilterPanel {
        IDictionary<string, object> GetValues();
        void SetValues(IDictionary<string, object> values);
    }
}
