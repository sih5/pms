﻿using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Core {
    public interface IFilterPanelControl {
        FilterItem GetFilter();

        object GetFilterValue();

        void SetFilterValue(object value);
    }
}
