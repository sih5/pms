﻿using System.Windows;

namespace Sunlight.Silverlight.Core {
    /// <summary>
    ///     定义用于提供数据表格控件清单面板模板的类的协定。
    /// </summary>
    public interface IRowDetailsTemplateProvider {
        /// <summary>
        ///     调用此方法返回清单界面的 <c>DataTemplate</c> 对象。
        /// </summary>
        /// <returns> 清单界面的模板类实例。 </returns>
        DataTemplate CreateTemplate();

        /// <summary>
        ///     在模板被创建时，该方法负责对模板的内容进行初始化。
        /// </summary>
        /// <param name="rootElement"> 模板中的根节点对象。 </param>
        void OnInitializing(FrameworkElement rootElement);
    }
}
