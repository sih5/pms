﻿using System;

namespace Sunlight.Silverlight.Core {
    public interface ISecurityActionResult : IAsyncResult {
        bool IsSuccess {
            get;
        }

        Exception Exception {
            get;
        }

        bool IsErrorHandled {
            get;
            set;
        }

        string ErrorMessage {
            get;
            set;
        }
        int? EnterpriseCategoryId {
            get;
            set;
        }
        string LoginId {
            get;
            set;
        }
        object this[string propertyName] {
            get;
        }

        bool? IsIdm {
            get;
            set;
        }
    }
}
