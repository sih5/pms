﻿using System;
using System.Collections.Generic;
using System.ServiceModel.DomainServices.Client;
using System.ServiceModel.DomainServices.Client.ApplicationServices;

namespace Sunlight.Silverlight.Core {
    /// <summary>
    ///     权限子系统进行嵌入的接口，对系统内所需的权限功能进行协定。
    /// </summary>
    public interface ISecurityManager {
        /// <summary>
        ///     获取当前权限系统所采用的 <see cref="AuthenticationService" /> 。
        /// </summary>
        /// <returns> 当前权限系统所采用的 <see cref="AuthenticationService" /> 实例。 </returns>
        AuthenticationService AuthenticationService {
            get;
        }

        /// <summary>
        ///     使用指定的用户身份信息进行登录。
        /// </summary>
        /// <param name="userId"> 用户的ID。 </param>
        /// <param name="password"> 用户的密码。 </param>
        /// <param name="isPersistent"> 是否在关闭浏览器后依然保存用户的认证信息。 </param>
        /// <param name="parameters"> 额外的登录信息。 </param>
        /// <param name="callback"> 当异步操作完成时，实现者应调用该回调方法以通知调用方。 </param>
        void Login(string userId, string password, bool isPersistent, IEnumerable<KeyValuePair<string, string>> parameters, AsyncCallback callback);

        /// <summary>
        ///     注销当前已登录用户。
        /// </summary>
        /// <param name="callback"> 当异步操作完成时，实现者应调用该回调方法以通知调用方。 </param>
        void Logout(AsyncCallback callback);

        /// <summary>
        ///     获取当前登录用户可访问的菜单项数据。
        /// </summary>
        /// <param name="cultureName">所需获取菜单项数据的语言标识 </param>
        /// <param name="callback"> 当异步操作完成时，实现者应调用该回调方法以通知调用方。 </param>
        /// <param name="domainContext"> 如果指定了该参数，则该操作会使用指定的 <see cref="DomainContext" /> 进行操作。 </param>
        void GetMenuXml(string cultureName, AsyncCallback callback, DomainContext domainContext = null);

        /// <summary>
        ///     获取当前登录用户在某个页面可访问的操作按钮项。
        /// </summary>
        /// <param name="pageId"> 页面的ID。 </param>
        /// <param name="callback"> 当异步操作完成时，实现者应调用该回调方法以通知调用方。 </param>
        /// <param name="domainContext"> 如果指定了该参数，则该操作会使用指定的 <see cref="DomainContext" /> 进行操作。 </param>
        void GetPageActions(int pageId, AsyncCallback callback, DomainContext domainContext = null);

        /// <summary>
        ///     当前登录用户收藏或取消收藏某个页面。
        /// </summary>
        /// <param name="pageId"> 页面的ID。 </param>
        /// <param name="isFavorite"> 若为true，则收藏该页面；若为false，则取消收藏该页面。</param>
        /// <param name="callback"> 当异步操作完成时，实现者应调用该回调方法以通知调用方。 </param>
        /// <param name="domainContext"> 如果指定了该参数，则该操作会使用指定的 <see cref="DomainContext" /> 进行操作。 </param>        
        void SetFavoritePage(int pageId, bool isFavorite, AsyncCallback callback, DomainContext domainContext = null);

        /// <summary>
        ///     当前登录用户调整收藏的页面显示顺序。
        /// </summary>
        /// <param name="pagesOrder">页面的ID和显示顺序。</param>
        /// <param name="callback"> 当异步操作完成时，实现者应调用该回调方法以通知调用方。 </param>
        /// <param name="domainContext"> 如果指定了该参数，则该操作会使用指定的 <see cref="DomainContext" /> 进行操作。 </param>
        void ChangeFavoritePagesOrder(Dictionary<int, int> pagesOrder, AsyncCallback callback, DomainContext domainContext = null);
    }
}
