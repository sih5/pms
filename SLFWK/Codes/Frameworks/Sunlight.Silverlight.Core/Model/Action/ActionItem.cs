﻿using System;

namespace Sunlight.Silverlight.Core.Model {
    public class ActionItem {
        public string UniqueId {
            get;
            set;
        }

        public string Title {
            get;
            set;
        }

        public Uri ImageUri {
            get;
            set;
        }

        public bool CanExecute {
            get;
            set;
        }

        public ActionItem() {
            this.CanExecute = true;
        }
    }
}
