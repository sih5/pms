﻿using System.Collections.Generic;

namespace Sunlight.Silverlight.Core.Model {
    public class ActionItemGroup {
        public string UniqueId {
            get;
            set;
        }

        public string Title {
            get;
            set;
        }

        public BuiltInTabs PlaceOn {
            get;
            set;
        }

        public IEnumerable<ActionItem> ActionItems {
            get;
            set;
        }

        public ActionItemGroup() {
            this.PlaceOn = BuiltInTabs.Action;
        }
    }
}
