﻿using System;
using System.Windows.Input;

namespace Sunlight.Silverlight.Core.Model {
    public class ButtonItem : ModelBase {
        private Uri icon;
        private string title;
        private ICommand command;
        private Action action;
        private object commandParameter;

        public Uri Icon {
            get {
                return this.icon;
            }
            set {
                if(this.icon == value)
                    return;
                this.icon = value;
                this.NotifyOfPropertyChange("Icon");
            }
        }

        public string Title {
            get {
                return this.title;
            }
            set {
                if(this.title == value)
                    return;
                this.title = value;
                this.NotifyOfPropertyChange("Title");
            }
        }

        public ICommand Command {
            get {
                return this.command;
            }
            set {
                if(this.command == value)
                    return;
                this.command = value;
                this.NotifyOfPropertyChange("Command");
            }
        }

        public object CommandParameter {
            get {
                return this.commandParameter;
            }
            set {
                if(this.commandParameter == value)
                    return;
                this.commandParameter = value;
                this.NotifyOfPropertyChange("CommandParameter");
            }
        }

        public Action Action {
            get {
                return this.action;
            }
            set {
                if(this.action == value)
                    return;
                this.action = value;
                this.NotifyOfPropertyChange("Action");
            }
        }
    }
}
