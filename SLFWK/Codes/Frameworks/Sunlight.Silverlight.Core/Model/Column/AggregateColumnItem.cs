﻿namespace Sunlight.Silverlight.Core.Model {
    /// <summary>
    ///     聚合函数的类型。
    /// </summary>
    public enum AggregateType {
        /// <summary>
        ///     计数。
        /// </summary>
        Count,
        /// <summary>
        ///     总和。
        /// </summary>
        Sum,
        /// <summary>
        ///     最小值。
        /// </summary>
        Min,
        /// <summary>
        ///     最大值。
        /// </summary>
        Max,
        /// <summary>
        ///     平均值。
        /// </summary>
        Average,
    }

    public class AggregateColumnItem : ColumnItem {
        /// <summary>
        ///     获取或设置一个值，指定该列所使用的聚合函数。
        /// </summary>
        public AggregateType AggregateType {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置该列聚合值的标题。如果值为 null 则使用默认标题。
        /// </summary>
        public string AggregateCaption {
            get;
            set;
        }

        public AggregateColumnItem() {
            this.AggregateType = AggregateType.Count;
        }
    }
}
