﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;

namespace Sunlight.Silverlight.Core.Model {
    /// <summary>
    ///     数据表格列的 Model。
    /// </summary>
    public class ColumnItem {
        /// <summary>
        ///     获取或设置所绑定的列名，必须和 Entity 中的属性名一致，包括大小写。
        /// </summary>
        public string Name {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置列的自定义标题，如果留空，则采用默认的标题。
        /// </summary>
        public string Title {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置该列是否为只读，只读的列不允许编辑。
        /// </summary>
        public bool IsReadOnly {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置该列是否显示唯一值过滤界面。
        /// </summary>
        public bool ShowDistinctFilters {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置该列是否可排序。
        /// </summary>
        public bool IsSortable {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置该列的默认排序方向。
        /// </summary>
        public bool? IsSortDescending {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置该列是否可分组。
        /// </summary>
        public bool IsGroupable {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置一个值，指定在该列允许分组的情况下，该列是否是查询后的默认分组列。
        /// </summary>
        public bool IsDefaultGroup {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置该列的在分组排列中的位置，数值越大越靠后。
        /// </summary>
        public int DefaultGroupPosition {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置该列的默认分组排序方向。
        /// </summary>
        public bool? IsGroupSortDescending {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置该列是否在分组时隐藏。
        /// </summary>
        public bool ShowColumnWhenGrouped {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置一个值，对应实体的某个属性名，框架会使用该属性在该列上显示其指定的图标。
        /// </summary>
        public string IconPropertyName {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置一个值，指定该列单元格内文字的对齐方向。
        /// </summary>
        /// <remarks>
        ///     注意：请不要为该属性赋值 <c>TextAlignment.Justify</c> 。
        /// </remarks>
        public TextAlignment TextAlignment {
            get;
            set;
        }

        public MaskType MaskType {
            get;
            set;
        }

        public string FormatString {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置该列的清单列。只有在该列为清单导航属性时才有效。
        /// </summary>
        public IEnumerable<ColumnItem> SubColumns {
            get;
            set;
        }

        public Binding IsReadOnlyBinding {
            get;
            set;
        }

        /// <inheritdoc />
        public ColumnItem() {
            this.TextAlignment = TextAlignment.Left;
            this.ShowDistinctFilters = true;
            this.IsSortable = true;
            this.IsGroupable = true;
        }
    }
}
