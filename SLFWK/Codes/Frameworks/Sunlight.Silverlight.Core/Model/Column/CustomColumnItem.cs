﻿using System;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Core.Model {
    /// <summary>
    ///     自定义的数据表格列。
    /// </summary>
    public class CustomColumnItem : ColumnItem {
        /// <summary>
        ///     用于创建自定义列实例的函数。
        /// </summary>
        public Func<GridViewBoundColumnBase> ColumnFactory {
            get;
            set;
        }
    }
}
