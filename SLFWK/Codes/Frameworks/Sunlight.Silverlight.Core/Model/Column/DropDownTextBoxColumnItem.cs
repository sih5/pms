﻿using System.Windows;

namespace Sunlight.Silverlight.Core.Model {
    /// <summary>
    ///     呈现下拉文本框的表格列配置项。
    /// </summary>
    public class DropDownTextBoxColumnItem : ColumnItem {
        /// <summary>
        ///     下拉项的内容。
        /// </summary>
        public UIElement DropDownContent {
            get;
            set;
        }

        /// <summary>
        ///     允许在编辑框内输入文本
        /// </summary>
        public bool IsEditable {
            get;
            set;
        }

        /// <summary>
        ///     默认可在编辑框内输入文本。
        /// </summary>
        public DropDownTextBoxColumnItem() {
            this.IsEditable = true;
        }
    }
}
