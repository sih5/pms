﻿using System.Collections.ObjectModel;

namespace Sunlight.Silverlight.Core.Model {
    public class KeyValuesColumnItem : ColumnItem {
        /// <summary>
        ///     获取或设置该列所关联的 <see cref="KeyValuePair" /> 集合。
        /// </summary>
        public ObservableCollection<KeyValuePair> KeyValueItems {
            get;
            set;
        }
    }
}
