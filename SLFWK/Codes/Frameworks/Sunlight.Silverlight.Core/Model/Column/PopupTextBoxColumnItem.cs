﻿using System.Windows;

namespace Sunlight.Silverlight.Core.Model {

    public class PopupTextBoxColumnItem : ColumnItem {
        /// <summary>
        ///     下拉项的内容。
        /// </summary>
        public UIElement DropDownContent {
            get;
            set;
        }

        /// <summary>
        ///     允许在编辑框内输入文本
        /// </summary>
        public bool IsEditable {
            get;
            set;
        }

        /// <summary>
        ///     弹出窗体title
        /// </summary>
        public string PopupTitle {
            get;
            set;
        }

        /// <summary>
        ///     默认可在编辑框内输入文本。
        /// </summary>
        public PopupTextBoxColumnItem() {
            this.IsEditable = true;
            PopupTitle = "查询";
        }
    }
}
