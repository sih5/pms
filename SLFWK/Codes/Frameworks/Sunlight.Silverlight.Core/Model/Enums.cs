﻿using System;

namespace Sunlight.Silverlight.Core.Model {
    [Flags]
    public enum BuiltInTabs {
        Query = 1,
        Action = 2,
        All = 3,
    }

    public enum MaskType {
        DateTime = 1,
        Numeric = 2,
        Standard = 3,
    }

    /// <summary>
    /// 用户在输入查询条件的值后，值自动转换大小写
    /// </summary>
    public enum CharacterCasing {
        Normal,
        Lower,
        Upper
    }
}
