﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Core.Model {
    public enum LogicalOperator {
        And,
        Or,
    }

    public class CompositeFilterItem : FilterItem {
        private class DictionaryComparer : IEqualityComparer<KeyValuePair<string, object>> {
            public bool Equals(KeyValuePair<string, object> x, KeyValuePair<string, object> y) {
                return x.Key == y.Key;
            }

            public int GetHashCode(KeyValuePair<string, object> obj) {
                return obj.Key.GetHashCode();
            }
        }

        private List<FilterItem> filters;

        public override bool IsEmpty {
            get {
                return this.Filters.All(f => f.IsEmpty);
            }
        }

        public LogicalOperator LogicalOperator {
            get;
            set;
        }

        public ICollection<FilterItem> Filters {
            get {
                return this.filters ?? (this.filters = new List<FilterItem>());
            }
        }

        public CompositeFilterItem() {
            this.LogicalOperator = LogicalOperator.And;
        }

        public override IDictionary<string, object> ToQueryParameters() {
            if(this.Filters.Count == 0)
                return null;

            if(this.Filters.Count == 2 && this.Filters.GroupBy(filter => filter.MemberName).Count() == 1) {
                var key = this.Filters.First().MemberName;
                if(this.Filters.All(filter => filter.MemberType == typeof(DateTime))) {
                    var min = this.Filters.Min(filter => (DateTime)filter.Value);
                    var max = this.Filters.Max(filter => (DateTime)filter.Value);
                    return new Dictionary<string, object>(1) {
                        {
                            key, new[] {
                                min == DateTime.MinValue ? (DateTime?)null : min, max == DateTime.MaxValue || max == DateTime.MaxValue.Date ? (DateTime?)null : max
                            }
                            },
                    };
                }
            }

            var comparer = new DictionaryComparer();
            var result = this.Filters.Aggregate(new Dictionary<string, object>(0), (current, filterItem) => current.Union(filterItem.ToQueryParameters(), comparer).ToDictionary(kv => kv.Key, kv => kv.Value));
            return result.Count == 0 ? null : result;
        }

        public override IFilterDescriptor ToFilterDescriptor() {
            var filterDescriptor = new CompositeFilterDescriptor {
                LogicalOperator = this.LogicalOperator == LogicalOperator.And ? FilterCompositionLogicalOperator.And : FilterCompositionLogicalOperator.Or
            };
            filterDescriptor.FilterDescriptors.AddRange(this.Filters.SelectMany(f => f.ToFilterDescriptors()));
            return filterDescriptor;
        }

        public override IEnumerable<IFilterDescriptor> ToFilterDescriptors() {
            var filterDescriptor = new CompositeFilterDescriptor {
                LogicalOperator = this.LogicalOperator == LogicalOperator.And ? FilterCompositionLogicalOperator.And : FilterCompositionLogicalOperator.Or
            };
            filterDescriptor.FilterDescriptors.AddRange(this.Filters.SelectMany(f => f.ToFilterDescriptors()));
            return new[] {
                filterDescriptor
            };
        }

        public override void ApplyToDomainDataSourceParameters(QueryParameterCollection queryParameters) {
            foreach(var filterItem in this.Filters)
                filterItem.ApplyToDomainDataSourceParameters(queryParameters);
        }
    }
}
