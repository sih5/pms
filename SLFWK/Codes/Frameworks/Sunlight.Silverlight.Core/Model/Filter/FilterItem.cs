﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Core.Model {
    public enum FilterOperator {
        IsLessThan = 0,
        IsLessThanOrEqualTo = 1,
        IsEqualTo = 2,
        IsNotEqualTo = 3,
        IsGreaterThanOrEqualTo = 4,
        IsGreaterThan = 5,
        StartsWith = 6,
        EndsWith = 7,
        Contains = 8,
        DoesNotContain = 9,
        IsContainedIn = 10,
        IsNotContainedIn = 11,
    }

    public class FilterItem {
        internal string ParameterName {
            get;
            set;
        }

        public bool IsCaseSensitive {
            get;
            set;
        }

        public bool IsExact {
            get;
            set;
        }

        public string MemberName {
            get;
            set;
        }

        public Type MemberType {
            get;
            set;
        }

        public FilterOperator Operator {
            get;
            set;
        }

        public object Value {
            get;
            set;
        }

        public object NullValue {
            get;
            set;
        }

        public virtual bool IsEmpty {
            get {
                return this.Value == this.NullValue;
            }
        }

        public FilterItem() {

        }

        public FilterItem(string memberName, Type memberType, FilterOperator filterOperator, object value) {
            this.MemberName = memberName;
            this.MemberType = memberType;
            this.Operator = filterOperator;
            this.Value = value;
        }

        public virtual IDictionary<string, object> ToQueryParameters() {
            return string.IsNullOrEmpty(this.MemberName) ? null : new Dictionary<string, object> {
                { this.MemberName, this.Value }
            };
        }

        public virtual IFilterDescriptor ToFilterDescriptor() {
            if(this.IsEmpty)
                return null;

            if(this.MemberType == typeof(string))
                return new FilterDescriptor(this.MemberName, this.IsExact ? Telerik.Windows.Data.FilterOperator.IsEqualTo : Telerik.Windows.Data.FilterOperator.Contains, this.Value) {
                    MemberType = this.MemberType,
                    IsCaseSensitive = this.IsCaseSensitive,
                };

            return new FilterDescriptor(this.MemberName, (Telerik.Windows.Data.FilterOperator)this.Operator, this.Value) {
                MemberType = this.MemberType,
                IsCaseSensitive = this.IsCaseSensitive,
            };
        }

        [Obsolete("使用 ToFilterDescriptor 替代本方法")]
        public virtual IEnumerable<IFilterDescriptor> ToFilterDescriptors() {
            if(this.IsEmpty)
                return Enumerable.Empty<IFilterDescriptor>();

            if(this.MemberType == typeof(string))
                return new[] {
                    new FilterDescriptor(this.MemberName, this.IsExact ? Telerik.Windows.Data.FilterOperator.IsEqualTo : Telerik.Windows.Data.FilterOperator.Contains, this.Value) {
                        MemberType = this.MemberType,
                        IsCaseSensitive = this.IsCaseSensitive,
                    }
                };

            return new[] {
                new FilterDescriptor(this.MemberName, (Telerik.Windows.Data.FilterOperator)this.Operator, this.Value) {
                    MemberType = this.MemberType,
                    IsCaseSensitive = this.IsCaseSensitive,
                }
            };
        }

        public virtual void ApplyToDomainDataSourceParameters(QueryParameterCollection queryParameters) {
            if(queryParameters == null)
                throw new ArgumentNullException("queryParameters");

            var queryParameter = queryParameters.SingleOrDefault(p => p.ParameterName == this.ParameterName);
            if(queryParameter == null)
                queryParameters.Add(new QueryParameter {
                    ParameterName = this.ParameterName,
                    Value = this.Value,
                });
            else
                queryParameter.Value = this.Value;
        }
    }
}
