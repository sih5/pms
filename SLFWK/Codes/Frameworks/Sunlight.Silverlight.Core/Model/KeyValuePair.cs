﻿namespace Sunlight.Silverlight.Core.Model {
    /// <summary>
    ///     字典键值项的 Model。
    /// </summary>
    public class KeyValuePair {
        /// <summary>
        ///     获取或设置该字典键值项的键。
        /// </summary>
        public int Key {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置该字典键值项的值。
        /// </summary>
        public string Value {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置用户自定义数据。
        /// </summary>
        public object UserObject {
            get;
            set;
        }
    }
}
