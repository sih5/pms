﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;

namespace Sunlight.Silverlight.Core.Model {
    /// <summary>
    ///     Model 的基类，主要提供 <see cref="INotifyPropertyChanged" /> 接口的实现。
    /// </summary>
    public abstract class ModelBase : INotifyPropertyChanged {
        private static MemberInfo GetMemberInfo(Expression expression) {
            MemberExpression memberExpression;
            var lambda = (LambdaExpression)expression;
            if(lambda.Body is UnaryExpression) {
                var unaryExpression = (UnaryExpression)lambda.Body;
                memberExpression = (MemberExpression)unaryExpression.Operand;
            } else
                memberExpression = (MemberExpression)lambda.Body;
            return memberExpression.Member;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///     通知监听者某个属性的值已经更改。
        /// </summary>
        /// <param name="propertyName"> 所更改的属性的名称。 </param>
        public virtual void NotifyOfPropertyChange(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        ///     通知监听者某个属性的值已经更改。该方法以属性泛型形式进行调用，可以获得在编译时检查属性名称是否正确的好处。
        /// </summary>
        /// <typeparam name="TProperty"> 属性本身的类型。 </typeparam>
        /// <param name="property"> 所更改的属性。 </param>
        public virtual void NotifyOfPropertyChange<TProperty>(Expression<Func<TProperty>> property) {
            this.NotifyOfPropertyChange(GetMemberInfo(property).Name);
        }

        /// <summary>
        ///     通知监听者多个属性的值已经更改。
        /// </summary>
        /// <param name="propertyNames"> 所更改的属性的名称列表。 </param>
        public virtual void NotifyOfPropertiesChange(params string[] propertyNames) {
            var handler = this.PropertyChanged;
            if(handler != null)
                foreach(var propertyName in propertyNames)
                    handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
