﻿using System;

namespace Sunlight.Silverlight.Core.Model {
    /// <summary>
    ///     自定义清单面板的 Model。
    /// </summary>
    public class CustomPanelItem : PanelItem {
        /// <summary>
        ///     获取或设置面板内容的类型，框架会自动创建该类型的实例并显示在面板上。
        /// </summary>
        public Type ContentType {
            get;
            set;
        }
    }
}
