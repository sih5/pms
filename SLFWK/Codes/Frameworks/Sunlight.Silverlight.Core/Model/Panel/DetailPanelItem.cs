﻿using System;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Core.Model {
    /// <summary>
    ///     清单面板项的 Model。
    /// </summary>
    public class DetailPanelItem : PanelItem {
        /// <summary>
        ///     获取或设置一个值，指明清单表格是否允许编辑。
        /// </summary>
        public bool IsReadOnly {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置一个值，指定清单是否采用“即需即载”模式。
        /// </summary>
        public bool FetchOnDemand {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置一个值，指定清单的列配置。
        /// </summary>
        public ColumnItem Column {
            get;
            set;
        }

        /// <summary>
        ///     获取或者设置用于获取清单数据的方法。
        /// </summary>
        public Action<Entity> Fetch {
            get;
            set;
        }

        /// <summary>
        ///     默认该清单表格不允许编辑。
        /// </summary>
        public DetailPanelItem() {
            this.IsReadOnly = true;
        }
    }
}
