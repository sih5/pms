﻿using System;

namespace Sunlight.Silverlight.Core.Model {
    /// <summary>
    ///     数据表格面板项的 Model。
    /// </summary>
    public class PanelItem {
        /// <summary>
        ///     获取或设置该对象的唯一Id。
        /// </summary>
        public string UniqueId {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置清单面板的Tab标题。
        /// </summary>
        public string Header {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置清单面板Tab上的图标 <c>Uri</c> 。
        /// </summary>
        public Uri ImageUri {
            get;
            set;
        }
    }
}
