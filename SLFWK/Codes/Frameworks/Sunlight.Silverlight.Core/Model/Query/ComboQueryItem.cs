﻿using System.Collections;
using System.Windows.Data;

namespace Sunlight.Silverlight.Core.Model {
    /// <summary>
    ///     ComboBox 式的查询控件配置类
    /// </summary>
    public class ComboQueryItem : QueryItem {
        /// <summary>
        ///     获取或设置数据来源
        /// </summary>
        public IEnumerable ItemsSource {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置当前选择项的绑定
        /// </summary>
        public Binding SelectedItemBinding {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置为当前选择项返回值的属性路径
        /// </summary>
        public string SelectedValuePath {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置为每个数据项显示的属性路径
        /// </summary>
        public string DisplayMemberPath {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置为每个数据项显示的属性路径
        /// </summary>
        public bool AllowMultiSelect {
            get;
            set;
        }
        public IList SelectedValues {
            get;
            set;
        }

    }
}
