﻿using System;
using System.Windows.Controls;

namespace Sunlight.Silverlight.Core.Model {
    public class CustomControlQueryItem : QueryItem {
        public Func<Type, Control> CreateControl {
            get;
            set;
        }
    }
}
