﻿using System;

namespace Sunlight.Silverlight.Core.Model {
    /// <summary>
    ///     自定义的查询条件类，用于扩展复杂的查询条件项。与 <see cref="QueryItem" /> 不同，本类无法与实体的某个属性进行对应。
    /// </summary>
    public class CustomQueryItem : QueryItem {
        /// <summary>
        ///     获取或设置该查询条件的值的类型。
        /// </summary>
        public Type DataType {
            get;
            set;
        }
    }
}
