﻿using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Calendar;

namespace Sunlight.Silverlight.Core.Model {
    public class DateTimeQueryItem : QueryItem {
        public InputMode InputMode {
            get;
            set;
        }
        /// <summary>
        /// 日期选择方式，如年、月、日，默认为日
        /// </summary>
        public DateSelectionMode DateSelectionMode {
            get;
            set;
        }

        public DateTimeQueryItem() {
            this.InputMode = InputMode.DatePicker;
            this.DateSelectionMode = DateSelectionMode.Day;
        }
    }
}
