﻿using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Core.Model {
    /// <summary>
    ///     日期范围查询控件配置类。
    /// </summary>
    public class DateTimeRangeQueryItem : QueryItem {
        public bool IsGreaterThanOrEqualToMinValue {
            get;
            set;
        }

        public bool IsLessThanOrEqualToMaxValue {
            get;
            set;
        }

        public InputMode InputMode {
            get;
            set;
        }

        public DateTimeRangeQueryItem() {
            this.IsGreaterThanOrEqualToMinValue = true;
            this.IsLessThanOrEqualToMaxValue = true;
            this.InputMode = InputMode.DatePicker;
        }
    }
}
