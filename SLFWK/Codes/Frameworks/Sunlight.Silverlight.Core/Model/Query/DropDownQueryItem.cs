﻿using System;
using System.Windows;

namespace Sunlight.Silverlight.Core.Model {
    /// <summary>
    ///     下拉文本框式的查询控件配置类。
    /// </summary>
    public class DropDownQueryItem : QueryItem {
        /// <summary>
        ///     设置当下拉框弹出时是否对背景进行虚化。
        /// </summary>
        public bool UseBlurEffect {
            get;
            set;
        }

        /// <summary>
        ///     下拉框中的控件内容。
        /// </summary>
        public UIElement DropDownContent {
            get;
            set;
        }

        /// <summary>
        ///     开发人员应调用该属性主动设置下拉框的打开状态。
        /// </summary>
        public Action<bool> SetDropDownOpen {
            get;
            internal set;
        }

        /// <summary>
        ///     开发人员应调用该属性主动设置文本框中的文本。
        /// </summary>
        public Action<string> SetText {
            get;
            internal set;
        }

        /// <summary>
        ///     框架会调用该属性获取文本框中应该显示的文本。
        /// </summary>
        public Func<object, string> GetText {
            get;
            set;
        }

        /// <summary>
        ///     框架会调用该属性以获取查询时所使用的值。
        /// </summary>
        public Func<object> GetQueryValue {
            get;
            set;
        }
    }
}
