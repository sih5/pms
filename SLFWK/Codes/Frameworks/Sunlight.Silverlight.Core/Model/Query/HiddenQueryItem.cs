﻿using System;

namespace Sunlight.Silverlight.Core.Model {
    /// <summary>
    ///     在界面上不显示的查询项配置类。
    /// </summary>
    public class HiddenQueryItem : QueryItem {
        /// <summary>
        ///     框架会调用该属性获取查询的值。
        /// </summary>
        public Func<object> GetQueryValue {
            get;
            set;
        }
    }
}
