﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;

namespace Sunlight.Silverlight.Core.Model {
    /// <summary>
    ///     基于字典项的下拉列表形式的查询控件配置类。
    /// </summary>
    public class KeyValuesQueryExItem : QueryItem {
        /// <summary>
        /// 指定绑定的DisplayMemberPath 
        /// </summary>
        public string DisplayMemberPath {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置该查询条件所对应的字典项集合。
        /// </summary>
        /// <seealso cref="KeyValuePair" />
        public ObservableCollection<KeyValuePair> KeyValueItems {
            get;
            set;
        }
        /// <summary>
        /// 指定绑定的DataTemplate
        /// </summary>
        public DataTemplate DataTemplate {
            get;
            set;
        }
        /// <summary>
        /// 指定绑定的SelectDataTemplate
        /// </summary>
        public DataTemplate SelectDataTemplate {
            get;
            set;
        }
        /// <summary>
        /// 指定绑定的SelectedValuePath
        /// </summary>
        public string SelectedValuePath {
            get;
            set;
        }

        public KeyValuesQueryExItem() {
            SelectedValuePath = "KeyEx";
            DisplayMemberPath = "Value";
        }
        /// <summary>
        /// 显示清空选项
        /// </summary>
        public Visibility ClearSelectionButtonVisibility {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置为每个数据项显示的属性路径
        /// </summary>
        public bool AllowMultiSelect {
            get;
            set;
        }

        public List<string> DefaultValues {
            get;
            set;
        }
    }
}