﻿using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Core.Model {
    /// <summary>
    ///     日期范围查询控件配置类。
    /// </summary>
    public class NumericRangeQueryItem : QueryItem {
        public bool IsGreaterThanOrEqualToMinValue {
            get;
            set;
        }

        public bool IsLessThanOrEqualToMaxValue {
            get;
            set;
        }

        /// <summary>
        /// 数据显示样式 有 Decimal、Currency、Percent
        /// </summary>
        public ValueFormat ValueFormat {
            get;
            set;
        }

        /// <summary>
        /// 小数位数
        /// </summary>
        public int DecimalDigits {
            get;
            set;
        }

        public NumericRangeQueryItem() {
            this.IsGreaterThanOrEqualToMinValue = true;
            this.IsLessThanOrEqualToMaxValue = true;
            this.ValueFormat = ValueFormat.Numeric;
            // this.DecimalDigits = 0;
            //默认值赋值方法
            //DefaultValue = new[] {
            //    10, 20
            //};
        }
    }
}
