﻿using System;
using System.Windows;

namespace Sunlight.Silverlight.Core.Model {
    public class PopupQueryItem : QueryItem {
        /// <summary>
        ///     下拉框显示内容。
        /// </summary>
        public string Text {
            get;
            set;
        }
        /// <summary>
        ///     下拉框中的控件内容。
        /// </summary>
        public UIElement PopupContent {
            get;
            set;
        }
        /// <summary>
        ///     开发人员应调用该属性主动设置文本框中的文本。
        /// </summary>
        public Action<string> SetText {
            get;
            internal set;
        }

        /// <summary>
        ///     框架会调用该属性获取文本框中应该显示的文本。
        /// </summary>
        public Func<object, string> GetText {
            get;
            set;
        }
        /// <summary>
        ///     框架会调用该属性以获取查询时所使用的值。
        /// </summary>
        public Func<object> GetQueryValue {
            get;
            set;
        }
        public Type DataType {
            get;
            set;
        }
        /// <summary>
        /// 根据文本框中的内容是否在数据库中存在更改文本框样式
        /// </summary>
        public Action<string, Action<bool>> QueryExecute;
    }
}
