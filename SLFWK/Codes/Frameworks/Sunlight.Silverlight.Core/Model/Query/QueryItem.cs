﻿namespace Sunlight.Silverlight.Core.Model {
    /// <summary>
    ///     用于描述单一查询条件的类。
    /// </summary>
    public class QueryItem {
        private string parameterName, uniqueId;

        public string UniqueId {
            get {
                return this.uniqueId ?? this.ColumnName;
            }
            set {
                this.uniqueId = value;
            }
        }

        /// <summary>
        ///     获取或者设置该查询条件的显示标题。
        /// </summary>
        public string Title {
            get;
            set;
        }

        public string ColumnName {
            get;
            set;
        }

        public string ParameterName {
            get {
                return this.parameterName ?? Utils.MakeCompliantFieldName(this.ColumnName);
            }
            set {
                this.parameterName = value;
            }
        }

        /// <summary>
        ///     获取或设置该查询条件的初始默认值。
        /// </summary>
        public object DefaultValue {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置当该查询条件的值为空时，所使用的替代值。
        /// </summary>
        public object NullValue {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置该查询控件是否允许用户使用。
        /// </summary>
        public bool IsEnabled {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置该查询控件是否使用模糊查询。
        /// </summary>
        public bool IsExact {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置该查询控件是否大小写敏感。
        /// </summary>
        private bool isCaseSensitive = true;
        public bool IsCaseSensitive {
            get {
                return isCaseSensitive;
            }
            set {
                isCaseSensitive = value;
            }
        }

        /// <summary>
        ///     获取或设置该查询控件是否默认转成大写。
        /// </summary>
        private bool? isdefaultToUpper;

        public bool? IsDefaultToUpper {
            get {
                return isdefaultToUpper;
            }
            set {
                isdefaultToUpper = value;
            }
        }

        /// <summary>
        ///     获取该查询配置项所属的面板。该属性只有当面板生成后才有效。
        /// </summary>
        public IFilterPanel FilterPanel {
            get;
            internal set;
        }

        /// <inheritdoc />
        public QueryItem() {
            this.IsEnabled = true;
        }


        private bool isThreeState = true;
        /// <summary>
        /// 获取或设置指示控件是支持两种状态还是三种状态的值。
        /// </summary>
        /// 
        /// <returns>
        /// 如果控件支持三种状态，则为 true；否则为 false。默认值为 true。
        /// </returns>
        public bool IsThreeState {
            get {
                return this.isThreeState;
            }
            set {
                this.isThreeState = value;
            }
        }
        /// <summary>
        ///     获取或设置该查询控件将值自动转换为大写或小写。
        /// </summary>
        public CharacterCasing CharacterCasing {
            get;
            set;
        }

    }
}
