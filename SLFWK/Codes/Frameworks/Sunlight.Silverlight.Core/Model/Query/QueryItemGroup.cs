﻿using System;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Core.Model {
    public class QueryItemGroup {
        public string UniqueId {
            get;
            set;
        }

        public string Title {
            get;
            set;
        }

        public Type EntityType {
            get;
            set;
        }

        public IEnumerable<QueryItem> QueryItems {
            get;
            set;
        }
    }
}
