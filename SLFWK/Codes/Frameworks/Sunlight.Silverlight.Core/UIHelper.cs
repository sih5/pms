﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Sunlight.Silverlight.Core.Resources;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Core {
    public static class UIHelper {
        /// <summary>
        ///     在屏幕下半部显示一个信息框，该信息框不会阻碍用户的操作，并在指定的时间后消失。
        /// </summary>
        /// <param name="message"> 要显示的信息。 </param>
        /// <param name="duration"> 信息框存在的时间，以秒为单位。 </param>
        public static void ShowNotification(string message, int duration = 3) {
            var animationDuration = TimeSpan.FromMilliseconds(300);
            var sbToastShow = new Storyboard {
                AutoReverse = true,
                RepeatBehavior = new RepeatBehavior(1),
                Duration = TimeSpan.FromSeconds(duration / 2d) + animationDuration,
            };
            sbToastShow.Children.Add(new DoubleAnimation {
                AutoReverse = false,
                RepeatBehavior = new RepeatBehavior(1),
                Duration = animationDuration,
                From = 0,
                To = 1,
            });

            var toastContent = new Border {
                BorderThickness = new Thickness(2),
                BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0x5b, 0x5b, 0x5b)),
                Background = new SolidColorBrush(Color.FromArgb(0xDD, 0x66, 0x66, 0x66)),
                CornerRadius = new CornerRadius(5),
                IsHitTestVisible = false,
                Opacity = 0,
                Child = new TextBlock {
                    Margin = new Thickness(10),
                    Foreground = new SolidColorBrush(Colors.White),
                    FontFamily = new FontFamily("Microsoft YaHei"),
                    FontSize = 13,
                    Text = message,
                    TextWrapping = TextWrapping.Wrap,
                    IsHitTestVisible = false,
                },
            };
            Storyboard.SetTarget(sbToastShow, toastContent);
            Storyboard.SetTargetProperty(sbToastShow, new PropertyPath("Opacity"));

            var toast = new Popup {
                IsHitTestVisible = false,
                Child = toastContent,
                IsOpen = true,
                Tag = "TOAST",
            };
            Action relocation = () => {
                var appWidth = Application.Current.Host.Content.ActualWidth;
                var appHeight = Application.Current.Host.Content.ActualHeight;
                toast.MaxWidth = appWidth * 0.8;
                toast.HorizontalOffset = appWidth / 2 - toastContent.ActualWidth / 2;
                toast.VerticalOffset = appHeight - toastContent.ActualHeight - appHeight * 0.2;
            };
            relocation();
            toastContent.SizeChanged += (sender, e) => relocation();
            EventHandler resizedHandler = (sender, e) => relocation();
            Application.Current.Host.Content.Resized += resizedHandler;
            sbToastShow.Completed += (sender, e) => {
                Application.Current.Host.Content.Resized -= resizedHandler;
                toast.IsOpen = false;
            };

            var existedToasts = VisualTreeHelper.GetOpenPopups().Where(p => p != toast && p.Tag is string && (string)p.Tag == "TOAST").ToArray();
            foreach(var existedToast in existedToasts) {
                var sbMoveToast = new Storyboard {
                    AutoReverse = false,
                    RepeatBehavior = new RepeatBehavior(1),
                    Duration = animationDuration,
                };
                sbMoveToast.Children.Add(new DoubleAnimation {
                    AutoReverse = false,
                    RepeatBehavior = new RepeatBehavior(1),
                    Duration = animationDuration,
                    By = -50,
                });
                Storyboard.SetTarget(sbMoveToast, existedToast);
                Storyboard.SetTargetProperty(sbMoveToast, new PropertyPath("VerticalOffset"));
                sbToastShow.Children.Add(sbMoveToast);
            }

            sbToastShow.Begin();
        }

        public static void ShowAlertMessage(string message) {
            RadWindow.Alert(new DialogParameters {
                Header = CoreUIStrings.AlertWindow_Title,
                Content = new TextBox {
                    Text = message,
                    TextWrapping = TextWrapping.Wrap,
                    AcceptsReturn = true,
                    IsReadOnly = true,
                    MaxWidth = 400,
                    MaxHeight = 800,
                    BorderThickness = new Thickness(0)
                },
                OkButtonContent = CoreUIStrings.AlertWindow_ButtonText,
            });
        }

        public static void ShowAlertMessage(string message, Exception ex) {
            if(ex != null)
                message = string.Concat(message, Environment.NewLine, Environment.NewLine, ex.ToString());

            ShowAlertMessage(message);
        }
    }
}
