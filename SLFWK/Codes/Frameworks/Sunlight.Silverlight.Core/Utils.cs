﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Windows;

namespace Sunlight.Silverlight.Core {
    public static class Utils {
        internal static string MakeCompliantFieldName(string fieldName) {
            fieldName = fieldName.StartsWith("@", StringComparison.Ordinal) ? fieldName.Substring(1) : fieldName;
            var length = fieldName.Length;
            if(char.IsLower(fieldName[0]))
                return ("_" + fieldName);
            for(var i = 0; i < (length - 1); i++)
                if(char.IsLower(fieldName[i + 1])) {
                    if(i == 0)
                        i = 1;
                    return (fieldName.Substring(0, i).ToLowerInvariant() + fieldName.Substring(i));
                }
            return (fieldName.ToLowerInvariant());
        }

        /// <summary>
        ///     获取某个实体类型的某个字段的本地化资源名称。
        /// </summary>
        /// <param name="entityType"> 对应的实体类型。 </param>
        /// <param name="columnName"> 实体上的字段名称，大小写敏感。 </param>
        /// <returns> 返回该字段的本地化资源名称，如果本地化资源内未找到，则返回所传入的列名。 </returns>
        public static string GetEntityLocalizedName(Type entityType, string columnName) {
            if(entityType == null)
                throw new ArgumentNullException("entityType");
            if(columnName == null)
                throw new ArgumentNullException("columnName");
            var propertyInfo = entityType.GetProperty(columnName);
            if(propertyInfo == null)
                return columnName;
            var attributes = propertyInfo.GetCustomAttributes(typeof(DisplayAttribute), false);
            if(attributes.Length == 0)
                return columnName;
            var display = attributes[0] as DisplayAttribute;
            if(display == null || display.ResourceType == null)
                return columnName;
            propertyInfo = display.ResourceType.GetProperty(display.Name);
            if(propertyInfo == null)
                return columnName;
            return propertyInfo.GetValue(null, null) as string ?? columnName;
        }

        /// <summary>
        ///     将服务端上的相对路径转换为服务端上的绝对路径，通常用于客户端下载服务端文件。
        /// </summary>
        /// <param name="relativePath"> 服务端的相对路径，因为程序可能部署在虚拟目录下，所以不要以“/”符号开头。 </param>
        /// <returns> 与该相对路径所对应的绝对路径。 </returns>
        /// <exception cref="InvalidOperationException">当前应用程序没有运行于某个网站环境之下。</exception>
        public static Uri MakeServerUri(string relativePath) {
            if(string.IsNullOrEmpty(relativePath))
                return null;

            if(Application.Current.Host.Source == null)
                throw new InvalidOperationException();

            var uri = Application.Current.Host.Source.OriginalString;
            var index = uri.IndexOf("ClientBin/", StringComparison.InvariantCultureIgnoreCase);
            if(index > -1)
                uri = uri.Remove(index);
            else {
                var parts = uri.Split(new[] {
                    '/'
                }, StringSplitOptions.RemoveEmptyEntries);
                if(parts.Length > 0) {
                    index = uri.IndexOf(parts[parts.Length - 1], StringComparison.InvariantCultureIgnoreCase);
                    uri = uri.Remove(index);
                }
            }

            return new Uri(string.Concat(uri, relativePath.StartsWith("/") ? relativePath.Substring(1) : relativePath), UriKind.Absolute);
        }
    }
}
