﻿using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Core.View {
    /// <summary>
    ///     业务操作面板的协定。
    /// </summary>
    [InjectionRelated]
    public interface IActionPanel : IBaseView {
        /// <summary>
        ///     框架将依据该属性的内容生成业务操作面板。
        /// </summary>
        ActionItemGroup ActionItemGroup {
            get;
        }
    }
}
