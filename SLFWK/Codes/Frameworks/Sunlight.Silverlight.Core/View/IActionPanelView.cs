﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Core.View {
    public class ActionStateChangedEventArgs : EventArgs {
        public string GroupUniqueId {
            get;
            private set;
        }

        public string UniqueId {
            get;
            private set;
        }

        public bool CanExecute {
            get;
            set;
        }

        /// <inheritdoc />
        public ActionStateChangedEventArgs(string groupUniqueId, string uniqueId) {
            this.GroupUniqueId = groupUniqueId;
            this.UniqueId = uniqueId;
            this.CanExecute = true;
        }
    }

    /// <summary>
    ///     框架会对实现该接口的View自动呈现动作条面板，用户点击时会反馈给View。
    /// </summary>
    [Obsolete("该接口设计已过期，请将实现该接口的界面中业务操作面板相关的部分提取出来实现一个 ActionPanelBase 类。")]
    public interface IActionPanelView {
        /// <summary>
        ///     框架会监听此事件以判断动作按钮的状态，例如是否可用。实现者应该利用此事件实现通知。
        /// </summary>
        event EventHandler<ActionStateChangedEventArgs> ActionStateChanged;

        /// <summary>
        ///     框架会调用该属性获取动作条面板的按钮项目。
        /// </summary>
        IEnumerable<ActionItemGroup> ActionItemGroups {
            get;
        }

        /// <summary>
        ///     当用户点击了动作条上的按钮时，框架会调用该方法，实现者应该依据传入的按钮ID作出反应。
        /// </summary>
        /// <param name="actionItemGroup"> 用户点击的按钮所属的分组。 </param>
        /// <param name="uniqueId"> 用户点击的按钮的唯一ID。 </param>
        void ExecuteAction(ActionItemGroup actionItemGroup, string uniqueId);
    }
}
