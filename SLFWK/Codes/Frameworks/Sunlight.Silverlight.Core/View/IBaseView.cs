﻿namespace Sunlight.Silverlight.Core.View {
    /// <summary>
    ///     系统内所有受框架管理的界面的协定。
    /// </summary>
    [InjectionRelated]
    public interface IBaseView {
        /// <summary>
        ///     外部界面可以向该界面传递所协定的数据主题以及数据内容，并获取所需的数据内容。
        /// </summary>
        /// <param name="sender"> 向该界面发送数据的界面实例。 </param>
        /// <param name="subject"> 所协定的数据主题。 </param>
        /// <param name="contents"> 与该数据主题相关的数据内容。 </param>
        /// <returns> 该界面向调用界面返回的数据内容。 </returns>
        object ExchangeData(IBaseView sender, string subject, params object[] contents);
    }
}
