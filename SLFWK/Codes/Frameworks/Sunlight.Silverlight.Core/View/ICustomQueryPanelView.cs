﻿using System;

namespace Sunlight.Silverlight.Core.View {
    public interface ICustomQueryPanelView : IQueryPanelView {
        /// <summary>
        ///     获取查询按钮的图标位置，留空则采用默认值。
        /// </summary>
        Uri QueryButtonIcon {
            get;
        }

        /// <summary>
        ///     获取查询按钮下面的文字，留空则采用默认值。
        /// </summary>
        string QueryButtonText {
            get;
        }
    }
}
