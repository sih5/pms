﻿using System.Collections.Generic;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Core.View {
    public interface ICustomTabsView {
        bool IsTabsReady {
            get;
        }

        IEnumerable<RadRibbonTab> CustomTabs {
            get;
        }
    }
}
