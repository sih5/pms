﻿namespace Sunlight.Silverlight.Core.View {
    /// <summary>
    ///     数据编辑面板的协定。数据编辑面板通常只针对一个数据对象的单一实例进行编辑。
    /// </summary>
    [InjectionRelated]
    public interface IDataEditPanel {
    }
}
