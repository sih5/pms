﻿namespace Sunlight.Silverlight.Core.View {
    /// <summary>
    ///     数据编辑界面的协定。数据编辑界面是指通过一系列的控件绑定达到编辑数据功能的界面。
    /// </summary>
    [InjectionRelated]
    public interface IDataEditView : IBaseView {
    }
}
