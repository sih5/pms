﻿using System;
using System.Collections.Generic;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Core.View {
    /// <summary>
    ///     数据表格界面的协定。数据表格界面主要用来呈现数据集合，并对数据进行过滤。
    /// </summary>
    [InjectionRelated]
    public interface IDataGridView : IBaseView {
        /// <summary>
        ///     当数据表格的实体数据被载入后会触发该事件。
        /// </summary>
        /// <remarks>
        ///     当数据表格的实体数据被载入后，实现方应触发该事件以通知监听组件获取其数据。
        /// </remarks>
        event EventHandler DataLoaded;

        /// <summary>
        ///     获取数据表格中当前所拥有的实体对象。
        /// </summary>
        /// <remarks>
        ///     实现方应该保持该属性内容与数据表格内容同步，并在数据更改时触发 <c>DataLoaded</c> 事件。
        /// </remarks>
        IEnumerable<Entity> Entities {
            get;
        }

        /// <summary>
        ///     设置或者获取数据表格界面的过滤条件。
        /// </summary>
        /// <remarks>
        ///     实现方在获取数据时应使用该属性进行过滤，并确保 <c>Entities</c> 属性是该属性应用后的结果。
        /// </remarks>
        FilterItem FilterItem {
            get;
            set;
        }

        /// <summary>
        ///     获取一个值，指定外部组件当前是否可以调用 <c>ExecuteQuery</c> 方法。
        /// </summary>
        /// <remarks>
        ///     如果界面有需要异步初始化的内容，实现方可通过该属性通知外部组件，本数据表格界面是否可以正常查询。如果界面没有需要异步初始化的内容，实现方应该保持该属性永远返回 <c>true</c> 。
        /// </remarks>
        bool CanExecuteQuery {
            get;
        }

        /// <summary>
        ///     执行本数据表格界面的查询。
        /// </summary>
        /// <remarks>
        ///     如果当前 <c>CanExecuteQuery</c> 属性的值为 <c>false</c> ，建议实现方应忽略本次调用请求，而非抛出异常。
        /// </remarks>
        void ExecuteQuery();
    }
}
