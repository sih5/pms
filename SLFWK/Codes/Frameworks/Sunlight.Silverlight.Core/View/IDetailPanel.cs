﻿using System;

namespace Sunlight.Silverlight.Core.View {
    /// <summary>
    ///     清单面板的协定。清单面板往往跟数据表格控件同用，可以用于显示某数据对象的更多的详细信息。
    /// </summary>
    [InjectionRelated]
    public interface IDetailPanel {
        /// <summary>
        ///     清单面板的标题。
        /// </summary>
        /// <remarks>
        ///     该值应该进行本地化。
        /// </remarks>
        string Title {
            get;
        }

        /// <summary>
        ///     清单面板的图标，如果没有图标则为空。
        /// </summary>
        /// <remarks>
        ///     图标长宽大小应相等，建议最小大小为16x16，最大大小为48x48。
        /// </remarks>
        Uri Icon {
            get;
        }
    }
}
