﻿namespace Sunlight.Silverlight.Core.View {
    /// <summary>
    ///     基础的View接口。
    /// </summary>
    public interface IGeneralView {
        /// <summary>
        ///     获取该View的页面标题。
        /// </summary>
        string PageTitle {
            get;
        }
    }
}
