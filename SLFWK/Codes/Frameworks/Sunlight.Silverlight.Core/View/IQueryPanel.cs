﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Core.View {
    /// <summary>
    ///     查询面板的协定。
    /// </summary>
    [InjectionRelated]
    public interface IQueryPanel : IBaseView {
        /// <summary>
        ///     是否允许执行一次无过滤条件的查询。
        /// </summary>
        bool AcceptEmptyFilter {
            get;
            set;
        }

        /// <summary>
        ///     获取查询按钮的图标地址，留空则采用默认值。
        /// </summary>
        Uri ButtonIcon {
            get;
        }

        /// <summary>
        ///     获取查询按钮的文字，留空则采用默认值。
        /// </summary>
        string ButtonText {
            get;
        }

        /// <summary>
        ///     框架将依据该属性的内容生成查询面板。
        /// </summary>
        IEnumerable<QueryItemGroup> QueryItemGroups {
            get;
        }
    }
}
