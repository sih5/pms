﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Core.View {
    /// <summary>
    ///     框架会对实现该接口的View自动呈现查询面板，允许用户输入查询条件。
    /// </summary>
    [Obsolete("该接口设计已过期，请将实现该接口的界面中查询面板相关的部分提取出来实现一个 QueryPanelBase 类。")]
    public interface IQueryPanelView {
        /// <summary>
        ///     是否允许执行一次无过滤条件的查询。
        /// </summary>
        bool AcceptEmptyFilter {
            get;
        }

        /// <summary>
        ///     框架会调用该属性获取查询面板的查询项目。
        /// </summary>
        IEnumerable<QueryItemGroup> QueryItemGroups {
            get;
        }

        /// <summary>
        ///     当用户执行了查询时，框架会调用该方法，实现者应该依据传入的查询条件作出反应。
        /// </summary>
        /// <param name="queryItemGroup"> 用户本次查询所属的分组。 </param>
        /// <param name="filter"> 用户的查询条件所对应的查询项。 </param>
        void ExecuteQuery(QueryItemGroup queryItemGroup, FilterItem filter);
    }
}
