﻿namespace Sunlight.Silverlight.Core.View {
    /// <summary>
    ///     查询窗口的协定。
    /// </summary>
    [InjectionRelated]
    public interface IQueryWindow : IBaseView {
        /// <summary>
        ///     窗口上查询面板的DI键。
        /// </summary>
        string QueryPanelKey {
            get;
        }

        /// <summary>
        ///     窗口上数据表格的DI键。
        /// </summary>
        string DataGridViewKey {
            get;
        }
    }
}
