﻿namespace Sunlight.Silverlight.Core.View {
    /// <summary>
    ///     根界面的协定。根界面是指与系统菜单直接关联的界面。
    /// </summary>
    [InjectionRelated]
    public interface IRootView : IBaseView {
        /// <summary>
        ///     用于唯一标识该页面的值，在整个系统中不能重复。
        /// </summary>
        object UniqueId {
            get;
        }

        /// <summary>
        ///     该界面的标题，通常会显示在外壳界面上，供用户理解当前的界面的核心内容。
        /// </summary>
        /// <remarks>
        ///     该值应该进行本地化处理。
        /// </remarks>
        string Title {
            get;
        }
    }
}
