﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Core.ViewModel {
    /// <summary>
    ///     系统中全部 ViewModel 的基类，提供实现一个 ViewModel 所必需的一些基础功能。
    /// </summary>
    public abstract class ViewModelBase : ModelBase, INotifyDataErrorInfo {
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        private Dictionary<string, string> propertyErrors;

        private Dictionary<string, string> PropertyErrors {
            get {
                return this.propertyErrors ?? (this.propertyErrors = new Dictionary<string, string>());
            }
        }

        /// <inheritdoc />
        public bool HasErrors {
            get {
                return this.PropertyErrors.Count > 0;
            }
        }

        /// <summary>
        ///     为参数所指定的属性名触发 ErrorsChanged 事件。
        /// </summary>
        /// <param name="propertyNames"> 要对其触发错误更改通知的属性名称。 </param>
        protected virtual void RaiseErrorsChanged(params string[] propertyNames) {
            var handler = this.ErrorsChanged;
            if(handler != null)
                foreach(var propertyName in propertyNames)
                    handler(this, new DataErrorsChangedEventArgs(propertyName));
        }

        /// <summary>
        ///     为 ViewModel 上的某个属性设定验证错误信息，并通知 View。
        /// </summary>
        /// <param name="propertyName"> 属性名称。 </param>
        /// <param name="errorMessage"> 错误信息。 </param>
        protected virtual void SetPropertyError(string propertyName, string errorMessage) {
            if(this.PropertyErrors.ContainsKey(propertyName))
                this.PropertyErrors[propertyName] = errorMessage;
            else
                this.PropertyErrors.Add(propertyName, errorMessage);
            this.RaiseErrorsChanged(propertyName);
        }

        /// <summary>
        ///     移除 ViewModel 上的某个属性的验证错误信息，并通知 View。
        /// </summary>
        /// <param name="propertyName"> 属性名称。 </param>
        protected virtual void RemovePropertyError(string propertyName) {
            if(!this.PropertyErrors.ContainsKey(propertyName))
                return;
            this.PropertyErrors.Remove(propertyName);
            this.RaiseErrorsChanged(propertyName);
        }

        /// <summary>
        ///     移除 ViewModel 上的全部属性的验证错误信息，并通知 View。
        /// </summary>
        protected virtual void ResetPropertyErrors() {
            var propertyNames = this.PropertyErrors.Keys.ToArray();
            this.PropertyErrors.Clear();
            this.RaiseErrorsChanged(propertyNames);
        }

        /// <inheritdoc />
        public virtual IEnumerable GetErrors(string propertyName) {
            if(this.PropertyErrors.ContainsKey(propertyName))
                yield return this.PropertyErrors[propertyName];
        }

        /// <summary>
        ///     在具备数据验证功能的 View 里会调用该方法，实现者需要在方法内对 ViewModel 本身的数据进行验证，并通知 View。
        /// </summary>
        /// <seealso cref="SetPropertyError" />
        /// <seealso cref="RemovePropertyError" />
        /// <seealso cref="ResetPropertyErrors" />
        public abstract void Validate();
    }
}
