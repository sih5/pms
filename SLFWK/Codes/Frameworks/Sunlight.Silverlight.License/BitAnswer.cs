﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace Sunlight.Silverlight.License {
    internal class BitAnswerException : Exception {
        public int ErrorCode {
            get;
            set;
        }

        public BitAnswerException(int status) {
            this.ErrorCode = status;
        }

        public override string Message {
            get {
                switch(this.ErrorCode) {
                    case 257:
                        return "网络通讯错误，请检查服务器地址及端口设置";
                    case 258:
                        return "错误的上下文句柄";
                    case 259:
                        return "不合法的参数输入，请检查参数";
                    case 260:
                        return "分配的存储区域太小";
                    case 261:
                        return "错误的产品识别码（Application Data），请检查所使用的头文件/类文件与连接库是否与产品匹配";
                    case 262:
                        return "心跳线程错误";
                    case 263:
                        return "服务正忙，请稍候再试";
                    case 264:
                        return "服务暂停，请稍候再试";
                    case 265:
                        return "服务器返回结果为空";
                    case 266:
                        return "错误的命令类型";
                    case 267:
                        return "错误的返回值";
                    case 268:
                        return "上下文句柄错误，或还没有登陆，请检查参数或重新登陆";
                    case 270:
                        return "无效的本地许可数据文件";
                    case 272:
                        return "没有找到本地授权码对应的许可数据文件";
                    case 273:
                        return "本地授权许可数据文件签名不正确";
                    case 274:
                        return "运行时错误";
                    case 275:
                        return "线程错误";
                    case 276:
                        return "没有找到相应的本地授权许可数据文件";
                    case 277:
                        return "本地内部错误";
                    case 278:
                        return "许可数据文件获取不完整";
                    case 279:
                        return "授权点数已经用完";
                    case 280:
                        return "内存分配空间不足";
                    case 281:
                        return "获取授权时只能插入一个USB存储器";
                    case 282:
                        return "请插入存储授权的USB存储器";
                    case 283:
                        return "获取USB存储器指纹出错";
                    case 284:
                        return "授权码输入错误，请检查授权码拼写是否正确";
                    case 285:
                        return "发现本地系统时间篡改。当前时间比最近一次使用时间还要早";
                    case 291:
                        return "授权码已经从本机迁出";
                    case 513:
                        return "请重新调用Login命令";
                    case 516:
                        return "服务器内部异常错误";
                    case 518:
                        return "授权产品已禁用";
                    case 519:
                        return "开发商被禁用";
                    case 520:
                        return "服务器的服务已经停止，请检查服务器地址是否正确，相应的端口是否打开，或与开发商联系";
                    case 521:
                        return "无效的请求码";
                    case 522:
                        return "产品不匹配，SN不属于该应用程序";
                    case 523:
                        return "不支持的参数范围";
                    case 524:
                        return "无效的确认码";
                    case 525:
                        return "授权模板被禁止或删除";
                    case 1283:
                        return "指定的特征项没有找到";
                    case 1284:
                        return "特征项类型不匹配";
                    case 1285:
                        return "加解密数据长度超过限制";
                    case 1537:
                        return "指定的配置项没有找到";
                    case 1538:
                        return "配置项索引超过限制";
                    case 1539:
                        return "配置项容量超过限制";
                    case 1540:
                        return "配置项数量超过限制";
                    case 1541:
                        return "无效的配置项名称";
                    case 1793:
                        return "授权码已经过期";
                    case 1794:
                        return "在线用户数超过限制";
                    case 1795:
                        return "激活的机器数量已达上限，授权码不能在更多的机器上使用";
                    case 1796:
                        return "授权码使用次数已达上限，不能继续登录";
                    case 1797:
                        return "授权码被禁用";
                    case 1798:
                        return "指定的授权码没有找到，请确认授权码格式、服务器地址是否正确以及授权码与产品是否一致";
                    case 1799:
                        return "同一台客户端登陆次数超过限制";
                    case 1800:
                        return "执行命令次数超过限制";
                    case 1801:
                        return "授权码被暂时禁用，请稍候再试";
                    case 1804:
                        return "IP地址被禁用，请联系开发商";
                    case 1805:
                        return "请插入存储授权的USB存储器";
                    case 1806:
                        return "无效的授权码格式";
                    case 1807:
                        return "升级授权码次数过于频繁，请过一段时间再进行升级操作";
                    case 1810:
                        return "特征项容量已用尽，需要等待其它进程或客户端释放";
                    case 1811:
                        return "特征项还没有被使用，无需释放";
                    case 1812:
                        return "授权码类型不匹配，要求必须是离线授权码";
                    case 1814:
                        return "请求码错误";
                    case 1815:
                        return "请求码已经过期，请重新产生新的请求码";
                    case 1816:
                        return "请求码已经成功使用过一次，请重新产生新的请求码";
                    case 1817:
                        return "授权码转移的机器数量超过限制";
                    case 1818:
                        return "机器被禁用";
                    case 1819:
                        return "授权码转移的次数超过限制";
                    case 1820:
                        return "授权码不允许转移";
                    case 2049:
                        return "许可证类型不匹配";
                    case 2050:
                        return "授权码类型为本地授权";
                    case 2051:
                        return "下载次数超过限制";
                    case 2052:
                        return "数据签名不匹配";
                    case 2053:
                        return "本地授权许可数据文件已经存在";
                    case 2054:
                        return "本地授权许可数据文件已经更新";
                    case 2055:
                        return "本地系统时间错误，请检查系统时间及时区设置";
                }
                return "ErrorCode: 0x" + Convert.ToString(this.ErrorCode, 16);
            }
        }
    }

    public enum LoginMode {
        Local = 1,
        Remote = 2,
        Auto = 3
    }

    public enum BindingType {
        Existing = 0,
        Local = 1,
        UsbStorage = 2
    }

    public enum SessionType {
        XML_TYPE_SESSION_INFO = 1,
        XML_TYPE_SYSTEM_INFO = 2,
        XML_TYPE_SN_INFO = 3,

        BIT_ADDRESS = 0x101,
        BIT_SYS_TIME = 0x201,
        BIT_REG_DATE = 0x301,
        BIT_CONTROL_TYPE = 0x302,
        BIT_VOL_NUM = 0x303,
        BIT_START_DATE = 0x304,
        BIT_END_DATE = 0x305,
        BIT_EXPIRATION_DAYS = 0x306,
        BIT_USAGE_NUM = 0x307,
        BIT_CONSUMED_USAGE_NUM = 0x308,
        BIT_CONCURRENT_NUM = 0x309,
        BIT_ACTIVATE_DATE = 0x30A,
        BIT_USER_LIMIT = 0x30B,
        BIT_LAST_REMOTE_ACCESS_DATE = 0x30C,
        BIT_MAX_OFFLINE_MINUTES = 0x30D
    }

    internal class BitAnswer {
        [DllImport("bitanswer.dll", EntryPoint = "Bit_Login")]
        private static extern int Bit_Login(string url, string sn, byte[] applicationData, ref uint handle, int mode);

        [DllImport("bitanswer.dll", EntryPoint = "Bit_Logout")]
        private static extern int Bit_Logout(uint handle);

        [DllImport("bitanswer.dll", EntryPoint = "Bit_ConvertFeature")]
        private static extern int Bit_ConvertFeature(uint handle, uint featureId, uint para1, uint para2, uint para3, uint para4, ref uint result);

        [DllImport("bitanswer.dll", EntryPoint = "Bit_ReadFeature")]
        private static extern int Bit_ReadFeature(uint handle, uint featureId, ref uint featureValue);

        [DllImport("bitanswer.dll", EntryPoint = "Bit_WriteFeature")]
        private static extern int Bit_WriteFeature(uint handle, uint featureId, uint featureValue);

        [DllImport("bitanswer.dll", EntryPoint = "Bit_EncryptFeature")]
        private static extern int Bit_EncryptFeature(uint handle, uint featureId, byte[] plainBuffer, byte[] cipherBuffer, uint bufferLen);

        [DllImport("bitanswer.dll", EntryPoint = "Bit_DecryptFeature")]
        private static extern int Bit_DecryptFeature(uint handle, uint featureId, byte[] plainBuffer, byte[] cipherBuffer, uint bufferLen);

        [DllImport("bitanswer.dll", EntryPoint = "Bit_GetDataItemNum")]
        private static extern int Bit_GetDataItemNum(uint handle, ref uint number);

        [DllImport("bitanswer.dll", EntryPoint = "Bit_GetDataItemName")]
        private static extern int Bit_GetDataItemName(uint handle, uint index, byte[] dataItemName, ref uint DataItemNameLen);

        [DllImport("bitanswer.dll", EntryPoint = "Bit_GetDataItem")]
        private static extern int Bit_GetDataItem(uint handle, string dataItemName, byte[] dataItemValue, ref uint dataItemValueLen);

        [DllImport("bitanswer.dll", EntryPoint = "Bit_SetDataItem")]
        private static extern int Bit_SetDataItem(uint handle, string dataItemName, byte[] dataItemValue, uint dataItemValueLen);

        [DllImport("bitanswer.dll", EntryPoint = "Bit_RemoveDataItem")]
        private static extern int Bit_RemoveDataItem(uint handle, string dataItemName);

        [DllImport("bitanswer.dll", EntryPoint = "Bit_GetSessionInfo")]
        private static extern int Bit_GetSessionInfo(uint handle, uint type, byte[] sessionInfo, ref uint sessionInfoLen);

        [DllImport("bitanswer.dll", EntryPoint = "Bit_GetRequestInfo")]
        private static extern int Bit_GetRequestInfo(string sn, byte[] applicationData, uint bindingType, byte[] requestInfo, ref uint requestInfoSize);

        [DllImport("bitanswer.dll", EntryPoint = "Bit_GetUpdateInfo")]
        private static extern int Bit_GetUpdateInfo(string url, string sn, byte[] applicationData, string requestInfo, byte[] updateInfo, ref uint updateInfoSize);

        [DllImport("bitanswer.dll", EntryPoint = "Bit_ApplyUpdateInfo")]
        private static extern int Bit_ApplyUpdateInfo(byte[] applicationData, string updateInfo, byte[] receiptInfo, ref uint receiptInfoSize);

        [DllImport("bitanswer.dll", EntryPoint = "Bit_UpdateOnline")]
        private static extern int Bit_UpdateOnline(string url, string sn, byte[] applicationData);

        [DllImport("bitanswer.dll", EntryPoint = "Bit_SetDbPath")]
        private static extern int Bit_SetDbPath(string szPath);

        private static readonly byte[] ApplicationData;

        static BitAnswer() {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", null).Replace('/', '\\'));
            if(path != null) {
                var fileName = Path.Combine(path, "license.dat");
                if(File.Exists(fileName)) {
                    ApplicationData = File.ReadAllBytes(fileName);
                    return;
                }
            }
            ApplicationData = new byte[94];
        }

        private uint handle;

        public void Login(string url, string sn, LoginMode mode) {
            var status = Bit_Login(url, sn, ApplicationData, ref this.handle, (int)mode);
            if(status != 0)
                throw new BitAnswerException(status);
        }

        public void Logout() {
            var status = Bit_Logout(this.handle);
            if(status != 0)
                throw new BitAnswerException(status);
        }

        public uint ConvertFeature(uint featureId, uint para1, uint para2, uint para3, uint para4) {
            uint result = 0;
            var status = Bit_ConvertFeature(this.handle, featureId, para1, para2, para3, para4, ref result);
            if(status != 0)
                throw new BitAnswerException(status);
            return result;
        }

        public uint ReadFeature(uint featureId) {
            uint featureValue = 0;
            var status = Bit_ReadFeature(this.handle, featureId, ref featureValue);
            if(status != 0)
                throw new BitAnswerException(status);
            return featureValue;
        }

        public void WriteFeature(uint featureId, uint featureValue) {
            var status = Bit_WriteFeature(this.handle, featureId, featureValue);
            if(status != 0)
                throw new BitAnswerException(status);
        }

        public byte[] EncryptFeature(uint featureId, byte[] plainBuffer) {
            var cipherBuffer = new byte[plainBuffer.Length];
            var status = Bit_EncryptFeature(this.handle, featureId, plainBuffer, cipherBuffer, (uint)plainBuffer.Length);
            if(status != 0)
                throw new BitAnswerException(status);
            return cipherBuffer;
        }

        public byte[] DecryptFeature(uint featureId, byte[] cipherBuffer) {
            var plainBuffer = new byte[cipherBuffer.Length];
            var status = Bit_DecryptFeature(this.handle, featureId, cipherBuffer, plainBuffer, (uint)cipherBuffer.Length);
            if(status != 0)
                throw new BitAnswerException(status);
            return plainBuffer;
        }

        public uint GetDataItemNum() {
            uint num = 0;
            var status = Bit_GetDataItemNum(this.handle, ref num);
            if(status != 0)
                throw new BitAnswerException(status);
            return num;
        }

        public string GetDataItemName(uint index) {
            uint nameLen = 129;
            var name = new byte[nameLen];
            var status = Bit_GetDataItemName(this.handle, index, name, ref nameLen);
            if(status != 0)
                throw new BitAnswerException(status);
            return Encoding.GetEncoding("gbk").GetString(name);
        }

        public byte[] GetDataItem(string dataItemName) {
            uint dataItemValueLen = 1024;
            var dataItemValueTemp = new byte[dataItemValueLen];
            var status = Bit_GetDataItem(this.handle, dataItemName, dataItemValueTemp, ref dataItemValueLen);
            if(status != 0)
                throw new BitAnswerException(status);

            var dataItemValue = new byte[dataItemValueLen];
            Array.Copy(dataItemValueTemp, dataItemValue, dataItemValueLen);
            return dataItemValue;
        }

        public void SetDataItem(string dataItemName, byte[] dataItemValue) {
            var status = Bit_SetDataItem(this.handle, dataItemName, dataItemValue, (uint)dataItemValue.Length);
            if(status != 0)
                throw new BitAnswerException(status);
        }

        public void RemoveDataItem(string dataItemName) {
            var status = Bit_RemoveDataItem(this.handle, dataItemName);
            if(status != 0)
                throw new BitAnswerException(status);
        }

        public string GetSessionInfo(SessionType type) {
            uint xmlSessionInfoLen = 1024;
            var xmlSessionInfo = new byte[xmlSessionInfoLen];
            var status = Bit_GetSessionInfo(this.handle, (uint)type, xmlSessionInfo, ref xmlSessionInfoLen);
            if(status != 0)
                throw new BitAnswerException(status);
            return Encoding.UTF8.GetString(xmlSessionInfo);
        }

        public void UpdateOnline(string url, string sn) {
            var status = Bit_UpdateOnline(url, sn, ApplicationData);
            if(status != 0)
                throw new BitAnswerException(status);
        }

        public string GetRequestInfo(string url, string sn, uint bindingType) {
            uint requestInfoSize = 102400;
            var requestInfo = new byte[requestInfoSize];
            var status = Bit_GetRequestInfo(sn, ApplicationData, bindingType, requestInfo, ref requestInfoSize);
            if(status != 0)
                throw new BitAnswerException(status);
            return Encoding.UTF8.GetString(requestInfo);
        }

        public string GetUpdateInfo(string url, string sn, string requestInfo) {
            uint updateInfoSize = 102400;
            var updateInfo = new byte[updateInfoSize];
            var status = Bit_GetUpdateInfo(url, sn, ApplicationData, requestInfo, updateInfo, ref updateInfoSize);
            if(status != 0)
                throw new BitAnswerException(status);
            return Encoding.UTF8.GetString(updateInfo);
        }

        public string ApplyUpdateInfo(string updateInfo) {
            uint receiptInfoSize = 102400;
            var receiptInfo = new byte[receiptInfoSize];
            var status = Bit_ApplyUpdateInfo(ApplicationData, updateInfo, receiptInfo, ref receiptInfoSize);
            if(status != 0)
                throw new BitAnswerException(status);
            return Encoding.UTF8.GetString(receiptInfo);
        }

        public void SetDbPath(string path) {
            var status = Bit_SetDbPath(path);
            if(status != 0)
                throw new BitAnswerException(status);
        }
    }
}
