﻿using System;
using System.Text;

namespace Sunlight.Silverlight.License {
    public class LicenseManager : IDisposable {
        private BitAnswer bitAnswer;
        private bool isLogined;

        private BitAnswer BitAnswer {
            get {
                return this.bitAnswer ?? (this.bitAnswer = new BitAnswer());
            }
        }

        public bool IsActivated {
            get {
                if(this.isLogined)
                    return true;
                BitAnswerException exception;
                return this.Login(out exception);
            }
        }

        private bool Login(out BitAnswerException exception) {
            return this.Login(null, out exception);
        }

        private bool Login(string sn, out BitAnswerException exception) {
            this.isLogined = false;
            exception = null;
            try {
                this.BitAnswer.Login(null, sn, LoginMode.Auto);
                this.isLogined = true;
                return true;
            } catch(BitAnswerException ex) {
                exception = ex;
                return false;
            }
        }

        public void Dispose() {
            try {
                if(this.bitAnswer != null)
                    this.bitAnswer.Logout();
            } catch {
                // ReSharper disable RedundantJumpStatement
                return;
                // ReSharper restore RedundantJumpStatement
            } finally {
                this.isLogined = false;
                this.bitAnswer = null;
            }
        }

        public bool Activate(string sn, out string errorMessage) {
            errorMessage = null;
            BitAnswerException exception;
            if(!this.Login(sn, out exception)) {
                errorMessage = exception.Message;
                return false;
            }
            return true;
        }

        public bool Validate(uint num0, uint num1, uint num2, uint num3, uint num4, out string errorMessage) {
            errorMessage = null;
            BitAnswerException exception;
            if(!this.isLogined && !this.Login(out exception)) {
                errorMessage = exception.Message;
                return false;
            }
            try {
                var result = this.BitAnswer.ConvertFeature(1, num1, num2, num3, num4) == num0;
                if(!result)
                    errorMessage = "验证结果不匹配";
                return result;
            } catch(BitAnswerException ex) {
                errorMessage = ex.Message;
                return false;
            }
        }

        public string GetValue(string key, out string errorMessage) {
            errorMessage = null;
            BitAnswerException exception;
            if(!this.isLogined && !this.Login(out exception)) {
                errorMessage = exception.Message;
                return null;
            }
            try {
                return Encoding.UTF8.GetString(this.BitAnswer.GetDataItem(key));
            } catch(BitAnswerException ex) {
                errorMessage = ex.Message;
                return null;
            }
        }

        public void UpdateOnline(string sn, out string errorMessage) {
            errorMessage = null;
            try {
                this.BitAnswer.UpdateOnline(null, sn);
            } catch(BitAnswerException ex) {
                errorMessage = ex.Message;
            }
        }

        public string GetSessionInfo(SessionType sessionType, out string errorMessage) {
            errorMessage = null;
            BitAnswerException exception;
            if(!this.isLogined && !this.Login(out exception)) {
                errorMessage = exception.Message;
                return null;
            }
            try {
                return this.BitAnswer.GetSessionInfo(sessionType);
            } catch(BitAnswerException ex) {
                errorMessage = ex.Message;
                return null;
            }
        }
    }
}
