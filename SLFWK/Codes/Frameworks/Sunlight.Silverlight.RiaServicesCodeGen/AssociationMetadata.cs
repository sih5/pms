﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.DomainServices;

namespace Sunlight.Silverlight.RiaServicesCodeGen {
    internal class AssociationMetadata {
        public AssociationAttribute AssociationAttribute {
            get;
            private set;
        }

        public CodeTypeReference AssociationType {
            get;
            private set;
        }

        public string AssociationTypeName {
            get;
            private set;
        }

        public IEnumerable<Attribute> Attributes {
            get;
            private set;
        }

        public string FieldName {
            get;
            private set;
        }

        public bool IsCollection {
            get;
            private set;
        }

        public bool IsExternal {
            get;
            private set;
        }

        public PropertyDescriptor PropertyDescriptor {
            get;
            private set;
        }

        public string PropertyName {
            get;
            private set;
        }

        public CodeTypeReference PropertyType {
            get;
            private set;
        }

        public string PropertyTypeName {
            get;
            private set;
        }

        public AssociationMetadata(PropertyDescriptor pd) {
            var propertyType = pd.PropertyType;
            this.PropertyDescriptor = pd;
            var source = CustomEntityGenerator.ExplicitAttributes(pd);
            this.AssociationAttribute = (AssociationAttribute)source[typeof(AssociationAttribute)];
            this.IsExternal = source[typeof(ExternalReferenceAttribute)] != null;
            this.IsCollection = CustomEntityGenerator.IsCollectionType(propertyType);
            if(!this.IsCollection) {
                this.PropertyType = TypeUtility.IsPredefinedSimpleType(propertyType) ? new CodeTypeReference(propertyType) : new CodeTypeReference(propertyType.Name);
                this.PropertyTypeName = CodeGenUtilities.GetTypeName(propertyType);
                this.AssociationType = new CodeTypeReference("EntityRef", new CodeTypeReference(this.PropertyType.BaseType, CodeTypeReferenceOptions.GenericTypeParameter));
                this.AssociationTypeName = "EntityRef<" + this.PropertyTypeName + ">";
                this.Attributes = from a in source.Cast<Attribute>()
                                  where a.GetType() != typeof(DataMemberAttribute)
                                  select a;
            } else {
                propertyType = TypeUtility.GetElementType(propertyType);
                this.PropertyType = TypeUtility.IsPredefinedSimpleType(propertyType) ? new CodeTypeReference(propertyType) : new CodeTypeReference(propertyType.Name);
                this.PropertyTypeName = CodeGenUtilities.GetTypeName(propertyType);
                this.AssociationType = new CodeTypeReference("EntityCollection", new CodeTypeReference(this.PropertyType.BaseType, CodeTypeReferenceOptions.GenericTypeParameter));
                this.AssociationTypeName = "EntityCollection<" + this.PropertyTypeName + ">";
                var list = source.Cast<Attribute>().ToList<Attribute>();
                var attribute = source.OfType<ReadOnlyAttribute>().SingleOrDefault<ReadOnlyAttribute>();
                if((attribute != null) && !source.OfType<EditableAttribute>().Any<EditableAttribute>())
                    list.Add(new EditableAttribute(!attribute.IsReadOnly));
                this.Attributes = from a in list
                                  where a.GetType() != typeof(DataMemberAttribute)
                                  select a;
            }
            this.PropertyName = CodeGenUtilities.GetSafeName(pd.Name);
            this.FieldName = CodeGenUtilities.MakeCompliantFieldName(this.PropertyName);
        }
    }
}
