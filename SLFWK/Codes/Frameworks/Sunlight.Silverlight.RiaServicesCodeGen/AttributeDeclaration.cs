﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Sunlight.Silverlight.RiaServicesCodeGen {
    internal sealed class AttributeDeclaration {
        // Fields
        private readonly Type attributeType;
        private List<object> constructorArguments;
        private readonly List<string> errors;
        private Dictionary<string, object> namedParameters;
        private List<MethodInfo> requiredMethods;
        private List<PropertyInfo> requiredProperties;
        private List<Type> requiredTypes;

        // Methods
        public AttributeDeclaration(Type attributeType) {
            if(attributeType == null)
                throw new ArgumentNullException("attributeType");
            this.attributeType = attributeType;
            this.errors = new List<string>();
        }

        // Properties
        public Type AttributeType {
            get {
                return this.attributeType;
            }
        }

        public IList<object> ConstructorArguments {
            get {
                return this.constructorArguments ?? (this.constructorArguments = new List<object>());
            }
        }

        public IList<string> Errors {
            get {
                return this.errors;
            }
        }

        public bool HasErrors {
            get {
                return (this.errors.Count > 0);
            }
        }

        public IDictionary<string, object> NamedParameters {
            get {
                return this.namedParameters ?? (this.namedParameters = new Dictionary<string, object>(StringComparer.Ordinal));
            }
        }

        public IList<MethodInfo> RequiredMethods {
            get {
                return this.requiredMethods ?? (this.requiredMethods = new List<MethodInfo>());
            }
        }

        public IList<PropertyInfo> RequiredProperties {
            get {
                return this.requiredProperties ?? (this.requiredProperties = new List<PropertyInfo>());
            }
        }

        public IList<Type> RequiredTypes {
            get {
                return this.requiredTypes ?? (this.requiredTypes = new List<Type>());
            }
        }
    }
}
