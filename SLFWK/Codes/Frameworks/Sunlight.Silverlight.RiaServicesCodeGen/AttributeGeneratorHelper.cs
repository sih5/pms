﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.DomainServices;
using Microsoft.ServiceModel.DomainServices.Tools;

namespace Sunlight.Silverlight.RiaServicesCodeGen {
    internal class AttributeGeneratorHelper {
        private static readonly Type[] BlockList = new[] {
            typeof(MetadataTypeAttribute), typeof(ScaffoldColumnAttribute), typeof(ScaffoldTableAttribute), typeof(SerializableAttribute), typeof(SuppressMessageAttribute)
        };
        private static readonly Dictionary<Type, ICustomAttributeBuilder> KnownBuilders = new Dictionary<Type, ICustomAttributeBuilder>();
        private static readonly Dictionary<Type, Type> KnownBuilderTypes = new Dictionary<Type, Type> {
            {
                typeof(CustomValidationAttribute), typeof(CustomValidationCustomAttributeBuilder)
                },
            {
                typeof(DataMemberAttribute), typeof(DataMemberAttributeBuilder)
                },
            {
                typeof(DisplayAttribute), typeof(DisplayCustomAttributeBuilder)
                },
            {
                typeof(DomainIdentifierAttribute), typeof(DomainIdentifierAttributeBuilder)
                },
            {
                typeof(EditableAttribute), typeof(EditableAttributeBuilder)
                },
            {
                typeof(RangeAttribute), typeof(RangeCustomAttributeBuilder)
                },
            {
                typeof(RegularExpressionAttribute), typeof(ValidationCustomAttributeBuilder)
                },
            {
                typeof(RequiredAttribute), typeof(ValidationCustomAttributeBuilder)
                },
            {
                typeof(StringLengthAttribute), typeof(ValidationCustomAttributeBuilder)
                },
            {
                typeof(UIHintAttribute), typeof(UIHintCustomAttributeBuilder)
                }
        };

        private static bool IsAttributeBlocked(Type attributeType) {
            return BlockList.Contains(attributeType);
        }

        private static ICustomAttributeBuilder GetCustomAttributeBuilder(Type attributeType) {
            if(attributeType == null)
                throw new ArgumentNullException("attributeType");
            ICustomAttributeBuilder builder;
            if(!KnownBuilders.TryGetValue(attributeType, out builder)) {
                Type type;
                if(!KnownBuilderTypes.TryGetValue(attributeType, out type)) {
                    var pair = KnownBuilderTypes.Where(kv => kv.Key.IsAssignableFrom(attributeType)).FirstOrDefault();
                    if(pair.Key != null && pair.Value != null)
                        type = pair.Value;
                }
                if(type == null)
                    type = typeof(StandardCustomAttributeBuilder);
                builder = Activator.CreateInstance(type) as ICustomAttributeBuilder;
                if(builder != null)
                    KnownBuilders[attributeType] = builder;
            }
            return builder;
        }

        private static void ValidateAttributeDeclarationRequirements(AttributeDeclaration attributeDeclaration, CustomClientCodeGenerator clientCodeGenerator) {
            switch(clientCodeGenerator.GetTypeShareKind(attributeDeclaration.AttributeType)) {
                case CodeMemberShareKind.Unknown:
                    attributeDeclaration.Errors.Add(string.Format(CultureInfo.CurrentCulture, "The attribute '{0}' references type '{1}' from assembly '{2}'. The type is not visible to the client project '{3}' because either a PDB file does not exist or the type does not contain user-defined code.", new object[] {
                        attributeDeclaration.AttributeType, attributeDeclaration.AttributeType.Assembly.GetName().Name, clientCodeGenerator.ClientProjectName
                    }));
                    break;
                case CodeMemberShareKind.NotShared:
                    attributeDeclaration.Errors.Add(string.Format(CultureInfo.CurrentCulture, "The attribute '{0}' is not visible in the client project '{1}'. Are you missing an assembly reference?", new object[] {
                        attributeDeclaration.AttributeType, clientCodeGenerator.ClientProjectName
                    }));
                    break;
            }

            foreach(var type in attributeDeclaration.RequiredTypes.OrderBy(t => t.FullName))
                switch(clientCodeGenerator.GetTypeShareKind(type)) {
                    case CodeMemberShareKind.Unknown: {
                        attributeDeclaration.Errors.Add(string.Format(CultureInfo.CurrentCulture, "The attribute '{0}' references type '{1}' from assembly '{2}'. The type is not visible to the client project '{3}' because either a PDB file does not exist or the type does not contain user-defined code.", new object[] {
                            attributeDeclaration.AttributeType, type, type.Assembly.GetName().Name, clientCodeGenerator.ClientProjectName
                        }));
                        continue;
                    }
                    case CodeMemberShareKind.NotShared:
                        attributeDeclaration.Errors.Add(string.Format(CultureInfo.CurrentCulture, "The attribute '{0}' references type '{1}' that is not accessible in the client project '{2}'. If you would like the attribute to be generated, make sure the assembly containing the attribute is referenced on the client.", new object[] {
                            attributeDeclaration.AttributeType, type, clientCodeGenerator.ClientProjectName
                        }));
                        break;
                }

            foreach(var info in from p in attributeDeclaration.RequiredMethods
                                orderby p.Name
                                select p)
                if(clientCodeGenerator.GetMethodShareKind(info) == CodeMemberShareKind.NotShared)
                    attributeDeclaration.Errors.Add(string.Format(CultureInfo.CurrentCulture, "The attribute '{0}' references a method '{1}' on type '{2}' that is not accessible in the client project '{3}'.", new object[] {
                        attributeDeclaration.AttributeType, info.Name, info.DeclaringType, clientCodeGenerator.ClientProjectName
                    }));

            foreach(var info2 in from p in attributeDeclaration.RequiredProperties
                                 orderby p.Name
                                 select p)
                if(clientCodeGenerator.GetPropertyShareKind(info2.DeclaringType, info2.Name) == CodeMemberShareKind.NotShared)
                    attributeDeclaration.Errors.Add(string.Format(CultureInfo.CurrentCulture, "The attribute '{0}' references a property '{1}' on type '{2}' that is not accessible in the client project '{3}'.", new object[] {
                        attributeDeclaration.AttributeType, info2.Name, info2.DeclaringType, clientCodeGenerator.ClientProjectName
                    }));
        }

        internal static AttributeDeclaration GetAttributeDeclaration(Attribute attribute, CustomClientCodeGenerator clientCodeGenerator, bool forcePropagation) {
            var attributeType = attribute.GetType();
            if(IsAttributeBlocked(attributeType))
                return null;

            AttributeDeclaration attributeDeclaration = null;
            var customAttributeBuilder = GetCustomAttributeBuilder(attributeType);
            if(customAttributeBuilder != null) {
                try {
                    attributeDeclaration = customAttributeBuilder.GetAttributeDeclaration(attribute);
                } catch(AttributeBuilderException) {
                    return null;
                }
                if(attributeDeclaration != null && !forcePropagation)
                    ValidateAttributeDeclarationRequirements(attributeDeclaration, clientCodeGenerator);
            }
            return attributeDeclaration;
        }

        internal static string GetContractNamespace(Type sourceType) {
            string str2;
            var dictionary = new Dictionary<string, string>();
            var customAttributes = (ContractNamespaceAttribute[])sourceType.Assembly.GetCustomAttributes(typeof(ContractNamespaceAttribute), true);
            if(customAttributes.Length > 0)
                foreach(var attribute in customAttributes.Where(attribute => attribute.ClrNamespace != null))
                    dictionary.Add(attribute.ClrNamespace, attribute.ContractNamespace);
            var key = sourceType.Namespace ?? string.Empty;
            if(dictionary.TryGetValue(key, out str2))
                return str2;
            return ("http://schemas.datacontract.org/2004/07/" + Uri.EscapeUriString(key));
        }

        internal static void GetContractNameAndNamespace(Type sourceType, out string dataContractNamespace, out string dataContractName) {
            dataContractNamespace = GetContractNamespace(sourceType);
            dataContractName = null;
            var customAttribute = (DataContractAttribute)Attribute.GetCustomAttribute(sourceType, typeof(DataContractAttribute));
            if(customAttribute == null)
                return;
            if(customAttribute.Namespace != null)
                dataContractNamespace = customAttribute.Namespace;
            if(customAttribute.Name != null)
                dataContractName = customAttribute.Name;
        }
    }
}
