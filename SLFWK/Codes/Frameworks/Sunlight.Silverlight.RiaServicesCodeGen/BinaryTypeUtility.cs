﻿using System;
using System.Reflection;

namespace Sunlight.Silverlight.RiaServicesCodeGen {
    internal static class BinaryTypeUtility {
        // Fields
        private static ConstructorInfo binaryConstructor;
        private static MethodInfo binaryToArrayMethod;
        private static Type binaryType;

        // Methods
        internal static object GetBinaryFromByteArray(byte[] bytes) {
            if(binaryConstructor == null)
                binaryConstructor = binaryType.GetConstructor(new[] {
                    typeof(byte[])
                });
            return binaryConstructor.Invoke(new object[] {
                bytes
            });
        }

        internal static byte[] GetByteArrayFromBinary(object binary) {
            if(binaryToArrayMethod == null)
                binaryToArrayMethod = binaryType.GetMethod("ToArray");
            return (byte[])binaryToArrayMethod.Invoke(binary, null);
        }

        internal static bool IsTypeBinary(Type type) {
            if(type != null) {
                if(binaryType == type)
                    return true;
                if(string.Compare(type.FullName, "System.Data.Linq.Binary", StringComparison.Ordinal) == 0) {
                    binaryType = type;
                    return true;
                }
            }
            return false;
        }
    }
}
