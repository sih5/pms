﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Microsoft.CSharp;
using Microsoft.ServiceModel.DomainServices.Tools;

namespace Sunlight.Silverlight.RiaServicesCodeGen {
    [DomainServiceClientCodeGenerator("CustomClientCodeGenerator", "C#")]
    public sealed class CustomClientCodeGenerator : IDomainServiceClientCodeGenerator {
        private CustomEntityGenerator entityGenerator;
        private CustomComplexObjectGenerator complexObjectGenerator;
        private CustomDomainContextGenerator domainContextGenerator;
        private CustomWebContextGenerator webContextGenerator;
        private CustomEnumGenerator enumGenerator;
        private HashSet<Type> enumTypesToGenerate;

        internal ICodeGenerationHost CodeGenerationHost {
            get;
            private set;
        }

        internal IEnumerable<DomainServiceDescription> DomainServiceDescriptions {
            get;
            private set;
        }

        internal ClientCodeGenerationOptions Options {
            get;
            private set;
        }

        internal string ClientProjectName {
            get {
                return Path.GetFileNameWithoutExtension(this.Options.ClientProjectPath);
            }
        }

        private CodeMemberShareKind GetDomainContextTypeShareKind(DomainServiceDescription domainServiceDescription) {
            var domainServiceType = domainServiceDescription.DomainServiceType;
            var domainContextTypeName = CustomDomainContextGenerator.GetDomainContextTypeName(domainServiceDescription);
            var typeName = domainServiceType.Namespace + "." + domainContextTypeName;
            return this.CodeGenerationHost.GetTypeShareKind(typeName);
        }

        private IEnumerable<CodeNamespace> GenerateEntitiesIfNotGenerated(DomainServiceDescription domainServiceDescription, ICollection<Type> generatedEntities) {
            foreach(var type in domainServiceDescription.EntityTypes.OrderBy(e => e.Name))
                if(!generatedEntities.Contains(type))
                    if((this.GetTypeShareKind(type) & CodeMemberShareKind.SharedByReference) != CodeMemberShareKind.Unknown)
                        this.CodeGenerationHost.LogError(string.Format(CultureInfo.CurrentCulture, "Type '{0}' already shared", new object[] {
                            type.FullName
                        }));
                    else {
                        generatedEntities.Add(type);
                        yield return this.entityGenerator.Generate(this, this.DomainServiceDescriptions, type);
                    }
        }

        private void MergeImports(CodeNamespace ns, IEnumerable<CodeNamespaceImport> imports) {
            var existedImports = ns.Imports.Cast<CodeNamespaceImport>().ToArray();
            foreach(var import in imports.Where(import => !existedImports.Contains(import)))
                ns.Imports.Add(import);
        }

        private void MergeNamespace(CodeCompileUnit compileUnit, CodeNamespace ns) {
            var existedNs = compileUnit.Namespaces.Cast<CodeNamespace>().SingleOrDefault(n => n.Name == ns.Name);
            if(existedNs == null)
                compileUnit.Namespaces.Add(ns);
            else {
                this.MergeImports(existedNs, ns.Imports.Cast<CodeNamespaceImport>());
                existedNs.Types.AddRange(ns.Types);
            }
        }

        private string GenerateCode() {
            if(!this.Options.Language.Equals("C#", StringComparison.OrdinalIgnoreCase)) {
                this.CodeGenerationHost.LogError("The target language must be C#.");
                return null;
            }

            var generationEnvironment = new StringBuilder();
            var compileUnit = new CodeCompileUnit();

            if(this.Options.IsApplicationContextGenerationEnabled)
                compileUnit.Namespaces.Add(this.webContextGenerator.Generate(this));

            var generatedEntities = new List<Type>();
            //var generatedComplexObjects = new List<Type>();

            foreach(var description in from d in this.DomainServiceDescriptions
                                       where (this.GetDomainContextTypeShareKind(d) & CodeMemberShareKind.Shared) == CodeMemberShareKind.Unknown
                                       orderby d.DomainServiceType.Name
                                       select d) {
                this.MergeNamespace(compileUnit, this.domainContextGenerator.Generate(this, description));
                foreach(var ns in this.GenerateEntitiesIfNotGenerated(description, generatedEntities))
                    this.MergeNamespace(compileUnit, ns);
                //foreach(var ns in this.GenerateComplexObjectsIfNotGenerated(description, generatedEntities))
                //    this.MergeNamespace(compileUnit, ns);
            }

            foreach(var ns in this.enumGenerator.Generate(this, this.enumTypesToGenerate))
                this.MergeNamespace(compileUnit, ns);

            var sortedNs = compileUnit.Namespaces.Cast<CodeNamespace>().OrderBy(n => n.Name).ToArray();
            compileUnit.Namespaces.Clear();
            compileUnit.Namespaces.AddRange(sortedNs);

            using(var provider = new CSharpCodeProvider()) {
                using(var writer = new StringWriter(generationEnvironment)) {
                    using(var indentedWriter = new IndentedTextWriter(writer)) {
                        provider.GenerateCodeFromCompileUnit(compileUnit, indentedWriter, new CodeGeneratorOptions {
                            BlankLinesBetweenMembers = true,
                            VerbatimOrder = true,
                            ElseOnClosing = true,
                            IndentString = "    ",
                        });
                    }
                }
            }

            return generationEnvironment.ToString();
        }

        internal string TranslateType(Type type) {
            var result = new StringBuilder();
            using(var provider = new CSharpCodeProvider()) {
                using(var writer = new StringWriter(result)) {
                    provider.GenerateCodeFromExpression(new CodeTypeReferenceExpression(type), writer, new CodeGeneratorOptions {
                        BlankLinesBetweenMembers = false,
                        VerbatimOrder = true,
                    });
                }
            }
            return result.ToString();
        }

        internal IEnumerable<CodeAttributeDeclaration> GenerateAttributes(IEnumerable<Attribute> attributes, bool forcePropagation = false) {
            foreach(var attribute in attributes.OrderBy(a => a.GetType().Name)) {
                var declaration = AttributeGeneratorHelper.GetAttributeDeclaration(attribute, this, forcePropagation);
                if(declaration == null || declaration.HasErrors)
                    continue;

                var attributeDeclaration = new CodeAttributeDeclaration {
                    Name = declaration.AttributeType.Name,
                };
                foreach(var argument in declaration.ConstructorArguments)
                    if(argument is Type)
                        attributeDeclaration.Arguments.Add(new CodeAttributeArgument(new CodeTypeOfExpression((Type)argument)));
                    else if(argument.GetType().IsEnum) {
                        var enumType = argument.GetType();
                        attributeDeclaration.Arguments.Add(new CodeAttributeArgument(new CodeFieldReferenceExpression(new CodeTypeReferenceExpression(enumType), Enum.GetName(enumType, argument))));
                    } else //if(argument is string || argument.GetType().IsPrimitive)
                        attributeDeclaration.Arguments.Add(new CodeAttributeArgument(new CodePrimitiveExpression(argument)));
                foreach(var argumentKv in declaration.NamedParameters)
                    if(argumentKv.Value is Type)
                        attributeDeclaration.Arguments.Add(new CodeAttributeArgument(argumentKv.Key, new CodeTypeOfExpression((Type)argumentKv.Value)));
                    else if(argumentKv.Value.GetType().IsEnum) {
                        var enumType = argumentKv.Value.GetType();
                        attributeDeclaration.Arguments.Add(new CodeAttributeArgument(argumentKv.Key, new CodeFieldReferenceExpression(new CodeTypeReferenceExpression(enumType), Enum.GetName(enumType, argumentKv.Value))));
                    } else //if(argumentKv.Value is string || argumentKv.Value.GetType().IsPrimitive)
                        attributeDeclaration.Arguments.Add(new CodeAttributeArgument(argumentKv.Key, new CodePrimitiveExpression(argumentKv.Value)));
                yield return attributeDeclaration;
            }
        }

        internal CodeAttributeDeclaration GenerateDataContractAttribute(Type sourceType) {
            string contractNamespace;
            string contractName;
            AttributeGeneratorHelper.GetContractNameAndNamespace(sourceType, out contractNamespace, out contractName);

            var attributeDeclaration = new CodeAttributeDeclaration("DataContract");
            attributeDeclaration.Arguments.Add(new CodeAttributeArgument("Namespace", new CodePrimitiveExpression(contractNamespace)));
            if(!string.IsNullOrEmpty(contractName))
                attributeDeclaration.Arguments.Add(new CodeAttributeArgument("Name", new CodePrimitiveExpression(contractName)));
            return attributeDeclaration;
        }

        internal CodeMemberShareKind GetTypeShareKind(Type type) {
            return this.CodeGenerationHost.GetTypeShareKind(type.AssemblyQualifiedName);
        }

        internal CodeMemberShareKind GetMethodShareKind(MethodBase methodBase) {
            var parameterTypeNames = from p in methodBase.GetParameters()
                                     select p.ParameterType.AssemblyQualifiedName;
            return this.CodeGenerationHost.GetMethodShareKind(methodBase.DeclaringType.AssemblyQualifiedName, methodBase.Name, parameterTypeNames);
        }

        internal CodeMemberShareKind GetPropertyShareKind(Type type, string propertyName) {
            return this.CodeGenerationHost.GetPropertyShareKind(type.AssemblyQualifiedName, propertyName);
        }

        internal bool CanExposeEnumType(Type enumType, out string errorMessage) {
            errorMessage = null;
            if(!enumType.IsPublic || enumType.IsNested) {
                errorMessage = "The Enum type must be public and cannot be nested.";
                return false;
            }
            if(((this.GetTypeShareKind(enumType) & CodeMemberShareKind.Shared) == CodeMemberShareKind.Unknown) && enumType.Assembly.IsSystemAssembly()) {
                errorMessage = "The Enum type is not visible to the client and belongs to the System namespace.  Are you missing an assembly reference?";
                return false;
            }
            return true;
        }

        internal bool NeedToGenerateEnumType(Type enumType) {
            return this.enumTypesToGenerate.Contains(enumType) || ((this.GetTypeShareKind(enumType) & CodeMemberShareKind.Shared) == CodeMemberShareKind.Unknown);
        }

        internal void AddEnumTypeToGenerate(Type enumType) {
            if(this.enumTypesToGenerate != null && !this.enumTypesToGenerate.Contains(enumType) && this.NeedToGenerateEnumType(enumType))
                this.enumTypesToGenerate.Add(enumType);
        }

        [Conditional("DEBUG")]
        internal void Trace(string message, bool isAppend = true) {
            if(isAppend)
                File.AppendAllText("D:\\Trace.txt", message + Environment.NewLine);
            else
                File.WriteAllText("D:\\Trace.txt", message + Environment.NewLine);
        }

        public string GenerateCode(ICodeGenerationHost codeGenerationHost, IEnumerable<DomainServiceDescription> domainServiceDescriptions, ClientCodeGenerationOptions options) {
            this.CodeGenerationHost = codeGenerationHost;
            this.DomainServiceDescriptions = domainServiceDescriptions;
            this.Options = options;

            this.enumTypesToGenerate = new HashSet<Type>();
            if(this.webContextGenerator == null)
                this.webContextGenerator = new CustomWebContextGenerator();
            if(this.domainContextGenerator == null)
                this.domainContextGenerator = new CustomDomainContextGenerator();
            if(this.entityGenerator == null)
                this.entityGenerator = new CustomEntityGenerator();
            if(this.enumGenerator == null)
                this.enumGenerator = new CustomEnumGenerator();
            if(this.complexObjectGenerator == null)
                this.complexObjectGenerator = new CustomComplexObjectGenerator();

            return this.GenerateCode();
        }
    }
}
