﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.RiaServicesCodeGen {
    internal static class CodeGenUtilities {
        private static readonly Dictionary<string, Dictionary<string, string>> NamespaceTypeReferences;
        private static readonly Dictionary<Type, string> KeywordTypeNames, TypeNamespaceTranslations;
        private static readonly HashSet<string> Keywords, TypeKeywords, NonTypeKeywords;
        internal static readonly Dictionary<Type, object[]> IntegralMinMaxValues;

        static CodeGenUtilities() {
            TypeNamespaceTranslations = new Dictionary<Type, string>();
            NamespaceTypeReferences = new Dictionary<string, Dictionary<string, string>>();
            Keywords = new HashSet<string> {
                "abstract", "as", "async", "await", "base", "bool", "break", "byte", "case", "catch", "char", "checked", "class", "const", "continue", "decimal",
                "default", "delegate", "do", "double", "else", "enum", "event", "explicit", "extern", "false", "finally", "fixed", "float", "for", "foreach", "goto",
                "if", "implicit", "in", "int", "interface", "internal", "is", "lock", "long", "namespace", "new", "null", "object", "operator", "out", "override",
                "params", "private", "protected", "public", "readonly", "ref", "return", "sbyte", "sealed", "short", "sizeof", "stackalloc", "static", "string", "struct", "switch",
                "this", "throw", "true", "try", "typeof", "uint", "ulong", "unchecked", "unsafe", "ushort", "using", "virtual", "void", "volatile", "while"
            };
            TypeKeywords = new HashSet<string> {
                "bool", "byte", "sbyte", "char", "decimal", "double", "float", "int", "uint", "long", "ulong", "object", "short", "ushort", "string", "void"
            };
            NonTypeKeywords = new HashSet<string>(Keywords.Except(TypeKeywords));
            KeywordTypeNames = new Dictionary<Type, string> {
                {
                    typeof(bool), "bool"
                    },
                {
                    typeof(sbyte), "sbyte"
                    },
                {
                    typeof(byte), "byte"
                    },
                {
                    typeof(ushort), "ushort"
                    },
                {
                    typeof(short), "short"
                    },
                {
                    typeof(int), "int"
                    },
                {
                    typeof(uint), "uint"
                    },
                {
                    typeof(long), "long"
                    },
                {
                    typeof(ulong), "ulong"
                    },
                {
                    typeof(char), "char"
                    },
                {
                    typeof(double), "double"
                    },
                {
                    typeof(float), "float"
                    },
                {
                    typeof(string), "string"
                    }
            };
            IntegralMinMaxValues = new Dictionary<Type, object[]> {
                {
                    typeof(byte), new object[] {
                        (byte)0, (byte)0xff, (byte)0
                    }
                    },
                {
                    typeof(sbyte), new object[] {
                        (sbyte)(-128), (sbyte)0x7f, (sbyte)0
                    }
                    },
                {
                    typeof(short), new object[] {
                        (short)(-32768), (short)0x7fff, (short)0
                    }
                    },
                {
                    typeof(ushort), new object[] {
                        (ushort)0, (ushort)0xffff, (ushort)0
                    }
                    },
                {
                    typeof(int), new object[] {
                        -2147483648, 0x7fffffff, 0
                    }
                    },
                {
                    typeof(uint), new object[] {
                        0, uint.MaxValue, 0
                    }
                    },
                {
                    typeof(long), new object[] {
                        -9223372036854775808L, 0x7fffffffffffffffL, 0L
                    }
                    },
                {
                    typeof(ulong), new object[] {
                        (ulong)0L, ulong.MaxValue, (ulong)0L
                    }
                    }
            };
        }

        internal static string GetBooleanString(bool value, bool isCSharp) {
            if(!isCSharp)
                return value.ToString();
            return value ? "true" : "false";
        }

        internal static Type[] GetDictionaryGenericArgumentTypes(Type type) {
            Type type2;
            return typeof(IDictionary<,>).DefinitionIsAssignableFrom(type, out type2) ? type2.GetGenericArguments() : null;
        }

        private static string GetGenericTypeName(Type type) {
            var str = type.ToString();
            var index = str.IndexOf('`');
            if(index < 0)
                return null;
            str = str.Substring(0, index);
            var genericArguments = type.GetGenericArguments();
            var str2 = string.Empty;
            for(var i = 0; i < genericArguments.Length; i++) {
                str2 = str2 + GetTypeName(genericArguments[i]);
                if((i + 1) < genericArguments.Length)
                    str2 = str2 + ", ";
            }
            return (str + "<" + str2 + ">");
        }

        internal static string GetSafeName(string name) {
            name = (Keywords.Contains(name) ? "@" : string.Empty) + name;
            return name;
        }

        internal static string GetTypeName(Type type) {
            var str = string.Empty;
            if(type == typeof(void))
                str = "void";
            else if(type.IsArray) {
                var genericTypeName = GetGenericTypeName(type);
                if(genericTypeName != null)
                    str = genericTypeName + "[]";
            } else if(type.IsGenericType)
                str = GetGenericTypeName(type);
            else
                KeywordTypeNames.TryGetValue(type, out str);
            if(string.IsNullOrEmpty(str))
                str = string.IsNullOrEmpty(type.FullName) ? type.ToString() : type.FullName;
            return GetValidTypeName(str);
        }

        internal static string GetTypeNameInGlobalNamespace(Type type) {
            var typeName = GetTypeName(type);
            return ("global::" + typeName);
        }

        private static string GetValidTypeName(string typeName) {
            var str = typeName;
            var str2 = typeName;
            var startIndex = typeName.LastIndexOf('.') + 1;
            if(startIndex > 0)
                str = typeName.Substring(startIndex);
            if(!string.IsNullOrEmpty(str) && NonTypeKeywords.Contains(str)) {
                str = "@" + str;
                str2 = typeName.Substring(0, startIndex) + str;
            }
            return str2;
        }

        internal static string MakeCompliantFieldName(string fieldName) {
            fieldName = fieldName.StartsWith("@", StringComparison.Ordinal) ? fieldName.Substring(1) : fieldName;
            var length = fieldName.Length;
            if(char.IsLower(fieldName[0]))
                return ("_" + fieldName);
            for(var i = 0; i < (length - 1); i++)
                if(char.IsLower(fieldName[i + 1])) {
                    if(i == 0)
                        i = 1;
                    return (fieldName.Substring(0, i).ToLowerInvariant() + fieldName.Substring(i));
                }
            return (fieldName.ToLowerInvariant());
        }

        internal static bool RegisterTypeName(Type type, string containingNamespace) {
            if(type.IsGenericType)
                type = type.GetGenericTypeDefinition();
            var name = type.Name;
            var str2 = type.Namespace;
            if(containingNamespace == null)
                containingNamespace = string.Empty;
            if(!NamespaceTypeReferences.ContainsKey(containingNamespace))
                NamespaceTypeReferences.Add(containingNamespace, new Dictionary<string, string>(StringComparer.Ordinal));
            string str3;
            var str4 = string.IsNullOrEmpty(str2) ? name : (str2 + "." + name);
            var flag = NamespaceTypeReferences[containingNamespace].TryGetValue(name, out str3) && (str3 != str4);
            if(!flag && (str3 == null))
                NamespaceTypeReferences[containingNamespace].Add(name, str4);
            return flag;
        }

        internal static string TranslateNamespace(Type type) {
            string str;
            if(!TypeNamespaceTranslations.TryGetValue(type, out str)) {
                str = type.Namespace;
                TypeNamespaceTranslations[type] = str;
            }
            return str;
        }

        internal static Type TranslateType(Type type) {
            if(BinaryTypeUtility.IsTypeBinary(type))
                return typeof(byte[]);
            if(!type.IsArray && TypeUtility.IsPredefinedListType(type))
                return typeof(IEnumerable<>).MakeGenericType(new[] {
                    TypeUtility.GetElementType(type)
                });
            if(!TypeUtility.IsPredefinedDictionaryType(type))
                return type;
            var typeArguments = (from t in GetDictionaryGenericArgumentTypes(type)
                                 select TranslateType(t)).ToArray<Type>();
            return typeof(Dictionary<,>).MakeGenericType(typeArguments);
        }
    }
}
