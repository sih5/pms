﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Sunlight.Silverlight.RiaServicesCodeGen {
    internal interface ICustomAttributeBuilder {
        AttributeDeclaration GetAttributeDeclaration(Attribute attribute);
    }

    internal class AttributeBuilderException : Exception {
        // Methods
        public AttributeBuilderException(Exception innerException, Type attributeType, string attributePropertyName)
            : base(string.Format("An exception occurred generating the '{0}' property on attribute of type '{1}'.", attributePropertyName, attributeType), innerException) {
        }
    }

    internal class StandardCustomAttributeBuilder : ICustomAttributeBuilder {
        // Methods
        private List<PropertyMap> BuildPropertyMaps(Attribute attribute) {
            var list = new List<PropertyMap>();
            foreach(var info in attribute.GetType().GetProperties()) {
                var a = this.MapProperty(info, attribute);
                if(a == null)
                    continue;
                var property = string.Equals(a, info.Name, StringComparison.Ordinal) ? info : attribute.GetType().GetProperty(a);
                if((property != null) && CanPropertyBeSetDeclaratively(property))
                    list.Add(new PropertyMap(info, property));
            }
            return list;
        }

        private static bool CanPropertyBeSetDeclaratively(PropertyInfo property) {
            var propertyType = property.PropertyType;
            if(propertyType.IsArray && propertyType.HasElementType)
                propertyType = propertyType.GetElementType();
            return ((propertyType == typeof(Type)) || (propertyType == typeof(object))) || TypeUtility.IsPredefinedSimpleType(propertyType);
        }

        private static bool CanValueBeAssignedToType(Type type, object value) {
            if(value == null)
                return (!type.IsValueType || (type.IsGenericType && (type.GetGenericTypeDefinition() == typeof(Nullable<>))));
            var c = value.GetType();
            return type.IsAssignableFrom(c);
        }

        private static object DefaultInstanceForType(Type t) {
            if(!t.IsValueType)
                return null;
            return Activator.CreateInstance(t);
        }

        private static IEnumerable<ParameterInfo> FindBestConstructor(ICollection<PropertyMap> propertyMaps, IDictionary<string, object> currentValues, Type attributeType) {
            var constructors = attributeType.GetConstructors();
            if(propertyMaps.Count > 0)
                return constructors.Select(info => info.GetParameters()).FirstOrDefault(parameters => ParametersAcceptAllProperties(parameters, propertyMaps, currentValues));
            ParameterInfo[] infoArray3 = null;
            foreach(var infoArray4 in constructors.Select(info2 => info2.GetParameters())) {
                if(infoArray4.Length == 0)
                    return infoArray4;
                if(((infoArray4.Length == 1) && (currentValues.Count == 1)) && CanValueBeAssignedToType(infoArray4[0].ParameterType, currentValues.Values.Single()))
                    infoArray3 = infoArray4;
                else if(infoArray3 == null)
                    infoArray3 = infoArray4;
            }
            return infoArray3;
        }

        public virtual AttributeDeclaration GetAttributeDeclaration(Attribute attribute) {
            if(attribute == null)
                throw new ArgumentNullException("attribute");
            var attributeType = attribute.GetType();
            var declaration = new AttributeDeclaration(attributeType);
            var propertyMaps = this.BuildPropertyMaps(attribute);
            var propertyValues = GetPropertyValues(propertyMaps, attribute);
            var defaultValues = GetDefaultPropertyValues(propertyMaps, attribute, propertyValues);
            var list2 = GetNonDefaultProperties(propertyMaps, propertyValues, defaultValues);
            var infoArray = FindBestConstructor(GetUnsettableProperties(list2), propertyValues, attributeType);
            if(infoArray == null)
                return null;
            foreach(var info in infoArray) {
                PropertyMap item = null;
                foreach(var map2 in propertyMaps) {
                    var setter = map2.Setter;
                    if(!setter.Name.Equals(info.Name, StringComparison.OrdinalIgnoreCase) || !CanValueBeAssignedToType(info.ParameterType, propertyValues[setter.Name]))
                        continue;
                    item = map2;
                    break;
                }
                var obj2 = (item != null) ? propertyValues[item.Getter.Name] : DefaultInstanceForType(info.ParameterType);
                declaration.ConstructorArguments.Add(obj2);
                if(item != null)
                    list2.Remove(item);
            }
            list2.Sort(((x, y) => string.Compare(x.Setter.Name, y.Setter.Name, StringComparison.Ordinal)));
            foreach(var map3 in list2)
                declaration.NamedParameters.Add(map3.Setter.Name, propertyValues[map3.Getter.Name]);
            return declaration;
        }

        private static Dictionary<string, object> GetDefaultPropertyValues(IEnumerable<PropertyMap> propertyMaps, Attribute attribute, Dictionary<string, object> currentValues) {
            var type = attribute.GetType();
            Attribute attribute2 = null;
            var constructor = type.GetConstructor(new Type[0]);
            if(((constructor != null) && constructor.IsPublic) && !type.IsAbstract)
                attribute2 = Activator.CreateInstance(type) as Attribute;
            if(attribute2 != null)
                return GetPropertyValues(propertyMaps, attribute2);
            var dictionary = new Dictionary<string, object>();
            foreach(var map in propertyMaps) {
                var getter = map.Getter;
                var obj2 = currentValues[getter.Name];
                var t = (obj2 == null) ? getter.PropertyType : obj2.GetType();
                var obj3 = DefaultInstanceForType(t);
                dictionary[getter.Name] = obj3;
            }
            return dictionary;
        }

        private static List<PropertyMap> GetNonDefaultProperties(IEnumerable<PropertyMap> propertyMaps, Dictionary<string, object> currentValues, Dictionary<string, object> defaultValues) {
            var list = new List<PropertyMap>();
            foreach(var map in propertyMaps) {
                var getter = map.Getter;
                var objA = currentValues[getter.Name];
                var objB = defaultValues[getter.Name];
                if(!Equals(objA, objB))
                    list.Add(map);
            }
            return list;
        }

        private static Dictionary<string, object> GetPropertyValues(IEnumerable<PropertyMap> propertyMaps, Attribute attribute) {
            var dictionary = new Dictionary<string, object>();
            foreach(var map in propertyMaps) {
                MethodInfo method;
                var getter = map.Getter;
                object obj2 = null;
                if((attribute is DisplayAttribute) && ((getter.Name == "AutoGenerateField") || (getter.Name == "AutoGenerateFilter"))) {
                    method = attribute.GetType().GetMethod("Get" + getter.Name);
                    if((method != null) && method.IsPublic)
                        obj2 = method.Invoke(attribute, null);
                } else {
                    method = getter.GetGetMethod();
                    if((method != null) && method.IsPublic)
                        try {
                            obj2 = getter.GetValue(attribute, null);
                        } catch(TargetInvocationException exception) {
                            throw new AttributeBuilderException(exception.InnerException, attribute.GetType(), getter.Name);
                        }
                }
                dictionary[getter.Name] = obj2;
            }
            return dictionary;
        }

        private static List<PropertyMap> GetUnsettableProperties(IEnumerable<PropertyMap> propertyMaps) {
            return (from map in propertyMaps
                    let setMethod = map.Setter.GetSetMethod()
                    where (setMethod == null) || !setMethod.IsPublic
                    select map).ToList();
        }

        protected virtual string MapProperty(PropertyInfo propertyInfo, Attribute attribute) {
            var name = propertyInfo.Name;
            return !name.Equals("TypeId", StringComparison.Ordinal) ? name : null;
        }

        private static bool ParametersAcceptAllProperties(ParameterInfo[] parameters, ICollection<PropertyMap> propertyMaps, IDictionary<string, object> currentValues) {
            foreach(var map in propertyMaps) {
                var setter = map.Setter;
                var flag = parameters.Any(info2 => setter.Name.Equals(info2.Name, StringComparison.OrdinalIgnoreCase) && CanValueBeAssignedToType(info2.ParameterType, currentValues[setter.Name]));
                if(!flag)
                    return (((propertyMaps.Count == 1) && (parameters.Length == 1)) && CanValueBeAssignedToType(parameters[0].ParameterType, currentValues[propertyMaps.First().Setter.Name]));
            }
            return true;
        }

        // Nested Types
        internal class PropertyMap {
            // Fields
            private readonly PropertyInfo _getter;
            private readonly PropertyInfo _setter;

            // Methods
            public PropertyMap(PropertyInfo getter, PropertyInfo setter) {
                this._getter = getter;
                this._setter = setter;
            }

            // Properties
            public PropertyInfo Getter {
                get {
                    return this._getter;
                }
            }

            public PropertyInfo Setter {
                get {
                    return this._setter;
                }
            }
        }
    }

    internal class ValidationCustomAttributeBuilder : StandardCustomAttributeBuilder {
        protected static void RegisterSharedResources(ValidationAttribute validationAttribute, AttributeDeclaration attributeDeclaration) {
            var errorMessageResourceName = validationAttribute.ErrorMessageResourceName;
            var errorMessageResourceType = validationAttribute.ErrorMessageResourceType;
            var flag = string.IsNullOrEmpty(errorMessageResourceName);
            var type = validationAttribute.GetType();
            if(((errorMessageResourceType != null) && flag) || ((errorMessageResourceType == null) && !flag)) {
                var str2 = (errorMessageResourceType != null) ? errorMessageResourceType.Name : "<unspecified>";
                var str3 = flag ? "<unspecified>" : errorMessageResourceName;
                attributeDeclaration.Errors.Add(string.Format(CultureInfo.CurrentCulture, "The validation attribute '{0}' contains ErrorMessageResourceType='{1}' and ErrorMessageResourceName='{2}'.  You must specify values for both or neither.", new object[] {
                    type, str2, str3
                }));
            } else if(errorMessageResourceType != null) {
                var property = errorMessageResourceType.GetProperty(errorMessageResourceName);
                if(property == null)
                    attributeDeclaration.Errors.Add(string.Format(CultureInfo.CurrentCulture, "The validation attribute '{0}' contains ErrorMessageResourceName='{1}' which was not found on declared ErrorMessageResourceType '{2}'.", new object[] {
                        type, errorMessageResourceName, errorMessageResourceType
                    }));
                else {
                    attributeDeclaration.RequiredTypes.Add(errorMessageResourceType);
                    attributeDeclaration.RequiredProperties.Add(property);
                }
            }
        }

        public override AttributeDeclaration GetAttributeDeclaration(Attribute attribute) {
            var validationAttribute = (ValidationAttribute)attribute;
            var attributeDeclaration = base.GetAttributeDeclaration(attribute);
            RegisterSharedResources(validationAttribute, attributeDeclaration);
            return attributeDeclaration;
        }
    }

    internal class CustomValidationCustomAttributeBuilder : ValidationCustomAttributeBuilder {
        public override AttributeDeclaration GetAttributeDeclaration(Attribute attribute) {
            var attribute2 = (CustomValidationAttribute)attribute;
            attribute2.FormatErrorMessage(string.Empty);
            var attributeDeclaration = base.GetAttributeDeclaration(attribute);
            attributeDeclaration.RequiredTypes.Add(attribute2.ValidatorType);
            attributeDeclaration.RequiredMethods.Add(attribute2.ValidatorType.GetMethod(attribute2.Method));
            return attributeDeclaration;
        }
    }

    internal class DataMemberAttributeBuilder : StandardCustomAttributeBuilder {
        protected override string MapProperty(PropertyInfo propertyInfo, Attribute attribute) {
            return !propertyInfo.Name.Equals("Order", StringComparison.Ordinal) ? base.MapProperty(propertyInfo, attribute) : null;
        }
    }

    internal class DisplayCustomAttributeBuilder : StandardCustomAttributeBuilder {
        public override AttributeDeclaration GetAttributeDeclaration(Attribute attribute) {
            var attribute2 = (DisplayAttribute)attribute;
            var declaration = new AttributeDeclaration(typeof(DisplayAttribute));
            var attributeType = attribute.GetType();
            try {
                attribute2.GetName();
            } catch(InvalidOperationException exception) {
                throw new AttributeBuilderException(exception, attributeType, "Name");
            }
            try {
                attribute2.GetShortName();
            } catch(InvalidOperationException exception2) {
                throw new AttributeBuilderException(exception2, attributeType, "ShortName");
            }
            try {
                attribute2.GetDescription();
            } catch(InvalidOperationException exception3) {
                throw new AttributeBuilderException(exception3, attributeType, "Description");
            }
            try {
                attribute2.GetPrompt();
            } catch(InvalidOperationException exception4) {
                throw new AttributeBuilderException(exception4, attributeType, "Prompt");
            }
            if(attribute2.GetAutoGenerateField().HasValue)
                declaration.NamedParameters.Add("AutoGenerateField", attribute2.AutoGenerateField);
            if(attribute2.GetAutoGenerateFilter().HasValue)
                declaration.NamedParameters.Add("AutoGenerateFilter", attribute2.AutoGenerateFilter);
            if(!string.IsNullOrEmpty(attribute2.Description))
                declaration.NamedParameters.Add("Description", attribute2.Description);
            if(!string.IsNullOrEmpty(attribute2.GroupName))
                declaration.NamedParameters.Add("GroupName", attribute2.GroupName);
            if(!string.IsNullOrEmpty(attribute2.Name))
                declaration.NamedParameters.Add("Name", attribute2.Name);
            if(attribute2.GetOrder().HasValue)
                declaration.NamedParameters.Add("Order", attribute2.Order);
            if(!string.IsNullOrEmpty(attribute2.Prompt))
                declaration.NamedParameters.Add("Prompt", attribute2.Prompt);
            if(attribute2.ResourceType != null)
                declaration.NamedParameters.Add("ResourceType", attribute2.ResourceType);
            if(!string.IsNullOrEmpty(attribute2.ShortName))
                declaration.NamedParameters.Add("ShortName", attribute2.ShortName);
            return declaration;
        }
    }

    internal class DomainIdentifierAttributeBuilder : StandardCustomAttributeBuilder {
        protected override string MapProperty(PropertyInfo propertyInfo, Attribute attribute) {
            return !propertyInfo.Name.Equals("CodeProcessor", StringComparison.Ordinal) ? base.MapProperty(propertyInfo, attribute) : null;
        }
    }

    internal class EditableAttributeBuilder : StandardCustomAttributeBuilder {
        public override AttributeDeclaration GetAttributeDeclaration(Attribute attribute) {
            var attribute2 = (EditableAttribute)attribute;
            var declaration = new AttributeDeclaration(typeof(EditableAttribute));
            var allowEdit = attribute2.AllowEdit;
            var allowInitialValue = attribute2.AllowInitialValue;
            declaration.ConstructorArguments.Add(allowEdit);
            if(allowEdit != allowInitialValue)
                declaration.NamedParameters.Add("AllowInitialValue", allowInitialValue);
            return declaration;
        }
    }

    internal class RangeCustomAttributeBuilder : ValidationCustomAttributeBuilder {
        public override AttributeDeclaration GetAttributeDeclaration(Attribute attribute) {
            var validationAttribute = (RangeAttribute)attribute;
            var attributeDeclaration = new AttributeDeclaration(typeof(RangeAttribute));
            RegisterSharedResources(validationAttribute, attributeDeclaration);
            if((validationAttribute.Minimum == null) || (validationAttribute.Minimum is string))
                attributeDeclaration.ConstructorArguments.Add(validationAttribute.OperandType);
            attributeDeclaration.ConstructorArguments.Add(validationAttribute.Minimum);
            attributeDeclaration.ConstructorArguments.Add(validationAttribute.Maximum);
            if(validationAttribute.ErrorMessage != null)
                attributeDeclaration.NamedParameters.Add("ErrorMessage", validationAttribute.ErrorMessage);
            if(validationAttribute.ErrorMessageResourceType != null)
                attributeDeclaration.NamedParameters.Add("ErrorMessageResourceType", validationAttribute.ErrorMessageResourceType);
            if(validationAttribute.ErrorMessageResourceName != null)
                attributeDeclaration.NamedParameters.Add("ErrorMessageResourceName", validationAttribute.ErrorMessageResourceName);
            return attributeDeclaration;
        }
    }

    internal class UIHintCustomAttributeBuilder : StandardCustomAttributeBuilder {
        public override AttributeDeclaration GetAttributeDeclaration(Attribute attribute) {
            var attribute2 = (UIHintAttribute)attribute;
            var controlParameters = attribute2.ControlParameters;
            if(controlParameters.Count == 0)
                return base.GetAttributeDeclaration(attribute);
            var declaration = new AttributeDeclaration(typeof(UIHintAttribute));
            declaration.ConstructorArguments.Add(attribute2.UIHint);
            declaration.ConstructorArguments.Add(attribute2.PresentationLayer);
            foreach(var pair in controlParameters) {
                declaration.ConstructorArguments.Add(pair.Key);
                declaration.ConstructorArguments.Add(pair.Value);
            }
            return declaration;
        }
    }
}
