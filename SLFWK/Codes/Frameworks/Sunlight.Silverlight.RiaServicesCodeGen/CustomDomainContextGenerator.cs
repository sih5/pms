﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.DomainServices.Hosting;
using System.ServiceModel.DomainServices.Server;
using System.ServiceModel.DomainServices.Server.ApplicationServices;

// ReSharper disable BitwiseOperatorOnEnumWihtoutFlags

namespace Sunlight.Silverlight.RiaServicesCodeGen {
    public sealed class CustomDomainContextGenerator {
        private const string CLASS_NAME_ENTITY_CONTAINER = "EntityContainer";

        internal static string GetDomainContextTypeName(DomainServiceDescription domainServiceDescription) {
            var name = domainServiceDescription.DomainServiceType.Name;
            if(name.EndsWith("Service", StringComparison.Ordinal))
                name = name.Substring(0, name.Length - 7) + "Context";
            return name;
        }

        private static string GetEntityContainerTypeName(DomainServiceDescription domainServiceDescription) {
            return GetDomainContextTypeName(domainServiceDescription) + CLASS_NAME_ENTITY_CONTAINER;
        }

        private static bool OperationHasSideEffects(DomainOperationEntry operation) {
            if(operation.Operation == DomainOperation.Query)
                return ((QueryAttribute)operation.OperationAttribute).HasSideEffects;
            return ((operation.Operation == DomainOperation.Invoke) && ((InvokeAttribute)operation.OperationAttribute).HasSideEffects);
        }

        private List<DomainOperationEntry> queryMethods, domainOperations;
        private List<Type> entitySets;

        private CustomClientCodeGenerator ClientCodeGenerator {
            get;
            set;
        }

        private DomainServiceDescription DomainServiceDescription {
            get;
            set;
        }

        private string ContractInterfaceName {
            get;
            set;
        }

        private AttributeCollection Attributes {
            get;
            set;
        }

        private bool RegisterEnumTypeIfNecessary(Type type, DomainOperationEntry domainOperationEntry) {
            var nonNullableType = TypeUtility.GetNonNullableType(type);
            if(nonNullableType.IsEnum) {
                string errorMessage;
                if(!this.ClientCodeGenerator.CanExposeEnumType(nonNullableType, out errorMessage)) {
                    this.ClientCodeGenerator.CodeGenerationHost.LogError(string.Format(CultureInfo.CurrentCulture, "DomainOperationEntry '{0}' in DomainService '{1}' will not be generated because it uses the Enum type '{2}'.  {3}", new object[] {
                        domainOperationEntry.Name, GetDomainContextTypeName(this.DomainServiceDescription), nonNullableType.FullName, errorMessage
                    }));
                    return false;
                }
                this.ClientCodeGenerator.AddEnumTypeToGenerate(nonNullableType);
            }
            return true;
        }

        private bool CanGenerateDomainOperationEntry(DomainOperationEntry domainOperationEntry) {
            foreach(var nonNullableType in domainOperationEntry.Parameters.Select(parameter => TypeUtility.GetNonNullableType(parameter.ParameterType)).Where(nonNullableType => nonNullableType.IsEnum)) {
                string errorMessage;
                if(!this.ClientCodeGenerator.CanExposeEnumType(nonNullableType, out errorMessage)) {
                    this.ClientCodeGenerator.CodeGenerationHost.LogError(string.Format(CultureInfo.CurrentCulture, "DomainOperationEntry '{0}' in DomainService '{1}' will not be generated because it uses the Enum type '{2}'.  {3}", new object[] {
                        domainOperationEntry.Name, GetDomainContextTypeName(this.DomainServiceDescription), nonNullableType.FullName, errorMessage
                    }));
                    return false;
                }
                this.ClientCodeGenerator.AddEnumTypeToGenerate(nonNullableType);
            }
            return true;
        }

        private void InitDomainContextData() {
            var set = new HashSet<Type>();
            this.queryMethods = new List<DomainOperationEntry>();
            this.domainOperations = new List<DomainOperationEntry>();
            this.entitySets = new List<Type>();
            foreach(var entry in from m in this.DomainServiceDescription.DomainOperationEntries
                                 where m.Operation == DomainOperation.Query
                                 orderby m.Name
                                 select m) {
                if(!this.CanGenerateDomainOperationEntry(entry))
                    continue;
                this.queryMethods.Add(entry);

                var entityType = TypeUtility.GetElementType(entry.ReturnType);
                if(set.Contains(entityType))
                    continue;

                set.Add(entityType);
                Func<PropertyDescriptor, bool> predicate = p => p.ComponentType != entityType;
                var flag = this.DomainServiceDescription.GetParentAssociations(entityType).Any(predicate);
                var rootEntityType = this.DomainServiceDescription.GetRootEntityType(entityType);
                if(!flag && (rootEntityType == entityType))
                    this.entitySets.Add(entityType);
            }
            foreach(var entry in this.DomainServiceDescription.EntityTypes.OrderBy(e => e.Name).SelectMany(type => this.DomainServiceDescription.GetCustomMethods(type).Where(this.CanGenerateDomainOperationEntry)))
                this.domainOperations.Add(entry);
        }

        private void Initialize() {
            var name = this.DomainServiceDescription.DomainServiceType.Name;
            this.ContractInterfaceName = "I" + name + "Contract";
            this.Attributes = this.DomainServiceDescription.Attributes;
            this.InitDomainContextData();
        }

        private string GetEntitySetOperationsEnumValue(Type entityType) {
            var canInsert = this.DomainServiceDescription.IsOperationSupported(entityType, DomainOperation.Insert);
            var canUpdate = this.DomainServiceDescription.IsOperationSupported(entityType, DomainOperation.Update);
            var canDelete = this.DomainServiceDescription.IsOperationSupported(entityType, DomainOperation.Delete);
            string str = null;
            if((!canInsert && !canUpdate) && !canDelete)
                return "EntitySetOperations.None";
            if((canInsert && canUpdate) && canDelete)
                return "EntitySetOperations.All";
            if(canInsert)
                str = "EntitySetOperations.Add";
            if(canUpdate)
                str = str + ((str == null) ? string.Empty : " | ") + "EntitySetOperations.Edit";
            if(canDelete)
                str = str + ((str == null) ? string.Empty : " | ") + "EntitySetOperations.Remove";
            return str;
        }

        private bool TypeRequiresRegistration(Type type) {
            if(type.IsPrimitive || (type == typeof(string)))
                return false;
            return !this.DomainServiceDescription.EntityTypes.Contains(type);
        }

        private IEnumerable<Attribute> GetContractServiceKnownTypes(DomainOperationEntry operation, ISet<Type> registeredTypes) {
            var list = new List<Attribute>();
            foreach(var parameter in operation.Parameters) {
                var nonNullableType = TypeUtility.GetNonNullableType(CodeGenUtilities.TranslateType(parameter.ParameterType));
                if(TypeUtility.IsPredefinedListType(nonNullableType) || TypeUtility.IsComplexTypeCollection(nonNullableType)) {
                    var elementType = TypeUtility.GetElementType(nonNullableType);
                    if(elementType != null)
                        nonNullableType = elementType.MakeArrayType();
                }
                if(registeredTypes.Contains(nonNullableType) || !this.TypeRequiresRegistration(nonNullableType))
                    continue;
                registeredTypes.Add(nonNullableType);
                if(nonNullableType.IsEnum && this.ClientCodeGenerator.NeedToGenerateEnumType(nonNullableType)) {
                    this.ClientCodeGenerator.AddEnumTypeToGenerate(nonNullableType);
                    nonNullableType = new VirtualType(nonNullableType.Name, CodeGenUtilities.TranslateNamespace(nonNullableType), nonNullableType.Assembly, nonNullableType.BaseType);
                }
                list.Add(new ServiceKnownTypeAttribute(nonNullableType));
            }
            return list;
        }

        private CodeMemberProperty GenerateEntitySet(Type entityType) {
            var property = new CodeMemberProperty {
                Name = Naming.MakePluralName(entityType.Name),
                Attributes = MemberAttributes.Public | MemberAttributes.Final,
                Type = new CodeTypeReference("EntitySet", new CodeTypeReference(entityType.Name, CodeTypeReferenceOptions.GenericTypeParameter)),
            };
            property.Comments.Add(new CodeCommentStatement("<summary>获取已载入到这个 <see cref=\"" + GetDomainContextTypeName(this.DomainServiceDescription) + "\"/> 实例中的 <see cref=\"" + entityType.Name + "\"/> 实体集。</summary>", true));
            property.GetStatements.Add(new CodeMethodReturnStatement(new CodeMethodInvokeExpression(new CodePropertyReferenceExpression(new CodeThisReferenceExpression(), CLASS_NAME_ENTITY_CONTAINER), "GetEntitySet<" + entityType.Name + ">")));
            return property;
        }

        private IEnumerable<CodeTypeMember> GenerateEntitySets() {
            return this.DomainServiceDescription.EntityTypes.OrderBy(t => t.FullName).Select(this.GenerateEntitySet);
        }

        private IEnumerable<CodeTypeMember> GenerateExtensibilityMethods() {
            var method = new CodeSnippetTypeMember("        partial void OnCreated();");
            yield return method;
        }

        private IEnumerable<CodeConstructor> GenerateConstructors() {
            // ReSharper disable PossibleNullReferenceException
            var requiresSecureEndpoint = this.Attributes.OfType<EnableClientAccessAttribute>().Single().RequiresSecureEndpoint;
            var svcName = string.Format("{0}.svc", this.DomainServiceDescription.DomainServiceType.FullName.Replace('.', '-'));
            // ReSharper restore PossibleNullReferenceException

            var domainClientClassName = "WebDomainClient<" + this.ContractInterfaceName + ">";

            var ctor = new CodeConstructor {
                Attributes = MemberAttributes.Private,
            };
            ctor.Parameters.Add(new CodeParameterDeclarationExpression("DomainClient", "domainClient"));
            ctor.BaseConstructorArgs.Add(new CodeVariableReferenceExpression("domainClient"));
            ctor.Statements.Add(new CodeVariableDeclarationStatement("var", "binding", new CodeSnippetExpression("((" + domainClientClassName + ")this.DomainClient).ChannelFactory.Endpoint.Binding")));
            ctor.Statements.Add(new CodeVariableDeclarationStatement("var", "timeout", new CodeMethodInvokeExpression(new CodeTypeReferenceExpression("TimeSpan"), "FromMinutes", new CodeSnippetExpression("Debugger.IsAttached ? 15 : 5"))));
            foreach(var propertyName in new[] {
                "OpenTimeout", "CloseTimeout", "SendTimeout"
            })
                ctor.Statements.Add(new CodeAssignStatement(new CodePropertyReferenceExpression(new CodeVariableReferenceExpression("binding"), propertyName), new CodeVariableReferenceExpression("timeout")));
            ctor.Statements.Add(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), "OnCreated"));
            yield return ctor;

            var expressions = new List<CodeExpression> {
                new CodeVariableReferenceExpression("uri")
            };
            if(requiresSecureEndpoint)
                expressions.Add(new CodePrimitiveExpression(true));

            ctor = new CodeConstructor {
                Attributes = MemberAttributes.Public,
            };
            ctor.Parameters.Add(new CodeParameterDeclarationExpression("Uri", "uri"));
            ctor.ChainedConstructorArgs.Add(new CodeObjectCreateExpression(domainClientClassName, expressions.ToArray()));
            ctor.Statements.Add(new CodeCommentStatement("DO NOTHING"));
            yield return ctor;

            expressions.Clear();
            expressions.Add(new CodeObjectCreateExpression("Uri", new CodePrimitiveExpression(svcName), new CodeSnippetExpression("UriKind.Relative")));
            if(requiresSecureEndpoint)
                expressions.Add(new CodePrimitiveExpression(true));
            ctor = new CodeConstructor {
                Attributes = MemberAttributes.Public,
            };
            ctor.ChainedConstructorArgs.Add(new CodeObjectCreateExpression(domainClientClassName, expressions.ToArray()));
            ctor.Statements.Add(new CodeCommentStatement("DO NOTHING"));
            yield return ctor;
        }

        private IEnumerable<CodeParameterDeclarationExpression> GenerateOperationParameters(IEnumerable<DomainOperationParameter> parameters, bool generateAttributes = true) {
            foreach(var parameter in parameters) {
                var parameterDeclaration = new CodeParameterDeclarationExpression(parameter.ParameterType, parameter.Name);
                if(generateAttributes)
                    parameterDeclaration.CustomAttributes.AddRange(this.ClientCodeGenerator.GenerateAttributes(parameter.Attributes.OfType<Attribute>()).ToArray());
                yield return parameterDeclaration;
            }
        }

        private IEnumerable<CodeMemberMethod> GenerateQueryMethods(DomainOperationEntry entry) {
            var genericTypeRef = new CodeTypeReference(TypeUtility.GetElementType(entry.ReturnType).Name, CodeTypeReferenceOptions.GenericTypeParameter);
            var method = new CodeMemberMethod {
                Name = entry.Name + "Query",
                Attributes = MemberAttributes.Public | MemberAttributes.Final,
                ReturnType = new CodeTypeReference("EntityQuery", genericTypeRef),
            };
            method.CustomAttributes.AddRange(this.ClientCodeGenerator.GenerateAttributes(entry.Attributes.OfType<Attribute>()).ToArray());
            method.Parameters.AddRange(this.GenerateOperationParameters(entry.Parameters).ToArray());
            CodeExpression parametersVariableExpression = new CodePrimitiveExpression(null);
            if(entry.Parameters.Count > 0) {
                const string VARIABLE_NAME = "parameters";
                parametersVariableExpression = new CodeVariableReferenceExpression(VARIABLE_NAME);
                method.Statements.Add(new CodeVariableDeclarationStatement("var", VARIABLE_NAME, new CodeObjectCreateExpression(typeof(Dictionary<string, object>), new CodePrimitiveExpression(entry.Parameters.Count))));
                foreach(var parameter in entry.Parameters)
                    method.Statements.Add(new CodeMethodInvokeExpression(new CodeVariableReferenceExpression(VARIABLE_NAME), "Add", new CodePrimitiveExpression(parameter.Name), new CodeVariableReferenceExpression(parameter.Name)));
            }
            var operationAttribute = (QueryAttribute)entry.OperationAttribute;
            method.Statements.Add(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), "ValidateMethod", new CodePrimitiveExpression(method.Name), parametersVariableExpression));
            method.Statements.Add(new CodeMethodReturnStatement(new CodeMethodInvokeExpression(new CodeMethodReferenceExpression(new CodeThisReferenceExpression(), "CreateQuery", genericTypeRef), new CodePrimitiveExpression(entry.Name), parametersVariableExpression, new CodePrimitiveExpression(operationAttribute.HasSideEffects), new CodePrimitiveExpression(operationAttribute.IsComposable))));

            foreach(var parameter in entry.Parameters)
                this.RegisterEnumTypeIfNecessary(parameter.ParameterType, entry);

            yield return method;
        }

        private CodeMemberMethod GenerateCustomMethod(DomainOperationEntry entry) {
            var method = new CodeMemberMethod {
                Name = entry.Name,
                Attributes = MemberAttributes.Public | MemberAttributes.Final,
            };
            method.Parameters.AddRange(this.GenerateOperationParameters(entry.Parameters).ToArray());
            method.Statements.Add(new CodeMethodInvokeExpression(new CodeVariableReferenceExpression(entry.Parameters[0].Name), entry.Name, entry.Parameters.Skip(1).Select(p => new CodeVariableReferenceExpression(p.Name)).ToArray()));
            return method;
        }

        private CodeMemberMethod GenerateInvokeOperation(DomainOperationEntry entry, bool generateCallback) {
            var method = new CodeMemberMethod {
                Name = entry.Name,
                Attributes = MemberAttributes.Public | MemberAttributes.Final,
                ReturnType = entry.ReturnType == typeof(void) ? new CodeTypeReference("InvokeOperation") : new CodeTypeReference("InvokeOperation", new CodeTypeReference(entry.ReturnType, CodeTypeReferenceOptions.GenericTypeParameter)),
            };
            method.Parameters.AddRange(this.GenerateOperationParameters(entry.Parameters).ToArray());
            if(generateCallback) {
                method.Parameters.Add(new CodeParameterDeclarationExpression(new CodeTypeReference("Action", method.ReturnType), "callback"));
                method.Parameters.Add(new CodeParameterDeclarationExpression(typeof(object), "userState"));
            }
            CodeExpression parametersVariableExpression = new CodePrimitiveExpression();
            if(entry.Parameters.Count > 0) {
                const string VARIABLE_NAME = "parameters";
                parametersVariableExpression = new CodeVariableReferenceExpression(VARIABLE_NAME);
                method.Statements.Add(new CodeVariableDeclarationStatement("var", VARIABLE_NAME, new CodeObjectCreateExpression(typeof(Dictionary<string, object>), new CodePrimitiveExpression(entry.Parameters.Count))));
                foreach(var parameter in entry.Parameters)
                    method.Statements.Add(new CodeMethodInvokeExpression(new CodeVariableReferenceExpression(VARIABLE_NAME), "Add", new CodePrimitiveExpression(parameter.Name), new CodeVariableReferenceExpression(parameter.Name)));
            }
            var operationAttribute = (InvokeAttribute)entry.OperationAttribute;
            method.Statements.Add(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), "ValidateMethod", new CodePrimitiveExpression(method.Name), parametersVariableExpression));
            CodeExpression callbackVariableExpression, userStateVariableExpression;
            if(generateCallback) {
                callbackVariableExpression = new CodeVariableReferenceExpression("callback");
                userStateVariableExpression = new CodeVariableReferenceExpression("userState");
            } else
                callbackVariableExpression = userStateVariableExpression = new CodePrimitiveExpression();
            var invokeExpression = new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), "InvokeOperation", new CodePrimitiveExpression(entry.Name), new CodeTypeOfExpression(entry.ReturnType), parametersVariableExpression, new CodePrimitiveExpression(operationAttribute.HasSideEffects), callbackVariableExpression, userStateVariableExpression);
            method.Statements.Add(method.ReturnType.TypeArguments.Count > 0 ? new CodeMethodReturnStatement(new CodeCastExpression(method.ReturnType, invokeExpression)) : new CodeMethodReturnStatement(invokeExpression));

            foreach(var parameter in entry.Parameters)
                this.RegisterEnumTypeIfNecessary(parameter.ParameterType, entry);
            this.RegisterEnumTypeIfNecessary(entry.ReturnType, entry);

            return method;
        }

        private IEnumerable<CodeTypeMember> GenerateMethods() {
            var method = new CodeMemberMethod {
                Name = "CreateEntityContainer",
                Attributes = MemberAttributes.Override | MemberAttributes.Family,
                ReturnType = new CodeTypeReference(CLASS_NAME_ENTITY_CONTAINER),
            };
            method.Statements.Add(new CodeMethodReturnStatement(new CodeObjectCreateExpression(GetEntityContainerTypeName(this.DomainServiceDescription))));
            yield return method;

            foreach(var queryMethod in this.queryMethods.SelectMany(this.GenerateQueryMethods))
                yield return queryMethod;

            foreach(var entry in this.domainOperations)
                yield return this.GenerateCustomMethod(entry);

            foreach(var entry in from m in this.DomainServiceDescription.DomainOperationEntries
                                 where m.Operation == DomainOperation.Invoke
                                 orderby m.Name
                                 select m) {
                yield return this.GenerateInvokeOperation(entry, false);
                yield return this.GenerateInvokeOperation(entry, true);
            }
        }

        private CodeTypeMember[] GenerateBody() {
            var result = new List<CodeTypeMember>();
            result.AddRange(this.GenerateEntitySets());
            result.AddRange(this.GenerateExtensibilityMethods());
            result.AddRange(this.GenerateConstructors());
            result.AddRange(this.GenerateMethods());
            return result.ToArray();
        }

        private CodeTypeDeclaration GenerateEntityContainerClassDeclaration() {
            var typeDeclaration = new CodeTypeDeclaration(GetEntityContainerTypeName(this.DomainServiceDescription)) {
                IsClass = true,
                TypeAttributes = TypeAttributes.Public | TypeAttributes.Sealed,
            };
            typeDeclaration.BaseTypes.Add(CLASS_NAME_ENTITY_CONTAINER);
            var ctor = new CodeConstructor {
                Attributes = MemberAttributes.Public,
            };
            ctor.Statements.AddRange(this.DomainServiceDescription.EntityTypes.OrderBy(t => t.FullName).Select(t => new CodeExpressionStatement(new CodeMethodInvokeExpression(new CodeMethodReferenceExpression(new CodeThisReferenceExpression(), "CreateEntitySet", new CodeTypeReference(t.Name)), new CodeSnippetExpression(this.GetEntitySetOperationsEnumValue(t))))).ToArray());
            typeDeclaration.Members.Add(ctor);
            return typeDeclaration;
        }

        private IEnumerable<CodeAttributeDeclaration> GenerateContractMethodAttributes(string operationName) {
            var name = this.DomainServiceDescription.DomainServiceType.Name;
            yield return new CodeAttributeDeclaration("FaultContract", new CodeAttributeArgument(new CodeTypeOfExpression("DomainServiceFault")), new CodeAttributeArgument("Action", new CodePrimitiveExpression(string.Format("http://tempuri.org/{0}/{1}{2}", name, operationName, typeof(DomainServiceFault).Name))), new CodeAttributeArgument("Name", new CodePrimitiveExpression(typeof(DomainServiceFault).Name)), new CodeAttributeArgument("Namespace", new CodePrimitiveExpression("DomainServices")));
            yield return new CodeAttributeDeclaration("OperationContract", new CodeAttributeArgument("AsyncPattern", new CodePrimitiveExpression(true)), new CodeAttributeArgument("Action", new CodePrimitiveExpression(string.Format("http://tempuri.org/{0}/{1}", name, operationName))), new CodeAttributeArgument("ReplyAction", new CodePrimitiveExpression(string.Format("http://tempuri.org/{0}/{1}Response", name, operationName))));
        }

        private IEnumerable<CodeMemberMethod> GenerateContractMethods(DomainOperationEntry entry) {
            var method = new CodeMemberMethod {
                Name = "Begin" + entry.Name,
                ReturnType = new CodeTypeReference("IAsyncResult"),
            };
            if(!OperationHasSideEffects(entry))
                method.CustomAttributes.Add(new CodeAttributeDeclaration("WebGet"));
            method.CustomAttributes.AddRange(this.GenerateContractMethodAttributes(entry.Name).ToArray());

            foreach(var parameter in entry.Parameters)
                method.Parameters.Add(new CodeParameterDeclarationExpression(parameter.ParameterType, parameter.Name));
            method.Parameters.Add(new CodeParameterDeclarationExpression("AsyncCallback", "callback"));
            method.Parameters.Add(new CodeParameterDeclarationExpression(typeof(object), "asyncState"));
            yield return method;

            method = new CodeMemberMethod {
                Name = "End" + entry.Name,
            };
            if(entry.Operation == DomainOperation.Query)
                if(entry.ReturnType == typeof(void))
                    method.ReturnType = new CodeTypeReference("QueryResult");
                else {
                    var genericType = CodeGenUtilities.TranslateType(entry.AssociatedType);
                    method.ReturnType = new CodeTypeReference("QueryResult", TypeUtility.IsPredefinedSimpleType(genericType) && genericType.Assembly.IsSystemAssembly() ? new CodeTypeReference(genericType, CodeTypeReferenceOptions.GenericTypeParameter) : new CodeTypeReference(genericType.Name, CodeTypeReferenceOptions.GenericTypeParameter));
                }
            else
                method.ReturnType = new CodeTypeReference(CodeGenUtilities.TranslateType(entry.ReturnType));

            method.Parameters.Add(new CodeParameterDeclarationExpression("IAsyncResult", "result"));
            yield return method;
        }

        private CodeTypeDeclaration GenerateServiceContractInterface() {
            var typeDeclaration = new CodeTypeDeclaration(this.ContractInterfaceName) {
                IsPartial = false,
                IsInterface = true,
                IsClass = false,
                TypeAttributes = TypeAttributes.Public | TypeAttributes.Interface,
            };
            typeDeclaration.CustomAttributes.Add(new CodeAttributeDeclaration("ServiceContract"));

            var registeredTypes = new HashSet<Type>();
            var list = new List<DomainOperationEntry>();
            foreach(var entry in this.DomainServiceDescription.DomainOperationEntries.Where(op => !(op.Operation != DomainOperation.Query && op.Operation != DomainOperation.Invoke) || op.Operation == DomainOperation.Custom).OrderBy(op => op.Name))
                if(entry.Operation == DomainOperation.Custom)
                    typeDeclaration.CustomAttributes.AddRange(this.ClientCodeGenerator.GenerateAttributes(this.GetContractServiceKnownTypes(entry, registeredTypes), true).ToArray());
                else
                    list.Add(entry);
            foreach(var entry in list)
                typeDeclaration.Members.AddRange(this.GenerateContractMethods(entry).ToArray());

            if(this.DomainServiceDescription.DomainOperationEntries.Any(op => !(op.Operation != DomainOperation.Delete && op.Operation != DomainOperation.Insert && op.Operation != DomainOperation.Update) || op.Operation == DomainOperation.Custom)) {
                var method = new CodeMemberMethod {
                    Name = "BeginSubmitChanges",
                    ReturnType = new CodeTypeReference("IAsyncResult"),
                };
                method.CustomAttributes.AddRange(this.GenerateContractMethodAttributes("SubmitChanges").ToArray());
                method.Parameters.Add(new CodeParameterDeclarationExpression("IEnumerable<ChangeSetEntry>", "changeSet"));
                method.Parameters.Add(new CodeParameterDeclarationExpression("AsyncCallback", "callback"));
                method.Parameters.Add(new CodeParameterDeclarationExpression(typeof(object), "asyncState"));
                typeDeclaration.Members.Add(method);

                method = new CodeMemberMethod {
                    Name = "EndSubmitChanges",
                    ReturnType = new CodeTypeReference("IEnumerable<ChangeSetEntry>"),
                };
                method.Parameters.Add(new CodeParameterDeclarationExpression("IAsyncResult", "result"));
                typeDeclaration.Members.Add(method);
            }

            return typeDeclaration;
        }

        private CodeTypeDeclaration GenerateClassDeclaration() {
            var typeDeclaration = new CodeTypeDeclaration(GetDomainContextTypeName(this.DomainServiceDescription)) {
                IsPartial = true,
                IsClass = true,
                TypeAttributes = TypeAttributes.Public | TypeAttributes.Sealed,
            };
            typeDeclaration.BaseTypes.Add(typeof(IAuthentication<>).DefinitionIsAssignableFrom(this.DomainServiceDescription.DomainServiceType) ? "AuthenticationDomainContextBase" : "DomainContext");
            typeDeclaration.Members.Add(this.GenerateServiceContractInterface());
            typeDeclaration.Members.Add(this.GenerateEntityContainerClassDeclaration());
            typeDeclaration.Members.AddRange(this.GenerateBody());
            return typeDeclaration;
        }

        private CodeNamespace GenerateNamespace() {
            var ns = new CodeNamespace(this.DomainServiceDescription.DomainServiceType.Namespace);
            ns.Imports.Add(new CodeNamespaceImport("System"));
            ns.Imports.Add(new CodeNamespaceImport("System.Collections.Generic"));
            ns.Imports.Add(new CodeNamespaceImport("System.ComponentModel"));
            ns.Imports.Add(new CodeNamespaceImport("System.ComponentModel.DataAnnotations"));
            ns.Imports.Add(new CodeNamespaceImport("System.Diagnostics"));
            ns.Imports.Add(new CodeNamespaceImport("System.Linq"));
            ns.Imports.Add(new CodeNamespaceImport("System.ServiceModel"));
            ns.Imports.Add(new CodeNamespaceImport("System.ServiceModel.DomainServices.Client"));
            ns.Imports.Add(new CodeNamespaceImport("System.ServiceModel.DomainServices.Client.ApplicationServices"));
            ns.Imports.Add(new CodeNamespaceImport("System.ServiceModel.Web"));
            ns.Types.Add(this.GenerateClassDeclaration());
            return ns;
        }

        public CodeNamespace Generate(CustomClientCodeGenerator clientCodeGenerator, DomainServiceDescription domainServiceDescription) {
            this.ClientCodeGenerator = clientCodeGenerator;
            this.DomainServiceDescription = domainServiceDescription;
            this.Initialize();
            return this.GenerateNamespace();
        }
    }
}

// ReSharper restore BitwiseOperatorOnEnumWihtoutFlags
