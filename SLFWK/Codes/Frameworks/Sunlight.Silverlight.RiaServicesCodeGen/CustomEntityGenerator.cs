﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.ServiceModel.DomainServices;
using System.ServiceModel.DomainServices.Server;
using System.ServiceModel.DomainServices.Server.ApplicationServices;

namespace Sunlight.Silverlight.RiaServicesCodeGen {
    // ReSharper disable BitwiseOperatorOnEnumWihtoutFlags
    public sealed class CustomEntityGenerator : DataContractProxyGenerator {
        private class DomainServiceDescriptionAggregate {
            private readonly HashSet<Type> complexTypes;
            private readonly HashSet<Type> entityTypes;

            //private IEnumerable<DomainServiceDescription> DomainServiceDescriptions {
            //    get;
            //    set;
            //}

            internal IEnumerable<Type> EntityTypes {
                get {
                    return this.entityTypes;
                }
            }

            //internal Type GetRootEntityType(Type entityType) {
            //    Type type = null;
            //    while(entityType != null) {
            //        if(this.EntityTypes.Contains(entityType))
            //            type = entityType;
            //        entityType = entityType.BaseType;
            //    }
            //    return type;
            //}

            internal IEnumerable<Type> ComplexTypes {
                get {
                    return this.complexTypes;
                }
            }

            //internal bool IsShared {
            //    get {
            //        return (this.DomainServiceDescriptions.Count() > 1);
            //    }
            //}

            internal DomainServiceDescriptionAggregate(IEnumerable<DomainServiceDescription> domainServiceDescriptions) {
                // ReSharper disable PossibleMultipleEnumeration
                //this.DomainServiceDescriptions = domainServiceDescriptions;
                this.complexTypes = new HashSet<Type>();
                this.entityTypes = new HashSet<Type>();
                foreach(var description in domainServiceDescriptions) {
                    foreach(var type in description.ComplexTypes)
                        this.complexTypes.Add(type);
                    foreach(var type2 in description.EntityTypes)
                        this.entityTypes.Add(type2);
                }
                // ReSharper restore PossibleMultipleEnumeration
            }

            internal Type GetEntityBaseType(Type entityType) {
                var baseType = entityType.BaseType;
                while(baseType != null) {
                    if(this.EntityTypes.Contains(baseType))
                        return baseType;
                    baseType = baseType.BaseType;
                }
                return null;
            }
        }

        internal static bool IsCollectionType(Type t) {
            return typeof(IEnumerable).IsAssignableFrom(t);
        }

        private static PropertyDescriptor GetReverseAssociation(PropertyDescriptor propertyDescriptor, AssociationAttribute assocAttrib) {
            return (from PropertyDescriptor descriptor in TypeDescriptor.GetProperties(TypeUtility.GetElementType(propertyDescriptor.PropertyType))
                    where descriptor.Name != propertyDescriptor.Name
                    let attribute = descriptor.Attributes[typeof(AssociationAttribute)] as AssociationAttribute
                    where (attribute != null) && (attribute.Name == assocAttrib.Name)
                    select descriptor).FirstOrDefault();
        }

        public static AttributeCollection GetAttributeCollection(Type type) {
            var attributes = TypeDescriptor.GetAttributes(type.BaseType);
            var list = new List<Attribute>(TypeDescriptor.GetAttributes(type).Cast<Attribute>());
            foreach(var attribute in from Attribute attribute in attributes
                                     let attribute2 = (AttributeUsageAttribute)TypeDescriptor.GetAttributes(attribute)[typeof(AttributeUsageAttribute)]
                                     where (attribute2 != null) && !attribute2.Inherited
                                     select attribute)
                for(var i = list.Count - 1; i >= 0; i--) {
                    if(!ReferenceEquals(attribute, list[i]))
                        continue;
                    list.RemoveAt(i);
                    break;
                }
            return new AttributeCollection(list.ToArray());
        }

        public static AttributeCollection ExplicitAttributes(PropertyDescriptor propertyDescriptor) {
            var list = new List<Attribute>(propertyDescriptor.Attributes.Cast<Attribute>());
            var attributes = TypeDescriptor.GetAttributes(propertyDescriptor.PropertyType);
            var flag = false;
            foreach(Attribute attribute in attributes)
                for(var i = list.Count - 1; i >= 0; i--)
                    if(ReferenceEquals(attribute, list[i])) {
                        list.RemoveAt(i);
                        flag = true;
                    }
            return !flag ? propertyDescriptor.Attributes : new AttributeCollection(list.ToArray());
        }

        private DomainServiceDescriptionAggregate domainServiceDescriptionAggregate;
        private Type visibleBaseType;
        private List<PropertyDescriptor> associationProperties;

        private IEnumerable<DomainServiceDescription> DomainServiceDescriptions {
            get;
            set;
        }

        private bool GenerateGetIdentity {
            get;
            set;
        }

        private bool IsUserType {
            get {
                return typeof(IUser).IsAssignableFrom(this.Type);
            }
        }

        protected override IEnumerable<Type> ComplexTypes {
            get {
                return this.domainServiceDescriptionAggregate.ComplexTypes;
            }
        }

        internal override bool IsDerivedType {
            get {
                return this.visibleBaseType != null;
            }
        }

        private IEnumerable<PropertyDescriptor> AssociationProperties {
            get {
                return this.associationProperties ?? (this.associationProperties = new List<PropertyDescriptor>());
            }
        }

        private IEnumerable<PropertyDescriptor> LoadOnDemandProperties {
            get {
                return this.AssociationProperties.Where(d => IsCollectionType(d.PropertyType));
            }
        }

        private void GetKeysInfo(out string[] keyNames, out string[] nullableKeyNames) {
            var list = new List<string>();
            var list2 = new List<string>();
            foreach(PropertyDescriptor descriptor in TypeDescriptor.GetProperties(this.Type))
                if(descriptor.Attributes[typeof(KeyAttribute)] is KeyAttribute) {
                    var item = CodeGenUtilities.MakeCompliantFieldName(descriptor.Name);
                    list.Add(item);
                    if(TypeUtility.IsNullableType(descriptor.PropertyType) || !descriptor.PropertyType.IsValueType)
                        list2.Add(item);
                }
            keyNames = list.ToArray();
            nullableKeyNames = list2.ToArray();
        }

        private void AddAssociationToGenerate(PropertyDescriptor pd) {
            var associationType = IsCollectionType(pd.PropertyType) ? TypeUtility.GetElementType(pd.PropertyType) : pd.PropertyType;
            if(!CodeGenUtilities.RegisterTypeName(associationType, this.Type.Namespace))
                foreach(var type in this.ClientCodeGenerator.DomainServiceDescriptions.SelectMany(d => d.EntityTypes).Where(t => t.Namespace == associationType.Namespace).Distinct())
                    CodeGenUtilities.RegisterTypeName(type, this.Type.Namespace);
            this.associationProperties.Add(pd);
        }

        private bool ShouldFlattenProperty(PropertyDescriptor propertyDescriptor) {
            var componentType = propertyDescriptor.ComponentType;
            if(componentType == this.Type)
                return true;
            if(!componentType.IsAssignableFrom(this.Type))
                return true;
            if(!this.domainServiceDescriptionAggregate.EntityTypes.Contains(componentType))
                for(var type2 = this.Type.BaseType; type2 != null; type2 = type2.BaseType) {
                    if(type2 == componentType)
                        return true;
                    if(this.domainServiceDescriptionAggregate.EntityTypes.Contains(type2))
                        break;
                }
            return false;
        }

        private bool IsMethodPolymorphic(MethodInfo methodInfo) {
            if((methodInfo != null) && (methodInfo.DeclaringType == this.Type)) {
                if(methodInfo.IsVirtual && !methodInfo.IsFinal)
                    return true;
                if(this.Type.BaseType != null) {
                    var types = (from p in methodInfo.GetParameters()
                                 select p.ParameterType).ToArray<Type>();
                    if(this.Type.BaseType.GetMethod(methodInfo.Name, types) != null)
                        return true;
                }
            }
            return false;
        }

        private IEnumerable<DomainOperationEntry> GetEntityCustomMethods() {
            var dictionary = new Dictionary<string, DomainOperationEntry>();
            var dictionary2 = new Dictionary<string, DomainServiceDescription>();
            foreach(var description in this.DomainServiceDescriptions)
                foreach(var entry in description.GetCustomMethods(this.Type)) {
                    var name = entry.Name;
                    if(dictionary.ContainsKey(name))
                        this.ClientCodeGenerator.CodeGenerationHost.LogError(string.Format(CultureInfo.CurrentCulture, "The custom method named '{0}' associated with entity type '{1}' is defined more than once in the '{2}' and '{3}' DomainService classes.", new object[] {
                            name, this.Type, dictionary2[name].DomainServiceType, description.DomainServiceType
                        }));
                    else {
                        dictionary.Add(name, entry);
                        dictionary2.Add(name, description);
                    }
                }
            return dictionary.Values.AsEnumerable();
        }

        private bool CanGenerateReverseAssociation(AssociationMetadata metadata) {
            var reverseAssociation = GetReverseAssociation(metadata.PropertyDescriptor, metadata.AssociationAttribute);
            return reverseAssociation != null && this.CanGenerateProperty(reverseAssociation) && metadata.IsCollection;
        }

        private IEnumerable<Type> GetVisibleBaseTypes(Type entityType) {
            var list = new List<Type>();
            for(var type = this.domainServiceDescriptionAggregate.GetEntityBaseType(entityType); type != null; type = this.domainServiceDescriptionAggregate.GetEntityBaseType(type))
                list.Add(type);
            return list;
        }

        private IEnumerable<CodeAttributeDeclaration> GenerateTypeAttributes() {
            var typeAttributes = this.GetTypeAttributes();
            foreach(var attributeDeclaration in this.ClientCodeGenerator.GenerateAttributes(typeAttributes))
                yield return attributeDeclaration;
            yield return this.ClientCodeGenerator.GenerateDataContractAttribute(this.Type);
            if(!this.IsDerivedType)
                foreach(var type in from t in this.GetDerivedTypes()
                                    orderby t.FullName
                                    select t)
                    yield return new CodeAttributeDeclaration("KnownType", new CodeAttributeArgument(new CodeTypeOfExpression(type)));
        }

        private IEnumerable<CodeMemberField> GenerateBackingFields() {
            foreach(var descriptor in this.Properties)
                yield return new CodeMemberField {
                    Attributes = MemberAttributes.Private,
                    Name = CodeGenUtilities.MakeCompliantFieldName(descriptor.Name),
                    Type = new CodeTypeReference(descriptor.PropertyType),
                };
            foreach(var metadata in this.AssociationProperties.Select(descriptor => new AssociationMetadata(descriptor)))
                yield return new CodeMemberField {
                    Attributes = MemberAttributes.Private,
                    Name = metadata.FieldName,
                    Type = metadata.AssociationType,
                };
            if(this.LoadOnDemandProperties.Any())
                yield return new CodeMemberField {
                    Attributes = MemberAttributes.Private,
                    Name = "isLoadingDetails",
                    Type = new CodeTypeReference(typeof(bool)),
                };
        }

        private CodeMemberProperty GenerateProperty(PropertyDescriptor propertyDescriptor) {
            var fieldName = CodeGenUtilities.MakeCompliantFieldName(propertyDescriptor.Name);
            var property = new CodeMemberProperty {
                Attributes = MemberAttributes.Public | MemberAttributes.Final,
                Name = propertyDescriptor.Name,
                Type = new CodeTypeReference(propertyDescriptor.PropertyType),
            };
            property.CustomAttributes.AddRange(this.ClientCodeGenerator.GenerateAttributes(this.GetPropertyAttributes(propertyDescriptor, propertyDescriptor.PropertyType)).ToArray());
            property.GetStatements.Add(new CodeMethodReturnStatement(new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), fieldName)));
            var value = new CodeVariableReferenceExpression("value");
            property.SetStatements.Add(new CodeConditionStatement(new CodeBinaryOperatorExpression(new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), fieldName), CodeBinaryOperatorType.ValueEquality, value), new CodeMethodReturnStatement()));
            property.SetStatements.Add(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), string.Format("On{0}Changing", propertyDescriptor.Name), value));
            property.SetStatements.Add(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), "RaiseDataMemberChanging", new CodePrimitiveExpression(propertyDescriptor.Name)));
            property.SetStatements.Add(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), "ValidateProperty", new CodePrimitiveExpression(propertyDescriptor.Name), value));
            property.SetStatements.Add(new CodeAssignStatement(new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), fieldName), value));
            property.SetStatements.Add(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), "RaiseDataMemberChanged", new CodePrimitiveExpression(propertyDescriptor.Name)));
            property.SetStatements.Add(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), string.Format("On{0}Changed", propertyDescriptor.Name)));
            if(propertyDescriptor.Attributes[typeof(KeyAttribute)] is KeyAttribute)
                property.SetStatements.Add(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), "RaisePropertyChanged", new CodePrimitiveExpression("IsNewlyCreated")));
            return property;
        }

        private IEnumerable<CodeStatement> GenerateSingletonAssociationGetStatements(AssociationMetadata metadata) {
            var fieldRef = new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), metadata.FieldName);
            var fieldEntityRef = new CodePropertyReferenceExpression(fieldRef, "Entity");
            var entityRefCreateExpression = new CodeObjectCreateExpression(new CodeTypeReference("EntityRef", new CodeTypeReference((metadata.IsCollection ? metadata.AssociationType : metadata.PropertyType).BaseType, CodeTypeReferenceOptions.GenericTypeParameter)), new CodeThisReferenceExpression(), new CodePrimitiveExpression(metadata.PropertyName), new CodeMethodReferenceExpression(new CodeThisReferenceExpression(), "Filter" + metadata.PropertyName));
            yield return new CodeConditionStatement(new CodeBinaryOperatorExpression(fieldRef, CodeBinaryOperatorType.ValueEquality, new CodePrimitiveExpression()), new CodeAssignStatement(fieldRef, entityRefCreateExpression));
            yield return new CodeMethodReturnStatement(fieldEntityRef);
        }

        private IEnumerable<CodeStatement> GenerateSingletonAssociationSetStatements(AssociationMetadata metadata) {
            var reverseAssociation = GetReverseAssociation(metadata.PropertyDescriptor, metadata.AssociationAttribute);
            var isBiDirectionalAssociation = reverseAssociation != null && this.CanGenerateProperty(reverseAssociation);
            var reverseIsSingleton = false;
            var associationName = isBiDirectionalAssociation ? reverseAssociation.Name : string.Empty;
            var fieldRef = new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), metadata.FieldName);
            var fieldEntityRef = new CodePropertyReferenceExpression(fieldRef, "Entity");
            var propertyRef = new CodePropertyReferenceExpression(new CodeThisReferenceExpression(), metadata.PropertyName);
            var previousRef = new CodeVariableReferenceExpression("previous");
            var valueRef = new CodeVariableReferenceExpression("value");
            yield return new CodeVariableDeclarationStatement("var", "previous", propertyRef);
            yield return new CodeConditionStatement(new CodeBinaryOperatorExpression(previousRef, CodeBinaryOperatorType.IdentityEquality, valueRef), new CodeMethodReturnStatement());
            yield return new CodeExpressionStatement(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), "ValidateProperty", new CodePrimitiveExpression(metadata.PropertyName), valueRef));
            if(isBiDirectionalAssociation && !metadata.IsExternal) {
                reverseIsSingleton = !IsCollectionType(reverseAssociation.PropertyType);
                var statement = new CodeConditionStatement(new CodeBinaryOperatorExpression(previousRef, CodeBinaryOperatorType.IdentityInequality, new CodePrimitiveExpression()));
                statement.TrueStatements.Add(new CodeAssignStatement(fieldEntityRef, new CodePrimitiveExpression()));
                if(reverseIsSingleton)
                    statement.TrueStatements.Add(new CodeAssignStatement(new CodePropertyReferenceExpression(previousRef, associationName), new CodePrimitiveExpression()));
                else
                    statement.TrueStatements.Add(new CodeMethodInvokeExpression(new CodePropertyReferenceExpression(previousRef, associationName), "Remove", new CodeThisReferenceExpression()));
                yield return statement;
            }

            Func<CodeStatement[]> getEntitySetStatements = () => {
                var result = new List<CodeStatement>(2) {
                    new CodeAssignStatement(fieldEntityRef, valueRef)
                };
                if(isBiDirectionalAssociation) {
                    var propRef = new CodePropertyReferenceExpression(valueRef, associationName);
                    CodeStatement statement;
                    if(reverseIsSingleton)
                        statement = new CodeAssignStatement(propRef, new CodeThisReferenceExpression());
                    else
                        statement = new CodeExpressionStatement(new CodeMethodInvokeExpression(propRef, "Add", new CodeThisReferenceExpression()));
                    result.Add(new CodeConditionStatement(new CodeBinaryOperatorExpression(valueRef, CodeBinaryOperatorType.IdentityInequality, new CodePrimitiveExpression()), statement));
                }
                return result.ToArray();
            };
            if(metadata.AssociationAttribute.IsForeignKey) {
                var strArray = metadata.AssociationAttribute.ThisKeyMembers.ToArray();
                var strArray2 = metadata.AssociationAttribute.OtherKeyMembers.ToArray();
                var statement = new CodeConditionStatement(new CodeBinaryOperatorExpression(valueRef, CodeBinaryOperatorType.IdentityEquality, new CodePrimitiveExpression()));
                for(var i = 0; i < strArray.Length; i++) {
                    var propRef = new CodePropertyReferenceExpression(new CodeThisReferenceExpression(), strArray[i]);
                    statement.TrueStatements.Add(new CodeAssignStatement(propRef, new CodeDefaultValueExpression(new CodeTypeReference(TypeDescriptor.GetProperties(this.Type).Find(strArray[i], false).PropertyType))));
                    statement.FalseStatements.Add(new CodeAssignStatement(propRef, new CodePropertyReferenceExpression(valueRef, strArray2[i])));
                }
                yield return statement;
                if(!metadata.IsExternal)
                    foreach(var s in getEntitySetStatements())
                        yield return s;
            } else
                foreach(var s in getEntitySetStatements())
                    yield return s;

            if(!metadata.IsExternal)
                yield return new CodeExpressionStatement(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), "RaisePropertyChanged", new CodePrimitiveExpression(metadata.PropertyName)));
        }

        private IEnumerable<CodeStatement> GenerateCollectionAssociationGetStatements(AssociationMetadata metadata) {
            var fieldRef = new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), metadata.FieldName);
            var expression = new CodeObjectCreateExpression(metadata.AssociationType, new CodeThisReferenceExpression(), new CodePrimitiveExpression(metadata.PropertyName), new CodeMethodReferenceExpression(new CodeThisReferenceExpression(), "Filter" + metadata.PropertyName));
            if(this.CanGenerateReverseAssociation(metadata)) {
                expression.Parameters.Add(new CodeMethodReferenceExpression(new CodeThisReferenceExpression(), "Attach" + metadata.PropertyName));
                expression.Parameters.Add(new CodeMethodReferenceExpression(new CodeThisReferenceExpression(), "Detach" + metadata.PropertyName));
            }
            yield return new CodeConditionStatement(new CodeBinaryOperatorExpression(fieldRef, CodeBinaryOperatorType.IdentityEquality, new CodePrimitiveExpression()), new CodeAssignStatement(fieldRef, expression), new CodeExpressionStatement(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), "OnRequestingDetails", new CodePrimitiveExpression(metadata.PropertyDescriptor.Name))));
            yield return new CodeMethodReturnStatement(fieldRef);
        }

        private CodeMemberProperty GenerateAssociation(PropertyDescriptor pd) {
            var metadata = new AssociationMetadata(pd);
            var property = new CodeMemberProperty {
                Attributes = MemberAttributes.Public | MemberAttributes.Final,
                Name = metadata.PropertyName,
                Type = metadata.IsCollection ? metadata.AssociationType : metadata.PropertyType,
            };
            property.CustomAttributes.AddRange(this.ClientCodeGenerator.GenerateAttributes(metadata.Attributes).ToArray());
            if(IsCollectionType(pd.PropertyType))
                property.GetStatements.AddRange(this.GenerateCollectionAssociationGetStatements(metadata).ToArray());
            else {
                property.GetStatements.AddRange(this.GenerateSingletonAssociationGetStatements(metadata).ToArray());
                property.SetStatements.AddRange(this.GenerateSingletonAssociationSetStatements(metadata).ToArray());
            }
            return property;
        }

        private IEnumerable<CodeMemberProperty> GenerateProperties() {
            if(this.GenerateGetIdentity) {
                string[] keyNames;
                string[] nullableKeyNames;
                this.GetKeysInfo(out keyNames, out nullableKeyNames);
                var property = new CodeMemberProperty {
                    Attributes = MemberAttributes.Public | MemberAttributes.Final,
                    Type = new CodeTypeReference(typeof(bool)),
                    Name = "IsNewlyCreated",
                };
                CodeExpression left, right;
                if(keyNames.Length == 1) {
                    var propertyDescriptor = TypeDescriptor.GetProperties(this.Type).Cast<PropertyDescriptor>().First(pd => pd.Attributes[typeof(KeyAttribute)] is KeyAttribute);
                    left = new CodePropertyReferenceExpression(new CodeThisReferenceExpression(), propertyDescriptor.Name);
                    right = new CodeDefaultValueExpression(new CodeTypeReference(propertyDescriptor.PropertyType));
                } else {
                    left = new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), "GetIdentity");
                    right = new CodePrimitiveExpression();
                }
                property.GetStatements.Add(new CodeMethodReturnStatement(new CodeBinaryOperatorExpression(left, CodeBinaryOperatorType.ValueEquality, right)));
                yield return property;
            }
            if(this.LoadOnDemandProperties.Any()) {
                var property = new CodeMemberProperty {
                    Attributes = MemberAttributes.Public | MemberAttributes.Final,
                    Type = new CodeTypeReference(typeof(bool)),
                    Name = "IsLoadingDetails",
                };
                var fieldRef = new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), "isLoadingDetails");
                var valueRef = new CodeVariableReferenceExpression("value");
                property.GetStatements.Add(new CodeMethodReturnStatement(fieldRef));
                property.SetStatements.Add(new CodeConditionStatement(new CodeBinaryOperatorExpression(fieldRef, CodeBinaryOperatorType.ValueEquality, valueRef), new CodeMethodReturnStatement()));
                property.SetStatements.Add(new CodeAssignStatement(fieldRef, valueRef));
                property.SetStatements.Add(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), "RaisePropertyChanged", new CodePrimitiveExpression(property.Name)));
                yield return property;
            }
            foreach(var descriptor in this.Properties)
                yield return this.GenerateProperty(descriptor);
            foreach(var descriptor in this.AssociationProperties)
                yield return this.GenerateAssociation(descriptor);
            foreach(var method in this.GetEntityCustomMethods().OrderBy(e => e.Name)) {
                var attribute = new CodeAttributeDeclaration("Display", new CodeAttributeArgument("AutoGenerateField", new CodePrimitiveExpression(false)));
                var property = new CodeMemberProperty {
                    Attributes = MemberAttributes.Public | MemberAttributes.Final,
                    Type = new CodeTypeReference(typeof(bool)),
                    Name = "Can" + method.Name,
                };
                property.CustomAttributes.Add(attribute);
                property.GetStatements.Add(new CodeMethodReturnStatement(new CodeMethodInvokeExpression(new CodeBaseReferenceExpression(), "CanInvokeAction", new CodePrimitiveExpression(method.Name))));
                yield return property;
                property = new CodeMemberProperty {
                    Attributes = MemberAttributes.Public | MemberAttributes.Final,
                    Type = new CodeTypeReference(typeof(bool)),
                    Name = "Is" + method.Name + "Invoked",
                };
                property.CustomAttributes.Add(attribute);
                property.GetStatements.Add(new CodeMethodReturnStatement(new CodeMethodInvokeExpression(new CodeBaseReferenceExpression(), "IsActionInvoked", new CodePrimitiveExpression(method.Name))));
                yield return property;
            }
            if(this.IsUserType) {
                var property = new CodeMemberProperty {
                    Type = new CodeTypeReference(typeof(string)),
                    PrivateImplementationType = new CodeTypeReference("global::System.Security.Principal.IIdentity"),
                    Name = "AuthenticationType",
                };
                property.GetStatements.Add(new CodeMethodReturnStatement(new CodePropertyReferenceExpression(new CodeTypeReferenceExpression(typeof(string)), "Empty")));
                yield return property;
                property = new CodeMemberProperty {
                    Type = new CodeTypeReference(typeof(string)),
                    PrivateImplementationType = new CodeTypeReference("global::System.Security.Principal.IIdentity"),
                    Name = "Name",
                };
                property.GetStatements.Add(new CodeMethodReturnStatement(new CodePropertyReferenceExpression(new CodeThisReferenceExpression(), "Name")));
                yield return property;
                property = new CodeMemberProperty {
                    Type = new CodeTypeReference("global::System.Security.Principal.IIdentity"),
                    PrivateImplementationType = new CodeTypeReference("global::System.Security.Principal.IPrincipal"),
                    Name = "Identity",
                };
                property.GetStatements.Add(new CodeMethodReturnStatement(new CodeThisReferenceExpression()));
                yield return property;
                property = new CodeMemberProperty {
                    Attributes = MemberAttributes.Public | MemberAttributes.Final,
                    Type = new CodeTypeReference(typeof(bool)),
                    Name = "IsAuthenticated",
                };
                property.GetStatements.Add(new CodeMethodReturnStatement(new CodeBinaryOperatorExpression(new CodeMethodInvokeExpression(new CodeTypeReferenceExpression(typeof(string)), "IsNullOrEmpty", new CodePropertyReferenceExpression(new CodeThisReferenceExpression(), "Name")), CodeBinaryOperatorType.ValueEquality, new CodePrimitiveExpression(false))));
                yield return property;
            }
        }

        private IEnumerable<CodeTypeMember> GenerateExtensibilityMethods() {
            yield return new CodeSnippetTypeMember("        partial void OnCreated();");
            foreach(var descriptor in this.NotificationMethodList) {
                yield return new CodeSnippetTypeMember(string.Format("        partial void On{0}Changing({1} value);", descriptor.Name, this.ClientCodeGenerator.TranslateType(descriptor.PropertyType)));
                yield return new CodeSnippetTypeMember(string.Format("        partial void On{0}Changed();", descriptor.Name));
            }
            foreach(var method in this.GetEntityCustomMethods().OrderBy(e => e.Name)) {
                var parameters = method.Parameters.Skip(1).Select(p => {
                    var type = CodeGenUtilities.TranslateType(p.ParameterType);
                    return string.Format("{0} {1}", TypeUtility.IsPredefinedSimpleType(type) ? CodeGenUtilities.GetTypeName(type) : type.Name, p.Name);
                }).ToArray();
                yield return new CodeSnippetTypeMember(string.Format("        partial void On{0}Invoking({1});", method.Name, string.Join(", ", parameters)));
                yield return new CodeSnippetTypeMember(string.Format("        partial void On{0}Invoked();", method.Name));
            }
            if(this.LoadOnDemandProperties.Any())
                yield return new CodeSnippetTypeMember("        partial void OnRequestingDetails(string propertyName);");
        }

        private IEnumerable<CodeConstructor> GenerateConstructors() {
            var ctor = new CodeConstructor {
                Attributes = this.IsAbstract ? MemberAttributes.Family : MemberAttributes.Public,
            };
            ctor.Statements.Add(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), "OnCreated"));
            yield return ctor;
        }

        private CodeMemberMethod GenerateAssociationAttachMethod(AssociationMetadata metadata) {
            var name = GetReverseAssociation(metadata.PropertyDescriptor, metadata.AssociationAttribute).Name;
            var method = new CodeMemberMethod {
                Attributes = MemberAttributes.Private,
                Name = "Attach" + metadata.PropertyName,
            };
            method.Parameters.Add(new CodeParameterDeclarationExpression(metadata.PropertyType, "entity"));
            var propRef = new CodePropertyReferenceExpression(new CodeVariableReferenceExpression("entity"), name);
            if(metadata.IsCollection)
                method.Statements.Add(new CodeAssignStatement(propRef, new CodeThisReferenceExpression()));
            else
                method.Statements.Add(new CodeMethodInvokeExpression(propRef, "Add", new CodeThisReferenceExpression()));
            return method;
        }

        private CodeMemberMethod GenerateAssociationDetachMethod(AssociationMetadata metadata) {
            var name = GetReverseAssociation(metadata.PropertyDescriptor, metadata.AssociationAttribute).Name;
            var method = new CodeMemberMethod {
                Attributes = MemberAttributes.Private,
                Name = "Detach" + metadata.PropertyName,
            };
            method.Parameters.Add(new CodeParameterDeclarationExpression(metadata.PropertyType, "entity"));
            var propRef = new CodePropertyReferenceExpression(new CodeVariableReferenceExpression("entity"), name);
            if(metadata.IsCollection)
                method.Statements.Add(new CodeAssignStatement(propRef, new CodePrimitiveExpression()));
            else
                method.Statements.Add(new CodeMethodInvokeExpression(propRef, "Remove", new CodeThisReferenceExpression()));
            return method;
        }

        private CodeMemberMethod GenerateAssociationFilterMethod(AssociationMetadata metadata) {
            var strArray = metadata.AssociationAttribute.ThisKeyMembers.ToArray();
            var strArray2 = metadata.AssociationAttribute.OtherKeyMembers.ToArray();
            var method = new CodeMemberMethod {
                Attributes = MemberAttributes.Private,
                Name = "Filter" + metadata.PropertyName,
                ReturnType = new CodeTypeReference(typeof(bool)),
            };
            method.Parameters.Add(new CodeParameterDeclarationExpression(metadata.PropertyType, "entity"));
            method.Statements.Add(new CodeMethodReturnStatement(strArray.Select((t, i) => new CodeBinaryOperatorExpression(new CodePropertyReferenceExpression(new CodeVariableReferenceExpression("entity"), strArray2[i]), CodeBinaryOperatorType.ValueEquality, new CodePropertyReferenceExpression(new CodeThisReferenceExpression(), t))).Aggregate<CodeBinaryOperatorExpression, CodeExpression>(null, (current, opExpression) => current == null ? opExpression : new CodeBinaryOperatorExpression(current, CodeBinaryOperatorType.BooleanAnd, opExpression))));
            return method;
        }

        private IEnumerable<CodeMemberMethod> GenerateDelegates() {
            foreach(var descriptor in this.AssociationProperties) {
                var metadata = new AssociationMetadata(descriptor);
                if(IsCollectionType(descriptor.PropertyType) && this.CanGenerateReverseAssociation(metadata)) {
                    yield return this.GenerateAssociationAttachMethod(metadata);
                    yield return this.GenerateAssociationDetachMethod(metadata);
                }
                yield return this.GenerateAssociationFilterMethod(metadata);
            }
        }

        private IEnumerable<CodeMemberMethod> GenerateCustomMethods() {
            var entityCustomMethods = this.GetEntityCustomMethods().OrderBy(e => e.Name).ToArray();
            foreach(var entry in entityCustomMethods) {
                var customMethod = new CodeMemberMethod {
                    Attributes = MemberAttributes.Public | MemberAttributes.Final,
                    Name = entry.Name,
                };
                var parameters = entry.Parameters.Skip(1).Select(p => TypeUtility.IsPredefinedSimpleType(p.ParameterType) ? new CodeParameterDeclarationExpression(p.ParameterType, p.Name) : new CodeParameterDeclarationExpression(p.ParameterType.Name, p.Name)).ToArray();
                var parameterExpressions = parameters.Select(p => new CodeVariableReferenceExpression(p.Name)).ToArray();
                customMethod.Parameters.AddRange(parameters);
                customMethod.Statements.Add(new CodeMethodInvokeExpression(new CodeMethodReferenceExpression(new CodeThisReferenceExpression(), "On" + entry.Name + "Invoking"), parameterExpressions));
                customMethod.Statements.Add(new CodeMethodInvokeExpression(new CodeBaseReferenceExpression(), "InvokeAction", (new CodeExpression[] {
                    new CodePrimitiveExpression(entry.Name)
                }).Union(parameterExpressions).ToArray()));
                customMethod.Statements.Add(new CodeMethodInvokeExpression(new CodeMethodReferenceExpression(new CodeThisReferenceExpression(), "On" + entry.Name + "Invoked")));
                yield return customMethod;
            }
            if(entityCustomMethods.Length > 0 && !this.IsDerivedType) {
                var method = new CodeMemberMethod {
                    Attributes = MemberAttributes.Family | MemberAttributes.Override,
                    Name = "OnActionStateChanged",
                };
                foreach(var entry in entityCustomMethods)
                    method.Statements.Add(new CodeMethodInvokeExpression(new CodeBaseReferenceExpression(), "UpdateActionState", new CodePrimitiveExpression(entry.Name), new CodePrimitiveExpression("Can" + entry.Name), new CodePrimitiveExpression("Is" + entry.Name + "Invoked")));
                yield return method;
            }
            if(this.IsUserType) {
                var method = new CodeMemberMethod {
                    Attributes = MemberAttributes.Public | MemberAttributes.Final,
                    Name = "IsInRole",
                    ReturnType = new CodeTypeReference(typeof(bool)),
                };
                method.Parameters.Add(new CodeParameterDeclarationExpression(typeof(string), "role"));
                method.Statements.Add(new CodeConditionStatement(new CodeBinaryOperatorExpression(new CodePropertyReferenceExpression(new CodeThisReferenceExpression(), "Roles"), CodeBinaryOperatorType.IdentityEquality, new CodePrimitiveExpression()), new CodeMethodReturnStatement(new CodePrimitiveExpression(false))));
                method.Statements.Add(new CodeMethodReturnStatement(new CodeMethodInvokeExpression(new CodeTypeReferenceExpression("global::System.Linq.Enumerable"), "Contains", new CodePropertyReferenceExpression(new CodeThisReferenceExpression(), "Roles"), new CodeVariableReferenceExpression("role"))));
                yield return method;
            }
        }

        private IEnumerable<CodeMemberMethod> GenerateMethods() {
            if(this.GenerateGetIdentity) {
                string[] keyNames;
                string[] nullableKeyNames;
                this.GetKeysInfo(out keyNames, out nullableKeyNames);
                var method = new CodeMemberMethod {
                    Attributes = MemberAttributes.Public | MemberAttributes.Override,
                    Name = "GetIdentity",
                    ReturnType = new CodeTypeReference(typeof(object)),
                };
                if(keyNames.Length == 1)
                    method.Statements.Add(new CodeMethodReturnStatement(new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), keyNames[0])));
                else {
                    if(nullableKeyNames.Length > 0) {
                        var expression = nullableKeyNames.Select(s => new CodeBinaryOperatorExpression(new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), s), CodeBinaryOperatorType.ValueEquality, new CodePrimitiveExpression())).Aggregate<CodeBinaryOperatorExpression, CodeBinaryOperatorExpression>(null, (current, eqExpression) => current == null ? eqExpression : new CodeBinaryOperatorExpression(current, CodeBinaryOperatorType.BooleanOr, eqExpression));
                        method.Statements.Add(new CodeConditionStatement(expression, new CodeMethodReturnStatement(new CodePrimitiveExpression())));
                    }
                    var parameters = keyNames.Select(s => new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), s)).ToArray();
                    method.Statements.Add(new CodeMethodReturnStatement(new CodeMethodInvokeExpression(new CodeTypeReferenceExpression("EntityKey"), "Create", parameters)));
                }
                yield return method;
            }
            foreach(var method in this.GenerateCustomMethods())
                yield return method;
        }

        private CodeTypeDeclaration GenerateEntityClass() {
            var typeDeclaration = new CodeTypeDeclaration(this.Type.Name) {
                IsPartial = true,
                IsClass = true,
                TypeAttributes = TypeAttributes.Public,
            };
            if(this.IsAbstract)
                typeDeclaration.TypeAttributes = typeDeclaration.TypeAttributes | TypeAttributes.Abstract;
            else if(!this.GetDerivedTypes().Any())
                typeDeclaration.TypeAttributes = typeDeclaration.TypeAttributes | TypeAttributes.Sealed;
            //typeDeclaration.Comments.Add(new CodeCommentStatement(string.Format("<summary>“{0}”实体类。</summary>", this.Type.Name), true));
            typeDeclaration.BaseTypes.AddRange(this.GetBaseTypes().ToArray());
            typeDeclaration.CustomAttributes.AddRange(this.GenerateTypeAttributes().ToArray());
            typeDeclaration.Members.AddRange(this.GenerateBackingFields().ToArray());
            typeDeclaration.Members.AddRange(this.GenerateProperties().ToArray());
            typeDeclaration.Members.AddRange(this.GenerateExtensibilityMethods().ToArray());
            typeDeclaration.Members.AddRange(this.GenerateDelegates().ToArray());
            typeDeclaration.Members.AddRange(this.GenerateConstructors().ToArray());
            typeDeclaration.Members.AddRange(this.GenerateMethods().ToArray());
            return typeDeclaration;
        }

        protected override IEnumerable<CodeTypeReference> GetBaseTypes() {
            yield return new CodeTypeReference(this.visibleBaseType != null ? this.visibleBaseType.FullName : "Entity");
            if(this.IsUserType) {
                yield return new CodeTypeReference(typeof(IIdentity));
                yield return new CodeTypeReference(typeof(IPrincipal));
            }
        }

        protected override IEnumerable<Type> GetDerivedTypes() {
            return from t in this.domainServiceDescriptionAggregate.EntityTypes
                   where t != this.Type && this.Type.IsAssignableFrom(t)
                   select t;
        }

        internal override bool CanGenerateProperty(PropertyDescriptor propertyDescriptor) {
            if(propertyDescriptor.Attributes[typeof(ExcludeAttribute)] != null)
                return false;
            if(propertyDescriptor.Attributes[typeof(ExternalReferenceAttribute)] == null) {
                if(!base.CanGenerateProperty(propertyDescriptor)) {
                    if(ExplicitAttributes(propertyDescriptor)[typeof(KeyAttribute)] != null) {
                        this.ClientCodeGenerator.CodeGenerationHost.LogError(string.Format(CultureInfo.CurrentCulture, "The property '{0}.{1}' is marked as a key property, but it's not serializable. Are you missing DataMemberAttribute?", new object[] {
                            this.Type, propertyDescriptor.Name
                        }));
                        return false;
                    }
                    var elementType = TypeUtility.GetElementType(propertyDescriptor.PropertyType);
                    if(!this.domainServiceDescriptionAggregate.EntityTypes.Contains(elementType) || (propertyDescriptor.Attributes[typeof(AssociationAttribute)] == null))
                        return false;
                }
                if(!this.CanGeneratePropertyIfPolymorphic(propertyDescriptor))
                    return false;
            }
            return true;
        }

        protected override bool CanGeneratePropertyIfPolymorphic(PropertyDescriptor pd) {
            var property = this.Type.GetProperty(pd.Name);
            if((property != null) && (this.IsMethodPolymorphic(property.GetGetMethod()) || this.IsMethodPolymorphic(property.GetSetMethod())))
                return this.GetVisibleBaseTypes(this.Type).All(type => type.GetProperty(pd.Name) == null);
            return true;
        }

        internal override bool IsPropertyShared(PropertyDescriptor pd) {
            if(!base.IsPropertyShared(pd))
                return false;
            if(ExplicitAttributes(pd)[typeof(KeyAttribute)] != null)
                this.GenerateGetIdentity = false;
            return true;
        }

        internal override bool ShouldDeclareProperty(PropertyDescriptor pd) {
            if(!base.ShouldDeclareProperty(pd))
                return false;
            if(!this.ShouldFlattenProperty(pd))
                return false;
            var attributes = ExplicitAttributes(pd);
            var propertyType = pd.PropertyType;
            if((attributes[typeof(KeyAttribute)] != null) && !TypeUtility.IsPredefinedSimpleType(propertyType)) {
                this.ClientCodeGenerator.CodeGenerationHost.LogError(string.Format(CultureInfo.CurrentCulture, "Property '{0}.{1}' is marked as a key property and is of type '{2}', which is not a supported type for a key member.", new object[] {
                    this.Type, pd.Name, propertyType
                }));
                return false;
            }
            return true;
        }

        protected override bool HandleNonSerializableProperty(PropertyDescriptor propertyDescriptor) {
            var attributes = ExplicitAttributes(propertyDescriptor);
            var attribute = (AssociationAttribute)attributes[typeof(AssociationAttribute)];
            if(attribute != null) {
                this.AddAssociationToGenerate(propertyDescriptor);
                return true;
            }
            return false;
        }

        internal override void Initialize() {
            this.domainServiceDescriptionAggregate = new DomainServiceDescriptionAggregate(from dsd in this.DomainServiceDescriptions
                                                                                           where dsd.EntityTypes.Contains(this.Type)
                                                                                           select dsd);
            this.visibleBaseType = this.domainServiceDescriptionAggregate.GetEntityBaseType(this.Type);
            this.GenerateGetIdentity = !this.IsDerivedType;
            this.associationProperties = new List<PropertyDescriptor>();
            base.Initialize();
        }

        public CodeNamespace Generate(CustomClientCodeGenerator clientCodeGenerator, IEnumerable<DomainServiceDescription> domainServiceDescriptions, Type entityType) {
            this.ClientCodeGenerator = clientCodeGenerator;
            this.DomainServiceDescriptions = domainServiceDescriptions;
            this.Type = entityType;

            this.Initialize();

            var ns = new CodeNamespace(this.Type.Namespace);
            ns.Imports.Add(new CodeNamespaceImport("System.ComponentModel.DataAnnotations.Schema"));
            ns.Imports.Add(new CodeNamespaceImport("System.Runtime.Serialization"));
            ns.Imports.Add(new CodeNamespaceImport("System.Xml.Serialization"));
            ns.Types.Add(this.GenerateEntityClass());
            return ns;
        }
    }

    // ReSharper restore BitwiseOperatorOnEnumWihtoutFlags
}
