﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.RiaServicesCodeGen {
    public sealed class CustomEnumGenerator {
        private CustomClientCodeGenerator ClientCodeGenerator {
            get;
            set;
        }

        private HashSet<Type> EnumsToGenerate {
            get;
            set;
        }

        private IEnumerable<CodeAttributeDeclaration> GenerateEnumMemberAttributes(MemberInfo fieldInfo) {
            var customAttribute = (EnumMemberAttribute)Attribute.GetCustomAttribute(fieldInfo, typeof(EnumMemberAttribute));
            if(customAttribute != null) {
                var enumMemberAttribute = new CodeAttributeDeclaration("EnumMember");
                if(!string.IsNullOrEmpty(customAttribute.Value))
                    enumMemberAttribute.Arguments.Add(new CodeAttributeArgument("Value", new CodePrimitiveExpression(customAttribute.Value)));
                yield return enumMemberAttribute;
            }
            foreach(var attributeDeclaration in this.ClientCodeGenerator.GenerateAttributes(fieldInfo.GetCustomAttributes(false).Cast<Attribute>().Where(a => a.GetType() != typeof(EnumMemberAttribute))))
                yield return attributeDeclaration;
        }

        private CodeNamespace GenerateEnum(Type enumType) {
            var ns = new CodeNamespace(enumType.Namespace);
            var typeDeclaration = new CodeTypeDeclaration(enumType.Name) {
                Attributes = MemberAttributes.Public,
                IsEnum = true,
            };

            var enumUnderlyingType = enumType.GetEnumUnderlyingType();
            if(enumUnderlyingType != typeof(int))
                typeDeclaration.BaseTypes.Add(new CodeTypeReference(enumUnderlyingType));

            if(Attribute.GetCustomAttribute(enumType, typeof(DataContractAttribute)) != null)
                typeDeclaration.CustomAttributes.Add(this.ClientCodeGenerator.GenerateDataContractAttribute(enumType));
            if(enumType.GetCustomAttributes(typeof(FlagsAttribute), false).Length > 0)
                typeDeclaration.CustomAttributes.Add(new CodeAttributeDeclaration("Flags"));

            foreach(var fieldInfo in Enum.GetNames(enumType).Select(enumType.GetField).Where(f => f != null)) {
                var rawConstantValue = fieldInfo.GetRawConstantValue();
                var field = new CodeMemberField(enumUnderlyingType, fieldInfo.Name) {
                    InitExpression = new CodePrimitiveExpression(rawConstantValue)
                };
                field.CustomAttributes.AddRange(this.GenerateEnumMemberAttributes(fieldInfo).ToArray());
                typeDeclaration.Members.Add(field);
            }

            ns.Types.Add(typeDeclaration);
            return ns;
        }

        internal IEnumerable<CodeNamespace> Generate(CustomClientCodeGenerator clientCodeGenerator, HashSet<Type> enumsToGenerate) {
            this.ClientCodeGenerator = clientCodeGenerator;
            this.EnumsToGenerate = enumsToGenerate;

            return this.EnumsToGenerate.Select(this.GenerateEnum);
        }
    }
}
