﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ServiceModel.DomainServices.Server;
using System.ServiceModel.DomainServices.Server.ApplicationServices;

// ReSharper disable BitwiseOperatorOnEnumWihtoutFlags

namespace Sunlight.Silverlight.RiaServicesCodeGen {
    public sealed class CustomWebContextGenerator {
        private const string WEB_CONTEXT_CLASS_NAME = "WebContext";
        private const string WEB_CONTEXT_BASE_CLASS_NAME = "WebContextBase";

        private CustomClientCodeGenerator ClientCodeGenerator {
            get;
            set;
        }

        private DomainServiceDescription GetDefaultAuthDescription() {
            var source = (from d in this.ClientCodeGenerator.DomainServiceDescriptions
                          where typeof(IAuthentication<>).DefinitionIsAssignableFrom(d.DomainServiceType)
                          select d).ToArray();
            if(source.Length > 1) {
                this.ClientCodeGenerator.CodeGenerationHost.LogMessage("Do not support mutliple authentication services.");
                return null;
            }
            return source.SingleOrDefault();
        }

        private IEnumerable<CodeMemberProperty> GenerateProperties() {
            const string PROPERTY_NAME_CURRENT = "Current";
            const string PROPERTY_NAME_USER = "User";

            var property = new CodeMemberProperty {
                Name = PROPERTY_NAME_CURRENT,
                Type = new CodeTypeReference(WEB_CONTEXT_CLASS_NAME),
                Attributes = MemberAttributes.Public | MemberAttributes.New | MemberAttributes.Final | MemberAttributes.Static,
            };
            property.GetStatements.Add(new CodeMethodReturnStatement(new CodeCastExpression(WEB_CONTEXT_CLASS_NAME, new CodePropertyReferenceExpression(new CodeTypeReferenceExpression(WEB_CONTEXT_BASE_CLASS_NAME), PROPERTY_NAME_CURRENT))));
            yield return property;

            var defaultAuthDescription = this.GetDefaultAuthDescription();
            if(defaultAuthDescription == null)
                yield break;
            Type genericType;
            typeof(IAuthentication<>).DefinitionIsAssignableFrom(defaultAuthDescription.DomainServiceType, out genericType);
            if(genericType == null || genericType.GetGenericArguments().Count() != 1)
                yield break;
            var type = genericType.GetGenericArguments()[0];
            property = new CodeMemberProperty {
                Name = PROPERTY_NAME_USER,
                Type = new CodeTypeReference(type),
                Attributes = MemberAttributes.Public | MemberAttributes.New | MemberAttributes.Final,
            };
            property.GetStatements.Add(new CodeMethodReturnStatement(new CodeCastExpression(type, new CodePropertyReferenceExpression(new CodeBaseReferenceExpression(), PROPERTY_NAME_USER))));
            yield return property;
        }

        private IEnumerable<CodeTypeMember> GenerateExtensibilityMethods() {
            var method = new CodeSnippetTypeMember("        partial void OnCreated();");
            yield return method;
        }

        private CodeConstructor GenerateConstructor() {
            var ctor = new CodeConstructor {
                Attributes = MemberAttributes.Public
            };
            ctor.Statements.Add(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), "OnCreated"));
            return ctor;
        }

        private CodeTypeMember[] GenerateBody() {
            var result = new List<CodeTypeMember>();
            result.AddRange(this.GenerateProperties());
            result.AddRange(this.GenerateExtensibilityMethods());
            result.Add(this.GenerateConstructor());
            return result.ToArray();
        }

        private CodeTypeDeclaration GenerateClassDeclaration() {
            var typeDeclaration = new CodeTypeDeclaration(WEB_CONTEXT_CLASS_NAME) {
                IsPartial = true,
                IsClass = true,
                TypeAttributes = TypeAttributes.Public | TypeAttributes.Sealed,
            };
            typeDeclaration.BaseTypes.Add(WEB_CONTEXT_BASE_CLASS_NAME);
            typeDeclaration.Members.AddRange(this.GenerateBody());
            return typeDeclaration;
        }

        private CodeNamespace GenerateNamespace() {
            var ns = new CodeNamespace(this.ClientCodeGenerator.Options.ClientRootNamespace);
            ns.Imports.Add(new CodeNamespaceImport("System.ServiceModel.DomainServices.Client.ApplicationServices"));
            ns.Types.Add(this.GenerateClassDeclaration());
            return ns;
        }

        public CodeNamespace Generate(CustomClientCodeGenerator clientCodeGenerator) {
            this.ClientCodeGenerator = clientCodeGenerator;
            return this.GenerateNamespace();
        }
    }
}

// ReSharper restore BitwiseOperatorOnEnumWihtoutFlags
