﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel.DomainServices.Server;
using Microsoft.ServiceModel.DomainServices.Tools;

namespace Sunlight.Silverlight.RiaServicesCodeGen {
    public abstract class DataContractProxyGenerator {
        private List<PropertyDescriptor> notificationMethodList;
        private IEnumerable<PropertyDescriptor> properties;

        protected CustomClientCodeGenerator ClientCodeGenerator {
            get;
            set;
        }

        protected abstract IEnumerable<Type> ComplexTypes {
            get;
        }

        internal bool IsAbstract {
            get;
            private set;
        }

        internal abstract bool IsDerivedType {
            get;
        }

        internal IEnumerable<PropertyDescriptor> NotificationMethodList {
            get {
                return this.notificationMethodList ?? (this.notificationMethodList = new List<PropertyDescriptor>());
            }
        }

        internal IEnumerable<PropertyDescriptor> Properties {
            get {
                return this.properties ?? (this.properties = new List<PropertyDescriptor>());
            }
        }

        protected Type Type {
            get;
            set;
        }

        internal virtual bool CanGenerateProperty(PropertyDescriptor propertyDescriptor) {
            var propertyType = propertyDescriptor.PropertyType;
            if(!SerializationUtility.IsSerializableDataMember(propertyDescriptor))
                return false;
            string errorMessage;
            var nonNullableType = TypeUtility.GetNonNullableType(propertyType);
            if(nonNullableType.IsEnum && !this.ClientCodeGenerator.CanExposeEnumType(nonNullableType, out errorMessage)) {
                this.ClientCodeGenerator.CodeGenerationHost.LogWarning(string.Format(CultureInfo.CurrentCulture, "Property '{0}.{1}' will not be generated because it uses the Enum type '{2}'.  {3}", new object[] {
                    this.Type, propertyDescriptor.Name, nonNullableType.FullName, errorMessage
                }));
                return false;
            }
            return true;
        }

        protected virtual bool CanGeneratePropertyIfPolymorphic(PropertyDescriptor pd) {
            return true;
        }

        protected abstract IEnumerable<CodeTypeReference> GetBaseTypes();
        protected abstract IEnumerable<Type> GetDerivedTypes();

        private IEnumerable<PropertyDescriptor> GetPropertiesToGenerate() {
            IEnumerable<PropertyDescriptor> enumerable = from p in TypeDescriptor.GetProperties(this.Type).Cast<PropertyDescriptor>()
                                                         orderby p.Name
                                                         select p;
            var list = new List<PropertyDescriptor>();
            foreach(var descriptor in enumerable)
                if(this.ShouldDeclareProperty(descriptor))
                    if(this.CanGenerateProperty(descriptor)) {
                        if(this.CanGeneratePropertyIfPolymorphic(descriptor) && !this.HandleNonSerializableProperty(descriptor)) {
                            var type = CodeGenUtilities.TranslateType(descriptor.PropertyType);
                            var list2 = new List<Type>();
                            var flag = true;
                            if(TypeUtility.IsPredefinedDictionaryType(type))
                                list2.AddRange(CodeGenUtilities.GetDictionaryGenericArgumentTypes(type));
                            else
                                list2.Add(TypeUtility.GetElementType(type));
                            foreach(var type2 in list2) {
                                var nonNullableType = TypeUtility.GetNonNullableType(type2);
                                if(nonNullableType.IsEnum)
                                    this.ClientCodeGenerator.AddEnumTypeToGenerate(nonNullableType);
                                else if(!this.ComplexTypes.Contains(type2) && ((this.ClientCodeGenerator.GetTypeShareKind(nonNullableType) & CodeMemberShareKind.Shared) == CodeMemberShareKind.Unknown)) {
                                    this.ClientCodeGenerator.CodeGenerationHost.LogWarning(string.Format(CultureInfo.CurrentCulture, "The property '{0}' in entity type '{1}' cannot be generated because its type '{2}' is not visible in the client project '{3}'. Are you missing an assembly reference?", new object[] {
                                        descriptor.Name, this.Type.FullName, type2.FullName, this.ClientCodeGenerator.ClientProjectName
                                    }));
                                    flag = false;
                                }
                            }
                            if(flag) {
                                this.notificationMethodList.Add(descriptor);
                                list.Add(descriptor);
                            }
                        }
                    } else
                        this.OnPropertySkipped(descriptor);
            return list;
        }

        internal IEnumerable<Attribute> GetPropertyAttributes(PropertyDescriptor propertyDescriptor, Type propertyType) {
            var source = CustomEntityGenerator.ExplicitAttributes(propertyDescriptor).Cast<Attribute>().ToList();
            if(!source.OfType<DataMemberAttribute>().Any<DataMemberAttribute>())
                source.Add(new DataMemberAttribute());
            var attribute = source.OfType<ReadOnlyAttribute>().SingleOrDefault<ReadOnlyAttribute>();
            if((attribute != null) && !source.OfType<EditableAttribute>().Any<EditableAttribute>())
                source.Add(new EditableAttribute(!attribute.IsReadOnly));
            if(TypeUtility.IsSupportedComplexType(propertyType) && !source.OfType<DisplayAttribute>().Any<DisplayAttribute>()) {
                var attribute3 = new DisplayAttribute {
                    AutoGenerateField = false
                };
                var item = attribute3;
                source.Add(item);
            }
            if(CustomEntityGenerator.GetAttributeCollection(this.Type)[typeof(RoundtripOriginalAttribute)] != null)
                source.RemoveAll(attr => attr is RoundtripOriginalAttribute);
            return source;
        }

        internal IEnumerable<Attribute> GetTypeAttributes() {
            var source = CustomEntityGenerator.GetAttributeCollection(this.Type);
            var filteredAttributes = new List<Attribute>();
            var enumerable = from a in source.Cast<Attribute>()
                             where a is DefaultMemberAttribute
                             select a;
            // ReSharper disable PossibleMultipleEnumeration
            if(enumerable.Any()) {
                var set = new HashSet<string>(from p in TypeDescriptor.GetProperties(this.Type).Cast<PropertyDescriptor>()
                                              select p.Name, StringComparer.Ordinal);
                foreach(DefaultMemberAttribute attribute in enumerable)
                    if(!set.Contains(attribute.MemberName))
                        filteredAttributes.Add(attribute);
            }
            // ReSharper restore PossibleMultipleEnumeration
            return (from a in source.Cast<Attribute>()
                    where ((a.GetType() != typeof(DataContractAttribute)) && (a.GetType() != typeof(KnownTypeAttribute))) && !filteredAttributes.Contains(a)
                    select a);
        }

        protected virtual bool HandleNonSerializableProperty(PropertyDescriptor pd) {
            return false;
        }

        internal virtual void Initialize() {
            this.IsAbstract = this.Type.IsAbstract;
            this.notificationMethodList = new List<PropertyDescriptor>();
            this.properties = this.GetPropertiesToGenerate();
        }

        private bool IsExcluded(PropertyDescriptor pd, AttributeCollection propertyAttributes) {
            var flag = propertyAttributes[typeof(ExcludeAttribute)] != null;
            if(flag && (propertyAttributes[typeof(IncludeAttribute)] != null))
                this.ClientCodeGenerator.CodeGenerationHost.LogWarning(string.Format(CultureInfo.CurrentCulture, "The property '{0}' in type '{1}' is marked with both IncludeAttribute and ExcludeAttribute.  Ignoring IncludeAttribute. Please remove the ExcludeAttribute if you want the association in the generated code.", new object[] {
                    pd.Name, this.Type
                }));
            return flag;
        }

        internal bool IsPropertyReadOnly(PropertyDescriptor property) {
            var attribute = property.Attributes[typeof(ReadOnlyAttribute)] as ReadOnlyAttribute;
            if((attribute != null) && attribute.IsReadOnly)
                return true;
            var attribute2 = property.Attributes[typeof(EditableAttribute)] as EditableAttribute;
            return ((attribute2 != null) && !attribute2.AllowEdit);
        }

        internal virtual bool IsPropertyShared(PropertyDescriptor pd) {
            return ((this.ClientCodeGenerator.GetPropertyShareKind(this.Type, pd.Name) & CodeMemberShareKind.Shared) != CodeMemberShareKind.Unknown);
        }

        protected virtual void OnPropertySkipped(PropertyDescriptor pd) {
        }

        internal virtual bool ShouldDeclareProperty(PropertyDescriptor pd) {
            var propertyAttributes = CustomEntityGenerator.ExplicitAttributes(pd);
            if(this.IsExcluded(pd, propertyAttributes))
                return false;
            return !this.IsPropertyShared(pd);
        }
    }
}
