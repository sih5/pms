﻿using System;

namespace Sunlight.Silverlight.RiaServicesCodeGen {
    internal static class Naming {
        // Methods
        private static bool IsVowel(char c) {
            switch(c) {
                case 'O':
                case 'U':
                case 'Y':
                case 'A':
                case 'E':
                case 'I':
                case 'o':
                case 'u':
                case 'y':
                case 'a':
                case 'e':
                case 'i':
                    return true;
            }
            return false;
        }

        public static string MakePluralName(string name) {
            if((name.EndsWith("x", StringComparison.OrdinalIgnoreCase) || name.EndsWith("ch", StringComparison.OrdinalIgnoreCase)) || (name.EndsWith("ss", StringComparison.OrdinalIgnoreCase) || name.EndsWith("sh", StringComparison.OrdinalIgnoreCase))) {
                name = name + "es";
                return name;
            }
            if((name.EndsWith("y", StringComparison.OrdinalIgnoreCase) && (name.Length > 1)) && !IsVowel(name[name.Length - 2])) {
                name = name.Remove(name.Length - 1, 1);
                name = name + "ies";
                return name;
            }
            if(!name.EndsWith("s", StringComparison.OrdinalIgnoreCase))
                name = name + "s";
            return name;
        }
    }
}
