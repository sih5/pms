﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.ServiceModel.DomainServices.Server;

namespace Sunlight.Silverlight.RiaServicesCodeGen {
    internal static class SerializationUtility {
        public static Type GetClientType(Type t) {
            return BinaryTypeUtility.IsTypeBinary(t) ? typeof(byte[]) : t;
        }

        public static object GetClientValue(Type targetType, object value) {
            if(value == null)
                return null;
            if((targetType == typeof(byte[])) && BinaryTypeUtility.IsTypeBinary(value.GetType()))
                return BinaryTypeUtility.GetByteArrayFromBinary(value);
            return value;
        }

        public static object GetServerValue(Type targetType, object value) {
            if(value == null)
                return null;
            var bytes = value as byte[];
            if(BinaryTypeUtility.IsTypeBinary(targetType) && (bytes != null))
                return BinaryTypeUtility.GetBinaryFromByteArray(bytes);
            return value;
        }

        public static bool IsSerializableDataMember(PropertyDescriptor propertyDescriptor) {
            if(!TypeUtility.IsPredefinedType(propertyDescriptor.PropertyType) && !TypeUtility.IsSupportedComplexType(propertyDescriptor.PropertyType))
                return false;
            if(propertyDescriptor.Attributes[typeof(ExcludeAttribute)] != null)
                return false;
            if(propertyDescriptor.Attributes[typeof(AssociationAttribute)] != null)
                return false;
            if(CustomEntityGenerator.GetAttributeCollection(propertyDescriptor.ComponentType)[typeof(DataContractAttribute)] != null) {
                if(propertyDescriptor.Attributes[typeof(DataMemberAttribute)] == null)
                    return false;
            } else if(propertyDescriptor.Attributes[typeof(IgnoreDataMemberAttribute)] != null)
                return false;
            return true;
        }
    }
}
