﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Microsoft.Office.Interop.Excel;

namespace LocalizationExporter {
    internal class Program {
        private const string FONT_NAME = "微软雅黑";
        private const int FONT_SIZE = 10;
        private const string DEFAULT_LOCALE = "zh-Hans";
        private static readonly string[] Locales = new[] {
            "en", "ja"
        };

        private static Dictionary<string, string> ReadResxData(string fileName) {
            var result = new Dictionary<string, string>();
            if(File.Exists(fileName))
                using(var stream = File.Open(fileName, FileMode.Open, FileAccess.Read)) {
                    var doc = XDocument.Load(stream, LoadOptions.PreserveWhitespace);
                    if(doc.Root != null)
                        foreach(var eData in doc.Root.Elements("data")) {
                            var aName = eData.Attribute("name");
                            if(aName == null)
                                continue;
                            var aValue = eData.Element("value");
                            result.Add(aName.Value, aValue == null ? string.Empty : aValue.Value);
                        }
                }
            return result;
        }

        private static void Process(Workbook workbook, string fileName) {
            Console.WriteLine("开始处理 {0}", fileName);

            try {
                workbook.Worksheets[1].Delete();
                workbook.Worksheets[1].Delete();

                var worksheet = workbook.Worksheets[1];
                var match = Regex.Match(fileName, @"\\(Sunlight\.Silverlight([\.a-zA-Z]+?)??)\\.+?([^\\]+).resx");
                if(match.Success) {
                    var systemName = match.Groups[2].Value.TrimStart('.');
                    if(systemName == "")
                        worksheet.Name = "App" + match.Groups[3].Value;
                    else {
                        var systemNameParts = systemName.Split(new[] {
                            '.'
                        }, StringSplitOptions.RemoveEmptyEntries);
                        if(systemNameParts.Length == 1 || match.Groups[3].Value.StartsWith(systemNameParts[1]))
                            worksheet.Name = match.Groups[3].Value;
                        else
                            worksheet.Name = systemNameParts[1] + match.Groups[3].Value;
                    }
                } else
                    worksheet.Name = "翻译表";
                worksheet.Columns.Font.Name = FONT_NAME;
                worksheet.Columns.Font.Size = FONT_SIZE;
                worksheet.Rows[1].Font.Bold = true;
                worksheet.Rows[1].Font.Color = 0xFFFFFF;
                worksheet.Rows[1].Interior.Color = 0x0;

                worksheet.Rows[1].Columns[1].Value = "Key";
                worksheet.Rows[1].Columns[2].Value = (new CultureInfo(DEFAULT_LOCALE)).DisplayName;
                var rowIndex = 2;
                foreach(var kv in ReadResxData(fileName).OrderBy(kv => kv.Key)) {
                    worksheet.Rows[rowIndex].Columns[1].Value = kv.Key;
                    worksheet.Rows[rowIndex].Columns[2].Value = kv.Value;
                    rowIndex++;
                }

                var columnIndex = 3;
                foreach(var locale in Locales) {
                    worksheet.Rows[1].Columns[columnIndex].Value = (new CultureInfo(locale)).DisplayName;
                    var localeData = ReadResxData(Path.ChangeExtension(fileName, locale + ".resx"));
                    rowIndex = 2;
                    string key;
                    while((key = worksheet.Rows[rowIndex].Columns[1].Value) != null) {
                        if(localeData.ContainsKey(key))
                            worksheet.Rows[rowIndex].Columns[columnIndex].Value = localeData[key];
                        rowIndex++;
                    }
                    columnIndex++;
                }

                for(var i = 1; i <= Locales.Length + 2; i++)
                    worksheet.Columns[i].AutoFit();

                worksheet.Activate();
                worksheet.Application.ActiveWindow.SplitRow = 1;
                worksheet.Application.ActiveWindow.FreezePanes = true;

                // ReSharper disable AssignNullToNotNullAttribute
                var excelFileName = Path.Combine(Path.GetDirectoryName(fileName), worksheet.Name + ".xlsx");
                // ReSharper restore AssignNullToNotNullAttribute
                if(File.Exists(excelFileName))
                    File.Delete(excelFileName);
                workbook.SaveAs(excelFileName);
            } finally {
                workbook.Close(false);
            }
        }

        private static void Main(string[] args) {
            var baseDirectory = args.Length > 0 ? args[0] : AppDomain.CurrentDomain.BaseDirectory;
            var resourceFiles = Directory.EnumerateFiles(baseDirectory, "*.resx", SearchOption.AllDirectories).Where(path => {
                var fileName = Path.GetFileNameWithoutExtension(path);
                if(fileName == null)
                    return false;
                if(fileName.Count(c => c == '.') > 1)
                    return false;
                return new[] {
                    "UI", "Entity", "Error"
                }.Any(s => fileName.EndsWith(s + "Strings"));
            }).ToArray();

            var excel = new Application();
            try {
                excel.Visible = false;
                foreach(var resourceFile in resourceFiles)
                    Process(excel.Workbooks.Add(), resourceFile);
            } finally {
                excel.Quit();
            }
        }
    }
}
