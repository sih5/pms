﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using LocalizationImporter.Properties;
using Microsoft.Office.Interop.Excel;

namespace LocalizationExporter {
    internal class Program {
        private static void WriteResxData(string fileName, Dictionary<string, string> data) {
            if(!File.Exists(fileName))
                File.Delete(fileName);
            File.WriteAllText(fileName, Resources.ResxTemplate, Encoding.UTF8);

            XDocument doc;
            using(var stream = File.Open(fileName, FileMode.Open, FileAccess.Read)) {
                doc = XDocument.Load(stream, LoadOptions.PreserveWhitespace);
                if(doc.Root != null)
                    foreach(var kv in data) {
                        var eData = doc.Root.Elements("data").Where(e => {
                            var attribute = e.Attribute("name");
                            return attribute != null && attribute.Value == kv.Key;
                        }).SingleOrDefault();
                        if(eData == null)
                            doc.Root.Add(new XElement("data", new XAttribute("name", kv.Key), new XAttribute(XNamespace.Xml + "space", "preserve"), new XElement("value", kv.Value)));
                        else {
                            var eValue = eData.Element("value");
                            if(eValue == null)
                                eData.Add(new XElement("value", kv.Value));
                            else
                                eValue.Value = kv.Value;
                        }
                    }
            }
            //using(var writer = new XmlTextWriter(fileName, Encoding.UTF8) {
            //    Formatting = Formatting.Indented,
            //    IndentChar = ' ',
            //    Indentation = 2,
            //})
            //    doc.Save(writer);
            using(var stream = File.Open(fileName, FileMode.Open, FileAccess.Write))
                doc.Save(stream, SaveOptions.None);
        }

        private static void Process(Workbook workbook, string fileName) {
            // ReSharper disable LocalizableElement
            Console.WriteLine("开始处理 {0}", fileName);
            // ReSharper restore LocalizableElement

            try {
                var worksheet = workbook.Worksheets[1];

                for(var columnIndex = 3;; columnIndex++) {
                    var localeName = worksheet.Rows[1].Columns[columnIndex].Value;
                    if(localeName == null)
                        break;
                    var cultureInfo = CultureInfo.GetCultures(CultureTypes.AllCultures).FirstOrDefault(c => c.DisplayName == localeName);
                    if(cultureInfo == null)
                        continue;

                    var data = new Dictionary<string, string>();
                    for(var rowIndex = 2;; rowIndex++) {
                        string key = worksheet.Rows[rowIndex].Columns[1].Value;
                        if(key == null)
                            break;
                        string value = worksheet.Rows[rowIndex].Columns[columnIndex].Value;
                        if(value == null)
                            continue;
                        data.Add(key, value);
                    }

                    // ReSharper disable PossibleNullReferenceException, AssignNullToNotNullAttribute
                    string resxFileName = null;
                    var path = Path.GetDirectoryName(fileName);
                    var fileName1 = Path.GetFileNameWithoutExtension(fileName);
                    if(fileName1.EndsWith("UIStrings"))
                        resxFileName = Directory.GetFiles(path, string.Format("*UIStrings.{0}.resx", cultureInfo.Name), SearchOption.TopDirectoryOnly).FirstOrDefault();
                    else if(fileName1.EndsWith("EntityStrings"))
                        resxFileName = Directory.GetFiles(path, string.Format("*EntityStrings.{0}.resx", cultureInfo.Name), SearchOption.TopDirectoryOnly).FirstOrDefault();

                    if(!string.IsNullOrEmpty(resxFileName))
                        WriteResxData(resxFileName, data);
                    // ReSharper restore PossibleNullReferenceException, AssignNullToNotNullAttribute
                }
            } finally {
                workbook.Close(false);
            }
        }

        private static void Main(string[] args) {
            var baseDirectory = args.Length > 0 ? args[0] : AppDomain.CurrentDomain.BaseDirectory;
            var excelFiles = Directory.EnumerateFiles(baseDirectory, "*.xlsx", SearchOption.AllDirectories).Where(path => path.Contains("\\Sunlight.Silverlight") && !path.Contains("~$")).ToArray();

            var excel = new Application();
            try {
                excel.Visible = false;
                foreach(var excelFile in excelFiles)
                    Process(excel.Workbooks.Open(excelFile), excelFile);
            } finally {
                excel.Quit();
            }
        }
    }
}
