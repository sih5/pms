﻿using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace PasswordHash {
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow {
        private static string HashPassword(string password) {
            var data = Encoding.UTF8.GetBytes(password);
            using(var sha1 = new SHA1Managed())
                data = sha1.ComputeHash(data);
            var sbResult = new StringBuilder(data.Length * 2);
            foreach(var b in data)
                sbResult.AppendFormat("{0:X2}", b);
            return sbResult.ToString();
        }

        public MainWindow() {
            this.InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e) {
            this.HashedPassword.Text = HashPassword(((TextBox)sender).Text);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            this.Password.Focus();
        }
    }
}
