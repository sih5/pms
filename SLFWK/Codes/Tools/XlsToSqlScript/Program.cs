﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using Microsoft.Office.Interop.Excel;

namespace XlsToSqlScript {
    internal class Program {
        private static void Main() {
            const string FILE_NAME = @"D:\CAMDB\Customer.xls";
            const string TABLE_NAME = "Customer";

            var excel = new Application();
            try {
                var outputFileName = Path.Combine(Path.GetDirectoryName(FILE_NAME) ?? "", TABLE_NAME + ".sql");
                if(File.Exists(outputFileName))
                    File.Delete(outputFileName);

                excel.Workbooks.Open(FILE_NAME, ReadOnly: true);
                var worksheet = excel.Workbooks[1].Worksheets[1];

                var insert = new StringBuilder();
                insert.AppendFormat("INSERT INTO [{0}] (", TABLE_NAME);
                var columnCount = 0;
                var columns = worksheet.Rows[1].Columns;
                for(var i = 1; i <= columns.Count; i++) {
                    if(columns[i].Value == null)
                        break;
                    if(i > 1)
                        insert.Append(", ");
                    insert.AppendFormat("[{0}]", columns[i].Value);
                    columnCount++;
                }
                insert.Append(") VALUES");
                var insertText = insert.ToString();

                var output = new StringBuilder();
                output.AppendFormat("TRUNCATE TABLE [{0}]\r\nGO\r\n\r\n", TABLE_NAME);
                output.AppendFormat("SET IDENTITY_INSERT [{0}] ON\r\n", TABLE_NAME);

                //用二叉树算法计算最大行数
                var rowCount = (int)ushort.MaxValue;
                if(worksheet.Rows[rowCount].Columns[1].Value == null) {
                    var binaryCount = 1;
                    while(true) {
                        var slice = ushort.MaxValue / (int)Math.Pow(2, binaryCount++);
                        if(slice < 1)
                            slice = 1;

                        if(worksheet.Rows[rowCount].Columns[1].Value == null) {
                            //大了
                            rowCount -= slice;
                            if(worksheet.Rows[rowCount].Columns[1].Value != null && worksheet.Rows[rowCount + 1].Columns[1].Value == null)
                                break;
                        } else {
                            //小了
                            rowCount += slice;
                            if(worksheet.Rows[rowCount].Columns[1].Value == null && worksheet.Rows[rowCount - 1].Columns[1].Value != null) {
                                rowCount--;
                                break;
                            }
                        }
                    }
                    rowCount--;
                }

                var count = 0;
                var watch = new Stopwatch();
                watch.Start();
                while(count < rowCount) {
                    if(count > 0 && count % 20 == 0) {
                        var per = watch.ElapsedMilliseconds / count;
                        var left = per * rowCount - watch.ElapsedMilliseconds;
                        Console.Clear();
                        Console.WriteLine(string.Format("已经处理：{0} / {1} 行 ({2:P0})", count, rowCount, (double)count / rowCount));
                        Console.WriteLine(string.Format("平均耗时：{0:N0} 毫秒", TimeSpan.FromMilliseconds(per).TotalMilliseconds));
                        Console.WriteLine(string.Format("预计完成：{0:N0} 秒", TimeSpan.FromMilliseconds(left).TotalSeconds));
                    }

                    if(count % 1000 == 0) {
                        if(count > 0) {
                            output.AppendLine(";\r\nGO");
                            File.AppendAllText(outputFileName, output.ToString(), Encoding.Default);
                            output.Clear();
                        }
                        output.AppendLine(insertText);
                    } else
                        output.AppendLine(",");

                    output.Append("(");
                    columns = worksheet.Rows[count + 2].Columns;
                    for(var j = 1; j <= columnCount; j++) {
                        var column = columns[j];
                        if(j > 1)
                            output.Append(", ");

                        var numberFormat = (string)column.NumberFormat;

                        DateTime dateTime;
                        if(numberFormat.StartsWith("yy") && DateTime.TryParse(column.Value as string, out dateTime))
                            output.AppendFormat("'{0}'", dateTime.TimeOfDay.TotalMinutes < 1 ? dateTime.ToString("yyyy-MM-dd") : dateTime.ToString("yyyy-MM-dd HH:mm:ss"));
                        else
                            switch(numberFormat) {
                                case "0_ ":
                                case "#,##0.00_ ":
                                    output.AppendFormat("{0}", column.Value ?? "null");
                                    break;
                                default:
                                    output.AppendFormat("{0}", column.Value == null ? "null" : string.Format("N'{0}'", column.Value.ToString().Replace("'", "''")));
                                    break;
                            }
                    }
                    output.Append(")");
                    count++;
                }
                if(count > 0)
                    output.AppendLine(";");
                watch.Stop();

                output.AppendFormat("SET IDENTITY_INSERT [{0}] OFF\r\nGO\r\n", TABLE_NAME);
                File.AppendAllText(outputFileName, output.ToString(), Encoding.Default);
            } finally {
                excel.Quit();
            }
        }
    }
}
