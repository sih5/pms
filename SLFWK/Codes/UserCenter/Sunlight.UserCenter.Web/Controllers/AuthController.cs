﻿using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth2;

namespace Sunlight.UserCenter.Controllers {
    public class AuthController : Controller {
        //
        // GET: /Auth/

        private readonly AuthorizationServer authorizationServer = new AuthorizationServer(new OAuth2AuthorizationServer());

        public ActionResult Token() {
            return this.authorizationServer.HandleTokenRequest(this.Request).AsActionResult();
        }

        public ActionResult Test() {
            var entities = new UserCenterEntities();
            entities.AddToNonces(new Nonce {
                Code = "dasdas",
                Context = "dasdas",
                Timestamp = DateTime.Now,
            });
            entities.SaveChanges();
            return new EmptyResult();
        }

        //[Authorize]
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Authorize() {
            var pendingRequest = this.authorizationServer.ReadAuthorizationRequest();
            if(pendingRequest == null)
                throw new HttpException((int)HttpStatusCode.BadRequest, "认证请求格式有误");

            //var requestingClient = MvcApplication.DataContext.Clients.First(c => c.ClientIdentifier == pendingRequest.ClientIdentifier);

            //// Consider auto-approving if safe to do so.
            //if(((OAuth2AuthorizationServer)this.authorizationServer.AuthorizationServerServices).CanBeAutoApproved(pendingRequest)) {
            //    var approval = this.authorizationServer.PrepareApproveAuthorizationRequest(pendingRequest, this.HttpContext.User.Identity.Name);
            //    return this.authorizationServer.Channel.PrepareResponse(approval).AsActionResult();
            //}

            //var model = new AccountAuthorizeModel {
            //    ClientApp = requestingClient.Name,
            //    Scope = pendingRequest.Scope,
            //    AuthorizationRequest = pendingRequest,
            //};

            //return View(model);

            return this.View();
        }
    }
}
