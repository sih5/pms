﻿using System.Web.Mvc;

namespace Sunlight.UserCenter.Controllers {
    public class HomeController : Controller {
        //
        // GET: /Home/

        public ActionResult Index() {
            //var provider = CertificateUtils.GetPublicRSACryptoServiceProvider("SDS");

            //var encryptedData = provider.Encrypt(Encoding.UTF8.GetBytes("Test"), false);
            //var decryptedData = Encoding.UTF8.GetString(provider.Decrypt(encryptedData, false));

            return this.View();
        }
    }
}
