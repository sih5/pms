﻿using System;
using System.Linq;
using DotNetOpenAuth.Messaging.Bindings;

namespace Sunlight.UserCenter {
    public class DatabaseKeyNonceStore : INonceStore {
        public bool StoreNonce(string context, string nonce, DateTime timestampUtc) {
            var entities = new UserCenterEntities();
            var existed = entities.Nonces.SingleOrDefault(n => n.Context == context && n.Code == nonce && n.Timestamp == timestampUtc);
            if(existed != null)
                return false;

            entities.Nonces.AddObject(new Nonce {
                Context = context,
                Code = nonce,
                Timestamp = timestampUtc,
            });

            entities.SaveChanges();
            return true;
        }
    }
}
