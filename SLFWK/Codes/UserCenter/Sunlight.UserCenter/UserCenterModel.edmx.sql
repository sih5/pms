
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 07/18/2012 17:06:00
-- Generated from EDMX file: D:\SLFWK\Codes\UserCenter\Sunlight.UserCenter\UserCenterModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [UserCenter];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Clients]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Clients];
GO
IF OBJECT_ID(N'[dbo].[Nonces]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Nonces];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Clients'
CREATE TABLE [dbo].[Clients] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ClientId] nvarchar(8)  NOT NULL,
    [Password] nvarchar(40)  NOT NULL,
    [Name] nvarchar(100)  NOT NULL,
    [RowVersion] binary(8)  NULL
);
GO

-- Creating table 'Nonces'
CREATE TABLE [dbo].[Nonces] (
    [Id] int  NOT NULL,
    [Context] nvarchar(100)  NOT NULL,
    [Code] nvarchar(100)  NOT NULL,
    [Timestamp] datetime  NOT NULL,
    [RowVersion] binary(8)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Clients'
ALTER TABLE [dbo].[Clients]
ADD CONSTRAINT [PK_Clients]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Nonces'
ALTER TABLE [dbo].[Nonces]
ADD CONSTRAINT [PK_Nonces]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------