@echo off
set MSB=%windir%\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe
set SOURCE=%CD%\Codes\Frameworks
set PARAM=/nologo /v:m /l:FileLogger,Microsoft.Build.Engine;logfile=CompileCode.log;verbosity=diagnostic /p:Configuration=Release;WarningLevel=0;Optimize=true;DebugType=pdbonly;DebugSymbols=false;BuildInParallel=true
set OUT=%CD%\Outputs\Frameworks

echo * 正在删除旧产出物 *
REM 排除%OUT%目录下的.svn目录。
forfiles /P "%OUT%" /C "cmd /C if @file NEQ \".svn\" (if @ISDIR==TRUE (rmdir /S /Q @Path) else (del /S /Q @path))"
echo * 旧产出物删除完成 *
echo.

echo * 正在编译 *
%MSB% %PARAM% "%SOURCE%\Frameworks.sln"
if errorlevel 1 goto err
echo * 所有项目编译完成 *
echo.
goto ok

:err
echo 
echo * 编译时出现错误，请检查错误信息，处理之后再继续 *
echo.

:ok
set OUT=
set PARAM=
set SOURCE=
set MSB=
if [%1] == [/autoclose] exit /b 0
pause
