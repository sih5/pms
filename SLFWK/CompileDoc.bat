@echo off
set MSB=%windir%\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe
set SOURCE=%CD%\Codes\Documents
set OUT=%CD%\Outputs\Frameworks

echo * 正在编译API文档 *
%MSB% /nologo /v:m "%SOURCE%\CoreAPI\CoreAPI.shfbproj"
copy /Y "%SOURCE%\CoreAPI\*.chm" "%OUT%"
if errorlevel 1 goto err
echo * 所有项目编译完成 *
echo.
goto ok

:err
echo 
echo * 编译时出现错误，请检查错误信息，处理之后再继续 *
echo.

:ok
set OUT=
set SOURCE=
set MSB=
if [%1] == [/autoclose] exit /b 0
pause
