﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Threading;
using System.Web.Configuration;
using System.Xml.Linq;
using NLog;
using NLog.Targets;

namespace Sunlight.Silverlight.Log.Web {
    public partial class LogManager {
        public static LogManager logManager = new LogManager();

        //是否开启日志
        private bool enable = false;

        //是否记录调度参数
        private bool paramEnable = false;

        //是否记录调度入口参数中的实体对象数据
        private bool paramEntityEnabled = true;
        //是否记录调度入口参数中的实体对象的原始数据
        private bool paramOriginalEntityEnabled = true;

        //是否记录Query操作结果
        private bool queryResultEnabled = false;

        //是否记录方法详细信息
        private bool detailEnable = false;

        private readonly PersistentLogManagerBase persistentLogManager;

        public LogManager() {
            Boolean.TryParse(WebConfigurationManager.AppSettings["isLogEnabled"], out enable);
            Boolean.TryParse(WebConfigurationManager.AppSettings["isLogParamEnabled"], out paramEnable);
            Boolean.TryParse(WebConfigurationManager.AppSettings["isLogDetailEnabled"], out detailEnable);
            Boolean.TryParse(WebConfigurationManager.AppSettings["isLogQueryResultEnabled"], out queryResultEnabled);
            Boolean.TryParse(WebConfigurationManager.AppSettings["isLogParamEntityEnabled"], out paramEntityEnabled);
            Boolean.TryParse(WebConfigurationManager.AppSettings["isLogParamOriginalEntityEnabled"], out paramOriginalEntityEnabled);

            if(NLog.LogManager.Configuration == null || NLog.LogManager.Configuration.LoggingRules == null)
                return;
            var rule = NLog.LogManager.Configuration.LoggingRules.SingleOrDefault(v => v.LoggerNamePattern == "Sunlight.Silverlight.Log.Web.*");
            if(rule == null)
                return;
            if(rule.Targets == null || !rule.Targets.Any())
                return;

            if(rule.Targets.First() is FileTarget)
                persistentLogManager = new PersistentLogByFile();
            else if(rule.Targets.First() is DatabaseTarget)
                persistentLogManager = new PersistentLogByDataBase();
        }

        public LogInfo NewLogInfo(string systemCode, string loginId, string name, string enterpriseCode, string enterpriseName, OperatingType operatingType, object param) {
            var logInfo = new LogInfo(enable, paramEnable, paramEntityEnabled, paramOriginalEntityEnabled, queryResultEnabled, detailEnable);
            if(enable)
                logInfo.InitLogInfo(systemCode, loginId, name, enterpriseCode, enterpriseName, operatingType, param);
            return logInfo;
        }

        public void SetLogInfoComplete(LogInfo logInfo) {
            if(!enable)
                return;
            logInfo.EndTime = DateTime.Now;
            new Thread(new ParameterizedThreadStart(persistentLogManager.Save)).Start(logInfo);
        }
    }

    public class LogInfo {
        public string Id {
            get {
                return string.Format("{0}|{1}", ClientIP, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
            }
        }

        public string ClientIP {
            get;
            private set;
        }

        public string SystemCode {
            get;
            private set;
        }

        public string OperatorID {
            get;
            private set;
        }

        public string OperatorName {
            get;
            private set;
        }

        public string EnterpriseCode {
            get;
            private set;
        }

        public string EnterpriseName {
            get;
            private set;
        }

        public DateTime StartTime {
            get;
            private set;
        }

        public DateTime EndTime {
            get;
            set;
        }

        public OperatingType Type {
            get;
            private set;
        }

        public bool Success {
            get;
            private set;
        }

        public object Param {
            get;
            private set;
        }

        public XElement ParamXml {
            get;
            private set;
        }

        public object QueryResult {
            get;
            set;
        }

        public XElement QueryResultXml {
            get;
            set;
        }


        public string ErrorMsg {
            get;
            private set;
        }

        public List<LogDetailInfo> LogDetailInfos {
            get;
            private set;
        }

        //是否开启日志
        private bool enable = false;

        //是否记录调度参数
        private bool paramEnable = false;

        //是否记录调度入口参数中的实体对象数据
        private bool paramEntityEnabled = true;
        //是否记录调度入口参数中的实体对象的原始数据
        private bool paramOriginalEntityEnabled = true;

        //是否记录Query操作结果
        private bool queryResultEnabled = false;

        //是否记录方法详细信息
        private bool detailEnable = false;

        public LogInfo(bool enable, bool paramEnable, bool paramEntityEnabled, bool paramOriginalEntityEnabled, bool queryResultEnabled, bool detailEnable) {
            this.enable = enable;
            this.paramEnable = paramEnable;
            this.paramEntityEnabled = paramEntityEnabled;
            this.paramOriginalEntityEnabled = paramOriginalEntityEnabled;
            this.queryResultEnabled = queryResultEnabled;
            this.detailEnable = detailEnable;
        }

        public void InitLogInfo(string systemCode, string loginId, string name, string enterpriseCode, string enterpriseName, OperatingType operatingType, object param) {
            SystemCode = string.IsNullOrEmpty(systemCode) ? "" : systemCode;
            OperatorID = string.IsNullOrEmpty(loginId) ? "" : loginId;
            OperatorName = string.IsNullOrEmpty(name) ? "" : name;
            EnterpriseCode = string.IsNullOrEmpty(enterpriseCode) ? "" : enterpriseCode;
            EnterpriseName = string.IsNullOrEmpty(enterpriseName) ? "" : enterpriseName;
            Type = operatingType;

            SetParam(param);

            ClientIP = System.Web.HttpContext.Current.Request.UserHostAddress;
            Success = true;
            ErrorMsg = "";
            StartTime = DateTime.Now;
        }

        private void SetParam(object param) {
            if(!enable || !paramEnable || param == null)
                return;
            if(!(param is ChangeSet || param is QueryDescription))
                return;

            Param = param;

            if(Param is ChangeSet) {
                var changeSet = Param as ChangeSet;
                ParamXml = new XElement("ChangeSetEntries");
                foreach(var changeSetEntry in changeSet.ChangeSetEntries) {
                    var changeSetEntryXml = new XElement("ChangeSetEntry",
                        new XAttribute("Id", changeSetEntry.Id),
                        new XAttribute("Operation", changeSetEntry.Operation),
                        new XAttribute("HasError", false),
                        new XAttribute("EntryType", changeSetEntry.Entity.GetType().FullName));
                    if(paramEntityEnabled)
                        changeSetEntryXml.Add(new XElement("Entity", Utils.EntityToElement(changeSetEntry.Entity, false)));
                    if(paramOriginalEntityEnabled && changeSetEntry.OriginalEntity != null)
                        changeSetEntryXml.Add(new XElement("OriginalEntity", Utils.EntityToElement(changeSetEntry.OriginalEntity, false)));
                    if(changeSetEntry.EntityActions != null) {
                        var entityActionsXml = new XElement("EntityActions");
                        foreach(var action in changeSetEntry.EntityActions) {
                            var valuesXml = Utils.XmlObject(new XElement("Values"), action.Value);
                            entityActionsXml.Add(new XElement("EntityAction", new XAttribute("Name", action.Key), valuesXml));
                        }
                        changeSetEntryXml.Add(entityActionsXml);
                    }
                    ParamXml.Add(changeSetEntryXml);
                }
            } else if(Param is QueryDescription) {
                var queryDescription = Param as QueryDescription;
                ParamXml = new XElement("QueryDescription", new XAttribute("Method", queryDescription.Method));
                if(queryDescription.Query != null && queryDescription.Query.Expression != null)
                    ParamXml.Add(new XAttribute("Expression", queryDescription.Query.Expression));
                if(queryDescription.ParameterValues.Length > 0) {
                    var parameterValuesXml = new XElement("ParameterValues");
                    foreach(var parameterValue in queryDescription.ParameterValues)
                        parameterValuesXml.Add(new XElement("ParameterValue", parameterValue));
                    ParamXml.Add(parameterValuesXml);
                }
            }
        }

        /// <summary>
        /// 记录方法详细信息
        /// </summary>
        /// <param name="type">日志类型，标记记录内容类型，目前该字段仅用于分类，不同分类处理方式一致</param>
        /// <param name="remark">说明</param>
        /// <param name="param">附加内容，允许传入数组、实体对象</param>
        /// <example>
        /// // logInfo为DomainService定义的类型为LogInfo的全局变量
        /// // agencyService、name为业务服务方法传入参数，其中agencyService为实体对象，name为string
        /// logInfo.AddDetailLog(LogDetailType.Param, "方法传入参数", new object[] { agencyService , name});
        /// </example>
        /// <returns></returns>
        public bool AddDetailLog(LogDetailType type, string remark, object param, bool includeDetail = true) {
            if(!enable || !detailEnable)
                return false;

            var detailInfo = new LogDetailInfo() {
                Type = type,
                Remark = remark,
            };
            if(param != null)
                detailInfo.SetContext(param, includeDetail);

            if(LogDetailInfos == null)
                LogDetailInfos = new List<LogDetailInfo>();
            detailInfo.Id = LogDetailInfos.Count + 1;
            LogDetailInfos.Add(detailInfo);
            return true;
        }

        public void SetErrorLog(object param, Exception ex) {
            if(!enable)
                return;
            Exception exception = ex;
            var errorMessages = new List<string>() { exception.Message };
            while(exception.InnerException != null) {
                exception = exception.InnerException;
                errorMessages.Add(exception.Message);
            }
            SetErrorLog(param, errorMessages.ToArray());
        }

        public void SetErrorLog(object param, params string[] messages) {
            if(!enable)
                return;
            Success = false;
            ErrorMsg = string.Join("\r\n", messages);

            if(!paramEnable || param == null || Param == null)
                return;

            if(Param is ChangeSet) {
                var changeSet = Param as ChangeSet;
                var changeSetEntriesXml = ParamXml.Elements("ChangeSetEntry");
                foreach(var changeSetEntry in changeSet.ChangeSetEntries) {
                    if(!changeSetEntry.HasError)
                        continue;
                    var xml = changeSetEntriesXml.Single(v => string.Compare(v.Attribute("Id").Value, changeSetEntry.Id.ToString(), true) == 0);
                    xml.SetAttributeValue("HasError", true);
                    if(changeSetEntry.ValidationErrors != null) {
                        var validationErrorsXml = new XElement("ValidationErrors");
                        foreach(var error in changeSetEntry.ValidationErrors)
                            validationErrorsXml.Add(new XElement("ValidationError", error.Message));
                        xml.Add(validationErrorsXml);
                    }
                    break;
                }
            } else if(Param is IEnumerable<ValidationResult>) {
                var validationResults = Param as IEnumerable<ValidationResult>;
                string tmpMsg = null;
                if(validationResults.Count() > 0)
                    tmpMsg = string.Join("\r\n", validationResults.Select(v => string.Format("message：{0}，memberNames：[{1}]", v.ErrorMessage, string.Join("、", v.MemberNames))));
                if(!string.IsNullOrWhiteSpace(tmpMsg))
                    ErrorMsg = string.Concat(ErrorMsg, "\r\n", tmpMsg);
            }
        }

        public void SetQueryResult(IEnumerable queryResult, int totalCount) {
            if(!enable)
                return;
            if(!queryResultEnabled || queryResult == null)
                return;

            QueryResult = queryResult;
            QueryResultXml = new XElement("QueryResults", new XAttribute("TotalCount", totalCount));
            QueryResultXml = Utils.XmlObject(QueryResultXml, queryResult);
        }
    }

    public class LogDetailInfo {
        public int Id {
            get;
            set;
        }

        public DateTime ExecutionTime {
            get;
            private set;
        }

        public string MethodName {
            get;
            private set;
        }

        public string Remark {
            get;
            set;
        }

        public LogDetailType Type {
            get;
            set;
        }

        public object Context {
            get;
            private set;
        }

        public XElement ContextXml {
            get;
            private set;
        }

        public LogDetailInfo() {
            ExecutionTime = DateTime.Now;

            //var method = new System.Diagnostics.StackFrame(1).GetMethod();
            //MethodName = string.Format("{0}({1})", method.Name, string.Join(",", method.GetParameters().Select(v => string.Format("{0} {1}", v.ParameterType, v.Name))));
            MethodName = new System.Diagnostics.StackFrame(1).GetMethod().ToString();
        }

        public void SetContext(object context, bool includeDetail = true) {
            Context = context;
            ContextXml = Utils.XmlObject(ContextXml ?? new XElement("Context"), context, includeDetail);
        }
    }

    public enum OperatingType {
        Query = 1,
        Submit = 2,
        Invoke = 3
    }

    public enum LogDetailType {
        Param = 1,
        Error = 2,
        Result = 3,
        Other = 0
    }

    abstract partial class PersistentLogManagerBase {
        protected Logger logger = NLog.LogManager.GetCurrentClassLogger();

        abstract public void Save(object logInfo);

        protected XElement GetDetailInfosXml(List<LogDetailInfo> logDetailInfos) {
            var detailInfosXml = new XElement("DetailInfos");
            foreach(var detail in logDetailInfos)
                detailInfosXml.Add(new XElement("DetailInfo",
                    new XAttribute("Id", detail.Id),
                    new XAttribute("MethodName", detail.MethodName),
                    new XAttribute("ExecutionTime", detail.ExecutionTime.ToString("yyyy-MM-dd HH:mm:ss.ffff")),
                    new XAttribute("Remark", detail.Remark),
                    new XAttribute("Type", (int)detail.Type),
                    detail.ContextXml == null ? new XElement("Context") : detail.ContextXml));
            return detailInfosXml;
        }
    }

    partial class PersistentLogByFile : PersistentLogManagerBase {
        private readonly string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;

        public override void Save(object param) {
            var logInfo = param as LogInfo;
            var logInfoXml = new XElement("LogInfo",
                new XAttribute("Id", logInfo.Id),
                new XAttribute("ClientIP", logInfo.ClientIP),
                new XAttribute("SystemCode", logInfo.SystemCode),
                new XAttribute("OperatorID", logInfo.OperatorID),
                new XAttribute("OperatorName", logInfo.OperatorName),
                new XAttribute("EnterpriseCode", logInfo.EnterpriseCode),
                new XAttribute("EnterpriseName", logInfo.EnterpriseName),
                new XAttribute("StartTime", logInfo.StartTime.ToString("yyyy-MM-dd HH:mm:ss.ffff")),
                new XAttribute("EndTime", logInfo.EndTime.ToString("yyyy-MM-dd HH:mm:ss.ffff")),
                new XAttribute("Type", (int)logInfo.Type),
                new XAttribute("Success", logInfo.Success),
                new XAttribute("ErrorMsg", logInfo.ErrorMsg));
            if(logInfo.ParamXml != null)
                logInfoXml.Add(logInfo.ParamXml);
            if(logInfo.QueryResultXml != null)
                logInfoXml.Add(logInfo.QueryResultXml);
            if(logInfo.LogDetailInfos != null && logInfo.LogDetailInfos.Count > 0)
                logInfoXml.Add(GetDetailInfosXml(logInfo.LogDetailInfos));
            logger.Info(logInfoXml.ToString());
        }
    }

    class PersistentLogByDataBase : PersistentLogManagerBase {
        public override void Save(object param) {
            var logInfo = param as LogInfo;
            var eventInfo = new LogEventInfo(LogLevel.Info, logger.Name, "");
            eventInfo.Properties["id"] = logInfo.Id;
            eventInfo.Properties["clientIP"] = logInfo.ClientIP;
            eventInfo.Properties["systemCode"] = logInfo.SystemCode;
            eventInfo.Properties["operatorID"] = logInfo.OperatorID;
            eventInfo.Properties["operatorName"] = logInfo.OperatorName;
            eventInfo.Properties["enterpriseCode"] = logInfo.EnterpriseCode;
            eventInfo.Properties["enterpriseName"] = logInfo.EnterpriseName;
            eventInfo.Properties["startTime"] = logInfo.StartTime;
            eventInfo.Properties["endTime"] = logInfo.EndTime;
            eventInfo.Properties["type"] = (int)logInfo.Type;
            eventInfo.Properties["success"] = logInfo.Success;
            eventInfo.Properties["paramXml"] = logInfo.ParamXml == null ? "" : logInfo.ParamXml.ToString();
            eventInfo.Properties["queryResultXml"] = logInfo.QueryResultXml == null ? "" : logInfo.QueryResultXml.ToString();
            eventInfo.Properties["errorMsg"] = logInfo.ErrorMsg;
            eventInfo.Properties["logDetailInfosXml"] = (logInfo.LogDetailInfos == null || logInfo.LogDetailInfos.Count == 0) ? "" : GetDetailInfosXml(logInfo.LogDetailInfos).ToString();
            logger.Log(eventInfo);
        }
    }

    public static class Utils {
        public static XElement EntityToElement(object entity, bool includeDetail) {
            Type type = entity.GetType();
            XElement eEntity = new XElement(type.Name);
            foreach(var property in type.GetProperties()) {
                var value = property.GetValue(entity, null);
                if(value == null)
                    continue;
                if(property.PropertyType.FullName == "System.Data.EntityState" || property.PropertyType.BaseType.FullName == "System.Data.Objects.DataClasses.EntityReference")
                    continue;
                if(property.PropertyType.FullName == value.ToString())
                    continue;
                if(!property.PropertyType.Equals(typeof(string)) && typeof(IEnumerable).IsAssignableFrom(property.PropertyType) && property.PropertyType.IsGenericType) {
                    if(!includeDetail)
                        continue;
                    var genericTypes = property.PropertyType.GetGenericArguments();
                    if(genericTypes.Length == 0)
                        continue;
                    var detail = new XElement(property.Name);
                    foreach(var detailEntity in value as IEnumerable)
                        detail.Add(EntityToElement(detailEntity, includeDetail));
                    eEntity.Add(detail);
                } else
                    eEntity.Add(new XAttribute(property.Name, value));
            }
            return eEntity;
        }

        public static XElement XmlObject(XElement xml, object param, bool includeDetail=true) {
            if(param is string)
                xml.Add(new XElement("Value", param));
            else if(param is IEnumerable)
                foreach(var p in param as IEnumerable)
                    xml = XmlObject(xml, p, includeDetail);
            else if(param is EntityObject || (param != null && param.GetType().IsClass))
                xml.Add(EntityToElement(param, includeDetail));
            else
                xml.Add(new XElement("Value", param));
            return xml;
        }
    }
}
