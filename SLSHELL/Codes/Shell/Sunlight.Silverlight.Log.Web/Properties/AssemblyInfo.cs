﻿using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Sunlight.Silverlight.Log.Web")]
[assembly: AssemblyCompany("上海晨阑数据技术有限公司")]
[assembly: AssemblyProduct("晨阑Silverlight产品平台")]
[assembly: AssemblyCopyright("版权所有(c) 上海晨阑数据技术有限公司")]
[assembly: ComVisible(false)]
[assembly: NeutralResourcesLanguage("zh-CN")]
[assembly: AssemblyVersion("2012.9.2.*")]
