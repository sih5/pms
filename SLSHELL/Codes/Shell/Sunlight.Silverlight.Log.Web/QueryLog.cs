﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.ServiceModel.DomainServices.EntityFramework;
using System.ServiceModel.DomainServices.Hosting;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Xml.Linq;
using Sunlight.Silverlight.Log.Web.Resources;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Log.Web {
    public partial class LogManager {
        public IEnumerable<XElement> GetLogs(string id, string clientIP, string systemCode, string operatorID, string operatorName, DateTime startTimeBegin, DateTime startTimeEnd, int type, bool? success) {
            if(persistentLogManager == null)
                return Enumerable.Empty<XElement>();
            return persistentLogManager.GetLogs(id, clientIP, systemCode, operatorID, operatorName, startTimeBegin, startTimeEnd, type, success);
        }
    }

    abstract partial class PersistentLogManagerBase {
        public virtual IEnumerable<XElement> GetLogs(DateTime startTime, DateTime endTime) {
            return null;
        }

        public virtual IEnumerable<XElement> GetLogs(string id, string clientIP, string systemCode, string operatorID, string operatorName, DateTime startTimeBegin, DateTime startTimeEnd, int type, bool? success) {
            return null;
        }
    }

    partial class PersistentLogByFile {
        private Dictionary<string, LogInfoWithWriteTime> dicLogs = null;

        private struct LogInfoWithWriteTime {
            public DateTime writeTime;
            public IEnumerable<XElement> logInfos;
        }

        public override IEnumerable<XElement> GetLogs(DateTime startTime, DateTime endTime) {
            var result = new List<XElement>();
            if(dicLogs == null)
                dicLogs = new Dictionary<string, LogInfoWithWriteTime>();
            for(DateTime start = startTime.Date, end = endTime.Date; start <= endTime; start = start.AddDays(1)) {
                var logPath = GetLogPath(start);
                IEnumerable<XElement> currentlogs = null;
                if(dicLogs.ContainsKey(logPath)) {
                    var logInfoWithWriteTime = dicLogs[logPath];
                    if(File.Exists(logPath))
                        if(File.GetLastWriteTime(logPath).Equals(logInfoWithWriteTime.writeTime))
                            currentlogs = logInfoWithWriteTime.logInfos;
                        else {
                            logInfoWithWriteTime = LoadLogInfoXml(logPath);
                            currentlogs = logInfoWithWriteTime.logInfos;
                        }
                } else {
                    var logInfoWithWriteTime = LoadLogInfoXml(logPath);
                    currentlogs = logInfoWithWriteTime.logInfos;
                    dicLogs.Add(logPath, logInfoWithWriteTime);
                }
                if(currentlogs != null)
                    result.AddRange(currentlogs);
            }
            return result;
        }

        public override IEnumerable<XElement> GetLogs(string id, string clientIP, string systemCode, string operatorID, string operatorName, DateTime startTimeBegin, DateTime startTimeEnd, int type, bool? success) {
            if(!string.IsNullOrWhiteSpace(id)) {
                var paramters = id.Split('|');
                if(paramters.Length <= 1)
                    return new List<XElement>();

                DateTime time;
                if(!DateTime.TryParse(paramters[1], out time))
                    return new List<XElement>();
                return GetLogs(time, time).Where(v => v.Attribute("Id").Value == id);
            }

            IEnumerable<XElement> result = GetLogs(startTimeBegin, startTimeEnd);

            if(!string.IsNullOrEmpty(clientIP))
                result = result.Where(v => v.Attribute("ClientIP").Value == clientIP);
            if(!string.IsNullOrEmpty(systemCode))
                result = result.Where(v => v.Attribute("SystemCode").Value.ToLower().Contains(systemCode.ToLower()));
            if(!string.IsNullOrEmpty(operatorID))
                result = result.Where(v => v.Attribute("OperatorID").Value.ToLower().Contains(operatorID.ToLower()));
            if(!string.IsNullOrEmpty(operatorName))
                result = result.Where(v => v.Attribute("OperatorName").Value.ToLower().Contains(operatorName.ToLower()));
            if(type > -1)
                result = result.Where(v => int.Parse(v.Attribute("Type").Value) == type);
            if(success.HasValue)
                result = result.Where(v => bool.Parse(v.Attribute("Success").Value) == success);

            return result;
        }

        private string GetLogPath(DateTime time) {
            return Path.Combine(baseDirectory, string.Format("logs\\log_{0}.txt", time.ToString("yyyy-MM-dd")));
        }

        private LogInfoWithWriteTime LoadLogInfoXml(string logPath) {
            var result = new LogInfoWithWriteTime();
            if(File.Exists(logPath)) {
                result.writeTime = File.GetLastWriteTime(logPath);
                result.logInfos = XElement.Load(new StringReader(string.Concat("<LogInfos>", File.ReadAllText(logPath, Encoding.UTF8), "</LogInfos>"))).Elements("LogInfo");
            } else {
                result.writeTime = File.GetLastWriteTime(logPath);
                result.logInfos = null;
            }
            return result;
        }
    }

    public class LogInfoEntity {
        [Key, Editable(false)]
        public string Id {
            get;
            set;
        }

        [Display(ResourceType = typeof(EntityStrings), Name = "LogInfoEntity_ClientIP"), Editable(false)]
        public string ClientIP {
            get;
            set;
        }

        [Display(ResourceType = typeof(EntityStrings), Name = "LogInfoEntity_SystemCode"), Editable(false)]
        public string SystemCode {
            get;
            set;
        }

        [Display(ResourceType = typeof(EntityStrings), Name = "LogInfoEntity_OperatorID"), Editable(false)]
        public string OperatorID {
            get;
            set;
        }

        [Display(ResourceType = typeof(EntityStrings), Name = "LogInfoEntity_OperatorName"), Editable(false)]
        public string OperatorName {
            get;
            set;
        }

        [Display(ResourceType = typeof(EntityStrings), Name = "LogInfoEntity_EnterpriseCode"), Editable(false)]
        public string EnterpriseCode {
            get;
            set;
        }

        [Display(ResourceType = typeof(EntityStrings), Name = "LogInfoEntity_EnterpriseName"), Editable(false)]
        public string EnterpriseName {
            get;
            set;
        }

        [Display(ResourceType = typeof(EntityStrings), Name = "LogInfoEntity_StartTime"), Editable(false)]
        public DateTime StartTime {
            get;
            set;
        }

        [Display(ResourceType = typeof(EntityStrings), Name = "LogInfoEntity_EndTime"), Editable(false)]
        public DateTime EndTime {
            get;
            set;
        }

        [Display(ResourceType = typeof(EntityStrings), Name = "LogInfoEntity_Type"), Editable(false)]
        public int Type {
            get;
            set;
        }

        [Display(ResourceType = typeof(EntityStrings), Name = "LogInfoEntity_Success"), Editable(false)]
        public bool? Success {
            get;
            set;
        }

        [Display(ResourceType = typeof(EntityStrings), Name = "LogInfoEntity_ErrorMsg"), Editable(false)]
        public string ErrorMsg {
            get;
            set;
        }

        //[Display(ResourceType = typeof(EntityStrings), Name = "LogInfoEntity_HasParam"), Editable(false)]
        //public bool HasParam {
        //    get;
        //    set;
        //}

        //[Display(ResourceType = typeof(EntityStrings), Name = "LogInfoEntity_HasDetail"), Editable(false)]
        //public bool HasDetail {
        //    get;
        //    set;
        //}
    }

    public class LogDetailInfoEntity {
        [Key, Editable(false)]
        public int Id {
            get;
            set;
        }

        public string ParentId {
            get;
            set;
        }

        [Display(ResourceType = typeof(EntityStrings), Name = "LogDetailInfoEntity_ExecutionTime"), Editable(false)]
        public DateTime ExecutionTime {
            get;
            set;
        }

        [Display(ResourceType = typeof(EntityStrings), Name = "LogDetailInfoEntity_MethodName"), Editable(false)]
        public string MethodName {
            get;
            set;
        }

        [Display(ResourceType = typeof(EntityStrings), Name = "LogDetailInfoEntity_Remark"), Editable(false)]
        public string Remark {
            get;
            set;
        }

        [Display(ResourceType = typeof(EntityStrings), Name = "LogDetailInfoEntity_Type"), Editable(false)]
        public int Type {
            get;
            set;
        }

        [Display(ResourceType = typeof(EntityStrings), Name = "LogDetailInfoEntity_HasDetailParam"), Editable(false)]
        public bool HasDetailParam {
            get;
            set;
        }
    }

    [EnableClientAccess]
    [RequiresAuthentication]
    public sealed partial class LogDomainService : LinqToEntitiesDomainService<LogEntities> {
        public IQueryable<LogInfoEntity> GetLogInfoEntitiesBy(string clientIP, string systemCode, string operatorID, string operatorName, DateTime startTimeBegin, DateTime startTimeEnd, int type, bool? success) {
            var logs = LogManager.logManager.GetLogs(null, clientIP, systemCode, operatorID, operatorName, startTimeBegin, startTimeEnd, type, success);
            if(logs != null)
                return (from log in logs
                        select new LogInfoEntity {
                            Id = log.Attribute("Id").Value,
                            ClientIP = log.Attribute("ClientIP").Value,
                            SystemCode = log.Attribute("SystemCode").Value,
                            OperatorID = log.Attribute("OperatorID").Value,
                            OperatorName = log.Attribute("OperatorName").Value,
                            EnterpriseCode = log.Attribute("EnterpriseCode").Value,
                            EnterpriseName = log.Attribute("EnterpriseName").Value,
                            StartTime = DateTime.Parse(log.Attribute("StartTime").Value),
                            EndTime = DateTime.Parse(log.Attribute("EndTime").Value),
                            Type = int.Parse(log.Attribute("Type").Value),
                            Success = bool.Parse(log.Attribute("Success").Value),
                            ErrorMsg = log.Attribute("ErrorMsg").Value,
                        }).AsQueryable<LogInfoEntity>();

            IQueryable<SystemLog> systemLogs = this.ObjectContext.SystemLogs;

            if(!string.IsNullOrEmpty(clientIP))
                systemLogs = systemLogs.Where(v => v.ClientIP == clientIP);
            if(!string.IsNullOrEmpty(systemCode))
                systemLogs = systemLogs.Where(v => v.SystemCode.ToLower().Contains(systemCode.ToLower()));
            if(!string.IsNullOrEmpty(operatorID))
                systemLogs = systemLogs.Where(v => v.OperatorID.ToLower().Contains(operatorID.ToLower()));
            if(!string.IsNullOrEmpty(operatorName))
                systemLogs = systemLogs.Where(v => v.OperatorName.ToLower().Contains(operatorName.ToLower()));
            if(type > -1)
                systemLogs = systemLogs.Where(v => v.Type == type);
            if(success.HasValue)
                systemLogs = systemLogs.Where(v => v.Success == success);
            systemLogs = systemLogs.Where(v => v.StartTime >= startTimeBegin);
            systemLogs = systemLogs.Where(v => v.StartTime <= startTimeEnd);

            return from log in systemLogs
                   select new LogInfoEntity {
                       Id = log.Id,
                       ClientIP = log.ClientIP,
                       SystemCode = log.SystemCode,
                       OperatorID = log.OperatorID,
                       OperatorName = log.OperatorName,
                       EnterpriseCode = log.EnterpriseCode,
                       EnterpriseName = log.EnterpriseName,
                       StartTime = log.StartTime,
                       EndTime = log.EndTime,
                       Type = log.Type,
                       Success = log.Success,
                       ErrorMsg = log.ErrorMsg,
                   };
        }

        public IQueryable<LogDetailInfoEntity> GetLogDetailInfoEntitiesBy(string logInfoId) {
            if(string.IsNullOrEmpty(logInfoId))
                return null;

            IEnumerable<XElement> logDetailInfos;
            var logs = LogManager.logManager.GetLogs(logInfoId, null, null, null, null, DateTime.Now, DateTime.Now, -1, null);
            if(logs == null) {
                var logDetailInfosXml = this.ObjectContext.SystemLogs.SingleOrDefault(v => v.Id == logInfoId).LogDetailInfosXml;
                if(string.IsNullOrWhiteSpace(logDetailInfosXml))
                    return null;
                logDetailInfos = XElement.Load(new StringReader(logDetailInfosXml)).Elements("DetailInfo");
            } else {
                if(logs.Count() == 0)
                    return null;
                var detailInfos = logs.First().Elements("DetailInfos");
                if(detailInfos == null)
                    return null;
                logDetailInfos = detailInfos.Elements("DetailInfo");
            }

            var logDetailInfoEntities = from detail in logDetailInfos
                                        let context = detail.Elements("Context")
                                        select new LogDetailInfoEntity {
                                            Id = int.Parse(detail.Attribute("Id").Value),
                                            ParentId = logInfoId,
                                            ExecutionTime = DateTime.Parse(detail.Attribute("ExecutionTime").Value),
                                            MethodName = detail.Attribute("MethodName").Value,
                                            Remark = detail.Attribute("Remark").Value,
                                            Type = int.Parse(detail.Attribute("Type").Value),
                                            HasDetailParam = context != null && context.Count() > 0 && context.First().HasElements
                                        };
            return logDetailInfoEntities.AsQueryable<LogDetailInfoEntity>();
        }

        public string GetLogInfoParam(string logInfoId) {
            if(string.IsNullOrEmpty(logInfoId))
                return null;
            string value = "";
            var logs = LogManager.logManager.GetLogs(logInfoId, null, null, null, null, DateTime.Now, DateTime.Now, -1, null);
            if(logs == null) {
                var systemLog = this.ObjectContext.SystemLogs.SingleOrDefault(v => v.Id == logInfoId);
                if(systemLog == null || string.IsNullOrEmpty(systemLog.ParamXml))
                    return null;
                value = systemLog.ParamXml;
            } else {
                if(logs.Count() == 0)
                    return null;
                var paramXml = logs.First().Element("ChangeSetEntries") ?? logs.First().Element("QueryDescription");
                if(paramXml == null)
                    return null;
                value = paramXml.ToString();
            }

            return string.Concat(xmlHead, value);
        }

        public string GetLogInfoQueryResult(string logInfoId) {
            if(string.IsNullOrEmpty(logInfoId))
                return null;
            string value = "";
            var logs = LogManager.logManager.GetLogs(logInfoId, null, null, null, null, DateTime.Now, DateTime.Now, -1, null);
            if(logs == null) {
                var systemLog = this.ObjectContext.SystemLogs.SingleOrDefault(v => v.Id == logInfoId);
                if(systemLog == null || string.IsNullOrEmpty(systemLog.QueryResultXml))
                    return null;
                value = systemLog.QueryResultXml;
            } else {
                if(logs.Count() == 0)
                    return null;
                var queryResultXml = logs.First().Element("QueryResults");
                if(queryResultXml == null)
                    return null;
                value = queryResultXml.ToString();
            }

            return string.Concat(xmlHead, value);
        }

        public string GetLogInfoDetailParam(string logInfoId, int logDetailId) {
            if(string.IsNullOrEmpty(logInfoId))
                return null;

            XElement xml = null;
            var logs = LogManager.logManager.GetLogs(logInfoId, null, null, null, null, DateTime.Now, DateTime.Now, -1, null);
            if(logs == null) {
                var systemLog = this.ObjectContext.SystemLogs.SingleOrDefault(v => v.Id == logInfoId);
                if(systemLog == null || string.IsNullOrEmpty(systemLog.LogDetailInfosXml))
                    return null;
                xml = XElement.Load(new StringReader(systemLog.LogDetailInfosXml));
            } else {
                if(logs.Count() == 0)
                    return null;
                xml = logs.First().Element("DetailInfos");
            }
            if(xml == null)
                return null;

            var logDetailInfo = xml.Elements("DetailInfo").SingleOrDefault(v => v.Attribute("Id") != null && v.Attribute("Id").Value == logDetailId.ToString());
            if(logDetailInfo == null)
                return null;

            return string.Concat(xmlHead, logDetailInfo.Element("Context").ToString());
        }

        private const string xmlHead = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
    }
}