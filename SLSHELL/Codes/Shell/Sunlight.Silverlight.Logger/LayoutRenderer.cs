﻿using System;
using System.Text;
using NLog;
using NLog.LayoutRenderers;

namespace Sunlight.Silverlight.Log.Web {
    [LayoutRenderer("event-context-dateTime")]
    public class DateTimeContextLayoutRenderer : EventContextLayoutRenderer {
        protected override void Append(StringBuilder builder, LogEventInfo logEvent) {
            object value;
            if(logEvent.Properties.TryGetValue(this.Item, out value))
                builder.Append(((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss"));
        }
    }

    [LayoutRenderer("event-context-bool")]
    public class BoolContextLayoutRenderer : EventContextLayoutRenderer {
        protected override void Append(StringBuilder builder, LogEventInfo logEvent) {
            object value;
            if(logEvent.Properties.TryGetValue(this.Item, out value))
                builder.Append(Convert.ToBoolean(value) ? 1 : 0);
        }
    }
}
