﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace Sunlight.Silverlight.Security.Controls {
    public partial class ActionToggleButton {
        private readonly Storyboard sbMarquee;

        private void OnMouseEnter(object sender, MouseEventArgs mouseEventArgs) {
            var range = this.DescriptionCanvas.Width - this.DescriptionText.ActualWidth;
            if(range >= 0)
                return;

            var beginTime = TimeSpan.FromSeconds(1);
            var pauseTime = TimeSpan.FromSeconds(1.5);
            var duration = TimeSpan.FromSeconds(-range / 50);
            this.sbMarquee.Duration = new Duration(beginTime + duration + pauseTime);
            var da = new DoubleAnimation {
                BeginTime = beginTime,
                Duration = new Duration(duration),
                From = 0,
                To = range,
            };
            Storyboard.SetTarget(da, this.DescriptionText);
            Storyboard.SetTargetProperty(da, new PropertyPath("(Canvas.Left)"));
            this.sbMarquee.Children.Clear();
            this.sbMarquee.Children.Add(da);
            this.sbMarquee.Stop();
            this.sbMarquee.Begin();
        }

        private void OnMouseLeave(object sender, MouseEventArgs mouseEventArgs) {
            this.sbMarquee.Stop();
        }

        private void OnUnloaded(object sender, RoutedEventArgs routedEventArgs) {
            this.sbMarquee.Stop();
        }

        public ActionToggleButton() {
            this.InitializeComponent();
            this.sbMarquee = new Storyboard {
                AutoReverse = true,
                RepeatBehavior = RepeatBehavior.Forever,
            };
            this.MouseEnter += this.OnMouseEnter;
            this.MouseLeave += this.OnMouseLeave;
            this.Unloaded += this.OnUnloaded;
        }
    }
}
