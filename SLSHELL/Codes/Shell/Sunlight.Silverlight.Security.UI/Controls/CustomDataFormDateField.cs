﻿using System.Windows.Controls;
using System.Windows.Data;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.Controls {
    public class CustomDataFormDateField : DataFormDataField {
        protected override Control GetControl() {
            var dependencyProperty = RadDateTimePicker.SelectedDateProperty;
            var control = new RadDatePicker {
                DateTimeWatermarkContent = LocalizationManager.GetString("EnterDate"),
            };
            if(this.DataMemberBinding != null)
                control.SetBinding(dependencyProperty, this.DataMemberBinding);
            control.SetBinding(IsEnabledProperty, new Binding("IsReadOnly") {
                Source = this,
                Converter = new InvertedBooleanConverter()
            });
            return control;
        }
    }
}
