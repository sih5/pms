﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;

namespace Sunlight.Silverlight.Security.Controls {
    internal static class Helper {
        public static void SetItemsControlByEntity<TEntity, TItem>(ItemsControl control, IEnumerable<TEntity> entities, string idColumnName = null, string parentColumnName = null, string sequenceColumnName = null, Action<TEntity, TItem> onItemAdded = null)
            where TEntity : Entity
            where TItem : FrameworkElement, new() {
            var entityType = typeof(TEntity);
            var idColumn = idColumnName == null ? null : entityType.GetProperty(idColumnName);
            var parentColumn = parentColumnName == null ? null : entityType.GetProperty(parentColumnName);
            var sequenceColumn = sequenceColumnName == null ? null : entityType.GetProperty(sequenceColumnName);
            var isItemsControl = typeof(TItem).IsSubclassOf(typeof(ItemsControl));
            if(!isItemsControl || idColumn == null || parentColumn == null) {
                var collection = entities;
                if(sequenceColumn != null)
                    collection = collection.OrderBy(e => sequenceColumn.GetValue(e, null));
                foreach(var entity in collection) {
                    var item = new TItem {
                        Tag = entity,
                    };
                    control.Items.Add(item);
                    if(onItemAdded != null)
                        onItemAdded(entity, item);
                }
            } else {
                Action<ItemCollection, int> populateItems = null;
                populateItems = (items, parentId) => {
                    var collection = entities.Where(e => (int)parentColumn.GetValue(e, null) == parentId);
                    if(sequenceColumn != null)
                        collection = collection.OrderBy(e => sequenceColumn.GetValue(e, null));
                    foreach(var entity in collection) {
                        var item = new TItem {
                            Tag = entity,
                        };
                        var itemsControl = item as ItemsControl;
                        // ReSharper disable AccessToModifiedClosure
                        if(itemsControl != null && populateItems != null)
                            populateItems(itemsControl.Items, (int)idColumn.GetValue(entity, null));
                        // ReSharper restore AccessToModifiedClosure
                        if(onItemAdded != null)
                            onItemAdded(entity, item);
                        items.Add(item);
                    }
                };
                populateItems(control.Items, 0);
            }
        }
    }
}
