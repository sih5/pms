﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Controls.Security {
    public partial class OrganizationTreeView : UserControl, INotifyPropertyChanged {
        private bool isDataLoaded, isMultiple;
        private List<Organization> organizations, selectedOrganizations = new List<Organization>();
        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsMultiple {
            get {
                return this.isMultiple;
            }
            set {
                if(this.isMultiple != value) {
                    this.isMultiple = value;
                    this.rtvMain.IsOptionElementsEnabled = value;
                    this.rtvMain.SelectionChanged -= new Telerik.Windows.Controls.SelectionChangedEventHandler(this.rtvMain_SelectionChanged);
                    this.rtvMain.Checked -= new EventHandler<RadRoutedEventArgs>(this.rtvMain_Checked);
                    this.rtvMain.Unchecked -= new EventHandler<RadRoutedEventArgs>(this.rtvMain_Unchecked);
                    if(value) {
                        this.rtvMain.SelectionMode = Telerik.Windows.Controls.SelectionMode.Extended;
                        this.rtvMain.Checked += new EventHandler<RadRoutedEventArgs>(this.rtvMain_Checked);
                        this.rtvMain.Unchecked += new EventHandler<RadRoutedEventArgs>(this.rtvMain_Unchecked);
                    } else {
                        this.rtvMain.SelectionMode = Telerik.Windows.Controls.SelectionMode.Single;
                        this.rtvMain.SelectionChanged += new Telerik.Windows.Controls.SelectionChangedEventHandler(this.rtvMain_SelectionChanged);
                    }
                }
            }
        }

        public IEnumerable<Organization> SelectedOrganizations {
            get {
                return this.selectedOrganizations;
            }
            set {
                if(!this.IsMultiple)
                    throw new InvalidOperationException();
                if(value != null) {
                    this.selectedOrganizations = value.ToList();
                    this.SetSelectedTreeView(this.rtvMain.Items, value.ToList());
                    this.OnPropertyChanged("SelectedOrganizations");
                }
            }
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SetSelectedTreeView(ItemCollection items, List<Organization> organizations) {
            foreach(RadTreeViewItem item in items) {
                item.IsSelected = false;
                item.IsChecked = false;
                Organization itemOrganization = item.Tag as Organization;
                if(itemOrganization == null)
                    continue;
                foreach(var organization in organizations)
                    if(organization.Id == itemOrganization.Id) {
                        if(this.IsMultiple) {
                            item.IsChecked = true;
                            organizations.Remove(organizations.Single(o => o.Id == organization.Id));
                            break;
                        } else {
                            item.IsSelected = true;
                            return;
                        }
                    } else {
                        if(this.IsMultiple)
                            item.IsChecked = false;
                        else
                            item.IsSelected = false;
                    }
                if(organizations.Count == 0)
                    return;
                this.SetSelectedTreeView(item.Items, organizations);
            }
        }

        private bool CheckSelectedItem(RadTreeViewItem item) {
            item.IsExpanded = false;
            bool isSelected = item.IsSelected || (item.IsChecked.HasValue && item.IsChecked.Value);
            if(item.Items.Count > 0)
                foreach(RadTreeViewItem subItem in item.Items)
                    if(this.CheckSelectedItem(subItem))
                        item.IsExpanded = true;
            return isSelected || item.IsExpanded;
        }

        private void PopulateItems(IEnumerable<Organization> organizations, int parentId, ItemCollection items) {
            foreach(var organization in organizations.Where(o => o.ParentId == parentId).OrderBy(o => o.Sequence)) {
                RadTreeViewItem item = new RadTreeViewItem {
                    Header = organization.Name,
                    Tag = organization,
                };
                switch(organization.Type) {
                    case 1:
                        item.DefaultImageSrc = new Uri("/Sunlight.Silverlight.Security.UI;component/Images/company-s.png", UriKind.Relative);
                        break;
                    case 2:
                        item.DefaultImageSrc = new Uri("/Sunlight.Silverlight.Security.UI;component/Images/department-s.png", UriKind.Relative);
                        break;
                    case 3:
                        item.DefaultImageSrc = new Uri("/Sunlight.Silverlight.Security.UI;component/Images/team.png-s", UriKind.Relative);
                        break;
                }
                items.Add(item);
                if(this.selectedOrganizations.Any(o => o.Id == organization.Id)) {
                    if(this.IsMultiple)
                        item.IsChecked = true;
                    else
                        item.IsSelected = true;
                }
                this.PopulateItems(organizations, organization.Id, item.Items);
            }
            if(parentId == 0)
                foreach(RadTreeViewItem item in items)
                    this.CheckSelectedItem(item);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e) {
            if(Utils.GetIsInDesignMode())
                return;
            this.rtvMain.Items.Clear();
            if(!this.isDataLoaded) {
                if(this.organizations == null) {
                    SecurityDomainContext domainContext = new SecurityDomainContext();
                    this.rbiMain.SetBinding(RadBusyIndicator.IsBusyProperty, new Binding("IsLoading") {
                        Source = domainContext,
                    });
                    domainContext.Load<Organization>(domainContext.GetOrganizationByQuery(null, 0, -1), (loadOp) => {
                        this.organizations = new List<Organization>(loadOp.Entities.Count());
                        if(this.selectedOrganizations.Count == 0)
                            this.organizations = loadOp.Entities.ToList();
                        else
                            foreach(Organization organization in loadOp.Entities) {
                                var selectedOrganization = this.selectedOrganizations.SingleOrDefault(o => o.Id == organization.Id);
                                if(selectedOrganization == null)
                                    this.organizations.Add(organization);
                                else
                                    this.organizations.Add(selectedOrganization);
                            }
                        this.PopulateItems(this.organizations, 0, this.rtvMain.Items);
                    }, null);
                } else
                    this.PopulateItems(this.organizations, 0, this.rtvMain.Items);
                this.isDataLoaded = true;
            } else
                this.PopulateItems(this.organizations, 0, this.rtvMain.Items);
        }

        private void rtvMain_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            this.selectedOrganizations.Clear();
            var treeItem = e.AddedItems.Cast<RadTreeViewItem>().FirstOrDefault();
            if(treeItem != null)
                this.selectedOrganizations.Add(treeItem.Tag as Organization);
            this.OnPropertyChanged("SelectedOrganizations");
        }

        private void rtvMain_Checked(object sender, RadRoutedEventArgs e) {
            RadTreeViewItem item = e.Source as RadTreeViewItem;
            if(item != null) {
                e.Handled = true;
                Organization organization = item.Tag as Organization;
                if(!this.selectedOrganizations.Any(o => o.Id == organization.Id)) {
                    this.selectedOrganizations.Add(organization);
                    this.OnPropertyChanged("SelectedOrganizations");
                }
            }
        }

        private void rtvMain_Unchecked(object sender, RadRoutedEventArgs e) {
            RadTreeViewItem item = e.Source as RadTreeViewItem;
            if(item != null) {
                e.Handled = true;
                Organization organization = item.Tag as Organization;
                if(this.selectedOrganizations.Any(o => o.Id == organization.Id)) {
                    this.selectedOrganizations.Remove(this.selectedOrganizations.First(o => o.Id == organization.Id));
                    this.OnPropertyChanged("SelectedOrganizations");
                }
            }
        }

        public OrganizationTreeView() {
            InitializeComponent();
            this.IsMultiple = true;
        }

        public void SetSelectedOrganization(Organization organization) {
            if(organization == null)
                throw new ArgumentNullException();
            if(this.IsMultiple)
                throw new InvalidOperationException();
            this.selectedOrganizations.Clear();
            this.selectedOrganizations.Add(organization);
            if(this.isDataLoaded)
                this.SetSelectedTreeView(this.rtvMain.Items, new List<Organization> {
                    organization,
                });
            this.OnPropertyChanged("SelectedOrganizations");
        }

        public void ClearSelectedOrganizations() {
            if(this.selectedOrganizations.Count > 0) {
                this.selectedOrganizations.Clear();
                this.OnPropertyChanged("SelectedOrganizations");
            }
        }
    }
}
