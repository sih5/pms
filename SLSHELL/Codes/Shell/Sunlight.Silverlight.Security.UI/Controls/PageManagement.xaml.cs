﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Configs;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.Controls {
    public partial class PageManagement : UserControl {
        public sealed class DataPageActionItem {
            public string Id {
                get;
                set;
            }

            public string Name {
                get;
                set;
            }

            public string Image {
                get;
                set;
            }

            public bool IsChecked {
                get;
                set;
            }
        }

        public sealed class DataPageColumnItem {
            public string Id {
                get;
                set;
            }

            public string Name {
                get;
                set;
            }

            public bool IsChecked {
                get;
                set;
            }
        }

        private SecurityDomainContext domainContext;
        private Sunlight.Silverlight.Security.Web.Page page;
        private Role role;

        public Sunlight.Silverlight.Security.Web.Page Page {
            get {
                return this.page;
            }
            set {
                if(this.page != value) {
                    this.page = value;
                    this.Refresh();
                }
            }
        }

        public Role Role {
            get {
                return this.role;
            }
            set {
                if(this.role != value) {
                    this.role = value;
                    this.Refresh();
                }
            }
        }

        private void SetUIVisible(bool visible) {
            if(this.page != null) {
                switch(this.page.PageType) {
                    case "Data":
                        this.grdActions.Visibility = this.grdColumns.Visibility = visible ? Visibility.Visible : Visibility.Collapsed;
                        break;
                    default:
                        this.grdActions.Visibility = this.grdColumns.Visibility = Visibility.Collapsed;
                        break;
                }
            } else
                this.grdActions.Visibility = this.grdColumns.Visibility = Visibility.Collapsed;
        }

        private void SetPageAccess() {
            //判断是否有页面访问权限
            var pageNode = this.domainContext.Nodes.SingleOrDefault(n => n.CategoryType == (int)NodeCategoryType.Page && n.CategoryId == this.page.Id);
            if(pageNode != null) {
                var rule = this.domainContext.Rules.SingleOrDefault(r => r.NodeId == pageNode.Id && r.RoleId == this.role.Id);
                this.chkAllowAccess.IsChecked = rule != null;
            } else
                this.chkAllowAccess.IsChecked = false;
            this.SetUIVisible(this.chkAllowAccess.IsChecked == true);
        }

        private void SubmitPageAccess() {
            Action submitPage = () => {
                switch(this.page.PageType) {
                    case "Data":
                        this.SubmitDataPage();
                        break;
                }
            };

            //节点规则修改
            Action<Node> submitRule = node => {
                var rule = this.domainContext.Rules.SingleOrDefault(r => r.NodeId == node.Id && r.RoleId == this.role.Id);
                if(this.chkAllowAccess.IsChecked == true && rule == null) {
                    this.domainContext.Rules.Add(new Rule {
                        NodeId = node.Id,
                        RoleId = this.role.Id,
                    });
                    this.domainContext.SubmitChanges(submitOpRule => {
                        if(submitOpRule.HasError)
                            return;
                        submitPage();
                    }, null);
                } else if(this.chkAllowAccess.IsChecked == false && rule != null) {
                    this.domainContext.Rules.Remove(this.domainContext.Rules.Single(r => r.NodeId == node.Id && r.RoleId == this.role.Id));
                    this.domainContext.SubmitChanges(submitOpRule => {
                        if(submitOpRule.HasError)
                            return;
                        submitPage();
                    }, null);
                } else
                    submitPage();
            };

            //页面访问权限修改
            var pageNode = this.domainContext.Nodes.SingleOrDefault(n => n.CategoryType == (int)NodeCategoryType.Page && n.CategoryId == this.page.Id);
            if(pageNode == null) {
                this.domainContext.Nodes.Add(new Node {
                    CategoryType = (int)NodeCategoryType.Page,
                    CategoryId = this.page.Id,
                    Status = 0,
                });
                this.domainContext.SubmitChanges(submitOpNode => {
                    if(submitOpNode.HasError)
                        return;
                    pageNode = (Node)submitOpNode.ChangeSet.AddedEntities[0];
                    submitRule(pageNode);
                }, null);
            } else
                submitRule(pageNode);
        }

        private void SetDataPage() {
            var pageConfigType = Type.GetType(this.page.Parameter);
            var pageConfig = (DataPageConfig)Activator.CreateInstance(pageConfigType);

            // Actions
            var actionConfigs = pageConfig.GetOperationConfigs();
            var actionItems = new List<DataPageActionItem>(actionConfigs.Count());
            foreach(var actionConfig in actionConfigs) {
                var actionItem = new DataPageActionItem {
                    Id = actionConfig.Id,
                    Name = actionConfig.Name,
                    Image = actionConfig.Image,
                    IsChecked = true,
                };
                var action = this.domainContext.SecurityActions.SingleOrDefault(a => a.OperationId == actionConfig.Id && a.PageId == this.page.Id);
                if(action != null) {
                    var node = this.domainContext.Nodes.SingleOrDefault(n => n.CategoryType == (int)NodeCategoryType.Action && n.CategoryId == action.Id);
                    if(node != null && this.domainContext.Rules.SingleOrDefault(r => r.NodeId == node.Id && r.RoleId == this.role.Id) != null)
                        actionItem.IsChecked = false;
                }
                actionItems.Add(actionItem);
            }
            this.icActions.ItemTemplate = (DataTemplate)this.Resources["DataPageActionsTemplate"];
            this.icActions.ItemsSource = actionItems;

            // Columns
            var columnConfigs = pageConfig.GetGridColumnConfigs();
            var columnItems = new List<DataPageColumnItem>(columnConfigs.Count());
            foreach(var columnConfig in columnConfigs) {
                var columnItem = new DataPageColumnItem {
                    Id = columnConfig.ColumnName,
                    Name = Utils.GetEntityLocalizedName(pageConfig.EntityType, columnConfig.ColumnName),
                    IsChecked = true,
                };
                var column = this.domainContext.AuthorizedColumns.SingleOrDefault(c => c.ColumnName == columnConfig.ColumnName && c.PageId == this.page.Id);
                if(column != null) {
                    var node = this.domainContext.Nodes.SingleOrDefault(n => n.CategoryType == (int)NodeCategoryType.Column && n.CategoryId == column.Id);
                    if(node != null && this.domainContext.Rules.SingleOrDefault(r => r.NodeId == node.Id && r.RoleId == this.role.Id) != null)
                        columnItem.IsChecked = false;
                }
                columnItems.Add(columnItem);
            }
            this.icColumns.ItemTemplate = (DataTemplate)this.Resources["DataPageColumnsTemplate"];
            this.icColumns.ItemsSource = columnItems;
        }

        private void SubmitDataPage() {
            var actionItems = this.icActions.ItemsSource.Cast<DataPageActionItem>().Where(i => !i.IsChecked);
            var columnItems = this.icColumns.ItemsSource.Cast<DataPageColumnItem>().Where(i => !i.IsChecked);

            Action submitColumns = () => {
                //删除Column记录
                foreach(var columnNode in this.domainContext.Nodes.Where(n => n.CategoryType == (int)NodeCategoryType.Column)) {
                    var column = this.domainContext.AuthorizedColumns.Single(c => c.Id == columnNode.CategoryId && c.PageId == this.page.Id);
                    var columnItem = columnItems.SingleOrDefault(ci => ci.Id == column.ColumnName);
                    if(columnItem == null) {
                        var rule = this.domainContext.Rules.SingleOrDefault(r => r.NodeId == columnNode.Id && r.RoleId == this.role.Id);
                        if(rule != null)
                            this.domainContext.Rules.Remove(rule);
                    }
                }
                //添加Column记录
                foreach(var columnItem in columnItems) {
                    var column = this.domainContext.AuthorizedColumns.Single(c => c.ColumnName == columnItem.Id && c.PageId == this.page.Id);
                    var columnNode = this.domainContext.Nodes.SingleOrDefault(n => n.CategoryType == (int)NodeCategoryType.Action && n.CategoryId == column.Id);
                    if(columnNode == null)
                        this.domainContext.Nodes.Add(new Node {
                            CategoryType = (int)NodeCategoryType.Column,
                            CategoryId = column.Id,
                            Status = 0,
                        });
                    else {
                        var rule = this.domainContext.Rules.SingleOrDefault(r => r.NodeId == columnNode.Id && r.RoleId == this.role.Id);
                        if(rule == null)
                            this.domainContext.Rules.Add(new Rule {
                                NodeId = columnNode.Id,
                                RoleId = this.role.Id,
                            });
                    }
                }
                //提交数据
                if(this.domainContext.HasChanges)
                    this.domainContext.SubmitChanges(submitOp => {
                        if(submitOp.HasError)
                            return;
                        foreach(var entity in submitOp.ChangeSet.AddedEntities)
                            if(entity is Node)
                                this.domainContext.Rules.Add(new Rule {
                                    NodeId = (entity as Node).Id,
                                    RoleId = this.role.Id,
                                });
                        if(this.domainContext.HasChanges)
                            this.domainContext.SubmitChanges(submitOpRule => {
                                if(submitOp.HasError)
                                    return;
                            }, null);
                    }, null);
            };

            Action submitActions = () => {
                //删除Action记录
                foreach(var actionNode in this.domainContext.Nodes.Where(n => n.CategoryType == (int)NodeCategoryType.Action)) {
                    var action = this.domainContext.SecurityActions.Single(a => a.Id == actionNode.CategoryId && a.PageId == this.page.Id);
                    var actionItem = actionItems.SingleOrDefault(ai => ai.Id == action.OperationId);
                    if(actionItem == null) {
                        var rule = this.domainContext.Rules.SingleOrDefault(r => r.NodeId == actionNode.Id && r.RoleId == this.role.Id);
                        if(rule != null)
                            this.domainContext.Rules.Remove(rule);
                    }
                }
                //添加Action记录
                foreach(var actionItem in actionItems) {
                    var action = this.domainContext.SecurityActions.Single(a => a.OperationId == actionItem.Id && a.PageId == this.page.Id);
                    var actionNode = this.domainContext.Nodes.SingleOrDefault(n => n.CategoryType == (int)NodeCategoryType.Action && n.CategoryId == action.Id);
                    if(actionNode == null)
                        this.domainContext.Nodes.Add(new Node {
                            CategoryType = (int)NodeCategoryType.Action,
                            CategoryId = action.Id,
                            Status = 0,
                        });
                    else {
                        var rule = this.domainContext.Rules.SingleOrDefault(r => r.NodeId == actionNode.Id && r.RoleId == this.role.Id);
                        if(rule == null)
                            this.domainContext.Rules.Add(new Rule {
                                NodeId = actionNode.Id,
                                RoleId = this.role.Id,
                            });
                    }
                }
                //提交数据
                if(this.domainContext.HasChanges)
                    this.domainContext.SubmitChanges(submitOp => {
                        if(submitOp.HasError)
                            return;
                        foreach(var entity in submitOp.ChangeSet.AddedEntities)
                            if(entity is Node)
                                this.domainContext.Rules.Add(new Rule {
                                    NodeId = (entity as Node).Id,
                                    RoleId = this.role.Id,
                                });
                        if(this.domainContext.HasChanges)
                            this.domainContext.SubmitChanges(submitOpRule => {
                                if(submitOp.HasError)
                                    return;
                                submitColumns();
                            }, null);
                        else
                            submitColumns();
                    }, null);
                else
                    submitColumns();
            };

            //更新窗体页面表
            foreach(var actionItem in actionItems)
                if(this.domainContext.SecurityActions.SingleOrDefault(a => a.OperationId == actionItem.Id && a.PageId == this.page.Id) == null)
                    this.domainContext.SecurityActions.Add(new SecurityAction {
                        OperationId = actionItem.Id,
                        PageId = this.page.Id,
                        Status = 0,
                    });
            //更新窗体表格列
            foreach(var columnItem in columnItems)
                if(this.domainContext.AuthorizedColumns.SingleOrDefault(c => c.ColumnName == columnItem.Id && c.PageId == this.page.Id) == null)
                    this.domainContext.AuthorizedColumns.Add(new AuthorizedColumn {
                        ColumnName = columnItem.Id,
                        PageId = this.page.Id,
                        Status = 0,
                    });

            //更新权限数据
            if(this.domainContext.HasChanges)
                this.domainContext.SubmitChanges(submitOp => {
                    this.domainContext.Load<SecurityAction>(this.domainContext.GetSecurityActionByPageIdQuery(this.page.Id), loadOpActions => {
                        if(loadOpActions.HasError)
                            return;
                        submitActions();
                    }, null);
                }, null);
            else
                submitActions();
        }

        private void Refresh() {
            if(this.role == null || this.page == null)
                return;

            this.SetUIVisible(false);
            this.chkAllowAccess.Content = string.Format("允许角色“{0}”访问页面“{1}”", this.role.Name, this.page.Name);

            this.domainContext.Load<Rule>(this.domainContext.GetRulesByRoleIdQuery(this.role.Id), loadOpRules => {
                if(loadOpRules.HasError)
                    return;
                this.domainContext.Load<Node>(this.domainContext.GetNodesByRoleIdQuery(this.role.Id), loadOpNodes => {
                    if(loadOpNodes.HasError)
                        return;
                    this.domainContext.Load<SecurityAction>(this.domainContext.GetSecurityActionByPageIdQuery(this.page.Id), loadOpActions => {
                        if(loadOpActions.HasError)
                            return;
                        this.domainContext.Load<AuthorizedColumn>(this.domainContext.GetAuthorizedColumnByPageIdQuery(this.page.Id), loadOpColumns => {
                            if(loadOpColumns.HasError)
                                return;
                            this.SetPageAccess();
                            //按页面类型设置界面
                            switch(this.page.PageType) {
                                case "Data":
                                    this.SetDataPage();
                                    break;
                                default:
                                    this.SetUIVisible(false);
                                    break;
                            }
                        }, null);
                    }, null);
                }, null);
            }, null);
        }

        private void chkAllowAccess_Checked(object sender, RoutedEventArgs e) {
            this.SetUIVisible((e.OriginalSource as CheckBox).IsChecked == true);
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e) {
            this.SubmitPageAccess();
        }

        public PageManagement() {
            InitializeComponent();
            this.domainContext = new SecurityDomainContext();
            this.rbiLoading.SetBinding(RadBusyIndicator.IsBusyProperty, new Binding("IsLoading") {
                Mode = BindingMode.OneWay,
                Source = this.domainContext,
            });
            this.rbiSubmitting.SetBinding(RadBusyIndicator.IsBusyProperty, new Binding("IsSubmitting") {
                Mode = BindingMode.OneWay,
                Source = this.domainContext,
            });
        }
    }
}
