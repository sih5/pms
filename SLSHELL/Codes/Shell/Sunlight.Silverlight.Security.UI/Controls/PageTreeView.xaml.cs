﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Controls.Security {
    public partial class PageTreeView : UserControl, INotifyPropertyChanged {
        private bool isDataLoaded, isMultiple;
        private List<Sunlight.Silverlight.Security.Web.Page> pages, selectedPages = new List<Sunlight.Silverlight.Security.Web.Page>();
        public event PropertyChangedEventHandler PropertyChanged;

        public PageTreeView() {
            InitializeComponent();
            this.IsMultiple = true;
        }

        public bool IsMultiple {
            get {
                return this.isMultiple;
            }
            set {
                if(this.isMultiple != value) {
                    this.isMultiple = value;
                    this.rtvMain.IsOptionElementsEnabled = value;
                    this.rtvMain.SelectionChanged -= new Telerik.Windows.Controls.SelectionChangedEventHandler(this.rtvMain_SelectionChanged);
                    this.rtvMain.Checked -= new EventHandler<RadRoutedEventArgs>(this.rtvMain_Checked);
                    this.rtvMain.Unchecked -= new EventHandler<RadRoutedEventArgs>(this.rtvMain_Unchecked);
                    if(value) {
                        this.rtvMain.SelectionMode = Telerik.Windows.Controls.SelectionMode.Extended;
                        this.rtvMain.Checked += new EventHandler<RadRoutedEventArgs>(this.rtvMain_Checked);
                        this.rtvMain.Unchecked += new EventHandler<RadRoutedEventArgs>(this.rtvMain_Unchecked);
                    } else {
                        this.rtvMain.SelectionMode = Telerik.Windows.Controls.SelectionMode.Single;
                        this.rtvMain.SelectionChanged += new Telerik.Windows.Controls.SelectionChangedEventHandler(this.rtvMain_SelectionChanged);
                    }
                }
            }
        }

        public IEnumerable<Sunlight.Silverlight.Security.Web.Page> SelectedPages {
            get {
                return this.selectedPages;
            }
            set {
                if(!this.isMultiple)
                    throw new InvalidOperationException();
                if(value != null) {
                    this.selectedPages = value.ToList();
                    if(this.isDataLoaded)
                        this.SetSelectedTreeView(this.rtvMain.Items, value.ToList());
                    this.OnPropertyChanged("SelectedPages");
                }
            }
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SetSelectedTreeView(ItemCollection items, List<Sunlight.Silverlight.Security.Web.Page> pages) {
            foreach(RadTreeViewItem item in items) {
                var itemPage = item.Tag as Sunlight.Silverlight.Security.Web.Page;
                if(itemPage == null)
                    continue;
                foreach(var page in pages)
                    if(page.Id == itemPage.Id) {
                        if(this.IsMultiple) {
                            item.IsChecked = true;
                            pages.Remove(page);
                            break;
                        } else {
                            item.IsSelected = true;
                            return;
                        }
                    } else {
                        if(this.IsMultiple)
                            item.IsChecked = false;
                        else
                            item.IsSelected = false;
                    }
                if(pages.Count == 0)
                    return;
                this.SetSelectedTreeView(item.Items, pages);
            }
        }

        private bool CheckSelectedItem(RadTreeViewItem item) {
            item.IsExpanded = false;
            bool isSelected = item.IsSelected || (item.IsChecked.HasValue && item.IsChecked.Value);
            if(item.Items.Count > 0)
                foreach(RadTreeViewItem subItem in item.Items)
                    if(this.CheckSelectedItem(subItem))
                        item.IsExpanded = true;
            return isSelected || item.IsExpanded;
        }


        private void PopulateItems(IEnumerable<Sunlight.Silverlight.Security.Web.Page> pages, int parentId, ItemCollection items) {
            foreach(var page in pages.Where(o => o.ParentId == parentId).OrderBy(o => o.PageId)) {
                RadTreeViewItem item = new RadTreeViewItem {
                    Header = page.Name,
                    Tag = page,
                };
                switch(page.Type) {
                    case 1:
                        item.DefaultImageSrc = new Uri("/Sunlight.Silverlight.Security.UI;component/Images/company-s.png", UriKind.Relative);
                        break;
                    case 2:
                        item.DefaultImageSrc = new Uri("/Sunlight.Silverlight.Security.UI;component/Images/department-s.png", UriKind.Relative);
                        break;
                    case 3:
                        item.DefaultImageSrc = new Uri("/Sunlight.Silverlight.Security.UI;component/Images/team.png-s", UriKind.Relative);
                        break;
                }
                items.Add(item);
                if(this.selectedPages.Any(o => o.Id == page.Id)) {
                    if(this.IsMultiple)
                        item.IsChecked = true;
                    else
                        item.IsSelected = true;
                }
                this.PopulateItems(pages, page.Id, item.Items);
            }
            if(parentId == 0)
                foreach(RadTreeViewItem item in items)
                    this.CheckSelectedItem(item);
        }



        private void UserControl_Loaded(object sender, RoutedEventArgs e) {
            if(!Utils.GetIsInDesignMode()) {
                if(!this.isDataLoaded) {
                    this.rtvMain.Items.Clear();
                    if(this.pages == null) {
                        SecurityDomainContext domainContext = new SecurityDomainContext();
                        this.rbiMain.SetBinding(RadBusyIndicator.IsBusyProperty, new System.Windows.Data.Binding("IsLoading") {
                            Source = domainContext,
                        });
                        domainContext.Load<Sunlight.Silverlight.Security.Web.Page>(domainContext.GetPageByQuery(null, null, 0, -1), (loadOp) => {
                            this.pages = new List<Sunlight.Silverlight.Security.Web.Page>(loadOp.Entities.Count());
                            if(this.selectedPages.Count == 0)
                                this.pages = loadOp.Entities.ToList();
                            else
                                foreach(Sunlight.Silverlight.Security.Web.Page page in loadOp.Entities) {
                                    var selectedPage = this.pages.SingleOrDefault(o => o.Id == page.Id);
                                    if(selectedPage == null)
                                        this.pages.Add(page);
                                    else
                                        this.pages.Add(selectedPage);
                                }
                            this.PopulateItems(this.pages, 0, this.rtvMain.Items);
                        }, null);
                    } else
                        this.PopulateItems(this.pages, 0, this.rtvMain.Items);
                    this.isDataLoaded = true;
                }
            }
        }

        private void rtvMain_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            this.selectedPages.Clear();
            var treeItem = e.AddedItems.Cast<RadTreeViewItem>().FirstOrDefault();
            if(treeItem != null)
                this.selectedPages.Add(treeItem.Tag as Sunlight.Silverlight.Security.Web.Page);
            this.OnPropertyChanged("SelectedPages");
        }

        private void rtvMain_Checked(object sender, RadRoutedEventArgs e) {
            RadTreeViewItem item = e.Source as RadTreeViewItem;
            if(item != null) {
                e.Handled = true;
                var page = item.Tag as Sunlight.Silverlight.Security.Web.Page;
                if(!this.selectedPages.Contains(page)) {
                    this.selectedPages.Add(page);
                    this.OnPropertyChanged("SelectedPages");
                }
            }
        }

        private void rtvMain_Unchecked(object sender, RadRoutedEventArgs e) {
            RadTreeViewItem item = e.Source as RadTreeViewItem;
            if(item != null) {
                e.Handled = true;
                var page = item.Tag as Sunlight.Silverlight.Security.Web.Page;
                if(this.selectedPages.Contains(page)) {
                    this.selectedPages.Remove(page);
                    this.OnPropertyChanged("SelectedPages");
                }
            }
        }

        public void SetSelectedPage(Sunlight.Silverlight.Security.Web.Page page) {
            if(page == null)
                throw new ArgumentNullException();
            if(this.IsMultiple)
                throw new InvalidOperationException();
            this.selectedPages.Clear();
            this.selectedPages.Add(page);
            if(this.isDataLoaded)
                this.SetSelectedTreeView(this.rtvMain.Items, new List<Sunlight.Silverlight.Security.Web.Page> {
                    page,
                });
            this.OnPropertyChanged("SelectedPages");
        }

        public void ClearSelectedPages() {
            if(this.selectedPages.Count > 0) {
                this.selectedPages.Clear();
                this.OnPropertyChanged("SelectedPages");
            }
        }
    }
}
