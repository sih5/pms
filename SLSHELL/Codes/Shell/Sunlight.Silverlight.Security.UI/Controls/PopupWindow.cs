﻿using System;
using System.Windows;
using Telerik.Windows.Controls;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Security.Controls {
    /// <summary>
    /// 弹出窗体
    /// 
    /// 在初始化时，Content完全加载后可根据WindowStartupLocation重新定位弹出窗体。
    /// 在窗体大小变更时，防止窗体超出主程序的显示区域。
    /// </summary>
    public class PopupWindow : RadWindow {
        private bool relocationByContent;

        private void Content_Loaded(object sender, RoutedEventArgs e) {
            this.relocationByContent = true;
        }

        private void PopupWindow_SizeChanged(object sender, SizeChangedEventArgs e) {
            if(this.relocationByContent) {
                this.relocationByContent = false;
                this.RelocateByStartupLocation();
            }
            this.RestrictToEdge();
        }

        private void RelocateByStartupLocation() {
            if(this.WindowStartupLocation == WindowStartupLocation.CenterScreen ||
                    this.WindowStartupLocation == WindowStartupLocation.CenterOwner) {
                double left = 0, top = 0;
                if(this.WindowStartupLocation == WindowStartupLocation.CenterScreen) {
                    left = Application.Current.RootVisual.RenderSize.Width / 2 - this.ActualWidth / 2;
                    top = Application.Current.RootVisual.RenderSize.Height / 2 - this.ActualHeight / 2;
                } else if(this.WindowStartupLocation == WindowStartupLocation.CenterOwner) {
                    left = this.Owner.RenderSize.Width / 2 - this.ActualWidth / 2;
                    top = this.Owner.RenderSize.Height / 2 - this.ActualHeight / 2;
                }
                this.Left = Math.Max(0, left);
                this.Top = Math.Max(0, top);
            }
        }

        private void RestrictToEdge() {
            this.MaxWidth = Math.Max(this.ActualWidth, Application.Current.RootVisual.RenderSize.Width);
            this.MaxHeight = Math.Max(this.ActualHeight, Application.Current.RootVisual.RenderSize.Height);
        }

        protected override void OnContentChanged(object oldContent, object newContent) {
            base.OnContentChanged(oldContent, newContent);
            if(oldContent == null && newContent is FrameworkElement) {
                ((FrameworkElement)newContent).Loaded -= this.Content_Loaded;
                ((FrameworkElement)newContent).Loaded += this.Content_Loaded;
            }
            this.RelocateByStartupLocation();
            this.RestrictToEdge();
        }

        public PopupWindow() {
            this.Header = string.Empty;
            this.FontSize = 12;
            this.MinHeight = 300;
            this.MinWidth = 400;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.IsRestricted = true;
            this.SizeChanged += this.PopupWindow_SizeChanged;
        }
    }
}