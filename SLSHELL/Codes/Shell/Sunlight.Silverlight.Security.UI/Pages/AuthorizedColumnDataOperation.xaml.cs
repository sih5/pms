﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.ValueConverters;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Pages.Security {
    public partial class AuthorizedColumnDataOperation : OperationControl {
        private bool initialized;
        //使用DataEditingManager类来统一管理DomainContext的调用
        private DataEditingManager<SecurityDomainContext, AuthorizedColumn> dataManager;

        //用于初始化当前页面，将一次性的工作放到这里
        private void Initialize() {
            if(this.initialized)
                return;
            this.initialized = true;
            //设置DataEditingManager类的DomainContext为DataPage所使用的DomainContext
            this.dataManager.SetEditingDomainContext(this.DataPage.PageConfig.DomainContext as SecurityDomainContext);
            //将当前页面的DataContext属性绑定到DataEditingManager对象的Entity属性上
            this.SetBinding(UserControl.DataContextProperty, new Binding("Entity") {
                Source = this.dataManager,
                Mode = BindingMode.OneWay,
            });
            //设置控件相关的数据绑定
            var buttonBinding = new Binding("IsSubmitting") {
                Source = this.dataManager.DomainContext,
                Converter = new NegateBooleanConverter(),
            };
            this.btnSubmit.SetBinding(Button.IsEnabledProperty, buttonBinding);
            this.btnReset.SetBinding(Button.IsEnabledProperty, buttonBinding);
            this.btnReturn.SetBinding(Button.IsEnabledProperty, buttonBinding);
        }

        private void AuthorizedColumnOperationAdd_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            //调用初始化方法，该方法自行处理只执行一次
            this.Initialize();

            if(this.dataManager.Mode == DataEditingMode.Edit && !object.ReferenceEquals(this.dataManager.Entity, this.Parameters[0])) {
                //如果当前页面处于编辑模式，并且当前绑定的数据对象和传入的数据对象不是同一个

                //获取参数，设置数据源
                this.dataManager.Entity = this.Parameters.FirstOrDefault() as AuthorizedColumn;
            }
        }

        private void btnSubmit_Click(object sender, System.Windows.RoutedEventArgs e) {
            //提交授权列的数据
            this.dataManager.Submit(submitOp => {
                if(!submitOp.HasError) {
                    this.DataPage.ShowMainUI();
                }
            });
        }

        private void btnReset_Click(object sender, RoutedEventArgs e) {
            this.dataManager.Reject();
        }

        private void btnReturn_Click(object sender, RoutedEventArgs e) {
            this.dataManager.Reject();
            this.DataPage.ShowMainUI();
        }

        public AuthorizedColumnDataOperation()
            : this(false) {
        }

        public AuthorizedColumnDataOperation(bool isEditing) {
            InitializeComponent();
            this.dataManager = new DataEditingManager<SecurityDomainContext, AuthorizedColumn> {
                Mode = isEditing ? DataEditingMode.Edit : DataEditingMode.Add,
            };
        }
    }
}
