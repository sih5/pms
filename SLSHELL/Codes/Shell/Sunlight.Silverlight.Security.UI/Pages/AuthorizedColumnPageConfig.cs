﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Configs;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.Security.Web.Resources;

namespace Sunlight.Silverlight.Pages.Security {
    public class AuthorizedColumnPageConfig : SecurityDataPageConfig {
        public override string Title {
            get {
                return SecurityStrings.AuthorizedColumnPageTitle;
            }
        }

        public override Type EntityType {
            get {
                return typeof(AuthorizedColumn);
            }
        }

        public override IEnumerable<GridColumnConfig> GetGridColumnConfigs() {
            yield return new GridColumnConfig {
                ColumnName = "Id",
                GridHeaderText = SecurityStrings.CommonColumnId,
                WidthUnit = 2,
                IsReadOnly = true,
            };

            yield return new GridColumnConfig {
                ColumnName = "SystemName",
                GridHeaderText = SecurityStrings.AuthorizedColumnColumnSystemName,
                WidthUnit = 3,
            };
            yield return new GridColumnConfig {
                ColumnName = "TableName",
                GridHeaderText = SecurityStrings.AuthorizedColumnColumnTableName,
                WidthUnit = 3,
            };
            yield return new GridColumnConfig {
                ColumnName = "ColumnName",
                GridHeaderText = SecurityStrings.AuthorizedColumnColumnColumnName,
                WidthUnit = 3,
            };
            yield return new GridColumnConfig {
                ColumnName = "Status",
                GridHeaderText = SecurityStrings.CommonColumnStatus,
                WidthUnit = 2,
                KeyValueItems = GetKeyValueItems("Common"),
            };
        }

        public override IEnumerable<FilterPanelConfig> GetFilterPanelConfigs() {
            yield return new FilterPanelConfig {
                Name = SecurityStrings.AuthorizedColumnFilterPanelTitle1,
                Controls = new List<FilterControlConfig> {
                    new FilterControlConfig {
                        ColumnName = "SystemName",
                        Width = 100,
                    },
                    new FilterControlConfig {
                        ColumnName = "TableName",
                        Width = 100,
                    },

                    new FilterControlConfig {
                        ColumnName = "ColumnName",
                        Width = 100,
                    },
                    new FilterControlConfig {
                        ColumnName = "Status",
                        Width = 100,
                        DefaultValue = 0,
                    },
                },
                LoadFunction = (parameters) => {
                    SecurityDomainContext domainContext = (SecurityDomainContext)this.DomainContext;
                    int status = parameters["Status"] == null ? -1 : Convert.ToInt32(parameters["Status"]);
                    var query = domainContext.GetAuthorizedColumnByQuery(Convert.ToString(parameters["ColumnName"]), status);
                    return domainContext.Load<AuthorizedColumn>(query);
                },
            };
        }

        public override IEnumerable<OperationConfig> GetOperationConfigs() {
            yield return new OperationConfig {
                Id = "Add",
                Name = SecurityStrings.AuthorizedColumnOperationAdd,
                GroupName = SecurityStrings.AuthorizedColumnOperationPanelTitle1,
                Image = "/Sunlight.Silverlight.Security.UI;component/Images/column.png",
                TemplateType = TemplateType.DataModifyingTemplate,
                Content = new AuthorizedColumnDataOperation(),
            };
            yield return new OperationConfig {
                Id = "Edit",
                Name = SecurityStrings.AuthorizedColumnOperationEdit,
                GroupName = SecurityStrings.AuthorizedColumnOperationPanelTitle1,
                Image = "/Sunlight.Silverlight.Security.UI;component/Images/column.png",
                SelectionType = OperationSelectionType.Single,
                TemplateType = TemplateType.DataModifyingTemplate,
                Content = new AuthorizedColumnDataOperation(true),
            };
        }
    }
}
