﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.Security.Web.Resources;
using Sunlight.Silverlight.ValueConverters;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Pages.Security {
    public partial class NodeDataOperation : OperationControl {
        public sealed class NodeBindingItem {
            public string Name {
                get;
                set;
            }

            public int Value {
                get;
                set;
            }
        }

        public sealed class NodeTypeConverter : IValueConverter {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
                switch((int)value) {
                    case 0:
                        return SecurityStrings.NodeTextSelectPage;
                    case 1:
                        return SecurityStrings.NodeTextSelectAction;
                    case 2:
                        return SecurityStrings.NodeTextSelectColumn;
                    default:
                        return null;
                }
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
                throw new NotImplementedException();
            }
        }

        private bool initialized;
        //使用DataEditingManager类来统一管理DomainContext的调用
        private DataEditingManager<SecurityDomainContext, Node> dataManager;
        private Dictionary<int, IEnumerable<NodeBindingItem>> itemsSourceCache;

        private void SetCategoryIdItemsSource(int id) {
            IEnumerable<NodeBindingItem> result = null;
            Action setItemsSource = () => {
                this.rcbCategoryId.ItemsSource = result;
                if(this.rcbCategoryId.Items.Count > 0)
                    this.rcbCategoryId.SelectedIndex = 0;
            };
            if(this.itemsSourceCache == null)
                this.itemsSourceCache = new Dictionary<int, IEnumerable<NodeBindingItem>>(3);
            if(this.itemsSourceCache.TryGetValue(id, out result))
                setItemsSource();
            else
                switch(id) {
                    case 0:
                        this.dataManager.DomainContext.Load<Sunlight.Silverlight.Security.Web.Page>(this.dataManager.DomainContext.GetAuthorizablePagesQuery(), loadOp => {
                            result = from p in loadOp.Entities
                                     select new NodeBindingItem {
                                         Name = p.Name,
                                         Value = p.Id,
                                     };
                            this.itemsSourceCache.Add(id, result);
                            setItemsSource();
                        }, null);
                        break;
                    case 1:
                        this.dataManager.DomainContext.Load<SecurityAction>(this.dataManager.DomainContext.GetSecurityActionByQuery(null, 0), loadOp => {
                            result = from a in loadOp.Entities
                                     select new NodeBindingItem {
                                         Name = string.Format("{0} - {1}", a.Page.Name, a.OperationId),
                                         Value = a.Id,
                                     };
                            this.itemsSourceCache.Add(id, result);
                            setItemsSource();
                        }, null);
                        break;
                    case 2:
                        this.dataManager.DomainContext.Load<AuthorizedColumn>(this.dataManager.DomainContext.GetAuthorizedColumnByQuery(null, 0), loadOp => {
                            result = from c in loadOp.Entities
                                     select new NodeBindingItem {
                                         Name = c.ColumnName,
                                         Value = c.Id,
                                     };
                            this.itemsSourceCache.Add(id, result);
                            setItemsSource();
                        }, null);
                        break;
                    default:
                        setItemsSource();
                        break;
                }
        }

        //用于初始化当前页面，将一次性的工作放到这里
        private void Initialize() {
            if(this.initialized)
                return;
            this.initialized = true;
            //设置DataEditingManager类的DomainContext为DataPage所使用的DomainContext
            this.dataManager.SetEditingDomainContext(this.DataPage.PageConfig.DomainContext as SecurityDomainContext);
            //将当前页面的DataContext属性绑定到DataEditingManager对象的Entity属性上
            this.SetBinding(UserControl.DataContextProperty, new Binding("Entity") {
                Source = this.dataManager,
                Mode = BindingMode.OneWay,
            });
            this.tbSelectNode.SetBinding(TextBlock.TextProperty, new Binding("CategoryType") {
                Mode = BindingMode.OneWay,
                Converter = new NodeTypeConverter(),
            });
            //设置控件相关的数据绑定
            var buttonBinding = new Binding("IsSubmitting") {
                Source = this.dataManager.DomainContext,
                Converter = new NegateBooleanConverter(),
            };
            this.btnSubmit.SetBinding(Button.IsEnabledProperty, buttonBinding);
            this.btnReset.SetBinding(Button.IsEnabledProperty, buttonBinding);
            this.btnReturn.SetBinding(Button.IsEnabledProperty, buttonBinding);
            //获取ComboBox的数据源
            this.rcbCategoryType.ItemsSource = (this.DataPage.PageConfig as SecurityDataPageConfig).GetKeyValueItems("NodeType");
        }

        private void OperationControl_Loaded(object sender, RoutedEventArgs e) {
            //调用初始化方法，该方法自行处理只执行一次
            this.Initialize();

            if(this.dataManager.Mode == DataEditingMode.Edit && !object.ReferenceEquals(this.dataManager.Entity, this.Parameters[0])) {
                //如果当前页面处于编辑模式，并且当前绑定的数据对象和传入的数据对象不是同一个

                //获取参数，设置数据源
                this.dataManager.Entity = this.Parameters.FirstOrDefault() as Node;
            }
        }

        private void rcbCategoryType_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            RadComboBox comboBox = sender as RadComboBox;
            if(comboBox != null)
                this.SetCategoryIdItemsSource((int)comboBox.SelectedValue);
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e) {
            //提交授权点的数据
            this.dataManager.Submit(submitOp => {
                if(!submitOp.HasError) {
                    this.DataPage.ShowMainUI();
                }
            });
        }

        private void btnReset_Click(object sender, RoutedEventArgs e) {
            this.dataManager.Reject();
            if(this.rcbCategoryId.Items.Count > 0)
                this.rcbCategoryId.SelectedIndex = 0;
        }

        private void btnReturn_Click(object sender, RoutedEventArgs e) {
            this.dataManager.Reject();
            this.DataPage.ShowMainUI();
        }

        public NodeDataOperation()
            : this(false) {
        }

        public NodeDataOperation(bool isEditing) {
            InitializeComponent();
            this.dataManager = new DataEditingManager<SecurityDomainContext, Node> {
                Mode = isEditing ? DataEditingMode.Edit : DataEditingMode.Add,
            };
        }
    }
}
