﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Configs;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.Security.Web.Resources;

namespace Sunlight.Silverlight.Pages.Security {

    public class NodePageConfig : SecurityDataPageConfig {

        public override string Title {
            get {
                return SecurityStrings.NodePageTitle;
            }
        }

        public override Type EntityType {
            get {
                return typeof(Node);
            }
        }

        public override IEnumerable<GridColumnConfig> GetGridColumnConfigs() {
            yield return new GridColumnConfig {
                WidthUnit = 2,
                IsReadOnly = true,
                ColumnName = "Id",
                GridHeaderText = SecurityStrings.CommonColumnId,
            };
            yield return new GridColumnConfig {
                WidthUnit = 3,
                IsReadOnly = true,
                ColumnName = "CategoryType",
                KeyValueItems = GetKeyValueItems("NodeType"),
                GridHeaderText = SecurityStrings.NodeColumnCategoryType,
            };
            yield return new GridColumnConfig {
                WidthUnit = 3,
                ColumnName = "Status",
                KeyValueItems = GetKeyValueItems("Common"),
                GridHeaderText = SecurityStrings.CommonColumnStatus,
            };
        }

        public override IEnumerable<FilterPanelConfig> GetFilterPanelConfigs() {
            yield return new FilterPanelConfig {
                Name = SecurityStrings.NodeFilterPanelTitle1,
                Controls = new List<FilterControlConfig> {
                    new FilterControlConfig {
                        Width = 130,
                        ColumnName = "Status",
                        DefaultValue = 0,
                    },
                },
                LoadFunction = (parameters) => {
                    SecurityDomainContext domainContext = (SecurityDomainContext)this.DomainContext;
                    int status = parameters["Status"] == null ? -1 : Convert.ToInt32(parameters["Status"]);
                    var query = domainContext.GetNodesByQuery(status);
                    return domainContext.Load<Node>(query);
                },
            };
        }

        public override IEnumerable<OperationConfig> GetOperationConfigs() {
            yield return new OperationConfig {
                Id = "Add",
                Name = SecurityStrings.NodeOperationAdd,
                GroupName = SecurityStrings.NodeOperationPanelTitle1,
                Image = "/Sunlight.Silverlight.Security.UI;component/Images/node-add.png",
                TemplateType = TemplateType.DataModifyingTemplate,
                Content = new NodeDataOperation(),
            };
            yield return new OperationConfig {
                Id = "Edit",
                Name = SecurityStrings.NodeOperationEdit,
                GroupName = SecurityStrings.NodeOperationPanelTitle1,
                Image = "/Sunlight.Silverlight.Security.UI;component/Images/node-edit.png",
                SelectionType = OperationSelectionType.Single,
                TemplateType = TemplateType.DataModifyingTemplate,
                Content = new NodeDataOperation(true),
            };
        }
    }
}
