﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Security;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.ValueConverters;

namespace Sunlight.Silverlight.Pages.Security {
    public partial class OrganizationDataOperation : OperationControl {
        private bool initialized;
        //使用DataEditingManager类来统一管理DomainContext的调用
        private DataEditingManager<SecurityDomainContext, Organization> dataManager;
        //用于存储与当前组织相关联的父级组织
        private Organization organization;

        private void SetOrganizationButtonEnabled(bool isEnabled) {
            this.rddbOrganizations.IsEnabled = isEnabled;
            if(isEnabled) {
                this.spnOrganization.Visibility = Visibility.Collapsed;
                this.imgOrganization.Visibility = Visibility.Visible;
            } else {
                this.spnOrganization.Visibility = Visibility.Visible;
                this.imgOrganization.Visibility = Visibility.Collapsed;
            }
        }

        //用于初始化当前页面，将一次性的工作放到这里
        private void Initialize() {
            if(this.initialized)
                return;
            this.initialized = true;
            //设置DataEditingManager类的DomainContext为DataPage所使用的DomainContext
            this.dataManager.SetEditingDomainContext(this.DataPage.PageConfig.DomainContext as SecurityDomainContext);
            //将当前页面的DataContext属性绑定到DataEditingManager对象的Entity属性上
            this.SetBinding(UserControl.DataContextProperty, new Binding("Entity") {
                Source = this.dataManager,
                Mode = BindingMode.OneWay,
            });
            //设置权限文本的数据绑定
            this.txtOrganization.SetBinding(TextBlock.TextProperty, new Binding("SelectedOrganizations") {
                Source = this.otvOrganization,
                Mode = BindingMode.OneWay,
                Converter = new OrganizationConverter(),
            });
            //设置控件相关的数据绑定
            var buttonBinding = new Binding("IsSubmitting") {
                Source = this.dataManager.DomainContext,
                Converter = new NegateBooleanConverter(),
            };
            this.btnCommit.SetBinding(Button.IsEnabledProperty, buttonBinding);
            this.btnReset.SetBinding(Button.IsEnabledProperty, buttonBinding);
            this.btnCancel.SetBinding(Button.IsEnabledProperty, buttonBinding);
            //获取ComboBox的数据源
            this.rcbOrganizationType.ItemsSource = (this.DataPage.PageConfig as SecurityDataPageConfig).GetKeyValueItems("OrganizationType");
        }

        //Loaded事件会在本窗体每次显示时都触发一次，要注意判断代码是否应该被重复执行
        private void OrganizationOperationAdd_Loaded(object sender, RoutedEventArgs e) {
            //调用初始化方法，该方法自行处理只执行一次
            this.Initialize();

            if(this.dataManager.Mode == DataEditingMode.Edit && !object.ReferenceEquals(this.dataManager.Entity, this.Parameters[0])) {
                //如果当前页面处于编辑模式，并且当前绑定的数据对象和传入的数据对象不是同一个

                //获取参数，设置数据源
                this.dataManager.Entity = this.Parameters.FirstOrDefault() as Organization;
                //设置组织树按钮不可用
                this.SetOrganizationButtonEnabled(false);
                //获取角色所关联的组织
                this.dataManager.Entity.GetParentOrganization(this.dataManager.DomainContext, (success, organization) => {
                    if(success) {
                        this.organization = organization;
                        if(organization != null)
                            this.otvOrganization.SetSelectedOrganization(organization);
                    }
                    //设置组织树按钮可用
                    this.SetOrganizationButtonEnabled(true);
                });
            }
        }

        private void btnCommit_Click(object sender, RoutedEventArgs e) {
            var parentOrganization = this.otvOrganization.SelectedOrganizations.FirstOrDefault();
            Organization organization = this.dataManager.Entity;
            //设置组织相关联的父级组织
            if(parentOrganization == null)
                organization.ParentId = 0;
            else
                organization.ParentId = parentOrganization.Id;
            //提交组织的数据
            this.dataManager.Submit(submitOp => {
                if(!submitOp.HasError) {
                    this.organization = organization;
                    this.DataPage.ShowMainUI();
                }
            });
        }

        private void btnReset_Click(object sender, RoutedEventArgs e) {
            this.dataManager.Reject();
            this.otvOrganization.ClearSelectedOrganizations();
            if(this.dataManager.Mode == DataEditingMode.Edit)
                this.otvOrganization.SetSelectedOrganization(this.organization);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e) {
            this.dataManager.Reject();
            this.DataPage.ShowMainUI();
        }

        public OrganizationDataOperation()
            : this(false) {
        }

        public OrganizationDataOperation(bool isEditing) {
            InitializeComponent();
            this.dataManager = new DataEditingManager<SecurityDomainContext, Organization> {
                Mode = isEditing ? DataEditingMode.Edit : DataEditingMode.Add,
            };
        }
    }
}
