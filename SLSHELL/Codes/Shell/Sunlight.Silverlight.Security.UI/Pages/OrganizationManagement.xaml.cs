﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.Pages {

    public enum OrgShowMode {
        Tree,
        Chart,
    }

    public partial class OrganizationManagement : OperationControl {

        private List<Organization> organizations = null;

        private void Initialize() {
            this.orgTree.IsOptionElementsEnabled = false;
            this.orgTree.SelectionChanged -= new Telerik.Windows.Controls.SelectionChangedEventHandler(this.orgTree_SelectionChanged);
            this.orgTree.SelectionMode = Telerik.Windows.Controls.SelectionMode.Single;
            this.orgTree.SelectionChanged += new Telerik.Windows.Controls.SelectionChangedEventHandler(this.orgTree_SelectionChanged);
            this.orgChart.IsOptionElementsEnabled = false;
            this.orgChart.SelectionMode = Telerik.Windows.Controls.SelectionMode.Single;
        }

        private void PopulateItems(IEnumerable<Organization> organizations, int parentId, ItemCollection items, OrgShowMode orgShowMode) {
            foreach(var organization in organizations.Where(o => o.ParentId == parentId).OrderBy(o => o.Sequence)) {
                RadTreeViewItem item = null;
                switch(orgShowMode) {
                    case OrgShowMode.Tree:
                        item = new RadTreeViewItem {
                            Header = organization.Name,
                            Tag = organization,
                            DefaultImageSrc = this.getOrganizationPic(organization.Type),
                        };
                        break;
                    case OrgShowMode.Chart:
                        item = new RadTreeViewItem {
                            Header = organization.Name,
                            Tag = organization,
                            DefaultImageSrc = this.getOrganizationPic(organization.Type),
                            Style = (Style)this.Resources["OrganizationTreeStyle"],
                            BorderThickness = new Thickness(1),
                            BorderBrush = new SolidColorBrush(Colors.Black),
                        };
                        break;
                }
                items.Add(item);
                this.PopulateItems(organizations, organization.Id, item.Items, orgShowMode);
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e) {
            this.Initialize();

            this.orgTree.Items.Clear();
            if(this.organizations == null) {
                SecurityDomainContext domainContext = this.DataPage.PageConfig.DomainContext as SecurityDomainContext;
                domainContext.Load<Organization>(domainContext.GetOrganizationByQuery(null, 0, -1), (loadOp) => {
                    this.organizations = new List<Organization>(loadOp.Entities.Count());
                    this.organizations = loadOp.Entities.ToList();
                    this.PopulateItems(this.organizations, 0, this.orgTree.Items, OrgShowMode.Tree);
                }, null);
            } else
                this.PopulateItems(this.organizations, 0, this.orgTree.Items, OrgShowMode.Tree);
        }

        private void orgTree_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var treeItem = e.AddedItems.Cast<RadTreeViewItem>().FirstOrDefault().Tag as Organization;
            if(treeItem != null)
                this.ShowOrgChart(treeItem);
        }

        private void ShowOrgChart(Organization org) {
            this.orgChart.Items.Clear();
            List<Organization> chartOrganizations = new List<Organization>();

            this.getCurrentAndChildrens(this.organizations, org.Id, -1, chartOrganizations);
            this.PopulateItems(chartOrganizations, org.ParentId, this.orgChart.Items, OrgShowMode.Chart);
        }

        private void getCurrentAndChildrens(IEnumerable<Organization> organizations, int id, int parentId, List<Organization> chartOrganizations) {
            foreach(Organization org in organizations)
                if(parentId < 0 && org.Id == id || org.ParentId == parentId) {
                    chartOrganizations.Add(org);
                    getCurrentAndChildrens(organizations, org.Id, org.Id, chartOrganizations);
                }
        }

        private Uri getOrganizationPic(int type) {
            Uri uri = null;
            switch(type) {
                case 1:
                    uri = new Uri("/Sunlight.Silverlight.Security.UI;component/Images/company-s.png", UriKind.Relative);
                    break;
                case 2:
                    uri = new Uri("/Sunlight.Silverlight.Security.UI;component/Images/department-s.png", UriKind.Relative);
                    break;
                case 3:
                    uri = new Uri("/Sunlight.Silverlight.Security.UI;component/Images/team.png-s", UriKind.Relative);
                    break;
            }
            return uri;
        }

        public OrganizationManagement() {
            InitializeComponent();
        }

    }
}
