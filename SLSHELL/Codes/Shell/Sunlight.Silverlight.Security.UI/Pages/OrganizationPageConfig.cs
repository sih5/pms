﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Configs;
using Sunlight.Silverlight.Security.Pages;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.Security.Web.Resources;

namespace Sunlight.Silverlight.Pages.Security {
    public sealed class OrganizationPageConfig : SecurityDataPageConfig {
        public override string Title {
            get {
                return SecurityStrings.OrganizationPageTitle;
            }
        }

        public override Type EntityType {
            get {
                return typeof(Organization);
            }
        }

        public override IEnumerable<GridColumnConfig> GetGridColumnConfigs() {
            yield return new GridColumnConfig {
                ColumnName = "Id",
                GridHeaderText = SecurityStrings.CommonColumnId,
                WidthUnit = 2,
                IsReadOnly = true,
            };
            yield return new GridColumnConfig {
                ColumnName = "Name",
                GridHeaderText = SecurityStrings.OrganizationColumnName,
                WidthUnit = 3,
            };
            yield return new GridColumnConfig {
                ColumnName = "Type",
                GridHeaderText = SecurityStrings.OrganizationColumnType,
                WidthUnit = 3,
                KeyValueItems = GetKeyValueItems("OrganizationType"),
            };
            yield return new GridColumnConfig {
                ColumnName = "Status",
                GridHeaderText = SecurityStrings.CommonColumnStatus,
                WidthUnit = 2,
                KeyValueItems = GetKeyValueItems("Common"),
            };
        }

        public override IEnumerable<FilterPanelConfig> GetFilterPanelConfigs() {
            yield return new FilterPanelConfig {
                Name = SecurityStrings.OrganizationFilterPanelTitle1,
                Controls = new List<FilterControlConfig>{
                    new FilterControlConfig {
                        ColumnName = "Name",
                        Width = 130,
                    }, 
                    new FilterControlConfig {
                        ColumnName = "Status",
                        DefaultValue = 0,
                    }, 
                    new FilterControlConfig {
                        ColumnName = "Type",
                        Width = 130,
                    }, 
                 },
                LoadFunction = (parameters) => {
                    SecurityDomainContext domainContext = (SecurityDomainContext)this.DomainContext;
                    int status = parameters["Status"] == null ? -1 : Convert.ToInt32(parameters["Status"]);
                    int type = parameters["Type"] == null ? -1 : Convert.ToInt32(parameters["Type"]);
                    var query = domainContext.GetOrganizationByQuery(Convert.ToString(parameters["Name"]), status, type);
                    return domainContext.Load<Organization>(query);
                },
            };
        }

        public override IEnumerable<OperationConfig> GetOperationConfigs() {
            yield return new OperationConfig {
                Id = "View",
                Name = SecurityStrings.OrganizationOperationView,
                GroupName = SecurityStrings.OrganizationPageTitle,
                Image = "/Sunlight.Silverlight.Security.UI;component/Images/organization-view.png",
                SelectionType = OperationSelectionType.None,
                Content = new OrganizationManagement(),
            };
            yield return new OperationConfig {
                Id = "Add",
                Name = SecurityStrings.OrganizationOperationAdd,
                GroupName = SecurityStrings.OrganizationOperationPanelTitle1,
                Image = "/Sunlight.Silverlight.Security.UI;component/Images/organization-add.png",
                TemplateType = TemplateType.DataModifyingTemplate,
                Content = new OrganizationDataOperation(),
            };
            yield return new OperationConfig {
                Id = "Edit",
                Name = SecurityStrings.OrganizationOperationEdit,
                GroupName = SecurityStrings.OrganizationOperationPanelTitle1,
                Image = "/Sunlight.Silverlight.Security.UI;component/Images/organization-edit.png",
                TemplateType = TemplateType.DataModifyingTemplate,
                SelectionType = OperationSelectionType.Single,
                Content = new OrganizationDataOperation(true),
            };
        }
    }
}
