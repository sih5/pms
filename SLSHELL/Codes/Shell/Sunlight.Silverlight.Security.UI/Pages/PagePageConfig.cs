﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Configs;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.Security.Web.Resources;

namespace Sunlight.Silverlight.Pages.Security {
    public class PagePageConfig : SecurityDataPageConfig {
        public override string Title {
            get {
                return SecurityStrings.PagePageTitle;
            }
        }

        public override Type EntityType {
            get {
                return typeof(Page);
            }
        }

        public override IEnumerable<GridColumnConfig> GetGridColumnConfigs() {
            yield return new GridColumnConfig {
                ColumnName = "Id",
                GridHeaderText = SecurityStrings.CommonColumnId,
                WidthUnit = 2,
                IsReadOnly = true,
            };
            yield return new GridColumnConfig {
                ColumnName = "Name",
                GridHeaderText = SecurityStrings.PageColumnName,
                WidthUnit = 3,
            };
            yield return new GridColumnConfig {
                ColumnName = "Type",
                GridHeaderText = SecurityStrings.PageColumnType,
                WidthUnit = 3,
                KeyValueItems = GetKeyValueItems("MenuType"),
            };
            yield return new GridColumnConfig {
                ColumnName = "PageId",
                GridHeaderText = SecurityStrings.PageColumnPageId,
                WidthUnit = 3,
            };
            yield return new GridColumnConfig {
                ColumnName = "PageType",
                GridHeaderText = SecurityStrings.PageColumnPageType,
                WidthUnit = 3,
            };
            yield return new GridColumnConfig {
                ColumnName = "Parameter",
                GridHeaderText = SecurityStrings.PageColumnParameter,
                WidthUnit = 3,
            };
            yield return new GridColumnConfig {
                ColumnName = "Description",
                GridHeaderText = SecurityStrings.PageColumnDescription,
                WidthUnit = 3,
            };
            yield return new GridColumnConfig {
                ColumnName = "LargeImage",
                GridHeaderText = SecurityStrings.PageColumnLargeImage,
                WidthUnit = 3,
            };
            yield return new GridColumnConfig {
                ColumnName = "SmallImage",
                GridHeaderText = SecurityStrings.PageColumnSmallImage,
                WidthUnit = 3,
            };
            yield return new GridColumnConfig {
                ColumnName = "Status",
                GridHeaderText = SecurityStrings.CommonColumnStatus,
                WidthUnit = 2,
                KeyValueItems = GetKeyValueItems("Common"),
            };
        }

        public override IEnumerable<FilterPanelConfig> GetFilterPanelConfigs() {
            yield return new FilterPanelConfig {
                Name = SecurityStrings.PageFilterPanelTitle1,
                Controls = new List<FilterControlConfig> {
                    new FilterControlConfig {
                       ColumnName = "Name",
                        Width = 130,
                   },
                   new FilterControlConfig {
                       ColumnName = "Type",
                        Width = 130,
                   },
                   new FilterControlConfig {
                       ColumnName = "PageId",
                        Width = 130,
                   },
                   new FilterControlConfig {
                       ColumnName = "Status",
                       Width = 130,
                       DefaultValue = 0,
                   },
                },

                LoadFunction = (parameters) => {
                    SecurityDomainContext domainContext = (SecurityDomainContext)this.DomainContext;
                    int status = parameters["Status"] == null ? -1 : Convert.ToInt32(parameters["Status"]);
                    int type = parameters["Type"] == null ? -1 : Convert.ToInt32(parameters["Type"]);
                    var query = domainContext.GetPageByQuery(Convert.ToString(parameters["Name"]), Convert.ToString(parameters["PageId"]), status, type);
                    return domainContext.Load<Page>(query);
                },
            };
        }

        public override IEnumerable<OperationConfig> GetOperationConfigs() {
            yield return new OperationConfig {
                Id = "Add",
                Name = SecurityStrings.PageOperationAdd,
                GroupName = SecurityStrings.PageOperationPanelTitle1,
                TemplateType = TemplateType.DataModifyingTemplate,
                Image = "/Sunlight.Silverlight.Security.UI;component/Images/page-add.png",
                Content = new PageDataOperation(),
            };
            yield return new OperationConfig {
                Id = "Edit",
                Name = SecurityStrings.PageOperationEdit,
                GroupName = SecurityStrings.PageOperationPanelTitle1,
                Image = "/Sunlight.Silverlight.Security.UI;component/Images/page-edit.png",
                TemplateType = TemplateType.DataModifyingTemplate,
                SelectionType = OperationSelectionType.Single,
                Content = new PageDataOperation(true),
            };
        }
    }
}
