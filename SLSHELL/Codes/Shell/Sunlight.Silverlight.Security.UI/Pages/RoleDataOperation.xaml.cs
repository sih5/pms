﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.ValueConverters;

namespace Sunlight.Silverlight.Pages.Security {
    public partial class RoleDataOperation : OperationControl {
        private bool initialized;
        //使用DataEditingManager类来统一管理DomainContext的调用
        private DataEditingManager<SecurityDomainContext, Role> dataManager;

        //用于初始化当前页面，将一次性的工作放到这里
        private void Initialize() {
            if(this.initialized)
                return;
            this.initialized = true;
            //设置DataEditingManager类的DomainContext为DataPage所使用的DomainContext
            this.dataManager.SetEditingDomainContext(this.DataPage.PageConfig.DomainContext as SecurityDomainContext);
            //将当前页面的DataContext属性绑定到DataEditingManager对象的Entity属性上
            this.SetBinding(UserControl.DataContextProperty, new Binding("Entity") {
                Source = this.dataManager,
                Mode = BindingMode.OneWay,
            });
            //设置控件相关的数据绑定
            var buttonBinding = new Binding("IsSubmitting") {
                Source = this.dataManager.DomainContext,
                Converter = new NegateBooleanConverter(),
            };
            this.btnSubmit.SetBinding(Button.IsEnabledProperty, buttonBinding);
            this.btnReset.SetBinding(Button.IsEnabledProperty, buttonBinding);
            this.btnReturn.SetBinding(Button.IsEnabledProperty, buttonBinding);
        }

        //Loaded事件会在本窗体每次显示时都触发一次，要注意判断代码是否应该被重复执行
        private void OperationControl_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            this.Initialize();

            if(this.dataManager.Mode == DataEditingMode.Edit && !object.ReferenceEquals(this.dataManager.Entity, this.Parameters[0])) {
                //如果当前页面处于编辑模式，并且当前绑定的数据对象和传入的数据对象不是同一个

                //获取参数，设置数据源
                this.dataManager.Entity = this.Parameters.FirstOrDefault() as Role;
            }
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e) {
            //提交角色的数据
            this.dataManager.Submit(submitOp => {
                if(!submitOp.HasError) {
                    this.DataPage.ShowMainUI();
                }
            });
        }

        private void btnReset_Click(object sender, RoutedEventArgs e) {
            this.dataManager.Reject();
        }

        private void btnReturn_Click(object sender, RoutedEventArgs e) {
            this.dataManager.Reject();
            this.DataPage.ShowMainUI();
        }

        public RoleDataOperation()
            : this(false) {
        }

        public RoleDataOperation(bool isEditing) {
            InitializeComponent();
            //实例化DataEditingManager对象
            this.dataManager = new DataEditingManager<SecurityDomainContext, Role> {
                Mode = isEditing ? DataEditingMode.Edit : DataEditingMode.Add,
            };
        }
    }
}
