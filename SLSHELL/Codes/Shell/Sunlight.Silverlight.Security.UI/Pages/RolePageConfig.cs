﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Configs;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.Security.Web.Resources;

namespace Sunlight.Silverlight.Pages.Security {

    public class RolePageConfig : SecurityDataPageConfig {

        public override string Title {
            get {
                return SecurityStrings.RolePageTitle;
            }
        }

        public override Type EntityType {
            get {
                return typeof(Role);
            }
        }

        public override IEnumerable<GridColumnConfig> GetGridColumnConfigs() {
            yield return new GridColumnConfig {
                WidthUnit = 2,
                ColumnName = "Id",
                IsReadOnly = true,
                GridHeaderText = SecurityStrings.CommonColumnId,
            };
            yield return new GridColumnConfig {
                WidthUnit = 3,
                IsReadOnly = true,
                ColumnName = "OrganizationName",
                GridHeaderText = SecurityStrings.UserAssociateOrganization,
            };
            yield return new GridColumnConfig {
                WidthUnit = 3,
                ColumnName = "Name",
                GridHeaderText = SecurityStrings.RoleColumnName,
            };
            yield return new GridColumnConfig {
                WidthUnit = 3,
                ColumnName = "Status",
                KeyValueItems = GetKeyValueItems("Common"),
                GridHeaderText = SecurityStrings.CommonColumnStatus,
            };
        }

        public override IEnumerable<FilterPanelConfig> GetFilterPanelConfigs() {
            yield return new FilterPanelConfig {
                Name = SecurityStrings.RoleFilterPanelTitle1,
                Controls = new List<FilterControlConfig> {
                    new FilterControlConfig {
                        Width = 130,
                        ColumnName = "Name",
                    },
                    new FilterControlConfig {
                        Width = 130,
                        ColumnName = "Status",
                        DefaultValue = 0,
                    },
                },
                LoadFunction = (parameters) => {
                    SecurityDomainContext domainContext = (SecurityDomainContext)this.DomainContext;
                    int status = parameters["Status"] == null ? -1 : Convert.ToInt32(parameters["Status"]);
                    var query = domainContext.GetRolesByQuery(Convert.ToString(parameters["Name"]), status);
                    return domainContext.Load<Role>(query);
                },
            };
        }

        public override IEnumerable<OperationConfig> GetOperationConfigs() {
            yield return new OperationConfig {
                Id = "Add",
                Name = SecurityStrings.RoleOperationAdd,
                GroupName = SecurityStrings.RoleOperationPanelTitle1,
                Image = "/Sunlight.Silverlight.Security.UI;component/Images/role-add.png",
                TemplateType = TemplateType.DataModifyingTemplate,
                Content = new RoleDataOperation(),
            };
            yield return new OperationConfig {
                Id = "Edit",
                Name = SecurityStrings.RoleOperationEdit,
                GroupName = SecurityStrings.RoleOperationPanelTitle1,
                Image = "/Sunlight.Silverlight.Security.UI;component/Images/role-edit.png",
                SelectionType = OperationSelectionType.Single,
                TemplateType = TemplateType.DataModifyingTemplate,
                Content = new RoleDataOperation(true),
            };
        }
    }
}
