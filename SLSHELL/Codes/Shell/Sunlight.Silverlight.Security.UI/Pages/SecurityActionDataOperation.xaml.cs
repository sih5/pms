﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Security;
using Sunlight.Silverlight.ValueConverters;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Pages.Security {
    public partial class SecurityActionDataOperation : OperationControl {
        private bool initialized;

        //使用DataEditingManager类来统一管理DomainContext的调用
        private DataEditingManager<SecurityDomainContext, SecurityAction> dataManager;

        //用于存储与当前操作相关联的页面集合
        private IEnumerable<Sunlight.Silverlight.Security.Web.Page> pages;

        //用于初始化当前页面，将一次性的工作放到这里
        private void Initialize() {
            if(this.initialized)
                return;
            this.initialized = true;
            //设置DataEditingManager类的DomainContext为DataPage所使用的DomainContext
            this.dataManager.SetEditingDomainContext(this.DataPage.PageConfig.DomainContext as SecurityDomainContext);
            //将当前页面的DataContext属性绑定到DataEditingManager对象的Entity属性上
            this.SetBinding(UserControl.DataContextProperty, new Binding("Entity") {
                Source = this.dataManager,
                Mode = BindingMode.OneWay,
            });
            //设置权限文本的数据绑定
            this.txtPages.SetBinding(TextBlock.TextProperty, new Binding("SelectedPages") {
                Source = this.otvPages,
                Mode = BindingMode.OneWay,
                Converter = new PageConverter(),
            });

            //设置控件相关的数据绑定
            var buttonBinding = new Binding("IsSubmitting") {
                Source = this.dataManager.DomainContext,
                Converter = new NegateBooleanConverter(),
            };

            this.btnSubmit.SetBinding(Button.IsEnabledProperty, buttonBinding);
            this.btnReset.SetBinding(Button.IsEnabledProperty, buttonBinding);
            this.btnReturn.SetBinding(Button.IsEnabledProperty, buttonBinding);
        }

        private void SecurityActionOperationAdd_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            //调用初始化方法，该方法自行处理只执行一次
            this.Initialize();
            //设置页面树按钮不可用
            this.SetPageButtonEnabled(true);
            //设置树形结构图为单选模式
            this.otvPages.IsMultiple = false;

            if(this.dataManager.Mode == DataEditingMode.Edit && !object.ReferenceEquals(this.dataManager.Entity, this.Parameters[0])) {
                //如果当前页面处于编辑模式，并且当前绑定的数据对象和传入的数据对象不是同一个

                //获取参数，设置数据源
                this.dataManager.Entity = this.Parameters.FirstOrDefault() as SecurityAction;
                //清除页面树上的选择项
                this.otvPages.ClearSelectedPages();
                this.pages = null;
                //获取所关联的页面
                this.dataManager.Entity.GetActionPages(this.dataManager.DomainContext, (success, pages) => {
                    if(success)
                        this.otvPages.SetSelectedPage(pages.First());
                    //设置页面树按钮可用
                    this.SetPageButtonEnabled(true);
                });
            }
        }

        //该方法用来统一地设置显示树控件的按钮
        private void SetPageButtonEnabled(bool isEnabled) {
            this.rddbPages.IsEnabled = isEnabled;
            if(isEnabled) {
                this.spnPage.Visibility = Visibility.Collapsed;
                this.imgPage.Visibility = Visibility.Visible;
            } else {
                this.spnPage.Visibility = Visibility.Visible;
                this.imgPage.Visibility = Visibility.Collapsed;
            }
        }


        private void btnSubmit_Click(object sender, RoutedEventArgs e) {
            //提交操作点的数据
            SecurityAction securityAction = this.dataManager.Entity;
            if(this.dataManager.Mode == DataEditingMode.Add) {  //如果当前页面处于新增模式
                if(this.otvPages.SelectedPages.Count() > 0) {   //如果页面树控件存在选择项
                    securityAction.PageId = otvPages.SelectedPages.First().Id;
                }
                this.dataManager.Submit(submitOp => {
                    securityAction = (SecurityAction)submitOp.ChangeSet.AddedEntities[0];
                    if(!submitOp.HasError) {    //如果成功提交
                        this.DataPage.ShowMainUI();
                    }
                });
            } else if(this.dataManager.Mode == DataEditingMode.Edit) {  //如果当前页面处于编辑模式 
                if(this.otvPages.SelectedPages.Count() > 0) {   //如果页面树控件存在选择项
                    securityAction.PageId = otvPages.SelectedPages.First().Id;
                }
                this.dataManager.Submit(submitOp => {
                    securityAction = (SecurityAction)submitOp.ChangeSet.ModifiedEntities[0];
                    if(!submitOp.HasError) {    //如果成功提交
                        this.DataPage.ShowMainUI();
                    }
                });
            }
        }
        private void btnReset_Click(object sender, RoutedEventArgs e) {
            this.dataManager.Reject();
            if(this.dataManager.Mode == DataEditingMode.Edit)
                this.otvPages.SetSelectedPage(pages.First());
        }

        private void btnReturn_Click(object sender, RoutedEventArgs e) {
            this.dataManager.Reject();
            this.DataPage.ShowMainUI();
        }

        public SecurityActionDataOperation()
            : this(false) {
        }

        public SecurityActionDataOperation(bool isEditing) {
            InitializeComponent();
            this.dataManager = new DataEditingManager<SecurityDomainContext, SecurityAction> {
                Mode = isEditing ? DataEditingMode.Edit : DataEditingMode.Add,
            };
        }
    }
}
