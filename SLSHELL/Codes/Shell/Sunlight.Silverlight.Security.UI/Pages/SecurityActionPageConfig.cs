﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Configs;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.Security.Web.Resources;

namespace Sunlight.Silverlight.Pages.Security {
    public class SecurityActionPageConfig : SecurityDataPageConfig {
        public override string Title {
            get {
                return SecurityStrings.SecurityActionPageTitle;
            }
        }

        public override Type EntityType {
            get {
                return typeof(SecurityAction);
            }
        }

        public override System.Collections.Generic.IEnumerable<GridColumnConfig> GetGridColumnConfigs() {
            yield return new GridColumnConfig {
                ColumnName = "Id",
                GridHeaderText = SecurityStrings.CommonColumnId,
                WidthUnit = 2,
                IsReadOnly = true,
            };
            yield return new GridColumnConfig {
                ColumnName = "PageName",
                GridHeaderText = SecurityStrings.PageColumnName,
                WidthUnit = 3,
                IsReadOnly = true,
            };
            yield return new GridColumnConfig {
                ColumnName = "OperationId",
                GridHeaderText = SecurityStrings.SecurityActionColumnOperationId,
                WidthUnit = 3,
            };
            yield return new GridColumnConfig {
                ColumnName = "Status",
                GridHeaderText = SecurityStrings.CommonColumnStatus,
                WidthUnit = 2,
                KeyValueItems = GetKeyValueItems("Common"),
            };
        }

        public override IEnumerable<FilterPanelConfig> GetFilterPanelConfigs() {
            yield return new FilterPanelConfig {
                Name = SecurityStrings.SecurityActionFilterPanelTitle1,
                Controls = new List<FilterControlConfig> {                 
                    new FilterControlConfig {
                        ColumnName = "OperationId",
                        Width = 120,
                    },
                    new FilterControlConfig {
                        ColumnName = "Status",
                        Width = 120,
                        DefaultValue = 0,
                    },
                },
                LoadFunction = (parameters) => {
                    SecurityDomainContext domainContext = (SecurityDomainContext)this.DomainContext;
                    int status = parameters["Status"] == null ? -1 : Convert.ToInt32(parameters["Status"]);
                    var query = domainContext.GetSecurityActionByQuery(Convert.ToString(parameters["OperationId"]),
                      status);
                    return domainContext.Load<SecurityAction>(query);
                },
            };
        }

        public override IEnumerable<OperationConfig> GetOperationConfigs() {
            yield return new OperationConfig {
                Id = "Add",
                Name = SecurityStrings.SecurityActionOperationAdd,
                GroupName = SecurityStrings.SecurityActionOperationPanelTitle1,
                Image = "/Sunlight.Silverlight.Security.UI;component/Images/action-add.png",
                TemplateType = TemplateType.DataModifyingTemplate,
                Content = new SecurityActionDataOperation(),
            };
            yield return new OperationConfig {
                Id = "Edit",
                Name = SecurityStrings.SecurityActionOperationEdit,
                GroupName = SecurityStrings.SecurityActionOperationPanelTitle1,
                Image = "/Sunlight.Silverlight.Security.UI;component/Images/action-edit.png",
                SelectionType = OperationSelectionType.Single,
                TemplateType = TemplateType.DataModifyingTemplate,
                Content = new SecurityActionDataOperation(true),
            };
        }
    }
}
