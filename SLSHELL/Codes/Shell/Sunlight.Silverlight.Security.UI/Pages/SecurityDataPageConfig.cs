﻿using System.Collections.Generic;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Configs;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Pages.Security {
    public abstract class SecurityDataPageConfig : DataPageConfig {
        private static Dictionary<string, IEnumerable<KeyValueMapping>> keyValueItems;
        private SecurityDomainContext domainContext = new SecurityDomainContext();

        /// <summary>
        /// 获取Security系统的DomainContext
        /// </summary>
        public override DomainContext DomainContext {
            get {
                return this.domainContext;
            }
        }

        /// <summary>
        /// 获取KeyValue结构的参数
        /// </summary>
        /// <param name="name">参数名称</param>
        /// <returns>KeyValue结构的参数</returns>
        internal IEnumerable<KeyValueMapping> GetKeyValueItems(string name) {
            IEnumerable<KeyValueMapping> result;
            if(keyValueItems == null)
                keyValueItems = new Dictionary<string, IEnumerable<KeyValueMapping>>();
            if(!keyValueItems.TryGetValue(name, out result)) {
                result = this.domainContext.Load<KeyValueMapping>(this.domainContext.GetKeyValueMappingByNameQuery(name)).Entities;
                keyValueItems.Add(name, result);
            }
            return result;
        }
    }
}
