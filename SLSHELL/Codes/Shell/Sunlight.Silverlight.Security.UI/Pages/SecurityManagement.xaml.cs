﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Sunlight.Silverlight.Security.Controls;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.Pages {
    public partial class SecurityManagement : System.Windows.Controls.Page {
        private TextBlock txtPleaseSelect = new TextBlock {
            Text = "请选择一个角色以及页面界面以查看授权选项。",
        };
        private PageManagement pageManagement = new PageManagement();

        public SecurityManagement() {
            InitializeComponent();
        }

        // 当用户导航到此页面时执行。
        protected override void OnNavigatedTo(NavigationEventArgs e) {
            //
        }

        private void Page_Loaded(object sender, RoutedEventArgs e) {
            this.ccMain.Content = this.txtPleaseSelect;
            SecurityDomainContext domainContext = new SecurityDomainContext();
            domainContext.Load<Role>(domainContext.GetRolesQuery(), loadOp => {
                if(!loadOp.HasError) {
                    Utils.SetItemsControlByEntity<Role, RadComboBoxItem>(this.rcbRoles, loadOp.Entities.Where(r => !r.IsAdmin), onItemAdded: (role, item) => {
                        item.Content = role.Name;
                    });
                    if(this.rcbRoles.HasItems)
                        this.rcbRoles.SelectedIndex = 0;
                }
            }, null);
            domainContext.Load<Sunlight.Silverlight.Security.Web.Page>(domainContext.GetPageByQuery(null, null, 0, -1), loadOp => {
                if(!loadOp.HasError) {
                    var pages = loadOp.Entities;
                    //pages = pages.Where(p => !(p.ParentId == 0 && p.Type == 0 && p.PageId == "Security"));
                    Utils.SetItemsControlByEntity<Sunlight.Silverlight.Security.Web.Page, RadTreeViewItem>(this.rtvMain, pages, "Id", "ParentId", "Sequence", (page, item) => {
                        item.Header = page.Name;
                    });
                }
            }, null);
        }

        private void rtvMain_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var roleItem = this.rcbRoles.SelectedItem as RadComboBoxItem;
            if(roleItem == null) {
                this.ccMain.Content = this.txtPleaseSelect;
                return;
            }
            var pageItem = this.rtvMain.SelectedItem as RadTreeViewItem;
            if(pageItem == null) {
                this.ccMain.Content = this.txtPleaseSelect;
                return;
            }
            var role = roleItem.Tag as Role;
            if(role == null) {
                this.ccMain.Content = this.txtPleaseSelect;
                return;
            }
            var page = pageItem.Tag as Sunlight.Silverlight.Security.Web.Page;
            if(page == null) {
                this.ccMain.Content = this.txtPleaseSelect;
                return;
            }
            if(page.Type != 2) {
                this.ccMain.Content = this.txtPleaseSelect;
                return;
            }
            this.pageManagement.Page = page;
            this.pageManagement.Role = role;
            this.ccMain.Content = this.pageManagement;
        }
    }
}
