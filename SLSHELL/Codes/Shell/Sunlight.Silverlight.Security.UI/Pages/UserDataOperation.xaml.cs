﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Security;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.Security.Web.Resources;
using Sunlight.Silverlight.ValueConverters;

namespace Sunlight.Silverlight.Pages.Security {
    public partial class UserDataOperation : OperationControl {
        //本类用于执行用户密码验证逻辑
        public sealed class PasswordValidator {
            private PasswordBox pbPassword;
            private string password;


            internal PasswordValidator(PasswordBox pbPassword) {
                this.pbPassword = pbPassword;
            }

            public string Password {
                get {
                    return this.password;
                }
                set {
                    //如果绑定进来的数据和pbPassword控件中的数据不一致，则抛出异常，使得数据验证失败
                    if(!string.IsNullOrEmpty(pbPassword.Password) && pbPassword.Password != value)
                        throw new Exception(SecurityStrings.UserValidationErrorPassword1);
                    this.password = value;
                }
            }
        }

        private bool initialized;
        //使用DataEditingManager类来统一管理DomainContext的调用
        private DataEditingManager<SecurityDomainContext, User> dataManager;
        //用于存储与当前用户相关联的组织集合
        private IEnumerable<Organization> organizations;
        private IEnumerable<Organization> bkorganizations;


        //该方法用来统一地设置显示组织树控件的按钮
        private void SetOrganizationButtonEnabled(bool isEnabled) {
            this.rddbOrganizations.IsEnabled = isEnabled;
            if(isEnabled) {
                this.spnOrganization.Visibility = Visibility.Collapsed;
                this.imgOrganization.Visibility = Visibility.Visible;
            } else {
                this.spnOrganization.Visibility = Visibility.Visible;
                this.imgOrganization.Visibility = Visibility.Collapsed;
            }
        }

        //用于初始化当前页面，将一次性的工作放到这里
        private void Initialize() {
            if(this.initialized)
                return;
            this.initialized = true;
            //设置DataEditingManager类的DomainContext为DataPage所使用的DomainContext
            this.dataManager.SetEditingDomainContext(this.DataPage.PageConfig.DomainContext as SecurityDomainContext);
            //将当前页面的DataContext属性绑定到DataEditingManager对象的Entity属性上
            this.SetBinding(UserControl.DataContextProperty, new Binding("Entity") {
                Source = this.dataManager,
                Mode = BindingMode.OneWay,
            });
            //设置权限文本的数据绑定
            this.txtOrganizations.SetBinding(TextBlock.TextProperty, new Binding("SelectedOrganizations") {
                Source = this.otvUser,
                Mode = BindingMode.OneWay,
                Converter = new OrganizationsConverter(),
            });
            //设置控件相关的数据绑定
            var buttonBinding = new Binding("IsSubmitting") {
                Source = this.dataManager.DomainContext,
                Converter = new NegateBooleanConverter(),
            };
            this.btnCommit.SetBinding(Button.IsEnabledProperty, buttonBinding);
            this.btnReset.SetBinding(Button.IsEnabledProperty, buttonBinding);
            this.btnCancel.SetBinding(Button.IsEnabledProperty, buttonBinding);
            this.pbComfirmPassword.SetBinding(PasswordBox.PasswordProperty, new Binding("Password") {
                Source = new PasswordValidator(this.pbPassword),
                Mode = BindingMode.TwoWay,
                ValidatesOnExceptions = true,
            });
            //获取ComboBox的数据源
            this.rcbSex.ItemsSource = (this.DataPage.PageConfig as SecurityDataPageConfig).GetKeyValueItems("UserSex");
            //设定密码输入控件的可见性
            this.pbPassword.Visibility = this.txtPassword.Visibility = this.pbComfirmPassword.Visibility = this.txtComfirmPassword.Visibility = this.dataManager.Mode == DataEditingMode.Add ? Visibility.Visible : Visibility.Collapsed;
        }

        //Loaded事件会在本窗体每次显示时都触发一次，要注意判断代码是否应该被重复执行
        private void UserOperationAdd_Loaded(object sender, RoutedEventArgs e) {
            //调用初始化方法，该方法自行处理只执行一次
            this.Initialize();
            if(this.dataManager.Mode == DataEditingMode.Add) {
                this.dataManager.Reject();
                this.pbComfirmPassword.Password = this.pbPassword.Password = "";
            } else if(this.dataManager.Mode == DataEditingMode.Edit) {
                //如果当前页面处于编辑模式，并且当前绑定的数据对象和传入的数据对象不是同一个
                //&& !object.ReferenceEquals(this.dataManager.Entity, this.Parameters[0])
                //获取参数，设置数据源
                this.dataManager.Entity = this.Parameters.FirstOrDefault() as User;
                //设置组织树按钮不可用
                this.SetOrganizationButtonEnabled(false);
                //清除组织树上的选择项
                this.otvUser.ClearSelectedOrganizations();
                this.organizations = null;
                //获取用户所关联的组织
                this.dataManager.Entity.GetOrganizations(this.dataManager.DomainContext, (success, organizations) => {
                    if(success)
                        this.otvUser.SelectedOrganizations = bkorganizations = this.organizations = organizations;
                    //设置组织树按钮可用
                    this.SetOrganizationButtonEnabled(true);
                });
                //} else if(this.dataManager.Mode == DataEditingMode.Edit && object.ReferenceEquals(this.dataManager.Entity, this.Parameters[0])) {
                //如果当前页面处于编辑模式，并且当前绑定的数据对象和传入的数据对象不是同一个

                //获取参数，设置数据源
                //this.dataManager.Entity = this.Parameters.FirstOrDefault() as User;
                ////设置组织树按钮不可用
                //this.SetOrganizationButtonEnabled(false);
                ////清除组织树上的选择项
                //this.otvUser.ClearSelectedOrganizations();
                //this.organizations = null;
                ////获取用户所关联的组织
                //this.dataManager.Entity.GetOrganizations(this.dataManager.DomainContext, (success, organizations) => {
                //    if(success)
                //        this.otvUser.SelectedOrganizations = bkorganizations = this.organizations = organizations;
                //    //设置组织树按钮可用
                //    this.SetOrganizationButtonEnabled(true);
                //});
            }
        }

        private void btnCommit_Click(object sender, RoutedEventArgs e) {
            User user = this.dataManager.Entity;
            if(this.dataManager.Mode == DataEditingMode.Add) {  //如果当前页面处于新增模式
                //向服务端提交数据
                this.dataManager.Submit(submitOp => {
                    if(!submitOp.HasError) {    //如果成功提交
                        if(this.otvUser.SelectedOrganizations.Count() > 0) {    //如果组织树控件存在选择项
                            //获取刚刚提交的用户对象，该对象的Id已被替换为数据库自动生成的Id
                            user = (User)submitOp.ChangeSet.AddedEntities[0];
                            //设置用户密码
                            this.dataManager.DomainContext.InitializePassword(user.Id, SharedUtils.HashPassword(this.pbPassword.Password));
                            //设置用户相关联的组织
                            user.SetOrganizations(this.dataManager.DomainContext, this.otvUser.SelectedOrganizations, success => {
                                if(success)
                                    //设置成功的话，再次提交用户的数据
                                    this.dataManager.Submit(submitOp1 => {
                                        if(!submitOp1.HasError)
                                            this.DataPage.ShowMainUI();

                                        bkorganizations = this.otvUser.SelectedOrganizations;
                                    });
                            });
                        } else
                            this.DataPage.ShowMainUI();
                    }
                }, false);
            } else if(this.dataManager.Mode == DataEditingMode.Edit) {  //如果当前页面处于编辑模式
                //设置用户相关联的组织
                user.SetOrganizations(this.dataManager.DomainContext, this.otvUser.SelectedOrganizations, success => {
                    if(success) {
                        //设置成功的话，提交用户的数据
                        this.dataManager.Submit(submitOp => {
                            if(!submitOp.HasError) {
                                bkorganizations = this.organizations = this.otvUser.SelectedOrganizations;
                                this.DataPage.ShowMainUI();
                            }
                        });
                    }
                });
            }
        }

        private void btnReset_Click(object sender, RoutedEventArgs e) {
            this.dataManager.Reject();
            this.otvUser.ClearSelectedOrganizations();
            if(this.dataManager.Mode == DataEditingMode.Add)
                this.pbPassword.Password = this.pbComfirmPassword.Password = "";
            else if(this.dataManager.Mode == DataEditingMode.Edit) {
                this.otvUser.SelectedOrganizations = bkorganizations;
                this.pbPassword.Password = this.pbComfirmPassword.Password = "";
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e) {
            this.dataManager.Reject();
            if(this.dataManager.Mode == DataEditingMode.Add) {
                this.otvUser.ClearSelectedOrganizations();
            } else
                this.otvUser.SelectedOrganizations = bkorganizations;
            this.DataPage.ShowMainUI();
        }

        public UserDataOperation()
            : this(false) {
        }

        public UserDataOperation(bool isEditing) {
            InitializeComponent();
            //实例化DataEditingManager对象
            this.dataManager = new DataEditingManager<SecurityDomainContext, User> {
                Mode = isEditing ? DataEditingMode.Edit : DataEditingMode.Add,
            };
        }
    }
}
