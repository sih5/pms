using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Pages.Security {
    public partial class UserOperationBindRole : OperationControl {
        private DataEditingManager<SecurityDomainContext, User> dataManager;
        private IEnumerable<Role> roles;

        public UserOperationBindRole() {
            InitializeComponent();
            this.dataManager = new DataEditingManager<SecurityDomainContext, User> {
                Mode = DataEditingMode.Edit,
            };
        }

        private void Initialize() {
            this.dataManager.SetEditingDomainContext(this.DataPage.PageConfig.DomainContext as SecurityDomainContext);
            this.SetBinding(UserControl.DataContextProperty, new Binding("Entity") {
                Source = this.dataManager,
                Mode = BindingMode.OneWay,
            });
        }

        private void btnCommit_Click(object sender, RoutedEventArgs e) {
            this.dataManager.Entity = this.Parameters.FirstOrDefault() as User;
            User user = this.dataManager.Entity;

            List<Role> SelectedRoles = new List<Role>();
            foreach(Role item in this.lbRole.SelectedItems)
                SelectedRoles.Add(item);
            this.roles = SelectedRoles.AsEnumerable<Role>();

            user.SetRoles(this.dataManager.DomainContext, this.roles, success => {
                if(success)
                    this.dataManager.Submit(submitOp => {
                        if(!submitOp.HasError)
                            this.DataPage.ShowMainUI();
                    });
            });
        }

        private void btnReset_Click(object sender, RoutedEventArgs e) {
            this.dataManager.Reject();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e) {
            this.dataManager.Reject();
            this.DataPage.ShowMainUI();
        }

        private void UserOperationBindRole_Loaded(object sender, RoutedEventArgs e) {
            this.Initialize();
            if(!object.ReferenceEquals(this.dataManager.Entity, this.Parameters[0])) {
                User user = this.Parameters.FirstOrDefault() as User;
                Binding userBinding = new Binding("Name");
                userBinding.Source = user;
                userBinding.Mode = BindingMode.OneWay;
                this.tbUser.SetBinding(TextBlock.TextProperty, userBinding);
                this.lbRole.ItemsSource = this.dataManager.DomainContext.Load<Role>(this.dataManager.DomainContext.GetRolesQuery()).Entities;
            }
        }
    }
}
