﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Configs;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.Security.Web.Resources;

namespace Sunlight.Silverlight.Pages.Security {
    public class UserPageConfig : SecurityDataPageConfig {
        public override string Title {
            get {
                return SecurityStrings.UserPageTitle;
            }
        }

        public override Type EntityType {
            get {
                return typeof(User);
            }
        }

        public override IEnumerable<GridColumnConfig> GetGridColumnConfigs() {
            yield return new GridColumnConfig {
                ColumnName = "Id",
                GridHeaderText = SecurityStrings.CommonColumnId,
                WidthUnit = 2,
                IsReadOnly = true,
            };
            yield return new GridColumnConfig {
                ColumnName = "LoginId",
                GridHeaderText = SecurityStrings.UserColumnLoginId,
                WidthUnit = 3,
            };
            yield return new GridColumnConfig {
                ColumnName = "Name",
                GridHeaderText = SecurityStrings.UserColumnName,
                WidthUnit = 3,
            };
            yield return new GridColumnConfig {
                ColumnName = "Email",
                GridHeaderText = SecurityStrings.UserColumnEmail,
                WidthUnit = 4,
            };
            yield return new GridColumnConfig {
                ColumnName = "Number",
                GridHeaderText = SecurityStrings.UserColumnNumber,
                WidthUnit = 3,
            };
            yield return new GridColumnConfig {
                ColumnName = "Sex",
                GridHeaderText = SecurityStrings.UserColumnSex,
                WidthUnit = 2,
                KeyValueItems = GetKeyValueItems("UserSex"),
            };
            yield return new GridColumnConfig {
                ColumnName = "Status",
                GridHeaderText = SecurityStrings.CommonColumnStatus,
                WidthUnit = 2,
                KeyValueItems = GetKeyValueItems("User"),
            };
        }

        public override IEnumerable<FilterPanelConfig> GetFilterPanelConfigs() {
            yield return new FilterPanelConfig {
                Name = SecurityStrings.UserFilterPanelTitle1,
                Controls = new List<FilterControlConfig> {
                    new FilterControlConfig{
                        ColumnName = "LoginId",
                        Width = 100,
                    },
                    new FilterControlConfig {
                        ColumnName = "Name",
                    },
                    new FilterControlConfig {
                        ColumnName = "Email",
                        Width = 130,
                    },
                    new FilterControlConfig {
                        ColumnName = "Number",
                    },
                    new FilterControlConfig {
                        ColumnName = "Status",
                        Width = 130,
                        DefaultValue = 0,
                    },
                    new FilterControlConfig {
                        ColumnName = "Sex",                      
                    },
                },
                LoadFunction = (parameters) => {
                    SecurityDomainContext domainContext = (SecurityDomainContext)this.DomainContext;
                    int status = parameters["Status"] == null ? -1 : Convert.ToInt32(parameters["Status"]);
                    int sex = parameters["Sex"] == null ? -1 : Convert.ToInt32(parameters["Sex"]);
                    var query = domainContext.GetUserByQuery(Convert.ToString(parameters["LoginId"]), Convert.ToString(parameters["Name"]), Convert.ToString(parameters["Email"]), Convert.ToString(parameters["Number"]), status, sex);
                    return domainContext.Load<User>(query);
                },
            };
        }

        public override IEnumerable<OperationConfig> GetOperationConfigs() {
            yield return new OperationConfig {
                Id = "Add",
                Name = SecurityStrings.UserOperationAdd,
                GroupName = SecurityStrings.UserOperationPanelTitle1,
                Image = "/Sunlight.Silverlight.Security.UI;component/Images/user-add.png",
                TemplateType = TemplateType.DataModifyingTemplate,
                Content = new UserDataOperation(),
            };
            yield return new OperationConfig {
                Id = "Edit",
                Name = SecurityStrings.UserOperationEdit,
                GroupName = SecurityStrings.UserOperationPanelTitle1,
                Image = "/Sunlight.Silverlight.Security.UI;component/Images/user-edit.png",
                SelectionType = OperationSelectionType.Single,
                TemplateType = TemplateType.DataModifyingTemplate,
                Content = new UserDataOperation(true),
            };
            yield return new OperationConfig {
                Id = "Bind",
                Name = "角色授权",
                GroupName = "角色授权",
                Image = "/Sunlight.Silverlight.Security.UI;component/Images/user-bind-role.png",
                SelectionType = OperationSelectionType.Single,
                TemplateType = TemplateType.DataModifyingTemplate,
                Content = new UserOperationBindRole(),
            };
        }
    }
}
