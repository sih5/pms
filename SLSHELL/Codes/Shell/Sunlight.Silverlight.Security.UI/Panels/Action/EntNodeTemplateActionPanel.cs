﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;

namespace Sunlight.Silverlight.Security.Panels.Action {
    public class EntNodeTemplateActionPanel : SecurityActionPanelBase {
        public EntNodeTemplateActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Security",
                Title = SecurityUIStrings.ActionPanel_Title_EntNodeTemplate,
                ActionItems = new[] {
                    new ActionItem {
                        Title = SecurityUIStrings.Action_Title_BindingEnterprise,
                        UniqueId = "Binding",
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/Add.png", UriKind.Relative),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
