﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using System;

namespace Sunlight.Silverlight.Security.Panels.Action {
    public class EnterpriseActionPanel : SecurityActionPanelBase {
        public EnterpriseActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Security",
                Title = SecurityUIStrings.ActionPanel_Title_Enterprise,
                ActionItems = new[] {
                    new ActionItem {
                        Title = SecurityUIStrings.Action_Title_Freeze,
                        UniqueId = "Freeze",
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/Freeze.png", UriKind.Relative),
                        CanExecute = false
                    }, new ActionItem {
                        Title = SecurityUIStrings.Action_Title_Recover,
                        UniqueId = "Recover",
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/Restore.png", UriKind.Relative),
                        CanExecute = false
                    }, new ActionItem {
                        Title = SecurityUIStrings.Action_Title_Validation,
                        UniqueId = "Validation",
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/Validation.png", UriKind.Relative),
                        CanExecute = false
                    }, new ActionItem {
                        Title = SecurityUIStrings.Action_Title_ChangeAdminPassword,
                        UniqueId = "ModifyAdminPassword",
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/ModifyAdminPassword.png", UriKind.Relative),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
