﻿
using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;

namespace Sunlight.Silverlight.Security.Panels.Action {
    public class OrganizationActionPanel : SecurityActionPanelBase {
        public OrganizationActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Security",
                Title = SecurityUIStrings.ActionPanel_Title_Organization,
                ActionItems = new[] {
                    new ActionItem {
                        Title = SecurityUIStrings.Action_Title_Reorder,
                        UniqueId = "Reorder",
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/Default.png", UriKind.Relative),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
