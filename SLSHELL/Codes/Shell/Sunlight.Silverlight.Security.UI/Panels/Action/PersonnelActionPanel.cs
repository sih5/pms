﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;

namespace Sunlight.Silverlight.Security.Panels.Action {
    public class PersonnelActionPanel : SecurityActionPanelBase {
        public PersonnelActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Security",
                Title = SecurityUIStrings.ActionPanel_Title_Personnel,
                ActionItems = new[] {
                    new ActionItem {
                        Title = SecurityUIStrings.Action_Title_Freeze,
                        UniqueId = "Freeze",
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/Freeze.png", UriKind.Relative),
                        CanExecute = false
                    }, new ActionItem {
                        Title = SecurityUIStrings.Action_Title_Recover,
                        UniqueId = "Recover",
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/Restore.png", UriKind.Relative),
                        CanExecute = false
                    }, new ActionItem {
                        Title = SecurityUIStrings.Action_Title_ChangePassword,
                        UniqueId = "ModifyPassword",
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/ModifyPassword.png", UriKind.Relative),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
