﻿
using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;

namespace Sunlight.Silverlight.Security.Panels.Action {
    public class RoleGroupActionPanel : SecurityActionPanelBase {
        public RoleGroupActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "RoleGroup",
                Title = SecurityUIStrings.ActionPanel_Title_RoleGroup,
                ActionItems = new[] {
                   new ActionItem {
                        Title = SecurityUIStrings.Action_Title_BatchAdd,
                        UniqueId = "BatchAddPermissions",
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/BatchAddPermissions.png", UriKind.Relative),
                        CanExecute = false
                    },  new ActionItem {
                        Title = SecurityUIStrings.Action_Title_BatchCancel,
                        UniqueId = "BatchCancelPermissions",
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/BatchCancelPermissions.png", UriKind.Relative),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
