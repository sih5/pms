﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;

namespace Sunlight.Silverlight.Security.Panels.Action {
    public class SecurityAddEditFreezeRecover : SecurityActionPanelBase {
        public SecurityAddEditFreezeRecover() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Security",
                Title = SecurityUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        Title = SecurityUIStrings.Action_Title_Add,
                        UniqueId = CommonActionKeys.ADD,
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/Add.png", UriKind.Relative)
                    }, new ActionItem {
                        Title = SecurityUIStrings.Action_Title_Edit,
                        UniqueId = CommonActionKeys.EDIT,
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/Edit.png", UriKind.Relative),
                        CanExecute = false
                    }, new ActionItem {
                        Title = SecurityUIStrings.Action_Title_Freeze,
                        UniqueId = CommonActionKeys.FREEZE,
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/Freeze.png", UriKind.Relative),
                        CanExecute = false
                    }, new ActionItem {
                        Title = SecurityUIStrings.Action_Title_Recover,
                        UniqueId = CommonActionKeys.RECOVER,
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/Restore.png", UriKind.Relative),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
