﻿
using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;

namespace Sunlight.Silverlight.Security.Panels.Action {
    public class SecurityExport : SecurityActionPanelBase {
        public SecurityExport() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Security",
                Title = SecurityUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        Title = SecurityUIStrings.Action_Title_Export,
                        UniqueId = "Export",
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/Export.png", UriKind.Relative)
                    }
                }
            };
        }
    }
}
