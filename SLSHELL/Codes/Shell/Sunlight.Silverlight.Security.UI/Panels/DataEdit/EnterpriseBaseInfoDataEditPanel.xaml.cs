﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.Panels.DataEdit {
    public partial class EnterpriseBaseInfoDataEditPanel {
        private readonly SecurityDomainContext domainContext = new SecurityDomainContext();
        private readonly ObservableCollection<KeyValuePair> kvCategories = new ObservableCollection<KeyValuePair>();

        public object KvCategories {
            get {
                return this.kvCategories;
            }
        }

        public EnterpriseBaseInfoDataEditPanel() {
            InitializeComponent();
            this.domainContext.Load(this.domainContext.GetEnterpriseCategoriesQuery().Where(c => c.Status == (int)SecurityCommonStatus.有效), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var category in loadOp.Entities) {
                    this.kvCategories.Add(new KeyValuePair {
                        Key = category.Id,
                        Value = category.Name
                    });
                }
            }, null);
        }
    }
}
