﻿using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.Panels.Detail {
    public class OrganizationPersonnelDetailPanel : RadListBox, IDetailPanel {
        private SecurityDomainContext domainContext;

        public SecurityDomainContext DomainContext {
            get {
                return domainContext ?? (domainContext = new SecurityDomainContext());
            }
            set {
                this.domainContext = value;
            }
        }

        public OrganizationPersonnelDetailPanel() {
            this.DataContextChanged += this.OrganizationPersonnelDetailPanel_DataContextChanged;
        }

        private void OrganizationPersonnelDetailPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var organization = this.DataContext as Organization;
            if(organization == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetOrganizationPersonnelsQuery().Where(u => u.OrganizationId == organization.Id), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.ItemsSource = loadOp.Entities;
            }, null);
        }

        public System.Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return null;
            }
        }
    }
}
