﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.Panels.Detail {
    public partial class PersonnelDetailDetailPanel {
        private readonly string[] kvNames = {
            "User_Status"
        };

        private void LoadEntityById(int id) {
            var securityDomainContext = new SecurityDomainContext();
            securityDomainContext.Load(securityDomainContext.GetPersonnelsWithOrganizationAndRoleQuery().Where(p => p.Id == id), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.DataContext = entity;
            }, null);
        }

        public void SetObjectById(int id) {
            if(id > 0)
                this.LoadEntityById(id);
        }

        public PersonnelDetailDetailPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
