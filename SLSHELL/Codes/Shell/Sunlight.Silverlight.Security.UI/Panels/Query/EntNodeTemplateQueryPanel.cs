﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.Panels.Query {
    public class EntNodeTemplateQueryPanel : SecurityQueryPanelBase {
        private readonly string[] kvNames = {
            "SecurityCommon_Status"
        };

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Security",
                    Title = SecurityUIStrings.QueryPanel_Title_EntNodeTemplate,
                    EntityType = typeof(EntNodeTemplate),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code"
                        }, new QueryItem {
                            ColumnName = "Name",
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)SecurityCommonStatus.有效
                        }
                    }
                }
            };
        }

        public EntNodeTemplateQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
