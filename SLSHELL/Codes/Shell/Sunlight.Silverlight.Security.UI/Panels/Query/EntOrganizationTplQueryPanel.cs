﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.Panels.Query {
    public class EntOrganizationTplQueryPanel : SecurityQueryPanelBase {
        private readonly string[] kvNames = {
            "SecurityCommon_Status"
        };

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Security",
                    Title = SecurityUIStrings.QueryPanel_Title_EntOrganizationTpl,
                    EntityType = typeof(EntOrganizationTpl),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code"
                        },new QueryItem {
                            ColumnName = "Name"
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)SecurityCommonStatus.有效
                        }
                    }
                }
            };
        }

        public EntOrganizationTplQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
