﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.Panels.Query {
    public class EnterpriseQueryPanel : SecurityQueryPanelBase {
        private readonly string[] kvNames = {
            "Enterprise_Status"
        };

        private void Initialize() {
            var kvEnterpriseCategory = new ObservableCollection<KeyValuePair>();
            var securityDomainContext = new SecurityDomainContext();

            securityDomainContext.Load(securityDomainContext.GetEnterpriseCategoriesQuery().Where(e => e.Status == (int)SecurityCommonStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var enterpriseCategory in loadOp.Entities)
                    kvEnterpriseCategory.Add(new KeyValuePair {
                        Key = enterpriseCategory.Id,
                        Value = enterpriseCategory.Name
                    });
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Security",
                        Title = SecurityUIStrings.QueryPanel_Title_Enterprise,
                        EntityType = typeof(Enterprise),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "Code"
                            }, new QueryItem {
                                ColumnName = "Name",
                            }, new KeyValuesQueryItem {
                                ColumnName = "EnterpriseCategoryId",
                                Title = SecurityUIStrings.QueryPanel_QueryItem_Title_Category,
                                KeyValueItems = kvEnterpriseCategory
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                                DefaultValue = (int)SecurityEnterpriseStatus.生效
                            }
                        }
                    }
                };
            }, null);
        }

        public EnterpriseQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
