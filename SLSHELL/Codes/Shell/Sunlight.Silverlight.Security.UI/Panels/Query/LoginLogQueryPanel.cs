﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;


namespace Sunlight.Silverlight.Security.Panels.Query {
    public class LoginLogQueryPanel : SecurityQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvEnterpriseCategory = new ObservableCollection<KeyValuePair>();
        private void Initialize() {

            //EnterpriseCategory
            var securityDomainContext = new SecurityDomainContext();

            securityDomainContext.Load(securityDomainContext.GetEnterpriseCategoriesQuery().Where(e => e.Status == (int)SecurityCommonStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var enterpriseCategory in loadOp.Entities)
                    kvEnterpriseCategory.Add(new KeyValuePair {
                        Key = enterpriseCategory.Id,
                        Value = enterpriseCategory.Name
                    });
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Security",
                        Title = SecurityUIStrings.QueryPanel_Title_LoginLog,
                        EntityType = typeof(LoginLog),
                        QueryItems = new QueryItem[] {
                            new CustomQueryItem {
                                ColumnName = "Personnel.Enterprise.Code",
                                DataType = typeof(string),
                                Title = SecurityUIStrings.QueryPanel_QueryItem_Title_EnterpriseCode,
                            },
                            new CustomQueryItem {
                                ColumnName = "Personnel.Enterprise.Name",
                                DataType = typeof(string),
                                Title = SecurityUIStrings.QueryPanel_QueryItem_Title_EnterpriseName,
                            },
                            new KeyValuesQueryItem {
                                ColumnName = "Personnel.Enterprise.EnterpriseCategoryId",
                                Title = SecurityUIStrings.QueryPanel_QueryItem_Title_Category,
                                 KeyValueItems = kvEnterpriseCategory
                            },
                            new CustomQueryItem {
                                ColumnName = "Personnel.LoginId",
                                DataType = typeof(string),
                                Title = SecurityUIStrings.QueryPanel_QueryItem_Title_PersonnelLoginId
                            },
                            new CustomQueryItem {
                                ColumnName = "Personnel.Name",
                                DataType = typeof(string),
                                Title = SecurityUIStrings.QueryPanel_QueryItem_Title_PersonnelName
                            },
                            new DateTimeRangeQueryItem {
                                ColumnName = "LoginTime",
                            }
                        }
                    }
                };
            }, null);
        }

        public LoginLogQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
    }
}
