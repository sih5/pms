﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.Panels.Query {
    public class OrganizationGroupQueryPanel : SecurityQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvEnterprises;
        private readonly string[] kvNames = {
            "SecurityCommon_Status"
        };
        private ObservableCollection<KeyValuePair> kvEnterpriseTypes;
        private void Initialize() {
            var securityDomainContext = new SecurityDomainContext();
            securityDomainContext.Load(securityDomainContext.GetEnterprisesQuery().Where(e => e.Status == (int)SecurityEnterpriseStatus.生效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvEnterprises = new ObservableCollection<KeyValuePair>();
                foreach(var enterprise in loadOp.Entities) {
                    this.kvEnterprises.Add(new KeyValuePair {
                        Key = enterprise.Id,
                        Value = enterprise.Name
                    });
                }
                securityDomainContext.Load(securityDomainContext.GetEnterpriseCategoriesQuery().Where(x => x.Status == (int)SecurityCommonStatus.有效), LoadBehavior.RefreshCurrent, load => {
                    if(load.HasError)
                        return;
                    this.kvEnterpriseTypes = new ObservableCollection<KeyValuePair>();
                    foreach(var enterpriseCategory in load.Entities) {
                        this.kvEnterpriseTypes.Add(new KeyValuePair {
                            Key = enterpriseCategory.Id,
                            Value = enterpriseCategory.Name
                        });
                    }
                    this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Security",
                        Title = SecurityUIStrings.QueryPanel_Title_Role,
                        EntityType = typeof(Role),
                        QueryItems = new QueryItem[] {
                            new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                                DefaultValue = (int)SecurityCommonStatus.有效
                            },new CustomQueryItem {
                                Title = SecurityUIStrings.QueryItem_Title_EnterpriseName,
                                ColumnName="Enterprise.Name",
                                DefaultValue =  BaseApp.Current.CurrentUserData.EnterpriseName,
                                IsCaseSensitive=true,
                                DataType=typeof(string)
                            },new KeyValuesQueryItem {
                                Title = SecurityUIStrings.QueryItem_Title_EnterpriseCategory,
                                ColumnName = "Enterprise.Category",
                                KeyValueItems = kvEnterpriseTypes
                            }
                        }
                    }
                };
                }, null);
            }, null);
        }

        public OrganizationGroupQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
