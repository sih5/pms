﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.Panels.Query {
    public class RoleQueryPanel : SecurityQueryPanelBase {
        private readonly string[] kvNames = {
            "Role_Status"
        };

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Security",
                    Title = SecurityUIStrings.QueryPanel_Title_Role,
                    EntityType = typeof(Role),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Name"
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)SecurityRoleStatus.有效
                        }
                    }
                }
            };
        }

        public RoleQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
