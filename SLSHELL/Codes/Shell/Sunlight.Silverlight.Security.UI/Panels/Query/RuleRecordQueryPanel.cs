﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.Panels.Query {
    public class RuleRecordQueryPanel : SecurityQueryPanelBase {
        private readonly string[] kvNames = {
            "RuleRecord_Status"
        };

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Security",
                    Title = SecurityUIStrings.QueryPanel_Title_RuleRecord,
                    EntityType = typeof(VirtualRuleRecord),
                    QueryItems = new [] {
                        new CustomQueryItem {
                            ColumnName = "EnterpriseCode",
                            DataType = typeof(string),
                            Title = SecurityUIStrings.QueryPanel_QueryItem_Title_EnterpriseCode,
                        }, new CustomQueryItem {
                            ColumnName = "EnterpriseName",
                            DataType = typeof(string),
                            Title = SecurityUIStrings.QueryPanel_QueryItem_Title_EnterpriseName,
                        }, new CustomQueryItem {
                            ColumnName = "RoleName",
                            DataType = typeof(string),
                            Title = SecurityUIStrings.QueryPanel_QueryItem_Title_RoleName,
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            Title = SecurityUIStrings.QueryPanel_QueryItem_Title_Status,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new CustomQueryItem {
                            ColumnName = "NodeName",
                            DataType = typeof(string),
                            Title = SecurityUIStrings.QueryPanel_QueryItem_Title_NodeName
                        }, new CustomQueryItem {
                            ColumnName = "ActionName",
                            DataType = typeof(string),
                            Title = SecurityUIStrings.QueryPanel_QueryItem_Title_ActionName
                        }, new QueryItem {
                            ColumnName = "CreatorName",
                            Title = SecurityUIStrings.QueryPanel_QueryItem_Title_CreatorName
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = SecurityUIStrings.QueryPanel_QueryItem_Title_CreateTime,
                        }
                    }
                }
            };
        }

        public RuleRecordQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
