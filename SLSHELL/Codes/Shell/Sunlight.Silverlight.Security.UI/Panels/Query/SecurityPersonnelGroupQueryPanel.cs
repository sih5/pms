﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.Panels.Query {
    public class SecurityPersonnelGroupQueryPanel : SecurityQueryPanelBase {
        private readonly string[] kvNames = {
            "User_Status"
        };
        private ObservableCollection<KeyValuePair> kvEnterpriseTypes;
        private void Initialize() {
            var securityDomainContext = new SecurityDomainContext();
            securityDomainContext.Load(securityDomainContext.GetEnterprisesQuery().Where(e => e.Status == (int)SecurityEnterpriseStatus.生效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var kvEnterprises = new ObservableCollection<KeyValuePair>();
                foreach(var enterprise in loadOp.Entities)
                    kvEnterprises.Add(new KeyValuePair {
                        Key = enterprise.Id,
                        Value = enterprise.Name
                    });
                securityDomainContext.Load(securityDomainContext.GetEnterpriseCategoriesQuery().Where(x => x.Status == (int)SecurityCommonStatus.有效), LoadBehavior.RefreshCurrent, load => {
                    if(load.HasError)
                        return;
                    this.kvEnterpriseTypes = new ObservableCollection<KeyValuePair>();
                    foreach(var enterpriseCategory in load.Entities) {
                        this.kvEnterpriseTypes.Add(new KeyValuePair {
                            Key = enterpriseCategory.Id,
                            Value = enterpriseCategory.Name
                        });
                    }
                    this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Security",
                        Title = SecurityUIStrings.QueryPanel_Title_Personnel,
                        EntityType = typeof(Personnel),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "LoginId"
                            }, new QueryItem {
                                ColumnName = "Name"
                            }, new CustomQueryItem {
                                ColumnName = "Enterprise.Code",
                                DataType = typeof(string),
                                Title = SecurityUIStrings.QueryItem_Title_EnterpriseCode,
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                                DefaultValue = (int)SecurityUserStatus.有效,
                                Title = SecurityUIStrings.QueryItem_Title_PersonnelStatus
                            },new CustomQueryItem {
                                Title = SecurityUIStrings.QueryItem_Title_EnterpriseName,
                                ColumnName="Enterprise.Name",
                                DefaultValue =  BaseApp.Current.CurrentUserData.EnterpriseName,
                                IsCaseSensitive=true,
                                DataType=typeof(string)
                            }, new KeyValuesQueryItem {
                                Title = SecurityUIStrings.QueryItem_Title_EnterpriseCategory,
                                ColumnName = "Enterprise.Category",
                                KeyValueItems = kvEnterpriseTypes
                            }
                        }
                    }
                };
                }, null);
            }, null);
        }

        public SecurityPersonnelGroupQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
