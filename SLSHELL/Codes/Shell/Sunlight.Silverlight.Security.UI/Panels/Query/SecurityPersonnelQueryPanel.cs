﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.Panels.Query {
    public class SecurityPersonnelQueryPanel : SecurityQueryPanelBase {
        private readonly string[] kvNames = {
            "User_Status"
        };

        private void Initialize() {
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Security",
                        Title = SecurityUIStrings.QueryPanel_Title_Personnel,
                        EntityType = typeof(Personnel),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "LoginId"
                            }, new QueryItem {
                                ColumnName = "Name"
                            }, new QueryItem {
                                ColumnName = "CellNumber"
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                                DefaultValue = (int)SecurityUserStatus.有效
                            }
                        }
                    }
                };
        }

        public SecurityPersonnelQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
