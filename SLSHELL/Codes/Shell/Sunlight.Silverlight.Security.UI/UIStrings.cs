﻿using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Security {
    public class UIStrings : ModelBase {
        public void Refresh(params string[] propertyNames) {
            this.NotifyOfPropertiesChange(propertyNames.Length == 0 ? this.GetType().GetProperties().Select(p => p.Name).ToArray() : propertyNames);
        }

        public string Button_Title_Cancel {
            get {
                return Resources.SecurityUIStrings.Button_Title_Cancel;
            }
        }

        public string Button_Title_Confirm {
            get {
                return Resources.SecurityUIStrings.Button_Title_Confirm;
            }
        }

        public string CustomControl_Organization_CurrentOrganizationInfo {
            get {
                return Resources.SecurityUIStrings.CustomControl_Organization_CurrentOrganizationInfo;
            }
        }

        public string CustomControl_Organization_GridView_Users {
            get {
                return Resources.SecurityUIStrings.CustomControl_Organization_GridView_Users;
            }
        }

        public string CustomControl_Organization_OrganizationAbandonTitle {
            get {
                return Resources.SecurityUIStrings.CustomControl_Organization_OrganizationAbandonTitle;
            }
        }

        public string CustomControl_Organization_OrganizationChangeSequenceTitle {
            get {
                return Resources.SecurityUIStrings.CustomControl_Organization_OrganizationChangeSequenceTitle;
            }
        }

        public string CustomControl_Organization_OrganizationInfo {
            get {
                return Resources.SecurityUIStrings.CustomControl_Organization_OrganizationInfo;
            }
        }

        public string CustomControl_Organization_OrganizationMngTitle {
            get {
                return Resources.SecurityUIStrings.CustomControl_Organization_OrganizationMngTitle;
            }
        }

        public string CustomControl_Role_NodeItems {
            get {
                return Resources.SecurityUIStrings.CustomControl_Role_NodeItems;
            }
        }

        public string CustomControl_Role_RoleInfo {
            get {
                return Resources.SecurityUIStrings.CustomControl_Role_RoleInfo;
            }
        }

        public string CustomControl_Role_RoleMngTitle {
            get {
                return Resources.SecurityUIStrings.CustomControl_Role_RoleMngTitle;
            }
        }

        public string CustomControl_Role_UserItems {
            get {
                return Resources.SecurityUIStrings.CustomControl_Role_UserItems;
            }
        }

        public string CustomControl_User_OrganizationItems {
            get {
                return Resources.SecurityUIStrings.CustomControl_User_OrganizationItems;
            }
        }

        public string CustomControl_User_RoleItems {
            get {
                return Resources.SecurityUIStrings.CustomControl_User_RoleItems;
            }
        }

        public string CustomControl_User_UserInfo {
            get {
                return Resources.SecurityUIStrings.CustomControl_User_UserInfo;
            }
        }

        public string CustomControl_User_UserMngTitle {
            get {
                return Resources.SecurityUIStrings.CustomControl_User_UserMngTitle;
            }
        }

        public string CustomControl_User_UserModifyPasswordTitle {
            get {
                return Resources.SecurityUIStrings.CustomControl_User_UserModifyPasswordTitle;
            }
        }

        public string DataQueryView_Enterprise_Roles {
            get {
                return Resources.SecurityUIStrings.DataQueryView_Enterprise_Roles;
            }
        }

        public string DataEditPanel_RadButton_SelectPhoto {
            get {
                return Resources.SecurityUIStrings.DataEditPanel_RadButton_SelectPhoto;
            }
        }

        public string DataEditView_GroupTitle_Functions {
            get {
                return Resources.SecurityUIStrings.DataEditView_GroupTitle_Functions;
            }
        }

        public string DataEditView_GroupTitle_Personnels {
            get {
                return Resources.SecurityUIStrings.DataEditView_GroupTitle_Personnels;
            }
        }

        public string DataEditView_GroupTitle_Enterprises {
            get {
                return Resources.SecurityUIStrings.DataEditView_GroupTitle_Enterprises;
            }
        }

        public string CustomControl_PersonnelSelectPanel_Title_Tab_Current {
            get {
                return Resources.SecurityUIStrings.CustomControl_PersonnelSelectPanel_Title_Tab_Current;
            }
        }

        public string CustomControl_PersonnelSelectPanel_Title_Tab_Personnels {
            get {
                return Resources.SecurityUIStrings.CustomControl_PersonnelSelectPanel_Title_Tab_Personnels;
            }
        }

        public string CustomControl_PersonnelSelectPanel_Button_Query {
            get {
                return Resources.SecurityUIStrings.CustomControl_PersonnelSelectPanel_Button_Query;
            }
        }

        public string CustomControl_PersonnelSelectPanel_Filter_Name {
            get {
                return Resources.SecurityUIStrings.CustomControl_PersonnelSelectPanel_Filter_Name;
            }
        }

        public string DataQueryView_Enterprise_Organizations {
            get {
                return Resources.SecurityUIStrings.DataQueryView_Enterprise_Organizations;
            }
        }

        public string CustomControl_AuthorizeView_BtnAllNodes {
            get {
                return Resources.SecurityUIStrings.CustomControl_AuthorizeView_BtnAllNodes;
            }
        }

        public string CustomControl_AuthorizeView_BtnInvert {
            get {
                return Resources.SecurityUIStrings.CustomControl_AuthorizeView_BtnInvert;
            }
        }

        public string CustomControl_AuthorizeView_BtnSelectAll {
            get {
                return Resources.SecurityUIStrings.CustomControl_AuthorizeView_BtnSelectAll;
            }
        }

        public string CustomControl_AuthorizeView_BtnSelectNone {
            get {
                return Resources.SecurityUIStrings.CustomControl_AuthorizeView_BtnSelectNone;
            }
        }

        public string CustomControl_AuthorizeView_SearchTextBoxDefaultText {
            get {
                return Resources.SecurityUIStrings.CustomControl_AuthorizeView_SearchTextBoxDefaultText;
            }
        }

        public string DataEditView_GroupTitle_EntNodeTemplate {
            get {
                return Resources.SecurityUIStrings.DataEditView_GroupTitle_EntNodeTemplate;
            }
        }

        public string DataEditView_GroupTitle_EntOrganizationTpl {
            get {
                return Resources.SecurityUIStrings.DataEditView_GroupTitle_EntOrganizationTpl;
            }
        }

        public string DataEditView_GroupTitle_RoleTemplate {
            get {
                return Resources.SecurityUIStrings.DataEditView_GroupTitle_RoleTemplate;
            }
        }

        public string DataEditView_GroupTitle_Organization {
            get {
                return Resources.SecurityUIStrings.DataEditView_GroupTitle_Organization;
            }
        }

        public string DataEditView_GroupTitle_Personnel {
            get {
                return Resources.SecurityUIStrings.DataEditView_GroupTitle_Personnel;
            }
        }

        public string DataEditView_GroupTitle_Role {
            get {
                return Resources.SecurityUIStrings.DataEditView_GroupTitle_Role;
            }
        }

        public string WorkflowPage_Tip_Enterprise_EntNodeTemplate {
            get {
                return Resources.SecurityUIStrings.WorkflowPage_Tip_Enterprise_EntNodeTemplate;
            }
        }

        public string WorkflowPage_Tip_Enterprise_EntOrganizationTpl {
            get {
                return Resources.SecurityUIStrings.WorkflowPage_Tip_Enterprise_EntOrganizationTpl;
            }
        }

        public string WorkflowPage_Tip_Enterprise_RoleTemplate {
            get {
                return Resources.SecurityUIStrings.WorkflowPage_Tip_Enterprise_RoleTemplate;
            }
        }

        public string WorkflowPage_Title_Enterprise_BaseInfoEdit {
            get {
                return Resources.SecurityUIStrings.WorkflowPage_Title_Enterprise_BaseInfoEdit;
            }
        }

        public string WorkflowPage_Title_Enterprise_EntNodeTemplateSelect {
            get {
                return Resources.SecurityUIStrings.WorkflowPage_Title_Enterprise_EntNodeTemplateSelect;
            }
        }

        public string WorkflowPage_Title_Enterprise_EntNodeTemplateSelectBatch {
            get {
                return Resources.SecurityUIStrings.WorkflowPage_Title_Enterprise_EntNodeTemplateSelectBatch;
            }
        }

        public string WorkflowPage_Title_Enterprise_EntOrganizationTplSelect {
            get {
                return Resources.SecurityUIStrings.WorkflowPage_Title_Enterprise_EntOrganizationTplSelect;
            }
        }

        public string WorkflowPage_Title_Enterprise_EntOrganizationTplSelectBatch {
            get {
                return Resources.SecurityUIStrings.WorkflowPage_Title_Enterprise_EntOrganizationTplSelectBatch;
            }
        }

        public string WorkflowPage_Title_Enterprise_RoleTemplateSelect {
            get {
                return Resources.SecurityUIStrings.WorkflowPage_Title_Enterprise_RoleTemplateSelect;
            }
        }

        public string WorkflowPage_Title_Enterprise_RoleTemplateSelectBatch {
            get {
                return Resources.SecurityUIStrings.WorkflowPage_Title_Enterprise_RoleTemplateSelectBatch;
            }
        }

        public string DataEditPanel_Text_Usage {
            get {
                return Resources.SecurityUIStrings.DataEditPanel_Text_Usage;
            }
        }

        public string DataEditView_Personnel_SelectPhoto {
            get {
                return Resources.SecurityUIStrings.DataEditView_Personnel_SelectPhoto;
            }
        }

        public string CustomView_ChangePassword_RepeatPassword {
            get {
                return Resources.SecurityUIStrings.CustomView_ChangePassword_RepeatPassword;
            }
        }

        public string CustomView_ChangePassword_Password {
            get {
                return Resources.SecurityUIStrings.CustomView_ChangePassword_Password;
            }
        }

        public string CustomView_ChangePassword_PasswordHint {
            get {
                return Resources.SecurityUIStrings.CustomView_ChangePassword_PasswordHint;
            }
        }

        public string CustomView_ChangePassword_OldPassword {
            get {
                return Resources.SecurityUIStrings.CustomView_ChangePassword_OldPassword;
            }
        }

        public string CustomView_ChangePassword_HintTitle {
            get {
                return Resources.SecurityUIStrings.CustomView_ChangePassword_HintTitle;
            }
        }

        public string CustomView_ChangePassword_Title {
            get {
                return Resources.SecurityUIStrings.CustomView_ChangePassword_Title;
            }
        }

        public string CustomView_ChangePassword_ButtonText_Submit {
            get {
                return Resources.SecurityUIStrings.CustomView_ChangePassword_ButtonText_Submit;
            }
        }

        public string DataEditView_TextBlock_Item1 {
            get {
                return Resources.SecurityUIStrings.DataEditView_TextBlock_Item1;
            }
        }

        public string DataEditView_TextBlock_Item2 {
            get {
                return Resources.SecurityUIStrings.DataEditView_TextBlock_Item2;
            }
        }

        public string DataEditView_TextBlock_Notification {
            get {
                return Resources.SecurityUIStrings.DataEditView_TextBlock_Notification;
            }
        }

    }
}
