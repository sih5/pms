﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security {
    public sealed class TreeViewItemLineVisibilityConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var item = (RadTreeViewItem)value;
            switch(parameter as string) {
                case "Top":
                    return item.ParentItem == null ? Visibility.Collapsed : Visibility.Visible;
                case "Bottom":
                    return item.IsExpanded && item.HasItems ? Visibility.Visible : Visibility.Collapsed;
                case "Left":
                    return item.ParentItem == null || item.PreviousSiblingItem == null ? Visibility.Collapsed : Visibility.Visible;
                case "Right":
                    return item.ParentItem == null || item.NextSiblingItem == null ? Visibility.Collapsed : Visibility.Visible;
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotSupportedException();
        }
    }

    public sealed class AvatarUriConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var photo = value as string;
            return SecurityUtils.GetPersonnelPhotoUri(photo);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
