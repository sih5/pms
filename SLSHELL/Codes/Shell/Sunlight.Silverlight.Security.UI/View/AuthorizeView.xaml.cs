﻿using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Security.ViewModel;
using System;
using System.Windows;

namespace Sunlight.Silverlight.Security.View {
    /// <summary>
    /// 授权操作的界面
    /// </summary>
    public partial class AuthorizeView {
        public static readonly DependencyProperty IsReadOnlyProperty =
            DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(AuthorizeView), new PropertyMetadata(default(bool)));
        public bool IsReadOnly {
            get {
                return (bool)GetValue(IsReadOnlyProperty);
            }
            set {
                SetValue(IsReadOnlyProperty, value);
            }
        }
        private void MainSearchTextBox_SearchTextChanged(object sender, EventArgs e) {
            var searchTextBox = sender as SearchTextBox;
            if(searchTextBox == null)
                return;
            var systemMenuItem = searchTextBox.DataContext as SystemNodeItem;
            if(systemMenuItem == null)
                return;

            systemMenuItem.Search(searchTextBox.SearchText);
        }

        public AuthorizeView() {
            this.InitializeComponent();
        }
    }
}
