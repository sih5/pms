﻿
using System;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Security.ViewModel;

namespace Sunlight.Silverlight.Security.View {
    /// <summary>
    /// 批量授权操作的界面
    /// </summary>
    public partial class BatchAuthorizView {

        public BatchAuthorizView() {
            InitializeComponent();
        }
        private void MainSearchTextBox_SearchTextChanged(object sender, EventArgs e) {
            var searchTextBox = sender as SearchTextBox;
            if(searchTextBox == null)
                return;
            var systemMenuItem = searchTextBox.DataContext as SystemNodeItem;
            if(systemMenuItem == null)
                return;

            systemMenuItem.Search(searchTextBox.SearchText);
        }
    }
}
