﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Sunlight.Silverlight.Security.View.Custom {
    public class DataItemCollection : ObservableCollection<DataItem> {
        protected override void InsertItem(int index, DataItem item) {
            this.AdoptItem(item);
            base.InsertItem(index, item);
        }

        protected override void RemoveItem(int index) {
            this.DiscardItem(this[index]);
            base.RemoveItem(index);
        }

        protected override void SetItem(int index, DataItem item) {
            this.AdoptItem(item);
            base.SetItem(index, item);
        }

        protected override void ClearItems() {
            foreach(var item in this)
                this.DiscardItem(item);
            base.ClearItems();
        }

        private void AdoptItem(DataItem item) {
            item.SetOwner(this);
        }

        private void DiscardItem(DataItem item) {
            item.SetOwner(null);
        }

        public void Sort<TKey>(Func<DataItem, TKey> keySelector, System.ComponentModel.ListSortDirection direction) {
            switch(direction) {
                case System.ComponentModel.ListSortDirection.Ascending: {
                        ApplySort(Items.OrderBy(keySelector));
                        break;
                    }
                case System.ComponentModel.ListSortDirection.Descending: {
                        ApplySort(Items.OrderByDescending(keySelector));
                        break;
                    }
            }
        }

        public void Sort<TKey>(Func<DataItem, TKey> keySelector, IComparer<TKey> comparer) {
            ApplySort(Items.OrderBy(keySelector, comparer));
        }

        private void ApplySort(IEnumerable<DataItem> sortedItems) {
            var sortedItemsList = sortedItems.ToList();
            this.Clear();
            foreach(var item in sortedItemsList)
                this.Add(item);
        }

        public DataItemCollection Search(string searchText) {
            if(string.IsNullOrEmpty(searchText)) {
                return new DataItemCollection(this);
            }
            var filteredCollection = new DataItemCollection(this.Where(item => item.Text.Contains(searchText)));
            var idList = filteredCollection.Select(item => item.ParentId).Distinct().ToList();
            foreach(var initId in idList) {
                var id = initId;
                while(id > 0) {
                    var item = this.Single(v => v.Id == id);
                    if(filteredCollection.Contains(item))
                        break;
                    filteredCollection.Add(item);
                    id = item.ParentId;
                }
            }
            return filteredCollection;
        }

        public DataItemCollection() {
        }

        public DataItemCollection(IEnumerable<DataItem> collection) {
            foreach(var item in collection)
                this.Add(item);
        }
    }
}
