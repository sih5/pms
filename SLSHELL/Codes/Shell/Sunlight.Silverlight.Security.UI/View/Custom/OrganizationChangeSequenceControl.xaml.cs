﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.Custom {
    public partial class OrganizationChangeSequenceControl {
        public UserControl ParentPage;
        private ObservableCollection<Organization> organizations;

        public ObservableCollection<Organization> Organizations {
            get {
                return this.organizations;
            }
            set {
                this.organizations = value;
                this.OnPropertyChanged("Organizations");
            }
        }

        public OrganizationChangeSequenceControl() {
            this.InitializeComponent();
        }

        private void organizationTreeView_PreviewDragEnded(object sender, RadTreeViewDragEndedEventArgs e) {
            if(e.DraggedItems == null || !e.DraggedItems.Any() || ((RadTreeViewItem)e.DraggedItems[0]).ParentTreeView == null)
                return;

            var currentItem = (RadTreeViewItem)e.DraggedItems.First();
            var targetItem = e.TargetDropItem;

            if(currentItem.Parent != targetItem.Parent || e.DropPosition == DropPosition.Inside)
                e.Handled = true;
            else
                e.Handled = false;
        }

        private void organizationTreeView_DragEnded(object sender, RadTreeViewDragEndedEventArgs e) {
            if(e.DraggedItems == null || !e.DraggedItems.Any() || ((RadTreeViewItem)e.DraggedItems[0]).ParentTreeView == null)
                return;

            var currentItem = (RadTreeViewItem)e.DraggedItems.First();
            var targetItem = e.TargetDropItem;

            if(e.DropPosition == DropPosition.Before)
                if(typeof(RadTreeViewItem) == targetItem.Parent.GetType())
                    ((RadTreeViewItem)currentItem.Parent).Items.Insert(targetItem.Index - 1, currentItem);
                else
                    ((RadTreeView)currentItem.Parent).Items.Insert(targetItem.Index - 1, currentItem);
            else if(e.DropPosition == DropPosition.After)
                if(typeof(RadTreeViewItem) == targetItem.Parent.GetType())
                    ((RadTreeViewItem)currentItem.Parent).Items.Insert(targetItem.Index + 1, currentItem);
                else
                    ((RadTreeView)currentItem.Parent).Items.Insert(targetItem.Index + 1, currentItem);
        }

        private void PopulateItems(IEnumerable<Organization> organizations, int id, ItemCollection items) {
            // ReSharper disable PossibleMultipleEnumeration
            foreach(var organization in organizations.Where(o => o.ParentId == id).OrderBy(o => o.Sequence)) {
                var text = new StringBuilder(organization.Name).Append("(").Append(organization.Code).Append(")");
                var item = new RadTreeViewItem {
                    Header = text,
                    Tag = organization,
                    IsExpanded = true
                };
                items.Add(item);
                this.PopulateItems(organizations, organization.Id, item.Items);
            }
            // ReSharper restore PossibleMultipleEnumeration
        }

        private void ChangeItemsSequence(IEnumerable<object> items) {
            var i = 1;
            foreach(var item in items.Cast<RadTreeViewItem>().OrderBy(item => item.Index)) {
                ((Organization)item.Tag).Sequence = i++;
                this.ChangeItemsSequence(item.Items);
            }
        }

        private void btnCommit_Click(object sender, RoutedEventArgs e) {
            this.ChangeItemsSequence(this.organizationTreeView.Items);
            foreach(var organization in this.Organizations)
                ((IEditableObject)organization).EndEdit();
            this.SecurityDomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    UIHelper.ShowAlertMessage(submitOp.Error.Message);
                    submitOp.MarkErrorAsHandled();
                } else {
                    UIHelper.ShowNotification(SecurityUIStrings.CustomControl_Organization_ChangeSequence_Success);
                    ((OrganizationMngControl)this.ParentPage).ReloadOrganizationMngControl(null);
                    ShellViewModel.Current.IsRibbonMinimized = false;
                }
            }, null);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e) {
            this.SecurityDomainContext.RejectChanges();
            ((OrganizationMngControl)this.ParentPage).ReloadOrganizationMngControl(null);
            ShellViewModel.Current.IsRibbonMinimized = false;
        }

        private void SecurityUserControlBase_Loaded(object sender, RoutedEventArgs e) {
            this.PopulateItems(this.Organizations, 0, this.organizationTreeView.Items);
        }
    }
}
