﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.Custom {
    public partial class OrganizationEditControl {
        public UserControl ParentPage;
        private string organizationEditTitle;
        private Organization organization;
        private ObservableCollection<Organization> organizations;
        private IEnumerable<Personnel> users;
        private bool isEditMode;
        private ObservableCollection<KeyValuePair> kvType, kvUserStatus;

        public string OrganizationEditTitle {
            get {
                return this.organizationEditTitle;
            }
            set {
                this.organizationEditTitle = value;
                this.OnPropertyChanged("OrganizationEditTitle");
            }
        }

        public Organization Organization {
            get {
                return this.organization;
            }
            set {
                this.organization = value;
                this.OnPropertyChanged("Organization");
            }
        }

        public ObservableCollection<Organization> Organizations {
            get {
                return this.organizations;
            }
            set {
                this.organizations = value;
                this.OnPropertyChanged("Organizations");
            }
        }

        public IEnumerable<Personnel> Users {
            get {
                return this.users ?? (this.users = Enumerable.Empty<Personnel>());
            }
            set {
                this.users = value;
                this.OnPropertyChanged("Users");
            }
        }

        public bool IsEditMode {
            get {
                return this.isEditMode;
            }
            private set {
                this.isEditMode = value;
                this.OnPropertyChanged("IsEditMode");
            }
        }

        public OrganizationEditControl() {
            this.InitializeComponent();
        }

        private void btnCommit_Click(object sender, RoutedEventArgs e) {
            var organnizationUserList = this.Organization.OrganizationPersonnels.ToList();
            foreach(var organnizationUser in organnizationUserList)
                if(!this.userGridView.SelectedItems.Cast<Personnel>().Select(user => user.Id).Contains(organnizationUser.PersonnelId))
                    this.SecurityDomainContext.OrganizationPersonnels.Remove(organnizationUser);
            foreach(var user in this.userGridView.SelectedItems.Cast<Personnel>())
                if(!organnizationUserList.Select(item => item.PersonnelId).Contains(user.Id))
                    this.Organization.OrganizationPersonnels.Add(new OrganizationPersonnel {
                        Personnel = user
                    });

            ((IEditableObject)this.Organization).EndEdit();
            this.SecurityDomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    UIHelper.ShowAlertMessage(submitOp.Error.Message);
                    submitOp.MarkErrorAsHandled();
                } else {
                    if(this.IsEditMode)
                        UIHelper.ShowNotification(SecurityUIStrings.CustomControl_Organization_Edit_Success);
                    else {
                        UIHelper.ShowNotification(SecurityUIStrings.CustomControl_Organization_Add_Success);
                        this.Organizations.Add(this.Organization);
                    }
                    ((OrganizationMngControl)this.ParentPage).ReloadOrganizationMngControl(this.Organization);
                    ShellViewModel.Current.IsRibbonMinimized = false;
                }
            }, null);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e) {
            this.SecurityDomainContext.RejectChanges();
            ((OrganizationMngControl)this.ParentPage).ReloadOrganizationMngControl(this.Organization);
            ShellViewModel.Current.IsRibbonMinimized = false;
        }

        private void SecurityUserControlBase_Loaded(object sender, RoutedEventArgs e) {
            this.IsEditMode = this.Organization.EntityState != EntityState.Detached && this.Organization.EntityState != EntityState.New;
            if(this.Organization.EntityState == EntityState.Detached)
                this.SecurityDomainContext.Organizations.Add(this.Organization);

            if(DesignerProperties.IsInDesignTool)
                return;

            this.OrganizationEditTitle = !this.IsEditMode ? SecurityUIStrings.CustomControl_Organization_OrganizationAddTitle : SecurityUIStrings.CustomControl_Organization_OrganizationEditTitle;

            KeyValueManager.GetKeyValuePairs(new Dictionary<string, ObservableCollection<KeyValuePair>> {
                {
                                                 "User_Status", this.kvUserStatus = new ObservableCollection<KeyValuePair>()
                                                 }
            }, () => {
                foreach(var column in this.userGridView.Columns) {
                    if(column.GetType() != typeof(GridViewComboBoxColumn))
                        continue;
                    var gridViewComboBoxColumn = (GridViewComboBoxColumn)column;
                    switch(gridViewComboBoxColumn.DataMemberBinding.Path.Path) {
                        case "Status":
                            gridViewComboBoxColumn.ItemsSource = this.kvUserStatus;
                            break;
                    }
                }
            });

            if(this.IsEditMode) {
                this.userGridView.SelectedItems.Clear();
                foreach(var user in this.Users)
                    if(user.OrganizationPersonnels.Select(r => r.OrganizationId).Contains(this.Organization.Id))
                        this.userGridView.SelectedItems.Add(user);
            }
        }

        private void organizationDataForm_Loaded(object sender, RoutedEventArgs e) {
            KeyValueManager.GetKeyValuePairs(new Dictionary<string, ObservableCollection<KeyValuePair>> {
                {
                                                 "Organization_OrganizationType", this.kvType = new ObservableCollection<KeyValuePair>()
                                                 }
            }, () => {
                foreach(var comboBox in this.organizationDataForm.FindChildrenByType<RadComboBox>()) {
                    var bindingExpression = comboBox.GetBindingExpression(Selector.SelectedValueProperty);
                    if(bindingExpression == null)
                        continue;
                    switch(bindingExpression.ParentBinding.Path.Path) {
                        case "Type":
                            comboBox.ItemsSource = this.kvType;
                            break;
                    }
                }
            });
        }
    }
}
