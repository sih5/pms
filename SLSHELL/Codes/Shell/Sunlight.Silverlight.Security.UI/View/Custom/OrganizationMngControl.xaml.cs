﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.RibbonBar;

namespace Sunlight.Silverlight.Security.View.Custom {
    public partial class OrganizationMngControl : ICustomTabsView {
        private ObservableCollection<Organization> organizations;
        private Organization currentOrganization;
        private IEnumerable<Personnel> users;
        private ObservableCollection<KeyValuePair> kvType, kvStatus, kvUserStatus;
        private readonly int currentEnterpriseId = BaseApp.Current.CurrentUserData.EnterpriseId;
        private RadRibbonTab ribbonTab;

        public ObservableCollection<Organization> Organizations {
            get {
                return this.organizations ?? (this.organizations = new ObservableCollection<Organization>());
            }
            set {
                this.organizations = value;
                this.OnPropertyChanged("Organizations");
            }
        }

        public Organization CurrentOrganization {
            get {
                return this.currentOrganization ?? (this.currentOrganization = new Organization());
            }
            set {
                this.currentOrganization = value;
                this.OnPropertyChanged("CurrentOrganization");
            }
        }

        private Organization SelectOrganization {
            get {
                var radTreeViewItem = this.organizationTreeView.SelectedItem;
                if(radTreeViewItem == null)
                    return null;
                return ((RadTreeViewItem)radTreeViewItem).Tag as Organization;
            }
        }

        public IEnumerable<Personnel> Users {
            get {
                return this.users ?? (this.users = Enumerable.Empty<Personnel>());
            }
            set {
                this.users = value;
                this.OnPropertyChanged("Users");
            }
        }

        public OrganizationMngControl() {
            this.InitializeComponent();
            this.SecurityDomainContext = new SecurityDomainContext();
        }


        #region 接口实现方法
        public bool IsTabsReady {
            get {
                return true;
            }
        }

        public string RibbonContextualName {
            get {
                return null;
            }
        }

        public IEnumerable<RadRibbonTab> CustomTabs {
            get {
                if(this.ribbonTab == null) {
                    var addButton = new RadRibbonButton {
                        Text = "新增",
                        Name = "add",
                        Size = ButtonSize.Large,
                        LargeImage = new BitmapImage {
                            UriSource = Utils.MakeServerUri("Client/Dms/Images/Operations/Add.png")
                        },
                        FontSize = 12,
                        Padding = new Thickness(5, 10, 5, 0),
                    };
                    addButton.Click += this.AddButton_Click;
                    var editButton = new RadRibbonButton {
                        Text = "修改",
                        Name = "edit",
                        Size = ButtonSize.Large,
                        LargeImage = new BitmapImage {
                            UriSource = Utils.MakeServerUri("Client/Dms/Images/Operations/Edit.png")
                        },
                        FontSize = 12,
                        Padding = new Thickness(5, 10, 5, 0),
                    };
                    editButton.Click += this.EditButton_Click;
                    var abandonButton = new RadRibbonButton {
                        Text = "作废",
                        Name = "abandon",
                        Size = ButtonSize.Large,
                        LargeImage = new BitmapImage {
                            UriSource = Utils.MakeServerUri("Client/Dms/Images/Operations/Abandon.png")
                        },
                        FontSize = 12,
                        Padding = new Thickness(5, 10, 5, 0),
                    };
                    abandonButton.Click += this.AbandonButton_Click;
                    var changeSequenceButton = new RadRibbonButton {
                        Text = "调整顺序",
                        Name = "changeSequence",
                        Size = ButtonSize.Large,
                        LargeImage = new BitmapImage {
                            UriSource = Utils.MakeServerUri("Client/Security/Images/Operations/Save.png")
                        },
                        FontSize = 12,
                        Padding = new Thickness(5, 10, 5, 0),
                    };
                    changeSequenceButton.Click += this.ChangeSequenceButtonButton_Click;

                    var group = new RadRibbonGroup {
                        Header = "基本操作",
                    };
                    group.Items.Add(addButton);
                    group.Items.Add(editButton);
                    group.Items.Add(abandonButton);
                    group.Items.Add(changeSequenceButton);

                    this.ribbonTab = new RadRibbonTab {
                        Header = "数据操作",
                    };
                    this.ribbonTab.Items.Add(group);
                }
                return new[] {
                    this.ribbonTab
                };
            }
        }
        #endregion 接口实现方法


        #region 事件方法
        private void organizationTreeView_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if(this.SelectOrganization == null)
                return;

            var buttons = this.ribbonTab.Items.Cast<RadRibbonGroup>().First().Items.Cast<RadRibbonButton>();
            if(buttons == null)
                return;

            // ReSharper disable PossibleMultipleEnumeration
            if(this.SelectOrganization.Type == (int)SecurityOrganizationOrganizationType.企业) {
                buttons.Single(button => button.Name == "edit").IsEnabled = false;
                buttons.Single(button => button.Name == "abandon").IsEnabled = false;
            } else {
                buttons.Single(button => button.Name == "edit").IsEnabled = true;
                buttons.Single(button => button.Name == "abandon").IsEnabled = true;
            }
            // ReSharper restore PossibleMultipleEnumeration

            this.UpdateCurrentOrganization(this.SelectOrganization);

            var orgCollection = new ObservableCollection<Organization>();
            orgCollection.Add(this.SelectOrganization);
            this.GetOrganizationAllChildren(this.Organizations, this.SelectOrganization.Id, orgCollection);
            var organizationUsers = new List<Personnel>();
            foreach(var organization in orgCollection)
                foreach(var user in this.Users)
                    if(user.OrganizationPersonnels.Select(r => r.OrganizationId).Contains(organization.Id) && !organizationUsers.Contains(user))
                        organizationUsers.Add(user);
            organizationUsers.Sort(CompareByName);
            this.userGridView.ItemsSource = organizationUsers;
        }
        #endregion 事件方法


        #region 按钮操作方法
        private void AddButton_Click(object sender, RoutedEventArgs e) {
            // ReSharper disable PossibleMultipleEnumeration
            if(this.SelectOrganization == null || this.SelectOrganization.Id <= 0) {
                UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_Organization_SelectParentOrganization);
                return;
            }
            var parentOrganization = this.Organizations.Where(org => org.ParentId == this.SelectOrganization.Id);
            var organization = new Organization {
                ParentId = this.SelectOrganization.Id,
                Type = (int)SecurityOrganizationOrganizationType.部门,
                Sequence = parentOrganization.Any() ? parentOrganization.Max(org => org.Sequence) + 1 : 1,
                Status = (int)SecurityCommonStatus.有效
            };
            var addWindow = new OrganizationEditControl {
                SecurityDomainContext = this.SecurityDomainContext,
                ParentPage = this,
                Organization = organization,
                Organizations = this.Organizations,
                Users = this.Users
            };
            this.MainTransition.Content = addWindow;
            // ReSharper restore PossibleMultipleEnumeration
        }

        private void EditButton_Click(object sender, RoutedEventArgs e) {
            if(this.SelectOrganization == null || this.SelectOrganization.Id <= 0) {
                UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_Organization_SelectOrganization);
                return;
            }
            var editWindow = new OrganizationEditControl {
                SecurityDomainContext = this.SecurityDomainContext,
                ParentPage = this,
                Organization = this.SelectOrganization,
                Users = this.Users
            };
            this.MainTransition.Content = editWindow;
        }

        private void AbandonButton_Click(object sender, RoutedEventArgs e) {
            if(this.SelectOrganization == null || this.SelectOrganization.Id <= 0) {
                UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_Organization_SelectOrganization);
                return;
            }
            var deleteWindow = new OrganizationAbandonControl {
                SecurityDomainContext = this.SecurityDomainContext,
                ParentPage = this,
                Organization = this.SelectOrganization,
                Organizations = this.Organizations,
                Users = this.Users
            };
            this.MainTransition.Content = deleteWindow;
        }

        private void ChangeSequenceButtonButton_Click(object sender, RoutedEventArgs e) {
            var changeSequenceWindow = new OrganizationChangeSequenceControl {
                SecurityDomainContext = this.SecurityDomainContext,
                ParentPage = this,
                Organizations = this.Organizations
            };
            this.MainTransition.Content = changeSequenceWindow;
        }
        #endregion 按钮操作方法


        #region 公共方法
        private static int CompareByName(Personnel xUser, Personnel yUser) {
            if(xUser == null)
                if(yUser == null)
                    return 0;
                else
                    return -1;
            if(yUser == null)
                return 1;
            var retval = string.CompareOrdinal(xUser.Name, yUser.Name);
            return retval != 0 ? retval : string.CompareOrdinal(xUser.Name, yUser.Name);
        }

        private void PopulateItems(int id, ICollection<object> items) {
            foreach(var organization in this.Organizations.Where(o => o.ParentId == id).OrderBy(o => o.Sequence)) {
                var text = new StringBuilder(organization.Name).Append("(").Append(organization.Code).Append(")");
                var item = new RadTreeViewItem {
                    Header = text,
                    Tag = organization,
                    IsExpanded = true
                };
                items.Add(item);
                this.PopulateItems(organization.Id, item.Items);
            }
        }

        private RadTreeViewItem GetSelectedItem(Organization organization, IEnumerable<object> items) {
            if(organization == null)
                return null;
            foreach(var item in items.Cast<RadTreeViewItem>()) {
                if(((Organization)item.Tag).Id == organization.Id)
                    return item;
                if(item.Items.Any())
                    return this.GetSelectedItem(organization, item.Items);
            }
            return null;
        }

        private void UpdateCurrentOrganization(Organization organization) {
            this.CurrentOrganization.Id = organization.Id;
            this.CurrentOrganization.Name = organization.Name;
            this.CurrentOrganization.Code = organization.Code;
            this.CurrentOrganization.Type = organization.Type;
            this.CurrentOrganization.Status = organization.Status;
        }

        private void GetOrganizationAllChildren(IEnumerable<Organization> organizations, int id, ObservableCollection<Organization> items) {
            // ReSharper disable PossibleMultipleEnumeration
            foreach(var organization in organizations.Where(o => o.ParentId == id)) {
                items.Add(organization);
                this.GetOrganizationAllChildren(organizations, organization.Id, items);
            }
            // ReSharper restore PossibleMultipleEnumeration
        }

        public void ReloadOrganizationMngControl(Organization organization) {
            this.MainTransition.Content = this.MainBusyIndicator;
            if(organization == null) {
                organization = this.SelectOrganization;
                this.organizationTreeView.Items.Clear();
                this.PopulateItems(0, this.organizationTreeView.Items);
                if(organization != null && !this.Organizations.Contains(organization)) {
                    var organizationUsers = new List<Personnel>();
                    foreach(var org in this.Organizations)
                        foreach(var user in this.Users)
                            if(user.OrganizationPersonnels.Select(r => r.OrganizationId).Contains(org.Id) && !organizationUsers.Contains(user))
                                organizationUsers.Add(user);
                    organizationUsers.Sort(CompareByName);
                    this.userGridView.ItemsSource = organizationUsers;
                } else
                    this.organizationTreeView.SelectedItem = this.GetSelectedItem(organization, this.organizationTreeView.Items);
            } else if(this.Organizations.Contains(organization)) {
                this.organizationTreeView.Items.Clear();
                this.PopulateItems(0, this.organizationTreeView.Items);
                this.organizationTreeView.SelectedItem = this.GetSelectedItem(organization, this.organizationTreeView.Items);
            }
        }
        #endregion 公共方法


        private void SecurityUserControlBase_Loaded(object sender, RoutedEventArgs e) {
            KeyValueManager.GetKeyValuePairs(new Dictionary<string, ObservableCollection<KeyValuePair>> {
                {
                                                 "User_Status", this.kvUserStatus = new ObservableCollection<KeyValuePair>()
                                                 }
            }, () => {
                foreach(var column in this.userGridView.Columns) {
                    if(column.GetType() != typeof(GridViewComboBoxColumn))
                        continue;
                    var gridViewComboBoxColumn = (GridViewComboBoxColumn)column;
                    switch(gridViewComboBoxColumn.DataMemberBinding.Path.Path) {
                        case "Status":
                            gridViewComboBoxColumn.ItemsSource = this.kvUserStatus;
                            break;
                    }
                }
            });

            this.SecurityDomainContext.Load(this.SecurityDomainContext.GetOrganizationsByQuery(-1, "", "", -1, (int)SecurityCommonStatus.有效, this.currentEnterpriseId), loadOp => {
                if(!loadOp.HasError && loadOp.Entities.Any()) {
                    this.Organizations.Clear();
                    foreach(var organization in loadOp.Entities.ToArray())
                        this.Organizations.Add(organization);
                    this.organizationTreeView.Items.Clear();
                    this.PopulateItems(0, this.organizationTreeView.Items);
                }
            }, null);

            this.SecurityDomainContext.Load(this.SecurityDomainContext.GetUsersByQuery(-1, this.currentEnterpriseId, "", "", "", -1).Where(user => user.Status != (int)SecurityUserStatus.作废).OrderBy(u => u.Name), loadOp => {
                if(!loadOp.HasError && loadOp.Entities.Any())
                    this.Users = loadOp.Entities;
            }, null);
        }

        private void organizationDataForm_Loaded(object sender, RoutedEventArgs e) {
            KeyValueManager.GetKeyValuePairs(new Dictionary<string, ObservableCollection<KeyValuePair>> {
                {
                                                 "Organization_OrganizationType", this.kvType = new ObservableCollection<KeyValuePair>()
                                                 },
                {
                                                 "Common_Status", this.kvStatus = new ObservableCollection<KeyValuePair>()
                                                 }
            }, () => {
                foreach(var comboBox in this.organizationDataForm.FindChildrenByType<RadComboBox>()) {
                    var bindingExpression = comboBox.GetBindingExpression(Selector.SelectedValueProperty);
                    if(bindingExpression == null)
                        continue;
                    switch(bindingExpression.ParentBinding.Path.Path) {
                        case "Type":
                            comboBox.ItemsSource = this.kvType;
                            break;
                        case "Status":
                            comboBox.ItemsSource = this.kvStatus;
                            break;
                    }
                }
            });
        }
    }
}
