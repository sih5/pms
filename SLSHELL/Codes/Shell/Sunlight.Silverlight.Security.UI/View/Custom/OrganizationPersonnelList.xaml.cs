﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Primitives;

namespace Sunlight.Silverlight.Security.View.Custom {
    public partial class OrganizationPersonnelList {
        private void RadListBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if(this.radListBox.SelectedItem == null)
                return;

            this.SelectedEntities = this.radListBox.SelectedItems.Cast<Entity>();
            this.OnSelectionChanged(sender, e);
        }

        public OrganizationPersonnelList() {
            this.InitializeComponent();
            this.radListBox.SetBinding(ItemsControlSelector.SelectedItemProperty, new Binding("SelectedEntity") {
                Source = this,
                Mode = BindingMode.TwoWay
            });
        }
    }
}
