﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Input;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Security.View.DataEdit;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.DragDrop.Behaviors;

namespace Sunlight.Silverlight.Security.View.Custom {
    public partial class OrganizationPersonnelSelectPanel : IBaseView {
        private bool initialized, isFilter;
        private DateTime lastTime = DateTime.Now;
        private Organization Organization;
        private IEnumerable<Personnel> CachePersonnels;
        private ObservableCollection<Personnel> enterprisePersonnels;
        private ObservableCollection<Personnel> filterEnterprisePersonnels;
        private ObservableCollection<Personnel> organizationPersonnels;
        private KeyValueManager KeyValueManager = new KeyValueManager();
        private readonly string[] kvNames = {
            "User_Status"
        };

        public SecurityDomainContext DomainContext {
            get;
            set;
        }

        public ObservableCollection<Personnel> EnterprisePersonnels {
            get {
                return this.enterprisePersonnels ?? (this.enterprisePersonnels = new ObservableCollection<Personnel>());
            }
        }

        public ObservableCollection<Personnel> FilterEnterprisePersonnels {
            get {
                return this.filterEnterprisePersonnels ?? (this.filterEnterprisePersonnels = new ObservableCollection<Personnel>());
            }
        }

        public ObservableCollection<Personnel> OrganizationPersonnels {
            get {
                return this.organizationPersonnels ?? (this.organizationPersonnels = new ObservableCollection<Personnel>());
            }
        }

        private void Initializer() {
            this.EnterprisePersonnelList.DragDropBehavior = new ListBoxDragDropBehavior();
            this.EnterprisePersonnelList.DragVisualProvider = new DragVisualProvider {
                DraggedItemTemplate = this.Resources["DragTemplate"] as DataTemplate
            };
            this.OrganizationPersonnelList.DragDropBehavior = new ListBoxDragDropBehavior();
            this.OrganizationPersonnelList.DragVisualProvider = new DragVisualProvider {
                DraggedItemTemplate = this.Resources["DragTemplate"] as DataTemplate
            };
            this.EnterprisePersonnelList.AddHandler(MouseLeftButtonDownEvent, new MouseButtonEventHandler(this.EnterprisePersonnelList_MouseLeftButtonDown), true);
            this.OrganizationPersonnelList.AddHandler(MouseLeftButtonDownEvent, new MouseButtonEventHandler(this.OrganizationPersonnelList_MouseLeftButtonDown), true);
            this.DataContextChanged += this.OrganizationPersonnelSelectPanel_DataContextChanged;
        }

        private void LoadPersonnels() {
            this.DomainContext.Load(this.DomainContext.GetPersonnelsWithDetailQuery().Where(o => o.EnterpriseId == BaseApp.Current.CurrentUserData.EnterpriseId && o.Status != (int)SecurityUserStatus.作废).OrderBy(o => o.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.CachePersonnels = loadOp.Entities;
                this.EnterprisePersonnels.Clear();
                this.OrganizationPersonnels.Clear();
                foreach(var entity in this.CachePersonnels) {
                    if(this.Organization.OrganizationPersonnels.Any(op => op.PersonnelId == entity.Id))
                        this.OrganizationPersonnels.Add(entity);
                    else
                        this.EnterprisePersonnels.Add(entity);
                }
            }, null);
        }

        private void OrganizationPersonnelSelectPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            this.Organization = this.DataContext as Organization;
            if(this.Organization == null)
                return;

            this.FilterOrganization.IsChecked = false;
            this.FilterName.Text = string.Empty;
            this.EnterprisePersonnelList.ItemsSource = this.EnterprisePersonnels;

            this.LoadPersonnels();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e) {
            foreach(var entity in this.FilterEnterprisePersonnels)
                if(!this.EnterprisePersonnels.Contains(entity))
                    this.EnterprisePersonnels.Add(entity);

            this.FilterEnterprisePersonnels.Clear();
            this.isFilter = true;

            var personnels = this.EnterprisePersonnels.Where(p => !p.OrganizationPersonnels.Any());
            if(!string.IsNullOrEmpty(this.FilterName.Text))
                personnels = personnels.Where(p => p.Name.Contains(this.FilterName.Text));
            foreach(var entity in personnels)
                this.FilterEnterprisePersonnels.Add(entity);
            foreach(var entity in this.FilterEnterprisePersonnels)
                this.EnterprisePersonnels.Remove(entity);
            this.EnterprisePersonnelList.ItemsSource = this.FilterEnterprisePersonnels;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e) {
            foreach(var entity in this.FilterEnterprisePersonnels)
                if(!this.EnterprisePersonnels.Contains(entity))
                    this.EnterprisePersonnels.Add(entity);

            this.FilterEnterprisePersonnels.Clear();

            if(string.IsNullOrEmpty(this.FilterName.Text)) {
                this.isFilter = false;
                this.EnterprisePersonnelList.ItemsSource = this.EnterprisePersonnels;
            } else {
                this.isFilter = true;
                foreach(var entity in this.EnterprisePersonnels.Where(p => p.Name.Contains(this.FilterName.Text)))
                    this.FilterEnterprisePersonnels.Add(entity);
                foreach(var entity in this.FilterEnterprisePersonnels)
                    this.EnterprisePersonnels.Remove(entity);
                this.EnterprisePersonnelList.ItemsSource = this.FilterEnterprisePersonnels;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            foreach(var entity in this.FilterEnterprisePersonnels)
                if(!this.EnterprisePersonnels.Contains(entity))
                    this.EnterprisePersonnels.Add(entity);

            this.FilterEnterprisePersonnels.Clear();

            if(string.IsNullOrEmpty(this.FilterName.Text)) {
                if((bool)this.FilterOrganization.IsChecked) {
                    this.isFilter = true;
                    foreach(var entity in this.EnterprisePersonnels.Where(p => !p.OrganizationPersonnels.Any()))
                        this.FilterEnterprisePersonnels.Add(entity);
                    foreach(var entity in this.FilterEnterprisePersonnels)
                        this.EnterprisePersonnels.Remove(entity);
                    this.EnterprisePersonnelList.ItemsSource = this.FilterEnterprisePersonnels;
                } else {
                    this.isFilter = false;
                    this.EnterprisePersonnelList.ItemsSource = this.EnterprisePersonnels;
                }
            } else {
                this.isFilter = true;
                var personnels = this.EnterprisePersonnels.Where(p => p.Name.Contains(this.FilterName.Text));
                if((bool)this.FilterOrganization.IsChecked)
                    personnels = personnels.Where(p => !p.OrganizationPersonnels.Any());
                foreach(var entity in personnels)
                    this.FilterEnterprisePersonnels.Add(entity);
                foreach(var entity in this.FilterEnterprisePersonnels)
                    this.EnterprisePersonnels.Remove(entity);
                this.EnterprisePersonnelList.ItemsSource = this.FilterEnterprisePersonnels;
            }
        }

        private void EnterprisePersonnelList_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            if(this.EnterprisePersonnelList.SelectedItem == null)
                return;
            if((DateTime.Now - lastTime).TotalMilliseconds < 300) {
                var personnel = this.EnterprisePersonnelList.SelectedItem as Personnel;
                if(this.isFilter)
                    this.FilterEnterprisePersonnels.Remove(personnel);
                else
                    this.EnterprisePersonnels.Remove(personnel);
                this.OrganizationPersonnels.Add(personnel);
            }
            lastTime = DateTime.Now;
        }

        private void OrganizationPersonnelList_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            if(this.OrganizationPersonnelList.SelectedItem == null)
                return;
            if((DateTime.Now - lastTime).TotalMilliseconds < 300) {
                var personnel = this.OrganizationPersonnelList.SelectedItem as Personnel;
                this.OrganizationPersonnels.Remove(personnel);
                if(this.isFilter)
                    this.FilterEnterprisePersonnels.Add(personnel);
                else
                    this.EnterprisePersonnels.Add(personnel);
            }
            lastTime = DateTime.Now;
        }

        public OrganizationPersonnelSelectPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            if(!this.initialized) {
                this.initialized = true;
                this.Initializer();
            }
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new NotImplementedException();
        }
    }
}