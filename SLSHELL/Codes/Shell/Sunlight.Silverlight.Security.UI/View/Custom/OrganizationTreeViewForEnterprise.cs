﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.Custom {
    public class OrganizationTreeViewForEnterprise : RadTreeView, IBaseView {
        private SecurityDomainContext domainContext;

        public SecurityDomainContext DomainContext {
            get {
                return domainContext ?? (domainContext = new SecurityDomainContext());
            }
            set {
                this.domainContext = value;
            }
        }

        private void PopulateItems(IEnumerable<Organization> organizations, int id, ICollection<object> items) {
            if(organizations == null)
                throw new ArgumentNullException("organizations");

            foreach(var organization in organizations.Where(o => o.ParentId == id).OrderBy(o => o.Sequence)) {
                var item = new RadTreeViewItem {
                    Header = string.Format("{0} ({1})", organization.Name, organization.Code),
                    Tag = organization,
                    IsExpanded = true
                };
                items.Add(item);
                this.PopulateItems(organizations, organization.Id, item.Items);
            }
        }

        private void OrganizationTreeViewForEnterprise_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var enterprise = this.DataContext as Enterprise;
            if(enterprise == null) {
                this.Items.Clear();
                return;
            }

            this.DomainContext.Load(this.DomainContext.GetOrganizationsQuery().Where(o => o.EnterpriseId == enterprise.Id && o.Status != (int)SecurityCommonStatus.作废), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.Items.Clear();
                this.PopulateItems(loadOp.Entities, -1, this.Items);
            }, null);
        }

        public OrganizationTreeViewForEnterprise() {
            this.DataContextChanged += this.OrganizationTreeViewForEnterprise_DataContextChanged;
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new NotImplementedException();
        }
    }
}
