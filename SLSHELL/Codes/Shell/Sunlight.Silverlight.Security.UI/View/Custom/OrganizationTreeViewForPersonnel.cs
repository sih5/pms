﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.Custom {
    public class OrganizationTreeViewForPersonnel : RadTreeView, IBaseView {
        private Personnel Personnel;
        private IEnumerable<Organization> Organizations;

        public SecurityDomainContext DomainContext {
            get;
            set;
        }

        private void PopulateItems(IEnumerable<Organization> organizations, int id, ICollection<object> items) {
            if(organizations == null)
                return;

            foreach(var organization in organizations.Where(o => o.ParentId == id).OrderBy(o => o.Sequence)) {
                var item = new RadTreeViewItem {
                    Header = string.Format("{0} ({1})", organization.Name, organization.Code),
                    Tag = organization,
                    IsExpanded = true,
                    IsChecked = this.Personnel != null && this.Personnel.OrganizationPersonnels.Select(r => r.OrganizationId).Contains(organization.Id)
                };
                items.Add(item);
                this.PopulateItems(organizations, organization.Id, item.Items);
            }
        }

        private void ReloadOrganizations() {
            this.DomainContext.Load(this.DomainContext.GetOrganizationsQuery().Where(o => o.EnterpriseId == BaseApp.Current.CurrentUserData.EnterpriseId && o.Status != (int)SecurityCommonStatus.作废), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.Organizations = loadOp.Entities;
                this.Items.Clear();
                this.PopulateItems(this.Organizations, -1, this.Items);
            }, null);
        }

        private void OrganizationTreeView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            this.Personnel = this.DataContext as Personnel;
            if(this.Personnel == null)
                return;
            this.ReloadOrganizations();
        }

        public OrganizationTreeViewForPersonnel(bool isOptionElementsEnabled, OptionListType optionListType) {
            this.IsOptionElementsEnabled = isOptionElementsEnabled;
            //this.ItemsOptionListType = optionListType;
            this.DataContextChanged += this.OrganizationTreeView_DataContextChanged;
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new System.NotImplementedException();
        }
    }
}