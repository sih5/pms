﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.Custom {
    public class OrganizationTreeViewForPersonnelGroup : RadTreeView, IBaseView {

        private IEnumerable<Organization> Organizations;

        public SecurityDomainContext DomainContext {
            get;
            set;
        }

        private int enterpriseId = BaseApp.Current.CurrentUserData.EnterpriseId;

        public int EnterpriseId {
            get {
                return enterpriseId;
            }
            set {
                enterpriseId = value;
            }
        }

        private void PopulateItems(IEnumerable<Organization> organizations, int id, ICollection<object> items) {
            if(organizations == null)
                return;
            var personnel = this.DataContext as Personnel;
            foreach(var organization in organizations.Where(o => o.ParentId == id).OrderBy(o => o.Sequence)) {
                var item = new RadTreeViewItem {
                    Header = string.Format("{0} ({1})", organization.Name, organization.Code),
                    Tag = organization,
                    IsExpanded = true,
                    IsChecked = personnel != null && personnel.OrganizationPersonnels.Select(r => r.OrganizationId).Contains(organization.Id)
                };
                items.Add(item);
                this.PopulateItems(organizations, organization.Id, item.Items);
            }
        }

        private void ReloadOrganizations() {
            var personnel = this.DataContext as Personnel;
            if(personnel == null)
                return;
            if(personnel.EnterpriseId != enterpriseId)
                return;
            this.DomainContext.Load(this.DomainContext.GetOrganizationsQuery().Where(o => o.EnterpriseId == enterpriseId && o.Status != (int)SecurityCommonStatus.作废), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.Organizations = loadOp.Entities;
                this.Items.Clear();
                this.PopulateItems(this.Organizations, -1, this.Items);
            }, null);
        }

        private void OrganizationTreeView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var personnel = e.NewValue as Personnel;
            if(personnel == null)
                return;
            this.ReloadOrganizations();
            personnel.PropertyChanged -= this.Personnel_PropertyChanged;
            personnel.PropertyChanged += this.Personnel_PropertyChanged;

        }

        void Personnel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var personnel = this.DataContext as Personnel;
            if(personnel == null)
                return;
            if(e.PropertyName == "EnterpriseId") {
                this.DomainContext.Load(this.DomainContext.GetOrganizationsQuery().Where(o => o.EnterpriseId == personnel.EnterpriseId && o.Status != (int)SecurityCommonStatus.作废), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    this.Organizations = loadOp.Entities;
                    this.Items.Clear();
                    this.PopulateItems(this.Organizations, -1, this.Items);
                }, null);
            }
        }

        public OrganizationTreeViewForPersonnelGroup(bool isOptionElementsEnabled, OptionListType optionListType) {
            this.IsOptionElementsEnabled = isOptionElementsEnabled;
            //this.ItemsOptionListType = optionListType;
            this.DataContextChanged += this.OrganizationTreeView_DataContextChanged;
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new System.NotImplementedException();
        }
    }
}