﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Data;
using Sunlight.Silverlight.Security.Controls;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.Custom {
    // ReSharper disable PossibleMultipleEnumeration
    public partial class PageAuthorization {
        private sealed class TreeViewIsCheckedConverter : IValueConverter {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
                var treeViewItem = (RadTreeViewItem)value;
                if(!treeViewItem.HasItems)
                    return ToggleState.Off;

                var checkedCount = treeViewItem.Items.Cast<RadTreeViewItem>().Count(item => item.CheckState == ToggleState.On);
                var count = treeViewItem.Items.Count;
                var indeterminateCount = treeViewItem.Items.Cast<RadTreeViewItem>().Count(item => item.CheckState == ToggleState.Indeterminate);

                if(checkedCount == count)
                    return ToggleState.On;
                return checkedCount == 0 && indeterminateCount == 0 ? ToggleState.Off : ToggleState.Indeterminate;
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
                throw new NotSupportedException();
            }
        }

        private sealed class PageData {
            public int Id {
                get;
                set;
            }

            public bool AllowAccess {
                get;
                set;
            }

            public IEnumerable<string> Operations {
                get;
                set;
            }

            public override string ToString() {
                return string.Format("{0}, {1}", this.Id, this.AllowAccess);
            }
        }

        public SecurityDomainContext SecurityDomainContext;
        private IEnumerable<Page> pagesCache;
        private IEnumerable<Node> nodesCache;
        private IEnumerable<SecurityAction> securityActionCache;
        private readonly Dictionary<int, int[]> authorizedPages;
        private readonly Dictionary<int, List<PageData>> authenticationData;

        public Role CurrentRole {
            get;
            private set;
        }

        private Page CurrentPage {
            get {
                var pageItem = this.PagesTreeView.SelectedItem as RadTreeViewItem;
                if(pageItem == null)
                    return null;
                return pageItem.Tag as Page;
            }
        }

        private RadTreeViewItem CurrentRadTreeViewItem {
            get {
                var pageItem = this.PagesTreeView.SelectedItem as RadTreeViewItem;
                if(pageItem == null)
                    return null;
                return pageItem;
            }
        }

        private IEnumerable<RadTreeViewItem> PageTreeViewItems {
            get {
                return this.PagesTreeView.Items.Cast<RadTreeViewItem>().SelectMany(item => item.Items.Cast<RadTreeViewItem>()).SelectMany(item => item.Items.Cast<RadTreeViewItem>());
            }
        }

        public PageAuthorization() {
            this.InitializeComponent();
            this.authorizedPages = new Dictionary<int, int[]>();
            this.authenticationData = new Dictionary<int, List<PageData>>();
            this.PagesTreeView.SelectionChanged += this.Pages_SelectionChanged;
            this.MainPanel.DataChanged += this.MainPanel_DataChanged;
        }

        private IEnumerable<PageData> EnsureRoleData() {
            if(this.CurrentRole == null)
                return null;

            List<PageData> result;
            if(!this.authenticationData.TryGetValue(this.CurrentRole.Id, out result)) {
                var items = this.PageTreeViewItems.ToArray();
                result = new List<PageData>(items.Select(item => new PageData {
                    Id = ((Page)item.Tag).Id,
                    AllowAccess = item.CheckState == ToggleState.On,
                }));
                this.authenticationData.Add(this.CurrentRole.Id, result);
            }
            return result;
        }

        public string AddAuthenticationData() {
            List<PageData> pageDataList;
            if(this.authenticationData.TryGetValue(this.CurrentRole.Id, out pageDataList)) {
                foreach(var pageData in pageDataList.Where(pageData => !pageData.AllowAccess).Where(pageData => pageData.Operations != null && pageData.Operations.Any() && this.PageTreeViewItems.Single(i => ((Page)i.Tag).Id == pageData.Id).CheckState == ToggleState.On))
                    pageData.AllowAccess = true;
                var allowAccessPageData = pageDataList.Where(pageData => pageData.AllowAccess);
                if(allowAccessPageData == null || !allowAccessPageData.Any())
                    return SecurityUIStrings.CustomControl_Role_NodeNotAllowNull;

                // 删除当前角色、被取消的页面授权规则及其页面内所有操作授权规则
                var pageNodeIds = this.nodesCache.Where(n => n.CategoryType == (int)SecurityNodeCategoryType.页面 && !allowAccessPageData.Select(p => p.Id).Contains(n.CategoryId) && n.Status == (int)SecurityCommonStatus.有效).Select(n => n.Id).ToArray();
                foreach(var rule in this.CurrentRole.Rules.Where(rule => pageNodeIds.Contains(rule.NodeId)))
                    this.SecurityDomainContext.Rules.Remove(rule);
                if(this.securityActionCache != null) {
                    var securityActionIds = this.securityActionCache.Where(o => !allowAccessPageData.Select(p => p.Id).Contains(o.PageId) && o.Status == (int)SecurityCommonStatus.有效).Select(o => o.Id).ToArray().ToArray();
                    var nodeIds = this.nodesCache.Where(n => securityActionIds.Contains(n.CategoryId) && n.Status == (int)SecurityCommonStatus.有效).Select(n => n.Id).ToArray();
                    foreach(var rule in this.CurrentRole.Rules.Where(rule => nodeIds.Contains(rule.NodeId)))
                        this.SecurityDomainContext.Rules.Remove(rule);
                }
                var ID = Int32.MaxValue;
                foreach(var pageData in allowAccessPageData) {
                    // 页面的处理
                    var pageNode = this.nodesCache.SingleOrDefault(n => n.CategoryType == (int)SecurityNodeCategoryType.页面 && n.CategoryId == pageData.Id && n.Status == (int)SecurityCommonStatus.有效);
                    if(pageNode == null) {
                        // 新增页面节点
                        pageNode = new Node {
                            CategoryType = (int)SecurityNodeCategoryType.页面,
                            CategoryId = pageData.Id,
                            Status = (int)SecurityCommonStatus.有效
                        };
                        this.SecurityDomainContext.Nodes.Add(pageNode);
                    }
                    if(pageNode.Id == 0 || !this.CurrentRole.Rules.Select(rule => rule.NodeId).Contains(pageNode.Id)) // 新增页面规则
                        this.SecurityDomainContext.Rules.Add(new Rule {
                            Role = this.CurrentRole,
                            Node = pageNode
                        });

                    // 页面里操作的处理
                    var allowAccessActionData = pageData.Operations;
                    if(allowAccessActionData == null)
                        continue;
                    if(!allowAccessActionData.Any() && this.securityActionCache != null) {
                        // 删除当前角色、当前页面的所有操作授权规则
                        var securityActionIds = this.securityActionCache.Where(o => o.PageId == pageData.Id && o.Status == (int)SecurityCommonStatus.有效).Select(o => o.Id).ToArray().ToArray();
                        var nodeIds = this.nodesCache.Where(n => securityActionIds.Contains(n.CategoryId) && n.Status == (int)SecurityCommonStatus.有效).Select(n => n.Id).ToArray();
                        foreach(var rule in this.CurrentRole.Rules.Where(rule => nodeIds.Contains(rule.NodeId)))
                            this.SecurityDomainContext.Rules.Remove(rule);
                    } else {
                        // 删除当前角色、当前页面被取消的操作授权规则
                        if(this.securityActionCache != null) {
                            var securityActionIds = this.securityActionCache.Where(o => o.PageId == pageData.Id && !allowAccessActionData.Contains(o.OperationId) && o.Status == (int)SecurityCommonStatus.有效).Select(o => o.Id).ToArray();
                            var nodeIds = this.nodesCache.Where(n => securityActionIds.Contains(n.CategoryId) && n.Status == (int)SecurityCommonStatus.有效).Select(n => n.Id).ToArray();
                            foreach(var rule in this.CurrentRole.Rules.Where(rule => nodeIds.Contains(rule.NodeId)))
                                this.SecurityDomainContext.Rules.Remove(rule);
                        }
                        foreach(var operationId in allowAccessActionData) {
                            var securityAction = this.securityActionCache != null ? this.securityActionCache.SingleOrDefault(o => o.PageId == pageData.Id && o.OperationId == operationId && o.Status == (int)SecurityCommonStatus.有效) : null;
                            if(securityAction == null) {
                                // 新增操作
                                securityAction = new SecurityAction {
                                    Id = ID,
                                    PageId = pageData.Id,
                                    OperationId = operationId,
                                    Status = (int)SecurityCommonStatus.有效
                                };
                                this.SecurityDomainContext.SecurityActions.Add(securityAction);
                            }
                            var node = this.nodesCache.SingleOrDefault(n => n.CategoryType == (int)SecurityNodeCategoryType.操作 && n.CategoryId == securityAction.Id && n.Status == (int)SecurityCommonStatus.有效);
                            if(node == null) {
                                // 新增操作节点
                                node = new Node {
                                    Id = ID,
                                    CategoryType = (int)SecurityNodeCategoryType.操作,
                                    SecurityAction = securityAction,
                                    Status = (int)SecurityCommonStatus.有效
                                };
                                this.SecurityDomainContext.Nodes.Add(node);
                            }
                            if(node.Id == 0 || !this.CurrentRole.Rules.Select(rule => rule.NodeId).Contains(node.Id)) // 新增操作规则
                                this.SecurityDomainContext.Rules.Add(new Rule {
                                    Role = this.CurrentRole,
                                    Node = node
                                });
                            ID--;
                        }
                    }
                }
                return null;
            }
            return SecurityUIStrings.CustomControl_Role_NodeNotAllowNull;
        }

        private void ReloadPages(IEnumerable<int> pageIds) {
            Action setTreeView = () => {
                this.PagesTreeView.SelectionChanged -= this.Pages_SelectionChanged;
                var expandedItems = this.PagesTreeView.Items.Cast<RadTreeViewItem>().Where(item => item.IsExpanded).ToList();
                expandedItems.AddRange(expandedItems.SelectMany(item => item.Items).Cast<RadTreeViewItem>().Where(item => item.IsExpanded).ToArray());
                var expandedIds = expandedItems.Select(item => ((Page)item.Tag).Id);
                var selectedId = this.PagesTreeView.SelectedItem == null ? 0 : ((Page)((RadTreeViewItem)this.PagesTreeView.SelectedItem).Tag).Id;
                this.PagesTreeView.Items.Clear();
                this.PagesTreeView.SelectionChanged += this.Pages_SelectionChanged;
                Helper.SetItemsControlByEntity<Page, RadTreeViewItem>(this.PagesTreeView, this.pagesCache, "Id", "ParentId", "Sequence", (page, item) => {
                    item.Header = page.Name;
                    item.IsSelected = page.Id == selectedId;
                    switch(page.Type) {
                        case 0:
                        case 1:
                            item.FontWeight = FontWeights.Bold;
                            item.IsExpanded = expandedIds.Contains(page.Id);
                            item.SetBinding(RadTreeViewItem.CheckStateProperty, new Binding {
                                Source = item,
                                Mode = BindingMode.OneWay,
                                Converter = new TreeViewIsCheckedConverter(),
                            });
                            break;
                        case 2:
                            item.CheckState = pageIds.Contains(page.Id) ? ToggleState.On : ToggleState.Off;
                            item.Checked += this.TreeViewItem_Checked_Unchecked;
                            item.Unchecked += this.TreeViewItem_Checked_Unchecked;
                            break;
                    }
                });
            };

            this.Dispatcher.BeginInvoke(setTreeView);
        }

        private void TreeViewItem_Checked_Unchecked(object sender, RadRoutedEventArgs e) {
            var pagesData = this.EnsureRoleData();
            if(pagesData == null)
                return;

            var treeViewItem = (RadTreeViewItem)sender;
            var page = (Page)treeViewItem.Tag;
            pagesData.Single(p => p.Id == page.Id).AllowAccess = treeViewItem.CheckState == ToggleState.On;
            if(treeViewItem.CheckState == ToggleState.Off && this.MainPanel.CurrentPage != null && page.Id == this.MainPanel.CurrentPage.Id)
                this.MainPanel.SetButtonsUnChecked();
        }

        private void Pages_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            this.MainPanel.Visibility = Visibility.Collapsed;

            if(this.CurrentRole == null)
                return;

            IEnumerable<string> actionIds = null;
            if(this.authenticationData.ContainsKey(this.CurrentRole.Id) && this.CurrentPage != null && this.CurrentPage.Type == 2)
                actionIds = this.authenticationData[this.CurrentRole.Id].Single(pageData => pageData.Id == this.CurrentPage.Id).Operations;

            var group = this.pagesCache.SingleOrDefault(p => p.Id == this.CurrentPage.ParentId);
            var system = group == null ? null : this.pagesCache.SingleOrDefault(p => p.Id == group.ParentId);

            this.MainPanel.SetAuthorizationNode(this.CurrentRole, system, group, this.CurrentPage, actionIds);
        }

        private void MainPanel_DataChanged(object sender, EventArgs e) {
            if(this.MainPanel.CurrentRole == null || this.MainPanel.CurrentPage == null)
                return;
            if(this.EnsureRoleData().Single(pd => pd.Id == this.MainPanel.CurrentPage.Id).AllowAccess == false) {
                this.CurrentRadTreeViewItem.Checked -= this.TreeViewItem_Checked_Unchecked;
                this.CurrentRadTreeViewItem.Unchecked -= this.TreeViewItem_Checked_Unchecked;
                this.CurrentRadTreeViewItem.CheckState = this.MainPanel.SelectedOperationIds != null && this.MainPanel.SelectedOperationIds.Any() ? ToggleState.On : ToggleState.Off;
                this.CurrentRadTreeViewItem.Checked += this.TreeViewItem_Checked_Unchecked;
                this.CurrentRadTreeViewItem.Unchecked += this.TreeViewItem_Checked_Unchecked;
            }
            this.EnsureRoleData().Single(pd => pd.Id == this.MainPanel.CurrentPage.Id).Operations = this.MainPanel.SelectedOperationIds;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e) {
            this.SecurityDomainContext = ((SecurityUserControlBase)this.DataContext).SecurityDomainContext;
            this.SecurityDomainContext.Load(this.SecurityDomainContext.GetAuthorizablePagesQuery(), loadOp => {
                if(loadOp.Entities.Any()) {
                    this.pagesCache = loadOp.Entities;
                    if(this.CurrentRole != null) {
                        this.PagesTreeView.SelectedItem = null;
                        if(this.authenticationData.ContainsKey(this.CurrentRole.Id))
                            this.ReloadPages(this.authenticationData[this.CurrentRole.Id].Where(pageData => pageData.AllowAccess).Select(pageData => pageData.Id).ToArray());
                        else {
                            int[] pageIds;
                            if(this.authorizedPages.TryGetValue(this.CurrentRole.Id, out pageIds))
                                this.ReloadPages(pageIds);
                            else
                                this.SecurityDomainContext.GetAuthorizedPageIdsByRoleId(this.CurrentRole.Id, invokeOp => {
                                    if(invokeOp.HasError)
                                        return;
                                    this.authorizedPages[this.CurrentRole.Id] = pageIds = invokeOp.Value;
                                    this.ReloadPages(pageIds);
                                }, null);
                        }
                    } else
                        this.ReloadPages(Enumerable.Empty<int>());
                }
            }, null);
            this.SecurityDomainContext.Load(this.SecurityDomainContext.GetNodesByQuery(), loadOp => {
                if(!loadOp.HasError && loadOp.Entities.Any())
                    this.nodesCache = loadOp.Entities;
            }, null);
            this.SecurityDomainContext.Load(this.SecurityDomainContext.GetSecurityActionsByQuery(null, -1, "", (int)SecurityCommonStatus.有效), loadOp => {
                if(!loadOp.HasError && loadOp.Entities.Any())
                    this.securityActionCache = loadOp.Entities;
            }, null);
        }

        public void SetCurrentRole(Role role) {
            this.CurrentRole = role;

            if(this.pagesCache != null && this.pagesCache.Any()) {
                this.PagesTreeView.SelectedItem = null;

                if(this.authenticationData.ContainsKey(this.CurrentRole.Id))
                    this.ReloadPages(this.authenticationData[this.CurrentRole.Id].Where(pageData => pageData.AllowAccess).Select(pageData => pageData.Id).ToArray());
                else
                    this.SecurityDomainContext.GetAuthorizedPageIdsByRoleId(this.CurrentRole.Id, invokeOp => {
                        if(invokeOp.HasError)
                            return;
                        int[] pageIds;
                        this.authorizedPages[this.CurrentRole.Id] = pageIds = invokeOp.Value;
                        this.ReloadPages(pageIds);
                    }, null);
            }
        }
    }

    // ReSharper restore PossibleMultipleEnumeration
}
