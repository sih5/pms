﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Input;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Security.View.DataEdit;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.DragDrop.Behaviors;

namespace Sunlight.Silverlight.Security.View.Custom {
    public partial class PersonnelSelectPanel : IBaseView {
        private bool initialized, isFilter;
        private DateTime lastTime = DateTime.Now;
        private Role Role;
        private IEnumerable<Personnel> CachePersonnels;
        private ObservableCollection<Personnel> enterprisePersonnels;
        private ObservableCollection<Personnel> filterEnterprisePersonnels;
        private ObservableCollection<Personnel> rolePersonnels;
        private KeyValueManager KeyValueManager = new KeyValueManager();
        private readonly string[] kvNames = {
            "User_Status"
        };
        
        public bool IsLoaded {
            get;
            set;
        }

        public SecurityDomainContext DomainContext {
            get;
            set;
        }

        public ObservableCollection<Personnel> EnterprisePersonnels {
            get {
                return this.enterprisePersonnels ?? (this.enterprisePersonnels = new ObservableCollection<Personnel>());
            }
        }

        public ObservableCollection<Personnel> FilterEnterprisePersonnels {
            get {
                return this.filterEnterprisePersonnels ?? (this.filterEnterprisePersonnels = new ObservableCollection<Personnel>());
            }
        }

        public ObservableCollection<Personnel> RolePersonnels {
            get {
                return this.rolePersonnels ?? (this.rolePersonnels = new ObservableCollection<Personnel>());
            }
        }

        private void Initializer() {
            this.EnterprisePersonnelList.DragDropBehavior = new ListBoxDragDropBehavior();
            this.EnterprisePersonnelList.DragVisualProvider = new DragVisualProvider {
                DraggedItemTemplate = this.Resources["DragTemplate"] as DataTemplate
            };
            this.RolePersonnelList.DragDropBehavior = new ListBoxDragDropBehavior();
            this.RolePersonnelList.DragVisualProvider = new DragVisualProvider {
                DraggedItemTemplate = this.Resources["DragTemplate"] as DataTemplate
            };
            this.EnterprisePersonnelList.AddHandler(MouseLeftButtonDownEvent, new MouseButtonEventHandler(this.EnterprisePersonnelList_MouseLeftButtonDown), true);
            this.RolePersonnelList.AddHandler(MouseLeftButtonDownEvent, new MouseButtonEventHandler(this.RolePersonnelList_MouseLeftButtonDown), true);
            this.DataContextChanged += this.PersonnelSelectPanel_DataContextChanged;
        }

        private void LoadPersonnels() {
            this.DomainContext.Load(this.DomainContext.GetPersonnelsQuery().Where(o => o.EnterpriseId == BaseApp.Current.CurrentUserData.EnterpriseId && o.Status != (int)SecurityUserStatus.作废).OrderBy(o => o.Name), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.CachePersonnels = loadOp.Entities;
                this.EnterprisePersonnels.Clear();
                this.RolePersonnels.Clear();
                foreach(var entity in this.CachePersonnels) {
                    if(this.Role.RolePersonnels.Any(rp => rp.PersonnelId == entity.Id))
                        this.RolePersonnels.Add(entity);
                    else
                        this.EnterprisePersonnels.Add(entity);
                }
            }, null);
        }

        private void PersonnelSelectPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            this.Role = this.DataContext as Role;
            if(this.Role == null)
                return;

            this.FilterName.Text = string.Empty;
            this.EnterprisePersonnelList.ItemsSource = this.EnterprisePersonnels;

            this.IsLoaded = true;
            if(this.CachePersonnels == null)
                this.LoadPersonnels();
            else {
                this.EnterprisePersonnels.Clear();
                this.RolePersonnels.Clear();
                foreach(var entity in this.CachePersonnels) {
                    if(this.Role.RolePersonnels.Any(rp => rp.Personnel.Id == entity.Id))
                        this.RolePersonnels.Add(entity);
                    else
                        this.EnterprisePersonnels.Add(entity);
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            foreach(var entity in this.FilterEnterprisePersonnels)
                if(!this.EnterprisePersonnels.Contains(entity))
                    this.EnterprisePersonnels.Add(entity);

            this.FilterEnterprisePersonnels.Clear();

            if(string.IsNullOrEmpty(this.FilterName.Text)) {
                this.isFilter = false;
                this.EnterprisePersonnelList.ItemsSource = this.EnterprisePersonnels;
            } else {
                this.isFilter = true;
                foreach(var entity in this.EnterprisePersonnels.Where(p => p.Name.Contains(this.FilterName.Text)))
                    this.FilterEnterprisePersonnels.Add(entity);
                foreach(var entity in this.FilterEnterprisePersonnels)
                    this.EnterprisePersonnels.Remove(entity);
                this.EnterprisePersonnelList.ItemsSource = this.FilterEnterprisePersonnels;
            }
        }

        private void EnterprisePersonnelList_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            if(this.EnterprisePersonnelList.SelectedItem == null)
                return;
            if((DateTime.Now - lastTime).TotalMilliseconds < 300) {
                var personnel = this.EnterprisePersonnelList.SelectedItem as Personnel;
                if(this.isFilter == true)
                    this.FilterEnterprisePersonnels.Remove(personnel);
                else
                    this.EnterprisePersonnels.Remove(personnel);
                this.RolePersonnels.Add(personnel);
            }
            lastTime = DateTime.Now;
        }

        private void RolePersonnelList_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            if(this.RolePersonnelList.SelectedItem == null)
                return;
            if((DateTime.Now - lastTime).TotalMilliseconds < 300) {
                var personnel = this.RolePersonnelList.SelectedItem as Personnel;
                this.RolePersonnels.Remove(personnel);
                if(this.isFilter == true)
                    this.FilterEnterprisePersonnels.Add(personnel);
                else
                    this.EnterprisePersonnels.Add(personnel);
            }
            lastTime = DateTime.Now;
        }

        public PersonnelSelectPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            if(!this.initialized) {
                this.initialized = true;
                this.Initializer();
            }
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new System.NotImplementedException();
        }
    }
}
