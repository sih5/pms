﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.Custom {
    public partial class RoleEditControl {
        public UserControl ParentPage;
        private string roleEditTitle;
        private Role role;
        private ObservableCollection<Role> roles;
        private IEnumerable<Personnel> users;
        private bool isEditMode;
        private ObservableCollection<KeyValuePair> kvStatus, kvUserStatus;

        public string RoleEditTitle {
            get {
                return this.roleEditTitle;
            }
            set {
                this.roleEditTitle = value;
                this.OnPropertyChanged("RoleEditTitle");
            }
        }

        public Role Role {
            get {
                return this.role;
            }
            set {
                this.role = value;
                this.OnPropertyChanged("Role");
            }
        }

        public ObservableCollection<Role> Roles {
            get {
                return this.roles;
            }
            set {
                this.roles = value;
                this.OnPropertyChanged("Roles");
            }
        }

        public IEnumerable<Personnel> Users {
            get {
                return this.users;
            }
            set {
                this.users = value;
                this.OnPropertyChanged("Users");
            }
        }

        public bool IsEditMode {
            get {
                return this.isEditMode;
            }
            private set {
                this.isEditMode = value;
                this.OnPropertyChanged("IsEditMode");
            }
        }

        public RoleEditControl() {
            this.InitializeComponent();
        }

        private void btnCommit_Click(object sender, RoutedEventArgs e) {
            var authorizationMessage = this.PageAuthorizationTabItem.AddAuthenticationData();
            if(authorizationMessage != null) {
                UIHelper.ShowAlertMessage(authorizationMessage);
                return;
            }

            var roleUserList = this.Role.RolePersonnels.ToList();
            foreach(var roleUser in roleUserList)
                if(!this.userGridView.SelectedItems.Cast<Personnel>().Select(user => user.Id).Contains(roleUser.PersonnelId))
                    this.SecurityDomainContext.RolePersonnels.Remove(roleUser);
            foreach(var user in this.userGridView.SelectedItems.Cast<Personnel>())
                if(!roleUserList.Select(item => item.PersonnelId).Contains(user.Id))
                    this.Role.RolePersonnels.Add(new RolePersonnel {
                        Personnel = user
                    });

            ((IEditableObject)this.Role).EndEdit();
            this.SecurityDomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    UIHelper.ShowAlertMessage(submitOp.Error.Message);
                    submitOp.MarkErrorAsHandled();
                } else {
                    if(this.IsEditMode)
                        UIHelper.ShowNotification(SecurityUIStrings.CustomControl_Role_Edit_Success);
                    else {
                        UIHelper.ShowNotification(SecurityUIStrings.CustomControl_Role_Add_Success);
                        this.Roles.Add(this.Role);
                    }
                    ((RoleMngControl)this.ParentPage).ReloadRoleMngControl(this.Role);
                    ShellViewModel.Current.IsRibbonMinimized = false;
                }
            }, null);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e) {
            this.SecurityDomainContext.RejectChanges();
            ((RoleMngControl)this.ParentPage).ReloadRoleMngControl(this.Role);
            ShellViewModel.Current.IsRibbonMinimized = false;
        }

        private void SecurityUserControlBase_Loaded(object sender, RoutedEventArgs e) {
            this.IsEditMode = this.Role.EntityState != EntityState.Detached && this.Role.EntityState != EntityState.New;
            if(this.Role.EntityState == EntityState.Detached)
                this.SecurityDomainContext.Roles.Add(this.Role);

            if(DesignerProperties.IsInDesignTool)
                return;

            this.RoleEditTitle = !this.IsEditMode ? SecurityUIStrings.CustomControl_Role_RoleAddTitle : SecurityUIStrings.CustomControl_Role_RoleEditTitle;

            KeyValueManager.GetKeyValuePairs(new Dictionary<string, ObservableCollection<KeyValuePair>> {
                {
                                                 "User_Status", this.kvUserStatus = new ObservableCollection<KeyValuePair>()
                                                 }
            }, () => {
                foreach(var column in this.userGridView.Columns) {
                    if(column.GetType() != typeof(GridViewComboBoxColumn))
                        continue;
                    var gridViewComboBoxColumn = (GridViewComboBoxColumn)column;
                    switch(gridViewComboBoxColumn.DataMemberBinding.Path.Path) {
                        case "Status":
                            gridViewComboBoxColumn.ItemsSource = this.kvUserStatus;
                            break;
                    }
                }
            });
            this.PageAuthorizationTabItem.SetCurrentRole(this.Role);
            if(this.IsEditMode) {
                this.userGridView.SelectedItems.Clear();
                foreach(var user in this.Users)
                    if(this.Role.RolePersonnels.Select(r => r.PersonnelId).Contains(user.Id))
                        this.userGridView.SelectedItems.Add(user);
            }
        }

        private void RoleDataForm_Loaded(object sender, RoutedEventArgs e) {
            KeyValueManager.GetKeyValuePairs(new Dictionary<string, ObservableCollection<KeyValuePair>> {
                {
                                                 "Common_Status", this.kvStatus = new ObservableCollection<KeyValuePair>()
                                                 }
            }, () => {
                foreach(var comboBox in this.roleDataForm.FindChildrenByType<RadComboBox>()) {
                    var bindingExpression = comboBox.GetBindingExpression(Selector.SelectedValueProperty);
                    if(bindingExpression == null)
                        continue;
                    switch(bindingExpression.ParentBinding.Path.Path) {
                        case "Status":
                            comboBox.ItemsSource = this.kvStatus;
                            break;
                    }
                }
            });
        }
    }
}
