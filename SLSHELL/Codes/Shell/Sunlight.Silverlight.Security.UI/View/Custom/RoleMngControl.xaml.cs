﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Media.Imaging;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.RibbonBar;

namespace Sunlight.Silverlight.Security.View.Custom {
    public partial class RoleMngControl : ICustomTabsView {
        private ObservableCollection<Role> roles;
        private IEnumerable<Personnel> users;
        private ObservableCollection<KeyValuePair> kvStatus, kvUserStatus;
        private readonly int currentEnterpriseId = BaseApp.Current.CurrentUserData.EnterpriseId;
        private RadRibbonTab ribbonTab;

        public ObservableCollection<Role> Roles {
            get {
                return this.roles ?? (this.roles = new ObservableCollection<Role>());
            }
            set {
                this.roles = value;
                this.OnPropertyChanged("Roles");
            }
        }

        public IEnumerable<Personnel> Users {
            get {
                return this.users ?? (this.users = Enumerable.Empty<Personnel>());
            }
            set {
                this.users = value;
                this.OnPropertyChanged("Users");
            }
        }

        private Role SelectRole {
            get {
                var selectedItem = this.roleGridView.SelectedItem;
                if(selectedItem == null)
                    return null;
                return selectedItem as Role;
            }
        }

        public RoleMngControl() {
            this.InitializeComponent();
            this.SecurityDomainContext = new SecurityDomainContext();
        }


        #region 接口实现方法
        public bool IsTabsReady {
            get {
                return true;
            }
        }

        public string RibbonContextualName {
            get {
                return null;
            }
        }

        public IEnumerable<RadRibbonTab> CustomTabs {
            get {
                if(this.ribbonTab == null) {
                    var addButton = new RadRibbonButton {
                        Text = "新增",
                        Name = "add",
                        Size = ButtonSize.Large,
                        LargeImage = new BitmapImage {
                            UriSource = Utils.MakeServerUri("Client/Dms/Images/Operations/Add.png")
                        },
                        FontSize = 12,
                        Padding = new Thickness(5, 10, 5, 0),
                    };
                    addButton.Click += this.AddButton_Click;
                    var editButton = new RadRibbonButton {
                        Text = "修改",
                        Name = "edit",
                        Size = ButtonSize.Large,
                        LargeImage = new BitmapImage {
                            UriSource = Utils.MakeServerUri("Client/Dms/Images/Operations/Edit.png")
                        },
                        FontSize = 12,
                        Padding = new Thickness(5, 10, 5, 0),
                    };
                    editButton.Click += this.EditButton_Click;
                    var abandonButton = new RadRibbonButton {
                        Text = "作废",
                        Name = "abandon",
                        Size = ButtonSize.Large,
                        LargeImage = new BitmapImage {
                            UriSource = Utils.MakeServerUri("Client/Dms/Images/Operations/Abandon.png")
                        },
                        FontSize = 12,
                        Padding = new Thickness(5, 10, 5, 0),
                    };
                    abandonButton.Click += this.AbandonButtonButton_Click;
                    var recoverButton = new RadRibbonButton {
                        Text = "恢复",
                        Name = "recover",
                        Size = ButtonSize.Large,
                        LargeImage = new BitmapImage {
                            UriSource = Utils.MakeServerUri("Client/Dms/Images/Operations/Rollback.png")
                        },
                        FontSize = 12,
                        Padding = new Thickness(5, 10, 5, 0),
                    };
                    recoverButton.Click += this.RecoverButtonButton_Click;

                    var group = new RadRibbonGroup {
                        Header = "基本操作",
                    };
                    group.Items.Add(addButton);
                    group.Items.Add(editButton);
                    group.Items.Add(abandonButton);
                    group.Items.Add(recoverButton);

                    this.ribbonTab = new RadRibbonTab {
                        Header = "数据操作",
                    };
                    this.ribbonTab.Items.Add(group);
                }
                return new[] {
                    this.ribbonTab
                };
            }
        }
        #endregion 接口实现方法


        #region 事件方法
        private void roleGridView_SelectionChanged(object sender, SelectionChangeEventArgs e) {
            if(this.SelectRole == null)
                return;

            var buttons = this.ribbonTab.Items.Cast<RadRibbonGroup>().First().Items.Cast<RadRibbonButton>();
            if(this.SelectRole.IsAdmin) {
                buttons.Single(button => button.Name == "edit").IsEnabled = false;
                buttons.Single(button => button.Name == "abandon").IsEnabled = false;
                buttons.Single(button => button.Name == "recover").IsEnabled = false;
            } else
                switch(this.SelectRole.Status) {
                    case (int)SecurityCommonStatus.有效:
                        buttons.Single(button => button.Name == "edit").IsEnabled = true;
                        buttons.Single(button => button.Name == "abandon").IsEnabled = true;
                        buttons.Single(button => button.Name == "recover").IsEnabled = false;
                        break;
                    case (int)SecurityCommonStatus.作废:
                        buttons.Single(button => button.Name == "edit").IsEnabled = false;
                        buttons.Single(button => button.Name == "abandon").IsEnabled = false;
                        buttons.Single(button => button.Name == "recover").IsEnabled = true;
                        break;
                }

            this.PageAuthorizationTabItem.SetCurrentRole(this.SelectRole);

            var roleUsers = new List<Personnel>();
            roleUsers.AddRange(this.Users.Where(u => this.SelectRole.RolePersonnels.Select(ru => ru.PersonnelId).Contains(u.Id)));
            roleUsers.Sort(CompareByName);
            this.userGridView.ItemsSource = roleUsers;
        }
        #endregion 事件方法


        #region 按钮操作方法
        private void AddButton_Click(object sender, RoutedEventArgs e) {
            var role = new Role {
                IsAdmin = false,
                Status = (int)SecurityCommonStatus.有效
            };
            var addWindow = new RoleEditControl {
                SecurityDomainContext = this.SecurityDomainContext,
                ParentPage = this,
                Role = role,
                Roles = this.Roles,
                Users = this.Users
            };
            this.MainTransition.Content = addWindow;
        }

        private void EditButton_Click(object sender, RoutedEventArgs e) {
            if(this.SelectRole == null || this.SelectRole.Id <= 0) {
                UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_Role_SelectRole);
                return;
            }
            var editWindow = new RoleEditControl {
                SecurityDomainContext = this.SecurityDomainContext,
                ParentPage = this,
                Role = this.SelectRole,
                Users = this.Users
            };
            this.MainTransition.Content = editWindow;
        }

        private void AbandonButtonButton_Click(object sender, RoutedEventArgs e) {
            if(this.SelectRole == null || this.SelectRole.Id <= 0) {
                UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_Role_SelectRole);
                return;
            }
            var abandoneWindow = new RoleOperationControl {
                SecurityDomainContext = this.SecurityDomainContext,
                ParentPage = this,
                RoleOperation = (int)SecurityRoleOperation.作废,
                Role = this.SelectRole,
                Users = this.Users
            };
            this.MainTransition.Content = abandoneWindow;
        }

        private void RecoverButtonButton_Click(object sender, RoutedEventArgs e) {
            if(this.SelectRole == null || this.SelectRole.Id <= 0) {
                UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_Role_SelectRole);
                return;
            }
            var recoverWindow = new RoleOperationControl {
                SecurityDomainContext = this.SecurityDomainContext,
                ParentPage = this,
                RoleOperation = (int)SecurityRoleOperation.恢复,
                Role = this.SelectRole,
                Users = this.Users
            };
            this.MainTransition.Content = recoverWindow;
        }
        #endregion 按钮操作方法


        #region 公共方法
        private static int CompareByName(Personnel xUser, Personnel yUser) {
            if(xUser == null)
                if(yUser == null)
                    return 0;
                else
                    return -1;
            if(yUser == null)
                return 1;
            var retval = xUser.Name.CompareTo(yUser.Name);
            return retval != 0 ? retval : xUser.Name.CompareTo(yUser.Name);
        }

        public void ReloadRoleMngControl(Role role) {
            this.MainTransition.Content = this.MainBusyIndicator;
            if(this.Roles.Contains(role)) {
                this.roleGridView.SelectedItem = null;
                this.roleGridView.SelectedItem = role;
                this.PageAuthorizationTabItem.SetCurrentRole(role);
            }
        }
        #endregion 公共方法


        private void SecurityUserControlBase_Loaded(object sender, RoutedEventArgs e) {
            KeyValueManager.GetKeyValuePairs(new Dictionary<string, ObservableCollection<KeyValuePair>> {
                {
                                                 "Common_Status", this.kvStatus = new ObservableCollection<KeyValuePair>()
                                                 },
                {
                                                 "User_Status", this.kvUserStatus = new ObservableCollection<KeyValuePair>()
                                                 }
            }, () => {
                foreach(var column in this.roleGridView.Columns) {
                    if(column.GetType() != typeof(GridViewComboBoxColumn))
                        continue;
                    var gridViewComboBoxColumn = (GridViewComboBoxColumn)column;
                    switch(gridViewComboBoxColumn.DataMemberBinding.Path.Path) {
                        case "Status":
                            gridViewComboBoxColumn.ItemsSource = this.kvStatus;
                            break;
                    }
                }
                foreach(var column in this.userGridView.Columns) {
                    if(column.GetType() != typeof(GridViewComboBoxColumn))
                        continue;
                    var gridViewComboBoxColumn = (GridViewComboBoxColumn)column;
                    switch(gridViewComboBoxColumn.DataMemberBinding.Path.Path) {
                        case "Status":
                            gridViewComboBoxColumn.ItemsSource = this.kvUserStatus;
                            break;
                    }
                }
            });

            this.SecurityDomainContext.Load(this.SecurityDomainContext.GetRolesByQuery(-1, this.currentEnterpriseId, "", null, -1), loadOp => {
                if(!loadOp.HasError && loadOp.Entities.Any()) {
                    this.Roles.Clear();
                    foreach(var role in loadOp.Entities.ToArray())
                        this.Roles.Add(role);
                }
            }, null);

            this.SecurityDomainContext.Load(this.SecurityDomainContext.GetUsersByQuery(-1, this.currentEnterpriseId, "", "", "", -1).Where(user => user.Status != (int)SecurityUserStatus.作废).OrderBy(u => u.Name), loadOp => {
                if(!loadOp.HasError && loadOp.Entities.Any())
                    this.Users = loadOp.Entities;
            }, null);
        }
    }
}
