﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.Custom {
    public partial class RoleOperationControl {
        public UserControl ParentPage;
        private int roleOperation;
        private string roleOperationTitle;
        private Role role;
        private IEnumerable<Personnel> users;
        private ObservableCollection<KeyValuePair> kvStatus, kvUserStatus;

        public int RoleOperation {
            get {
                return this.roleOperation;
            }
            set {
                this.roleOperation = value;
                this.OnPropertyChanged("RoleOperation");
            }
        }

        public string RoleOperationTitle {
            get {
                return this.roleOperationTitle;
            }
            set {
                this.roleOperationTitle = value;
                this.OnPropertyChanged("RoleOperationTitle");
            }
        }

        public Role Role {
            get {
                return this.role;
            }
            set {
                this.role = value;
                this.OnPropertyChanged("Role");
            }
        }

        public IEnumerable<Personnel> Users {
            get {
                return this.users;
            }
            set {
                this.users = value;
                this.OnPropertyChanged("Users");
            }
        }

        public RoleOperationControl() {
            this.InitializeComponent();
        }

        private void btnCommit_Click(object sender, RoutedEventArgs e) {
            switch(this.RoleOperation) {
                case (int)SecurityRoleOperation.作废:
                    this.Role.Status = (int)SecurityCommonStatus.作废;
                    break;
                case (int)SecurityRoleOperation.恢复:
                    this.Role.Status = (int)SecurityCommonStatus.有效;
                    break;
            }
            ((IEditableObject)this.Role).EndEdit();
            this.SecurityDomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    UIHelper.ShowAlertMessage(submitOp.Error.Message);
                    submitOp.MarkErrorAsHandled();
                } else {
                    switch(this.RoleOperation) {
                        case (int)SecurityRoleOperation.作废:
                            UIHelper.ShowNotification(SecurityUIStrings.CustomControl_Role_Abandon_Success);
                            break;
                        case (int)SecurityRoleOperation.恢复:
                            UIHelper.ShowNotification(SecurityUIStrings.CustomControl_Role_Recover_Success);
                            break;
                    }
                    ((RoleMngControl)this.ParentPage).ReloadRoleMngControl(this.Role);
                    ShellViewModel.Current.IsRibbonMinimized = false;
                }
            }, null);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e) {
            this.SecurityDomainContext.RejectChanges();
            ((RoleMngControl)this.ParentPage).ReloadRoleMngControl(this.Role);
            ShellViewModel.Current.IsRibbonMinimized = false;
        }

        private void SecurityUserControlBase_Loaded(object sender, RoutedEventArgs e) {
            if(DesignerProperties.IsInDesignTool)
                return;

            switch(this.RoleOperation) {
                case (int)SecurityRoleOperation.作废:
                    this.RoleOperationTitle = SecurityUIStrings.CustomControl_Role_RoleAbandonTitle;
                    break;
                case (int)SecurityRoleOperation.恢复:
                    this.RoleOperationTitle = SecurityUIStrings.CustomControl_Role_RoleRecoverTitle;
                    break;
            }

            KeyValueManager.GetKeyValuePairs(new Dictionary<string, ObservableCollection<KeyValuePair>> {
                {
                                                 "User_Status", this.kvUserStatus = new ObservableCollection<KeyValuePair>()
                                                 }
            }, () => {
                foreach(var column in this.userGridView.Columns) {
                    if(column.GetType() != typeof(GridViewComboBoxColumn))
                        continue;
                    var gridViewComboBoxColumn = (GridViewComboBoxColumn)column;
                    switch(gridViewComboBoxColumn.DataMemberBinding.Path.Path) {
                        case "Status":
                            gridViewComboBoxColumn.ItemsSource = this.kvUserStatus;
                            break;
                    }
                }
            });

            this.PageAuthorizationTabItem.SetCurrentRole(this.Role);
            var roleUsers = new List<Personnel>();
            roleUsers.AddRange(this.Users.Where(u => this.Role.RolePersonnels.Select(ru => ru.PersonnelId).Contains(u.Id)));
            this.userGridView.ItemsSource = roleUsers;
        }

        private void RoleDataForm_Loaded(object sender, RoutedEventArgs e) {
            KeyValueManager.GetKeyValuePairs(new Dictionary<string, ObservableCollection<KeyValuePair>> {
                {
                                                 "Common_Status", this.kvStatus = new ObservableCollection<KeyValuePair>()
                                                 }
            }, () => {
                foreach(var comboBox in this.roleDataForm.FindChildrenByType<RadComboBox>()) {
                    var bindingExpression = comboBox.GetBindingExpression(Selector.SelectedValueProperty);
                    if(bindingExpression == null)
                        continue;
                    switch(bindingExpression.ParentBinding.Path.Path) {
                        case "Status":
                            comboBox.ItemsSource = this.kvStatus;
                            break;
                    }
                }
            });
        }
    }

    public enum SecurityRoleOperation {
        作废 = 0,
        恢复 = 1
    }
}
