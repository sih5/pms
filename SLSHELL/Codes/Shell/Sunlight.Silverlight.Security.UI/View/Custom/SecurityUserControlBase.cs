﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.ViewModel.Contract;

namespace Sunlight.Silverlight.Security.View.Custom {
    ///<summary>
    ///    用户自定义控件基类 该类已实现INotifyPropertyChanged接口，可作为控件的DataContext使用。
    ///</summary>
    public class SecurityUserControlBase : UserControl, INotifyPropertyChanged {
        public SecurityDomainContext SecurityDomainContext;
        public IDataGridViewModel ParentViewModel;

        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler Closed;

        protected void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public void OnClosed(EventArgs e) {
            var handler = this.Closed;
            if(handler != null)
                handler(this, e);
        }

        protected void Close() {
            if(this.ParentViewModel != null)
                this.ParentViewModel.Content = null;
            else
                this.OnClosed(EventArgs.Empty);
            ShellViewModel.Current.IsRibbonMinimized = false;
        }

        // ReSharper disable PossibleMultipleEnumeration
        ///<summary>
        ///    在GridView控件中删除(多条)记录。 若GridView.ItemsSource绑定源为EntityCollection，且该集合具有Composition属性，请在该控件的Deleting事件中调用本方法删除记录。
        ///</summary>
        ///<param name="items"> 被删除的记录 </param>
        ///<example>
        ///    <![CDATA[
        /// <code>
        /// public RadGridView_Deleting(object sender, GridViewDeletingEventArgs e) {
        ///    if(!e.Cancel) {
        ///        this.RemoveEntitiesFromGridView(e.Items.Cast<Entity>());
        ///    }
        /// }
        /// </code>
        /// ]]>
        ///</example>
        ///<remarks>
        ///    版本：WCF RIA Services 1.0 SP1 方法： <c>public void System.ServiceModel.DomainServices.Client.EntityCollection&lt;TEntity&gt;+ListCollectionViewProxy&lt;TEntity,T&gt;.Remove(Object)</c> BUG：若EntityCollection具有Composition属性，该方法试图在EntitySet中重复删除Entity，导致抛出 <c>InvalidOperationException(Resource.Entity_Not_In_Collection)</c>
        ///</remarks>
        protected void RemoveEntitiesFromGridView(IEnumerable<Entity> items) {
            if(!items.Any())
                return;
            var entityType = items.First().GetType();
            var entitySet = this.SecurityDomainContext.EntityContainer.GetEntitySet(entityType);
            foreach(var entity in items)
                entitySet.Remove(entity);
        }

        // ReSharper restore PossibleMultipleEnumeration

        public SecurityUserControlBase() {
            this.SetBinding(DataContextProperty, new Binding {
                RelativeSource = new RelativeSource(RelativeSourceMode.Self)
            });
            this.FontSize = 12;
            this.Resources.MergedDictionaries.Add(new ResourceDictionary {
                Source = new Uri("/Sunlight.Silverlight.Security.UI;component/Assets/Styles/MetroButton.xaml", UriKind.Relative)
            });
            this.Resources.MergedDictionaries.Add(new ResourceDictionary {
                Source = new Uri("/Sunlight.Silverlight.Security.UI;component/Assets/Styles/SecurityDataForm.xaml", UriKind.Relative)
            });
            this.Resources.MergedDictionaries.Add(new ResourceDictionary {
                Source = new Uri("/Sunlight.Silverlight.Security.UI;component/Assets/Styles/ToolbarButton.xaml", UriKind.Relative)
            });
        }
    }
}
