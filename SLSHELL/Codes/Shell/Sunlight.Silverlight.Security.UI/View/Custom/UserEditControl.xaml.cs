﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.Custom {
    public partial class UserEditControl {
        public UserControl ParentPage;
        private string userEditTitle;
        private Personnel user;
        private ObservableCollection<Personnel> users;
        private ObservableCollection<Organization> organizations;
        private IEnumerable<Role> roles;
        private bool isEditMode;
        private Visibility viewMode;
        private ObservableCollection<KeyValuePair> kvRoleStatus;
        private string defaultPassword;

        public string UserEditTitle {
            get {
                return this.userEditTitle;
            }
            set {
                this.userEditTitle = value;
                this.OnPropertyChanged("UserEditTitle");
            }
        }

        public Personnel User {
            get {
                return this.user;
            }
            set {
                this.user = value;
                this.OnPropertyChanged("User");
            }
        }

        public ObservableCollection<Personnel> Users {
            get {
                return this.users;
            }
            set {
                this.users = value;
                this.OnPropertyChanged("Users");
            }
        }

        public ObservableCollection<Organization> Organizations {
            get {
                return this.organizations;
            }
            set {
                this.organizations = value;
                this.OnPropertyChanged("Organizations");
            }
        }

        public IEnumerable<Role> Roles {
            get {
                return this.roles;
            }
            set {
                this.roles = value;
                this.OnPropertyChanged("Roles");
            }
        }

        public bool IsEditMode {
            get {
                return this.isEditMode;
            }
            private set {
                this.isEditMode = value;
                this.OnPropertyChanged("IsEditMode");
            }
        }

        public Visibility ViewMode {
            get {
                return this.viewMode;
            }
            set {
                this.viewMode = value;
                this.OnPropertyChanged("ViewMode");
            }
        }

        public UserEditControl() {
            this.InitializeComponent();
            this.defaultPassword = "";
        }

        private void btnCommit_Click(object sender, RoutedEventArgs e) {
            if(!this.IsEditMode && "admin".Equals(this.User.LoginId)) {
                UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_User_LoginIdNotAllowAdmin);
                return;
            }

            if(!this.IsEditMode) {
                if(!this.User.IsDefaultPassword) {
                    if(string.IsNullOrWhiteSpace(this.User.Password)) {
                        UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_User_PasswordNotNull);
                        return;
                    }
                    if(this.User.Password.Equals(BaseApp.Current.CurrentUserData.EnterpriseCode)) {
                        UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_User_PasswordNotEqualsCurrentEnterpriseCode);
                        return;
                    }
                    if(this.User.Password.Length < 6 || this.User.Password.Length > 20) {
                        UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_User_PasswordLenghtIsError);
                        return;
                    }
                } else
                    this.User.Password = this.defaultPassword;
                if(this.User.CanUpdateUserPassword)
                    this.User.UpdateUserPassword(SecurityManager.HashPassword(this.User.Password));
            }

            var organnizationUserList = this.User.OrganizationPersonnels.ToList();
            foreach(var organnizationUser in organnizationUserList.Where(organnizationUser => !this.organizationTreeView.CheckedItems.Cast<RadTreeViewItem>().Select(radTreeViewItem => (radTreeViewItem.Tag as Organization).Id).Contains(organnizationUser.OrganizationId)))
                this.SecurityDomainContext.OrganizationPersonnels.Remove(organnizationUser);
            foreach(var organization in this.organizationTreeView.CheckedItems.Cast<RadTreeViewItem>().Select(radTreeViewItem => radTreeViewItem.Tag as Organization).Where(organization => !organnizationUserList.Select(item => item.OrganizationId).Contains(organization.Id)))
                this.User.OrganizationPersonnels.Add(new OrganizationPersonnel {
                    Organization = organization
                });

            var roleUserList = this.User.RolePersonnels.ToList();
            foreach(var roleUser in roleUserList)
                if(!this.roleGridView.SelectedItems.Cast<Role>().Select(role => role.Id).Contains(roleUser.RoleId))
                    this.SecurityDomainContext.RolePersonnels.Remove(roleUser);
            foreach(var role in this.roleGridView.SelectedItems.Cast<Role>())
                if(!roleUserList.Select(item => item.RoleId).Contains(role.Id))
                    this.User.RolePersonnels.Add(new RolePersonnel {
                        Role = role
                    });

            ((IEditableObject)this.User).EndEdit();
            this.SecurityDomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    UIHelper.ShowAlertMessage(submitOp.Error.Message);
                    submitOp.MarkErrorAsHandled();
                } else {
                    if(this.IsEditMode)
                        UIHelper.ShowNotification(SecurityUIStrings.CustomControl_User_Edit_Success);
                    else {
                        UIHelper.ShowNotification(SecurityUIStrings.CustomControl_User_Add_Success);
                        this.Users.Add(this.User);
                    }
                    ((UserMngControl)this.ParentPage).ReloadUserMngControl(this.User);
                    ShellViewModel.Current.IsRibbonMinimized = false;
                }
            }, null);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e) {
            this.SecurityDomainContext.RejectChanges();
            ((UserMngControl)this.ParentPage).ReloadUserMngControl(this.User);
            ShellViewModel.Current.IsRibbonMinimized = false;
        }

        private void SecurityUserControlBase_Loaded(object sender, RoutedEventArgs e) {
            this.IsEditMode = this.User.EntityState != EntityState.Detached && this.User.EntityState != EntityState.New;
            if(this.User.EntityState == EntityState.Detached)
                this.SecurityDomainContext.Personnels.Add(this.User);

            if(DesignerProperties.IsInDesignTool)
                return;

            this.UserEditTitle = !this.IsEditMode ? SecurityUIStrings.CustomControl_User_UserAddTitle : SecurityUIStrings.CustomControl_User_UserEditTitle;

            KeyValueManager.GetKeyValuePairs(new Dictionary<string, ObservableCollection<KeyValuePair>> {
                {
                                                 "Common_Status", this.kvRoleStatus = new ObservableCollection<KeyValuePair>()
                                                 }
            }, () => {
                foreach(var column in this.roleGridView.Columns) {
                    if(column.GetType() != typeof(GridViewComboBoxColumn))
                        continue;
                    var gridViewComboBoxColumn = (GridViewComboBoxColumn)column;
                    switch(gridViewComboBoxColumn.DataMemberBinding.Path.Path) {
                        case "Status":
                            gridViewComboBoxColumn.ItemsSource = this.kvRoleStatus;
                            break;
                    }
                }
            });

            this.SecurityDomainContext.Load(this.SecurityDomainContext.GetPasswordPolicysByQuery(-1, true), loadOp => {
                if(!loadOp.HasError && loadOp.Entities.Any())
                    this.defaultPassword = loadOp.Entities.First().DefaultPassword;
            }, null);

            this.PopulateItems(0, this.organizationTreeView.Items);

            if(this.IsEditMode) {
                this.roleGridView.SelectedItems.Clear();
                foreach(var role in this.Roles)
                    if(this.User.RolePersonnels.Select(r => r.RoleId).Contains(role.Id))
                        this.roleGridView.SelectedItems.Add(role);
            }
        }

        private void PopulateItems(int id, ItemCollection items) {
            foreach(var organization in this.Organizations.Where(o => o.ParentId == id).OrderBy(o => o.Sequence)) {
                var text = new StringBuilder(organization.Name).Append("(").Append(organization.Code).Append(")");
                var item = new RadTreeViewItem {
                    Header = text,
                    Tag = organization,
                    IsExpanded = true
                };
                items.Add(item);
                this.PopulateItems(organization.Id, item.Items);
            }
        }

        private void OrganizationTreeView_Loaded(object sender, RoutedEventArgs e) {
            if(this.IsEditMode)
                foreach(var item in this.organizationTreeView.ChildrenOfType<RadTreeViewItem>())
                    item.IsChecked = this.User.OrganizationPersonnels.Select(r => r.OrganizationId).Contains(((Organization)item.Tag).Id);
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e) {
            if(!this.isEditMode)
                if(this.User.IsDefaultPassword) {
                    this.User.Password = "";
                    this.userDataForm.FindChildByType<PasswordBox>().IsEnabled = false;
                } else
                    this.userDataForm.FindChildByType<PasswordBox>().IsEnabled = true;
        }
    }
}
