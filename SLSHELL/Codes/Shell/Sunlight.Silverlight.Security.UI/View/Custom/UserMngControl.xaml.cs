﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.RibbonBar;

namespace Sunlight.Silverlight.Security.View.Custom {
    public partial class UserMngControl : ICustomTabsView {
        private ObservableCollection<Personnel> users;
        private ObservableCollection<Organization> organizations;
        private IEnumerable<Role> roles;
        private ObservableCollection<KeyValuePair> kvStatus, kvRoleStatus;
        private readonly int currentEnterpriseId = BaseApp.Current.CurrentUserData.EnterpriseId;
        private RadRibbonTab ribbonTab;

        public ObservableCollection<Personnel> Users {
            get {
                return this.users ?? (this.users = new ObservableCollection<Personnel>());
            }
            set {
                this.users = value;
                this.OnPropertyChanged("Users");
            }
        }

        public ObservableCollection<Organization> Organizations {
            get {
                return this.organizations ?? (this.organizations = new ObservableCollection<Organization>());
            }
            set {
                this.organizations = value;
                this.OnPropertyChanged("Organizations");
            }
        }

        public IEnumerable<Role> Roles {
            get {
                return this.roles ?? (this.roles = Enumerable.Empty<Role>());
            }
            set {
                this.roles = value;
                this.OnPropertyChanged("Roles");
            }
        }

        private Personnel SelectUser {
            get {
                var selectedItem = this.userGridView.SelectedItem;
                if(selectedItem == null)
                    return null;
                return selectedItem as Personnel;
            }
        }

        public UserMngControl() {
            this.InitializeComponent();
            this.SecurityDomainContext = new SecurityDomainContext();
        }


        #region 接口实现方法
        public bool IsTabsReady {
            get {
                return true;
            }
        }

        public string RibbonContextualName {
            get {
                return null;
            }
        }

        public IEnumerable<RadRibbonTab> CustomTabs {
            get {
                if(this.ribbonTab == null) {
                    //var addButton = new RadRibbonButton {
                    //    Text = "新增",
                    //    Name = "add",
                    //    Size = ButtonSize.Large,
                    //    LargeImage = new BitmapImage {
                    //        UriSource = Core.Utils.MakeServerUri("Client/Dms/Images/Operations/Add.png")
                    //    },
                    //    FontSize = 12,
                    //    Padding = new Thickness(5, 10, 5, 0),
                    //};
                    //addButton.Click += this.AddButton_Click;
                    var editButton = new RadRibbonButton {
                        Text = "修改",
                        Name = "edit",
                        Size = ButtonSize.Large,
                        LargeImage = new BitmapImage {
                            UriSource = Utils.MakeServerUri("Client/Dms/Images/Operations/Edit.png")
                        },
                        FontSize = 12,
                        Padding = new Thickness(5, 10, 5, 0),
                    };
                    editButton.Click += this.EditButton_Click;
                    var abandonButton = new RadRibbonButton {
                        Text = "作废",
                        Name = "abandon",
                        Size = ButtonSize.Large,
                        LargeImage = new BitmapImage {
                            UriSource = Utils.MakeServerUri("Client/Dms/Images/Operations/Abandon.png")
                        },
                        FontSize = 12,
                        Padding = new Thickness(5, 10, 5, 0),
                    };
                    abandonButton.Click += this.AbandonButtonButton_Click;
                    var freezeButton = new RadRibbonButton {
                        Text = "冻结",
                        Name = "freeze",
                        Size = ButtonSize.Large,
                        LargeImage = new BitmapImage {
                            UriSource = Utils.MakeServerUri("Client/Security/Images/Operations/Save.png")
                        },
                        FontSize = 12,
                        Padding = new Thickness(5, 10, 5, 0),
                    };
                    freezeButton.Click += this.FreezeButtonButtonButton_Click;
                    var recoverButton = new RadRibbonButton {
                        Text = "恢复",
                        Name = "recover",
                        Size = ButtonSize.Large,
                        LargeImage = new BitmapImage {
                            UriSource = Utils.MakeServerUri("Client/Dms/Images/Operations/Rollback.png")
                        },
                        FontSize = 12,
                        Padding = new Thickness(5, 10, 5, 0),
                    };
                    recoverButton.Click += this.RecoverButtonButton_Click;
                    //var modifyPasswordButton = new RadRibbonButton {
                    //    Text = "修改密码",
                    //    Name = "modifyPassword",
                    //    Size = ButtonSize.Large,
                    //    LargeImage = new BitmapImage {
                    //        UriSource = Core.Utils.MakeServerUri("Client/Security/Images/Operations/Save.png")
                    //    },
                    //    FontSize = 12,
                    //    Padding = new Thickness(5, 10, 5, 0),
                    //};
                    //modifyPasswordButton.Click += this.ModifyPasswordButtonButton_Click;

                    var group = new RadRibbonGroup {
                        Header = "基本操作",
                    };
                    //group.Items.Add(addButton);
                    group.Items.Add(editButton);
                    group.Items.Add(abandonButton);
                    group.Items.Add(freezeButton);
                    group.Items.Add(recoverButton);
                    //group.Items.Add(modifyPasswordButton);

                    this.ribbonTab = new RadRibbonTab {
                        Header = "数据操作",
                    };
                    this.ribbonTab.Items.Add(group);
                }
                return new[] {
                    this.ribbonTab
                };
            }
        }
        #endregion 接口实现方法


        #region 事件方法
        private void userGridView_SelectionChanged(object sender, SelectionChangeEventArgs e) {
            if(this.SelectUser != null) {
                var buttons = this.ribbonTab.Items.Cast<RadRibbonGroup>().First().Items.Cast<RadRibbonButton>();
                switch(this.SelectUser.Status) {
                    case (int)SecurityUserStatus.有效:
                        buttons.Single(button => button.Name == "edit").IsEnabled = true;
                        buttons.Single(button => button.Name == "abandon").IsEnabled = true;
                        buttons.Single(button => button.Name == "freeze").IsEnabled = true;
                        buttons.Single(button => button.Name == "recover").IsEnabled = false;
                        //buttons.Single(button => button.Name == "modifyPassword").IsEnabled = true;
                        break;
                    case (int)SecurityUserStatus.冻结:
                        buttons.Single(button => button.Name == "edit").IsEnabled = true;
                        buttons.Single(button => button.Name == "abandon").IsEnabled = false;
                        buttons.Single(button => button.Name == "freeze").IsEnabled = false;
                        buttons.Single(button => button.Name == "recover").IsEnabled = true;
                        //buttons.Single(button => button.Name == "modifyPassword").IsEnabled = false;
                        break;
                    default:
                        buttons.Single(button => button.Name == "edit").IsEnabled = false;
                        buttons.Single(button => button.Name == "abandon").IsEnabled = false;
                        buttons.Single(button => button.Name == "freeze").IsEnabled = false;
                        buttons.Single(button => button.Name == "recover").IsEnabled = false;
                        //buttons.Single(button => button.Name == "modifyPassword").IsEnabled = false;
                        break;
                }

                foreach(var item in this.organizationTreeView.ChildrenOfType<RadTreeViewItem>())
                    item.IsChecked = this.SelectUser.OrganizationPersonnels.Select(r => r.OrganizationId).Contains((item.Tag as Organization).Id);

                var userRoles = new List<Role>();
                userRoles.AddRange(this.Roles.Where(r => this.SelectUser.RolePersonnels.Select(ru => ru.RoleId).Contains(r.Id)));
                this.roleGridView.ItemsSource = userRoles;
            }
        }
        #endregion 事件方法


        #region 按钮操作方法
        //private void AddButton_Click(object sender, RoutedEventArgs e) {
        //    var user = new User {
        //        Status = (int)SecurityUserStatus.有效
        //    };
        //    var addWindow = new UserEditControl {
        //        SecurityDomainContext = this.SecurityDomainContext,
        //        ParentPage = this,
        //        User = user,
        //        Users = this.Users,
        //        Organizations = this.Organizations,
        //        Roles = this.Roles,
        //        ViewMode = System.Windows.Visibility.Visible
        //    };
        //    this.MainTransition.Content = addWindow;
        //}

        private void EditButton_Click(object sender, RoutedEventArgs e) {
            if(this.SelectUser == null || this.SelectUser.Id <= 0) {
                UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_User_SelectUser);
                return;
            }
            var editWindow = new UserEditControl {
                SecurityDomainContext = this.SecurityDomainContext,
                ParentPage = this,
                User = this.SelectUser,
                Organizations = this.Organizations,
                Roles = this.Roles,
                ViewMode = Visibility.Collapsed
            };
            this.MainTransition.Content = editWindow;
        }

        private void AbandonButtonButton_Click(object sender, RoutedEventArgs e) {
            if(this.SelectUser == null || this.SelectUser.Id <= 0) {
                UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_User_SelectUser);
                return;
            }
            var abandoneWindow = new UserOperationControl {
                SecurityDomainContext = this.SecurityDomainContext,
                ParentPage = this,
                UserOperation = (int)SecurityUserOperation.作废,
                User = this.SelectUser,
                Organizations = this.Organizations,
                Roles = this.Roles
            };
            this.MainTransition.Content = abandoneWindow;
        }

        private void FreezeButtonButtonButton_Click(object sender, RoutedEventArgs e) {
            if(this.SelectUser == null || this.SelectUser.Id <= 0) {
                UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_User_SelectUser);
                return;
            }
            var freezeWindow = new UserOperationControl {
                SecurityDomainContext = this.SecurityDomainContext,
                ParentPage = this,
                UserOperation = (int)SecurityUserOperation.冻结,
                User = this.SelectUser,
                Organizations = this.Organizations,
                Roles = this.Roles
            };
            this.MainTransition.Content = freezeWindow;
        }

        private void RecoverButtonButton_Click(object sender, RoutedEventArgs e) {
            if(this.SelectUser == null || this.SelectUser.Id <= 0) {
                UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_User_SelectUser);
                return;
            }
            var recoverWindow = new UserOperationControl {
                SecurityDomainContext = this.SecurityDomainContext,
                ParentPage = this,
                UserOperation = (int)SecurityUserOperation.恢复,
                User = this.SelectUser,
                Organizations = this.Organizations,
                Roles = this.Roles
            };
            this.MainTransition.Content = recoverWindow;
        }

        //private void ModifyPasswordButtonButton_Click(object sender, RoutedEventArgs e) {
        //    if(this.SelectUser == null || this.SelectUser.Id <= 0) {
        //        UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_User_SelectUser);
        //        return;
        //    }
        //    var modifyPasswordWindow = new UserModifyPasswordControl {
        //        SecurityDomainContext = this.SecurityDomainContext,
        //        ParentPage = this,
        //        User = this.SelectUser,
        //        Organizations = this.Organizations,
        //        Roles = this.Roles
        //    };
        //    this.MainTransition.Content = modifyPasswordWindow;
        //}
        #endregion 按钮操作方法


        #region 公共方法
        private void PopulateItems(IEnumerable<Organization> organizations, int id, ItemCollection items) {
            foreach(var organization in organizations.Where(o => o.ParentId == id).OrderBy(o => o.Sequence)) {
                var text = new StringBuilder(organization.Name).Append("(").Append(organization.Code).Append(")");
                var item = new RadTreeViewItem {
                    Header = text,
                    Tag = organization,
                    IsExpanded = true
                };
                items.Add(item);
                this.PopulateItems(organizations, organization.Id, item.Items);
            }
        }

        public void ReloadUserMngControl(Personnel user) {
            this.MainTransition.Content = this.MainBusyIndicator;
            if(this.Users.Contains(user)) {
                this.userGridView.SelectedItem = null;
                this.userGridView.SelectedItem = user;
            }
        }
        #endregion 公共方法


        private void SecurityUserControlBase_Loaded(object sender, RoutedEventArgs e) {
            KeyValueManager.GetKeyValuePairs(new Dictionary<string, ObservableCollection<KeyValuePair>> {
                {
                                                 "User_Status", this.kvStatus = new ObservableCollection<KeyValuePair>()
                                                 },
                {
                                                 "Common_Status", this.kvRoleStatus = new ObservableCollection<KeyValuePair>()
                                                 }
            }, () => {
                foreach(var column in this.userGridView.Columns) {
                    if(column.GetType() != typeof(GridViewComboBoxColumn))
                        continue;
                    var gridViewComboBoxColumn = (GridViewComboBoxColumn)column;
                    switch(gridViewComboBoxColumn.DataMemberBinding.Path.Path) {
                        case "Status":
                            gridViewComboBoxColumn.ItemsSource = this.kvStatus;
                            break;
                    }
                }
                foreach(var column in this.roleGridView.Columns) {
                    if(column.GetType() != typeof(GridViewComboBoxColumn))
                        continue;
                    var gridViewComboBoxColumn = (GridViewComboBoxColumn)column;
                    switch(gridViewComboBoxColumn.DataMemberBinding.Path.Path) {
                        case "Status":
                            gridViewComboBoxColumn.ItemsSource = this.kvRoleStatus;
                            break;
                    }
                }
            });

            this.SecurityDomainContext.Load(this.SecurityDomainContext.GetUsersByQuery(-1, this.currentEnterpriseId, "", "", "", -1), loadOp => {
                if(!loadOp.HasError && loadOp.Entities.Any()) {
                    this.Users.Clear();
                    foreach(var user in loadOp.Entities.ToArray())
                        this.Users.Add(user);
                }
            }, null);

            this.SecurityDomainContext.Load(this.SecurityDomainContext.GetOrganizationsByQuery(-1, "", "", -1, -1, this.currentEnterpriseId).Where(organization => organization.Status != (int)SecurityCommonStatus.作废), loadOp => {
                if(!loadOp.HasError && loadOp.Entities.Any()) {
                    this.Organizations.Clear();
                    foreach(var organization in loadOp.Entities.ToArray())
                        this.Organizations.Add(organization);
                    this.PopulateItems(this.Organizations, 0, this.organizationTreeView.Items);
                }
            }, null);

            this.SecurityDomainContext.Load(this.SecurityDomainContext.GetRolesByQuery(-1, this.currentEnterpriseId, "", null, -1).Where(organization => organization.Status != (int)SecurityCommonStatus.作废), loadOp => {
                if(!loadOp.HasError && loadOp.Entities.Any())
                    this.Roles = loadOp.Entities;
            }, null);
        }
    }
}
