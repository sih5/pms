﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.Custom {
    public partial class UserModifyPasswordControl {
        public UserControl ParentPage;
        private Personnel user;
        private ObservableCollection<Organization> organizations;
        private IEnumerable<Role> roles;
        private ObservableCollection<KeyValuePair> kvRoleStatus;

        public Personnel User {
            get {
                return this.user;
            }
            set {
                this.user = value;
                this.OnPropertyChanged("User");
            }
        }

        public ObservableCollection<Organization> Organizations {
            get {
                return this.organizations;
            }
            set {
                this.organizations = value;
                this.OnPropertyChanged("Organizations");
            }
        }

        public IEnumerable<Role> Roles {
            get {
                return this.roles;
            }
            set {
                this.roles = value;
                this.OnPropertyChanged("Roles");
            }
        }

        public UserModifyPasswordControl() {
            this.InitializeComponent();
        }

        private void btnCommit_Click(object sender, RoutedEventArgs e) {
            if(string.IsNullOrWhiteSpace(this.User.Password)) {
                UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_User_PasswordNotNull);
                return;
            }
            if(this.User.Password.Equals(BaseApp.Current.CurrentUserData.EnterpriseCode)) {
                UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_User_PasswordNotEqualsCurrentEnterpriseCode);
                return;
            }
            if(this.User.Password.Length < 6 || this.User.Password.Length > 20) {
                UIHelper.ShowAlertMessage(SecurityUIStrings.CustomControl_User_PasswordLenghtIsError);
                return;
            }

            if(this.User.CanUpdateUserPassword)
                this.User.UpdateUserPassword(SecurityManager.HashPassword(this.User.Password));

            ((IEditableObject)this.User).EndEdit();
            this.SecurityDomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    UIHelper.ShowAlertMessage(submitOp.Error.Message);
                    submitOp.MarkErrorAsHandled();
                } else {
                    UIHelper.ShowNotification(SecurityUIStrings.CustomControl_User_ModifyPasssword_Success);
                    ((UserMngControl)this.ParentPage).ReloadUserMngControl(this.User);
                    ShellViewModel.Current.IsRibbonMinimized = false;
                }
            }, null);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e) {
            this.SecurityDomainContext.RejectChanges();
            ((UserMngControl)this.ParentPage).ReloadUserMngControl(this.User);
            ShellViewModel.Current.IsRibbonMinimized = false;
        }

        private void SecurityUserControlBase_Loaded(object sender, RoutedEventArgs e) {
            if(DesignerProperties.IsInDesignTool)
                return;

            KeyValueManager.GetKeyValuePairs(new Dictionary<string, ObservableCollection<KeyValuePair>> {
                {
                                                 "Common_Status", this.kvRoleStatus = new ObservableCollection<KeyValuePair>()
                                                 }
            }, () => {
                foreach(var column in this.roleGridView.Columns) {
                    if(column.GetType() != typeof(GridViewComboBoxColumn))
                        continue;
                    var gridViewComboBoxColumn = (GridViewComboBoxColumn)column;
                    switch(gridViewComboBoxColumn.DataMemberBinding.Path.Path) {
                        case "Status":
                            gridViewComboBoxColumn.ItemsSource = this.kvRoleStatus;
                            break;
                    }
                }
            });

            this.PopulateItems(0, this.organizationTreeView.Items);

            var userRoles = new List<Role>();
            userRoles.AddRange(this.Roles.Where(r => this.User.RolePersonnels.Select(ru => ru.RoleId).Contains(r.Id)));
            this.roleGridView.ItemsSource = userRoles;
        }

        private void PopulateItems(int id, ItemCollection items) {
            foreach(var organization in this.Organizations.Where(o => o.ParentId == id).OrderBy(o => o.Sequence)) {
                var text = new StringBuilder(organization.Name).Append("(").Append(organization.Code).Append(")");
                var item = new RadTreeViewItem {
                    Header = text,
                    Tag = organization,
                    IsExpanded = true
                };
                items.Add(item);
                this.PopulateItems(organization.Id, item.Items);
            }
        }

        private void OrganizationTreeView_Loaded(object sender, RoutedEventArgs e) {
            foreach(var item in this.organizationTreeView.ChildrenOfType<RadTreeViewItem>())
                item.IsChecked = this.User.OrganizationPersonnels.Select(r => r.OrganizationId).Contains((item.Tag as Organization).Id);
        }
    }
}
