﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.Custom {
    public partial class UserOperationControl {
        public UserControl ParentPage;
        private int userOperation;
        private string userOperationTitle;
        private Personnel user;
        private ObservableCollection<Organization> organizations;
        private IEnumerable<Role> roles;
        private ObservableCollection<KeyValuePair> kvRoleStatus;

        public int UserOperation {
            get {
                return this.userOperation;
            }
            set {
                this.userOperation = value;
                this.OnPropertyChanged("UserOperation");
            }
        }

        public string UserOperationTitle {
            get {
                return this.userOperationTitle;
            }
            set {
                this.userOperationTitle = value;
                this.OnPropertyChanged("UserOperationTitle");
            }
        }

        public Personnel User {
            get {
                return this.user;
            }
            set {
                this.user = value;
                this.OnPropertyChanged("User");
            }
        }

        public ObservableCollection<Organization> Organizations {
            get {
                return this.organizations;
            }
            set {
                this.organizations = value;
                this.OnPropertyChanged("Organizations");
            }
        }

        public IEnumerable<Role> Roles {
            get {
                return this.roles;
            }
            set {
                this.roles = value;
                this.OnPropertyChanged("Roles");
            }
        }

        public UserOperationControl() {
            this.InitializeComponent();
        }

        private void btnCommit_Click(object sender, RoutedEventArgs e) {
            switch(this.UserOperation) {
                case (int)SecurityUserOperation.作废:
                    this.User.Status = (int)SecurityUserStatus.作废;
                    var organnizationUserList = this.User.OrganizationPersonnels.ToList();
                    foreach(var organnizationUser in organnizationUserList)
                        this.SecurityDomainContext.OrganizationPersonnels.Remove(organnizationUser);
                    var roleUserList = this.User.RolePersonnels.ToList();
                    foreach(var roleUser in roleUserList)
                        this.SecurityDomainContext.RolePersonnels.Remove(roleUser);
                    break;
                case (int)SecurityUserOperation.冻结:
                    this.User.Status = (int)SecurityUserStatus.冻结;
                    break;
                case (int)SecurityUserOperation.恢复:
                    this.User.Status = (int)SecurityUserStatus.有效;
                    break;
            }
            ((IEditableObject)this.User).EndEdit();
            this.SecurityDomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    UIHelper.ShowAlertMessage(submitOp.Error.Message);
                    submitOp.MarkErrorAsHandled();
                } else {
                    switch(this.UserOperation) {
                        case (int)SecurityUserOperation.作废:
                            UIHelper.ShowNotification(SecurityUIStrings.CustomControl_User_Abandon_Success);
                            break;
                        case (int)SecurityUserOperation.冻结:
                            UIHelper.ShowNotification(SecurityUIStrings.CustomControl_User_Freeze_Success);
                            break;
                        case (int)SecurityUserOperation.恢复:
                            UIHelper.ShowNotification(SecurityUIStrings.CustomControl_User_Recover_Success);
                            break;
                    }
                    ((UserMngControl)this.ParentPage).ReloadUserMngControl(this.User);
                    ShellViewModel.Current.IsRibbonMinimized = false;
                }
            }, null);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e) {
            this.SecurityDomainContext.RejectChanges();
            ((UserMngControl)this.ParentPage).ReloadUserMngControl(this.User);
            ShellViewModel.Current.IsRibbonMinimized = false;
        }

        private void SecurityUserControlBase_Loaded(object sender, RoutedEventArgs e) {
            if(DesignerProperties.IsInDesignTool)
                return;

            switch(this.UserOperation) {
                case (int)SecurityUserOperation.作废:
                    this.UserOperationTitle = SecurityUIStrings.CustomControl_User_UserAbandonTitle;
                    break;
                case (int)SecurityUserOperation.冻结:
                    this.UserOperationTitle = SecurityUIStrings.CustomControl_User_UserFreezeTitle;
                    break;
                case (int)SecurityUserOperation.恢复:
                    this.UserOperationTitle = SecurityUIStrings.CustomControl_User_UserRecoverTitle;
                    break;
            }

            KeyValueManager.GetKeyValuePairs(new Dictionary<string, ObservableCollection<KeyValuePair>> {
                {
                                                 "Common_Status", this.kvRoleStatus = new ObservableCollection<KeyValuePair>()
                                                 }
            }, () => {
                foreach(var column in this.roleGridView.Columns) {
                    if(column.GetType() != typeof(GridViewComboBoxColumn))
                        continue;
                    var gridViewComboBoxColumn = (GridViewComboBoxColumn)column;
                    switch(gridViewComboBoxColumn.DataMemberBinding.Path.Path) {
                        case "Status":
                            gridViewComboBoxColumn.ItemsSource = this.kvRoleStatus;
                            break;
                    }
                }
            });

            this.PopulateItems(0, this.organizationTreeView.Items);

            var userRoles = new List<Role>();
            userRoles.AddRange(this.Roles.Where(r => this.User.RolePersonnels.Select(ru => ru.RoleId).Contains(r.Id)));
            this.roleGridView.ItemsSource = userRoles;
        }

        private void PopulateItems(int id, ItemCollection items) {
            foreach(var organization in this.Organizations.Where(o => o.ParentId == id).OrderBy(o => o.Sequence)) {
                var text = new StringBuilder(organization.Name).Append("(").Append(organization.Code).Append(")");
                var item = new RadTreeViewItem {
                    Header = text,
                    Tag = organization,
                    IsExpanded = true
                };
                items.Add(item);
                this.PopulateItems(organization.Id, item.Items);
            }
        }

        private void OrganizationTreeView_Loaded(object sender, RoutedEventArgs e) {
            foreach(var item in this.organizationTreeView.ChildrenOfType<RadTreeViewItem>())
                item.IsChecked = this.User.OrganizationPersonnels.Select(r => r.OrganizationId).Contains(((Organization)item.Tag).Id);
        }
    }

    public enum SecurityUserOperation {
        作废 = 0,
        冻结 = 1,
        恢复 = 2
    }
}
