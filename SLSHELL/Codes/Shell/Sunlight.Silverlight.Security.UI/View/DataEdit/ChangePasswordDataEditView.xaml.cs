﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.View.DataEdit {
    public partial class ChangePasswordDataEditView {
        protected override string Title {
            get {
                return SecurityUIStrings.DataEditView_Personnel_ModifyPassword;
            }
        }

        protected override void OnEditSubmitting() {
            var personnel = this.DataContext as Personnel;
            if(personnel == null)
                return;
            personnel.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(personnel.Password))
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_NewPasswordIsNull, new[] {
                    "Password"
                }));
            else if(personnel.Password.Length < 6 || personnel.Password.Length > 20)
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_NewPasswordLengthIsError, new[] {
                    "Password"
                }));
            else if(personnel.Password == BaseApp.Current.CurrentUserData.EnterpriseCode)
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_NewPasswordEqualsEnterpriseCode, new[] {
                    "Password"
                }));
            if(personnel.ConfirmPassword != personnel.Password)
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_ConfirmPasswordNotEqualsPassword, new[] {
                    "ConfirmPassword"
                }));
            if(personnel.HasValidationErrors)
                return;
            ((IEditableObject)personnel).EndEdit();
            try {
                if(personnel.Can首次修改人员密码)
                    personnel.首次修改人员密码(personnel.Password, false, SecurityManager.HashPassword(personnel.OldPassword));
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }


        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public ChangePasswordDataEditView() {
            this.InitializeComponent();
            this.Loaded += ChangePasswordDataEditView_Loaded;
        }

        private void ChangePasswordDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetPersonnelsQuery().Where(ex => ex.Id == BaseApp.Current.CurrentUserData.UserId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }


    }
}