﻿
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.ViewModel;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.View.DataEdit {
    public partial class EntNodeTemplateAddDataEditView {
        private readonly AuthorizeViewModel authorizeViewModel = new AuthorizeViewModel();

        protected override string BusinessName {
            get {
                return SecurityUIStrings.BusinessName_EntNodeTemplate;
            }
        }

        private void EntNodeTemplateAddDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            this.InitMainAuthorizeView();
        }

        private void InitMainAuthorizeView() {
            this.DomainContext.Load(this.DomainContext.GetPagesWithDetailQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                this.authorizeViewModel.Initialize(loadOp.Entities, loadOp.AllEntities.Where(entity => entity is Node).Cast<Node>());
                this.MainAuthorizeView.DataContext = this.authorizeViewModel;
                this.MainAuthorizeView.MainTabControl.SelectedIndex = 0;
            }, null);
        }

        protected void CreateUI() {
            var dataEditPanel = DI.GetDataEditPanel("EntNodeTemplate");
            dataEditPanel.SetValue(Grid.RowProperty, 1);
            this.EntNodeTemplateGrid.Children.Add(dataEditPanel);
            this.Root.Children.Add(this.CreateVerticalLine(1));
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var entNodeTemplate = this.DataContext as EntNodeTemplate;
            if(entNodeTemplate == null)
                return;
            var selectedNodes = this.authorizeViewModel.SelectedNodes;
            foreach(var node in selectedNodes)
                entNodeTemplate.EntNodeTemplateDetails.Add(new EntNodeTemplateDetail {
                    EntNodeTemplateId = entNodeTemplate.Id,
                    NodeId = node.Id
                });
            ((IEditableObject)entNodeTemplate).EndEdit();
            base.OnEditSubmitting();
        }

        public EntNodeTemplateAddDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += this.EntNodeTemplateAddDataEditView_Loaded;
            this.Initializer.Register(this.InitMainAuthorizeView);
        }
    }
}
