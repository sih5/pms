﻿using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.ViewModel;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security.View.DataEdit {
    public partial class EntNodeTemplateDataEditView {
        private DataGridViewBase enterpriseDataGridView;
        private readonly AuthorizeViewModel authorizeViewModel = new AuthorizeViewModel();
        private bool initialized;

        public bool Initialized {
            get {
                return this.initialized;
            }
            private set {
                if(this.initialized != value) {
                    this.initialized = value;
                    this.OnPropertyChanged("Initialized");
                }
            }
        }

        public DataGridViewBase EnterpriseDataGridView {
            get {
                return this.enterpriseDataGridView ?? (this.enterpriseDataGridView = DI.GetDataGridView("EnterpriseForTab"));
            }
        }

        protected override string BusinessName {
            get {
                return SecurityUIStrings.BusinessName_EntNodeTemplate;
            }
        }

        private void InitMainAuthorizeView() {
            this.DomainContext.Load(this.DomainContext.GetPagesWithDetailQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                this.authorizeViewModel.Initialize(loadOp.Entities, loadOp.AllEntities.Where(entity => entity is Node).Cast<Node>());
                this.MainAuthorizeView.DataContext = this.authorizeViewModel;
                this.MainAuthorizeView.MainTabControl.SelectedIndex = 0;
                this.Initialized = true;
            }, null);
        }

        private void SetMainAuthorizeView() {
            var template = this.DataContext as EntNodeTemplate;
            if(template == null || template.Id == default(int))
                return;
            this.DomainContext.Load(this.DomainContext.GetEntNodeTemplatesWithDetailByIdQuery(template.Id), LoadBehavior.RefreshCurrent, loadOp => {
                var entTemplate = loadOp.Entities.FirstOrDefault();
                if(entTemplate == null)
                    return;
                this.authorizeViewModel.SelectedNodes = entTemplate.EntNodeTemplateDetails.ToList().Select(entity => entity.Node).Where(node => node != null);
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetEntNodeTemplatesQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
                this.DelayCall(view => view.Initialized, view => view.SetMainAuthorizeView());
            }, null);
        }

        protected override void Reset() {
            this.authorizeViewModel.SelectedNodes = Enumerable.Empty<Node>();
        }

        protected void CreateUI() {
            var dataEditPanel = DI.GetDataEditPanel("EntNodeTemplate");
            dataEditPanel.SetValue(Grid.RowProperty, 1);
            this.EntNodeTemplateGrid.Children.Add(dataEditPanel);
            this.Root.Children.Add(this.CreateVerticalLine(1));
            this.EnterprisesTab.Content = this.EnterpriseDataGridView;
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var entNodeTemplate = this.DataContext as EntNodeTemplate;
            if(entNodeTemplate == null)
                return;
            var selectedNodeIds = this.authorizeViewModel.SelectedNodes.Select(node => node.Id).ToList();
            var template = this.DomainContext.EntNodeTemplates.Single(v => v.Id == entNodeTemplate.Id);
            var details = template.EntNodeTemplateDetails.Where(detail => detail.EntityState == EntityState.Unmodified).ToList();
            // LINQ to Objects 中，进行 Contains 判断的集合元素应尽量使用简单类型，若元素为 Entity 子类则 Equals 执行效率很低。
            var removed = details.Where(detail => !selectedNodeIds.Contains(detail.NodeId));
            var added = selectedNodeIds.Except(details.Select(detail => detail.NodeId)).Select(nodeId => new EntNodeTemplateDetail {
                EntNodeTemplateId = template.Id,
                NodeId = nodeId
            });
            foreach(var detail in removed)
                entNodeTemplate.EntNodeTemplateDetails.Remove(detail);
            foreach(var detail in added)
                entNodeTemplate.EntNodeTemplateDetails.Add(detail);
            ((IEditableObject)entNodeTemplate).EndEdit();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public EntNodeTemplateDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Initializer.Register(this.InitMainAuthorizeView);
        }
    }
}
