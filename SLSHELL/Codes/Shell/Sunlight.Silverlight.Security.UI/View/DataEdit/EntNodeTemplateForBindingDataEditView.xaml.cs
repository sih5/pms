﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.View.DataGrid;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security.View.DataEdit {
    public partial class EntNodeTemplateForBindingDataEditView {
        private DataGridViewBase enterpriseDataGridView;
        private readonly List<Enterprise> selectedEnterprises = new List<Enterprise>();

        public DataGridViewBase EnterpriseDataGridView {
            get {
                if(this.enterpriseDataGridView == null) {
                    this.enterpriseDataGridView = DI.GetDataGridView("EnterpriseForBinding");
                    this.enterpriseDataGridView.DomainContext = this.DomainContext;
                }
                return this.enterpriseDataGridView;
            }
        }

        protected override string Title {
            get {
                return SecurityUIStrings.DataEditView_Title_EntNodeTemplate_BindingEnterprise;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetEntNodeTemplatesQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
                this.DomainContext.Load(this.DomainContext.GetEnterprisesQuery().Where(e => e.EntNodeTemplateId == entity.Id), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        if(!loadOp1.IsErrorHandled)
                            loadOp1.MarkErrorAsHandled();
                        SecurityUtils.ShowDomainServiceOperationWindow(loadOp1);
                        return;
                    }
                    var grid = this.EnterpriseDataGridView as EnterpriseForBindingDataGridView;
                    if(grid == null)
                        return;
                    grid.SelectedEnterprises.Clear();
                    this.selectedEnterprises.Clear();
                    grid.SelectedEnterprises.AddRange(loadOp1.Entities);
                    this.selectedEnterprises.AddRange(loadOp1.Entities);
                }, null);
            }, null);
        }

        private void CreateUI() {
            this.EnterprisesTab.Content = this.EnterpriseDataGridView;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var entNodeTemplate = this.DataContext as EntNodeTemplate;
            if(entNodeTemplate == null)
                return;
            var grid = this.EnterpriseDataGridView as EnterpriseForBindingDataGridView;
            if(grid == null)
                return;
            foreach(var enterprise in this.selectedEnterprises) {
                enterprise.EntNodeTemplateId = null;
                ((IEditableObject)enterprise).EndEdit();
            }
            foreach(var entity in this.EnterpriseDataGridView.SelectedEntities) {
                if(grid.SelectedEnterprises.Contains(entity))
                    grid.SelectedEnterprises.Remove(entity);
                grid.SelectedEnterprises.AddRange(this.EnterpriseDataGridView.SelectedEntities);
            }
            foreach(var enterprise in grid.SelectedEnterprises.Cast<Enterprise>()) {
                if(enterprise == null)
                    return;
                enterprise.EntNodeTemplateId = entNodeTemplate.Id;
                ((IEditableObject)enterprise).EndEdit();
            }
            base.OnEditSubmitting();
        }

        public EntNodeTemplateForBindingDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
    }
}
