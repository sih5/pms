﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Security.Controls;
using Sunlight.Silverlight.Security.Panels.DataEdit;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.DataEdit {
    public partial class EntOrganizationTplDataEditView {
        private EntOrganizationTplDetailTreeView entOrganizationTplDetailsTreeView;
        private SecurityDetailDataEditView detailDataEditView;
        private EntOrganizationTplDetailDataEditPanel detailDataEditPanel;
        private RadContextMenu contextMenu;
        private int ID = Int32.MaxValue;
        private ObservableCollection<EntOrganizationTplDetail> DeleteDetails = new ObservableCollection<EntOrganizationTplDetail>();

        private RadTreeViewItem ClickedTreeViewItem {
            get {
                return this.ContextMenu.GetClickedElement<RadTreeViewItem>();
            }
        }

        private RadTreeViewItem SelectedTreeViewItem {
            get {
                return this.EntOrganizationTplDetailsTreeView.SelectedItem as RadTreeViewItem;
            }
        }

        private EntOrganizationTplDetailTreeView EntOrganizationTplDetailsTreeView {
            get {
                if(this.entOrganizationTplDetailsTreeView == null) {
                    this.entOrganizationTplDetailsTreeView = (EntOrganizationTplDetailTreeView)DI.GetDetailPanel("EntOrganizationTplDetail");
                    RadContextMenu.SetContextMenu(this.entOrganizationTplDetailsTreeView, this.ContextMenu);
                    this.entOrganizationTplDetailsTreeView.AddHandler(RadMenuItem.ClickEvent, new RoutedEventHandler(OnMenuItemClicked));
                    this.entOrganizationTplDetailsTreeView.DomainContext = this.DomainContext;
                    this.entOrganizationTplDetailsTreeView.IsDragDropEnabled = true;
                    this.entOrganizationTplDetailsTreeView.SelectionChanged += this.EntOrganizationTplDetailTreeView_SelectionChanged;
                    this.entOrganizationTplDetailsTreeView.PreviewDragStarted += this.EntOrganizationTplDetailTreeView_PreviewDragStarted;
                    this.entOrganizationTplDetailsTreeView.PreviewDragEnded += EntOrganizationTplDetailTreeView_PreviewDragEnded;
                }
                return this.entOrganizationTplDetailsTreeView;
            }
        }

        private SecurityDetailDataEditView DetailDataEditView {
            get {
                if(this.detailDataEditView == null) {
                    this.detailDataEditView = new SecurityDetailDataEditView();
                    this.detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(EntOrganizationTpl), "EntOrganizationTplDetails"), null, () => this.EntOrganizationTplDetailsTreeView);
                    return this.detailDataEditView;
                }
                return this.detailDataEditView;
            }
        }

        private EntOrganizationTplDetailDataEditPanel DetailDataEditPanel {
            get {
                return this.detailDataEditPanel ?? (this.detailDataEditPanel = (EntOrganizationTplDetailDataEditPanel)DI.GetDataEditPanel("EntOrganizationTplDetail"));
            }
        }

        private RadContextMenu ContextMenu {
            get {
                if(this.contextMenu == null) {
                    this.contextMenu = new RadContextMenu();
                    contextMenu.Items.Add(new RadMenuItem {
                        Header = SecurityUIStrings.CustomControl_EntOrganizationTpl_AddChild,
                        Name = "AddChild"
                    });
                    contextMenu.Items.Add(new RadMenuItem {
                        Header = SecurityUIStrings.CustomControl_EntOrganizationTpl_AbandonOrganization,
                        Name = "Abandon"
                    });
                    this.contextMenu.Opened += (sender, e) => {
                        if(this.ClickedTreeViewItem == null) {
                            this.ContextMenu.IsOpen = false;
                            return;
                        }
                        (this.ContextMenu.Items[1] as RadMenuItem).IsEnabled = this.ClickedTreeViewItem.ParentItem != null;
                    };
                }
                return this.contextMenu;
            }
        }

        #region 公共方法

        private void HandleTreeView(IEnumerable<object> items, EntOrganizationTpl entity) {
            var i = 1;
            foreach(var item in items.Cast<RadTreeViewItem>().OrderBy(item => item.Index)) {
                var detail = item.Tag as EntOrganizationTplDetail;
                entity.EntOrganizationTplDetails.Single(d => d.Id == detail.Id).Sequence = i++;
                this.HandleTreeView(item.Items, entity);
            }
        }

        private void DeleteEntity(IEnumerable<object> items, ObservableCollection<EntOrganizationTplDetail> deleteDetails) {
            foreach(var item in items.Cast<RadTreeViewItem>()) {
                deleteDetails.Add(item.Tag as EntOrganizationTplDetail);
                this.DeleteEntity(item.Items, deleteDetails);
            }
        }

        #endregion

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetEntOrganizationTplsWithDetailQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        private void CreateUI() {
            var dataEditPanel = DI.GetDataEditPanel("EntOrganizationTpl");
            dataEditPanel.SetValue(Grid.RowProperty, 1);
            this.EntOrganizationTplGrid.Children.Add(dataEditPanel);
            this.Root.Children.Add(this.CreateVerticalLine(1));
            this.DetailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(this.DetailDataEditView);
            this.Root.Children.Add(this.CreateVerticalLine(3));
            this.DetailDataEditPanel.SetValue(Grid.ColumnProperty, 4);
            this.Root.Children.Add(this.DetailDataEditPanel);
        }

        private void OnMenuItemClicked(object sender, RoutedEventArgs args) {
            var item = (args as RadRoutedEventArgs).OriginalSource as RadMenuItem;
            if((item == null))
                return;

            var entOrganizationTpl = this.DataContext as EntOrganizationTpl;
            switch(item.Name) {
                case "AddChild":
                    var detail = new EntOrganizationTplDetail {
                        Id = this.ID--,
                        ParentId = (this.ClickedTreeViewItem.Tag as EntOrganizationTplDetail).Id,
                        OrganizationType = (int)SecurityOrganizationOrganizationType.部门
                    };
                    entOrganizationTpl.EntOrganizationTplDetails.Add(detail);
                    this.ClickedTreeViewItem.Items.Add(new RadTreeViewItem {
                        Tag = detail,
                        IsSelected = true,
                        IsExpanded = true
                    });
                    detail.PropertyChanged += this.Detail_PropertyChanged;
                    this.DetailDataEditPanel.DataContext = detail;
                    break;
                case "Abandon":
                    this.DeleteDetails.Add(this.ClickedTreeViewItem.Tag as EntOrganizationTplDetail);
                    if(this.ClickedTreeViewItem.Items.Any())
                        this.DeleteEntity(this.ClickedTreeViewItem.Items, this.DeleteDetails);
                    this.ClickedTreeViewItem.ParentItem.Items.Remove(this.ClickedTreeViewItem);
                    this.DetailDataEditPanel.txtOrganizationCode.IsReadOnly = true;
                    this.DetailDataEditPanel.txtOrganizationName.IsReadOnly = true;
                    this.DetailDataEditPanel.DataContext = null;
                    break;
            }
        }

        private void Detail_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(this.SelectedTreeViewItem == null)
                return;
            var detail = this.SelectedTreeViewItem.Tag as EntOrganizationTplDetail;
            if(detail == null)
                return;
            if(e.PropertyName.Equals("OrganizationCode") || e.PropertyName.Equals("OrganizationName"))
                if(!string.IsNullOrEmpty(detail.OrganizationCode) && !string.IsNullOrEmpty(detail.OrganizationName))
                    this.SelectedTreeViewItem.Header = string.Format("{0} ({1})", detail.OrganizationName, detail.OrganizationCode);
        }

        private void EntOrganizationTplDetailTreeView_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            if(this.SelectedTreeViewItem == null)
                return;

            if(SelectedTreeViewItem.ParentItem == null) {
                this.DetailDataEditPanel.txtOrganizationCode.IsReadOnly = true;
                this.DetailDataEditPanel.txtOrganizationName.IsReadOnly = true;
            } else {
                this.DetailDataEditPanel.txtOrganizationCode.IsReadOnly = false;
                this.DetailDataEditPanel.txtOrganizationName.IsReadOnly = false;
            }

            this.DetailDataEditPanel.DataContext = this.SelectedTreeViewItem.Tag as EntOrganizationTplDetail;
        }

        private void EntOrganizationTplDetailTreeView_PreviewDragStarted(object sender, RadTreeViewDragEventArgs e) {
            var item = e.DraggedItems[0] as RadTreeViewItem;
            if(item == null)
                return;
            if(item.ParentItem == null)
                e.Handled = true;
        }

        private void EntOrganizationTplDetailTreeView_PreviewDragEnded(object sender, RadTreeViewDragEndedEventArgs e) {
            if(e.TargetDropItem == null || e.TargetDropItem.ParentItem == null)
                e.Handled = true;
        }

        private void EntOrganizationTpl_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var entOrganizationTpl = this.DataContext as EntOrganizationTpl;
            if(entOrganizationTpl == null)
                return;

            if(e.PropertyName.Equals("Code") || e.PropertyName.Equals("Name")) {
                if(!string.IsNullOrEmpty(entOrganizationTpl.Code) && !string.IsNullOrEmpty(entOrganizationTpl.Name)) {
                    if(!this.EntOrganizationTplDetailsTreeView.Items.Any()) {
                        var detail = new EntOrganizationTplDetail {
                            Id = this.ID--,
                            ParentId = -1,
                            Sequence = 1,
                            OrganizationCode = entOrganizationTpl.Code,
                            OrganizationName = entOrganizationTpl.Name,
                            OrganizationType = (int)SecurityOrganizationOrganizationType.企业
                        };
                        this.EntOrganizationTplDetailsTreeView.Items.Clear();
                        this.EntOrganizationTplDetailsTreeView.Items.Add(new RadTreeViewItem {
                            Header = string.Format("{0} ({1})", entOrganizationTpl.Name, entOrganizationTpl.Code),
                            Tag = detail,
                            IsExpanded = true
                        });
                    } else {
                        var root = this.EntOrganizationTplDetailsTreeView.Items[0] as RadTreeViewItem;
                        var rootEntity = (EntOrganizationTplDetail)root.Tag;
                        root.Header = string.Format("{0} ({1})", entOrganizationTpl.Name, entOrganizationTpl.Code);
                        rootEntity.OrganizationName = entOrganizationTpl.Name;
                        rootEntity.OrganizationCode = entOrganizationTpl.Code;
                    }
                }
            }
        }

        private void EntOrganizationTplDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DeleteDetails.Clear();
        }

        private void EntOrganizationTplDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var entOrganizationTpl = this.DataContext as EntOrganizationTpl;
            if(entOrganizationTpl == null)
                return;

            entOrganizationTpl.PropertyChanged -= this.EntOrganizationTpl_PropertyChanged;
            entOrganizationTpl.PropertyChanged += this.EntOrganizationTpl_PropertyChanged;
            this.DetailDataEditPanel.DataContext = null;
        }

        protected override string BusinessName {
            get {
                return SecurityUIStrings.BusinessName_EntOrganizationTpl;
            }
        }

        protected override void OnEditSubmitting() {
            var entOrganizationTpl = this.DataContext as EntOrganizationTpl;
            if(entOrganizationTpl == null)
                return;

            var root = this.EntOrganizationTplDetailsTreeView.Items.Cast<RadTreeViewItem>().SingleOrDefault();
            if(root == null)
                return;
            var validationErrors = new List<string>();
            if(!root.Items.Any())
                validationErrors.Add(string.Format(SecurityUIStrings.DataEditView_Error_EntOrganizationTpl_DetailNotAllowEmpty));
            if(validationErrors.Count > 0) {
                UIHelper.ShowNotification(string.Join(Environment.NewLine, validationErrors));
                return;
            }

            var rootEntity = (EntOrganizationTplDetail)root.Tag;
            if(rootEntity.ParentId == -1 && this.EditState == DataEditState.New)
                entOrganizationTpl.EntOrganizationTplDetails.Add(rootEntity);

            foreach(var detail in this.DeleteDetails)
                entOrganizationTpl.EntOrganizationTplDetails.Remove(detail);

            this.HandleTreeView(this.EntOrganizationTplDetailsTreeView.Items, entOrganizationTpl);

            ((IEditableObject)entOrganizationTpl).EndEdit();
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public EntOrganizationTplDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += this.EntOrganizationTplDataEditView_Loaded;
            this.DataContextChanged += this.EntOrganizationTplDataEditView_DataContextChanged;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}