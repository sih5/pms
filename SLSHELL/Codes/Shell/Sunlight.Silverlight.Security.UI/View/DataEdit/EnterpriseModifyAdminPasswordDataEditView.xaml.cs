﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.DataEdit {
    public partial class EnterpriseModifyAdminPasswordDataEditView {
        private void LoadEnterpriseAdminToEdit(Enterprise enterprise) {
            this.DomainContext.Load(this.DomainContext.获取企业管理员Query(enterprise.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(!loadOp.Entities.Any()) {
                    this.EnterpriseAdmin.ItemsSource = null;
                    this.CheckBoxEmail.IsChecked = false;
                    this.DataContext = null;
                    return;
                }

                this.EnterpriseAdmin.ItemsSource = loadOp.Entities;
                if(loadOp.Entities.Count() == 1)
                    this.EnterpriseAdmin.SelectedIndex = 0;
            }, null);
        }

        private void PersonnelDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var personnel = this.DataContext as Personnel;
            if(personnel == null) {
                this.CheckBoxEmail.IsEnabled = false;
                return;
            }
            if(string.IsNullOrEmpty(personnel.Email))
                this.CheckBoxEmail.IsEnabled = false;
            else
                this.CheckBoxEmail.IsEnabled = true;
        }

        private void EnterpriseAdmin_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if(e.AddedItems != null && e.AddedItems.Count > 0)
                this.DataContext = e.AddedItems.Cast<Personnel>().First();
        }

        protected override string Title {
            get {
                return SecurityUIStrings.DataEditView_Personnel_ModifyAdminPassword;
            }
        }

        protected override void OnEditSubmitting() {
            var personnel = this.DataContext as Personnel;
            if(personnel == null)
                return;

            personnel.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(personnel.Password))
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_NewPasswordIsNull, new[] {
                    "Password"
                }));
            else if(personnel.Password.Length < 6 || personnel.Password.Length > 20)
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_NewPasswordLengthIsError, new[] {
                    "Password"
                }));
            else if(personnel.Password == BaseApp.Current.CurrentUserData.EnterpriseCode)
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_NewPasswordEqualsEnterpriseCode, new[] {
                    "Password"
                }));
            if(personnel.ConfirmPassword != personnel.Password)
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_ConfirmPasswordNotEqualsPassword, new[] {
                    "ConfirmPassword"
                }));
            if(personnel.HasValidationErrors)
                return;

            bool needSendEmail = (bool)this.CheckBoxEmail.IsChecked && !string.IsNullOrEmpty(personnel.Email);
            ((IEditableObject)personnel).EndEdit();
            try {
                if(personnel.Can修改人员密码)
                    personnel.修改人员密码(personnel.Password, needSendEmail, null);
            } catch(ValidationException e) {
                personnel.ValidationErrors.Add(e.ValidationResult);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitted() {
            UIHelper.ShowNotification(SecurityUIStrings.DataEditView_Notification_ModifyAdminPasswordSuccess);
        }

        public EnterpriseModifyAdminPasswordDataEditView() {
            this.InitializeComponent();
            this.DataContextChanged += this.PersonnelDataEditView_DataContextChanged;
        }

        public void SetEnterprise(object enterprise) {
            this.EnterpriseCode.Text = ((Enterprise)enterprise).Code;
            this.EnterpriseName.Text = ((Enterprise)enterprise).Name;

            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEnterpriseAdminToEdit((Enterprise)enterprise));
            else
                this.LoadEnterpriseAdminToEdit((Enterprise)enterprise);
        }
    }
}
