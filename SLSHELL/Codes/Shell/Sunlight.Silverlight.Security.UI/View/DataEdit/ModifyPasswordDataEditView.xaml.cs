﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.View.DataEdit {
    public partial class ModifyPasswordDataEditView {
        private void ModifyPasswordDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetPersonnelsQuery().Where(p => p.Id == BaseApp.Current.CurrentUserData.UserId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    entity.PropertyChanged -= this.Personnel_PropertyChanged;
                    entity.PropertyChanged += this.Personnel_PropertyChanged;
                    this.SetObjectToEdit(entity);
                }
            }, null);
            this.DomainContext.Load(this.DomainContext.GetPasswordPoliciesQuery().Where(p => p.IsEnabled == true), loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities.Any()) {
                    var passwordPolicy = loadOp.Entities.First() as PasswordPolicy;
                    this.WarmPromptGrid.Visibility = Visibility.Visible;
                    this.WarmPrompt1.Text = string.Format(SecurityUIStrings.DataEditView_ModifyPassword_WarmPrompt1, passwordPolicy.ValidPeriod);
                    this.WarmPrompt2.Text = string.Format(SecurityUIStrings.DataEditView_ModifyPassword_WarmPrompt2, passwordPolicy.RemindDays);
                } else
                    this.WarmPromptGrid.Visibility = Visibility.Collapsed;
            }, null);
        }

        private void Personnel_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var personnel = this.DataContext as Personnel;
            switch(e.PropertyName) {
                case "OldPassword":
                    var oldPasswordError = personnel.ValidationErrors.FirstOrDefault(error => error.MemberNames.Contains("OldPassword"));
                    if(oldPasswordError != null)
                        personnel.ValidationErrors.Remove(oldPasswordError);
                    break;
                case "Password":
                    var passwordError = personnel.ValidationErrors.FirstOrDefault(error => error.MemberNames.Contains("Password"));
                    if(passwordError != null)
                        personnel.ValidationErrors.Remove(passwordError);
                    break;
                case "ConfirmPassword":
                    var confirmPasswordError = personnel.ValidationErrors.FirstOrDefault(error => error.MemberNames.Contains("ConfirmPassword"));
                    if(confirmPasswordError != null)
                        personnel.ValidationErrors.Remove(confirmPasswordError);
                    break;
            }
        }

        protected override string Title {
            get {
                return SecurityUIStrings.DataEditView_Personnel_ModifyPassword;
            }
        }

        protected override void OnEditSubmitting() {
            var personnel = this.DataContext as Personnel;
            if(personnel == null)
                return;

            personnel.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(personnel.OldPassword))
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_OldPasswordIsNull, new[] {
                    "OldPassword"
                }));
            if(string.IsNullOrEmpty(personnel.Password))
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_NewPasswordIsNull, new[] {
                    "Password"
                }));
            else if(personnel.Password.Length < 6 || personnel.Password.Length > 20)
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_NewPasswordLengthIsError, new[] {
                    "Password"
                }));
            else if(personnel.Password == BaseApp.Current.CurrentUserData.EnterpriseCode)
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_NewPasswordEqualsEnterpriseCode, new[] {
                    "Password"
                }));
            if(personnel.ConfirmPassword != personnel.Password)
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_ConfirmPasswordNotEqualsPassword, new[] {
                    "ConfirmPassword"
                }));
            if(personnel.HasValidationErrors)
                return;

            ((IEditableObject)personnel).EndEdit();
            try {
                if(personnel.Can修改人员密码)
                    personnel.修改人员密码(personnel.Password, false, SecurityManager.HashPassword(personnel.OldPassword));
            } catch(ValidationException e) {
                personnel.ValidationErrors.Add(e.ValidationResult);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public ModifyPasswordDataEditView() {
            this.InitializeComponent();
            this.Loaded += this.ModifyPasswordDataEditView_Loaded;
        }
    }
}
