﻿
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Sunlight.Silverlight.Security.Controls;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.DataEdit {
    public partial class OrganizationChangeSequenceDataEditView {
        private RadTreeView organizationTreeView;

        public RadTreeView OrganizationTreeView {
            get {
                if(this.organizationTreeView == null) {
                    this.organizationTreeView = new OrganizationTreeView {
                        DomainContext = this.DomainContext
                    };
                    this.organizationTreeView.IsDragDropEnabled = true;
                    this.organizationTreeView.PreviewDragEnded -= this.organizationTreeView_PreviewDragEnded;
                    this.organizationTreeView.PreviewDragEnded += this.organizationTreeView_PreviewDragEnded;
                    this.organizationTreeView.DragEnded -= this.organizationTreeView_DragEnded;
                    this.organizationTreeView.DragEnded += this.organizationTreeView_DragEnded;
                }
                return this.organizationTreeView;
            }
        }

        private void organizationTreeView_PreviewDragEnded(object sender, RadTreeViewDragEndedEventArgs e) {
            if(e.DraggedItems == null || !e.DraggedItems.Any() || ((RadTreeViewItem)e.DraggedItems[0]).ParentTreeView == null)
                return;

            var currentItem = (RadTreeViewItem)e.DraggedItems.First();
            var targetItem = e.TargetDropItem;
            if(targetItem == null) {
                e.Handled = true;
                return;
            }
            if(currentItem.Parent != targetItem.Parent || e.DropPosition == DropPosition.Inside)
                e.Handled = true;
            else
                e.Handled = false;
        }

        private void organizationTreeView_DragEnded(object sender, RadTreeViewDragEndedEventArgs e) {
            if(e.DraggedItems == null || !e.DraggedItems.Any() || ((RadTreeViewItem)e.DraggedItems[0]).ParentTreeView == null)
                return;

            var currentItem = (RadTreeViewItem)e.DraggedItems.First();
            var targetItem = e.TargetDropItem;

            switch(e.DropPosition) {
                case DropPosition.Before:
                    if(typeof(RadTreeViewItem) == targetItem.Parent.GetType())
                        ((RadTreeViewItem)currentItem.Parent).Items.Insert(targetItem.Index - 1, currentItem);
                    else
                        ((RadTreeView)currentItem.Parent).Items.Insert(targetItem.Index - 1, currentItem);
                    break;
                case DropPosition.After:
                    if(typeof(RadTreeViewItem) == targetItem.Parent.GetType())
                        ((RadTreeViewItem)currentItem.Parent).Items.Insert(targetItem.Index + 1, currentItem);
                    else
                        ((RadTreeView)currentItem.Parent).Items.Insert(targetItem.Index + 1, currentItem);
                    break;
            }
        }

        protected override string Title {
            get {
                return Security.Resources.SecurityUIStrings.DataEditView_Title_ChangeSequence_Organization;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(this.OrganizationTreeView);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            this.ChangeItemsSequence(this.organizationTreeView.Items);
            foreach(var item in this.OrganizationTreeView.Items)
                ((IEditableObject)(item as RadTreeViewItem).Tag).EndEdit();
            base.OnEditSubmitting();
        }

        private void ChangeItemsSequence(IEnumerable<object> items) {
            var i = 1;
            foreach(var item in items.Cast<RadTreeViewItem>().OrderBy(item => item.Index)) {
                ((Organization)item.Tag).Sequence = i++;
                foreach(var d in item.Items.Cast<RadTreeViewItem>()){
                    ((Organization)d.Tag).ParentId = ((Organization)item.Tag).Id;
                }
                this.ChangeItemsSequence(item.Items);
            }
        }

        public OrganizationChangeSequenceDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
    }
}
