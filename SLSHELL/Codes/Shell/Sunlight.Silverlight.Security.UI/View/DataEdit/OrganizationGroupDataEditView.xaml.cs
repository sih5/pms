﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.View.Custom;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security.View.DataEdit {
    public partial class OrganizationGroupDataEditView {
        private OrganizationPersonnelGroupSelectPanel personnelView;

        public OrganizationPersonnelGroupSelectPanel PersonnelSelectView {
            get {
                if(this.personnelView == null) {
                    this.personnelView = (OrganizationPersonnelGroupSelectPanel)DI.GetView("OrganizationPersonnelGroupSelectPanel");
                    this.personnelView.DomainContext = this.DomainContext;
                    var organization = this.DataContext as Organization;
                    if(organization == null)
                        return this.personnelView;
                    this.personnelView.EnterpriseId = organization.EnterpriseId;
                    this.personnelView.Loaded += personnelView_Loaded;
                }
                return this.personnelView;
            }
        }

        private void personnelView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var organization = this.DataContext as Organization;
            this.personnelView.EnterpriseId = organization.EnterpriseId;
            this.personnelView.CachePersonnels = null;
            this.personnelView.Refesh();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetOrganizationsWithDetailQuery().Where(o => o.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    PersonnelSelectView.EnterpriseId = entity.EnterpriseId;
                }
            }, null);
        }



        private void CreateUI() {
            var dataEditPanel = DI.GetDataEditPanel("Organization");
            dataEditPanel.SetValue(Grid.RowProperty, 1);
            this.OrganizationGrid.Children.Add(dataEditPanel);
            this.Root.Children.Add(this.CreateVerticalLine(1));
            this.PersonnelSelectView.SetValue(Grid.ColumnProperty, 2);
            //this.Loaded += OrganizationGroupDataEditView_Loaded;
            
            this.Root.Children.Add(this.PersonnelSelectView);
        }

        //private void OrganizationGroupDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            
        //}

        protected override string BusinessName {
            get {
                return SecurityUIStrings.BusinessName_Organization;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {

            var organization = this.DataContext as Organization;
            if(organization == null)
                return;

            organization.ValidationErrors.Clear();

            foreach(var entity in organization.OrganizationPersonnels)
                if(!this.PersonnelSelectView.OrganizationPersonnels.Any(e => e.Id == entity.PersonnelId))
                    this.DomainContext.OrganizationPersonnels.Remove(entity);
            foreach(var entity in this.PersonnelSelectView.OrganizationPersonnels)
                if(!organization.OrganizationPersonnels.Any(e => e.PersonnelId == entity.Id))
                    organization.OrganizationPersonnels.Add(new OrganizationPersonnel {
                        PersonnelId = entity.Id
                    });
            foreach(var cacheOrganization in this.DomainContext.Organizations.Where(v => v.EntityState == EntityState.Unmodified).ToList()) {
                this.DomainContext.Organizations.Detach(cacheOrganization);
            }
            ((IEditableObject)organization).EndEdit();
            try {
                if(this.EditState == DataEditState.New) {
                    if(organization.Can集中创建组织)
                        organization.集中创建组织();
                } else {
                    if(organization.Can调整组织)
                        organization.调整组织();
                }
            } catch(ValidationException e) {
                organization.ValidationErrors.Add(e.ValidationResult);
                return;
            }
            base.OnEditSubmitting();
        }

        public OrganizationGroupDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}