﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.View.Custom;
using Sunlight.Silverlight.Security.View.DataGrid;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.DataEdit {
    public partial class PersonnelDataEditView {
        private OrganizationTreeViewForPersonnel organizationTreeView;
        private DataGridViewBase roleDataGridView;
        private SecurityDetailDataEditView detailDataEditView;
        private RadUpload uploader;
        private BitmapImage avatar;

        protected override string BusinessName {
            get {
                return SecurityUIStrings.BusinessName_Personnel;
            }
        }

        private Personnel Personnel {
            get {
                var personnel = this.DataContext as Personnel;
                return personnel;
            }
        }

        /// <summary>
        ///     所属组织
        /// </summary>
        private OrganizationTreeViewForPersonnel OrganizationTreeView {
            get {
                if(this.organizationTreeView == null) {
                    this.organizationTreeView = new OrganizationTreeViewForPersonnel(true, OptionListType.CheckList);
                    this.organizationTreeView.DomainContext = this.DomainContext;
                }
                return this.organizationTreeView;
            }
        }

        /// <summary>
        ///     担任角色
        /// </summary>
        private DataGridViewBase RoleDataGridView {
            get {
                return this.roleDataGridView ?? (this.roleDataGridView = DI.GetDataGridView("RoleForEdit"));
            }
        }

        /// <summary>
        ///     关联实体编辑容器
        /// </summary>
        private SecurityDetailDataEditView DetailDataEditView {
            get {
                if(this.detailDataEditView == null) {
                    this.detailDataEditView = new SecurityDetailDataEditView();
                    this.detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(Personnel), "OrganizationPersonnels"), null, () => this.OrganizationTreeView);
                    this.detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(Personnel), "RolePersonnels"), null, () => this.RoleDataGridView);
                    return this.detailDataEditView;
                }
                return this.detailDataEditView;
            }
        }

        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload {
                        IsAppendFilesEnabled = false,
                        IsMultiselect = false,
                        Filter = SecurityUIStrings.DataEditView_Personnel_Filters,
                        UploadServiceUrl = SecurityUtils.GetUploadHandlerUrl().ToString(),
                    };
                    this.uploader.AdditionalPostFields.Add("Category", "Personnels");
                    this.uploader.FileUploadStarting += (source, e) => {
                        e.UploadData.FileName = Guid.NewGuid().ToString() + e.SelectedFile.File.Extension;
                    };
                    this.uploader.FileUploaded += (sender, e) => {
                        if(this.Personnel != null)
                            this.Personnel.Photo = e.HandlerData.CustomData["Path"].ToString();
                    };
                    this.uploader.UploadFinished += (sender, e) => {
                        if(this.DomainContext.IsSubmitting)
                            return;
                        this.Submit();
                    };
                    this.uploader.FileUploadFailed += (sender, e) => {
                        UIHelper.ShowAlertMessage(e.ErrorMessage);
                        e.Handled = true;
                    };
                }
                return this.uploader;
            }
        }

        private FileInfo SelectedAvatarFile {
            get {
                if(this.Uploader.CurrentSession != null && this.Uploader.CurrentSession.SelectedFiles.Any())
                    return this.uploader.CurrentSession.SelectedFiles.First().File;
                return null;
            }
        }

        public BitmapImage Avatar {
            get {
                if(this.avatar == null)
                    this.avatar = new BitmapImage();
                if(this.SelectedAvatarFile != null)
                    this.avatar.SetSource(this.SelectedAvatarFile.OpenRead());
                else if(this.Personnel != null)
                    this.avatar.UriSource = SecurityUtils.GetPersonnelPhotoUri(this.Personnel.Photo);
                return this.avatar;
            }
        }

        private void Avatar_Click(object sender, RoutedEventArgs e) {
            this.uploader.ShowFileDialog();
            if(this.SelectedAvatarFile != null)
                this.OnPropertyChanged("Avatar");
        }

        private void PasswordBox_OnKeyDown(object sender, KeyEventArgs e) {
            var passwordBox = sender as PasswordBox;
            if(passwordBox != null && passwordBox.Password == GlobalVar.ASSIGNED_BY_SERVER)
                passwordBox.Password = string.Empty;
        }

        private void Submit() {
            var personnel = this.DataContext as Personnel;
            if(personnel == null)
                return;

            ((IEditableObject)personnel).EndEdit();
            if(this.EditState == DataEditState.New)
                try {
                    if(personnel.Password == GlobalVar.ASSIGNED_BY_SERVER) {
                        if(personnel.Can设置人员默认密码)
                            personnel.设置人员默认密码();
                    } else if(personnel.Can修改人员密码)
                        personnel.修改人员密码(personnel.Password, false, null);
                } catch(ValidationException ex) {
                    personnel.ValidationErrors.Add(ex.ValidationResult);
                    return;
                } else
                try {
                    if(personnel.Can调整人员)
                        personnel.调整人员();
                } catch(ValidationException ex) {
                    personnel.ValidationErrors.Add(ex.ValidationResult);
                    return;
                }
            base.OnEditSubmitting();
        }

        private void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            this.DetailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(this.DetailDataEditView);
        }

        private void PersonnelDataEditView_Loaded(object sender, RoutedEventArgs routedEventArgs) {
            if(this.EditState == DataEditState.New)
                this.DomainContext.Load(this.DomainContext.GetDefaultPasswordPolicyQuery(), loadOp => {
                    if(this.Personnel == null)
                        return;
                    if(loadOp.HasError) {
                        this.Personnel.Password = string.Empty;
                        loadOp.MarkErrorAsHandled();
                    } else if(!loadOp.Entities.Any())
                        this.Personnel.Password = string.Empty;
                }, null);
        }

        private void PersonnelDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var personnel = e.NewValue as Personnel;
            if(personnel == null)
                return;
            personnel.PropertyChanged -= this.Personnel_PropertyChanged;
            personnel.PropertyChanged += this.Personnel_PropertyChanged;
            this.OnPropertyChanged("Avatar");
        }

        private void Personnel_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(e.PropertyName == "Photo")
                this.OnPropertyChanged("Avatar");
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPersonnelsWithOrganizationAndRoleQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
                var roleGridView = this.RoleDataGridView as RoleForEditDataGridView;
                if(roleGridView == null)
                    return;
                roleGridView.SelectedRoles.Clear();
                roleGridView.SelectedRoles.AddRange(entity.RolePersonnels.Select(r => r.Role));
            }, null);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var personnel = this.DataContext as Personnel;
            if(personnel == null)
                return;

            personnel.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(personnel.LoginId))
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_LoginIdIsNull, new[] {
                    "LoginId"
                }));
            else if(personnel.LoginId.ToLower() == "admin")
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_LoginIdEqualsAdmin, new[] {
                    "LoginId"
                }));
            if(string.IsNullOrWhiteSpace(personnel.Name))
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_NameIsNull, new[] {
                    "Name"
                }));
            if(this.EditState == DataEditState.New && personnel.Password != GlobalVar.ASSIGNED_BY_SERVER)
                if(string.IsNullOrWhiteSpace(personnel.Password))
                    personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_PasswordIsNull, new[] {
                        "Password"
                    }));
                else if(personnel.Password.Length < 6 || personnel.Password.Length > 20)
                    personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_PasswordLengthIsError, new[] {
                        "Password"
                    }));
                else if(personnel.Password == BaseApp.Current.CurrentUserData.EnterpriseCode)
                    personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_PasswordEqualsEnterpriseCode, new[] {
                        "Password"
                    }));

            var validationErrors = new List<string>();
            if(!this.OrganizationTreeView.CheckedItems.Cast<RadTreeViewItem>().Where(o=>o.CheckState == System.Windows.Automation.ToggleState.On).Any())
                validationErrors.Add(string.Format(SecurityUIStrings.DataEditView_Error_Personnel_OrganizationIsNull));
            if(validationErrors.Count > 0) {
                UIHelper.ShowNotification(string.Join(Environment.NewLine, validationErrors));
                return;
            }

            var orgPersonnelList = personnel.OrganizationPersonnels.ToList();
            foreach(var orgPersonnel in orgPersonnelList.Where(orgPersonnel => !this.OrganizationTreeView.CheckedItems.Cast<RadTreeViewItem>().Where(o => o.CheckState == System.Windows.Automation.ToggleState.On).Select(item => (item.Tag as Organization).Id).Contains(orgPersonnel.OrganizationId)))
                this.DomainContext.OrganizationPersonnels.Remove(orgPersonnel);
            foreach(var org in this.organizationTreeView.CheckedItems.Cast<RadTreeViewItem>().Where(o=>o.CheckState == System.Windows.Automation.ToggleState.On).Select(item => item.Tag as Organization).Where(org => !orgPersonnelList.Select(item => item.OrganizationId).Contains(org.Id)))
                personnel.OrganizationPersonnels.Add(new OrganizationPersonnel {
                    Organization = org
                });

            var roleGridView = this.RoleDataGridView as RoleForEditDataGridView;
            if(roleGridView != null) {
                var rolePersonnelList = personnel.RolePersonnels.ToList();
                foreach(var rolePersonnel in rolePersonnelList.Where(rolePersonnel => !roleGridView.SelectedRoles.Select(role => role.Id).Contains(rolePersonnel.RoleId)))
                    this.DomainContext.RolePersonnels.Remove(rolePersonnel);
                foreach(var role in roleGridView.SelectedRoles.Where(role => !rolePersonnelList.Select(item => item.RoleId).Contains(role.Id)))
                    personnel.RolePersonnels.Add(new RolePersonnel {
                        RoleId = role.Id,
                        PersonnelId = personnel.Id
                    });
            }
            if(this.SelectedAvatarFile == null)
                this.Submit();
            else
                this.uploader.StartUpload();
        }

        protected override void Reset() {
            var roleGridView = (this.Root.Children[2]).FindChildByType<RadGridView>();
            if(roleGridView != null)
                roleGridView.SelectedItems.Clear();
            this.Uploader.CurrentSession.SelectedFiles.Clear();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public PersonnelDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += this.PersonnelDataEditView_Loaded;
            this.DataContextChanged += this.PersonnelDataEditView_DataContextChanged;
        }
    }
}
