﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.View.DataEdit {
    public partial class PersonnelModifyPasswordDataEditView {
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPersonnelsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        private void PersonnelDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var personnel = this.DataContext as Personnel;
            if(personnel == null) {
                this.CheckBoxEmail.IsEnabled = false;
                return;
            }
            if(string.IsNullOrEmpty(personnel.Email))
                this.CheckBoxEmail.IsEnabled = false;
            else
                this.CheckBoxEmail.IsEnabled = true;
        }

        protected override string Title {
            get {
                return SecurityUIStrings.DataEditView_Personnel_ModifyPassword;
            }
        }

        protected override void OnEditSubmitting() {
            var personnel = this.DataContext as Personnel;
            if(personnel == null)
                return;

            personnel.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(personnel.Password))
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_NewPasswordIsNull, new[] {
                    "Password"
                }));
            else if(personnel.Password.Length < 6 || personnel.Password.Length > 20)
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_NewPasswordLengthIsError, new[] {
                    "Password"
                }));
            else if(personnel.Password == BaseApp.Current.CurrentUserData.EnterpriseCode)
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_NewPasswordEqualsEnterpriseCode, new[] {
                    "Password"
                }));
            if(personnel.ConfirmPassword != personnel.Password)
                personnel.ValidationErrors.Add(new ValidationResult(SecurityUIStrings.DataEditView_Validation_Personnel_ConfirmPasswordNotEqualsPassword, new[] {
                    "ConfirmPassword"
                }));
            if(personnel.HasValidationErrors)
                return;

            bool needSendEmail = (bool)this.CheckBoxEmail.IsChecked && !string.IsNullOrEmpty(personnel.Email);
            ((IEditableObject)personnel).EndEdit();
            try {
                if(personnel.Can修改人员密码)
                    personnel.修改人员密码(personnel.Password, needSendEmail, null);
            } catch(ValidationException e) {
                personnel.ValidationErrors.Add(e.ValidationResult);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public PersonnelModifyPasswordDataEditView() {
            this.InitializeComponent();
            this.DataContextChanged += this.PersonnelDataEditView_DataContextChanged;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
