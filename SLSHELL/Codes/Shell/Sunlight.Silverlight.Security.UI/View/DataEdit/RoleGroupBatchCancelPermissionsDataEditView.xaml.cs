﻿
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.ViewModel;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.DataEdit {
    public partial class RoleGroupBatchCancelPermissionsDataEditView {
        protected readonly AuthorizeViewModel authorizeViewModel = new AuthorizeViewModel();
        protected int[] roleIds;

        protected override string Title {
            get {
                return SecurityUIStrings.DataEditView_Title_RoleGroup_BatchCancelPermissions;
            }
        }

        public RoleGroupBatchCancelPermissionsDataEditView() {
            this.InitializeComponent();
        }
        private RadTabControl tabControl;

        private RadTabControl TabControl {
            get {
                if(this.tabControl == null)
                    this.tabControl = this.MainAuthorizeView.FindChildrenByType<RadTabControl>().SingleOrDefault();
                return tabControl;
            }
        }

        private void InitAuthorizeViewModel() {
            if(this.DomainContext == null)
                this.DomainContext = new SecurityDomainContext();
            this.DomainContext.Load(this.DomainContext.GetCurrentEnterprisePagesWithDetailByRoleIdQuery(roleIds[0]), LoadBehavior.RefreshCurrent, loadOp => {
                this.authorizeViewModel.Initialize(loadOp.Entities, loadOp.AllEntities.Where(entity => entity is Node).Cast<Node>());
                this.MainAuthorizeView.DataContext = this.authorizeViewModel;

                TabControl.SelectedIndex = 0;
            }, null);
        }

        protected override void Reset() {
            this.authorizeViewModel.SelectedNodes = Enumerable.Empty<Node>();
            roleIds = null;
            var radBusyIndicator = this.FindChildrenByType<RadBusyIndicator>().SingleOrDefault();
            if(radBusyIndicator == null)
                return;
            radBusyIndicator.IsBusy = false;
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var selectedNodeIds = this.authorizeViewModel.SelectedNodes.Select(node => node.Id).ToArray();
            this.DomainContext.批量删除授权规则(roleIds, selectedNodeIds);
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object ids) {
            var objIds = ids as object[];
            if(objIds == null)
                return;
            roleIds = new int[objIds.Length];
            for(var i = 0; i < objIds.Length; i++) {
                this.roleIds[i] = (int)objIds[i];
            }
            this.InitAuthorizeViewModel();
        }
    }
}
