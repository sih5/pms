﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.View.Custom;
using Sunlight.Silverlight.Security.ViewModel;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.DataEdit {
    public partial class RoleGroupDataEditView {
        private PersonnelGroupSelectPanel personnelSelectView;
        private readonly AuthorizeViewModel authorizeViewModel = new AuthorizeViewModel();
        private FrameworkElement roleDataEditPanel;
        private bool initialized;

        public bool Initialized {
            get {
                return this.initialized;
            }
            private set {
                if(this.initialized != value) {
                    this.initialized = value;
                    this.OnPropertyChanged("Initialized");
                }
            }
        }

        protected override string BusinessName {
            get {
                return SecurityUIStrings.BusinessName_Role;
            }
        }

        public FrameworkElement RoleDataEditPanel {
            get {
                return this.roleDataEditPanel ?? (this.roleDataEditPanel = DI.GetDataEditPanel("Role"));
            }
        }

        public PersonnelGroupSelectPanel PersonnelSelectView {
            get {
                if(this.personnelSelectView == null) {
                    this.personnelSelectView = (PersonnelGroupSelectPanel)DI.GetView("PersonnelGroupSelectPanel");
                    //this.personnelSelectView.DomainContext = this.DomainContext;//不放此处 因为初始化时候this.DomainContext 本身为null
                    //this.PersonnelSelectView.Loaded += PersonnelSelectView_Loaded;
                }
                return this.personnelSelectView;
            }
        }

        //private void PersonnelSelectView_Loaded(object sender, RoutedEventArgs e) {
        //    var role = this.DataContext as Role;
        //    this.personnelSelectView.EnterpriseId = role.EnterpriseId;
        //    this.personnelSelectView.CachePersonnels = null;
        //    this.personnelSelectView.Refesh();
        //}

        private void SetAuthorizeViewByRole() {
            var role = this.DataContext as Role;
            if(role == null || role.Id == default(int))
                return;

            this.DomainContext.Load(this.DomainContext.GetRulesByRoleQuery(role.Id), LoadBehavior.RefreshCurrent, loadOp => {
                this.authorizeViewModel.SelectedNodes = loadOp.Entities.Select(rule => rule.Node).Where(node => node != null);
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DataContext = null;
            this.DomainContext.Load(this.DomainContext.GetRolesWithDetailQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.PersonnelSelectView.EnterpriseId = entity.EnterpriseId;
                this.SetObjectToEdit(entity);
                this.InitAuthorizeViewModel();
            }, null);
        }

        private RadTabControl tabControl;

        private RadTabControl TabControl {
            get {
                if(this.tabControl == null)
                    this.tabControl = this.MainAuthorizeView.FindChildrenByType<RadTabControl>().SingleOrDefault();
                return tabControl;
            }
        }

        private void InitAuthorizeViewModel() {
            var role = this.DataContext as Role;
            if(role == null || this.DomainContext == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetCurrentEnterprisePagesWithDetailByIdQuery(role.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                this.authorizeViewModel.Initialize(loadOp.Entities, loadOp.AllEntities.Where(entity => entity is Node).Cast<Node>());
                this.MainAuthorizeView.DataContext = this.authorizeViewModel;
                this.Initialized = true;
                TabControl.SelectedIndex = 0;
                this.DelayCall(view => view.Initialized, view => view.SetAuthorizeViewByRole());
            }, null);
        }

        private void SecurityDataEditViewBase_Loaded(object sender, RoutedEventArgs e) {
            this.PersonnelSelectView.IsLoaded = false;
        }

        protected override void Reset() {
            this.authorizeViewModel.SelectedNodes = Enumerable.Empty<Node>();
            this.personnelSelectView.RolePersonnels.Clear();
            this.personnelSelectView.EnterprisePersonnels.Clear();
            this.personnelSelectView.CachePersonnels = null;
        }

        protected virtual void CreateUI() {
            this.RoleDataEditPanel.SetValue(Grid.RowProperty, 1);
            this.RoleGrid.Children.Add(this.RoleDataEditPanel);
            this.Root.Children.Add(this.CreateHorizontalLine(1));
            this.PersonnelTab.Content = this.PersonnelSelectView;
            InitAuthorizeViewModel();
        }

        private void role_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var role = this.DataContext as Role;
            if(role == null)
                return;
            switch(e.PropertyName) {
                case "EnterpriseId":
                    this.PersonnelSelectViewLoad();
                    this.InitAuthorizeViewModel();
                    break;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var role = this.DataContext as Role;
            if(role == null)
                return;

            role.ValidationErrors.Clear();
            var selectedNodeIds = this.authorizeViewModel.SelectedNodes.Select(node => node.Id).ToList();
            var rules = this.DomainContext.Rules.Where(rule => rule.RoleId == role.Id && rule.EntityState == EntityState.Unmodified).ToList();
            var removedRules = rules.Where(rule => !selectedNodeIds.Contains(rule.NodeId));
            var addedRules = selectedNodeIds.Except(rules.Select(rule => rule.NodeId)).Select(nodeId => new Rule {
                RoleId = role.Id,
                NodeId = nodeId
            });
            foreach(var rule in removedRules)
                this.DomainContext.Rules.Remove(rule);
            foreach(var rule in addedRules)
                this.DomainContext.Rules.Add(rule);
            ((IEditableObject)role).EndEdit();
            if(this.PersonnelSelectView.IsLoaded) {
                foreach(var entity in role.RolePersonnels.Where(entity => this.PersonnelSelectView.RolePersonnels.All(e => e.Id != entity.PersonnelId)))
                    this.DomainContext.RolePersonnels.Remove(entity);
                foreach(var entity in this.PersonnelSelectView.RolePersonnels.Where(entity => role.RolePersonnels.All(e => e.PersonnelId != entity.Id)))
                    role.RolePersonnels.Add(new RolePersonnel {
                        PersonnelId = entity.Id
                    });
            }
            foreach(var unmodifiedrole in this.DomainContext.Roles.Where(v => v.EntityState == EntityState.Unmodified).ToList()) {
                this.DomainContext.Roles.Detach(unmodifiedrole);
            }
            try {
                if(this.EditState == DataEditState.New) {
                    if(role.Can创建角色)
                        role.创建角色();
                } else if(role.Can调整角色)
                    role.调整角色();
            } catch(ValidationException e) {
                role.ValidationErrors.Add(e.ValidationResult);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public RoleGroupDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += RoleGroupDataEditView_DataContextChanged;
        }

        private void RoleGroupDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var role = this.DataContext as Role;
            if(role == null)
                return;
            role.PropertyChanged -= role_PropertyChanged;
            role.PropertyChanged += role_PropertyChanged;
        }

        private void PersonnelSelectViewLoad() {
            var role = this.DataContext as Role;
            if(role == null)
                return;
            this.PersonnelSelectView.EnterpriseId = role.EnterpriseId;
            this.PersonnelSelectView.CachePersonnels = null;
            this.PersonnelSelectView.Refesh();
        }
    }
}
