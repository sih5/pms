﻿using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.ViewModel;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.View.DataEdit {
    public partial class RoleTemplateDataEditView {
        private readonly AuthorizeViewModel authorizeViewModel = new AuthorizeViewModel();
        private bool initialized;

        public bool Initialized {
            get {
                return this.initialized;
            }
            private set {
                if(this.initialized != value) {
                    this.initialized = value;
                    this.OnPropertyChanged("Initialized");
                }
            }
        }

        protected override string BusinessName {
            get {
                return SecurityUIStrings.BusinessName_RoleTemplate;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetRoleTemplatesQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
                this.DelayCall(view => view.Initialized, view => view.SetMainAuthorizeView());
            }, null);
        }

        private void InitMainAuthorizeView() {
            if(this.DomainContext == null)
                this.DomainContext = new SecurityDomainContext();
            this.DomainContext.Load(this.DomainContext.GetCurrentEnterprisePagesWithDetailQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                this.authorizeViewModel.Initialize(loadOp.Entities, loadOp.AllEntities.Where(entity => entity is Node).Cast<Node>());
                this.MainAuthorizeView.DataContext = this.authorizeViewModel;
                this.MainAuthorizeView.MainTabControl.SelectedIndex = 0;
                this.Initialized = true;
            }, null);
        }

        private void SetMainAuthorizeView() {
            var template = this.DataContext as RoleTemplate;
            if(template == null || template.Id == default(int))
                return;
            this.DomainContext.Load(this.DomainContext.GetRoleTemplatesWithDetailByIdQuery(template.Id), LoadBehavior.RefreshCurrent, loadOp => {
                var roleTemplate = loadOp.Entities.FirstOrDefault();
                if(roleTemplate == null)
                    return;
                this.authorizeViewModel.SelectedNodes = roleTemplate.RoleTemplateRules.ToList().Select(entity => entity.Node).Where(node => node != null);
            }, null);
        }

        protected override void Reset() {
            this.authorizeViewModel.SelectedNodes = Enumerable.Empty<Node>();
        }

        protected void CreateUI() {
            var dataEditPanel = DI.GetDataEditPanel("RoleTemplate");
            dataEditPanel.SetValue(Grid.RowProperty, 1);
            this.RoleTemplateGrid.Children.Add(dataEditPanel);
            this.Root.Children.Add(this.CreateVerticalLine(1));
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var roleTemplate = this.DataContext as RoleTemplate;
            if(roleTemplate == null)
                return;

            var selectedNodeIds = this.authorizeViewModel.SelectedNodes.Select(node => node.Id).ToList();
            var rules = this.DomainContext.RoleTemplates.Single(v => v.Id == roleTemplate.Id).RoleTemplateRules.Where(rule => rule.EntityState == EntityState.Unmodified).ToList();
            var removed = rules.Where(rule => !selectedNodeIds.Contains(rule.NodeId));
            var added = selectedNodeIds.Except(rules.Select(rule => rule.NodeId)).Select(nodeId => new RoleTemplateRule {
                RoleTemplateId = roleTemplate.Id,
                NodeId = nodeId
            });
            foreach(var rule in removed)
                roleTemplate.RoleTemplateRules.Remove(rule);
            foreach(var rule in added)
                roleTemplate.RoleTemplateRules.Add(rule);
            ((IEditableObject)roleTemplate).EndEdit();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public RoleTemplateDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Initializer.Register(this.InitMainAuthorizeView);
        }
    }
}
