﻿using System.Linq;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.View.DataGrid {
    public class EntNodeTemplateForEditDataGridView : EntNodeTemplateDataGridView {
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.DataLoaded += GridView_DataLoaded;
        }

        private void GridView_DataLoaded(object sender, System.EventArgs e) {
            var enterprise = this.DataContext as Enterprise;
            if(enterprise == null)
                return;
            var selectedEntNodeTemplate = this.GridView.Items.Cast<EntNodeTemplate>().FirstOrDefault(r => r.Id == enterprise.EntNodeTemplateId);
            if(selectedEntNodeTemplate == null)
                return;
            if(this.GridView.SelectedItems.Cast<EntNodeTemplate>().Any(r => r.Id == selectedEntNodeTemplate.Id))
                return;
            this.GridView.SelectedItems.Add(selectedEntNodeTemplate);
        }
    }
}
