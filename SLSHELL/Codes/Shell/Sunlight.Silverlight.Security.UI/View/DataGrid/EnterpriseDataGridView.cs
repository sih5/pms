﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Security.View.DataGrid {
    public class EnterpriseDataGridView : SecurityDataGridViewBase {
        private readonly string[] kvNames = {
            "Enterprise_Status"
        };

        protected override Type EntityType {
            get {
                return typeof(Enterprise);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "EnterpriseCategory.Name",
                        Title = SecurityUIStrings.DataGridView_ColumnItem_Title_EnterpriseCategory
                    }, new ColumnItem {
                        Name = "EntNodeTemplate.Name",
                        Title = SecurityUIStrings.DataGridView_ColumnItem_Title_EntNodeTemplate
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetEnterprisesWithDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        public EnterpriseDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
