﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Security.View.DataGrid {
    public class EnterpriseForBindingDataGridView : SecurityDataGridViewBase {
        private List<object> selectedEnterprises;
        private readonly string[] kvNames = {
            "Enterprise_Status"
        };

        public List<object> SelectedEnterprises {
            get {
                return this.selectedEnterprises ?? (this.selectedEnterprises = new List<object>());
            }
        }

        protected override System.Type EntityType {
            get {
                return typeof(Enterprise);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "EnterpriseCategory.Name",
                        Title = SecurityUIStrings.DataGridView_ColumnItem_Title_EnterpriseCategory
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetEnterprisesWithCategory";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.DataContextChanged += EnterpriseForBindingDataGridView_DataContextChanged;
            this.DataPager.PageIndexChanging += DataPager_PageIndexChanging;
            this.GridView.RowLoaded += GridView_RowLoaded;
            this.GridView.SelectionChanged += GridView_SelectionChanged;
        }

        private void GridView_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangeEventArgs e) {
            foreach(var removedEnterprise in e.RemovedItems.OfType<Enterprise>()) {
                this.SelectedEnterprises.Remove(this.SelectedEnterprises.Cast<Enterprise>().First(r => r.Id == removedEnterprise.Id));
            }
        }

        private void GridView_RowLoaded(object sender, RowLoadedEventArgs e) {
            var row = e.Row as GridViewRow;
            if(row == null)
                return;
            if(!this.SelectedEnterprises.Contains(row.Item))
                return;
            row.IsSelected = true;
        }

        private void DataPager_PageIndexChanging(object sender, Telerik.Windows.Controls.PageIndexChangingEventArgs e) {
            foreach(var entity in this.Entities.Where(entity => this.SelectedEnterprises.Contains(entity))) {
                this.SelectedEnterprises.Remove(entity);
            }
            this.SelectedEnterprises.AddRange(this.GridView.SelectedItems);
        }

        private void EnterpriseForBindingDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            if(this.SelectedEnterprises != null)
                this.SelectedEnterprises.Clear();
            this.ExecuteQueryDelayed();
        }

        public EnterpriseForBindingDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
