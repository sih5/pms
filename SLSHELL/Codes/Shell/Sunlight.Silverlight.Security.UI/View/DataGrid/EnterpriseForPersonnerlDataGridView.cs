﻿
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Security.View.DataGrid {
    public class EnterpriseForPersonnerlDataGridView : SecurityDataGridViewBase {

        public EnterpriseForPersonnerlDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        private readonly string[] kvNames = {
            "Enterprise_Status"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "EnterpriseCategory.Name",
                        Title = SecurityUIStrings.DataGridView_ColumnItem_Title_EnterpriseCategory
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetEnterprisesByPersonnel";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem != null) {
                switch(parameterName) {
                    case "personnelName":
                        return filterItem.Filters.Any(item => item.MemberName == "PersonnelName") ? filterItem.Filters.Single(item => item.MemberName == "PersonnelName").Value : "";
                    case "personnelLoginId":
                        return filterItem.Filters.Any(item => item.MemberName == "PersonnelLoginId") ? filterItem.Filters.Single(item => item.MemberName == "PersonnelLoginId").Value : "";
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "PersonnelName" && filter.MemberName != "PersonnelLoginId"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.DataPager.NumericButtonCount = 4;
            this.DataPager.PageSize = 100;
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override System.Type EntityType {
            get {
                return typeof(Enterprise);
            }
        }
    }
}
