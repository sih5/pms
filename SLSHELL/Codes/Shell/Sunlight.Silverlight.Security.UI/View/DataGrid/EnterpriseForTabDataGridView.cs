﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Sunlight.Silverlight.Security.View.DataGrid {
    public class EnterpriseForTabDataGridView : EnterpriseDataGridView {
        private readonly string[] kvNames = {
            "Enterprise_Status"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "EnterpriseCategory.Name",
                        Title = SecurityUIStrings.DataGridView_ColumnItem_Title_EnterpriseCategory
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }

        protected override string OnRequestQueryName() {
            return "GetEnterprisesWithCategory";
        }

        private void EnterpriseDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var entNodeTemplate = e.NewValue as EntNodeTemplate;

            if(this.FilterItem == null)
                this.FilterItem = new CompositeFilterItem {
                    LogicalOperator = LogicalOperator.And,
                    Filters = {
                        new FilterItem {
                            MemberType = typeof(int),
                            MemberName = "EntNodeTemplateId",
                            Operator = FilterOperator.IsEqualTo
                        },
                        new FilterItem {
                            MemberType = typeof(int),
                            MemberName = "Status",
                            Operator = FilterOperator.IsNotEqualTo,
                            Value = (int)SecurityEnterpriseStatus.作废,
                        }
                    }
                };
            ((CompositeFilterItem)this.FilterItem).Filters.Single(item => item.MemberName == "EntNodeTemplateId").Value = entNodeTemplate != null ? entNodeTemplate.Id : default(int);
            this.ExecuteQueryDelayed();
        }

        public EnterpriseForTabDataGridView() {
            this.DataContextChanged += this.EnterpriseDataGridView_DataContextChanged;
        }
    }
}
