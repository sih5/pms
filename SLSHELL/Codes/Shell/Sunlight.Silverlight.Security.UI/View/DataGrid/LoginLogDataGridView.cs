﻿
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Security.View.DataGrid {
    public class LoginLogDataGridView : SecurityDataGridViewBase {

        protected override Type EntityType {
            get {
                return typeof(LoginLog);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Personnel.Enterprise.Code",
                        Title = SecurityUIStrings.QueryPanel_ColumnItem_Title_EnterpriseCode
                    },new ColumnItem {
                        Name = "Personnel.Enterprise.Name",
                        Title = SecurityUIStrings.QueryPanel_ColumnItem_Title_EnterpriseName
                    },new ColumnItem {
                        Name = "Personnel.LoginId",
                        Title = SecurityUIStrings.QueryPanel_ColumnItem_Title_PersonnelLoginId
                    },new ColumnItem {
                        Name = "Personnel.Name",
                        Title = SecurityUIStrings.QueryPanel_ColumnItem_Title_PersonnelName
                    },new ColumnItem {
                        Name = "IP"
                    },new ColumnItem {
                        Name = "LoginTime"
                    },new ColumnItem {
                        Name = "LogoutTime"
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetLoginLogAndPersonnels";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
