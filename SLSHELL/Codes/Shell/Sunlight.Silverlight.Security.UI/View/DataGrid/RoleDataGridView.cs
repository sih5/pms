﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;

namespace Sunlight.Silverlight.Security.View.DataGrid {
    public class RoleDataGridView : SecurityDataGridViewBase {
        protected readonly string[] kvNames = {
            "Role_Status"
        };

        protected override Type EntityType {
            get {
                return typeof(Role);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Name",
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "IsAdmin"
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetRoles";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.DataPager.PageSize = 500;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return base.OnRequestFilterDescriptor(queryName);

            var filter = filterItem.Filters.SingleOrDefault(item => item.MemberName == "EnterpriseId");
            if(filter != null)
                filterItem.Filters.Remove(filter);
            filterItem.Filters.Add(new FilterItem {
                MemberName = "EnterpriseId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });

            return filterItem.ToFilterDescriptor();
        }

        protected IFilterDescriptor RequestFilterDescriptor(string queryName) {
            return base.OnRequestFilterDescriptor(queryName);
        }

        public RoleDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
