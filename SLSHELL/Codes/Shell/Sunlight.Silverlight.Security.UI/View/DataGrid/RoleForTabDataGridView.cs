﻿using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.View.DataGrid {
    public class RoleForTabDataGridView : RoleDataGridView {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Name",
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "IsAdmin"
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }

        private void RoleDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var enterprise = e.NewValue as Enterprise;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberType = typeof(int),
                    MemberName = "EnterpriseId",
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = enterprise != null ? enterprise.Id : default(int);
            this.ExecuteQueryDelayed();
        }

        public RoleForTabDataGridView() {
            this.DataContextChanged += this.RoleDataGridView_DataContextChanged;
        }
    }
}
