﻿using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Security.View.DataGrid {
    public class RoleGroupDataGridView : SecurityDataGridViewBase {
        protected readonly string[] kvNames = {
            "Role_Status"
        };

        public RoleGroupDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            return RequestFilterDescriptor(queryName);
        }

        protected IFilterDescriptor RequestFilterDescriptor(string queryName) {
            return base.OnRequestFilterDescriptor(queryName);
        }

        protected override string OnRequestQueryName() {
            return "GetRolesWithEnterprise";
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Name",
                      //  IsSortDescending = false
                    }, new ColumnItem {
                        Name = "IsAdmin"
                    }, new ColumnItem {
                        Title = SecurityUIStrings.QueryItem_Title_EnterpriseCategory,
                        Name = "Enterprise.EnterpriseCategory.Name"
                    }, new ColumnItem {
                         Title = SecurityUIStrings.QueryItem_Title_EnterpriseCode,
                        Name = "Enterprise.Code"
                    }, new ColumnItem {
                         Title = SecurityUIStrings.QueryItem_Title_EnterpriseName,
                        Name = "Enterprise.Name"
                    },  new ColumnItem {
                         Title = SecurityUIStrings.QueryItem_Title_EntNodeTemplateName,
                        Name = "Enterprise.EntNodeTemplate.Name"
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.DataPager.PageSize = 100;
            this.DataPager.NumericButtonCount = 7;
        }

        protected override System.Type EntityType {
            get {
                return typeof(Role);
            }
        }
    }
}
