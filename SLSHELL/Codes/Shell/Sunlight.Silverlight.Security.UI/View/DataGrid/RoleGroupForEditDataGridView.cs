﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Security.View.DataGrid {
    public class RoleGroupForEditDataGridView : RoleDataGridView {
        private List<Role> selectedRoles;

        public List<Role> SelectedRoles {
            get {
                return this.selectedRoles ?? (this.selectedRoles = new List<Role>());
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.RowLoaded += GridView_RowLoaded;
            this.GridView.SelectionChanged += GridView_SelectionChanged;
        }

        private void GridView_RowLoaded(object sender, RowLoadedEventArgs e) {
            var row = e.Row as GridViewRow;
            if(row == null)
                return;
            var personnel = this.DataContext as Personnel;
            if(personnel == null)
                return;
            var ids = this.SelectedRoles.Select(r => r.Id);
            if(!ids.Contains(((Role)row.Item).Id))
                return;
            row.IsSelected = true;
        }

        private void GridView_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangeEventArgs e) {
            foreach(var addRole in e.AddedItems.Cast<Role>().Where(addRole => this.SelectedRoles.All(r => r.Id != addRole.Id))) {
                this.SelectedRoles.Add(addRole);
            }
            foreach(var removeRole in e.RemovedItems.Cast<Role>().Where(removeRole => this.SelectedRoles.Any(r => r.Id == removeRole.Id))) {
                this.SelectedRoles.Remove(this.SelectedRoles.First(r => r.Id == removeRole.Id));
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        private void RoleDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var personnel = this.DataContext as Personnel;
            if(personnel == null)
                return;
            this.SelectedRoles.Clear();
            this.SelectedRoles.AddRange(personnel.RolePersonnels.Select(r => r.Role));
            this.FilterItem = new CompositeFilterItem {
                LogicalOperator = LogicalOperator.And,
                Filters = {
                        new FilterItem {
                            MemberType = typeof(int),
                            MemberName = "EnterpriseId",
                            Operator = Sunlight.Silverlight.Core.Model.FilterOperator.IsEqualTo,
                            Value = personnel.EnterpriseId
                        },
                        new FilterItem {
                            MemberType = typeof(int),
                            MemberName = "Status",
                            Operator = Sunlight.Silverlight.Core.Model.FilterOperator.IsNotEqualTo,
                            Value = (int)SecurityCommonStatus.作废,
                        }
                    }
            };
            this.ExecuteQueryDelayed();
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            return filterItem.ToFilterDescriptor();
        }

        public RoleGroupForEditDataGridView() {
            this.DataContextChanged += this.RoleDataGridView_DataContextChanged;
        }

    }
}
