﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Security.View.DataGrid {
    public class RoleTemplateDataGridView : SecurityDataGridViewBase {
        private readonly string[] kvNames = {
            "SecurityCommon_Status"
        };

        protected override Type EntityType {
            get {
                return typeof(RoleTemplate);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Name",
                        IsSortDescending = false
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetRoleTemplates";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
        }

        public RoleTemplateDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
