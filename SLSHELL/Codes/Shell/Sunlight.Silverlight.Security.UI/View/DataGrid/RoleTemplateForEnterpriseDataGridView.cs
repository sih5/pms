﻿
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Security.View.DataGrid {
    public class RoleTemplateForEnterpriseDataGridView : RoleTemplateDataGridView {
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Extended;
        }
    }
}
