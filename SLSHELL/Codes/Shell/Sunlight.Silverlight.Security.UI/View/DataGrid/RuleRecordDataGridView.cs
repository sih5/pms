﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Security.View.DataGrid {
    public class RuleRecordDataGridView : SecurityDataGridViewBase {
        private readonly string[] kvNames = {
            "RuleRecord_Status"
        };

        public RuleRecordDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override System.Type EntityType {
            get {
                return typeof(VirtualRuleRecord);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        Title = SecurityUIStrings.DataGridView_ColumnItem_Title_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "EnterpriseCode",
                        Title = SecurityUIStrings.DataGridView_ColumnItem_Title_EnterpriseCode,
                    },new ColumnItem {
                        Name = "EnterpriseName",
                        Title = SecurityUIStrings.DataGridView_ColumnItem_Title_EnterpriseName,
                    },new ColumnItem {
                        Name = "RoleName",
                        Title = SecurityUIStrings.DataGridView_ColumnItem_Title_RoleName,
                    },new ColumnItem {
                        Name = "NodeName",
                        Title = SecurityUIStrings.DataGridView_ColumnItem_Title_NodeName
                    },new ColumnItem {
                        Name = "ActionName",
                        Title = SecurityUIStrings.DataGridView_ColumnItem_Title_ActionName
                    },new ColumnItem {
                        Name = "CreatorName",
                        Title = SecurityUIStrings.DataGridView_ColumnItem_Title_CreatorName
                    },new ColumnItem {
                        Name = "CreateTime",
                        Title = SecurityUIStrings.DataGridView_ColumnItem_Title_CreateTime
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "查询授权规则履历";
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem != null) {
                switch(parameterName) {
                    case "enterprisecode":
                        return filterItem.Filters.Any(item => item.MemberName == "EnterpriseCode") ? filterItem.Filters.Single(item => item.MemberName == "EnterpriseCode").Value : null;
                    case "enterprisename":
                        return filterItem.Filters.Any(item => item.MemberName == "EnterpriseName") ? filterItem.Filters.Single(item => item.MemberName == "EnterpriseName").Value : null;
                    case "rolename":
                        return filterItem.Filters.Any(item => item.MemberName == "RoleName") ? filterItem.Filters.Single(item => item.MemberName == "RoleName").Value : null;
                    case "nodename":
                        return filterItem.Filters.Any(item => item.MemberName == "NodeName") ? filterItem.Filters.Single(item => item.MemberName == "NodeName").Value : null;
                    case "actionname":
                        return filterItem.Filters.Any(item => item.MemberName == "ActionName") ? filterItem.Filters.Single(item => item.MemberName == "ActionName").Value : null;
                    case "status":
                        return filterItem.Filters.Any(item => item.MemberName == "Status") ? filterItem.Filters.Single(item => item.MemberName == "Status").Value : null;
                    case "creatorname":
                        return filterItem.Filters.Any(item => item.MemberName == "CreatorName") ? filterItem.Filters.Single(item => item.MemberName == "CreatorName").Value : null;
                    case "startcreatetime":
                    case "endcreatetime":
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        if(createTime == null)
                            return null;
                        if(parameterName == "startcreatetime") {
                            return createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        } else {
                            return createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

    }
}
