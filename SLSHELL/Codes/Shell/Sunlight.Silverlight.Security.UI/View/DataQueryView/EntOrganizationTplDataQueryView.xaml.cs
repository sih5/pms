﻿
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Security.Controls;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security.View.DataQueryView {
    public partial class EntOrganizationTplDataQueryView {
        private bool initialized;
        private DataGridViewBase entOrganizationTplGridView;
        private EntOrganizationTplDetailTreeView entOrganizationTplDetailTreeView;
        private SecurityDetailDataEditView detailDataEditView;

        public override DataGridViewBase DataGridView {
            get {
                if(this.entOrganizationTplGridView == null) {
                    this.entOrganizationTplGridView = DI.GetDataGridView("EntOrganizationTpl");
                    this.entOrganizationTplGridView.SelectionChanged += this.entOrganizationTplGridView_SelectionChanged;
                }
                return this.entOrganizationTplGridView;
            }
        }

        public EntOrganizationTplDetailTreeView EntOrganizationTplDetailTreeView {
            get {
                return this.entOrganizationTplDetailTreeView ?? (this.entOrganizationTplDetailTreeView = (EntOrganizationTplDetailTreeView)DI.GetDetailPanel("EntOrganizationTplDetail"));
            }
        }

        private SecurityDetailDataEditView DetailDataEditView {
            get {
                if(this.detailDataEditView == null) {
                    this.detailDataEditView = new SecurityDetailDataEditView();
                    this.detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(EntOrganizationTpl), "EntOrganizationTplDetails"), null, () => this.EntOrganizationTplDetailTreeView);
                    return this.detailDataEditView;
                }
                return this.detailDataEditView;
            }
        }

        private void entOrganizationTplGridView_SelectionChanged(object sender, System.EventArgs e) {
            this.EntOrganizationTplDetailTreeView.DataContext = this.DataGridView.SelectedEntities.Count() == 1 ? this.DataGridView.SelectedEntities.First() : null;
        }

        private void CreateUI() {
            this.Root.Children.Add(this.DataGridView);
            this.Root.Children.Add(this.CreateVerticalLine(1));
            this.DetailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(this.DetailDataEditView);
        }

        private void EntOrganizationTplDataQueryView_Loaded(object sender, RoutedEventArgs e) {
            if(!this.initialized) {
                this.initialized = true;
                this.CreateUI();
            }
        }

        public EntOrganizationTplDataQueryView() {
            InitializeComponent();
            this.Loaded += this.EntOrganizationTplDataQueryView_Loaded;
        }
    }
}
