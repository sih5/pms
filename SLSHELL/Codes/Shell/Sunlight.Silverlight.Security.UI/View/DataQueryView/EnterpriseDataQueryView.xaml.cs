﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Security.View.Custom;
using Sunlight.Silverlight.Security.ViewModel;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.DataQueryView {
    public partial class EnterpriseDataQueryView {
        private bool initialized;
        private DataGridViewBase enterpriseGridView;
        private RadTreeView organizationTreeView;
        private DataGridViewBase roleGridView;
        private readonly SecurityDomainContext domainContext = new SecurityDomainContext();
        private readonly AuthorizeViewModel authorizeViewModel = new AuthorizeViewModel();
        private readonly AuthorizeViewModel authorizeViewModelForRole = new AuthorizeViewModel();

        public override DataGridViewBase DataGridView {
            get {
                if(this.enterpriseGridView == null) {
                    this.enterpriseGridView = DI.GetDataGridView("Enterprise");
                    this.enterpriseGridView.SelectionChanged += this.DataGridView_SelectionChanged;
                }
                return this.enterpriseGridView;
            }
        }

        public RadTreeView OrganizationTreeView {
            get {
                return this.organizationTreeView ?? (this.organizationTreeView = new OrganizationTreeViewForEnterprise());
            }
        }

        public DataGridViewBase RoleDataGridView {
            get {
                if(this.roleGridView == null) {
                    this.roleGridView = DI.GetDataGridView("RoleForTab");
                    this.roleGridView.SelectionChanged += this.RoleGridView_SelectionChanged;
                }
                return this.roleGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(this.DataGridView);
            this.Root.Children.Add(this.CreateVerticalLine(1));
            this.TabOrganizations.Content = this.OrganizationTreeView;
            this.RoleGrid.Children.Add(this.RoleDataGridView);
            this.RoleGrid.Children.Add(this.CreateHorizontalLine(1));
        }

        private void Initialize() {
            this.domainContext.Load(this.domainContext.GetPagesWithDetailQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                this.authorizeViewModel.Initialize(loadOp.Entities, loadOp.AllEntities.Where(entity => entity is Node).Cast<Node>());
                this.MainAuthorizeView.DataContext = this.authorizeViewModel;
            }, null);
        }

        private void Initialize2() {
            this.domainContext.Load(this.domainContext.GetPagesWithDetailQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                this.authorizeViewModelForRole.Initialize(loadOp.Entities, loadOp.AllEntities.Where(entity => entity is Node).Cast<Node>());
                this.AuthorizeViewForRole.DataContext = this.authorizeViewModelForRole;
            }, null);
        }

        private void SecurityDataQueryViewBase_Loaded(object sender, RoutedEventArgs e) {
            if(!this.initialized) {
                this.initialized = true;
                this.CreateUI();
                this.Initialize();
                this.Initialize2();
            }
        }

        private void DataGridView_SelectionChanged(object sender, EventArgs e) {
            this.OrganizationTreeView.DataContext = this.DataGridView.SelectedEntities.Count() == 1 ? this.DataGridView.SelectedEntities.First() : null;
            this.RoleDataGridView.DataContext = this.DataGridView.SelectedEntities.Count() == 1 ? this.DataGridView.SelectedEntities.First() : null;
            var selectedItem = this.DataGridView.SelectedEntities.FirstOrDefault() as Enterprise;
            if(selectedItem == null)
                return;
            var templateId = selectedItem.EntNodeTemplateId.HasValue ? selectedItem.EntNodeTemplateId.Value : 0;
            this.domainContext.Load(this.domainContext.GetEntNodeTemplatesWithDetailByIdQuery(templateId), LoadBehavior.RefreshCurrent, loadOp => {
                var entTemplate = loadOp.Entities.FirstOrDefault();
                if(entTemplate == null)
                    return;
                this.authorizeViewModel.SelectedNodes = entTemplate.EntNodeTemplateDetails.ToList().Select(entity => entity.Node).Where(node => node != null);
            }, null);
        }

        private void RoleGridView_SelectionChanged(object sender, EventArgs e) {
            var selectedItem = this.RoleDataGridView.SelectedEntities.FirstOrDefault() as Role;
            if(selectedItem == null)
                return;
            this.domainContext.Load(this.domainContext.GetRulesByRoleQuery(selectedItem.Id), LoadBehavior.RefreshCurrent, loadOp => {
                this.authorizeViewModelForRole.SelectedNodes = loadOp.Entities.Where(rule => rule.Node != null).ToList().Select(entity => entity.Node).Where(node => node != null);
            }, null);
        }

        public EnterpriseDataQueryView() {
            this.InitializeComponent();
        }
    }
}
