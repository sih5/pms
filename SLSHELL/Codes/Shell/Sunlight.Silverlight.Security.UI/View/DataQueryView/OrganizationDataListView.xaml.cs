﻿
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Sunlight.Silverlight.Security.Controls;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;

namespace Sunlight.Silverlight.Security.View.DataQueryView {
    public partial class OrganizationDataListView {
        private bool initialized;
        private RadTreeView organizationTreeView;
        private FrameworkElement personnelDetailPanel;
        private DataListViewBase organizationPersonnelView;
        private SecurityDomainContext ScurityDomainContext = new SecurityDomainContext();

        public RadTreeView OrganizationTreeView {
            get {
                if(this.organizationTreeView == null) {
                    this.organizationTreeView = new OrganizationTreeViewForEdit {
                        DomainContext = this.ScurityDomainContext
                    };
                    this.organizationTreeView.SelectionChanged += this.OrganizationTreeView_SelectionChanged;
                }
                return this.organizationTreeView;
            }
        }

        public FrameworkElement PersonnelDetailPanel {
            get {
                return this.personnelDetailPanel ?? (this.personnelDetailPanel = DI.GetDetailPanel("OrganizationPersonnelDetail"));
            }
        }

        public override DataListViewBase DataListView {
            get {
                if(this.organizationPersonnelView == null) {
                    this.organizationPersonnelView = (DataListViewBase)DI.GetView("OrganizationPersonnelList");
                    this.organizationPersonnelView.SelectionChanged += this.OrganizationPersonnelView_SelectionChanged;
                }
                return this.organizationPersonnelView;
            }
        }

        private void OrganizationPersonnelView_SelectionChanged(object sender, System.EventArgs e) {
            var selectedItem = (sender as RadListBox).SelectedItem;
            if(selectedItem != null)
                this.PersonnelDetailPanel.DataContext = selectedItem as Personnel;
            else
                this.PersonnelDetailPanel.DataContext = null;
        }

        private void OrganizationTreeView_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if(this.OrganizationTreeView.SelectedItem == null)
                return;

            this.PersonnelDetailPanel.DataContext = null;
            this.SetDtaListView(((Organization)((RadTreeViewItem)this.OrganizationTreeView.SelectedItem).Tag).Id);
        }

        private void SetDtaListView(int id) {
            var organizationIds = new List<int>();
            organizationIds.Add(id);
            this.GetSelectedOrganizationIds((RadTreeViewItem)this.OrganizationTreeView.SelectedItem, organizationIds);
            this.ScurityDomainContext.Load(this.ScurityDomainContext.GetPersonnelsByOrganizationIdsQuery(organizationIds.ToArray()), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.DataListView.DataContext = loadOp.Entities;
            }, null);
        }

        private void GetSelectedOrganizationIds(RadTreeViewItem treeViewItem, List<int> ids) {
            if(treeViewItem == null)
                return;
            foreach(var item in treeViewItem.Items) {
                var organization = ((RadTreeViewItem)item).Tag as Organization;
                if(organization != null)
                    ids.Add(organization.Id);
                this.GetSelectedOrganizationIds((RadTreeViewItem)item, ids);
            }
        }

        private void CreateUI() {
            this.OrganizationTreeView.SetValue(Grid.RowSpanProperty, 3);
            this.Root.Children.Add(this.OrganizationTreeView);
            var line = this.CreateVerticalLine(1);
            line.SetValue(Grid.RowSpanProperty, 3);
            this.Root.Children.Add(line);
            this.DataListView.SetValue(Grid.ColumnProperty, 2);
            this.DataListView.SetValue(Grid.RowProperty, 0);
            this.Root.Children.Add(this.DataListView);
            var horizontalLine = new Rectangle {
                Height = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(0, 8, 0, 8),
            };
            horizontalLine.SetValue(Grid.ColumnProperty, 2);
            horizontalLine.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(horizontalLine);
            this.PersonnelDetailPanel.SetValue(Grid.ColumnProperty, 2);
            this.PersonnelDetailPanel.SetValue(Grid.RowProperty, 2);
            this.Root.Children.Add(this.PersonnelDetailPanel);
        }

        protected override void ExecuteQuery() {
            //throw new System.NotImplementedException();
        }

        public override object ExchangeData(Core.View.IBaseView sender, string subject, params object[] contents) {
            if(subject.Equals("SetEntity")) {
                var organization = contents[0] as Organization;
                if(organization != null)
                    this.SetDtaListView(organization.Id);
            }
            return null;
        }

        private void OrganizationDataListView_Loaded(object sender, RoutedEventArgs e) {
            if(!this.initialized) {
                this.initialized = true;
                this.CreateUI();
            }
        }

        public OrganizationDataListView() {
            InitializeComponent();
            this.Loaded += this.OrganizationDataListView_Loaded;
        }
    }
}
