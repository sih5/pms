﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Controls;
using Sunlight.Silverlight.Security.Panels.Detail;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.DataQueryView {
    public partial class PersonnelDataListView {
        private readonly string[] filterNames = new[] { 
            "LoginId", "Name", "CellNumber", "Status" 
        };
        private bool initialized;
        private RadTreeView organizationTreeView;
        private DataListViewBase organizationPersonnelView;
        private PersonnelDetailDetailPanel personnelDetailPanel;
        private IEnumerable<Personnel> personnels;
        private SecurityDomainContext domainContext;

        private SecurityDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new SecurityDomainContext());
            }
        }

        public RadTreeView OrganizationTreeView {
            get {
                if(this.organizationTreeView == null) {
                    this.organizationTreeView = new OrganizationTreeView();
                    this.organizationTreeView.SelectionChanged += this.OrganizationTreeView_SelectionChanged;
                }
                return this.organizationTreeView;
            }
        }

        public override DataListViewBase DataListView {
            get {
                if(this.organizationPersonnelView == null) {
                    this.organizationPersonnelView = (DataListViewBase)DI.GetView("OrganizationPersonnelList");
                    this.organizationPersonnelView.SecurityDomainContext = this.DomainContext;
                    this.organizationPersonnelView.SelectionChanged += this.OrganizationPersonnelView_SelectionChanged;
                }
                return this.organizationPersonnelView;
            }
        }

        public PersonnelDetailDetailPanel PersonnelDetailPanel {
            get {
                return this.personnelDetailPanel ?? (this.personnelDetailPanel = (PersonnelDetailDetailPanel)DI.GetDetailPanel("PersonnelDetail"));
            }
        }

        private void CreateUI() {
            this.OrganizationTreeView.SetValue(Grid.RowSpanProperty, 3);
            this.Root.Children.Add(this.OrganizationTreeView);
            var verticalLine = this.CreateVerticalLine(1);
            verticalLine.SetValue(Grid.RowSpanProperty, 3);
            this.Root.Children.Add(verticalLine);
            this.DataListView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(this.DataListView);
            var horizontalLine = this.CreateHorizontalLine(1);
            horizontalLine.SetValue(Grid.ColumnProperty, 2);
            horizontalLine.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(horizontalLine);
            this.PersonnelDetailPanel.SetValue(Grid.ColumnProperty, 2);
            this.PersonnelDetailPanel.SetValue(Grid.RowProperty, 2);
            this.Root.Children.Add(this.PersonnelDetailPanel);
        }

        private void SecurityDataListViewBase_Loaded(object sender, RoutedEventArgs e) {
            if(!this.initialized) {
                this.initialized = true;
                this.CreateUI();
            }
            this.RefreshPersonnelData();
        }

        private void RefreshPersonnelData() {
            var listBox = this.DataListView.ChildrenOfType<RadListBox>().FirstOrDefault();
            if(listBox == null)
                return;
            var selectedItem = listBox.SelectedItem;
            if(selectedItem == null)
                return;
            this.PersonnelDetailPanel.SetObjectById(((Personnel)selectedItem).Id);
        }

        private void OrganizationTreeView_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var selectedItem = this.OrganizationTreeView.SelectedItem;
            if(selectedItem == null || this.personnels == null)
                return;
            this.PersonnelDetailPanel.DataContext = null;
            var organization = ((RadTreeViewItem)selectedItem).Tag as Organization;
            if(organization != null)
                this.SetDataListView(organization.Id);
        }

        private void SetDataListView(int id) {
            var organizationIds = new List<int>();
            organizationIds.Add(id);
            this.GetSelectedOrganizationIds((RadTreeViewItem)this.OrganizationTreeView.SelectedItem, organizationIds);
            this.DataListView.DataContext = this.personnels.Where(p => p.OrganizationPersonnels.Any(op => organizationIds.Contains(op.OrganizationId)));
            var listBox = this.DataListView.ChildrenOfType<RadListBox>().FirstOrDefault();
            if(listBox == null)
                return;
            this.DataListView.SelectedEntities = listBox.SelectedItems.Cast<Entity>();
            var management = this.Parent.ParentOfType<DataManagementViewBase>();
            if(management == null)
                return;
            management.ExchangeData(null, "CheckActionsCanExecute", null);
        }

        private void GetSelectedOrganizationIds(RadTreeViewItem treeViewItem, List<int> ids) {
            if(treeViewItem == null)
                return;
            foreach(var item in treeViewItem.Items) {
                var organization = ((RadTreeViewItem)item).Tag as Organization;
                if(organization != null)
                    ids.Add(organization.Id);
                this.GetSelectedOrganizationIds((RadTreeViewItem)item, ids);
            }
        }

        private void OrganizationPersonnelView_SelectionChanged(object sender, EventArgs e) {
            var listBox = sender as RadListBox;
            if(listBox == null)
                return;
            var selectedItem = (sender as RadListBox).SelectedItem;
            if(selectedItem == null)
                return;
            this.PersonnelDetailPanel.SetObjectById(((Personnel)selectedItem).Id);
        }

        protected override void ExecuteQuery() {
            var entityQuery = this.DomainContext.GetPersonnelsWithDetailQuery().Where(e => e.EnterpriseId == BaseApp.Current.CurrentUserData.EnterpriseId);
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            foreach(var filterName in this.filterNames) {
                var filter = compositeFilterItem.Filters.SingleOrDefault(item => item.MemberName == filterName);
                if(filter == null || filter.Value == null)
                    continue;

                switch(filterName) {
                    case "LoginId":
                        var loginIdValue = (string)filter.Value;
                        entityQuery = entityQuery.Where(e => e.LoginId.ToLower().Contains(loginIdValue.ToLower()));
                        break;
                    case "Name":
                        var nameValue = (string)filter.Value;
                        entityQuery = entityQuery.Where(e => e.Name.ToLower().Contains(nameValue.ToLower()));
                        break;
                    case "CellNumber":
                        var cellNumberValue = (string)filter.Value;
                        entityQuery = entityQuery.Where(e => e.CellNumber.ToLower().Contains(cellNumberValue.ToLower()));
                        break;
                    case "Status":
                        if(((int)filter.Value) > -1)
                            entityQuery = entityQuery.Where(e => e.Status == (int)filter.Value);
                        break;
                }
            }
            var listBox = this.DataListView.ChildrenOfType<RadListBox>().FirstOrDefault();
            if(listBox != null) {
                var selectedItem = listBox.SelectedItem;
                if(selectedItem != null) {
                    var personnel = selectedItem as Personnel;
                    if(personnel != null)
                        foreach(var entity in personnel.OrganizationPersonnels)
                            this.DomainContext.OrganizationPersonnels.Detach(entity);
                }
            }
            this.DomainContext.Load(entityQuery, LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.personnels = loadOp.Entities;
                this.DataListView.DataContext = loadOp.Entities;
            }, null);
        }

        public override object ExchangeData(Core.View.IBaseView sender, string subject, params object[] contents) {
            if(subject == "SetEntity") {
                var id = contents != null ? (int)contents[0] : 0;
                if(id > 0)
                    this.SetDataListView(id);
            }
            return null;
        }

        public PersonnelDataListView() {
            this.InitializeComponent();
        }
    }
}
