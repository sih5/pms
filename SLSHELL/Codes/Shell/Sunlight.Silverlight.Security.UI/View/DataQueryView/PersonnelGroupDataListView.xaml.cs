﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Panels.Detail;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.DataQueryView {
    public partial class PersonnelGroupDataListView {
        private readonly string[] filterNames = new[] { 
            "LoginId", "Name", "Enterprise.Code", "Status", "Enterprise.Name","Enterprise.Category" 
        };
        private bool initialized;
        //private RadTreeView organizationTreeView;
        private DataListViewBase organizationPersonnelView;
        private PersonnelDetailDetailPanel personnelDetailPanel;
        private DataGridViewBase dataGridView;
        private IEnumerable<Personnel> Personnels;
        private SecurityDomainContext ScurityDomainContext = new SecurityDomainContext();

        //public RadTreeView OrganizationTreeView {
        //    get {
        //        if(this.organizationTreeView == null) {
        //            this.organizationTreeView = new OrganizationGroupTreeView();
        //            this.organizationTreeView.SelectionChanged += this.OrganizationTreeView_SelectionChanged;
        //        }
        //        return this.organizationTreeView;
        //    }
        //}

        private DataGridViewBase EnterpriseDataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("EnterpriseForPersonnerl");
                    this.dataGridView.SelectionChanged += DataGridView_SelectionChanged;
                }
                return this.dataGridView;
            }
        }

        public override DataListViewBase DataListView {
            get {
                if(this.organizationPersonnelView == null) {
                    this.organizationPersonnelView = (DataListViewBase)DI.GetView("OrganizationPersonnelList");
                    this.organizationPersonnelView.SecurityDomainContext = this.ScurityDomainContext;
                    this.organizationPersonnelView.SelectionChanged += this.OrganizationPersonnelView_SelectionChanged;
                }
                return this.organizationPersonnelView;
            }
        }

        public PersonnelDetailDetailPanel PersonnelDetailPanel {
            get {
                return this.personnelDetailPanel ?? (this.personnelDetailPanel = (PersonnelDetailDetailPanel)DI.GetDetailPanel("PersonnelDetail"));
            }
        }

        private void CreateUI() {
            this.EnterpriseDataGridView.SetValue(Grid.RowSpanProperty, 3);
            this.Root.Children.Add(this.EnterpriseDataGridView);
            var verticalLine = this.CreateVerticalLine(1);
            verticalLine.SetValue(Grid.RowSpanProperty, 3);
            this.Root.Children.Add(verticalLine);
            this.DataListView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(this.DataListView);
            var horizontalLine = this.CreateHorizontalLine(1);
            horizontalLine.SetValue(Grid.ColumnProperty, 2);
            horizontalLine.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(horizontalLine);
            this.PersonnelDetailPanel.SetValue(Grid.ColumnProperty, 2);
            this.PersonnelDetailPanel.SetValue(Grid.RowProperty, 2);
            this.Root.Children.Add(this.PersonnelDetailPanel);

        }

        private void SecurityDataListViewBase_Loaded(object sender, RoutedEventArgs e) {
            if(!this.initialized) {
                this.initialized = true;
                this.CreateUI();
            }
        }

        private void DataGridView_SelectionChanged(object sender, EventArgs e) {
            if(this.EnterpriseDataGridView.SelectedEntities == null)
                return;
            var enterprise = this.EnterpriseDataGridView.SelectedEntities.Cast<Enterprise>().SingleOrDefault();
            if(enterprise == null)
                return;
            this.DataListView.DataContext = this.Personnels.Where(p => p.EnterpriseId == enterprise.Id);
            this.enterpriseId = enterprise.Id;
        }

        //private void OrganizationTreeView_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
        //    var selectedItem = this.OrganizationTreeView.SelectedItem;
        //    if(selectedItem == null || this.Personnels == null)
        //        return;
        //    var organizations = new ObservableCollection<Organization>();
        //    organizations.Add(((RadTreeViewItem)selectedItem).Tag as Organization);
        //    this.GetOrganizations((RadTreeViewItem)selectedItem, organizations);
        //    var organizationIds = organizations.Select(o => o.Id).ToList();
        //    this.DataListView.DataContext = this.Personnels.Where(p => p.OrganizationPersonnels.Any(op => organizationIds.Contains(op.OrganizationId)));
        //    //点击获取鼠标选择的企业Id
        //    this.enterpriseId = ((Organization)((RadTreeViewItem)this.OrganizationTreeView.SelectedItem).Tag).EnterpriseId;
        //}

        //private void GetOrganizations(RadTreeViewItem treeViewItem, ObservableCollection<Organization> organizations) {
        //    foreach(var item in treeViewItem.Items) {
        //        organizations.Add(((RadTreeViewItem)item).Tag as Organization);
        //        this.GetOrganizations((RadTreeViewItem)item, organizations);
        //    }
        //}

        private int enterpriseId = BaseApp.Current.CurrentUserData.EnterpriseId;


        public int EnterpriseId {
            get {
                return enterpriseId;
            }
            set {
                enterpriseId = value;
            }
        }

        private void OrganizationPersonnelView_SelectionChanged(object sender, EventArgs e) {

            var selectedItem = (sender as RadListBox).SelectedItem;
            if(selectedItem != null) {
                this.PersonnelDetailPanel.SetObjectById(((Personnel)selectedItem).Id);
            } else
                this.PersonnelDetailPanel.DataContext = null;
        }

        protected override void ExecuteQuery() {
            EntityQuery<Personnel> entityQuery = this.ScurityDomainContext.GetPersonnelsWithDetailQuery();
            string enterpriseName = "";
            string enterpriseCode = "";
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var gridViewFilterItem = new CompositeFilterItem();
            gridViewFilterItem.LogicalOperator = LogicalOperator.And;
            FilterItem filter;
            foreach(var filterName in this.filterNames) {
                filter = compositeFilterItem.Filters.SingleOrDefault(item => item.MemberName == filterName);
                if(filter == null || filter.Value == null) {
                    continue;
                }
                switch(filterName) {
                    case "LoginId":
                        string loginIdValue = (string)filter.Value;
                        entityQuery = entityQuery.Where(e => e.LoginId.ToLower().Contains(loginIdValue.ToLower()));
                        gridViewFilterItem.Filters.Add(new FilterItem {
                            MemberName = "PersonnelLoginId",
                            MemberType = typeof(string),
                            Operator = FilterOperator.Contains,
                            Value = loginIdValue
                        });
                        break;
                    case "Name":
                        string nameValue = (string)filter.Value;
                        entityQuery = entityQuery.Where(e => e.Name.ToLower().Contains(nameValue.ToLower()));
                        gridViewFilterItem.Filters.Add(new FilterItem {
                            MemberName = "PersonnelName",
                            MemberType = typeof(string),
                            Operator = FilterOperator.Contains,
                            Value = nameValue
                        });
                        break;
                    case "Enterprise.Code":
                        if(!string.IsNullOrEmpty((string)filter.Value)) {
                            enterpriseCode = (string)filter.Value;
                            entityQuery = entityQuery.Where(e => e.Enterprise.Code.Contains(enterpriseCode));
                            gridViewFilterItem.Filters.Add(new FilterItem {
                                MemberName = "Code",
                                MemberType = typeof(string),
                                Operator = FilterOperator.Contains,
                                Value = enterpriseCode
                            });
                        }
                        break;
                    case "Status":
                        if(((int)filter.Value) > -1) {
                            int status = (int)filter.Value;
                            entityQuery = entityQuery.Where(e => e.Status == status);
                        }
                        break;
                    case "Enterprise.Name":
                        if(!string.IsNullOrEmpty((string)filter.Value)) {
                            enterpriseName = (string)filter.Value;
                            entityQuery = entityQuery.Where(e => e.Enterprise.Name.Contains(enterpriseName));
                            gridViewFilterItem.Filters.Add(new FilterItem {
                                MemberName = "Name",
                                MemberType = typeof(string),
                                Operator = FilterOperator.Contains,
                                Value = enterpriseName
                            });
                        }
                        break;
                    case "Enterprise.Category":
                        if(((int)filter.Value) > -1) {
                            int enterpriseCategory = (int)filter.Value;
                            gridViewFilterItem.Filters.Add(new FilterItem {
                                MemberName = "EnterpriseCategoryId",
                                MemberType = typeof(int),
                                Operator = FilterOperator.IsEqualTo,
                                Value = enterpriseCategory
                            });
                            entityQuery = entityQuery.Where(e => e.Enterprise.EnterpriseCategoryId == enterpriseCategory);
                        }
                        break;
                }
            }
            if(!gridViewFilterItem.Filters.Any()) {
                UIHelper.ShowNotification("请填写企业信息，用户信息任意一查询条件，再执行查询");
                return;
            }



            this.DataListView.DataContext = null;
            this.PersonnelDetailPanel.DataContext = null;

            this.EnterpriseDataGridView.FilterItem = gridViewFilterItem;
            this.EnterpriseDataGridView.ExecuteQueryDelayed();
            //((OrganizationGroupTreeView)this.OrganizationTreeView).ExecuteQuery((int)SecurityCommonStatus.有效, enterpriseCode, enterpriseName, enterpriseCategory);

            this.ScurityDomainContext.Load(entityQuery, LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError || loadOp.Entities == null || !loadOp.Entities.Any())
                    return;
                //取获取的第一个人员的企业ID
                EnterpriseId = loadOp.Entities.First().EnterpriseId;
                this.DataListView.DataContext = this.Personnels = loadOp.Entities;
            }, null);
        }

        public PersonnelGroupDataListView() {
            this.InitializeComponent();
        }
    }
}
