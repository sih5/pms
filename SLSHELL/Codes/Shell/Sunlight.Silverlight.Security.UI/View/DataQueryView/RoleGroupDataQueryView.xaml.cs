﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Security.ViewModel;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security.View.DataQueryView {
    public partial class RoleGroupDataQueryView {
        private bool initialized;
        private DataGridViewBase roleDataGridView;
        private DataListViewBase personnelView;
        private FrameworkElement personnelDetailPanel;
        private readonly SecurityDomainContext domainContext = new SecurityDomainContext();
        private readonly AuthorizeViewModel authorizeViewModel = new AuthorizeViewModel();

        public override DataGridViewBase DataGridView {
            get {
                if(this.roleDataGridView == null) {
                    this.roleDataGridView = DI.GetDataGridView("RoleGroup");
                    this.roleDataGridView.SelectionChanged += this.DataGridView_SelectionChanged;
                    
                }
                return this.roleDataGridView;
            }
        }

        public DataListViewBase DataListView {
            get {
                return this.personnelView ?? (this.personnelView = (DataListViewBase)DI.GetView("OrganizationPersonnelList"));
            }
        }

        public FrameworkElement PersonnelDetailPanel {
            get {
                return this.personnelDetailPanel ?? (this.personnelDetailPanel = DI.GetDetailPanel("OrganizationPersonnelDetail"));
            }
        }

        private void CreateUI() {
            this.DataGridView.SetValue(Grid.RowSpanProperty, 2);
            this.Root.Children.Add(this.DataGridView);
            this.DataListView.SetValue(Grid.ColumnProperty, 2);
            this.PersonnelsGrid.Children.Add(this.DataListView);
            this.PersonnelDetailPanel.SetValue(Grid.ColumnProperty, 2);
            this.PersonnelDetailPanel.SetValue(Grid.RowProperty, 1);
            this.PersonnelsGrid.Children.Add(this.PersonnelDetailPanel);
            this.PersonnelDetailPanel.SetBinding(DataContextProperty, new Binding("SelectedEntity") {
                Source = this.DataListView,
                Mode = BindingMode.OneWay
            });
        }

        private void Initialize() {
            this.domainContext.Load(this.domainContext.GetCurrentEnterprisePagesWithDetailQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                this.authorizeViewModel.Initialize(loadOp.Entities, loadOp.AllEntities.Where(entity => entity is Node).Cast<Node>());
                this.MainAuthorizeView.DataContext = this.authorizeViewModel;
            }, null);
        }

        private void SecurityDataQueryViewBase_Loaded(object sender, RoutedEventArgs e) {
            if(this.initialized)
                return;
            this.initialized = true;
            this.CreateUI();
            this.Initialize();
        }

        private int enterpriseId = 0;

        public int EnterpriseId
        {
            get { return enterpriseId; }
            set { enterpriseId = value; }
        }

        private void DataGridView_SelectionChanged(object sender, EventArgs e) {
            if(this.DataGridView.SelectedEntities.Count() != 1) {
                this.DataListView.DataContext = null;
                return;
            }
            var selectedItem = this.DataGridView.SelectedEntities.FirstOrDefault() as Role;
            if(selectedItem == null)
                return;
            this.domainContext.Load(this.domainContext.GetRulesByRoleQuery(selectedItem.Id), LoadBehavior.RefreshCurrent, loadOp => {
                this.authorizeViewModel.SelectedNodes = loadOp.Entities.Select(rule => rule.Node).Where(node => node != null);
            }, null);
            //点击获取鼠标选择的企业Id
            EnterpriseId = selectedItem.EnterpriseId;
            this.domainContext.Load(this.domainContext.GetPersonnelsByRoleIdQuery(selectedItem.Id).Where(p => p.Status != (int)SecurityUserStatus.作废), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.DataListView.DataContext = loadOp.Entities;
                this.DataListView.SelectedEntity = loadOp.Entities.Any() ? loadOp.Entities.First() : null;
            }, null);
        }

        public RoleGroupDataQueryView() {
            InitializeComponent();
        }
    }
}
