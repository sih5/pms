﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Security.ViewModel;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security.View.DataQueryView {
    public partial class RoleTemplateDataQueryView {
        private bool initialized;
        private DataGridViewBase roleTemplateGridView;
        private readonly SecurityDomainContext domainContext = new SecurityDomainContext();
        private readonly AuthorizeViewModel authorizeViewModel = new AuthorizeViewModel();

        public override DataGridViewBase DataGridView {
            get {
                if(this.roleTemplateGridView == null) {
                    this.roleTemplateGridView = DI.GetDataGridView("RoleTemplate");
                    this.roleTemplateGridView.SelectionChanged += this.DataGridView_SelectionChanged;
                }
                return this.roleTemplateGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(this.DataGridView);
            this.Root.Children.Add(this.CreateVerticalLine(1));
        }

        private void Initialize() {
            this.domainContext.Load(this.domainContext.GetPagesWithDetailQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                this.authorizeViewModel.Initialize(loadOp.Entities, loadOp.AllEntities.Where(entity => entity is Node).Cast<Node>());
                this.MainAuthorizeView.DataContext = this.authorizeViewModel;
            }, null);
        }

        private void SecurityDataQueryViewBase_Loaded(object sender, RoutedEventArgs e) {
            if(!this.initialized) {
                this.initialized = true;
                this.CreateUI();
                this.Initialize();
            }
        }

        private void DataGridView_SelectionChanged(object sender, EventArgs e) {
            var selectedItem = this.DataGridView.SelectedEntities.FirstOrDefault() as RoleTemplate;
            if(selectedItem == null)
                return;
            foreach(var entity in this.domainContext.RoleTemplates.ToList()) {
                this.domainContext.RoleTemplates.Detach(entity);
            }
            this.domainContext.Load(this.domainContext.GetRoleTemplatesWithDetailByIdQuery(selectedItem.Id), LoadBehavior.RefreshCurrent, loadOp => {
                var roleTemplate = loadOp.Entities.FirstOrDefault();
                if(roleTemplate == null)
                    return;
                this.authorizeViewModel.SelectedNodes = roleTemplate.RoleTemplateRules.ToList().Select(entity => entity.Node).Where(node => node != null);
            }, null);
        }

        public RoleTemplateDataQueryView() {
            InitializeComponent();
        }
    }
}
