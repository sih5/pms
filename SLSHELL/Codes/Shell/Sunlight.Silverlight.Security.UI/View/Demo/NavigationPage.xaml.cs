﻿using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;

namespace Sunlight.Silverlight.Security.View.Demo {
    public partial class NavigationPage : Page {
        public NavigationPage() {
            this.InitializeComponent();
        }

        // 当用户导航到此页面时执行。
        protected override void OnNavigatedTo(NavigationEventArgs e) {
        }

        private void Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            HtmlPage.Window.NavigateToBookmark("/sale/retail/returned");
        }
    }
}
