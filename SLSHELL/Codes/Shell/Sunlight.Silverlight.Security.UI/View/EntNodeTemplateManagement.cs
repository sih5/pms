﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security.View {
    [PageMeta("Security", "Security", "EntNodeTemplate", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON,"EntNodeTemplate"
    })]
    public class EntNodeTemplateManagement : SecurityDataManagementViewBase {
        private SecurityDataQueryViewBase dataQueryView;
        private DataEditViewBase dataAddView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataBindingView;
        protected const string DATA_ADD_VIEW = "_DataAddView_";
        protected const string DATA_BINDING_VIEW = "_DataBindingView_";

        private SecurityDataQueryViewBase DataQueryView {
            get {
                return this.dataQueryView ?? (this.dataQueryView = (SecurityDataQueryViewBase)DI.GetView("EntNodeTemplateDataQueryView"));
            }
        }

        private DataEditViewBase DataAddView {
            get {
                if(this.dataAddView == null) {
                    this.dataAddView = DI.GetDataEditView("EntNodeTemplateAdd");
                    this.dataAddView.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataAddView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataAddView;
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("EntNodeTemplate");
                    this.dataEditView.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        public DataEditViewBase DataBindingView {
            get {
                if(this.dataBindingView == null) {
                    this.dataBindingView = DI.GetDataEditView("EntNodeTemplateForBinding");
                    this.dataBindingView.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataBindingView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataBindingView;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "EntNodeTemplate"
                };
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_QUERY_VIEW, () => this.DataQueryView);
            this.RegisterView(DATA_ADD_VIEW, () => this.DataAddView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_BINDING_VIEW, () => this.DataBindingView);
        }

        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataQueryView.FilterItem != null)
                this.DataQueryView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_QUERY_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_QUERY_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_QUERY_VIEW);
            this.DataQueryView.FilterItem = filterItem;
            this.DataQueryView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var entNodeTemplate = this.DataAddView.CreateObjectToEdit<EntNodeTemplate>();
                    entNodeTemplate.Status = (int)SecurityCommonStatus.有效;
                    this.SwitchViewTo(DATA_ADD_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataQueryView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    SecurityUtils.Confirm(SecurityUIStrings.DataManagementView_Confirm_Abandon, () => this.DataQueryView.UpdateSelectedEntities(entity => ((EntNodeTemplate)entity).Status = (int)SecurityCommonStatus.作废, () => {
                        UIHelper.ShowNotification(SecurityUIStrings.DataManagementView_Notification_AbandonSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
                case "Binding":
                    this.DataBindingView.SetObjectToEditById(this.DataQueryView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_BINDING_VIEW);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_QUERY_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case "Binding":
                    if(this.DataQueryView.SelectedEntities == null)
                        return false;
                    var entities = this.DataQueryView.SelectedEntities.Cast<EntNodeTemplate>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)SecurityCommonStatus.有效;
                default:
                    return false;
            }
        }

        public EntNodeTemplateManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = SecurityUIStrings.DataManagementView_Title_EntNodeTemplate;
        }
    }
}
