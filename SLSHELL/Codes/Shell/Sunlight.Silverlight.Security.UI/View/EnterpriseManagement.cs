﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.View.DataEdit;
using Sunlight.Silverlight.Security.ViewModel.Workflow;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View {
    [PageMeta("Security", "Security", "Enterprise", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON, "Enterprise"
    })]
    public class EnterpriseManagement : SecurityDataManagementViewBase {
        private SecurityDataQueryViewBase dataQueryView;
        private EnterpriseModifyAdminPasswordDataEditView modifyAdminPasswordView;
        private RadWindow modifyAdminPasswordWindow;
        private SecurityWorkflowView workflowView;
        private SecurityWorkflowView batchWorkflowView;
        private const string DATA_WORKFLOW_SETENTERPRISE = "_DataWorkflowSetEnterprise_";
        private const string DATA_WORKFLOW_SETENTERPRISE_BATCH = "_DataWorkflowSetEnterpriseBatch_";
        private readonly ObservableCollection<Enterprise> enterprises = new ObservableCollection<Enterprise>();
        private SecurityDomainContext domainContext;

        private SecurityDataQueryViewBase DataQueryView {
            get {
                return this.dataQueryView ?? (this.dataQueryView = (SecurityDataQueryViewBase)DI.GetView("EnterpriseDataQueryView"));
            }
        }

        private EnterpriseModifyAdminPasswordDataEditView ModifyAdminPasswordView {
            get {
                if(this.modifyAdminPasswordView == null) {
                    this.modifyAdminPasswordView = (EnterpriseModifyAdminPasswordDataEditView)DI.GetDataEditView("EnterpriseModifyAdminPassword");
                    this.modifyAdminPasswordView.EditSubmitted += this.modifyAdminPasswordView_EditSubmitted;
                    this.modifyAdminPasswordView.EditCancelled += this.modifyAdminPasswordView_EditCancelled;
                }
                return this.modifyAdminPasswordView;
            }
        }

        public RadWindow ModifyAdminPasswordWindow {
            get {
                return this.modifyAdminPasswordWindow ?? (this.modifyAdminPasswordWindow = new RadWindow {
                    CanMove = true,
                    Content = this.ModifyAdminPasswordView,
                    WindowState = WindowState.Normal,
                    Language = this.Language,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    Header = SecurityUIStrings.DataEditView_Title_ModifyAdminPassword_Enterprise,
                    Height = 350,
                    Width = 450,
                    MaxHeight = 350,
                    MaxWidth = 450,
                    AllowDrop = false
                });
            }
        }

        public SecurityWorkflowView WorkflowView {
            get {
                var workFlow = new EnterpriseWorkflow {
                    EnterpriseInfo = new EnterpriseInfo {
                        Enterprises = this.enterprises,
                        DomainContext = this.DomainContext
                    }
                };
                workFlow.WorkflowCompleted += this.workFlow_WorkflowCompleted;
                workFlow.WorkflowCancelled += this.workFlow_WorkflowCancelled;
                return this.workflowView ?? (this.workflowView = new SecurityWorkflowView(workFlow));
            }
        }

        public SecurityWorkflowView BatchWorkflowView {
            get {
                var batchWorkflow = new EnterpriseBatchWorkflow {
                    EnterpriseInfo = new EnterpriseInfo {
                        Enterprises = this.enterprises,
                        DomainContext = this.DomainContext
                    }
                };
                batchWorkflow.WorkflowCompleted += this.workFlow_WorkflowCompleted;
                batchWorkflow.WorkflowCancelled += this.workFlow_WorkflowCancelled;
                return this.batchWorkflowView ?? (this.batchWorkflowView = new SecurityWorkflowView(batchWorkflow));
            }
        }

        public SecurityDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new SecurityDomainContext());
            }
        }

        private void workFlow_WorkflowCompleted(object sender, EventArgs e) {
            if(this.DataQueryView.FilterItem != null)
                this.DataQueryView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_QUERY_VIEW);
        }

        private void workFlow_WorkflowCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_QUERY_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "Enterprise"
                };
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_QUERY_VIEW, () => this.DataQueryView);
            //this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_WORKFLOW_SETENTERPRISE, () => this.WorkflowView);
            this.RegisterView(DATA_WORKFLOW_SETENTERPRISE_BATCH, () => this.BatchWorkflowView);
        }

        private void EditEnterprises() {
            this.enterprises.Clear();
            var ids = this.DataQueryView.SelectedEntities.Cast<Enterprise>().Select(r => r.Id).ToArray();
            this.DomainContext.Load(this.DomainContext.GetEnterprisesByIdsQuery(ids), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(!loadOp.Entities.Any())
                    return;
                foreach(var entity in loadOp.Entities) {
                    this.enterprises.Add(entity);
                }
                this.SwitchViewTo(ids.Count() == 1 ? DATA_WORKFLOW_SETENTERPRISE : DATA_WORKFLOW_SETENTERPRISE_BATCH);
            }, null);
        }

        private void modifyAdminPasswordView_EditSubmitted(object sender, EventArgs e) {
            this.ModifyAdminPasswordWindow.Close();
        }

        private void modifyAdminPasswordView_EditCancelled(object sender, EventArgs e) {
            this.ModifyAdminPasswordWindow.Close();
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_QUERY_VIEW);
            this.DataQueryView.FilterItem = filterItem;
            this.DataQueryView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    this.enterprises.Clear();
                    this.enterprises.Add(new Enterprise {
                        Status = (int)SecurityEnterpriseStatus.新建,
                        Password = GlobalVar.ASSIGNED_BY_SERVER
                    });
                    this.SwitchViewTo(DATA_WORKFLOW_SETENTERPRISE);
                    break;
                case CommonActionKeys.EDIT:
                    this.EditEnterprises();
                    break;
                case CommonActionKeys.ABANDON:
                    SecurityUtils.Confirm(SecurityUIStrings.DataManagementView_Confirm_Abandon, () => this.DataQueryView.UpdateSelectedEntities(entity => {
                        try {
                            if(((Enterprise)entity).Can作废企业)
                                ((Enterprise)entity).作废企业();
                        } catch(ValidationException e) {
                            UIHelper.ShowAlertMessage(e.Message);
                        }
                    }, () => {
                        UIHelper.ShowNotification(SecurityUIStrings.DataManagementView_Notification_AbandonSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
                case "Freeze":
                    SecurityUtils.Confirm(SecurityUIStrings.DataManagementView_Confirm_Freeze, () => this.DataQueryView.UpdateSelectedEntities(entity => {
                        try {
                            if(((Enterprise)entity).Can冻结企业)
                                ((Enterprise)entity).冻结企业();
                        } catch(ValidationException e) {
                            UIHelper.ShowAlertMessage(e.Message);
                        }
                    }, () => {
                        UIHelper.ShowNotification(SecurityUIStrings.DataManagementView_Notification_FreezeSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
                case "Recover":
                    SecurityUtils.Confirm(SecurityUIStrings.DataManagementView_Confirm_Recover, () => this.DataQueryView.UpdateSelectedEntities(entity => {
                        try {
                            if(((Enterprise)entity).Can恢复企业)
                                ((Enterprise)entity).恢复企业();
                        } catch(ValidationException e) {
                            UIHelper.ShowAlertMessage(e.Message);
                        }
                    }, () => {
                        UIHelper.ShowNotification(SecurityUIStrings.DataManagementView_Notification_RecoverSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
                case "Validation":
                    SecurityUtils.Confirm(SecurityUIStrings.DataManagementView_Confirm_Validation, () => this.DataQueryView.UpdateSelectedEntities(entity => {
                        try {
                            if(((Enterprise)entity).Can生效企业)
                                ((Enterprise)entity).生效企业();
                        } catch(ValidationException e) {
                            UIHelper.ShowAlertMessage(e.Message);
                        }
                    }, () => {
                        UIHelper.ShowNotification(SecurityUIStrings.DataManagementView_Notification_ValidationSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
                case "ModifyAdminPassword":
                    this.ModifyAdminPasswordView.SetEnterprise(this.DataQueryView.SelectedEntities.First());
                    this.ModifyAdminPasswordWindow.ShowDialog();
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_QUERY_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                    if(this.DataQueryView.SelectedEntities == null)
                        return false;
                    if(!this.DataQueryView.SelectedEntities.Any())
                        return false;
                    return !this.DataQueryView.SelectedEntities.Cast<Enterprise>().Any(entity => entity.Status != (int)SecurityEnterpriseStatus.新建 && entity.Status != (int)SecurityEnterpriseStatus.冻结 && entity.Status != (int)SecurityEnterpriseStatus.生效);
                case CommonActionKeys.ABANDON:
                case "Freeze":
                case "Recover":
                case "Validation":
                case "ModifyAdminPassword":
                    if(this.DataQueryView.SelectedEntities == null)
                        return false;
                    var entities = this.DataQueryView.SelectedEntities.Cast<Enterprise>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId == "Freeze")
                        return entities[0].Status == (int)SecurityEnterpriseStatus.生效;
                    if(uniqueId == "Recover")
                        return entities[0].Status == (int)SecurityEnterpriseStatus.冻结;
                    if(uniqueId == "Validation")
                        return entities[0].Status == (int)SecurityEnterpriseStatus.新建;
                    return entities[0].Status == (int)SecurityEnterpriseStatus.新建 || entities[0].Status == (int)SecurityEnterpriseStatus.冻结 || entities[0].Status == (int)SecurityEnterpriseStatus.生效;
                default:
                    return false;
            }
        }

        public EnterpriseManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = SecurityUIStrings.DataManagementView_Title_Enterprise;
        }
    }
}
