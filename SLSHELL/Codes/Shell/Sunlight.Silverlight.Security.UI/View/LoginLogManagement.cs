﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Security.View {
    [PageMeta("Security", "Log", "LoginLog", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class LoginLogManagement : SecurityDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private SecurityDomainContext domainContext;

        private SecurityDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new SecurityDomainContext());
            }
        }

        public LoginLogManagement() {
            this.Initializer.Register(this.Initialize);
            Title = SecurityUIStrings.DataManagementView_Title_LoginLog;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("LoginLog"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "LoginLog"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case "Export":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "Export":
                    ShellViewModel.Current.IsBusy = true;
                    var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(compositeFilterItem != null) {
                        var code = compositeFilterItem.Filters.Single(r => r.MemberName == "Personnel.Enterprise.Code").Value as string;
                        var name = compositeFilterItem.Filters.Single(r => r.MemberName == "Personnel.Enterprise.Name").Value as string;
                        var loginId = compositeFilterItem.Filters.Single(r => r.MemberName == "Personnel.LoginId").Value as string;
                        var categoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "Personnel.Enterprise.EnterpriseCategoryId").Value as int?;
                        var personnelName = compositeFilterItem.Filters.Single(r => r.MemberName == "Personnel.Name").Value as string;
                        var createTime = compositeFilterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? begianDate = null;
                        DateTime? endDate = null;
                        if(createTime != null) {
                            begianDate = createTime.Filters.First(r => r.MemberName == "LoginTime").Value as DateTime?;
                            endDate = createTime.Filters.Last(r => r.MemberName == "LoginTime").Value as DateTime?;
                        }
                        this.DomainContext.ExportLoginLogForReport(code, name, loginId,categoryId, personnelName, begianDate, endDate, invokeOp => {
                            if(invokeOp.HasError)
                                return;
                            if(invokeOp.Value == null || string.IsNullOrEmpty(invokeOp.Value)) {
                                UIHelper.ShowNotification(invokeOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(invokeOp.Value != null && !string.IsNullOrEmpty(invokeOp.Value)) {
                                HtmlPage.Window.Navigate(SecurityUtils.GetDownloadFileUrl(invokeOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    }
                    break;
            }
        }
    }
}
