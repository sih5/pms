﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security.View {
    [PageMeta("Security", "Security", "ModifyPassword")]
    public class ModifyPasswordManagement : SecurityDataManagementViewBase {
        private DataEditViewBase dataEditView;

        private DataEditViewBase DataEditView {
            get {
                return this.dataEditView ?? (this.dataEditView = DI.GetDataEditView("ModifyPassword"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
        }

        public ModifyPasswordManagement() {
            this.Initializer.Register(this.Initialize);
        }
    }
}
