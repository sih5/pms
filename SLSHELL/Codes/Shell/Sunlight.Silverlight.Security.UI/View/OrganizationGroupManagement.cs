﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Security.Controls;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.View.DataQueryView;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Security.View {
    [PageMeta("Security", "Security", "OrganizationGroup", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON,"Organization"
    })]
    public class OrganizationGroupManagement : SecurityDataManagementViewBase {
        private SecurityDataQueryViewBase dataQueryView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase changeSequenceDataEditView;
        private RadTreeView organizationTreeView;
        protected const string DATA_EDIT_VIEW_CHANGESEQUENCE = "_DataQueryViewChangeSequence_";

        private SecurityDataQueryViewBase DataQueryView {
            get {
                return this.dataQueryView ?? (this.dataQueryView = (SecurityDataListViewBase)DI.GetView("OrganizationGroupDataListView"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("OrganizationGroup");
                    this.dataEditView.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase ChangeSequenceDataEditView {
            get {
                if(this.changeSequenceDataEditView == null) {
                    this.changeSequenceDataEditView = DI.GetDataEditView("OrganizationChangeSequence");
                    this.changeSequenceDataEditView.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.changeSequenceDataEditView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.changeSequenceDataEditView;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "OrganizationGroup"
                };
            }
        }

        public RadTreeView OrganizationGroupTreeView {
            get {
                return this.organizationTreeView ?? (this.organizationTreeView = (this.DataQueryView as OrganizationGroupDataListView).OrganizationTreeView);
            }
        }

        private void dataEditView_EditSubmitted(object sender, System.EventArgs e) {
            if(this.DataQueryView.FilterItem != null)
                this.DataQueryView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_QUERY_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, System.EventArgs e) {
            this.SwitchViewTo(DATA_QUERY_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_QUERY_VIEW, () => this.DataQueryView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_CHANGESEQUENCE, () => this.ChangeSequenceDataEditView);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var parentOrganizationGroup = (OrganizationGroupTreeView.SelectedItem as RadTreeViewItem).Tag as Organization;
                    if(parentOrganizationGroup == null)
                        return;
                    var organization = this.DataEditView.CreateObjectToEdit<Organization>();
                    organization.ParentId = parentOrganizationGroup.Id;
                    organization.Type = (int)SecurityOrganizationOrganizationType.部门;
                    organization.Status = (int)SecurityCommonStatus.有效;
                    //var panelFilter = ((CompositeFilterItem)this.DataQueryView.FilterItem).Filters.SingleOrDefault(f => f.MemberName == "EnterpriseId");
                    //organization.EnterpriseId = (int)panelFilter.Value;
                    var db = this.DataQueryView as OrganizationGroupDataListView;
                    organization.EnterpriseId = db.EnterpriseId;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    var selectedOrganizationGroup = (OrganizationGroupTreeView.SelectedItem as RadTreeViewItem).Tag as Organization;
                    if(selectedOrganizationGroup == null)
                        return;
                    this.DataEditView.SetObjectToEditById(selectedOrganizationGroup.Id);
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    var radTreeViewItem = this.OrganizationGroupTreeView.SelectedItem as RadTreeViewItem;
                    if(radTreeViewItem == null)
                        return;
                    foreach(var item in radTreeViewItem.Items) {
                        if(((item as RadTreeViewItem).Tag as Organization).Status == (int)SecurityCommonStatus.有效) {
                            UIHelper.ShowNotification(Security.Resources.SecurityUIStrings.DataManagementView_Validation_Organization_AbandonIsForbidden);
                            return;
                        }
                    }
                    var domainContext = (this.OrganizationGroupTreeView as OrganizationTreeView).DomainContext;
                    domainContext.Load(domainContext.GetPersonnelsByOrganizationIdQuery(((Organization)radTreeViewItem.Tag).Id).Where(p => p.Status == (int)SecurityUserStatus.有效), loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        if(loadOp.Entities.Any()) {
                            UIHelper.ShowNotification(Security.Resources.SecurityUIStrings.DataManagementView_Validation_Organization_ExistOrganizationPersonnel);
                            return;
                        }
                        SecurityUtils.Confirm(SecurityUIStrings.DataManagementView_Confirm_AbandonOrganization, () => this.DataQueryView.UpdateSelectedEntities(domainContext, (Organization)radTreeViewItem.Tag, entity => ((Organization)entity).Status = (int)SecurityCommonStatus.作废, () => {
                            (radTreeViewItem.Parent as RadTreeViewItem).Items.Remove(radTreeViewItem);
                            UIHelper.ShowNotification(SecurityUIStrings.DataManagementView_Notification_AbandonSuccess);
                            this.CheckActionsCanExecute();
                        }));
                    }, null);
                    break;
                case "Reorder":
                    this.SwitchViewTo(DATA_EDIT_VIEW_CHANGESEQUENCE);
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, Core.Model.FilterItem filterItem) {
            this.SwitchViewTo(DATA_QUERY_VIEW);
            this.DataQueryView.FilterItem = filterItem;
            this.DataQueryView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_QUERY_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return OrganizationGroupTreeView.SelectedItem != null && ((OrganizationGroupTreeView.SelectedItem as RadTreeViewItem).Tag as Organization).Status != (int)SecurityCommonStatus.作废;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    return OrganizationGroupTreeView.SelectedItem != null && ((OrganizationGroupTreeView.SelectedItem as RadTreeViewItem).Tag as Organization).Status != (int)SecurityCommonStatus.作废 && (OrganizationGroupTreeView.SelectedItem as RadTreeViewItem).ParentItem != null;
                case "Reorder":
                    return true;
                default:
                    return false;
            }
        }

        private void organizationTreeView_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            this.CheckActionsCanExecute();
        }

        public OrganizationGroupManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = SecurityUIStrings.DataManagementView_Title_OrganizationGroup;
            OrganizationGroupTreeView.SelectionChanged += this.organizationTreeView_SelectionChanged;
        }
    }
}