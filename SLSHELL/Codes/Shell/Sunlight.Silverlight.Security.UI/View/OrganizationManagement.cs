﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Security.Controls;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.View.DataQueryView;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View {
    [PageMeta("Security", "Security", "Organization", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON,"Organization"
    })]
    public class OrganizationManagement : SecurityDataManagementViewBase {
        private SecurityDataQueryViewBase dataQueryView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase changeSequenceDataEditView;
        private RadTreeView organizationTreeView;
        protected const string DATA_EDIT_VIEW_CHANGESEQUENCE = "_DataQueryViewChangeSequence_";
        private Organization organization = new Organization();

        private SecurityDataQueryViewBase DataQueryView {
            get {
                return this.dataQueryView ?? (this.dataQueryView = (SecurityDataQueryViewBase)DI.GetView("OrganizationDataListView"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("Organization");
                    this.dataEditView.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase ChangeSequenceDataEditView {
            get {
                if(this.changeSequenceDataEditView == null) {
                    this.changeSequenceDataEditView = DI.GetDataEditView("OrganizationChangeSequence");
                    this.changeSequenceDataEditView.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.changeSequenceDataEditView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.changeSequenceDataEditView;
            }
        }

        public RadTreeView OrganizationTreeView {
            get {
                return this.organizationTreeView ?? (this.organizationTreeView = (this.DataQueryView as OrganizationDataListView).OrganizationTreeView);
            }
        }

        private void dataEditView_EditSubmitted(object sender, System.EventArgs e) {
            if(this.DataQueryView.FilterItem != null)
                this.DataQueryView.ExecuteQueryDelayed();
            this.DataQueryView.ExchangeData(null, "SetEntity", this.organization);
            this.SwitchViewTo(DATA_QUERY_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, System.EventArgs e) {
            this.SwitchViewTo(DATA_QUERY_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_QUERY_VIEW, () => this.DataQueryView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_CHANGESEQUENCE, () => this.ChangeSequenceDataEditView);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var parentOrganization = (OrganizationTreeView.SelectedItem as RadTreeViewItem).Tag as Organization;
                    if(parentOrganization == null)
                        return;
                    var organization = this.DataEditView.CreateObjectToEdit<Organization>();
                    organization.ParentId = parentOrganization.Id;
                    organization.Type = (int)SecurityOrganizationOrganizationType.部门;
                    organization.Status = (int)SecurityCommonStatus.有效;
                    organization.EnterpriseId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    var selectedOrganization = (OrganizationTreeView.SelectedItem as RadTreeViewItem).Tag as Organization;
                    if(selectedOrganization == null)
                        return;
						
                    this.organization = selectedOrganization;
                    this.DataEditView.SetObjectToEditById(selectedOrganization.Id);
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    var radTreeViewItem = this.OrganizationTreeView.SelectedItem as RadTreeViewItem;
                    if(radTreeViewItem == null)
                        return;
                    foreach(var item in radTreeViewItem.Items) {
                        if(((item as RadTreeViewItem).Tag as Organization).Status == (int)SecurityCommonStatus.有效) {
                            UIHelper.ShowNotification(Security.Resources.SecurityUIStrings.DataManagementView_Validation_Organization_AbandonIsForbidden);
                            return;
                        }
                    }
                    var domainContext = (this.OrganizationTreeView as OrganizationTreeView).DomainContext;
                    domainContext.Load(domainContext.GetPersonnelsByOrganizationIdQuery(((Organization)radTreeViewItem.Tag).Id).Where(p => p.Status == (int)SecurityUserStatus.有效), loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        if(loadOp.Entities.Any()) {
                            UIHelper.ShowNotification(Security.Resources.SecurityUIStrings.DataManagementView_Validation_Organization_ExistOrganizationPersonnel);
                            return;
                        }
                        SecurityUtils.Confirm(SecurityUIStrings.DataManagementView_Confirm_AbandonOrganization, () => this.DataQueryView.UpdateSelectedEntities(domainContext, (Organization)radTreeViewItem.Tag, entity => ((Organization)entity).Status = (int)SecurityCommonStatus.作废, () => {
                            (radTreeViewItem.Parent as RadTreeViewItem).Items.Remove(radTreeViewItem);
                            UIHelper.ShowNotification(SecurityUIStrings.DataManagementView_Notification_AbandonSuccess);
                            this.CheckActionsCanExecute();
                        }));
                    }, null);
                    break;
                case "Reorder":
                    this.SwitchViewTo(DATA_EDIT_VIEW_CHANGESEQUENCE);
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, Core.Model.FilterItem filterItem) {
            this.SwitchViewTo(DATA_QUERY_VIEW);
            this.DataQueryView.FilterItem = filterItem;
            this.DataQueryView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_QUERY_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return OrganizationTreeView.SelectedItem != null && ((OrganizationTreeView.SelectedItem as RadTreeViewItem).Tag as Organization).Status != (int)SecurityCommonStatus.作废;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    return OrganizationTreeView.SelectedItem != null && ((OrganizationTreeView.SelectedItem as RadTreeViewItem).Tag as Organization).Status != (int)SecurityCommonStatus.作废 && (OrganizationTreeView.SelectedItem as RadTreeViewItem).ParentItem != null;
                case "Reorder":
                    return true;
                default:
                    return false;
            }
        }

        private void organizationTreeView_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            this.CheckActionsCanExecute();
        }

        public OrganizationManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = SecurityUIStrings.DataManagementView_Title_Organization;
            OrganizationTreeView.SelectionChanged += this.organizationTreeView_SelectionChanged;
        }
    }
}
