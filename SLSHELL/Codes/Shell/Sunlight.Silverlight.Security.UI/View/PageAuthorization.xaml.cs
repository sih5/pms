﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Xml.Linq;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Security.Controls;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.RibbonBar;

namespace Sunlight.Silverlight.Security.View {
    public partial class PageAuthorization : ICustomTabsView {
        private sealed class TreeViewIsCheckedConverter : IValueConverter {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
                var treeViewItem = (RadTreeViewItem)value;
                if(!treeViewItem.HasItems)
                    return ToggleState.Off;

                var checkedCount = treeViewItem.Items.Cast<RadTreeViewItem>().Count(item => item.CheckState == ToggleState.On);
                var count = treeViewItem.Items.Count;

                if(checkedCount == count)
                    return ToggleState.On;
                return checkedCount == 0 ? ToggleState.Off : ToggleState.Indeterminate;
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
                throw new NotSupportedException();
            }
        }

        private sealed class PageData {
            public int Id {
                get;
                set;
            }

            public bool AllowAccess {
                get;
                set;
            }

            public IEnumerable<string> Operations {
                get;
                set;
            }

            public override string ToString() {
                return string.Format("{0}, {1}", this.Id, this.AllowAccess);
            }
        }

        private IEnumerable<Role> rolesCache;
        private IEnumerable<Page> pagesCache;
        private RadRibbonTab ribbonTab;
        private readonly Dictionary<int, int[]> authorizedPages;
        private readonly Dictionary<int, List<PageData>> authenticationData;

        private Role CurrentRole {
            get {
                var roleItem = this.RolesComboBox.SelectedItem as RadComboBoxItem;
                if(roleItem == null)
                    return null;
                return roleItem.Tag as Role;
            }
        }

        private Page CurrentPage {
            get {
                var pageItem = this.PagesTreeView.SelectedItem as RadTreeViewItem;
                if(pageItem == null)
                    return null;
                return pageItem.Tag as Page;
            }
        }

        private IEnumerable<RadTreeViewItem> PageTreeViewItems {
            get {
                return this.PagesTreeView.Items.Cast<RadTreeViewItem>().SelectMany(item => item.Items.Cast<RadTreeViewItem>()).SelectMany(item => item.Items.Cast<RadTreeViewItem>());
            }
        }

        public bool IsTabsReady {
            get {
                return true;
            }
        }

        public string RibbonContextualName {
            get {
                return null;
            }
        }

        public IEnumerable<RadRibbonTab> CustomTabs {
            get {
                if(this.ribbonTab == null) {
                    var button = new RadRibbonButton {
                        Text = "保存修改",
                        Size = ButtonSize.Large,
                        LargeImage = new BitmapImage {
                            UriSource = Core.Utils.MakeServerUri("Client/Security/Images/Operations/save.png")
                        },
                        FontSize = 12,
                        Padding = new Thickness(5, 10, 5, 0),
                    };
                    button.Click += this.SaveButton_Click;

                    var group = new RadRibbonGroup {
                        Header = "数据操作",
                    };
                    group.Items.Add(button);

                    this.ribbonTab = new RadRibbonTab {
                        Header = "数据操作",
                    };
                    this.ribbonTab.Items.Add(group);
                }
                return new[] { this.ribbonTab };
            }
        }

        private IEnumerable<PageData> EnsureRoleData() {
            if(this.CurrentRole == null)
                return null;

            List<PageData> result;
            if(!this.authenticationData.TryGetValue(this.CurrentRole.Id, out result)) {
                var items = this.PageTreeViewItems.ToArray();
                result = new List<PageData>(items.Select(item => new PageData {
                    Id = ((Page)item.Tag).Id,
                    AllowAccess = item.CheckState == ToggleState.On,
                }));
                this.authenticationData.Add(this.CurrentRole.Id, result);
            }
            return result;
        }

        private string GetSubmitXml() {
            if(this.authenticationData.Count == 0)
                return null;

            var eAuthenticationData = new XElement("AuthenticationData");
            foreach(var kv in this.authenticationData) {
                var eRole = new XElement("Role", new XAttribute("Id", kv.Key));
                foreach(var pageData in kv.Value) {
                    var ePage = new XElement("Page", new XAttribute("Id", pageData.Id), new XAttribute("AllowAccess", pageData.AllowAccess));
                    if(pageData.Operations != null)
                        ePage.Add(new XElement("Actions", pageData.Operations.Select(op => new XElement("Id", op))));
                    eRole.Add(ePage);
                }
                eAuthenticationData.Add(eRole);
            }
            return eAuthenticationData.ToString(SaveOptions.DisableFormatting | SaveOptions.OmitDuplicateNamespaces);
        }

        private void ReloadPages(IEnumerable<int> pageIds, bool discardCache = false) {
            Action setTreeView = () => {
                this.PagesTreeView.SelectionChanged -= this.Pages_SelectionChanged;
                var expandedItems = this.PagesTreeView.Items.Cast<RadTreeViewItem>().Where(item => item.IsExpanded).ToList();
                expandedItems.AddRange(expandedItems.SelectMany(item => item.Items).Cast<RadTreeViewItem>().Where(item => item.IsExpanded).ToArray());
                var expandedIds = expandedItems.Select(item => ((Page)item.Tag).Id);
                var selectedId = this.PagesTreeView.SelectedItem == null ? 0 : ((Page)((RadTreeViewItem)this.PagesTreeView.SelectedItem).Tag).Id;
                this.PagesTreeView.Items.Clear();
                this.PagesTreeView.SelectionChanged += this.Pages_SelectionChanged;
                Helper.SetItemsControlByEntity<Page, RadTreeViewItem>(this.PagesTreeView, this.pagesCache, "Id", "ParentId", "Sequence", (page, item) => {
                    item.Header = page.Name;
                    item.IsSelected = page.Id == selectedId;
                    switch(page.Type) {
                        case 0:
                        case 1:
                            item.FontWeight = FontWeights.Bold;
                            item.IsExpanded = expandedIds.Contains(page.Id);
                            item.SetBinding(RadTreeViewItem.CheckStateProperty, new Binding {
                                Source = item,
                                Mode = BindingMode.OneWay,
                                Converter = new TreeViewIsCheckedConverter(),
                            });
                            break;
                        case 2:
                            item.CheckState = pageIds.Contains(page.Id) ? ToggleState.On : ToggleState.Off;
                            item.Checked += this.TreeViewItem_Checked_Unchecked;
                            item.Unchecked += this.TreeViewItem_Checked_Unchecked;
                            break;
                    }
                });
            };

            if(this.pagesCache != null && !discardCache)
                this.Dispatcher.BeginInvoke(setTreeView);
            else {
                var domainContext = new SecurityDomainContext();
                domainContext.Load(domainContext.GetAuthorizablePagesQuery(), loadOp => {
                    if(loadOp.HasError)
                        return;
                    this.pagesCache = loadOp.Entities;
                    this.Dispatcher.BeginInvoke(setTreeView);
                }, null);
            }
        }

        private void ReloadRoles(bool discardCache = false) {
            Action setComboBox = () => {
                this.RolesComboBox.SelectionChanged -= this.Roles_SelectionChanged;
                var selectedIndex = this.RolesComboBox.SelectedIndex > -1 ? this.RolesComboBox.SelectedIndex : 0;
                this.RolesComboBox.Items.Clear();
                this.RolesComboBox.SelectionChanged += this.Roles_SelectionChanged;
                Helper.SetItemsControlByEntity<Role, RadComboBoxItem>(this.RolesComboBox, this.rolesCache.Where(r => !r.IsAdmin), onItemAdded: (role, item) => {
                    item.Content = role.Name;
                });
                if(this.RolesComboBox.HasItems)
                    this.RolesComboBox.SelectedIndex = selectedIndex < this.RolesComboBox.Items.Count ? selectedIndex : 0;
            };

            if(this.rolesCache != null && !discardCache)
                this.Dispatcher.BeginInvoke(setComboBox);
            else {
                var domainContext = new SecurityDomainContext();
                domainContext.Load(domainContext.GetAuthorizableRolesQuery(), loadOp => {
                    if(loadOp.HasError)
                        return;
                    this.rolesCache = loadOp.Entities;
                    this.Dispatcher.BeginInvoke(setComboBox);
                }, null);
            }
        }

        private void Roles_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if(this.CurrentRole == null)
                return;

            var currentRoleId = this.CurrentRole.Id;
            if(this.authenticationData.ContainsKey(currentRoleId))
                this.ReloadPages(this.authenticationData[currentRoleId].Where(pageData => pageData.AllowAccess).Select(pageData => pageData.Id).ToArray());
            else {
                int[] pageIds;
                if(this.authorizedPages.TryGetValue(currentRoleId, out pageIds))
                    this.ReloadPages(pageIds);
                else {
                    this.MainBusyIndicator.IsBusy = true;
                    var domainContext = new SecurityDomainContext();
                    domainContext.GetAuthorizedPageIdsByRoleId(currentRoleId, invokeOp => {
                        try {
                            if(invokeOp.HasError)
                                return;
                            this.authorizedPages[currentRoleId] = pageIds = invokeOp.Value;
                            this.ReloadPages(pageIds);
                        } finally {
                            this.MainBusyIndicator.IsBusy = false;
                        }
                    }, null);
                }
            }
        }

        private void Pages_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            this.MainPanel.Visibility = Visibility.Collapsed;

            if(this.CurrentRole == null || this.CurrentPage == null || this.CurrentPage.Type != 2)
                return;

            IEnumerable<string> actionIds = null;
            if(this.authenticationData.ContainsKey(this.CurrentRole.Id))
                actionIds = this.authenticationData[this.CurrentRole.Id].Single(pageData => pageData.Id == this.CurrentPage.Id).Operations;

            this.MainPanel.SetAuthorizationNode(this.CurrentRole, this.CurrentPage, actionIds);
        }

        private void MainPanel_DataChanged(object sender, EventArgs e) {
            if(this.MainPanel.CurrentRole == null || this.MainPanel.CurrentPage == null)
                return;

            this.EnsureRoleData().Single(pd => pd.Id == this.MainPanel.CurrentPage.Id).Operations = this.MainPanel.SelectedOperationIds;
        }

        private void TreeViewItem_Checked_Unchecked(object sender, RadRoutedEventArgs e) {
            var pagesData = this.EnsureRoleData();
            if(pagesData == null)
                return;

            var treeViewItem = (RadTreeViewItem)sender;
            var page = (Page)treeViewItem.Tag;
            pagesData.Single(p => p.Id == page.Id).AllowAccess = treeViewItem.CheckState == ToggleState.On;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e) {
            var xml = this.GetSubmitXml();
            if(xml == null)
                return;

            this.MainBusyIndicator.IsBusy = true;
            var domainContext = new SecurityDomainContext();
            domainContext.SetRoleAuthorizationData(xml, invokeOp => {
                try {
                    if(invokeOp.HasError)
                        return;
                    this.authenticationData.Clear();
                } finally {
                    this.MainBusyIndicator.IsBusy = false;
                }
            }, null);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e) {
            this.ReloadRoles();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            //this.SetTitle("页面授权");
        }

        public PageAuthorization() {
            this.InitializeComponent();
            this.authorizedPages = new Dictionary<int, int[]>();
            this.authenticationData = new Dictionary<int, List<PageData>>();
            this.RolesComboBox.SelectionChanged += this.Roles_SelectionChanged;
            this.PagesTreeView.SelectionChanged += this.Pages_SelectionChanged;
            this.MainPanel.DataChanged += this.MainPanel_DataChanged;
        }
    }
}
