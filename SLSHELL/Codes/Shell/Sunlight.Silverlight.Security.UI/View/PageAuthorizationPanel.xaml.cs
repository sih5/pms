﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows;
using System.Xml.Linq;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.ViewModel.Contract;

namespace Sunlight.Silverlight.Security.View {
    public partial class PageAuthorizationPanel {
        public sealed class Button {
            private bool isChecked;
            public event EventHandler ValueChanged;

            public string UniqueId {
                get;
                internal set;
            }

            public string Caption {
                get;
                internal set;
            }

            public Uri ImageUri {
                get;
                internal set;
            }

            public bool IsChecked {
                get {
                    return this.isChecked;
                }
                set {
                    if(this.isChecked == value)
                        return;
                    this.isChecked = value;
                    this.OnValueChanged();
                }
            }

            private void OnValueChanged() {
                var handler = this.ValueChanged;
                if(handler != null)
                    handler(this, EventArgs.Empty);
            }
        }

        public sealed class ButtonGroup {
            public string Name {
                get;
                internal set;
            }

            public IEnumerable<Button> Buttons {
                get;
                internal set;
            }
        }

        public sealed class ViewModel : INotifyPropertyChanged {
            private IEnumerable<ButtonGroup> buttonGroups;
            public event PropertyChangedEventHandler PropertyChanged;

            public IEnumerable<ButtonGroup> ButtonGroups {
                get {
                    return this.buttonGroups;
                }
                internal set {
                    this.buttonGroups = value;
                    this.OnPropertyChanged("ButtonGroups");
                }
            }

            public void OnPropertyChanged(string propertyName) {
                var handler = this.PropertyChanged;
                if(handler != null)
                    handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private static Assembly[] loadedAssemblies;

        private static IDataGridViewModel GetDataGridViewModelByTypeName(string parameter) {
            if(parameter == null)
                return null;
            var pageViewModelType = Type.GetType(parameter);
            return pageViewModelType != null ? Activator.CreateInstance(pageViewModelType) as IDataGridViewModel : null;
        }

        private static object GetViewInstanceByXamlUri(string xamlUri) {
            var resourceInfo = Application.GetResourceStream(new Uri(xamlUri, UriKind.Relative));
            if(resourceInfo == null)
                return null;

            string xaml;
            using(var reader = new StreamReader(resourceInfo.Stream))
                xaml = reader.ReadToEnd();
            var match = Regex.Match(xaml, "x:Class=\"([^\"]+?)\"");
            if(!match.Success)
                return null;

            if(loadedAssemblies == null)
                loadedAssemblies = (from part in Deployment.Current.Parts
                                    let info = Application.GetResourceStream(new Uri(part.Source, UriKind.Relative))
                                    let assembly = part.Load(info.Stream)
                                    where assembly.FullName.StartsWith("Sunlight.") && !assembly.FullName.Contains(".resources,")
                                    select assembly).ToArray();

            var typeName = match.Groups[1].Value;
            Type type = null;
            foreach(var assembly in loadedAssemblies) {
                type = assembly.GetType(typeName);
                if(type != null)
                    break;
            }
            if(type == null)
                return null;

            return Activator.CreateInstance(type);
        }

        private readonly ViewModel viewModel;
        public event EventHandler DataChanged;

        public Role CurrentRole {
            get;
            private set;
        }

        public Page CurrentPage {
            get;
            private set;
        }

        public IEnumerable<string> SelectedOperationIds {
            get {
                return this.viewModel.ButtonGroups.SelectMany(g => g.Buttons).Where(b => b.IsChecked).Select(b => b.UniqueId).ToArray();
            }
        }

        private void OnDataChanged() {
            var handler = this.DataChanged;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        private IEnumerable<ButtonGroup> GetButtonGroupsForDataGridViewModel(Page page, IEnumerable<string> actionIds) {
            if(page == null)
                throw new ArgumentNullException("page");
            if(actionIds == null)
                throw new ArgumentNullException("actionIds");

            var dataGridViewModel = GetDataGridViewModelByTypeName(page.Parameter);
            if(dataGridViewModel == null)
                return Enumerable.Empty<ButtonGroup>();

            return dataGridViewModel.OperationGroupViewModels.Select(operationGroupViewModel => new ButtonGroup {
                Name = operationGroupViewModel.Name,
                Buttons = operationGroupViewModel.OperationViewModels.Where(operationViewModel => !string.IsNullOrEmpty(operationViewModel.UniqueId)).Select(operationViewModel => {
                    var button = new Button {
                        UniqueId = operationViewModel.UniqueId,
                        Caption = operationViewModel.Name,
                        ImageUri = operationViewModel.ImageUri,
                        IsChecked = actionIds.Contains(operationViewModel.UniqueId),
                    };
                    button.ValueChanged += (sender, e) => this.OnDataChanged();
                    return button;
                }).ToArray(),
            }).ToArray();
        }

        private IEnumerable<ButtonGroup> GetButtonGroupsForView(Page page, IEnumerable<string> actionIds) {
            if(page == null)
                throw new ArgumentNullException("page");
            if(actionIds == null)
                throw new ArgumentNullException("actionIds");

            var view = GetViewInstanceByXamlUri(page.Parameter);
            if(view == null)
                return Enumerable.Empty<ButtonGroup>();

            var result = new List<ButtonGroup>();

            var actionPanelView = view as IActionPanelView;
            if(actionPanelView != null)
                result.AddRange((actionPanelView).ActionItemGroups.Select(actionItemGroup => new ButtonGroup {
                    Name = actionItemGroup.Title,
                    Buttons = actionItemGroup.ActionItems.Where(actionItem => !string.IsNullOrEmpty(actionItem.UniqueId)).Select(actionItem => {
                        var uniqueId = string.Format("{0}|{1}", actionItemGroup.UniqueId, actionItem.UniqueId);
                        var button = new Button {
                            UniqueId = uniqueId,
                            Caption = actionItem.Title,
                            ImageUri = actionItem.ImageUri,
                            IsChecked = actionIds.Contains(uniqueId),
                        };
                        button.ValueChanged += (sender, e) => this.OnDataChanged();
                        return button;
                    }).ToArray(),
                }));

            return result.ToArray();
        }

        public PageAuthorizationPanel() {
            this.InitializeComponent();
            this.viewModel = new ViewModel();
        }

        public void SetAuthorizationNode(Role role, Page page, IEnumerable<string> actionIds = null) {
            if(role == null)
                throw new ArgumentNullException("role");
            if(page == null)
                throw new ArgumentNullException("page");

            this.CurrentRole = role;
            this.CurrentPage = page;

            var domainContext = new SecurityDomainContext();
            domainContext.GetRoleAuthorizationDataForPage(role.Id, page.Id, invokeOp => {
                try {
                    if(invokeOp.HasError || invokeOp.Value == null) {
                        this.Dispatcher.BeginInvoke(() => this.ActionsText.Text = invokeOp.Error.Message);
                        if(invokeOp.HasError)
                            invokeOp.MarkErrorAsHandled();
                        return;
                    }
                    var eAuthorizationData = XElement.Parse(invokeOp.Value);
                    var eActions = eAuthorizationData.Elements("Actions").FirstOrDefault();
                    if(eActions == null)
                        return;

                    if(actionIds == null)
                        actionIds = eActions.Elements("Action").Select(e => e.Value).ToArray();

                    switch(page.PageType) {
                        case "DataGrid":
                        case "DataGridEx":
                            this.viewModel.ButtonGroups = this.GetButtonGroupsForDataGridViewModel(page, actionIds);
                            break;
                        case "Custom":
                            this.viewModel.ButtonGroups = this.GetButtonGroupsForView(page, actionIds);
                            break;
                        default:
                            return;
                    }

                    var text = (this.viewModel.ButtonGroups != null && this.viewModel.ButtonGroups.Any()) ? string.Format("请选择角色“{0}”在本页面上可以访问的操作按钮：", role.Name) : "本页面没有可以用于授权的操作按钮。";
                    this.Dispatcher.BeginInvoke(() => this.ActionsText.Text = text);

                    this.DataContext = null;
                    this.DataContext = this.viewModel;
                } finally {
                    this.Visibility = Visibility.Visible;
                }
            }, null);
        }
    }
}
