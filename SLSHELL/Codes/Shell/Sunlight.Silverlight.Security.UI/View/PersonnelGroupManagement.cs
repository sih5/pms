﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.View.DataQueryView;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security.View {
    [PageMeta("Security", "Security", "PersonnelGroup", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON, "Personnel"
    })]
    public class PersonnelGroupManagement : SecurityDataManagementViewBase {
        private SecurityDataListViewBase dataListView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase modifyPasswordView;
        private const string DATA_MODIFYPASSWORD_VIEW = "_DataModifyPasswordView_";

        private SecurityDataListViewBase DataListView {
            get {
                return this.dataListView ?? (this.dataListView = (SecurityDataListViewBase)DI.GetView("PersonnelGroupDataListView"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PersonnelGroup");
                    this.dataEditView.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase ModifyPasswordView {
            get {
                if(this.modifyPasswordView == null) {
                    this.modifyPasswordView = DI.GetDataEditView("PersonnelModifyPassword");
                    this.modifyPasswordView.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.modifyPasswordView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.modifyPasswordView;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SecurityPersonnelGroup"
                };
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_QUERY_VIEW, () => this.DataListView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_MODIFYPASSWORD_VIEW, () => this.ModifyPasswordView);
        }

        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataListView.FilterItem != null)
                this.DataListView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_QUERY_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_QUERY_VIEW);
        }

        private void DataListView_SelectionChanged(object sender, EventArgs e) {
            this.CheckActionsCanExecute();
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_QUERY_VIEW);
            this.DataListView.FilterItem = filterItem;
            this.DataListView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var personnel = this.DataEditView.CreateObjectToEdit<Personnel>();
                    personnel.Photo = GlobalVar.DEFAULT_AVATAR_URL;
                    personnel.Password = GlobalVar.ASSIGNED_BY_SERVER;
                    personnel.Status = (int)SecurityUserStatus.有效;
                    //var panelFilter = ((CompositeFilterItem)this.DataListView.FilterItem).Filters.SingleOrDefault(f => f.MemberName == "EnterpriseId");
                    //personnel.EnterpriseId = (int)panelFilter.Value;
                    var db = this.DataListView as PersonnelGroupDataListView;
                    personnel.EnterpriseId = db.EnterpriseId;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.DataContext = null;
                    this.DataEditView.SetObjectToEditById(this.DataListView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    SecurityUtils.Confirm(SecurityUIStrings.DataManagementView_Confirm_Abandon, () => this.DataListView.UpdateSelectedEntities(entity => {
                        try {
                            if(((Personnel)entity).Can作废人员)
                                ((Personnel)entity).作废人员();
                        } catch(ValidationException e) {
                            UIHelper.ShowAlertMessage(e.Message);
                        }
                    }, () => {
                        UIHelper.ShowNotification(SecurityUIStrings.DataManagementView_Notification_AbandonSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
                case "Freeze":
                    SecurityUtils.Confirm(SecurityUIStrings.DataManagementView_Confirm_Freeze, () => this.DataListView.UpdateSelectedEntities(entity => {
                        try {
                            if(((Personnel)entity).Can冻结人员)
                                ((Personnel)entity).冻结人员();
                        } catch(ValidationException e) {
                            UIHelper.ShowAlertMessage(e.Message);
                        }
                    }, () => {
                        UIHelper.ShowNotification(SecurityUIStrings.DataManagementView_Notification_FreezeSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
                case "Recover":
                    SecurityUtils.Confirm(SecurityUIStrings.DataManagementView_Confirm_Recover, () => this.DataListView.UpdateSelectedEntities(entity => {
                        try {
                            if(((Personnel)entity).Can恢复人员)
                                ((Personnel)entity).恢复人员();
                        } catch(ValidationException e) {
                            UIHelper.ShowAlertMessage(e.Message);
                        }
                    }, () => {
                        UIHelper.ShowNotification(SecurityUIStrings.DataManagementView_Notification_RecoverSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
                case "ModifyPassword":
                    this.ModifyPasswordView.SetObjectToEditById(this.DataListView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_MODIFYPASSWORD_VIEW);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_QUERY_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case "Freeze":
                case "Recover":
                case "ModifyPassword":
                    if(this.DataListView.SelectedEntities == null)
                        return false;
                    var entities = this.DataListView.SelectedEntities.Cast<Personnel>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId == CommonActionKeys.EDIT || uniqueId == CommonActionKeys.ABANDON)
                        return entities[0].LoginId != "admin" && (entities[0].Status == (int)SecurityUserStatus.有效 || entities[0].Status == (int)SecurityUserStatus.冻结);
                    if(uniqueId == "Freeze")
                        return entities[0].Status == (int)SecurityUserStatus.有效 && entities[0].LoginId != "admin";
                    if(uniqueId == "Recover")
                        return entities[0].Status == (int)SecurityUserStatus.冻结;
                    return entities[0].Status == (int)SecurityUserStatus.有效;
                default:
                    return false;
            }
        }

        public PersonnelGroupManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = SecurityUIStrings.DataManagementView_Title_PersonnelGroup;
            this.DataListView.SelectionChanged += this.DataListView_SelectionChanged;
        }
    }
}
