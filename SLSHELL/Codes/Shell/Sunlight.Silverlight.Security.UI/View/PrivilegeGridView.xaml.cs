﻿using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Security.ViewModel;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Security.View {
    /// <summary>
    /// 权限矩阵的界面
    /// </summary>
    public partial class PrivilegeGridView {
        /// <summary>
        /// 根据 <see cref="PrivilegeGridViewModel.MaxActionCount"/> 绘制 GridView 列。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrivilegeGrid_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var viewModel = e.NewValue as PrivilegeGridViewModel;
            if(viewModel == null)
                return;

            this.MainGridView.Columns.RemoveItems((this.MainGridView.Columns.OfType<GridViewBoundColumnBase>()).Where(column => column.UniqueName != "PageNode").ToArray());
            for(var i = 0; i < viewModel.MaxActionCount; i++)
                this.MainGridView.Columns.Add(new GridViewDataColumn {
                    Header = "业务操作",
                    HeaderTextAlignment = TextAlignment.Center,
                    DataMemberBinding = new Binding(string.Format("Actions[{0}]", i)),
                    UniqueName = "ActionNode" + i,
                    MinWidth = 65
                });
            this.MainGridView.FilterDescriptors.Add(new FilterDescriptor("Type", FilterOperator.IsEqualTo, SecurityPageType.页面));
        }

        public PrivilegeGridView() {
            this.InitializeComponent();
            this.DataContextChanged += this.PrivilegeGrid_DataContextChanged;
        }
    }
}
