﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.View.DataQueryView;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security.View {
    [PageMeta("Security", "Security", "RoleGroup", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_FREEZE_RECOVER,"RoleGroup"
    })]
    public class RoleGroupManagement : SecurityDataManagementViewBase {
        private const string DATA_BATCHCANCEL_VIEW = "_BatchCancelPermissions_";
        private const string DATA_BATCHADD_VIEW = "_BatchAddPermissions_";
        private SecurityDataQueryViewBase dataQueryView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase roleGroupBatchAddPermissions;
        private DataEditViewBase roleGroupBatchCancelPermissions;
        private SecurityDataQueryViewBase DataQueryView {
            get {
                return this.dataQueryView ?? (this.dataQueryView = (SecurityDataQueryViewBase)DI.GetView("RoleGroupDataQueryView"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("RoleGroup");
                    this.dataEditView.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase RoleGroupBatchAddPermissions {
            get {
                if(this.roleGroupBatchAddPermissions == null) {
                    this.roleGroupBatchAddPermissions = DI.GetDataEditView("RoleGroupBatchAddPermissions");
                    this.roleGroupBatchAddPermissions.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.roleGroupBatchAddPermissions.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.roleGroupBatchAddPermissions;
            }
        }

        private DataEditViewBase RoleGroupBatchCancelPermissions {
            get {
                if(this.roleGroupBatchCancelPermissions == null) {
                    this.roleGroupBatchCancelPermissions = DI.GetDataEditView("RoleGroupBatchCancelPermissions");
                    this.roleGroupBatchCancelPermissions.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.roleGroupBatchCancelPermissions.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.roleGroupBatchCancelPermissions;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "RoleGroup"
                };
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_QUERY_VIEW, () => this.DataQueryView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_BATCHADD_VIEW, () => this.RoleGroupBatchAddPermissions);
            this.RegisterView(DATA_BATCHCANCEL_VIEW, () => this.RoleGroupBatchCancelPermissions);
        }

        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataQueryView.FilterItem != null)
                this.DataQueryView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_QUERY_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_QUERY_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_QUERY_VIEW);
            this.DataQueryView.FilterItem = filterItem;
            this.DataQueryView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var role = this.DataEditView.CreateObjectToEdit<Role>();
                    role.Status = (int)SecurityRoleStatus.有效;
                    role.IsAdmin = false;
                    var db = this.DataQueryView as RoleGroupDataQueryView;
                    if(db.DataGridView.Entities != null) {
                        var x = db.DataGridView.Entities.FirstOrDefault() as Role;
                        role.EnterpriseId = x.EnterpriseId;
                    } else if(db.EnterpriseId == 0 && db.DataGridView.Entities == null) {
                        role.EnterpriseId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    } else {
                        role.EnterpriseId = db.EnterpriseId;
                    }
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataQueryView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.FREEZE:
                    SecurityUtils.Confirm(SecurityUIStrings.DataManagementView_Confirm_Freeze, () => this.DataQueryView.UpdateSelectedEntities(entity => {
                        try {
                            if(((Role)entity).Can冻结角色)
                                ((Role)entity).冻结角色();
                        } catch(ValidationException e) {
                            UIHelper.ShowAlertMessage(e.Message);
                            return;
                        }
                    }, () => {
                        UIHelper.ShowNotification(SecurityUIStrings.DataManagementView_Notification_FreezeSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
                case CommonActionKeys.RECOVER:
                    SecurityUtils.Confirm(SecurityUIStrings.DataManagementView_Confirm_Recover, () => this.DataQueryView.UpdateSelectedEntities(entity => {
                        try {
                            if(((Role)entity).Can恢复角色)
                                ((Role)entity).恢复角色();
                        } catch(ValidationException e) {
                            UIHelper.ShowAlertMessage(e.Message);
                            return;
                        }
                    }, () => {
                        UIHelper.ShowNotification(SecurityUIStrings.DataManagementView_Notification_RecoverSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
                case "BatchCancelPermissions":
                    var q = from a in this.DataQueryView.SelectedEntities.Cast<Role>()
                            group a by a.Enterprise.EntNodeTemplateId
                                into m
                                select new {
                                    a = m.Count()
                                };
                    if(q.Count() > 1) {
                        UIHelper.ShowNotification("企业授权模板不同，无法进行授权");
                        return;
                    }
                    var roleId = this.DataQueryView.SelectedEntities.Select(r => r.GetIdentity()).ToArray();
                    if(roleId == null)
                        return;
                    this.RoleGroupBatchCancelPermissions.SetObjectToEditById(roleId);
                    this.SwitchViewTo(DATA_BATCHCANCEL_VIEW);
                    break;
                case "BatchAddPermissions":
                    var s = from a in this.DataQueryView.SelectedEntities.Cast<Role>()
                            group a by a.Enterprise.EntNodeTemplateId
                                into m
                                select new {
                                    a = m.Count()
                                };
                    if(s.Count() > 1) {
                        UIHelper.ShowNotification("企业授权模板不同，无法进行授权");
                        return;
                    }
                    var roleIds = this.DataQueryView.SelectedEntities.Select(r => r.GetIdentity()).ToArray();
                    if(roleIds == null)
                        return;
                    this.RoleGroupBatchAddPermissions.SetObjectToEditById(roleIds);
                    this.SwitchViewTo(DATA_BATCHADD_VIEW);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_QUERY_VIEW)
                return false;
            if(uniqueId == CommonActionKeys.ADD)
                return true;

            if(this.DataQueryView.SelectedEntities == null)
                return false;
            var roles = this.DataQueryView.SelectedEntities.Cast<Role>().ToArray();

            switch(uniqueId) {
                case CommonActionKeys.EDIT:
                    if(roles.Length != 1)
                        return false;
                    return roles[0].Status == (int)SecurityRoleStatus.有效 && !roles[0].IsAdmin;
                case CommonActionKeys.FREEZE:
                    if(roles.Length != 1)
                        return false;
                    return roles[0].Status == (int)SecurityRoleStatus.有效 && !roles[0].IsAdmin;
                case CommonActionKeys.RECOVER:
                    if(roles.Length != 1)
                        return false;
                    return roles[0].Status == (int)SecurityRoleStatus.冻结;
                case "BatchAddPermissions":
                case "BatchCancelPermissions":
                    if(roles.Length > 1)
                        return roles.All(e => !e.IsAdmin);
                    return false;
                default:
                    return false;
            }
        }

        public RoleGroupManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = SecurityUIStrings.DataManagementView_Title_RoleGroup;
        }
    }
}
