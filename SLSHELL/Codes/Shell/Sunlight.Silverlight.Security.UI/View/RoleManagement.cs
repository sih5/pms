﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security.View {
    [PageMeta("Security", "Security", "Role", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_FREEZE_RECOVER
    })]
    public class RoleManagement : SecurityDataManagementViewBase {
        private SecurityDataQueryViewBase dataQueryView;
        private DataEditViewBase dataEditView;
        private int customId = -1;

        private SecurityDataQueryViewBase DataQueryView {
            get {
                return this.dataQueryView ?? (this.dataQueryView = (SecurityDataQueryViewBase)DI.GetView("RoleDataQueryView"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("Role");
                    this.dataEditView.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "Role"
                };
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_QUERY_VIEW, () => this.DataQueryView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataQueryView.FilterItem != null)
                this.DataQueryView.ExecuteQueryDelayed();
            this.DataQueryView.ExchangeData(null, "SetEntity", this.customId);
            this.SwitchViewTo(DATA_QUERY_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_QUERY_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_QUERY_VIEW);
            this.DataQueryView.FilterItem = filterItem;
            this.DataQueryView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var role = this.DataEditView.CreateObjectToEdit<Role>();
                    role.Status = (int)SecurityRoleStatus.有效;
                    role.IsAdmin = false;
                    role.EnterpriseId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataQueryView.SelectedEntities.First().GetIdentity());
                    this.customId = (int)this.dataQueryView.SelectedEntities.First().GetIdentity();
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.FREEZE:
                    SecurityUtils.Confirm(SecurityUIStrings.DataManagementView_Confirm_Freeze, () => this.DataQueryView.UpdateSelectedEntities(entity => {
                        try {
                            if(((Role)entity).Can冻结角色)
                                ((Role)entity).冻结角色();
                        } catch(ValidationException e) {
                            UIHelper.ShowAlertMessage(e.Message);
                            return;
                        }
                    }, () => {
                        UIHelper.ShowNotification(SecurityUIStrings.DataManagementView_Notification_FreezeSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
                case CommonActionKeys.RECOVER:
                    SecurityUtils.Confirm(SecurityUIStrings.DataManagementView_Confirm_Recover, () => this.DataQueryView.UpdateSelectedEntities(entity => {
                        try {
                            if(((Role)entity).Can恢复角色)
                                ((Role)entity).恢复角色();
                        } catch(ValidationException e) {
                            UIHelper.ShowAlertMessage(e.Message);
                            return;
                        }
                    }, () => {
                        UIHelper.ShowNotification(SecurityUIStrings.DataManagementView_Notification_RecoverSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_QUERY_VIEW)
                return false;
            if(uniqueId == CommonActionKeys.ADD)
                return true;

            if(this.DataQueryView.SelectedEntities == null)
                return false;
            var roles = this.DataQueryView.SelectedEntities.Cast<Role>().ToArray();
            if(roles.Length != 1)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EDIT:
                    return roles[0].Status == (int)SecurityRoleStatus.有效 && !roles[0].IsAdmin;
                case CommonActionKeys.FREEZE:
                    return roles[0].Status == (int)SecurityRoleStatus.有效 && !roles[0].IsAdmin;
                case CommonActionKeys.RECOVER:
                    return roles[0].Status == (int)SecurityRoleStatus.冻结;
                default:
                    return false;
            }
        }

        public RoleManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = SecurityUIStrings.DataManagementView_Title_Role;
        }
    }
}
