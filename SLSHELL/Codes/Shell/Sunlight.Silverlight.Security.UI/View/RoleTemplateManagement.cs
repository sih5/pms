﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security.View {
    [PageMeta("Security", "Security", "RoleTemplate", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON
    })]
    public class RoleTemplateManagement : SecurityDataManagementViewBase {
        private SecurityDataQueryViewBase dataQueryView;
        private DataEditViewBase dataEditView;

        private SecurityDataQueryViewBase DataQueryView {
            get {
                return this.dataQueryView ?? (this.dataQueryView = (SecurityDataQueryViewBase)DI.GetView("RoleTemplateDataQueryView"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("RoleTemplate");
                    this.dataEditView.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "RoleTemplate"
                };
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_QUERY_VIEW, () => this.DataQueryView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataQueryView.FilterItem != null)
                this.DataQueryView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_QUERY_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_QUERY_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_QUERY_VIEW);
            this.DataQueryView.FilterItem = filterItem;
            this.DataQueryView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(Silverlight.View.ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var roleTemplate = this.DataEditView.CreateObjectToEdit<RoleTemplate>();
                    roleTemplate.Status = (int)SecurityCommonStatus.有效;
                    roleTemplate.EnterpriseId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataQueryView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    SecurityUtils.Confirm(SecurityUIStrings.DataManagementView_Confirm_Abandon, () => this.DataQueryView.UpdateSelectedEntities(entity => ((RoleTemplate)entity).Status = (int)SecurityCommonStatus.作废, () => {
                        UIHelper.ShowNotification(SecurityUIStrings.DataManagementView_Notification_AbandonSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_QUERY_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataQueryView.SelectedEntities == null)
                        return false;
                    var entities = this.DataQueryView.SelectedEntities.Cast<RoleTemplate>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)SecurityCommonStatus.有效;
                default:
                    return false;
            }
        }

        public RoleTemplateManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = SecurityUIStrings.DataManagementView_Title_RoleTemplate;
        }
    }
}
