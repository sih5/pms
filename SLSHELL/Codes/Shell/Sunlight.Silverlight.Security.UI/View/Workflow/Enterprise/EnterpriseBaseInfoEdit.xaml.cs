﻿
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Security.Panels.DataEdit;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.View.Workflow.Enterprise {
    public partial class EnterpriseBaseInfoEdit {
        private EnterpriseInfo enterpriseInfo;
        private string defaultPassword = string.Empty;

        private SecurityDomainContext DomainContext {
            get {
                return this.enterpriseInfo.DomainContext;
            }
        }

        public EnterpriseInfo EnterpriseInfo {
            get {
                return this.enterpriseInfo;
            }
            set {
                this.enterpriseInfo = value;
                this.NotifyPropertyChanged("EnterpriseInfo");
            }
        }

        public EnterpriseBaseInfoEdit() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += this.EnterpriseBaseInfoEdit_Loaded;
        }

        private void CreateUI() {
            var dataEditPanel = DI.GetDataEditPanel("EnterpriseBaseInfo");
            dataEditPanel.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(dataEditPanel);
        }

        private void EnterpriseBaseInfoEdit_Loaded(object sender, RoutedEventArgs e) {
            var enterprise = this.EnterpriseInfo.Enterprises.FirstOrDefault();
            if(enterprise == null)
                return;
            if(enterprise.Id == default(int)) {
                this.SetViewControlProperty(Visibility.Visible, false);
                this.DomainContext.Load(this.DomainContext.GetPasswordPoliciesQuery().Where(p => p.IsEnabled), loadOp => {
                    if(loadOp.HasError || !loadOp.Entities.Any())
                        return;
                    this.defaultPassword = loadOp.Entities.First().DefaultPassword;
                }, null);
            } else {
                this.SetViewControlProperty(Visibility.Collapsed, true);
            }
            this.DataContext = enterprise;
        }

        private void SetViewControlProperty(Visibility visibility, bool flag) {
            var grid = ((EnterpriseBaseInfoDataEditPanel)this.Root.Children[1]).Root;
            foreach(var control in grid.Children) {
                if(control is TextBlock) {
                    var textBlock = control as TextBlock;
                    if(textBlock.Name == "textPassword")
                        textBlock.Visibility = visibility;
                }
                if(control is PasswordBox)
                    control.Visibility = visibility;
                if(control is TextBox) {
                    var textBox = control as TextBox;
                    if(textBox.Name == "txtCode")
                        textBox.IsReadOnly = flag;
                }
            }
        }

        public bool ActionHandle() {
            var enterprise = this.EnterpriseInfo.Enterprises.FirstOrDefault();
            if(string.IsNullOrWhiteSpace(enterprise.Code)) {
                UIHelper.ShowNotification(Security.Resources.SecurityUIStrings.DataEditView_Validation_Enterprise_CodeIsNull);
                return false;
            }
            if(string.IsNullOrWhiteSpace(enterprise.Name)) {
                UIHelper.ShowNotification(Security.Resources.SecurityUIStrings.DataEditView_Validation_Enterprise_NameIsNull);
                return false;
            }
            if(enterprise.Id == default(int) && string.IsNullOrWhiteSpace(enterprise.Password)) {
                UIHelper.ShowNotification(Security.Resources.SecurityUIStrings.DataEditView_Validation_Enterprise_AdminPasswordIsNull);
                return false;
            }
            if(enterprise.EnterpriseCategoryId == default(int)) {
                UIHelper.ShowNotification(Security.Resources.SecurityUIStrings.DataEditView_Validation_Enterprise_CategoryIsNull);
                return false;
            }
            if(enterprise.Id == default(int) && enterprise.Password == GlobalVar.ASSIGNED_BY_SERVER)
                enterprise.Password = this.defaultPassword;
            return true;
        }
    }
}
