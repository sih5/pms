﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.ViewModel;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security.View.Workflow.Enterprise {
    public partial class EnterpriseEntNodeTemplateBatchEdit {
        private bool initialized;
        private DataGridViewBase entNodeTemplateDataGridView;
        private EnterpriseInfo enterpriseInfo;
        private readonly AuthorizeViewModel authorizeViewModel = new AuthorizeViewModel();
        private readonly List<EntNodeTemplateDetail> details = new List<EntNodeTemplateDetail>();

        private SecurityDomainContext DomainContext {
            get {
                return this.enterpriseInfo.DomainContext;
            }
        }

        public DataGridViewBase EntNodeTemplateDataGridView {
            get {
                if(this.entNodeTemplateDataGridView == null) {
                    this.entNodeTemplateDataGridView = DI.GetDataGridView("EntNodeTemplate");
                    this.entNodeTemplateDataGridView.SelectionChanged += this.entNodeTemplateDataGridView_SelectionChanged;
                }
                return this.entNodeTemplateDataGridView;
            }
        }

        public EnterpriseInfo EnterpriseInfo {
            get {
                return this.enterpriseInfo;
            }
            set {
                this.enterpriseInfo = value;
                this.NotifyPropertyChanged("EnterpriseInfo");
            }
        }

        private void CreateUI() {
            this.EntNodeTemplateDataGridView.SetValue(Grid.RowProperty, 1);
            this.EntNodeTemplateGrid.Children.Add(this.EntNodeTemplateDataGridView);
            var verticalLine = new Rectangle {
                Width = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(8, 0, 8, 0)
            };
            verticalLine.SetValue(Grid.RowProperty, 1);
            verticalLine.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(verticalLine);
        }

        private void Initialize() {
            this.DomainContext.Load(this.DomainContext.GetPagesWithDetailQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                this.authorizeViewModel.Initialize(loadOp.Entities, loadOp.AllEntities.Where(entity => entity is Node).Cast<Node>());
                this.MainAuthorizeView.DataContext = this.authorizeViewModel;
            }, null);
        }

        private void EnterpriseEntNodeTemplateBatchEdit_Loaded(object sender, RoutedEventArgs e) {
            this.EntNodeTemplateDataGridView.ExecuteQueryDelayed();
            if(!this.initialized) {
                this.initialized = true;
                this.CreateUI();
                this.Initialize();
            }
        }

        private void entNodeTemplateDataGridView_SelectionChanged(object sender, System.EventArgs e) {
            var selectedItem = this.EntNodeTemplateDataGridView.SelectedEntities.FirstOrDefault() as EntNodeTemplate;
            this.details.Clear();
            if(selectedItem == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetEntNodeTemplatesWithDetailByIdQuery(selectedItem.Id), LoadBehavior.RefreshCurrent, loadOp => {
                var entTemplate = loadOp.Entities.FirstOrDefault();
                if(entTemplate == null)
                    return;
                this.authorizeViewModel.SelectedNodes = entTemplate.EntNodeTemplateDetails.ToList().Select(entity => entity.Node).Where(node => node != null);
            }, null);
        }

        public EnterpriseEntNodeTemplateBatchEdit() {
            InitializeComponent();
            this.Loaded += this.EnterpriseEntNodeTemplateBatchEdit_Loaded;
        }

        public bool ActionHandle() {
            if(this.EntNodeTemplateDataGridView.SelectedEntities != null) {
                var entNodeTemplate = this.EntNodeTemplateDataGridView.SelectedEntities.Cast<EntNodeTemplate>().FirstOrDefault();
                if(entNodeTemplate == null) {
                    UIHelper.ShowNotification(SecurityUIStrings.WorkflowPage_Validation_Enterprise_EntNodeTemplateIsNull);
                    return false;
                }
                foreach(var enterprise in this.EnterpriseInfo.Enterprises) {
                    enterprise.EntNodeTemplateId = entNodeTemplate.Id;
                }
                return true;
            }
            UIHelper.ShowNotification(SecurityUIStrings.WorkflowPage_Validation_Enterprise_EntNodeTemplateIsNull);
            return false;
        }
    }
}
