﻿
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.ViewModel;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security.View.Workflow.Enterprise {
    public partial class EnterpriseEntNodeTemplateEdit {
        private bool initialized;
        private DataGridViewBase entNodeTemplateDataGridView;
        private EnterpriseInfo enterpriseInfo;
        private SecurityDataEditPanelBase enterpriseCommonEditPanel;
        private readonly AuthorizeViewModel authorizeViewModel = new AuthorizeViewModel();

        private SecurityDomainContext DomainContext {
            get {
                return this.enterpriseInfo.DomainContext;
            }
        }

        public DataGridViewBase EntNodeTemplateDataGridView {
            get {
                if(this.entNodeTemplateDataGridView == null) {
                    this.entNodeTemplateDataGridView = DI.GetDataGridView("EntNodeTemplateForEdit");
                    this.entNodeTemplateDataGridView.SelectionChanged += this.entNodeTemplateDataGridView_SelectionChanged;
                }
                return this.entNodeTemplateDataGridView;
            }
        }

        public EnterpriseInfo EnterpriseInfo {
            get {
                return this.enterpriseInfo;
            }
            set {
                this.enterpriseInfo = value;
                this.NotifyPropertyChanged("EnterpriseInfo");
            }
        }

        public SecurityDataEditPanelBase EnterpriseCommonEditPanel {
            get {
                return this.enterpriseCommonEditPanel ?? (this.enterpriseCommonEditPanel = DI.GetDataEditPanel("EnterpriseCommon") as SecurityDataEditPanelBase);
            }
        }

        private void CreateUI() {
            this.EnterpriseCommonEditPanel.SetValue(HorizontalAlignmentProperty, HorizontalAlignment.Left);
            this.EnterpriseCommonEditPanel.SetValue(VerticalAlignmentProperty, VerticalAlignment.Center);
            this.EnterpriseCommonEditPanel.SetValue(Grid.RowProperty, 1);
            this.EnterpriseCommonEditPanel.SetValue(Grid.ColumnSpanProperty, 3);
            this.Root.Children.Add(this.EnterpriseCommonEditPanel);
            var horizontalLine = new Rectangle {
                Height = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(0, 8, 0, 8),
            };
            horizontalLine.SetValue(Grid.ColumnSpanProperty, 3);
            horizontalLine.SetValue(Grid.RowProperty, 2);
            this.Root.Children.Add(horizontalLine);
            this.EntNodeTemplateDataGridView.SetValue(Grid.RowProperty, 1);
            this.EntNodeTemplateGrid.Children.Add(this.EntNodeTemplateDataGridView);
            var verticalLine = new Rectangle {
                Width = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(8, 0, 8, 0)
            };
            verticalLine.SetValue(Grid.ColumnProperty, 1);
            verticalLine.SetValue(Grid.RowProperty, 3);
            this.Root.Children.Add(verticalLine);
        }

        private void Initialize() {
            this.DomainContext.Load(this.DomainContext.GetPagesWithDetailQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                this.authorizeViewModel.Initialize(loadOp.Entities, loadOp.AllEntities.Where(entity => entity is Node).Cast<Node>());
                this.MainAuthorizeView.DataContext = this.authorizeViewModel;
            }, null);
        }

        private void entNodeTemplateDataGridView_SelectionChanged(object sender, System.EventArgs e) {
            var selectedItem = this.EntNodeTemplateDataGridView.SelectedEntities.FirstOrDefault() as EntNodeTemplate;
            if(selectedItem == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetEntNodeTemplatesWithDetailByIdQuery(selectedItem.Id), LoadBehavior.RefreshCurrent, loadOp => {
                var entTemplate = loadOp.Entities.FirstOrDefault();
                if(entTemplate == null)
                    return;
                this.authorizeViewModel.SelectedNodes = entTemplate.EntNodeTemplateDetails.ToList().Select(entity => entity.Node).Where(node => node != null);
            }, null);
        }

        private void EnterpriseEntNodeTemplateEdit_Loaded(object sender, RoutedEventArgs e) {
            this.SetValue(DataContextProperty, this.EnterpriseInfo.Enterprises.FirstOrDefault());
            this.EntNodeTemplateDataGridView.ExecuteQueryDelayed();
            if(this.initialized)
                return;
            this.initialized = true;
            this.CreateUI();
            this.Initialize();
        }

        public EnterpriseEntNodeTemplateEdit() {
            InitializeComponent();
            this.Loaded += this.EnterpriseEntNodeTemplateEdit_Loaded;
        }

        public bool ActionHandle() {
            if(this.EntNodeTemplateDataGridView.SelectedEntities != null) {
                if(this.EntNodeTemplateDataGridView.SelectedEntities.Count() > 1) {
                    UIHelper.ShowNotification(SecurityUIStrings.WorkflowPage_Validation_Enterprise_EntNodeTemplateIsNotSingle);
                    return false;
                }
                var entNodeTemplate = this.EntNodeTemplateDataGridView.SelectedEntities.Cast<EntNodeTemplate>().FirstOrDefault();
                if(entNodeTemplate == null) {
                    UIHelper.ShowNotification(SecurityUIStrings.WorkflowPage_Validation_Enterprise_EntNodeTemplateIsNull);
                    return false;
                }
                this.EnterpriseInfo.Enterprises.First().EntNodeTemplateId = entNodeTemplate.Id;
                return true;
            }
            UIHelper.ShowNotification(SecurityUIStrings.WorkflowPage_Validation_Enterprise_EntNodeTemplateIsNull);
            return false;
        }
    }
}
