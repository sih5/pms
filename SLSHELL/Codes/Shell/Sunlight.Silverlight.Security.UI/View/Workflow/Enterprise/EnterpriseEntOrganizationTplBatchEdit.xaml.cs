﻿
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Security.Controls;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.Workflow.Enterprise {
    public partial class EnterpriseEntOrganizationTplBatchEdit {
        private DataGridViewBase entOrganizationTplDataGridView;
        private EnterpriseInfo enterpriseInfo;
        private EntOrganizationTplDetailTreeView entOrganizationTplDetailTreeView;

        public DataGridViewBase EntOrganizationTplDataGridView {
            get {
                if(this.entOrganizationTplDataGridView == null) {
                    this.entOrganizationTplDataGridView = DI.GetDataGridView("EntOrganizationTpl");
                    this.entOrganizationTplDataGridView.SelectionChanged += this.entOrganizationTplDataGridView_SelectionChanged;
                }
                return this.entOrganizationTplDataGridView;
            }
        }

        public EnterpriseInfo EnterpriseInfo {
            get {
                return this.enterpriseInfo;
            }
            set {
                this.enterpriseInfo = value;
                this.NotifyPropertyChanged("EnterpriseInfo");
            }
        }

        public EntOrganizationTplDetailTreeView EntOrganizationTplDetailTreeView {
            get {
                return this.entOrganizationTplDetailTreeView ?? (this.entOrganizationTplDetailTreeView = (EntOrganizationTplDetailTreeView)DI.GetDetailPanel("EntOrganizationTplDetail"));
            }
        }

        private void entOrganizationTplDataGridView_SelectionChanged(object sender, System.EventArgs e) {
            this.EntOrganizationTplDetailTreeView.DataContext = this.EntOrganizationTplDataGridView.SelectedEntities.Count() == 1 ? this.EntOrganizationTplDataGridView.SelectedEntities.First() : null;
        }

        private void CreateUI() {
            this.EntOrganizationTplDataGridView.SetValue(Grid.RowProperty, 1);
            this.EntOrganizationTplGrid.Children.Add(this.EntOrganizationTplDataGridView);
            var verticalLine = new Rectangle {
                Width = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(8, 0, 8, 0)
            };
            verticalLine.SetValue(Grid.RowProperty, 1);
            verticalLine.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(verticalLine);
            var organizationTab = new RadTabControl();
            organizationTab.Items.Add(new RadTabItem {
                Header = Utils.GetEntityLocalizedName(typeof(Web.Enterprise), "Organizations"),
                Content = this.EntOrganizationTplDetailTreeView
            });
            organizationTab.SetValue(Grid.RowProperty, 1);
            organizationTab.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(organizationTab);
        }

        public EnterpriseEntOrganizationTplBatchEdit() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += this.EnterpriseEntOrganizationTplBatchEdit_Loaded;
        }

        private void EnterpriseEntOrganizationTplBatchEdit_Loaded(object sender, RoutedEventArgs e) {
            this.EntOrganizationTplDataGridView.ExecuteQueryDelayed();
        }

        public bool ActionHandle() {
            if(this.EntOrganizationTplDataGridView.SelectedEntities != null) {
                var count = this.EntOrganizationTplDataGridView.SelectedEntities.Count();
                if(count > 1) {
                    UIHelper.ShowNotification(SecurityUIStrings.WorkflowPage_Validation_Enterprise_EntOrganizationTplIsNotSingle);
                    return false;
                }
                if(this.EntOrganizationTplDataGridView.SelectedEntities.Any()) {
                    this.EnterpriseInfo.EntOrganizationTplId = this.EntOrganizationTplDataGridView.SelectedEntities.Cast<EntOrganizationTpl>().FirstOrDefault().Id;
                    return true;
                }
            }
            return true;
        }
    }
}
