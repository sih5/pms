﻿
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Security.Controls;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.View.Custom;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.View.Workflow.Enterprise {
    public partial class EnterpriseEntOrganizationTplEdit {
        private DataGridViewBase entOrganizationTplDataGridView;
        private EnterpriseInfo enterpriseInfo;
        private SecurityDataEditPanelBase enterpriseCommonEditPanel;
        private EntOrganizationTplDetailTreeView entOrganizationTplDetailTreeView;
        private RadTreeView organizationTreeView;
        private bool isExistOrganizations;
        private Visibility organizationGridVisibility;
        private Visibility errorMsgVisibility;

        private bool IsExistOrganizations {
            get {
                return this.isExistOrganizations;
            }
            set {
                this.isExistOrganizations = value;
                if(value) {
                    this.ErrorMsgVisibility = Visibility.Visible;
                    this.OrganizationGridVisibility = Visibility.Collapsed;
                } else {
                    this.ErrorMsgVisibility = Visibility.Collapsed;
                    this.OrganizationGridVisibility = Visibility.Visible;
                }
            }
        }

        public Visibility OrganizationGridVisibility {
            get {
                return this.organizationGridVisibility;
            }
            set {
                this.organizationGridVisibility = value;
                this.NotifyPropertyChanged("OrganizationGridVisibility");
            }
        }

        public Visibility ErrorMsgVisibility {
            get {
                return this.errorMsgVisibility;
            }
            set {
                this.errorMsgVisibility = value;
                this.NotifyPropertyChanged("ErrorMsgVisibility");
            }
        }

        private SecurityDomainContext DomainContext {
            get {
                return this.enterpriseInfo.DomainContext;
            }
        }

        public DataGridViewBase EntOrganizationTplDataGridView {
            get {
                if(this.entOrganizationTplDataGridView == null) {
                    this.entOrganizationTplDataGridView = DI.GetDataGridView("EntOrganizationTpl");
                    this.entOrganizationTplDataGridView.SelectionChanged += this.entOrganizationTplDataGridView_SelectionChanged;
                }
                return this.entOrganizationTplDataGridView;
            }
        }

        public EnterpriseInfo EnterpriseInfo {
            get {
                return this.enterpriseInfo;
            }
            set {
                this.enterpriseInfo = value;
                this.NotifyPropertyChanged("EnterpriseInfo");
            }
        }

        public SecurityDataEditPanelBase EnterpriseCommonEditPanel {
            get {
                return this.enterpriseCommonEditPanel ?? (this.enterpriseCommonEditPanel = DI.GetDataEditPanel("EnterpriseCommon") as SecurityDataEditPanelBase);
            }
        }

        public EntOrganizationTplDetailTreeView EntOrganizationTplDetailTreeView {
            get {
                return this.entOrganizationTplDetailTreeView ?? (this.entOrganizationTplDetailTreeView = (EntOrganizationTplDetailTreeView)DI.GetDetailPanel("EntOrganizationTplDetail"));
            }
        }

        public RadTreeView OrganizationTreeView {
            get {
                return this.organizationTreeView ?? (this.organizationTreeView = new OrganizationTreeViewForEnterprise());
            }
        }

        public EnterpriseEntOrganizationTplEdit() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += this.EnterpriseEntOrganizationTplEdit_Loaded;
        }

        private void entOrganizationTplDataGridView_SelectionChanged(object sender, System.EventArgs e) {
            this.EntOrganizationTplDetailTreeView.DataContext = this.EntOrganizationTplDataGridView.SelectedEntities.Count() == 1 ? this.EntOrganizationTplDataGridView.SelectedEntities.First() : null;
        }

        private void EnterpriseEntOrganizationTplEdit_Loaded(object sender, RoutedEventArgs e) {
            var enterprise = this.EnterpriseInfo.Enterprises.FirstOrDefault();
            if(enterprise == null)
                return;
            var enterpriseId = enterprise.Id;
            if(enterpriseId == default(int)) {
                this.IsExistOrganizations = false;
            } else {
                this.DomainContext.Load(this.DomainContext.GetOrganizationsQuery().Where(o => o.EnterpriseId == enterpriseId && o.Type != (int)SecurityOrganizationOrganizationType.企业), loadOp => {
                    if(loadOp.HasError)
                        return;
                    this.IsExistOrganizations = loadOp.Entities.Any();
                    if(!IsExistOrganizations)
                        this.EntOrganizationTplDataGridView.ExecuteQueryDelayed();
                }, null);
            }
            this.SetValue(DataContextProperty, enterprise);
            this.OrganizationTreeView.SetValue(DataContextProperty, enterprise);
        }

        private void CreateUI() {
            this.EnterpriseCommonEditPanel.SetValue(HorizontalAlignmentProperty, HorizontalAlignment.Left);
            this.EnterpriseCommonEditPanel.SetValue(VerticalAlignmentProperty, VerticalAlignment.Center);
            this.EnterpriseCommonEditPanel.SetValue(Grid.RowProperty, 1);
            this.EnterpriseCommonEditPanel.SetValue(Grid.ColumnSpanProperty, 3);
            this.Root.Children.Add(this.EnterpriseCommonEditPanel);
            var horizontalLine = new Rectangle {
                Height = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(0, 8, 0, 8),
            };
            horizontalLine.SetValue(Grid.ColumnSpanProperty, 3);
            horizontalLine.SetValue(Grid.RowProperty, 2);
            this.Root.Children.Add(horizontalLine);
            var verticalLine = new Rectangle {
                Width = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(8, 0, 8, 0)
            };
            verticalLine.SetValue(Grid.ColumnProperty, 1);
            verticalLine.SetValue(Grid.RowProperty, 3);
            this.Root.Children.Add(verticalLine);

            this.EntOrganizationTplDataGridView.SetValue(Grid.RowProperty, 1);
            this.EntOrganizationTplDataGridView.SetBinding(VisibilityProperty, new Binding {
                Mode = BindingMode.TwoWay,
                Path = new PropertyPath("OrganizationGridVisibility"),
                RelativeSource = new RelativeSource {
                    AncestorType = typeof(EnterpriseEntOrganizationTplEdit)
                }
            });
            this.EntOrganizationTplGrid.Children.Add(this.EntOrganizationTplDataGridView);
            var organizationTab = new RadTabControl();
            organizationTab.Items.Add(new RadTabItem {
                Header = Utils.GetEntityLocalizedName(typeof(Web.Enterprise), "Organizations"),
                Content = this.EntOrganizationTplDetailTreeView
            });
            organizationTab.SetValue(Grid.ColumnProperty, 2);
            organizationTab.SetValue(Grid.RowProperty, 3);
            organizationTab.SetBinding(VisibilityProperty, new Binding {
                Mode = BindingMode.TwoWay,
                Path = new PropertyPath("OrganizationGridVisibility"),
                RelativeSource = new RelativeSource {
                    AncestorType = typeof(EnterpriseEntOrganizationTplEdit)
                }
            });
            this.txtTip.SetBinding(VisibilityProperty, new Binding {
                Mode = BindingMode.TwoWay,
                Path = new PropertyPath("OrganizationGridVisibility"),
                RelativeSource = new RelativeSource {
                    AncestorType = typeof(EnterpriseEntOrganizationTplEdit)
                }
            });
            this.Root.Children.Add(organizationTab);

            var textBlock = new TextBlock {
                Text = SecurityUIStrings.WorkflowPage_Prompt_Enterprise_OrganizationsIsNotNull
            };
            textBlock.SetValue(VerticalAlignmentProperty, VerticalAlignment.Center);
            textBlock.SetValue(Grid.RowProperty, 3);
            textBlock.SetBinding(VisibilityProperty, new Binding {
                Mode = BindingMode.TwoWay,
                Path = new PropertyPath("ErrorMsgVisibility"),
                RelativeSource = new RelativeSource {
                    AncestorType = typeof(EnterpriseEntOrganizationTplEdit)
                }
            });
            this.Root.Children.Add(textBlock);

            var radTabControl = new RadTabControl {
                BackgroundVisibility = Visibility.Collapsed,
                Margin = new Thickness(3)
            };
            radTabControl.Items.Add(new RadTabItem {
                Header = Utils.GetEntityLocalizedName(typeof(Web.Enterprise), "Organizations"),
                Content = this.OrganizationTreeView
            });
            radTabControl.SetValue(Grid.ColumnProperty, 2);
            radTabControl.SetValue(Grid.RowProperty, 3);
            radTabControl.SetBinding(VisibilityProperty, new Binding {
                Mode = BindingMode.TwoWay,
                Path = new PropertyPath("ErrorMsgVisibility"),
                RelativeSource = new RelativeSource {
                    AncestorType = typeof(EnterpriseEntOrganizationTplEdit)
                }
            });
            this.Root.Children.Add(radTabControl);
        }

        public bool ActionHandle() {
            if(!this.IsExistOrganizations)
                if(this.EntOrganizationTplDataGridView.SelectedEntities != null) {
                    var count = this.EntOrganizationTplDataGridView.SelectedEntities.Count();
                    if(count > 1) {
                        UIHelper.ShowNotification(SecurityUIStrings.WorkflowPage_Validation_Enterprise_EntOrganizationTplIsNotSingle);
                        return false;
                    }
                    if(this.EntOrganizationTplDataGridView.SelectedEntities.Any()) {
                        this.EnterpriseInfo.EntOrganizationTplId = this.EntOrganizationTplDataGridView.SelectedEntities.Cast<EntOrganizationTpl>().First().Id;
                        return true;
                    }
                } else
                    this.EnterpriseInfo.EntOrganizationTplId = null;
            return true;
        }
    }
}
