﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.ViewModel;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security.View.Workflow.Enterprise {
    public partial class EnterpriseRoleTemplateBatchEdit {
        private bool initialized;
        private DataGridViewBase roleTemplateDataGridView;
        private EnterpriseInfo enterpriseInfo;
        private readonly AuthorizeViewModel authorizeViewModel = new AuthorizeViewModel();
        private readonly List<RoleTemplateRule> rules = new List<RoleTemplateRule>();

        private SecurityDomainContext DomainContext {
            get {
                return this.enterpriseInfo.DomainContext;
            }
        }

        public DataGridViewBase RoleTemplateDataGridView {
            get {
                if(this.roleTemplateDataGridView == null) {
                    this.roleTemplateDataGridView = DI.GetDataGridView("RoleTemplateForEnterprise");
                    this.roleTemplateDataGridView.SelectionChanged += this.roleTemplateDataGridView_SelectionChanged;
                }
                return this.roleTemplateDataGridView;
            }
        }

        public EnterpriseInfo EnterpriseInfo {
            get {
                return this.enterpriseInfo;
            }
            set {
                this.enterpriseInfo = value;
                this.NotifyPropertyChanged("EnterpriseInfo");
            }
        }

        private void CreateUI() {
            this.RoleTemplateDataGridView.SetValue(Grid.RowProperty, 1);
            this.RoleTemplateGrid.Children.Add(this.RoleTemplateDataGridView);
            var verticalLine = new Rectangle {
                Width = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(8, 0, 8, 0)
            };
            verticalLine.SetValue(Grid.RowProperty, 1);
            verticalLine.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(verticalLine);
        }

        private void Initialize() {
            this.DomainContext.Load(this.DomainContext.GetPagesWithDetailQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                this.authorizeViewModel.Initialize(loadOp.Entities, loadOp.AllEntities.Where(entity => entity is Node).Cast<Node>());
                this.MainAuthorizeView.DataContext = this.authorizeViewModel;
            }, null);
        }

        private void EnterpriseRoleTemplateBatchEdit_Loaded(object sender, RoutedEventArgs e) {
            this.RoleTemplateDataGridView.ExecuteQueryDelayed();
            if(!this.initialized) {
                this.initialized = true;
                this.CreateUI();
                this.Initialize();
            }
        }

        private void roleTemplateDataGridView_SelectionChanged(object sender, EventArgs e) {
            var selectedItem = this.RoleTemplateDataGridView.SelectedEntities.FirstOrDefault() as RoleTemplate;
            this.rules.Clear();
            if(selectedItem == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetRoleTemplatesWithDetailByIdQuery(selectedItem.Id), LoadBehavior.RefreshCurrent, loadOp => {
                var roleTemplate = loadOp.Entities.FirstOrDefault();
                if(roleTemplate == null)
                    return;
                this.authorizeViewModel.SelectedNodes = roleTemplate.RoleTemplateRules.ToList().Select(entity => entity.Node).Where(node => node != null);
            }, null);
        }

        public EnterpriseRoleTemplateBatchEdit() {
            InitializeComponent();
            this.Loaded += this.EnterpriseRoleTemplateBatchEdit_Loaded;
        }

        public void ActionHandle(Action action) {
            this.EnterpriseInfo.RoleTemplateIds = new int[0];
            if(this.RoleTemplateDataGridView.SelectedEntities != null)
                if(this.RoleTemplateDataGridView.SelectedEntities.Any())
                    this.EnterpriseInfo.RoleTemplateIds = this.RoleTemplateDataGridView.SelectedEntities.Cast<RoleTemplate>().Select(r => r.Id).ToArray();
            try {
                foreach(var enterprise in this.EnterpriseInfo.Enterprises) {
                    enterprise.ValidationErrors.Clear();
                    this.DomainContext.调整企业(enterprise, this.EnterpriseInfo.EntOrganizationTplId, this.EnterpriseInfo.RoleTemplateIds);
                }
                this.DomainContext.SubmitChanges(so => {
                    if(so.HasError) {
                        if(!so.IsErrorHandled)
                            so.MarkErrorAsHandled();
                        SecurityUtils.ShowDomainServiceOperationWindow(so);
                        return;
                    }
                    UIHelper.ShowNotification(SecurityUIStrings.DataEditView_Notification_Enterprise_UpdateSuccess);
                    action();
                }, null);
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
        }
    }
}
