﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.ViewModel;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security.View.Workflow.Enterprise {
    public partial class EnterpriseRoleTemplateEdit {
        private bool initialized;
        private DataGridViewBase roleTemplateDataGridView;
        private EnterpriseInfo enterpriseInfo;
        private SecurityDataEditPanelBase enterpriseCommonEditPanel;
        private readonly AuthorizeViewModel authorizeViewModel = new AuthorizeViewModel();

        public DataGridViewBase RoleTemplateDataGridView {
            get {
                if(this.roleTemplateDataGridView == null) {
                    this.roleTemplateDataGridView = DI.GetDataGridView("RoleTemplateForEnterprise");
                    this.roleTemplateDataGridView.SelectionChanged += this.roleTemplateDataGridView_SelectionChanged;
                }
                return this.roleTemplateDataGridView;
            }
        }

        public EnterpriseInfo EnterpriseInfo {
            get {
                return this.enterpriseInfo;
            }
            set {
                this.enterpriseInfo = value;
                this.NotifyPropertyChanged("EnterpriseInfo");
            }
        }

        public SecurityDataEditPanelBase EnterpriseCommonEditPanel {
            get {
                return this.enterpriseCommonEditPanel ?? (this.enterpriseCommonEditPanel = DI.GetDataEditPanel("EnterpriseCommon") as SecurityDataEditPanelBase);
            }
        }

        private SecurityDomainContext DomainContext {
            get {
                return this.EnterpriseInfo.DomainContext;
            }
        }

        private void CreateUI() {
            this.EnterpriseCommonEditPanel.SetValue(HorizontalAlignmentProperty, HorizontalAlignment.Left);
            this.EnterpriseCommonEditPanel.SetValue(VerticalAlignmentProperty, VerticalAlignment.Center);
            this.EnterpriseCommonEditPanel.SetValue(Grid.RowProperty, 1);
            this.EnterpriseCommonEditPanel.SetValue(Grid.ColumnSpanProperty, 3);
            this.Root.Children.Add(this.EnterpriseCommonEditPanel);
            var horizontalLine = new Rectangle {
                Height = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(0, 8, 0, 8),
            };
            horizontalLine.SetValue(Grid.ColumnSpanProperty, 3);
            horizontalLine.SetValue(Grid.RowProperty, 2);
            this.Root.Children.Add(horizontalLine);
            this.RoleTemplateDataGridView.SetValue(Grid.RowProperty, 1);
            this.RoleTemplateGrid.Children.Add(this.RoleTemplateDataGridView);
            var verticalLine = new Rectangle {
                Width = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(8, 0, 8, 0)
            };
            verticalLine.SetValue(Grid.ColumnProperty, 1);
            verticalLine.SetValue(Grid.RowProperty, 3);
            this.Root.Children.Add(verticalLine);
        }

        private void Initialize() {
            this.DomainContext.Load(this.DomainContext.GetPagesWithDetailQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                this.authorizeViewModel.Initialize(loadOp.Entities, loadOp.AllEntities.Where(entity => entity is Node).Cast<Node>());
                this.MainAuthorizeView.DataContext = this.authorizeViewModel;
            }, null);
        }

        private void EnterpriseRoleTemplateEdit_Loaded(object sender, RoutedEventArgs e) {
            this.SetValue(DataContextProperty, this.EnterpriseInfo.Enterprises.First());
            this.RoleTemplateDataGridView.ExecuteQueryDelayed();
            if(this.initialized)
                return;
            this.initialized = true;
            this.CreateUI();
            this.Initialize();
        }

        private void roleTemplateDataGridView_SelectionChanged(object sender, EventArgs e) {
            var selectedItem = this.RoleTemplateDataGridView.SelectedEntities.FirstOrDefault() as RoleTemplate;
            if(selectedItem == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetRoleTemplatesWithDetailByIdQuery(selectedItem.Id), LoadBehavior.RefreshCurrent, loadOp => {
                var roleTemplate = loadOp.Entities.FirstOrDefault();
                if(roleTemplate == null)
                    return;
                this.authorizeViewModel.SelectedNodes = roleTemplate.RoleTemplateRules.ToList().Select(entity => entity.Node).Where(node => node != null);
            }, null);
        }

        public EnterpriseRoleTemplateEdit() {
            InitializeComponent();
            this.Loaded += this.EnterpriseRoleTemplateEdit_Loaded;
        }

        public void ActionHandle(Action action) {
            this.EnterpriseInfo.RoleTemplateIds = new int[0];
            if(this.RoleTemplateDataGridView.SelectedEntities != null)
                if(this.RoleTemplateDataGridView.SelectedEntities.Any())
                    this.EnterpriseInfo.RoleTemplateIds = this.RoleTemplateDataGridView.SelectedEntities.Cast<RoleTemplate>().Select(r => r.Id).ToArray();
            var enterprise = this.EnterpriseInfo.Enterprises.FirstOrDefault();
            if(enterprise == null)
                return;
            enterprise.ValidationErrors.Clear();
            try {
                if(enterprise.Id == default(int)) {
                    this.DomainContext.Enterprises.Add(enterprise);
                    if(enterprise.Can创建企业)
                        enterprise.创建企业(this.EnterpriseInfo.EntOrganizationTplId, enterprise.Password, this.EnterpriseInfo.RoleTemplateIds);
                    this.DomainContext.SubmitChanges(so => {
                        if(so.HasError) {
                            if(!so.IsErrorHandled)
                                so.MarkErrorAsHandled();
                            SecurityUtils.ShowDomainServiceOperationWindow(so);
                            return;
                        }
                        UIHelper.ShowNotification(SecurityUIStrings.DataEditView_Notification_Enterprise_AddSuccess);
                        action();
                    }, null);
                } else {
                    if(enterprise.Can调整企业)
                        enterprise.调整企业(this.EnterpriseInfo.EntOrganizationTplId, this.EnterpriseInfo.RoleTemplateIds);
                    this.DomainContext.SubmitChanges(so => {
                        if(so.HasError) {
                            if(!so.IsErrorHandled)
                                so.MarkErrorAsHandled();
                            SecurityUtils.ShowDomainServiceOperationWindow(so);
                            return;
                        }
                        UIHelper.ShowNotification(SecurityUIStrings.DataEditView_Notification_Enterprise_UpdateSuccess);
                        action();
                    }, null);
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
        }
    }
}
