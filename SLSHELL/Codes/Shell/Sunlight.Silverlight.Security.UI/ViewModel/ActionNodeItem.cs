﻿namespace Sunlight.Silverlight.Security.ViewModel {
    /// <summary>
    /// 表示业务操作的 ViewModel
    /// </summary>
    public class ActionNodeItem : NodeItem {
        private bool isChecked;

        /// <summary>
        /// 该操作被选中
        /// </summary>
        public bool IsChecked {
            get {
                return this.isChecked;
            }
            set {
                this.isChecked = value;
                this.OnPropertyChanged("IsChecked");
            }
        }

        public ActionNodeItem(int id, string displayName)
            : base(id, displayName, null) {
        }
    }
}
