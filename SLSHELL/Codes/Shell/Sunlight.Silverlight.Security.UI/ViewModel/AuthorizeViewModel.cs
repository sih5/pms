﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Security.Controls;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.View;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.ViewModels;

namespace Sunlight.Silverlight.Security.ViewModel {
    /// <summary>
    ///     表达授权操作的 ViewModel
    /// </summary>
    public class AuthorizeViewModel : ViewModelBase {
        private readonly PrivilegeGridView privilegeGridView = new PrivilegeGridView();
        private ObservableCollection<SystemNodeItem> systemNodeItems;
        private IEnumerable<Node> allNodes = new List<Node>();

        private readonly PopupWindow privilegeGridWindow = new PopupWindow {
            Header = SecurityUIStrings.CustomControl_PopupWindow_Title_Nodes
        };

        private IEnumerable<PageNodeItem> pageNodeItems {
            get {
                return this.SystemNodeItems.SelectMany(system => system.GroupNodeItems).SelectMany(group => group.PageNodeItems);
            }
        }

        private IEnumerable<ActionNodeItem> actionNodeItems {
            get {
                return this.pageNodeItems.SelectMany(page => page.ActionNodeItems);
            }
        }

        /// <summary>
        ///     所有子系统的节点
        /// </summary>
        public ObservableCollection<SystemNodeItem> SystemNodeItems {
            get {
                return this.systemNodeItems;
            }
            private set {
                if(this.systemNodeItems != value) {
                    this.systemNodeItems = value;
                    this.OnPropertyChanged("SystemNodeItems");
                }
            }
        }

        /// <summary>
        ///     获取或设置被选中的<see cref="Node" />集合。
        /// </summary>
        public IEnumerable<Node> SelectedNodes {
            get {
                var checkedPages = this.pageNodeItems.Where(page => page.IsChecked).ToList();
                var pageNodes = this.allNodes.Where(node => checkedPages.Select(item => item.Id).Contains(node.CategoryId)
                                                    && node.CategoryType == (int)SecurityNodeCategoryType.页面);
                var checkedActions = checkedPages.SelectMany(page => page.ActionNodeItems.Where(action => action.IsChecked)).ToList();
                var actionNodes = this.allNodes.Where(node => checkedActions.Select(item => item.Id).Contains(node.CategoryId)
                                                    && node.CategoryType == (int)SecurityNodeCategoryType.操作);
                return pageNodes.Union(actionNodes);
            }
            set {
                this.Reset();
                var nodes = this.allNodes.Intersect(value).ToList();
                foreach(var pageNodeItem in nodes.Where(entity => entity.CategoryType == (int)SecurityNodeCategoryType.页面)
                                                 .Select(node => this.pageNodeItems.SingleOrDefault(page => page.Id == node.CategoryId))
                                                 .Where(pageNodeItem => pageNodeItem != null))
                    pageNodeItem.IsChecked = true;
                foreach(var actionNodeItem in nodes.Where(entity => entity.CategoryType == (int)SecurityNodeCategoryType.操作)
                                                   .Select(node => this.actionNodeItems.SingleOrDefault(action => action.Id == node.CategoryId))
                                                   .Where(actionNodeItem => actionNodeItem != null))
                    actionNodeItem.IsChecked = true;
            }
        }

        /// <summary>
        ///     显示权限矩阵
        /// </summary>
        public DelegateCommand ShowPrivilegeGrid {
            get;
            private set;
        }

        private void ShowPrivilegeGridAction() {
            this.privilegeGridView.DataContext = new PrivilegeGridViewModel(this.SystemNodeItems);
            this.privilegeGridWindow.Content = this.privilegeGridView;
            this.privilegeGridWindow.ShowDialog();
        }

        private void Reset() {
            foreach(var pageNodeItem in this.pageNodeItems) {
                pageNodeItem.IsChecked = false;
                pageNodeItem.SetActionsCheckState(false);
            }
        }

        /// <summary>
        ///     初始化 ViewModel，构造 <see cref="SystemNodeItems" /> 属性。
        /// </summary>
        /// <param name="pages"> 所有流程节点，包括系统/分组/页面 <see cref="Page" /> 、页面上的业务操作 <see cref="SecurityAction" /> </param>
        /// <param name="nodes"> <paramref name="pages" /> 参数中所关联的 <see cref="Page.Nodes" /> 和 <see cref="SecurityAction.Nodes" /> </param>
        public void Initialize(IEnumerable<Page> pages, IEnumerable<Node> nodes) {
            this.allNodes = nodes;
            this.Reset();
            pages = pages.ToList();
            var items = new List<SystemNodeItem>();
            var actions = pages.SelectMany(page => page.Actions).ToList();
            // ReSharper disable AccessToForEachVariableInClosure
            foreach(var system in pages.Where(entity => entity.Type == (int)SecurityPageType.系统).OrderBy(entity => entity.Sequence)) {
                var systemNodeItem = new SystemNodeItem(system.Id, system.Name, Utils.MakeServerUri(system.Icon));
                foreach(var group in pages.Where(entity => entity.Type == (int)SecurityPageType.分组 && entity.ParentId == system.Id)) {
                    var groupNodeItem = new GroupNodeItem(group.Id, group.Name);
                    foreach(var page in pages.Where(entity => entity.Type == (int)SecurityPageType.页面 && entity.ParentId == group.Id)) {
                        var pageNodeItem = new PageNodeItem(page.Id, page.Name, Utils.MakeServerUri(page.Icon));
                        foreach(var action in actions.Where(entity => entity.PageId == page.Id)) {
                            var actionNodeItem = new ActionNodeItem(action.Id, action.Name);
                            pageNodeItem.ActionNodeItems.Add(actionNodeItem);
                        }
                        groupNodeItem.PageNodeItems.Add(pageNodeItem);
                    }
                    systemNodeItem.GroupNodeItems.Add(groupNodeItem);
                }
                items.Add(systemNodeItem);
            }
            // ReSharper restore AccessToForEachVariableInClosure
            this.SystemNodeItems = new ObservableCollection<SystemNodeItem>(items);
        }

        public AuthorizeViewModel() {
            this.SystemNodeItems = new ObservableCollection<SystemNodeItem>();
            this.ShowPrivilegeGrid = new DelegateCommand(this.ShowPrivilegeGridAction);
        }
    }
}
