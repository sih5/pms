﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.ViewModel {
    public class ChangePasswordViewModel : ViewModelBase {
        private string photo;
        private string password, oldPassword, repeatPassword;
        private SecurityDomainContext domainContext;

        private SecurityDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new SecurityDomainContext());
            }
        }

        public string Photo {
            get {
                return this.photo;
            }
            private set {
                if(this.photo == value)
                    return;
                this.photo = value;
                this.NotifyOfPropertyChange(() => this.Photo);
            }
        }

        public string LoginId {
            get {
                return BaseApp.Current.CurrentUserData.UserCode;
            }
        }

        public string Name {
            get {
                return BaseApp.Current.CurrentUserData.UserName;
            }
        }

        public string OldPassword {
            get {
                return this.oldPassword;
            }
            set {
                if(this.oldPassword == value)
                    return;
                this.ValidateOldPassword(value);
                this.oldPassword = value;
                this.NotifyOfPropertyChange(() => this.OldPassword);
            }
        }

        public string Password {
            get {
                return this.password;
            }
            set {
                if(this.password == value)
                    return;
                this.ValidatePassword(value);
                this.password = value;
                this.NotifyOfPropertyChange(() => this.Password);
                this.ValidateRepeatPassword(this.RepeatPassword);
            }
        }

        public string RepeatPassword {
            get {
                return this.repeatPassword;
            }
            set {
                if(this.repeatPassword == value)
                    return;
                this.ValidateRepeatPassword(value);
                this.repeatPassword = value;
                this.NotifyOfPropertyChange(() => this.RepeatPassword);
            }
        }

        public DelegateCommand SubmitCommand {
            get;
            private set;
        }

        private void ValidateOldPassword(string value) {
            this.RemovePropertyError("OldPassword");
            if(string.IsNullOrEmpty(value))
                this.SetPropertyError("OldPassword", SecurityUIStrings.DataEditView_Validation_Personnel_OldPasswordIsNull);
        }

        private void ValidatePassword(string value) {
            this.RemovePropertyError("Password");
            if(string.IsNullOrEmpty(value))
                this.SetPropertyError("Password", SecurityUIStrings.DataEditView_Validation_Personnel_NewPasswordIsNull);
            if(value != null && (value.Length < 6 || value.Length > 20))
                this.SetPropertyError("Password", SecurityUIStrings.DataEditView_Validation_Personnel_NewPasswordLengthIsError);
            if(value == BaseApp.Current.CurrentUserData.EnterpriseCode)
                this.SetPropertyError("Password", SecurityUIStrings.DataEditView_Validation_Personnel_NewPasswordEqualsEnterpriseCode);
        }

        private void ValidateRepeatPassword(string value) {
            this.RemovePropertyError("RepeatPassword");
            if(value != this.Password)
                this.SetPropertyError("RepeatPassword", SecurityUIStrings.DataEditView_Validation_Personnel_ConfirmPasswordNotEqualsPassword);
        }

        private bool CanSubmit() {
            return !this.HasErrors;
        }

        private void Submit() {
            this.Validate();
            if(this.CanSubmit())
                this.DomainContext.ChangePassword(SecurityManager.HashPassword(this.OldPassword), SecurityManager.HashPassword(this.Password), invokeOp => {
                    if(!invokeOp.HasError) {
                        UIHelper.ShowNotification(SecurityUIStrings.CustomView_ChangePasssword_Notification_Success);
                        this.Reset();
                    } else
                        SecurityUtils.ShowDomainServiceOperationWindow(invokeOp);
                }, null);
        }

        private void Reset() {
            this.OldPassword = string.Empty;
            this.Password = string.Empty;
            this.RepeatPassword = string.Empty;
            this.ResetPropertyErrors();
        }

        public override void Validate() {
            this.ValidateOldPassword(this.OldPassword);
            this.ValidatePassword(this.Password);
            this.ValidateRepeatPassword(this.RepeatPassword);
        }

        public ChangePasswordViewModel() {
            this.SubmitCommand = new DelegateCommand(this.Submit, this.CanSubmit);
            this.ErrorsChanged += (sender, args) => this.SubmitCommand.RaiseCanExecuteChanged();

            this.DomainContext.Load(this.DomainContext.GetPersonnelsQuery().Where(personnel => personnel.Id == BaseApp.Current.CurrentUserData.UserId), loadOp => {
                if(loadOp.HasError)
                    loadOp.MarkErrorAsHandled();
                else {
                    var personnel = loadOp.Entities.FirstOrDefault();
                    if(personnel != null)
                        this.Photo = personnel.Photo;
                }
            }, null);
        }
    }
}
