﻿using Sunlight.Silverlight.Controls;
using System;
using System.Linq;
using System.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Security.ViewModel {
    /// <summary>
    /// 自定义的过滤条件行为定义。
    /// 过滤条件值应来自于 <see cref="SearchTextBox"/>，应用于 <see cref="GridViewDataControl"/>（<see cref="RadGridView"/> 和 <see cref="RadTreeListView"/> 的父类）。
    /// </summary>
    public class CustomFilterBehavior : ViewModelBase {
        private readonly GridViewDataControl gridView;
        private readonly SearchTextBox textBox;
        private CustomFilterDescriptor customFilterDescriptor;

        /// <summary>
        /// 自定义的过滤条件描述符
        /// </summary>
        public CustomFilterDescriptor CustomFilterDescriptor {
            get {
                if(this.customFilterDescriptor == null) {
                    this.customFilterDescriptor = new CustomFilterDescriptor(this.gridView.Columns.OfType<GridViewColumn>());
                    this.gridView.FilterDescriptors.Add(this.customFilterDescriptor);
                }
                return this.customFilterDescriptor;
            }
        }

        /// <summary>
        /// 绑定的<see cref="SearchTextBox"/>控件，用户可在此控件中输入过滤条件值。
        /// </summary>
        public static readonly DependencyProperty SearchTextBoxProperty =
            DependencyProperty.RegisterAttached("SearchTextBox", typeof(SearchTextBox), typeof(CustomFilterBehavior), new PropertyMetadata(SearchTextBox_PropertyChanged));

        private void FilterValue_TextChanged(object sender, EventArgs eventArgs) {
            this.CustomFilterDescriptor.FilterValue = this.textBox.SearchText;
            this.textBox.Focus();
        }

        public static void SetSearchTextBox(DependencyObject dependencyObject, SearchTextBox textBox) {
            dependencyObject.SetValue(SearchTextBoxProperty, textBox);
        }

        public static SearchTextBox GetSearchTextBox(DependencyObject dependencyObject) {
            return (SearchTextBox)dependencyObject.GetValue(SearchTextBoxProperty);
        }

        public static void SearchTextBox_PropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e) {
            var grid = dependencyObject as GridViewDataControl;
            var textBox = e.NewValue as SearchTextBox;

            if(grid != null && textBox != null) {
                var behavior = new CustomFilterBehavior(grid, textBox);
            }
        }

        public CustomFilterBehavior(GridViewDataControl gridView, SearchTextBox textBox) {
            this.gridView = gridView;
            this.textBox = textBox;

            this.textBox.SearchTextChanged -= this.FilterValue_TextChanged;
            this.textBox.SearchTextChanged += this.FilterValue_TextChanged;
        }
    }
}
