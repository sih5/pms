﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Command;

namespace Sunlight.Silverlight.Security.ViewModel {
    /// <summary>
    /// 表示分组项的 ViewModel
    /// </summary>
    public class GroupNodeItem : NodeItem {
        private ObservableCollection<PageNodeItem> pageNodeItems;

        private IEnumerable<PageNodeItem> VisiblePageNodeItems {
            get {
                return this.PageNodeItems.Where(page => page.Visibility == Visibility.Visible);
            }
        }

        /// <summary>
        /// 该分组的流程节点集合
        /// </summary>
        public ObservableCollection<PageNodeItem> PageNodeItems {
            get {
                return this.pageNodeItems;
            }
            set {
                this.pageNodeItems = value;
                this.OnPropertyChanged("PageNodeItems");
            }
        }

        /// <summary>
        /// 选中每个当前可见的 <see cref="PageNodeItem"/>
        /// </summary>
        public DelegateCommand CheckAll {
            get;
            private set;
        }

        /// <summary>
        /// 取消对每个当前可见的 <see cref="PageNodeItem"/> 的选中状态
        /// </summary>
        public DelegateCommand UncheckAll {
            get;
            private set;
        }

        /// <summary>
        /// 反转每个当前可见的被选中的 <see cref="PageNodeItem"/> 的 <see cref="PageNodeItem.ActionNodeItems"/> 中每个业务操作的选中状态        
        /// </summary>        
        public DelegateCommand ReverseActions {
            get;
            private set;
        }

        private void SetVisiblePagesCheckState(bool isChecked) {
            foreach(var page in this.VisiblePageNodeItems) {
                page.IsChecked = isChecked;
                page.SetActionsCheckState(isChecked);
            }
        }

        /// <summary>
        /// 参见 <see cref="CheckAll"/> 的注释内容
        /// </summary>
        internal void CheckVisiblePages() {
            this.SetVisiblePagesCheckState(true);
        }

        /// <summary>
        /// 参见 <see cref="UncheckAll"/> 的注释内容
        /// </summary>
        internal void UncheckVisiblePages() {
            this.SetVisiblePagesCheckState(false);
        }

        /// <summary>
        /// 参见 <see cref="ReverseActions"/> 的注释内容
        /// </summary>        
        internal void ReverseVisiblePagesActionState() {
            foreach(var action in this.VisiblePageNodeItems.Where(page => page.IsChecked).SelectMany(page => page.ActionNodeItems))
                action.IsChecked = !action.IsChecked;
        }

        private void WireCommands() {
            this.CheckAll = new DelegateCommand(this.CheckVisiblePages);
            this.UncheckAll = new DelegateCommand(this.UncheckVisiblePages);
            this.ReverseActions = new DelegateCommand(this.ReverseVisiblePagesActionState);
        }

        public GroupNodeItem(int id, string displayName)
            : base(id, displayName, null) {
            this.PageNodeItems = new ObservableCollection<PageNodeItem>();
            this.WireCommands();
        }
    }
}
