﻿using System;
using System.Windows;
using Sunlight.Silverlight.ViewModels;

namespace Sunlight.Silverlight.Security.ViewModel {
    /// <summary>
    /// 表示某菜单项的 ViewModel
    /// </summary>
    public class NodeItem : ViewModelBase {
        private Visibility visibility;

        /// <summary>
        /// 数据ID
        /// </summary>
        public int Id {
            get;
            private set;
        }

        /// <summary>
        /// 显示名称
        /// </summary>
        public string DisplayName {
            get;
            private set;
        }

        /// <summary>
        /// 图标
        /// </summary>
        public Uri ImageUri {
            get;
            private set;
        }

        /// <summary>
        /// 该菜单项可见
        /// </summary>
        public Visibility Visibility {
            get {
                return this.visibility;
            }
            set {
                this.visibility = value;
                this.OnPropertyChanged("Visibility");
            }
        }

        public NodeItem(int id, string displayName, Uri imageUri) {
            this.Id = id;
            this.DisplayName = displayName;
            this.ImageUri = imageUri;
        }
    }
}
