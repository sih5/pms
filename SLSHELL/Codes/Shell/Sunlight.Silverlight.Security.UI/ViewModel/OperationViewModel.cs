﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.ViewModel.Contract;

namespace Sunlight.Silverlight.Security.ViewModel {
    public class OperationViewModel : ViewModelBase, IOperationViewModel {
        public sealed class ConcreteExecuteCommand : CommandBase {
            private readonly OperationViewModel viewModel;

            internal void DomainContextStateChanged() {
                this.RaiseCanExecuteChanged();
            }

            internal ConcreteExecuteCommand(OperationViewModel viewModel) {
                if(viewModel == null)
                    throw new ArgumentNullException("viewModel");
                this.viewModel = viewModel;
                this.viewModel.PropertyChanged += this.viewModel_PropertyChanged;
            }

            private void viewModel_PropertyChanged(object sender, PropertyChangedEventArgs e) {
                if(e.PropertyName == "SelectedEntities")
                    this.RaiseCanExecuteChanged();
            }

            protected override bool CanExecute(object parameter) {
                if(this.viewModel.OperationAction == null)
                    return false;

                var count = this.viewModel.SelectedEntities == null ? 0 : this.viewModel.SelectedEntities.Cast<object>().Count();
                var canExecute = (this.viewModel.MinSelection < 0 || count >= this.viewModel.MinSelection) && (this.viewModel.MaxSelection < 0 || count <= this.viewModel.MaxSelection);
                if(!canExecute)
                    return false;

                return this.viewModel.CanExecute == null || this.viewModel.CanExecute(this.viewModel);
            }

            protected override void Execute(object parameter) {
                if(this.viewModel.OperationAction != null)
                    this.viewModel.OperationAction(this.viewModel);
            }
        }

        private IEnumerable selectedEntities;
        private ConcreteExecuteCommand executeCommand;

        internal int MinSelection {
            get;
            set;
        }

        internal int MaxSelection {
            get;
            set;
        }

        internal Action<OperationViewModel> OperationAction {
            get;
            set;
        }

        public string UniqueId {
            get;
            internal set;
        }

        bool IOperationViewModel.IsAuthorized {
            get;
            set;
        }

        public string Name {
            get;
            internal set;
        }

        public Uri ImageUri {
            get;
            internal set;
        }

        public IEnumerable SelectedEntities {
            get {
                return this.selectedEntities;
            }
            set {
                this.selectedEntities = value;
                this.NotifyOfPropertyChange(() => this.SelectedEntities);
            }
        }

        public ICommand ExecuteCommand {
            get {
                return this.executeCommand ?? (this.executeCommand = new ConcreteExecuteCommand(this));
            }
        }
        public Func<IOperationViewModel, bool> CanExecute {
            get;
            internal set;
        }

        public override void Validate() {
            throw new NotSupportedException();
        }
    }
}
