﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using Sunlight.Silverlight.Core.Command;

namespace Sunlight.Silverlight.Security.ViewModel {
    /// <summary>
    /// 表示流程节点的 ViewModel
    /// </summary>
    public class PageNodeItem : NodeItem {
        private bool isChecked;
        private ObservableCollection<ActionNodeItem> actionNodeItems;

        /// <summary>
        /// 该流程节点被选中
        /// </summary>
        public bool IsChecked {
            get {
                return this.isChecked;
            }
            set {
                this.isChecked = value;
                this.OnPropertyChanged("IsChecked");
            }
        }

        /// <summary>
        /// 该流程节点的业务操作集合
        /// </summary>
        public ObservableCollection<ActionNodeItem> ActionNodeItems {
            get {
                return this.actionNodeItems;
            }
            set {
                this.actionNodeItems = value;
                this.OnPropertyChanged("ActionNodeItems");
            }
        }

        /// <summary>
        /// 设置业务操作集合中所有 <see cref="ActionNodeItem"/> 的 <see cref="ActionNodeItem.IsChecked"/> 属性
        /// </summary>
        public DelegateCommand<bool?> SetActionsState {
            get;
            private set;
        }

        private void ActionNodeItem_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            // 若某个业务操作被选中，则本流程节点也应被选中。
            // 但业务操作从选中状态变为未选中状态，本流程节点的选中状态不变化。
            if(e.PropertyName == "IsChecked")
                if(!this.IsChecked) {
                    this.PropertyChanged -= this.PageNodeItem_PropertyChanged;
                    this.IsChecked = this.ActionNodeItems.Any(operation => operation.IsChecked);
                    this.PropertyChanged += this.PageNodeItem_PropertyChanged;
                }
        }

        private void ActionNodeItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            foreach(ActionNodeItem item in e.NewItems) {
                item.PropertyChanged -= this.ActionNodeItem_PropertyChanged;
                item.PropertyChanged += this.ActionNodeItem_PropertyChanged;
            }
        }

        private void PageNodeItem_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            switch(e.PropertyName) {
                case "ActionNodeItems":
                    foreach(var actionNodeItem in this.ActionNodeItems) {
                        actionNodeItem.PropertyChanged -= this.ActionNodeItem_PropertyChanged;
                        actionNodeItem.PropertyChanged += this.ActionNodeItem_PropertyChanged;
                    }
                    this.ActionNodeItems.CollectionChanged -= this.ActionNodeItems_CollectionChanged;
                    this.ActionNodeItems.CollectionChanged += this.ActionNodeItems_CollectionChanged;
                    break;
            }
        }

        internal void SetActionsCheckState(bool? checkState) {
            if(!checkState.HasValue)
                return;

            foreach(var actionNodeItem in this.ActionNodeItems) {
                actionNodeItem.PropertyChanged -= this.ActionNodeItem_PropertyChanged;
                actionNodeItem.IsChecked = checkState.Value;
                actionNodeItem.PropertyChanged += this.ActionNodeItem_PropertyChanged;
            }
        }

        public PageNodeItem(int id, string displayName, Uri imageUri)
            : base(id, displayName, imageUri) {
            this.PropertyChanged += this.PageNodeItem_PropertyChanged;
            this.ActionNodeItems = new ObservableCollection<ActionNodeItem>();
            this.ActionNodeItems.CollectionChanged += this.ActionNodeItems_CollectionChanged;
            this.SetActionsState = new DelegateCommand<bool?>(this.SetActionsCheckState);
        }
    }
}
