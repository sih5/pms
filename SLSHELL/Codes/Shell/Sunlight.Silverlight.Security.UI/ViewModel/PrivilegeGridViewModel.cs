﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.ViewModels;

namespace Sunlight.Silverlight.Security.ViewModel {
    /// <summary>
    /// 表达权限矩阵的 ViewModel，构造已授权的节点集合。
    /// </summary>
    public class PrivilegeGridViewModel : ViewModelBase {
        private ObservableCollection<DisplayItem> displayItems;
        private int maxActionCount;

        /// <summary>
        /// 已授权的节点集合
        /// </summary>
        public ObservableCollection<DisplayItem> DisplayItems {
            get {
                return this.displayItems;
            }
            private set {
                if(this.displayItems != value) {
                    this.displayItems = value;
                    this.OnPropertyChanged("DisplayItems");
                }
            }
        }

        /// <summary>
        /// 已授权的节点集合 <see cref="DisplayItems"/> 中业务操作按钮的最大数量。
        /// </summary>
        public int MaxActionCount {
            get {
                return this.maxActionCount;
            }
            private set {
                if(this.maxActionCount != value) {
                    this.maxActionCount = value;
                    this.OnPropertyChanged("MaxActionCount");
                }
            }
        }

        /// <summary>
        /// 初始化 ViewModel，根据 <paramref name="systemNodeItems"/> 构造 <see cref="DisplayItems"/> 属性。
        /// </summary>
        /// <param name="systemNodeItems">所有子系统的流程节点集合</param>
        public PrivilegeGridViewModel(IList<SystemNodeItem> systemNodeItems) {
            var items = new ObservableCollection<DisplayItem>();
            this.MaxActionCount = (from systemItem in systemNodeItems
                                   from groupItem in systemItem.GroupNodeItems
                                   from pageItem in groupItem.PageNodeItems
                                   where pageItem.IsChecked
                                   select pageItem.ActionNodeItems.Count(action => action.IsChecked)).DefaultIfEmpty().Max();
            foreach(var system in systemNodeItems.Where(item => item.GroupNodeItems.Any(group => group.PageNodeItems.Any(page => page.IsChecked)))) {
                var systemDisplayItem = new DisplayItem(system.DisplayName, SecurityPageType.系统, this.MaxActionCount);
                foreach(var group in system.GroupNodeItems.Where(item => item.PageNodeItems.Any(page => page.IsChecked))) {
                    var groupDisplayItem = new DisplayItem(group.DisplayName, SecurityPageType.分组, this.MaxActionCount);
                    foreach(var page in group.PageNodeItems.Where(item => item.IsChecked))
                        groupDisplayItem.Items.Add(new DisplayItem(page.DisplayName, SecurityPageType.页面, this.MaxActionCount,
                                                                   page.ActionNodeItems.Where(item => item.IsChecked).Select(item => item.DisplayName).ToList()));
                    systemDisplayItem.Items.Add(groupDisplayItem);
                }
                items.Add(systemDisplayItem);
            }
            this.DisplayItems = items;
        }
    }

    /// <summary>
    /// 用于<see cref="PrivilegeGridViewModel"/>构造授权节点的集合。
    /// </summary>
    public class DisplayItem {
        /// <summary>
        /// 节点名称
        /// </summary>
        public string Name {
            get;
            private set;
        }

        /// <summary>
        /// 节点类型
        /// </summary>
        public SecurityPageType Type {
            get;
            private set;
        }

        /// <summary>
        /// 该节点的下属节点集合
        /// </summary>
        public List<DisplayItem> Items {
            get;
            set;
        }

        /// <summary>
        /// 该节点的业务操作集合
        /// </summary>
        public List<string> Actions {
            get;
            set;
        }

        /// <summary>
        /// 初始化授权节点的表示项，将 <see cref="Actions"/> 采用 <paramref name="actions" /> 填充，若其数量不足 <paramref name="actionCapacity"/>，则使用空字符串补充。
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="actionCapacity"></param>
        /// <param name="actions"></param>
        public DisplayItem(string name, SecurityPageType type, int actionCapacity, IList<string> actions = null) {
            this.Name = name;
            this.Type = type;
            this.Items = new List<DisplayItem>();
            this.Actions = new List<string>(actionCapacity);
            if(actions != null && actions.Any())
                this.Actions.AddRange(actions);
            this.Actions.AddRange(Enumerable.Repeat(string.Empty, actionCapacity - (actions != null ? actions.Count : 0)));
        }
    }
}
