﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.ViewModel.Contract;

namespace Sunlight.Silverlight.Security.ViewModel {
    public class QueryViewModel<T> : ViewModelBase, IQueryViewModel where T : Entity {
        public sealed class ConcreteQueryCommand : CommandBase {
            private readonly QueryViewModel<T> viewModel;

            internal void DomainContextStateChanged() {
                this.RaiseCanExecuteChanged();
            }

            internal ConcreteQueryCommand(QueryViewModel<T> viewModel) {
                if(viewModel == null)
                    throw new ArgumentNullException("viewModel");
                this.viewModel = viewModel;
            }

            protected override bool CanExecute(object parameter) {
                return !this.viewModel.Parent.DomainContext.IsLoading && !this.viewModel.Parent.DomainContext.IsSubmitting;
            }

            protected override void Execute(object parameter) {
                var parameters = parameter as IDictionary<string, object>;

                if(this.viewModel.BuildQuery == null)
                    return;
                var query = this.viewModel.BuildQuery(parameters);
                this.viewModel.Parent.DomainContext.Load(query, LoadBehavior.MergeIntoCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(loadOp.Error != null)
                            UIHelper.ShowNotification(string.Format(SecurityUIStrings.QueryViewModel_QueryFailed, loadOp.Error.Message));
                        loadOp.MarkErrorAsHandled();
                    } else {
                        this.viewModel.Parent.QueryResult = loadOp.Entities;
                        if(!this.viewModel.Parent.IsAutoLoad)
                            UIHelper.ShowNotification(loadOp.TotalEntityCount > 0 ? string.Format(SecurityUIStrings.QueryViewModel_QuerySuccess, loadOp.TotalEntityCount) : SecurityUIStrings.QueryViewModel_QuerySuccessEmpty);
                    }
                }, null);
            }
        }

        private IQueryableViewModel parent;
        private ConcreteQueryCommand queryCommand;

        internal Func<IDictionary<string, object>, EntityQuery<T>> BuildQuery {
            get;
            set;
        }

        public string Name {
            get;
            internal set;
        }

        public IQueryableViewModel Parent {
            get {
                return this.parent;
            }
            internal set {
                if(this.parent != null)
                    this.parent.DomainContext.PropertyChanged -= this.DomainContext_PropertyChanged;
                this.parent = value;
                this.parent.DomainContext.PropertyChanged += this.DomainContext_PropertyChanged;
            }
        }

        private void DomainContext_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(this.queryCommand != null && (e.PropertyName == "IsLoading" || e.PropertyName == "IsSubmitting"))
                this.queryCommand.DomainContextStateChanged();
        }

        public IEnumerable<QueryItem> QueryItems {
            get;
            internal set;
        }

        public ICommand QueryCommand {
            get {
                return this.queryCommand ?? (this.queryCommand = new ConcreteQueryCommand(this));
            }
        }

        public override void Validate() {
            throw new NotSupportedException();
        }
    }
}
