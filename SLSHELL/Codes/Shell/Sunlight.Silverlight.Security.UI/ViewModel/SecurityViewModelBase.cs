﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.ViewModel {
    public class SecurityViewModelBase : ViewModelBase {
        private static readonly Dictionary<string, KeyValuePair[]> KeyValueItems = new Dictionary<string, KeyValuePair[]>();
        private static readonly object KeyValueItemsLock = new object();
        private readonly SecurityDomainContext domainContext = new SecurityDomainContext();

        protected SecurityDomainContext SecurityDomainContext {
            get {
                return this.domainContext;
            }
        }

        public DomainContext DomainContext {
            get {
                return this.domainContext;
            }
        }

        protected internal static void GetKeyValueItems(SecurityDomainContext domainContext, string name, ObservableCollection<KeyValuePair> collection) {
            if(domainContext == null)
                throw new ArgumentNullException("domainContext");
            if(string.IsNullOrEmpty(name))
                throw new ArgumentNullException("name");
            if(collection == null)
                throw new ArgumentNullException("collection");

            KeyValuePair[] keyValueItems;
            bool success;
            lock(KeyValueItemsLock)
                success = KeyValueItems.TryGetValue(name, out keyValueItems);
            if(success) {
                collection.Clear();
                foreach(var keyValueItem in keyValueItems)
                    collection.Add(keyValueItem);
                return;
            }

            domainContext.Load(domainContext.GetKeyValueMappingByNameQuery(name), loadOp => {
                if(loadOp.HasError)
                    return;
                keyValueItems = loadOp.Entities.Select(e => new KeyValuePair {
                    Key = e.Key,
                    Value = e.Value,
                }).OrderBy(kvp => kvp.Key).ToArray();
                lock(KeyValueItemsLock) {
                    if(!KeyValueItems.ContainsKey(name))
                        KeyValueItems.Add(name, keyValueItems);
                }
                collection.Clear();
                foreach(var keyValueItem in keyValueItems)
                    collection.Add(keyValueItem);
            }, null);
        }

        protected internal static void GetKeyValueItems(SecurityDomainContext domainContext, IDictionary<string, ObservableCollection<KeyValuePair>> collections) {
            GetKeyValueItems(domainContext, collections, null);
        }

        protected internal static void GetKeyValueItems(SecurityDomainContext domainContext, IDictionary<string, ObservableCollection<KeyValuePair>> collections, Action callback) {
            if(domainContext == null)
                throw new ArgumentNullException("domainContext");
            if(collections == null)
                throw new ArgumentNullException("collections");

            var namesToQuery = new List<string>();
            foreach(var kv in collections.Where(kv => kv.Value != null)) {
                bool success;
                KeyValuePair[] keyValueItems;
                lock(KeyValueItemsLock)
                    success = KeyValueItems.TryGetValue(kv.Key, out keyValueItems);
                if(success) {
                    kv.Value.Clear();
                    foreach(var keyValueItem in keyValueItems)
                        kv.Value.Add(keyValueItem);
                } else
                    namesToQuery.Add(kv.Key);
            }
            if(!namesToQuery.Any()) {
                if(callback != null)
                    callback();
                return;
            }

            domainContext.Load(domainContext.GetKeyValueMappingByNamesQuery(namesToQuery.ToArray()), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var name in namesToQuery) {
                    var keyValueItems = loadOp.Entities.Where(e => e.Name == name).Select(e => new KeyValuePair {
                        Key = e.Key,
                        Value = e.Value,
                    }).OrderBy(kvp => kvp.Key).ToArray();
                    lock(KeyValueItemsLock) {
                        if(!KeyValueItems.ContainsKey(name))
                            KeyValueItems.Add(name, keyValueItems);
                    }
                    var collection = collections[name];
                    collection.Clear();
                    foreach(var keyValueItem in keyValueItems)
                        collection.Add(keyValueItem);
                }
                if(callback != null)
                    callback();
            }, null);
        }

        public override void Validate() {
            throw new NotSupportedException();
        }

        protected static void SpliteDateRange(object range, out DateTime? begin, out DateTime? end) {
            var dateRange = range as DateTime?[];
            if(dateRange == null)
                throw new ArgumentNullException("range");
            if(dateRange.Length != 2)
                throw new ArgumentOutOfRangeException("range");
            begin = end = null;
            if(dateRange[0] != null)
                begin = Convert.ToDateTime(dateRange[0]).Date;
            if(dateRange[1] != null)
                end = Convert.ToDateTime(dateRange[1]).Date.AddDays(1).AddSeconds(-1);
        }
    }
}
