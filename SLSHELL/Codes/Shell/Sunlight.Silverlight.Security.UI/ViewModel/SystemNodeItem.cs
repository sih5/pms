﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Command;

namespace Sunlight.Silverlight.Security.ViewModel {
    /// <summary>
    /// 表达系统项的 ViewModel
    /// </summary>
    public class SystemNodeItem : NodeItem {
        private ObservableCollection<GroupNodeItem> groupNodeItems;

        private IEnumerable<GroupNodeItem> VisibleGroupNodeItems {
            get {
                return this.GroupNodeItems.Where(group => group.Visibility == Visibility.Visible);
            }
        }

        /// <summary>
        /// 该系统的分组集合
        /// </summary>
        public ObservableCollection<GroupNodeItem> GroupNodeItems {
            get {
                return this.groupNodeItems;
            }
            set {
                this.groupNodeItems = value;
                this.OnPropertyChanged("GroupNodeItems");
            }
        }

        /// <summary>
        /// 选中所有分组中每个当前可见的 <see cref="PageNodeItem"/>
        /// </summary>
        public DelegateCommand CheckAll {
            get;
            private set;
        }

        /// <summary>
        /// 取消对所有分组中每个当前可见的 <see cref="PageNodeItem"/> 的选中状态
        /// </summary>
        public DelegateCommand UncheckAll {
            get;
            private set;
        }

        /// <summary>
        /// 反转所有分组中每个当前可见的被选中的 <see cref="PageNodeItem"/> 的 <see cref="PageNodeItem.ActionNodeItems"/> 中每个业务操作的选中状态        
        /// </summary>  
        public DelegateCommand ReverseActions {
            get;
            private set;
        }

        /// <summary>
        /// 参见 <see cref="CheckAll"/> 的注释内容
        /// </summary>
        internal void CheckVisiblePages() {
            foreach(var group in this.VisibleGroupNodeItems)
                group.CheckVisiblePages();
        }

        /// <summary>
        /// 参见 <see cref="UncheckAll"/> 的注释内容
        /// </summary>
        internal void UncheckVisiblePages() {
            foreach(var group in this.VisibleGroupNodeItems)
                group.UncheckVisiblePages();
        }

        /// <summary>
        /// 参见 <see cref="ReverseActions"/> 的注释内容
        /// </summary>   
        internal void ReverseVisiblePagesActionState() {
            foreach(var group in this.VisibleGroupNodeItems)
                group.ReverseVisiblePagesActionState();
        }

        /// <summary>
        /// 将所有分组中流程节点和业务操作的名称中包含有 <paramref name="content"/> 的递归节点设置为可见
        /// </summary>
        /// <param name="content">被搜索的文本内容</param>
        internal void Search(string content) {
            var searchText = content.ToLower();
            foreach(var pageMenuItem in this.GroupNodeItems.SelectMany(groupMenuItem => groupMenuItem.PageNodeItems))
                pageMenuItem.Visibility = pageMenuItem.DisplayName.ToLower().Contains(searchText) ||
                                          pageMenuItem.ActionNodeItems.Any(menuItem => menuItem.DisplayName.ToLower().Contains(searchText))
                                              ? Visibility.Visible
                                              : Visibility.Collapsed;

            foreach(var groupMenuItem in this.GroupNodeItems)
                groupMenuItem.Visibility = groupMenuItem.PageNodeItems.All(pageMenuItem => pageMenuItem.Visibility == Visibility.Collapsed)
                                               ? Visibility.Collapsed
                                               : Visibility.Visible;
        }

        private void WireCommands() {
            this.CheckAll = new DelegateCommand(this.CheckVisiblePages);
            this.UncheckAll = new DelegateCommand(this.UncheckVisiblePages);
            this.ReverseActions = new DelegateCommand(this.ReverseVisiblePagesActionState);
        }

        public SystemNodeItem(int id, string displayName, Uri imageUri)
            : base(id, displayName, imageUri) {
            this.GroupNodeItems = new ObservableCollection<GroupNodeItem>();
            this.WireCommands();
        }
    }
}
