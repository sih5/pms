﻿
using System.Linq;
using Sunlight.Silverlight.Security.View.Workflow.Enterprise;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.ViewModel.Workflow {
    public class EnterpriseBatchWorkflow : SecurityWorkflowViewModelBase {
        private const string KEY_PAGE1 = "EnterpriseEntNodeTemplateBatch";
        private const string KEY_PAGE2 = "EnterpriseEntOrganizationTplBatch";
        private const string KEY_PAGE3 = "EnterpriseRoleTemplateBatch";
        private EnterpriseEntNodeTemplateBatchEdit flow1;
        private EnterpriseEntOrganizationTplBatchEdit flow2;
        private EnterpriseRoleTemplateBatchEdit flow3;
        private EnterpriseInfo enterpriseInfo;
        private bool isExistOrganizations;

        public EnterpriseEntNodeTemplateBatchEdit Flow1 {
            get {
                return this.flow1 ?? (this.flow1 = new EnterpriseEntNodeTemplateBatchEdit {
                    EnterpriseInfo = this.EnterpriseInfo
                });
            }
        }

        public EnterpriseEntOrganizationTplBatchEdit Flow2 {
            get {
                return this.flow2 ?? (this.flow2 = new EnterpriseEntOrganizationTplBatchEdit {
                    EnterpriseInfo = this.EnterpriseInfo
                });
            }
        }

        public EnterpriseRoleTemplateBatchEdit Flow3 {
            get {
                return this.flow3 ?? (this.flow3 = new EnterpriseRoleTemplateBatchEdit {
                    EnterpriseInfo = this.EnterpriseInfo
                });
            }
        }

        public EnterpriseInfo EnterpriseInfo {
            get {
                return this.enterpriseInfo;
            }
            set {
                this.enterpriseInfo = value;
                this.NotifyOfPropertiesChange("EnterpriseInfo");
            }
        }

        protected override void OnRequestLeaveCurrentPage(System.Action<bool> canLeaveCallback) {
            switch(this.CurrentPageKey) {
                case KEY_PAGE1:
                    if(!this.Flow1.ActionHandle())
                        return;
                    canLeaveCallback(true);
                    break;
                case KEY_PAGE2:
                    if(!this.Flow2.ActionHandle())
                        return;
                    canLeaveCallback(true);
                    break;
                case KEY_PAGE3:
                    this.Flow3.ActionHandle(() => canLeaveCallback(true));
                    break;
            }
        }

        protected override string OnRequestNextPageKey(string currentKey, Silverlight.View.WorkflowPageBase currentPage) {
            switch(currentKey) {
                case KEY_PAGE1:
                    return this.isExistOrganizations ? KEY_PAGE3 : KEY_PAGE2;
                case KEY_PAGE2:
                    return KEY_PAGE3;
                default:
                    return KEY_PAGE1;
            }
        }

        protected override void OnPageSwitched(string key, Silverlight.View.WorkflowPageBase value) {
            this.IsFinalPage = key == KEY_PAGE3;
            this.RegisterPage(KEY_PAGE1, () => this.Flow1);
            this.RegisterPage(KEY_PAGE2, () => this.Flow2);
            this.RegisterPage(KEY_PAGE3, () => this.Flow3);
        }

        public EnterpriseBatchWorkflow() {
            this.RegisterPage(KEY_PAGE1, () => this.Flow1);
            var DomainContext = new SecurityDomainContext();
            DomainContext.Load(DomainContext.GetOrganizationsQuery(), loadOp => {
                if(loadOp.HasError)
                    return;
                if(this.EnterpriseInfo.Enterprises.Any(enterprise => loadOp.Entities.Count(o => o.EnterpriseId == enterprise.Id) > 0)) {
                    isExistOrganizations = true;
                }
            }, null);
        }
    }
}
