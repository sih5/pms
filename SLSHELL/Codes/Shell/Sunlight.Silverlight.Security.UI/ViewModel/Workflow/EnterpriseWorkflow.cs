﻿
using Sunlight.Silverlight.Security.View.Workflow.Enterprise;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security.ViewModel.Workflow {
    public class EnterpriseWorkflow : SecurityWorkflowViewModelBase {
        private const string KEY_PAGE1 = "EnterpriseBaseInfo";
        private const string KEY_PAGE2 = "EnterpriseEntNodeTemplate";
        private const string KEY_PAGE3 = "EnterpriseEntOrganizationTpl";
        private const string KEY_PAGE4 = "EnterpriseRoleTemplate";
        private EnterpriseBaseInfoEdit flow1;
        private EnterpriseEntNodeTemplateEdit flow2;
        private EnterpriseEntOrganizationTplEdit flow3;
        private EnterpriseRoleTemplateEdit flow4;
        private EnterpriseInfo enterpriseInfo;

        public EnterpriseBaseInfoEdit Flow1 {
            get {
                return this.flow1 ?? (this.flow1 = new EnterpriseBaseInfoEdit {
                    EnterpriseInfo = this.EnterpriseInfo
                });
            }
        }

        public EnterpriseEntNodeTemplateEdit Flow2 {
            get {
                return this.flow2 ?? (this.flow2 = new EnterpriseEntNodeTemplateEdit {
                    EnterpriseInfo = this.EnterpriseInfo
                });
            }
        }

        public EnterpriseEntOrganizationTplEdit Flow3 {
            get {
                return this.flow3 ?? (this.flow3 = new EnterpriseEntOrganizationTplEdit {
                    EnterpriseInfo = this.EnterpriseInfo
                });
            }
        }

        public EnterpriseRoleTemplateEdit Flow4 {
            get {
                return this.flow4 ?? (this.flow4 = new EnterpriseRoleTemplateEdit {
                    EnterpriseInfo = this.EnterpriseInfo
                });
            }
        }

        public EnterpriseInfo EnterpriseInfo {
            get {
                return this.enterpriseInfo;
            }
            set {
                this.enterpriseInfo = value;
                this.NotifyOfPropertiesChange("EnterpriseInfo");
            }
        }

        protected override void OnRequestLeaveCurrentPage(System.Action<bool> canLeaveCallback) {
            switch(this.CurrentPageKey) {
                case KEY_PAGE1:
                    if(!this.Flow1.ActionHandle())
                        return;
                    canLeaveCallback(true);
                    break;
                case KEY_PAGE2:
                    if(!this.Flow2.ActionHandle())
                        return;
                    canLeaveCallback(true);
                    break;
                case KEY_PAGE3:
                    if(!this.Flow3.ActionHandle())
                        return;
                    canLeaveCallback(true);
                    break;
                case KEY_PAGE4:
                    this.Flow4.ActionHandle(() => canLeaveCallback(true));
                    break;
            }
        }

        protected override string OnRequestNextPageKey(string currentKey, Silverlight.View.WorkflowPageBase currentPage) {
            switch(currentKey) {
                case KEY_PAGE1:
                    return KEY_PAGE2;
                case KEY_PAGE2:
                    return KEY_PAGE3;
                case KEY_PAGE3:
                    return KEY_PAGE4;
                default:
                    return KEY_PAGE1;
            }
        }

        protected override void OnPageSwitched(string key, Silverlight.View.WorkflowPageBase value) {
            this.IsFinalPage = key == KEY_PAGE4;
            this.RegisterPage(KEY_PAGE1, () => this.Flow1);
            this.RegisterPage(KEY_PAGE2, () => this.Flow2);
            this.RegisterPage(KEY_PAGE3, () => this.Flow3);
            this.RegisterPage(KEY_PAGE4, () => this.Flow4);
        }

        public EnterpriseWorkflow() {
            this.RegisterPage(KEY_PAGE1, () => this.Flow1);
        }
    }
}
