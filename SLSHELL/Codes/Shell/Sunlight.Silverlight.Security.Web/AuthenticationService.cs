﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.ServiceModel.DomainServices.Server;
using System.ServiceModel.DomainServices.Server.ApplicationServices;
using System.Text;
using System.Transactions;
using System.Web.Security;
using System.Xml.Linq;
using Sunlight.Silverlight.Security.Web.Resources;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService : IAuthentication<AuthenticationUser> {
        public static SystemTypes SystemType = SystemTypes.Dms;
        public static string HashPassword1(string password) {
            var data = Encoding.UTF8.GetBytes(password);
            using(var sha1 = new SHA1Managed())
                data = sha1.ComputeHash(data);
            var sbResult = new StringBuilder(data.Length * 2);
            foreach(var b in data)
                sbResult.AppendFormat("{0:X2}", b);
            return sbResult.ToString();
        }
        private struct UserInfo {
            // ReSharper disable NotAccessedField.Local
            public int Id;
            public string LoginId;
            public string Name;
            public int EnterpriseId;
            public string EnterpriseCode;
            public string EnterpriseName;
            // ReSharper restore NotAccessedField.Local
        }

        private static readonly AuthenticationUser DefaultUser = new AuthenticationUser {
            Name = string.Empty,
        };

        private static string CreateUserInfo(Personnel user) {
            return user == null ? string.Empty : string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", user.Id, user.LoginId, user.Name, user.Enterprise == null ? 0 : user.Enterprise.Id, user.Enterprise == null ? "" : user.Enterprise.Code, user.Enterprise == null ? "" : user.Enterprise.Name, user.Enterprise == null ? 0 : user.Enterprise.EnterpriseCategoryId, user.Enterprise == null ? null : user.Enterprise.IsIDM);
        }

        private UserInfo GetCurrentUserInfo() {
            var result = new UserInfo {
                Id = 0,
                LoginId = null,
                Name = null,
                EnterpriseId = 0,
                EnterpriseCode = null,
                EnterpriseName = null,
            };
            if(this.ServiceContext == null || this.ServiceContext.User == null || !this.ServiceContext.User.Identity.IsAuthenticated || string.IsNullOrWhiteSpace(this.ServiceContext.User.Identity.Name))
                return result;
            var array = this.ServiceContext.User.Identity.Name.Split(',');
            if(array.Length > 0)
                int.TryParse(array[0], out result.Id);
            if(array.Length > 1)
                result.LoginId = array[1];
            if(array.Length > 2)
                result.Name = array[2];
            if(array.Length > 3)
                int.TryParse(array[3], out result.EnterpriseId);
            if(array.Length > 4)
                result.EnterpriseCode = array[4];
            if(array.Length > 5)
                result.EnterpriseName = array[5];
            return result;
        }
        string enterpriseCode = string.Empty;
        string IP = string.Empty;
        string UserMac = string.Empty;
        string MacAddPassword = string.Empty;

        private void GetParameters(string customData) {
            Dictionary<string, string> parameters = null;
            if(!string.IsNullOrWhiteSpace(customData))
                try {
                    var eRoot = XElement.Parse(customData);
                    if(eRoot.Name == "LoginParameters")
                        parameters = eRoot.Elements().ToDictionary(eParameter => eParameter.Name.LocalName, eParameter => eParameter.Value);
                } catch {
                    parameters = null;
                }
            if(parameters != null && parameters.ContainsKey("EnterpriseCode"))
                enterpriseCode = parameters["EnterpriseCode"];
            if(parameters != null && parameters.ContainsKey("IP"))
                IP = parameters["IP"];
            if(parameters != null && parameters.ContainsKey("Mac"))
                UserMac = parameters["Mac"];
            if(parameters != null && parameters.ContainsKey("MacAddPassword"))
                MacAddPassword = parameters["MacAddPassword"];
        }
        private Personnel ValidateUser(string loginId, string password, UserLoginInfo userLoginInfo) {
            // return this.ObjectContext.Personnels.Include("Enterprise").FirstOrDefault(u => (u.LoginId == loginId || u.CellNumber == loginId) && u.Password == password && u.Status == (int)SecurityUserStatus.有效 && (u.Enterprise.Code == enterpriseCode && u.Enterprise.Status == (int)SecurityEnterpriseStatus.生效 && (SystemType & SystemTypes.Dcs) == SystemTypes.Dcs || SystemType == SystemTypes.Dms));
            return this.ObjectContext.Personnels.Include("Enterprise").FirstOrDefault(u => (u.LoginId == loginId || u.CellNumber == loginId) && u.Status == (int)SecurityUserStatus.有效 && (u.Enterprise.Code == enterpriseCode && u.Enterprise.Status == (int)SecurityEnterpriseStatus.生效 && (SystemType & SystemTypes.Dcs) == SystemTypes.Dcs || SystemType == SystemTypes.Dms));

        }

        private void SetUserLoginInfo(UserLoginInfo userLoginInfo) {
            if(userLoginInfo == null)
                return;
            using(var transaction = new TransactionScope(TransactionScopeOption.RequiresNew)) {
                var nowTime = DateTime.Now;
                if(userLoginInfo.ErrorPassNum == 4) {
                    userLoginInfo.ErrorPassNum = 5;
                    userLoginInfo.BeginLockTime = nowTime;
                } else if(userLoginInfo.ErrorPassNum < 4) {
                    userLoginInfo.ErrorPassNum += 1;
                    userLoginInfo.BeginLockTime = null;
                } else {
                    if(userLoginInfo.BeginLockTime == null) {
                        throw new DomainException(string.Format("用户名为{0}的开始锁定时间为空，请联系管理员！", userLoginInfo.LoginId));
                    }
                    var endTime = userLoginInfo.BeginLockTime.Value.AddMinutes(userLoginInfo.LockLenght);
                    if(endTime > nowTime) {
                        //throw new ValidationDomainException(string.Format("已连续登录失败5次，请在{0}后再试！", endTime));
                    } else {
                        userLoginInfo.ErrorPassNum = 1;
                        userLoginInfo.BeginLockTime = null;
                    }
                }
                this.ObjectContext.SaveChanges();
                transaction.Complete();
            }
        }

        private void 调整用户登录信息(UserLoginInfo userLoginInfo, Personnel user) {
            #region 调整用户登录信息
            userLoginInfo.ErrorPassNum = 0;
            userLoginInfo.BeginLockTime = null;

            UpdateToDatabase(userLoginInfo);
            var loginLog = new LoginLog();
            loginLog.PersonnelId = user.Id;
            loginLog.IP = IP;
            InsertLoginLog(loginLog);
            this.ObjectContext.SaveChanges();
            #endregion
        }

        private UserLoginInfo SetLoginInfoMesage(IQueryable<UserLoginInfo> result, UserLoginInfo userLoginInfo, string password, string loginId) {
            if(result == null || !result.Any()) {
                SetUserLoginInfo(userLoginInfo);
                //校验企业编号和用户名有效性、两者是否匹配（两者都存在，但是当前人员不在该企业下）
                throw new DomainException(this.GetUserLoginErrorMessage(enterpriseCode, loginId));
            }
            if(result.Any() && result.Count() > 1) {
                SetUserLoginInfo(userLoginInfo);
                throw new DomainException(EntityStrings.SecurityManager_Error_RepeatUserLoginInfo);
            }
            if(result.Any()) {
                if(userLoginInfo != null && userLoginInfo.ErrorPassNum >= 5 && ((DateTime)userLoginInfo.BeginLockTime).AddMinutes(userLoginInfo.LockLenght) > userLoginInfo.SysTime) {
                    SetUserLoginInfo(userLoginInfo);
                    throw new DomainException(string.Format(EntityStrings.SecurityManager_Error_LoginInfoFailure, userLoginInfo.LockLenght));
                }
            }
            return userLoginInfo;
        }

        public AuthenticationUser Login(string userName, string password, bool isPersistent, string customData) {
            //if(null != password) {
            //    throw new DomainException(string.Format("密码错误：{0}哈哈", password));
            //}
            UserLoginInfo userLoginInfo = null;
            GetParameters(customData);
            var loginPassword = password.Replace(MacAddPassword, "");
            //调接口验证用户密码
            this.validateFoeUser(userName, loginPassword);
            IQueryable<UserLoginInfo> result = this.GetUserLoginInfosByEnterpriseCodeLoginId(enterpriseCode, userName);
            if(result.Any()) {
                result.First().SysTime = DateTime.Now;
                userLoginInfo = result.FirstOrDefault();
            }

            SetLoginInfoMesage(result, userLoginInfo, loginPassword, userName);
            //password = HashPassword1(password);
            var user = this.ValidateUser(userName, loginPassword, userLoginInfo);
            if(user == null) {
                SetUserLoginInfo(userLoginInfo);

                return null;
            }
            if(string.IsNullOrEmpty(user.MAC)) {
                user.MAC = UserMac;
                user.Remark = string.Format(@"[自动绑定IP:{0},MAC地址:{1}]", IP, UserMac);
                UpdateToDatabase(user);
                this.ObjectContext.SaveChanges();
            } else if(!password.Contains(MacAddPassword) && string.IsNullOrEmpty(UserMac)) {
                throw new DomainException("未获取到本机MAC地址.");
            } else if(!password.Contains(MacAddPassword) && (UserMac.IndexOf(user.MAC) < 0)) {
                throw new DomainException("当登录的电脑未授权.");
            }
            var userInfo = CreateUserInfo(user);
            var authenticationUser = new AuthenticationUser {
                Name = userInfo,
                User = user,
                IsFisrt = userLoginInfo != null && userLoginInfo.IsFisrt
            };

            FormsAuthentication.SetAuthCookie(userInfo, isPersistent);
            authenticationUser.Name = userInfo;
            调整用户登录信息(userLoginInfo, user);
            return authenticationUser;
        }
        public void validateFoeUser(string username, string password) {
            password = password.Replace("%", "%25").Replace("&", "%26").Replace("+", "%2B").Replace("/", "%2F").Replace("?", "%3F").Replace("#", "%23").Replace("=", "%3D");
            string content = "userName=" + username + "&password=" + password + "&appCode=" + "ebv09odrx2ty381fncud";
            string url = "https://accounts.sih.cq.cn/api/webauth/auth";
            //获取提交的字节
            byte[] bs = Encoding.UTF8.GetBytes(content);
            //设置提交的相关参数
            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(url);
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = bs.Length;
            //提交请求数据
            Stream reqStream = req.GetRequestStream();
            reqStream.Write(bs, 0, bs.Length);
            reqStream.Close();
            //接收返回的页面，必须的，不能省略
            WebResponse wr = req.GetResponse();
            System.IO.Stream respStream = wr.GetResponseStream();
            System.IO.StreamReader reader = new System.IO.StreamReader(respStream, System.Text.Encoding.GetEncoding("utf-8"));
            string message = reader.ReadToEnd();
            if("200" != message) {
                throw new DomainException("登录失败，请检查您的用户名及密码是否正确输入");
            }
            wr.Close();
        }
        public AuthenticationUser Logout() {
            FormsAuthentication.SignOut();
            return DefaultUser;
        }

        public AuthenticationUser GetUser() {
            if(this.ServiceContext == null || this.ServiceContext.User == null || !this.ServiceContext.User.Identity.IsAuthenticated)
                return DefaultUser;
            var userId = this.GetCurrentUserInfo().Id;
            var user = this.ObjectContext.Personnels.Include("Enterprise").FirstOrDefault(u => u.Id == userId);
            if(user == null)
                return DefaultUser;
            return new AuthenticationUser {
                Name = CreateUserInfo(user),
                User = user,
            };
        }

        public void UpdateUser(AuthenticationUser user) {
            //
        }

        ///  <summary>
        ///  PDA登陆方法
        ///  </summary>
        ///  <param  name="userName"></param>
        ///  <param  name="password"></param>
        ///  <param  name="enterpriseCode"></param>
        ///  <returns></returns>
        public Personnel LoginForPDA(string userName, string password, string enterpriseCode) {
            var user = this.ValidateUserForPDA(userName, password, enterpriseCode);

            return user;
        }


        private Personnel ValidateUserForPDA(string loginId, string password, string enterpriseCode) {
            var loweredLoginId = loginId.ToLower();
            var lowEnterpriseCode = enterpriseCode.ToLower();

            //调接口验证用户密码
            this.validateFoeUser(loginId, password);

            return this.ObjectContext.Personnels.Include("Enterprise").FirstOrDefault(u => (u.LoginId.ToLower() == loweredLoginId || u.CellNumber.ToLower() == loweredLoginId)
                && u.Status == (int)SecurityUserStatus.有效 && (u.Enterprise.Code.ToLower() == lowEnterpriseCode && u.Enterprise.Status == (int)SecurityEnterpriseStatus.生效));
        }
    }
}
