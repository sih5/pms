﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.ServiceModel.DomainServices.Server.ApplicationServices;

namespace Sunlight.Silverlight.Security.Web {
    public class AuthenticationUser : IUser {
        [Key]
        public string Name {
            get;
            set;
        }

        [DataMember]
        public Personnel User {
            get;
            set;
        }

        [DataMember]
        public IEnumerable<string> Roles {
            get;
            set;
        }

        [DataMember]
        public DateTime Timestamp {
            get;
            set;
        }
        [DataMember]
        public string ErrorMessage
        {
            get;
            set;
        }

        [DataMember]
        public bool IsFisrt
        {
            get;
            set;
        }

        ///// <summary>
        ///// 企业类型
        ///// </summary>
        //[DataMember]
        //public int? EnterpriseCategoryId {
        //    get;
        //    set;
        //}

        //[DataMember]
        //public bool? IsIdm {
        //    get;
        //    set;
        //}

    }
}
