﻿using System;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Security.Web {
    /// <summary>
    /// 用户登陆信息
    /// </summary>
    public partial class UserLoginInfo {
        /// <summary>
        /// 系统时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? SysTime {
            get;
            set;
        }

        /// <summary>
        /// 企业类型
        /// </summary>
        //[DataMemberAttribute]
        //public int? EnterpriseCategoryId {
        //    get;
        //    set;
        //}

        /// <summary>
        /// 是否供应商 走IDM接口
        /// </summary>
        //[DataMemberAttribute]
        //public bool? IsIdm {
        //    get;
        //    set;
        //}
    }
}
