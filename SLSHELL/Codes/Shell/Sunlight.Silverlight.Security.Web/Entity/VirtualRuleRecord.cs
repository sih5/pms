﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Security.Web {
    [DataContract(IsReference = true)]
    public class VirtualRuleRecord : EntityObject {

        [Key]
        [DataMemberAttribute]
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 角色id
        /// </summary>
        [DataMemberAttribute]
        public int RoleId {
            get;
            set;
        }

        /// <summary>
        /// 节点id
        /// </summary>
        [DataMemberAttribute]
        public int NodeId {
            get;
            set;
        }

        /// <summary>
        /// 操作类型
        /// </summary>
        [DataMemberAttribute]
        public int? Status {
            get;
            set;
        }

        /// <summary>
        /// 创建人id
        /// </summary>
        [DataMemberAttribute]
        public int? CreatorId {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        [DataMemberAttribute]
        public string CreatorName {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DataMemberAttribute]
        public DateTime? CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 企业编号
        /// </summary>
        [DataMemberAttribute]
        public string EnterpriseCode {
            get;
            set;
        }

        /// <summary>
        /// 企业名称
        /// </summary>
        [DataMemberAttribute]
        public string EnterpriseName {
            get;
            set;
        }

        /// <summary>
        /// 角色名称
        /// </summary>
        [DataMemberAttribute]
        public string RoleName {
            get;
            set;
        }

        /// <summary>
        /// 节点名称
        /// </summary>
        [DataMemberAttribute]
        public string NodeName {
            get;
            set;
        }

        /// <summary>
        /// 操作名称
        /// </summary>
        [DataMemberAttribute]
        public string ActionName {
            get;
            set;
        }

    }
}