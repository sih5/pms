﻿
using System.Data;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.ServiceModel.DomainServices.EntityFramework;
using System.ServiceModel.DomainServices.Hosting;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Security.Web {
    [EnableClientAccess]
    public sealed partial class SecurityDomainService : LinqToEntitiesDomainService<SecurityEntities> {
        protected override SecurityEntities CreateObjectContext() {
            var connString = DomainServiceHelper.CreateEntityConnectionString("Security");
            return connString == null ? base.CreateObjectContext() : new SecurityEntities(new EntityConnection(connString));
        }

        private void InsertToDatabase(EntityObject entity) {
            if(entity.EntityState != EntityState.Detached)
                this.ObjectContext.ObjectStateManager.ChangeObjectState(entity, EntityState.Added);
            else {
                switch(entity.GetType().Name) {
                    case "AuthorizedColumn":
                        this.ObjectContext.AuthorizedColumns.AddObject(entity as AuthorizedColumn);
                        break;
                    case "Enterprise":
                        this.ObjectContext.Enterprises.AddObject(entity as Enterprise);
                        break;
                    case "EnterpriseCategory":
                        this.ObjectContext.EnterpriseCategories.AddObject(entity as EnterpriseCategory);
                        break;
                    case "EnterpriseImageResource":
                        this.ObjectContext.EnterpriseImageResources.AddObject(entity as EnterpriseImageResource);
                        break;
                    case "EntNodeTemplate":
                        this.ObjectContext.EntNodeTemplates.AddObject(entity as EntNodeTemplate);
                        break;
                    case "EntNodeTemplateDetail":
                        this.ObjectContext.EntNodeTemplateDetails.AddObject(entity as EntNodeTemplateDetail);
                        break;
                    case "EntOrganizationTpl":
                        this.ObjectContext.EntOrganizationTpls.AddObject(entity as EntOrganizationTpl);
                        break;
                    case "EntOrganizationTplDetail":
                        this.ObjectContext.EntOrganizationTplDetails.AddObject(entity as EntOrganizationTplDetail);
                        break;
                    case "FavoritePage":
                        this.ObjectContext.FavoritePages.AddObject(entity as FavoritePage);
                        break;
                    case "KeyValueItem":
                        this.ObjectContext.KeyValueItems.AddObject(entity as KeyValueItem);
                        break;
                    case "LoginLog":
                        this.ObjectContext.LoginLogs.AddObject(entity as LoginLog);
                        break;
                    case "Node":
                        this.ObjectContext.Nodes.AddObject(entity as Node);
                        break;
                    case "Organization":
                        this.ObjectContext.Organizations.AddObject(entity as Organization);
                        break;
                    case "OrganizationPersonnel":
                        this.ObjectContext.OrganizationPersonnels.AddObject(entity as OrganizationPersonnel);
                        break;
                    case "Page":
                        this.ObjectContext.Pages.AddObject(entity as Page);
                        break;
                    case "PasswordPolicy":
                        this.ObjectContext.PasswordPolicies.AddObject(entity as PasswordPolicy);
                        break;
                    case "Personnel":
                        this.ObjectContext.Personnels.AddObject(entity as Personnel);
                        break;
                    case "Role":
                        this.ObjectContext.Roles.AddObject(entity as Role);
                        break;
                    case "RolePersonnel":
                        this.ObjectContext.RolePersonnels.AddObject(entity as RolePersonnel);
                        break;
                    case "RoleTemplate":
                        this.ObjectContext.RoleTemplates.AddObject(entity as RoleTemplate);
                        break;
                    case "RoleTemplateRule":
                        this.ObjectContext.RoleTemplateRules.AddObject(entity as RoleTemplateRule);
                        break;
                    case "Rule":
                        this.ObjectContext.Rules.AddObject(entity as Rule);
                        break;
                    case "RuleRecord":
                        this.ObjectContext.RuleRecords.AddObject(entity as RuleRecord);
                        break;
                    case "SecurityAction":
                        this.ObjectContext.SecurityActions.AddObject(entity as SecurityAction);
                        break;
                    case "UserLoginInfo":
                        this.ObjectContext.UserLoginInfoes.AddObject(entity as UserLoginInfo);
                        break;
                }
            }
        }

        private void UpdateToDatabase<T>(ObjectSet<T> objectSet, T entity) where T : class {
            if(this.ChangeSet == null || !this.ChangeSet.ChangeSetEntries.Any(e => ReferenceEquals(e.Entity, entity))) {
                objectSet.AttachAsModified(entity);
                return;
            }
            var original = this.ChangeSet.GetOriginal(entity);
            if(original == null)
                objectSet.AttachAsModified(entity);
            else
                objectSet.AttachAsModified(entity, original);
        }

        private void UpdateToDatabase(EntityObject entity) {
            switch(entity.GetType().Name) {
                case "AuthorizedColumn":
                    this.UpdateToDatabase(this.ObjectContext.AuthorizedColumns, (AuthorizedColumn)entity);
                    break;
                case "Enterprise":
                    this.UpdateToDatabase(this.ObjectContext.Enterprises, (Enterprise)entity);
                    break;
                case "EnterpriseCategory":
                    this.UpdateToDatabase(this.ObjectContext.EnterpriseCategories, (EnterpriseCategory)entity);
                    break;
                case "EnterpriseImageResource":
                    this.UpdateToDatabase(this.ObjectContext.EnterpriseImageResources, (EnterpriseImageResource)entity);
                    break;
                case "EntNodeTemplate":
                    this.UpdateToDatabase(this.ObjectContext.EntNodeTemplates, (EntNodeTemplate)entity);
                    break;
                case "EntNodeTemplateDetail":
                    this.UpdateToDatabase(this.ObjectContext.EntNodeTemplateDetails, (EntNodeTemplateDetail)entity);
                    break;
                case "EntOrganizationTpl":
                    this.UpdateToDatabase(this.ObjectContext.EntOrganizationTpls, (EntOrganizationTpl)entity);
                    break;
                case "EntOrganizationTplDetail":
                    this.UpdateToDatabase(this.ObjectContext.EntOrganizationTplDetails, (EntOrganizationTplDetail)entity);
                    break;
                case "FavoritePage":
                    this.UpdateToDatabase(this.ObjectContext.FavoritePages, (FavoritePage)entity);
                    break;
                case "KeyValueItem":
                    this.UpdateToDatabase(this.ObjectContext.KeyValueItems, (KeyValueItem)entity);
                    break;
                case "LoginLog":
                    this.UpdateToDatabase(this.ObjectContext.LoginLogs, (LoginLog)entity);
                    break;
                case "Node":
                    this.UpdateToDatabase(this.ObjectContext.Nodes, (Node)entity);
                    break;
                case "Organization":
                    this.UpdateToDatabase(this.ObjectContext.Organizations, (Organization)entity);
                    break;
                case "OrganizationPersonnel":
                    this.UpdateToDatabase(this.ObjectContext.OrganizationPersonnels, (OrganizationPersonnel)entity);
                    break;
                case "Page":
                    this.UpdateToDatabase(this.ObjectContext.Pages, (Page)entity);
                    break;
                case "PasswordPolicy":
                    this.UpdateToDatabase(this.ObjectContext.PasswordPolicies, (PasswordPolicy)entity);
                    break;
                case "Personnel":
                    this.UpdateToDatabase(this.ObjectContext.Personnels, (Personnel)entity);
                    break;
                case "Role":
                    this.UpdateToDatabase(this.ObjectContext.Roles, (Role)entity);
                    break;
                case "RolePersonnel":
                    this.UpdateToDatabase(this.ObjectContext.RolePersonnels, (RolePersonnel)entity);
                    break;
                case "RoleTemplate":
                    this.UpdateToDatabase(this.ObjectContext.RoleTemplates, (RoleTemplate)entity);
                    break;
                case "RoleTemplateRule":
                    this.UpdateToDatabase(this.ObjectContext.RoleTemplateRules, (RoleTemplateRule)entity);
                    break;
                case "Rule":
                    this.UpdateToDatabase(this.ObjectContext.Rules, (Rule)entity);
                    break;
                case "RuleRecord":
                    this.UpdateToDatabase(this.ObjectContext.RuleRecords, (RuleRecord)entity);
                    break;
                case "SecurityAction":
                    this.UpdateToDatabase(this.ObjectContext.SecurityActions, (SecurityAction)entity);
                    break;
                case "UserLoginInfo":
                    this.UpdateToDatabase(this.ObjectContext.UserLoginInfoes, (UserLoginInfo)entity);
                    break;
            }
        }

        private void DeleteFromDatabase<T>(ObjectSet<T> objectSet, T entity) where T : class {
            objectSet.Attach(entity);
            objectSet.DeleteObject(entity);
        }

        private void DeleteFromDatabase(EntityObject entity) {
            if(entity.EntityState != EntityState.Detached)
                this.ObjectContext.ObjectStateManager.ChangeObjectState(entity, EntityState.Deleted);
            else {
                switch(entity.GetType().Name) {
                    case "AuthorizedColumn":
                        this.DeleteFromDatabase(this.ObjectContext.AuthorizedColumns, (AuthorizedColumn)entity);
                        break;
                    case "Enterprise":
                        this.DeleteFromDatabase(this.ObjectContext.Enterprises, (Enterprise)entity);
                        break;
                    case "EnterpriseCategory":
                        this.DeleteFromDatabase(this.ObjectContext.EnterpriseCategories, (EnterpriseCategory)entity);
                        break;
                    case "EnterpriseImageResource":
                        this.DeleteFromDatabase(this.ObjectContext.EnterpriseImageResources, (EnterpriseImageResource)entity);
                        break;
                    case "EntNodeTemplate":
                        this.DeleteFromDatabase(this.ObjectContext.EntNodeTemplates, (EntNodeTemplate)entity);
                        break;
                    case "EntNodeTemplateDetail":
                        this.DeleteFromDatabase(this.ObjectContext.EntNodeTemplateDetails, (EntNodeTemplateDetail)entity);
                        break;
                    case "EntOrganizationTpl":
                        this.DeleteFromDatabase(this.ObjectContext.EntOrganizationTpls, (EntOrganizationTpl)entity);
                        break;
                    case "EntOrganizationTplDetail":
                        this.DeleteFromDatabase(this.ObjectContext.EntOrganizationTplDetails, (EntOrganizationTplDetail)entity);
                        break;
                    case "FavoritePage":
                        this.DeleteFromDatabase(this.ObjectContext.FavoritePages, (FavoritePage)entity);
                        break;
                    case "KeyValueItem":
                        this.DeleteFromDatabase(this.ObjectContext.KeyValueItems, (KeyValueItem)entity);
                        break;
                    case "LoginLog":
                        this.DeleteFromDatabase(this.ObjectContext.LoginLogs, (LoginLog)entity);
                        break;
                    case "Node":
                        this.DeleteFromDatabase(this.ObjectContext.Nodes, (Node)entity);
                        break;
                    case "Organization":
                        this.DeleteFromDatabase(this.ObjectContext.Organizations, (Organization)entity);
                        break;
                    case "OrganizationPersonnel":
                        this.DeleteFromDatabase(this.ObjectContext.OrganizationPersonnels, (OrganizationPersonnel)entity);
                        break;
                    case "Page":
                        this.DeleteFromDatabase(this.ObjectContext.Pages, (Page)entity);
                        break;
                    case "PasswordPolicy":
                        this.DeleteFromDatabase(this.ObjectContext.PasswordPolicies, (PasswordPolicy)entity);
                        break;
                    case "Personnel":
                        this.DeleteFromDatabase(this.ObjectContext.Personnels, (Personnel)entity);
                        break;
                    case "Role":
                        this.DeleteFromDatabase(this.ObjectContext.Roles, (Role)entity);
                        break;
                    case "RolePersonnel":
                        this.DeleteFromDatabase(this.ObjectContext.RolePersonnels, (RolePersonnel)entity);
                        break;
                    case "RoleTemplate":
                        this.DeleteFromDatabase(this.ObjectContext.RoleTemplates, (RoleTemplate)entity);
                        break;
                    case "RoleTemplateRule":
                        this.DeleteFromDatabase(this.ObjectContext.RoleTemplateRules, (RoleTemplateRule)entity);
                        break;
                    case "Rule":
                        this.DeleteFromDatabase(this.ObjectContext.Rules, (Rule)entity);
                        break;
                    case "RuleRecord":
                        this.DeleteFromDatabase(this.ObjectContext.RuleRecords, (RuleRecord)entity);
                        break;
                    case "SecurityAction":
                        this.DeleteFromDatabase(this.ObjectContext.SecurityActions, (SecurityAction)entity);
                        break;
                    case "UserLoginInfo":
                        this.DeleteFromDatabase(this.ObjectContext.UserLoginInfoes, (UserLoginInfo)entity);
                        break;
                }
            }
        }

		[RequiresAuthentication]
        public IQueryable<AuthorizedColumn> GetAuthorizedColumns() {
            return this.ObjectContext.AuthorizedColumns.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<Enterprise> GetEnterprises() {
            return this.ObjectContext.Enterprises.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<EnterpriseCategory> GetEnterpriseCategories() {
            return this.ObjectContext.EnterpriseCategories.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<EnterpriseImageResource> GetEnterpriseImageResources() {
            return this.ObjectContext.EnterpriseImageResources.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<EntNodeTemplate> GetEntNodeTemplates() {
            return this.ObjectContext.EntNodeTemplates.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<EntNodeTemplateDetail> GetEntNodeTemplateDetails() {
            return this.ObjectContext.EntNodeTemplateDetails.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<EntOrganizationTpl> GetEntOrganizationTpls() {
            return this.ObjectContext.EntOrganizationTpls.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<EntOrganizationTplDetail> GetEntOrganizationTplDetails() {
            return this.ObjectContext.EntOrganizationTplDetails.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<FavoritePage> GetFavoritePages() {
            return this.ObjectContext.FavoritePages.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<KeyValueItem> GetKeyValueItems() {
            return this.ObjectContext.KeyValueItems.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<LoginLog> GetLoginLogs() {
            return this.ObjectContext.LoginLogs.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<Node> GetNodes() {
            return this.ObjectContext.Nodes.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<Organization> GetOrganizations() {
            return this.ObjectContext.Organizations.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<OrganizationPersonnel> GetOrganizationPersonnels() {
            return this.ObjectContext.OrganizationPersonnels.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<Page> GetPages() {
            return this.ObjectContext.Pages.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<PasswordPolicy> GetPasswordPolicies() {
            return this.ObjectContext.PasswordPolicies.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<Personnel> GetPersonnels() {
            return this.ObjectContext.Personnels.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<Role> GetRoles() {
            return this.ObjectContext.Roles.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<RolePersonnel> GetRolePersonnels() {
            return this.ObjectContext.RolePersonnels.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<RoleTemplate> GetRoleTemplates() {
            return this.ObjectContext.RoleTemplates.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<RoleTemplateRule> GetRoleTemplateRules() {
            return this.ObjectContext.RoleTemplateRules.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<Rule> GetRules() {
            return this.ObjectContext.Rules.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<RuleRecord> GetRuleRecords() {
            return this.ObjectContext.RuleRecords.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<SecurityAction> GetSecurityActions() {
            return this.ObjectContext.SecurityActions.OrderBy(e => e.Id);
        }

		[RequiresAuthentication]
        public IQueryable<UserLoginInfo> GetUserLoginInfoes() {
            return this.ObjectContext.UserLoginInfoes.OrderBy(e => e.Id);
        }
    }
}

