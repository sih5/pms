﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Transactions;
using Sunlight.Silverlight.Log.Web;
#if ORACLE
using System.Web.Configuration;
using Devart.Data.Oracle;
using Devart.Data.Oracle.Entity.Configuration;
#endif

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
#if ORACLE
        static readonly OracleMonitor MyMonitor = new OracleMonitor();

        static SecurityDomainService() {
            var config = OracleEntityProviderConfig.Instance;
            config.Workarounds.IgnoreSchemaName = true;
            config.Workarounds.DisableQuoting = true;
            config.DmlOptions.BatchUpdates.Enabled = true;
            config.DmlOptions.ReuseParameters = true;
            config.DmlOptions.InsertNullBehaviour = InsertNullBehaviour.InsertDefaultOrNull;

            bool isActive;
            if(bool.TryParse(WebConfigurationManager.AppSettings["OracleMonitorIsActive"], out isActive))
                MyMonitor.IsActive = isActive;
        }
#endif

        private bool ValidateUniqueValue<T>(Func<T, object> getValue, Func<T, bool> filter, string[] propertyName) where T : EntityObject {
            var entries = this.ChangeSet.ChangeSetEntries.Where(e => e.Operation == DomainOperation.Insert || e.Operation == DomainOperation.Update).ToArray();
            var entities = entries.Select(e => e.Entity).OfType<T>().ToArray();
            if(filter != null)
                entities = entities.Where(filter).ToArray();
            if(entities.Length == 0)
                return true;
            var values = entities.Select(getValue).ToArray();
            var result = values.Length == values.Distinct().Count();
            if(!result)
                foreach(var changeSetEntry in entries)
                    changeSetEntry.ValidationErrors = new[] { new ValidationResultInfo("属性值不允许重复", propertyName) };
            return result;
        }

        protected override bool ValidateChangeSet() {
            if(!base.ValidateChangeSet())
                return false;

            if(this.ChangeSet != null) {
                if(!this.ValidateUniqueValue<EntNodeTemplate>(e => e.Code.ToLower(), f => f.Status != (int)SecurityCommonStatus.作废, new[] { "Code" }))
                    return false;
                if(!this.ValidateUniqueValue<EntNodeTemplate>(e => e.Name.ToLower(), f => f.Status != (int)SecurityCommonStatus.作废, new[] { "Name" }))
                    return false;
                if(!this.ValidateUniqueValue<EntOrganizationTpl>(e => e.Code.ToLower(), f => f.Status != (int)SecurityCommonStatus.作废, new[] { "Code" }))
                    return false;
                if(!this.ValidateUniqueValue<EntOrganizationTpl>(e => e.Name.ToLower(), f => f.Status != (int)SecurityCommonStatus.作废, new[] { "Name" }))
                    return false;
                if(!this.ValidateUniqueValue<RoleTemplate>(e => e.Name.ToLower(), f => f.Status != (int)SecurityCommonStatus.作废, new[] { "Name" }))
                    return false;
                if(!this.ValidateUniqueValue<Enterprise>(e => e.Code.ToLower(), f => f.Status != (int)SecurityEnterpriseStatus.作废, new[] { "Code" }))
                    return false;
                if(!this.ValidateUniqueValue<Enterprise>(e => e.Name.ToLower(), f => f.Status != (int)SecurityEnterpriseStatus.作废, new[] { "Name" }))
                    return false;
                if(!this.ValidateUniqueValue<Organization>(e => new {
                    e.EnterpriseId,
                    Code = e.Code.ToLower()
                }, f => f.Status != (int)SecurityCommonStatus.作废, new[] { "EnterpriseId", "Code" }))
                    return false;
                if(!this.ValidateUniqueValue<Organization>(e => new {
                    e.EnterpriseId,
                    Name = e.Name.ToLower()
                }, f => f.Status != (int)SecurityCommonStatus.作废, new[] { "EnterpriseId", "Name" }))
                    return false;
                if(!this.ValidateUniqueValue<OrganizationPersonnel>(e => new {
                    e.PersonnelId,
                    e.OrganizationId
                }, null, new[] { "PersonnelId", "OrganizationId" }))
                    return false;
                if(!this.ValidateUniqueValue<Personnel>(e => e.CellNumber.ToLower(), f => f.Status != (int)SecurityUserStatus.作废 && !string.IsNullOrEmpty(f.CellNumber), new[] { "CellNumber" }))
                    return false;
                if(!this.ValidateUniqueValue<Personnel>(e => new {
                    e.EnterpriseId,
                    e.LoginId
                }, f => f.Status != (int)SecurityUserStatus.作废, new[] { "EnterpriseId", "LoginId" }))
                    return false;
                if(!this.ValidateUniqueValue<RolePersonnel>(e => new {
                    e.RoleId,
                    e.PersonnelId
                }, null, new[] { "RoleId", "PersonnelId" }))
                    return false;
                if(!this.ValidateUniqueValue<Node>(e => new {
                    e.CategoryType,
                    e.CategoryId
                }, f => f.Status != (int)SecurityCommonStatus.作废, new[] { "CategoryType", "CategoryId" }))
                    return false;
                if(!this.ValidateUniqueValue<FavoritePage>(e => new {
                    e.PersonnelId,
                    e.PageId
                }, null, new[] { "PersonnelId", "PageId" }))
                    return false;
            }
            return true;
        }

        private LogInfo logInfo;

        private string[] GetFullExceptionMessage(Exception ex) {
            var exception = ex;
            var errorMessages = new List<string> {
                exception.Message
            };
            while(exception.InnerException != null) {
                exception = exception.InnerException;
                errorMessages.Add(exception.Message);
            }
            return errorMessages.ToArray();
        }

        public override bool Submit(ChangeSet changeSet) {
            try {
                var userInfo = this.GetCurrentUserInfo();
                this.logInfo = LogManager.logManager.NewLogInfo("Security", userInfo.LoginId, userInfo.Name, userInfo.EnterpriseCode, userInfo.EnterpriseName, OperatingType.Submit, changeSet);

                bool result;

                using(var tx = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions {
                    IsolationLevel = IsolationLevel.ReadCommitted
                })) {
                    //TODO:如数据库错误，则信息无法抛出至客户端
                    result = base.Submit(changeSet);
                    if(!this.ChangeSet.HasError)
                        tx.Complete();
                    else
                        this.logInfo.SetErrorLog(changeSet, "变更内容提交出错");
                }

                return result;
            } catch(Exception ex) {
                var errorMessages = this.GetFullExceptionMessage(ex);
                this.logInfo.SetErrorLog(null, errorMessages);
                //异常信息作为实体对象本身的错误信息返回
                changeSet.ChangeSetEntries.First(e => e.Operation == DomainOperation.Insert || e.Operation == DomainOperation.Update).ValidationErrors = new[] { new ValidationResultInfo(string.Format("提交出错：{0}", string.Join("\r\n", errorMessages)), new string[0]) };
                return false;
            } finally {
                LogManager.logManager.SetLogInfoComplete(this.logInfo);
            }
        }

        public override IEnumerable Query(QueryDescription queryDescription, out IEnumerable<ValidationResult> validationErrors, out int totalCount) {
            validationErrors = null;
            try {
                var userInfo = this.GetCurrentUserInfo();
                this.logInfo = LogManager.logManager.NewLogInfo("Security", userInfo.LoginId, userInfo.Name, userInfo.EnterpriseCode, userInfo.EnterpriseName, OperatingType.Query, queryDescription);
                var result = base.Query(queryDescription, out validationErrors, out totalCount);
                this.logInfo.SetQueryResult(result, totalCount);
                return result;
            } catch(Exception ex) {
                this.logInfo.SetErrorLog(validationErrors, ex);
                throw;
            } finally {
                LogManager.logManager.SetLogInfoComplete(this.logInfo);
            }
        }
    }
}