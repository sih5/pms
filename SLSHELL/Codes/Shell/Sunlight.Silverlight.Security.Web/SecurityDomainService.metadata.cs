using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Security.Web.Resources;

// ReSharper disable ConvertToStaticClass, ConvertNullableToShortForm, RedundantNameQualifier
namespace Sunlight.Silverlight.Security.Web {
    [MetadataType(typeof(AuthorizedColumn.Metadata))]
    partial class AuthorizedColumn {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal]
            public string ColumnName {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal]
            public int PageId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Status")]
            public int Status {
                get;
                set;
            }

            [Include]
            public EntityCollection<Node> Nodes {
                get;
                set;
            }

            [Include]
            public Page Page {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(Enterprise.Metadata))]
    partial class Enterprise {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "Enterprise_Code")]
            public string Code {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime"), Editable(false)]
            public Nullable<System.DateTime> CreateTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorId"), Editable(false)]
            public Nullable<int> CreatorId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorName"), Editable(false)]
            public string CreatorName {
                get;
                set;
            }

            [RoundtripOriginal]
            public string CustomProperty1 {
                get;
                set;
            }

            [RoundtripOriginal]
            public string CustomProperty2 {
                get;
                set;
            }

            [RoundtripOriginal]
            public int EnterpriseCategoryId {
                get;
                set;
            }

            [RoundtripOriginal]
            public Nullable<int> EntNodeTemplateId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal]
            public Nullable<bool> IsIDM {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierId"), Editable(false)]
            public Nullable<int> ModifierId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierName"), Editable(false)]
            public string ModifierName {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifyTime"), Editable(false)]
            public Nullable<System.DateTime> ModifyTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "Enterprise_Name")]
            public string Name {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Remark")]
            public string Remark {
                get;
                set;
            }

            [Timestamp]
            public Nullable<System.DateTime> RowVersion {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Status")]
            public int Status {
                get;
                set;
            }

            [Include]
            public EnterpriseCategory EnterpriseCategory {
                get;
                set;
            }

            [Include]
            public EntNodeTemplate EntNodeTemplate {
                get;
                set;
            }

            [Include, Display(ResourceType = typeof(EntityStrings), Name = "Enterprise_Organizations")]
            public EntityCollection<Organization> Organizations {
                get;
                set;
            }

            [Include]
            public EntityCollection<Personnel> Personnels {
                get;
                set;
            }

            [Include]
            public EntityCollection<Role> Roles {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(EnterpriseCategory.Metadata))]
    partial class EnterpriseCategory {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Code")]
            public string Code {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime"), Editable(false)]
            public Nullable<System.DateTime> CreateTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorId"), Editable(false)]
            public Nullable<int> CreatorId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorName"), Editable(false)]
            public string CreatorName {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierId"), Editable(false)]
            public Nullable<int> ModifierId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierName"), Editable(false)]
            public string ModifierName {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifyTime"), Editable(false)]
            public Nullable<System.DateTime> ModifyTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Name")]
            public string Name {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Remark")]
            public string Remark {
                get;
                set;
            }

            [Timestamp]
            public Nullable<System.DateTime> RowVersion {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Status")]
            public int Status {
                get;
                set;
            }

            [Include]
            public EntityCollection<Enterprise> Enterprises {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(EnterpriseImageResource.Metadata))]
    partial class EnterpriseImageResource {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal]
            public int EnterpriseId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Name")]
            public string Name {
                get;
                set;
            }

            [RoundtripOriginal]
            public string Path {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Remark")]
            public string Remark {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(EntNodeTemplate.Metadata))]
    partial class EntNodeTemplate {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "EntNodeTemplate_Code")]
            public string Code {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime"), Editable(false)]
            public Nullable<System.DateTime> CreateTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorId"), Editable(false)]
            public Nullable<int> CreatorId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorName"), Editable(false)]
            public string CreatorName {
                get;
                set;
            }

            [RoundtripOriginal]
            public int EnterpriseId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierId"), Editable(false)]
            public Nullable<int> ModifierId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierName"), Editable(false)]
            public string ModifierName {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifyTime"), Editable(false)]
            public Nullable<System.DateTime> ModifyTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "EntNodeTemplate_Name")]
            public string Name {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Remark")]
            public string Remark {
                get;
                set;
            }

            [Timestamp]
            public Nullable<System.DateTime> RowVersion {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Status")]
            public int Status {
                get;
                set;
            }

            [Include, Display(ResourceType = typeof(EntityStrings), Name = "EntNodeTemplate_Enterprises")]
            public EntityCollection<Enterprise> Enterprises {
                get;
                set;
            }

            [Include, Composition, Display(ResourceType = typeof(EntityStrings), Name = "EntNodeTemplate_EntNodeTemplateDetails")]
            public EntityCollection<EntNodeTemplateDetail> EntNodeTemplateDetails {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(EntNodeTemplateDetail.Metadata))]
    partial class EntNodeTemplateDetail {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal]
            public int EntNodeTemplateId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal]
            public int NodeId {
                get;
                set;
            }

            [Include]
            public EntNodeTemplate EntNodeTemplate {
                get;
                set;
            }

            [Include]
            public Node Node {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(EntOrganizationTpl.Metadata))]
    partial class EntOrganizationTpl {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "EntOrganizationTpl_Code")]
            public string Code {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime"), Editable(false)]
            public Nullable<System.DateTime> CreateTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorId"), Editable(false)]
            public Nullable<int> CreatorId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorName"), Editable(false)]
            public string CreatorName {
                get;
                set;
            }

            [RoundtripOriginal]
            public int EnterpriseId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierId"), Editable(false)]
            public Nullable<int> ModifierId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierName"), Editable(false)]
            public string ModifierName {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifyTime"), Editable(false)]
            public Nullable<System.DateTime> ModifyTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "EntOrganizationTpl_Name")]
            public string Name {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "EntOrganizationTpl_Remark")]
            public string Remark {
                get;
                set;
            }

            [Timestamp]
            public Nullable<System.DateTime> RowVersion {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Status")]
            public int Status {
                get;
                set;
            }

            [Include, Composition, Display(ResourceType = typeof(EntityStrings), Name = "EntOrganizationTpl_EntOrganizationTplDetails")]
            public EntityCollection<EntOrganizationTplDetail> EntOrganizationTplDetails {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(EntOrganizationTplDetail.Metadata))]
    partial class EntOrganizationTplDetail {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal]
            public int EntOrganizationTplId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "EntOrganizationTplDetail_OrganizationCode")]
            public string OrganizationCode {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "EntOrganizationTplDetail_OrganizationName")]
            public string OrganizationName {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "EntOrganizationTplDetail_OrganizationType")]
            public int OrganizationType {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ParentId")]
            public int ParentId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Sequence")]
            public int Sequence {
                get;
                set;
            }

            [Include]
            public EntOrganizationTplDetail ChildEntOrganizationTplDetails {
                get;
                set;
            }

            [Include]
            public EntOrganizationTpl EntOrganizationTpl {
                get;
                set;
            }

            [Include]
            public EntOrganizationTplDetail ParentEntOrganizationTplDetail {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(FavoritePage.Metadata))]
    partial class FavoritePage {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal]
            public int PageId {
                get;
                set;
            }

            [RoundtripOriginal]
            public int PersonnelId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Sequence")]
            public int Sequence {
                get;
                set;
            }

            [Include]
            public Page Page {
                get;
                set;
            }

            [Include]
            public Personnel Personnel {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(KeyValueItem.Metadata))]
    partial class KeyValueItem {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal]
            public string Caption {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Category")]
            public string Category {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal]
            public bool IsBuiltIn {
                get;
                set;
            }

            [RoundtripOriginal]
            public int Key {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Name")]
            public string Name {
                get;
                set;
            }

            [Timestamp]
            public Nullable<System.DateTime> RowVersion {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Status")]
            public int Status {
                get;
                set;
            }

            [RoundtripOriginal]
            public string Value {
                get;
                set;
            }

            [RoundtripOriginal]
            public string Value_enUS {
                get;
                set;
            }

            [RoundtripOriginal]
            public string Value_jaJP {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(LoginLog.Metadata))]
    partial class LoginLog {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal]
            public string IP {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "LoginLog_LoginTime")]
            public System.DateTime LoginTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "LoginLog_LogoutTime")]
            public Nullable<System.DateTime> LogoutTime {
                get;
                set;
            }

            [RoundtripOriginal]
            public string Mac {
                get;
                set;
            }

            [RoundtripOriginal]
            public int PersonnelId {
                get;
                set;
            }

            [Include]
            public Personnel Personnel {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(Node.Metadata))]
    partial class Node {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal]
            public int CategoryId {
                get;
                set;
            }

            [RoundtripOriginal]
            public int CategoryType {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Status")]
            public int Status {
                get;
                set;
            }

            [Include]
            public AuthorizedColumn AuthorizedColumn {
                get;
                set;
            }

            [Include]
            public EntityCollection<EntNodeTemplateDetail> EntNodeTemplateDetails {
                get;
                set;
            }

            [Include]
            public Page Page {
                get;
                set;
            }

            [Include]
            public EntityCollection<RoleTemplateRule> RoleTemplateRules {
                get;
                set;
            }

            [Include]
            public EntityCollection<RuleRecord> RuleRecords {
                get;
                set;
            }

            [Include]
            public EntityCollection<Rule> Rules {
                get;
                set;
            }

            [Include]
            public SecurityAction SecurityAction {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(Organization.Metadata))]
    partial class Organization {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "Organization_Code")]
            public string Code {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime"), Editable(false)]
            public Nullable<System.DateTime> CreateTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorId"), Editable(false)]
            public Nullable<int> CreatorId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorName"), Editable(false)]
            public string CreatorName {
                get;
                set;
            }

            [RoundtripOriginal]
            public int EnterpriseId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierId"), Editable(false)]
            public Nullable<int> ModifierId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierName"), Editable(false)]
            public string ModifierName {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifyTime"), Editable(false)]
            public Nullable<System.DateTime> ModifyTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "Organization_Name")]
            public string Name {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ParentId")]
            public int ParentId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Remark")]
            public string Remark {
                get;
                set;
            }

            [Timestamp]
            public Nullable<System.DateTime> RowVersion {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Sequence")]
            public int Sequence {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "Organization_Status")]
            public int Status {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "Organization_Type")]
            public int Type {
                get;
                set;
            }

            [Include]
            public Organization ChildOrganizations {
                get;
                set;
            }

            [Include]
            public Enterprise Enterprise {
                get;
                set;
            }

            [Include]
            public EntityCollection<OrganizationPersonnel> OrganizationPersonnels {
                get;
                set;
            }

            [Include]
            public Organization ParentOrganization {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(OrganizationPersonnel.Metadata))]
    partial class OrganizationPersonnel {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal]
            public int OrganizationId {
                get;
                set;
            }

            [RoundtripOriginal]
            public int PersonnelId {
                get;
                set;
            }

            [Include]
            public Organization Organization {
                get;
                set;
            }

            [Include]
            public Personnel Personnel {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(Page.Metadata))]
    partial class Page {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "Page_Description")]
            public string Description {
                get;
                set;
            }

            [RoundtripOriginal]
            public string Description_enUS {
                get;
                set;
            }

            [RoundtripOriginal]
            public string Description_jaJP {
                get;
                set;
            }

            [RoundtripOriginal]
            public string Icon {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Name")]
            public string Name {
                get;
                set;
            }

            [RoundtripOriginal]
            public string Name_enUS {
                get;
                set;
            }

            [RoundtripOriginal]
            public string Name_jaJP {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "Page_PageId")]
            public string PageId {
                get;
                set;
            }

            [RoundtripOriginal]
            public int PageSystem {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "Page_PageType")]
            public string PageType {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "Page_Parameter")]
            public string Parameter {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ParentId")]
            public int ParentId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Sequence")]
            public int Sequence {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Status")]
            public int Status {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Type")]
            public int Type {
                get;
                set;
            }

            [Include]
            public EntityCollection<SecurityAction> Actions {
                get;
                set;
            }

            [Include]
            public EntityCollection<AuthorizedColumn> AuthorizedColumns {
                get;
                set;
            }

            [Include]
            public EntityCollection<FavoritePage> FavoritePages {
                get;
                set;
            }

            [Include]
            public EntityCollection<Node> Nodes {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(PasswordPolicy.Metadata))]
    partial class PasswordPolicy {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal]
            public string DefaultPassword {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal]
            public bool IsEnabled {
                get;
                set;
            }

            [RoundtripOriginal]
            public int RemindDays {
                get;
                set;
            }

            [RoundtripOriginal]
            public int ValidPeriod {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(Personnel.Metadata))]
    partial class Personnel {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "Personnel_CellNumber")]
            public string CellNumber {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime"), Editable(false)]
            public Nullable<System.DateTime> CreateTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorId"), Editable(false)]
            public Nullable<int> CreatorId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorName"), Editable(false)]
            public string CreatorName {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "Personnel_Email")]
            public string Email {
                get;
                set;
            }

            [RoundtripOriginal]
            public int EnterpriseId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "Personnel_LoginId")]
            public string LoginId {
                get;
                set;
            }

            [RoundtripOriginal]
            public string MAC {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierId"), Editable(false)]
            public Nullable<int> ModifierId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierName"), Editable(false)]
            public string ModifierName {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifyTime"), Editable(false)]
            public Nullable<System.DateTime> ModifyTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "Personnel_Name")]
            public string Name {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "Personnel_Password")]
            internal string Password {
                get;
                set;
            }

            [RoundtripOriginal]
            public System.DateTime PasswordModifyTime {
                get;
                set;
            }

            [RoundtripOriginal]
            public string Photo {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Remark")]
            public string Remark {
                get;
                set;
            }

            [Timestamp]
            public Nullable<System.DateTime> RowVersion {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Status")]
            public int Status {
                get;
                set;
            }

            [Include]
            public Enterprise Enterprise {
                get;
                set;
            }

            [Include]
            public EntityCollection<FavoritePage> FavoritePages {
                get;
                set;
            }

            [Include]
            public EntityCollection<LoginLog> LoginLogs {
                get;
                set;
            }

            [Include, Display(ResourceType = typeof(EntityStrings), Name = "Personnel_OrganizationPersonnels")]
            public EntityCollection<OrganizationPersonnel> OrganizationPersonnels {
                get;
                set;
            }

            [Include, Display(ResourceType = typeof(EntityStrings), Name = "Personnel_RolePersonnels")]
            public EntityCollection<RolePersonnel> RolePersonnels {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(Role.Metadata))]
    partial class Role {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime"), Editable(false)]
            public Nullable<System.DateTime> CreateTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorId"), Editable(false)]
            public Nullable<int> CreatorId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorName"), Editable(false)]
            public string CreatorName {
                get;
                set;
            }

            [RoundtripOriginal]
            public int EnterpriseId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "Role_IsAdmin")]
            public bool IsAdmin {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierId"), Editable(false)]
            public Nullable<int> ModifierId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierName"), Editable(false)]
            public string ModifierName {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifyTime"), Editable(false)]
            public Nullable<System.DateTime> ModifyTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "Role_Name")]
            public string Name {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Remark")]
            public string Remark {
                get;
                set;
            }

            [Timestamp]
            public Nullable<System.DateTime> RowVersion {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Status")]
            public int Status {
                get;
                set;
            }

            [Include]
            public Enterprise Enterprise {
                get;
                set;
            }

            [Include, Display(ResourceType = typeof(EntityStrings), Name = "Role_RolePersonnels")]
            public EntityCollection<RolePersonnel> RolePersonnels {
                get;
                set;
            }

            [Include]
            public EntityCollection<RuleRecord> RuleRecords {
                get;
                set;
            }

            [Include]
            public EntityCollection<Rule> Rules {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(RolePersonnel.Metadata))]
    partial class RolePersonnel {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal]
            public int PersonnelId {
                get;
                set;
            }

            [RoundtripOriginal]
            public int RoleId {
                get;
                set;
            }

            [Include]
            public Personnel Personnel {
                get;
                set;
            }

            [Include]
            public Role Role {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(RoleTemplate.Metadata))]
    partial class RoleTemplate {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime"), Editable(false)]
            public Nullable<System.DateTime> CreateTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorId"), Editable(false)]
            public Nullable<int> CreatorId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorName"), Editable(false)]
            public string CreatorName {
                get;
                set;
            }

            [RoundtripOriginal]
            public int EnterpriseId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierId"), Editable(false)]
            public Nullable<int> ModifierId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifierName"), Editable(false)]
            public string ModifierName {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_ModifyTime"), Editable(false)]
            public Nullable<System.DateTime> ModifyTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "RoleTemplate_Name")]
            public string Name {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Remark")]
            public string Remark {
                get;
                set;
            }

            [Timestamp]
            public Nullable<System.DateTime> RowVersion {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Status")]
            public int Status {
                get;
                set;
            }

            [Include, Composition, Display(ResourceType = typeof(EntityStrings), Name = "RoleTemplate_RoleTemplateRules")]
            public EntityCollection<RoleTemplateRule> RoleTemplateRules {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(RoleTemplateRule.Metadata))]
    partial class RoleTemplateRule {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal]
            public int NodeId {
                get;
                set;
            }

            [RoundtripOriginal]
            public int RoleTemplateId {
                get;
                set;
            }

            [Include]
            public Node Node {
                get;
                set;
            }

            [Include]
            public RoleTemplate RoleTemplate {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(Rule.Metadata))]
    partial class Rule {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal]
            public int NodeId {
                get;
                set;
            }

            [RoundtripOriginal]
            public int RoleId {
                get;
                set;
            }

            [Include]
            public Node Node {
                get;
                set;
            }

            [Include]
            public Role Role {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(RuleRecord.Metadata))]
    partial class RuleRecord {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreateTime"), Editable(false)]
            public Nullable<System.DateTime> CreateTime {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorId"), Editable(false)]
            public Nullable<int> CreatorId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_CreatorName"), Editable(false)]
            public string CreatorName {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal]
            public int NodeId {
                get;
                set;
            }

            [RoundtripOriginal]
            public int RoleId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Status")]
            public int Status {
                get;
                set;
            }

            [Include]
            public Node Node {
                get;
                set;
            }

            [Include]
            public Role Role {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(SecurityAction.Metadata))]
    partial class SecurityAction {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Name")]
            public string Name {
                get;
                set;
            }

            [RoundtripOriginal]
            public string Name_enUS {
                get;
                set;
            }

            [RoundtripOriginal]
            public string Name_jaJP {
                get;
                set;
            }

            [RoundtripOriginal]
            public string OperationId {
                get;
                set;
            }

            [RoundtripOriginal]
            public int PageId {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Status")]
            public int Status {
                get;
                set;
            }

            [Include]
            public EntityCollection<Node> Nodes {
                get;
                set;
            }

            [Include]
            public Page Page {
                get;
                set;
            }

        }
    }

    [MetadataType(typeof(UserLoginInfo.Metadata))]
    partial class UserLoginInfo {
        internal sealed class Metadata {
            private Metadata() {
                //元数据类不会实例化
            }

            [RoundtripOriginal]
            public Nullable<System.DateTime> BeginLockTime {
                get;
                set;
            }

            [RoundtripOriginal]
            public string EnterpriseCode {
                get;
                set;
            }

            [RoundtripOriginal]
            public int ErrorPassNum {
                get;
                set;
            }

            [RoundtripOriginal, Display(ResourceType = typeof(EntityStrings), Name = "_Common_Id")]
            public int Id {
                get;
                set;
            }

            [RoundtripOriginal]
            public bool IsFisrt {
                get;
                set;
            }

            [RoundtripOriginal]
            public int LockLenght {
                get;
                set;
            }

            [RoundtripOriginal]
            public string LoginId {
                get;
                set;
            }

        }
    }

}
// ReSharper restore ConvertToStaticClass, ConvertNullableToShortForm, RedundantNameQualifier
