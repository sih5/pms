﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Security.Web.Resources;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        [RequiresAuthentication]
        public IQueryable<SecurityAction> GetSecurityActionsBy(int[] id, int pageId, string operationId, int status) {
            IQueryable<SecurityAction> result = this.ObjectContext.SecurityActions;
            if(id != null && id.Length > 0)
                result = result.Where(v => id.Contains(v.Id));
            if(pageId > 0)
                result = result.Where(r => r.PageId == pageId);
            if(!string.IsNullOrEmpty(operationId))
                result = result.Where(r => r.OperationId.ToLower().Contains(operationId.ToLower()));
            if(status >= 0)
                result = result.Where(r => r.Status == status);
            return result;
        }

        //新增
        [RequiresAuthentication]
        public void InsertSecurityAction(SecurityAction securityAction) {
            this.InsertToDatabase(securityAction);
        }

        //更新
        [RequiresAuthentication]
        public void UpdateSecurityAction(SecurityAction securityAction) {
            this.UpdateToDatabase(securityAction);
        }

        //作废窗体操作
        [Update(UsingCustomMethod = true)]
        [RequiresAuthentication]
        public void 作废窗体操作(SecurityAction securityAction) {
            //1、作废窗体操作，调用【窗体操作.更新窗体操作】，其中赋值：  1）状态 = 作废
            this.UpdateToDatabase(securityAction);
            securityAction.Status = (int)SecurityCommonStatus.作废;

            //2、根据目标Id = 当前窗体操作.窗体操作Id，查找并作废资源节点数据，调用【资源节点.更新资源节点】，其中赋值：  1）状态 = 作废
            var actionNode = this.ObjectContext.Nodes.SingleOrDefault(v => v.CategoryType == (int)SecurityNodeCategoryType.操作 && v.CategoryId == securityAction.Id);
            if(actionNode == null)
                throw new ValidationException(string.Format(ErrorStrings.SecurityAction_Validation1, securityAction.OperationId));
            actionNode.Status = (int)SecurityCommonStatus.作废;
            this.UpdateNodeValidate(actionNode);

            //3、根据节点Id = 当前窗体操作.窗体操作Id，删除授权规则，调用【授权规则.删除授权规则】
            var rules = this.ObjectContext.Rules.Where(v => v.NodeId == securityAction.Id).ToArray();
            foreach(var rule in rules)
                this.DeleteFromDatabase(rule);

            //4、调用【企业授权模板.查询企业授权模板】，依据：  1）企业授权模板资源清单.节点Id = 当前窗体操作.窗体操作Id
            //5、对于步骤4中查询到数据，删除清单中节点Id = 当前窗体操作.窗体操作Id的数据，然后调用【企业授权模板.更新企业授权模板】
            var entNodeTemplates = this.ObjectContext.EntNodeTemplates.Where(v => this.ObjectContext.EntNodeTemplateDetails.Any(detail => detail.NodeId == securityAction.Id && detail.EntNodeTemplateId == v.Id)).ToArray();
            foreach(var entNodeTemplate in entNodeTemplates) {
                var details = entNodeTemplate.EntNodeTemplateDetails.ToArray();
                foreach(var detail in details)
                    this.DeleteFromDatabase(detail);
            }
        }
    }
}
