﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Xml.Linq;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        private const string SECURITY_SYSTEM_PAGE_ID = "Security";
        private const string WARNING_SYSTEM_PAGE_ID = "Warning";
        private const string CHART_SYSTEM_PAGE_ID = "Chart";

        private IEnumerable<Node> GetUserNodes(Personnel user) {
            if(user == null)
                throw new ArgumentNullException("user");

            var entNodeTemplateDetailNodeIds = this.ObjectContext.EntNodeTemplateDetails.Where(detail => this.ObjectContext.Enterprises.Any(ent => ent.Id == user.EnterpriseId && ent.EntNodeTemplateId == detail.EntNodeTemplateId)
                && detail.EntNodeTemplate.Status == (int)SecurityCommonStatus.有效).Select(v => v.NodeId);

            return from roleUser in this.ObjectContext.RolePersonnels
                   from rule in this.ObjectContext.Rules
                   from node in this.ObjectContext.Nodes
                   where roleUser.RoleId == rule.RoleId && rule.NodeId == node.Id && roleUser.PersonnelId == user.Id
                   && entNodeTemplateDetailNodeIds.Contains(node.Id)
                   select node;
        }

        /*     private IEnumerable<Node> GetUserNodes(Personnel user) {
                 if(user == null)
                     throw new ArgumentNullException("user");

                 var entNodeTemplateDetailNodeIds = this.ObjectContext.EntNodeTemplateDetails.Where(detail => this.ObjectContext.Enterprises.Any(ent => ent.Id == user.EnterpriseId && ent.EntNodeTemplateId == detail.EntNodeTemplateId)
                     && detail.EntNodeTemplate.Status == (int)SecurityCommonStatus.有效).Select(v => v.NodeId);

                 return from roleUser in this.ObjectContext.RolePersonnels
                        from role in this.ObjectContext.Roles
                        from rule in this.ObjectContext.Rules
                        from node in this.ObjectContext.Nodes
                        where roleUser.RoleId == rule.RoleId && rule.NodeId == node.Id && roleUser.PersonnelId == user.Id
                        && role.Id == roleUser.RoleId && role.Status == (int)SecurityRoleStatus.有效
                        && entNodeTemplateDetailNodeIds.Contains(node.Id)
                        select node;
             } */

        private static XElement GetApplicationMenuElement(string type, Page page, CultureInfo cultureInfo) {
            var result = new XElement(type);
            result.Add(new XAttribute("Id", page.Id));
            result.Add(new XAttribute("PageId", page.PageId));
            var nameAttribute = new XAttribute("Name", page.Name);
            var descriptionAttribute = new XAttribute("Description", page.Description ?? string.Empty);
            if(cultureInfo != null) {
                switch(cultureInfo.Name) {
                    case "en-US":
                        if(!string.IsNullOrEmpty(page.Name_enUS))
                            nameAttribute.Value = page.Name_enUS;
                        if(!string.IsNullOrEmpty(page.Description_enUS))
                            descriptionAttribute.Value = page.Description_enUS;
                        break;
                    case "ja-JP":
                        if(!string.IsNullOrEmpty(page.Name_jaJP))
                            nameAttribute.Value = page.Name_jaJP;
                        if(!string.IsNullOrEmpty(page.Description_jaJP))
                            descriptionAttribute.Value = page.Description_jaJP;
                        break;
                }
            }
            result.Add(nameAttribute);
            if(!string.IsNullOrEmpty(descriptionAttribute.Value))
                result.Add(descriptionAttribute);
            if(!string.IsNullOrEmpty(page.Icon))
                result.Add(new XAttribute("Icon", page.Icon));
            switch(type) {
                case "Page":
                    if(!string.IsNullOrEmpty(page.PageType))
                        result.Add(new XAttribute("Type", page.PageType));
                    if(!string.IsNullOrEmpty(page.Parameter))
                        result.Add(new XAttribute("Parameter", page.Parameter));
                    break;
            }
            return result;
        }

        private static XElement GetFavoritePagesElement(Dictionary<int, int> pagesOrder) {
            var result = new XElement("FavoritePages");
            foreach(var pageOrder in pagesOrder)
                result.Add(new XElement("Page", new XAttribute("Id", pageOrder.Key), new XAttribute("Sequence", pageOrder.Value)));
            return result;
        }

        [RequiresAuthentication]
        public string GetMenuXml(string cultureName) {
            CultureInfo cultureInfo = CultureInfo.GetCultureInfo("zh-CHS");
            if(!string.IsNullOrEmpty(cultureName))
                try {
                    cultureInfo = CultureInfo.GetCultureInfo(cultureName);
                } catch(Exception) {
                    //
                }

            var eAppMenus = new XElement("AppMenus");

            var userId = this.GetCurrentUserInfo().Id;
            var user = this.ObjectContext.Personnels.SingleOrDefault(u => u.Id == userId);
            if(user == null)
                return eAppMenus.ToString(SaveOptions.DisableFormatting);

            var appMenus = this.ObjectContext.Pages.Where(p => p.Status == (int)SecurityCommonStatus.有效).ToArray();
            var pageIds = Enumerable.Empty<int>();
            var nodes = this.GetUserNodes(user).ToArray();
            if(nodes.Length > 0) {
                //获取被授权的流程节点、分组及系统
                var excludedSystems = new[] {
                        WARNING_SYSTEM_PAGE_ID, CHART_SYSTEM_PAGE_ID
                    };
                pageIds = nodes.Where(n => n.CategoryType == (int)SecurityNodeCategoryType.页面).Select(n => n.CategoryId).Distinct();
                var authorizedPages = appMenus.Where(p => pageIds.Contains(p.Id)).ToArray();
                var groupIds = authorizedPages.Select(p => p.ParentId).Distinct();
                var authorizedGroups = appMenus.Where(g => g.Type == (int)SecurityPageType.分组 && groupIds.Contains(g.Id)).ToArray();
                var systemIds = authorizedGroups.Select(g => g.ParentId).Distinct();
                var authorizedSystems = appMenus.Where(s => s.Type == (int)SecurityPageType.系统 && !excludedSystems.Contains(s.PageId) && systemIds.Contains(s.Id))
                                                .OrderBy(s => s.Sequence).ToArray();
                foreach(var system in authorizedSystems) {
                    var parentId = system.Id;
                    var systemElement = GetApplicationMenuElement("System", system, cultureInfo);
                    var groups = authorizedGroups.Where(g => g.ParentId == parentId).OrderBy(g => g.Sequence);
                    foreach(var group in groups) {
                        var parentId1 = group.Id;
                        var groupElement = GetApplicationMenuElement("Group", group, cultureInfo);
                        var pages = authorizedPages.Where(p => p.ParentId == parentId1).OrderBy(p => p.Sequence);
                        foreach(var pageElement in pages.Select(p => GetApplicationMenuElement("Page", p, cultureInfo)))
                            groupElement.Add(pageElement);
                        if(groupElement.HasElements)
                            systemElement.Add(groupElement);
                    }
                    if(systemElement.HasElements)
                        eAppMenus.Add(systemElement);
                }
            }
            var favoritePages = this.ObjectContext.FavoritePages.Where(fav => fav.PersonnelId == userId && pageIds.Contains(fav.PageId))
                                                                    .ToDictionary(fav => fav.PageId, fav => fav.Sequence);
            eAppMenus.Add(GetFavoritePagesElement(favoritePages));

            return eAppMenus.ToString(SaveOptions.DisableFormatting | SaveOptions.OmitDuplicateNamespaces);
        }

        [RequiresAuthentication]
        public string[] GetAuthorizedPageActions(int pageId) {
            var userId = this.GetCurrentUserInfo().Id;
            var user = this.ObjectContext.Personnels.SingleOrDefault(u => u.Id == userId);
            if(user == null)
                return null;

            var actions = this.ObjectContext.SecurityActions.Where(a => a.PageId == pageId).ToArray();
            if(actions.Length == 0)
                return new string[0];

            if(user.RolePersonnels.Any(ru => ru != null && ru.Role != null && ru.Role.IsAdmin)) {
                var pageIds = this.ObjectContext.Pages.Where(p => p.Id == pageId).Select(v => v.ParentId);
                var groupIds = this.ObjectContext.Pages.Where(p => pageIds.Contains(p.Id)).Select(v => v.ParentId);
                var system = this.ObjectContext.Pages.SingleOrDefault(p => groupIds.Contains(p.Id) && p.ParentId == 0 && p.Type == (int)SecurityPageType.系统 && p.PageId == "Security");
                if(system != null)
                    return actions.Select(a => a.OperationId).ToArray();
            }
            var nodes = this.GetUserNodes(user).ToArray();
            return (from n in nodes
                    from a in actions
                    where n.CategoryType == (int)SecurityNodeCategoryType.操作 && a.Id == n.CategoryId
                    select a.OperationId).Distinct().ToArray();
        }

        [RequiresAuthentication]
        public int[] GetAuthorizedPageIdsByRoleId(int roleId) {
            return (from r in this.ObjectContext.Rules
                    where r.RoleId == roleId && r.Node.CategoryType == (int)SecurityNodeCategoryType.页面 && r.Node.Status == (int)SecurityCommonStatus.有效
                    select r.Node.CategoryId).ToArray();
        }

        [RequiresAuthentication]
        public string GetRoleAuthorizationDataForPage(int roleId, int pageId) {
            var authorized = (from r in this.ObjectContext.Rules
                              where r.RoleId == roleId && r.Node.CategoryType == (int)SecurityNodeCategoryType.页面 && r.Node.CategoryId == pageId && r.Node.Status == (int)SecurityCommonStatus.有效
                              select r).Any();
            if(!authorized)
                return null;

            var eActions = new XElement("Actions");
            var actionIds = (from n in this.ObjectContext.Nodes
                             from r in this.ObjectContext.Rules
                             where n.CategoryType == (int)SecurityNodeCategoryType.操作 && n.Status == (int)SecurityCommonStatus.有效 && r.RoleId == roleId && r.NodeId == n.Id
                             select n.CategoryId).ToArray();
            var actions = from a in this.ObjectContext.SecurityActions
                          where a.PageId == pageId && actionIds.Contains(a.Id) && a.Status == (int)SecurityCommonStatus.有效
                          select a;
            foreach(var action in actions)
                eActions.Add(new XElement("Action", action.OperationId));

            var eAuthorizationData = new XElement("AuthorizationData", eActions);
            return eAuthorizationData.ToString(SaveOptions.DisableFormatting | SaveOptions.OmitDuplicateNamespaces);
        }

        [RequiresAuthentication]
        public void SetRoleAuthorizationData(string dataXml) {
            var eAuthorizationData = XElement.Parse(dataXml);
            foreach(var eRole in eAuthorizationData.Elements("Role")) {
                var aRoleId = eRole.Attribute("Id");
                if(aRoleId == null)
                    continue;

                var roleId = int.Parse(aRoleId.Value);
                var allowedPageIds = eRole.Elements("Page").Where(e => {
                    var eAllowAccess = e.Attribute("AllowAccess");
                    return eAllowAccess != null && eAllowAccess.Value == "true";
                }).Select(e => {
                    var aPageId = e.Attribute("Id");
                    return aPageId == null ? 0 : int.Parse(aPageId.Value);
                }).Distinct().ToArray();

                /**************************************************************************************/

                //添加不存在的Node
                var nodesToAdd = allowedPageIds.Where(id => !this.ObjectContext.Nodes.Where(n => n.CategoryType == (int)SecurityNodeCategoryType.页面 && n.Status == (int)SecurityCommonStatus.有效).Select(n => n.CategoryId).Contains(id)).Select(id => new Node {
                    CategoryType = (int)SecurityNodeCategoryType.页面,
                    CategoryId = id,
                }).ToArray();
                foreach(var node in nodesToAdd)
                    this.InsertToDatabase(node);
                this.ObjectContext.SaveChanges();
                //删除不在提交集合里的Rule
                var rulesToDelete = this.ObjectContext.Rules.Where(r => r.RoleId == roleId && r.Node.CategoryType == (int)SecurityNodeCategoryType.页面 && !allowedPageIds.Contains(r.Node.CategoryId) && r.Node.Status == (int)SecurityCommonStatus.有效);
                foreach(var rule in rulesToDelete)
                    this.DeleteFromDatabase(rule);
                //添加提交集合里不存在的Rule
                var allowedNodeIds = this.ObjectContext.Nodes.Where(n => n.CategoryType == (int)SecurityNodeCategoryType.页面 && n.Status == (int)SecurityCommonStatus.有效 && allowedPageIds.Contains(n.CategoryId)).Select(n => n.Id).ToArray();
                var rulesToAdd = allowedNodeIds.Where(id => !this.ObjectContext.Rules.Where(r => r.RoleId == roleId).Select(r => r.Id).Contains(id)).Select(id => new Rule {
                    RoleId = roleId,
                    NodeId = id,
                });
                foreach(var rule in rulesToAdd)
                    this.InsertToDatabase(rule);
                this.ObjectContext.SaveChanges();

                /**************************************************************************************/

                // ReSharper disable PossibleNullReferenceException
                var pagesWithAction = eRole.Elements("Page").Where(e => e.Element("Actions") != null).Select(e => new {
                    Id = int.Parse(e.Attribute("Id").Value),
                    Actions = e.Element("Actions").Elements("Id").Select(e1 => e1.Value).ToArray(),
                }).ToArray();
                // ReSharper restore PossibleNullReferenceException
                //添加不存在的Action
                var actionsToAdd = new List<SecurityAction>();
                foreach(var pageWithAction in pagesWithAction) {
                    var pageId = pageWithAction.Id;
                    var currentActionIds = this.ObjectContext.SecurityActions.Where(sa => sa.PageId == pageId).Select(sa => sa.OperationId).ToArray();
                    actionsToAdd.AddRange(pageWithAction.Actions.Where(action => !currentActionIds.Contains(action)).Select(action => new SecurityAction {
                        PageId = pageId,
                        OperationId = action,
                    }));
                }
                foreach(var action in actionsToAdd)
                    this.InsertToDatabase(action);
                this.ObjectContext.SaveChanges();

                foreach(var allowedActionIds in from pageWithAction in pagesWithAction
                                                let pageId = pageWithAction.Id
                                                let pageActions = pageWithAction.Actions
                                                select this.ObjectContext.SecurityActions.Where(sa => sa.PageId == pageId && pageActions.Contains(sa.OperationId)).Select(sa => sa.Id).ToArray()) {
                    // ReSharper disable AccessToModifiedClosure
                    //添加不存在的Node
                    nodesToAdd = allowedActionIds.Where(id => !this.ObjectContext.Nodes.Where(n => n.CategoryType == (int)SecurityNodeCategoryType.操作 && n.Status == (int)SecurityCommonStatus.有效).Select(n => n.CategoryId).Contains(id)).Select(id => new Node {
                        CategoryType = (int)SecurityNodeCategoryType.操作,
                        CategoryId = id,
                    }).ToArray();
                    foreach(var node in nodesToAdd)
                        this.InsertToDatabase(node);
                    //删除不在提交集合里的Rule
                    rulesToDelete = this.ObjectContext.Rules.Where(r => r.RoleId == roleId && r.Node.CategoryType == (int)SecurityNodeCategoryType.操作 && !allowedActionIds.Contains(r.Node.CategoryId) && r.Node.Status == (int)SecurityCommonStatus.有效);
                    foreach(var rule in rulesToDelete)
                        this.DeleteFromDatabase(rule);
                    //添加提交集合里不存在的Rule
                    allowedNodeIds = this.ObjectContext.Nodes.Where(n => n.CategoryType == (int)SecurityNodeCategoryType.操作 && n.Status == (int)SecurityCommonStatus.有效 && allowedActionIds.Contains(n.CategoryId)).Select(n => n.Id).ToArray();
                    rulesToAdd = allowedNodeIds.Where(id => !this.ObjectContext.Rules.Where(r => r.RoleId == roleId).Select(r => r.Id).Contains(id)).Select(id => new Rule {
                        RoleId = roleId,
                        NodeId = id,
                    });
                    foreach(var rule in rulesToAdd)
                        this.InsertToDatabase(rule);
                    // ReSharper restore AccessToModifiedClosure
                }
                this.ObjectContext.SaveChanges();
            }
        }

        private static XElement GetCurrentUserInfoXml() {
            var result = new XElement("AuthorizeUser", new XAttribute("UserId", "@UserId"), new XAttribute("UserCode", "@UserCode"), new XAttribute("UserName", "@UserName"), new XAttribute("EnterpriseId", "@EnterpriseId"), new XAttribute("EnterpriseCode", "@EnterpriseCode"), new XAttribute("EnterpriseName", "@EnterpriseName"), new XAttribute("EnterpriseType", "@EnterpriseType"));

            return result;
        }

        public string GetMenuXmlForPDA(int userId, string cultureName = "zh-CHS") {
            CultureInfo cultureInfo = CultureInfo.GetCultureInfo("zh-CHS");
            if(!string.IsNullOrEmpty(cultureName))
                try {
                    cultureInfo = CultureInfo.GetCultureInfo(cultureName);
                } catch(Exception) {
                    //
                }
            var eAppMenus = new XElement("AppMenus");

            var user = this.ObjectContext.Personnels.SingleOrDefault(u => u.Id == userId);
            if(user == null)
                return eAppMenus.ToString(SaveOptions.DisableFormatting);

            eAppMenus.Add(GetCurrentUserInfoXml());

            var appMenus = this.ObjectContext.Pages.Where(p => p.Status == (int)SecurityCommonStatus.有效 && p.PageSystem == (int)SecurityPageSystem.PDA).ToArray();
            //var appMenus = this.ObjectContext.Pages.Where(p => p.Status == (int)SecurityCommonStatus.有效 && p.PageId.IndexOf("BQ") >= 0).ToArray();
            IEnumerable<int> pageIds;
            var nodes = this.GetUserNodes(user).ToArray();
            if(nodes.Length > 0) {
                //获取被授权的流程节点、分组及系统
                var excludedSystems = new[]  {
                                                WARNING_SYSTEM_PAGE_ID,  CHART_SYSTEM_PAGE_ID
                                        };
                pageIds = nodes.Where(n => n.CategoryType == (int)SecurityNodeCategoryType.页面).Select(n => n.CategoryId).Distinct();
                var authorizedPages = appMenus.Where(p => pageIds.Contains(p.Id)).ToArray();
                var groupIds = authorizedPages.Select(p => p.ParentId).Distinct();
                var authorizedGroups = appMenus.Where(g => g.Type == (int)SecurityPageType.分组 && groupIds.Contains(g.Id)).ToArray();
                var systemIds = authorizedGroups.Select(g => g.ParentId).Distinct();
                var authorizedSystems = appMenus.Where(s => s.Type == (int)SecurityPageType.系统 && !excludedSystems.Contains(s.PageId) && systemIds.Contains(s.Id))
                                                                                .OrderBy(s => s.Sequence).ToArray();
                foreach(var system in authorizedSystems) {
                    var parentId = system.Id;
                    var systemElement = GetApplicationMenuElement("System", system, cultureInfo);
                    var groups = authorizedGroups.Where(g => g.ParentId == parentId).OrderBy(g => g.Sequence);
                    foreach(var group in groups) {
                        var parentId1 = group.Id;
                        var groupElement = GetApplicationMenuElement("Group", group, cultureInfo);
                        var pages = authorizedPages.Where(p => p.ParentId == parentId1).OrderBy(p => p.Sequence);
                        foreach(var pageElement in pages.Select(p => GetApplicationMenuElement("Page", p, cultureInfo)))
                            groupElement.Add(pageElement);
                        if(groupElement.HasElements)
                            systemElement.Add(groupElement);
                    }
                    if(systemElement.HasElements)
                        eAppMenus.Add(systemElement);
                }
            }

            return eAppMenus.ToString(SaveOptions.DisableFormatting | SaveOptions.OmitDuplicateNamespaces);
        }
    }
}
