﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Security.Web.Resources;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        [RequiresAuthentication]
        public IQueryable<EntNodeTemplate> GetEntNodeTemplatesBy(int id, string code, string name, int status, int nodeId) {
            IQueryable<EntNodeTemplate> result = this.ObjectContext.EntNodeTemplates;
            if(id > 0)
                result = result.Where(r => r.Id == id);
            if(!string.IsNullOrEmpty(code))
                result = result.Where(r => r.Code.ToLower().Contains(code.ToLower()));
            if(!string.IsNullOrEmpty(name))
                result = result.Where(r => r.Name.ToLower().Contains(name.ToLower()));
            if(status >= 0)
                result = result.Where(r => r.Status == status);
            if(nodeId > 0)
                result = result.Where(v => this.ObjectContext.EntNodeTemplateDetails.Any(detail => detail.NodeId == nodeId && detail.EntNodeTemplateId == v.Id));
            return result.Include("EntNodeTemplateDetails").Include("Pages").Include("Actions"); //todo: .Include("AuthorizedColumns")
        }

        //新增企业授权模板
        [RequiresAuthentication]
        public void InsertEntNodeTemplate(EntNodeTemplate entNodeTemplate) {
            //在ObjectContext中登记，指明主单将要新增至数据库
            this.InsertToDatabase(entNodeTemplate);

            //获取主单所关联的清单实体对象，并指明将清单新增至数据库
            var entNodeTemplateDetails = this.ChangeSet.GetAssociatedChanges(entNodeTemplate, o => o.EntNodeTemplateDetails, ChangeOperation.Insert);
            foreach(EntNodeTemplateDetail entNodeTemplateDetail in entNodeTemplateDetails)
                this.InsertToDatabase(entNodeTemplateDetail);

            //校验实体对象并做默认处理
            this.InsertEntNodeTemplateValidate(entNodeTemplate);
        }

        //针对新增操作的数据校验及默认操作
        private void InsertEntNodeTemplateValidate(EntNodeTemplate entNodeTemplate) {
            //业务规则检查1：数据库中Code必须唯一
            var conflictEntNodeTemplate = this.ObjectContext.EntNodeTemplates.Where(a => a.Code.ToLower() == entNodeTemplate.Code.ToLower() && a.Status == (int)SecurityCommonStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(conflictEntNodeTemplate != null)
                //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
                //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
                throw new ValidationException(string.Format(ErrorStrings.EntNodeTemplate_Validation1, entNodeTemplate.Code)); //数据库中已经包含编号为“{0}”的企业授权模板

            //业务规则检查2：数据库中Name必须唯一
            var conflictEntNodeTemplateName = this.ObjectContext.EntNodeTemplates.Where(a => a.Name.ToLower() == entNodeTemplate.Name.ToLower() && a.Status == (int)SecurityCommonStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(conflictEntNodeTemplateName != null)
                //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
                //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
                throw new ValidationException(string.Format(ErrorStrings.EntNodeTemplate_Validation2, entNodeTemplate.Name)); //数据库中已经包含名称为“{0}”的企业授权模板

            //更新创建人等数据
            var userInfo = this.GetCurrentUserInfo();
            entNodeTemplate.EnterpriseId = userInfo.EnterpriseId;
            entNodeTemplate.CreatorId = userInfo.Id;
            entNodeTemplate.CreatorName = userInfo.Name;
            entNodeTemplate.CreateTime = DateTime.Now;
        }

        //更新企业授权模板
        [RequiresAuthentication]
        public void UpdateEntNodeTemplate(EntNodeTemplate entNodeTemplate) {
            //在ObjectContext中登记，指明主单将要保存至数据库，注意该步骤不会立刻去UPDATE数据库
            entNodeTemplate.EntNodeTemplateDetails.Clear();
            this.UpdateToDatabase(entNodeTemplate);

            //获取主单所关联的清单实体对象
            var entNodeTemplateDetails = this.ChangeSet.GetAssociatedChanges(entNodeTemplate, o => o.EntNodeTemplateDetails);

            foreach(EntNodeTemplateDetail entNodeTemplateDetail in entNodeTemplateDetails) //获取清单实体对象的操作类型
                switch(this.ChangeSet.GetChangeOperation(entNodeTemplateDetail)) {
                    //新增操作
                    case ChangeOperation.Insert:
                        this.InsertToDatabase(entNodeTemplateDetail);
                        break;
                    //更新操作
                    case ChangeOperation.Update:
                        this.UpdateToDatabase(entNodeTemplateDetail);
                        break;
                    //删除操作
                    case ChangeOperation.Delete:
                        this.DeleteFromDatabase(entNodeTemplateDetail);
                        break;
                }

            //校验实体对象并做默认处理
            this.UpdateEntNodeTemplateValidate(entNodeTemplate);
        }

        //针对更新操作的数据校验及默认操作
        private void UpdateEntNodeTemplateValidate(EntNodeTemplate entNodeTemplate) {
            var dbEntNodeTemplate = this.ObjectContext.EntNodeTemplates.Where(v => v.Id == entNodeTemplate.Id && v.Status == (int)SecurityCommonStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();

            //业务规则检查1：只允许更新“有效”状态的企业授权模板
            if(dbEntNodeTemplate == null)
                //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
                //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
                throw new ValidationException(ErrorStrings.EntNodeTemplate_Validation); //只允许更新“有效”状态的企业授权模板

            //业务规则检查2：数据库中Name必须唯一
            if(string.Compare(dbEntNodeTemplate.Name, entNodeTemplate.Name, StringComparison.OrdinalIgnoreCase) != 0) {
                var conflictEntNodeTemplate = this.ObjectContext.EntNodeTemplates.Where(a => a.Name.ToLower() == entNodeTemplate.Name.ToLower() && a.Status == (int)SecurityCommonStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                if(conflictEntNodeTemplate != null)
                    throw new ValidationException(string.Format(ErrorStrings.EntNodeTemplate_Validation2, entNodeTemplate.Name)); //数据库中已经包含名称为“{0}”的企业授权模板
            }

            //合法性校验：没有被有效和冻结状态的企业使用，才能被作废
            var result = this.ObjectContext.Enterprises.Where(v => v.EntNodeTemplateId == entNodeTemplate.Id && (v.Status == (int)SecurityEnterpriseStatus.冻结 || v.Status == (int)SecurityEnterpriseStatus.生效));
            if(entNodeTemplate.Status == (int)SecurityCommonStatus.作废 && result.Any())
                throw new ValidationException(ErrorStrings.EntNodeTemplate_Validation3);

            //更新修改人等数据
            var userInfo = this.GetCurrentUserInfo();
            entNodeTemplate.ModifierId = userInfo.Id;
            entNodeTemplate.ModifierName = userInfo.Name;
            entNodeTemplate.ModifyTime = DateTime.Now;
        }

        public IQueryable<EntNodeTemplate> GetEntNodeTemplatesWithDetailById(int id) {
            return this.ObjectContext.EntNodeTemplates.Where(v => v.Id == id).Include("EntNodeTemplateDetails").Include("EntNodeTemplateDetails.Node");
        }
    }
}
