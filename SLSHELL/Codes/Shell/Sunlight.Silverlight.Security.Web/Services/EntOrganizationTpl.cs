﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Security.Web.Resources;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        [RequiresAuthentication]
        public IQueryable<EntOrganizationTpl> GetEnterpriseOrgaizationTemplatesBy(int id, string code, string name, int status) {
            IQueryable<EntOrganizationTpl> result = this.ObjectContext.EntOrganizationTpls;
            if(id > 0)
                result = result.Where(r => r.Id == id);
            if(!string.IsNullOrEmpty(code))
                result = result.Where(r => r.Code.ToLower().Contains(code.ToLower()));
            if(!string.IsNullOrEmpty(name))
                result = result.Where(r => r.Name.ToLower().Contains(name.ToLower()));
            if(status >= 0)
                result = result.Where(r => r.Status == status);
            return result.Include("EntOrganizationTplDetails");
        }

        public IQueryable<EntOrganizationTpl> GetEntOrganizationTplsWithDetail() {
            return this.ObjectContext.EntOrganizationTpls.Include("EntOrganizationTplDetails");
        }

        //新增企业组织模板
        [RequiresAuthentication]
        public void InsertEnterpriseOrgaizationTemplate(EntOrganizationTpl entOrganizationTpl) {
            //在ObjectContext中登记，指明主单将要新增至数据库
            this.InsertToDatabase(entOrganizationTpl);

            //获取主单所关联的清单实体对象，并指明将清单新增至数据库
            var entOrganizationTplDetails = this.ChangeSet.GetAssociatedChanges(entOrganizationTpl, o => o.EntOrganizationTplDetails, ChangeOperation.Insert);
            foreach(EntOrganizationTplDetail entOrganizationTplDetail in entOrganizationTplDetails)
                this.InsertToDatabase(entOrganizationTplDetail);

            //校验实体对象并做默认处理
            this.InsertEntOrganizationTplValidate(entOrganizationTpl);
        }

        //针对新增操作的数据校验及默认操作
        private void InsertEntOrganizationTplValidate(EntOrganizationTpl entOrganizationTpl) {
            //业务规则检查1：数据库中Code必须唯一
            var conflictEntOrganizationTpl = this.ObjectContext.EntOrganizationTpls.Where(a => a.Code.ToLower() == entOrganizationTpl.Code.ToLower() && a.Status == (int)SecurityCommonStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(conflictEntOrganizationTpl != null)
                //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
                //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
                throw new ValidationException(string.Format(ErrorStrings.EntOrganizationTpl_Validation1, entOrganizationTpl.Code)); //数据库中已经包含编号为“{0}”的企业组织模板

            //业务规则检查2：数据库中Name必须唯一
            var conflictEntOrganizationTplName = this.ObjectContext.EntOrganizationTpls.Where(a => a.Name.ToLower() == entOrganizationTpl.Name.ToLower() && a.Status == (int)SecurityCommonStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(conflictEntOrganizationTplName != null)
                //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
                //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
                throw new ValidationException(string.Format(ErrorStrings.EntOrganizationTpl_Validation2, entOrganizationTpl.Name)); //数据库中已经包含名称为“{0}”的企业组织模板

            //更新创建人等数据
            var userInfo = this.GetCurrentUserInfo();
            entOrganizationTpl.EnterpriseId = userInfo.EnterpriseId;
            entOrganizationTpl.CreatorId = userInfo.Id;
            entOrganizationTpl.CreatorName = userInfo.Name;
            entOrganizationTpl.CreateTime = DateTime.Now;
        }

        //更新企业组织模板
        [RequiresAuthentication]
        public void UpdateEntOrganizationTpl(EntOrganizationTpl entOrganizationTpl) {
            entOrganizationTpl.EntOrganizationTplDetails.Clear();
            //在ObjectContext中登记，指明主单将要保存至数据库，注意该步骤不会立刻去UPDATE数据库
            this.UpdateToDatabase(entOrganizationTpl);

            //获取主单所关联的清单实体对象
            var entOrganizationTplDetails = this.ChangeSet.GetAssociatedChanges(entOrganizationTpl, o => o.EntOrganizationTplDetails);

            foreach(EntOrganizationTplDetail entOrganizationTplDetail in entOrganizationTplDetails) //获取清单实体对象的操作类型
                switch(this.ChangeSet.GetChangeOperation(entOrganizationTplDetail)) {
                    //新增操作
                    case ChangeOperation.Insert:
                        this.InsertToDatabase(entOrganizationTplDetail);
                        break;
                    //更新操作
                    case ChangeOperation.Update:
                        this.UpdateToDatabase(entOrganizationTplDetail);
                        break;
                    //删除操作
                    case ChangeOperation.Delete:
                        this.DeleteFromDatabase(entOrganizationTplDetail);
                        break;
                }

            //校验实体对象并做默认处理
            this.UpdateEntOrganizationTplValidate(entOrganizationTpl);
        }

        //针对更新操作的数据校验及默认操作
        private void UpdateEntOrganizationTplValidate(EntOrganizationTpl entOrganizationTpl) {
            var dbEntOrganizationTpl = this.ObjectContext.EntOrganizationTpls.Where(v => v.Id == entOrganizationTpl.Id && v.Status == (int)SecurityCommonStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();

            //业务规则检查1：只允许更新“有效”状态的企业组织模板
            if(dbEntOrganizationTpl == null)
                //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
                //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
                throw new ValidationException(ErrorStrings.EntOrganizationTpl_Validation); //只允许更新“有效”状态的企业组织模板

            //业务规则检查2：数据库中Name必须唯一
            if(string.Compare(dbEntOrganizationTpl.Name, entOrganizationTpl.Name, StringComparison.OrdinalIgnoreCase) != 0) {
                var conflictEntOrganizationTpl = this.ObjectContext.EntOrganizationTpls.Where(a => a.Name.ToLower() == entOrganizationTpl.Name.ToLower() && a.Status == (int)SecurityCommonStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                if(conflictEntOrganizationTpl != null)
                    throw new ValidationException(string.Format(ErrorStrings.EntOrganizationTpl_Validation2, entOrganizationTpl.Name)); //数据库中已经包含名称为“{0}”的企业组织模板
            }

            //更新修改人等数据
            var userInfo = this.GetCurrentUserInfo();
            entOrganizationTpl.ModifierId = userInfo.Id;
            entOrganizationTpl.ModifierName = userInfo.Name;
            entOrganizationTpl.ModifyTime = DateTime.Now;
        }
    }
}
