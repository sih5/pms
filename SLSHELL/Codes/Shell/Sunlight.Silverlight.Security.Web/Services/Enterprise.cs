﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Security.Web.Resources;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        [RequiresAuthentication]
        public IQueryable<Enterprise> GetEnterprisesBy(int id, string code, string name, int entNodeTemplatedId, int[] status) {
            IQueryable<Enterprise> result = this.ObjectContext.Enterprises;
            if(id > 0)
                result = result.Where(r => r.Id == id);
            if(!string.IsNullOrEmpty(code))
                result = result.Where(r => r.Code.ToLower().Contains(code.ToLower()));
            if(!string.IsNullOrEmpty(name))
                result = result.Where(r => r.Name.ToLower().Contains(name.ToLower()));
            if(status != null && status.Length > 0)
                result = result.Where(v => status.Contains(v.Status));
            if(entNodeTemplatedId > 0)
                result = result.Where(r => r.EntNodeTemplateId == entNodeTemplatedId);
            return result.Include("EntNodeTemplate");
        }

        public IQueryable<Enterprise> GetEnterprisesWithCategory() {
            return this.ObjectContext.Enterprises.Include("EnterpriseCategory").OrderBy(e => e.Id);
        }

        public IQueryable<Enterprise> GetEnterprisesByPersonnel(string personnelName, string personnelLoginId) {
            return this.ObjectContext.Enterprises.Include("EnterpriseCategory").Where(r => ObjectContext.Personnels.Any(e => e.EnterpriseId == r.Id && e.Name.Contains(personnelName) && e.LoginId.Contains(personnelLoginId))).OrderBy(e => e.Id);
        }


        public IQueryable<Enterprise> GetEnterprisesWithDetails() {
            return this.ObjectContext.Enterprises.Include("EnterpriseCategory").Include("EntNodeTemplate").OrderBy(e => e.Id);
        }

        public IQueryable<Enterprise> GetEnterprisesByIds(int[] ids) {
            return this.ObjectContext.Enterprises.Where(r => ids.Contains(r.Id));
        }

        //新增企业
        [RequiresAuthentication]
        public void InsertEnterprise(Enterprise enterprise) {
            //在ObjectContext中登记，指明主单将要新增至数据库
            this.InsertToDatabase(enterprise);

            //校验实体对象并做默认处理
            this.InsertEnterpriseValidate(enterprise);
        }

        //针对新增操作的数据校验及默认操作
        private void InsertEnterpriseValidate(Enterprise enterprise) {
            //业务规则检查1：数据库中Code必须唯一
            var conflictEnterprise = this.ObjectContext.Enterprises.Where(a => a.Code.ToLower() == enterprise.Code.ToLower() && a.Status != (int)SecurityEnterpriseStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(conflictEnterprise != null)
                //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
                //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
                throw new ValidationException(string.Format(ErrorStrings.Enterprise_Validation1, enterprise.Code)); //数据库中已经包含编号为“{0}”的企业

            //业务规则检查2：数据库中Name必须唯一
            //var conflictEnterpriseName = this.ObjectContext.Enterprises.Where(a => a.Name.ToLower() == enterprise.Name.ToLower() && a.Status != (int)SecurityEnterpriseStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            //if(conflictEnterpriseName != null)
            //    //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
            //    //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
            //    throw new ValidationException(string.Format(ErrorStrings.Enterprise_Validation2, enterprise.Name)); //数据库中已经包含名称为“{0}”的企业

            enterprise.Id = 1 + this.ObjectContext.Enterprises.Select(v => v.Id).Max();

            //更新创建人等数据
            var userInfo = this.GetCurrentUserInfo();
            enterprise.CreatorId = userInfo.Id;
            enterprise.CreatorName = userInfo.Name;
            enterprise.CreateTime = DateTime.Now;
        }

        //更新企业
        [RequiresAuthentication]
        public void UpdateEnterprise(Enterprise enterprise) {
            //在ObjectContext中登记，指明主单将要保存至数据库，注意该步骤不会立刻去UPDATE数据库
            this.UpdateToDatabase(enterprise);

            //校验实体对象并做默认处理
            this.UpdateEnterpriseValidate(enterprise);
        }

        //针对更新操作的数据校验及默认操作
        private void UpdateEnterpriseValidate(Enterprise enterprise) {
            var dbEnterprise = this.ObjectContext.Enterprises.Where(v => v.Id == enterprise.Id && v.Status != (int)SecurityEnterpriseStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();

            //业务规则检查1：只允许更新“有效”状态的企业
            if(dbEnterprise == null)
                //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
                //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
                throw new ValidationException(ErrorStrings.Enterprise_Validation); //只允许更新“有效”状态的企业

            //业务规则检查2：数据库中Code必须唯一
            if(string.Compare(dbEnterprise.Code, enterprise.Code, StringComparison.OrdinalIgnoreCase) != 0) {
                var conflictEnterprise = this.ObjectContext.Enterprises.Where(a => a.Code.ToLower() == enterprise.Code.ToLower() && a.Status != (int)SecurityEnterpriseStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                if(conflictEnterprise != null)
                    throw new ValidationException(string.Format(ErrorStrings.Enterprise_Validation1, enterprise.Code)); //数据库中已经包含名称为“{0}”的企业
            }

            //业务规则检查3：数据库中Name必须唯一
            //if(string.Compare(dbEnterprise.Name, enterprise.Name, StringComparison.OrdinalIgnoreCase) != 0) {
            //    var conflictEnterprise = this.ObjectContext.Enterprises.Where(a => a.Name.ToLower() == enterprise.Name.ToLower() && a.Status != (int)SecurityEnterpriseStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            //    if(conflictEnterprise != null)
            //        throw new ValidationException(string.Format(ErrorStrings.Enterprise_Validation2, enterprise.Name)); //数据库中已经包含名称为“{0}”的企业
            //}

            //更新修改人等数据
            var userInfo = this.GetCurrentUserInfo();
            enterprise.ModifierId = userInfo.Id;
            enterprise.ModifierName = userInfo.Name;
            enterprise.ModifyTime = DateTime.Now;
        }

        //根据组织模板，递归生成下级企业
        private void GenerateOrganization(Enterprise enterprise, Organization parentOrganization, EntOrganizationTplDetail parentEntOrganizationTplDetail, List<EntOrganizationTplDetail> conflicts) {
            foreach(var childEntOrganizationTplDetail in parentEntOrganizationTplDetail.ChildEntOrganizationTplDetails) {
                if(conflicts.Contains(childEntOrganizationTplDetail))
                    throw new ValidationException(ErrorStrings.Enterprise_Validation6);
                conflicts.Add(childEntOrganizationTplDetail);
                var childOrganization = new Organization {
                    Code = childEntOrganizationTplDetail.OrganizationCode,
                    Name = childEntOrganizationTplDetail.OrganizationName,
                    Type = childEntOrganizationTplDetail.OrganizationType,
                    Sequence = childEntOrganizationTplDetail.Sequence,
                    Status = (int)SecurityCommonStatus.有效
                };
                enterprise.Organizations.Add(childOrganization);
                parentOrganization.ChildOrganizations.Add(childOrganization);
                this.InsertOrganizationValidate(childOrganization);
                GenerateOrganization(enterprise, childOrganization, childEntOrganizationTplDetail, conflicts);
            }
        }

        //根据角色模板Id，为指定企业生成角色
        private void GenerateRoleByTemplate(Enterprise enterprise, int[] roleTemplateIds) {
            if(!enterprise.EntNodeTemplateId.HasValue)
                throw new ValidationException(ErrorStrings.Enterprise_Validation5);

            roleTemplateIds = roleTemplateIds.Distinct().ToArray();
            var roleTemplates = this.ObjectContext.RoleTemplates.Where(v => roleTemplateIds.Contains(v.Id)).ToArray();
            if(roleTemplateIds.Length != roleTemplates.Length)
                throw new ValidationException(ErrorStrings.Enterprise_Validation4);

            if(this.ObjectContext.RoleTemplateRules.Any(v => roleTemplateIds.Contains(v.RoleTemplateId) && this.ObjectContext.EntNodeTemplateDetails.All(v1 => v1.EntNodeTemplateId == enterprise.EntNodeTemplateId.Value && v1.NodeId != v.NodeId)))
                throw new ValidationException(ErrorStrings.Enterprise_Validation3);

            if(enterprise.EntityState != EntityState.Added && this.ObjectContext.RoleTemplates.Any(v => roleTemplateIds.Contains(v.Id) && this.ObjectContext.Roles.Any(role => role.Name.ToLower() == v.Name.ToLower() && role.EnterpriseId == enterprise.Id && role.Status == (int)SecurityRoleStatus.有效)))
                throw new ValidationException(ErrorStrings.Enterprise_Validation9);

            //将模板对应授权规则取到内存中
            var roleTemplateRules = this.ObjectContext.RoleTemplateRules.Where(v => roleTemplateIds.Contains(v.RoleTemplateId)).ToArray();
            foreach(var roleTemplate in roleTemplates) {
                var role = new Role {
                    Name = roleTemplate.Name,
                    IsAdmin = false,
                    Status = (int)SecurityRoleStatus.有效
                };
                enterprise.Roles.Add(role);
                this.InsertRoleValidate(role);
                foreach(var roleTemplateRule in roleTemplate.RoleTemplateRules) {
                    var rule = new Rule {
                        NodeId = roleTemplateRule.NodeId
                    };
                    // 当前角色（role）是新增状态，无需针对此角色新增的 Rule 做校验
                    role.Rules.Add(rule);
                }
            }
        }

        //为企业admin角色授予指定企业授权模板中的Security相关权限
        private void GenerateAdminRule(Enterprise enterprise, Role adminRole) {
            if(adminRole == null) {
                adminRole = this.ObjectContext.Roles.SingleOrDefault(v => v.EnterpriseId == enterprise.Id && v.IsAdmin && v.Status == (int)SecurityRoleStatus.有效);
                if(adminRole == null)
                    throw new ValidationException(ErrorStrings.Enterprise_Validation14);
            }

            var pageIds = new List<int>();
            var systemId = this.ObjectContext.Pages.Where(v => v.Type == (int)SecurityPageType.系统 && v.PageId == SECURITY_SYSTEM_PAGE_ID).Select(v => v.Id).SingleOrDefault();
            if(systemId != 0) {
                pageIds.Add(systemId);
                var groupIds = this.ObjectContext.Pages.Where(v => v.Type == (int)SecurityPageType.分组 && v.ParentId == systemId).Select(v => v.Id).ToArray();
                if(groupIds.Length > 0) {
                    pageIds.AddRange(groupIds);
                    pageIds.AddRange(this.ObjectContext.Pages.Where(v => v.Type == (int)SecurityPageType.页面 && groupIds.Contains(v.ParentId)).Select(v => v.Id).ToArray());
                }
            }
            if(pageIds.Count == 0)
                return;

            var nodeIds = new List<int>();
            nodeIds.AddRange(this.ObjectContext.Nodes.Where(node => node.CategoryType == (int)SecurityNodeCategoryType.页面 && node.Status == (int)SecurityCommonStatus.有效 && pageIds.Contains(node.CategoryId)).Select(v => v.Id).ToArray());
            nodeIds.AddRange(this.ObjectContext.Nodes.Where(node => node.CategoryType == (int)SecurityNodeCategoryType.操作 && node.Status == (int)SecurityCommonStatus.有效
                && this.ObjectContext.SecurityActions.Any(action => action.Id == node.CategoryId && pageIds.Contains(action.PageId) && action.Status == (int)SecurityCommonStatus.有效)).Select(v => v.Id).ToArray());
            if(nodeIds.Count == 0)
                return;

            var templateNodeIds = this.ObjectContext.EntNodeTemplateDetails.Where(detail => detail.EntNodeTemplateId == enterprise.EntNodeTemplateId.Value && nodeIds.Contains(detail.NodeId)).Select(v => v.NodeId).ToArray();
            var currentRules = new Rule[0];
            if(adminRole.EntityState != EntityState.Added)
                //企业模板调整，删除现有全部管理员角色的权限，包括业务节点
                currentRules = this.ObjectContext.Rules.Where(v => v.RoleId == adminRole.Id).ToArray();

            var deleteRules = currentRules.Where(v => !templateNodeIds.Contains(v.NodeId));
            var insertRuleIds = templateNodeIds.Where(v => !currentRules.Any(current => current.NodeId == v));

            foreach(var rule in deleteRules)
                DeleteFromDatabase(rule);

            foreach(var nodeId in insertRuleIds) {
                var rule = new Rule {
                    NodeId = nodeId
                };
                adminRole.Rules.Add(rule);
                this.InsertRuleValidate(rule);
            }
        }

        [RequiresAuthentication]
        public void 创建企业(Enterprise enterprise, int? entOrganizationTplId, string password, int[] roleTemplateIds) {
            //企业根组织
            var rootOrganization = new Organization {
                ParentId = -1,
                Code = enterprise.Code,
                Name = enterprise.Name,
                Type = (int)SecurityOrganizationOrganizationType.企业,
                Sequence = 0,
                Status = (int)SecurityCommonStatus.有效
            };
            enterprise.Organizations.Add(rootOrganization);
            this.InsertOrganizationValidate(rootOrganization);

            if(entOrganizationTplId.HasValue) {
                //根据组织模板，复制组织
                var conflicts = new List<EntOrganizationTplDetail>();
                var entOrganizationTplDetails = this.ObjectContext.EntOrganizationTplDetails.Where(v => v.EntOrganizationTplId == entOrganizationTplId.Value).ToArray();
                var rootEntOrganizationTplDetail = entOrganizationTplDetails.Single(v => v.ParentId == -1);
                GenerateOrganization(enterprise, rootOrganization, rootEntOrganizationTplDetail, conflicts);
            }

            var adminPersonnel = new Personnel {
                LoginId = "admin",
                Password = password,
                Name = "企业管理员",
                Status = (int)SecurityUserStatus.有效
            };
            enterprise.Personnels.Add(adminPersonnel);
            this.InsertUserValidate(adminPersonnel);

            var adminRole = new Role {
                Name = "企业管理员",
                IsAdmin = true,
                Status = (int)SecurityRoleStatus.有效
            };
            enterprise.Roles.Add(adminRole);
            this.InsertRoleValidate(adminRole);

            var organizationPersonnel = new OrganizationPersonnel();
            rootOrganization.OrganizationPersonnels.Add(organizationPersonnel);
            adminPersonnel.OrganizationPersonnels.Add(organizationPersonnel);
            this.InsertOrganizationUserValidate(organizationPersonnel);

            var rolePersonnel = new RolePersonnel();
            adminPersonnel.RolePersonnels.Add(rolePersonnel);
            adminRole.RolePersonnels.Add(rolePersonnel);
            this.InsertRoleUserValidate(rolePersonnel);

            if(enterprise.EntNodeTemplateId.HasValue) {
                this.GenerateAdminRule(enterprise, adminRole);
            }

            if(roleTemplateIds.Length > 0) {
                this.GenerateRoleByTemplate(enterprise, roleTemplateIds);
            }
        }

        [RequiresAuthentication]
        public void 调整企业(Enterprise enterprise, int? entOrganizationTplId, int[] roleTemplateIds) {
            var oldEnterprise = this.ChangeSet.GetOriginal(enterprise);
            var oldStatus = oldEnterprise == null ? enterprise.Status : oldEnterprise.Status;
            //修改前状态 = 新建/冻结/生效，为合法
            if(oldStatus != (int)SecurityEnterpriseStatus.新建 && oldStatus != (int)SecurityEnterpriseStatus.冻结 && oldStatus != (int)SecurityEnterpriseStatus.生效)
                throw new ValidationException(ErrorStrings.Enterprise_Validation7);
            //修改前状态 = 冻结，则企业编号不允许修改
            if(oldEnterprise != null && oldEnterprise.Status == (int)SecurityEnterpriseStatus.冻结 && oldEnterprise.Code != enterprise.Code)
                throw new ValidationException(ErrorStrings.Enterprise_Validation8);

            //企业存在有效的组织，且组织模板Id（参数）不为空，则不合法
            if(entOrganizationTplId.HasValue && this.ObjectContext.Organizations.Any(v => v.EnterpriseId == enterprise.Id && v.Type == (int)SecurityOrganizationOrganizationType.部门 && v.Status == (int)SecurityCommonStatus.有效))
                throw new ValidationException(ErrorStrings.Enterprise_Validation7);

            if(oldEnterprise != null && enterprise.EntNodeTemplateId.HasValue && oldEnterprise.EntNodeTemplateId != enterprise.EntNodeTemplateId)
                this.GenerateAdminRule(enterprise, null);

            if(roleTemplateIds.Length > 0)
                this.GenerateRoleByTemplate(enterprise, roleTemplateIds);

            var rootOrganization = this.ObjectContext.Organizations.Single(v => v.EnterpriseId == enterprise.Id && v.Type == (int)SecurityOrganizationOrganizationType.企业 && v.Status == (int)SecurityCommonStatus.有效);
            rootOrganization.Code = enterprise.Code;
            rootOrganization.Name = enterprise.Name;
            this.UpdateOrganizationValidate(rootOrganization);

            if(entOrganizationTplId.HasValue) {
                var conflicts = new List<EntOrganizationTplDetail>();
                //将模板对应授权规则取到内存中
                var entOrganizationTplDetails = this.ObjectContext.EntOrganizationTplDetails.Where(v => v.EntOrganizationTplId == entOrganizationTplId.Value).ToArray();
                var rootEntOrganizationTplDetail = entOrganizationTplDetails.Single(v => v.ParentId == -1);
                GenerateOrganization(enterprise, rootOrganization, rootEntOrganizationTplDetail, conflicts);
            }
        }

        [RequiresAuthentication]
        public void 作废企业(Enterprise enterprise) {
            var dbEnterprise = this.ObjectContext.Enterprises.Where(v => v.Id == enterprise.Id && (v.Status == (int)SecurityEnterpriseStatus.冻结 || v.Status == (int)SecurityEnterpriseStatus.新建 || v.Status == (int)SecurityEnterpriseStatus.生效)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbEnterprise == null)
                throw new ValidationException(ErrorStrings.Enterprise_Validation10);

            this.UpdateToDatabase(enterprise);
            enterprise.Status = (int)SecurityEnterpriseStatus.作废;
            this.UpdateEnterpriseValidate(enterprise);

            var organizations = this.ObjectContext.Organizations.Where(v => v.EnterpriseId == enterprise.Id).ToArray();
            foreach(var organization in organizations) {
                organization.Status = (int)SecurityCommonStatus.作废;
                this.UpdateOrganizationValidate(organization);
            }

            var personnels = this.ObjectContext.Personnels.Where(v => v.EnterpriseId == enterprise.Id).ToArray();
            foreach(var personnel in personnels)
                作废人员(personnel);

            var roles = this.ObjectContext.Roles.Where(v => v.EnterpriseId == enterprise.Id).ToArray();
            foreach(var role in roles)
                冻结角色(role);
        }

        [RequiresAuthentication]
        public void 冻结企业(Enterprise enterprise) {
            var dbEnterprise = this.ObjectContext.Enterprises.Where(v => v.Id == enterprise.Id && v.Status == (int)SecurityEnterpriseStatus.生效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbEnterprise == null)
                throw new ValidationException(ErrorStrings.Enterprise_Validation11);
            this.UpdateToDatabase(enterprise);
            enterprise.Status = (int)SecurityEnterpriseStatus.冻结;
            this.UpdateEnterpriseValidate(enterprise);
        }

        [RequiresAuthentication]
        public void 恢复企业(Enterprise enterprise) {
            var dbEnterprise = this.ObjectContext.Enterprises.Where(v => v.Id == enterprise.Id && v.Status == (int)SecurityEnterpriseStatus.冻结).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbEnterprise == null)
                throw new ValidationException(ErrorStrings.Enterprise_Validation12);
            this.UpdateToDatabase(enterprise);
            enterprise.Status = (int)SecurityEnterpriseStatus.生效;
            this.UpdateEnterpriseValidate(enterprise);
        }

        [RequiresAuthentication]
        public void 生效企业(Enterprise enterprise) {
            var dbEnterprise = this.ObjectContext.Enterprises.Where(v => v.Id == enterprise.Id && v.Status == (int)SecurityEnterpriseStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbEnterprise == null)
                throw new ValidationException(ErrorStrings.Enterprise_Validation13);
            this.UpdateToDatabase(enterprise);
            enterprise.Status = (int)SecurityEnterpriseStatus.生效;
            this.UpdateEnterpriseValidate(enterprise);
        }
    }
}
