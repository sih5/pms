﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.DomainServices.Server;
using System.Web.Security;
using System.Xml.Linq;
using Quartz;
using Quartz.Job;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Web.Extensions;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;



namespace Sunlight.Silverlight.Security.Web
{
    partial class SecurityDomainService 
    {
        public string ExportAllData(Object[][] result, List<string> columnNames, string filename) {
            var exportDataFileName = GetExportFilePath(string.Format("{0}_{1}.xls", filename, DateTime.Now.ToString("ddHHmmssffff")));
            if(result.Any()) {
                using(var excelExport = new ExcelExport(exportDataFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == result.Count() + 1) {
                            return null;
                        }
                        return index == 0 ? columnNames.ToArray<Object>() : result[index - 1];
                    });
                }
            }
            return exportDataFileName;
        }
        public string GetErrorFilePath(string fileName)
        {
            return Path.Combine(Path.GetDirectoryName(fileName) ?? "", string.Format("{0}_ErrorData{1}", Path.GetFileNameWithoutExtension(fileName), Path.GetExtension(fileName)));
        }

        public string GetExportFilePath(string fileName) {
            var enterpriseCode = Utils.GetCurrentUserInfo().EnterpriseCode;
            return Path.Combine(GlobalVar.DOWNLOAD_EXPORTFILE_DIR, enterpriseCode, string.Format("{0}_{1:yyMMdd_HHmmss_fff}{2}", Path.GetFileNameWithoutExtension(fileName), DateTime.Now, Path.GetExtension(fileName)));
        }

        public string GetImportFilePath(string fileName) {
            return fileName;
        }

        /// <summary>
        ///     生成导入用的 Excel 模板文件
        /// </summary>
        /// <param name="fileName">文件名</param>
        /// <param name="columns">列名</param>
        /// <returns>文件路径</returns>
        [Invoke]
        public string 导出模板(string fileName, List<string> columns) {
            var filePath = "";
            filePath = GetExportFilePath(fileName);
            using(var excelExport = new ExcelExport(filePath)) {
                excelExport.ExportByRow(index => (columns != null && index == 0) ? columns.ToArray() : null);
            }
            return filePath;
        }

        /// <summary>
        ///     生成带有格式的导入用 Excel 模板文件
        /// </summary>
        /// <param name="fileName">文件名</param>
        /// <param name="columns">列定义</param>
        /// <returns>文件路径</returns>
        [Invoke]
        public string GetImportTemplateFileWithFormat(string fileName, List<ImportTemplateColumn> columns) {
            var filePath = GetExportFilePath(fileName);
            new ImportTemplate(columns).GenerateExcelFile(filePath);
            return filePath;
        }

        /// <summary>
        ///     根据当前用户生成用于 <see cref="FormsAuthentication" /> 方式身份验证的 Cookie
        /// </summary>
        /// <param name="expireTime">过期时间</param>
        /// <returns>当前用户可通过身份验证的 Cookie</returns>
        //private Cookie GetAuthenticationCookie(TimeSpan expireTime)
        //{

        //    var ticket = new FormsAuthenticationTicket(2, ServiceContext.User.Identity.Name, DateTime.Now, DateTime.Now.Add(expireTime), false, string.Empty);
        //    var encryptedTicket = FormsAuthentication.Encrypt(ticket);
        //    var cookie = new Cookie(FormsAuthentication.FormsCookieName, encryptedTicket);
        //    cookie.Expires = DateTime.Now.Add(expireTime);
        //    cookie.Domain = OperationContext.Current.RequestContext.RequestMessage.Headers.To.Host;
        //    cookie.Path = "/";
        //    return cookie;
        //}

        ///// <summary>
        /////     将定时导出任务加入到调度队列中
        ///// </summary>
        ///// <param name="methodName">导出方法名称</param>
        ///// <param name="exportParams">导出方法所使用的参数集合</param>
        ///// <param name="remark">备注</param>
        ///// <returns>任务加入队列的结果，若成功则为 true，否则为 false</returns>
        //[Invoke]
        //public bool AddExportJobToScheduler(string methodName, Dictionary<string, string> exportParams, string remark)
        //{
        //    var user = Utils.GetCurrentUserInfo();
        //    XNamespace serviceNameSpace = "http://sunlight.bz/excelservice/";
        //    var jobName = string.Format("{0}_{1}_{2}_{3:MMddHHmmssffff}", methodName, user.EnterpriseCode, user.LoginId, DateTime.Now);
        //    var job = JobBuilder.Create<HttpWebRequestJob>().WithIdentity(jobName, "Excel_Service").Build();
        //    // URL and headers
        //    var webServiceUri = new Uri(new Uri(OperationContext.Current.RequestContext.RequestMessage.Headers.To.GetLeftPart(UriPartial.Authority)), "ExcelService.svc");
        //    job.JobDataMap.Add("URL", webServiceUri);
        //    job.JobDataMap.Add("METHOD", "POST");
        //    job.JobDataMap.Add("CONTENT-TYPE", "text/xml; charset=utf-8");
        //    var headerCollection = new WebHeaderCollection {
        //        {
        //            "SOAPAction", serviceNameSpace.ToString() + "IExcelService/" + methodName
        //        }
        //    };
        //    job.JobDataMap.Add("HEADERS", headerCollection);

        //    var cookies = new CookieCollection();
        //    cookies.Add(GetAuthenticationCookie(new TimeSpan(0, 30, 0)));
        //    job.JobDataMap.Add("COOKIES", cookies);

        //    // body
        //    XNamespace soap = "http://schemas.xmlsoap.org/soap/envelope/";
        //    XNamespace instance = "http://www.w3.org/2001/XMLSchema-instance";
        //    var method = new XElement(serviceNameSpace + methodName);
        //    foreach (var parameter in exportParams.Where(kv => kv.Key != "jobName"))
        //    {
        //        XElement element;
        //        if (string.IsNullOrEmpty(parameter.Value))
        //            element = new XElement(serviceNameSpace + parameter.Key, new XAttribute(instance + "nil", true));
        //        else
        //        {
        //            DateTime date;
        //            var value = DateTime.TryParse(parameter.Value, out date) ? date.ToString("O") : parameter.Value;
        //            element = new XElement(serviceNameSpace + parameter.Key, value);
        //        }
        //        method.Add(element);
        //    }
        //    method.Add(new XElement(serviceNameSpace + "jobName", jobName));

        //    var body = new XElement(soap + "Body", method);
        //    var envelope = new XElement(soap + "Envelope", new XAttribute(XNamespace.Xmlns + "s", soap), new XAttribute(XNamespace.Xmlns + "i", instance), new XAttribute("xmlns", serviceNameSpace), body);
        //    job.JobDataMap.Add("BODY", envelope.ToString(SaveOptions.DisableFormatting));

        //    // 默认启动时间为隔日 02:00 - 04:00 段的某一时刻
        //    var random = new Random(jobName.GetHashCode());
        //    var runTime = DateBuilder.TomorrowAt(random.Next(2, 4), random.Next(0, 59), 0);
        //    var trigger = TriggerBuilder.Create().WithIdentity(jobName, "Excel_Service").StartAt(runTime).Build();

        //    var client = new SchedulerServiceClient();
        //    var scheduleTime = client.ScheduleJob(job, trigger);
        //    var state = new ScheduleExportState
        //    {
        //        JobName = jobName,
        //        Category = methodName,
        //        ScheduleTime = scheduleTime.LocalDateTime,
        //        Remark = remark,
        //        EnterpriseId = user.EnterpriseId,
        //        CreatorId = user.Id,
        //        CreateTime = DateTime.Now
        //    };
        //    ObjectContext.ScheduleExportStates.AddObject(state);
        //    ObjectContext.SaveChanges();
        //    return true;
        //}
    }
}
