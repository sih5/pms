﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Security.Web.Resources;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        [RequiresAuthentication]
        public IQueryable<FavoritePage> GetFavoritePagesBy(int id, int userId, int pageId) {
            IQueryable<FavoritePage> result = this.ObjectContext.FavoritePages;
            if(id > 0)
                result = result.Where(r => r.Id == id);
            if(pageId > 0)
                result = result.Where(r => r.PageId == pageId);
            if(userId > 0)
                result = result.Where(r => r.PersonnelId == userId);
            return result;
        }

        //新增
        [RequiresAuthentication]
        public void InsertFavoritePage(FavoritePage favoritePage) {
            this.InsertToDatabase(favoritePage);
            this.InsertFavoritePageValidate(favoritePage);
        }

        //针对新增操作的数据校验及默认操作
        private void InsertFavoritePageValidate(FavoritePage favoritePage) {
            //组合唯一性校验：用户Id、页面Id
            var dbFavoritePage = this.ObjectContext.FavoritePages.Where(a => a.PersonnelId == favoritePage.PersonnelId && a.PageId == favoritePage.PageId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbFavoritePage != null)
                throw new ValidationException(string.Format(ErrorStrings.FavoritePage_Validation));

            //显示顺序 = 当前该用户的页面收藏夹数据中显示顺序最大值 + 1
            var maxSequence = this.ObjectContext.FavoritePages.Where(r => r.PersonnelId == favoritePage.PersonnelId).Max(r => (int?)r.Sequence) ?? 0;
            favoritePage.Sequence = maxSequence + 1;
        }

        //更新
        [RequiresAuthentication]
        public void UpdateFavoritePage(FavoritePage favoritePage) {
            this.UpdateToDatabase(favoritePage);
            this.UpdateFavoritePageValidate(favoritePage);
        }

        //针对更新操作的数据校验及默认操作
        private void UpdateFavoritePageValidate(FavoritePage favoritePage) {
            //组合唯一性校验：用户Id、页面Id
            var dbFavoritePage = this.ObjectContext.FavoritePages.Where(a => a.PersonnelId == favoritePage.PersonnelId && a.PageId == favoritePage.PageId && a.Id != favoritePage.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbFavoritePage != null)
                throw new ValidationException(string.Format(ErrorStrings.FavoritePage_Validation));
        }

        //删除
        [RequiresAuthentication]
        public void DeleteFavoritePage(FavoritePage favoritePage) {
            var dbFavoritePage = this.ObjectContext.FavoritePages.Where(a => a.PersonnelId == favoritePage.PersonnelId && a.PageId == favoritePage.PageId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbFavoritePage == null)
                throw new ValidationException(string.Format(ErrorStrings.FavoritePage_Validation1));
            this.DeleteFromDatabase(favoritePage);
        }

        //isFavorite参数为true时表示需添加至收藏夹，否则表示需从收藏夹中删除
        [RequiresAuthentication, Invoke]
        public void SetFavoritePage(int pageId, bool isFavorite) {
            var userId = this.GetCurrentUserInfo().Id;
            if(isFavorite) {
                var favoritePage = new FavoritePage {
                    PageId = pageId,
                    PersonnelId = userId
                };
                this.InsertFavoritePage(favoritePage);
            } else
                foreach(var favoritePage in this.ObjectContext.FavoritePages.Where(fav => fav.PageId == pageId && fav.PersonnelId == userId).ToList())
                    this.DeleteFavoritePage(favoritePage);
            this.ObjectContext.SaveChanges();
        }

        [RequiresAuthentication, Invoke]
        public void ChangeFavoritePagesOrder(Dictionary<int, int> pagesOrder) {
            var userId = this.GetCurrentUserInfo().Id;
            var favoritePages = this.ObjectContext.FavoritePages.Where(fav => fav.PersonnelId == userId);
            foreach(var pageOrder in pagesOrder) {
                var favoritePage = favoritePages.SingleOrDefault(fav => fav.PageId == pageOrder.Key);
                if(favoritePage != null)
                    favoritePage.Sequence = pageOrder.Value;
            }
            this.ObjectContext.SaveChanges();
        }
    }
}
