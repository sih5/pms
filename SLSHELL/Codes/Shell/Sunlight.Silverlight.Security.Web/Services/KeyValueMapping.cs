﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        [RequiresAuthentication]
        public IEnumerable<KeyValueItem> GetKeyValueMappingByName(string name) {
            if(name == null)
                throw new ArgumentNullException("name");

            return this.ObjectContext.KeyValueItems.Where(kvm => kvm.Name == name);
        }

        [RequiresAuthentication]
        public IEnumerable<KeyValueItem> GetKeyValueMappingByNames(string[] names) {
            if(names == null)
                throw new ArgumentNullException("names");

            switch(names.Length) {
                case 0:
                    return Enumerable.Empty<KeyValueItem>();
                case 1:
                    return this.GetKeyValueMappingByName(names[0]);
            }

            return this.ObjectContext.KeyValueItems.Where(kvm => names.Contains(kvm.Name));
        }
    }
}
