﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Shell.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        [RequiresAuthentication]
        public IQueryable<LoginLog> GetLoginLogAndPersonnels() {
            return this.ObjectContext.LoginLogs.Include("Personnel").Include("Personnel.Enterprise").OrderBy(v => v.Id);
        }

        public IQueryable<LoginLog> 查询登录日志(string enterprisecode, string enterprisename) {
            var LoginQuery = from lg in ObjectContext.LoginLogs
                             join ps in ObjectContext.Personnels on lg.PersonnelId equals ps.Id
                             join ep in ObjectContext.Enterprises on ps.EnterpriseId equals ep.Id
                             where (string.IsNullOrEmpty(enterprisecode) || ep.Code == enterprisecode) &&
                                   (string.IsNullOrEmpty(enterprisename) || ep.Name == enterprisename)
                             select lg;
            return LoginQuery.Include("Personnel").Include("Personnel.Enterprise");
        }

        [Invoke]
        [RequiresAuthentication]
        public void 修改退出时间(int personid) {
            var loginlog = ObjectContext.LoginLogs.Where(l => l.PersonnelId == personid).OrderByDescending(r => r.LoginTime).FirstOrDefault();
            if(loginlog != null) {
                loginlog.LogoutTime = DateTime.Now;
                UpdateToDatabase(loginlog);
                this.ObjectContext.SaveChanges();
            }
        }


        public void InsertLoginLog(LoginLog loginLog) {
            InsertToDatabase(loginLog);
            InsertLoginLogValidate(loginLog);
        }

        private void InsertLoginLogValidate(LoginLog loginLog) {
            //var userInfo = this.GetCurrentUserInfo();
            //loginLog.PersonnelId = userInfo.Id;
            loginLog.LoginTime = DateTime.Now;
        }

        [RequiresAuthentication]
        public string ExportLoginLogForReport(string enterprisecode, string enterprisename, string loginid, int? categoryId, string personnelname, DateTime? startlogintime, DateTime? endlogintime) {
            string returnFileName = GetExportFilePath(string.Format("登陆日志导出_{0}.xls", DateTime.Now.ToString("ddHHmmssffff")));
            var excelColumns = new List<string>();
            var ruleRecorddata = this.查询登录日志(enterprisecode, enterprisename);
            if(!string.IsNullOrEmpty(enterprisecode)) {
                ruleRecorddata = ruleRecorddata.Where(r => r.Personnel.Enterprise.Code == enterprisecode);
            }
            if(!string.IsNullOrEmpty(enterprisename)) {
                ruleRecorddata = ruleRecorddata.Where(r => r.Personnel.Enterprise.Name == enterprisecode);
            }
            if(categoryId.HasValue) {
                ruleRecorddata = ruleRecorddata.Where(r => r.Personnel.Enterprise.EnterpriseCategoryId == categoryId);
            }
            if(!string.IsNullOrEmpty(loginid)) {
                ruleRecorddata = ruleRecorddata.Where(r => r.Personnel.LoginId == loginid);
            }
            if(!string.IsNullOrEmpty(personnelname)) {
                ruleRecorddata = ruleRecorddata.Where(r => r.Personnel.Name == personnelname);
            }
            if(startlogintime.HasValue) {
                ruleRecorddata = ruleRecorddata.Where(r => r.LoginTime >= startlogintime);
            }
            if(endlogintime.HasValue) {
                ruleRecorddata = ruleRecorddata.Where(r => r.LoginTime <= endlogintime);
            }

            var dataArray = ruleRecorddata.ToArray();
            if(dataArray.Any()) {
                excelColumns.Add("企业编号");
                excelColumns.Add("企业名称");
                excelColumns.Add("登录Id");
                excelColumns.Add("人员姓名");
                excelColumns.Add("IP");
                excelColumns.Add("Mac");
                excelColumns.Add("登录时间");
                excelColumns.Add("退出时间");

                using(var excelExport = new ExcelExport(returnFileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;

                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];

                        var values = new object[] {
                            //写入行
                            detail.Personnel.Enterprise.Code,
                            detail.Personnel.Enterprise.Name,
                            detail.Personnel.LoginId,
                            detail.Personnel.Name,
                            detail.IP,
                            detail.Mac,
                            detail.LoginTime,
                            detail.LogoutTime
                        };
                        return values;
                    });
                }
            }

            return returnFileName;
        }


    }
}