﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Security.Web.Resources;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        [RequiresAuthentication]
        public IQueryable<Node> GetNodesBy() {
            var resultPages = this.ObjectContext.Nodes.Where(v => v.CategoryType == (int)SecurityNodeCategoryType.页面 && v.Status == (int)SecurityCommonStatus.有效).ToArray();
            //var pageIds = resultPages.Select(s => s.CategoryId).ToArray();
            //if(pageIds.Length > 0) {
            //    var pages = this.GetPages(pageIds, -1, -1, null, null, -1).ToArray();
            //}
            var resultActions = this.ObjectContext.Nodes.Where(v => v.CategoryType == (int)SecurityNodeCategoryType.操作 && v.Status == (int)SecurityCommonStatus.有效).ToArray();
            //var actionIds = resultActions.Select(s => s.CategoryId).ToArray();
            //if(actionIds.Length > 0) {
            //    var actions = this.GetSecurityActions(actionIds, -1, null, -1).ToArray();
            //}
            var result = resultPages.Union(resultActions);
            return result.AsQueryable();
        }

        //查询未使用的资源节点
        //[Update(UsingCustomMethod = true)]
        public IQueryable<Node> 查询未使用的资源节点(int a) {
            //2、调用【企业.查询企业】，获得企业授权模板Id，依据：  1）企业Id = 当前企业Id
            var enterprise = this.ObjectContext.Enterprises.SingleOrDefault(v => v.Id == this.GetCurrentUserInfo().EnterpriseId);
            if(enterprise == null)
                throw new ValidationException(string.Format(ErrorStrings.Node_Validation2));
            var entNodeTemplateId = enterprise.EntNodeTemplateId.HasValue ? enterprise.EntNodeTemplateId.Value : 0;

            //3、根据企业授权模板Id，获得该模板下所有的资源节点Id，再根据这些资源节点Id，查询状态 = 有效的资源节点，并返回资源节点相关信息
            var entNodeTemplate = this.ObjectContext.EntNodeTemplates.SingleOrDefault(r => r.Id == entNodeTemplateId);

            if(entNodeTemplate == null)
                throw new ValidationException(string.Format(ErrorStrings.Node_Validation3));
            var details = entNodeTemplate.EntNodeTemplateDetails.ToArray();
            var nodeIds = details.Select(t => t.NodeId).ToArray();

            //4、调用【角色.查询角色】，获得本企业所有角色Id，依据：  1）企业Id = 当前企业Id
            var roleIds = this.ObjectContext.Roles.Where(v => v.EnterpriseId == this.GetCurrentUserInfo().EnterpriseId).Select(t => t.Id).ToArray();

            //5、调用【授权规则.查询授权规则】，依据：  1）角色Id in （步骤4中获得的所有角色Id）
            var nodeIdsRole = this.ObjectContext.Rules.Where(v => roleIds.Contains(v.RoleId)).Select(t => t.NodeId).ToArray();

            //6、获得存在于步骤3中但不存在于步骤5中的所有节点Id
            nodeIds.Except(nodeIdsRole); //TODO: 这一句有问题

            //7、调用【资源节点.查询资源节点】，获得并返回所有资源节点数据，依据：  1）资源节点Id in （步骤6中的所有节点Id）
            var resultPages = this.ObjectContext.Nodes.Where(v => nodeIds.Contains(v.Id) && v.CategoryType == (int)SecurityNodeCategoryType.页面 && v.Status == (int)SecurityCommonStatus.有效).ToArray();
            //var pageIds = resultPages.Select(s => s.CategoryId).ToArray();
            //if(pageIds.Length > 0) {
            //    var pages = this.GetPages(pageIds, -1, -1, null, null, -1).ToArray();
            //}
            var resultActions = this.ObjectContext.Nodes.Where(v => nodeIds.Contains(v.Id) && v.CategoryType == (int)SecurityNodeCategoryType.操作 && v.Status == (int)SecurityCommonStatus.有效).ToArray();
            //var actionIds = resultActions.Select(s => s.CategoryId).ToArray();
            //if(actionIds.Length > 0) {
            //    var actions = this.GetSecurityActions(actionIds, -1, null, -1).ToArray();
            //}
            var result = resultPages.Union(resultActions);

            return result.AsQueryable();
        }

        //查询企业模板资源节点
        //[Update(UsingCustomMethod = true)]
        public IQueryable<Node> 查询企业模板资源节点(int entNodeTemplateId) {
            //1、根据企业授权模板Id，获得该模板下所有的资源节点Id，
            var entNodeTemplate = this.ObjectContext.EntNodeTemplates.SingleOrDefault(r => r.Id == entNodeTemplateId);
            if(entNodeTemplate == null)
                throw new ValidationException(string.Format(ErrorStrings.Node_Validation4));
            var details = entNodeTemplate.EntNodeTemplateDetails.ToArray();
            var nodeIds = details.Select(t => t.NodeId).ToArray();

            //2、再根据这些资源节点Id，查询状态 = 有效的资源节点，并返回资源节点相关信息
            var resultPages = this.ObjectContext.Nodes.Where(v => nodeIds.Contains(v.Id) && v.CategoryType == (int)SecurityNodeCategoryType.页面 && v.Status == (int)SecurityCommonStatus.有效).ToArray();
            //var pageIds = resultPages.Select(s => s.CategoryId).ToArray();
            //if(pageIds.Length > 0) {
            //    var pages = this.GetPages(pageIds, -1, -1, null, null, -1).ToArray();
            //}
            var resultActions = this.ObjectContext.Nodes.Where(v => nodeIds.Contains(v.Id) && v.CategoryType == (int)SecurityNodeCategoryType.操作 && v.Status == (int)SecurityCommonStatus.有效).ToArray();
            //var actionIds = resultActions.Select(s => s.CategoryId).ToArray();
            //if(actionIds.Length > 0) {
            //    var actions = this.GetSecurityActions(actionIds, -1, null, -1).ToArray();
            //}
            var result = resultPages.Union(resultActions);

            return result.AsQueryable();
        }

        //新增
        [RequiresAuthentication]
        public void InsertNode(Node node) {
            this.InsertToDatabase(node);
            this.InsertNodeValidate(node);
        }

        //针对新增操作的数据校验及默认操作
        private void InsertNodeValidate(Node node) {
            //CategoryType/CategoryId必须组合唯一
            var dbNodeUniqueness = this.ObjectContext.Nodes.Where(a => a.CategoryType == node.CategoryType && a.CategoryId == node.CategoryId && a.Status != (int)SecurityCommonStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbNodeUniqueness != null)
                throw new ValidationException(string.Format(ErrorStrings.Node_Validation1));
        }

        //更新
        [RequiresAuthentication]
        public void UpdateNode(Node node) {
            this.UpdateToDatabase(node);

            this.UpdateNodeValidate(node);
        }

        //针对更新操作的数据校验及默认操作
        private void UpdateNodeValidate(Node node) {
            //只能对有效的数据进行修改
            var dbNode = this.ObjectContext.Nodes.Where(v => v.Id == node.Id && v.Status == (int)SecurityCommonStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbNode == null)
                throw new ValidationException(ErrorStrings.Node_Validation);

            //CategoryType/CategoryId必须组合唯一
            var dbNodeUniqueness = this.ObjectContext.Nodes.Where(a => a.CategoryType == node.CategoryType && a.CategoryId == node.CategoryId && a.Id != node.Id && a.Status != (int)SecurityCommonStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbNodeUniqueness != null)
                throw new ValidationException(string.Format(ErrorStrings.Node_Validation1));
        }
    }
}
