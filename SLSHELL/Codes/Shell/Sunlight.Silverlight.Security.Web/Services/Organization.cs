﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Security.Web.Resources;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        [RequiresAuthentication]
        public IQueryable<Organization> GetOrganizationsBy(int id, string code, string name, int type, int status, int enterpriseId) {
            IQueryable<Organization> result = this.ObjectContext.Organizations;
            if(id > 0)
                result = result.Where(r => r.Id == id);
            if(!string.IsNullOrEmpty(code))
                result = result.Where(r => r.Code.ToLower().Contains(code.ToLower()));
            if(!string.IsNullOrEmpty(name))
                result = result.Where(r => r.Name.ToLower().Contains(name.ToLower()));
            if(type >= 0)
                result = result.Where(r => r.Type == type);
            if(status >= 0)
                result = result.Where(r => r.Status == status);
            if(enterpriseId > 0)
                result = result.Where(r => r.EnterpriseId == enterpriseId);

            return result.Include("OrganizationPersonnels.Personnel");
        }

        [RequiresAuthentication]
        public IQueryable<Organization> GetOrganizationsWithDetail() {
            return this.ObjectContext.Organizations.Include("OrganizationPersonnels").OrderBy(v => v.Id);
        }

        //新增
        [RequiresAuthentication]
        public void InsertOrganization(Organization organization) {
            //在ObjectContext中登记，指明主单将要新增至数据库
            this.InsertToDatabase(organization);
            //校验实体对象并做默认处理
            this.InsertOrganizationValidate(organization);
        }

        //针对新增操作的数据校验及默认操作
        private void InsertOrganizationValidate(Organization organization) {
            //更新创建人等数据
            var userInfo = this.GetCurrentUserInfo();
            //organization.EnterpriseId = userInfo.EnterpriseId;  组织.企业Id为组织所属企业，非创建人所属企业
            organization.CreatorId = userInfo.Id;
            organization.CreatorName = userInfo.Name;
            organization.CreateTime = DateTime.Now;
        }

        //更新
        [RequiresAuthentication]
        public void UpdateOrganization(Organization organization) {
            //在ObjectContext中登记，指明主单将要保存至数据库，注意该步骤不会立刻去UPDATE数据库
            this.UpdateToDatabase(organization);

            //校验实体对象并做默认处理
            this.UpdateOrganizationValidate(organization);
        }

        //针对更新操作的数据校验及默认操作
        private void UpdateOrganizationValidate(Organization organization) {
            //更新修改人等数据
            var userInfo = this.GetCurrentUserInfo();
            organization.ModifierId = userInfo.Id;
            organization.ModifierName = userInfo.Name;
            organization.ModifyTime = DateTime.Now;
        }

        //  根据ParentId取得所有有效的组织
        [RequiresAuthentication]
        public IQueryable<Organization> GetOrganizationsByParentId(int parentId) {
            return this.ObjectContext.Organizations.Where(o => o.ParentId == parentId && o.Status == (int)SecurityCommonStatus.有效);
        }

        [RequiresAuthentication]
        public void 创建组织(Organization organization) {
            //组织.企业Id为组织所属企业，与创建人所属企业一致
            var userInfo = this.GetCurrentUserInfo();
            organization.EnterpriseId = userInfo.EnterpriseId;
            //业务规则检查1：数据库中EnterpriseId、Code必须唯一
            var conflictOrganization = this.ObjectContext.Organizations.Where(a => a.EnterpriseId == organization.EnterpriseId && a.Code.ToLower() == organization.Code.ToLower() && a.Status != (int)SecurityCommonStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(conflictOrganization != null)
                //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
                //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
                throw new ValidationException(string.Format(ErrorStrings.Organization_Validation1, organization.Code)); //数据库中已经包含编号为“{0}”的XX

            //业务规则检查2：数据库中EnterpriseId、Name必须唯一
            var conflictOrganizationName = this.ObjectContext.Organizations.Where(a => a.EnterpriseId == organization.EnterpriseId && a.Name.ToLower() == organization.Name.ToLower() && a.Status != (int)SecurityCommonStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(conflictOrganizationName != null)
                //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
                //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
                throw new ValidationException(string.Format(ErrorStrings.Organization_Validation2, organization.Name)); //数据库中已经包含名称为“{0}”的XX
        }

        [RequiresAuthentication]
        public void 集中创建组织(Organization organization) {
            //业务规则检查1：数据库中EnterpriseId、Code必须唯一
            var conflictOrganization = this.ObjectContext.Organizations.Where(a => a.EnterpriseId == organization.EnterpriseId && a.Code.ToLower() == organization.Code.ToLower() && a.Status != (int)SecurityCommonStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(conflictOrganization != null)
                //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
                //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
                throw new ValidationException(string.Format(ErrorStrings.Organization_Validation1, organization.Code)); //数据库中已经包含编号为“{0}”的XX

            //业务规则检查2：数据库中EnterpriseId、Name必须唯一
            var conflictOrganizationName = this.ObjectContext.Organizations.Where(a => a.EnterpriseId == organization.EnterpriseId && a.Name.ToLower() == organization.Name.ToLower() && a.Status != (int)SecurityCommonStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(conflictOrganizationName != null)
                //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
                //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
                throw new ValidationException(string.Format(ErrorStrings.Organization_Validation2, organization.Name)); //数据库中已经包含名称为“{0}”的XX
        }

        [RequiresAuthentication]
        public void 调整组织(Organization organization) {
            var dbOrganization = this.ObjectContext.Organizations.Where(v => v.Id == organization.Id && v.Status != (int)SecurityCommonStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();

            //业务规则检查1：只允许更新“有效”状态的XX
            if(dbOrganization == null)
                //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
                //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
                throw new ValidationException(ErrorStrings.Organization_Validation); //只允许更新“有效”状态的XX

            //业务规则检查1：数据库中EnterpriseId、Code必须唯一
            if(string.Compare(dbOrganization.Code, organization.Code, StringComparison.OrdinalIgnoreCase) != 0 || dbOrganization.EnterpriseId != organization.EnterpriseId) {
                var conflictOrganizationCode = this.ObjectContext.Organizations.Where(a => a.EnterpriseId == organization.EnterpriseId && a.Code.ToLower() == organization.Code.ToLower() && a.Status != (int)SecurityCommonStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                if(conflictOrganizationCode != null)
                    throw new ValidationException(string.Format(ErrorStrings.Organization_Validation1, organization.Name));
            }

            //业务规则检查2：数据库中EnterpriseId、Name必须唯一
            if(string.Compare(dbOrganization.Name, organization.Name, StringComparison.OrdinalIgnoreCase) != 0 || dbOrganization.EnterpriseId != organization.EnterpriseId) {
                var conflictOrganizationName = this.ObjectContext.Organizations.Where(a => a.EnterpriseId == organization.EnterpriseId && a.Name.ToLower() == organization.Name.ToLower() && a.Status != (int)SecurityCommonStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                if(conflictOrganizationName != null)
                    throw new ValidationException(string.Format(ErrorStrings.Organization_Validation2, organization.Name));
            }
        }

        public IQueryable<Organization> GetOrganizationsByEnterpriseNameAndEnterpriseCategory(int status, string enterpriseCode, string enterpriseName, int enterpriseCategory) {
            IQueryable<Organization> result = this.ObjectContext.Organizations;
            if(status >= 0)
                result = result.Where(r => r.Status == status);
            if(!string.IsNullOrEmpty(enterpriseName)) {
                if(enterpriseCategory >= 0)
                    result = result.Where(r => r.Enterprise.Name.Contains(enterpriseName) && r.Enterprise.Code.Contains(enterpriseCode) && (r.Enterprise.EnterpriseCategoryId == enterpriseCategory));
                else {
                    result = result.Where(r => r.Enterprise.Name.Contains(enterpriseName) && r.Enterprise.Code.Contains(enterpriseCode));
                }
            }
            return result.Include("OrganizationPersonnels.Personnel");
        }

    }
}
