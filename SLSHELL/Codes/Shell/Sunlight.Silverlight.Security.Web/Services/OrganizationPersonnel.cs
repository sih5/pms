﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Security.Web.Resources;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        [RequiresAuthentication]
        public IQueryable<OrganizationPersonnel> GetOrganizationUsersBy(int userId, int organizationId) {
            IQueryable<OrganizationPersonnel> result = this.ObjectContext.OrganizationPersonnels;
            if(userId > 0)
                result = result.Where(r => r.PersonnelId == userId);
            if(organizationId > 0)
                result = result.Where(r => r.OrganizationId == organizationId);

            return result;
        }

        //新增
        [RequiresAuthentication]
        public void InsertOrganizationUser(OrganizationPersonnel organizationUser) {
            //在ObjectContext中登记，指明主单将要新增至数据库
            this.InsertToDatabase(organizationUser);

            //校验实体对象并做默认处理
            this.InsertOrganizationUserValidate(organizationUser);
        }

        //针对新增操作的数据校验及默认操作
        private void InsertOrganizationUserValidate(OrganizationPersonnel organizationUser) {
            //业务规则检查1：数据库中UserId/OrganizationId必须组合唯一
            var conflictOrganizationUser = this.ObjectContext.OrganizationPersonnels.Where(a => a.PersonnelId == organizationUser.PersonnelId && a.OrganizationId == organizationUser.OrganizationId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(conflictOrganizationUser != null)
                //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
                //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
                throw new ValidationException(string.Format(ErrorStrings.OrganizationPersonnel_Validation1));
        }

        //删除
        [RequiresAuthentication]
        public void DeleteOrganizationUser(OrganizationPersonnel organizationUser) {
            //在ObjectContext中登记，指明主单将要从数据库删除
            this.DeleteFromDatabase(organizationUser);
        }
    }
}
