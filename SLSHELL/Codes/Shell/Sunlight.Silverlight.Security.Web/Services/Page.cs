﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Security.Web.Resources;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        [RequiresAuthentication]
        public IQueryable<Page> GetPagesBy(int[] id, int parentId, int type, string pageType, string name, int status) {
            IQueryable<Page> result = this.ObjectContext.Pages;
            if(id != null && id.Length > 0)
                result = result.Where(v => id.Contains(v.Id));
            if(parentId > 0)
                result = result.Where(r => r.ParentId == parentId);
            if(type >= 0)
                result = result.Where(r => r.Type == type);
            if(!string.IsNullOrEmpty(pageType))
                result = result.Where(r => r.PageType.ToLower().Contains(pageType.ToLower()));
            if(!string.IsNullOrEmpty(name))
                result = result.Where(r => r.Name.ToLower().Contains(name.ToLower()));
            if(status >= 0)
                result = result.Where(r => r.Status == status);
            return result;
        }

        //新增
        [RequiresAuthentication]
        public void InsertPage(Page page) {
            this.InsertToDatabase(page);
        }

        //更新
        [RequiresAuthentication]
        public void UpdatePage(Page page) {
            this.UpdateToDatabase(page);
        }

        //作废系统
        [Update(UsingCustomMethod = true)]
        [RequiresAuthentication]
        public void 作废系统(Page page) {
            //合法性校验：存在有效的下级分组，不允许作废
            var groupPage = this.ObjectContext.Pages.FirstOrDefault(v => v.ParentId == page.Id && v.Status == (int)SecurityCommonStatus.有效);
            if(groupPage != null)
                throw new ValidationException(ErrorStrings.Page_Validation);

            //1、作废系统节点，调用【窗体页面.更新窗体页面】，其中赋值：  1）状态 = 作废
            this.UpdateToDatabase(page);
            page.Status = (int)SecurityCommonStatus.作废;

            //2、根据目标Id = 当前窗体页面.窗体页面Id，查找并作废资源节点数据，调用【资源节点.更新资源节点】，其中赋值：  1）状态 = 作废
            var systemNode = this.ObjectContext.Nodes.SingleOrDefault(v => v.CategoryType == (int)SecurityNodeCategoryType.页面 && v.CategoryId == page.Id);
            if(systemNode == null)
                throw new ValidationException(string.Format(ErrorStrings.Page_Validation1, page.Name));
            systemNode.Status = (int)SecurityCommonStatus.作废;
            this.UpdateNodeValidate(systemNode);

            //3、根据节点Id = 当前窗体页面.窗体页面Id，删除授权规则，调用【授权规则.删除授权规则】
            var rules = this.ObjectContext.Rules.Where(v => v.NodeId == page.Id).ToArray();
            foreach(var rule in rules)
                this.DeleteFromDatabase(rule);

            //4、调用【企业授权模板.查询企业授权模板】，依据：  1）企业授权模板资源清单.节点Id = 当前窗体页面.窗体页面Id
            //5、对于步骤4中查询到数据，删除清单中节点Id = 当前窗体页面.窗体页面Id的数据，然后调用【企业授权模板.更新企业授权模板】
            var entNodeTemplates = this.ObjectContext.EntNodeTemplates.Where(v => this.ObjectContext.EntNodeTemplateDetails.Any(detail => detail.NodeId == page.Id && detail.EntNodeTemplateId == v.Id)).ToArray();

            foreach(var entNodeTemplate in entNodeTemplates) {
                var details = entNodeTemplate.EntNodeTemplateDetails.ToArray();
                foreach(var detail in details)
                    this.DeleteFromDatabase(detail);
            }
        }

        //作废分组
        [Update(UsingCustomMethod = true)]
        [RequiresAuthentication]
        public void 作废分组(Page page) {
            //合法性校验：存在有效的下级页面，不允许作废
            var groupPage = this.ObjectContext.Pages.FirstOrDefault(v => v.ParentId == page.Id && v.Status == (int)SecurityCommonStatus.有效);
            if(groupPage != null)
                throw new ValidationException(ErrorStrings.Page_Validation);

            //1、作废分组节点，调用【窗体页面.更新窗体页面】，其中赋值：  1）状态 = 作废
            this.UpdateToDatabase(page);
            page.Status = (int)SecurityCommonStatus.作废;

            //2、根据目标Id = 当前窗体页面.窗体页面Id，查找并作废资源节点数据，调用【资源节点.更新资源节点】，其中赋值：  1）状态 = 作废
            var groupNode = this.ObjectContext.Nodes.SingleOrDefault(v => v.CategoryType == (int)SecurityNodeCategoryType.页面 && v.CategoryId == page.Id);
            if(groupNode == null)
                throw new ValidationException(string.Format(ErrorStrings.Page_Validation1, page.Name));
            groupNode.Status = (int)SecurityCommonStatus.作废;
            this.UpdateNodeValidate(groupNode);

            //3、根据节点Id = 当前窗体页面.窗体页面Id，删除授权规则，调用【授权规则.删除授权规则】
            var rules = this.ObjectContext.Rules.Where(v => v.NodeId == page.Id).ToArray();
            foreach(var rule in rules)
                this.DeleteFromDatabase(rule);

            //4、调用【企业授权模板.查询企业授权模板】，依据：  1）企业授权模板资源清单.节点Id = 当前窗体页面.窗体页面Id
            //5、对于步骤4中查询到数据，删除清单中节点Id = 当前窗体页面.窗体页面Id的数据，然后调用【企业授权模板.更新企业授权模板】
            var entNodeTemplates = this.ObjectContext.EntNodeTemplates.Where(v => this.ObjectContext.EntNodeTemplateDetails.Any(detail => detail.NodeId == page.Id && detail.EntNodeTemplateId == v.Id)).ToArray();
            foreach(var entNodeTemplate in entNodeTemplates) {
                var details = entNodeTemplate.EntNodeTemplateDetails.ToArray();
                foreach(var detail in details)
                    this.DeleteFromDatabase(detail);
            }
        }

        //作废窗体页面
        [Update(UsingCustomMethod = true)]
        [RequiresAuthentication]
        public void 作废窗体页面(Page page) {
            //1、调用【窗体操作.查询窗体操作】，依据：  1）页面Id = 当前窗体页面.窗体页面Id
            //2、对于步骤1中查询到的窗体操作，循环调用【窗体操作.作废窗体操作】
            var actions = this.ObjectContext.SecurityActions.Where(r => r.PageId == page.Id).ToArray();
            foreach(var action in actions)
                this.作废窗体操作(action);

            //3、作废页面节点，调用【窗体页面.更新窗体页面】，其中赋值：  1）状态 = 作废
            this.UpdateToDatabase(page);
            page.Status = (int)SecurityCommonStatus.作废;

            //4、根据目标Id = 当前窗体页面.窗体页面Id，查找并作废资源节点数据，调用【资源节点.更新资源节点】，其中赋值：  1）状态 = 作废
            var groupNode = this.ObjectContext.Nodes.SingleOrDefault(v => v.CategoryType == (int)SecurityNodeCategoryType.页面 && v.CategoryId == page.Id);
            if(groupNode == null)
                throw new ValidationException(string.Format(ErrorStrings.Page_Validation1, page.Name));
            groupNode.Status = (int)SecurityCommonStatus.作废;
            this.UpdateNodeValidate(groupNode);

            //5、根据节点Id = 当前窗体页面.窗体页面Id，删除授权规则，调用【授权规则.删除授权规则】
            var rules = this.ObjectContext.Rules.Where(v => v.NodeId == page.Id).ToArray();
            foreach(var rule in rules)
                this.DeleteFromDatabase(rule);

            //6、调用【企业授权模板.查询企业授权模板】，依据：  1）企业授权模板资源清单.节点Id = 当前窗体页面.窗体页面Id
            //7、对于步骤6中查询到数据，删除清单中节点Id = 当前窗体页面.窗体页面Id的数据，然后调用【企业授权模板.更新企业授权模板】
            var entNodeTemplates = this.ObjectContext.EntNodeTemplates.Where(v => this.ObjectContext.EntNodeTemplateDetails.Any(detail => detail.NodeId == page.Id && detail.EntNodeTemplateId == v.Id)).ToArray();
            foreach(var entNodeTemplate in entNodeTemplates) {
                var details = entNodeTemplate.EntNodeTemplateDetails.ToArray();
                foreach(var detail in details)
                    this.DeleteFromDatabase(detail);
            }
        }

        [RequiresAuthentication]
        public IEnumerable<Page> GetAuthorizablePages() {
            var pages = (from page in this.ObjectContext.Pages
                         where page.Id >= 1000 && page.Status == (int)SecurityCommonStatus.有效
                         select page).ToList();
            var pageToRemove = pages.Where(page => (page.Type == (int)SecurityPageType.系统 || page.Type == (int)SecurityPageType.分组) && pages.Count(p => p.ParentId == page.Id) == 0).ToArray();
            foreach(var page in pageToRemove)
                pages.Remove(page);
            return pages;
        }

        public IQueryable<Page> GetPagesWithDetail() {
            return this.ObjectContext.Pages.Include("Nodes").Include("Actions.Nodes");
        }

        public IEnumerable<Page> GetCurrentEnterprisePagesWithDetail() {
            var userInfo = this.GetCurrentUserInfo();
            var enterprise = this.ObjectContext.Enterprises.Include("EntNodeTemplate.EntNodeTemplateDetails.Node").FirstOrDefault(entity => entity.Id == userInfo.EnterpriseId);
            if(enterprise == null)
                return null;

            var template = enterprise.EntNodeTemplate;
            if(template == null)
                return null;
            var authorizedNodes = template.EntNodeTemplateDetails.Where(detail => detail.Node != null).Select(detail => detail.Node).ToList();

            // 已授权的业务操作
            var authorizedActionIds = authorizedNodes.Where(node => node.CategoryType == (int)SecurityNodeCategoryType.操作).Select(node => node.CategoryId);
            var actions = this.ObjectContext.SecurityActions.Where(action => authorizedActionIds.Contains(action.Id)).ToList();
            // 已授权的流程节点
            var pages = (from page in this.ObjectContext.Pages
                         where page.Status == (int)SecurityCommonStatus.有效
                         select page).ToList();
            var authorizedPageIds = authorizedNodes.Where(node => node.CategoryType == (int)SecurityNodeCategoryType.页面).Select(node => node.CategoryId);
            var parentPageIds = pages.Where(page => authorizedPageIds.Contains(page.Id)).Select(page => page.ParentId).ToList();
            while(parentPageIds.Any()) {
                authorizedPageIds = authorizedPageIds.Concat(parentPageIds);
                parentPageIds = pages.Where(page => parentPageIds.Contains(page.Id)).Select(page => page.ParentId).ToList();
            }
            return pages.Where(page => authorizedPageIds.Contains(page.Id));
        }
        public IEnumerable<Page> GetCurrentEnterprisePagesWithDetailById(int id) {
            var userInfo = this.GetCurrentUserInfo();
            var enterprise = this.ObjectContext.Enterprises.Include("EntNodeTemplate.EntNodeTemplateDetails.Node").FirstOrDefault(entity => entity.Id == id);
            if(enterprise == null)
                return null;

            var template = enterprise.EntNodeTemplate;
            if(template == null)
                return null;
            var authorizedNodes = template.EntNodeTemplateDetails.Where(detail => detail.Node != null).Select(detail => detail.Node).ToList();

            // 已授权的业务操作
            var authorizedActionIds = authorizedNodes.Where(node => node.CategoryType == (int)SecurityNodeCategoryType.操作).Select(node => node.CategoryId);
            var actions = this.ObjectContext.SecurityActions.Where(action => authorizedActionIds.Contains(action.Id)).ToList();
            // 已授权的流程节点
            var pages = (from page in this.ObjectContext.Pages
                         where page.Status == (int)SecurityCommonStatus.有效
                         select page).ToList();
            var authorizedPageIds = authorizedNodes.Where(node => node.CategoryType == (int)SecurityNodeCategoryType.页面).Select(node => node.CategoryId);
            var parentPageIds = pages.Where(page => authorizedPageIds.Contains(page.Id)).Select(page => page.ParentId).ToList();
            while(parentPageIds.Any()) {
                authorizedPageIds = authorizedPageIds.Concat(parentPageIds);
                parentPageIds = pages.Where(page => parentPageIds.Contains(page.Id)).Select(page => page.ParentId).ToList();
            }
            return pages.Where(page => authorizedPageIds.Contains(page.Id));
        }

        public IEnumerable<Page> GetCurrentEnterprisePagesWithDetailByRoleId(int id) {
            var role = this.ObjectContext.Roles.Single(r => r.Id == id);
            var enterprise = this.ObjectContext.Enterprises.Include("EntNodeTemplate.EntNodeTemplateDetails.Node").FirstOrDefault(entity => entity.Id == role.EnterpriseId);
            if(enterprise == null)
                return null;

            var template = enterprise.EntNodeTemplate;
            if(template == null)
                return null;
            var authorizedNodes = template.EntNodeTemplateDetails.Where(detail => detail.Node != null).Select(detail => detail.Node).ToList();

            // 已授权的业务操作
            var authorizedActionIds = authorizedNodes.Where(node => node.CategoryType == (int)SecurityNodeCategoryType.操作).Select(node => node.CategoryId);
            var actions = this.ObjectContext.SecurityActions.Where(action => authorizedActionIds.Contains(action.Id)).ToList();
            // 已授权的流程节点
            var pages = (from page in this.ObjectContext.Pages
                         where page.Status == (int)SecurityCommonStatus.有效
                         select page).ToList();
            var authorizedPageIds = authorizedNodes.Where(node => node.CategoryType == (int)SecurityNodeCategoryType.页面).Select(node => node.CategoryId);
            var parentPageIds = pages.Where(page => authorizedPageIds.Contains(page.Id)).Select(page => page.ParentId).ToList();
            while(parentPageIds.Any()) {
                authorizedPageIds = authorizedPageIds.Concat(parentPageIds);
                parentPageIds = pages.Where(page => parentPageIds.Contains(page.Id)).Select(page => page.ParentId).ToList();
            }
            return pages.Where(page => authorizedPageIds.Contains(page.Id));
        }
    }
}
