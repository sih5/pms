﻿using System.Linq;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        public PasswordPolicy GetDefaultPasswordPolicy() {
            return this.ObjectContext.PasswordPolicies.Where(v => v.IsEnabled).OrderBy(v => v.Id).FirstOrDefault();
        }
    }
}
