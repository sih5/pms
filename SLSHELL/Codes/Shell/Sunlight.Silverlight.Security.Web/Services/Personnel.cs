﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Security.Web.Resources;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        private static string HashPassword(string password) {
            if(string.IsNullOrEmpty(password))
                throw new ValidationException(ErrorStrings.Personnel_Validation9);
            var data = Encoding.UTF8.GetBytes(password);
            using(var sha1 = new SHA1Managed())
                data = sha1.ComputeHash(data);
            var sbResult = new StringBuilder(data.Length * 2);
            foreach(var b in data)
                sbResult.AppendFormat("{0:X2}", b);
            return sbResult.ToString();
        }

        [RequiresAuthentication]
        public IQueryable<Personnel> GetUsersBy(int id, int enterpriseId, string loginId, string name, string cellNumber, int status) {
            IQueryable<Personnel> result = this.ObjectContext.Personnels;
            if(id > 0)
                result = result.Where(r => r.Id == id);
            if(enterpriseId > 0)
                result = result.Where(r => r.EnterpriseId == enterpriseId);
            if(!string.IsNullOrEmpty(loginId))
                result = result.Where(r => r.LoginId.ToLower().Contains(loginId.ToLower()));
            if(!string.IsNullOrEmpty(name))
                result = result.Where(r => r.Name.ToLower().Contains(name.ToLower()));
            if(!string.IsNullOrEmpty(cellNumber))
                result = result.Where(r => r.CellNumber.ToLower().Contains(cellNumber.ToLower()));
            if(status >= 0)
                result = result.Where(r => r.Status == status);

            return result.Include("OrganizationPersonnels").Include("RolePersonnels");
        }

        //新增
        [RequiresAuthentication]
        public void InsertUser(Personnel user) {
            //在ObjectContext中登记，指明主单将要新增至数据库
            this.InsertToDatabase(user);

            //var userInfo = this.GetCurrentUserInfo();
            //user.EnterpriseId = userInfo.EnterpriseId;   //人员.企业Id为人员所属企业，与创建人所属企业一致

            //校验实体对象并做默认处理
            this.InsertUserValidate(user);
        }

        //针对新增操作的数据校验及默认操作
        private void InsertUserValidate(Personnel user) {
            //业务规则检查1：数据库中CellNumber必须唯一
            if(!string.IsNullOrEmpty(user.CellNumber)) {
                var conflictUser = this.ObjectContext.Personnels.Where(a => a.CellNumber.ToLower() == user.CellNumber.ToLower() && a.Status != (int)SecurityUserStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                if(conflictUser != null)
                    //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
                    //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
                    throw new ValidationException(string.Format(ErrorStrings.Personnel_Validation1, user.CellNumber));
            }
            //业务规则检查2：数据库中企业Id、登录Id必须组合唯一
            var conflictUser2 = this.ObjectContext.Personnels.Where(a => a.EnterpriseId == user.EnterpriseId && a.LoginId == user.LoginId && a.Status != (int)SecurityUserStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(conflictUser2 != null)
                //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
                //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
                throw new ValidationException(string.Format(ErrorStrings.Personnel_Validation2, user.LoginId));

            //更新创建人等数据
            var userInfo = this.GetCurrentUserInfo();
            //user.EnterpriseId = userInfo.EnterpriseId;   人员.企业Id为人员所属企业，非创建人所属企业
            user.CreatorId = userInfo.Id;
            user.CreatorName = userInfo.Name;
            user.CreateTime = DateTime.Now;
            user.PasswordModifyTime = DateTime.Now;
        }

        //更新
        [RequiresAuthentication]
        public void UpdateUserByClient(Personnel user) {
            this.UpdateToDatabase(user);

            this.UpdateUserValidate(user);
        }

        //针对更新操作的数据校验及默认操作
        private void UpdateUserValidate(Personnel user) {
            //更新修改人等数据
            var userInfo = this.GetCurrentUserInfo();
            user.ModifierId = userInfo.Id;
            user.ModifierName = userInfo.Name;
            user.ModifyTime = DateTime.Now;
        }

        [RequiresAuthentication]
        public IQueryable<Personnel> GetPersonnelsByOrganizationId(int organizationId) {
            return this.ObjectContext.Personnels.Where(v => v.OrganizationPersonnels.Any(v1 => v1.OrganizationId == organizationId));
        }

        [RequiresAuthentication]
        public IQueryable<Personnel> GetPersonnelsByRoleId(int roleId) {
            return this.ObjectContext.Personnels.Where(v => v.RolePersonnels.Any(v1 => v1.RoleId == roleId));
        }

        [RequiresAuthentication]
        public IQueryable<Personnel> GetPersonnelsWithDetail() {
            return this.ObjectContext.Personnels.Include("Enterprise").Include("OrganizationPersonnels").Include("RolePersonnels").OrderBy(v => v.Id);
        }

        [RequiresAuthentication]
        public IQueryable<Personnel> GetPersonnelsWithOrganizationAndRole() {
            return this.ObjectContext.Personnels.Include("Enterprise").Include("OrganizationPersonnels.Organization").Include("RolePersonnels.Role").OrderBy(v => v.Id);
        }

        [RequiresAuthentication]
        public IQueryable<Personnel> 获取企业管理员(int enterpriseId) {
            return this.ObjectContext.Personnels.Where(v => this.ObjectContext.RolePersonnels.Any(rp => rp.PersonnelId == v.Id && this.ObjectContext.Roles.Any(role => role.Id == rp.RoleId && role.IsAdmin && role.EnterpriseId == enterpriseId)));
        }

        [RequiresAuthentication]
        public void 调整人员(Personnel personnel) {
            var dbPersonnel = this.ObjectContext.Personnels.Where(v => v.Id == personnel.Id && (v.Status == (int)SecurityUserStatus.有效 || v.Status == (int)SecurityUserStatus.冻结)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPersonnel == null)
                throw new ValidationException(ErrorStrings.Personnel_Validation3);
            personnel.Password = dbPersonnel.Password;
            if(String.Compare(personnel.CellNumber, dbPersonnel.CellNumber, StringComparison.OrdinalIgnoreCase) != 0) {
                if(this.ObjectContext.Personnels.Any(v => v.CellNumber.ToLower() == personnel.CellNumber.ToLower() && v.Status != (int)SecurityUserStatus.作废))
                    throw new ValidationException(string.Format(ErrorStrings.Personnel_Validation1, personnel.CellNumber));
            }
            this.UpdateToDatabase(personnel);
        }

        [RequiresAuthentication]
        public void 作废人员(Personnel personnel) {
            var dbPersonnel = this.ObjectContext.Personnels.Where(v => v.Id == personnel.Id && (v.Status == (int)SecurityUserStatus.有效 || v.Status == (int)SecurityUserStatus.冻结)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPersonnel == null)
                throw new ValidationException(ErrorStrings.Personnel_Validation4);
            this.UpdateToDatabase(personnel);
            personnel.Status = (int)SecurityUserStatus.作废;
            this.UpdateUserValidate(personnel);
            var userLoginInfo = this.ObjectContext.UserLoginInfoes.Where(r => r.EnterpriseCode.ToUpper() == personnel.Enterprise.Code.ToUpper() && r.LoginId == personnel.LoginId).ToArray();
            if(userLoginInfo.Any())
                foreach(var loginInfo in userLoginInfo) {
                    DeleteFromDatabase(loginInfo);
                }
        }

        [RequiresAuthentication]
        public void 冻结人员(Personnel personnel) {
            var dbPersonnel = this.ObjectContext.Personnels.Where(v => v.Id == personnel.Id && v.Status == (int)SecurityUserStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPersonnel == null)
                throw new ValidationException(ErrorStrings.Personnel_Validation5);
            personnel.Password = dbPersonnel.Password;
            this.UpdateToDatabase(personnel);
            personnel.Status = (int)SecurityUserStatus.冻结;
            this.UpdateUserValidate(personnel);
        }

        [RequiresAuthentication]
        public void 恢复人员(Personnel personnel) {
            var dbPersonnel = this.ObjectContext.Personnels.Where(v => v.Id == personnel.Id && v.Status == (int)SecurityUserStatus.冻结).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPersonnel == null)
                throw new ValidationException(ErrorStrings.Personnel_Validation6);
            personnel.Password = dbPersonnel.Password;
            this.UpdateToDatabase(personnel);
            personnel.Status = (int)SecurityUserStatus.有效;
            this.UpdateUserValidate(personnel);
        }

        [RequiresAuthentication]
        public void 设置人员默认密码(Personnel personnel) {
            var passwordPolicy = this.GetDefaultPasswordPolicy();
            if(passwordPolicy == null)
                throw new ValidationException(ErrorStrings.Personnel_Validation10);
            personnel.PasswordModifyTime = DateTime.Now;
            personnel.Password = passwordPolicy.DefaultPassword;
            //新增操作，此处不需要重复作新增BO的校验
        }

        /// <summary>
        /// 管理员修改用户的密码
        /// </summary>
        /// <param name="personnel">被修改的用户</param>
        /// <param name="password">新密码</param>
        /// <param name="needSendEmail">是否需要发送邮件</param>
        /// <param name="originalPassword">原密码</param>
        [RequiresAuthentication]
        public void 修改人员密码(Personnel personnel, string password, bool needSendEmail, string originalPassword) {
            //客户端存在新增、更新两种操作
            if(this.ChangeSet.GetChangeOperation(personnel) == ChangeOperation.Update) {
                this.UpdateToDatabase(personnel);
                var oldPersonnel = this.ObjectContext.Personnels.Where(v => v.Id == personnel.Id && v.Status == (int)SecurityUserStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
                if(oldPersonnel == null)
                    throw new ValidationException(ErrorStrings.Personnel_Validation7);
                if(!string.IsNullOrEmpty(originalPassword) && oldPersonnel.Password != originalPassword)
                    throw new ValidationException(ErrorStrings.Personnel_Validation8);
            }
            personnel.PasswordModifyTime = DateTime.Now;
            //user.EnterpriseId = userInfo.EnterpriseId;   人员.企业Id为人员所属企业，非创建人所属企业
            personnel.Password = HashPassword(password);
            this.UpdateUserValidate(personnel);
            //此处不需要重复作BO的校验
            if(needSendEmail) {
                //TODO：发送邮件给当前人员.Email
            }
        }



        /// <summary>
        /// 管理员修改用户的密码
        /// </summary>
        /// <param name="personnel">被修改的用户</param>
        /// <param name="password">新密码</param>
        /// <param name="needSendEmail">是否需要发送邮件</param>
        /// <param name="originalPassword">原密码</param>
        [RequiresAuthentication]
        public void 首次修改人员密码(Personnel personnel, string password, bool needSendEmail, string originalPassword) {
            修改人员密码(personnel, password, needSendEmail, originalPassword);

            var userLoginInfos = this.ObjectContext.UserLoginInfoes.Where(r => r.LoginId == personnel.LoginId && r.EnterpriseCode == personnel.Enterprise.Code);
            if(!userLoginInfos.Any()) {
                throw new ValidationException(string.Format("不存在{0}企业下{1}的登录信息，请联系管理员！", personnel.Enterprise.Code, personnel.LoginId));
            } else if(userLoginInfos.Count() > 1) {
                throw new ValidationException(string.Format("存在多条{0}企业下{1}的登录信息，请联系管理员！", personnel.Enterprise.Code, personnel.LoginId));
            } else {
                var userLoginInfo = userLoginInfos.First();
                userLoginInfo.IsFisrt = false;
                this.UpdateToDatabase(userLoginInfo);
            }
        }

        [RequiresAuthentication]
        public IQueryable<Personnel> GetPersonnelsByOrganizationIds(int[] organizationIds) {
            return this.ObjectContext.Personnels.Where(v => v.OrganizationPersonnels.Any(v1 => organizationIds.Contains(v1.OrganizationId)));
        }

        /// <summary>
        /// 当前登录用户修改自己的密码
        /// </summary>
        /// <param name="oldPassword">原密码，已Hash</param>
        /// <param name="newPassword">新密码，已Hash</param>
        [RequiresAuthentication, Invoke]
        public void ChangePassword(string oldPassword, string newPassword) {
            var personnelId = this.GetCurrentUserInfo().Id;
            var personnel = this.ObjectContext.Personnels.SingleOrDefault(v => v.Id == personnelId && v.Status == (int)SecurityUserStatus.有效);
            if(personnel == null)
                throw new ValidationException(ErrorStrings.Personnel_Validation7);
            if(!string.IsNullOrEmpty(oldPassword) && personnel.Password != oldPassword)
                throw new ValidationException(ErrorStrings.Personnel_Validation8);

            personnel.PasswordModifyTime = DateTime.Now;
            personnel.Password = newPassword;
            this.ObjectContext.SaveChanges();
        }
    }
}
