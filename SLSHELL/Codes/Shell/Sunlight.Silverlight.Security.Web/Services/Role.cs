﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Security.Web.Resources;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        [RequiresAuthentication]
        public IQueryable<Role> GetRolesBy(int id, int enterpriseId, string name, bool? isAdmin, int status) {
            IQueryable<Role> result = this.ObjectContext.Roles;
            if(id > 0)
                result = result.Where(r => r.Id == id);
            if(enterpriseId > 0)
                result = result.Where(r => r.EnterpriseId == enterpriseId);
            if(!string.IsNullOrEmpty(name))
                result = result.Where(r => r.Name.ToLower().Contains(name.ToLower()));
            if(isAdmin.HasValue)
                result = result.Where(r => r.IsAdmin == isAdmin.Value);
            if(status >= 0)
                result = result.Where(r => r.Status == status);
            return result.Include("RolePersonnels").Include("Rules");
        }

        [RequiresAuthentication]
        public IQueryable<Role> GetRolesWithDetail() {
            return this.ObjectContext.Roles.Include("RolePersonnels").OrderBy(v => v.Id);
        }

        //新增
        [RequiresAuthentication]
        public void InsertRole(Role role) {
            this.InsertToDatabase(role);

            var userInfo = this.GetCurrentUserInfo();
            //role.EnterpriseId = userInfo.EnterpriseId;   //角色.企业Id为角色所属企业，与创建人所属企业一致

            this.InsertRoleValidate(role);
        }

        //针对新增操作的数据校验及默认操作
        private void InsertRoleValidate(Role role) {
            var userInfo = this.GetCurrentUserInfo();

            //role.EnterpriseId = userInfo.EnterpriseId;  角色.企业Id为角色所属企业，非创建人所属企业
            role.CreatorId = userInfo.Id;
            role.CreatorName = userInfo.Name;
            role.CreateTime = DateTime.Now;
        }

        //更新
        [RequiresAuthentication]
        public void UpdateRole(Role role) {
            this.UpdateToDatabase(role);

            this.UpdateRoleValidate(role);
        }

        //针对更新操作的数据校验及默认操作
        private void UpdateRoleValidate(Role role) {
            var userInfo = this.GetCurrentUserInfo();
            role.ModifierId = userInfo.Id;
            role.ModifierName = userInfo.Name;
            role.ModifyTime = DateTime.Now;
        }

        [RequiresAuthentication]
        public IQueryable<Role> GetAuthorizableRoles() {
            return from role in this.ObjectContext.Roles
                   where !role.IsAdmin && role.Status == (int)SecurityRoleStatus.有效
                   select role;
        }

        [RequiresAuthentication]
        public void 创建角色(Role role) {
            //同一企业下，角色名称不允许重复
            var conflictRoleName = this.ObjectContext.Roles.Where(a => a.EnterpriseId == role.EnterpriseId && a.Name.ToLower() == role.Name.ToLower()).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(conflictRoleName != null)
                throw new ValidationException(string.Format(ErrorStrings.Role_Validation1, role.Name));

            //同一企业下，管理员角色不允许多个
            if(role.IsAdmin) {
                var conflictRoleAdmin = this.ObjectContext.Roles.Where(a => a.EnterpriseId == role.EnterpriseId && a.IsAdmin).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                if(conflictRoleAdmin != null)
                    throw new ValidationException(string.Format(ErrorStrings.Role_Validation2));
            }
        }

        [RequiresAuthentication]
        public void 调整角色(Role role) {
            var oldRole = this.ChangeSet.GetOriginal(role);
            if(oldRole != null) {
                if(oldRole.Status != (int)SecurityRoleStatus.有效)
                    throw new ValidationException(ErrorStrings.Role_Validation3);
                if(role.EnterpriseId != oldRole.EnterpriseId || String.Compare(role.Name, oldRole.Name, StringComparison.OrdinalIgnoreCase) != 0) {
                    var conflictRoleName = this.ObjectContext.Roles.Where(a => a.EnterpriseId == role.EnterpriseId && a.Name.ToLower() == role.Name.ToLower()).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                    if(conflictRoleName != null)
                        throw new ValidationException(string.Format(ErrorStrings.Role_Validation1, role.Name));
                }
            }
            if(role.EntityState == EntityState.Detached)
                this.ObjectContext.Roles.Attach(role);
            if(role.Rules.GroupBy(v => v.NodeId).Any(v => v.Count() > 1))
                throw new ValidationException(string.Format(ErrorStrings.Role_Validation7));
        }

        [RequiresAuthentication]
        public void 冻结角色(Role role) {
            var dbRole = this.ObjectContext.Roles.Where(v => v.Id == role.Id && v.Status == (int)SecurityRoleStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbRole == null)
                throw new ValidationException(ErrorStrings.Role_Validation5);
            this.UpdateToDatabase(role);
            role.Status = (int)SecurityRoleStatus.冻结;
            this.UpdateRoleValidate(role);
        }

        [RequiresAuthentication]
        public void 恢复角色(Role role) {
            var dbRole = this.ObjectContext.Roles.Where(v => v.Id == role.Id && v.Status == (int)SecurityRoleStatus.冻结).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbRole == null)
                throw new ValidationException(ErrorStrings.Role_Validation6);
            this.UpdateToDatabase(role);
            role.Status = (int)SecurityRoleStatus.有效;
            this.UpdateRoleValidate(role);
        }

        [RequiresAuthentication]
        public IQueryable<Role> GetRolesWithEnterprise() {
            return this.ObjectContext.Roles.Include("Enterprise").Include("Enterprise.EnterpriseCategory").Include("Enterprise.EntNodeTemplate").OrderBy(v => v.Id); ;
        }
    }
}
