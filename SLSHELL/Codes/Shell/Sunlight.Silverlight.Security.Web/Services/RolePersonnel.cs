﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Security.Web.Resources;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        [RequiresAuthentication]
        public IQueryable<RolePersonnel> GetRoleUsersBy(int userId, int roleId) {
            IQueryable<RolePersonnel> result = this.ObjectContext.RolePersonnels;
            if(userId > 0)
                result = result.Where(r => r.PersonnelId == userId);
            if(roleId > 0)
                result = result.Where(r => r.RoleId == roleId);
            return result;
        }

        //新增
        [RequiresAuthentication]
        public void InsertRoleUser(RolePersonnel roleUser) {
            //在ObjectContext中登记，指明主单将要新增至数据库
            this.InsertToDatabase(roleUser);

            //校验实体对象并做默认处理
            this.InsertRoleUserValidate(roleUser);
        }

        //针对新增操作的数据校验及默认操作
        private void InsertRoleUserValidate(RolePersonnel roleUser) {
            //业务规则检查1：数据库中UserId/RoleId必须组合唯一
            var conflictRoleUser = this.ObjectContext.RolePersonnels.Where(a => a.PersonnelId == roleUser.PersonnelId && a.RoleId == roleUser.RoleId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(conflictRoleUser != null)
                //错误信息需要放到资源文件ErrorStrings.resx中以便本地化，命名规则：[实体名（当前代码文件名）]_Validation[序号]
                //异常的错误类型必须是 System.ComponentModel.DataAnnotations.ValidationException
                throw new ValidationException(string.Format(ErrorStrings.RolePersonnel_Validation1));
        }

        //删除
        [RequiresAuthentication]
        public void DeleteRoleUser(RolePersonnel roleUser) {
            //在ObjectContext中登记，指明主单将要从数据库删除
            this.DeleteFromDatabase(roleUser);

            //校验实体对象并做默认处理
            this.DeleteRoleUserValidate(roleUser);
        }

        //如果该角色是管理员角色，则删除之后必须还有其他人员绑定该角色
        private void DeleteRoleUserValidate(RolePersonnel roleUser) {
            var dbRole = this.ObjectContext.Roles.Where(v => v.Id == roleUser.RoleId && v.IsAdmin).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbRole != null) {
                var dbRoleUser = this.ObjectContext.RolePersonnels.Where(a => a.RoleId == roleUser.RoleId && a.PersonnelId != roleUser.PersonnelId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                if(dbRoleUser == null)
                    throw new ValidationException(ErrorStrings.RolePersonnel_Validation);
            }
        }
    }
}
