﻿using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Security.Web.Resources;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        private void InsertRoleTemplateValidate(RoleTemplate roleTemplate) {
            var conflictRoleTemplate = this.ObjectContext.RoleTemplates.Where(v => v.Name.ToLower() == roleTemplate.Name.ToLower() && v.Status == (int)SecurityCommonStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(conflictRoleTemplate != null)
                throw new ValidationException(string.Format(ErrorStrings.RoleTemplate_Validation1, roleTemplate.Name));
            var userInfo = this.GetCurrentUserInfo();
            roleTemplate.EnterpriseId = userInfo.EnterpriseId;
            roleTemplate.CreatorId = userInfo.Id;
            roleTemplate.CreatorName = userInfo.Name;
            roleTemplate.CreateTime = DateTime.Now;
        }

        private void UpdateRoleTemplateValidate(RoleTemplate roleTemplate) {
            var dbRoleTemplate = this.ObjectContext.RoleTemplates.Where(v => v.Id == roleTemplate.Id && v.Status == (int)SecurityCommonStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbRoleTemplate == null)
                throw new ValidationException(ErrorStrings.RoleTemplate_Validation2);
            if(string.Compare(dbRoleTemplate.Name, roleTemplate.Name, StringComparison.OrdinalIgnoreCase) != 0) {
                var conflictRoleTemplate = this.ObjectContext.RoleTemplates.Where(v => v.Name.ToLower() == roleTemplate.Name.ToLower() && v.Status == (int)SecurityCommonStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                if(conflictRoleTemplate != null)
                    throw new ValidationException(string.Format(ErrorStrings.RoleTemplate_Validation1, roleTemplate.Name));
            }
            var userInfo = this.GetCurrentUserInfo();
            roleTemplate.ModifierId = userInfo.Id;
            roleTemplate.ModifierName = userInfo.Name;
            roleTemplate.ModifyTime = DateTime.Now;
        }

        [RequiresAuthentication]
        public void InsertRoleTemplate(RoleTemplate roleTemplate) {
            this.InsertToDatabase(roleTemplate);
            var entNodeTemplateDetails = this.ChangeSet.GetAssociatedChanges(roleTemplate, v => v.RoleTemplateRules, ChangeOperation.Insert);
            foreach(RoleTemplateRule entNodeTemplateDetail in entNodeTemplateDetails)
                this.InsertToDatabase(entNodeTemplateDetail);
            this.InsertRoleTemplateValidate(roleTemplate);
        }

        [RequiresAuthentication]
        public void UpdateRoleTemplate(RoleTemplate roleTemplate) {
            roleTemplate.RoleTemplateRules.Clear();
            this.UpdateToDatabase(roleTemplate);
            var entNodeTemplateDetails = this.ChangeSet.GetAssociatedChanges(roleTemplate, v => v.RoleTemplateRules);
            foreach(RoleTemplateRule entNodeTemplateDetail in entNodeTemplateDetails) {
                switch(this.ChangeSet.GetChangeOperation(entNodeTemplateDetail)) {
                    case ChangeOperation.Insert:
                        this.InsertToDatabase(entNodeTemplateDetail);
                        break;
                    case ChangeOperation.Update:
                        this.UpdateToDatabase(entNodeTemplateDetail);
                        break;
                    case ChangeOperation.Delete:
                        this.DeleteFromDatabase(entNodeTemplateDetail);
                        break;
                }
            }
            this.UpdateRoleTemplateValidate(roleTemplate);
        }

        public IQueryable<RoleTemplate> GetRoleTemplatesWithDetailById(int id) {
            return this.ObjectContext.RoleTemplates.Where(v => v.Id == id).Include("RoleTemplateRules").Include("RoleTemplateRules.Node");
        }
    }
}