﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Shell.Web;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        [RequiresAuthentication]
        public IQueryable<Rule> GetRulesBy(int id, int[] roleId, int nodeId) {
            IQueryable<Rule> result = this.ObjectContext.Rules;
            if(id > 0)
                result = result.Where(r => r.Id == id);
            if(roleId != null && roleId.Length > 0)
                result = result.Where(v => roleId.Contains(v.RoleId));
            if(nodeId > 0)
                result = result.Where(r => r.NodeId == nodeId);
            return result;
        }

        //新增
        [RequiresAuthentication]
        public void InsertRule(Rule rule) {
            this.InsertToDatabase(rule);
            this.InsertRuleValidate(rule);
        }

        //针对新增操作的数据校验及默认操作
        private void InsertRuleValidate(Rule rule) {
            //大量新增授权规则，多次查询数据库效率低下，考虑到目前该校验仅在“调整角色”时需要，因此将校验移至“调整角色”处
            //数据库中RoleId/NodeId必须组合唯一
            //var dbRule = this.ObjectContext.Rules.Where(a => a.RoleId == rule.RoleId && a.NodeId == rule.NodeId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            //if(dbRule != null)
            //    throw new ValidationException(string.Format(ErrorStrings.Rule_Validation1));
            

            RuleRecord rr = new RuleRecord();
            rr.NodeId = rule.NodeId;
            rr.RoleId = rule.RoleId;
            //rulerecord.status 没有字典项,用1代表新增
            rr.Status = 1;
            this.InsertRuleRecord(rr);
        }

        //删除
        [RequiresAuthentication]
        public void DeleteRule(Rule rule) {

            RuleRecord rr = new RuleRecord();
            rr.NodeId = rule.NodeId;
            rr.RoleId = rule.RoleId;
            //rulerecord.status 没有字典项,用4代表删除
            rr.Status = 4;
            this.InsertRuleRecord(rr);
            this.DeleteFromDatabase(rule);
        }

        public IQueryable<Rule> GetRulesByRole(int roleId) {
            return this.ObjectContext.Rules.Where(entity => entity.RoleId == roleId).Include("Node");
        }

        [RequiresAuthentication, Invoke]
        public void 批量新增授权规则(int[] roleIds, int[] nodeIds) {
            #region
            //var roleNodeIds = (from a in this.ObjectContext.Roles.Where(r => roleIds.Contains(r.Id)).ToArray()
            //                   join b in this.ObjectContext.Enterprises on a.EnterpriseId equals b.Id into t1
            //                   from c in t1
            //                   join d in this.ObjectContext.EntNodeTemplateDetails.Where(r => nodeIds.Contains(r.NodeId)) on c.EntNodeTemplateId equals d.EntNodeTemplateId into t2
            //                   from e in t2
            //                   select new {
            //                       EntNodeTemplateId = c.EntNodeTemplateId ?? 0,
            //                       e.NodeId
            //                   }).Distinct().ToArray();
            //var roleNodeIds1 = (from a in this.ObjectContext.Roles.Where(r => roleIds.Contains(r.Id)).ToArray()
            //                    join b in this.ObjectContext.Enterprises on a.EnterpriseId equals b.Id into t1
            //                    from c in t1
            //                    from d in nodeIds
            //                    select new {
            //                        EntNodeTemplateId = c.EntNodeTemplateId ?? 0,
            //                        NodeId = d
            //                    }).Distinct().ToArray();
            //foreach(var entNodeTemplateDetail in roleNodeIds1.Except(roleNodeIds).Select(roleNodeId => new EntNodeTemplateDetail {
            //    NodeId = roleNodeId.NodeId,
            //    EntNodeTemplateId = roleNodeId.EntNodeTemplateId
            //})) {
            //    this.InsertToDatabase(entNodeTemplateDetail);
            //}



            //var ruleTemp = from roleId in roleIds
            //               from nodeId in nodeIds
            //               select new {
            //                   RoleId = roleId,
            //                   NodeId = nodeId
            //               };
            //var rules = this.ObjectContext.Rules.Select(r => new {
            //    r.RoleId,
            //    r.NodeId
            //});

            //foreach(var t in ruleTemp.Except(rules).Select(rule => new Rule {
            //    RoleId = rule.RoleId,
            //    NodeId = rule.NodeId
            //})) {
            //    this.InsertToDatabase(t);
            //    this.InsertRuleValidate(t);
            //}
            #endregion

            var combinations = (from a in roleIds
                               from b in nodeIds
                               select new {
                                   roleIdOfCombination = a,
                                   nodeIdOfCombination = b
                               }).ToArray();
            var arrayOfDbcombinations = (from a in ObjectContext.Rules
                                 where nodeIds.Contains(a.NodeId)
                                 where roleIds.Contains(a.RoleId)
                                 select a).ToArray();
            var combinationsNeedInsert = combinations.Where(r => !arrayOfDbcombinations.Any(v => v.NodeId == r.nodeIdOfCombination && v.RoleId == r.roleIdOfCombination)).ToArray();
            foreach(var item in combinationsNeedInsert) {
                var tempRule = new Rule();
                tempRule.NodeId = item.nodeIdOfCombination;
                tempRule.RoleId = item.roleIdOfCombination;
                this.InsertRule(tempRule);

            }

            this.ObjectContext.SaveChanges();
        }

        [RequiresAuthentication, Invoke]
        public void 批量删除授权规则(int[] roleids, int[] nodeIds) {
            //
     
            var sql = String.Format("delete from Rule where RoleId in ({0}) and NodeId in ({1})", string.Join(",", roleids), string.Join(",", nodeIds));
            this.ObjectContext.ExecuteStoreCommand(sql);
        }
    }
}
