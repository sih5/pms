﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Security.Web.Resources;
using Sunlight.Silverlight.Shell.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        /// <summary>
        /// 查询授权规则履历
        /// </summary>
        /// <param name="enterprisecode">企业编号</param>
        /// <param name="enterprisename">企业名称</param>
        /// <param name="rolename">角色名称</param>
        /// <param name="nodename">节点名称</param>
        /// <param name="actionname">操作名称</param>
        /// <returns></returns>

        [RequiresAuthentication]
        public IQueryable<VirtualRuleRecord> 查询授权规则履历(string enterprisecode, string enterprisename, string rolename, string nodename, string actionname, int? status, string creatorname, DateTime? startcreatetime, DateTime? endcreatetime) {
            //CategoryType 无字典项
            var Node1Query = from nd1 in ObjectContext.Nodes.Where(n => n.CategoryType == 0)
                             from pg1 in ObjectContext.Pages
                             where nd1.CategoryId == pg1.Id
                             select new {
                                 NodeId1 = nd1.Id,
                                 NodeCategoryType1 = nd1.CategoryType,
                                 PageName1 = pg1.Name
                             };

            var Node2Query = from nd1 in ObjectContext.Nodes.Where(n => n.CategoryType == 1)
                             join ac in ObjectContext.SecurityActions on nd1.CategoryId equals ac.Id
                             join pg1 in ObjectContext.Pages on ac.PageId equals pg1.Id
                             select new {
                                 NodeId2 = nd1.Id,
                                 NodeCategoryType2 = nd1.CategoryType,
                                 ActionName2 = ac.Name,
                                 pageName2 = pg1.Name
                             };

            var roleRecordv1list1 = from rur in ObjectContext.RuleRecords
                                    join ro in ObjectContext.Roles on rur.RoleId equals ro.Id
                                    join et in ObjectContext.Enterprises on ro.EnterpriseId equals et.Id
                                    join nd in ObjectContext.Nodes on rur.NodeId equals nd.Id
                                    join nd1 in Node1Query on nd.Id equals nd1.NodeId1 into tnd1
                                    from t1 in tnd1.DefaultIfEmpty()
                                    join nd2 in Node2Query on rur.NodeId equals nd2.NodeId2 into tnd2
                                    from t2 in tnd2.DefaultIfEmpty()
                                    select new VirtualRuleRecord {
                                        Id = rur.Id,
                                        Status = rur.Status,
                                        EnterpriseCode = et.Code,
                                        EnterpriseName = et.Name,
                                        RoleName = ro.Name,
                                        NodeName = (int?)nd.CategoryType == null ? "" : (nd.CategoryType == 0 ? t1.PageName1 : (nd.CategoryType == 1 ? t2.pageName2 : "")),
                                        ActionName = (int?)nd.CategoryType == null ? "" : (nd.CategoryType == 0 ? "" : (nd.CategoryType == 1 ? t2.ActionName2 : "")),
                                        CreatorName = rur.CreatorName,
                                        CreateTime = rur.CreateTime
                                    };
            if(!string.IsNullOrEmpty(enterprisecode)) {
                roleRecordv1list1 = roleRecordv1list1.Where(list1 => list1.EnterpriseCode == enterprisecode);
            }
            if(!string.IsNullOrEmpty(enterprisename)) {
                roleRecordv1list1 = roleRecordv1list1.Where(list1 => list1.EnterpriseName == enterprisename);
            }
            if(!string.IsNullOrEmpty(rolename)) {
                roleRecordv1list1 = roleRecordv1list1.Where(list1 => list1.RoleName == rolename);
            }
            if(!string.IsNullOrEmpty(nodename)) {
                roleRecordv1list1 = roleRecordv1list1.Where(list1 => list1.NodeName == nodename);
            }
            if(!string.IsNullOrEmpty(actionname)) {
                roleRecordv1list1 = roleRecordv1list1.Where(list1 => list1.ActionName == actionname);
            }
            if(status.HasValue) {
                roleRecordv1list1 = roleRecordv1list1.Where(list1 => list1.Status == status);
            }
            if(!string.IsNullOrEmpty(creatorname)) {
                roleRecordv1list1 = roleRecordv1list1.Where(list1 => list1.CreatorName == creatorname);
            }
            if(startcreatetime.HasValue) {
                roleRecordv1list1 = roleRecordv1list1.Where(list1 => list1.CreateTime >= startcreatetime);
            }
            if(endcreatetime.HasValue) {
                roleRecordv1list1 = roleRecordv1list1.Where(list1 => list1.CreateTime <= endcreatetime);
            }

            return roleRecordv1list1.OrderBy(r=> r.Id);
        }

        private void InsertRuleRecordValidate(RuleRecord ruleRecord) {
            //var dbruleRecord = ObjectContext.RuleRecords.Where(r => r.Id == ruleRecord.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            //if(dbruleRecord != null)
            //    throw new ValidationException(ErrorStrings.Rule_Validation1);
            var userInfo = this.GetCurrentUserInfo();
            ruleRecord.CreatorId = userInfo.Id;
            ruleRecord.CreatorName = userInfo.Name;
            ruleRecord.CreateTime = DateTime.Now;
        }


        //新增授权规则履历
        public void InsertRuleRecord(RuleRecord ruleRecord) {
            //在ObjectContext中登记，指明主单将要新增至数据库
            this.InsertToDatabase(ruleRecord);
            //校验实体对象并做默认处理
            InsertRuleRecordValidate(ruleRecord);
        }

        [RequiresAuthentication]
        public string ExportRuleRecordForReport(string enterprisecode, string enterprisename, string rolename, string nodename, string actionname, int? status, string creatorname, DateTime? startcreatetime, DateTime? endcreatetime) {
            string returnfilename = GetExportFilePath(string.Format("授权规则履历导出_{0}.xls", DateTime.Now.ToString("ddHHmmssffff")));
            var excelColumns = new List<string>();
            var ruleRecorddata = this.查询授权规则履历(enterprisecode, enterprisename, rolename, nodename, actionname, status, creatorname, startcreatetime, endcreatetime);

            var dataArray = ruleRecorddata.ToArray();

            if(dataArray.Any()) {
                excelColumns.Add("操作类型");
                excelColumns.Add("企业编号");
                excelColumns.Add("企业名称");
                excelColumns.Add("角色名称");
                excelColumns.Add("节点名称");
                excelColumns.Add("操作名称");
                excelColumns.Add("授权人员");
                excelColumns.Add("授权时间");

                using(var excelExport = new ExcelExport(returnfilename)) {
                    excelExport.ExportByRow(index => {
                        if(index == dataArray.Count() + 1)
                            return null;

                        if(index == 0)
                            return excelColumns.ToArray<Object>();
                        var detail = dataArray[index - 1];

                        var values = new object[] {
                            detail.Status==1 ? "新建":(detail.Status==4 ? "删除":"" ),
                            detail.EnterpriseCode,
                            detail.EnterpriseName,
                            detail.RoleName,
                            detail.NodeName,
                            detail.ActionName,
                            detail.CreatorName,
                            detail.CreateTime
                        };
                        return values;
                    });
                }
            }
            return returnfilename;
        }

    }
}