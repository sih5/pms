﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Web;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainService {
        public IQueryable<UserLoginInfo> GetUserLoginInfosByEnterpriseCodeLoginId(string enterpriseCode, string loginId) {
            IQueryable<UserLoginInfo> result = this.ObjectContext.UserLoginInfoes;
            if(!string.IsNullOrWhiteSpace(enterpriseCode) && !string.IsNullOrWhiteSpace(loginId)) {
                result = result.Where(r => r.EnterpriseCode == enterpriseCode && r.LoginId == loginId);
            }
            if(result.Any()) {
                //var enterprise = this.ObjectContext.Enterprises.Where(r => r.Code == enterpriseCode).FirstOrDefault();
                //if(enterprise != null) {
                //    result.First().EnterpriseCategoryId = enterprise.EnterpriseCategoryId;
                //    result.First().IsIdm = enterprise.IsIDM;
                //} else {
                //    result.First().EnterpriseCategoryId = -1;
                //    result.First().IsIdm = false;
                //}
                result.First().SysTime = DateTime.Now;
            }
            return result;
        }

        public string GetUserLoginErrorMessage(string enterpriseCode, string loginId) {
            IQueryable<UserLoginInfo> result = this.ObjectContext.UserLoginInfoes;
            result = result.Where(r => r.EnterpriseCode == enterpriseCode);
            //用企业编号校验
            if(result == null || result.Count() == 0)
                return string.Format("不存在企业[{0}]", enterpriseCode);
            else
                return string.Format("当前企业下不存在用户[{0}]", loginId);
        }

        [Invoke]
        public void 调整用户登录信息(string enterpriseCode, string loginId, bool isLoginsuccess) {
            var nowTime = DateTime.Now;
            //1、合法性校验：查询用户登录信息，依据：企业编号 = 企业编号（参数）、登录Id = 登录Id（参数），如果不存在数据，则报错：不存在【企业编号】企业下【登录Id】的登录信息，请联系管理员！
            IQueryable<UserLoginInfo> result = this.ObjectContext.UserLoginInfoes;
            if(!string.IsNullOrWhiteSpace(enterpriseCode) && !string.IsNullOrWhiteSpace(loginId)) {
                result = result.Where(r => r.EnterpriseCode == enterpriseCode && r.LoginId == loginId);
            }
            if(!result.Any()) {
                throw new ValidationException(string.Format("不存在{0}企业下{1}的登录信息，请联系管理员！", enterpriseCode, loginId));
            }
            if(result.Count() > 1) {
                throw new ValidationException(string.Format("存在多条{0}企业下{1}的登录信息，请联系管理员！", enterpriseCode, loginId));
            }
            var userLoginInfo = result.First();

            //1、如果是否登录成功 = 是，则更新用户登录信息，赋值：
            //    1.1、输错密码次数 = 0
            //    1.2、开始锁定时间 = 空
            //2、如果是否登录成功 = 否，则：
            //    2.1、如果修改前的用户登录信息.输错密码次数 = 4，则更新用户登录信息，赋值：
            //        2.1.1、输错密码次数 = 5
            //        2.1.2、开始锁定时间 = 系统时间
            //    2.2、如果修改前的用户登录信息.输错密码次数 < 4，则更新用户登录信息，赋值：
            //        2.2.1、输错密码次数 = 输错密码次数 + 1
            //        2.2.2、开始锁定时间 = 空
            //    2.3、如果修改前的用户登录信息.输错密码次数 > 4，则：
            //        2.3.1、如果系统时间 < 锁定结束时间（即：用户登录信息.开始锁定时间 + 用户登录信息.锁定时长（分钟）），则报错：已连续登录失败5次，请在【锁定结束时间】后再试！
            //        2.3.2、如果系统时间 >= 锁定结束时间，则更新用户登录信息，赋值：
            //            2.3.2.1、输错密码次数 = 1
            //            2.3.2.2、开始锁定时间 = 空
            if(isLoginsuccess) {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception("身份认证失效,请登录系统后再次尝试");
                }
                userLoginInfo.ErrorPassNum = 0;
                userLoginInfo.BeginLockTime = null;
            } else {
                if(userLoginInfo.ErrorPassNum == 4) {
                    userLoginInfo.ErrorPassNum = 5;
                    userLoginInfo.BeginLockTime = nowTime;
                } else if(userLoginInfo.ErrorPassNum < 4) {
                    userLoginInfo.ErrorPassNum += 1;
                    userLoginInfo.BeginLockTime = null;
                } else {
                    if(userLoginInfo.BeginLockTime == null) {
                        throw new ValidationException(string.Format("用户名为{0}的开始锁定时间为空，请联系管理员！", userLoginInfo.LoginId));
                    }
                    var endTime = userLoginInfo.BeginLockTime.Value.AddMinutes(userLoginInfo.LockLenght);
                    if(endTime > nowTime) {
                        throw new ValidationException(string.Format("已连续登录失败5次，请在{0}后再试！", endTime));
                    } else {
                        userLoginInfo.ErrorPassNum = 1;
                        userLoginInfo.BeginLockTime = null;
                    }

                }
            }
            UpdateToDatabase(userLoginInfo);
            if(isLoginsuccess) {
                var loginLog = new LoginLog();
                var user = this.GetCurrentUserInfo();
                loginLog.PersonnelId = user.Id;
                // loginLog.IP =  BaseApp.Current.InitParams["IP"];
                this.InsertToDatabase(loginLog);
            }
            this.ObjectContext.SaveChanges();
        }
    }
}
