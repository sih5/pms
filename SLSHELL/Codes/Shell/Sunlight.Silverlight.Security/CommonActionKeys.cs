﻿namespace Sunlight.Silverlight.Security {
    public static class CommonActionKeys {
        private const string SECURITY = "Security";

        public const string ADD = "Add";
        public const string EDIT = "Edit";
        public const string ABANDON = "Abandon";
        public const string RECOVER = "Recover";
        public const string FREEZE = "Freeze";
        public const string EXPORT = SECURITY + "Export";

        public const string ADD_EDIT_ABANDON = SECURITY + ADD + EDIT + ABANDON;
        public const string ADD_EDIT_FREEZE_RECOVER = SECURITY + ADD + EDIT + FREEZE + RECOVER;
    }
}
