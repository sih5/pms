﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.Controls {
    public class EntOrganizationTplDetailTreeView : RadTreeView, IBaseView {
        private SecurityDomainContext domainContext;

        public SecurityDomainContext DomainContext {
            get {
                return domainContext ?? (domainContext = new SecurityDomainContext());
            }
            set {
                this.domainContext = value;
            }
        }

        private void PopulateItems(IEnumerable<EntOrganizationTplDetail> details, int id, ICollection<object> items) {
            if(details == null)
                return;
            foreach(var detail in details.Where(d => d.ParentId == id).OrderBy(d => d.Sequence)) {
                var item = new RadTreeViewItem {
                    Header = string.Format("{0} ({1})", detail.OrganizationName, detail.OrganizationCode),
                    Tag = detail,
                    IsExpanded = true
                };
                items.Add(item);
                this.PopulateItems(details, detail.Id, item.Items);
            }
        }

        private void EntOrganizationTplDetailTreeView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var entOrganizationTpl = this.DataContext as EntOrganizationTpl;
            if(entOrganizationTpl == null) {
                this.Items.Clear();
                return;
            }

            this.DomainContext.Load(this.DomainContext.GetEntOrganizationTplDetailsQuery().Where(o => o.EntOrganizationTplId == entOrganizationTpl.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.Items.Clear();
                this.PopulateItems(loadOp.Entities, -1, this.Items);
            }, null);
        }

        public EntOrganizationTplDetailTreeView() {
            this.DataContextChanged += this.EntOrganizationTplDetailTreeView_DataContextChanged;
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new System.NotImplementedException();
        }
    }
}
