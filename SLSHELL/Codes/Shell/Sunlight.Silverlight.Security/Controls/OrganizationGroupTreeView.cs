﻿using System.ServiceModel.DomainServices.Client;
using System.Windows;

namespace Sunlight.Silverlight.Security.Controls {
    public class OrganizationGroupTreeView : OrganizationTreeView {
        protected override void OrganizationTreeView_Loaded(object sender, RoutedEventArgs e) {
        }

        public void ExecuteQuery(int status, string enterpriseCode, string enterpriseName, int enterpriseCategory) {
            this.DomainContext.Load(this.DomainContext.GetOrganizationsByEnterpriseNameAndEnterpriseCategoryQuery(status, enterpriseCode, enterpriseName, enterpriseCategory), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.Items.Clear();
                this.PopulateItems(loadOp.Entities, -1, this.Items);
            }, null);
        }
    }
}
