﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Security.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.Controls {
    public class OrganizationTreeView : RadTreeView {
        private SecurityDomainContext domainContext;

        public SecurityDomainContext DomainContext {
            get {
                return domainContext ?? (domainContext = new SecurityDomainContext());
            }
            set {
                this.domainContext = value;
            }
        }

        protected void PopulateItems(IEnumerable<Organization> organizations, int id, ICollection<object> items) {
            if(organizations == null)
                throw new ArgumentNullException("organizations");

            // ReSharper disable PossibleMultipleEnumeration
            foreach(var organization in organizations.Where(o => o.ParentId == id).OrderBy(o => o.Sequence)) {
                var item = new RadTreeViewItem {
                    Header = string.Format("{0} ({1})", organization.Name, organization.Code),
                    Tag = organization,
                    IsExpanded = true
                };
                items.Add(item);
                this.PopulateItems(organizations, organization.Id, item.Items);
            }
            // ReSharper restore PossibleMultipleEnumeration
        }

        protected void ReloadOrganizations() {
            this.DomainContext.Load(this.DomainContext.GetOrganizationsQuery().Where(o => o.EnterpriseId == BaseApp.Current.CurrentUserData.EnterpriseId && o.Status != (int)SecurityCommonStatus.作废), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    SecurityUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.Items.Clear();
                this.PopulateItems(loadOp.Entities, -1, this.Items);
            }, null);
        }

        protected virtual void OrganizationTreeView_Loaded(object sender, RoutedEventArgs e) {
            this.ReloadOrganizations();
        }

        public OrganizationTreeView() {
            this.Loaded += this.OrganizationTreeView_Loaded;
        }
    }
}
