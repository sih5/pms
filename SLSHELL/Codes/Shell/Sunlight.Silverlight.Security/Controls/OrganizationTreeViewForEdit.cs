﻿using System.Windows;

namespace Sunlight.Silverlight.Security.Controls {
    public class OrganizationTreeViewForEdit : OrganizationTreeView {
        protected override void OrganizationTreeView_Loaded(object sender, RoutedEventArgs e) {
            this.ReloadOrganizations();
        }
    }
}
