﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight {
    /// <summary>
    /// 自定义的过滤条件描述符，针对<see cref="RadGridView"/>控件的每列均增加一个过滤条件，条件之间为Or的关系。
    /// 若该列数据为<see cref="string"/>类型，则运算符为<see cref="FilterOperator.Contains"/>，否则为<see cref="FilterOperator.IsEqualTo"/>）。
    /// </summary>
    public class CustomFilterDescriptor : FilterDescriptorBase {
        private readonly CompositeFilterDescriptor compositeFilterDesriptor;
        private static readonly ConstantExpression TrueExpression = Expression.Constant(true);
        private string filterValue;

        public CustomFilterDescriptor(IEnumerable<GridViewColumn> columns) {
            this.compositeFilterDesriptor = new CompositeFilterDescriptor();
            this.compositeFilterDesriptor.LogicalOperator = FilterCompositionLogicalOperator.Or;

            foreach(GridViewDataColumn column in columns)
                this.compositeFilterDesriptor.FilterDescriptors.Add(this.CreateFilterForColumn(column));
        }

        /// <summary>
        /// 过滤条件值
        /// </summary>
        public string FilterValue {
            get {
                return this.filterValue;
            }
            set {
                if(this.filterValue != value) {
                    this.filterValue = value;
                    this.UpdateCompositeFilterValues();
                    this.OnPropertyChanged("FilterValue");
                }
            }
        }

        private IFilterDescriptor CreateFilterForColumn(IDataFieldDescriptor column) {
            var filterOperator = GetFilterOperatorForType(column.DataType);
            var descriptor = new FilterDescriptor(column.GetDataMemberName(), filterOperator, this.filterValue);
            descriptor.MemberType = column.DataType;
            return descriptor;
        }

        private static FilterOperator GetFilterOperatorForType(Type dataType) {
            return dataType == typeof(string) ? FilterOperator.Contains : FilterOperator.IsEqualTo;
        }

        private void UpdateCompositeFilterValues() {
            foreach(FilterDescriptor descriptor in this.compositeFilterDesriptor.FilterDescriptors) {
                object convertedValue;
                try {
                    convertedValue = Convert.ChangeType(this.FilterValue, descriptor.MemberType, CultureInfo.CurrentCulture);
                } catch {
                    convertedValue = OperatorValueFilterDescriptorBase.UnsetValue;
                }

                if(descriptor.MemberType.IsAssignableFrom(typeof(DateTime))) {
                    DateTime date;
                    if(DateTime.TryParse(this.FilterValue, out date))
                        convertedValue = date;
                }
                descriptor.Value = convertedValue;
            }
        }

        protected override Expression CreateFilterExpression(ParameterExpression parameterExpression) {
            if(string.IsNullOrEmpty(this.FilterValue))
                return TrueExpression;
            // ReSharper disable EmptyGeneralCatchClause
            try {
                return this.compositeFilterDesriptor.CreateFilterExpression(parameterExpression);
            } catch {
            }
            // ReSharper restore EmptyGeneralCatchClause
            return TrueExpression;
        }
    }
}
