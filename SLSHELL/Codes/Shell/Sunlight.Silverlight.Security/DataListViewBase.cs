﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Silverlight.Security {
    public class DataListViewBase : UserControl, IBaseView, INotifyPropertyChanged {
        private Entity selectedEntity;
        private IEnumerable<Entity> selectedEntities;
        public event EventHandler SelectionChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        public SecurityDomainContext SecurityDomainContext {
            get;
            set;
        }

        public virtual IEnumerable<Entity> SelectedEntities {
            get {
                return this.selectedEntities;
            }
            set {
                if(this.selectedEntities != value) {
                    this.selectedEntities = value;
                    this.OnPropertyChanged("SelectedEntities");
                }
            }
        }

        public virtual Entity SelectedEntity {
            get {
                return this.selectedEntity;
            }
            set {
                if(this.selectedEntity != value) {
                    this.selectedEntity = value;
                    this.OnPropertyChanged("SelectedEntity");
                }
            }
        }

        protected void OnSelectionChanged(object sender, EventArgs e) {
            var handler = this.SelectionChanged;
            if(handler != null)
                handler(sender, e);
        }

        protected virtual void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public void UpdateSelectedEntities(Action<Entity> updateAction, Action callback = null) {
            if(updateAction == null)
                return;

            if(this.SelectedEntities != null) {
                foreach(var entity in this.SelectedEntities)
                    updateAction(entity);
                this.SecurityDomainContext.SubmitChanges(delegate(SubmitOperation submitOp) {
                    try {
                        if(!submitOp.HasError) {
                            if(callback != null)
                                callback();
                        } else {
                            if(!submitOp.IsErrorHandled)
                                submitOp.MarkErrorAsHandled();
                            ShellUtils.ShowDomainServiceOperationWindow(submitOp);
                        }
                    } finally {
                    }
                }, null);
            }
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new NotImplementedException();
        }
    }
}
