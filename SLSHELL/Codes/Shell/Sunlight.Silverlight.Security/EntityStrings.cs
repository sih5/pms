﻿using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Security {
    public class EntityStrings : ModelBase {
        public void Refresh(params string[] propertyNames) {
            this.NotifyOfPropertiesChange(propertyNames.Length == 0 ? this.GetType().GetProperties().Select(p => p.Name).ToArray() : propertyNames);
        }

        public string EntNodeTemplate_Enterprises {
            get {
                return Web.Resources.EntityStrings.EntNodeTemplate_Enterprises;
            }
        }

        public string EntNodeTemplate_EntNodeTemplateDetails {
            get {
                return Web.Resources.EntityStrings.EntNodeTemplate_EntNodeTemplateDetails;
            }
        }

        public string EntOrganizationTplDetail_OrganizationCode {
            get {
                return Web.Resources.EntityStrings.EntOrganizationTplDetail_OrganizationCode;
            }
        }

        public string EntOrganizationTplDetail_OrganizationName {
            get {
                return Web.Resources.EntityStrings.EntOrganizationTplDetail_OrganizationName;
            }
        }

        public string EntOrganizationTplDetail_OrganizationStatus {
            get {
                return Web.Resources.EntityStrings.EntOrganizationTplDetail_OrganizationStatus;
            }
        }

        public string EntOrganizationTplDetail_OrganizationType {
            get {
                return Web.Resources.EntityStrings.EntOrganizationTplDetail_OrganizationType;
            }
        }

        public string EntOrganizationTpl_EntOrganizationTplDetails {
            get {
                return Web.Resources.EntityStrings.EntOrganizationTpl_EntOrganizationTplDetails;
            }
        }

        public string Organization_Code {
            get {
                return Web.Resources.EntityStrings.Organization_Code;
            }
        }

        public string Organization_Name {
            get {
                return Web.Resources.EntityStrings.Organization_Name;
            }
        }

        public string Organization_Status {
            get {
                return Web.Resources.EntityStrings.Organization_Status;
            }
        }

        public string Organization_Type {
            get {
                return Web.Resources.EntityStrings.Organization_Type;
            }
        }

        public string Page_Description {
            get {
                return Web.Resources.EntityStrings.Page_Description;
            }
        }

        public string Page_PageId {
            get {
                return Web.Resources.EntityStrings.Page_PageId;
            }
        }

        public string Page_PageType {
            get {
                return Web.Resources.EntityStrings.Page_PageType;
            }
        }

        public string Page_Parameter {
            get {
                return Web.Resources.EntityStrings.Page_Parameter;
            }
        }

        public string Role_IsAdmin {
            get {
                return Web.Resources.EntityStrings.Role_IsAdmin;
            }
        }

        public string Role_Name {
            get {
                return Web.Resources.EntityStrings.Role_Name;
            }
        }

        public string Personnel_CellNumber {
            get {
                return Web.Resources.EntityStrings.Personnel_CellNumber;
            }
        }

        public string Personnel_IsAdmin {
            get {
                return Web.Resources.EntityStrings.Personnel_IsAdmin;
            }
        }

        public string Personnel_IsDefaultPassword {
            get {
                return Web.Resources.EntityStrings.Personnel_IsDefaultPassword;
            }
        }

        public string Personnel_LoginId {
            get {
                return Web.Resources.EntityStrings.Personnel_LoginId;
            }
        }

        public string Personnel_Name {
            get {
                return Web.Resources.EntityStrings.Personnel_Name;
            }
        }

        public string Personnel_OrganizationName {
            get {
                return Web.Resources.EntityStrings.Personnel_OrganizationName;
            }
        }

        public string Personnel_OrganizationType {
            get {
                return Web.Resources.EntityStrings.Personnel_OrganizationType;
            }
        }

        public string Personnel_Password {
            get {
                return Web.Resources.EntityStrings.Personnel_Password;
            }
        }

        public string _Common_Category {
            get {
                return Web.Resources.EntityStrings._Common_Category;
            }
        }

        public string _Common_Code {
            get {
                return Web.Resources.EntityStrings._Common_Code;
            }
        }

        public string _Common_CreateTime {
            get {
                return Web.Resources.EntityStrings._Common_CreateTime;
            }
        }

        public string _Common_CreatorId {
            get {
                return Web.Resources.EntityStrings._Common_CreatorId;
            }
        }

        public string _Common_CreatorName {
            get {
                return Web.Resources.EntityStrings._Common_CreatorName;
            }
        }

        public string _Common_Id {
            get {
                return Web.Resources.EntityStrings._Common_Id;
            }
        }

        public string _Common_ModifierId {
            get {
                return Web.Resources.EntityStrings._Common_ModifierId;
            }
        }

        public string _Common_ModifierName {
            get {
                return Web.Resources.EntityStrings._Common_ModifierName;
            }
        }

        public string _Common_ModifyTime {
            get {
                return Web.Resources.EntityStrings._Common_ModifyTime;
            }
        }

        public string _Common_Name {
            get {
                return Web.Resources.EntityStrings._Common_Name;
            }
        }

        public string _Common_ParentId {
            get {
                return Web.Resources.EntityStrings._Common_ParentId;
            }
        }

        public string _Common_Remark {
            get {
                return Web.Resources.EntityStrings._Common_Remark;
            }
        }

        public string _Common_Sequence {
            get {
                return Web.Resources.EntityStrings._Common_Sequence;
            }
        }

        public string _Common_Status {
            get {
                return Web.Resources.EntityStrings._Common_Status;
            }
        }

        public string _Common_Type {
            get {
                return Web.Resources.EntityStrings._Common_Type;
            }
        }

        public string Enterprise_Category {
            get {
                return Web.Resources.EntityStrings.Enterprise_Category;
            }
        }

        public string EntOrganizationTpl_Code {
            get {
                return Web.Resources.EntityStrings.EntOrganizationTpl_Code;
            }
        }

        public string EntOrganizationTpl_Name {
            get {
                return Web.Resources.EntityStrings.EntOrganizationTpl_Name;
            }
        }

        public string EntOrganizationTpl_Remark {
            get {
                return Web.Resources.EntityStrings.EntOrganizationTpl_Remark;
            }
        }

        public string RoleTemplate_RoleTemplateRules {
            get {
                return Web.Resources.EntityStrings.RoleTemplate_RoleTemplateRules;
            }
        }

        public string Enterprise_Organizations {
            get {
                return Web.Resources.EntityStrings.Enterprise_Organizations;
            }
        }

        public string Enterprise_Code {
            get {
                return Web.Resources.EntityStrings.Enterprise_Code;
            }
        }

        public string Enterprise_Name {
            get {
                return Web.Resources.EntityStrings.Enterprise_Name;
            }
        }

        public string EntNodeTemplate_Code {
            get {
                return Web.Resources.EntityStrings.EntNodeTemplate_Code;
            }
        }

        public string EntNodeTemplate_Name {
            get {
                return Web.Resources.EntityStrings.EntNodeTemplate_Name;
            }
        }

        public string RoleTemplate_Name {
            get {
                return Web.Resources.EntityStrings.RoleTemplate_Name;
            }
        }

        public string Personnel_OrganizationPersonnels {
            get {
                return Web.Resources.EntityStrings.Personnel_OrganizationPersonnels;
            }
        }

        public string Personnel_RolePersonnels {
            get {
                return Web.Resources.EntityStrings.Personnel_RolePersonnels;
            }
        }

        public string Role_RolePersonnels {
            get {
                return Web.Resources.EntityStrings.Role_RolePersonnels;
            }
        }

        public string Role_RoleRules {
            get {
                return Web.Resources.EntityStrings.Role_RoleRules;
            }
        }

        public string Personnel_Email {
            get {
                return Web.Resources.EntityStrings.Personnel_Email;
            }
        }

        public string Personnel_CreateTime {
            get {
                return Web.Resources.EntityStrings.Personnel_CreateTime;
            }
        }

        public string Personnel_CreatorName {
            get {
                return Web.Resources.EntityStrings.Personnel_CreatorName;
            }
        }

        public string Personnel_ModifierName {
            get {
                return Web.Resources.EntityStrings.Personnel_ModifierName;
            }
        }

        public string Personnel_ModifyTime {
            get {
                return Web.Resources.EntityStrings.Personnel_ModifyTime;
            }
        }

        public string LoginLog_LoginTime {
            get {
                return Web.Resources.EntityStrings.LoginLog_LoginTime;
            }
        }

        public string LoginLog_LogoutTime {
            get {
                return Web.Resources.EntityStrings.LoginLog_LogoutTime;
            }
        }

        public string SecurityManager_Error_LoginInfoFailure {
            get {
                return Web.Resources.EntityStrings.SecurityManager_Error_LoginInfoFailure;
            }
        }

        public string SecurityManager_Error_NoUserLoginInfo {
            get {
                return Web.Resources.EntityStrings.SecurityManager_Error_NoUserLoginInfo;
            }
        }

        public string SecurityManager_Error_RepeatUserLoginInfo {
            get {
                return Web.Resources.EntityStrings.SecurityManager_Error_RepeatUserLoginInfo;
            }
        }

    }
}
