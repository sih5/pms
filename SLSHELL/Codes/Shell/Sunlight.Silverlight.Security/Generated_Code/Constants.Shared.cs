﻿
namespace Sunlight.Silverlight.Security.Web {
    public static class GlobalVar {
        //该值由服务端填充
        public const string ASSIGNED_BY_SERVER = "<尚未生成>";
        //人员默认头像的 URL
        public const string DEFAULT_AVATAR_URL = "Client/Images/DefaultAvatar.png";
        //服务端文件下载 URL 前缀
        public const string DOWNLOAD_FILE_URL_PREFIX = "FileDownloadHandler.ashx?Path=";
        //服务端上传文件处理路径
        public const string UPLOAD_FILE_URL_PERFIX = "FileUploadHandler.ashx";
        //导入文件存放目录
        public const string UPLOAD_IMPORTFILE_DIR = "ImportedFiles";
        //导出文件存放目录T
        public const string DOWNLOAD_EXPORTFILE_DIR = "ExportedFiles";
    }
}