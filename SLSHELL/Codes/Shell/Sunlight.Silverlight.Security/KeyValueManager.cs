﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Security {
    public sealed class KeyValueManager {
        private static readonly Dictionary<string, KeyValuePair[]> KeyValueItems = new Dictionary<string, KeyValuePair[]>();
        private static readonly object KeyValueItemsLock = new object();

        public static void GetKeyValuePairs(IDictionary<string, ObservableCollection<KeyValuePair>> collections, Action callback) {
            if(collections == null)
                throw new ArgumentNullException("collections");

            ShellViewModel.Current.IsBusy = true;

            var namesToQuery = new List<string>();
            foreach(var kv in collections.Where(kv => kv.Value != null)) {
                bool success;
                KeyValuePair[] keyValueItems;
                lock(KeyValueItemsLock)
                    success = KeyValueItems.TryGetValue(kv.Key, out keyValueItems);
                if(success) {
                    kv.Value.Clear();
                    foreach(var keyValueItem in keyValueItems)
                        kv.Value.Add(keyValueItem);
                } else
                    namesToQuery.Add(kv.Key);
            }
            if(!namesToQuery.Any()) {
                try {
                    if(callback != null)
                        callback();
                } finally {
                    ShellViewModel.Current.IsBusy = false;
                }
                return;
            }

            var domainContext = new SecurityDomainContext();
            domainContext.Load(domainContext.GetKeyValueMappingByNamesQuery(namesToQuery.ToArray()), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var name in namesToQuery) {
                    var keyValueItems = loadOp.Entities.Where(e => e.Name == name).Select(e => new KeyValuePair {
                        Key = e.Key,
                        Value = e.Value,
                    }).OrderBy(kvp => kvp.Key).ToArray();
                    lock(KeyValueItemsLock) {
                        var cacheKey = name;
                        if(!KeyValueItems.ContainsKey(cacheKey))
                            KeyValueItems.Add(cacheKey, keyValueItems);
                    }
                    var collection = collections[name];
                    collection.Clear();
                    foreach(var keyValueItem in keyValueItems)
                        collection.Add(keyValueItem);
                }
                try {
                    if(callback != null)
                        callback();
                } finally {
                    ShellViewModel.Current.IsBusy = false;
                }
            }, null);
        }

        internal static IEnumerable<KeyValuePair> GetKeyValuePairs(string cacheKey) {
            KeyValuePair[] result;
            return KeyValueItems.TryGetValue(cacheKey, out result) ? result : Enumerable.Empty<KeyValuePair>();
        }

        private Dictionary<string, ObservableCollection<KeyValuePair>> kvPairs;

        public ObservableCollection<KeyValuePair> this[string name] {
            get {
                if(this.kvPairs == null || !this.kvPairs.ContainsKey(name))
                    return null;

                var result = this.kvPairs[name];
                if(result == null)
                    return this.kvPairs[name] = new ObservableCollection<KeyValuePair>();
                return result;
            }
        }

        private void CreateAllInstances() {
            if(this.kvPairs == null)
                return;

            var names = this.kvPairs.Keys.Where(key => this.kvPairs[key] == null).ToArray();
            foreach(var name in names)
                this.kvPairs[name] = new ObservableCollection<KeyValuePair>();
        }

        public void Register(params string[] names) {
            if(names.Length == 0)
                return;

            if(this.kvPairs == null)
                this.kvPairs = new Dictionary<string, ObservableCollection<KeyValuePair>>(names.Length);
            foreach(var name in names.Where(name => !this.kvPairs.ContainsKey(name)))
                this.kvPairs.Add(name, null);
        }

        public void LoadData(Action callback = null) {
            if(this.kvPairs == null) {
                if(callback != null)
                    callback();
                return;
            }
            this.CreateAllInstances();
            GetKeyValuePairs(this.kvPairs, callback);
        }
    }
}
