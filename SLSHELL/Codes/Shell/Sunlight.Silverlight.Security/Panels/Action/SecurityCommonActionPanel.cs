﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security.Panels.Action {
    public class SecurityCommonActionPanel : ActionPanelBase {
        // 初始化函数，为 ActionItemGroup 属性赋值并引发界面的生成
        private void Initialize() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = SecurityUIStrings.ActionPanel_Title_Organization,
                ActionItems = new[] {
                    new ActionItem {
                        Title = SecurityUIStrings.Action_Title_Add,
                        UniqueId = CommonActionKeys.ADD,
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/Add.png", UriKind.Relative),
                    }, new ActionItem {
                        Title = SecurityUIStrings.Action_Title_Edit,
                        UniqueId = CommonActionKeys.EDIT,
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/Edit.png", UriKind.Relative),
                        CanExecute = false
                    }, new ActionItem {
                        Title = SecurityUIStrings.Action_Title_Abandon,
                        UniqueId = CommonActionKeys.ABANDON,
                        ImageUri = new Uri("/Sunlight.Silverlight.Security;component/Images/Actions/Abandon.png", UriKind.Relative),
                        CanExecute = false
                    }
                }
            };
        }

        public SecurityCommonActionPanel() {
            this.SkipPermissionCheck = true;
            this.Initializer.Register(this.Initialize);
        }
    }
}
