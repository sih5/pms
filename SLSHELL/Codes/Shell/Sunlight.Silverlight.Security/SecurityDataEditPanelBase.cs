﻿using System.Windows.Controls;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security {
    public class SecurityDataEditPanelBase : UserControl, IDataEditPanel {
        private ViewInitializer initializer;
        private KeyValueManager keyValueManager;

        protected ViewInitializer Initializer {
            get {
                return this.initializer ?? (this.initializer = new ViewInitializer(this));
            }
        }

        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void RefreshTextBlockBindings() {
            foreach(var textBlock in this.FindChildrenByType<TextBlock>()) {
                var expression = textBlock.GetBindingExpression(TextBlock.TextProperty);
                if(expression == null)
                    continue;
                var binding = expression.ParentBinding;
                if(binding.Converter is KeyValueItemConverter) {
                    textBlock.Text = string.Empty;
                    textBlock.SetBinding(TextBlock.TextProperty, binding);
                }
            }
        }

        private void Initialize() {
            this.KeyValueManager.LoadData(this.RefreshTextBlockBindings);
        }

        public SecurityDataEditPanelBase() {
            this.Initializer.Register(this.Initialize);
        }
    }
}
