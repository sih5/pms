﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;
using Sunlight.Silverlight.Security.Resources;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security {
    public class SecurityDataEditViewBase : DataEditViewBase, INotifyPropertyChanged {
        private bool uiCreated;
        private Border contentContainer;
        private TextBlock titleText;

        public event PropertyChangedEventHandler PropertyChanged;

        protected new SecurityDomainContext DomainContext {
            get {
                return base.DomainContext as SecurityDomainContext;
            }
            set {
                base.DomainContext = value;
            }
        }

        protected virtual string BusinessName {
            get {
                return null;
            }
        }

        protected virtual string Title {
            get {
                return null;
            }
        }

        public new UIElement Content {
            get {
                return this.uiCreated ? this.contentContainer.Child : base.Content;
            }
            set {
                if(this.uiCreated)
                    this.contentContainer.Child = value;
                else
                    base.Content = value;
            }
        }

        private void CreateUI() {
            if(this.uiCreated)
                return;
            this.uiCreated = true;

            // 合并按钮的资源字典
            this.Resources.MergedDictionaries.Add(new ResourceDictionary {
                Source = new Uri("/Sunlight.Silverlight.Security;component/Styles/FlatButton.xaml", UriKind.Relative),
            });

            // 创建标题栏内的布局容器
            var titleGrid = new Grid();
            titleGrid.ColumnDefinitions.Add(new ColumnDefinition());
            titleGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = GridLength.Auto
            });
            // 创建标题文本
            titleGrid.Children.Add(this.titleText = new TextBlock {
                Margin = new Thickness(8, 5, 8, 5),
                FontSize = 18,
                Text = this.GetTitleText(),
            });
            // 创建操作按钮的布局容器
            var stackPanel = new StackPanel {
                Orientation = Orientation.Horizontal,
                HorizontalAlignment = HorizontalAlignment.Right,
                Margin = new Thickness(0, 1, 15, 1),
            };
            stackPanel.SetValue(Grid.ColumnProperty, 1);
            titleGrid.Children.Add(stackPanel);
            // 创建保存按钮
            var button = new RadButton {
                Style = (Style)this.Resources["SubmitButton"],
                Command = this.SubmitCommand,
            };
            stackPanel.Children.Add(button);
            // 创建取消按钮
            button = new RadButton {
                Margin = new Thickness(5, 0, 0, 0),
                Style = (Style)this.Resources["RejectButton"],
                Command = this.CancelCommand,
            };
            stackPanel.Children.Add(button);

            // 创建用于承载内容的容器
            var busyIndicator = new RadBusyIndicator {
                DisplayAfter = TimeSpan.Zero,
                Content = this.contentContainer = new Border {
                    BorderThickness = new Thickness(1),
                    BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0xC0, 0xC0, 0xC0)),
                },
            };
            busyIndicator.SetBinding(RadBusyIndicator.IsBusyProperty, new Binding("IsBusy") {
                Source = base.DomainContext,
                Mode = BindingMode.OneWay,
            });
            busyIndicator.SetValue(Grid.RowProperty, 1);

            // 创建整个页面的根布局
            var root = new Grid {
                Background = new SolidColorBrush(Colors.White)
            };
            root.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            root.RowDefinitions.Add(new RowDefinition());
            root.Children.Add(new Border {
                BorderThickness = new Thickness(1, 1, 1, 0),
                BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0xC0, 0xC0, 0xC0)),
                Background = new SolidColorBrush(Color.FromArgb(0xFF, 0xED, 0xED, 0xED)),
                Child = titleGrid
            });
            root.Children.Add(busyIndicator);

            var originalContent = base.Content;
            base.Content = root;
            this.contentContainer.Child = originalContent;
        }

        private string GetTitleText() {
            return string.IsNullOrEmpty(this.Title) ? string.Format(this.EditState == DataEditState.New ? SecurityUIStrings.DataEditView_Add : SecurityUIStrings.DataEditView_Edit, this.BusinessName) : this.Title;
        }

        protected virtual void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected SecurityDataEditViewBase() {
            this.Initializer.Register(this.CreateUI);
        }

        protected FrameworkElement CreateVerticalLine(int gridColumnIndex = 0, int gridRowIndex = 0) {
            var result = new Rectangle {
                Width = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(8, 0, 8, 0),
            };
            result.SetValue(Grid.ColumnProperty, gridColumnIndex);
            result.SetValue(Grid.RowProperty, gridRowIndex);
            return result;
        }

        protected FrameworkElement CreateHorizontalLine(int gridColumnIndex = 0, int gridRowIndex = 0) {
            var result = new Rectangle {
                Height = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(0, 8, 0, 8),
            };
            result.SetValue(Grid.ColumnProperty, gridColumnIndex);
            result.SetValue(Grid.RowProperty, gridRowIndex);
            return result;
        }

        protected override void OnEditStateChanged() {
            if(this.titleText != null)
                this.titleText.Text = this.GetTitleText();
        }

        /// <summary>
        /// 重置当前状态，在提交成功后或退出当前编辑界面前被调用。
        /// </summary>
        protected virtual void Reset() {
            
        }

        protected override void OnEditSubmitted() {
            this.Reset();
            base.OnEditSubmitted();
        }

        protected override void OnEditCancelled() {
            this.Reset();
            base.OnEditCancelled();
        }
    }
}
