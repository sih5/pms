﻿using System;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Security {
    public abstract class SecurityDataGridViewBase : DataGridViewBase {
        private bool keyValuesLoaded;
        private KeyValueManager keyValueManager;

        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void Initialize() {
            this.KeyValueManager.LoadData(() => {
                this.RebindKeyValueColumns();
                this.keyValuesLoaded = true;
                this.NotifyOfPropertyChange("CanExecuteQuery");
            });
        }

        protected SecurityDataGridViewBase() {
            this.RegisterCanExecuteQueryDependency(() => this.keyValuesLoaded);
            this.Initializer.Register(this.Initialize);
        }

        protected override void OnControlsCreated() {
            //
        }

        protected override string OnRequestQueryName() {
            return null;
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            throw new NotSupportedException();
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            return this.FilterItem == null ? null : this.FilterItem.ToFilterDescriptor();
        }

        internal void SetButtonCommandTargetToGridView(RadButton button) {
            if(button == null)
                throw new ArgumentNullException("button");
            button.CommandTarget = this.GridView;
        }
    }
}
