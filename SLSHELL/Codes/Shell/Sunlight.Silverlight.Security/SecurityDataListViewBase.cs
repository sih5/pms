﻿using System;
using System.Collections.Generic;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Security {
    public abstract class SecurityDataListViewBase : SecurityDataQueryViewBase {
        private FilterItem filterItem;

        public virtual DataListViewBase DataListView {
            get;
            protected set;
        }

        protected abstract void ExecuteQuery();

        public override FilterItem FilterItem {
            get {
                return this.filterItem;
            }
            set {
                this.filterItem = value;
            }
        }

        public SecurityDataListViewBase() {
            if(this.DataListView != null)
                this.DataListView.SelectionChanged += this.OnSelectionChanged;
        }

        public override void ExecuteQueryDelayed() {
            this.ExecuteQuery();
        }

        public override IEnumerable<Entity> SelectedEntities {
            get {
                if(this.DataListView == null)
                    return null;
                return this.DataListView.SelectedEntities;
            }
        }

        public override void UpdateSelectedEntities(Action<Entity> updateAction, Action callback = null) {
            this.DataListView.UpdateSelectedEntities(updateAction, callback);
        }
    }
}
