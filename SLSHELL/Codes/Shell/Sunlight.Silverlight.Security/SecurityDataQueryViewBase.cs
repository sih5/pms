﻿using System;
using System.Collections.Generic;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security {
    public class SecurityDataQueryViewBase : UserControl, IBaseView {

        public event EventHandler SelectionChanged;

        public virtual DataGridViewBase DataGridView {
            get;
            protected set;
        }

        protected void OnSelectionChanged(object sender, EventArgs e) {
            var handler = this.SelectionChanged;
            if(handler != null)
                handler(sender, e);
        }

        protected FrameworkElement CreateVerticalLine(int gridColumnIndex = 0, int gridRowIndex = 0) {
            var result = new Rectangle {
                Width = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(8, 0, 8, 0),
            };
            result.SetValue(Grid.ColumnProperty, gridColumnIndex);
            result.SetValue(Grid.RowProperty, gridRowIndex);
            return result;
        }

        protected FrameworkElement CreateHorizontalLine(int gridColumnIndex = 0, int gridRowIndex = 0) {
            var result = new Rectangle {
                Height = 1,
                Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xA0, 0xA0, 0xA0)),
                Margin = new Thickness(0, 8, 0, 8),
            };
            result.SetValue(Grid.ColumnProperty, gridColumnIndex);
            result.SetValue(Grid.RowProperty, gridRowIndex);
            return result;
        }

        public SecurityDataQueryViewBase() {
            if(this.DataGridView != null)
                this.DataGridView.SelectionChanged += this.OnSelectionChanged;
        }

        public virtual FilterItem FilterItem {
            get {
                return this.DataGridView.FilterItem;
            }
            set {
                this.DataGridView.FilterItem = value;
            }
        }

        public virtual void ExecuteQueryDelayed() {
            this.DataGridView.ExecuteQueryDelayed();
        }

        public virtual IEnumerable<Entity> SelectedEntities {
            get {
                return this.DataGridView.SelectedEntities;
            }
        }

        public virtual void UpdateSelectedEntities(Action<Entity> updateAction, Action callback = null) {
            if(this.DataGridView != null)
                this.DataGridView.UpdateSelectedEntities(updateAction, callback);
        }

        public void UpdateSelectedEntities(SecurityDomainContext domainContext, Entity entity, Action<Entity> updateAction, Action callback = null) {
            if(domainContext == null || entity == null || updateAction == null)
                return;

            updateAction(entity);
            domainContext.SubmitChanges(delegate(SubmitOperation submitOp) {
                try {
                    if(!submitOp.HasError) {
                        if(callback != null)
                            callback();
                    } else {
                        if(!submitOp.IsErrorHandled)
                            submitOp.MarkErrorAsHandled();
                        ShellUtils.ShowDomainServiceOperationWindow(submitOp);
                    }
                } finally {
                }
            }, null);
        }

        public virtual object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            return null;
        }
    }
}