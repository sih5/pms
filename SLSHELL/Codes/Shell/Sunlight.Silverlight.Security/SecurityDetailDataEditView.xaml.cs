﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security {
    public partial class SecurityDetailDataEditView {
        private bool initialized;
        private List<Tuple<object, Lazy<IBaseView>>> dataBaseViews;

        private void SecurityDetailDataEditView_Loaded(object sender, RoutedEventArgs e) {
            if(this.initialized)
                return;
            this.initialized = true;

            if(this.dataBaseViews == null)
                return;

            foreach(var dataBaseView in this.dataBaseViews) {
                var view = dataBaseView.Item2.Value;
                if(view == null)
                    continue;

                this.MainTabControl.Items.Add(new RadTabItem {
                    Margin = new Thickness(2),
                    Header = dataBaseView.Item1,
                    Content = view,
                });
            }

            if(this.MainTabControl.HasItems)
                this.MainTabControl.SelectedIndex = 0;
        }

        public SecurityDetailDataEditView() {
            this.InitializeComponent();
            this.Loaded += this.SecurityDetailDataEditView_Loaded;
        }

        public void Register(string title, Uri icon, IBaseView dataBaseView) {
            this.Register(title, icon, () => dataBaseView);
        }

        public void Register(string title, Uri icon, Func<IBaseView> dataBaseViewFactory) {
            if(this.dataBaseViews == null)
                this.dataBaseViews = new List<Tuple<object, Lazy<IBaseView>>>();

            object header;
            if(icon == null)
                header = title;
            else
                header = BusinessViewUtils.MakeTabHeader(icon, title);

            this.dataBaseViews.Add(Tuple.Create(header, new Lazy<IBaseView>(dataBaseViewFactory)));
        }
    }
}
