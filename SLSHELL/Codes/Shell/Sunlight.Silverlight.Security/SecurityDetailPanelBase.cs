﻿using System;
using System.ComponentModel;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security {
    public class SecurityDetailPanelBase : UserControl, IDetailPanel, INotifyPropertyChanged {
        private KeyValueManager keyValueManager;
        private bool keyValuesLoaded;
        private ViewInitializer initializer;

        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        protected ViewInitializer Initializer {
            get {
                return this.initializer ?? (this.initializer = new ViewInitializer(this));
            }
        }

        public bool KeyValuesLoaded {
            get {
                return this.keyValuesLoaded;
            }
            private set {
                this.keyValuesLoaded = value;
                this.NotifyPropertyChanged("KeyValuesLoaded");
            }
        }

        protected void NotifyPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual string Title {
            get {
                return null;
            }
        }

        public virtual Uri Icon {
            get {
                return null;
            }
        }

        public SecurityDetailPanelBase() {
            this.Initializer.Register(this.Initialize);
            this.PropertyChanged += this.SecurityDetailPanelBase_PropertyChanged;
        }

        private void SecurityDetailPanelBase_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(e.PropertyName == "KeyValuesLoaded") {
                foreach(var textBox in this.ChildrenOfType<TextBox>()) {
                    var expression = textBox.GetBindingExpression(TextBox.TextProperty);
                    if(expression == null)
                        continue;
                    var binding = expression.ParentBinding;
                    if(!(binding.Converter is KeyValueItemConverter))
                        continue;
                    textBox.SetBinding(TextBox.TextProperty, binding);
                }
            }
        }

        private void Initialize() {
            this.KeyValueManager.LoadData(() => {
                this.KeyValuesLoaded = true;
            });
        }
    }
}
