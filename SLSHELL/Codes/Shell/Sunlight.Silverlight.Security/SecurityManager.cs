﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.ServiceModel.DomainServices.Client;
using System.ServiceModel.DomainServices.Client.ApplicationServices;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security {
    public sealed class SecurityActionResult : ISecurityActionResult {
        private Dictionary<string, object> properties;

        internal IDictionary<string, object> Properties {
            get {
                return this.properties ?? (this.properties = new Dictionary<string, object>(4));
            }
        }

        public WaitHandle AsyncWaitHandle {
            get {
                return null;
            }
        }

        public bool CompletedSynchronously {
            get {
                return false;
            }
        }

        public bool IsCompleted {
            get;
            internal set;
        }

        public object AsyncState {
            get;
            internal set;
        }

        public bool IsSuccess {
            get;
            internal set;
        }

        public Exception Exception {
            get;
            internal set;
        }

        public bool IsErrorHandled {
            get;
            set;
        }
        public string LoginId {
            get;
            set;
        }

        public object this[string propertyName] {
            get {
                if(propertyName == null)
                    throw new ArgumentNullException("propertyName");
                return this.properties == null ? null : this.properties[propertyName.ToLower()];
            }
        }

        public bool? IsIdm {
            get;
            set;
        }

        public string ErrorMessage {
            get;
            set;
        }

        public int? EnterpriseCategoryId {
            get;
            set;
        }
    }

    public sealed class SecurityManager : ISecurityManager {
        public static string HashPassword(string password) {
            var data = Encoding.UTF8.GetBytes(password);
            using(var sha1 = new SHA1Managed())
                data = sha1.ComputeHash(data);
            var sbResult = new StringBuilder(data.Length * 2);
            foreach(var b in data)
                sbResult.AppendFormat("{0:X2}", b);
            return sbResult.ToString();
            return password;
        }

        //public int? enterpriseCategoryId = 0;
        //public int enterpriseId = 0;
        //public string loginId = "";
        private AuthenticationService authenticationService;

        public AuthenticationService AuthenticationService {
            get {
                return this.authenticationService ?? (this.authenticationService = new FormsAuthentication {
                    DomainContextType = typeof(SecurityDomainContext).AssemblyQualifiedName,
                });
            }
        }

        private void ExecuteCallback(OperationBase operation, AsyncCallback callback, Dictionary<string, object> properties = null) {
            if(operation == null)
                throw new ArgumentNullException("operation");
            if(callback == null)
                return;

            var result = new SecurityActionResult {
                IsCompleted = operation.IsComplete,
                IsSuccess = !operation.HasError,
                IsErrorHandled = operation.IsErrorHandled,
                Exception = operation.HasError ? operation.Error : null
            };
            if(properties != null)
                foreach(var property in properties)
                    result.Properties.Add(property.Key.ToLower(), property.Value);

            callback(result);

            if(operation.HasError && !operation.IsErrorHandled && !result.IsErrorHandled) {
                result.IsErrorHandled = true;
                if(result.Exception != null)
                    UIHelper.ShowAlertMessage(result.Exception.Message, result.Exception);
            }
            if(operation.HasError && result.IsErrorHandled)
                operation.MarkErrorAsHandled();
        }

        private SecurityDomainContext securityDomainContext;

        private SecurityDomainContext SecurityDomainContext {
            get {
                if(this.securityDomainContext == null) {
                    this.securityDomainContext = new SecurityDomainContext();
                }
                return this.securityDomainContext;
            }
        }


        public void Login(string userId, string password, bool isPersistent, IEnumerable<KeyValuePair<string, string>> parameters, AsyncCallback callback) {
            string customData = null;
            if(parameters != null) {
                var eRoot = new XElement("LoginParameters");
                foreach(var kvp in parameters)
                    eRoot.Add(new XElement(kvp.Key, kvp.Value));
                customData = eRoot.ToString();
            }
            //if(null != password) {
            //    throw new DomainException(string.Format("密码错误：{0}", password));
            //}
           // var loginParameters = new LoginParameters(userId, HashPassword(password), isPersistent, customData);
            var loginParameters = new LoginParameters(userId, password, isPersistent, customData);
            this.AuthenticationService.Login(loginParameters, loginOp => this.LoginExecuteCallback(loginOp, callback, new Dictionary<string, object> {
                    {
                        "LoginSuccess", loginOp.LoginSuccess
                    }, {
                        "User", loginOp.User
                    }
                    , {
                        "AuthenticationUser", loginOp.User==null?null:loginOp.User.Identity
                    }
                }), null);

        }




        private void LoginExecuteCallback(OperationBase operation, AsyncCallback callback, Dictionary<string, object> properties = null) {
            if(operation == null)
                throw new ArgumentNullException("operation");
            if(callback == null)
                return;
            AuthenticationUser authenticationUser = null;
            if(properties != null) {
                authenticationUser = properties["AuthenticationUser"] as AuthenticationUser;
            }
            var result = new SecurityActionResult {
                IsCompleted = operation.IsComplete,
                IsSuccess = !operation.HasError,
                IsErrorHandled = operation.IsErrorHandled,
                Exception = operation.HasError ? operation.Error : null,
                //EnterpriseCategoryId = authenticationUser == null ? null : authenticationUser.EnterpriseCategoryId,
            };
            //enterpriseCategoryId = authenticationUser == null ? null : authenticationUser.EnterpriseCategoryId;
            if(properties != null) {
                foreach(var property in properties)
                    result.Properties.Add(property.Key.ToLower(), property.Value);
            }
            callback(result);

            if(operation.HasError && !operation.IsErrorHandled && !result.IsErrorHandled) {
                result.IsErrorHandled = true;
                if(result.Exception != null)
                    UIHelper.ShowAlertMessage(result.Exception.Message, result.Exception);
            }
            if(operation.HasError && result.IsErrorHandled)
                operation.MarkErrorAsHandled();
            //点击登陆调用服务端方法 调整用户登录信息
            if(authenticationUser != null) {
                if((bool)properties["LoginSuccess"] && authenticationUser.IsFisrt) {
                    this.RadWindow.ShowDialog();
                }
            }
        }

        /// <summary>
        /// 登陆日志
        /// </summary>
        //private void LoginLog() {
        //    var loginLog = new LoginLog();
        //    loginLog.PersonnelId = BaseApp.Current.CurrentUserData.UserId;
        //    loginLog.IP = BaseApp.Current.InitParams["IP"];
        //    securityDomainContext.LoginLogs.Add(loginLog);
        //    securityDomainContext.SubmitChanges();
        //}

        private RadWindow radWindow;

        private RadWindow RadWindow {
            get {
                return this.radWindow ?? (this.radWindow = new RadWindow {
                    Header = "",
                    CanClose = false,
                    Content = this.DataEditView,

                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
        }
        private SecurityDomainContext domainContext;

        public SecurityDomainContext DomainContext {
            get {
                return domainContext ?? (domainContext = new SecurityDomainContext());
            }
            set {
                this.domainContext = value;
            }
        }
        private DataEditViewBase dataEditView;

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("ChangePassword");
                    this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.RadWindow.Close();
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            UIHelper.ShowNotification("首次登录，必须修改密码！界面不能关闭");
        }


        public void Logout(AsyncCallback callback) {
            this.SecurityDomainContext.修改退出时间(BaseApp.Current.CurrentUserData.UserId);
            this.AuthenticationService.Logout(logoutOp => this.ExecuteCallback(logoutOp, callback), null);
        }

        public void GetMenuXml(string cultureName, AsyncCallback callback, DomainContext domainContext = null) {
            var context = domainContext as SecurityDomainContext ?? new SecurityDomainContext();
            context.GetMenuXml(cultureName, invokeOp => this.ExecuteCallback(invokeOp, callback, new Dictionary<string, object> {
                {
                    "Value", invokeOp.Value
                }
            }), null);
        }

        public void GetPageActions(int pageId, AsyncCallback callback, DomainContext domainContext = null) {
            var context = domainContext as SecurityDomainContext ?? new SecurityDomainContext();
            context.GetAuthorizedPageActions(pageId, invokeOp => this.ExecuteCallback(invokeOp, callback, new Dictionary<string, object> {
                {
                    "Value", invokeOp.Value
                }
            }), null);
        }

        public void SetFavoritePage(int pageId, bool isFavorite, AsyncCallback callback, DomainContext domainContext = null) {
            var context = domainContext as SecurityDomainContext ?? new SecurityDomainContext();
            context.SetFavoritePage(pageId, isFavorite, invokeOp => this.ExecuteCallback(invokeOp, callback, new Dictionary<string, object> {
                {
                    "Value", invokeOp.Value
                }
            }), null);
        }

        public void ChangeFavoritePagesOrder(Dictionary<int, int> pagesOrder, AsyncCallback callback, DomainContext domainContext = null) {
            var context = domainContext as SecurityDomainContext ?? new SecurityDomainContext();
            context.ChangeFavoritePagesOrder(pagesOrder, invokeOp => this.ExecuteCallback(invokeOp, callback, new Dictionary<string, object> {
                {
                    "Value", invokeOp.Value
                }
            }), null);
        }
    }

}