﻿using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Security {
    public class SecurityQueryPanelBase : QueryPanelBase {
        private KeyValueManager keyValueManager;

        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void Initialize() {
            this.KeyValueManager.LoadData();
        }

        protected SecurityQueryPanelBase() {
            this.Initializer.Register(this.Initialize);
        }
    }
}