﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security {
    public static class SecurityUtils {

        internal static readonly Dictionary<string, KeyValuePair[]> KeyValueItems = new Dictionary<string, KeyValuePair[]>();

        private static void ShowDomainServiceOperationWindowInternal(object context) {
            (new RadWindow {
                Header = ((BaseApp)Application.Current).ApplicationName,
                WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                IsTopmost = true,
                ResizeMode = ResizeMode.NoResize,
                CanClose = true,
                CanMove = true,
                Content = new DomainServiceOperationView {
                    Margin = new Thickness(10),
                    DataContext = context,
                },
            }).ShowDialog();
        }

        internal static IEnumerable<KeyValuePair> GetKeyValuePairs(string cacheKey) {
            KeyValuePair[] result;
            return KeyValueItems.TryGetValue(cacheKey, out result) ? result : Enumerable.Empty<KeyValuePair>();
        }

        public static void ShowDomainServiceOperationWindow(Exception ex) {
            ShowDomainServiceOperationWindowInternal(ex);
        }

        public static void ShowDomainServiceOperationWindow(OperationBase operation) {
            ShowDomainServiceOperationWindowInternal(operation);
        }

        public static void Confirm(string text, Action action) {
            if(action == null)
                throw new ArgumentNullException("action");

            var parameters = new DialogParameters {
                Header = BaseApp.Current.ApplicationName,
                Content = text,
            };
            parameters.Closed += (sender, args) => {
                if(args.DialogResult.HasValue && args.DialogResult.Value)
                    action();
            };
            RadWindow.Confirm(parameters);
        }

        /// <summary>
        /// 获取服务端文件的下载路径
        /// </summary>
        /// <param name="path">相对于服务端存储附件根目录的路径</param>
        /// <returns></returns>
        public static Uri GetDownloadFileUrl(string path) {
            return Core.Utils.MakeServerUri(string.Format("{0}{1}", GlobalVar.DOWNLOAD_FILE_URL_PREFIX, path));
        }

        /// <summary>
        /// 获取服务端上传文件处理程序的路径
        /// </summary>
        /// <returns></returns>
        public static Uri GetUploadHandlerUrl() {
            return Core.Utils.MakeServerUri(GlobalVar.UPLOAD_FILE_URL_PERFIX);
        }

        /// <summary>
        /// 获取头像文件的下载路径
        /// </summary>
        /// <param name="photo">用户的头像文件存储路径</param>
        /// <returns></returns>
        public static Uri GetPersonnelPhotoUri(string photo) {
            if(string.IsNullOrEmpty(photo))
                return null;
            return photo == GlobalVar.DEFAULT_AVATAR_URL ? Core.Utils.MakeServerUri(photo) : GetDownloadFileUrl(photo);
        }
    }
}
