﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media.Imaging;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Security {
    public partial class SecurityWorkflowView {
        public SecurityWorkflowView() {
            InitializeComponent();
            this.DataContextChanged += this.SecurityWorkflowView_DataContextChanged;
        }

        public SecurityWorkflowView(WorkflowViewModelBase workflow)
            : this() {
            this.DataContext = workflow;
            this.Loaded += (sender, e) => workflow.Reset();
        }

        private void SecurityWorkflowView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var dataContext = e.OldValue as INotifyPropertyChanged;
            if(dataContext != null)
                dataContext.PropertyChanged -= this.DataContext_PropertyChanged;

            dataContext = e.NewValue as INotifyPropertyChanged;
            if(dataContext != null)
                dataContext.PropertyChanged += this.DataContext_PropertyChanged;
        }

        private void DataContext_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(e.PropertyName != "IsFinalPage")
                return;

            var workflow = sender as WorkflowViewModelBase;
            if(workflow == null)
                return;

            this.NextButton.Text = workflow.IsFinalPage ? "完 成" : "下一步";
            var filename = workflow.IsFinalPage ? "workflow-finish.png" : "workflow-next.png";
            this.NextImage.Source = new BitmapImage(new Uri(string.Concat("/Sunlight.Silverlight.Dms;component/Images/", filename), UriKind.Relative));
        }
    }
}
