﻿
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Security {
    public class SecurityWorkflowViewModelBase : WorkflowViewModelBase {

        protected override void OnPageSwitched(string key, View.WorkflowPageBase value) {
            //throw new System.NotImplementedException();
        }

        protected override void OnRequestLeaveCurrentPage(System.Action<bool> canLeaveCallback) {
            //throw new System.NotImplementedException();
        }

        protected override string OnRequestNextPageKey(string currentKey, View.WorkflowPageBase currentPage) {
            return null;
        }

        public override string WorkflowName {
            get {
                return null;
            }
        }
    }
}
