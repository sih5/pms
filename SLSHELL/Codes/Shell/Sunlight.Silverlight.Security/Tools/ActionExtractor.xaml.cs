﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml.Linq;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.ViewModel.Contract;

namespace Sunlight.Silverlight.Security.Tools {
    [PageMeta("SDS", "Tools", "ActionExtractor")]
    public partial class ActionExtractor : IRootView {
        private static readonly XNamespace X = "http://schemas.microsoft.com/winfx/2006/xaml";

        private static Type GetType(string typeName) {
            return AppDomain.CurrentDomain.GetAssemblies().Select(assembly => assembly.GetExportedTypes().SingleOrDefault(t => t.FullName == typeName)).FirstOrDefault(type => type != null);
        }

        private readonly StringBuilder output = new StringBuilder(40960);
        private List<SecurityAction> actions;
        private List<Node> nodes;
        private List<RolePersonnel> roleUsers;

        public object UniqueId {
            get {
                return 0;
            }
        }

        public string Title {
            get {
                return "权限脚本生成工具";
            }
        }

        private void GetUsers() {
            var domainContext = new SecurityDomainContext();
            domainContext.Load(domainContext.GetUsersByQuery(-1, -1, null, null, null, 1), loadOp => {
                if(loadOp.HasError)
                    return;
                this.Dispatcher.BeginInvoke(() => this.MainListBox.ItemsSource = loadOp.Entities.OrderBy(u => u.LoginId));
            }, null);
        }

        private IEnumerable<string> GetActionOperationIdByDataGridPage(string parameter) {
            var type = Type.GetType(parameter, false, true);
            if(type == null)
                yield break;

            if(!typeof(IDataGridViewModel).IsAssignableFrom(type))
                yield break;

            var dataGridViewModel = (IDataGridViewModel)Activator.CreateInstance(type);
            foreach(var @group in dataGridViewModel.OperationGroupViewModels)
                foreach(var item in @group.OperationViewModels)
                    yield return item.UniqueId;
        }

        private IEnumerable<string> GetActionOperationIdByCustomPage(string parameter) {
            var resourceInfo = Application.GetResourceStream(new Uri(parameter, UriKind.Relative));
            if(resourceInfo == null)
                yield break;

            string xaml;
            using(var reader = new StreamReader(resourceInfo.Stream))
                xaml = reader.ReadToEnd();
            var typeName = XElement.Parse(xaml).Attribute(X + "Class").Value;
            var type = GetType(typeName);
            if(type == null)
                yield break;

            if(!typeof(IActionPanelView).IsAssignableFrom(type))
                yield break;

            var actionPanelView = (IActionPanelView)Activator.CreateInstance(type);
            foreach(var @group in actionPanelView.ActionItemGroups)
                foreach(var item in @group.ActionItems)
                    yield return string.Concat(@group.UniqueId, "|", item.UniqueId);
        }

        private void CreateActionsData() {
            this.actions = new List<SecurityAction>();
            var domainContext = new SecurityDomainContext();
            domainContext.Load(domainContext.GetPagesByQuery(null, -1, 2, null, null, 2), loadOp => {
                if(loadOp.HasError)
                    return;
                var pages = loadOp.Entities.OrderBy(p => p.Id);
                foreach(var page in pages) {
                    IEnumerable<string> operationIds = null;
                    switch(page.PageType) {
                        case "DataGrid":
                            operationIds = this.GetActionOperationIdByDataGridPage(page.Parameter);
                            break;
                        case "Custom":
                            operationIds = this.GetActionOperationIdByCustomPage(page.Parameter);
                            break;
                    }
                    if(operationIds != null)
                        foreach(var operationId in operationIds.Where(s => !string.IsNullOrWhiteSpace(s))) {
                            var action = new SecurityAction {
                                Id = this.actions.Any() ? this.actions.Max(a => a.Id) + 1 : 1,
                                PageId = page.Id,
                                OperationId = operationId,
                            };
                            if(!this.actions.Any(a => a.PageId == action.PageId && a.OperationId == action.OperationId))
                                this.actions.Add(action);
                        }
                }
                this.CreateNodesData();
            }, null);
        }

        private void CreateNodesData() {
            var domainContext = new SecurityDomainContext();
            domainContext.Load(domainContext.GetNodesByQuery(), loadOp => {
                if(loadOp.HasError)
                    return;
                this.nodes = loadOp.Entities.ToList();
                foreach(var node in this.nodes.Where(n => n.CategoryType == 1))
                    node.CategoryType = int.MinValue;
                foreach(var action in this.actions.OrderBy(a => a.Id))
                    this.nodes.Add(new Node {
                        Id = this.nodes.Any() ? this.nodes.Max(n => n.Id) + 1 : 1,
                        CategoryType = 1,
                        CategoryId = action.Id,
                    });

                this.CreateRoleUsersData();
            }, null);
        }

        private void CreateRoleUsersData() {
            var domainContext = new SecurityDomainContext();
            domainContext.Load(domainContext.GetRoleUsersByQuery(-1, -1), loadOp => {
                if(loadOp.HasError)
                    return;

                this.roleUsers = loadOp.Entities.ToList();
                this.ShowResult();
            }, null);
        }

        private void OutputActionsData() {
            this.output.AppendLine("-- Actions");
            this.output.AppendLine("TRUNCATE TABLE [Actions];");
            this.output.AppendLine("SET IDENTITY_INSERT [Actions] ON;");
            var count = 0;
            foreach(var action in this.actions.OrderBy(a => a.Id)) {
                if(count % 1000 == 0) {
                    if(count > 0)
                        this.output.AppendLine(";");
                    this.output.AppendLine("INSERT INTO [Actions] (Id, PageId, OperationId, Status) VALUES");
                } else
                    this.output.AppendLine(",");
                this.output.AppendFormat("({0}, {1}, '{2}', 2)", action.Id, action.PageId, action.OperationId);
                count++;
            }
            this.output.AppendLine(";");
            this.output.AppendLine("SET IDENTITY_INSERT [Actions] OFF;");
            this.output.AppendLine();
        }

        private void OutputNodesData() {
            this.output.AppendLine("-- Nodes");
            this.output.AppendLine("DELETE [Nodes] WHERE CategoryType = 1;");
            this.output.AppendLine("SET IDENTITY_INSERT [Nodes] ON;");
            var count = 0;
            foreach(var node in this.nodes.Where(n => n.CategoryType == 1)) {
                if(count % 1000 == 0) {
                    if(count > 0)
                        this.output.AppendLine(";");
                    this.output.AppendLine("INSERT INTO [Nodes] (Id, CategoryType, CategoryId, Status) VALUES");
                } else
                    this.output.AppendLine(",");
                this.output.AppendFormat("({0}, 1, {1}, 2)", node.Id, node.CategoryId);
                count++;
            }
            this.output.AppendLine(";");
            this.output.AppendLine("SET IDENTITY_INSERT [Nodes] OFF;");
            this.output.AppendLine();
        }

        private void OutputRulesData() {
            var userIds = this.MainListBox.SelectedItems == null ? Enumerable.Empty<int>() : this.MainListBox.SelectedItems.Cast<Personnel>().Select(u => u.Id);
            var roleIds = this.roleUsers.Where(ru => userIds.Contains(ru.PersonnelId)).Select(ru => ru.RoleId).Distinct();
            this.output.AppendLine("-- Rules");
            var count = 0;
            foreach(var roleId in roleIds.OrderBy(id => id))
                foreach(var node in this.nodes.Where(n => n.CategoryType == 1)) {
                    if(count % 1000 == 0) {
                        if(count > 0)
                            this.output.AppendLine(";");
                        this.output.AppendLine("INSERT INTO [Rules] VALUES");
                    } else
                        this.output.AppendLine(",");
                    this.output.AppendFormat("({0}, {1})", roleId, node.Id);
                    count++;
                }
            this.output.AppendLine(";");
            this.output.AppendLine();
        }

        private void ShowResult() {
            this.output.Clear();
            this.OutputActionsData();
            this.OutputNodesData();
            this.OutputRulesData();
            this.MainTextBox.Text = this.output.ToString();
            this.MainBusyIndicator.IsBusy = false;
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            this.MainBusyIndicator.IsBusy = true;
            if(this.actions == null)
                this.CreateActionsData();
            else
                this.ShowResult();
        }

        public ActionExtractor() {
            this.InitializeComponent();
            this.GetUsers();
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new NotSupportedException();
        }
    }
}
