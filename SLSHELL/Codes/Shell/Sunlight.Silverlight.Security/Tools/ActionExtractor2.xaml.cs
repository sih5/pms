﻿using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Security.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel.Contract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Xml.Linq;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Security.Tools {
    [PageMeta("SDS", "Tools", "ActionExtractor2")]
    public partial class ActionExtractor2 : IRootView {
        private static readonly XNamespace X = "http://schemas.microsoft.com/winfx/2006/xaml";

        private static Type GetType(string typeName) {
            return AppDomain.CurrentDomain.GetAssemblies().Select(assembly => assembly.GetExportedTypes().SingleOrDefault(t => t.FullName == typeName)).FirstOrDefault(type => type != null);
        }

        private readonly StringBuilder output = new StringBuilder(40960);
        private IEnumerable<SecurityAction> actions;

        public object UniqueId {
            get {
                return 0;
            }
        }

        public string Title {
            get {
                return "Action 数据生成工具";
            }
        }

        private IEnumerable<SecurityAction> GetActionsByDataGridPage(string parameter, int pageId) {
            var type = Type.GetType(parameter, false, true);
            if(type == null)
                yield break;

            if(!typeof(IDataGridViewModel).IsAssignableFrom(type))
                yield break;

            var dataGridViewModel = (IDataGridViewModel)Activator.CreateInstance(type);
            foreach(var @group in dataGridViewModel.OperationGroupViewModels)
                foreach(var item in @group.OperationViewModels)
                    yield return new SecurityAction {
                        PageId = pageId,
                        OperationId = item.UniqueId,
                        Name = item.Name
                    };
        }

        private IEnumerable<SecurityAction> GetActionsByCustomPage(string parameter, int pageId) {
            var resourceInfo = Application.GetResourceStream(new Uri(parameter, UriKind.Relative));
            if(resourceInfo == null)
                yield break;

            string xaml;
            using(var reader = new StreamReader(resourceInfo.Stream))
                xaml = reader.ReadToEnd();
            var typeName = XElement.Parse(xaml).Attribute(X + "Class").Value;
            var type = GetType(typeName);
            if(type == null)
                yield break;

            if(!typeof(IActionPanelView).IsAssignableFrom(type))
                yield break;

            var actionPanelView = (IActionPanelView)Activator.CreateInstance(type);
            foreach(var @group in actionPanelView.ActionItemGroups)
                foreach(var item in @group.ActionItems)
                    yield return new SecurityAction {
                        PageId = pageId,
                        OperationId = string.Concat(@group.UniqueId, "|", item.UniqueId),
                        Name = item.Title
                    };
        }

        private IEnumerable<SecurityAction> GetActionsByDIPage(string pageName, string groupName, string systemName, int pageId) {
            var container = ((BaseApp)Application.Current).Container;
            var frameUri = ShellUtils.GetFrameUriFromMenuItemName(systemName, groupName, pageName);
            var node = (DataManagementViewBase)container.Resolve(typeof(IRootView), frameUri);
            if(node == null)
                yield break;

            var propertyInfo = node.GetType().GetProperty("RibbonTabs", BindingFlags.Instance | BindingFlags.Public);
            var tabs = (IEnumerable<RadRibbonTab>)(propertyInfo.GetValue(node, null));

            foreach(var actionPanel in tabs.SelectMany(tab => tab.Items).OfType<ActionPanelBase>()) {
                var actionItemGroup = actionPanel.ActionItemGroup;
                foreach(var item in actionItemGroup.ActionItems)
                    yield return new SecurityAction {
                        PageId = pageId,
                        OperationId = string.Concat(actionItemGroup.UniqueId, "|", item.UniqueId),
                        Name = item.Title
                    };
            }
        }

        private void CreateActionsData() {
            this.actions = new List<SecurityAction>();
            var domainContext = new SecurityDomainContext();
            domainContext.Load(domainContext.GetPagesQuery(), loadOp => {
                if(loadOp.HasError)
                    return;
                var pages = loadOp.Entities.ToList();
                var begin = DateTime.Now.Ticks;
                foreach(var page in pages.Where(page => page.Type == 2).OrderBy(p => p.Id))
                    switch(page.PageType) {
                        case "DataGrid":
                            this.actions = this.actions.Union(this.GetActionsByDataGridPage(page.Parameter, page.Id));
                            break;
                        case "Custom":
                            this.actions = this.actions.Union(this.GetActionsByCustomPage(page.Parameter, page.Id));
                            break;
                        case "Released": {
                            var group = pages.Single(entity => entity.Id == page.ParentId);
                            var system = pages.Single(entity => entity.Id == group.ParentId);
                            this.actions = this.actions.Union(this.GetActionsByDIPage(page.PageId, group.PageId, system.PageId, page.Id));
                            break;
                        }
                    }
                var exportEnd = DateTime.Now.Ticks;
                Debug.WriteLine("CreateActionsData " + new TimeSpan(exportEnd - begin).TotalMilliseconds);
                this.ShowResult();
                var showEnd = DateTime.Now.Ticks;
                Debug.WriteLine("ShowResult " + new TimeSpan(showEnd - exportEnd).TotalMilliseconds);
            }, null);
        }

        private void OutputActionsDataForMSSQL() {
            this.output.AppendLine("-- Actions");
            this.output.AppendLine("TRUNCATE TABLE [Action];");
            this.output.AppendLine("SET IDENTITY_INSERT [Action] ON;");
            var pageActions = this.actions.OrderBy(a => a.PageId).ToList();
            for(var i = 0; i < pageActions.Count(); i++) {
                if(i % 1000 == 0) {
                    if(i > 0)
                        this.output.AppendLine(";");
                    this.output.AppendLine("INSERT INTO [Action] ([PageId], [OperationId], [Name], [Status]) VALUES");
                } else
                    this.output.AppendLine(",");
                this.output.AppendFormat("({0}, N'{1}', N'{2}', 2)", pageActions[i].PageId, pageActions[i].OperationId, pageActions[i].Name);
            }
            this.output.AppendLine(";");
            this.output.AppendLine("SET IDENTITY_INSERT [Action] OFF;");
            this.output.AppendLine();
        }

        private void OutputActionsDataForOracle() {
            this.output.AppendLine("TRUNCATE TABLE Action");
            this.output.AppendLine("/");
            this.output.AppendLine();
            var pageActions = this.actions.OrderBy(a => a.PageId).ToList();
            foreach(var action in pageActions) {
                this.output.AppendFormat("INSERT INTO Action (Id, PageId, OperationId, Name, Status) VALUES (S_Action.Nextval, {0}, '{1}', '{2}', 2);", action.PageId, action.OperationId, action.Name);
                this.output.AppendLine();
            }
            this.output.AppendLine("/");
            this.output.AppendLine();
        }

        private void ShowResult() {
            this.output.Clear();
            if(this.MssqlType.IsChecked.HasValue && this.MssqlType.IsChecked.Value)
                this.OutputActionsDataForMSSQL();
            if(this.OracleType.IsChecked.HasValue && this.OracleType.IsChecked.Value)
                this.OutputActionsDataForOracle();
            this.MainTextBox.Text = this.output.ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            this.CreateActionsData();
        }

        public ActionExtractor2() {
            this.InitializeComponent();
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new NotSupportedException();
        }
    }
}
