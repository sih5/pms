﻿using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Security {
    public class UIStrings : ModelBase {
        public void Refresh(params string[] propertyNames) {
            this.NotifyOfPropertiesChange(propertyNames.Length == 0 ? this.GetType().GetProperties().Select(p => p.Name).ToArray() : propertyNames);
        }

        public string BusinessName_EntNodeTemplate {
            get {
                return Resources.SecurityUIStrings.BusinessName_EntNodeTemplate;
            }
        }

        public string CustomControl_Enterprise_OrganizationPersonnels {
            get {
                return Resources.SecurityUIStrings.CustomControl_Enterprise_OrganizationPersonnels;
            }
        }

        public string CustomControl_Enterprise_Personnels {
            get {
                return Resources.SecurityUIStrings.CustomControl_Enterprise_Personnels;
            }
        }

        public string DataEditPanel_Enterprise_AdminPassword {
            get {
                return Resources.SecurityUIStrings.DataEditPanel_Enterprise_AdminPassword;
            }
        }

        public string DataEditPanel_Enterprise_EnterpriseCategory {
            get {
                return Resources.SecurityUIStrings.DataEditPanel_Enterprise_EnterpriseCategory;
            }
        }

        public string DataEditView_Add {
            get {
                return Resources.SecurityUIStrings.DataEditView_Add;
            }
        }

        public string DataEditView_Edit {
            get {
                return Resources.SecurityUIStrings.DataEditView_Edit;
            }
        }

        public string DataEditView_Enterprise_Admin {
            get {
                return Resources.SecurityUIStrings.DataEditView_Enterprise_Admin;
            }
        }

        public string DataEditView_Enterprise_AdminNewPassword {
            get {
                return Resources.SecurityUIStrings.DataEditView_Enterprise_AdminNewPassword;
            }
        }

        public string DataEditView_Enterprise_Code {
            get {
                return Resources.SecurityUIStrings.DataEditView_Enterprise_Code;
            }
        }

        public string DataEditView_Enterprise_ConfirmNewPassword {
            get {
                return Resources.SecurityUIStrings.DataEditView_Enterprise_ConfirmNewPassword;
            }
        }

        public string DataEditView_Enterprise_Name {
            get {
                return Resources.SecurityUIStrings.DataEditView_Enterprise_Name;
            }
        }

        public string DataEditView_Enterprise_NewPassword {
            get {
                return Resources.SecurityUIStrings.DataEditView_Enterprise_NewPassword;
            }
        }

        public string DataEditView_Personnel_Email {
            get {
                return Resources.SecurityUIStrings.DataEditView_Personnel_Email;
            }
        }

        public string UserControl_Button_Query {
            get {
                return Resources.SecurityUIStrings.UserControl_Button_Query;
            }
        }

        public string UserControl_Filter_Name {
            get {
                return Resources.SecurityUIStrings.UserControl_Filter_Name;
            }
        }

        public string UserControl_Filter_Organization {
            get {
                return Resources.SecurityUIStrings.UserControl_Filter_Organization;
            }
        }

    }
}
