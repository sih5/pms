﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Sunlight.Silverlight.Security {
    public sealed class KeyValueItemConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if(value == null || parameter == null)
                return null;

            var result = KeyValueManager.GetKeyValuePairs(parameter as string).FirstOrDefault(kvp => kvp.Key == (int)value);
            return result == null ? value.ToString() : result.Value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
