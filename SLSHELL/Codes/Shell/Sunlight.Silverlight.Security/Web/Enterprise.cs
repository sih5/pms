﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Security.Web {
    partial class Enterprise {
        private string password;
        private string confirmPassword;

        public string Password {
            get {
                return this.password;
            }
            set {
                this.password = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Password"));
            }
        }

        public string ConfirmPassword {
            get {
                return this.confirmPassword;
            }
            set {
                this.confirmPassword = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ConfirmPassword"));
            }
        }
    }
}
