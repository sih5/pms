﻿
using System.Collections.ObjectModel;

namespace Sunlight.Silverlight.Security.Web {
    public class EnterpriseInfo {
        public SecurityDomainContext DomainContext {
            get;
            set;
        }

        public ObservableCollection<Enterprise> Enterprises {
            get;
            set;
        }

        public int? EntOrganizationTplId {
            get;
            set;
        }

        public int[] RoleTemplateIds {
            get;
            set;
        }
    }
}
