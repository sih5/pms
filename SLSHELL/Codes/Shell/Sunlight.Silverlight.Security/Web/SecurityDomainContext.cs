﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Security.Web {
    partial class SecurityDomainContext {
        private bool isInvoking;

        public bool IsInvoking {
            set {
                this.isInvoking = value;
                this.RaisePropertyChanged("IsBusy");
            }
        }

        public bool IsBusy {
            get {
                return this.IsLoading || this.IsSubmitting || this.isInvoking;
            }
        }

        partial void OnCreated() {
            ((WebDomainClient<ISecurityDomainServiceContract>)this.DomainClient).ChannelFactory.Endpoint.Binding.SendTimeout = TimeSpan.FromMinutes(5);

            this.PropertyChanged += this.OnPropertyChanged;
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(e.PropertyName == "IsLoading" || e.PropertyName == "IsSubmitting")
                this.RaisePropertyChanged("IsBusy");
        }

        public override InvokeOperation InvokeOperation(string operationName, Type returnType, IDictionary<string, object> parameters, bool hasSideEffects, Action<InvokeOperation> callback, object userState) {
            this.IsInvoking = true;
            return base.InvokeOperation(operationName, returnType, parameters, hasSideEffects, callback, userState);
        }

        public override InvokeOperation<TValue> InvokeOperation<TValue>(string operationName, Type returnType, IDictionary<string, object> parameters, bool hasSideEffects, Action<InvokeOperation<TValue>> callback, object userState) {
            this.IsInvoking = true;
            return base.InvokeOperation(operationName, returnType, parameters, hasSideEffects, callback, userState);
        }
    }
}
