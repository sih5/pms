﻿using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Sunlight.Silverlight.Security.Web {
    public partial class Personnel {
        private string oldPassword;
        private string password;
        private string confirmPassword;

        public string OldPassword {
            get {
                return this.oldPassword;
            }
            set {
                if((this.oldPassword != value)) {
                    this.ValidateProperty("OldPassword", value);
                    this.oldPassword = value;
                    this.RaisePropertyChanged("OldPassword");
                }
            }
        }

        [Display(Name = "Personnel_Password", ResourceType = typeof(EntityStrings))]
        public string Password {
            get {
                return this.password;
            }
            set {
                if((this.password != value)) {
                    this.ValidateProperty("Password", value);
                    this.password = value;
                    this.RaisePropertyChanged("Password");
                }
            }
        }

        public string ConfirmPassword {
            get {
                return this.confirmPassword;
            }
            set {
                if((this.confirmPassword != value)) {
                    this.ValidateProperty("ConfirmPassword", value);
                    this.confirmPassword = value;
                    this.RaisePropertyChanged("ConfirmPassword");
                }
            }
        }

        [Display(Name = "Personnel_OrganizationPersonnels", ResourceType = typeof(Resources.EntityStrings))]
        public string Organizations {
            get {
                var nameArray = this.OrganizationPersonnels.Select(e => e.Organization.Name).ToArray();
                return string.Join(";", nameArray, 0, nameArray.Length);
            }
        }

        [Display(Name = "Personnel_RolePersonnels", ResourceType = typeof(Resources.EntityStrings))]
        public string Roles {
            get {
                if(!this.RolePersonnels.Any())
                    return string.Empty;
                var nameArray = this.RolePersonnels.Select(e => e.Role.Name).ToArray();
                return string.Join(";", nameArray, 0, nameArray.Length);
            }
        }
    }
}
