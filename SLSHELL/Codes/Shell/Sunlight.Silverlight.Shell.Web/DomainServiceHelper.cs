﻿using System;
using System.Configuration;
using System.Data.EntityClient;
using System.Text.RegularExpressions;
using Sunlight.Silverlight.License;

namespace Sunlight.Silverlight.Shell.Web {
    public static class DomainServiceHelper {
        public static string CreateEntityConnectionString(string prefix, string provider = "System.Data.SqlClient") {
            var connStringName = prefix + "Entities";
            if(ConfigurationManager.ConnectionStrings[connStringName] != null)
                return null;

            var connStringSettings = ConfigurationManager.ConnectionStrings[prefix + "ConnectionString"];
            var dbConnString = connStringSettings == null ? string.Format("Data Source=127.0.0.1;Initial Catalog={0};User ID=sa", prefix) : connStringSettings.ConnectionString;
            dbConnString = Regex.Replace(dbConnString, @"Password\s*=\s*[^\s;]+\s*;*", string.Empty, RegexOptions.IgnoreCase).TrimEnd(';');
            string password;
            using(var manager = new LicenseManager()) {
                string errorMessage;
                password = manager.GetValue(prefix + "DbPassword", out errorMessage);
                if(!string.IsNullOrEmpty(errorMessage))
                    throw new Exception(errorMessage);
            }
            if(password != null)
                dbConnString = string.Concat(dbConnString, ";Password=", password);

            var connStringBuilder = new EntityConnectionStringBuilder {
                Metadata = string.Format("res://*/{0}DomainModel.csdl|res://*/{0}DomainModel.ssdl|res://*/{0}DomainModel.msl", prefix),
                Provider = provider,
                ProviderConnectionString = dbConnString,
            };
            return connStringBuilder.ToString();
        }
    }
}
