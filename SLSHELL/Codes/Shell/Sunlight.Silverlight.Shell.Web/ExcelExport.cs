﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web.Caching;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace Sunlight.Silverlight.Shell.Web {

    public delegate string GetFieldTitleHandle(string fieldName);
    public delegate string GetBoolValueHandle(string fieldName, Boolean value);
    public delegate string GetEnumValueHandle(string fieldName, int value);

    public class ExcelExport : IDisposable {
        private readonly SqlConnection conn;
        private readonly string connectionStr;
        private readonly List<string> fieldTitles;
        private readonly SqlCommand sqlCmd;
        private string aFileName;

        public ExcelExport(string excelFileName, string connectionString, string exportSql) {
            if(string.IsNullOrEmpty(connectionString.Trim()))
                throw new ArgumentNullException("connectionString", "不能为空字符串");
            if(string.IsNullOrEmpty(exportSql.Trim()))
                throw new ArgumentNullException("exportSql", "查询SQL不能为空字符串");
            if(string.IsNullOrEmpty(excelFileName.Trim()))
                throw new ArgumentNullException("excelFileName", "必须指定Excel文件名");

            FileName = excelFileName;
            fieldTitles = new List<string>();
            connectionStr = connectionString;
            conn = new SqlConnection(connectionStr);
            conn.Open();
            sqlCmd = new SqlCommand(exportSql, conn);
        }

        public ExcelExport(string excelFileName, SqlCommand aCommand) {
            if(string.IsNullOrEmpty(excelFileName.Trim()))
                throw new ArgumentNullException("excelFileName", "必须指定Excel文件名");
            FileName = excelFileName;
            if(sqlCmd == null) {
                throw new ArgumentNullException("aCommand", "必须指定SQLCommand类的实例");
            }
            sqlCmd = aCommand;
        }

        public ExcelExportHelper ExportHelper {
            get;
            set;
        }

        public string FileName {
            get {
                return aFileName;
            }
            set {
                aFileName = value.Trim();
                while(File.Exists(aFileName)) {
                    aFileName = Path.GetDirectoryName(aFileName) + Path.GetFileNameWithoutExtension(aFileName) +
                                 "_" + DateTime.Now.ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("hhmmss") +
                                 ".xls";
                }
            }
        }

        #region IDisposable Members

        public void Dispose() {
            if(conn != null) {
                conn.Close();
                sqlCmd.Dispose();
                conn.Dispose();
            }
        }
        #endregion

        /// <summary>
        /// 获取字段标题事件
        /// </summary>        
        public event GetFieldTitleHandle OnGetFieldTitle;

        /// <summary>
        /// 获得布尔值自定义值
        /// </summary>       
        public event GetBoolValueHandle OnGetBoolValue;

        /// <summary>
        /// 获得枚举自定义值
        /// </summary>        
        public event GetEnumValueHandle OnGetEnumValue;

        public Boolean Export() {
            if(sqlCmd == null) {
                throw new DatabaseNotEnabledForNotificationException("数据源未就绪。");
            }
            var reader = sqlCmd.ExecuteReader();
            if(!reader.HasRows) throw new DataException("查询无数据");

            try {
                IWorkbook wb = new HSSFWorkbook();
                ISheet st = wb.CreateSheet();

                int iRow = 0;
                while(reader.Read()) {
                    IRow row = st.CreateRow(iRow);
                    if(iRow == 0) {
                        for(int i = 0; i < reader.FieldCount; i++) {
                            string sName = "";
                            if(ExportHelper != null)
                                sName = ExportHelper.GetColumnHeader(reader.GetName(i));
                            else {
                                if((fieldTitles.IndexOf(reader.GetName(i)) == -1)) {
                                    sName = reader.GetName(i);
                                    if(OnGetFieldTitle != null) sName = OnGetFieldTitle(sName);
                                }
                            }
                            row.CreateCell(i).SetCellValue(sName);
                        }
                        iRow++;
                        row = st.CreateRow(iRow);
                    }
                    for(int i = 0; i < reader.FieldCount; i++) {
                        string retvalue = "";
                        switch(reader.GetFieldType(i).Name) {
                            case "String":
                                row.CreateCell(i).SetCellValue(reader.GetString(i));
                                break;
                            case "Char":
                                row.CreateCell(i).SetCellValue(reader.GetChar(i));
                                break;
                            case "Boolean":
                                if(ExportHelper != null)
                                    retvalue = ExportHelper.GetBoolEnum(reader.GetName(i), reader.GetBoolean(i));
                                else
                                    retvalue = reader.GetBoolean(i) ? "是" : "否";
                                if(OnGetBoolValue != null)
                                    retvalue = OnGetBoolValue(reader.GetName(i), reader.GetBoolean(i));
                                row.CreateCell(i).SetCellValue(retvalue);
                                break;
                            case "Int16":
                                if(ExportHelper != null)
                                    retvalue = ExportHelper.GetEnumValue(reader.GetName(i), reader.GetInt16(i));
                                if(OnGetEnumValue != null)
                                    retvalue = OnGetEnumValue(reader.GetName(i), reader.GetInt16(i));
                                if(retvalue.Equals(reader.GetInt16(i).ToString(CultureInfo.InvariantCulture)))
                                    row.CreateCell(i).SetCellValue(reader.GetInt16(i));
                                else row.CreateCell(i).SetCellValue(retvalue);
                                break;
                            case "Int32":
                                if(ExportHelper != null)
                                    retvalue = ExportHelper.GetEnumValue(reader.GetName(i), reader.GetInt32(i));
                                if(OnGetEnumValue != null)
                                    retvalue = OnGetEnumValue(reader.GetName(i), reader.GetInt32(i));
                                if(retvalue.Equals(reader.GetInt32(i).ToString(CultureInfo.InvariantCulture)))
                                    row.CreateCell(i).SetCellValue(reader.GetInt32(i));
                                else row.CreateCell(i).SetCellValue(retvalue);
                                break;
                            case "Int64":
                                row.CreateCell(i).SetCellValue(reader.GetInt64(i));
                                break;
                            case "DateTime":
                                row.CreateCell(i).SetCellValue(reader.GetDateTime(i).ToLongDateString());
                                break;
                            case "Byte":
                                if(ExportHelper != null)
                                    retvalue = ExportHelper.GetEnumValue(reader.GetName(i), reader.GetByte(i));
                                if(OnGetEnumValue != null)
                                    retvalue = OnGetEnumValue(reader.GetName(i), reader.GetByte(i));
                                if(retvalue.Equals(reader.GetByte(i).ToString(CultureInfo.InvariantCulture)))
                                    row.CreateCell(i).SetCellValue(reader.GetByte(i));
                                else row.CreateCell(i).SetCellValue(retvalue);
                                break;
                            case "Float":
                                row.CreateCell(i).SetCellValue(reader.GetFloat(i));
                                break;
                            case "Decimal":
                                row.CreateCell(i).SetCellValue((Double)reader.GetDecimal(i));
                                break;
                            case "Double":
                                row.CreateCell(i).SetCellValue(reader.GetDouble(i));
                                break;
                            case "Guid":
                                row.CreateCell(i).SetCellValue(reader.GetGuid(i).ToString());
                                break;
                            default:
                                row.CreateCell(i).SetCellValue(reader.GetString(i));
                                break;
                        }
                    }
                    iRow++;
                }
                reader.Close();

                using(var xlsF = new FileStream(FileName, FileMode.Create)) {
                    wb.Write(xlsF);
                    xlsF.Close();
                }
            } catch {
                return false;
            }
            return true;
        }
    }
}