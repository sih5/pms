﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;

namespace Sunlight.Silverlight.Shell.Web {
    public class ExcelExportHelper {
        private readonly Dictionary<string, Dictionary<int, string>> eNumList;
        private readonly Dictionary<string, string> columnHeader;
        private readonly Dictionary<string, Dictionary<Boolean, string>> eNumBool;

        public ExcelExportHelper() {
            eNumList = new Dictionary<string, Dictionary<int, string>>();
            columnHeader = new Dictionary<string, string>();
            eNumBool = new Dictionary<string, Dictionary<Boolean, string>>();
        }

        public void AddEnum(string fieldName, params KeyValuePair<int, string>[] valuePair) {
            if(string.IsNullOrEmpty(fieldName) || valuePair.Length <= 0) return;
            if(eNumList.ContainsKey(fieldName)) {
                foreach(var value in valuePair) {
                    var sValue = string.IsNullOrEmpty(value.Value) ? value.Key.ToString(CultureInfo.InvariantCulture) : value.Value;
                    eNumList[fieldName].Add(value.Key, sValue);
                }
            } else {
                var tmpDict = new Dictionary<int, string>();
                foreach(var value in valuePair) {
                    var sValue = string.IsNullOrEmpty(value.Value) ? value.Key.ToString(CultureInfo.InvariantCulture) : value.Value;
                    tmpDict.Add(value.Key, sValue);
                }
                eNumList.Add(fieldName.ToLower(), tmpDict);
            }
        }

        public Boolean LoadEnumFromDb(string conntionStr, string keyValueTableName = "KeyValueItem",
                                      params KeyValuePair<string, string>[] fieldPair) {
            if(fieldPair.Length <= 0 || string.IsNullOrEmpty(conntionStr.Trim()))
                return false;
            if(string.IsNullOrEmpty(keyValueTableName.Trim())) keyValueTableName = "KeyValueItem";
            var conn = new SqlConnection(conntionStr);
            conn.Open();
            var cmd = new SqlCommand("SELECT [Key],Value FROM " + keyValueTableName, conn);
            foreach(var value in fieldPair) {
                if(string.IsNullOrEmpty(value.Key) || string.IsNullOrEmpty(value.Value)) continue;
                cmd.CommandText = string.Format("{0} WHERE name='{1}'",
                                                "SELECT [Key],Value FROM " + keyValueTableName, value.Value);
                var reader = cmd.ExecuteReader();
                var tmpValue = new Dictionary<int, string>();
                while(reader.Read()) {
                    tmpValue.Add(reader.GetInt32(0), reader.GetString(1));
                }
                eNumList.Add(value.Key.ToLower(), tmpValue);
                reader.Close();
            }
            return true;
        }

        public void AddBooleanEnum(string fieldName, string trueValue, string falseValue) {
            if(string.IsNullOrEmpty(fieldName)) return;
            if(string.IsNullOrEmpty(trueValue)) trueValue = "真";
            if(string.IsNullOrEmpty(falseValue)) falseValue = "假";
            if(eNumBool.ContainsKey(fieldName.ToLower())) {
                eNumBool[fieldName.ToLower()][true] = trueValue;
                eNumBool[fieldName.ToLower()][false] = falseValue;
            } else {
                var tmpDict = new Dictionary<Boolean, string> { { true, trueValue }, { false, falseValue } };
                eNumBool.Add(fieldName.ToLower(), tmpDict);
            }
        }

        public void AddColumnHeader(params KeyValuePair<string, string>[] columnTitle) {
            foreach(var valuePair in columnTitle) {
                if(string.IsNullOrEmpty(valuePair.Key)) continue;
                var sValue = valuePair.Value;
                if(string.IsNullOrEmpty(valuePair.Value)) sValue = valuePair.Key;
                if(columnHeader.ContainsKey(valuePair.Key.ToLower()))
                    columnHeader[valuePair.Key.ToLower()] = sValue;
                else columnHeader.Add(valuePair.Key.ToLower(), sValue);
            }
        }

        public string GetEnumValue(string fieldName, int enumKey) {
            if(!eNumList.ContainsKey(fieldName.ToLower()))
                return enumKey.ToString(CultureInfo.InvariantCulture);
            var tmpDict = eNumList[fieldName.ToLower()];
            if(!tmpDict.ContainsKey(enumKey))
                return enumKey.ToString(CultureInfo.InvariantCulture);
            return tmpDict[enumKey];
        }

        public string GetColumnHeader(string fieldName) {
            if(!columnHeader.ContainsKey(fieldName.ToLower()))
                return fieldName;
            return columnHeader[fieldName.ToLower()];
        }

        public string GetBoolEnum(string fieldName, Boolean value) {
            if(!eNumBool.ContainsKey(fieldName))
                return value ? "真" : "假";
            return eNumBool[fieldName][value];
        }
    }
}