﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace Sunlight.Silverlight.Shell.Web {
    public class ExcelNoDataException : Exception {
        public ExcelNoDataException()
            : base("Excel文件无数据") {
        }
    }

    public delegate string ParseValueHandle(ExcelColumns source, string valueString);

    public delegate Boolean CustomVerifyHandle(ExcelImport source, int col, int row, string cellValue);

    public class ExcelColumns {
        private readonly string sName;

        public string Name {
            get {
                return sName;
            }
        }

        public ExcelImportHelper Helper {
            get;
            set;
        }

        /// <summary>
        /// 绑定字段名
        /// </summary>
        public string FieldName {
            get;
            set;
        }

        /// <summary>
        /// Excel列索引值
        /// </summary>
        public int Index {
            get;
            set;
        }

        public ExcelColumns(string columnName, int index) {
            if(string.IsNullOrEmpty(columnName)) throw new ArgumentNullException("columnName");
            sName = columnName;
            Index = index;
        }
    }

    public class ExcelImport : IDisposable {

        private readonly DataSet cacheTable;
        private SqlDataAdapter adapter;

        private readonly IWorkbook wb;
        private readonly ISheet sheet;

        private readonly SqlConnection conn;

        public event ParseValueHandle OnParseValue;
        public event CustomVerifyHandle OnCustomVerify;

        public ISheet Sheet {
            get {
                return sheet;
            }
        }

        /// <summary>
        /// 第一行是否为标题
        /// </summary>
        public Boolean FirstRowIsTitle {
            get;
            set;
        }
        /// <summary>
        /// 字段列表
        /// </summary>
        public Dictionary<string, List<ExcelColumns>> Columns {
            get;
            set;
        }

        public ExcelImportHelper ImportHelper {
            get;
            set;
        }

        private int ExcelIndexOfName(string columnName) {
            if(FirstRowIsTitle) {
                var row = sheet.GetRow(0);
                for(int i = 0; i <= row.LastCellNum - 1; i++) {
                    var cell = row.GetCell(i);
                    if(cell == null) continue;
                    if(cell.StringCellValue.Trim().Equals(columnName, StringComparison.OrdinalIgnoreCase)) return i;
                }
            } else {
                int iTmp;
                if(int.TryParse(columnName, out iTmp)) return iTmp;
            }
            return -1;
        }

        private int IndexOfColumnName(string columnName, string tableName) {
            var sTable = tableName.ToUpper();
            if(Columns.ContainsKey(sTable)) {
                for(int i = 0; i <= Columns[sTable].Count - 1; i++) {
                    if(columnName.Equals(Columns[sTable][i].Name, StringComparison.OrdinalIgnoreCase)) return i;
                }
            }
            return -1;
        }

        private int IndexOfFieldName(string fieldName, string tableName) {
            var sTable = tableName.ToUpper();
            if(Columns.ContainsKey(sTable)) {
                for(int i = 0; i <= Columns[sTable].Count - 1; i++) {
                    if(fieldName.Equals(Columns[sTable][i].FieldName, StringComparison.OrdinalIgnoreCase)) return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// 校验数据
        /// </summary>
        private Boolean Verify() {
            if(Columns.Count == 0) throw new Exception("请先调用AddColumnDataSource方法，以指定Excel列对应的表及字段。");

            //检测空值
            foreach(DataTable table in cacheTable.Tables) {
                foreach(DataColumn column in table.Columns) {
                    if(column.AutoIncrement) continue;
                    if((!column.AllowDBNull) && (IndexOfFieldName(column.ColumnName, table.TableName) == -1))
                        throw new Exception(String.Format("数据表 {0} 的字段 {1}必须有值，请调用AddColumnDataSource方法，以指定该字段在Excel中对应列。", table.TableName, column.ColumnName));
                }
            }

            foreach(var tableitem in Columns) {
                var dt = cacheTable.Tables[tableitem.Key];
                for(int i = FirstRowIsTitle ? 1 : 0; i <= sheet.LastRowNum; i++) {
                    var row = sheet.GetRow(i);
                    var datarow = dt.NewRow();
                    foreach(var fields in tableitem.Value) {
                        if(fields.Index == -1) continue;
                        var cell = row.GetCell(fields.Index);
                        if(cell == null) {
                            dt.Rows.Remove(datarow);
                            break;
                        }

                        string sValue = "";

                        if(OnCustomVerify != null) {
                            string sTmp = "";
                            if(cell.CellType == CellType.STRING || cell.CellType == CellType.FORMULA || cell.CellType == CellType.Unknown)
                                sTmp = cell.StringCellValue.Trim();
                            else if(cell.CellType == CellType.NUMERIC)
                                sTmp = cell.NumericCellValue.ToString();
                            else if(cell.CellType == CellType.BOOLEAN)
                                sTmp = cell.BooleanCellValue.ToString();
                            if(!OnCustomVerify(this, fields.Index, i, sTmp)) {
                                dt.Rows.Remove(datarow);
                                break;
                            }
                        };


                        if(ImportHelper != null) {
                            int? iValue = null;
                            switch(dt.Columns[fields.FieldName].DataType.Name.ToLower().Trim()) {
                                case "byte":
                                case "int16":
                                case "int32":
                                case "boolean":
                                    if(cell.CellType == CellType.STRING || cell.CellType == CellType.FORMULA || cell.CellType == CellType.Unknown)
                                        iValue = ImportHelper.GetEnumValue(fields.FieldName, cell.StringCellValue.Trim());
                                    else if(cell.CellType == CellType.NUMERIC)
                                        iValue = ImportHelper.GetEnumValue(fields.FieldName, cell.NumericCellValue.ToString());
                                    else if(cell.CellType == CellType.BOOLEAN)
                                        iValue = ImportHelper.GetEnumValue(fields.FieldName, cell.BooleanCellValue ? "1" : "0");

                                    if(iValue == null) {
                                        Boolean? bRet = null;
                                        if(cell.CellType == CellType.STRING || cell.CellType == CellType.FORMULA || cell.CellType == CellType.Unknown)
                                            bRet = ImportHelper.GetBoolEnum(fields.FieldName, cell.StringCellValue.Trim());
                                        else if(cell.CellType == CellType.NUMERIC)
                                            bRet = ImportHelper.GetBoolEnum(fields.FieldName, cell.NumericCellValue.ToString());
                                        else if(cell.CellType == CellType.BOOLEAN)
                                            bRet = ImportHelper.GetBoolEnum(fields.FieldName, cell.BooleanCellValue ? "TRUE" : "FALSE");
                                        if(bRet != null) sValue = (Boolean)bRet ? "1" : "0";
                                    } else sValue = iValue.ToString();
                                    break;
                            }
                        }


                        if(OnParseValue != null) {
                            switch(cell.CellType) {
                                case CellType.NUMERIC:
                                    sValue = OnParseValue(fields, cell.NumericCellValue.ToString()).Trim() == "" ? cell.NumericCellValue.ToString() : sValue;
                                    break;
                                case CellType.BOOLEAN:
                                    sValue = OnParseValue(fields, cell.BooleanCellValue.ToString()) == "" ? cell.BooleanCellValue.ToString() : sValue;
                                    break;
                                case CellType.ERROR:
                                case CellType.BLANK:
                                    throw new Exception(string.Format("Excel中第{0}行，第{1}列值有误！", i, fields.Index));
                                default:
                                    sValue = OnParseValue(fields, cell.StringCellValue.Trim());
                                    break;
                            }
                        }
                        if(!string.IsNullOrEmpty(sValue))
                            datarow[fields.FieldName] = sValue;
                        else {
                            switch(cell.CellType) {
                                case CellType.NUMERIC:
                                    datarow[fields.FieldName] = cell.NumericCellValue.ToString();
                                    break;
                                case CellType.BOOLEAN:
                                    datarow[fields.FieldName] = cell.BooleanCellValue ? "1" : "0";
                                    break;
                                case CellType.ERROR:
                                case CellType.BLANK:
                                    throw new Exception(string.Format("Excel中第{0}行，第{1}列值有误！", i, fields.Index));
                                default:
                                    datarow[fields.FieldName] = cell.StringCellValue.Trim();
                                    break;
                            }
                        }
                    }
                    if(datarow.HasErrors) throw new Exception(datarow.RowError);
                    dt.Rows.Add(datarow);
                }
            }
            return true;
        }

        /// <summary>
        /// 指定数据表对应列
        /// </summary>
        /// <param name="dbTableName">数据表名称</param>        
        /// <param name="columnSources"> 字段对应列(字段名，列名)</param>
        public void AddColumnDataSource(string dbTableName, Dictionary<string, string> columnSources) {
            //填充DataSet
            var sTable = dbTableName.ToUpper();
            if(cacheTable.Tables.IndexOf(sTable) == -1) {
                adapter = new SqlDataAdapter(string.Format("SELECT * FROM {0}", sTable), conn);
                adapter.FillSchema(cacheTable, SchemaType.Mapped, sTable);
                //adapter.Fill(cacheTable, sTable);
            }
            //更新Columns属性
            foreach(var columnsource in columnSources) {
                var sFieldName = columnsource.Key.ToUpper();
                var sColumnName = columnsource.Value.ToUpper();
                var excelIndex = ExcelIndexOfName(sColumnName);

                if(Columns.ContainsKey(sTable)) {
                    var index = IndexOfColumnName(sColumnName, sTable);
                    var cols = Columns[sTable];
                    if(index == -1) {
                        cols.Add(new ExcelColumns(sColumnName, excelIndex) {
                            FieldName = sFieldName
                        });
                    } else {
                        cols[index].FieldName = sFieldName;
                        cols[index].Index = excelIndex;
                    }
                } else
                    Columns.Add(sTable,
                                new List<ExcelColumns>() { new ExcelColumns(sColumnName, excelIndex) { FieldName = sFieldName } });
            }
        }

        public ExcelImport(string excelFileName, string connectionStr, Boolean firstRowIsTitle = true, string sheetName = "") {
            if(string.IsNullOrEmpty(connectionStr))
                throw new ArgumentNullException("connectionStr");
            if(string.IsNullOrEmpty(excelFileName) || !File.Exists(excelFileName))
                throw new ArgumentException("未指定Excel文件名或找不到指定文件。", excelFileName);
            FirstRowIsTitle = firstRowIsTitle;
            Columns = new Dictionary<string, List<ExcelColumns>>();
            conn = new SqlConnection(connectionStr);
            conn.Open();
            cacheTable = new DataSet();
            using(var xlsF = new FileStream(excelFileName, FileMode.Open, FileAccess.Read, FileShare.Read)) {
                wb = new HSSFWorkbook(xlsF);
                sheet = sheetName == "" ? wb.GetSheetAt(0) : wb.GetSheet(sheetName);
                if(sheet == null) throw new Exception("找不到符合条件的Sheet");
                if(sheet.LastRowNum == 0) throw new ExcelNoDataException();
            }
        }

        public void Import(Boolean commitAtEachRow = false) {
            if(!Verify()) return;
            var qb = new SqlCommandBuilder(adapter);
            SqlTransaction trans = null;
            if(!commitAtEachRow) trans = conn.BeginTransaction();
            try {
                foreach(var table in cacheTable.Tables)
                    adapter.Update((DataTable)table);
            } catch(Exception e) {
                if(trans != null) trans.Rollback();
                throw new Exception(e.Message);
            } finally {
                if(trans != null) trans.Commit();
            }
        }

        public void Dispose() {
            Columns.Clear();
            cacheTable.Dispose();
            conn.Dispose();
        }
    }
}