﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;

namespace Sunlight.Silverlight.Shell.Web {
    public class ExcelImportHelper {
        private readonly Dictionary<string, Dictionary<string, int>> eNumList;
        private readonly Dictionary<string, Dictionary<string, Boolean>> eNumBool;

        public ExcelImportHelper() {
            eNumList = new Dictionary<string, Dictionary<string, int>>();
            eNumBool = new Dictionary<string, Dictionary<string, Boolean>>();
        }

        /// <summary>
        /// 添加枚举替换值
        /// </summary>
        /// <param name="fieldName">替换字段</param>
        /// <param name="valuePair">替换值(第一个值为Excel实际值，第二个为替换值)</param>
        public void AddEnum(string fieldName, params KeyValuePair<string, int>[] valuePair) {
            if(string.IsNullOrEmpty(fieldName) || valuePair.Length <= 0) return;
            if(eNumList.ContainsKey(fieldName)) {
                foreach(var value in valuePair) {
                    var sKey = string.IsNullOrEmpty(value.Key) ? value.Value.ToString(CultureInfo.InvariantCulture) : value.Key;
                    eNumList[fieldName].Add(sKey, value.Value);
                }
            } else {
                foreach(var value in valuePair) {
                    var sKey = string.IsNullOrEmpty(value.Key) ? value.Value.ToString(CultureInfo.InvariantCulture) : value.Key;
                    eNumList.Add(fieldName.ToLower(), new Dictionary<string, int>() { { sKey, value.Value } });
                }
            }
        }

        public void AddBooleanEnum(string fieldName, string trueValue, string falseValue) {
            if(string.IsNullOrEmpty(fieldName)) return;
            if(string.IsNullOrEmpty(trueValue)) trueValue = "真";
            if(string.IsNullOrEmpty(falseValue)) falseValue = "假";
            if(eNumBool.ContainsKey(fieldName.ToLower())) {
                eNumBool[fieldName.ToLower()][trueValue] = true;
                eNumBool[fieldName.ToLower()][falseValue] = false;
            } else {
                eNumBool.Add(fieldName.ToLower(), new Dictionary<string, Boolean> { { trueValue, true }, { falseValue, false } });
            }
        }

        /// <summary>
        /// 从数据库调入枚举值列表
        /// </summary>
        /// <param name="conntionStr">连接字符串</param>
        /// <param name="keyValueTableName">枚举值表名</param>
        /// <param name="fieldPair">枚举值列表（第一个参数为目标表中被替换的字段名，第二个参数为枚举值表名里枚举名称限制值）</param>
        /// <returns></returns>
        public Boolean LoadEnumFromDb(string conntionStr, string keyValueTableName = "KeyValueItem",
                                      params KeyValuePair<string, string>[] fieldPair) {
            if(fieldPair.Length <= 0 || string.IsNullOrEmpty(conntionStr.Trim()))
                return false;
            if(string.IsNullOrEmpty(keyValueTableName.Trim())) keyValueTableName = "KeyValueItem";
            var conn = new SqlConnection(conntionStr);
            conn.Open();
            var cmd = new SqlCommand("SELECT [Key],Value FROM " + keyValueTableName, conn);
            foreach(var value in fieldPair) {
                if(string.IsNullOrEmpty(value.Key) || string.IsNullOrEmpty(value.Value)) continue;
                cmd.CommandText = string.Format("{0} WHERE Name='{1}'",
                                                "SELECT [Key],Value FROM " + keyValueTableName, value.Value);
                var reader = cmd.ExecuteReader();
                var tmpValue = new Dictionary<string, int>();
                while(reader.Read()) {
                    tmpValue.Add(reader.GetString(1).ToLower(), reader.GetInt32(0));
                }
                eNumList.Add(value.Key.ToLower(), tmpValue);
                reader.Close();
            }
            return true;
        }

        public int? GetEnumValue(string fieldName, string enumKey) {
            if(!eNumList.ContainsKey(fieldName.ToLower()))
                return null;
            var tmpDict = eNumList[fieldName.ToLower()];
            if(!tmpDict.ContainsKey(enumKey))
                return null;
            return tmpDict[enumKey];
        }

        public Boolean? GetBoolEnum(string fieldName, string value) {
            if(!eNumBool.ContainsKey(fieldName))
                return null;
            return eNumBool[fieldName][value];
        }
    }
}