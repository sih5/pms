﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Sunlight.Silverlight.Shell.Web {
    public class ExportHelper {
        // ,
        private const char COMMA = ',';
        // "
        private const char DOUBLE_QUOTES = '"';

        private const string ExtensionName = ".csv";

        private static readonly string storageDirectoryName;
        private static readonly string storageDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Excel");
        private static readonly int storageDate = -2;

        static ExportHelper() {
            int tmpStorageDate;
            if(int.TryParse(ConfigurationManager.AppSettings["storageDate"], out tmpStorageDate) && tmpStorageDate > 0)
                storageDate = -tmpStorageDate;
            var tmpStorageDirectory = ConfigurationManager.AppSettings["storageDirectory"];
            if(!string.IsNullOrWhiteSpace(tmpStorageDirectory))
                storageDirectory = tmpStorageDirectory;
            if(!Directory.Exists(storageDirectory))
                Directory.CreateDirectory(storageDirectory);
            storageDirectoryName = Path.GetFileName(storageDirectory);
        }

        private void Export(IDataReader reader, Stream fileStream) {
            using(var streamWriter = new StreamWriter(fileStream, Encoding.GetEncoding("gb18030"))) {
                var stringBuilder = new StringBuilder();
                for(var i = 0; i < reader.FieldCount; i++) {
                    stringBuilder.Append(reader.GetName(i).Replace("\"", "\"\""));
                    stringBuilder.Append(COMMA);
                }
                streamWriter.WriteLine(stringBuilder);

                while(reader.Read()) {
                    stringBuilder.Clear();
                    for(var i = 0; i < reader.FieldCount; i++) {
                        stringBuilder.Append(DOUBLE_QUOTES);
                        stringBuilder.Append(reader[i].ToString().Replace("\"", "\"\""));
                        stringBuilder.Append(DOUBLE_QUOTES);
                        stringBuilder.Append(COMMA);
                    }
                    streamWriter.WriteLine(stringBuilder);
                }
            }
        }

        private void Delete() {
            var expirationDate = DateTime.Now.AddDays(storageDate);
            var expirationFiles = Directory.EnumerateFiles(storageDirectory, "*" + ExtensionName).Where(v => File.GetCreationTime(v) < expirationDate);
            foreach(var file in expirationFiles)
                File.Delete(file);
        }

        public string ExcelExport(DbDataReader reader, string fileName) {
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
            fileName = fileNameWithoutExtension + ExtensionName;
            var path = Path.Combine(storageDirectory, fileName);
            if(File.Exists(path)) {
                fileName = string.Format("{0}_{1}{2}", fileNameWithoutExtension, DateTime.Now.ToString("yyyyMMdd_HHmmssff"), ExtensionName);
                path = Path.Combine(storageDirectory, fileName);
            }
            using(var fileStream = new FileStream(path, FileMode.CreateNew)) {
                Export(reader, fileStream);
            }

            new Thread(Delete).Start();

            return Path.Combine(storageDirectoryName, fileName);
        }
    }
}
