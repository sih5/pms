﻿using System.ServiceModel;
using System.ServiceModel.Activation;
using Sunlight.Silverlight.License;

namespace Sunlight.Silverlight.Shell.Web {
    [ServiceContract(Namespace = "http://sunlight.bz")]
    public interface ILicenseService {
        [OperationContract]
        bool IsActivated();

        [OperationContract]
        string Activate(string sn);
    }

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class LicenseService : ILicenseService {
        public static bool IsEnabled = true;

        public bool IsActivated() {
            if(!IsEnabled)
                return true;

            using(var manager = new LicenseManager())
                return manager.IsActivated;
        }

        public string Activate(string sn) {
            using(var manager = new LicenseManager()) {
                string errorMessage;
                return manager.Activate(sn, out errorMessage) ? null : errorMessage;
            }
        }
    }
}
