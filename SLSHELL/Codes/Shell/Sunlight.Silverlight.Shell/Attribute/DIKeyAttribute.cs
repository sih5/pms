﻿using System;

namespace Sunlight.Silverlight {
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class DIKeyAttribute : Attribute {
        public string Key {
            get;
            private set;
        }

        public DIKeyAttribute(string key) {
            this.Key = key;
        }
    }
}
