﻿using System;

namespace Sunlight.Silverlight {
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class PageMetaAttribute : Attribute {
        public string System {
            get;
            private set;
        }

        public string Group {
            get;
            private set;
        }

        public string Page {
            get;
            private set;
        }

        public string[] ActionPanelKeys {
            get;
            set;
        }

        public PageMetaAttribute(string system, string @group, string page) {
            this.System = system;
            this.Group = @group;
            this.Page = page;
        }
    }
}
