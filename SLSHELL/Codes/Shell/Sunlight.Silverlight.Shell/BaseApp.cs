﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using Microsoft.Practices.Unity;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Model;
using Sunlight.Silverlight.Shell;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight {
    public abstract class BaseApp : Application {
        private static string[] authenticationData;
        private object lastExceptionView;

        private static string[] AuthenticationData {
            get {
                if(string.IsNullOrWhiteSpace(WebContext.Current.User.Identity.Name))
                    return null;

                if(authenticationData == null) {
                    var result = WebContext.Current.User.Identity.Name.Split(new[] {
                        ','
                    }, StringSplitOptions.RemoveEmptyEntries);
                    authenticationData = (result.Length == 8 || result.Length == 7 || result.Length == 6) ? result : null;
                }
                return authenticationData;
            }
        }

        public new static BaseApp Current {
            get {
                return (BaseApp)Application.Current;
            }
        }

        private UnityContainer container;
        private IEnumerable<Assembly> ownAssemblies;

        internal IUnityContainer Container {
            get {
                if(this.container == null) {
                    this.container = new UnityContainer();
                    this.container.AddExtension(new ShellContainerExtension());
                }
                return this.container;
            }
        }

        internal IEnumerable<Assembly> OwnAssemblies {
            get {
                return this.ownAssemblies ?? (this.ownAssemblies = AppDomain.CurrentDomain.GetAssemblies().Where(assembly => {
                    if(assembly == Assembly.GetExecutingAssembly())
                        return false;
                    var assemblyName = new AssemblyName(assembly.FullName);
                    if(assemblyName.Name == "Sunlight.Silverlight.Core" || assemblyName.Name.StartsWith("Sunlight.Silverlight.Controls"))
                        return false;
                    return assemblyName.Name.StartsWith("Sunlight.Silverlight.") && !assemblyName.Name.EndsWith(".resources");
                }));
            }
        }

        public abstract string ApplicationName {
            get;
        }

        public abstract SystemTypes SystemType {
            get;
        }

        public UIElement MainView {
            get;
            protected set;
        }

        public UserData CurrentUserData {
            get {
                var userId = AuthenticationData != null ? int.Parse(AuthenticationData[0]) : 0;
                var userCode = AuthenticationData != null ? AuthenticationData[1] : null;
                var userName = AuthenticationData != null ? AuthenticationData[2] : null;
                var enterpriseId = AuthenticationData != null ? int.Parse(AuthenticationData[3]) : 0;
                var enterpriseCode = AuthenticationData != null ? AuthenticationData[4] : null;
                var enterpriseName = AuthenticationData != null ? AuthenticationData[5] : null;
                var enterpriseCategoryId = AuthenticationData != null ? int.Parse(AuthenticationData[6]) : 0;
                return new UserData(userId, userCode, userName, enterpriseId, enterpriseCode, enterpriseName, enterpriseCategoryId);
            }
        }

        public IDictionary<string, string> InitParams {
            get;
            private set;
        }
        private void ModifyRadDataPagerStyle(string shortThemeName) {
            var resourcePath = string.Format("/Telerik.Windows.Themes.{0};component/Themes/Telerik.Windows.Controls.Data.xaml", shortThemeName);
            var resourceInfo = GetResourceStream(new Uri(resourcePath, UriKind.Relative));
            if(resourceInfo == null)
                return;

            string xaml;
            using(var reader = new StreamReader(resourceInfo.Stream))
                xaml = reader.ReadToEnd();
            const string TEXT_TO_SEARCH = "<Border HorizontalAlignment=\"Left\" Margin=\"5,0,0,0\" Width=\"1\" Background=\"{StaticResource RadPager_Separator1}\" VerticalAlignment=\"Center\" Height=\"18\"/>";
            var pos = xaml.IndexOf(TEXT_TO_SEARCH, StringComparison.Ordinal);
            if(pos <= -1)
                return;

            xaml = xaml.Insert(pos, "<Border xmlns:shellControls=\"clr-namespace:Sunlight.Silverlight.Controls;assembly=Sunlight.Silverlight.Shell\" VerticalAlignment=\"Center\" Margin=\"5,0,5,2\"><shellControls:RadDataPagerTotalRecordsText Target=\"{Binding Mode=OneTime, RelativeSource={RelativeSource TemplatedParent}}\" /></Border>" + Environment.NewLine);
            this.Resources.MergedDictionaries.Add((ResourceDictionary)XamlReader.Load(xaml));
        }

        private UserControl GetNotificationView(string text) {
            var grid = new Grid {
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center
            };
            grid.Children.Add(new TextBlock {
                FontSize = 38,
                FontFamily = new FontFamily("Microsoft YaHei"),
                TextWrapping = TextWrapping.Wrap,
                Text = text
            });
            return new UserControl {
                Content = grid
            };
        }

        private void BaseApp_Startup(object sender, StartupEventArgs e) {
            this.InitParams = e.InitParams;

            this.LoadResources();
            ShellUtils.ChangeHtmlTitle(this.ApplicationName);

            if(this.IsRunningOutOfBrowser) {
                this.CheckAndDownloadUpdateCompleted += (o, args) => {
                    if(args.UpdateAvailable)
                        this.RootVisual = this.GetNotificationView("系统已检测到更新并自动安装完毕，请关闭并重新运行本程序。");
                    else if(args.Error is PlatformNotSupportedException)
                        this.RootVisual = this.GetNotificationView("系统已检测到更新，但是您的运行环境版本不支持新程序，请联系技术支持人员。");
                    else if(args.Error != null)
                        this.RootVisual = new Border {
                            Child = new ExceptionView {
                                HorizontalAlignment = HorizontalAlignment.Center,
                                VerticalAlignment = VerticalAlignment.Center,
                                DataContext = args.Error,
                            }
                        };
                    else if(this.MainView != null)
                        this.RootVisual = this.MainView;
                };
                this.CheckAndDownloadUpdateAsync();
            } else {
                var client = new LicenseServiceClient();
                client.IsActivatedCompleted += (o, args) => {
                    if(args.Error != null)
                        this.RootVisual = new Border {
                            Child = new ExceptionView {
                                HorizontalAlignment = HorizontalAlignment.Center,
                                VerticalAlignment = VerticalAlignment.Center,
                                DataContext = args.Error,
                            }
                        };
                    else if(args.Result)
                        this.CheckIfAuthorized(() => {
                            if(this.MainView != null)
                                this.RootVisual = this.MainView;
                        });
                    else
                        this.RootVisual = new ActivateView();
                };
                client.IsActivatedAsync();
            }
        }

        private void BaseApp_Exit(object sender, EventArgs e) {
            if(this.container != null)
                this.container.Dispose();
        }

        private void BaseApp_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e) {
            if(e.ExceptionObject != null && e.ExceptionObject.ToString().IndexOf("System.ServiceModel.DomainServices.Client.EntityCollection`1.ListCollectionViewProxy`1.Remove(Object value)", StringComparison.Ordinal) > -1) {
                e.Handled = true;
                return;
            }

            if(Debugger.IsAttached)
                return;

            e.Handled = true;

            // 若异常为Session失效但界面上正在显示/已显示登录窗口，则忽略此异常。
            var isExpiredException = ShellUtils.IsSessionExpiredException(e.ExceptionObject);
            var allWindows = RadWindowManager.Current.GetWindows();
            if(isExpiredException && (this.lastExceptionView is DomainServiceOperationView && allWindows.Count == 0
                                        || allWindows.Any(windowBase => windowBase.Content is DomainServiceOperationView)))
                return;

            object view;
            if(isExpiredException)
                view = new DomainServiceOperationView {
                    Margin = new Thickness(10),
                    DataContext = e.ExceptionObject,
                };
            else
                view = new ExceptionView {
                    Margin = new Thickness(10),
                    DataContext = e.ExceptionObject,
                };
            lastExceptionView = view;
            var window = new RadWindow {
                Header = this.ApplicationName,
                WindowStartupLocation = WindowStartupLocation.CenterScreen,
                IsTopmost = true,
                ResizeMode = ResizeMode.NoResize,
                CanClose = true,
                CanMove = true,
                Content = view,
            };
            if(view is DomainServiceOperationView)
                window.ShowDialog();
            else
                window.Show();
        }

        protected BaseApp() {
            this.Startup += this.BaseApp_Startup;
            this.Exit += this.BaseApp_Exit;
            this.UnhandledException += this.BaseApp_UnhandledException;

            var securityManager = this.Container.Resolve<ISecurityManager>();
            this.ApplicationLifetimeObjects.Add(new WebContext {
                Authentication = securityManager.AuthenticationService
            });

            LocalizationManager.Manager = new RadControlLocalizationManager();
        }

        protected virtual void LoadResources() {
            if(StyleManager.ApplicationTheme == null) {
                throw new Exception("请先指定当前项目的主题");
            }
            var themeName = StyleManager.ApplicationTheme.GetType().Name;
            var shortThemeName = themeName.Substring(0, themeName.IndexOf("Theme", StringComparison.OrdinalIgnoreCase));
            // 载入Telerik控件的样式，以改变默认的Silverlight自带控件的样子

            #region 2013Q2控件样式载入，已经无效
            //const string THEME_PATH = "/Telerik.Windows.Controls;component/Themes/Office/Black/";
            //foreach(var fileName in new[] {
            //    "System.Windows.Button.xaml", 
            //    "System.Windows.CheckBox.xaml", 
            //    "System.Windows.RadioButton.xaml", 
            //    "System.Windows.ListBox.xaml", 
            //    "System.Windows.RepeatButton.xaml", 
            //    "System.Windows.TextBox.xaml", 
            //    "System.Windows.PasswordBox.xaml", 
            //    "System.Windows.Scrollviewer.xaml"
            //})
            //    this.Resources.MergedDictionaries.Add(new ResourceDictionary {
            //        Source = new Uri(string.Concat(THEME_PATH, fileName), UriKind.Relative)
            //    });
            #endregion

            //this.Resources.MergedDictionaries.Add(new ResourceDictionary {
            //    Source = new Uri(string.Format("/Telerik.Windows.Themes.{0};component/Themes/System.Windows.xaml", shortThemeName), UriKind.Relative)
            //});
            // 载入自定义的样式
            this.Resources.MergedDictionaries.Add(new ResourceDictionary {
                Source = new Uri("/Sunlight.Silverlight.Shell;component/BaseApp.Resources.xaml", UriKind.Relative),
            });
            this.ModifyRadDataPagerStyle(shortThemeName);
        }

        public bool IsPageUriRegistered(string pageUri) {
            return this.Container.IsRegistered<IRootView>(pageUri);
        }

        public void CheckIfAuthorized(Action callback) {
            WebContext.Current.Authentication.LoadUser(luOp => {
                if(luOp.HasError) {
                    UIHelper.ShowAlertMessage(luOp.Error.Message);
                    luOp.MarkErrorAsHandled();
                    return;
                }
                LoginViewModel.SetCurrentUICulture();
                if(callback != null)
                    callback();
            }, null);
        }
    }
}
