﻿using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Resources;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Commands {
    public sealed class DataGridViewResetEntityCommand : CommandBase {
        internal DataGridViewResetEntityCommand() {
            //只允许使用单例
        }

        protected override bool CanExecute(object parameter) {
            var dataGridView = parameter as DataGridView;
            return dataGridView != null && dataGridView.MainGridView.SelectedItems.Cast<Entity>().Any(e => e.HasChanges);
        }

        protected override void Execute(object parameter) {
            var dataGridView = parameter as DataGridView;
            if(dataGridView == null)
                return;

            var parameters = new DialogParameters {
                Header = ShellUIStrings.DataGridView_Confirm,
                Content = ShellUIStrings.DataGridView_ConfirmReset,
            };
            parameters.Closed += (sender, e) => {
                if(e.DialogResult != true)
                    return;
                foreach(var entity in dataGridView.MainGridView.SelectedItems.Cast<IRevertibleChangeTracking>().Where(entity => entity.IsChanged))
                    entity.RejectChanges();
            };
            RadWindow.Confirm(parameters);
        }
    }

    public sealed class DataGridViewAddRowCommand : CommandBase {
        internal DataGridViewAddRowCommand() {
            //只允许使用单例
        }

        protected override bool CanExecute(object parameter) {
            var dataGridView = parameter as DataGridView;
            return dataGridView != null && !dataGridView.MainGridView.IsReadOnly && dataGridView.MainGridView.CanUserInsertRows;
        }

        protected override void Execute(object parameter) {
            var dataGridView = parameter as DataGridView;
            if(dataGridView != null)
                dataGridView.MainGridView.BeginInsert();
        }
    }

    public sealed class DataGridViewRemoveRowCommand : CommandBase {
        internal DataGridViewRemoveRowCommand() {
            //只允许使用单例
        }

        protected override bool CanExecute(object parameter) {
            var dataGridView = parameter as DataGridView;
            var result = dataGridView != null && !dataGridView.MainGridView.IsReadOnly && (dataGridView.MainGridView.CanUserInsertRows || dataGridView.MainGridView.CanUserDeleteRows);
            if(result) {
                var selectedItems = dataGridView.MainGridView.SelectedItems;
                if(selectedItems == null || (!dataGridView.CanRemoveEntity && selectedItems.Cast<Entity>().Any(entity => entity.EntityState != EntityState.New)))
                    result = false;
            }
            return result;
        }

        protected override void Execute(object parameter) {
            var dataGridView = parameter as DataGridView;
            if(dataGridView == null)
                return;

            var itemsToRemove = dataGridView.MainGridView.SelectedItems.Cast<Entity>().ToList();
            if(itemsToRemove.Any(item => !dataGridView.ViewModel.CanRemoveEntity(item)))
                return;
            foreach(var item in itemsToRemove) {
                dataGridView.ViewModel.RemovingEntity(item);
                ((IEditableCollectionView)dataGridView.MainGridView.ItemsSource).Remove(item);
            }
        }
    }

    public sealed class DataGridViewSubmitChangesCommand : CommandBase {
        internal DataGridViewSubmitChangesCommand() {
            //只允许使用单例
        }

        protected override bool CanExecute(object parameter) {
            var dataGridView = parameter as DataGridView;
            return dataGridView != null && dataGridView.ViewModel.DomainContext.HasChanges;
        }

        protected override void Execute(object parameter) {
            var dataGridView = parameter as DataGridView;
            if(dataGridView == null)
                return;

            var window = new SubmitPreviewWindow {
                DomainContext = dataGridView.ViewModel.DomainContext,
                GetValueByKey = (entityType, columnName, key) => {
                    if(dataGridView.ViewModel.EntityType != entityType || dataGridView.ViewModel.Columns == null)
                        return null;
                    var column = dataGridView.ViewModel.Columns.OfType<KeyValuesColumnItem>().SingleOrDefault(c => c.Name == columnName);
                    if(column == null || column.KeyValueItems == null)
                        return null;
                    var pair = column.KeyValueItems.SingleOrDefault(kvp => kvp.Key == key);
                    return pair == null ? null : pair.Value;
                },
            };
            window.ShowDialog();
        }
    }

    public sealed class DataGridViewRejectChangesCommand : CommandBase {
        internal DataGridViewRejectChangesCommand() {
            //只允许使用单例
        }

        protected override bool CanExecute(object parameter) {
            var dataGridView = parameter as DataGridView;
            return dataGridView != null && dataGridView.ViewModel.DomainContext.HasChanges;
        }

        protected override void Execute(object parameter) {
            var dataGridView = parameter as DataGridView;
            if(dataGridView == null)
                return;

            var parameters = new DialogParameters {
                Header = ShellUIStrings.DataGridView_Confirm,
                Content = ShellUIStrings.DataGridView_ConfirmReject,
            };
            parameters.Closed += (_, args) => {
                if(args.DialogResult != true)
                    return;
                var changes = dataGridView.ViewModel.DomainContext.EntityContainer.GetChanges();
                var itemsToRemove = changes.AddedEntities.ToArray();
                var itemsToRestore = changes.RemovedEntities.ToArray();
                dataGridView.ViewModel.DomainContext.RejectChanges();
                foreach(var entity in itemsToRemove)
                    ((IEditableCollectionView)dataGridView.MainGridView.ItemsSource).Remove(entity);
                var collection = ((ICollectionView)dataGridView.MainGridView.ItemsSource).SourceCollection as IList;
                if(collection != null)
                    foreach(var entity in itemsToRestore)
                        collection.Add(entity);
            };
            RadWindow.Confirm(parameters);
        }
    }

    public sealed class DataGridViewExportCommand : CommandBase {
        internal DataGridViewExportCommand() {
            //只允许使用单例
        }

        protected override bool CanExecute(object parameter) {
            var dataGridView = parameter as DataGridView;
            return dataGridView != null;
        }

        protected override void Execute(object parameter) {
            var dataGridView = parameter as DataGridView;
            if(dataGridView == null)
                return;

            var dialog = new SaveFileDialog {
                DefaultExt = "xlsx",
                Filter = "Excel 文档 (*.xlsx;*.xls)|*.xlsx;*.xls",
            };
            if(dialog.ShowDialog() == true)
                using(var stream = dialog.OpenFile())
                    dataGridView.MainGridView.Export(stream, new GridViewExportOptions {
                        Format = ExportFormat.ExcelML,
                        ShowColumnFooters = true,
                        ShowColumnHeaders = true,
                        ShowGroupFooters = true,
                    });
        }
    }

    public static class DataGridViewCommands {
        public static readonly DataGridViewResetEntityCommand ResetEntity = new DataGridViewResetEntityCommand();
        public static readonly DataGridViewAddRowCommand AddRow = new DataGridViewAddRowCommand();
        public static readonly DataGridViewRemoveRowCommand RemoveRow = new DataGridViewRemoveRowCommand();
        public static readonly DataGridViewSubmitChangesCommand SubmitChanges = new DataGridViewSubmitChangesCommand();
        public static readonly DataGridViewRejectChangesCommand RejectChanges = new DataGridViewRejectChangesCommand();
        public static readonly DataGridViewExportCommand Export = new DataGridViewExportCommand();

        internal static void RaiseCanExecuteChanged() {
            ResetEntity.RaiseCanExecuteChanged();
            AddRow.RaiseCanExecuteChanged();
            RemoveRow.RaiseCanExecuteChanged();
            SubmitChanges.RaiseCanExecuteChanged();
            RejectChanges.RaiseCanExecuteChanged();
            Export.RaiseCanExecuteChanged();
        }
    }
}
