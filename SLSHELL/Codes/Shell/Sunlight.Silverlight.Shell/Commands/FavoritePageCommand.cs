﻿using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Model;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Commands {
    public class FavoritePageCommand : CommandBase {
        protected override bool CanExecute(object parameter) {
            return true;
        }

        protected override void Execute(object parameter) {
            if(!this.CanExecute(parameter))
                return;
            var pageMenuItem = parameter as PageMenuItem;
            if(pageMenuItem == null)
                return;

            if(pageMenuItem.IsFavorite)
                ShellViewModel.Current.FavoritePageManager.Add(pageMenuItem);
            else
                ShellViewModel.Current.FavoritePageManager.Remove(pageMenuItem);
        }
    }
}
