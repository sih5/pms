﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Xml.Linq;
using Microsoft.Practices.Unity;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Model;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Commands {
    public sealed class LoadMenuCommand : CommandBase {
        private static void SetBaseAttributes(MenuItem obj, XElement element) {
            var attribute = element.Attribute("Id");
            if(attribute != null)
                obj.Id = int.Parse(attribute.Value);
            attribute = element.Attribute("PageId");
            if(attribute != null)
                obj.Name = attribute.Value;
            attribute = element.Attribute("Name");
            if(attribute != null)
                obj.DisplayName = attribute.Value;
            attribute = element.Attribute("Description");
            if(attribute != null)
                obj.Description = attribute.Value;
            attribute = element.Attribute("Icon");
            if(attribute != null)
                obj.ImageUri = Utils.MakeServerUri(attribute.Value);
        }

        private static PageMenuItem CreatePageMenuItem(XElement element) {
            var result = new PageMenuItem();
            SetBaseAttributes(result, element);
            var attribute = element.Attribute("Type");
            if(attribute != null)
                result.PageType = attribute.Value;
            attribute = element.Attribute("Parameter");
            if(attribute != null)
                result.PageParameter = attribute.Value;
            return result;
        }

        private static GroupMenuItem CreateGroupMenuItem(XElement element) {
            var result = new GroupMenuItem();
            SetBaseAttributes(result, element);
            var pages = new List<PageMenuItem>();
            foreach(var pageMenuItem in element.Elements("Page").Select(CreatePageMenuItem)) {
                pageMenuItem.GroupMenuItem = result;
                pages.Add(pageMenuItem);
            }
            result.PageMenuItems = pages;
            return result;
        }

        private static SystemMenuItem CreateSystemMenuItem(XElement element) {
            var result = new SystemMenuItem();
            SetBaseAttributes(result, element);
            var groups = new List<GroupMenuItem>();
            foreach(var groupMenuItem in element.Elements("Group").Select(CreateGroupMenuItem)) {
                groupMenuItem.SystemMenuItem = result;
                groups.Add(groupMenuItem);
            }
            result.GroupMenuItems = groups;
            return result;
        }

        private static IEnumerable<SystemMenuItem> ParseMenuXml(string xml) {
            if(xml == null)
                throw new ArgumentNullException("xml");

            var eRoot = XDocument.Parse(xml).Root;
            if(eRoot == null)
                return Enumerable.Empty<SystemMenuItem>();

            var eSystems = eRoot.Elements("System");
            var result = new List<SystemMenuItem>();
            result.AddRange(eSystems.Select(CreateSystemMenuItem));
            return result;
        }

        private IList<PageMenuItem> GetFavoritePages(string xml, IEnumerable<SystemMenuItem> authorizedSystems) {
            if(xml == null)
                throw new ArgumentNullException("xml");

            var result = new List<PageMenuItem>();
            var eRoot = XDocument.Parse(xml).Root;
            if(eRoot == null)
                return result;

            var eFavoritePages = eRoot.Element("FavoritePages");
            if(eFavoritePages == null)
                return new List<PageMenuItem>();

            var pagesOrder = new Dictionary<int, int>();
            foreach(var pageOrder in eFavoritePages.Elements("Page")) {
                var id = pageOrder.Attribute("Id");
                var sequence = pageOrder.Attribute("Sequence");
                if(id != null && sequence != null)
                    pagesOrder.Add(int.Parse(id.Value), int.Parse(sequence.Value));
            }
            return authorizedSystems.SelectMany(system => system.GroupMenuItems).SelectMany(group => group.PageMenuItems)
                                    .Where(page => pagesOrder.ContainsKey(page.Id)).OrderBy(page => pagesOrder[page.Id]).ToList();
        }

        private readonly ShellViewModel shellViewModel;
        private bool isMenuLoaded;

        public LoadMenuCommand(ShellViewModel shellViewModel) {
            this.shellViewModel = shellViewModel;
        }

        protected override bool CanExecute(object parameter) {
            return !this.isMenuLoaded;
        }

        protected override void Execute(object parameter) {
            this.isMenuLoaded = true;
            this.RaiseCanExecuteChanged();
            ShellViewModel.Current.IsBusy = true;
            var securityManager = BaseApp.Current.Container.Resolve<ISecurityManager>();
            securityManager.GetMenuXml(Thread.CurrentThread.CurrentUICulture.Name, result => {
                var securityActionResult = (ISecurityActionResult)result;
                try {
                    if(securityActionResult.IsSuccess) {
                        this.shellViewModel.SystemMenuItems = ParseMenuXml((string)securityActionResult["Value"]);

                        var favoritePages = this.GetFavoritePages((string)securityActionResult["Value"], this.shellViewModel.SystemMenuItems);
                        foreach(var page in favoritePages)
                            page.IsFavorite = true;
                        this.shellViewModel.FavoritePageManager.Initialize(favoritePages);
                    } else {
                        this.isMenuLoaded = false;
                        this.RaiseCanExecuteChanged();
                    }
                } finally {
                    ShellViewModel.Current.IsBusy = false;
                }
            });
        }
    }
}
