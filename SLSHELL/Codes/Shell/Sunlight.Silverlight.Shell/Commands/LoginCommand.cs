﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.Automation;
using System.ServiceModel.DomainServices.Client;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Practices.Unity;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Resources;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Commands {
    public sealed class LoginCommand : CommandBase {
        private readonly LoginViewModel viewModel;
        private MyClient myClient = null;
        internal LoginCommand(LoginViewModel viewModel) {
            if(viewModel == null)
                throw new ArgumentNullException("viewModel");
            this.viewModel = viewModel;
            this.viewModel.ErrorsChanged += (sender, e) => this.RaiseCanExecuteChanged();

            myClient = new MyClient();
        }

        protected override bool CanExecute(object parameter) {
            return !this.viewModel.HasErrors && !this.viewModel.IsBusy;
        }

        protected override void Execute(object parameter) {
            this.viewModel.Validate();
            if(!this.CanExecute(parameter))
                return;

            #region 获取exe安装包传过来的mac
            var url = BaseApp.Current.InitParams["Url"];
            var mac = string.Empty;
            if (url.Contains("?")){
                var staymac = url.Substring(url.IndexOf("?") + 1);
                byte[] buff = Convert.FromBase64String(staymac);
                mac = Encoding.Unicode.GetString(buff, 0, buff.Length).Replace("mac=","");
            }
            var macAddress = string.IsNullOrEmpty(mac) ? string.Join("", myClient.macAddress) : mac;
            #endregion
            this.viewModel.ErrorMessage = string.Empty;
            this.viewModel.IsBusy = true;
            this.viewModel.LoginSuccess = false;
            var securityManager = BaseApp.Current.Container.Resolve<ISecurityManager>();
            var parameters = new Dictionary<string, string> {
                { "EnterpriseCode", this.viewModel.EnterpriseCode },
                 { "IP",  BaseApp.Current.InitParams["IP"] },
                 { "Mac", macAddress },
                 { "MacAddPassword",  BaseApp.Current.InitParams["MacAddPassword"] }
            };
            //if(null!= this.viewModel.Password) {
            //    throw new DomainException(string.Format("密码错误：{0}", this.viewModel.Password));
            //}
            securityManager.Login(this.viewModel.UserName, this.viewModel.Password, false, parameters, result => {
                var securityActionResult = (ISecurityActionResult)result;
                try {
                    if(securityActionResult.IsSuccess)
                        if((bool)securityActionResult["LoginSuccess"]) {
                            Thread.CurrentThread.CurrentUICulture = LoginViewModel.GetUICultureByLanguage(this.viewModel.Language);
                            //LoginViewModel.SetCurrentUICulture();
                            this.viewModel.LoginSuccess = true;

                            ShellUtils.ChangeHtmlTitle();
                        } else
                            this.viewModel.ErrorMessage = ShellUIStrings.LoginViewModel_Error_LoginFailed;
                    else
                        this.viewModel.ErrorMessage = securityActionResult.Exception.Message;
                } finally {
                    securityActionResult.IsErrorHandled = true;
                    this.viewModel.IsBusy = false;
                }
            });
        }
    }

    public class MACAddressManager {
        private dynamic sWbemServices;
        private dynamic sWbemSink;

        public List<string> MACAddress = new List<string>();
        public event EventHandler OnGetMACAddressCompleted;

        private void EndGetMACAddress(object sender, EventArgs e) {
            dynamic objWbemObject = sender;
            MACAddress.Add(objWbemObject.MACAddress);
            if(OnGetMACAddressCompleted != null)
                OnGetMACAddressCompleted(this, EventArgs.Empty);
        }

        public void BeginGetMACAddress() {
            if((Application.Current.HasElevatedPermissions) && (AutomationFactory.IsAvailable)) {
                dynamic sWbemLocator = AutomationFactory.CreateObject("WbemScripting.SWBemLocator");
                sWbemServices = sWbemLocator.ConnectServer(".");
                sWbemServices.Security_.ImpersonationLevel = 3; //impersonate

                sWbemSink = AutomationFactory.CreateObject("WbemScripting.SWbemSink");
                sWbemSink.OnObjectReady += new EventHandler(EndGetMACAddress);

                string query = "SELECT * FROM Win32_NetworkAdapterConfiguration where IPEnabled=true";
                sWbemServices.ExecQueryAsync(sWbemSink, query);
            }
        }
    }

    public partial class MyClient : UserControl {
        public List<string> macAddress = new List<string>();
        public MyClient() {
            MACAddressManager macAddressManager = new MACAddressManager();
            macAddressManager.OnGetMACAddressCompleted += new EventHandler(macAddressManager_OnGetMACAddressCompleted);
            macAddressManager.BeginGetMACAddress();
        }

        void macAddressManager_OnGetMACAddressCompleted(object sender, EventArgs e) {
            MACAddressManager manager = (MACAddressManager)sender;
            if(macAddress.Count == 0)
                macAddress = manager.MACAddress;
        }
    }
}
