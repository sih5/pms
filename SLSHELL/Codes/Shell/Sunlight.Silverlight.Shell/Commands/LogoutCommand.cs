﻿using System;
using System.Windows.Browser;
using System.Windows.Input;
using Microsoft.Practices.Unity;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Resources;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Commands {
    public sealed class LogoutCommand : CommandBase {
        protected override bool CanExecute(object parameter) {
            return true;
        }

        protected override void Execute(object parameter) {
            var parameters = new DialogParameters {
                Header = ShellUIStrings.ShellViewModel_LogoutText,
                Content = ShellUIStrings.ShellViewModel_LogoutConfirmText,
            };
            parameters.Closed += (_, args) => {
                if(args.DialogResult == null || !args.DialogResult.Value)
                    return;
                ShellViewModel.Current.IsBusy = true;
                var securityManager = BaseApp.Current.Container.Resolve<ISecurityManager>();
                securityManager.Logout(result => {
                    var securityActionResult = (ISecurityActionResult)result;
                    try {
                        if(securityActionResult.IsSuccess) {
                            ((ICommand)(new RestartCommand())).Execute(null);
                        }
                    } finally {
                        ShellViewModel.Current.IsBusy = false;
                    }
                });
            };
            RadWindow.Confirm(parameters);
        }
    }
}
