﻿using System;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Model;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Commands {
    public class OpenPageCommand : CommandBase {
        public bool UpdateRecentList {
            get;
            set;
        }

        protected override bool CanExecute(object parameter) {
            if(parameter == null || ShellViewModel.Current == null)
                return false;
            return parameter is PageMenuItem;
        }

        protected override void Execute(object parameter) {
            var pageMenuItem = parameter as PageMenuItem;
            if(pageMenuItem == null)
                return;

            ShellViewModel.Current.IsMenuOpen = false;
            ShellViewModel.Current.PageUri = pageMenuItem.GroupMenuItem == null ? pageMenuItem.PageUri
                : new Uri(ShellUtils.GetFrameUriFromMenuItemName(pageMenuItem.GroupMenuItem.SystemMenuItem.Name, pageMenuItem.GroupMenuItem.Name, pageMenuItem.Name), UriKind.Relative);

            if(ShellViewModel.Current.RecentPageManager == null)
                return;
            if(this.UpdateRecentList)
                ShellViewModel.Current.RecentPageManager.Add(pageMenuItem);
            else
                ShellViewModel.Current.RecentPageManager.MarkCurrentPage(pageMenuItem);
        }

        public OpenPageCommand() {
            this.UpdateRecentList = true;
        }
    }
}
