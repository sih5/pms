﻿using System;
using System.Runtime.InteropServices.Automation;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Command;

namespace Sunlight.Silverlight.Commands {
    public sealed class RestartCommand : CommandBase {
        protected override bool CanExecute(object parameter) {
            return true;
        }

        protected override void Execute(object parameter) {
            if(Application.Current.IsRunningOutOfBrowser && Application.Current.HasElevatedPermissions) {
                string command = null;
                foreach(var specialFolderName in new[] {
                    "Desktop", "StartMenu", "Programs", "Recent", "AllUsersDesktop", "AllUsersStartMenu", "AllUsersPrograms"
                }) {
                    string specialFolderPath;
                    using(var wShell = AutomationFactory.CreateObject("WScript.Shell"))
                        specialFolderPath = wShell.SpecialFolders(specialFolderName);
                    using(var shellApp = AutomationFactory.CreateObject("Shell.Application")) {
                        foreach(var item in shellApp.NameSpace(specialFolderPath).Items()) {
                            if(!item.IsLink || !item.Name.Contains(BaseApp.Current.ApplicationName))
                                continue;
                            var link = item.GetLink();
                            command = string.Format("\"{0}\" {1}", link.Path, link.Arguments);
                        }
                    }
                    if(!string.IsNullOrEmpty(command))
                        break;
                }
                // ReSharper disable EmptyGeneralCatchClause
                if(!string.IsNullOrEmpty(command))
                    try {
                        using(var wShell = AutomationFactory.CreateObject("WScript.Shell"))
                            wShell.Run(command);
                    } catch {
                        // Eat exceptions
                    }
                // ReSharper restore EmptyGeneralCatchClause
                Application.Current.MainWindow.Close();
            } else if(HtmlPage.IsEnabled) {
                var uri = HtmlPage.Document.DocumentUri.OriginalString;
                var index = uri.IndexOf('#');
                HtmlPage.Window.Navigate(index > -1 ? new Uri(uri.Remove(index)) : HtmlPage.Document.DocumentUri);
            }
        }
    }
}
