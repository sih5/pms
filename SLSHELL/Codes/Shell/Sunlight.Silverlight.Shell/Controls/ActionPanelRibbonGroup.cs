﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Practices.Unity;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Resources;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.RibbonView;

namespace Sunlight.Silverlight.Controls {
    public class ActionPanelRibbonGroup : RadRibbonGroup {
        public class ExecutingActionEventArgs : EventArgs {
            public ActionItemGroup ActionItemGroup {
                get;
                internal set;
            }

            public string UniqueId {
                get;
                internal set;
            }
        }

        public static readonly DependencyProperty ActionItemGroupProperty = DependencyProperty.Register("ActionItemGroup", typeof(ActionItemGroup), typeof(ActionPanelRibbonGroup), new PropertyMetadata((obj, args) => ((ActionPanelRibbonGroup)obj).ReloadUI()));
        public static readonly DependencyProperty PageIdProperty = DependencyProperty.Register("PageId", typeof(int?), typeof(ActionPanelRibbonGroup), null);
        public event EventHandler<ExecutingActionEventArgs> ExecutingAction;

        protected bool SkipPermissionCheck {
            get;
            set;
        }

        public ActionItemGroup ActionItemGroup {
            get {
                return (ActionItemGroup)this.GetValue(ActionItemGroupProperty);
            }
            set {
                this.SetValue(ActionItemGroupProperty, value);
            }
        }

        public int? PageId {
            get {
                return (int?)this.GetValue(PageIdProperty);
            }
            set {
                this.SetValue(PageIdProperty, value);
            }
        }

        private void RaiseExecutingAction(string uniqueId) {
            var handler = this.ExecutingAction;
            if(handler != null)
                handler(this, new ExecutingActionEventArgs {
                    ActionItemGroup = this.ActionItemGroup,
                    UniqueId = uniqueId,
                });
        }

        public ActionPanelRibbonGroup() {
            //设置按钮组的缩放样式
            this.Variants.Clear();
            this.Variants.Add(new GroupVariant() {
                Variant = RibbonGroupVariant.Large
            });
            this.Variants.Add(new GroupVariant() {
                Variant = RibbonGroupVariant.Medium
            });
            //this.Variants.Add(new GroupVariant() {
            //    Variant = RibbonGroupVariant.Small,
            //});
        }

        private void ReloadUI() {
            this.IsEnabled = true;
            this.Items.Clear();

            if(this.ActionItemGroup == null)
                return;

            this.Header = this.ActionItemGroup.Title;

            if(this.ActionItemGroup.ActionItems == null)
                return;

            var radCollapsiblePanel = new RadCollapsiblePanel();
            this.Items.Add(radCollapsiblePanel);

            Action<IEnumerable<ActionItem>> createButtons = actionItems => {
                foreach(var ribbonButton in actionItems.Select(actionItem => new RadRibbonButton {
                    FontSize = 13,
                    VerticalAlignment = VerticalAlignment.Center,
                    CollapseToMedium = CollapseThreshold.WhenGroupIsMedium,
                    CollapseToSmall = CollapseThreshold.WhenGroupIsSmall,
                    //IsAutoSize = false,
                    Size = ButtonSize.Large,
                    Text = actionItem.Title,
                    LargeImage = new BitmapImage(actionItem.ImageUri),
                    IsEnabled = actionItem.CanExecute,
                    Tag = actionItem.UniqueId,
                })) {
                    ribbonButton.Click += (sender, e) => this.RaiseExecutingAction(((RadRibbonButton)sender).Tag as string);
                    radCollapsiblePanel.Children.Add(ribbonButton);
                }
                this.OnUIReloaded();
            };

            this.OnUIReloading();
            if(!this.SkipPermissionCheck && this.ActionItemGroup.ActionItems.Any(actionItem => !string.IsNullOrEmpty(actionItem.UniqueId))) {
                var securityManager = BaseApp.Current.Container.Resolve<ISecurityManager>();
                securityManager.GetPageActions(this.PageId == null ? ShellViewModel.Current.GetPageId(ShellViewModel.Current.PageUri) : this.PageId.Value, result => {
                    var securityActionResult = (ISecurityActionResult)result;
                    if(!securityActionResult.IsSuccess)
                        securityActionResult.IsErrorHandled = true;
                    var authorizedActions = securityActionResult.IsSuccess ? (string[])securityActionResult["Value"] : new string[0];
                    var authorizedActionItems = this.ActionItemGroup.ActionItems.Where(actionItem => {
                        if(string.IsNullOrEmpty(actionItem.UniqueId))
                            return true;
                        var uniqueId = string.Format("{0}|{1}", this.ActionItemGroup.UniqueId, actionItem.UniqueId);
                        return authorizedActions.FirstOrDefault(authorizedAction => string.Compare(authorizedAction, uniqueId, StringComparison.CurrentCultureIgnoreCase) == 0) != null;
                    }).ToList();
                    if(authorizedActionItems.Any())
                        createButtons(authorizedActionItems);
                    else {
                        this.IsEnabled = false;
                        this.Items.Add(new TextBlock {
                            FontSize = 13,
                            VerticalAlignment = VerticalAlignment.Center,
                            Margin = new Thickness(10),
                            Foreground = new SolidColorBrush(Colors.Gray),
                            Text = ShellUIStrings.ActionPanelRibbonGroup_NoAccess,
                        });
                    }
                });
            } else
                createButtons(this.ActionItemGroup.ActionItems);
        }

        protected virtual void OnUIReloading() {
            //
        }

        protected virtual void OnUIReloaded() {
            //
        }
    }
}
