﻿using System;
using System.Windows.Input;
using System.Windows.Threading;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Controls {
    public class AutoSelectRibbonBackstageItem : RadRibbonBackstageItem {
        private static readonly DispatcherTimer Timer;
        private static RadRibbonBackstageItem CurrentWaitingItem;

        static AutoSelectRibbonBackstageItem() {
            Timer = new DispatcherTimer {
                Interval = TimeSpan.FromMilliseconds(150)
            };
            Timer.Tick += (sender, args) => {
                Timer.Stop();
                CurrentWaitingItem.IsSelected = true;
            };
        }

        private void OnMouseEnter(object sender, MouseEventArgs mouseEventArgs) {
            if(this.IsSelected)
                return;
            Timer.Stop();
            CurrentWaitingItem = this;
            Timer.Start();
        }

        private void OnMouseLeave(object sender, MouseEventArgs mouseEventArgs) {
            if(this.IsSelected)
                return;
            Timer.Stop();
        }

        public AutoSelectRibbonBackstageItem() {
            this.MouseEnter += this.OnMouseEnter;
            this.MouseLeave += this.OnMouseLeave;
        }
    }
}
