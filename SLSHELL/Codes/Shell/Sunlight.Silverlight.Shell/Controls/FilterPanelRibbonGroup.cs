﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Sunlight.Silverlight.Annotations;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Resources;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.RibbonView;

namespace Sunlight.Silverlight.Controls {
    public class FilterPanelRibbonGroup : RadRibbonGroup, INotifyPropertyChanged, IDelayCall {
        public class ExecutingQueryEventArgs : EventArgs {
            public QueryItemGroup QueryItemGroup {
                get;
                internal set;
            }

            public FilterItem Filter {
                get;
                internal set;
            }
        }

        private static readonly Uri ButtonIconUri = new Uri("/Sunlight.Silverlight.Shell;component/Images/query.png", UriKind.Relative);
        public static readonly DependencyProperty QueryItemGroupsProperty = DependencyProperty.Register("QueryItemGroups", typeof(IEnumerable<QueryItemGroup>), typeof(FilterPanelRibbonGroup), new PropertyMetadata((obj, args) => ((FilterPanelRibbonGroup)obj).ReloadUI()));
        public static readonly DependencyProperty AcceptEmptyFilterProperty = DependencyProperty.Register("AcceptEmptyFilter", typeof(bool), typeof(FilterPanelRibbonGroup), new PropertyMetadata(true));

        public static readonly DependencyProperty ButtonIconProperty = DependencyProperty.Register("ButtonIcon", typeof(Uri), typeof(FilterPanelRibbonGroup), new PropertyMetadata(ButtonIconUri, (obj, args) => {
            var button = ((FilterPanelRibbonGroup)obj).queryButton;
            if(button != null)
                button.LargeImage = new BitmapImage((args.NewValue as Uri) ?? ButtonIconUri);
        }));

        public static readonly DependencyProperty ButtonTextProperty = DependencyProperty.Register("ButtonText", typeof(string), typeof(FilterPanelRibbonGroup), new PropertyMetadata(ShellUIStrings.FilterPanelRibbonGroup_ExecuteQuery, (obj, args) => {
            var button = ((FilterPanelRibbonGroup)obj).queryButton;
            if(button != null)
                button.Text = (args.NewValue as string) ?? ShellUIStrings.FilterPanelRibbonGroup_ExecuteQuery;
        }));

        private bool isUILoaded;
        private RadRibbonButton queryButton;
        private Dictionary<string, FilterPanel> panels;
        public event EventHandler<ExecutingQueryEventArgs> ExecutingQuery;
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public bool IsUILoaded {
            get {
                return this.isUILoaded;
            }
            private set {
                if(this.isUILoaded == value)
                    return;
                this.isUILoaded = value;
                this.OnPropertyChanged("IsUILoaded");
            }
        }

        public IEnumerable<QueryItemGroup> QueryItemGroups {
            get {
                return (IEnumerable<QueryItemGroup>)this.GetValue(QueryItemGroupsProperty);
            }
            set {
                this.SetValue(QueryItemGroupsProperty, value);
            }
        }

        public bool AcceptEmptyFilter {
            get {
                return (bool)this.GetValue(AcceptEmptyFilterProperty);
            }
            set {
                this.SetValue(AcceptEmptyFilterProperty, value);
            }
        }

        public Uri ButtonIcon {
            get {
                return (Uri)this.GetValue(ButtonIconProperty);
            }
            set {
                this.SetValue(ButtonIconProperty, value);
            }
        }

        public string ButtonText {
            get {
                return (string)this.GetValue(ButtonTextProperty);
            }
            set {
                this.SetValue(ButtonTextProperty, value);
            }
        }

        private void RaiseExecutingQuery(QueryItemGroup queryItemGroup, FilterItem filter) {
            var handler = this.ExecutingQuery;
            if(handler != null)
                handler(this, new ExecutingQueryEventArgs {
                    QueryItemGroup = queryItemGroup,
                    Filter = filter,
                });
        }

        private void ReloadUI() {
            this.Items.Clear();

            if(this.QueryItemGroups == null || !this.QueryItemGroups.Any())
                return;

            var stackPanel = new StackPanel {
                Orientation = Orientation.Horizontal
            };
            this.Items.Add(stackPanel);

            if(this.panels == null)
                this.panels = new Dictionary<string, FilterPanel>();
            this.panels.Clear();

            var switcher = new FilterPanelSwitcher();
            switcher.FilterPanelSwitched += (sender, e) => {
                var queryItemGroup = this.QueryItemGroups.SingleOrDefault(g => g.UniqueId == ((FilterPanelSwitcher)sender).CurrentKey);
                if(queryItemGroup != null)
                    this.Header = queryItemGroup.Title;
            };
            foreach(var queryItemGroup in this.QueryItemGroups) {
                var filterPanel = new FilterPanel {
                    Tag = queryItemGroup,
                };
                filterPanel.CreateFilterControls(queryItemGroup.EntityType, queryItemGroup.QueryItems);
                filterPanel.FiltersAccepted += (sender, e) => {
                    var panel = (FilterPanel)sender;
                    var filters = panel.GetFilters();
                    if(!this.AcceptEmptyFilter && (filters == null || filters.IsEmpty))
                        UIHelper.ShowNotification(ShellUIStrings.FilterPanelRibbonGroup_EmptyFilters);
                    else
                        this.RaiseExecutingQuery(panel.Tag as QueryItemGroup, filters);
                };
                switcher.Register(queryItemGroup.UniqueId, filterPanel);
                this.panels.Add(queryItemGroup.UniqueId, filterPanel);
            }
            stackPanel.Children.Add(switcher);

            stackPanel.Children.Add(new Rectangle {
                Width = 2,
                Margin = new Thickness(2),
                HorizontalAlignment = HorizontalAlignment.Center,
                Fill = new LinearGradientBrush {
                    StartPoint = new Point(0.5, 0),
                    EndPoint = new Point(0.5, 1),
                    GradientStops = new GradientStopCollection {
                        new GradientStop {
                            Offset = 0,
                            Color = Color.FromArgb(0x10, 0x0, 0x0, 0x0)
                        },
                        new GradientStop {
                            Offset = 0.25,
                            Color = Color.FromArgb(0x40, 0x0, 0x0, 0x0)
                        },
                        new GradientStop {
                            Offset = 0.75,
                            Color = Color.FromArgb(0x40, 0x0, 0x0, 0x0)
                        },
                        new GradientStop {
                            Offset = 1,
                            Color = Color.FromArgb(0x10, 0x0, 0x0, 0x0)
                        },
                    },
                }
            });

            this.queryButton = new RadRibbonButton {
                FontSize = 13,
                VerticalAlignment = VerticalAlignment.Center,
                Size = ButtonSize.Large,
                LargeImage = new BitmapImage(this.ButtonIcon),
                Text = this.ButtonText,
                Tag = switcher,
                IsTabStop = true
            };
            this.queryButton.Click += (sender, e) => {
                var filterPanelSwitcher = ((FrameworkElement)sender).Tag as FilterPanelSwitcher;
                if(filterPanelSwitcher == null)
                    return;
                var filters = filterPanelSwitcher.CurrentFilterPanel.GetFilters();
                if(filters == null && !this.AcceptEmptyFilter)
                    UIHelper.ShowNotification(ShellUIStrings.FilterPanelRibbonGroup_EmptyFilters);
                else
                    this.RaiseExecutingQuery(filterPanelSwitcher.CurrentFilterPanel.Tag as QueryItemGroup, filters);
            };
            this.Items.Add(this.queryButton);

            this.IsUILoaded = true;
        }

        private void SetValueInternal(string groupUniqueId, string uniqueId, object value) {
            FilterPanel filterPanel;
            if(!this.panels.TryGetValue(groupUniqueId, out filterPanel))
                return;
            filterPanel.SetValueByUniqueId(uniqueId, value);
        }

        private void SetValuesInternal(IDictionary<string, string> values) {
            foreach(var kv in this.panels)
                kv.Value.SetValues(values);
        }

        private void SetEnabledInternal(string groupUniqueId, string uniqueId, bool value) {
            FilterPanel filterPanel;
            if(!this.panels.TryGetValue(groupUniqueId, out filterPanel))
                return;
            filterPanel.SetEnabledByUniqueId(uniqueId, value);
        }

        private void ExecuteQueryInternal() {
            if(this.queryButton == null)
                return;
            var peer = new ButtonAutomationPeer(this.queryButton);
            ((IInvokeProvider)peer.GetPattern(PatternInterface.Invoke)).Invoke();
        }

        public FilterPanelRibbonGroup() {
            this.CallEntryList = new List<CallEntry>();
            this.Variants.Add(new GroupVariant {
                Variant = RibbonGroupVariant.Large
            });
        }

        public void ExecuteQuery() {
            if(this.IsUILoaded)
                this.ExecuteQueryInternal();
            else {
                this.DelayCall(panel => panel.IsUILoaded, panel => panel.ExecuteQuery());
            }
        }

        public void SetValue(string groupUniqueId, string uniqueId, object value) {
            if(this.IsUILoaded)
                this.SetValueInternal(groupUniqueId, uniqueId, value);
            else {
                this.DelayCall(panel => panel.IsUILoaded, panel => panel.SetValue(groupUniqueId, uniqueId, value));
            }
        }

        public void SetValues(IDictionary<string, string> values) {
            if(this.IsUILoaded)
                this.SetValuesInternal(values);
            else {
                this.DelayCall(panel => panel.IsUILoaded, panel => panel.SetValues(values));
            }
        }

        public void SetEnabled(string groupUniqueId, string uniqueId, bool value) {
            if(this.IsUILoaded)
                this.SetEnabledInternal(groupUniqueId, uniqueId, value);
            else {
                this.DelayCall(panel => panel.IsUILoaded, panel => panel.SetEnabled(groupUniqueId, uniqueId, value));
            }
        }

        public List<CallEntry> CallEntryList {
            get;
            private set;
        }
    }
}
