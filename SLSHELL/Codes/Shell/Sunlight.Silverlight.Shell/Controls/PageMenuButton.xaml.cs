﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Sunlight.Silverlight.Commands;
using Sunlight.Silverlight.Converters;

namespace Sunlight.Silverlight.Controls {
    public partial class PageMenuButton {
        private static readonly PageMenuButtonEnableConverter PageMenuButtonEnableConverter = new PageMenuButtonEnableConverter();
        private readonly Storyboard sbMarquee;

        private void OnMouseEnter(object sender, MouseEventArgs mouseEventArgs) {
            if(this.FavoriteButton.IsChecked != true)
                this.FavoriteButton.Foreground = new SolidColorBrush(Color.FromArgb(255, 169, 169, 169));

            var range = this.DescriptionCanvas.Width - this.DescriptionText.ActualWidth;
            if(range >= 0)
                return;

            var beginTime = TimeSpan.FromSeconds(1);
            var pauseTime = TimeSpan.FromSeconds(1.5);
            var duration = TimeSpan.FromSeconds(-range / 50);
            this.sbMarquee.Duration = new Duration(beginTime + duration + pauseTime);
            var da = new DoubleAnimation {
                BeginTime = beginTime,
                Duration = new Duration(duration),
                From = 0,
                To = range,
            };
            Storyboard.SetTarget(da, this.DescriptionText);
            Storyboard.SetTargetProperty(da, new PropertyPath("(Canvas.Left)"));
            this.sbMarquee.Children.Clear();
            this.sbMarquee.Children.Add(da);
            this.sbMarquee.Stop();
            this.sbMarquee.Begin();
        }

        private void OnMouseLeave(object sender, MouseEventArgs mouseEventArgs) {
            if(this.FavoriteButton.IsChecked != true)
                this.FavoriteButton.Foreground = new SolidColorBrush(Colors.Transparent);

            this.sbMarquee.Stop();
        }

        private void OnLoaded(object sender, RoutedEventArgs e) {
            this.SetBinding(IsEnabledProperty, new Binding("PageType") {
                Mode = BindingMode.OneTime,
                Converter = PageMenuButtonEnableConverter,
            });
        }

        private void OnUnloaded(object sender, RoutedEventArgs routedEventArgs) {
            if(this.FavoriteButton.IsChecked != true)
                this.FavoriteButton.Foreground = new SolidColorBrush(Colors.Transparent);

            this.sbMarquee.Stop();
        }

        public PageMenuButton() {
            this.InitializeComponent();
            this.sbMarquee = new Storyboard {
                AutoReverse = true,
                RepeatBehavior = RepeatBehavior.Forever,
            };
            this.Command = new OpenPageCommand();
            this.MouseEnter += this.OnMouseEnter;
            this.MouseLeave += this.OnMouseLeave;
            this.Loaded += this.OnLoaded;
            this.Unloaded += this.OnUnloaded;

            this.FavoriteButton.Command = new FavoritePageCommand();
        }
    }
}
