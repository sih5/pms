﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Resources;
using Telerik.Windows.Controls.Data.DataPager;

namespace Sunlight.Silverlight.Controls {
    public sealed class RadDataPagerTotalRecordsText : UserControl {
        private sealed class RadDataPagerTotalRecordsConverter : IValueConverter {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
                var count = System.Convert.ToInt32(value);
                var pageSize = ((DataPagerPresenter)parameter).PageSize;
                return pageSize == int.MaxValue ? string.Format(ShellUIStrings.DataGridViewBase_TotalRecordsWithoutPageSize, count < 0 ? 0 : count) : string.Format(ShellUIStrings.DataGridViewBase_TotalRecords, count < 0 ? 0 : count, pageSize);
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
                throw new NotSupportedException();
            }
        }

        public static readonly DependencyProperty TargetProperty = DependencyProperty.Register("Target", typeof(DataPagerPresenter), typeof(RadDataPagerTotalRecordsText), new PropertyMetadata((o, args) => {
            var control = o as RadDataPagerTotalRecordsText;
            if(control == null)
                return;

            if(args.NewValue != null)
                control.text.SetBinding(TextBlock.TextProperty, new Binding("ItemCount") {
                    Source = args.NewValue,
                    Mode = BindingMode.OneWay,
                    Converter = new RadDataPagerTotalRecordsConverter(),
                    ConverterParameter = args.NewValue,
                });
        }));

        public DataPagerPresenter Target {
            get {
                return (DataPagerPresenter)this.GetValue(TargetProperty);
            }
            set {
                this.SetValue(TargetProperty, value);
            }
        }

        private readonly TextBlock text;

        public RadDataPagerTotalRecordsText() {
            this.Content = this.text = new TextBlock {
                FontSize = 11,
            };
        }
    }
}
