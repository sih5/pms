﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using Sunlight.Silverlight.Commands;
using Sunlight.Silverlight.Model;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Controls {
    public partial class RecentPageButton {
        public RecentPageButton() {
            this.InitializeComponent();
            if(this.ShowDisplayName) {
                var textBlock = new TextBlock {
                    MaxWidth = 80,
                };
                textBlock.SetBinding(TextBlock.TextProperty, new Binding("DisplayName"));
                this.Content = textBlock;
            }
            if(DesignerProperties.IsInDesignTool)
                return;
            this.Command = new OpenPageCommand {
                UpdateRecentList = false
            };
            this.MouseRightButtonDown += (sender, args) => {
                args.Handled = true;
                var pageMenuItem = this.CommandParameter as PageMenuItem;
                if(pageMenuItem == null || ShellViewModel.Current.RecentPageManager == null)
                    return;
                var uri = new Uri(ShellUtils.GetFrameUriFromMenuItemName(pageMenuItem.GroupMenuItem.SystemMenuItem.Name, pageMenuItem.GroupMenuItem.Name, pageMenuItem.Name), UriKind.Relative);
                RootViewCacheManager.Current.Remove(uri, () => {
                    ShellViewModel.Current.RecentPageManager.Remove(pageMenuItem);
                    if(ShellViewModel.Current.PageUri == uri)
                        ShellViewModel.Current.PageUri = HomeUri;
                });
            };
        }
        private static readonly Uri HomeUri = new Uri("/home", UriKind.Relative);

        /// <summary>
        /// 是否显示标题文字
        /// </summary>
        public bool ShowDisplayName {
            get {
                return (bool)GetValue(ShowDisplayNameProperty);
            }
            set {
                SetValue(ShowDisplayNameProperty, value);
            }
        }

        // Using a DependencyProperty as the backing store for ShowDisplayName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowDisplayNameProperty =
            DependencyProperty.Register("ShowDisplayName", typeof(bool), typeof(RecentPageButton), new PropertyMetadata(ShowDisplayNamePropertyChanged));

        private static void ShowDisplayNamePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args) {
            var recentPageButton = (RecentPageButton)obj;
            if((bool)args.NewValue) {
                var textBlock = new TextBlock();
                textBlock.SetBinding(TextBlock.MaxWidthProperty, new Binding("DisplayNameMaxWidth") {
                    Source = recentPageButton
                });
                textBlock.SetBinding(TextBlock.ForegroundProperty, new Binding("DisplayNameForeground") {
                    Source = recentPageButton
                });
                textBlock.SetBinding(TextBlock.TextProperty, new Binding("DisplayName"));
                recentPageButton.Content = textBlock;
            } else {
                recentPageButton.Content = null;
            }
        }

        /// <summary>
        /// 标题文字最大长度
        /// </summary>
        public int DisplayNameMaxWidth {
            get {
                return (int)GetValue(DisplayNameMaxWidthProperty);
            }
            set {
                SetValue(DisplayNameMaxWidthProperty, value);
            }
        }

        // Using a DependencyProperty as the backing store for DisplayNameMaxWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisplayNameMaxWidthProperty =
            DependencyProperty.Register("DisplayNameMaxWidth", typeof(int), typeof(RecentPageButton), null);

        /// <summary>
        /// 标题文字颜色
        /// </summary>
        public Brush DisplayNameForeground {
            get {
                return (Brush)GetValue(DisplayNameForegroundProperty);
            }
            set {
                SetValue(DisplayNameForegroundProperty, value);
            }
        }

        // Using a DependencyProperty as the backing store for DisplayNameForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisplayNameForegroundProperty =
            DependencyProperty.Register("DisplayNameForeground", typeof(Brush), typeof(RecentPageButton), null);
    }
}
