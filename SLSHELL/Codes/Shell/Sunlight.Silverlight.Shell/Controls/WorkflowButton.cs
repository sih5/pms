﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using Microsoft.Expression.Controls;
using Microsoft.Expression.Media;
using Sunlight.Silverlight.Commands;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Controls {
    public class WorkflowButton : Button {
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(WorkflowButton), null);
        public static readonly DependencyProperty ImageUriProperty = DependencyProperty.Register("ImageUri", typeof(string), typeof(WorkflowButton), new PropertyMetadata(OnImageUriPropertyChanged));
        public static readonly DependencyProperty StyleNumberProperty = DependencyProperty.Register("StyleNumber", typeof(int), typeof(WorkflowButton), new PropertyMetadata(1, OnStyleNumberPropertyChanged));
        public static readonly DependencyProperty PageUriProperty = DependencyProperty.Register("PageUri", typeof(string), typeof(WorkflowButton), new PropertyMetadata(OnPageUriPropertyChanged));
        public static readonly DependencyProperty DisallowMarkVisibilityProperty = DependencyProperty.Register("DisallowMarkVisibility", typeof(Visibility), typeof(WorkflowButton), new PropertyMetadata(Visibility.Collapsed));

        private static void OnImageUriPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args) {
            ((WorkflowButton)obj).Content = new Image {
                Source = new BitmapImage(Utils.MakeServerUri((string)args.NewValue)),
                Stretch = Stretch.None,
            };
        }

        private static void OnStyleNumberPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args) {
            ((WorkflowButton)obj).SetStyleNumber((int)args.NewValue);
        }

        private static void OnPageUriPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args) {
            var workflowButton = (WorkflowButton)obj;
            var value = (string)args.NewValue;

            workflowButton.CommandParameter = null;

            if(ShellViewModel.Current == null)
                return;

            var pageNames = ShellUtils.GetMenuItemNamesFromFrameUri(value);
            var systemMenuItem = ShellViewModel.Current.SystemMenuItems.SingleOrDefault(m => string.Compare(m.Name, pageNames[0], StringComparison.InvariantCultureIgnoreCase) == 0);
            if(systemMenuItem == null)
                return;
            var groupMenuItem = systemMenuItem.GroupMenuItems.SingleOrDefault(m => string.Compare(m.Name, pageNames[1], StringComparison.InvariantCultureIgnoreCase) == 0);
            if(groupMenuItem == null)
                return;
            var pageMenuItem = groupMenuItem.PageMenuItems.SingleOrDefault(m => string.Compare(m.Name, pageNames[2], StringComparison.InvariantCultureIgnoreCase) == 0);
            if(pageMenuItem == null)
                return;

            workflowButton.CommandParameter = pageMenuItem;
            workflowButton.CheckAvaliability();
        }

        private readonly Callout callout;
        private readonly TextBlock textBlock;
        private Popup popup;
        private Storyboard calloutAnimation;

        public string Text {
            get {
                return (string)this.GetValue(TextProperty);
            }
            set {
                this.SetValue(TextProperty, value);
            }
        }

        public string ImageUri {
            get {
                return (string)this.GetValue(ImageUriProperty);
            }
            set {
                this.SetValue(ImageUriProperty, value);
            }
        }

        public int StyleNumber {
            get {
                return (int)this.GetValue(StyleNumberProperty);
            }
            set {
                this.SetValue(StyleNumberProperty, value);
            }
        }

        public string PageUri {
            get {
                return (string)this.GetValue(PageUriProperty);
            }
            set {
                this.SetValue(PageUriProperty, value);
            }
        }

        public Visibility DisallowMarkVisibility {
            get {
                return (Visibility)this.GetValue(DisallowMarkVisibilityProperty);
            }
            set {
                this.SetValue(DisallowMarkVisibilityProperty, value);
            }
        }

        private void SetStyleNumber(int number) {
            this.Style = this.Resources["WorkflowButton" + number] as Style;
        }

        private Storyboard CreateCalloutAnimation() {
            var result = new Storyboard();
            Storyboard.SetTarget(result, this.callout);

            var duration = TimeSpan.FromMilliseconds(300);

            var animation = new DoubleAnimation {
                From = 0,
                To = 1,
                Duration = duration,
                EasingFunction = new CircleEase {
                    EasingMode = EasingMode.EaseIn,
                },
            };
            result.Children.Add(animation);
            Storyboard.SetTargetProperty(animation, new PropertyPath("Opacity"));

            animation = new DoubleAnimation {
                From = 0 - this.Width * 0.2,
                To = 0,
                Duration = duration,
            };
            result.Children.Add(animation);
            Storyboard.SetTargetProperty(animation, new PropertyPath("(UIElement.RenderTransform).(TranslateTransform.X)"));

            animation = new DoubleAnimation {
                From = this.Height * 0.5,
                To = 0,
                Duration = duration,
            };
            result.Children.Add(animation);
            Storyboard.SetTargetProperty(animation, new PropertyPath("(UIElement.RenderTransform).(TranslateTransform.Y)"));

            return result;
        }

        private void CheckAvaliability() {
            var valid = this.Command.CanExecute(this.CommandParameter);
            this.Cursor = valid ? Cursors.Hand : Cursors.Arrow;
            this.DisallowMarkVisibility = valid || string.IsNullOrWhiteSpace(this.PageUri) ? Visibility.Collapsed : Visibility.Visible;
        }

        protected override void OnMouseEnter(MouseEventArgs e) {
            base.OnMouseEnter(e);
            if(string.IsNullOrWhiteSpace(this.Text))
                return;

            if(this.popup == null) {
                this.calloutAnimation = this.CreateCalloutAnimation();
                this.popup = new Popup {
                    Child = this.callout,
                };
                this.popup.Opened += (sender, e1) => {
                    var point = this.TransformToVisual(Application.Current.RootVisual).Transform(new Point(this.Width * 0.6, 0));
                    this.popup.HorizontalOffset = point.X;
                    this.popup.VerticalOffset = point.Y - this.textBlock.ActualHeight * 1.4 - 10;
                    this.calloutAnimation.Begin();
                };
            }
            this.popup.IsOpen = true;
        }

        protected override void OnMouseLeave(MouseEventArgs e) {
            base.OnMouseLeave(e);
            if(this.popup != null)
                this.popup.IsOpen = false;
        }

        public WorkflowButton() {
            this.Resources.MergedDictionaries.Add(new ResourceDictionary {
                Source = new Uri("/Sunlight.Silverlight.Shell;component/Themes/WorkflowButton.xaml", UriKind.Relative)
            });
            this.SetStyleNumber(this.StyleNumber);
            this.textBlock = new TextBlock {
                Foreground = new SolidColorBrush(Colors.White),
                FontFamily = new FontFamily("Microsoft YaHei"),
                MaxWidth = 320,
                TextWrapping = TextWrapping.Wrap,
                FontSize = 14,
                Margin = new Thickness(10, 5, 10, 5),
            };
            this.textBlock.SetBinding(TextBlock.TextProperty, new Binding("Text") {
                Source = this,
                Mode = BindingMode.TwoWay,
            });
            this.callout = new Callout {
                Fill = new SolidColorBrush(Color.FromArgb(0xff, 0x6e, 0x6e, 0x6e)),
                CalloutStyle = CalloutStyle.RoundedRectangle,
                AnchorPoint = new Point(0.15, 1.4),
                Content = this.textBlock,
                RenderTransform = new TranslateTransform {
                    X = -10,
                    Y = 25,
                },
            };

            this.Command = new OpenPageCommand();
            this.CheckAvaliability();
        }
    }
}
