﻿using System;
using System.Globalization;
using System.Windows.Data;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Converters {
    public sealed class DataGridViewCommandParameterConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var viewModel = value as BuiltInOperationViewModel;
            return viewModel == null ? null : viewModel.CommandParameter;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotSupportedException();
        }
    }
}