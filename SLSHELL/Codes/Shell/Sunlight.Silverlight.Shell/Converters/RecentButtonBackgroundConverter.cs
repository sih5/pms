﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Sunlight.Silverlight.Converters {
    public class RecentButtonBackgroundConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            return (bool)value ? new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0xA5, 0x00)) : null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotSupportedException();
        }
    }
}
