﻿using System;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Practices.Unity;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight {
    public static class DI {
        private static readonly IUnityContainer Container = BaseApp.Current.Container;

        internal static DomainContext GetDomainContextByType(Type type) {
            var ns = type.Namespace.Split('.');
            if(ns.Length >= 3 && ns[0] == "Sunlight" && ns[1] == "Silverlight" && Container.IsRegistered(typeof(DomainContext), ns[2]))
                return Container.Resolve<DomainContext>(ns[2]);
            return null;
        }

        public static QueryPanelBase GetQueryPanel(string key) {
            return Container.Resolve<IQueryPanel>(key) as QueryPanelBase;
        }

        public static ActionPanelBase GetActionPanel(string key) {
            return Container.Resolve<IActionPanel>(key) as ActionPanelBase;
        }

        public static FrameworkElement GetDetailPanel(string key) {
            return Container.Resolve<IDetailPanel>(key) as FrameworkElement;
        }

        public static FrameworkElement GetDataEditPanel(string key) {
            return Container.Resolve<IDataEditPanel>(key) as FrameworkElement;
        }

        public static DataGridViewBase GetDataGridView(string key) {
            return Container.Resolve<IDataGridView>(key) as DataGridViewBase;
        }

        public static DataEditViewBase GetDataEditView(string key) {
            return Container.Resolve<IDataEditView>(key) as DataEditViewBase;
        }

        public static QueryWindowBase GetQueryWindow(string key) {
            return Container.Resolve<IQueryWindow>(key) as QueryWindowBase;
        }

        public static WorkflowViewModelBase GetWorkflow(string key) {
            return Container.Resolve<WorkflowViewModelBase>(key);
        }

        public static UserControl GetView(string key) {
            return Container.Resolve<IBaseView>(key) as UserControl;
        }
    }
}
