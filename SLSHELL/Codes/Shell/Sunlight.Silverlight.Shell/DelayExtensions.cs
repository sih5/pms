﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Sunlight.Silverlight {
    /// <summary>
    /// 延迟调用的辅助接口
    /// </summary>
    public interface IDelayCall {
        /// <summary>
        /// 延迟调用的辅助队列,防止弱引用的调用方法被提前释放
        /// </summary>
        List<CallEntry> CallEntryList {
            get;
        }
    }

    //public sealed class WeakReferenceHelper {
    //    internal CallEntry CallEntry {
    //        get;
    //        set;
    //    }
    //}

    public sealed class CallEntry {
        internal string PropertyName {
            get;
            set;
        }

        internal Action<object> ActionToCall {
            get;
            set;
        }

        internal Func<bool> CanCall {
            get;
            set;
        }

        /// <summary>
        /// 所在队列，用于调用后释放
        /// </summary>
        internal List<CallEntry> OwnerList;
    }

    public static class DelayExtensions {

        private static Dictionary<WeakReference, List<WeakReference>> weakCallPool;

        private static void TargetPropertyChanged(object sender, PropertyChangedEventArgs e) {
            var target = sender as INotifyPropertyChanged;
            var wcp = new KeyValuePair<WeakReference, List<WeakReference>>();
            if(weakCallPool != null)
                wcp = weakCallPool.SingleOrDefault(c => ReferenceEquals(c.Key.Target, target));

            if(wcp.Key == null) {
                if(target != null)
                    target.PropertyChanged -= TargetPropertyChanged;
            } else if(wcp.Key != null && weakCallPool != null) {
                var list = weakCallPool[wcp.Key];
                var entries = list.Select(wr => new {
                    wr,
                    CallEntry = (CallEntry)wr.Target,
                }).Where(entry => entry.CallEntry.PropertyName == e.PropertyName && entry.CallEntry.CanCall()).ToArray();
                foreach(var entry in entries) {
                    list.Remove(entry.wr);
                    entry.CallEntry.OwnerList.Remove(entry.CallEntry);
                    entry.CallEntry.ActionToCall(target);
                }

                if(list.Count > 0)
                    return;

                if(target != null)
                    target.PropertyChanged -= TargetPropertyChanged;
                weakCallPool.Remove(wcp.Key);
            }
        }

        private static void RegisterCall(INotifyPropertyChanged target, CallEntry helper) {
            //if(helper == null) {
            //    if(callPool == null)
            //        callPool = new Dictionary<WeakReference, List<CallEntry>>();
            //    var cp = callPool.SingleOrDefault(c => ReferenceEquals(c.Key.Target, target));
            //    if(cp.Key != null)
            //        callPool[cp.Key].Add(entry);
            //    else {
            //        callPool.Add(new WeakReference(target, false), new List<CallEntry>(new[] {
            //            entry
            //        }));
            //        target.PropertyChanged += TargetPropertyChanged;
            //    }
            //} else {
            if(weakCallPool == null)
                weakCallPool = new Dictionary<WeakReference, List<WeakReference>>();
            var cp = weakCallPool.SingleOrDefault(c => ReferenceEquals(c.Key.Target, target));
            if(cp.Key != null)
                weakCallPool[cp.Key].Add(new WeakReference(helper));
            else {
                weakCallPool.Add(new WeakReference(target, false), new List<WeakReference>(new[] {
                        new WeakReference(helper)
                    }));
                target.PropertyChanged += TargetPropertyChanged;
            }
            //}
        }

        /// <summary>
        /// 待窗体初始化完成后，再执行property的方法
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="target">需要同时实现INotifyPropertyChanged, IDelayCallHelper这两个接口，IDelayCallHelper接口的属性需要在构造函数中初始化</param>
        /// <param name="property"></param>
        /// <param name="action"></param>
        public static void DelayCall<T, TProperty>(this T target, Expression<Func<T, TProperty>> property, Action<T> action) where T : INotifyPropertyChanged, IDelayCall {
            if(property == null)
                throw new ArgumentNullException("property");
            if(action == null)
                throw new ArgumentNullException("action");

            var lambdaExpression = (LambdaExpression)property;
            var memberExpression = lambdaExpression.Body as MemberExpression;
            if(memberExpression == null)
                throw new ArgumentException("必须提供一个返回值是布尔类型的属性的表达式", "property");

            var propertyInfo = memberExpression.Member as PropertyInfo;

            if(propertyInfo == null)
                throw new ArgumentException(string.Format("成员 {0} 的类型必须是一个属性", memberExpression.Member.Name), "property");
            if(propertyInfo.DeclaringType != typeof(T))
                throw new ArgumentException(string.Format("属性 {0} 必须属于 {1} 类型", propertyInfo.Name, typeof(T).FullName), "property");
            if(propertyInfo.PropertyType != typeof(bool))
                throw new ArgumentException(string.Format("属性 {0} 的类型必须是布尔类型", propertyInfo.Name), "property");

            Func<bool> canCall = () => (bool)propertyInfo.GetValue(target, null);
            if(canCall()) {
                action(target);
                return;
            }
            var entry = new CallEntry {
                PropertyName = propertyInfo.Name,
                ActionToCall = obj => action((T)obj),
                CanCall = canCall,
                OwnerList = target.CallEntryList,
            };
            target.CallEntryList.Add(entry);
            RegisterCall(target, entry);
        }
    }
}
