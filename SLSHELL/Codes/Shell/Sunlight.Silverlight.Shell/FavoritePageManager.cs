﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Microsoft.Practices.Unity;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Model;
using Sunlight.Silverlight.Resources;

namespace Sunlight.Silverlight {
    public sealed class FavoritePageManager : ModelBase, INotifyCollectionChanged {
        private ISecurityManager securityManager;
        private readonly ObservableCollection<PageMenuItem> pageMenuItems;
        private readonly ReadOnlyObservableCollection<PageMenuItem> menuItems;

        public ISecurityManager SecurityManager {
            get {
                return this.securityManager ?? (this.securityManager = BaseApp.Current.Container.Resolve<ISecurityManager>());
            }
        }

        public ReadOnlyObservableCollection<PageMenuItem> MenuItems {
            get {
                return this.menuItems;
            }
        }

        private void SetFavoritePage(int pageId, bool isFavorite) {
            this.SecurityManager.SetFavoritePage(pageId, isFavorite, ar => {
                var result = ar as ISecurityActionResult;
                if(result != null)
                    result.IsErrorHandled = true;
            });
        }

        private void HandleCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            this.OnCollectionChanged(e);
        }

        private void OnCollectionChanged(NotifyCollectionChangedEventArgs args) {
            if(this.CollectionChanged != null)
                this.CollectionChanged(this, args);
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;
        public event EventHandler MenuItemsOrderChanged;

        private void OnMenuItemsOrderChanged() {
            var handler = this.MenuItemsOrderChanged;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        public void Add(PageMenuItem menuItem) {
            if(this.pageMenuItems.Contains(menuItem))
                return;
            menuItem.IsFavorite = true;
            this.pageMenuItems.Add(menuItem);

            this.SetFavoritePage(menuItem.Id, true);
        }

        public void Remove(PageMenuItem menuItem) {
            menuItem.IsFavorite = false;
            if(!this.pageMenuItems.Remove(menuItem))
                return;
            this.SetFavoritePage(menuItem.Id, false);
        }

        public void ChangeMenuItemsOrder(List<int> menuItemsOrder) {
            var tmpMenuItems = this.pageMenuItems.ToList();
            if(!menuItemsOrder.OrderBy(k => k).SequenceEqual(tmpMenuItems.Select(item => item.Id).OrderBy(id => id)))
                throw new ArgumentException(ShellUIStrings.FavoritePageManager_ChangeSequence_ArgumentException1, "menuItemsOrder");

            var pagesOrder = new Dictionary<int, int>(menuItemsOrder.Count);
            for(var i = 0; i < menuItemsOrder.Count; i++) {
                pagesOrder[menuItemsOrder[i]] = i;
                this.pageMenuItems[i] = tmpMenuItems.Single(menuItem => menuItem.Id == menuItemsOrder[i]);
            }

            this.OnMenuItemsOrderChanged();
            this.SecurityManager.ChangeFavoritePagesOrder(pagesOrder, asyncResult => {
                var result = asyncResult as ISecurityActionResult;
                if(result != null)
                    result.IsErrorHandled = true;
            });
        }

        internal void Initialize(IEnumerable<PageMenuItem> pages) {
            this.pageMenuItems.Clear();
            foreach(var page in pages)
                this.pageMenuItems.Add(page);
        }

        public FavoritePageManager() {
            this.pageMenuItems = new ObservableCollection<PageMenuItem>();
            this.menuItems = new ReadOnlyObservableCollection<PageMenuItem>(this.pageMenuItems);
            this.pageMenuItems.CollectionChanged += this.HandleCollectionChanged;
        }
    }
}
