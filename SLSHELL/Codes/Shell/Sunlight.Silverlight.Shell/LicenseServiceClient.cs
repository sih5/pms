﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading;
using System.Windows;
using Sunlight.Silverlight.Core;

namespace Sunlight.Silverlight {
    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    [ServiceContract(Namespace = "http://sunlight.bz")]
    public interface ILicenseService {
        [OperationContract(AsyncPattern = true, Action = "http://sunlight.bz/ILicenseService/IsActivated", ReplyAction = "http://sunlight.bz/ILicenseService/IsActivatedResponse")]
        IAsyncResult BeginIsActivated(AsyncCallback callback, object asyncState);

        bool EndIsActivated(IAsyncResult result);

        [OperationContract(AsyncPattern = true, Action = "http://sunlight.bz/ILicenseService/Activate", ReplyAction = "http://sunlight.bz/ILicenseService/ActivateResponse")]
        IAsyncResult BeginActivate(string sn, AsyncCallback callback, object asyncState);

        string EndActivate(IAsyncResult result);
    }

    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    public interface ILicenseServiceChannel : ILicenseService, IClientChannel {
    }

    [DebuggerStepThrough]
    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    public class IsActivatedCompletedEventArgs : AsyncCompletedEventArgs {
        private readonly object[] results;

        public IsActivatedCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState) {
            this.results = results;
        }

        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
    }

    [DebuggerStepThrough]
    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    public class ActivateCompletedEventArgs : AsyncCompletedEventArgs {
        private readonly object[] results;

        public ActivateCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState) {
            this.results = results;
        }

        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }

    [DebuggerStepThrough]
    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    public class LicenseServiceClient : ClientBase<ILicenseService>, ILicenseService {
        private BeginOperationDelegate onBeginIsActivatedDelegate;

        private EndOperationDelegate onEndIsActivatedDelegate;

        private SendOrPostCallback onIsActivatedCompletedDelegate;

        private BeginOperationDelegate onBeginActivateDelegate;

        private EndOperationDelegate onEndActivateDelegate;

        private SendOrPostCallback onActivateCompletedDelegate;

        private BeginOperationDelegate onBeginOpenDelegate;

        private EndOperationDelegate onEndOpenDelegate;

        private SendOrPostCallback onOpenCompletedDelegate;

        private BeginOperationDelegate onBeginCloseDelegate;

        private EndOperationDelegate onEndCloseDelegate;

        private SendOrPostCallback onCloseCompletedDelegate;

        public LicenseServiceClient() : base(new BasicHttpBinding(Application.Current.Host.Source.Scheme == Uri.UriSchemeHttps ? BasicHttpSecurityMode.Transport : BasicHttpSecurityMode.None) {
            MaxBufferSize = 2147483647,
            MaxReceivedMessageSize = 2147483647,
        }, new EndpointAddress(Utils.MakeServerUri("/LicenseService.svc"))) {
        }

        public CookieContainer CookieContainer {
            get {
                var httpCookieContainerManager = this.InnerChannel.GetProperty<IHttpCookieContainerManager>();
                return (httpCookieContainerManager != null) ? httpCookieContainerManager.CookieContainer : null;
            }
            set {
                var httpCookieContainerManager = this.InnerChannel.GetProperty<IHttpCookieContainerManager>();
                if((httpCookieContainerManager != null))
                    httpCookieContainerManager.CookieContainer = value;
                else
                    throw new InvalidOperationException("Unable to set the CookieContainer. Please make sure the binding contains an HttpCookieContainerBindingElement.");
            }
        }

        public event EventHandler<IsActivatedCompletedEventArgs> IsActivatedCompleted;

        public event EventHandler<ActivateCompletedEventArgs> ActivateCompleted;

        public event EventHandler<AsyncCompletedEventArgs> OpenCompleted;

        public event EventHandler<AsyncCompletedEventArgs> CloseCompleted;

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        IAsyncResult ILicenseService.BeginIsActivated(AsyncCallback callback, object asyncState) {
            return this.Channel.BeginIsActivated(callback, asyncState);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        bool ILicenseService.EndIsActivated(IAsyncResult result) {
            return this.Channel.EndIsActivated(result);
        }

        private IAsyncResult OnBeginIsActivated(object[] inValues, AsyncCallback callback, object asyncState) {
            return ((ILicenseService)(this)).BeginIsActivated(callback, asyncState);
        }

        private object[] OnEndIsActivated(IAsyncResult result) {
            var retVal = ((ILicenseService)(this)).EndIsActivated(result);
            return new object[] {
                retVal
            };
        }

        private void OnIsActivatedCompleted(object state) {
            if((this.IsActivatedCompleted != null)) {
                var e = ((InvokeAsyncCompletedEventArgs)(state));
                this.IsActivatedCompleted(this, new IsActivatedCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
            }
        }

        public void IsActivatedAsync() {
            this.IsActivatedAsync(null);
        }

        public void IsActivatedAsync(object userState) {
            if((this.onBeginIsActivatedDelegate == null))
                this.onBeginIsActivatedDelegate = this.OnBeginIsActivated;
            if((this.onEndIsActivatedDelegate == null))
                this.onEndIsActivatedDelegate = this.OnEndIsActivated;
            if((this.onIsActivatedCompletedDelegate == null))
                this.onIsActivatedCompletedDelegate = this.OnIsActivatedCompleted;
            this.InvokeAsync(this.onBeginIsActivatedDelegate, null, this.onEndIsActivatedDelegate, this.onIsActivatedCompletedDelegate, userState);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        IAsyncResult ILicenseService.BeginActivate(string sn, AsyncCallback callback, object asyncState) {
            return this.Channel.BeginActivate(sn, callback, asyncState);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        string ILicenseService.EndActivate(IAsyncResult result) {
            return this.Channel.EndActivate(result);
        }

        private IAsyncResult OnBeginActivate(object[] inValues, AsyncCallback callback, object asyncState) {
            var sn = ((string)(inValues[0]));
            return ((ILicenseService)(this)).BeginActivate(sn, callback, asyncState);
        }

        private object[] OnEndActivate(IAsyncResult result) {
            var retVal = ((ILicenseService)(this)).EndActivate(result);
            return new object[] {
                retVal
            };
        }

        private void OnActivateCompleted(object state) {
            if((this.ActivateCompleted != null)) {
                var e = ((InvokeAsyncCompletedEventArgs)(state));
                this.ActivateCompleted(this, new ActivateCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
            }
        }

        public void ActivateAsync(string sn) {
            this.ActivateAsync(sn, null);
        }

        public void ActivateAsync(string sn, object userState) {
            if((this.onBeginActivateDelegate == null))
                this.onBeginActivateDelegate = this.OnBeginActivate;
            if((this.onEndActivateDelegate == null))
                this.onEndActivateDelegate = this.OnEndActivate;
            if((this.onActivateCompletedDelegate == null))
                this.onActivateCompletedDelegate = this.OnActivateCompleted;
            this.InvokeAsync(this.onBeginActivateDelegate, new object[] {
                sn
            }, this.onEndActivateDelegate, this.onActivateCompletedDelegate, userState);
        }

        private IAsyncResult OnBeginOpen(object[] inValues, AsyncCallback callback, object asyncState) {
            return ((ICommunicationObject)(this)).BeginOpen(callback, asyncState);
        }

        private object[] OnEndOpen(IAsyncResult result) {
            ((ICommunicationObject)(this)).EndOpen(result);
            return null;
        }

        private void OnOpenCompleted(object state) {
            if((this.OpenCompleted != null)) {
                var e = ((InvokeAsyncCompletedEventArgs)(state));
                this.OpenCompleted(this, new AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }

        public void OpenAsync() {
            this.OpenAsync(null);
        }

        public void OpenAsync(object userState) {
            if((this.onBeginOpenDelegate == null))
                this.onBeginOpenDelegate = this.OnBeginOpen;
            if((this.onEndOpenDelegate == null))
                this.onEndOpenDelegate = this.OnEndOpen;
            if((this.onOpenCompletedDelegate == null))
                this.onOpenCompletedDelegate = this.OnOpenCompleted;
            this.InvokeAsync(this.onBeginOpenDelegate, null, this.onEndOpenDelegate, this.onOpenCompletedDelegate, userState);
        }

        private IAsyncResult OnBeginClose(object[] inValues, AsyncCallback callback, object asyncState) {
            return ((ICommunicationObject)(this)).BeginClose(callback, asyncState);
        }

        private object[] OnEndClose(IAsyncResult result) {
            ((ICommunicationObject)(this)).EndClose(result);
            return null;
        }

        private void OnCloseCompleted(object state) {
            if((this.CloseCompleted != null)) {
                var e = ((InvokeAsyncCompletedEventArgs)(state));
                this.CloseCompleted(this, new AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }

        public void CloseAsync() {
            this.CloseAsync(null);
        }

        public void CloseAsync(object userState) {
            if((this.onBeginCloseDelegate == null))
                this.onBeginCloseDelegate = this.OnBeginClose;
            if((this.onEndCloseDelegate == null))
                this.onEndCloseDelegate = this.OnEndClose;
            if((this.onCloseCompletedDelegate == null))
                this.onCloseCompletedDelegate = this.OnCloseCompleted;
            this.InvokeAsync(this.onBeginCloseDelegate, null, this.onEndCloseDelegate, this.onCloseCompletedDelegate, userState);
        }

        protected override ILicenseService CreateChannel() {
            return new LicenseServiceClientChannel(this);
        }

        private class LicenseServiceClientChannel : ChannelBase<ILicenseService>, ILicenseService {
            public LicenseServiceClientChannel(ClientBase<ILicenseService> client) : base(client) {
            }

            public IAsyncResult BeginIsActivated(AsyncCallback callback, object asyncState) {
                var _args = new object[0];
                var _result = this.BeginInvoke("IsActivated", _args, callback, asyncState);
                return _result;
            }

            public bool EndIsActivated(IAsyncResult result) {
                var _args = new object[0];
                var _result = ((bool)(this.EndInvoke("IsActivated", _args, result)));
                return _result;
            }

            public IAsyncResult BeginActivate(string sn, AsyncCallback callback, object asyncState) {
                var _args = new object[1];
                _args[0] = sn;
                var _result = this.BeginInvoke("Activate", _args, callback, asyncState);
                return _result;
            }

            public string EndActivate(IAsyncResult result) {
                var _args = new object[0];
                var _result = ((string)(this.EndInvoke("Activate", _args, result)));
                return _result;
            }
        }
    }
}
