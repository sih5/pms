﻿using System.Collections.Generic;

namespace Sunlight.Silverlight.Model {
    /// <summary>
    ///     菜单分组项的 Model。
    /// </summary>
    public class GroupMenuItem : MenuItem {
        /// <summary>
        ///     获取或设置该分组的子级页面项的 Model 集合。
        /// </summary>
        public IEnumerable<PageMenuItem> PageMenuItems {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置该分组的父级系统菜单项的 Model 实例。
        /// </summary>
        public SystemMenuItem SystemMenuItem {
            get;
            set;
        }
    }
}
