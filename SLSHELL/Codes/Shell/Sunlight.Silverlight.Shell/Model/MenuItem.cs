﻿using System;

namespace Sunlight.Silverlight.Model {
    /// <summary>
    ///     菜单项 Model。
    /// </summary>
    public class MenuItem {
        /// <summary>
        ///     菜单项的数据ID。
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        ///     菜单项的名称。
        /// </summary>
        public string Name {
            get;
            set;
        }

        /// <summary>
        ///     菜单项的显示名称。
        /// </summary>
        public string DisplayName {
            get;
            set;
        }

        /// <summary>
        ///     菜单项的描述信息。
        /// </summary>
        public string Description {
            get;
            set;
        }

        /// <summary>
        ///     菜单项的图标 <c>Uri</c>。
        /// </summary>
        public Uri ImageUri {
            get;
            set;
        }
    }
}
