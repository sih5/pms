﻿using System;
using System.Windows;

namespace Sunlight.Silverlight.Model {
    public sealed class NavigationItem {
        public string Title {
            get;
            set;
        }

        public Uri Thumbnail {
            get;
            set;
        }

        public UIElement Content {
            get;
            set;
        }
    }
}
