﻿using System;
using System.ComponentModel;
using Sunlight.Silverlight.Annotations;

namespace Sunlight.Silverlight.Model {
    /// <summary>
    ///     页面菜单项的 Model。
    /// </summary>
    public class PageMenuItem : MenuItem, INotifyPropertyChanged {
        private bool isFavorite;
        private bool isCurrent;

        /// <summary>
        ///     获取或设置页面的类型。页面的类型决定了使用哪一种框架 View 来呈现该页面。
        /// </summary>
        public string PageType {
            get;
            set;
        }

        public Uri PageUri {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置页面的参数，不同的页面类型有不同的解析参数的方式。
        /// </summary>
        public string PageParameter {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置该页面项的父级分组项的 Model 实例。
        /// </summary>
        public GroupMenuItem GroupMenuItem {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置页面的已添加至收藏夹
        /// </summary>
        public bool IsFavorite {
            get {
                return this.isFavorite;
            }
            set {
                if(this.isFavorite == value)
                    return;
                this.isFavorite = value;
                this.OnPropertyChanged("IsFavorite");
            }
        }

        /// <summary>
        /// 是否当前访问页面
        /// </summary>
        public bool IsCurrent {
            get {
                return this.isCurrent;
            }
            set {
                if(this.isCurrent == value)
                    return;
                this.isCurrent = value;
                this.OnPropertyChanged("IsCurrent");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName) {
            var handler = PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
