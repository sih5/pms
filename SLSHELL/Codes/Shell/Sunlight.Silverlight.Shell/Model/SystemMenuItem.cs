﻿using System.Collections.Generic;

namespace Sunlight.Silverlight.Model {
    /// <summary>
    ///     系统菜单项的 Model。
    /// </summary>
    public class SystemMenuItem : MenuItem {
        /// <summary>
        ///     获取或设置该系统的子级分组项的 Model 集合。
        /// </summary>
        public IEnumerable<GroupMenuItem> GroupMenuItems {
            get;
            set;
        }
    }
}
