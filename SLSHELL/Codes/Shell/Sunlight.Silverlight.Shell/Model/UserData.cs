﻿namespace Sunlight.Silverlight.Model {
    public struct UserData {
        private readonly int userId, enterpriseId, enterpriseCategoryId;
        private readonly string userCode, userName, enterpriseCode, enterpriseName;

        public int UserId {
            get {
                return this.userId;
            }
        }

        public string UserCode {
            get {
                return this.userCode;
            }
        }

        public string UserName {
            get {
                return this.userName;
            }
        }

        public int EnterpriseId {
            get {
                return this.enterpriseId;
            }
        }

        public string EnterpriseCode {
            get {
                return this.enterpriseCode;
            }
        }

        public string EnterpriseName {
            get {
                return this.enterpriseName;
            }
        }
        public int EnterpriseCategoryId {
            get {
                return this.enterpriseCategoryId;
            }
        }

        public UserData(int userId, string userCode, string userName, int enterpriseId, string enterpriseCode, string enterpriseName, int enterpriseCategoryId) {
            this.userId = userId;
            this.userCode = userCode;
            this.userName = userName;
            this.enterpriseId = enterpriseId;
            this.enterpriseCode = enterpriseCode;
            this.enterpriseName = enterpriseName;
            this.enterpriseCategoryId = enterpriseCategoryId;
        }
    }
}
