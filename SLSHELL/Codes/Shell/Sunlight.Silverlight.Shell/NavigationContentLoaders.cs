﻿using System;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Practices.Unity;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight {
    public class ContentLoaderAsyncResult : IAsyncResult {
        internal Uri TargetUri {
            get;
            private set;
        }

        internal Exception Exception {
            get;
            set;
        }

        internal object Content {
            get;
            set;
        }

        public bool IsCompleted {
            get;
            internal set;
        }

        public WaitHandle AsyncWaitHandle {
            get {
                return null;
            }
        }

        public object AsyncState {
            get;
            private set;
        }

        public bool CompletedSynchronously {
            get {
                return false;
            }
        }

        public ContentLoaderAsyncResult(Uri targetUri, object asyncState) {
            this.TargetUri = targetUri;
            this.AsyncState = asyncState;
        }
    }

    public abstract class ViewNavigationContentLoader : INavigationContentLoader {
        public virtual bool CanLoad(Uri targetUri, Uri currentUri) {
            return false;
        }

        public virtual void CancelLoad(IAsyncResult asyncResult) {
        }

        public virtual IAsyncResult BeginLoad(Uri targetUri, Uri currentUri, AsyncCallback userCallback, object asyncState) {
            if(targetUri == null)
                throw new ArgumentNullException("targetUri");

            var result = new ContentLoaderAsyncResult(targetUri, asyncState);

            if(SynchronizationContext.Current != null)
                SynchronizationContext.Current.Post(args => this.Load(result, userCallback), null);
            else
                Deployment.Current.Dispatcher.BeginInvoke(() => this.Load(result, userCallback));

            return result;
        }

        public virtual LoadResult EndLoad(IAsyncResult asyncResult) {
            if(asyncResult == null)
                throw new ArgumentNullException("asyncResult");

            var result = asyncResult as ContentLoaderAsyncResult;
            if(result == null)
                throw new InvalidOperationException();

            if(result.Exception != null)
                throw result.Exception;

            return new LoadResult(result.Content);
        }

        protected abstract void Load(ContentLoaderAsyncResult result, AsyncCallback userCallback);
    }

    public class RootViewNavigationContentLoader : INavigationContentLoader {
        private const string BUILTIN_SYSTEM = "SDS";
        private const string BUILTIN_HOME = "Home";
        private readonly PageResourceContentLoader legacyContentLoader = new PageResourceContentLoader();

        /// <summary>
        /// 仅返回 <param name="uri">路径</param> 中的 Path 部分作为新的 Uri
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        private static Uri GetPathUri(Uri uri) {
            var absoluteUri = uri.IsAbsoluteUri ? uri : new Uri(BaseApp.Current.Host.Source, uri);
            var path = absoluteUri.GetComponents(UriComponents.Path, UriFormat.SafeUnescaped);
            path = (path.StartsWith("/") ? "" : "/") + path;
            return new Uri(path, UriKind.Relative);
        }

        private static void Load(ContentLoaderAsyncResult result, AsyncCallback userCallback) {
            if(result == null)
                throw new ArgumentNullException("result");

            try {
                // 忽略 result.TargetUri 中可能包含的 Query 部分
                var uri = GetPathUri(result.TargetUri);
                var content = RootViewCacheManager.Current.Get(uri);
                if(content != null) {
                    result.Content = content;
                    result.IsCompleted = true;
                    if(userCallback != null)
                        userCallback(result);
                    return;
                }

                content = BaseApp.Current.Container.Resolve<IRootView>(uri.OriginalString);
                RootViewCacheManager.Current.Update(uri, content, () => {
                    try {
                        result.Content = content;

                        var page = content as Page;
                        if(page != null)
                            page.NavigationCacheMode = NavigationCacheMode.Disabled;

                        var pageNames = ShellUtils.GetMenuItemNamesFromFrameUri(uri.OriginalString);
                        if(string.Compare(pageNames[0], BUILTIN_HOME, StringComparison.InvariantCultureIgnoreCase) == 0 || string.Compare(pageNames[0], BUILTIN_SYSTEM, StringComparison.InvariantCultureIgnoreCase) == 0)
                            return;

                        var dataManagementView = content as DataManagementViewBase;
                        if(dataManagementView != null) {
                            var systemMenuItem = ShellViewModel.Current.SystemMenuItems.Single(m => string.Compare(m.Name, pageNames[0], StringComparison.InvariantCultureIgnoreCase) == 0);
                            var groupMenuItem = systemMenuItem.GroupMenuItems.Single(m => string.Compare(m.Name, pageNames[1], StringComparison.InvariantCultureIgnoreCase) == 0);
                            var pageMenuItem = groupMenuItem.PageMenuItems.Single(m => string.Compare(m.Name, pageNames[2], StringComparison.InvariantCultureIgnoreCase) == 0);
                            dataManagementView.UniqueId = pageMenuItem.Id;
                            if(ShellViewModel.Current.RecentPageManager == null)
                                return;
                            if(!ShellViewModel.Current.RecentPageManager.Contains(pageMenuItem))
                                ShellViewModel.Current.RecentPageManager.Add(pageMenuItem);
                            pageMenuItem.PageUri = uri;
                            ShellViewModel.Current.RecentPageManager.MarkCurrentPage(pageMenuItem);
                        }
                    } catch(Exception ex) {
                        result.Exception = ex;
                    } finally {
                        result.IsCompleted = true;
                        if(userCallback != null)
                            userCallback(result);
                    }
                });
            } catch(Exception ex) {
                result.Exception = ex;
            }
        }

        public virtual bool CanLoad(Uri targetUri, Uri currentUri) {
            Func<bool> canLoad = () => {
                if(targetUri == null || ShellViewModel.Current == null)
                    return false;

                var uri = GetPathUri(targetUri);
                if(!BaseApp.Current.Container.IsRegistered<IRootView>(uri.OriginalString))
                    return false;

                var pageNames = ShellUtils.GetMenuItemNamesFromFrameUri(uri.OriginalString);
                if(pageNames.Length != 3)
                    return false;

                if(string.Compare(pageNames[0], BUILTIN_HOME, StringComparison.InvariantCultureIgnoreCase) == 0 || string.Compare(pageNames[0], BUILTIN_SYSTEM, StringComparison.InvariantCultureIgnoreCase) == 0)
                    return true;

                if(ShellViewModel.Current.SystemMenuItems == null)
                    return false;

                var systemMenuItem = ShellViewModel.Current.SystemMenuItems.SingleOrDefault(m => string.Compare(m.Name, pageNames[0], StringComparison.InvariantCultureIgnoreCase) == 0);
                if(systemMenuItem == null)
                    return false;
                var groupMenuItem = systemMenuItem.GroupMenuItems.SingleOrDefault(m => string.Compare(m.Name, pageNames[1], StringComparison.InvariantCultureIgnoreCase) == 0);
                if(groupMenuItem == null)
                    return false;
                return groupMenuItem.PageMenuItems.SingleOrDefault(m => string.Compare(m.Name, pageNames[2], StringComparison.InvariantCultureIgnoreCase) == 0) != null;
            };

            return canLoad() || this.legacyContentLoader.CanLoad(targetUri, currentUri);
        }

        public virtual void CancelLoad(IAsyncResult asyncResult) {
        }

        public virtual IAsyncResult BeginLoad(Uri targetUri, Uri currentUri, AsyncCallback userCallback, object asyncState) {
            if(targetUri == null)
                throw new ArgumentNullException("targetUri");

            var uri = GetPathUri(targetUri);
            if(!BaseApp.Current.Container.IsRegistered<IRootView>(uri.OriginalString))
                return this.legacyContentLoader.BeginLoad(targetUri, currentUri, userCallback, asyncState);

            var result = new ContentLoaderAsyncResult(targetUri, asyncState);
            if(SynchronizationContext.Current != null)
                SynchronizationContext.Current.Post(args => Load(result, userCallback), null);
            else
                Deployment.Current.Dispatcher.BeginInvoke(() => Load(result, userCallback));
            return result;
        }

        public virtual LoadResult EndLoad(IAsyncResult asyncResult) {
            if(asyncResult == null)
                throw new ArgumentNullException("asyncResult");

            var result = asyncResult as ContentLoaderAsyncResult;
            if(result == null)
                return this.legacyContentLoader.EndLoad(asyncResult);

            if(result.Exception != null)
                throw result.Exception;

            return new LoadResult(result.Content);
        }
    }
}
