﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Markup;

[assembly: AssemblyTitle("Sunlight.Silverlight.Shell")]
[assembly: AssemblyCompany("上海晨阑数据技术有限公司")]
[assembly: AssemblyProduct("晨阑Silverlight产品平台")]
[assembly: AssemblyCopyright("版权所有(c) 上海晨阑数据技术有限公司")]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(true)]
[assembly: NeutralResourcesLanguage("zh-CN")]
[assembly: AssemblyVersion("2012.9.4")]
[assembly: XmlnsDefinition("http://www.sunlight.bz/silverlight/shell", "Sunlight.Silverlight.View", AssemblyName = "Sunlight.Silverlight.Shell")]
[assembly: XmlnsPrefix("http://www.sunlight.bz/silverlight/shell", "shell")]
[assembly: InternalsVisibleTo("Sunlight.Silverlight.Security, PublicKey=002400000480000094000000060200000024000052534131000400000100010085b31f4974563a92a155a14f5a6e6f6f8b36e8ba9b4829fa7df852bc6920d3e52efeddfd72b99328e50a1d8dca791cdc3fd8d6c991dd96496fd771c030ce58c7ba9ef95fda96456f2beebf79e628c4b3a412645a38684e59d5afda6e6868715a5adf60c4006a4af82760c29259922c709d5139aa6ed78ece253c1503fef801ae")]
[assembly: InternalsVisibleTo("Sunlight.Silverlight.Security.UI, PublicKey=002400000480000094000000060200000024000052534131000400000100010085b31f4974563a92a155a14f5a6e6f6f8b36e8ba9b4829fa7df852bc6920d3e52efeddfd72b99328e50a1d8dca791cdc3fd8d6c991dd96496fd771c030ce58c7ba9ef95fda96456f2beebf79e628c4b3a412645a38684e59d5afda6e6868715a5adf60c4006a4af82760c29259922c709d5139aa6ed78ece253c1503fef801ae")]
