﻿using System.Resources;
using System.Threading;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight {
    public class RadControlLocalizationManager : LocalizationManager {
        private readonly ResourceManager resourceManager;

        internal RadControlLocalizationManager() {
            this.resourceManager = new ResourceManager("Sunlight.Silverlight.Resources.RadControlStrings", typeof(RadControlLocalizationManager).Assembly);
        }

        public override string GetStringOverride(string key) {
            if(Thread.CurrentThread.CurrentUICulture.Name == "zh-CN")
                return this.resourceManager.GetString(key, null) ?? base.GetStringOverride(key);
            return base.GetStringOverride(key);
        }
    }
}
