﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Sunlight.Silverlight.Model;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight {
    public class RecentPageManager : INotifyPropertyChanged {
        private static string GetRecentMenuItemsFileName() {
            return string.Concat("RecentMenuItems.", BaseApp.Current.CurrentUserData.UserCode ?? "_", ".xml");
        }

        private readonly IEnumerable<SystemMenuItem> systemMenuItems;
        private int maxCount;
        private List<PageMenuItem> menuItems;
        private bool showQuickAccessButtonName;
        public event PropertyChangedEventHandler PropertyChanged;

        public int MaxCount {
            get {
                return this.maxCount;
            }
            set {
                if(value <= 0)
                    throw new ArgumentOutOfRangeException("value");
                this.maxCount = value;
            }
        }

        public IEnumerable<PageMenuItem> MenuItems {
            get {
                return this.menuItems ?? Enumerable.Empty<PageMenuItem>();
            }
        }

        private void RaisePropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SaveRecentMenuItems() {
            if(!IsolatedStorageFile.IsEnabled)
                return;

            var eMenuItems = new XElement("Items");
            foreach(var menuItem in this.menuItems)
                eMenuItems.Add(new XElement("Id", menuItem.Id));
            eMenuItems.Add(new XElement("ShowQuickAccessButtonName", ShowQuickAccessButtonName));
            try {
                using(var storage = IsolatedStorageFile.GetUserStoreForApplication()) {
                    var fileName = GetRecentMenuItemsFileName();
                    using(var stream = storage.OpenFile(fileName, FileMode.Create, FileAccess.Write))
                        eMenuItems.Save(stream, SaveOptions.DisableFormatting);
                }
            } catch(IsolatedStorageException) {
                // 用户在打开业务系统后，删除应用程序存储，避免因此造成的访问异常。
            }
        }

        private void RestoreRecentMenuItems() {
            if(!IsolatedStorageFile.IsEnabled)
                return;

            var eMenuItems = new XElement("Items");
            try {
                using(var storage = IsolatedStorageFile.GetUserStoreForApplication()) {
                    var fileName = GetRecentMenuItemsFileName();
                    if(!storage.FileExists(fileName))
                        return;
                    using(var stream = storage.OpenFile(fileName, FileMode.Open, FileAccess.Read))
                        eMenuItems = XElement.Load(stream);
                }
            } catch(XmlException) {
                // 加载 RecentMenuItems 文件的内容时，如解析失败，则不抛出异常。
            } catch(IsolatedStorageException) {
                // 用户在打开业务系统后，删除应用程序存储，避免因此造成的访问异常。
            }

            var ids = eMenuItems.Elements("Id").Select(e => int.Parse(e.Value));
            this.menuItems = new List<PageMenuItem>(from systemMenuItem in this.systemMenuItems
                                                    from groupMenuItem in systemMenuItem.GroupMenuItems
                                                    from pageMenuItem in groupMenuItem.PageMenuItems
                                                    where ids.Contains(pageMenuItem.Id)
                                                    select pageMenuItem);
            if(eMenuItems.Element("ShowQuickAccessButtonName") != null)
                Boolean.TryParse(eMenuItems.Element("ShowQuickAccessButtonName").Value, out this.showQuickAccessButtonName);
            this.RaisePropertyChanged("MenuItems");
        }

        public void Add(PageMenuItem menuItem) {
            if(this.menuItems == null)
                this.menuItems = new List<PageMenuItem>(this.MaxCount + 1);

            if(this.menuItems.Contains(menuItem))
                this.menuItems.Remove(menuItem);

            this.menuItems.Add(menuItem);
            while(this.menuItems.Count > this.MaxCount)
                this.menuItems.RemoveAt(0);

            this.SaveRecentMenuItems();
            this.RaisePropertyChanged("MenuItems");
        }

        /// <summary>
        /// 判断节点在最近访问中是否存在
        /// </summary>
        /// <param name="menuItem"></param>
        /// <returns></returns>
        public bool Contains(PageMenuItem menuItem) {
            return this.menuItems.Contains(menuItem);
        }

        public void Remove(PageMenuItem menuItem) {
            if(!this.menuItems.Remove(menuItem))
                return;

            this.SaveRecentMenuItems();
            this.RaisePropertyChanged("MenuItems");
        }

        internal RecentPageManager(IEnumerable<SystemMenuItem> systemMenuItems) {
            this.systemMenuItems = systemMenuItems;
            this.MaxCount = 5;
            this.RestoreRecentMenuItems();
        }

        /// <summary>
        /// 标记当前页面
        /// </summary>
        /// <param name="pageUri"></param>
        public void MarkCurrentPage(Uri pageUri) {
            var currentItem = MenuItems.First(m => m.PageUri == pageUri);
            MarkCurrentPage(currentItem);
        }

        /// <summary>
        /// 标记当前页面
        /// </summary>
        /// <param name="menuItem"></param>
        public void MarkCurrentPage(PageMenuItem menuItem) {
            menuItem.PageUri = ShellViewModel.Current.PageUri;
            foreach(var pageMenuItem in menuItems) {
                pageMenuItem.IsCurrent = menuItem == pageMenuItem;
            }
            this.RaisePropertyChanged("MenuItems");
        }

        /// <summary>
        /// 是否显示快捷访问按钮的名称
        /// </summary>
        public bool ShowQuickAccessButtonName {
            get {
                return this.showQuickAccessButtonName;
            }
            set {
                if(value == this.showQuickAccessButtonName)
                    return;
                this.showQuickAccessButtonName = value;
                SaveRecentMenuItems();
            }
        }
    }
}
