﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight {
    public class RootViewCacheManager {
        private sealed class CacheItem {
            public IRootView View {
                get;
                set;
            }

            public DateTime LastAccessTime {
                get;
                set;
            }
        }

        private static RootViewCacheManager current;

        public static RootViewCacheManager Current {
            get {
                return current ?? (current = new RootViewCacheManager());
            }
        }

        /// <summary>
        /// 最大缓存页面数
        /// </summary>
        public int MaxCacheSize {
            get {
                return maxCacheSize;
            }

            set {
                if(value > 5 || value < 1)
                    throw new ArgumentOutOfRangeException("缓存数量目前只能是1到5");
                maxCacheSize = value;
            }
        }

        private int maxCacheSize = 5;
        private readonly Dictionary<Uri, CacheItem> cache;

        private RootViewCacheManager() {
            this.cache = new Dictionary<Uri, CacheItem>(MaxCacheSize * 2);
        }

        public void Update(Uri uri, IRootView view, Action updated) {
            if(updated == null)
                throw new ArgumentNullException("updated");
            CacheItem item;
            if(this.cache.TryGetValue(uri, out item)) {
                item.View = view;
                item.LastAccessTime = DateTime.Now;
                updated();
            } else {
                Action addCache = () => {
                    item = new CacheItem {
                        View = view,
                        LastAccessTime = DateTime.Now,
                    };
                    this.cache.Add(uri, item);
                    updated();
                };

                if(this.cache.Count >= MaxCacheSize) {
                    var keyForRemove = this.cache.OrderBy(kv => kv.Value.LastAccessTime).First().Key;
                    this.Remove(keyForRemove, () => {
                        addCache();
                    }, () => {
                        ShellViewModel.Current.PageUri = keyForRemove;
                        ShellViewModel.Current.RecentPageManager.MarkCurrentPage(keyForRemove);
                    });
                } else
                    addCache();
            }
        }

        public IRootView Get(Uri uri) {
            CacheItem item;
            if(this.cache.TryGetValue(uri, out item)) {
                item.LastAccessTime = DateTime.Now;
                return item.View;
            }
            return null;
        }

        /// <summary>
        /// 移除缓存中的页面
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="removed">成功移除后的操作</param>
        /// <param name="userCanceled"></param>
        /// <returns></returns>
        public void Remove(Uri uri, Action removed, Action userCanceled = null) {
            Action removeCache = () => {
                if(cache.ContainsKey(uri))
                    cache.Remove(uri);
                removed.Invoke();
            };
            CacheItem item;
            if(!this.cache.TryGetValue(uri, out item)) {
                removed.Invoke();
                return;
            }
            var view = item.View as DataManagementViewBase;
            if(view != null && view.CheckEditViewHasEditingEntity()) {
                ShellUtils.Confirm(string.Format(ShellUIStrings.RootViewCacheManager_Remove, view.Title), removeCache, () => {
                    if(userCanceled != null)
                        userCanceled();
                });
            } else
                removeCache.Invoke();
        }
    }
}
