﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Caliburn.Micro;
using Microsoft.Practices.Unity;

namespace Sunlight.Silverlight {
    public class ShellBootstrapper : Bootstrapper {
        private static IUnityContainer container;

        private static IUnityContainer Container {
            get {
                return container ?? (container = BaseApp.Current.Container);
            }
        }

        internal ShellBootstrapper() : base(false) {
            //
        }

        protected override IEnumerable<Assembly> SelectAssemblies() {
            return BaseApp.Current.OwnAssemblies;
        }

        protected override object GetInstance(Type serviceType, string key) {
            return string.IsNullOrEmpty(key) ? Container.Resolve(serviceType) : Container.Resolve(serviceType, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type serviceType) {
            return Container.ResolveAll(serviceType);
        }

        protected override void Configure() {
            // Config container
            Container.RegisterType<IWindowManager, WindowManager>();
            Container.RegisterType<IEventAggregator, EventAggregator>();
            // Config conventions
            //ConventionManager.AddElementConvention<BusyIndicator>(BusyIndicator.IsBusyProperty, "IsBusy", "IsBusyChanged");

            //ViewLocator.NameTransformer.AddRule(
            //    @"(?<namespace>(.*\.)*)Model\.(?<basename>[A-Za-z]\w*)",
            //    @"${namespace}Views.${basename}",
            //    @"(.*\.)*Model\.[A-Za-z]\w*"
            //    );
        }
    }
}
