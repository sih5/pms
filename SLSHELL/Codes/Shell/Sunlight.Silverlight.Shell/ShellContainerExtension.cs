﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Practices.Unity;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight {
    internal class ShellContainerExtension : UnityContainerExtension {
        private static string StripTail(string input, string tail) {
            if(input == null)
                throw new ArgumentNullException("input");
            if(string.IsNullOrEmpty(tail))
                return input;
            return input == tail || !input.EndsWith(tail) ? input : input.Remove(input.Length - tail.Length);
        }

        private static string GetKey(Type type, string tail = null) {
            var attribute = type.GetCustomAttributes(typeof(DIKeyAttribute), false).Cast<DIKeyAttribute>().SingleOrDefault();
            if(attribute == null || string.IsNullOrWhiteSpace(attribute.Key))
                return StripTail(type.Name, tail);
            return attribute.Key;
        }

        protected override void Initialize() {
            foreach(var type in BaseApp.Current.OwnAssemblies.SelectMany(assembly => assembly.GetExportedTypes()).Where(t => t.IsClass && !t.IsAbstract && t.IsPublic && t.IsVisible))
                if(typeof(ISecurityManager).IsAssignableFrom(type))
                    this.Container.RegisterInstance((ISecurityManager)Activator.CreateInstance(type));
                else if(type.IsSubclassOf(typeof(DomainContext))) // DomainContext
                    this.Container.RegisterType(typeof(DomainContext), type, GetKey(type, "DomainContext"), new InjectionConstructor());
                else if(type.IsSubclassOf(typeof(QueryPanelBase))) // QueryPanel
                    this.Container.RegisterType(typeof(IQueryPanel), type, GetKey(type, "QueryPanel"));
                else if(type.IsSubclassOf(typeof(ActionPanelBase))) // ActionPanel
                    this.Container.RegisterType(typeof(IActionPanel), type, GetKey(type, "ActionPanel"));
                else if(typeof(IDetailPanel).IsAssignableFrom(type) && type.IsSubclassOf(typeof(FrameworkElement))) // DetailPanel
                    this.Container.RegisterType(typeof(IDetailPanel), type, GetKey(type, "DetailPanel"));
                else if(typeof(IDataEditPanel).IsAssignableFrom(type) && type.IsSubclassOf(typeof(FrameworkElement))) // DataEditPanel
                    this.Container.RegisterType(typeof(IDataEditPanel), type, GetKey(type, "DataEditPanel"));
                else if(type.IsSubclassOf(typeof(DataGridViewBase))) // DataGridView
                    this.Container.RegisterType(typeof(IDataGridView), type, GetKey(type, "DataGridView"));
                else if(type.IsSubclassOf(typeof(DataEditViewBase))) // DataEditView
                    this.Container.RegisterType(typeof(IDataEditView), type, GetKey(type, "DataEditView"));
                else if(type.IsSubclassOf(typeof(QueryWindowBase))) // QueryWindow
                    this.Container.RegisterType(typeof(IQueryWindow), type, GetKey(type, "QueryWindow"));
                else if(type.IsSubclassOf(typeof(WorkflowViewModelBase))) // WorkflowViewModel
                    this.Container.RegisterType(typeof(WorkflowViewModelBase), type, GetKey(type, "Workflow"));
                else if(typeof(IRootView).IsAssignableFrom(type)) {
                    var metadata = type.GetCustomAttributes(typeof(PageMetaAttribute), false).Cast<PageMetaAttribute>().SingleOrDefault();
                    if(metadata != null)
                        this.Container.RegisterType(typeof(IRootView), type, ShellUtils.GetFrameUriFromMenuItemName(metadata.System, metadata.Group, metadata.Page));
                } else if(typeof(IBaseView).IsAssignableFrom(type) && type.IsSubclassOf(typeof(UserControl)))
                    this.Container.RegisterType(typeof(IBaseView), type, GetKey(type));
        }
    }
}
