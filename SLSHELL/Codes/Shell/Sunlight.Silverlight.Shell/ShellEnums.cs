﻿using System;

namespace Sunlight.Silverlight.Shell {
    [Flags]
    public enum SystemTypes {
        Dms = 0x1,
        Dcs = 0x2
    }
}
