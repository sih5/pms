﻿using System;
using System.Collections.Generic;
using System.ServiceModel.DomainServices.Client;
using System.Text;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight {
    public static class ShellUtils {
        internal static string GetFrameUriFromMenuItemName(string systemName, string groupName, string pageName) {
            var result = new StringBuilder();
            if(!string.IsNullOrWhiteSpace(systemName)) {
                result.AppendFormat("/{0}", systemName.ToLower());
                if(!string.IsNullOrWhiteSpace(groupName)) {
                    if(string.Compare(systemName, groupName, StringComparison.CurrentCultureIgnoreCase) != 0)
                        result.AppendFormat("/{0}", groupName.ToLower());
                    if(!string.IsNullOrWhiteSpace(pageName))
                        result.AppendFormat("/{0}", pageName.ToLower());
                }
            }
            return result.ToString();
        }

        internal static string[] GetMenuItemNamesFromFrameUri(string uri) {
            if(string.IsNullOrEmpty(uri))
                return null;
            var names = uri.Split(new[] {
                '/'
            }, StringSplitOptions.RemoveEmptyEntries);
            if(names.Length == 0 || names.Length > 3)
                return null;
            if(names.Length == 3)
                return names;
            return names.Length == 2 ? new[] {
                names[0], names[0], names[1]
            } : new[] {
                names[0], string.Empty, string.Empty
            };
        }

        internal static void ChangeHtmlTitle(string title) {
            if(!HtmlPage.IsEnabled)
                return;

            var window = HtmlPage.Window.GetProperty("top") as HtmlWindow;
            if(window == null)
                return;

            var document = window.GetProperty("document") as HtmlDocument;
            if(document != null)
                document.SetProperty("title", title);
        }

        public static void ChangeHtmlTitle() {
            var userData = WebContext.Current.User.Identity.Name.Split(',');
            ChangeHtmlTitle(string.Format("{0}({1}) - {2}", userData[2], userData[4], ((BaseApp)Application.Current).ApplicationName));
        }

        internal static Dictionary<string, object> GetCurrentEnvironmentProperties() {
            var result = new Dictionary<string, object>(16);
            result.Add("Time", DateTime.Now.ToString("F"));
            var app = Application.Current;
            result.Add("App.IsRunningOutOfBrowser", app.IsRunningOutOfBrowser);
            result.Add("App.InstallState", app.InstallState);
            var host = app.Host;
            result.Add("Host.Source", host.Source.GetComponents(UriComponents.SchemeAndServer, UriFormat.SafeUnescaped));
            result.Add("Host.Path", host.Source.LocalPath);
            result.Add("Host.NavigationState", host.NavigationState);
            var settings = host.Settings;
            result.Add("Settings.EnableAutoZoom", settings.EnableAutoZoom);
            result.Add("Settings.EnableCacheVisualization", settings.EnableCacheVisualization);
            result.Add("Settings.EnableFrameRateCounter", settings.EnableFrameRateCounter);
            result.Add("Settings.EnableGPUAcceleration", settings.EnableGPUAcceleration);
            result.Add("Settings.EnableHTMLAccess", settings.EnableHTMLAccess);
            result.Add("Settings.EnableRedrawRegions", settings.EnableRedrawRegions);
            var content = host.Content;
            result.Add("Content.IsFullScreen", content.IsFullScreen);
            result.Add("Content.ActualWidth", content.ActualWidth);
            result.Add("Content.ActualHeight", content.ActualHeight);
            result.Add("Content.ZoomFactor", content.ZoomFactor);
            return result;
        }

        internal static bool IsSessionExpiredException(Exception ex) {
            var exception = ex;
            while(exception != null && !(exception is DomainOperationException))
                exception = exception.InnerException;

            var domainOperationException = exception as DomainOperationException;
            if(domainOperationException == null || String.IsNullOrEmpty(domainOperationException.Message))
                return false;

            return domainOperationException.ErrorCode == 401;
        }

        internal static void ShowDomainServiceOperationWindow(object context) {
            (new RadWindow {
                Header = ((BaseApp)Application.Current).ApplicationName,
                WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                IsTopmost = true,
                ResizeMode = ResizeMode.NoResize,
                CanClose = true,
                CanMove = true,
                Content = new DomainServiceOperationView {
                    Margin = new Thickness(10),
                    DataContext = context,
                },
            }).ShowDialog();
        }

        public static void Confirm(string text, Action action) {
            if(action == null)
                throw new ArgumentNullException("action");

            var parameters = new DialogParameters {
                Header = BaseApp.Current.ApplicationName,
                Content = text,
            };
            parameters.Closed += (sender, args) => {
                if(args.DialogResult.HasValue && args.DialogResult.Value)
                    action();
            };
            RadWindow.Confirm(parameters);
        }

        public static void Confirm(string text, Action yesAction, Action noAction) {
            if(yesAction == null)
                throw new ArgumentNullException("yesAction");
            if(noAction == null)
                throw new ArgumentNullException("noAction");
            var parameters = new DialogParameters {
                Header = BaseApp.Current.ApplicationName,
                Content = text,
            };
            parameters.Closed += (sender, args) => {
                if(args.DialogResult.HasValue && args.DialogResult.Value)
                    yesAction();
                else
                    noAction();
            };
            RadWindow.Confirm(parameters);
        }
    }
}
