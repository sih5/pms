﻿using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using Microsoft.Practices.Unity;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight {
    public class TabControlRowDetailsTemplateProvider : IRowDetailsTemplateProvider {
        public virtual IEnumerable<string> DetailPanelNames {
            get;
            set;
        }

        public virtual DataTemplate CreateTemplate() {
            var sbTemplate = new StringBuilder();
            sbTemplate.Append("<DataTemplate xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\" xmlns:telerik=\"http://schemas.telerik.com/2008/xaml/presentation\" xmlns:sds=\"clr-namespace:Sunlight.Silverlight.Controls;assembly=Sunlight.Silverlight.Controls\">");
            sbTemplate.Append("<telerik:RadTabControl BackgroundVisibility=\"Collapsed\" Margin=\"24,0,4,4\" Background=\"Transparent\" HorizontalAlignment=\"Left\" />");
            sbTemplate.Append("</DataTemplate>");
            return (DataTemplate)XamlReader.Load(sbTemplate.ToString());
        }

        public virtual void OnInitializing(FrameworkElement rootElement) {
            var tabControl = rootElement as RadTabControl;
            if(tabControl == null || this.DetailPanelNames == null)
                return;

            tabControl.Items.Clear();

            var counter = 1;
            var container = BaseApp.Current.Container;
            foreach(var detailPanelName in this.DetailPanelNames) {
                var detailPanel = container.Resolve<IDetailPanel>(detailPanelName);
                var detailPanelView = detailPanel as UIElement;
                if(detailPanelView == null)
                    continue;

                var tabItem = new RadTabItem {
                    Margin = new Thickness(counter++ == 1 ? 8 : 4, 0, 0, 0),
                    Content = new Border {
                        Padding = new Thickness(2),
                        Child = detailPanelView,
                    }
                };
                if(detailPanel.Icon == null)
                    tabItem.Header = detailPanel.Title;
                else
                    tabItem.Header = BusinessViewUtils.MakeTabHeader(detailPanel.Icon, detailPanel.Title);
                tabControl.Items.Add(tabItem);
            }
        }
    }
}
