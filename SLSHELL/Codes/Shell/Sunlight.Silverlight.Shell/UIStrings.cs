﻿using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight {
    public class UIStrings : ModelBase {
        public void Refresh(params string[] propertyNames) {
            this.NotifyOfPropertiesChange(propertyNames.Length == 0 ? this.GetType().GetProperties().Select(p => p.Name).ToArray() : propertyNames);
        }

        public string ExceptionView_ErrorMessage {
            get {
                return Resources.ShellUIStrings.ExceptionView_ErrorMessage;
            }
        }

        public string ExceptionView_ShowDetailMessages {
            get {
                return Resources.ShellUIStrings.ExceptionView_ShowDetailMessages;
            }
        }

        public string MenuContentView_NavigationText {
            get {
                return Resources.ShellUIStrings.MenuContentView_NavigationText;
            }
        }

        public string MenuContentView_PageMenuText {
            get {
                return Resources.ShellUIStrings.MenuContentView_PageMenuText;
            }
        }

        public string PageMenuView_SearchBoxPromptText {
            get {
                return Resources.ShellUIStrings.PageMenuView_SearchBoxPromptText;
            }
        }

        public string QueryView_Button_ExecuteQuery {
            get {
                return Resources.ShellUIStrings.QueryView_Button_ExecuteQuery;
            }
        }

    }
}
