﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Core.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.View {
    public abstract class ActionPanelBase : ActionPanelRibbonGroup, IActionPanel {
        private bool uiLoaded;
        private ViewInitializer initializer;
        private Dictionary<string, bool> pendingIsEnabledChanges;

        protected ViewInitializer Initializer {
            get {
                return this.initializer ?? (this.initializer = new ViewInitializer(this));
            }
        }

        internal bool IsLoaded {
            get;
            private set;
        }

        private RadRibbonButton GetButton(string uniqueId) {
            //return this.Items.OfType<RadRibbonButton>().SingleOrDefault(button => string.Compare(button.Tag as string, uniqueId, StringComparison.InvariantCultureIgnoreCase) == 0);
            return this.Items.OfType<RadCollapsiblePanel>().First().ChildrenOfType<RadRibbonButton>().SingleOrDefault(button => string.Compare(button.Tag as string, uniqueId, StringComparison.InvariantCultureIgnoreCase) == 0);
        }

        private void SetIsEnabled(string buttonUniqueId, bool value) {
            var button = this.GetButton(buttonUniqueId);
            if(button != null)
                button.IsEnabled = value;
        }

        protected override void OnUIReloading() {
            this.uiLoaded = false;
        }

        protected override void OnUIReloaded() {
            this.uiLoaded = true;
            if(this.pendingIsEnabledChanges != null)
                foreach(var change in this.pendingIsEnabledChanges)
                    this.SetIsEnabled(change.Key, change.Value);
            this.pendingIsEnabledChanges = null;
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            switch(subject) {
                case "SetIsEnabled":
                    if(contents.Length < 2)
                        throw new ArgumentException(string.Format("SetIsEnabled 协定需要 2 个参数，目前仅提供了 {0} 个", contents.Length));
                    if(this.uiLoaded)
                        this.SetIsEnabled((string)contents[0], (bool)contents[1]);
                    else {
                        if(this.pendingIsEnabledChanges == null)
                            this.pendingIsEnabledChanges = new Dictionary<string, bool>();
                        this.pendingIsEnabledChanges[(string)contents[0]] = (bool)contents[1];
                    }
                    break;
            }
            return null;
        }

        protected ActionPanelBase() {
            this.Loaded += (sender, args) => this.IsLoaded = true;
            this.Unloaded += (sender, args) => this.IsLoaded = false;
        }
    }
}
