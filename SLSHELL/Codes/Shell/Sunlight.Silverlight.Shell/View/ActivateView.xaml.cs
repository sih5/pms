﻿using System;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Sunlight.Silverlight.View {
    public partial class ActivateView {
        private bool success;

        private bool Success {
            get {
                return this.success;
            }
            set {
                this.success = value;
                this.ActionButton.Content = this.success ? "重新载入系统" : "激活产品";
            }
        }

        private void SetResult(bool success, string errorMessage) {
            this.Success = success;
            this.MessageText.Foreground = new SolidColorBrush(success ? Color.FromArgb(0xFF, 0x0, 0xC0, 0x0) : Color.FromArgb(0xFF, 0xC0, 0x0, 0x0));
            this.MessageText.Text = errorMessage;
            this.Icon.Source = new BitmapImage(new Uri(string.Format("/Sunlight.Silverlight.Shell;component/Images/{0}.png", success ? "check" : "cross"), UriKind.Relative));
            this.ResultPanel.Visibility = Visibility.Visible;
        }

        private void ActionButton_Click(object sender, RoutedEventArgs e) {
            if(this.Success) {
                if(HtmlPage.IsEnabled)
                    HtmlPage.Window.Navigate(new Uri("/", UriKind.Relative));
            } else {
                this.MainBusyIndicator.IsBusy = true;
                var client = new LicenseServiceClient();
                client.ActivateCompleted += (o, args) => {
                    try {
                        if(args.Cancelled)
                            this.SetResult(false, "激活请求被取消");
                        else if(args.Error != null)
                            this.SetResult(false, args.Error.Message);
                        else if(args.Result != null)
                            this.SetResult(false, args.Result);
                        else
                            this.SetResult(true, "感谢您，产品已激活成功，请重新载入本系统");
                    } finally {
                        this.MainBusyIndicator.IsBusy = false;
                    }
                };
                //client.ActivateAsync(this.KeyText.MaskedText.Replace(" ", null).Replace("-", null));
            }
        }

        public ActivateView() {
            this.InitializeComponent();
            this.Success = false;
        }
    }
}
