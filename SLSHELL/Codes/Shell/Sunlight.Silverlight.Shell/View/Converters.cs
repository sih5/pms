﻿using System;
using System.Globalization;
using System.Windows.Data;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.View {
    public sealed class LanguageToRadioButtonConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if(value == null || parameter == null)
                return false;
            return value.ToString() == parameter.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            UILanguage language;
            if((bool)value && Enum.TryParse((string)parameter, true, out language))
                return language;
            return null;
        }
    }
}
