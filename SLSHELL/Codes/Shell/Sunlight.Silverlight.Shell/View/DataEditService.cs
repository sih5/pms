﻿using System.Windows;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.View {
    public static class DataEditService {
        public static readonly DependencyProperty HideWhenProperty = DependencyProperty.RegisterAttached("HideWhen", typeof(DataEditState), typeof(DataEditService), new PropertyMetadata((o, args) => RegisterEditStateSensitiveAs("HideWhen", o as FrameworkElement)));
        public static readonly DependencyProperty DisableWhenProperty = DependencyProperty.RegisterAttached("DisableWhen", typeof(DataEditState), typeof(DataEditService), new PropertyMetadata((o, args) => RegisterEditStateSensitiveAs("DisableWhen", o as FrameworkElement)));

        private static bool TryRegisterAsHideWhen(FrameworkElement element) {
            var dataEditView = element.GetVisualParent<DataEditViewBase>();
            if(dataEditView == null)
                return false;
            dataEditView.RegisterHideWhenElement(element);
            return true;
        }

        private static bool TryRegisterAsDisableWhen(FrameworkElement element) {
            var dataEditView = element.GetVisualParent<DataEditViewBase>();
            if(dataEditView == null)
                return false;
            dataEditView.RegisterDisableWhenElement(element);
            return true;
        }

        private static void OnElementAsHideWhenLoaded(object sender, RoutedEventArgs e) {
            var element = sender as FrameworkElement;
            if(element != null && TryRegisterAsHideWhen(element))
                element.Loaded -= OnElementAsHideWhenLoaded;
        }

        private static void OnElementAsDisableWhenLoaded(object sender, RoutedEventArgs e) {
            var element = sender as FrameworkElement;
            if(element != null && TryRegisterAsDisableWhen(element))
                element.Loaded -= OnElementAsDisableWhenLoaded;
        }

        private static void RegisterEditStateSensitiveAs(string type, FrameworkElement element) {
            if(element == null)
                return;
            switch(type) {
                case "HideWhen":
                    element.Loaded -= OnElementAsHideWhenLoaded;
                    if(!TryRegisterAsHideWhen(element))
                        element.Loaded += OnElementAsHideWhenLoaded;
                    break;
                case "DisableWhen":
                    element.Loaded -= OnElementAsDisableWhenLoaded;
                    if(!TryRegisterAsHideWhen(element))
                        element.Loaded += OnElementAsDisableWhenLoaded;
                    break;
            }
        }

        public static DataEditState GetHideWhen(DependencyObject obj) {
            return (DataEditState)obj.GetValue(HideWhenProperty);
        }

        public static void SetHideWhen(DependencyObject obj, DataEditState value) {
            obj.SetValue(HideWhenProperty, value);
        }

        public static DataEditState GetDisableWhen(DependencyObject obj) {
            return (DataEditState)obj.GetValue(DisableWhenProperty);
        }

        public static void SetDisableWhen(DependencyObject obj, DataEditState value) {
            obj.SetValue(DisableWhenProperty, value);
        }
    }
}
