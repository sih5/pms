﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Resources;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.View {
    [Flags]
    public enum DataEditState {
        New = 1,
        Edit = 2,
    }

    public abstract class DataEditViewBase : UserControlBase, IDataEditView, IWeakEventListener, IDelayCall {
        private ViewInitializer initializer;
        private DataEditState editState;
        private DomainContext domainContext;
        private List<FrameworkElement> hideWhenElements, disableWhenElements;
        private List<ValidationError> validationErrors;
        private DelegateCommand submitCommand, cancelCommand;
        public event EventHandler EditSubmitted, EditCancelled;

        protected ViewInitializer Initializer {
            get {
                return this.initializer ?? (this.initializer = new ViewInitializer(this));
            }
        }

        protected DomainContext DomainContext {
            get {
                return this.domainContext;
            }
            set {
                this.domainContext = value;
                this.EnsureEntityAdded();
            }
        }

        protected Entity ObjectToAdd {
            get;
            private set;
        }

        protected Entity ObjectToEdit {
            get;
            private set;
        }

        /// <summary>
        /// 是否显示Cancel提示框
        /// </summary>
        protected virtual bool ShowCancelNotification {
            get {
                return true;
            }
        }

        protected ICommand SubmitCommand {
            get {
                return this.submitCommand ?? (this.submitCommand = new DelegateCommand(this.ExecuteSubmitCommand, this.CanSubmitCommandExecute));
            }
        }

        protected ICommand CancelCommand {
            get {
                return this.cancelCommand ?? (this.cancelCommand = new DelegateCommand(this.ExecuteCancelCommand, this.CanCancelCommandExecute));
            }
        }

        public DataEditState EditState {
            get {
                return this.editState;
            }
            private set {
                if(this.editState == value)
                    return;
                this.editState = value;
                this.OnEditStateChangedInternal();
            }
        }

        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        private void NotifyEditCancelled() {
            var handler = this.EditCancelled;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        private bool IsDomainContextBusy() {
            return this.DomainContext != null && (this.DomainContext.IsLoading || this.DomainContext.IsSubmitting);
        }

        private bool CanSubmitCommandExecute(object parameter) {
            if(this.DomainContext == null)
                return false;
            if(this.IsDomainContextBusy() || (this.validationErrors != null && this.validationErrors.Any())
                || this.ChildrenOfType<DataGridViewBase>().Any(view => view.HasErrors))
                return false;
            return this.OnRequestCanSubmit();
        }

        private bool CanCancelCommandExecute(object parameter) {
            return this.DomainContext == null || !this.IsDomainContextBusy();
        }

        private void ExecuteSubmitCommand(object parameter) {
            this.OnEditSubmitting();
        }

        private void ExecuteCancelCommand(object parameter) {
            Action editCanceling = () => {
                if(this.validationErrors != null)
                    this.validationErrors.Clear();
                foreach(var gridView in this.ChildrenOfType<RadGridView>())
                    gridView.CancelEdit();
                this.DomainContext.RejectChanges();
                this.OnEditCancelled();
                this.NotifyEditCancelled();
            };

            if(this.DomainContext != null && this.DomainContext.HasChanges && this.ShowCancelNotification) {
                var dialogParameters = new DialogParameters {
                    Header = BaseApp.Current.ApplicationName,
                    Content = ShellUIStrings.DataEditViewBase_ConfirmCancel,
                };
                dialogParameters.Closed += (sender, args) => {
                    if(args.DialogResult == null || !args.DialogResult.Value)
                        return;
                    editCanceling();
                };
                RadWindow.Confirm(dialogParameters);
            } else {
                editCanceling();
            }
        }

        private void SetElementVisibilityByEditState(UIElement element) {
            var hideWhen = DataEditService.GetHideWhen(element);
            element.Visibility = (hideWhen & this.EditState) == this.EditState ? Visibility.Collapsed : Visibility.Visible;
        }

        private void SetElementIsEnabledByEditState(DependencyObject element) {
            var propertyInfo = element.GetType().GetProperty("IsEnabled");
            if(propertyInfo == null || propertyInfo.PropertyType != typeof(bool))
                return;
            var disableWhen = DataEditService.GetDisableWhen(element);
            propertyInfo.SetValue(element, (disableWhen & this.EditState) != this.EditState, null);
        }

        private void SetAllElementsByEditState() {
            if(this.hideWhenElements != null)
                foreach(var element in this.hideWhenElements)
                    this.SetElementVisibilityByEditState(element);
            if(this.disableWhenElements != null)
                foreach(var element in this.disableWhenElements)
                    this.SetElementIsEnabledByEditState(element);
        }

        private void OnEditStateChangedInternal() {
            this.SetAllElementsByEditState();
            this.OnEditStateChanged();
        }

        private void EnsureEntityAdded() {
            if(this.DomainContext == null || this.ObjectToAdd == null)
                return;

            EntitySet entitySet;
            if(!this.DomainContext.EntityContainer.TryGetEntitySet(this.ObjectToAdd.GetType(), out entitySet))
                return;

            if(entitySet.Cast<Entity>().Contains(this.ObjectToAdd))
                return;

            if(this.ObjectToAdd.EntityState == EntityState.Detached)
                entitySet.Add(this.ObjectToAdd);
        }

        private void Initialize() {
            if(this.DomainContext == null)
                this.DomainContext = DI.GetDomainContextByType(this.GetType());
            PropertyChangedEventManager.RemoveListener(this.DomainContext, this, string.Empty);
            PropertyChangedEventManager.AddListener(this.DomainContext, this, string.Empty);
            this.OnEditStateChangedInternal();
        }

        private void DataEditViewBase_BindingValidationError(object sender, ValidationErrorEventArgs e) {
            if(this.validationErrors == null)
                this.validationErrors = new List<ValidationError>();
            if(e.Action == ValidationErrorEventAction.Added) {
                if(!this.validationErrors.Contains(e.Error))
                    this.validationErrors.Add(e.Error);
            } else
                this.validationErrors.Remove(e.Error);
            this.NotifyCommandsCanExecuteChanged();
        }

        internal void RegisterHideWhenElement(FrameworkElement element) {
            if(this.hideWhenElements == null)
                this.hideWhenElements = new List<FrameworkElement>();
            if(!this.hideWhenElements.Contains(element))
                this.hideWhenElements.Add(element);
            this.SetElementVisibilityByEditState(element);
        }

        internal void RegisterDisableWhenElement(FrameworkElement element) {
            if(this.disableWhenElements == null)
                this.disableWhenElements = new List<FrameworkElement>();
            if(!this.disableWhenElements.Contains(element))
                this.disableWhenElements.Add(element);
            this.SetElementIsEnabledByEditState(element);
        }

        protected DataEditViewBase() {
            this.BindingValidationError += this.DataEditViewBase_BindingValidationError;
            this.editState = DataEditState.New;
            this.CallEntryList = new List<CallEntry>();
            this.Initializer.Register(this.Initialize);
        }

        protected virtual bool OnRequestCanSubmit() {
            return this.DomainContext.HasChanges;
        }

        protected virtual void OnEditStateChanged() {
        }

        protected virtual void OnEditSubmitting() {
            this.DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    ShellUtils.ShowDomainServiceOperationWindow(submitOp);
                    return;
                }
                this.OnEditSubmitted();
                this.NotifyEditSubmitted();
            }, null);
        }

        protected virtual void OnEditSubmitted() {
        }

        protected virtual void OnEditCancelled() {
        }

        protected void NotifyCommandsCanExecuteChanged() {
            if(this.submitCommand != null)
                this.submitCommand.InvalidateCanExecute();
            if(this.cancelCommand != null)
                this.cancelCommand.InvalidateCanExecute();
        }

        protected void SetObjectToEdit(Entity entity) {
            if(entity == null)
                throw new ArgumentNullException("entity");
            this.DataContext = this.ObjectToEdit = entity;
            this.EditState = DataEditState.Edit;
        }

        public virtual void SetObjectToEditById(object id) {
        }

        public TEntity CreateObjectToEdit<TEntity>() where TEntity : Entity, new() {
            var entity = new TEntity();
            this.DataContext = this.ObjectToAdd = entity;
            this.EditState = DataEditState.New;
            this.EnsureEntityAdded();
            return entity;
        }

        public virtual object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new NotSupportedException();
        }

        public void Cancel() {
            if(this.DomainContext != null && this.DomainContext.HasChanges)
                this.DomainContext.RejectChanges();
        }

        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e) {
            if(managerType == typeof(PropertyChangedEventManager)) {
                if(sender is DomainContext) {
                    this.NotifyCommandsCanExecuteChanged();
                }
            } else
                return false;
            return true;
        }

        public List<CallEntry> CallEntryList {
            get;
            private set;
        }

        /// <summary>
        /// 当前页面有没有编辑中的Entity
        /// </summary>
        /// <returns></returns>
        public virtual bool HasEditingEntity() {
            return this.DomainContext != null && this.DomainContext.HasChanges;
        }
    }
}
