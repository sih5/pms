﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Microsoft.Practices.Unity;
using Sunlight.Silverlight.Commands;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Resources;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.ViewModel.Contract;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls.TreeListView;

namespace Sunlight.Silverlight.View {
    public partial class DataGridView : IGeneralView, ICustomTabsView {
        private static UIElement MakeTabHeader(Uri imageUri, string title) {
            if(imageUri == null)
                throw new ArgumentNullException("imageUri");
            if(title == null)
                throw new ArgumentNullException("title");

            var result = new StackPanel {
                Orientation = Orientation.Horizontal
            };
            result.Children.Add(new Image {
                Source = new BitmapImage(imageUri),
                Stretch = Stretch.None,
            });
            result.Children.Add(new TextBlock {
                Margin = new Thickness(4, 0, 0, 0),
                Text = title,
            });
            return result;
        }

        private bool initialized, isAutoLoaded, isOperationViewsLoaded;
        private IDataGridViewModel viewModel;
        private QueryView[] queryViews;
        private OperationView[] operationViews;
        private RadRibbonTab[] ribbonTabs;
        private EntitySet entitySet;

        private EntitySet EntitySet {
            get {
                if(this.ViewModel == null)
                    return null;
                return this.entitySet ?? (this.entitySet = this.ViewModel.DomainContext.EntityContainer.GetEntitySet(this.ViewModel.EntityType));
            }
        }

        private IEnumerable<QueryView> QueryViews {
            get {
                if(this.ViewModel.QueryViewModels == null)
                    return Enumerable.Empty<QueryView>();
                return this.queryViews ?? (this.queryViews = this.ViewModel.QueryViewModels.Select(queryViewModel => new QueryView {
                    ViewModel = queryViewModel
                }).ToArray());
            }
        }

        internal GridViewDataControl MainGridView {
            get;
            set;
        }

        internal bool CanAddEntity {
            get {
                return this.EntitySet != null && this.EntitySet.CanAdd;
            }
        }

        internal bool CanRemoveEntity {
            get {
                return this.EntitySet != null && this.EntitySet.CanRemove;
            }
        }

        internal bool CanEditEntity {
            get {
                return this.EntitySet != null && this.EntitySet.CanEdit;
            }
        }

        public string PageTitle {
            get {
                return this.ViewModel != null ? this.ViewModel.PageTitle : string.Empty;
            }
        }

        public bool IsTabsReady {
            get {
                return this.ViewModel != null && this.isOperationViewsLoaded;
            }
        }

        public string RibbonContextualName {
            get;
            set;
        }

        public IEnumerable<RadRibbonTab> CustomTabs {
            get {
                if(!this.IsTabsReady)
                    return Enumerable.Empty<RadRibbonTab>();

                if(this.ribbonTabs == null) {
                    var tabs = new List<RadRibbonTab>();

                    RadRibbonGroup[] ribbonGroups;

                    if(!this.ViewModel.HideQueryPanel) {
                        ribbonGroups = this.QueryViews.Select(queryView => {
                            var ribbonGroup = new RadRibbonGroup {
                                Header = queryView.ViewModel.Name,
                            };
                            ribbonGroup.Items.Add(queryView);
                            return ribbonGroup;
                        }).ToArray();
                        if(ribbonGroups.Length > 0) {
                            var tab = new RadRibbonTab {
                                Header = MakeTabHeader(new Uri("/Sunlight.Silverlight.Shell;component/Images/tab-query.png", UriKind.Relative), ShellUIStrings.DataGridView_TabText_Querying),
                            };
                            foreach(var ribbonGroup in ribbonGroups)
                                tab.Items.Add(ribbonGroup);
                            tabs.Add(tab);
                        }
                    }

                    if(this.operationViews != null && this.operationViews.Any()) {
                        ribbonGroups = this.operationViews.Select(operationView => {
                            var ribbonGroup = new RadRibbonGroup {
                                Header = operationView.ViewModel.Name,
                            };
                            ribbonGroup.Items.Add(operationView);
                            return ribbonGroup;
                        }).ToArray();
                        if(ribbonGroups.Length > 0) {
                            var tab = new RadRibbonTab {
                                Header = MakeTabHeader(new Uri("/Sunlight.Silverlight.Shell;component/Images/tab-actions.png", UriKind.Relative), ShellUIStrings.DataGridView_TabText_Operations),
                            };
                            foreach(var ribbonGroup in ribbonGroups)
                                tab.Items.Add(ribbonGroup);
                            tabs.Add(tab);
                        }
                    }

                    this.ribbonTabs = tabs.ToArray();
                }

                return this.ribbonTabs;
            }
        }

        public IDataGridViewModel ViewModel {
            get {
                return this.viewModel;
            }
            set {
                if(ReferenceEquals(this.viewModel, value))
                    return;

                if(this.viewModel != null) {
                    this.viewModel.PropertyChanged -= this.ViewModel_PropertyChanged;
                    this.viewModel.DomainContext.PropertyChanged -= this.DomainContext_PropertyChanged;
                }
                this.DataContext = this.viewModel = value;
                this.viewModel.PropertyChanged += this.ViewModel_PropertyChanged;
                this.viewModel.DomainContext.PropertyChanged += this.DomainContext_PropertyChanged;

                if(this.viewModel.OperationGroupViewModels != null)
                    foreach(var builtInOperationViewModel in this.viewModel.OperationGroupViewModels.SelectMany(groupViewModel => groupViewModel.OperationViewModels).OfType<BuiltInOperationViewModel>())
                        builtInOperationViewModel.CommandParameter = this;

                this.Reset();
            }
        }

        private void Reset() {
            this.initialized = false;
            this.isAutoLoaded = false;
            this.queryViews = null;
            this.ribbonTabs = null;
        }

        private void GetSecurityData() {
            var id = this.GetPageId();
            if(id == 0 || this.ViewModel.OperationGroupViewModels == null) {
                this.isOperationViewsLoaded = true;
                return;
            }

            Action createOperationViews = () => {
                this.operationViews = this.ViewModel.OperationGroupViewModels.Select(vm => new OperationView {
                    ViewModel = vm
                }).ToArray();
                this.isOperationViewsLoaded = true;
            };

            //判断是否需要获取动作条权限数据
            if(this.ViewModel.OperationGroupViewModels.SelectMany(vm => vm.OperationViewModels).Any(vm => !string.IsNullOrWhiteSpace(vm.UniqueId))) {
                var securityManager = BaseApp.Current.Container.Resolve<ISecurityManager>();
                securityManager.GetPageActions(id, result => {
                    var securityActionResult = (ISecurityActionResult)result;
                    if(!securityActionResult.IsSuccess)
                        securityActionResult.IsErrorHandled = true;
                    var authorizedActions = securityActionResult.IsSuccess ? (string[])securityActionResult["Value"] : new string[0];
                    foreach(var operationViewModel in this.ViewModel.OperationGroupViewModels.SelectMany(vm => vm.OperationViewModels).Where(vm => !string.IsNullOrWhiteSpace(vm.UniqueId)))
                        operationViewModel.IsAuthorized = authorizedActions.Contains(operationViewModel.UniqueId);
                    createOperationViews();
                });
            } else
                createOperationViews();
        }

        private void CreateContextMenu() {
            // ReSharper disable PossibleMultipleEnumeration
            var operationGroupViewModels = this.ViewModel.OperationGroupViewModels;
            if(operationGroupViewModels == null || !operationGroupViewModels.Any())
                return;

            var contextMenu = new RadContextMenu();
            RadContextMenu.SetContextMenu(this.MainGridView, contextMenu);

            Action addSeparator = () => {
                if(contextMenu.HasItems)
                    contextMenu.Items.Add(new RadMenuItem {
                        IsSeparator = true,
                    });
            };

            Func<Uri, Image> createIcon = uri => new Image {
                Source = new BitmapImage(uri),
                Stretch = Stretch.Fill,
                Width = 16,
                Height = 16,
            };

            foreach(var operationGroupViewModel in operationGroupViewModels) {
                addSeparator();
                foreach(var operationViewModel in operationGroupViewModel.OperationViewModels) {
                    var menuItem = new RadMenuItem {
                        Header = operationViewModel.Name,
                        Command = operationViewModel.ExecuteCommand,
                        CommandParameter = operationViewModel is BuiltInOperationViewModel ? ((BuiltInOperationViewModel)operationViewModel).CommandParameter : null,
                    };
                    if(operationViewModel.ImageUri != null)
                        menuItem.Icon = createIcon(operationViewModel.ImageUri);
                    contextMenu.Items.Add(menuItem);
                }
            }
            // ReSharper restore PossibleMultipleEnumeration
        }

        private void AutoLoadData() {
            if(this.isAutoLoaded)
                return;

            var queryView = this.QueryViews.FirstOrDefault();
            if(queryView != null)
                queryView.ExecuteQuery(true);

            this.isAutoLoaded = true;
        }

        private void InitializeGridView() {
            if(string.IsNullOrWhiteSpace(this.ViewModel.TreeChildrenColumnName))
                this.MainGridView = new RadGridView();
            else {
                this.MainGridView = new RadTreeListView();
                this.MainGridView.ChildTableDefinitions.Add(new TreeListViewTableDefinition {
                    ItemsSource = new Binding(this.ViewModel.TreeChildrenColumnName),
                });
            }

            this.MainGridView.FontSize = 12;
            this.MainGridView.ClipboardCopyMode = GridViewClipboardCopyMode.Cells;
            this.MainGridView.ClipboardPasteMode = GridViewClipboardPasteMode.None;
            this.MainGridView.SelectionMode = System.Windows.Controls.SelectionMode.Extended;
            this.MainGridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.MainGridView.AutoGenerateColumns = false;
            this.MainGridView.CanUserFreezeColumns = false;
            this.MainGridView.CanUserReorderColumns = true;
            this.MainGridView.CanUserResizeColumns = false;
            this.MainGridView.CanUserSortColumns = true;
            this.MainGridView.RowDetailsVisibilityMode = GridViewRowDetailsVisibilityMode.Collapsed;
            this.MainGridView.EnableRowVirtualization = false;
            this.MainGridView.EnableColumnVirtualization = false;
            //this.MainGridView.RowIndicatorVisibility = Visibility.Collapsed;
            this.MainGridView.SetBinding(GridViewDataControl.IsReadOnlyProperty, new Binding("IsReadOnly"));
            this.MainGridView.SetBinding(GridViewDataControl.ShowGroupPanelProperty, new Binding("IsGroupable"));
            this.MainGridView.SetBinding(DataControl.SelectedItemProperty, new Binding("SelectedItem") {
                Mode = BindingMode.TwoWay
            });
            this.MainGridView.SetBinding(DataControl.ItemsSourceProperty, new Binding("PagedSource") {
                Source = this.MainDataPager
            });
            this.MainGridView.SelectionChanged += this.MainGridView_SelectionChanged;
            this.MainGridView.MouseRightButtonDown += this.MainGridView_MouseRightButtonDown;
            this.MainGridView.RowValidating += this.MainGridView_RowValidating;
            this.MainGridView.AddingNewDataItem += this.MainGridView_AddingNewDataItem;
            this.MainGridView.RowEditEnded += this.MainGridView_RowEditEnded;
            this.MainGridView.Deleting += this.MainGridView_Deleting;
            this.MainGridView.LoadingRowDetails += this.MainGridView_LoadingRowDetails;
            this.MainGridView.RowLoaded += this.MainGridView_RowLoaded;
            this.MainGridView.CellLoaded += this.MainGridView_CellLoaded;
            this.MainGridView.ElementExporting += this.MainGridView_ElementExporting;
            if(this.MainGridView is RadTreeListView)
                this.MainGridView.RowIsExpandedChanged += this.MainGridView_RowIsExpandedChanged;

            this.MainGrid.Children.Add(this.MainGridView);
        }

        private bool Initialize() {
            if(this.ViewModel == null)
                return false;

            if(!this.initialized) {
                try {
                    //获取权限数据
                    this.GetSecurityData();
                    //初始化GridView对象
                    this.InitializeGridView();
                    //创建DataGrid列
                    this.MainGridView.SetColumns(this.ViewModel.EntityType, this.ViewModel.Columns);
                    //创建DataGrid清单面板
                    this.MainGridView.CreateDetailPanel(this.ViewModel.EntityType, this.ViewModel.Panels);
                    //创建Context菜单
                    this.CreateContextMenu();
                } catch(Exception ex) {
                    this.FatalError(ex.Message);
                    return false;
                }

                if(!this.CanEditEntity)
                    this.MainGridView.IsReadOnly = true;
                this.MainGridView.CanUserInsertRows = this.CanAddEntity;
                this.MainGridView.CanUserDeleteRows = this.CanRemoveEntity;

                if(this.ViewModel.QueryResult == null)
                    this.ViewModel.QueryResult = (IEnumerable)Activator.CreateInstance(typeof(ObservableCollection<>).MakeGenericType(this.ViewModel.EntityType));

                this.initialized = true;
            }
            return true;
        }

        private void DomainContext_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(e.PropertyName != "IsLoading" && e.PropertyName != "IsSubmitting")
                return;
            DataGridViewCommands.RaiseCanExecuteChanged();
            this.MainBusyIndicator.IsBusy = this.ViewModel.DomainContext.IsLoading || this.ViewModel.DomainContext.IsSubmitting;
            if(this.MainBusyIndicator.IsBusy)
                this.MainBusyIndicator.BusyContent = e.PropertyName == "IsLoading" ? ShellUIStrings.DataGridView_BusyQuerying : ShellUIStrings.DataGridView_BusySubmitting;
        }

        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            switch(e.PropertyName) {
                case "UserObject":
                    this.Reset();
                    break;
                case "Content":
                    this.MainTransition.Content = this.ViewModel.Content as FrameworkElement ?? this.MainBusyIndicator;
                    break;
            }
        }

        private void MainGridView_SelectionChanged(object sender, SelectionChangeEventArgs e) {
            DataGridViewCommands.RaiseCanExecuteChanged();
            var selectedItems = this.MainGridView.SelectedItems;
            foreach(var operationViewModel in this.ViewModel.OperationGroupViewModels.SelectMany(operationGroupViewModel => operationGroupViewModel.OperationViewModels))
                operationViewModel.SelectedEntities = selectedItems ?? Enumerable.Empty<object>();
        }

        private void MainGridView_MouseRightButtonDown(object sender, MouseButtonEventArgs e) {
            var gridView = (GridViewDataControl)sender;
            var elements = VisualTreeHelper.FindElementsInHostCoordinates(e.GetPosition(null), gridView);
            var row = elements.OfType<GridViewRow>().FirstOrDefault();
            if(row == null || row.IsSelected)
                return;

            if(gridView.SelectedItems != null) {
                var items = gridView.SelectedItems.ToArray();
                foreach(var item in items)
                    gridView.SelectedItems.Remove(item);
            }
            row.IsSelected = true;
        }

        private void MainGridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var newObject = this.ViewModel.CreateNewEntity(this.ViewModel.EntityType);
            if(newObject != null)
                e.NewObject = newObject;
        }

        private void MainGridView_RowValidating(object sender, GridViewRowValidatingEventArgs e) {
            var validationResult = this.ViewModel.ValidateEntity((Entity)e.Row.DataContext);
            if(validationResult == null)
                return;
            e.IsValid = false;
            e.ValidationResults.Add(new GridViewCellValidationResult {
                ErrorMessage = validationResult.ErrorMessage,
                PropertyName = validationResult.MemberNames.FirstOrDefault(),
            });
        }

        private void MainGridView_RowEditEnded(object sender, GridViewRowEditEndedEventArgs e) {
            if(e.EditAction == GridViewEditAction.Cancel)
                return;
            if(e.EditOperationType == GridViewEditOperationType.Insert)
                this.ViewModel.AddingEntity((Entity)e.NewData);
        }

        private void MainGridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            if(e.Items.Cast<Entity>().Any(item => !this.ViewModel.CanRemoveEntity(item)))
                e.Cancel = true;
        }

        private void MainGridView_LoadingRowDetails(object sender, GridViewRowDetailsEventArgs e) {
            BusinessViewUtils.ProcessDetailPanel(this.ViewModel, e.DetailsElement as RadTabControl);
        }

        private void MainGridView_RowLoaded(object sender, RowLoadedEventArgs e) {
            if(this.MainGridView is RadTreeListView && e.Row is TreeListViewRow && this.ViewModel.TreeLoadChildren != null)
                ((TreeListViewRow)e.Row).IsExpandable = true;
        }

        private void MainGridView_CellLoaded(object sender, CellEventArgs e) {
            if(e.Cell is GridViewCell && e.Cell.Column is GridViewDataColumn && e.Cell.Content is TextBlock) {
                var text = ((TextBlock)e.Cell.Content).Text;
                if(text.Length >= 25)
                    ToolTipService.SetToolTip(e.Cell, text);
            }
        }

        private void MainGridView_ElementExporting(object sender, GridViewElementExportingEventArgs e) {
            this.ViewModel.CustomExportStyle(e);
        }

        private void MainGridView_RowIsExpandedChanged(object sender, RowEventArgs e) {
            if(this.MainGridView is RadTreeListView && e.Row is TreeListViewRow && this.ViewModel.TreeLoadChildren != null && ((TreeListViewRow)e.Row).IsExpanded)
                this.ViewModel.TreeLoadChildren((Entity)e.Row.DataContext);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e) {
            if(this.NavigationContext != null || this.ViewModel == null)
                return;

            if(!this.Initialize())
                return;

            //自动查询
            if(this.ViewModel.IsAutoLoad)
                this.AutoLoadData();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            if(this.ViewModel == null) {
                this.ViewModel = this.CreateViewModelFromQueryString<IDataGridViewModel>();
                if(this.ViewModel == null)
                    return;
            }

            if(!this.Initialize())
                return;

            //自动查询
            if(this.ViewModel.IsAutoLoad)
                this.AutoLoadData();
        }

        public DataGridView() {
            this.InitializeComponent();
        }
    }
}
