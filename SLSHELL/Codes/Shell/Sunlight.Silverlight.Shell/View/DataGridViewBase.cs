﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.DomainServices;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.View {
    public sealed class DataGridViewRowDoubleClickEventArgs : EventArgs {
        public GridViewRow Row {
            get;
            internal set;
        }

        public bool Handled {
            get;
            set;
        }
    }

    public abstract class DataGridViewBase : UserControl, IDataGridView, INotifyPropertyChanged, IDelayCall {
        private const double DOUBLE_CLICK_POSITION_DEVIATION = 4.0;
        private static readonly TimeSpan DoubleClickThreshold = TimeSpan.FromMilliseconds(450);
        private bool isUIReady, isDefaultGroupApplied;
        private DateTime lastClickTime;
        private Point lastClickPosition;
        private ViewInitializer initializer;
        private DomainContext domainContext;
        private List<Func<bool>> canExecuteQueryDependencies;
        private Dictionary<string, Dictionary<string, Type>> parametersCache;
        private List<GridViewRowItem> validationErrors;
        public event EventHandler DataLoaded;
        public event EventHandler SelectionChanged;
        public event EventHandler<DataGridViewRowDoubleClickEventArgs> RowDoubleClick;
        public event PropertyChangedEventHandler PropertyChanged;

        protected ViewInitializer Initializer {
            get {
                return this.initializer ?? (this.initializer = new ViewInitializer(this));
            }
        }

        protected RadDomainDataSource DomainDataSource {
            get;
            private set;
        }

        protected RadGridView GridView {
            get;
            private set;
        }

        protected RadDataPager DataPager {
            get;
            private set;
        }

        protected abstract Type EntityType {
            get;
        }

        protected abstract IEnumerable<ColumnItem> ColumnItems {
            get;
        }

        protected virtual IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return null;
            }
        }

        protected virtual bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected virtual bool UsePaging {
            get {
                return true;
            }
        }

        public DomainContext DomainContext {
            get {
                return this.domainContext;
            }
            set {
                this.domainContext = value;
                this.CreateDomainDataSource();
            }
        }

        public virtual FilterItem FilterItem {
            get;
            set;
        }

        public virtual IEnumerable<Entity> Entities {
            get;
            protected set;
        }

        public virtual IEnumerable<Entity> SelectedEntities {
            get;
            protected set;
        }

        public bool CanExecuteQuery {
            get {
                return this.canExecuteQueryDependencies == null || this.canExecuteQueryDependencies.All(f => f());
            }
        }

        public bool HasErrors {
            get {
                return this.validationErrors != null && this.validationErrors.Count > 0;
            }
        }

        private void RaiseRowDoubleClick(DataGridViewRowDoubleClickEventArgs args) {
            var handler = this.RowDoubleClick;
            if(handler != null)
                handler(this, args);
        }

        private void SetGridViewIsBusyBinding() {
            this.GridView.SetBinding(GridViewDataControl.IsBusyProperty, new Binding("IsBusy") {
                Source = this.DomainDataSource,
                Mode = BindingMode.OneWay,
            });
        }

        private void CreateDomainDataSource() {
            if(this.DomainDataSource == null) {
                this.DomainDataSource = new RadDomainDataSource {
                    AutoLoad = false,
                    PageSize = this.UsePaging ? 15 : int.MaxValue,
                };
                //this.DomainDataSource.LoadingData += DomainDataSourceOnLoadingData;
                this.DomainDataSource.LoadingData += (sender, args) => args.LoadBehavior = LoadBehavior.RefreshCurrent;
                this.DomainDataSource.LoadedData += this.DomainDataSource_LoadedData;
            }
            this.DomainDataSource.DomainContext = this.DomainContext;
            this.CreateDataPager();
        }

        private void CreateDataPager() {
            if(this.DataPager == null) {
                this.DataPager = new RadDataPager {
                    FontSize = 10,
                    AutoEllipsisMode = AutoEllipsisModes.Both,
                    DisplayMode = PagerDisplayModes.All,
                    NumericButtonCount = 10,
                    IsTotalItemCountFixed = true,
                };
                this.DataPager.SetValue(Grid.RowProperty, 1);
                this.DataPager.SetBinding(RadDataPager.SourceProperty, this.OnRequestDataSourceBinding());
                this.DataPager.SetBinding(RadDataPager.PageSizeProperty, new Binding("PageSize") {
                    Source = this.DomainDataSource,
                    Mode = BindingMode.TwoWay,
                });
            }
            this.CreateGridView();
        }

        private void CreateGridView() {
            if(this.GridView == null) {
                this.GridView = new RadGridView {
                    FontSize = 12,
                    ActionOnLostFocus = ActionOnLostFocus.None,
                    AutoGenerateColumns = false,
                    AutoExpandGroups = true,
                    CanUserFreezeColumns = false,
                    CanUserReorderColumns = true,
                    CanUserResizeColumns = true,
                    CanUserSortColumns = true,
                    IsReadOnly = true,
                    IsSynchronizedWithCurrentItem = false,
                    ClipboardCopyMode = GridViewClipboardCopyMode.Cells,
                    ClipboardPasteMode = GridViewClipboardPasteMode.Cells | GridViewClipboardPasteMode.OverwriteWithEmptyValues | GridViewClipboardPasteMode.SkipHiddenColumns,
                    SelectionMode = System.Windows.Controls.SelectionMode.Extended,
                    SelectionUnit = GridViewSelectionUnit.Cell,
                    RowDetailsVisibilityMode = GridViewRowDetailsVisibilityMode.Collapsed,
                };
                this.GridView.SelectionChanged += (sender, args) => {
                    this.SelectedEntities = ((RadGridView)sender).SelectedItems.Cast<Entity>();
                    this.OnSelectionChanged();
                };
                if(this.RowDetailsTemplateProvider != null) {
                    var template = this.RowDetailsTemplateProvider.CreateTemplate();
                    if(template != null) {
                        this.GridView.RowDetailsTemplate = template;
                        this.GridView.LoadingRowDetails += this.GridView_LoadingRowDetails;
                    }
                }
                this.GridView.SetBinding(DataControl.ItemsSourceProperty, new Binding("PagedSource") {
                    Source = this.DataPager
                });
                this.SetGridViewIsBusyBinding();

                this.GridView.SetColumns(this.EntityType, this.ColumnItems, !this.ShowCheckBox, true);
                if(this.GridView.RowDetailsTemplate != null)
                    this.GridView.Columns.Insert(0, new GridViewToggleRowDetailsColumn());

                this.DomainDataSource.SortDescriptors.Clear();
                foreach(var columnItem in this.ColumnItems.Where(c => c.IsSortDescending != null && c.SubColumns == null))
                    this.DomainDataSource.SortDescriptors.Add(new ColumnSortDescriptor {
                        Column = this.GridView.Columns.OfType<GridViewBoundColumnBase>().Single(c => c.DataMemberBinding.Path.Path == columnItem.Name),
                        SortDirection = (columnItem.IsSortDescending != null && columnItem.IsSortDescending.Value) ? ListSortDirection.Descending : ListSortDirection.Ascending,
                    });

                this.GridView.BindingValidationError += (sender, args) => {
                    var row = ((RadGridView)sender).CurrentCell.ParentRow;
                    if(args.Action == ValidationErrorEventAction.Added) {
                        if(this.validationErrors == null)
                            this.validationErrors = new List<GridViewRowItem>();
                        if(this.validationErrors.Contains(row))
                            return;
                        this.validationErrors.Add(row);
                        this.NotifyOfPropertyChange("HasErrors");
                    } else if(args.Action == ValidationErrorEventAction.Removed) {
                        if(this.validationErrors != null && this.validationErrors.Remove(row)) {
                            this.NotifyOfPropertyChange("HasErrors");
                        }
                    }
                };

                this.GridView.RowEditEnded += (sender, args) => {
                    if(args.EditAction == GridViewEditAction.Cancel)
                        if(this.validationErrors != null && this.validationErrors.Remove(args.Row))
                            this.NotifyOfPropertyChange("HasErrors");
                };

                this.GridView.Deleted += (sender, args) => {
                    if(this.validationErrors != null) {
                        var deleted = (from item in args.Items
                                       from gridViewRowItem in this.validationErrors
                                       where gridViewRowItem.Item == item
                                       select gridViewRowItem).ToList();
                        foreach(var item in deleted)
                            this.validationErrors.Remove(item);
                    }
                };
            }
            this.CreateContent();
        }

        private void CreateContent() {
            var content = this.Content as Grid;
            if(content == null) {
                this.Content = content = new Grid();
                content.RowDefinitions.Add(new RowDefinition());
                content.RowDefinitions.Add(new RowDefinition {
                    Height = GridLength.Auto
                });
            }
            content.Children.Clear();
            content.Children.Add(this.GridView);
            content.Children.Add(this.DataPager);

            this.OnControlsCreated();
            this.isUIReady = true;
            this.NotifyOfPropertyChange("CanExecuteQuery");
        }

        private Dictionary<string, Type> GetQueryParameters(string queryName) {
            if(queryName == null)
                throw new ArgumentNullException("queryName");

            if(this.parametersCache == null)
                this.parametersCache = new Dictionary<string, Dictionary<string, Type>>();
            if(this.parametersCache.ContainsKey(queryName))
                return this.parametersCache[queryName];

            var type = this.DomainContext.GetType();
            var methodInfo = type.GetMethod(queryName + "Query");
            if(methodInfo == null)
                throw new Exception(string.Format("{0} 类里不包含名为 {1} 的查询方法", type.FullName, queryName));
            var parameters = methodInfo.GetParameters().ToDictionary(info => info.Name, info => info.ParameterType);
            this.parametersCache.Add(queryName, parameters);
            return parameters;
        }

        private void Initialize() {
            if(this.DomainContext == null)
                this.DomainContext = DI.GetDomainContextByType(this.GetType());
        }

        private void DomainDataSource_LoadedData(object sender, LoadedDataEventArgs e) {
            if(e.HasError) {
                if(!e.IsErrorHandled)
                    e.MarkErrorAsHandled();
                ShellUtils.ShowDomainServiceOperationWindow(e.Error);
                return;
            }
            this.Entities = e.Entities;
            if(!this.isDefaultGroupApplied) {
                this.isDefaultGroupApplied = true;
                this.DomainDataSource.GroupDescriptors.Clear();
                foreach(var columnItem in this.ColumnItems.Where(c => c.IsDefaultGroup && c.SubColumns == null).OrderBy(c => c.DefaultGroupPosition))
                    this.DomainDataSource.GroupDescriptors.Add(new ColumnGroupDescriptor {
                        Column = this.GridView.Columns.OfType<GridViewBoundColumnBase>().Single(c => c.DataMemberBinding.Path.Path == columnItem.Name),
                        SortDirection = columnItem.IsGroupSortDescending == null ? (ListSortDirection?)null : (columnItem.IsGroupSortDescending.Value ? ListSortDirection.Descending : ListSortDirection.Ascending),
                    });
            }
            this.OnDataLoaded();
        }

        private void DomainDataSourceOnLoadingData(object sender, LoadingDataEventArgs loadingDataEventArgs) {
            var first = this.Entities.FirstOrDefault();
            if(first == null)
                return;
            EntitySet entitySet;
            if(!this.DomainContext.EntityContainer.TryGetEntitySet(first.GetType(), out entitySet))
                return;
            if(entitySet.Count == 0)
                return;
            foreach(var entity in this.Entities)
                try {
                    entitySet.Detach(entity);
                } catch(Exception e) {
                    Console.WriteLine(e);
                }
        }

        private void GridView_LoadingRowDetails(object sender, GridViewRowDetailsEventArgs e) {
            if(this.RowDetailsTemplateProvider != null)
                this.RowDetailsTemplateProvider.OnInitializing(e.DetailsElement);
        }

        private void GridView_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
            var element = sender as UIElement;
            if(element == null)
                return;

            var position = e.GetPosition(element);
            if(DateTime.Now - this.lastClickTime <= DoubleClickThreshold && Math.Abs(this.lastClickPosition.X - position.X) < DOUBLE_CLICK_POSITION_DEVIATION && Math.Abs(this.lastClickPosition.Y - position.Y) < DOUBLE_CLICK_POSITION_DEVIATION) {
                this.lastClickTime = DateTime.MinValue;
                this.lastClickPosition = new Point();

                var elements = VisualTreeHelper.FindElementsInHostCoordinates(e.GetPosition(null), element).ToArray();
                if(elements.OfType<DetailsPresenter>().Any())
                    return;

                var row = elements.OfType<GridViewRow>().FirstOrDefault();
                if(row != null) {
                    var args = new DataGridViewRowDoubleClickEventArgs {
                        Row = row,
                        Handled = e.Handled,
                    };
                    this.RaiseRowDoubleClick(args);
                    e.Handled = args.Handled;
                }
            } else {
                this.lastClickTime = DateTime.Now;
                this.lastClickPosition = position;
            }
        }

        protected DataGridViewBase() {
            this.AddHandler(MouseLeftButtonUpEvent, new MouseButtonEventHandler(this.GridView_MouseLeftButtonUp), true);
            this.RegisterCanExecuteQueryDependency(() => this.isUIReady);
            this.Initializer.Register(this.Initialize);
            this.CallEntryList = new List<CallEntry>();
            this.BindingValidationError += (sender, args) => {
                args.Handled = true;
            };
        }

        protected abstract void OnControlsCreated();

        protected abstract string OnRequestQueryName();

        protected abstract object OnRequestQueryParameter(string queryName, string parameterName);

        protected abstract IFilterDescriptor OnRequestFilterDescriptor(string queryName);

        protected virtual Binding OnRequestDataSourceBinding() {
            return new Binding("DataView") {
                Source = this.DomainDataSource,
                Mode = BindingMode.OneWay,
                TargetNullValue = Array.CreateInstance(this.EntityType, 0)
            };
        }

        protected virtual void OnDataLoaded() {
            var handler = this.DataLoaded;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        protected virtual void OnSelectionChanged() {
            var handler = this.SelectionChanged;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        protected void NotifyOfPropertyChange(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void RegisterCanExecuteQueryDependency(Func<bool> func) {
            if(this.canExecuteQueryDependencies == null)
                this.canExecuteQueryDependencies = new List<Func<bool>>();
            if(!this.canExecuteQueryDependencies.Contains(func))
                this.canExecuteQueryDependencies.Add(func);
            this.NotifyOfPropertyChange("CanExecuteQuery");
        }

        protected void RebindKeyValueColumns() {
            if(this.GridView == null)
                return;

            var columns = this.GridView.Columns.OfType<GridViewComboBoxColumn>();
            foreach(var column in columns) {
                var kvItems = column.ItemsSource;
                column.ItemsSource = null;
                column.ItemsSource = kvItems;
            }
        }

        public virtual object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new NotSupportedException();
        }

        public virtual void ExecuteQuery() {
            if(!this.CanExecuteQuery)
                return;

            var queryName = this.OnRequestQueryName();
            if(string.IsNullOrEmpty(queryName))
                return;

            this.DomainDataSource.QueryName = queryName;

            this.DomainDataSource.QueryParameters.Clear();
            var parameters = this.GetQueryParameters(queryName);
            foreach(var parameter in parameters) {
                var value = this.OnRequestQueryParameter(queryName, parameter.Key);
                this.DomainDataSource.QueryParameters.Add(new QueryParameter {
                    ParameterName = parameter.Key,
                    Value = value,
                });
            }

            this.DomainDataSource.FilterDescriptors.Clear();
            var filterDescriptor = this.OnRequestFilterDescriptor(queryName);
            if(filterDescriptor != null)
                this.DomainDataSource.FilterDescriptors.Add(filterDescriptor);

            this.DomainDataSource.Load();
        }

        public void ExecuteQueryDelayed() {
            this.DelayCall(self => self.CanExecuteQuery, self => self.ExecuteQuery());
        }

        public void UpdateSelectedEntities(Action<Entity> updateAction, Action callback = null) {
            if(updateAction == null)
                throw new ArgumentNullException("updateAction");

            if(this.SelectedEntities == null)
                return;

            this.GridView.IsBusy = true;

            foreach(var entity in this.SelectedEntities)
                updateAction(entity);

            this.DomainContext.SubmitChanges(submitOp => {
                try {
                    this.GridView.IsBusy = true;
                    if(!submitOp.HasError) {
                        if(callback != null)
                            callback();
                        return;
                    }
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    ShellUtils.ShowDomainServiceOperationWindow(submitOp);
                } finally {
                    this.SetGridViewIsBusyBinding();
                }
            }, null);
        }

        public bool CommitEdit() {
            return this.GridView == null || this.GridView.CommitEdit();
        }

        public List<CallEntry> CallEntryList {
            get;
            private set;
        }
    }
}
