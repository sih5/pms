﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Resources;
using Sunlight.Silverlight.Shell;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.TransitionEffects;

namespace Sunlight.Silverlight.View {
    public abstract class DataManagementViewBase : Page, IRootView, IRibbonTabsView {
        private ViewInitializer initializer;
        private RadTransitionControl transition;
        private List<RadRibbonTab> ribbonTabs;
        private Dictionary<string, Func<UserControl>> views;

        private IDictionary<string, ActionPanelBase> ActionPanels {
            get;
            set;
        }

        private IDictionary<string, QueryPanelBase> QueryPanels {
            get;
            set;
        }

        protected ViewInitializer Initializer {
            get {
                return this.initializer ?? (this.initializer = new ViewInitializer(this));
            }
        }

        protected abstract IEnumerable<string> QueryPanelKeys {
            get;
        }

        protected abstract IEnumerable<string> ActionPanelKeys {
            get;
        }

        protected abstract IEnumerable<RadRibbonTab> CustomRibbonTabs {
            get;
        }

        protected string CurrentViewKey {
            get;
            private set;
        }

        public object UniqueId {
            get;
            internal set;
        }

        public IEnumerable<RadRibbonTab> RibbonTabs {
            get {
                if(this.ribbonTabs == null) {
                    this.ribbonTabs = new List<RadRibbonTab>(4);
                    // Query Panels
                    if(this.QueryPanelKeys != null && this.QueryPanelKeys.Any()) {
                        var ribbonTab = new RadRibbonTab {
                            Header = BusinessViewUtils.MakeTabHeader(new Uri("/Sunlight.Silverlight.Shell;component/Images/tab-query.png", UriKind.Relative), ShellUIStrings.ShellViewModel_QueryTabText),
                            Tag = ShellUIStrings.ShellViewModel_QueryTabText
                        };
                        this.QueryPanels = new Dictionary<string, QueryPanelBase>(this.QueryPanelKeys.Count());
                        foreach(var key in this.QueryPanelKeys) {
                            var panel = DI.GetQueryPanel(key);
                            panel.SetRootView(this);
                            panel.ExecutingQuery += this.QueryPanel_ExecutingQuery;
                            ribbonTab.Items.Add(panel);
                            this.QueryPanels.Add(key, panel);
                        }
                        this.ribbonTabs.Add(ribbonTab);
                    }
                    // Action Panels
                    if(this.ActionPanelKeys != null && this.ActionPanelKeys.Any()) {
                        var ribbonTab = new RadRibbonTab {
                            Header = BusinessViewUtils.MakeTabHeader(new Uri("/Sunlight.Silverlight.Shell;component/Images/tab-actions.png", UriKind.Relative), ShellUIStrings.ShellViewModel_ActionTabText),
                            Tag = ShellUIStrings.ShellViewModel_ActionTabText
                        };
                        this.ActionPanels = new Dictionary<string, ActionPanelBase>(this.ActionPanelKeys.Count());
                        foreach(var key in this.ActionPanelKeys) {
                            var panel = DI.GetActionPanel(key);
                            panel.PageId = Convert.ToInt32(this.UniqueId);
                            panel.Loaded += (sender, args) => this.CheckActionsCanExecute();
                            panel.ExecutingAction += this.ActionPanel_ExecutingAction;
                            ribbonTab.Items.Add(panel);
                            this.ActionPanels.Add(key, panel);
                        }
                        this.ribbonTabs.Add(ribbonTab);
                    }
                    // Custom Tabs
                    if(this.CustomRibbonTabs != null && this.CustomRibbonTabs.Any())
                        foreach(var ribbonTab in this.CustomRibbonTabs)
                            this.ribbonTabs.Add(ribbonTab);
                }
                return this.ribbonTabs;
            }
        }

        public new UIElement Content {
            get {
                return base.Content;
            }
        }

        private void Initialize() {
            this.transition = new RadTransitionControl {
                Duration = TimeSpan.FromMilliseconds(500),
                Transition = new PixelateTransition {
                    IsPixelLED = false,
                },
            };
            base.Content = this.transition;
        }

        private void QueryPanel_ExecutingQuery(object sender, FilterPanelRibbonGroup.ExecutingQueryEventArgs e) {
            var queryPanel = sender as QueryPanelBase;
            if(queryPanel == null)
                return;
            if(BaseApp.Current.SystemType == SystemTypes.Dcs && queryPanel.QueryItemGroups != null) {
                foreach(var queryItemGroup in queryPanel.QueryItemGroups) {
                    if(queryItemGroup.QueryItems != null && queryItemGroup.QueryItems.Any()) {
                        foreach(var queryItem in queryItemGroup.QueryItems) {
                            queryItem.IsCaseSensitive = false;
                        }
                    }
                }
            }
            this.OnExecutingQuery(queryPanel, e.Filter);
        }

        /// <summary>
        /// 执行按钮操作前的统一操作覆盖该方法
        /// </summary>
        /// <param name="actionPanel"></param>
        /// <param name="uniqueId"></param>
        /// <param name="executeNextCode">是否继续执行接下来的代码</param>
        protected virtual void BeforeExcutingAction(ActionPanelBase actionPanel, string uniqueId, out bool executeNextCode) {
            executeNextCode = true;
        }

        private void ActionPanel_ExecutingAction(object sender, ActionPanelRibbonGroup.ExecutingActionEventArgs e) {
            var actionPanel = sender as ActionPanelBase;
            if(actionPanel == null)
                return;
            bool executeNextCode;
            BeforeExcutingAction(actionPanel, e.UniqueId, out executeNextCode);
            if(executeNextCode)
                this.OnExecutingAction(actionPanel, e.UniqueId);
        }

        protected DataManagementViewBase() {
            this.Initializer.Register(this.Initialize);
        }

        protected abstract void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem);

        protected abstract void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId);

        protected abstract bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId);

        protected abstract void OnViewSwitched(string key, UserControl view);

        protected QueryPanelBase GetQueryPanel(string key) {
            QueryPanelBase result;
            return this.QueryPanels != null && this.QueryPanels.TryGetValue(key, out result) ? result : null;
        }

        protected ActionPanelBase GetActionPanel(string key) {
            ActionPanelBase result;
            return this.ActionPanels != null && this.ActionPanels.TryGetValue(key, out result) ? result : null;
        }

        protected void RegisterView<T>(string key, Func<T> factoryFunc) where T : UserControl {
            if(key == null)
                throw new ArgumentNullException("key");
            if(factoryFunc == null)
                throw new ArgumentNullException("factoryFunc");

            if(this.views == null)
                this.views = new Dictionary<string, Func<UserControl>>(4);

            var countBeforeAdd = this.views.Count;
            this.views[key] = factoryFunc;
            if(countBeforeAdd == 0 && this.views.Count == 1)
                this.SwitchViewTo(key);
        }

        /// <summary>
        /// 移除缓存中的view
        /// </summary>
        /// <param name="key"></param>
        protected void RemoveView(string key) {
            if(this.views == null || !this.views.ContainsKey(key))
                return;
            this.views.Remove(key);
        }

        protected void CheckActionsCanExecute() {
            if(this.ActionPanels == null)
                return;
            foreach(var actionPanel in this.ActionPanels.Values.Where(p => p.IsLoaded && p.ActionItemGroup != null && p.ActionItemGroup.ActionItems != null))
                foreach(var uniqueId in actionPanel.ActionItemGroup.ActionItems.Select(a => a.UniqueId)) {
                    var canExecute = this.OnRequestActionCanExecute(actionPanel, uniqueId);
                    actionPanel.ExchangeData(this, "SetIsEnabled", uniqueId, canExecute);
                }
        }

        /// <summary>
        /// 获取指定的View
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected UserControl GetViewByKey(string key) {
            if(this.views == null || !this.views.ContainsKey(key))
                return null;
            return this.views[key]();
        }

        protected void SwitchViewTo(string key) {
            if(this.views == null || !this.views.ContainsKey(key))
                return;

            var view = this.views[key]();
            if(view == null || (ReferenceEquals(this.transition.Content, view)))
                return;

            var currentView = this.transition.Content as DataEditViewBase;
            if(currentView != null) {
                currentView.Cancel();
            }

            this.transition.Content = view;
            this.CurrentViewKey = key;
            this.OnViewSwitched(key, view);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            base.OnNavigatedTo(e);

            ShellUtils.ChangeHtmlTitle();

            if(!this.NavigationContext.QueryString.Any() || !this.QueryPanels.Any())
                return;

            var queryPanel = this.QueryPanels.First().Value;
            if(queryPanel != null) {
                queryPanel.SetValues(this.NavigationContext.QueryString);
                if(this.NavigationContext.QueryString.ContainsKey("ExecuteQuery"))
                    queryPanel.ExecuteQuery();
            }
        }

        public virtual object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new NotSupportedException();
        }

        /// <summary>
        /// 当前页面下EditView中有没有编辑中的Entity
        /// </summary>
        /// <returns></returns>
        public virtual bool CheckEditViewHasEditingEntity() {
            return this.views.Select(v => v.Value()).OfType<DataEditViewBase>().Any(v => v.HasEditingEntity());
        }
    }
}
