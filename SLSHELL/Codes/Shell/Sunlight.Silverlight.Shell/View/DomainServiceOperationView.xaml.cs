﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.ServiceModel.DomainServices.Client.ApplicationServices;
using System.Text;
using System.Windows;
using Sunlight.Silverlight.Resources;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.View {
    public partial class DomainServiceOperationView {
        private void ShowValidationError(IEnumerable<ValidationResult> validationErrors) {
            // ReSharper disable PossibleMultipleEnumeration
            if(validationErrors != null && validationErrors.Any()) {
                var returnedMessage = string.Join(Environment.NewLine, validationErrors.Select(ve => ve.ErrorMessage).Distinct());
                this.Message.Text = ShellUIStrings.ExceptionView_ValidationErrorMessage;
                this.ValidationMessage.Text = returnedMessage;
                this.ValidationMessage.Visibility = Visibility.Visible;
            } else
                this.Message.Text = ShellUIStrings.ExceptionView_GeneralMessage;
            // ReSharper restore PossibleMultipleEnumeration
        }

        private void ProcessLoadOperation(LoadOperation op, IDictionary<string, object> properties) {
            properties.Add("Operation.Type", "Load");
            properties.Add("Operation.LoadBehavior", op.LoadBehavior);
            properties.Add("Operation.QueryName", op.EntityQuery.QueryName);
            properties.Add("Operation.EntityType", op.EntityQuery.EntityType);
            properties.Add("Operation.Expression", op.EntityQuery.Query.Expression.ToString());

            this.ShowValidationError(op.ValidationErrors);
        }

        private void ProcessSubmitOperation(SubmitOperation op, IDictionary<string, object> properties) {
            // ReSharper disable PossibleMultipleEnumeration
            properties.Add("Operation.Type", "Submit");

            if(op.EntitiesInError.Any()) {
                var conflictEntities = op.EntitiesInError.Where(e => e.EntityConflict != null);
                if(conflictEntities.Any()) {
                    var index = 1;
                    foreach(var entity in conflictEntities) {
                        var entityKey = string.Format("Operation.ConflictEntity{0}", index++);
                        properties.Add(entityKey, entity);
                        properties.Add(entityKey + ".Properties", string.Join(", ", entity.EntityConflict.PropertyNames));
                        properties.Add(entityKey + ".IsDeleted", entity.EntityConflict.IsDeleted);
                    }
                    this.Message.Text = ShellUIStrings.ExceptionView_ConflictMessage;
                } else
                    this.ShowValidationError(op.EntitiesInError.Where(e => e.HasValidationErrors).SelectMany(e => e.ValidationErrors));
            } else
                this.Message.Text = ShellUIStrings.ExceptionView_GeneralMessage;
            // ReSharper restore PossibleMultipleEnumeration
        }

        private void ProcessInvokeOperation(InvokeOperation op, IDictionary<string, object> properties) {
            properties.Add("Operation.Type", "Invoke");
            properties.Add("Operation.Name", op.OperationName);
            var index = 1;
            foreach(var kv in op.Parameters)
                properties.Add(string.Format("Operation.Parameter{0}", index++), string.Format("{0} = {1}", kv.Key, kv.Value));

            this.ShowValidationError(op.ValidationErrors);
        }

        private void ProcessAuthenticationOperation(AuthenticationOperation op, IDictionary<string, object> properties) {
            properties.Add("Operation.Type", "Authentication");
            if(op.User != null && op.User.Identity != null) {
                properties.Add("Operation.User", op.User.Identity.Name);
                properties.Add("Operation.IsAuthenticated", op.User.Identity.IsAuthenticated);
            }

            this.Message.Text = ShellUIStrings.ExceptionView_GeneralMessage;
        }

        private void ProcessOperationBase(OperationBase op) {
            var messageBuilder = new StringBuilder();
            var isSessionExpired = false;

            if(op.Error != null) {
                isSessionExpired = ShellUtils.IsSessionExpiredException(op.Error);
                messageBuilder.Append(op.Error);
                messageBuilder.AppendLine();
                messageBuilder.AppendLine();
            }

            if(isSessionExpired) {
                this.Message.Text = ShellUIStrings.ExceptionView_SessionExpiredMessage;
                this.LoginPanel.Visibility = Visibility.Visible;
            }

            var properties = ShellUtils.GetCurrentEnvironmentProperties();

            try {
                properties.Add("Operation.IsComplete", op.IsComplete);
                properties.Add("Operation.IsCanceled", op.IsCanceled);

                var loadOp = op as LoadOperation;
                if(loadOp != null) {
                    this.ProcessLoadOperation(loadOp, properties);
                    return;
                }

                var submitOp = op as SubmitOperation;
                if(submitOp != null) {
                    this.ProcessSubmitOperation(submitOp, properties);
                    return;
                }

                var invokeOp = op as InvokeOperation;
                if(invokeOp != null) {
                    this.ProcessInvokeOperation(invokeOp, properties);
                    return;
                }

                var authOp = op as AuthenticationOperation;
                if(authOp != null)
                    this.ProcessAuthenticationOperation(authOp, properties);
            } catch(Exception ex) {
                messageBuilder.AppendLine("Exception while logging:");
                messageBuilder.Append(ex);
                messageBuilder.AppendLine();
                messageBuilder.AppendLine();
            } finally {
                foreach(var property in properties)
                    messageBuilder.AppendFormat("{0}: {1}\r\n", property.Key, property.Value);
                this.DetailMessage.Text = messageBuilder.ToString();
            }
        }

        private void ProcessException(Exception ex) {
            var isSessionExpired = ShellUtils.IsSessionExpiredException(ex);
            this.Message.Text = isSessionExpired ? ShellUIStrings.ExceptionView_SessionExpiredMessage : ShellUIStrings.ExceptionView_GeneralMessage;
            if(isSessionExpired)
                this.LoginPanel.Visibility = Visibility.Visible;

            var messageBuilder = new StringBuilder();

            if(ex != null) {
                messageBuilder.Append(ex);
                messageBuilder.AppendLine();
                messageBuilder.AppendLine();
            }

            foreach(var property in ShellUtils.GetCurrentEnvironmentProperties())
                messageBuilder.AppendFormat("{0}: {1}\r\n", property.Key, property.Value);
            this.DetailMessage.Text = messageBuilder.ToString();
        }

        private void DomainServiceOperationView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            this.DetailMessage.Text = string.Empty;
            this.LoginPanel.Visibility = Visibility.Collapsed;

            var exception = e.NewValue as Exception;
            if(exception != null)
                this.ProcessException(exception);
            else {
                var op = e.NewValue as OperationBase;
                if(op != null)
                    this.ProcessOperationBase(op);
            }
        }

        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(e.PropertyName != "LoginSuccess" || !((LoginViewModel)sender).LoginSuccess)
                return;
            var window = this.ParentOfType<RadWindow>();
            if(window != null)
                window.Close();
        }

        public DomainServiceOperationView() {
            this.InitializeComponent();
            this.DataContextChanged += this.DomainServiceOperationView_DataContextChanged;
            this.LoginPanel.ViewModel.PropertyChanged += this.ViewModel_PropertyChanged;
        }
    }
}
