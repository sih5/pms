﻿using System;
using System.Text;
using System.Windows;

namespace Sunlight.Silverlight.View {
    public partial class ExceptionView {
        private void ExceptionView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var message = new StringBuilder();

            var exception = e.NewValue as Exception;
            if(exception != null) {
                message.Append(exception);
                message.AppendLine();
                message.AppendLine();
            }

            foreach(var property in ShellUtils.GetCurrentEnvironmentProperties())
                message.AppendFormat("{0}: {1}\r\n", property.Key, property.Value);

            this.DetailMessage.Text = message.ToString();
        }

        public ExceptionView() {
            this.InitializeComponent();
            this.DataContextChanged += this.ExceptionView_DataContextChanged;
        }
    }
}
