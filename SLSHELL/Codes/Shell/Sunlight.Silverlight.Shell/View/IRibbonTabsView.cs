﻿using System.Collections.Generic;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.View {
    public interface IRibbonTabsView {
        IEnumerable<RadRibbonTab> RibbonTabs {
            get;
        }
    }
}
