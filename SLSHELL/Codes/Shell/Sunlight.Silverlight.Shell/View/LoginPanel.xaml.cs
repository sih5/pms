﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Input;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.View {
    public partial class LoginPanel {
        public LoginViewModel ViewModel {
            get;
            private set;
        }

        public bool HideLanguageOptions {
            get {
                return this.LanguagePanel.Visibility == Visibility.Collapsed;
            }
            set {
                this.LanguageText.Visibility = this.LanguagePanel.Visibility = value ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e) {
            if(e.Key != Key.Enter || this.LoginButton.IsEnabled == false)
                return;
            var peer = new ButtonAutomationPeer(this.LoginButton);
            ((IInvokeProvider)peer.GetPattern(PatternInterface.Invoke)).Invoke();
        }

        public LoginPanel() {
            this.InitializeComponent();
            if(!DesignerProperties.IsInDesignTool)
                this.DataContext = this.ViewModel = new LoginViewModel();
        }
    }
}
