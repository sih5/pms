﻿using System.Windows;
using Sunlight.Silverlight.Model;

namespace Sunlight.Silverlight.View {
    public partial class MenuContentView {
        public SystemMenuItem MenuItem {
            get;
            set;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs) {
            if(this.PageMenuView.RootMenuItem == null)
                this.PageMenuView.RootMenuItem = this.MenuItem;
            if(this.NavigationView.RootMenuItem == null)
                this.NavigationView.RootMenuItem = this.MenuItem;
        }

        public MenuContentView() {
            this.InitializeComponent();
            this.Loaded += this.OnLoaded;
        }
    }
}
