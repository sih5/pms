﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Model;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.View {
    public partial class NavigationView {
        private static readonly Queue<NavigationView> ProcessingQueue = new Queue<NavigationView>();
        private static string NavigationXml;

        private SystemMenuItem rootMenuItem;
        private List<NavigationItem> navigationItems;

        public SystemMenuItem RootMenuItem {
            get {
                return this.rootMenuItem;
            }
            set {
                this.rootMenuItem = value;
                this.CheckXml();
            }
        }

        private void CheckXml() {
            if(NavigationXml == null)
                lock(ProcessingQueue) {
                    ProcessingQueue.Enqueue(this);
                    if(ProcessingQueue.Count == 1) {
                        var client = new WebClient();
                        client.DownloadStringCompleted += (_, args) => {
                            try {
                                if(args.Error != null || args.Cancelled)
                                    return;
                                NavigationXml = args.Result;
                                foreach(var view in ProcessingQueue)
                                    view.CreateChartData();
                            } finally {
                                ProcessingQueue.Clear();
                            }
                        };
                        client.DownloadStringAsync(Core.Utils.MakeServerUri("/Navigation.xml"));
                    }
                }
            else
                this.CreateChartData();
        }

        private void CreateChartData() {
            if(this.RootMenuItem == null)
                return;

            var eRoot = XDocument.Parse(NavigationXml).Root;
            if(eRoot == null)
                return;

            var eSystem = eRoot.Elements("System").Where(e => {
                var attribute = e.Attribute("Id");
                return attribute != null && attribute.Value == this.RootMenuItem.Name;
            }).SingleOrDefault();
            if(eSystem == null)
                return;

            if(this.navigationItems == null)
                this.navigationItems = new List<NavigationItem>();
            this.navigationItems.Clear();

            foreach(var eChart in eSystem.Elements("Flowchart")) {
                var navigationItem = new NavigationItem();

                var eName = eChart.Element("Name");
                if(eName != null) {
                    var eLocaleName = eName.Element("default");
                    if(eLocaleName != null)
                        navigationItem.Title = eLocaleName.Value;
                    eLocaleName = eName.Element(Thread.CurrentThread.CurrentUICulture.Name);
                    if(eLocaleName != null)
                        navigationItem.Title = eLocaleName.Value;
                    else {
                        eLocaleName = eName.Element(Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName);
                        if(eLocaleName != null)
                            navigationItem.Title = eLocaleName.Value;
                    }
                }

                var eThumbnail = eChart.Element("Thumbnail");
                if(eThumbnail != null && !string.IsNullOrWhiteSpace(eThumbnail.Value))
                    navigationItem.Thumbnail = Core.Utils.MakeServerUri(eThumbnail.Value.Trim());

                var eContent = eChart.Element("Content");
                if(eContent != null && !string.IsNullOrWhiteSpace(eContent.Value)) {
                    var resourceInfo = Application.GetResourceStream(new Uri(eContent.Value.Trim(), UriKind.Relative));
                    if(resourceInfo != null)
                        using(var reader = new StreamReader(resourceInfo.Stream)) {
                            var content = (UIElement)XamlReader.Load(reader.ReadToEnd());
                            foreach(var image in content.ChildrenOfType<Image>())
                                image.Source = new BitmapImage(Core.Utils.MakeServerUri(((BitmapImage)image.Source).UriSource.OriginalString));
                            navigationItem.Content = content;
                        }
                }

                this.navigationItems.Add(navigationItem);
            }

            this.MainTileView.ItemsSource = null;
            this.MainTileView.ItemsSource = this.navigationItems;
            var tabItem = this.ParentOfType<RadTabItem>();
            if(tabItem != null)
                tabItem.Visibility = this.navigationItems.Count == 0 ? Visibility.Collapsed : Visibility.Visible;
        }

        private void SmallContent_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            var tileViewItem = ((UIElement)sender).ParentOfType<RadTileViewItem>();
            if(tileViewItem != null)
                tileViewItem.TileState = TileViewItemState.Maximized;
        }

        public NavigationView() {
            this.InitializeComponent();
        }
    }
}
