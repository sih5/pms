﻿using System;
using Sunlight.Silverlight.ViewModel.Contract;

namespace Sunlight.Silverlight.View {
    public partial class OperationView {
        private IOperationGroupViewModel viewModel;

        public IOperationGroupViewModel ViewModel {
            get {
                return this.viewModel;
            }
            set {
                if(value == null)
                    throw new ArgumentNullException("value");
                this.DataContext = this.viewModel = value;
            }
        }

        public OperationView() {
            this.InitializeComponent();
        }
    }
}
