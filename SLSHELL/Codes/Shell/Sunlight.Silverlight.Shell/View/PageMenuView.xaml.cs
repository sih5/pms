﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Model;

namespace Sunlight.Silverlight.View {
    public partial class PageMenuView {
        private static void CopyMenuItem(MenuItem source, MenuItem target) {
            target.Id = source.Id;
            target.Name = source.Name;
            target.Description = source.Description;
            target.DisplayName = source.DisplayName;
            target.ImageUri = source.ImageUri;
        }

        private SystemMenuItem rootMenuItem, filteredRootMenuItem;

        public SystemMenuItem RootMenuItem {
            get {
                return this.rootMenuItem;
            }
            set {
                this.rootMenuItem = value;
                this.DataContext = null;
                this.DataContext = this.filteredRootMenuItem ?? this.rootMenuItem;
            }
        }

        private void RefreshFilter(string filterText) {
            if(string.IsNullOrEmpty(filterText) || this.rootMenuItem == null)
                this.filteredRootMenuItem = null;
            else {
                this.filteredRootMenuItem = new SystemMenuItem();
                CopyMenuItem(this.rootMenuItem, this.filteredRootMenuItem);
                var filteredGroupMenuItems = new List<GroupMenuItem>(this.rootMenuItem.GroupMenuItems.Count());
                foreach(var groupMenuItem in this.rootMenuItem.GroupMenuItems) {
                    var pageMenuItems = groupMenuItem.PageMenuItems.Where(item => (item.DisplayName != null && item.DisplayName.ToLower().Contains(filterText.ToLower())) || (item.Description != null && item.Description.ToLower().Contains(filterText.ToLower()))).ToArray();
                    if(pageMenuItems.Length == 0)
                        continue;
                    var filteredGroupMenuItem = new GroupMenuItem {
                        SystemMenuItem = this.filteredRootMenuItem,
                    };
                    CopyMenuItem(groupMenuItem, filteredGroupMenuItem);
                    foreach(var pageMenuItem in pageMenuItems)
                        pageMenuItem.GroupMenuItem = filteredGroupMenuItem;
                    filteredGroupMenuItem.PageMenuItems = pageMenuItems;
                    filteredGroupMenuItems.Add(filteredGroupMenuItem);
                }
                this.filteredRootMenuItem.GroupMenuItems = filteredGroupMenuItems.ToArray();
            }
            this.RootMenuItem = this.rootMenuItem;
        }

        private void MainSearchTextBox_SearchTextChanged(object sender, EventArgs e) {
            this.RefreshFilter(this.MainSearchTextBox.SearchText);
        }

        public PageMenuView() {
            this.InitializeComponent();
        }
    }
}
