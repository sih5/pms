﻿using System;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Core.View;

namespace Sunlight.Silverlight.View {
    public abstract class QueryPanelBase : FilterPanelRibbonGroup, IQueryPanel {
        private ViewInitializer initializer;

        protected ViewInitializer Initializer {
            get {
                return this.initializer ?? (this.initializer = new ViewInitializer(this));
            }
        }

        protected QueryPanelBase() {
            this.AcceptEmptyFilter = true;
        }

        protected internal virtual void SetRootView(IRootView rootView) {
            if(rootView == null)
                throw new ArgumentNullException("rootView");
        }

        public virtual object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            return null;
        }
    }
}
