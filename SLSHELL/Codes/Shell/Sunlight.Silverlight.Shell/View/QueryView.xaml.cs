﻿using System;
using System.Threading;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Resources;
using Sunlight.Silverlight.ViewModel.Contract;

namespace Sunlight.Silverlight.View {
    public partial class QueryView {
        private IQueryViewModel viewModel;

        public IQueryViewModel ViewModel {
            get {
                return this.viewModel;
            }
            set {
                if(value == null)
                    throw new ArgumentNullException("value");
                if(this.viewModel != null)
                    this.viewModel.QueryCommand.CanExecuteChanged -= this.QueryCommand_CanExecuteChanged;
                this.DataContext = this.viewModel = value;
                this.viewModel.QueryCommand.CanExecuteChanged += this.QueryCommand_CanExecuteChanged;
                this.MainFilterPanel.CreateFilterControls(this.viewModel.Parent.EntityType, this.viewModel.QueryItems);
            }
        }

        private void QueryCommand_CanExecuteChanged(object sender, EventArgs e) {
            this.QueryButton.IsEnabled = this.ViewModel.QueryCommand.CanExecute(null);
        }

        private void QueryButton_Click(object sender, RoutedEventArgs e) {
            this.ExecuteQuery();
        }

        private void MainFilterPanel_FiltersAccepted(object sender, EventArgs e) {
            this.ExecuteQuery();
        }

        private void ExecuteQueryInternal(object parameters) {
            var startTime = DateTime.Now;
            while(!this.ViewModel.QueryCommand.CanExecute(parameters)) {
                if((DateTime.Now - startTime).TotalSeconds > 4)
                    return;
                Thread.Sleep(100);
            }
            this.Dispatcher.BeginInvoke(() => this.ViewModel.QueryCommand.Execute(parameters));
        }

        public bool ExecuteQuery(bool isAutoLoad = false) {
            var parameters = this.MainFilterPanel.GetQueryParameters();
            if(parameters == null && !this.ViewModel.Parent.AllowEmptyFilters) {
                if(!isAutoLoad)
                    UIHelper.ShowAlertMessage(ShellUIStrings.QueryView_Error_EmptyFilter);
                return false;
            }

            if(!this.ViewModel.QueryCommand.CanExecute(parameters)) {
                ThreadPool.QueueUserWorkItem(this.ExecuteQueryInternal, parameters);
                return true;
            }

            this.ViewModel.QueryCommand.Execute(parameters);
            return true;
        }

        public QueryView() {
            this.InitializeComponent();
        }
    }
}
