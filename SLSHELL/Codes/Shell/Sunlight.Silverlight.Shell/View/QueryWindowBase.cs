﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;

namespace Sunlight.Silverlight.View {
    public abstract class QueryWindowBase : UserControlBase, IQueryWindow {
        private ViewInitializer initializer;
        private QueryPanelBase queryPanel;
        private DataGridViewBase dataGridView;
        private Entity selectedEntityByDoubleClick;
        public event EventHandler SelectionChanged;
        public event EventHandler SelectionDecided;

        protected ViewInitializer Initializer {
            get {
                return this.initializer ?? (this.initializer = new ViewInitializer(this));
            }
        }

        protected QueryPanelBase QueryPanel {
            get {
                if(this.queryPanel == null && !string.IsNullOrEmpty(this.QueryPanelKey)) {
                    this.queryPanel = DI.GetQueryPanel(this.QueryPanelKey);
                    this.queryPanel.ExecutingQuery += this.QueryPanel_ExecutingQuery;
                }
                return this.queryPanel;
            }
        }

        protected DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null && !string.IsNullOrEmpty(this.DataGridViewKey)) {
                    this.dataGridView = DI.GetDataGridView(this.DataGridViewKey);
                    this.dataGridView.SelectionChanged += this.DataGridView_SelectionChanged;
                    this.dataGridView.RowDoubleClick += this.DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }

        public abstract string QueryPanelKey {
            get;
        }

        public abstract string DataGridViewKey {
            get;
        }

        public IEnumerable<Entity> SelectedEntities {
            get {
                if(this.selectedEntityByDoubleClick != null)
                    return new[] {
                        this.selectedEntityByDoubleClick
                    };
                return this.DataGridView == null ? Enumerable.Empty<Entity>() : this.DataGridView.SelectedEntities;
            }
        }

        private void RaiseSelectionChanged() {
            var handler = this.SelectionChanged;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        private void RaiseSelectionDecided() {
            var handler = this.SelectionDecided;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        private void QueryPanel_ExecutingQuery(object sender, FilterPanelRibbonGroup.ExecutingQueryEventArgs e) {
            if(this.DataGridView == null)
                return;
            
            this.DataGridView.FilterItem = this.OnRequestFilterItem(e.Filter);
            this.DataGridView.ExecuteQueryDelayed();
        }

        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            this.selectedEntityByDoubleClick = e.Row.DataContext as Entity;
            if(this.selectedEntityByDoubleClick == null)
                return;

            this.RaiseSelectionChanged();
            this.RaiseSelectionDecided();
        }

        private void DataGridView_SelectionChanged(object sender, EventArgs e) {
            this.selectedEntityByDoubleClick = null;
            this.RaiseSelectionChanged();
        }

        protected virtual FilterItem OnRequestFilterItem(FilterItem filterItem) {
            return filterItem;
        }     

        public virtual object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new NotSupportedException();
        }
    }
}
