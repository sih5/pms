﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.ViewModel.Contract;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.DomainServices;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.View {
    public partial class QueryWindowView : IGeneralView {
        private const double DOUBLE_CLICK_POSITION_DEVIATION = 4.0;
        private static readonly TimeSpan DoubleClickThreshold = TimeSpan.FromMilliseconds(450);
        private bool initialized;
        private IQueryWindowViewModel viewModel;
        private QueryView[] queryViews;
        private DateTime lastClickTime;
        private Point lastClickPosition;
        private QueryItemGroup lastQueryItemGroup;
        private FilterItem lastFilterItem;
        private Dictionary<QueryItemGroup, IQueryViewModel> queryViewModelTable;
        public event EventHandler SelectionAccepted;
        public event EventHandler Closed;

        private IEnumerable<QueryView> QueryViews {
            get {
                if(this.ViewModel.QueryViewModels == null)
                    return Enumerable.Empty<QueryView>();
                return this.queryViews ?? (this.queryViews = this.ViewModel.QueryViewModels.Select(queryViewModel => new QueryView {
                    ViewModel = queryViewModel
                }).ToArray());
            }
        }

        public string PageTitle {
            get {
                return this.ViewModel == null ? null : this.ViewModel.PageTitle;
            }
        }

        public bool ShowTitleBar {
            get {
                return this.TitleBar.Visibility == Visibility.Visible;
            }
            set {
                this.TitleBar.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public IEnumerable SelectedItems {
            get;
            private set;
        }

        public IQueryWindowViewModel ViewModel {
            get {
                return this.viewModel;
            }
            set {
                if(ReferenceEquals(this.viewModel, value))
                    return;
                if(this.viewModel != null)
                    this.viewModel.RequestingExecuteLastQuery -= this.ViewModel_RequestingExecuteLastQuery;
                if(value != null)
                    value.RequestingExecuteLastQuery += this.ViewModel_RequestingExecuteLastQuery;
                this.DataContext = this.viewModel = value;
                this.MainDataGrid.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            }
        }

        private void RaiseSelectionAccepted() {
            var handler = this.SelectionAccepted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        private void RaiseClosed() {
            var handler = this.Closed;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        private IEnumerable<QueryItemGroup> CreateQueryItemGroups() {
            var queryItemGroups = new List<QueryItemGroup>(this.ViewModel.QueryViewModels.Count());
            this.queryViewModelTable = new Dictionary<QueryItemGroup, IQueryViewModel>(queryItemGroups.Capacity);
            foreach(var queryViewModel in this.ViewModel.QueryViewModels) {
                var queryItemGroup = new QueryItemGroup {
                    UniqueId = queryViewModel.Name,
                    Title = queryViewModel.Name,
                    EntityType = this.ViewModel.EntityType,
                    QueryItems = queryViewModel.QueryItems,
                };
                queryItemGroups.Add(queryItemGroup);
                this.queryViewModelTable.Add(queryItemGroup, queryViewModel);
            }
            return queryItemGroups;
        }

        private void ExecuteQuery(QueryItemGroup queryItemGroup, FilterItem filter) {
            if(filter == null)
                return;

            var queryViewModel = this.queryViewModelTable[queryItemGroup];

            this.MainDataSource.QueryParameters.Clear();
            var parameters = this.ViewModel.GetQueryFunctionParameters(queryViewModel, filter);
            if(parameters != null)
                foreach(var kv in parameters)
                    this.MainDataSource.QueryParameters.Add(new QueryParameter {
                        ParameterName = kv.Key,
                        Value = kv.Value
                    });
            this.MainDataSource.FilterDescriptors.Clear();
            var filterDescriptors = this.ViewModel.GetFilterDescriptors(queryViewModel, filter);
            if(filterDescriptors != null || (!filter.IsEmpty && (filterDescriptors = filter.ToFilterDescriptors()) != null))
                this.MainDataSource.FilterDescriptors.AddRange(filterDescriptors);

            this.MainDataSource.Load();
            this.lastQueryItemGroup = queryItemGroup;
            this.lastFilterItem = filter;
        }

        private void ExecuteLastQuery() {
            if(this.lastQueryItemGroup != null && this.lastFilterItem != null)
                this.ExecuteQuery(this.lastQueryItemGroup, this.lastFilterItem);
        }

        #region Original architecture events
        private void MainDataGrid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
            var element = sender as UIElement;
            if(element == null)
                return;
            var position = e.GetPosition(element);
            if(DateTime.Now - this.lastClickTime <= DoubleClickThreshold && Math.Abs(this.lastClickPosition.X - position.X) < DOUBLE_CLICK_POSITION_DEVIATION && Math.Abs(this.lastClickPosition.Y - position.Y) < DOUBLE_CLICK_POSITION_DEVIATION) {
                this.lastClickTime = DateTime.MinValue;
                this.lastClickPosition = new Point();
                if(VisualTreeHelper.FindElementsInHostCoordinates(e.GetPosition(null), this.MainDataGrid).OfType<GridViewRow>().Any())
                    this.RaiseSelectionAccepted();
            } else {
                this.lastClickTime = DateTime.Now;
                this.lastClickPosition = position;
            }
        }

        private void MainDataGrid_RowLoaded(object sender, RowLoadedEventArgs e) {
            e.Row.KeyDown += (_, args) => {
                if(args.Key != Key.Enter)
                    return;
                args.Handled = true;
                this.RaiseSelectionAccepted();
            };
        }

        private void MainDataGrid_LoadingRowDetails(object sender, GridViewRowDetailsEventArgs e) {
            BusinessViewUtils.ProcessDetailPanel(this.ViewModel, e.DetailsElement as RadTabControl);
        }

        private void MainDataGrid_SelectionChanged(object sender, SelectionChangeEventArgs e) {
            this.SelectedItems = this.MainDataGrid.SelectedItems;
        }
        #endregion

        #region New view architecture events
        private void ViewModel_RequestingExecuteLastQuery(object sender, EventArgs e) {
            this.ExecuteLastQuery();
        }

        private void MainGridView_RequestingDetailPanelContent(object sender, GridViewRequestingDetailPanelContentEventArgs e) {
            object header;
            UIElement content;
            this.ViewModel.GetDetailPanel(this.ViewModel.Panels.Single(p => p.UniqueId == e.UniqueId), e.DataContext, out header, out content);
            e.Header = header;
            e.Content = content;
        }

        private void MainGridView_SelectionChanged(object sender, SelectionChangeEventArgs e) {
            this.SelectedItems = this.MainGridView.SelectedItems;
        }

        private void MainGridView_RowDoubleClick(object sender, GridViewRowDoubleClickEventArgs e) {
            if(e.Row.DataContext == null)
                return;
            this.SelectedItems = new[] {
                e.Row.DataContext
            };
            this.RaiseSelectionAccepted();
        }

        private void MainGridView_RowKeyDown(object sender, GridViewRowKeyDownEventArgs e) {
            if(e.Key != Key.Enter || e.Row.DataContext == null)
                return;
            this.SelectedItems = new[] {
                e.Row.DataContext
            };
            this.RaiseSelectionAccepted();
        }
        #endregion

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            this.Close();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e) {
            if(this.ViewModel == null || this.initialized)
                return;

            try {
                if(string.IsNullOrWhiteSpace(this.ViewModel.QueryFunctionName)) {
                    this.MainGridView.Visibility = Visibility.Collapsed;
                    this.MainDataGrid.SetColumns(this.ViewModel.EntityType, this.ViewModel.Columns);
                    this.MainDataGrid.CreateDetailPanel(this.ViewModel.EntityType, this.ViewModel.Panels);
                    foreach(var queryView in this.QueryViews)
                        this.MainStackPanel.Children.Add(queryView);
                } else {
                    this.MainDataGrid.Visibility = this.MainDataPager.Visibility = this.PageText.Visibility = Visibility.Collapsed;
                    this.MainDataSource.QueryName = this.ViewModel.QueryFunctionName;
                    this.MainGridView.SetColumns(this.ViewModel.EntityType, this.ViewModel.Columns);
                    this.MainGridView.SetDetailPanels(this.ViewModel.Panels);
                    var filterPanelRibbonGroup = new FilterPanelRibbonGroup {
                        QueryItemGroups = this.CreateQueryItemGroups(),
                        AcceptEmptyFilter = this.ViewModel.AllowEmptyFilters,
                    };
                    filterPanelRibbonGroup.ExecutingQuery += (o, args) => this.ExecuteQuery(args.QueryItemGroup, args.Filter);
                    this.MainStackPanel.Children.Add(filterPanelRibbonGroup);
                }
            } catch(Exception ex) {
                this.FatalError(ex.Message);
            } finally {
                this.initialized = true;
            }
        }

        private void MainDataSource_LoadingData(object sender, LoadingDataEventArgs e) {
            e.LoadBehavior = LoadBehavior.RefreshCurrent;
        }

        public void Close() {
            var window = this.ParentOfType<RadWindow>();
            if(window != null)
                window.Close();
            else {
                var button = this.ParentOfType<RadDropDownButton>();
                if(button != null)
                    button.IsOpen = false;
                else
                    this.RaiseClosed();
            }
        }

        public QueryWindowView() {
            this.InitializeComponent();
            this.lastClickTime = DateTime.MinValue;
            this.AddHandler(MouseLeftButtonUpEvent, new MouseButtonEventHandler(this.MainDataGrid_MouseLeftButtonUp), true);
        }
    }
}
