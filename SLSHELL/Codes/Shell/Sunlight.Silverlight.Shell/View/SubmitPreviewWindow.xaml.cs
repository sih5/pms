﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;

namespace Sunlight.Silverlight.View {
    public partial class SubmitPreviewWindow {
        public sealed class BindingItem {
            private List<BindingItem> items;

            public string Name {
                get;
                set;
            }

            public object OldValue {
                get;
                set;
            }

            public object NewValue {
                get;
                set;
            }

            public List<BindingItem> Items {
                get {
                    return this.items ?? (this.items = new List<BindingItem>());
                }
            }

            internal BindingItem() {
                //
            }
        }

        private static readonly string[] SystemColumnNames = new[] {
            "Id",
            "ParentId",
            "CreatorId",
            "CreatorName",
            "CreateTime",
            "ModifierId",
            "ModifierName",
            "ModifyTime",
            "CorporationId",
            "CorporationName",
            "RowVersion"
        };

        public DomainContext DomainContext {
            get;
            set;
        }

        public Func<Type, string, int, string> GetValueByKey {
            get;
            set;
        }

        private void RadWindow_Loaded(object sender, RoutedEventArgs e) {
            this.MainBusyIndicator.IsBusy = false;

            if(this.DomainContext == null)
                return;

            var changes = this.DomainContext.EntityContainer.GetChanges();

            var itemsSource = new List<BindingItem>();

            if(changes.ModifiedEntities.Count > 0) {
                var item = new BindingItem {
                    Name = "已修改数据",
                };

                foreach(var entity in changes.ModifiedEntities) {
                    var original = entity.GetOriginal();
                    if(original == null)
                        continue;

                    var entityType = entity.GetType();
                    var subItem = new BindingItem {
                        Name = string.Concat("数据项 - ", entity.ToString()),
                    };

                    foreach(var property in entityType.GetProperties().Where(p => !SystemColumnNames.Contains(p.Name) && p.GetCustomAttributes(typeof(DataMemberAttribute), false).Length > 0)) {
                        var newValue = property.GetValue(entity, null);
                        var oldValue = /* original == null ? newValue : */ property.GetValue(original, null);

                        if(this.GetValueByKey != null && property.PropertyType == typeof(int)) {
                            var newKeyValue = this.GetValueByKey(entityType, property.Name, (int)newValue);
                            var oldKeyValue = Equals(newValue, oldValue) ? newKeyValue : this.GetValueByKey(entityType, property.Name, (int)oldValue);
                            if(newKeyValue != null)
                                newValue = newKeyValue;
                            if(oldKeyValue != null)
                                oldValue = oldKeyValue;
                        }

                        var propertyItem = new BindingItem {
                            Name = Core.Utils.GetEntityLocalizedName(entityType, property.Name),
                            OldValue = oldValue,
                            NewValue = newValue,
                        };
                        subItem.Items.Add(propertyItem);
                    }

                    if(subItem.Items.Count > 0)
                        item.Items.Add(subItem);
                }

                if(item.Items.Count > 0)
                    itemsSource.Add(item);
            }

            if(changes.AddedEntities.Count > 0) {
                var item = new BindingItem {
                    Name = "已新增数据",
                };

                foreach(var entity in changes.AddedEntities) {
                    var entityType = entity.GetType();
                    var subItem = new BindingItem {
                        Name = string.Concat("数据项 - ", entity.ToString()),
                    };

                    foreach(var property in entityType.GetProperties().Where(p => !SystemColumnNames.Contains(p.Name) && p.GetCustomAttributes(typeof(DataMemberAttribute), false).Length > 0)) {
                        var newValue = property.GetValue(entity, null);

                        if(this.GetValueByKey != null && property.PropertyType == typeof(int)) {
                            var newKeyValue = this.GetValueByKey(entityType, property.Name, (int)newValue);
                            if(newKeyValue != null)
                                newValue = newKeyValue;
                        }

                        var propertyItem = new BindingItem {
                            Name = Core.Utils.GetEntityLocalizedName(entityType, property.Name),
                            NewValue = newValue,
                        };
                        subItem.Items.Add(propertyItem);
                    }

                    if(subItem.Items.Count > 0)
                        item.Items.Add(subItem);
                }

                if(item.Items.Count > 0)
                    itemsSource.Add(item);
            }

            if(changes.RemovedEntities.Count > 0) {
                var item = new BindingItem {
                    Name = "已删除数据",
                };

                foreach(var entity in changes.RemovedEntities) {
                    var entityType = entity.GetType();
                    var subItem = new BindingItem {
                        Name = string.Concat("数据项 - ", entity.ToString()),
                    };

                    foreach(var property in entityType.GetProperties().Where(p => !SystemColumnNames.Contains(p.Name) && p.GetCustomAttributes(typeof(DataMemberAttribute), false).Length > 0)) {
                        var oldValue = property.GetValue(entity, null);

                        if(this.GetValueByKey != null && property.PropertyType == typeof(int)) {
                            var oldKeyValue = this.GetValueByKey(entityType, property.Name, (int)oldValue);
                            if(oldKeyValue != null)
                                oldValue = oldKeyValue;
                        }

                        var propertyItem = new BindingItem {
                            Name = Core.Utils.GetEntityLocalizedName(entityType, property.Name),
                            OldValue = oldValue,
                        };
                        subItem.Items.Add(propertyItem);
                    }

                    if(subItem.Items.Count > 0)
                        item.Items.Add(subItem);
                }

                if(item.Items.Count > 0)
                    itemsSource.Add(item);
            }

            this.MainTreeListView.ItemsSource = itemsSource;

            this.Dispatcher.BeginInvoke(() => this.MainTreeListView.ExpandAllHierarchyItems());
        }

        private void YesButton_Click(object sender, RoutedEventArgs e) {
            this.MainBusyIndicator.IsBusy = true;
            var entityChangeSet = this.DomainContext.EntityContainer.GetChanges();
            this.DomainContext.SubmitChanges(submitOp => {
                this.MainBusyIndicator.IsBusy = false;
                if(submitOp.HasError) {
                    UIHelper.ShowAlertMessage(submitOp.Error.Message, submitOp.Error);
                    submitOp.MarkErrorAsHandled();
                    return;
                }
                if(submitOp.UserState != null)
                    foreach(var entity in (IEnumerable<Entity>)submitOp.UserState)
                        this.DomainContext.EntityContainer.GetEntitySet(entity.GetType()).Detach(entity);
                this.Close();
            }, entityChangeSet == null ? null : entityChangeSet.ModifiedEntities);
        }

        private void NoButton_Click(object sender, RoutedEventArgs e) {
            this.Close();
        }

        public SubmitPreviewWindow() {
            this.InitializeComponent();
        }
    }
}
