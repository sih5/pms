﻿using System.Threading;
using System.Windows.Controls;
using System.Windows.Markup;

namespace Sunlight.Silverlight.View {
    public class UserControlBase : UserControl {
        public UserControlBase() {
            this.Language = XmlLanguage.GetLanguage(Thread.CurrentThread.CurrentUICulture.Name);
        }
    }
}
