﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Sunlight.Silverlight.View {
    public sealed class ViewInitializer {
        private bool initialized;
        private List<Action> initializeActions;

        private void View_Loaded(object sender, RoutedEventArgs e) {
            if(this.initialized || this.initializeActions == null)
                return;
            this.initialized = true;
            foreach(var action in this.initializeActions)
                action();
        }

        public ViewInitializer(FrameworkElement view) {
            view.Loaded += this.View_Loaded;
        }

        public void Register(Action action) {
            if(action == null)
                throw new ArgumentNullException("action");
            if(this.initializeActions == null)
                this.initializeActions = new List<Action>();
            if(!this.initializeActions.Contains(action))
                this.initializeActions.Add(action);
        }
    }
}
