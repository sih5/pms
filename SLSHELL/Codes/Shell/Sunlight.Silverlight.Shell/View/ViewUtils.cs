﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Resources;
using Sunlight.Silverlight.ViewModel.Contract;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.View {
    public static class BusinessViewUtils {
        public static RadGridView CreateDetailGridView(IDataGridViewModel viewModel, DetailPanelItem detailPanelItem, object bindingEntity) {
            if(viewModel.Columns == null)
                return null;

            var detailProperty = viewModel.EntityType.GetProperty(detailPanelItem.Column.Name);
            if(detailProperty == null || !detailProperty.PropertyType.IsGenericType || detailProperty.PropertyType.GetGenericTypeDefinition().FullName != "System.ServiceModel.DomainServices.Client.EntityCollection`1")
                return null;

            var detailType = detailProperty.PropertyType.GetGenericArguments()[0];
            var changeSet = viewModel.DomainContext.EntityContainer.GetEntitySet(detailType);
            var detailContainer = detailProperty.GetValue(bindingEntity, null);

            var result = new RadGridView {
                FontSize = 12,
                Margin = new Thickness(2),
                AutoGenerateColumns = false,
                CanUserFreezeColumns = false,
                CanUserResizeColumns = false,
                ShowGroupPanel = false,
                IsReadOnly = detailPanelItem.IsReadOnly,
                CanUserInsertRows = !detailPanelItem.IsReadOnly,
                CanUserDeleteRows = !detailPanelItem.IsReadOnly,
                SelectionMode = System.Windows.Controls.SelectionMode.Extended,
                SelectionUnit = GridViewSelectionUnit.Cell,
                EnableRowVirtualization = false,
                EnableColumnVirtualization = false,
                RowIndicatorVisibility = Visibility.Collapsed,
            };
            result.AddingNewDataItem += (sender, e) => {
                var newObject = viewModel.CreateNewEntity(detailType);
                if(newObject != null)
                    e.NewObject = newObject;
            };
            result.RowValidating += (sender, e) => {
                var validationResult = viewModel.ValidateEntity((Entity)e.Row.DataContext);
                if(validationResult == null)
                    return;
                e.IsValid = false;
                e.ValidationResults.Add(new GridViewCellValidationResult {
                    ErrorMessage = validationResult.ErrorMessage,
                    PropertyName = validationResult.MemberNames.FirstOrDefault(),
                });
            };
            result.RowEditEnded += (sender, e) => {
                if(e.EditAction == GridViewEditAction.Cancel)
                    return;
                if(e.EditOperationType == GridViewEditOperationType.Insert)
                    viewModel.AddingDetailEntity(detailPanelItem.Column.Name, (Entity)bindingEntity, (Entity)e.NewData);
            };
            result.Deleting += (sender, e) => {
                if((!changeSet.CanRemove && e.Items.Cast<Entity>().Any(item => item.EntityState != EntityState.New)) || e.Items.Cast<Entity>().Any(item => !viewModel.CanRemoveEntity(item)))
                    e.Cancel = true;
            };
            result.Deleted += (sender, e) => {
                foreach(Entity item in e.Items)
                    viewModel.RemovingDetailEntity(detailPanelItem.Column.Name, (Entity)bindingEntity, item);
            };

            result.SetColumns(detailType, detailPanelItem.Column.SubColumns);
            result.ItemsSource = Activator.CreateInstance(typeof(ObservableCollection<>).MakeGenericType(detailType), detailContainer);

            return result;
        }

        public static void ProcessDetailPanel(IDataGridViewModel viewModel, RadTabControl tabControl) {
            if(tabControl == null)
                return;

            var tabItems = tabControl.Items.Cast<RadTabItem>().ToArray();
            Action<Type, Func<RadTabItem, PanelItem, UIElement>> process = (type, processTab) => {
                var name = type.Name;
                foreach(var tab in tabItems.Where(ti => ti.Name.StartsWith(name))) {
                    var index = int.Parse(tab.Name.Replace(name, null));
                    tab.Content = processTab(tab, viewModel.Panels.Skip(index).First());
                }
            };

            process(typeof(DetailPanelItem), (tab, panelItem) => CreateDetailGridView(viewModel, (DetailPanelItem)panelItem, tab.DataContext));
        }

        public static int GetPageId(this Page page) {
            if(page == null || page.NavigationContext == null)
                return 0;

            int result;
            return int.TryParse(page.NavigationContext.QueryString["id"], out result) ? result : 0;
        }

        public static string[] BreakDownParameters(this NavigationContext context) {
            if(!context.QueryString.ContainsKey("param"))
                return new string[0];

            return context.QueryString["param"].Split(new[] {
                '|'
            }, StringSplitOptions.RemoveEmptyEntries);
        }

        /// <summary>
        ///     如果发生导致该View不可能正常运行的错误，则使用该函数显示错误信息
        /// </summary>
        /// <param name="control"> </param>
        /// <param name="errorMessage"> 错误信息 </param>
        public static void FatalError(this UserControl control, string errorMessage) {
            control.Content = new TextBox {
                FontSize = 18,
                IsReadOnly = true,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                TextWrapping = TextWrapping.Wrap,
                Padding = new Thickness(10),
                BorderThickness = new Thickness(0),
                Text = errorMessage,
            };
        }

        public static T CreateViewModelFromQueryString<T>(this Page page) {
            //获取传入的参数
            var paramArray = page.NavigationContext.BreakDownParameters();
            if(paramArray.Length == 0)
                return default(T);

            //载入ViewModel
            var viewModelType = Type.GetType(paramArray[0], false, true);
            if(viewModelType == null) {
                FatalError(page, string.Format(ShellUIStrings.View_FatalError1, paramArray[0]));
                return default(T);
            }
            if(!typeof(T).IsAssignableFrom(viewModelType)) {
                FatalError(page, string.Format(ShellUIStrings.View_FatalError2, viewModelType.FullName, typeof(T).FullName));
                return default(T);
            }
            return (T)Activator.CreateInstance(viewModelType);
        }

        public static void CreateDetailPanel(this GridViewDataControl dataGrid, Type entityType, IEnumerable<PanelItem> panelItems) {
            if(dataGrid == null)
                throw new ArgumentNullException("dataGrid");
            if(entityType == null)
                throw new ArgumentNullException("entityType");
            if(panelItems == null)
                throw new ArgumentNullException("panelItems");

            var sbTemplate = new StringBuilder();
            sbTemplate.Append("<DataTemplate xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\" xmlns:telerik=\"http://schemas.telerik.com/2008/xaml/presentation\">");
            sbTemplate.Append("<telerik:RadTabControl BackgroundVisibility=\"Collapsed\" Margin=\"28,0,4,4\" Background=\"Transparent\">");
            var index = 0;
            foreach(var panelItem in panelItems) {
                string header, content = null;
                switch(panelItem.GetType().FullName) {
                    case "Sunlight.Silverlight.Core.Model.DetailPanelItem":
                        var detailPanelItem = (DetailPanelItem)panelItem;
                        header = string.IsNullOrEmpty(detailPanelItem.Header) ? Utils.GetEntityLocalizedName(entityType, detailPanelItem.Column.Name) : string.Format(detailPanelItem.Header, Utils.GetEntityLocalizedName(entityType, detailPanelItem.Column.Name));
                        break;
                    case "Sunlight.Silverlight.Core.Model.CustomPanelItem":
                        var customPanelItem = (CustomPanelItem)panelItem;
                        header = customPanelItem.Header;
                        var assemblyName = customPanelItem.ContentType.Assembly.FullName.Split(',').First();
                        content = string.Format("<custom:{0} xmlns:custom=\"clr-namespace:{1};assembly={2}\" />", customPanelItem.ContentType.Name, customPanelItem.ContentType.Namespace, assemblyName);
                        break;
                    default:
                        continue;
                }
                sbTemplate.AppendFormat("<telerik:RadTabItem x:Name=\"{0}{1}\" Header=\"{2}\" Margin=\"2,0,0,0\" Height=\"24\">{3}</telerik:RadTabItem>", panelItem.GetType().Name, index++, header, content);
            }
            sbTemplate.Append("</telerik:RadTabControl>");
            sbTemplate.Append("</DataTemplate>");
            dataGrid.RowDetailsTemplate = (DataTemplate)XamlReader.Load(sbTemplate.ToString());
            dataGrid.RowDetailsVisibilityMode = GridViewRowDetailsVisibilityMode.Collapsed;

            if(index > 0 && !dataGrid.Columns.OfType<GridViewToggleRowDetailsColumn>().Any())
                dataGrid.Columns.Insert(0, new GridViewToggleRowDetailsColumn());
        }

        public static UIElement MakeTabHeader(Uri imageUri, string title) {
            if(imageUri == null)
                throw new ArgumentNullException("imageUri");
            if(title == null)
                throw new ArgumentNullException("title");

            var result = new StackPanel {
                Orientation = Orientation.Horizontal
            };
            result.Children.Add(new Image {
                Source = new BitmapImage(imageUri),
                Stretch = Stretch.None,
            });
            result.Children.Add(new TextBlock {
                Margin = new Thickness(4, 0, 0, 0),
                Text = title,
            });
            return result;
        }
    }
}
