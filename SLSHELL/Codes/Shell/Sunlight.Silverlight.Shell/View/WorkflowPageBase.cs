﻿using System.ComponentModel;

namespace Sunlight.Silverlight.View {
    public class WorkflowPageBase : UserControlBase, INotifyPropertyChanged {
        private ViewInitializer initializer;
        public event PropertyChangedEventHandler PropertyChanged;

        protected ViewInitializer Initializer {
            get {
                return this.initializer ?? (this.initializer = new ViewInitializer(this));
            }
        }

        public virtual bool IsValid {
            get {
                return true;
            }
        }

        protected void NotifyPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void NotifyIsValidChanged() {
            this.NotifyPropertyChanged("IsValid");
        }

        public virtual void Reset() {
            //
        }
    }
}
