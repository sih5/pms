﻿using System;
using System.Collections;
using System.Windows.Input;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.ViewModel.Contract;

namespace Sunlight.Silverlight.ViewModel {
    /// <summary>
    ///     用于执行内建命令的 <see cref = "IOperationViewModel" />。
    /// </summary>
    public class BuiltInOperationViewModel : ViewModelBase, IOperationViewModel {
        public object CommandParameter {
            get;
            set;
        }

        bool IOperationViewModel.IsAuthorized {
            get;
            set;
        }

        /// <inheritdoc />
        public string UniqueId {
            get;
            set;
        }

        /// <inheritdoc />
        public string Name {
            get;
            set;
        }

        /// <inheritdoc />
        public Uri ImageUri {
            get;
            set;
        }

        /// <inheritdoc />
        public IEnumerable SelectedEntities {
            get;
            set;
        }

        /// <inheritdoc />
        public ICommand ExecuteCommand {
            get;
            set;
        }

        /// <inheritdoc />
        public override void Validate() {
            throw new NotSupportedException();
        }
    }
}
