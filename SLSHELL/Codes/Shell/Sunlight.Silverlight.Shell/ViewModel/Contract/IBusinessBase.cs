﻿using System.ComponentModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.ViewModel.Contract {
    /// <summary>
    ///     该接口定义了与业务用 ViewModel 相关的属性。
    /// </summary>
    public interface IBusinessBase : INotifyPropertyChanged {
        /// <summary>
        ///     用于该页面中主要数据操作的 <see cref = "System.ServiceModel.DomainServices.Client.DomainContext">DomainContext</see> 实例。
        /// </summary>
        DomainContext DomainContext {
            get;
        }

        /// <summary>
        ///     获取页面的标题。
        /// </summary>
        string PageTitle {
            get;
        }

        /// <summary>
        ///     获取页面在 Ribbon 标签栏的上下文分组名称。
        /// </summary>
        string RibbonContextualName {
            get;
        }

        /// <summary>
        ///     获取或设置一个自定义对象，不同的 View 可能对该属性的内容有着不同的要求。
        /// </summary>
        object UserObject {
            get;
            set;
        }
    }
}
