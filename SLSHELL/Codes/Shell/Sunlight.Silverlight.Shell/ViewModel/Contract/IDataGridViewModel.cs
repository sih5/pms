﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.ViewModel.Contract {
    /// <summary>
    ///     DataGridView 的 ViewModel 接口，业务系统通过实现该接口创建具有业务功能的 DataGridView。
    /// </summary>
    public interface IDataGridViewModel : IQueryableViewModel {
        /// <summary>
        ///     获取一个值，用于指定数据表格是否为只读。如果只读，则数据无法做新增修改以及删除操作。
        /// </summary>
        bool IsReadOnly {
            get;
        }

        /// <summary>
        ///     获取或设置一个值，指定当前View上的显示内容。如果值为空，则显示默认的 DataGrid 界面。
        /// </summary>
        UserControl Content {
            get;
            set;
        }

        /// <summary>
        ///     获取一个值，用于指定在树形结构下，子级集合的字段名称。
        /// </summary>
        /// <remarks>
        ///     当该属性不为空值时，数据表格会变为树形数据表格。
        /// </remarks>
        string TreeChildrenColumnName {
            get;
        }

        /// <summary>
        ///     获取一个 <see cref="Action{T}" /> ，当树形数据表格上某一行数据被展开时，会自动调用该 <c>Action</c> 获取子级的数据。
        /// </summary>
        Action<Entity> TreeLoadChildren {
            get;
        }

        /// <summary>
        ///     获取该界面上的数据表格的列定义集合。
        /// </summary>
        IEnumerable<ColumnItem> Columns {
            get;
        }

        /// <summary>
        ///     获取该界面上的数据表格的清单面板配置集合。
        /// </summary>
        IEnumerable<PanelItem> Panels {
            get;
        }

        /// <summary>
        ///     获取该界面上的数据操作分组的配置集合。
        /// </summary>
        IEnumerable<IOperationGroupViewModel> OperationGroupViewModels {
            get;
        }

        /// <summary>
        ///     当用户需要增加一条新记录时，该方法负责返回新记录的示例。业务系统可以在该方法内设定新记录的初始值。
        /// </summary>
        /// <param name="entityType"> 新记录的实体类型。业务系统需要判断该类型是主单还是清单，以避免创建错误的实例。 </param>
        /// <returns> 新记录的实例。 </returns>
        Entity CreateNewEntity(Type entityType);

        /// <summary>
        ///     当系统需要验证某一个实体时会调用此方法，业务系统有机会在该方法内对实体进行客户端验证并返回验证结果。
        /// </summary>
        /// <param name="entity"> 需要验证的实体。 </param>
        /// <returns> 验证结果，如果返回 null 则表明验证通过。 </returns>
        ValidationResult ValidateEntity(Entity entity);

        /// <summary>
        ///     当系统需要删除一个实体时，会调用此方法判断该实体是否允许删除。
        /// </summary>
        /// <param name="entity"> 即将删除的实体。 </param>
        /// <returns> 如果实体可以删除，则返回 true，否则返回 false。 </returns>
        bool CanRemoveEntity(Entity entity);

        /// <summary>
        ///     当系统正在对某实体进行新增操作时会调用此方法，业务系统应该负责将实体加入相应的 <see cref="DomainContext" /> 内。
        /// </summary>
        /// <param name="entity"> 正在新增的实体实例。 </param>
        void AddingEntity(Entity entity);

        /// <summary>
        ///     当系统正在对某实体进行移除操作时会调用此方法，业务系统应该负责将实体从相应的 <see cref="DomainContext" /> 内移除。
        /// </summary>
        /// <param name="entity"> 正在移除的实体实例。 </param>
        void RemovingEntity(Entity entity);

        /// <summary>
        ///     当系统正在对某实体的关联清单进行新增操作时会调用此方法，业务系统应该负责将实体加入相应的 <see cref="DomainContext" /> 内。
        /// </summary>
        /// <param name="detailColumnName"> 清单在主单上的关联属性名称。 </param>
        /// <param name="headerEntity"> 主单实体的实例。 </param>
        /// <param name="detailEntity"> 正在新增的清单实体的实例。 </param>
        void AddingDetailEntity(string detailColumnName, Entity headerEntity, Entity detailEntity);

        /// <summary>
        ///     当系统正在对某实体的关联清单进行移除操作时会调用此方法，业务系统应该负责将实体从相应的 <see cref="DomainContext" /> 内移除。
        /// </summary>
        /// <param name="detailColumnName"> 清单在主单上的关联属性名称。 </param>
        /// <param name="headerEntity"> 主单实体的实例。 </param>
        /// <param name="detailEntity"> 正在移除的清单实体的实例。 </param>
        void RemovingDetailEntity(string detailColumnName, Entity headerEntity, Entity detailEntity);

        /// <summary>
        ///     当系统正在对表格数据进行导出时会调用此方法。
        /// </summary>
        /// <param name="args"> 用于控制导出数据的样式的参数。 </param>
        void CustomExportStyle(GridViewElementExportingEventArgs args);

        void GetDetailPanel(PanelItem panelItem, object dataContext, out object header, out UIElement content);
    }
}
