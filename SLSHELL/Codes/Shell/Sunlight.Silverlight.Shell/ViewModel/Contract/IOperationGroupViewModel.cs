﻿using System.Collections.Generic;

namespace Sunlight.Silverlight.ViewModel.Contract {
    /// <summary>
    ///     用于对界面操作分组进行协定的接口。
    /// </summary>
    public interface IOperationGroupViewModel {
        /// <summary>
        ///     获取一个指定分组名称的值。
        /// </summary>
        string Name {
            get;
        }

        /// <summary>
        ///     获取分组内的界面操作项的配置集合。
        /// </summary>
        IEnumerable<IOperationViewModel> OperationViewModels {
            get;
        }
    }
}
