﻿using System;
using System.Collections;
using System.Windows.Input;

namespace Sunlight.Silverlight.ViewModel.Contract {
    /// <summary>
    ///     操作界面的 ViewModel 接口，框架会使用该接口的实现类来创建页面上的操作界面。
    /// </summary>
    public interface IOperationViewModel {
        /// <summary>
        ///     获取一个值，指定该操作在当前界面中的唯一ID。
        /// </summary>
        string UniqueId {
            get;
        }

        /// <summary>
        ///     获取或设置一个值，指定当前用户是否已被授权使用该操作。
        /// </summary>
        bool IsAuthorized {
            get;
            set;
        }

        /// <summary>
        ///     获取一个值，指定该操作的名称。
        /// </summary>
        string Name {
            get;
        }

        /// <summary>
        ///     获取该操作的图标 <c>Uri</c>。
        /// </summary>
        Uri ImageUri {
            get;
        }

        /// <summary>
        ///     获取或设置一个值，指定该操作所处于的操作界面上所选择的实体集合。
        /// </summary>
        IEnumerable SelectedEntities {
            get;
            set;
        }

        /// <summary>
        ///     获取一个 <see cref = "ICommand" />，当用户选择执行该操作时会执行此命令。
        /// </summary>
        ICommand ExecuteCommand {
            get;
        }
    }
}
