﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.ViewModel.Contract {
    /// <summary>
    ///     查询条件界面的 ViewModel 接口，任何子系统必须在自己系统内实现该接口以支持数据查询。
    /// </summary>
    public interface IQueryViewModel : INotifyPropertyChanged {
        /// <summary>
        ///     获取查询界面的名称。
        /// </summary>
        string Name {
            get;
        }

        /// <summary>
        ///     获取该 ViewModel 所属的 <see cref = "IQueryableViewModel" /> 的实例。
        /// </summary>
        IQueryableViewModel Parent {
            get;
        }

        /// <summary>
        ///     获取一个 <see cref = "QueryItem" /> 集合，用于生成查询条件控件。
        /// </summary>
        IEnumerable<QueryItem> QueryItems {
            get;
        }

        /// <summary>
        ///     获取一个 <see cref = "ICommand" />，用于执行查询。
        /// </summary>
        ICommand QueryCommand {
            get;
        }
    }
}
