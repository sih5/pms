﻿using System;
using System.Windows;

namespace Sunlight.Silverlight.ViewModel.Contract {
    /// <summary>
    ///     弹出式查询界面的 ViewModel 接口，子系统可实现该接口用于创建一个弹出式的查询界面。
    /// </summary>
    public interface IQueryWindowViewModel : IDataGridViewModel {
        event EventHandler RequestingExecuteLastQuery;

        /// <summary>
        ///     获取一个值，用于指定查询项是否可以多选。
        /// </summary>
        bool IsMultiSelection {
            get;
        }

        /// <summary>
        ///     获取一个值，用于指定弹出窗口的绝对大小。默认情况下窗口会自动符合窗口内容的大小。
        /// </summary>
        Size WindowSize {
            get;
        }
    }
}
