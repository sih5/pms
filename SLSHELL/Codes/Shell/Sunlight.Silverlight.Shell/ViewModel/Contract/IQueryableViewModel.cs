﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.ViewModel.Contract {
    /// <summary>
    ///     该接口定义了一个可查询界面所应该具备的属性，任何实现该接口的 ViewModel 都将作为可查询界面。
    /// </summary>
    public interface IQueryableViewModel : IBusinessBase {
        /// <summary>
        ///     获取指定当前 ViewModel 所要查询的实体的类型。
        /// </summary>
        Type EntityType {
            get;
        }

        /// <summary>
        ///     获取一个值，指定是否允许查询条件为空。
        /// </summary>
        bool AllowEmptyFilters {
            get;
        }

        /// <summary>
        ///     获取一个值，指定是否要隐藏查询面板。
        /// </summary>
        bool HideQueryPanel {
            get;
        }

        /// <summary>
        ///     获取一个值，指定是否在界面载入完成后，自动查询第一个 <see cref="IQueryViewModel" /> 的内容。
        /// </summary>
        /// <seealso cref="QueryViewModels" />
        bool IsAutoLoad {
            get;
        }

        /// <summary>
        ///     获取或设置当前 ViewModel 所选定的实体对象。
        /// </summary>
        object SelectedItem {
            get;
            set;
        }

        /// <summary>
        ///     获取或设置当前 ViewModel 的查询结果，集合内的对象的类型必须与 <see cref="EntityType" /> 一致。
        /// </summary>
        IEnumerable QueryResult {
            get;
            set;
        }

        /// <summary>
        ///     获取一个值，用于指定客户端分页，每一页的数据条数大小。
        /// </summary>
        int PageSize {
            get;
        }

        string QueryFunctionName {
            get;
        }

        /// <summary>
        ///     获取当前 ViewModel 的 <see cref="IQueryViewModel" /> 集合，用于指定当前 ViewModel 所支持的查询条件。
        /// </summary>
        IEnumerable<IQueryViewModel> QueryViewModels {
            get;
        }

        IDictionary<string, object> GetQueryFunctionParameters(IQueryViewModel queryViewModel, FilterItem filter);

        IEnumerable<IFilterDescriptor> GetFilterDescriptors(IQueryViewModel queryViewModel, FilterItem filter);
    }
}
