﻿using System;
using System.Globalization;
using System.IO.IsolatedStorage;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using Sunlight.Silverlight.Commands;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Resources;
using Sunlight.Silverlight.Shell;

namespace Sunlight.Silverlight.ViewModel {
    public enum UILanguage {
        Chinese = 2052,
        English = 1033,
        Japanese = 1041,
    }

    public class LoginViewModel : ViewModelBase {
        private const string KEY_LAST_USER_NAME = "LastUserName";
        private const string KEY_LAST_PASSWORD = "LastPassword";
        private const string KEY_LAST_LANGUAGE = "LastLanguage";
        private const string KEY_LAST_ENTERPRISECODE = "LastEnterpriseCode";

        private static IsolatedStorageSettings Settings {
            get {
                try {
                    return IsolatedStorageSettings.ApplicationSettings;
                } catch(IsolatedStorageException) {
                    return null;
                }
            }
        }

        internal static CultureInfo GetUICultureByLanguage(UILanguage language) {
            var locale = "zh-CN";
            switch(language) {
                case UILanguage.English:
                    locale = "en-US";
                    break;
                case UILanguage.Japanese:
                    locale = "ja-JP";
                    break;
            }
            return new CultureInfo(locale);
        }

        internal static void SetCurrentUICulture() {
            UILanguage language;
            if(Settings == null || !Settings.Contains(KEY_LAST_LANGUAGE) || !Enum.TryParse((string)Settings[KEY_LAST_LANGUAGE], out language))
                language = UILanguage.Chinese;
            Thread.CurrentThread.CurrentUICulture = GetUICultureByLanguage(language);
        }

        private bool loginSuccess, isBusy;
        private string userName, password, errorMessage, enterpriseCode;
        private UILanguage language;
        private LoginCommand loginCommand;

        public string EnterpriseCodeText {
            get {
                return ShellUIStrings.LoginViewModel_EnterpriseCodeText;
            }
        }

        public string UserNameText {
            get {
                return ShellUIStrings.LoginViewModel_UserNameText;
            }
        }

        public string UserNamePromptText {
            get {
                return ShellUIStrings.LoginViewModel_UserNamePromptText;
            }
        }

        public string PasswordText {
            get {
                return ShellUIStrings.LoginViewModel_PasswordText;
            }
        }

        public string LanguageText {
            get {
                return ShellUIStrings.LoginViewModel_LanguageText;
            }
        }

        public string LoginText {
            get {
                return ShellUIStrings.LoginViewModel_LoginText;
            }
        }

        public string UserName {
            get {
                return this.userName;
            }
            set {
                if(this.userName == value)
                    return;
                this.ValidateUserName(value);
                this.userName = value;
                this.NotifyOfPropertyChange(() => this.UserName);
            }
        }

        public string Password {
            get {
                return this.password;
            }
            set {
                if(this.password == value)
                    return;
                this.ValidatePassword(value);
                this.password = value;
                this.NotifyOfPropertyChange(() => this.Password);
            }
        }

        public string EnterpriseCode {
            get {
                return this.enterpriseCode;
            }
            set {
                if(this.enterpriseCode == value)
                    return;
                this.ValidateEnterpriseCode(value);
                this.enterpriseCode = value;
                this.NotifyOfPropertyChange(() => this.EnterpriseCode);
            }
        }

        public UILanguage Language {
            get {
                return this.language;
            }
            set {
                if(this.language == value)
                    return;
                this.language = value;
                this.NotifyOfPropertyChange(() => this.Language);
                this.SwitchUILanguage(value);
            }
        }

        public bool LoginSuccess {
            get {
                return this.loginSuccess;
            }
            internal set {
                if(this.loginSuccess == value)
                    return;
                this.loginSuccess = value;
                if(value)
                    this.SaveSettings();
                this.NotifyOfPropertyChange(() => this.LoginSuccess);
            }
        }

        public bool IsBusy {
            get {
                return this.isBusy;
            }
            internal set {
                if(this.isBusy == value)
                    return;
                this.isBusy = value;
                this.NotifyOfPropertyChange(() => this.IsBusy);
            }
        }

        public string ErrorMessage {
            get {
                return this.errorMessage;
            }
            internal set {
                if(this.errorMessage == value)
                    return;
                this.errorMessage = value;
                this.NotifyOfPropertyChange(() => this.ErrorMessage);
            }
        }

        public ICommand LoginCommand {
            get {
                return this.loginCommand ?? (this.loginCommand = new LoginCommand(this));
            }
        }

        /// <summary>
        ///     当前运行环境中<b>仅</b>包含DMS系统时，登录面板不显示企业编号输入框。
        /// </summary>
        public bool HideEnterpriseCodeInput {
            get {
                return ((BaseApp)Application.Current).SystemType == SystemTypes.Dms;
            }
        }

        private void ValidateUserName(string value) {
            if(string.IsNullOrWhiteSpace(value))
                this.SetPropertyError("UserName", ShellUIStrings.LoginViewModel_Error_UserNameEmpty);
            else
                this.RemovePropertyError("UserName");
        }

        private void ValidatePassword(string value) {
            if(string.IsNullOrWhiteSpace(value))
                this.SetPropertyError("Password", ShellUIStrings.LoginViewModel_Error_PasswordEmpty);
            else
                this.RemovePropertyError("Password");
        }

        /// <summary>
        ///     当前运行环境中包含DCS系统时，校验用户是否输入正确的企业编号。
        /// </summary>
        /// <param name="value">用户输入的企业编号</param>
        private void ValidateEnterpriseCode(string value) {
            if(string.IsNullOrWhiteSpace(value) && (((BaseApp)Application.Current).SystemType & SystemTypes.Dcs) == SystemTypes.Dcs)
                this.SetPropertyError("EnterpriseCode", ShellUIStrings.LoginViewModel_Error_EnterpriseCodeEmpty);
            else
                this.RemovePropertyError("EnterpriseCode");
        }

        private void SwitchUILanguage(UILanguage uiLanguage) {
            var culture = GetUICultureByLanguage(uiLanguage);
            if(ShellUIStrings.Culture == null || ShellUIStrings.Culture.Name != culture.Name) {
                ShellUIStrings.Culture = culture;
                ShellUtils.ChangeHtmlTitle(((BaseApp)Application.Current).ApplicationName);
            }
            this.NotifyOfPropertyChange(() => this.EnterpriseCodeText);
            this.NotifyOfPropertyChange(() => this.UserNameText);
            this.NotifyOfPropertyChange(() => this.UserNamePromptText);
            this.NotifyOfPropertyChange(() => this.PasswordText);
            this.NotifyOfPropertyChange(() => this.LanguageText);
            this.NotifyOfPropertyChange(() => this.LoginText);
        }

        private void LoadSettings() {
            if(Settings == null)
                return;

            if(Settings.Contains(KEY_LAST_USER_NAME))
                this.UserName = (string)Settings[KEY_LAST_USER_NAME];
            if(Settings.Contains(KEY_LAST_LANGUAGE)) {
                UILanguage lang;
                this.Language = Enum.TryParse((string)Settings[KEY_LAST_LANGUAGE], out lang) ? lang : UILanguage.Chinese;
            } else
                this.Language = UILanguage.Chinese;
#if DEBUG
            if(Settings.Contains(KEY_LAST_PASSWORD))
                this.Password = (string)Settings[KEY_LAST_PASSWORD];
#endif
            if(Settings.Contains(KEY_LAST_ENTERPRISECODE))
                this.EnterpriseCode = (string)Settings[KEY_LAST_ENTERPRISECODE];
        }

        private void SaveSettings() {
            if(Settings == null)
                return;

            Settings[KEY_LAST_ENTERPRISECODE] = this.EnterpriseCode;
            Settings[KEY_LAST_USER_NAME] = this.UserName;
            Settings[KEY_LAST_LANGUAGE] = this.Language.ToString();
#if DEBUG
            Settings[KEY_LAST_PASSWORD] = this.Password;
#endif
        }

        public LoginViewModel() {
            this.LoadSettings();
        }

        public override void Validate() {
            this.ValidateUserName(this.UserName);
            this.ValidatePassword(this.Password);
            this.ValidateEnterpriseCode(this.EnterpriseCode);
        }
    }
}
