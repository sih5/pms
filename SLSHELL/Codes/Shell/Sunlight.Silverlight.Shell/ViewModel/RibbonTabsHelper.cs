﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Resources;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.ViewModel {
    internal static class RibbonTabsHelper {
        public static IEnumerable<RadRibbonTab> GetTabsForView(UserControl view) {
            if(view == null)
                throw new ArgumentNullException("view");

            var queryTab = view is IQueryPanelView ? new RadRibbonTab {
                Header = BusinessViewUtils.MakeTabHeader(new Uri("/Sunlight.Silverlight.Shell;component/Images/tab-query.png", UriKind.Relative), ShellUIStrings.ShellViewModel_QueryTabText)
            } : null;
            var actionTab = view is IActionPanelView ? new RadRibbonTab {
                Header = BusinessViewUtils.MakeTabHeader(new Uri("/Sunlight.Silverlight.Shell;component/Images/tab-actions.png", UriKind.Relative), ShellUIStrings.ShellViewModel_ActionTabText)
            } : null;

            //数据查询
            if(queryTab != null) {
                var queryPanelView = (IQueryPanelView)view;
                if(queryPanelView.QueryItemGroups.Any()) {
                    var filterPanelRibbonGroup = new FilterPanelRibbonGroup {
                        QueryItemGroups = queryPanelView.QueryItemGroups,
                        AcceptEmptyFilter = queryPanelView.AcceptEmptyFilter,
                    };
                    var customQueryPanelView = queryPanelView as ICustomQueryPanelView;
                    if(customQueryPanelView != null) {
                        filterPanelRibbonGroup.ButtonIcon = customQueryPanelView.QueryButtonIcon;
                        filterPanelRibbonGroup.ButtonText = customQueryPanelView.QueryButtonText;
                    }
                    filterPanelRibbonGroup.ExecutingQuery += (sender, e) => queryPanelView.ExecuteQuery(e.QueryItemGroup, e.Filter);
                    queryTab.Items.Add(filterPanelRibbonGroup);
                } else
                    queryTab = null;
            }

            //业务操作
            if(actionTab != null) {
                var actionPanelView = (IActionPanelView)view;

                Action<RadRibbonTab, BuiltInTabs> createRibbonGroups = (tab, targetTab) => {
                    var actionItemGroups = actionPanelView.ActionItemGroups.Where(g => (g.PlaceOn & targetTab) == targetTab).ToArray();
                    if(actionItemGroups.Length == 0)
                        return;

                    foreach(var actionPanelRibbonGroup in actionItemGroups.Select(actionItemGroup => new ActionPanelRibbonGroup {
                        ActionItemGroup = actionItemGroup
                    })) {
                        actionPanelRibbonGroup.ExecutingAction += (sender, e) => actionPanelView.ExecuteAction(e.ActionItemGroup, e.UniqueId);
                        tab.Items.Add(actionPanelRibbonGroup);
                    }

                    actionPanelView.ActionStateChanged += (sender, e) => {
                        var group = tab.Items.OfType<ActionPanelRibbonGroup>().SingleOrDefault(g => g.ActionItemGroup != null && string.Compare(g.ActionItemGroup.UniqueId, e.GroupUniqueId, StringComparison.CurrentCultureIgnoreCase) == 0);
                        if(group == null)
                            return;
                        var button = group.Items.OfType<RadRibbonButton>().SingleOrDefault(b => string.Compare(b.Tag as string, e.UniqueId, StringComparison.CurrentCultureIgnoreCase) == 0);
                        if(button == null)
                            return;
                        button.IsEnabled = e.CanExecute;
                    };
                };

                createRibbonGroups(actionTab, BuiltInTabs.Action);
                if(queryTab != null)
                    createRibbonGroups(queryTab, BuiltInTabs.Query);

                if(!actionTab.HasItems)
                    actionTab = null;
            }

            return (new[] {
                queryTab, actionTab
            }).Where(tab => tab != null).ToArray();
        }
    }
}
