﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Microsoft.Practices.Unity;
using Sunlight.Silverlight.Commands;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Model;
using Sunlight.Silverlight.Resources;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.ViewModel {
    public class ShellViewModel : Core.ViewModel.ViewModelBase {
        private sealed class TabCacheItem {
            public DateTime ExpireTime {
                get;
                private set;
            }

            public IEnumerable<RadRibbonTab> Tabs {
                get;
                set;
            }

            public void ExtendExpireTime() {
                this.ExpireTime = DateTime.Now.AddMinutes(30);
            }
        }

        public static ShellViewModel Current {
            get;
            set;
        }

        private readonly Dictionary<string, int> pageIds;
        private readonly Dictionary<object, TabCacheItem> tabsCache;
        private object menuButtonContent, page;
        private string pageTitle;
        private bool? isMultiLine;
        private bool isMenuOpen, isRibbonMinimized, isBusy;
        private bool isNotShowPwd;
        private Uri pageUri;
        private ObservableCollection<RadRibbonBackstageItem> menuItems;
        private ObservableCollection<RadRibbonTab> ribbonTabs;
        private IEnumerable<SystemMenuItem> systemMenuItems;
        private IEnumerable<UriMapping> uriMappings;
        private LoadMenuCommand loadMenuCommand;
        private RecentPageManager recentPageManager;
        private FavoritePageManager favoritePageManager;
        private RadRibbonBackstageItem selectedMenuItem;

        //目录加载完成后做个通知
        public Action SystemMenuItemsChanged {
            get;
            set;
        }

        public IEnumerable<SystemMenuItem> SystemMenuItems {
            get {
                return this.systemMenuItems;
            }
            set {
                this.systemMenuItems = value;
                this.RefreshMenuItems();
                this.RefreshUriMappings();
                this.RecentPageManager = new RecentPageManager(value);
                if(SystemMenuItemsChanged != null)
                    SystemMenuItemsChanged();
            }
        }

        public string ApplicationTitle {
            get {
                return BaseApp.Current.ApplicationName;
            }
        }

        public string PageTitle {
            get {
                return this.pageTitle;
            }
            set {
                if(this.pageTitle == value)
                    return;
                this.pageTitle = value;
                this.NotifyOfPropertyChange(() => this.PageTitle);
            }
        }

        public bool? IsMultiLine {
            get {
                return this.isMultiLine;
            }
            set {

                this.isMultiLine = value;
                this.NotifyOfPropertyChange(() => this.IsMultiLine);
            }
        }

        public object MenuButtonContent {
            get {
                return this.menuButtonContent;
            }
            set {
                if(this.menuButtonContent == value)
                    return;
                this.menuButtonContent = value;
                this.NotifyOfPropertyChange(() => this.MenuButtonContent);
            }
        }

        public bool IsMenuOpen {
            get {
                return this.isMenuOpen;
            }
            set {
                if(this.isMenuOpen == value)
                    return;
                this.isMenuOpen = value;
                this.NotifyOfPropertyChange(() => this.IsMenuOpen);
                // 打开菜单时，仅当前选择的菜单默认被选中。
                if(this.isMenuOpen)
                    foreach(var menuItem in this.MenuItems)
                        menuItem.IsDefault = this.SelectedMenuItem == menuItem;
            }
        }

        public bool IsRibbonMinimized {
            get {
                return this.isRibbonMinimized;
            }
            set {
                if(this.isRibbonMinimized == value)
                    return;
                if(!value && this.RibbonTabs.Count == 0)
                    return;
                this.isRibbonMinimized = value;
                this.NotifyOfPropertyChange(() => this.IsRibbonMinimized);
            }
        }

        public bool IsBusy {
            get {
                return this.isBusy;
            }
            set {
                if(this.isBusy == value)
                    return;
                this.isBusy = value;
                this.NotifyOfPropertyChange(() => this.IsBusy);
            }
        }

        public bool IsNotShowPwd {
            get {
                return this.isNotShowPwd;
            }
            set {
                if(this.isNotShowPwd == value)
                    return;
                this.isNotShowPwd = value;
                this.NotifyOfPropertyChange(() => this.IsNotShowPwd);
            }
        }

        public Uri PageUri {
            get {
                return this.pageUri;
            }
            set {
                if(this.pageUri == value)
                    return;
                this.pageUri = value;
                this.IsMenuOpen = false;
                this.NotifyOfPropertyChange(() => this.PageUri);
            }
        }

        public object Page {
            get {
                return this.page;
            }
            set {
                this.page = value;
                this.OnPageChanged(value);
                this.NotifyOfPropertyChange(() => this.Page);
            }
        }

        public RecentPageManager RecentPageManager {
            get {
                return this.recentPageManager;
            }
            private set {
                if(this.recentPageManager == value)
                    return;
                this.recentPageManager = value;
                this.NotifyOfPropertyChange(() => this.RecentPageManager);
            }
        }

        public FavoritePageManager FavoritePageManager {
            get {
                return this.favoritePageManager;
            }
            private set {
                if(this.favoritePageManager == value)
                    return;
                this.favoritePageManager = value;
                this.NotifyOfPropertyChange(() => this.FavoritePageManager);
            }
        }

        public ObservableCollection<RadRibbonBackstageItem> MenuItems {
            get {
                return this.menuItems ?? (this.menuItems = new ObservableCollection<RadRibbonBackstageItem>());
            }
        }

        public ObservableCollection<RadRibbonTab> RibbonTabs {
            get {
                return this.ribbonTabs ?? (this.ribbonTabs = new ObservableCollection<RadRibbonTab>());
            }
        }

        public IEnumerable<UriMapping> UriMappings {
            get {
                return this.uriMappings ?? Enumerable.Empty<UriMapping>();
            }
            private set {
                this.uriMappings = value;
                this.NotifyOfPropertyChange(() => this.UriMappings);
            }
        }

        public ICommand LoadMenuCommand {
            get {
                return this.loadMenuCommand ?? (this.loadMenuCommand = new LoadMenuCommand(this));
            }
        }

        public RadRibbonBackstageItem SelectedMenuItem {
            get {
                return this.selectedMenuItem;
            }
            set {
                if(this.selectedMenuItem == value)
                    return;
                this.selectedMenuItem = value;
                this.NotifyOfPropertyChange(() => this.SelectedMenuItem);
            }
        }

        private void RefreshMenuItems() {
            this.MenuItems.Clear();
            foreach(var systemMenuItem in from systemMenuItem in this.SystemMenuItems
                                          let pageItemCount = systemMenuItem.GroupMenuItems.SelectMany(groupMenuItem => groupMenuItem.PageMenuItems).Count()
                                          where pageItemCount > 0
                                          select systemMenuItem)
                this.MenuItems.Add(new RadRibbonBackstageItem {
                    Header = systemMenuItem.DisplayName,
                    Icon = new BitmapImage(systemMenuItem.ImageUri),
                    IsSelectable = true,
                    Content = new MenuContentView {
                        MenuItem = systemMenuItem,
                    },
                });
            this.MenuItems.Add(new RadRibbonBackstageItem {
                IsGroupSeparator = true,
                IsSelectable = false,
            });
            this.MenuItems.Add(new RadRibbonBackstageItem {
                Header = ShellUIStrings.ShellViewModel_PasswordText,
                Icon = new BitmapImage(new Uri("/Sunlight.Silverlight.Shell;component/Images/password.png", UriKind.Relative)),
                IsSelectable = true,
                Visibility = this.IsNotShowPwd ? Visibility.Collapsed : Visibility.Visible,
                Content = DI.GetView("ChangePasswordView")
            });
            this.MenuItems.Add(new RadRibbonBackstageItem {
                Header = ShellUIStrings.ShellViewModel_LogoutText,
                Icon = new BitmapImage(new Uri("/Sunlight.Silverlight.Shell;component/Images/logout.png", UriKind.Relative)),
                IsSelectable = false,
                Command = new LogoutCommand(),
            });
            if(this.SelectedMenuItem == null)
                this.SelectedMenuItem = this.MenuItems.FirstOrDefault();
        }

        private void RefreshUriMappings() {
            var mappings = new List<UriMapping>();

            Action<string, string, int, string> register = (uri, mappedUri, id, parameter) => {
                this.pageIds.Add(uri, id);
                mappings.AddRange(new[] {
                    new UriMapping {
                        Uri = new Uri(uri, UriKind.Relative),
                        MappedUri = new Uri(string.Format("{0}?id={1}&param={2}", mappedUri, id, parameter), UriKind.Relative),
                    }, new UriMapping {
                        Uri = new Uri(uri + "/{ex}", UriKind.Relative),
                        MappedUri = new Uri(string.Format("{0}?id={1}&param={2}|{{ex}}", mappedUri, id, parameter), UriKind.Relative),
                    }
                });
            };

            Action<string, string, int> registerCustom = (uri, mappedUri, id) => {
                this.pageIds.Add(uri, id);
                mappings.Add(new UriMapping {
                    Uri = new Uri(uri, UriKind.Relative),
                    MappedUri = new Uri(string.Format("{0}?id={1}", mappedUri, id), UriKind.Relative),
                });
            };

            foreach(var systemMenuItem in this.SystemMenuItems)
                foreach(var groupMenuItem in systemMenuItem.GroupMenuItems)
                    foreach(var pageMenuItem in groupMenuItem.PageMenuItems) {
                        var frameUri = ShellUtils.GetFrameUriFromMenuItemName(systemMenuItem.Name, groupMenuItem.Name, pageMenuItem.Name);
                        if(BaseApp.Current.Container.IsRegistered<IRootView>(frameUri)) {
                            this.pageIds.Add(frameUri, pageMenuItem.Id);
                            continue;
                        }

                        switch(pageMenuItem.PageType) {
                            case "DataGrid":
                                register(frameUri, "/Sunlight.Silverlight.Shell;component/View/DataGridView.xaml", pageMenuItem.Id, pageMenuItem.PageParameter);
                                break;
                            case "Custom":
                                registerCustom(frameUri, pageMenuItem.PageParameter, pageMenuItem.Id);
                                break;
                        }
                    }
            this.UriMappings = mappings;
        }

        private void RefreshRibbonTabs(IEnumerable<RadRibbonTab> ribbonTabs) {
            if(ribbonTabs == null)
                throw new ArgumentNullException("ribbonTabs");
            this.RibbonTabs.Clear();
            foreach(var ribbonTab in ribbonTabs)
                this.RibbonTabs.Add(ribbonTab);
            this.NotifyOfPropertyChange(() => this.RibbonTabs);
        }

        private void LoadCustomTabs(object view) {
            var startTime = DateTime.Now;
            var customTabsView = (ICustomTabsView)this.page;
            while(!customTabsView.IsTabsReady) {
                if((DateTime.Now - startTime).TotalSeconds >= 20)
                    return;
                Thread.Sleep(100);
            }
            var dependencyObject = view as DependencyObject;
            if(dependencyObject != null)
                dependencyObject.Dispatcher.BeginInvoke(() => this.RefreshRibbonTabs(customTabsView.CustomTabs));
        }

        private void OnPageChanged(object page) {
            if(page == null)
                return;

            var rootView = page as IRootView;
            if(rootView != null) {
                this.PageTitle = rootView.Title;

                ShellUtils.ChangeHtmlTitle(BaseApp.Current.ApplicationName);

                var ribbonTabsView = rootView as IRibbonTabsView;
                if(ribbonTabsView != null && ribbonTabsView.RibbonTabs != null)
                    this.RefreshRibbonTabs(ribbonTabsView.RibbonTabs);

                return;
            }

            try {
                var generalView = page as IGeneralView;
                this.PageTitle = generalView != null ? generalView.PageTitle : null;

                if(page is ICustomTabsView) {
                    var customTabsView = (ICustomTabsView)page;
                    if(customTabsView.CustomTabs != null)
                        if(customTabsView.IsTabsReady)
                            this.RefreshRibbonTabs(customTabsView.CustomTabs);
                        else
                            ThreadPool.QueueUserWorkItem(this.LoadCustomTabs, page);
                } else {
                    TabCacheItem cacheItem;
                    if(!this.tabsCache.TryGetValue(page, out cacheItem))
                        this.tabsCache.Add(page, cacheItem = new TabCacheItem());
                    cacheItem.ExtendExpireTime();

                    var userControl = page as UserControl;
                    if(userControl != null)
                        this.RefreshRibbonTabs(cacheItem.Tabs ?? (cacheItem.Tabs = RibbonTabsHelper.GetTabsForView(userControl)));

                    var views = this.tabsCache.Where(kv => kv.Value.ExpireTime <= DateTime.Now).Select(kv => kv.Key).ToArray();
                    foreach(var view in views)
                        this.tabsCache.Remove(view);
                }
            } finally {
                ShellUtils.ChangeHtmlTitle(BaseApp.Current.ApplicationName);
            }
        }

        public ShellViewModel() {
            Current = this;
            this.pageIds = new Dictionary<string, int>(128);
            this.tabsCache = new Dictionary<object, TabCacheItem>(16);
            this.FavoritePageManager = new FavoritePageManager();
        }

        public int GetPageId(Uri pageUri) {
            if(pageUri == null)
                throw new ArgumentNullException("pageUri");
            var originalStr = pageUri.OriginalString;
            if(originalStr.Contains('?'))
                originalStr = originalStr.Substring(0, originalStr.IndexOf("?"));
            return this.pageIds.ContainsKey(originalStr) ? this.pageIds[originalStr] : 0;
        }

        public Uri GetPageUri(int pageId) {
            var pageMenuItem = this.SystemMenuItems.SelectMany(system => system.GroupMenuItems)
                                                   .SelectMany(group => group.PageMenuItems).SingleOrDefault(p => p.Id == pageId);
            return pageMenuItem == null ? null :
                new Uri(ShellUtils.GetFrameUriFromMenuItemName(pageMenuItem.GroupMenuItem.SystemMenuItem.Name, pageMenuItem.GroupMenuItem.Name, pageMenuItem.Name),
                        UriKind.Relative);
        }

        public override void Validate() {
            throw new NotSupportedException();
        }
    }
}
