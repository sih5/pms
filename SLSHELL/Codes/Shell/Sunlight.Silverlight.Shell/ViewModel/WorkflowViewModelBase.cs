﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Resources;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.ViewModel {
    public abstract class WorkflowViewModelBase : ModelBase {
        private readonly Queue<string> pageKeyQueue;
        private bool isFinalPage;
        private string firstPageKey, currentPageKey;
        private WorkflowPageBase currentPage;
        private Dictionary<string, Lazy<WorkflowPageBase>> pages;
        private DelegateCommand<WorkflowPageBase> goToNextCommand, goToPreviousCommand, startOverCommand, cancelCommand;
        public event EventHandler WorkflowCompleted;
        public event EventHandler WorkflowCancelled;

        public abstract string WorkflowName {
            get;
        }

        public bool IsFinalPage {
            get {
                return this.isFinalPage;
            }
            protected set {
                if(this.isFinalPage == value)
                    return;
                this.isFinalPage = value;
                this.NotifyOfPropertyChange("IsFinalPage");
            }
        }

        public WorkflowPageBase CurrentPage {
            get {
                if(this.currentPage == null) {
                    if(this.firstPageKey == null)
                        return null;
                    this.GoToPage(this.firstPageKey, true);
                }
                return this.currentPage;
            }
            private set {
                if(ReferenceEquals(this.currentPage, value))
                    return;
                if(this.currentPage != null)
                    this.currentPage.PropertyChanged -= this.CurrentPage_PropertyChanged;
                this.currentPage = value;
                this.currentPage.PropertyChanged += this.CurrentPage_PropertyChanged;
                this.NotifyOfPropertyChange("CurrentPage");
                this.RaiseAllCommandsCanExecuteChanged();
            }
        }

        public string CurrentPageKey {
            get {
                return this.currentPageKey;
            }
            private set {
                if(this.currentPageKey != null)
                    this.pageKeyQueue.Enqueue(this.currentPageKey);
                this.currentPageKey = value;
            }
        }

        public ICommand GoToNextCommand {
            get {
                return this.goToNextCommand ?? (this.goToNextCommand = new DelegateCommand<WorkflowPageBase>(this.ExecuteGoToNextCommand, this.CanExecuteGoToNextCommand));
            }
        }

        public ICommand GoToPreviousCommand {
            get {
                return this.goToPreviousCommand ?? (this.goToPreviousCommand = new DelegateCommand<WorkflowPageBase>(this.ExecuteGoToPreviousCommand, this.CanExecuteGoToPreviousCommand));
            }
        }

        public ICommand StartOverCommand {
            get {
                return this.startOverCommand ?? (this.startOverCommand = new DelegateCommand<WorkflowPageBase>(this.ExecuteStartOverCommand, this.CanExecuteStartOverCommand));
            }
        }

        public ICommand CancelCommand {
            get {
                return this.cancelCommand ?? (this.cancelCommand = new DelegateCommand<WorkflowPageBase>(this.ExecuteCancelCommand, this.CanExecuteCancelCommand));
            }
        }

        public void Reset() {
            this.pageKeyQueue.Clear();
            this.GoToPage(this.firstPageKey, true);
        }

        private void RaiseAllCommandsCanExecuteChanged() {
            if(this.goToNextCommand != null)
                this.goToNextCommand.RaiseCanExecuteChanged();
            if(this.goToPreviousCommand != null)
                this.goToPreviousCommand.RaiseCanExecuteChanged();
            if(this.startOverCommand != null)
                this.startOverCommand.RaiseCanExecuteChanged();
        }

        private bool CanExecuteGoToNextCommand(WorkflowPageBase page) {
            if(page == null)
                page = this.CurrentPage;
            return page != null && page.IsValid;
        }

        private void ExecuteGoToNextCommand(WorkflowPageBase page) {
            if(this.CurrentPage == null)
                return;

            if(this.IsFinalPage)
                this.OnRequestLeaveCurrentPage(canLeave => {
                    if(!canLeave)
                        return;
                    var handler = this.WorkflowCompleted;
                    if(handler != null)
                        handler(this, EventArgs.Empty);
                });
            else
                this.OnRequestLeaveCurrentPage(canLeave => {
                    if(!canLeave)
                        return;
                    var key = this.OnRequestNextPageKey(this.CurrentPageKey, this.CurrentPage);
                    if(key != null)
                        this.GoToPage(key, true);
                });
        }

        private bool CanExecuteGoToPreviousCommand(WorkflowPageBase page) {
            return this.pageKeyQueue.Count > 0;
        }

        private void ExecuteGoToPreviousCommand(WorkflowPageBase page) {
            if(this.pageKeyQueue.Count == 0)
                return;
            var previousPageKey = this.pageKeyQueue.Dequeue();
            this.GoToPage(previousPageKey);
        }

        private bool CanExecuteStartOverCommand(WorkflowPageBase page) {
            return this.firstPageKey != null;
        }

        private void ExecuteStartOverCommand(WorkflowPageBase page) {
            var parameters = new DialogParameters {
                Header = BaseApp.Current.ApplicationName,
                Content = ShellUIStrings.WorkflowViewModelBase_StartOver_ConfirmMessage,
            };
            parameters.Closed += (sender, args) => {
                if(!args.DialogResult.HasValue || !args.DialogResult.Value)
                    return;
                this.pageKeyQueue.Clear();
                this.GoToPage(this.firstPageKey, true);
            };
            RadWindow.Confirm(parameters);
        }

        private bool CanExecuteCancelCommand(WorkflowPageBase page) {
            return true;
        }

        private void ExecuteCancelCommand(WorkflowPageBase page) {
            var parameters = new DialogParameters {
                Header = BaseApp.Current.ApplicationName,
                Content = ShellUIStrings.WorkflowViewModelBase_Cancel_ConfirmMessage,
            };
            parameters.Closed += (sender, args) => {
                if(!args.DialogResult.HasValue || !args.DialogResult.Value)
                    return;
                this.pageKeyQueue.Clear();
                var handler = this.WorkflowCancelled;
                if(handler != null)
                    handler(this, EventArgs.Empty);
            };
            RadWindow.Confirm(parameters);
        }

        private void GoToPage(string key, bool reset = false) {
            if(this.pages == null || !this.pages.ContainsKey(key))
                return;

            var view = this.pages[key];
            if(view == null || (view.IsValueCreated && ReferenceEquals(this.CurrentPage, view.Value)))
                return;

            if(reset)
                view.Value.Reset();
            this.CurrentPage = view.Value;
            this.CurrentPageKey = key;

            this.OnPageSwitched(key, view.Value);
        }

        private void CurrentPage_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(e.PropertyName != "IsValid")
                return;
            this.RaiseAllCommandsCanExecuteChanged();
        }

        protected abstract string OnRequestNextPageKey(string currentKey, WorkflowPageBase currentPage);

        protected abstract void OnRequestLeaveCurrentPage(Action<bool> canLeaveCallback);

        protected abstract void OnPageSwitched(string key, WorkflowPageBase value);

        protected void RegisterPage<T>(string key, Func<T> factoryFunc, bool isFirstPage = false) where T : WorkflowPageBase {
            if(key == null)
                throw new ArgumentNullException("key");
            if(factoryFunc == null)
                throw new ArgumentNullException("factoryFunc");

            if(this.pages == null) {
                this.pages = new Dictionary<string, Lazy<WorkflowPageBase>>(8);
                this.firstPageKey = key;
            }

            this.pages[key] = new Lazy<WorkflowPageBase>(factoryFunc);
            if(isFirstPage)
                this.firstPageKey = key;
        }

        protected WorkflowViewModelBase() {
            this.pageKeyQueue = new Queue<string>(8);
        }
    }
}
