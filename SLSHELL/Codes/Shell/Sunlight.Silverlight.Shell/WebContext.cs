﻿using System.Security.Principal;
using System.ServiceModel.DomainServices.Client.ApplicationServices;

namespace Sunlight.Silverlight {
    public sealed class WebContext : WebContextBase {
        public new static WebContext Current {
            get {
                return ((WebContext)(WebContextBase.Current));
            }
        }

        public new IPrincipal User {
            get {
                return base.User;
            }
        }
    }
}
