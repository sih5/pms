﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models {
    /// <summary>
    ///     导入模板的列定义
    /// </summary>
    /// <remarks>
    ///     这个类需要在 WCF RIA Services 客户端使用，需保证存在不带参数的构造函数。
    /// </remarks>
    [DataContract]
    public class ImportTemplateColumn {
        /// <summary>
        ///     名称
        /// </summary>
        [DataMember]
        public string Name {
            get;
            set;
        }

        /// <summary>
        ///     是必填项
        /// </summary>
        [DataMember]
        public bool IsRequired {
            get;
            set;
        }
    }
}
