﻿using System.IO;
using System.Web.Configuration;

namespace Sunlight.Silverlight.Web.Extensions.Storages {
    public class StorageProvider {
        /// <summary>
        ///     根据网站的配置文件来返回用于存储文件的 Storage。
        /// </summary>
        /// <param name="path">文件的相对路径</param>
        /// <returns>文件此附件的 Storage</returns>
        public static BaseStorage GetStorage(string path) {
            var param = path.Split('/', '\\');
            return GetStorage(param.Length > 1 ? param[0] : "", param.Length > 2 ? param[1] : "", Path.GetFileName(path));
        }

        /// <summary>
        ///     根据网站的配置文件来返回用于存储文件的 Storage，默认为 <see cref="FileSystemStorage" />。
        /// </summary>
        /// <param name="category">文件分类</param>
        /// <param name="enterpriseCode">文件所属企业编号</param>
        /// <param name="fileName">文件名</param>
        /// <returns>存储此文件的 Storage</returns>
        public static BaseStorage GetStorage(string category, string enterpriseCode, string fileName) {
            switch(WebConfigurationManager.AppSettings["DeploymentType"]) {
                case "FTP":
                    return new FtpStorage(category, enterpriseCode, fileName);
                default:
                    return new FileSystemStorage(category, enterpriseCode, fileName);
            }
        }
    }
}
