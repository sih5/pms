﻿using System.Web;

namespace Sunlight.Silverlight.Web {
    public class Utils {
        public struct UserInfo {
            public int Id;
            public string LoginId;
            public string Name;
            public int EnterpriseId;
            public string EnterpriseCode;
            public string EnterpriseName;
        }

        public static UserInfo GetCurrentUserInfo() {
            var result = new UserInfo {
                Id = 0,
                LoginId = null,
                Name = null,
                EnterpriseId = 0,
                EnterpriseCode = null,
                EnterpriseName = null,
            };
            if(HttpContext.Current == null || HttpContext.Current.User == null || !HttpContext.Current.User.Identity.IsAuthenticated || string.IsNullOrWhiteSpace(HttpContext.Current.User.Identity.Name))
                return result;
            var array = HttpContext.Current.User.Identity.Name.Split(',');
            if(array.Length > 0)
                int.TryParse(array[0], out result.Id);
            if(array.Length > 1)
                result.LoginId = array[1];
            if(array.Length > 2)
                result.Name = array[2];
            if(array.Length > 3)
                int.TryParse(array[3], out result.EnterpriseId);
            if(array.Length > 4)
                result.EnterpriseCode = array[4];
            if(array.Length > 5)
                result.EnterpriseName = array[5];
            return result;
        }
    }
}