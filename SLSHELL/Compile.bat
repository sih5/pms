@echo off
set SOURCE=%CD%\Codes\Shell
set OUT=%CD%\Outputs
set SQL=%CD%\Publishing\SqlScripts
set MSB=%windir%\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe /nologo /v:m /t:Rebuild /p:Configuration=Release;WarningLevel=0;Optimize=true;DebugType=pdbonly;DebugSymbols=false;BuildInParallel=true;DocumentationFile=""

echo 正在删除旧产出物
REM 排除%OUT%目录下的.svn目录。
forfiles /P "%OUT%" /C "cmd /C if @file NEQ \".svn\" (if @ISDIR==TRUE (rmdir /S /Q @Path) else (del /S /Q @path))"
echo 旧产出物删除完成
echo.

echo 正在编译 SqlServer 版本
::%MSB% /p:DBMS=SqlServer ::/l:FileLogger,Microsoft.Build.Engine;logfile=Compile.SqlServer.log;verbosity=diagnostic "%SOURCE%\Shell.sln"
::if errorlevel 1 goto err
::echo.

echo 正在编译 Oracle 版本
%MSB% /p:DBMS=Oracle /l:FileLogger,Microsoft.Build.Engine;logfile=Compile.Oracle.log;verbosity=diagnostic "%SOURCE%\Shell.sln"
if errorlevel 1 goto err
echo.

if exist SqlScripts.txt del SqlScripts.txt
md "%OUT%\SqlScripts"
dir "%SQL%" /AD /O-N /B > SqlScripts.txt
for /f %%i in (SqlScripts.txt) do (
xcopy /E "%SQL%\%%i\*.*" "%OUT%\SqlScripts"
goto sqldone
)
:sqldone
del SqlScripts.txt

echo * 所有项目编译完成 *
echo.
goto ok

:err
echo 
echo * 编译时出现错误，请检查错误信息，处理之后再继续 *
echo.
if [%1] == [/autoclose] exit /b 1
:ok
set MSB=
set SQL=
set OUT=
set SOURCE=
if [%1] == [/autoclose] exit /b 0

pause
