﻿/*==============================================================*/
/* Database name:  Security                                     */
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2012/8/22 9:33:29                            */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Action');
  if num>0 then
    execute immediate 'drop table Action cascade constraints';
  end if;
end;
/

/*==============================================================*/
/* Table: Action                                                */
/*==============================================================*/
create table Action  (
   Id                   NUMBER(9)                       not null,
   PageId               NUMBER(9)                       not null,
   OperationId          VARCHAR2(50)                    not null,
   Name                 VARCHAR2(50),
   Name_enUS            VARCHAR2(50),
   Name_jaJP            VARCHAR2(50),
   Status               NUMBER(9)                       not null,
   constraint PK_ACTION primary key (Id)
)
/

comment on column Action.Status is
'字典项 - 基础数据状态'
/

