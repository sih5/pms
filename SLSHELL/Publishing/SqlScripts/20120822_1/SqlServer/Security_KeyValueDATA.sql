TRUNCATE TABLE [KeyValueItem]
GO

INSERT INTO [KeyValueItem] ([Category], [Name], [Caption], [Key], [Value], [IsBuiltIn], [Status]) VALUES
(N'SECURITY', N'SecurityCommon_Status', N'权限系统基础数据状态', 0, N'作废', 1, 2),
(N'SECURITY', N'SecurityCommon_Status', N'权限系统基础数据状态', 2, N'有效', 1, 2),
(N'SECURITY', N'Enterprise_Status', N'权限系统企业状态', 0, N'作废', 1, 2),
(N'SECURITY', N'Enterprise_Status', N'权限系统企业状态', 1, N'新建', 1, 2),
(N'SECURITY', N'Enterprise_Status', N'权限系统企业状态', 2, N'冻结', 1, 2),
(N'SECURITY', N'Enterprise_Status', N'权限系统企业状态', 3, N'生效', 1, 2),
(N'SECURITY', N'Page_Type', N'页面级别', 0, N'系统', 1, 2),
(N'SECURITY', N'Page_Type', N'页面级别', 1, N'分组', 1, 2),
(N'SECURITY', N'Page_Type', N'页面级别', 2, N'页面', 1, 2),
(N'SECURITY', N'Organization_OrganizationType', N'组织类型', 0, N'企业', 1, 2),
(N'SECURITY', N'Organization_OrganizationType', N'组织类型', 1, N'部门', 1, 2),
(N'SECURITY', N'Node_CategoryType', N'节点类型', 0, N'页面', 1, 2),
(N'SECURITY', N'Node_CategoryType', N'节点类型', 1, N'操作', 1, 2),
(N'SECURITY', N'Node_CategoryType', N'节点类型', 2, N'数据列', 1, 2),
(N'SECURITY', N'User_Status', N'权限人员状态', 0, N'作废', 1, 2),
(N'SECURITY', N'User_Status', N'权限人员状态', 1, N'有效', 1, 2),
(N'SECURITY', N'User_Status', N'权限人员状态', 2, N'冻结', 1, 2),
(N'SECURITY', N'Enterprise_Type', N'企业类型', 0, N'4S店', 1, 2),
(N'SECURITY', N'Enterprise_Type', N'企业类型', 1, N'二级网点', 1, 2);
GO
