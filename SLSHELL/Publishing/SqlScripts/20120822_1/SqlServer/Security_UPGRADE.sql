﻿/*==============================================================*/
/* Database name:  Security                                     */
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     2012/8/22 9:30:46                            */
/*==============================================================*/


if exists (select 1
            from  sysobjects
           where  id = object_id('Action')
            and   type = 'U')
   drop table Action
go

use Security
go

use Security
go

/*==============================================================*/
/* Table: Action                                                */
/*==============================================================*/
create table Action (
   Id                   int                  identity,
   PageId               int                  not null,
   OperationId          varchar(50)          not null,
   Name                 varchar(50)          null,
   Name_enUS            varchar(50)          null,
   Name_jaJP            varchar(50)          null,
   Status               int                  not null,
   constraint PK_ACTION primary key nonclustered (Id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '字典项 - 基础数据状态',
   'user', @CurrentUser, 'table', 'Action', 'column', 'Status'
go

