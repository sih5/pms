﻿/*==============================================================*/
/* Database name:  Security                                     */
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2012/8/29 17:52:38                           */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Enterprise');
  if num>0 then
    execute immediate 'drop table Enterprise cascade constraints';
  end if;
end;
/

/*==============================================================*/
/* Table: Enterprise                                            */
/*==============================================================*/
create table Enterprise  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   EnterpriseCategoryId NUMBER(9)                       not null,
   EntNodeTemplateId    NUMBER(9),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CustomProperty1      VARCHAR2(200),
   CustomProperty2      VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ENTERPRISE primary key (Id)
)
/

comment on column Enterprise.Status is
'字典项 - 权限系统企业状态'
/

