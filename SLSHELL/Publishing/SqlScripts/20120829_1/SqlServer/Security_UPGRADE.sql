﻿/*==============================================================*/
/* Database name:  Security                                     */
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     2012/8/29 17:54:55                           */
/*==============================================================*/


if exists (select 1
            from  sysobjects
           where  id = object_id('Enterprise')
            and   type = 'U')
   drop table Enterprise
go

use Security
go

use Security
go

/*==============================================================*/
/* Table: Enterprise                                            */
/*==============================================================*/
create table Enterprise (
   Id                   int                  not null,
   Code                 varchar(50)          not null,
   Name                 varchar(100)         not null,
   EnterpriseCategoryId int                  not null,
   EntNodeTemplateId    int                  null,
   Status               int                  not null,
   Remark               varchar(200)         null,
   CustomProperty1      varchar(200)         null,
   CustomProperty2      varchar(200)         null,
   CreatorId            int                  null,
   CreatorName          varchar(100)         null,
   CreateTime           datetime             null,
   ModifierId           int                  null,
   ModifierName         varchar(100)         null,
   ModifyTime           datetime             null,
   RowVersion           rowversion           null,
   constraint PK_ENTERPRISE primary key nonclustered (Id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '字典项 - 权限系统企业状态',
   'user', @CurrentUser, 'table', 'Enterprise', 'column', 'Status'
go

