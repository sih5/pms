﻿/*==============================================================*/
/* Database name:  Security                                     */
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2012/9/27 15:45:24                           */
/*==============================================================*/

create sequence S_EnterpriseImageResource
/

/*==============================================================*/
/* Table: EnterpriseImageResource                               */
/*==============================================================*/
create table EnterpriseImageResource  (
   Id                   NUMBER(9)                       not null,
   EnterpriseId         NUMBER(9)                       not null,
   Name                 VARCHAR2(100)                   not null,
   Path                 VARCHAR2(200),
   Remark               VARCHAR2(500),
   constraint PK_ENTERPRISEIMAGERESOURCE primary key (Id)
)
/
