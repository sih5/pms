﻿/*==============================================================*/
/* Database name:  Security                                     */
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     2012/9/27 15:41:35                           */
/*==============================================================*/


use Security
go

use Security
go

/*==============================================================*/
/* Table: EnterpriseImageResource                               */
/*==============================================================*/
create table EnterpriseImageResource (
   Id                   int                  identity,
   EnterpriseId         int                  not null,
   Name                 varchar(100)         not null,
   Path                 varchar(200)         null,
   Remark               varchar(500)         null,
   constraint PK_ENTERPRISEIMAGERESOURCE primary key nonclustered (Id)
)
go
