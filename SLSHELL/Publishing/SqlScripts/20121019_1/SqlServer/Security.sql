﻿/*==============================================================*/
/* Database name:  Security                                     */
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     2012/10/19 14:58:47                          */
/*==============================================================*/


use Security
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Action')
            and   type = 'U')
   drop table Action
go

if exists (select 1
            from  sysobjects
           where  id = object_id('AuthorizedColumn')
            and   type = 'U')
   drop table AuthorizedColumn
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EntNodeTemplate')
            and   type = 'U')
   drop table EntNodeTemplate
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EntNodeTemplateDetail')
            and   type = 'U')
   drop table EntNodeTemplateDetail
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EntOrganizationTpl')
            and   type = 'U')
   drop table EntOrganizationTpl
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EntOrganizationTplDetail')
            and   type = 'U')
   drop table EntOrganizationTplDetail
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Enterprise')
            and   type = 'U')
   drop table Enterprise
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EnterpriseCategory')
            and   type = 'U')
   drop table EnterpriseCategory
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EnterpriseImageResource')
            and   type = 'U')
   drop table EnterpriseImageResource
go

if exists (select 1
            from  sysobjects
           where  id = object_id('FavoritePage')
            and   type = 'U')
   drop table FavoritePage
go

if exists (select 1
            from  sysobjects
           where  id = object_id('KeyValueItem')
            and   type = 'U')
   drop table KeyValueItem
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Node')
            and   type = 'U')
   drop table Node
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Organization')
            and   type = 'U')
   drop table Organization
go

if exists (select 1
            from  sysobjects
           where  id = object_id('OrganizationPersonnel')
            and   type = 'U')
   drop table OrganizationPersonnel
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Page')
            and   type = 'U')
   drop table Page
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PasswordPolicy')
            and   type = 'U')
   drop table PasswordPolicy
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Personnel')
            and   type = 'U')
   drop table Personnel
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Role')
            and   type = 'U')
   drop table Role
go

if exists (select 1
            from  sysobjects
           where  id = object_id('RolePersonnel')
            and   type = 'U')
   drop table RolePersonnel
go

if exists (select 1
            from  sysobjects
           where  id = object_id('RoleTemplate')
            and   type = 'U')
   drop table RoleTemplate
go

if exists (select 1
            from  sysobjects
           where  id = object_id('RoleTemplateRule')
            and   type = 'U')
   drop table RoleTemplateRule
go

if exists (select 1
            from  sysobjects
           where  id = object_id('"Rule"')
            and   type = 'U')
   drop table "Rule"
go

/*==============================================================*/
/* Table: Action                                                */
/*==============================================================*/
create table Action (
   Id                   int                  identity,
   PageId               int                  not null,
   OperationId          varchar(50)          not null,
   Name                 varchar(50)          null,
   Name_enUS            varchar(50)          null,
   Name_jaJP            varchar(50)          null,
   Status               int                  not null,
   constraint PK_ACTION primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: AuthorizedColumn                                      */
/*==============================================================*/
create table AuthorizedColumn (
   Id                   int                  identity,
   PageId               int                  not null,
   ColumnName           varchar(50)          not null,
   Status               int                  not null,
   constraint PK_AUTHORIZEDCOLUMN primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: EntNodeTemplate                                       */
/*==============================================================*/
create table EntNodeTemplate (
   Id                   int                  identity,
   Code                 varchar(50)          not null,
   Name                 varchar(100)         not null,
   Status               int                  not null,
   Remark               varchar(200)         null,
   EnterpriseId         int                  not null,
   CreatorId            int                  null,
   CreatorName          varchar(100)         null,
   CreateTime           datetime             null,
   ModifierId           int                  null,
   ModifierName         varchar(100)         null,
   ModifyTime           datetime             null,
   RowVersion           rowversion           null,
   constraint PK_ENTNODETEMPLATE primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: EntNodeTemplateDetail                                 */
/*==============================================================*/
create table EntNodeTemplateDetail (
   Id                   int                  identity,
   EntNodeTemplateId    int                  not null,
   NodeId               int                  not null,
   constraint PK_ENTNODETEMPLATEDETAIL primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: EntOrganizationTpl                                    */
/*==============================================================*/
create table EntOrganizationTpl (
   Id                   int                  identity,
   Code                 varchar(50)          not null,
   Name                 varchar(100)         not null,
   Status               int                  not null,
   Remark               varchar(200)         null,
   EnterpriseId         int                  not null,
   CreatorId            int                  null,
   CreatorName          varchar(100)         null,
   CreateTime           datetime             null,
   ModifierId           int                  null,
   ModifierName         varchar(100)         null,
   ModifyTime           datetime             null,
   RowVersion           rowversion           null,
   constraint PK_ENTORGANIZATIONTPL primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: EntOrganizationTplDetail                              */
/*==============================================================*/
create table EntOrganizationTplDetail (
   Id                   int                  identity,
   EntOrganizationTplId int                  not null,
   ParentId             int                  not null,
   OrganizationCode     varchar(50)          not null,
   OrganizationName     varchar(100)         not null,
   OrganizationType     int                  not null,
   Sequence             int                  not null,
   constraint PK_ENTORGANIZATIONTPLDETAIL primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: Enterprise                                            */
/*==============================================================*/
create table Enterprise (
   Id                   int                  not null,
   Code                 varchar(50)          not null,
   Name                 varchar(100)         not null,
   EnterpriseCategoryId int                  not null,
   EntNodeTemplateId    int                  null,
   Status               int                  not null,
   Remark               varchar(200)         null,
   CustomProperty1      varchar(200)         null,
   CustomProperty2      varchar(200)         null,
   CreatorId            int                  null,
   CreatorName          varchar(100)         null,
   CreateTime           datetime             null,
   ModifierId           int                  null,
   ModifierName         varchar(100)         null,
   ModifyTime           datetime             null,
   RowVersion           rowversion           null,
   constraint PK_ENTERPRISE primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: EnterpriseCategory                                    */
/*==============================================================*/
create table EnterpriseCategory (
   Id                   int                  identity,
   Code                 varchar(50)          not null,
   Name                 varchar(100)         not null,
   Status               int                  not null,
   Remark               varchar(200)         null,
   CreatorId            int                  null,
   CreatorName          varchar(100)         null,
   CreateTime           datetime             null,
   ModifierId           int                  null,
   ModifierName         varchar(100)         null,
   ModifyTime           datetime             null,
   RowVersion           rowversion           null,
   constraint PK_ENTERPRISECATEGORY primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: EnterpriseImageResource                               */
/*==============================================================*/
create table EnterpriseImageResource (
   Id                   int                  identity,
   EnterpriseId         int                  not null,
   Name                 varchar(100)         not null,
   Path                 varchar(200)         null,
   Remark               varchar(500)         null,
   constraint PK_ENTERPRISEIMAGERESOURCE primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: FavoritePage                                          */
/*==============================================================*/
create table FavoritePage (
   Id                   int                  identity,
   PersonnelId          int                  not null,
   PageId               int                  not null,
   Sequence             int                  not null,
   constraint PK_FAVORITEPAGE primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: KeyValueItem                                          */
/*==============================================================*/
create table KeyValueItem (
   Id                   int                  identity,
   Category             varchar(50)          not null,
   Name                 varchar(50)          not null,
   Caption              varchar(100)         not null,
   "Key"                int                  not null,
   Value                varchar(200)         not null,
   Value_enUS           varchar(200)         null,
   Value_jaJP           varchar(200)         null,
   IsBuiltIn            bit                  not null,
   Status               int                  not null,
   RowVersion           rowversion           null,
   constraint PK_KEYVALUEITEM primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: Node                                                  */
/*==============================================================*/
create table Node (
   Id                   int                  identity,
   CategoryType         int                  not null,
   CategoryId           int                  not null,
   Status               int                  not null,
   constraint PK_NODE primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: Organization                                          */
/*==============================================================*/
create table Organization (
   Id                   int                  identity,
   ParentId             int                  not null,
   Code                 varchar(50)          not null,
   Name                 varchar(50)          not null,
   Type                 int                  not null,
   Sequence             int                  not null,
   Status               int                  not null,
   EnterpriseId         int                  not null,
   Remark               varchar(200)         null,
   CreatorId            int                  null,
   CreatorName          varchar(100)         null,
   CreateTime           datetime             null,
   ModifierId           int                  null,
   ModifierName         varchar(100)         null,
   ModifyTime           datetime             null,
   RowVersion           rowversion           null,
   constraint PK_ORGANIZATION primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: OrganizationPersonnel                                 */
/*==============================================================*/
create table OrganizationPersonnel (
   Id                   int                  identity,
   OrganizationId       int                  not null,
   PersonnelId          int                  not null,
   constraint PK_ORGANIZATIONPERSONNEL primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: Page                                                  */
/*==============================================================*/
create table Page (
   Id                   int                  identity,
   ParentId             int                  not null,
   Type                 int                  not null,
   PageId               varchar(50)          null,
   PageType             varchar(20)          null,
   Parameter            varchar(500)         null,
   Name                 varchar(50)          not null,
   Name_enUS            varchar(50)          null,
   Name_jaJP            varchar(50)          null,
   Description          varchar(200)         null,
   Description_enUS     varchar(500)         null,
   Description_jaJP     varchar(500)         null,
   Icon                 varchar(100)         null,
   Sequence             int                  not null,
   Status               int                  not null,
   constraint PK_PAGE primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: PasswordPolicy                                        */
/*==============================================================*/
create table PasswordPolicy (
   Id                   int                  identity,
   ValidPeriod          int                  not null,
   RemindDays           int                  not null,
   DefaultPassword      varchar(50)          not null,
   IsEnabled            bit                  not null,
   constraint PK_PASSWORDPOLICY primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: Personnel                                             */
/*==============================================================*/
create table Personnel (
   Id                   int                  identity,
   EnterpriseId         int                  not null,
   LoginId              varchar(20)          not null,
   Password             char(40)             null,
   Name                 varchar(50)          not null,
   Status               int                  not null,
   CellNumber           varchar(20)          null,
   Email                varchar(50)          null,
   PasswordModifyTime   datetime             not null,
   Photo                varchar(200)         null,
   Remark               varchar(200)         null,
   CreatorId            int                  null,
   CreatorName          varchar(100)         null,
   CreateTime           datetime             null,
   ModifierId           int                  null,
   ModifierName         varchar(100)         null,
   ModifyTime           datetime             null,
   RowVersion           rowversion           null,
   constraint PK_PERSONNEL primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: Role                                                  */
/*==============================================================*/
create table Role (
   Id                   int                  identity,
   EnterpriseId         int                  not null,
   Name                 varchar(50)          not null,
   IsAdmin              bit                  not null,
   Status               int                  not null,
   Remark               varchar(200)         null,
   CreatorId            int                  null,
   CreatorName          varchar(100)         null,
   CreateTime           datetime             null,
   ModifierId           int                  null,
   ModifierName         varchar(100)         null,
   ModifyTime           datetime             null,
   RowVersion           rowversion           null,
   constraint PK_ROLE primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: RolePersonnel                                         */
/*==============================================================*/
create table RolePersonnel (
   Id                   int                  identity,
   RoleId               int                  not null,
   PersonnelId          int                  not null,
   constraint PK_ROLEPERSONNEL primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: RoleTemplate                                          */
/*==============================================================*/
create table RoleTemplate (
   Id                   int                  identity,
   EnterpriseId         int                  not null,
   Name                 varchar(100)         not null,
   Status               int                  not null,
   Remark               varchar(200)         null,
   CreatorId            int                  null,
   CreatorName          varchar(100)         null,
   CreateTime           datetime             null,
   ModifierId           int                  null,
   ModifierName         varchar(100)         null,
   ModifyTime           datetime             null,
   RowVersion           rowversion           null,
   constraint PK_ROLETEMPLATE primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: RoleTemplateRule                                      */
/*==============================================================*/
create table RoleTemplateRule (
   Id                   int                  identity,
   RoleTemplateId       int                  not null,
   NodeId               int                  not null,
   constraint PK_ROLETEMPLATERULE primary key nonclustered (Id)
)
go

/*==============================================================*/
/* Table: "Rule"                                                */
/*==============================================================*/
create table "Rule" (
   Id                   int                  identity,
   RoleId               int                  not null,
   NodeId               int                  not null,
   constraint PK_RULE primary key nonclustered (Id)
)
go

