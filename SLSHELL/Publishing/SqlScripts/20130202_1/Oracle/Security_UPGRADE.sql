declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DashboardSetting');
  if num>0 then
    execute immediate 'drop table DashboardSetting cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DashboardSetting');
  if num>0 then
    execute immediate 'drop sequence S_DashboardSetting';
  end if;
end;
/

create sequence S_DashboardSetting
/

create table DashboardSetting  (
   Id                   NUMBER(9)                       not null,
   PersonnelId          NUMBER(9)                       not null,
   PageId               NUMBER(9)                       not null,
   Sequence             NUMBER(9)                       not null,
   constraint PK_DASHBOARDSETTING primary key (Id)
)
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_EnterpriseCategoryDetail');
  if num>0 then
    execute immediate 'drop sequence S_EnterpriseCategoryDetail';
  end if;
end;
/
