if exists (select 1
            from  sysobjects
           where  id = object_id('DashboardSetting')
            and   type = 'U')
   drop table DashboardSetting
go

create table DashboardSetting (
   Id                   int                  identity,
   PersonnelId          int                  not null,
   PageId               int                  not null,
   Sequence             int                  not null,
   constraint PK_DASHBOARDSETTING primary key nonclustered (Id)
)
go