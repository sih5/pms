﻿/*==============================================================*/
/* Database name:  Security                                     */
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2013/2/2 16:01:35                            */
/*==============================================================*/

declare
  num integer;
begin
  select count(*) into num from user_views where VIEW_NAME=upper('PageUri');
  if num>0 then
    execute immediate 'drop view PageUri';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Action');
  if num>0 then
    execute immediate 'drop table Action cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AuthorizedColumn');
  if num>0 then
    execute immediate 'drop table AuthorizedColumn cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DashboardSetting');
  if num>0 then
    execute immediate 'drop table DashboardSetting cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('EntNodeTemplate');
  if num>0 then
    execute immediate 'drop table EntNodeTemplate cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('EntNodeTemplateDetail');
  if num>0 then
    execute immediate 'drop table EntNodeTemplateDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('EntOrganizationTpl');
  if num>0 then
    execute immediate 'drop table EntOrganizationTpl cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('EntOrganizationTplDetail');
  if num>0 then
    execute immediate 'drop table EntOrganizationTplDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Enterprise');
  if num>0 then
    execute immediate 'drop table Enterprise cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('EnterpriseCategory');
  if num>0 then
    execute immediate 'drop table EnterpriseCategory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('EnterpriseImageResource');
  if num>0 then
    execute immediate 'drop table EnterpriseImageResource cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('FavoritePage');
  if num>0 then
    execute immediate 'drop table FavoritePage cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('KeyValueItem');
  if num>0 then
    execute immediate 'drop table KeyValueItem cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Node');
  if num>0 then
    execute immediate 'drop table Node cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Organization');
  if num>0 then
    execute immediate 'drop table Organization cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('OrganizationPersonnel');
  if num>0 then
    execute immediate 'drop table OrganizationPersonnel cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Page');
  if num>0 then
    execute immediate 'drop table Page cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PasswordPolicy');
  if num>0 then
    execute immediate 'drop table PasswordPolicy cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Personnel');
  if num>0 then
    execute immediate 'drop table Personnel cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Role');
  if num>0 then
    execute immediate 'drop table Role cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RolePersonnel');
  if num>0 then
    execute immediate 'drop table RolePersonnel cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RoleTemplate');
  if num>0 then
    execute immediate 'drop table RoleTemplate cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RoleTemplateRule');
  if num>0 then
    execute immediate 'drop table RoleTemplateRule cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Rule');
  if num>0 then
    execute immediate 'drop table Rule cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Action');
  if num>0 then
    execute immediate 'drop sequence S_Action';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AuthorizedColumn');
  if num>0 then
    execute immediate 'drop sequence S_AuthorizedColumn';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DashboardSetting');
  if num>0 then
    execute immediate 'drop sequence S_DashboardSetting';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_EntNodeTemplate');
  if num>0 then
    execute immediate 'drop sequence S_EntNodeTemplate';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_EntNodeTemplateDetail');
  if num>0 then
    execute immediate 'drop sequence S_EntNodeTemplateDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_EntOrganizationTpl');
  if num>0 then
    execute immediate 'drop sequence S_EntOrganizationTpl';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_EntOrganizationTplDetail');
  if num>0 then
    execute immediate 'drop sequence S_EntOrganizationTplDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_EnterpriseCategory');
  if num>0 then
    execute immediate 'drop sequence S_EnterpriseCategory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_EnterpriseImageResource');
  if num>0 then
    execute immediate 'drop sequence S_EnterpriseImageResource';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_FavoritePage');
  if num>0 then
    execute immediate 'drop sequence S_FavoritePage';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_KeyValueItem');
  if num>0 then
    execute immediate 'drop sequence S_KeyValueItem';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Node');
  if num>0 then
    execute immediate 'drop sequence S_Node';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Organization');
  if num>0 then
    execute immediate 'drop sequence S_Organization';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_OrganizationPersonnel');
  if num>0 then
    execute immediate 'drop sequence S_OrganizationPersonnel';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Page');
  if num>0 then
    execute immediate 'drop sequence S_Page';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PasswordPolicy');
  if num>0 then
    execute immediate 'drop sequence S_PasswordPolicy';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Personnel');
  if num>0 then
    execute immediate 'drop sequence S_Personnel';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Role');
  if num>0 then
    execute immediate 'drop sequence S_Role';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RolePersonnel');
  if num>0 then
    execute immediate 'drop sequence S_RolePersonnel';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RoleTemplate');
  if num>0 then
    execute immediate 'drop sequence S_RoleTemplate';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RoleTemplateRule');
  if num>0 then
    execute immediate 'drop sequence S_RoleTemplateRule';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Rule');
  if num>0 then
    execute immediate 'drop sequence S_Rule';
  end if;
end;
/

create sequence S_Action
/

create sequence S_AuthorizedColumn
/

create sequence S_DashboardSetting
/

create sequence S_EntNodeTemplate
/

create sequence S_EntNodeTemplateDetail
/

create sequence S_EntOrganizationTpl
/

create sequence S_EntOrganizationTplDetail
/

create sequence S_EnterpriseCategory
/

create sequence S_EnterpriseImageResource
/

create sequence S_FavoritePage
/

create sequence S_KeyValueItem
/

create sequence S_Node
/

create sequence S_Organization
/

create sequence S_OrganizationPersonnel
/

create sequence S_Page
/

create sequence S_PasswordPolicy
/

create sequence S_Personnel
/

create sequence S_Role
/

create sequence S_RolePersonnel
/

create sequence S_RoleTemplate
/

create sequence S_RoleTemplateRule
/

create sequence S_Rule
/

/*==============================================================*/
/* Table: Action                                                */
/*==============================================================*/
create table Action  (
   Id                   NUMBER(9)                       not null,
   PageId               NUMBER(9)                       not null,
   OperationId          VARCHAR2(50)                    not null,
   Name                 VARCHAR2(50),
   Name_enUS            VARCHAR2(50),
   Name_jaJP            VARCHAR2(50),
   Status               NUMBER(9)                       not null,
   constraint PK_ACTION primary key (Id)
)
/

/*==============================================================*/
/* Table: AuthorizedColumn                                      */
/*==============================================================*/
create table AuthorizedColumn  (
   Id                   NUMBER(9)                       not null,
   PageId               NUMBER(9)                       not null,
   ColumnName           VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   constraint PK_AUTHORIZEDCOLUMN primary key (Id)
)
/

/*==============================================================*/
/* Table: DashboardSetting                                      */
/*==============================================================*/
create table DashboardSetting  (
   Id                   NUMBER(9)                       not null,
   PersonnelId          NUMBER(9)                       not null,
   PageId               NUMBER(9)                       not null,
   Sequence             NUMBER(9)                       not null,
   constraint PK_DASHBOARDSETTING primary key (Id)
)
/

/*==============================================================*/
/* Table: EntNodeTemplate                                       */
/*==============================================================*/
create table EntNodeTemplate  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   EnterpriseId         NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ENTNODETEMPLATE primary key (Id)
)
/

/*==============================================================*/
/* Table: EntNodeTemplateDetail                                 */
/*==============================================================*/
create table EntNodeTemplateDetail  (
   Id                   NUMBER(9)                       not null,
   EntNodeTemplateId    NUMBER(9)                       not null,
   NodeId               NUMBER(9)                       not null,
   constraint PK_ENTNODETEMPLATEDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: EntOrganizationTpl                                    */
/*==============================================================*/
create table EntOrganizationTpl  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   EnterpriseId         NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ENTORGANIZATIONTPL primary key (Id)
)
/

/*==============================================================*/
/* Table: EntOrganizationTplDetail                              */
/*==============================================================*/
create table EntOrganizationTplDetail  (
   Id                   NUMBER(9)                       not null,
   EntOrganizationTplId NUMBER(9)                       not null,
   ParentId             NUMBER(9)                       not null,
   OrganizationCode     VARCHAR2(50)                    not null,
   OrganizationName     VARCHAR2(100)                   not null,
   OrganizationType     NUMBER(9)                       not null,
   Sequence             NUMBER(9)                       not null,
   constraint PK_ENTORGANIZATIONTPLDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: Enterprise                                            */
/*==============================================================*/
create table Enterprise  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   EnterpriseCategoryId NUMBER(9)                       not null,
   EntNodeTemplateId    NUMBER(9),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CustomProperty1      VARCHAR2(200),
   CustomProperty2      VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ENTERPRISE primary key (Id)
)
/

/*==============================================================*/
/* Table: EnterpriseCategory                                    */
/*==============================================================*/
create table EnterpriseCategory  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ENTERPRISECATEGORY primary key (Id)
)
/

/*==============================================================*/
/* Table: EnterpriseImageResource                               */
/*==============================================================*/
create table EnterpriseImageResource  (
   Id                   NUMBER(9)                       not null,
   EnterpriseId         NUMBER(9)                       not null,
   Name                 VARCHAR2(100)                   not null,
   Path                 VARCHAR2(200),
   Remark               VARCHAR2(500),
   constraint PK_ENTERPRISEIMAGERESOURCE primary key (Id)
)
/

/*==============================================================*/
/* Table: FavoritePage                                          */
/*==============================================================*/
create table FavoritePage  (
   Id                   NUMBER(9)                       not null,
   PersonnelId          NUMBER(9)                       not null,
   PageId               NUMBER(9)                       not null,
   Sequence             NUMBER(9)                       not null,
   constraint PK_FAVORITEPAGE primary key (Id)
)
/

/*==============================================================*/
/* Table: KeyValueItem                                          */
/*==============================================================*/
create table KeyValueItem  (
   Id                   NUMBER(9)                       not null,
   Category             VARCHAR2(50)                    not null,
   Name                 VARCHAR2(50)                    not null,
   Caption              VARCHAR2(100)                   not null,
   Key                  NUMBER(9)                       not null,
   Value                VARCHAR2(200)                   not null,
   Value_enUS           VARCHAR2(200),
   Value_jaJP           VARCHAR2(200),
   IsBuiltIn            NUMBER(1)                       not null,
   Status               NUMBER(9)                       not null,
   RowVersion           TIMESTAMP,
   constraint PK_KEYVALUEITEM primary key (Id)
)
/

/*==============================================================*/
/* Table: Node                                                  */
/*==============================================================*/
create table Node  (
   Id                   NUMBER(9)                       not null,
   CategoryType         NUMBER(9)                       not null,
   CategoryId           NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   constraint PK_NODE primary key (Id)
)
/

/*==============================================================*/
/* Table: Organization                                          */
/*==============================================================*/
create table Organization  (
   Id                   NUMBER(9)                       not null,
   ParentId             NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(50)                    not null,
   Type                 NUMBER(9)                       not null,
   Sequence             NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   EnterpriseId         NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ORGANIZATION primary key (Id)
)
/

/*==============================================================*/
/* Table: OrganizationPersonnel                                 */
/*==============================================================*/
create table OrganizationPersonnel  (
   Id                   NUMBER(9)                       not null,
   OrganizationId       NUMBER(9)                       not null,
   PersonnelId          NUMBER(9)                       not null,
   constraint PK_ORGANIZATIONPERSONNEL primary key (Id)
)
/

/*==============================================================*/
/* Table: Page                                                  */
/*==============================================================*/
create table Page  (
   Id                   NUMBER(9)                       not null,
   ParentId             NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   PageId               VARCHAR2(50),
   PageType             VARCHAR2(20),
   Parameter            VARCHAR2(500),
   Name                 VARCHAR2(50)                    not null,
   Name_enUS            VARCHAR2(50),
   Name_jaJP            VARCHAR2(50),
   Description          VARCHAR2(200),
   Description_enUS     VARCHAR2(500),
   Description_jaJP     VARCHAR2(500),
   Icon                 VARCHAR2(100),
   Sequence             NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   constraint PK_PAGE primary key (Id)
)
/

/*==============================================================*/
/* Table: PasswordPolicy                                        */
/*==============================================================*/
create table PasswordPolicy  (
   Id                   NUMBER(9)                       not null,
   ValidPeriod          NUMBER(9)                       not null,
   RemindDays           NUMBER(9)                       not null,
   DefaultPassword      VARCHAR2(50)                    not null,
   IsEnabled            NUMBER(1)                       not null,
   constraint PK_PASSWORDPOLICY primary key (Id)
)
/

/*==============================================================*/
/* Table: Personnel                                             */
/*==============================================================*/
create table Personnel  (
   Id                   NUMBER(9)                       not null,
   EnterpriseId         NUMBER(9)                       not null,
   LoginId              VARCHAR2(20)                    not null,
   Password             CHAR(40),
   Name                 VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   CellNumber           VARCHAR2(20),
   Email                VARCHAR2(50),
   PasswordModifyTime   DATE                            not null,
   Photo                VARCHAR2(200),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PERSONNEL primary key (Id)
)
/

/*==============================================================*/
/* Table: Role                                                  */
/*==============================================================*/
create table Role  (
   Id                   NUMBER(9)                       not null,
   EnterpriseId         NUMBER(9)                       not null,
   Name                 VARCHAR2(50)                    not null,
   IsAdmin              NUMBER(1)                       not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ROLE primary key (Id)
)
/

/*==============================================================*/
/* Table: RolePersonnel                                         */
/*==============================================================*/
create table RolePersonnel  (
   Id                   NUMBER(9)                       not null,
   RoleId               NUMBER(9)                       not null,
   PersonnelId          NUMBER(9)                       not null,
   constraint PK_ROLEPERSONNEL primary key (Id)
)
/

/*==============================================================*/
/* Table: RoleTemplate                                          */
/*==============================================================*/
create table RoleTemplate  (
   Id                   NUMBER(9)                       not null,
   EnterpriseId         NUMBER(9)                       not null,
   Name                 VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ROLETEMPLATE primary key (Id)
)
/

/*==============================================================*/
/* Table: RoleTemplateRule                                      */
/*==============================================================*/
create table RoleTemplateRule  (
   Id                   NUMBER(9)                       not null,
   RoleTemplateId       NUMBER(9)                       not null,
   NodeId               NUMBER(9)                       not null,
   constraint PK_ROLETEMPLATERULE primary key (Id)
)
/

/*==============================================================*/
/* Table: Rule                                                  */
/*==============================================================*/
create table Rule  (
   Id                   NUMBER(9)                       not null,
   RoleId               NUMBER(9)                       not null,
   NodeId               NUMBER(9)                       not null,
   constraint PK_RULE primary key (Id)
)
/

/*==============================================================*/
/* View: PageUri                                                */
/*==============================================================*/
create or replace view PageUri as
SELECT Page.Id AS PageId, 
       LOWER('/' || SystemNode.PageId || (CASE WHEN SystemNode.PageId <> GroupNode.PageId THEN '/' || GroupNode.PageId ELSE '' END) || '/' || Page.PageId) AS Uri
FROM Page
LEFT OUTER JOIN Page GroupNode ON Page.ParentId = GroupNode.Id 
LEFT OUTER JOIN Page SystemNode ON GroupNode.ParentId = SystemNode.Id
WHERE Page.Type = 2 AND GroupNode.Type = 1 AND SystemNode.Type = 0 
  AND Page.PageType <> 'Chart' AND Page.PageType <> 'Warning'
with read only
/

