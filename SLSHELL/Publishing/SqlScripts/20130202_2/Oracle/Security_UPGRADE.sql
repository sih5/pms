declare
  num integer;
begin
  select count(*) into num from user_views where VIEW_NAME=upper('PageUri');
  if num>0 then
    execute immediate 'drop view PageUri';
  end if;
end;
/

create or replace view PageUri as
SELECT Page.Id AS PageId, 
       LOWER('/' || SystemNode.PageId || (CASE WHEN SystemNode.PageId <> GroupNode.PageId THEN '/' || GroupNode.PageId ELSE '' END) || '/' || Page.PageId) AS Uri
FROM Page
LEFT OUTER JOIN Page GroupNode ON Page.ParentId = GroupNode.Id 
LEFT OUTER JOIN Page SystemNode ON GroupNode.ParentId = SystemNode.Id
WHERE Page.Type = 2 AND GroupNode.Type = 1 AND SystemNode.Type = 0 
  AND Page.PageType <> 'Chart' AND Page.PageType <> 'Warning'
with read only
/