if exists (select 1
            from  sysobjects
           where  id = object_id('PageUri')
            and   type = 'V')
   drop view PageUri
go

create view PageUri as
SELECT Page.Id AS PageId, 
       LOWER('/' + SystemNode.PageId + (CASE WHEN SystemNode.PageId <> GroupNode.PageId THEN '/' + GroupNode.PageId ELSE '' END) + '/' + Page.PageId) AS Uri
FROM Page
LEFT OUTER JOIN Page AS GroupNode ON Page.ParentId = GroupNode.Id 
LEFT OUTER JOIN Page AS SystemNode ON GroupNode.ParentId = SystemNode.Id
WHERE Page.Type = 2 AND GroupNode.Type = 1 AND SystemNode.Type = 0 
    AND Page.PageType <> 'Chart' AND Page.PageType <> 'Warning'
go
