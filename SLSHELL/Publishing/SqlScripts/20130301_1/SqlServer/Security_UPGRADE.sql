﻿ALTER TABLE Action DROP CONSTRAINT PK_ACTION;
ALTER TABLE Action ADD CONSTRAINT PK_ACTION PRIMARY KEY (Id);

ALTER TABLE AuthorizedColumn DROP CONSTRAINT PK_AUTHORIZEDCOLUMN;
ALTER TABLE AuthorizedColumn ADD CONSTRAINT PK_AUTHORIZEDCOLUMN PRIMARY KEY (Id);

ALTER TABLE DashboardSetting DROP CONSTRAINT PK_DASHBOARDSETTING;
ALTER TABLE DashboardSetting ADD CONSTRAINT PK_DASHBOARDSETTING PRIMARY KEY (Id);

ALTER TABLE EntNodeTemplate DROP CONSTRAINT PK_ENTNODETEMPLATE;
ALTER TABLE EntNodeTemplate ADD CONSTRAINT PK_ENTNODETEMPLATE PRIMARY KEY (Id);

ALTER TABLE EntNodeTemplateDetail DROP CONSTRAINT PK_ENTNODETEMPLATEDETAIL;
ALTER TABLE EntNodeTemplateDetail ADD CONSTRAINT PK_ENTNODETEMPLATEDETAIL PRIMARY KEY (Id);

ALTER TABLE EntOrganizationTpl DROP CONSTRAINT PK_ENTORGANIZATIONTPL;
ALTER TABLE EntOrganizationTpl ADD CONSTRAINT PK_ENTORGANIZATIONTPL PRIMARY KEY (Id);

ALTER TABLE EntOrganizationTplDetail DROP CONSTRAINT PK_ENTORGANIZATIONTPLDETAIL;
ALTER TABLE EntOrganizationTplDetail ADD CONSTRAINT PK_ENTORGANIZATIONTPLDETAIL PRIMARY KEY (Id);

ALTER TABLE Enterprise DROP CONSTRAINT PK_ENTERPRISE;
ALTER TABLE Enterprise ADD CONSTRAINT PK_ENTERPRISE PRIMARY KEY (Id);

ALTER TABLE EnterpriseCategory DROP CONSTRAINT PK_ENTERPRISECATEGORY;
ALTER TABLE EnterpriseCategory ADD CONSTRAINT PK_ENTERPRISECATEGORY PRIMARY KEY (Id);

ALTER TABLE EnterpriseImageResource DROP CONSTRAINT PK_ENTERPRISEIMAGERESOURCE;
ALTER TABLE EnterpriseImageResource ADD CONSTRAINT PK_ENTERPRISEIMAGERESOURCE PRIMARY KEY (Id);

ALTER TABLE FavoritePage DROP CONSTRAINT PK_FAVORITEPAGE;
ALTER TABLE FavoritePage ADD CONSTRAINT PK_FAVORITEPAGE PRIMARY KEY (Id);

ALTER TABLE KeyValueItem DROP CONSTRAINT PK_KEYVALUEITEM;
ALTER TABLE KeyValueItem ADD CONSTRAINT PK_KEYVALUEITEM PRIMARY KEY (Id);

ALTER TABLE Node DROP CONSTRAINT PK_NODE;
ALTER TABLE Node ADD CONSTRAINT PK_NODE PRIMARY KEY (Id);

ALTER TABLE Organization DROP CONSTRAINT PK_ORGANIZATION;
ALTER TABLE Organization ADD CONSTRAINT PK_ORGANIZATION PRIMARY KEY (Id);

ALTER TABLE OrganizationPersonnel DROP CONSTRAINT PK_ORGANIZATIONPERSONNEL;
ALTER TABLE OrganizationPersonnel ADD CONSTRAINT PK_ORGANIZATIONPERSONNEL PRIMARY KEY (Id);

ALTER TABLE Page DROP CONSTRAINT PK_PAGE;
ALTER TABLE Page ADD CONSTRAINT PK_PAGE PRIMARY KEY (Id);

ALTER TABLE PasswordPolicy DROP CONSTRAINT PK_PASSWORDPOLICY;
ALTER TABLE PasswordPolicy ADD CONSTRAINT PK_PASSWORDPOLICY PRIMARY KEY (Id);

ALTER TABLE Personnel DROP CONSTRAINT PK_PERSONNEL;
ALTER TABLE Personnel ADD CONSTRAINT PK_PERSONNEL PRIMARY KEY (Id);

ALTER TABLE Role DROP CONSTRAINT PK_ROLE;
ALTER TABLE Role ADD CONSTRAINT PK_ROLE PRIMARY KEY (Id);

ALTER TABLE RolePersonnel DROP CONSTRAINT PK_ROLEPERSONNEL;
ALTER TABLE RolePersonnel ADD CONSTRAINT PK_ROLEPERSONNEL PRIMARY KEY (Id);

ALTER TABLE RoleTemplate DROP CONSTRAINT PK_ROLETEMPLATE;
ALTER TABLE RoleTemplate ADD CONSTRAINT PK_ROLETEMPLATE PRIMARY KEY (Id);

ALTER TABLE RoleTemplateRule DROP CONSTRAINT PK_ROLETEMPLATERULE;
ALTER TABLE RoleTemplateRule ADD CONSTRAINT PK_ROLETEMPLATERULE PRIMARY KEY (Id);

ALTER TABLE "Rule" DROP CONSTRAINT PK_RULE;
ALTER TABLE "Rule" ADD CONSTRAINT PK_RULE PRIMARY KEY (Id);