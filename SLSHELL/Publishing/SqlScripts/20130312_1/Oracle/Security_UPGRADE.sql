﻿/*==============================================================*/
/* Database name:  Security                                     */
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2013/3/12 14:37:50                           */
/*==============================================================*/


drop view PageUri
/


create or replace procedure CleanupRuleByEntNodeTemplate as
begin
  delete Rule
  where exists (select * from Role 
                 where Rule.RoleId = Role.Id 
                   and exists (select * from Enterprise
                                where status != 0 and EntNodeTemplateId is not null and EntNodeTemplateId != 0
                                  and Role.EnterpriseId = Enterprise.Id
                                  and not exists (select * from EntNodeTemplateDetail
                                                   where EntNodeTemplateDetail.EntNodeTemplateId = Enterprise.EntNodeTemplateId
                                                     and Rule.NodeId = EntNodeTemplateDetail.NodeId
                                                  )
                               )
                );

  delete RoleTemplateRule
  where exists (select * from RoleTemplate 
                 where RoleTemplateRule.RoleTemplateId = RoleTemplate.Id 
                   and exists (select * from Enterprise
                                where status != 0 and EntNodeTemplateId is not null and EntNodeTemplateId != 0
                                  and RoleTemplate.EnterpriseId = Enterprise.Id
                                  and not exists (select * from EntNodeTemplateDetail
                                                   where EntNodeTemplateDetail.EntNodeTemplateId = Enterprise.EntNodeTemplateId
                                                     and RoleTemplateRule.NodeId = EntNodeTemplateDetail.NodeId
                                                  )
                               )
                );
end;
/

