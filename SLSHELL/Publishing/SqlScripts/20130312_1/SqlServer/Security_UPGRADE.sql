/*==============================================================*/
/* Database name:  Security                                     */
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     2013/3/12 14:30:45                           */
/*==============================================================*/

if exists (select 1
            from  sysobjects
           where  id = object_id('PageUri')
            and   type = 'V')
   drop view PageUri
go


create procedure CleanupRuleByEntNodeTemplate as
delete [Rule]
where exists (select * from Role 
			   where [Rule].RoleId = Role.Id 
				 and exists (select * from Enterprise
							  where status != 0 and EntNodeTemplateId is not null and EntNodeTemplateId != 0
								and Role.EnterpriseId = Enterprise.Id
								and not exists (select * from EntNodeTemplateDetail
												 where EntNodeTemplateDetail.EntNodeTemplateId = Enterprise.EntNodeTemplateId
												   and [Rule].NodeId = EntNodeTemplateDetail.NodeId
												 )
							)
				)

delete RoleTemplateRule
where exists (select * from RoleTemplate 
			   where RoleTemplateRule.RoleTemplateId = RoleTemplate.Id 
				 and exists (select * from Enterprise
							  where status != 0 and EntNodeTemplateId is not null and EntNodeTemplateId != 0
								and RoleTemplate.EnterpriseId = Enterprise.Id
								and not exists (select * from EntNodeTemplateDetail
												 where EntNodeTemplateDetail.EntNodeTemplateId = Enterprise.EntNodeTemplateId
												   and RoleTemplateRule.NodeId = EntNodeTemplateDetail.NodeId
												 )
							)
				)
go

