/*==============================================================*/
/* Database name:  Security                                     */
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2013/9/6 11:19:18                            */
/*==============================================================*/


declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Action');
  if num>0 then
    execute immediate 'drop table Action cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('AuthorizedColumn');
  if num>0 then
    execute immediate 'drop table AuthorizedColumn cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('DashboardSetting');
  if num>0 then
    execute immediate 'drop table DashboardSetting cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('EntNodeTemplate');
  if num>0 then
    execute immediate 'drop table EntNodeTemplate cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('EntNodeTemplateDetail');
  if num>0 then
    execute immediate 'drop table EntNodeTemplateDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('EntOrganizationTpl');
  if num>0 then
    execute immediate 'drop table EntOrganizationTpl cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('EntOrganizationTplDetail');
  if num>0 then
    execute immediate 'drop table EntOrganizationTplDetail cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Enterprise');
  if num>0 then
    execute immediate 'drop table Enterprise cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('EnterpriseCategory');
  if num>0 then
    execute immediate 'drop table EnterpriseCategory cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('EnterpriseImageResource');
  if num>0 then
    execute immediate 'drop table EnterpriseImageResource cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('FavoritePage');
  if num>0 then
    execute immediate 'drop table FavoritePage cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('KeyValueItem');
  if num>0 then
    execute immediate 'drop table KeyValueItem cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Node');
  if num>0 then
    execute immediate 'drop table Node cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Organization');
  if num>0 then
    execute immediate 'drop table Organization cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('OrganizationPersonnel');
  if num>0 then
    execute immediate 'drop table OrganizationPersonnel cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Page');
  if num>0 then
    execute immediate 'drop table Page cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('PasswordPolicy');
  if num>0 then
    execute immediate 'drop table PasswordPolicy cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Personnel');
  if num>0 then
    execute immediate 'drop table Personnel cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Role');
  if num>0 then
    execute immediate 'drop table Role cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RolePersonnel');
  if num>0 then
    execute immediate 'drop table RolePersonnel cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RoleTemplate');
  if num>0 then
    execute immediate 'drop table RoleTemplate cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('RoleTemplateRule');
  if num>0 then
    execute immediate 'drop table RoleTemplateRule cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_tables where TABLE_NAME=upper('Rule');
  if num>0 then
    execute immediate 'drop table Rule cascade constraints';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Action');
  if num>0 then
    execute immediate 'drop sequence S_Action';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_AuthorizedColumn');
  if num>0 then
    execute immediate 'drop sequence S_AuthorizedColumn';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_DashboardSetting');
  if num>0 then
    execute immediate 'drop sequence S_DashboardSetting';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_EntNodeTemplate');
  if num>0 then
    execute immediate 'drop sequence S_EntNodeTemplate';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_EntNodeTemplateDetail');
  if num>0 then
    execute immediate 'drop sequence S_EntNodeTemplateDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_EntOrganizationTpl');
  if num>0 then
    execute immediate 'drop sequence S_EntOrganizationTpl';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_EntOrganizationTplDetail');
  if num>0 then
    execute immediate 'drop sequence S_EntOrganizationTplDetail';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_EnterpriseCategory');
  if num>0 then
    execute immediate 'drop sequence S_EnterpriseCategory';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_EnterpriseImageResource');
  if num>0 then
    execute immediate 'drop sequence S_EnterpriseImageResource';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_FavoritePage');
  if num>0 then
    execute immediate 'drop sequence S_FavoritePage';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_KeyValueItem');
  if num>0 then
    execute immediate 'drop sequence S_KeyValueItem';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Node');
  if num>0 then
    execute immediate 'drop sequence S_Node';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Organization');
  if num>0 then
    execute immediate 'drop sequence S_Organization';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_OrganizationPersonnel');
  if num>0 then
    execute immediate 'drop sequence S_OrganizationPersonnel';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Page');
  if num>0 then
    execute immediate 'drop sequence S_Page';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_PasswordPolicy');
  if num>0 then
    execute immediate 'drop sequence S_PasswordPolicy';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Personnel');
  if num>0 then
    execute immediate 'drop sequence S_Personnel';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Role');
  if num>0 then
    execute immediate 'drop sequence S_Role';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RolePersonnel');
  if num>0 then
    execute immediate 'drop sequence S_RolePersonnel';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RoleTemplate');
  if num>0 then
    execute immediate 'drop sequence S_RoleTemplate';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_RoleTemplateRule');
  if num>0 then
    execute immediate 'drop sequence S_RoleTemplateRule';
  end if;
end;
/

declare
  num integer;
begin
  select count(*) into num from user_sequences where upper(SEQUENCE_NAME)=upper('S_Rule');
  if num>0 then
    execute immediate 'drop sequence S_Rule';
  end if;
end;
/

create sequence S_Action
/

create sequence S_AuthorizedColumn
/

create sequence S_DashboardSetting
/

create sequence S_EntNodeTemplate
/

create sequence S_EntNodeTemplateDetail
/

create sequence S_EntOrganizationTpl
/

create sequence S_EntOrganizationTplDetail
/

create sequence S_EnterpriseCategory
/

create sequence S_EnterpriseImageResource
/

create sequence S_FavoritePage
/

create sequence S_KeyValueItem
/

create sequence S_Node
/

create sequence S_Organization
/

create sequence S_OrganizationPersonnel
/

create sequence S_Page
/

create sequence S_PasswordPolicy
/

create sequence S_Personnel
/

create sequence S_Role
/

create sequence S_RolePersonnel
/

create sequence S_RoleTemplate
/

create sequence S_RoleTemplateRule
/

create sequence S_Rule
/

/*==============================================================*/
/* Table: Action                                                */
/*==============================================================*/
create table Action  (
   Id                   NUMBER(9)                       not null,
   PageId               NUMBER(9)                       not null,
   OperationId          VARCHAR2(50)                    not null,
   Name                 VARCHAR2(50),
   Name_enUS            VARCHAR2(50),
   Name_jaJP            VARCHAR2(50),
   Status               NUMBER(9)                       not null,
   constraint PK_ACTION primary key (Id)
)
/

/*==============================================================*/
/* Table: AuthorizedColumn                                      */
/*==============================================================*/
create table AuthorizedColumn  (
   Id                   NUMBER(9)                       not null,
   PageId               NUMBER(9)                       not null,
   ColumnName           VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   constraint PK_AUTHORIZEDCOLUMN primary key (Id)
)
/

/*==============================================================*/
/* Table: DashboardSetting                                      */
/*==============================================================*/
create table DashboardSetting  (
   Id                   NUMBER(9)                       not null,
   PersonnelId          NUMBER(9)                       not null,
   PageId               NUMBER(9)                       not null,
   Sequence             NUMBER(9)                       not null,
   constraint PK_DASHBOARDSETTING primary key (Id)
)
/

/*==============================================================*/
/* Table: EntNodeTemplate                                       */
/*==============================================================*/
create table EntNodeTemplate  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   EnterpriseId         NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ENTNODETEMPLATE primary key (Id)
)
/

/*==============================================================*/
/* Table: EntNodeTemplateDetail                                 */
/*==============================================================*/
create table EntNodeTemplateDetail  (
   Id                   NUMBER(9)                       not null,
   EntNodeTemplateId    NUMBER(9)                       not null,
   NodeId               NUMBER(9)                       not null,
   constraint PK_ENTNODETEMPLATEDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: EntOrganizationTpl                                    */
/*==============================================================*/
create table EntOrganizationTpl  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   EnterpriseId         NUMBER(9)                       not null,
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ENTORGANIZATIONTPL primary key (Id)
)
/

/*==============================================================*/
/* Table: EntOrganizationTplDetail                              */
/*==============================================================*/
create table EntOrganizationTplDetail  (
   Id                   NUMBER(9)                       not null,
   EntOrganizationTplId NUMBER(9)                       not null,
   ParentId             NUMBER(9)                       not null,
   OrganizationCode     VARCHAR2(50)                    not null,
   OrganizationName     VARCHAR2(100)                   not null,
   OrganizationType     NUMBER(9)                       not null,
   Sequence             NUMBER(9)                       not null,
   constraint PK_ENTORGANIZATIONTPLDETAIL primary key (Id)
)
/

/*==============================================================*/
/* Table: Enterprise                                            */
/*==============================================================*/
create table Enterprise  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   EnterpriseCategoryId NUMBER(9)                       not null,
   EntNodeTemplateId    NUMBER(9),
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CustomProperty1      VARCHAR2(200),
   CustomProperty2      VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ENTERPRISE primary key (Id)
)
/

/*==============================================================*/
/* Table: EnterpriseCategory                                    */
/*==============================================================*/
create table EnterpriseCategory  (
   Id                   NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ENTERPRISECATEGORY primary key (Id)
)
/

/*==============================================================*/
/* Table: EnterpriseImageResource                               */
/*==============================================================*/
create table EnterpriseImageResource  (
   Id                   NUMBER(9)                       not null,
   EnterpriseId         NUMBER(9)                       not null,
   Name                 VARCHAR2(100)                   not null,
   Path                 VARCHAR2(200),
   Remark               VARCHAR2(500),
   constraint PK_ENTERPRISEIMAGERESOURCE primary key (Id)
)
/

/*==============================================================*/
/* Table: FavoritePage                                          */
/*==============================================================*/
create table FavoritePage  (
   Id                   NUMBER(9)                       not null,
   PersonnelId          NUMBER(9)                       not null,
   PageId               NUMBER(9)                       not null,
   Sequence             NUMBER(9)                       not null,
   constraint PK_FAVORITEPAGE primary key (Id)
)
/

/*==============================================================*/
/* Table: KeyValueItem                                          */
/*==============================================================*/
create table KeyValueItem  (
   Id                   NUMBER(9)                       not null,
   Category             VARCHAR2(50)                    not null,
   Name                 VARCHAR2(50)                    not null,
   Caption              VARCHAR2(100)                   not null,
   Key                  NUMBER(9)                       not null,
   Value                VARCHAR2(200)                   not null,
   Value_enUS           VARCHAR2(200),
   Value_jaJP           VARCHAR2(200),
   IsBuiltIn            NUMBER(1)                       not null,
   Status               NUMBER(9)                       not null,
   RowVersion           TIMESTAMP,
   constraint PK_KEYVALUEITEM primary key (Id)
)
/

/*==============================================================*/
/* Table: Node                                                  */
/*==============================================================*/
create table Node  (
   Id                   NUMBER(9)                       not null,
   CategoryType         NUMBER(9)                       not null,
   CategoryId           NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   constraint PK_NODE primary key (Id)
)
/

/*==============================================================*/
/* Table: Organization                                          */
/*==============================================================*/
create table Organization  (
   Id                   NUMBER(9)                       not null,
   ParentId             NUMBER(9)                       not null,
   Code                 VARCHAR2(50)                    not null,
   Name                 VARCHAR2(100)                   not null,
   Type                 NUMBER(9)                       not null,
   Sequence             NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   EnterpriseId         NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ORGANIZATION primary key (Id)
)
/

/*==============================================================*/
/* Table: OrganizationPersonnel                                 */
/*==============================================================*/
create table OrganizationPersonnel  (
   Id                   NUMBER(9)                       not null,
   OrganizationId       NUMBER(9)                       not null,
   PersonnelId          NUMBER(9)                       not null,
   constraint PK_ORGANIZATIONPERSONNEL primary key (Id)
)
/

/*==============================================================*/
/* Table: Page                                                  */
/*==============================================================*/
create table Page  (
   Id                   NUMBER(9)                       not null,
   ParentId             NUMBER(9)                       not null,
   Type                 NUMBER(9)                       not null,
   PageId               VARCHAR2(50),
   PageType             VARCHAR2(20),
   Parameter            VARCHAR2(500),
   Name                 VARCHAR2(50)                    not null,
   Name_enUS            VARCHAR2(50),
   Name_jaJP            VARCHAR2(50),
   Description          VARCHAR2(200),
   Description_enUS     VARCHAR2(500),
   Description_jaJP     VARCHAR2(500),
   Icon                 VARCHAR2(100),
   Sequence             NUMBER(9)                       not null,
   Status               NUMBER(9)                       not null,
   constraint PK_PAGE primary key (Id)
)
/

/*==============================================================*/
/* Table: PasswordPolicy                                        */
/*==============================================================*/
create table PasswordPolicy  (
   Id                   NUMBER(9)                       not null,
   ValidPeriod          NUMBER(9)                       not null,
   RemindDays           NUMBER(9)                       not null,
   DefaultPassword      VARCHAR2(50)                    not null,
   IsEnabled            NUMBER(1)                       not null,
   constraint PK_PASSWORDPOLICY primary key (Id)
)
/

/*==============================================================*/
/* Table: Personnel                                             */
/*==============================================================*/
create table Personnel  (
   Id                   NUMBER(9)                       not null,
   EnterpriseId         NUMBER(9)                       not null,
   LoginId              VARCHAR2(20)                    not null,
   Password             CHAR(40),
   Name                 VARCHAR2(50)                    not null,
   Status               NUMBER(9)                       not null,
   CellNumber           VARCHAR2(20),
   Email                VARCHAR2(50),
   PasswordModifyTime   DATE                            not null,
   Photo                VARCHAR2(200),
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_PERSONNEL primary key (Id)
)
/

/*==============================================================*/
/* Table: Role                                                  */
/*==============================================================*/
create table Role  (
   Id                   NUMBER(9)                       not null,
   EnterpriseId         NUMBER(9)                       not null,
   Name                 VARCHAR2(50)                    not null,
   IsAdmin              NUMBER(1)                       not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ROLE primary key (Id)
)
/

/*==============================================================*/
/* Table: RolePersonnel                                         */
/*==============================================================*/
create table RolePersonnel  (
   Id                   NUMBER(9)                       not null,
   RoleId               NUMBER(9)                       not null,
   PersonnelId          NUMBER(9)                       not null,
   constraint PK_ROLEPERSONNEL primary key (Id)
)
/

/*==============================================================*/
/* Table: RoleTemplate                                          */
/*==============================================================*/
create table RoleTemplate  (
   Id                   NUMBER(9)                       not null,
   EnterpriseId         NUMBER(9)                       not null,
   Name                 VARCHAR2(100)                   not null,
   Status               NUMBER(9)                       not null,
   Remark               VARCHAR2(200),
   CreatorId            NUMBER(9),
   CreatorName          VARCHAR2(100),
   CreateTime           DATE,
   ModifierId           NUMBER(9),
   ModifierName         VARCHAR2(100),
   ModifyTime           DATE,
   RowVersion           TIMESTAMP,
   constraint PK_ROLETEMPLATE primary key (Id)
)
/

/*==============================================================*/
/* Table: RoleTemplateRule                                      */
/*==============================================================*/
create table RoleTemplateRule  (
   Id                   NUMBER(9)                       not null,
   RoleTemplateId       NUMBER(9)                       not null,
   NodeId               NUMBER(9)                       not null,
   constraint PK_ROLETEMPLATERULE primary key (Id)
)
/

/*==============================================================*/
/* Table: Rule                                                  */
/*==============================================================*/
create table Rule  (
   Id                   NUMBER(9)                       not null,
   RoleId               NUMBER(9)                       not null,
   NodeId               NUMBER(9)                       not null,
   constraint PK_RULE primary key (Id)
)
/


create or replace procedure AbandonEnterprise(PId number, PModifierId number, PModifierName varchar2, PModifyTime date) as
  EnterpriseId integer;
begin
  begin
    select Id into EnterpriseId from Enterprise
     where Id = PId and Status != 0;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20008,'只允许作废处于“新建”、“冻结”、“生效”状态下的企业');
    when others then
      raise;
  end;
  update Enterprise set Status = 0, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime where Enterprise.Id = PId;
  update Organization set Status = 0, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime where Organization.EnterpriseId = PId;
  update Personnel set Status = 0, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime where Personnel.EnterpriseId = PId;
  update Role set Status = 0, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime where Role.EnterpriseId = PId;
end;
/


create or replace procedure AddEnterprise(PId number, PCode varchar2, PName varchar2, PType number, PCreatorId number, PCreatorName varchar2, PCreateTime date) as
  EnterpriseId integer;
  OrganizationId integer;
  PersonnelId integer;
  RoleId integer;
begin
  begin
    select Id into EnterpriseId from Enterprise
     where Status != 0 and Code = PCode and rownum = 1;
    raise_application_error(-20001,'已存在相同编号的企业');
  exception
    when NO_DATA_FOUND then
      null;
    when others then
      raise;
  end;
  
  begin
    select Id into EnterpriseId from Enterprise
       where Status != 0 and Name = PName and rownum = 1;
      raise_application_error(-20002,'已存在相同名称的企业');
    exception
      when NO_DATA_FOUND then
        null;
      when others then
        raise;
  end;

  insert into Enterprise (Id, Code, Name, EnterpriseCategoryId, Status, CreatorId, CreatorName, CreateTime)
  values (PId, PCode, PName, PType, 1, PCreatorId, PCreatorName, PCreateTime);

  select S_Organization.Nextval into OrganizationId from dual;
  insert into Organization (Id, ParentId, Code, Name, Type, Sequence, Status, EnterpriseId, CreatorId, CreatorName, CreateTime)
  values (OrganizationId, 0, PCode, PName, 0, 0, 2, PId, PCreatorId, PCreatorName, PCreateTime);

  select S_Personnel.Nextval into PersonnelId from dual;
  insert into Personnel (Id, LoginId, Name, Password, PasswordModifyTime, Status, EnterpriseId, CreatorId, CreatorName, CreateTime)
  values (PersonnelId, 'admin', '企业管理员', '7C4A8D09CA3762AF61E59520943DC26494F8941B', sysdate, 1, PId, PCreatorId, PCreatorName, PCreateTime);

  select S_Role.Nextval into RoleId from dual;
  insert into Role (Id, Name, IsAdmin, Status, EnterpriseId, CreatorId, CreatorName, CreateTime)
  values (RoleId, '企业管理员', 1, 1, PId, PCreatorId, PCreatorName, PCreateTime);

  insert into OrganizationPersonnel(Id, OrganizationId, PersonnelId)
  values (S_OrganizationPersonnel.Nextval, OrganizationId, PersonnelId);

  insert into RolePersonnel(Id, RoleId, PersonnelId)
  values (S_RolePersonnel.Nextval, RoleId, PersonnelId);
  
  insert into Rule(Id, RoleId, NodeId)
  select S_Rule.Nextval, RoleId, Id
    from Node 
   where Node.Status = 2
     and (exists (select * from Page where Page.Id = Node.CategoryId and Page.Id < 1000 and Node.CategoryType = 0)
      or exists (select * from Action
                  where exists (select * from Page where Page.Id = Action.PageId and Page.Id < 1000)
                    and Action.Id = Node.CategoryId
                    and Node.CategoryType = 1));
end;
/


create or replace procedure CleanupRuleByEntNodeTemplate as
begin
  delete Rule
  where exists (select * from Role 
                 where Rule.RoleId = Role.Id 
                   and exists (select * from Enterprise
                                where status != 0 and EntNodeTemplateId is not null and EntNodeTemplateId != 0
                                  and Role.EnterpriseId = Enterprise.Id
                                  and not exists (select * from EntNodeTemplateDetail
                                                   where EntNodeTemplateDetail.EntNodeTemplateId = Enterprise.EntNodeTemplateId
                                                     and Rule.NodeId = EntNodeTemplateDetail.NodeId
                                                  )
                               )
                );

  delete RoleTemplateRule
  where exists (select * from RoleTemplate 
                 where RoleTemplateRule.RoleTemplateId = RoleTemplate.Id 
                   and exists (select * from Enterprise
                                where status != 0 and EntNodeTemplateId is not null and EntNodeTemplateId != 0
                                  and RoleTemplate.EnterpriseId = Enterprise.Id
                                  and not exists (select * from EntNodeTemplateDetail
                                                   where EntNodeTemplateDetail.EntNodeTemplateId = Enterprise.EntNodeTemplateId
                                                     and RoleTemplateRule.NodeId = EntNodeTemplateDetail.NodeId
                                                  )
                               )
                );
end;
/


create or replace procedure EditEnterprise(PId number, PName varchar2, PType number, PModifierId number, PModifierName varchar2, PModifyTime date) as
  EnterpriseId integer;
begin
  begin
    select Id into EnterpriseId from Enterprise
     where Status != 0 and Name = PName and Id != PId and rownum = 1;
    raise_application_error(-20003,'已存在相同名称的企业');
  exception
    when NO_DATA_FOUND then
      null;
    when others then
      raise;
  end;
  
  begin
    select Id into EnterpriseId from Enterprise
     where Status = 0 and Id = PId;
    raise_application_error(-20004,'只允许更新处于“新建”、“冻结”、“生效”状态下的企业');
  exception
    when NO_DATA_FOUND then
      null;
    when others then
      raise;
  end;

  update Enterprise set Name = PName, EnterpriseCategoryId = PType, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime
   where Enterprise.Id = PId;

  update Organization set Name = PName, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime
   where Organization.EnterpriseId = PId and Organization.Type = 0;
end;
/


create or replace procedure FreezeEnterprise(PId number, PModifierId number, PModifierName varchar2, PModifyTime date) as
  EnterpriseId integer;
begin
  begin
    select Id into EnterpriseId from Enterprise
     where Id = PId and Status = 3;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20005,'只允许冻结处于“生效”状态下的企业');
    when others then
      raise;
  end;
  update Enterprise set Status = 2, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime where Enterprise.Id = PId;
end;
/


create or replace function GetPersonnelRule(OrganizationName varchar2, RoleName varchar2, PersonnelName varchar2, PageName varchar2) return SYS_REFCURSOR
as
  Result SYS_REFCURSOR;
  my_sql varchar2(4000);
begin
  my_sql:= 'select Enterprise.Name as EnterpriseName,
                   Organization.Name as OrganizationName,
                   Personnel.LoginId as LoginId,
                   Personnel.Name as PersonnelName,
                   Role.Name as RoleName,
                   nvl(Page.Name,pa.Name) as PageName,
                   Action.Name as ActionName
              from Enterprise
              inner join Organization ON Enterprise.Id = Organization.EnterpriseId
              inner join OrganizationPersonnel ON Organization.Id = OrganizationPersonnel.OrganizationId
              inner join Personnel ON OrganizationPersonnel.PersonnelId = Personnel.Id
              inner join RolePersonnel ON Personnel.Id = RolePersonnel.PersonnelId
              inner join Role ON Role.Id = RolePersonnel.RoleId and Role.Status = 1
              inner join Rule ON Rule.RoleId = Role.Id
              inner join Node ON Node.Id = Rule.NodeId and Node.Status = 2
              inner join Page ON Page.Id = Node.CategoryId and Node.CategoryType = 0 and Page.Status = 2
              left join Action ON Action.Id = Node.CategoryId and Node.CategoryType = 1 and Action.Status = 2
              left join Page pa ON pa.Id = Action.PageId and pa.Status = 2
              where Enterprise.Status in (1, 3)
              and Organization.Status = 2
              and Personnel.Status = 1';
  if OrganizationName is not null then
    my_sql:= my_sql || ' and Organization.Name like ''%'||OrganizationName||'%''';
  end if;
  if RoleName is not null then
    my_sql:= my_sql || ' and Role.Name like ''%'||RoleName||'%''';
  end if;
  if PersonnelName is not null then
    my_sql:= my_sql || ' and Personnel.Name like ''%'||PersonnelName||'%''';
  end if;
  if PageName is not null then
    my_sql:= my_sql || ' and (Page.Name like ''%'||PageName||'%'' or pa.Name like ''%'||PageName||'%'')';
  end if;
  open Result for my_sql;
  return(Result);
end GetPersonnelRule;
/


create or replace procedure RecoverEnterprise(PId number, PModifierId number, PModifierName varchar2, PModifyTime date) as
  EnterpriseId integer;
begin
  begin
    select Id into EnterpriseId from Enterprise
     where Id = PId and Status = 2;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20006,'只允许恢复处于“冻结”状态下的企业');
    when others then
      raise;
  end;
  update Enterprise set Status = 3, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime where Enterprise.Id = PId;
end;
/


create or replace procedure ValidationEnterprise(PId number, PModifierId number, PModifierName varchar2, PModifyTime date) as
  EnterpriseId integer;
begin
  begin
    select Id into EnterpriseId from Enterprise
     where Id = PId and Status =1;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20007,'只允许生效处于“新建”状态下的企业');
    when others then
      raise;
  end;
  update Enterprise set Status = 3, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime where Enterprise.Id = PId;
end;
/

