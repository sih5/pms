/*==============================================================*/
/* Database name:  Security                                     */
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2013/9/6 11:18:52                            */
/*==============================================================*/



create or replace procedure AbandonEnterprise(PId number, PModifierId number, PModifierName varchar2, PModifyTime date) as
  EnterpriseId integer;
begin
  begin
    select Id into EnterpriseId from Enterprise
     where Id = PId and Status != 0;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20008,'只允许作废处于“新建”、“冻结”、“生效”状态下的企业');
    when others then
      raise;
  end;
  update Enterprise set Status = 0, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime where Enterprise.Id = PId;
  update Organization set Status = 0, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime where Organization.EnterpriseId = PId;
  update Personnel set Status = 0, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime where Personnel.EnterpriseId = PId;
  update Role set Status = 0, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime where Role.EnterpriseId = PId;
end;
/


create or replace procedure AddEnterprise(PId number, PCode varchar2, PName varchar2, PType number, PCreatorId number, PCreatorName varchar2, PCreateTime date) as
  EnterpriseId integer;
  OrganizationId integer;
  PersonnelId integer;
  RoleId integer;
begin
  begin
    select Id into EnterpriseId from Enterprise
     where Status != 0 and Code = PCode and rownum = 1;
    raise_application_error(-20001,'已存在相同编号的企业');
  exception
    when NO_DATA_FOUND then
      null;
    when others then
      raise;
  end;
  
  begin
    select Id into EnterpriseId from Enterprise
       where Status != 0 and Name = PName and rownum = 1;
      raise_application_error(-20002,'已存在相同名称的企业');
    exception
      when NO_DATA_FOUND then
        null;
      when others then
        raise;
  end;

  insert into Enterprise (Id, Code, Name, EnterpriseCategoryId, Status, CreatorId, CreatorName, CreateTime)
  values (PId, PCode, PName, PType, 1, PCreatorId, PCreatorName, PCreateTime);

  select S_Organization.Nextval into OrganizationId from dual;
  insert into Organization (Id, ParentId, Code, Name, Type, Sequence, Status, EnterpriseId, CreatorId, CreatorName, CreateTime)
  values (OrganizationId, 0, PCode, PName, 0, 0, 2, PId, PCreatorId, PCreatorName, PCreateTime);

  select S_Personnel.Nextval into PersonnelId from dual;
  insert into Personnel (Id, LoginId, Name, Password, PasswordModifyTime, Status, EnterpriseId, CreatorId, CreatorName, CreateTime)
  values (PersonnelId, 'admin', '企业管理员', '7C4A8D09CA3762AF61E59520943DC26494F8941B', sysdate, 1, PId, PCreatorId, PCreatorName, PCreateTime);

  select S_Role.Nextval into RoleId from dual;
  insert into Role (Id, Name, IsAdmin, Status, EnterpriseId, CreatorId, CreatorName, CreateTime)
  values (RoleId, '企业管理员', 1, 1, PId, PCreatorId, PCreatorName, PCreateTime);

  insert into OrganizationPersonnel(Id, OrganizationId, PersonnelId)
  values (S_OrganizationPersonnel.Nextval, OrganizationId, PersonnelId);

  insert into RolePersonnel(Id, RoleId, PersonnelId)
  values (S_RolePersonnel.Nextval, RoleId, PersonnelId);
  
  insert into Rule(Id, RoleId, NodeId)
  select S_Rule.Nextval, RoleId, Id
    from Node 
   where Node.Status = 2
     and (exists (select * from Page where Page.Id = Node.CategoryId and Page.Id < 1000 and Node.CategoryType = 0)
      or exists (select * from Action
                  where exists (select * from Page where Page.Id = Action.PageId and Page.Id < 1000)
                    and Action.Id = Node.CategoryId
                    and Node.CategoryType = 1));
end;
/


create or replace procedure EditEnterprise(PId number, PName varchar2, PType number, PModifierId number, PModifierName varchar2, PModifyTime date) as
  EnterpriseId integer;
begin
  begin
    select Id into EnterpriseId from Enterprise
     where Status != 0 and Name = PName and Id != PId and rownum = 1;
    raise_application_error(-20003,'已存在相同名称的企业');
  exception
    when NO_DATA_FOUND then
      null;
    when others then
      raise;
  end;
  
  begin
    select Id into EnterpriseId from Enterprise
     where Status = 0 and Id = PId;
    raise_application_error(-20004,'只允许更新处于“新建”、“冻结”、“生效”状态下的企业');
  exception
    when NO_DATA_FOUND then
      null;
    when others then
      raise;
  end;

  update Enterprise set Name = PName, EnterpriseCategoryId = PType, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime
   where Enterprise.Id = PId;

  update Organization set Name = PName, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime
   where Organization.EnterpriseId = PId and Organization.Type = 0;
end;
/


create or replace procedure FreezeEnterprise(PId number, PModifierId number, PModifierName varchar2, PModifyTime date) as
  EnterpriseId integer;
begin
  begin
    select Id into EnterpriseId from Enterprise
     where Id = PId and Status = 3;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20005,'只允许冻结处于“生效”状态下的企业');
    when others then
      raise;
  end;
  update Enterprise set Status = 2, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime where Enterprise.Id = PId;
end;
/


create or replace procedure RecoverEnterprise(PId number, PModifierId number, PModifierName varchar2, PModifyTime date) as
  EnterpriseId integer;
begin
  begin
    select Id into EnterpriseId from Enterprise
     where Id = PId and Status = 2;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20006,'只允许恢复处于“冻结”状态下的企业');
    when others then
      raise;
  end;
  update Enterprise set Status = 3, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime where Enterprise.Id = PId;
end;
/


create or replace procedure ValidationEnterprise(PId number, PModifierId number, PModifierName varchar2, PModifyTime date) as
  EnterpriseId integer;
begin
  begin
    select Id into EnterpriseId from Enterprise
     where Id = PId and Status =1;
  exception
    when NO_DATA_FOUND then
      raise_application_error(-20007,'只允许生效处于“新建”状态下的企业');
    when others then
      raise;
  end;
  update Enterprise set Status = 3, ModifierId = PModifierId, ModifierName = PModifierName, ModifyTime = PModifyTime where Enterprise.Id = PId;
end;
/

