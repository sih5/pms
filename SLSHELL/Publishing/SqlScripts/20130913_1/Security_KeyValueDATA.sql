TRUNCATE TABLE KeyValueItem
/

Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'SecurityCommon_Status', '权限系统基础数据状态', 0, '作废', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'SecurityCommon_Status', '权限系统基础数据状态', 2, '有效', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'Enterprise_Status', '权限系统企业状态', 0, '作废', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'Enterprise_Status', '权限系统企业状态', 1, '新建', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'Enterprise_Status', '权限系统企业状态', 2, '冻结', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'Enterprise_Status', '权限系统企业状态', 3, '生效', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'Page_Type', '页面级别', 0, '系统', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'Page_Type', '页面级别', 1, '分组', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'Page_Type', '页面级别', 2, '页面', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'Organization_OrganizationType', '组织类型', 0, '企业', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'Organization_OrganizationType', '组织类型', 1, '部门', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'Node_CategoryType', '节点类型', 0, '页面', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'Node_CategoryType', '节点类型', 1, '操作', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'Node_CategoryType', '节点类型', 2, '数据列', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'User_Status', '权限人员状态', 0, '作废', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'User_Status', '权限人员状态', 1, '有效', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'User_Status', '权限人员状态', 2, '冻结', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'Enterprise_Type', '企业类型', 0, '4S店', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'Enterprise_Type', '企业类型', 1, '二级网点', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'Role_Status', '权限角色状态', 0, '冻结', 1, 2);
Insert into KeyValueItem (Id ,Category, Name, Caption, Key, Value, IsBuiltIn, Status) Values (S_KeyValueItem.Nextval, 'SECURITY', 'Role_Status', '权限角色状态', 1, '有效', 1, 2);
/
