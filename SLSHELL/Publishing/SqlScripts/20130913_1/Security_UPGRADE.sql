/*==============================================================*/
/* Database name:  Security                                     */
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     2013/9/12 15:14:25                           */
/*==============================================================*/


drop procedure AddEnterprise
/


create or replace procedure AddEnterprise(PId number, PCode varchar2, PName varchar2, PType number, PCreatorId number, PCreatorName varchar2, PCreateTime date) as
  EnterpriseId integer;
  OrganizationId integer;
  PersonnelId integer;
  RoleId integer;
begin
  begin
    select Id into EnterpriseId from Enterprise
     where Status != 0 and Code = PCode and rownum = 1;
    raise_application_error(-20001,'已存在相同编号的企业');
  exception
    when NO_DATA_FOUND then
      null;
    when others then
      raise;
  end;
  
  begin
    select Id into EnterpriseId from Enterprise
       where Status != 0 and Name = PName and rownum = 1;
      raise_application_error(-20002,'已存在相同名称的企业');
    exception
      when NO_DATA_FOUND then
        null;
      when others then
        raise;
  end;

  insert into Enterprise (Id, Code, Name, EnterpriseCategoryId, Status, CreatorId, CreatorName, CreateTime)
  values (PId, PCode, PName, PType, 1, PCreatorId, PCreatorName, PCreateTime);

  select S_Organization.Nextval into OrganizationId from dual;
  insert into Organization (Id, ParentId, Code, Name, Type, Sequence, Status, EnterpriseId, CreatorId, CreatorName, CreateTime)
  values (OrganizationId, -1, PCode, PName, 0, 0, 2, PId, PCreatorId, PCreatorName, PCreateTime);

  select S_Personnel.Nextval into PersonnelId from dual;
  insert into Personnel (Id, LoginId, Name, Password, PasswordModifyTime, Status, EnterpriseId, CreatorId, CreatorName, CreateTime)
  values (PersonnelId, 'admin', '企业管理员', '7C4A8D09CA3762AF61E59520943DC26494F8941B', sysdate, 1, PId, PCreatorId, PCreatorName, PCreateTime);

  select S_Role.Nextval into RoleId from dual;
  insert into Role (Id, Name, IsAdmin, Status, EnterpriseId, CreatorId, CreatorName, CreateTime)
  values (RoleId, '企业管理员', 1, 1, PId, PCreatorId, PCreatorName, PCreateTime);

  insert into OrganizationPersonnel(Id, OrganizationId, PersonnelId)
  values (S_OrganizationPersonnel.Nextval, OrganizationId, PersonnelId);

  insert into RolePersonnel(Id, RoleId, PersonnelId)
  values (S_RolePersonnel.Nextval, RoleId, PersonnelId);
  
  insert into Rule(Id, RoleId, NodeId)
  select S_Rule.Nextval, RoleId, Id
    from Node 
   where Node.Status = 2
     and (exists (select * from Page where Page.Id = Node.CategoryId and Page.Id < 1000 and Node.CategoryType = 0)
      or exists (select * from Action
                  where exists (select * from Page where Page.Id = Action.PageId and Page.Id < 1000)
                    and Action.Id = Node.CategoryId
                    and Node.CategoryType = 1));
end;
/

