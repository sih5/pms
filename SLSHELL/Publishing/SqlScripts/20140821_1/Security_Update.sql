 /*    [+-]<Table Symbol> 授权规则履历
      [+-]<Table Symbol> 登录日志
  Tables:
    [+-]<Table> 登录日志
    [+-]<Table> 授权规则履历
  Sequences:
    [+-]<Sequence> S_登录日志
    [+-]<Sequence> S_授权规则履历*/


create sequence "S_RuleRecord"
/
create sequence "S_LoginLog"
/
/*==============================================================*/
/* Table: "RuleRecord"                                          */
/*==============================================================*/
create table "RuleRecord"  (
   "Id"                 NUMBER(9)                       not null,
   "RoleId"             NUMBER(9)                       not null,
   "NodeId"             NUMBER(9)                       not null,
   "Status"             NUMBER(9)                       not null,
   "CreatorId"          NUMBER(9),
   "CreatorName"        VARCHAR2(100),
   "CreateTime"         DATE,
   constraint PK_RULERECORD primary key ("Id")
)
/

/*==============================================================*/
/* Table: "LoginLog"                                            */
/*==============================================================*/
create table "LoginLog"  (
   "Id"                 NUMBER(9)                       not null,
   "PersonnelId"        NUMBER(9)                       not null,
   IP                   VARCHAR2(50),
   "Mac"                VARCHAR2(50),
   "LoginTime"          DATE                            not null,
   "LogoutTime"         DATE,
   constraint PK_LOGINLOG primary key ("Id")
)
/
